﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Veit.GsmWorker {
    /// <summary>
    /// Multiple background workers that continuosly read SMS from multiple modems
    /// </summary>
    public class SmsReadWorkers : SmsPeriodicWorkersBase {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="portList">List of COM ports where the GSM modems are connected</param>
        /// <param name="progressChanged"></param>
        /// <param name="runWorkerCompleted"></param>
        public SmsReadWorkers(List<int> portList, ProgressChangedEventHandler progressChanged, RunWorkerCompletedEventHandler runWorkerCompleted)
            : base(portList, progressChanged, runWorkerCompleted) {
        }

        /// <summary>
        /// Start all workers
        /// </summary>
        public void Start() {
            foreach (SmsWorker worker in workerList) {
                worker.StartReadAll();
            }
        }

    }
}
