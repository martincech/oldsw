﻿//******************************************************************************
//
//   Statistics.cs      Statistic calculations with same algorithms as in the scale
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;
using Veit.Scale;

namespace Veit.ScaleStatistics {
    
    /// <summary>
    /// Histogram mode
    /// </summary>
    public enum HistogramMode {
         RANGE,
         STEP,
    }
    
    /// <summary>
    /// Histogram parameters
    /// </summary>
    public struct HistogramConfig {
        public HistogramMode Mode;             // Mode of histogram
        public int           Range;		       // Histogram step in +/- perecents of average weight (or 0 if not used)
        public double        Step;		       // Histogram step in kg (or 0 if not used)
    }

    /// <summary>
    /// Histogram data
    /// </summary>
    public class Histogram {
        /// <summary>
        /// Number of slots in histogram
        /// </summary>
        public readonly int SlotsCount;

        /// <summary>
        /// Array of values
        /// </summary>
        public float[] Values;
        
        /// <summary>
        /// Array of counts
        /// </summary>
        public int[] Counts;

        /// <summary>
        /// Histogram step
        /// </summary>
        public float Step;

        public Histogram(int slotsCount) {
            SlotsCount = slotsCount;
            Values     = new float[slotsCount];
            Counts     = new int[slotsCount];
            Step       = 0;
        }
    }
    
    /// <summary>
    /// Statistic calculations
    /// </summary>
    public class Statistics {
        
        /// <summary>
        /// List of values for calculation
        /// </summary>
        private List<float> valueList = new List<float>();

        /// <summary>
        /// Sum of weights
        /// </summary>
        private float xSum;

        /// <summary>
        /// Sum of weight powers
        /// </summary>
        private float x2Sum;

        /// <summary>
        /// Constructor
        /// </summary>
        public Statistics() {
            Clear();
        }

        /// <summary>
        /// Clear all items
        /// </summary>
        public void Clear() {
            valueList.Clear();
            xSum  = 0;
            x2Sum = 0;
        }

        /// <summary>
        /// Add value
        /// </summary>
        /// <param name="value">Added value</param>
        public void Add(float value) {
            valueList.Add(value);
            xSum  += value;
            x2Sum += value * value;
        }
        
        /// <summary>
        /// Get number of values
        /// </summary>
        /// <returns>Number of values</returns>
        public int Count() {
            return valueList.Count;
        }

        /// <summary>
        /// Calculate average value
        /// </summary>
        /// <returns>Average value</returns>
        public float Average() {
            if (Count() == 0) {
                return 0;       // Prazdny seznam
            }
            
            return xSum / (float)Count();
        }

        /// <summary>
        /// Calculate standard deviation
        /// </summary>
        /// <returns>Standard deviation</returns>
        public float Sigma() {
            if (Count() <= 1) {
                return 0;       // Prazdny seznam nebo 1 hodnota
            }
            
            float tmp = xSum * xSum / (float)Count();
            tmp = x2Sum - tmp;
/*            if( tmp <= MIN_SIGMA){
                return 0.0F;
            }*/
            return (float)Math.Sqrt((double)(1.0F / (float)(Count() - 1) * tmp));
        }

        /// <summary>
        /// Calculate coefficient of variation
        /// </summary>
        /// <returns>Coefficient of variation in percents</returns>
        public float Variation(float average, float sigma) {
            if (average == 0) {
                return 0;       // Deleni nulou
            }
            return sigma / average * 100.0F;
        }

        /// <summary>
        /// Calculate uniformity
        /// </summary>
        /// <param name="Range">Range in percents</param>
        /// <returns>Uniformity</returns>
        public float Uniformity(int Range, float average) {
            if (Count() == 0) {
                return 100.0F;       // Prazdny seznam
            }
            
            float min = average * (float)(100 - Range) / 100.0F;
            float max = average * (float)(100 + Range) / 100.0F;
            int inside = 0, outside = 0;        // Pocet vzorku uvnitr a vne pasma uniformity

            foreach (float value in valueList) {
                if (value < min || value > max) {
                    outside++;      // Vzorek je mimo pasmo
                } else {
                    inside++;       // Vzorek je v pasmu
                }
            }

            return 100.0F * (float)inside / (float)(inside + outside);
        }

        /// <summary>
        /// Normalized histogram steps in grams
        /// </summary>
        private int [] NORMALIZED_STEPS_G = {1, 2, 5, 10, 15, 20, 25, 30, 40, 50, 60, 70, 80, 90, 100, 150, 200, 250, 300, 400, 500, 600, 700, 800, 900, 1000};

        /// <summary>
        /// Normalize histogram step in grams
        /// </summary>
        /// <param name="step">Step in grams</param>
        /// <returns>Normalized step in grams</returns>
        private int NormalizedStepInGrams(float step) {
            for (int i = 0; i < NORMALIZED_STEPS_G.Length; i++) {
                if (step <= (float)NORMALIZED_STEPS_G[i]) {
                    return NORMALIZED_STEPS_G[i];
                }
            }
            return (int)step;        // Vetsi nez maximalni normalizovany krok
        }
        
        /// <summary>
        /// Normalize histogram step
        /// </summary>
        /// <param name="step">Original step</param>
        /// <param name="units">Units used</param>
        /// <returns>Normalized step</returns>
        private float NormalizeStep(float step, Units units) {
            // Prevedu zadany krok na gramy, aby se mne dobre pracovalo s polem
            float stepGrams;
            if (units != Units.G) {
                stepGrams = 1000.0F * step;
            } else {
                stepGrams = step;
            }

            // Vypoctu normalizovany krok v gramech
            stepGrams = NormalizedStepInGrams(stepGrams);

            // Prepoctu zpet na puvodni jednotky
            if (units != Units.G) {
                step = stepGrams / 1000.0F;
            } else {
                step = stepGrams;
            }

            // Vratim
            return step;
        }

        /// <summary>
        /// Calculate histogram
        /// </summary>
        /// <param name="step">Step of histogram</param>
        /// <param name="values">Array of bar values</param>
        /// <param name="counts">Array of value counts</param>
        public Histogram Histogram(HistogramConfig histogramConfig, Units units, int slotsCount) {
            if (slotsCount < 3) {
                // Histogram musi mit minimalne 3 sloupce (mimo pod, 1 sloupec a mimo nad)
                throw new Exception("Histogram must have minimum 3 slots");         
            }

            Histogram histogram = new Histogram(slotsCount);

            float average = Average();

            // Vypoctu krok
            float step;
            if (histogramConfig.Mode == HistogramMode.RANGE) {
                // Rozsah z prumeru
                step = (average * 2.0F * (float)histogramConfig.Range) / (float)(100 * slotsCount);
            } else {
                // Je zadany primo krok
                step = (float)histogramConfig.Step;
            }
            
            // Radeji omezim minimalni krok na 1g
            float minStep;
            if (units == Units.G) {
                minStep = 1;
            } else {
                // Kg nebo Lb beru nastejno
                minStep = 0.001F;
            }
            if (step < minStep) {
                step = minStep;
            }

            // Normalizuju krok
             step = NormalizeStep(step, units);

            // Ulozim vypocteny krok
            histogram.Step = step;

            // Normalizuju stred histogramu (az po normalizaci kroku)
            average += step / 2.0F;
            average = (int)(average / step);
            average *= step;

            // Vyplnim hodnoty, vcetne krajnich sloupcu. Ty jsou sice mimo rozsah, ale pro jednoduche
            // vykreslovani v grafu je vyplnim take

            // Hodnota prvniho sloupce
            if (slotsCount % 2 == 0) {
                // Sudy pocet sloupcu
                histogram.Values[0] = average - ((float)slotsCount / 2.0F - 0.5F) * step;
            } else {
                // Lichy pocet
                histogram.Values[0] = average - (((float)slotsCount - 1.0F) / 2.0F) * step;
            }

            // Osetrim, aby levy sloupec nesel do zapornych hodnot
            if (histogram.Values[0] < 0) {
                histogram.Values[0] = 0;
            }
            
            // Hodnoty ostatnich sloupcu nastavim se zadanym krokem
            for (int i = 1; i < slotsCount; i++) {
                histogram.Values[i] = histogram.Values[i - 1] + step;
            }

            // Vyplnim vysky histogramu
            foreach (float value in valueList) {
                if (value < histogram.Values[1] - step / 2.0) {
                    // Pod rozsahem
                    histogram.Counts[0]++;
                    continue;
                }
                if (value > histogram.Values[slotsCount - 2] + step / 2.0) {
                    // Nad rozsahem
                    histogram.Counts[slotsCount - 1]++;
                    continue;
                }
                // V rozsahu
                for (int i = 1; i < slotsCount - 1; i++) {
                    if (value < histogram.Values[i] + step / 2.0) {
                        histogram.Counts[i]++;
                        break;
                    }
                }
            }

            return histogram;
        }

    }
}