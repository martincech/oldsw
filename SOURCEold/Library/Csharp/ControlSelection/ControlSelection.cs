﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Veit.ControlSelection {
    /// <summary>
    /// Selection of controls
    /// </summary>
    public class ControlSelection {
        /// <summary>
        /// Select row in a DataGridView
        /// </summary>
        /// <param name="index">Row index</param>
        public static void SelectRow(DataGridView dataGridView, int index) {
            if (dataGridView.Rows.Count == 0) {
                return;     // Tabulka je prazdna
            }
            if (index < 0) {
                return;     // Zadny vyber
            }
            if (index >= dataGridView.Rows.Count) {
                index = dataGridView.Rows.Count - 1;     // Omezim
            }
            
            // Presunu kurzor na vybrany radek
            dataGridView.ClearSelection();
            dataGridView.Rows[index].Selected = true;

            // Pokud je vybrany radek mimo zobrazeni, presunu se na nej. Pokud radek zobrazeny je,
            // nikam nepresouvam a necham vybrany radek treba uprostred tabulky, jinak to vypada blbe.
            // Pozor, dataGridView.DisplayedRowCount(false) vraci pri prvnim zobrazeni nulu, i kdyz uz jsou v Gridu
            // nahrana hejna. Hazelo to vyjimku s jednim hejnem, kdy se nastavil FirstDisplayedScrollingRowIndex na 1.
            // Zrejme se DisplayedRowCount nastavi az po prvnim vykresleni.
            int displayedRowCount = dataGridView.DisplayedRowCount(false);
            if (displayedRowCount > 0 && index >= dataGridView.FirstDisplayedScrollingRowIndex + displayedRowCount - 1) {
                // Vybrany radek je pod, nastavim jako predposledni
                dataGridView.FirstDisplayedScrollingRowIndex = index - displayedRowCount + 1;
            }
            if (index < dataGridView.FirstDisplayedScrollingRowIndex) {
                // Vybrany radek je nad, nastavim ho jako prvni
                dataGridView.FirstDisplayedScrollingRowIndex = index;
            }

            // Dam tabulce focus
            dataGridView.Focus();
        }
    }
}
