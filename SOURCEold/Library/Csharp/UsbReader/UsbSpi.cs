﻿using System;
using System.Collections.Generic;
using System.Text;
using FTD2XX_NET;

namespace Veit.UsbReader {
    // SPI Clock idle level :
    public enum ClockPolarity {
        CLOCK_LOW,
        CLOCK_HIGH
    }
    // SPI Read strobe :
    public enum ReadStrobe {
        READ_ON_FALL,
        READ_ON_RISE
    }
    // SPI Write strobe :
    public enum WriteStrobe {
        WRITE_ON_FALL,
        WRITE_ON_RISE
    }

    public class UsbSpi {
        private FTDI ftdiDevice;
        private string deviceName;
        private string serialNumber;
        private int txLatency;
        private int identifier;
        private int clockRate;         // SPI clock rate
        private bool clockHigh;         // SPI clock idle level
        private byte modeMask;          // SPI mode mask

        private const int INVALID_IDENTIFIER = -1;
        private const int DEFAULT_LATENCY = 16;                       // default latency is 16ms

        // Device pins :
        private const byte SPI_SK = 0x01;                 // ADBUS0
        private const byte SPI_DO = 0x02;                 // ADBUS1
        private const byte SPI_DI = 0x04;                 // ADBUS2
        private const byte SPI_CS = 0x08;                 // ADBUS3
        private const byte SPI_DIRECTION = (SPI_SK | SPI_DO | SPI_CS);  // '0' == input, '1' == output

        // Implementation limits :
        private const int MAX_BUFFER = 600;

        public bool IsOpen { get { return ftdiDevice.IsOpen; } }

        public UsbSpi() {
            ftdiDevice = new FTDI();

            identifier = INVALID_IDENTIFIER;
            txLatency  = DEFAULT_LATENCY;
        }

        private int GetNumberOfDevices() {
            FTDI.FT_STATUS status;
            uint deviceCount = 0;
            
            // Determine the number of FTDI devices connected to the machine
            status = ftdiDevice.GetNumberOfDevices(ref deviceCount);
            // Check status
            if (status != FTDI.FT_STATUS.FT_OK) {
                return 0;
            }

            return (int)deviceCount;
        }

        private FTDI.FT_DEVICE_INFO_NODE[] GetDeviceArray() {
            int deviceCount = GetNumberOfDevices();
            FTDI.FT_DEVICE_INFO_NODE[] deviceArray = new FTDI.FT_DEVICE_INFO_NODE[deviceCount];

            if (deviceCount == 0) {
                return deviceArray;
            }

            // Populate our device list
            ftdiDevice.GetDeviceList(deviceArray);
            return deviceArray;
        }

        public bool Locate(string name, out int identifier) {
            // Find device by <Name>, returns <Identifier>

            FTDI.FT_DEVICE_INFO_NODE[] deviceArray = GetDeviceArray();
            identifier = 0;

            if (deviceArray.Length == 0) {
                return false;
            }

            int count = 0;
            for (int i = 0; i < deviceArray.Length; i++) {
                FTDI.FT_DEVICE_INFO_NODE device = deviceArray[i];
                if (device == null) {
                    continue;
                }
                if (device.Description == name) {
                    count++;
                    identifier = i;
                }
            }

            if (count == 0) {
                return false;                  // no device
            }
            if (count > 1) {
                return false;                  // more devices
            }
            return true;
        }

        private bool Access(int identifier) {
            // Check device access by <Identifier>
            if (ftdiDevice.OpenByIndex((uint)identifier) != FTDI.FT_STATUS.FT_OK) {
                return false;
            }
            ftdiDevice.Close();
            return true;
        }

        public bool Open(int identifier) {
            // Open device by <Identifier>
            if (ftdiDevice.IsOpen) {
                ftdiDevice.Close();                  // close previous
            }
           
            if (ftdiDevice.OpenByIndex((uint)identifier) != FTDI.FT_STATUS.FT_OK) {
                return false;
            }

            if (ftdiDevice.GetDescription(out deviceName) != FTDI.FT_STATUS.FT_OK) {
                return false;
            }

            if (ftdiDevice.GetSerialNumber(out serialNumber) != FTDI.FT_STATUS.FT_OK) {
                return false;
            }

            // USB timeouts :
            if (ftdiDevice.SetTimeouts(50, 50) != FTDI.FT_STATUS.FT_OK) {
                return false;
            }
            if (ftdiDevice.SetLatency((byte)txLatency) != FTDI.FT_STATUS.FT_OK) {
                return false;
            }

            this.identifier = identifier;

            // Reset device :
            if (ftdiDevice.SetBitMode(0, 0) != FTDI.FT_STATUS.FT_OK) {
                return false;
            }

            // Set MPSSE mode :
            if (ftdiDevice.SetBitMode(0xFF, 2) != FTDI.FT_STATUS.FT_OK) {       // all IO to output, MPSSE code = 2
                return false;
            }
            
            return true;
        }

        public void Close() {
            // Close device
            if (IsOpen) {
                ftdiDevice.Close();
            }
            identifier = INVALID_IDENTIFIER;
        }

        public bool SetClockRate(int hz) {
            // Set clock rate [Hz]
            if (!IsOpen) {
                return false;
            }
            if ((hz > 6000000) || (hz < 90)) {
                return false;
            }
            
            clockRate = hz;                    // remember
           
            // write to device :
            byte[] buffer = new byte[3];
            ushort value = (ushort)((6000000 / hz) - 1);    // encode dividier
            buffer[0] = 0x86;                  // set TCK/SK divisor
            buffer[1] = (byte)(value & 0xFF);          // Value low
            buffer[2] = (byte)(value >> 8);            // Value High
            uint bytesWritten = 0;
            return (ftdiDevice.Write(buffer, 3, ref bytesWritten) == FTDI.FT_STATUS.FT_OK);
        }

        public void SetMode(ClockPolarity polarity, ReadStrobe readStrobe, WriteStrobe writeStrobe) {
            // Set SPI mode
            clockHigh = (polarity == ClockPolarity.CLOCK_HIGH);
            modeMask  = 0;
            if (readStrobe  == ReadStrobe.READ_ON_FALL) {
                modeMask |= 0x04;
            }
            if (writeStrobe == WriteStrobe.WRITE_ON_FALL) {
                modeMask |= 0x01;
            }
        }

        public bool ClrCS() {
            // Device chipselect L
            if (!IsOpen) {
                return false;
            }
            byte value = clockHigh ? SPI_SK : (byte)0;    // CS active Low
            byte[] buffer = new byte[3];
            buffer[0] = 0x80;            // set data bits Low Byte
            buffer[1] = value;           // Value
            buffer[2] = SPI_DIRECTION;   // Direction
            uint bytesWritten = 0;
            return (ftdiDevice.Write(buffer, 3, ref bytesWritten) == FTDI.FT_STATUS.FT_OK);
        }

        public bool SetCS() {
            // Device chipselect H
            if (!IsOpen) {
                return false;
            }
            byte value = (byte)((clockHigh ? SPI_SK : (byte)0) | SPI_CS);    // CS active Low
            byte[] buffer = new byte[3];
            buffer[0] = 0x80;            // set data bits Low Byte
            buffer[1] = value;           // Value
            buffer[2] = SPI_DIRECTION;   // Direction
            uint bytesWritten = 0;
            return (ftdiDevice.Write(buffer, 3, ref bytesWritten) == FTDI.FT_STATUS.FT_OK);
        }

        public bool Write(byte[] data, int length) {
            // Write <Data> of size <Length>
            if (!IsOpen) {
                return false;
            }
            if (length > MAX_BUFFER) {
                return false;
            }
            byte[] buffer = new byte[MAX_BUFFER + 3];
            ushort tmpLength = (ushort)(length - 1);        // Encode length
            buffer[0] = (byte)(modeMask | 0x10);      // Clock data Out
            buffer[1] = (byte)(tmpLength & 0xFF);      // LengthL
            buffer[2] = (byte)(tmpLength >> 8);        // LengthH
            for (int i = 0; i < length; i++) {
                buffer[i + 3] = data[i];                // Data
            }
            return Send(buffer, length + 3); // send write command & data
        }

        public bool Read(byte[] data, int length) {
            // Read data <Data> of size <Length>
            if (!IsOpen) {
                return false;
            }
            byte[] buffer = new byte[16];
            ushort tmpLength = (ushort)(length - 1);        // Encode length
            buffer[0] = (byte)(modeMask | 0x20);      // Clock data In
            buffer[1] = (byte)(tmpLength & 0xFF);      // LengthL
            buffer[2] = (byte)(tmpLength >> 8);        // LengthH
            if (!Send(buffer, 3)){             // send read command
                return false;                  // unable send
            }
            int retLength = Receive(data, length);
            if (retLength != length){
                return false;
            }
            return true;
        }

        public bool WriteByte(byte data) {
            // Write single byte
            byte[] dataArray = new byte[1];
            dataArray[0] = data;
            return Write(dataArray, 1);
        }

        public bool ReadByte(out byte data) {
            // Read single byte
            byte[] dataArray = new byte[1];
            if (!Read(dataArray, 1)) {
                data = 0;
                return false;
            }
            data = dataArray[0];
            return true;
        }

        public void Flush() {
            // Make input/output queue empty
            if (!IsOpen) {
                return;
            }
            // Wait for data :
            for( int i = 0; i < 5; i++){
                if (ftdiDevice.Purge(FTDI.FT_PURGE.FT_PURGE_RX | FTDI.FT_PURGE.FT_PURGE_RX) != FTDI.FT_STATUS.FT_OK) {
                    System.Threading.Thread.Sleep(50);
                    continue;
                }
                return;
            }
            throw new Exception("Cannot make Flush");
        }

        string GetName() {
            // Get device name by <Identifier>
            if( !IsOpen){
                return( "?");
            }
            return(deviceName);
        }

        void SetTxLatency( int Latency) {
            // Set devices output buffer latency
            if( Latency < 1 || Latency > 255){
                txLatency = DEFAULT_LATENCY;             // out of range, set default
            } else {
                txLatency = Latency;
            }
            // device latency
            if (ftdiDevice.SetLatency((byte)txLatency) != FTDI.FT_STATUS.FT_OK) {
                txLatency = DEFAULT_LATENCY;             // error, remember default
            }
        }

        bool Send(byte[] data, int length) {
            // Send <Data> of size <Length>
            if (!IsOpen) {
                return false;
            }
            uint bytesWritten = 0;
            if (ftdiDevice.Write(data, length, ref bytesWritten) != FTDI.FT_STATUS.FT_OK) {
                Close();
                return false;
            }
            return (bool)(bytesWritten == length);
        }

        int Receive(byte[] data, int length) {
            // Receive data <Data> of size <Length>, returns Rx length
            if (!IsOpen) {
                return 0;
            }
            uint bytesRead = 0;
            if (ftdiDevice.Read(data, (uint)length, ref bytesRead) != FTDI.FT_STATUS.FT_OK) {
                return 0;
            }
            return (int)bytesRead;
        }

    }
}
