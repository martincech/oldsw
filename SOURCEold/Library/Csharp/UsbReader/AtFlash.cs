﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veit.UsbReader {
    class AtFlash : At45dbxx {
        public const int FLASH_VIRTUAL_PAGE = 512;                       // virtual page size
        public const int FLASH_SIZE = (FLASH_PAGES * FLASH_VIRTUAL_PAGE);// memory size
        
        private const uint FLASH_FLUSH_ADDRESS = 0xFFFFFFFE;    // invalid virtual address - flush
        private const ushort INVALID_PAGE      = 0xFFFF;         // invalid page address

        // AT45DB161 only :
        private ushort MkBufferAddress(uint a) { return (ushort)(a & 0x1FF); }
        private ushort MkPageAddress(uint a) { return (ushort)(a >> 9); }

        // write context address :
        private ushort WritePage;
        private ushort WriteOffset;
        // read context address :
        private ushort ReadPage;
        private ushort ReadOffset;

        public AtFlash() : base() {
           WritePage = INVALID_PAGE;
           WriteOffset = 0;
           ReadPage  = INVALID_PAGE;
           ReadOffset  = 0;
        } // TAtFlash

        public bool Read(int Address, out byte[] Data, int Length) {
            // Read from virtual <Address>
            if (Length == 0) {
                Data = null;
                return false;
            }
           ReadPage   = MkPageAddress(   (uint)Address);
           ReadOffset = MkBufferAddress( (uint)Address);
           int Size    = FLASH_VIRTUAL_PAGE - ReadOffset;         // up to end of page
           int Index   = 0;                                        // into Data array
           if( Size > Length){
              Size = Length;                                       // read inside page
           }
            Data = new byte[Length];
           do {
              byte[] array = new byte[Size];
              if( !ReadArray( ReadPage, ReadOffset, array, Size)){
                 return( false);
              }
                for (int i = 0; i < Size; i++) {
                    Data[Index + i] = array[i];
                }
              Length -= Size;                  // remainder of data
              Index  += Size;                  // move data pointer
              // next page :
              ReadPage++;
              ReadOffset = 0;
              // next fragment :
              Size = Length;
              if( Size > FLASH_VIRTUAL_PAGE){
                 Size = FLASH_VIRTUAL_PAGE;    // cut to page size
              }
           } while( Length > 0);
           return( true);
        } // Read

        public bool Write( ushort Address, byte[] Data, int Length) {
            // Write to virtual <Address>
           if( Length == 0){
              return false;
           }
           ushort NewPage = MkPageAddress( Address);
           if( NewPage != WritePage){
              // page change
              SwapBuffer( WritePage, NewPage);
           } // else same page
           WritePage   = NewPage;
           WriteOffset = MkBufferAddress( Address);
           int Size    = FLASH_VIRTUAL_PAGE - WriteOffset;         // up to end of page
           int Index   = 0;                    // into Data array
           if( Size > Length){
              // does fit inside page 
              Size = Length;
           } else {
              // at least one-times up to end of page
              do {

                  
                  byte[] array = new byte[Size];
                  for (int i = 0; i < Size; i++) {
                     array[i] = Data[Index + i];
                  }
                  
                  // write into buffer :
                 if( !WriteBuffer( WriteOffset, array, Size)){
                    return( false);
                 }
                 // save buffer {
                 if( !SwapBuffer( (ushort)WritePage, (ushort)(WritePage + 1))){
                    return( false);
                 }
                 Length -= Size;               // remainder of data
                 Index  += Size;               // move data pointer
                 // next page :
                 WritePage++;
                 WriteOffset = 0;
                 // next fragment :
                 Size = Length;
                 if( Size > FLASH_VIRTUAL_PAGE){
                    Size = FLASH_VIRTUAL_PAGE; // cut to page size
                 }
              } while( Size == FLASH_VIRTUAL_PAGE);
              // remainder doesn't fill whole page
           }
           // test remainder of page :
           if( Size > 0){
              byte[] array = new byte[Size];
              for (int i = 0; i < Size; i++) {
                 array[i] = Data[Index + i];
              }
              // write page fragment into buffer only :
              if( !WriteBuffer( WriteOffset, array, Size)){
                 return( false);
              }
              WriteOffset += (ushort)Size;
           } else {
              WritePage = INVALID_PAGE;        // new page without modification, make it invalid
           }
           return( true);
        } // Write

        public bool Flush() {
            // Flush all written data
           if( !SwapBuffer( WritePage, INVALID_PAGE)){
              return( false);
           }
           WritePage = INVALID_PAGE;
           return( true);
        } // Flush

        private bool SwapBuffer( ushort OldPage, ushort NewPage) {
            // Save <OldPage>, read <NewPage>
           if( OldPage < FLASH_PAGES){
              // in the range, save the old page
              if( !SaveBuffer( OldPage)){
                 return( false);
              }
              if( !WaitForReady()){
                 return( false);
              }
           }
           if( NewPage >= FLASH_PAGES){
              return( true);                   // out of capacity, don't load the new page
           }
           // load the new page into buffer :
           if( !LoadBuffer( NewPage)){
              return( false);
           }
           if( !WaitForReady()){
              return( false);
           }
           return( true);
        } // SwapBuffer



    }
}
