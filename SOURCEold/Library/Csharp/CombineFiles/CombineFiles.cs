//******************************************************************************
//
//   CombineFiles.cs    Combining more files to one file
//   Version 1.0, Petr Veit
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;                                // Work with files

namespace Veit.CombineFiles {
    class CombineFiles {

        #region Fields
        private string name;                                    // Name of the combination
        private string version;                                 // Version
        private List<string> fileList = new List<string>();     // File list
        #endregion

        #region Constuctors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="newName">Name of the combination</param>
        /// <param name="newVersion">Version string</param>
        public CombineFiles(string newName, string newVersion) {
            name    = newName;
            version = newVersion;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Add new file to the file list
        /// </summary>
        /// <param name="fileName">File name</param>
        public bool Add(string fileName) {
            // Zkontroluju, zda soubor existuje
            if (!File.Exists(fileName)) {
                return false;       // Soubor neexistuje
            }

            // Pridam soubor do seznamu
            fileList.Add(fileName);

            return true;
        }

        /// <summary>
        /// Save all files to one file
        /// </summary>
        /// <param name="fileName">File name</param>
        public bool Save(string fileName) {
            // Zkontroluju, zda uz soubor neexistuje
            if (File.Exists(fileName)) {
                return false;
            }

            try {
                // Zalozim novy soubor pro zapis
                FileStream stream = new FileStream(fileName, FileMode.CreateNew, FileAccess.Write);

                // Zapisu vsechna data
                using (BinaryWriter writer = new BinaryWriter(stream, Encoding.UTF8)) {
                    // Jmeno, verze a pocet souboru
                    writer.Write(name);
                    writer.Write(version);
                    writer.Write(fileList.Count);
                    
                    // Jednotlive soubory
                    foreach (string file in fileList) {
                        FileInfo info = new FileInfo(file);
                        writer.Write(info.Name);                    // Nazev souboru bez cesty
                        writer.Write((int)info.Length);             // Delka souboru
                        byte[] data = File.ReadAllBytes(file);      // Obsah souboru
                        writer.Write(data);
                    }
                    writer.Close();
                }
            }
            catch {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Read header name from file [fileName]
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>Name read from the file</returns>
        public static string ReadHeaderName(string fileName) {
            string name;

            // Otevru soubor pro cteni
            FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);

            // Nactu nazev kompilace
            using (BinaryReader reader = new BinaryReader(stream, Encoding.UTF8)) {
                name = reader.ReadString();
            }

            return name;
        }
        
        /// <summary>
        /// Extract all files from file [fileName] to path [destinationPath]
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="destinationPath">Destination path for the extracted files</param>
        /// <returns></returns>
        public static bool Load(string fileName, string destinationPath) {
            int fileCount;
            int fileSize;

            try {
                // Pokud adresar neexistuje, zalozim novy
                if (!Directory.Exists(destinationPath)) {
                    Directory.CreateDirectory(destinationPath);
                }

                // Otevru soubor pro cteni
                FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
               
                using (BinaryReader reader = new BinaryReader(stream)) {
                    // Preskocim nazev a verzi
                    reader.ReadString();
                    reader.ReadString();

                    // Nactu pocet souboru
                    fileCount = reader.ReadInt32();

                    // Nactu jednotlive soubory
                    for (int i = 0; i < fileCount; i++) {
                        // Nactu udaje o souboru
                        fileName = reader.ReadString();             // Nazev souboru, vyuziju jiz existujici fileName
                        fileSize = reader.ReadInt32();              // Delka souboru
                        byte[] data = reader.ReadBytes(fileSize);   // Obsah souboru

                        // Vytvorim novy soubor a ulozim do nej data (pokud soubor existuje, prepisu ho)
                        using (FileStream writer = new FileStream(destinationPath + fileName, FileMode.Create, FileAccess.Write)) {
                            writer.Write((byte[])data, 0, fileSize);
                        }
                    }
                }
            } catch {
                return false;
            }
            return true;
        }
        #endregion

    } // class CombineFiles
} // namespace Veit.CombineFiles
