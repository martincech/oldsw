﻿//******************************************************************************
//
//   StructConversion.cs      Conversion of byte array to struct
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Veit.StructConversion {
    
    public static class StructConversion {
    
        /// <summary>
        /// Convert a byte array to a struct
        /// </summary>
        /// <typeparam name="T">Struct type</typeparam>
        /// <param name="data">Data in array of bytes</param>
        /// <returns>New struct instance</returns>
        public static T ArrayToStruct<T>(byte[] data) where T : struct {
            // Zjistim adresu pole v pameti, zaroven zakazu Garbage Collectoru, aby pole v pameti premistoval
            GCHandle gch = GCHandle.Alloc(data, GCHandleType.Pinned);
            try {
                // Prevedu pole na strukturu a vratim
                return (T)Marshal.PtrToStructure(gch.AddrOfPinnedObject(), typeof(T));
            } finally {
                // Povolim opet Garbage Collectoru manipulaci s polem (je treba uvolnit rucne)
                gch.Free();
            }
        }

        /// <summary>
        /// Calculate size of struct
        /// </summary>
        /// <param name="t">Type</param>
        /// <returns>Size in bytes</returns>
        public static int SizeOf(Type t) {
            return Marshal.SizeOf(t);
        }

    }
}