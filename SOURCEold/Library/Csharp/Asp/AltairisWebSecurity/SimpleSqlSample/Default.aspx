﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Altairis.Web.Security - Simple SQL Providers Sample Web Site</title>
    <link rel="stylesheet" type="text/css" href="~/Images/StyleSheet.css" />
</head>
<body>
    <form id="form1" runat="server">
    <h1>Altairis.Web.Security - Simple SQL Providers Sample Web Site</h1>
    <a href="http://www.codeplex.com/AltairisWebSecurity/">
        <img src="Images/CP_banner_75x45_hstd.jpg" alt="Project hosted by CodePlex" width="75" height="45" style="float: right; border: none;" /></a>
    <p>This web site demonstrates use and configuration of <i>Simple SQL Providers</i> from <a href="http://www.codeplex.com/AltairisWebSecurity/">Altairis.Web.Security</a>, made by <a href="http://www.altairis.cz/">Altairis</a>.</p>
    <p>For login, use predefined user <strong>demouser</strong> with password <strong>demopass</strong> or create new account.</p>
    <table>
        <tr>
            <td valign="top">
                <asp:Login ID="Login1" runat="server"></asp:Login>
            </td>
            <td>
                <asp:CreateUserWizard ID="CreateUserWizard1" runat="server">
                    <WizardSteps>
                        <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server"></asp:CreateUserWizardStep>
                        <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server"></asp:CompleteWizardStep>
                    </WizardSteps>
                </asp:CreateUserWizard>
            </td>
        </tr>
    </table>
    </form>
    <div class="footer">
        <a href="http://www.codeplex.com/AltairisWebSecurity/">Altairis.Web.Security - Simple SQL Providers</a> | &copy; <a href="http://www.rider.cz/">Michal A. Valasek</a> - <a href="http://www.altairis.cz/">Altairis</a>, 2006-2009
    </div>
</body>
</html>
