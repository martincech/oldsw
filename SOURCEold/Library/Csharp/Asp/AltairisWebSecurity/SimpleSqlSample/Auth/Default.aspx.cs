using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Auth_Default : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        if (!IsPostBack) {
            this.TextBoxFullName.Text = Profile.FullName;
            this.TextBoxBirthDate.Text = Profile.BirthDate.ToShortDateString();
            this.TextBoxPageSize.Text = Profile.PageSize.ToString();
        }
    }
    protected void ButtonSubmit_Click(object sender, EventArgs e) {
        if (!IsValid) return;

        Profile.FullName = this.TextBoxFullName.Text;
        Profile.BirthDate = DateTime.Parse(this.TextBoxBirthDate.Text);
        Profile.PageSize = int.Parse(this.TextBoxPageSize.Text);
        Profile.Save();

        this.MultiViewPage.SetActiveView(this.ViewResult);
    }
}
