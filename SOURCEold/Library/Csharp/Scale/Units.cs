﻿//******************************************************************************
//
//   Scale.cs       Scale definitions
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;

namespace Veit.Scale {
    /// <summary>
    /// Weighing units
    /// </summary>
    public enum Units {
        KG,
        G,
        LB,
    };
}