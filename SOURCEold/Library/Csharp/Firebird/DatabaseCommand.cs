﻿using System;
using System.Collections.Generic;
using System.Text;
using FirebirdSql.Data.FirebirdClient;
using System.Data.Common;

namespace Veit.Firebird {

    /// <summary>
    /// Firebird database command with parameters
    /// </summary>
    public partial class DatabaseCommand : IDisposable {

        /// <summary>
        /// Command used for insertion
        /// </summary>
        private FbCommand command;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connection">Database connection to use</param>
        public DatabaseCommand(DatabaseConnection connection) {
            command = connection.Connection.CreateCommand();
            if (connection.Transaction != null) {
                // Pouziva transakci, ulozim ji k prikazu
                command.Transaction = connection.Transaction;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connection">Database connection to use</param>
        /// <param name="commandText">SQL command text</param>
        public DatabaseCommand(string commandText, DatabaseConnection connection) : this(connection) {
            SetCommand(commandText);
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~DatabaseCommand() {
            // Call Dispose with false. Since we're in the destructor call,
            // the managed resources will be disposed of anyways.
            Dispose(false);
        }

        /// <summary>
        /// Set new command
        /// </summary>
        /// <param name="commandText">SQL command</param>
        public void SetCommand(string commandText) {
            command.CommandText = commandText;
        }
        
        /// <summary>
        /// Clear all parameters
        /// </summary>
        public void ClearParameters() {
            command.Parameters.Clear();
        }

        /// <summary>
        /// Add 32bit integer value
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Value</param>
        public void AddField(string fieldName, int value) {
            command.Parameters.Add("@" + fieldName, FbDbType.Integer).Value = value;
        }

        /// <summary>
        /// Add 16bit integer value
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Value</param>
        public void AddField(string fieldName, short value) {
            command.Parameters.Add("@" + fieldName, FbDbType.SmallInt).Value = value;
        }

        /// <summary>
        /// Add double value
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Value</param>
        public void AddField(string fieldName, double value) {
            command.Parameters.Add("@" + fieldName, FbDbType.Double).Value = value;
        }

        /// <summary>
        /// Add timestamp value
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Value</param>
        public void AddField(string fieldName, DateTime value) {
            command.Parameters.Add("@" + fieldName, FbDbType.TimeStamp).Value = value;
        }

        /// <summary>
        /// Add string value
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Value</param>
        /// <param name="length">Text length</param>
        public void AddField(string fieldName, string value, int length) {
            command.Parameters.Add("@" + fieldName, FbDbType.VarChar, length).Value = value;
        }

        /// <summary>
        /// Execute non query
        /// </summary>
        /// <returns>Number of rows affected</returns>
        public int ExecuteNonQuery() {
            return command.ExecuteNonQuery();
        }

        /// <summary>
        /// Execute reader
        /// </summary>
        /// <returns>Reader</returns>
        public DbDataReader ExecuteReader() {
            return command.ExecuteReader();
        }

        /// <summary>
        /// Execute scala
        /// </summary>
        /// <returns>Scalar</returns>
        public Object ExecuteScalar() {
            return command.ExecuteScalar();
        }

    }

    
    // ---------------------------- IDisposable --------------------------------
    
    public partial class DatabaseCommand : IDisposable {
        // Track whether Dispose has been called
        private bool disposed = false;

        public void Dispose() {
            // Dispose of the managed and unmanaged resources
            Dispose(true);

            // Tell the GC that the Finalize process no longer needs to be run for this object
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposeManagedResources) {
            // Process only if mananged and unmanaged resources have not been disposed of
            if (!this.disposed) {
                if (disposeManagedResources) {
                    // Dispose managed resources:
                    command.Dispose();
                }

                // Dispose unmanaged resources:

                // Note disposing has been done
                disposed = true;
            }
        }
    }

}


