﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using Veit.Endian;

namespace Veit.Bat2Flash {

    // Definice typu
    using word  = System.UInt16;
    using dword = System.UInt32;
    using TRawValue = System.Single;
    using TNumber = System.Single;
    using THistogramCount = System.UInt16;     // pocet vzorku
    using THistogramValue = System.Int32;     // hodnota vzorku, musi byt signed

    public class Bat2Struct {

        //-----------------------------------------------------------------------------
        // Obecne definice
        //-----------------------------------------------------------------------------

        public enum TUnits : byte {
            UNITS_KG,
            UNITS_LB,
            _UNITS_COUNT
        }

        public enum TBacklight : byte {
            BACKLIGHT_OFF,
            BACKLIGHT_ON,
            BACKLIGHT_AUTO,
            _BACKLIGHT_COUNT
        }

        // Typ automatickeho zjistovani cilove hmotnosti
        public enum TAutoMode : byte {
            AUTOMODE_WITHOUT_GAIN,    // Cilova hmotnost se vypocte jako prumer ze vcerejska
            AUTOMODE_WITH_GAIN,       // Cilova hmotnost se vypocte jako prumer + denni prirustek ze vcerejska
            _AUTOMODE_COUNT
        }

        // Typ naskoku/seskoku na vahu
        public enum TJumpMode : byte {
            JUMPMODE_ENTER,           // Vyhodnocuje se pouze naskok na vahu
            JUMPMODE_LEAVE,           // Vyhodnocuje se pouze seskok z vahy
            JUMPMODE_BOTH,            // Vyhodnocuje se naskok i seskok z vahy
            _JUMPMODE_COUNT
        }

        // Rychlost komunikace RS-485 - pouziva se pouze pri editaci v menu
        public enum TSpeed : byte {
            RS485_SPEED_1200,
            RS485_SPEED_2400,
            RS485_SPEED_4800,
            RS485_SPEED_9600,
            RS485_SPEED_19200,
            RS485_SPEED_38400,
            RS485_SPEED_57600,
            RS485_SPEED_115200,
            _RS485_SPEED_COUNT
        }

        // Parita pri komunikaci RS-485, POZOR, musi odpovidat parite definovane v MODBUS (alespon prvni mody)
        public enum TParity : byte {
            RS485_PARITY_NONE,        // Zadna parita (paritni bit se nevysila)
            RS485_PARITY_EVEN,        // Suda parita
            RS485_PARITY_ODD,         // Licha parita
            RS485_PARITY_MARK,        // Vysilaji se 2 stop bity
            _RS485_PARITY_COUNT
        }

        // Komunikacni protokol
        public enum TProtocol : byte {
            RS485_PROTOCOL_MODBUS_RTU,
            RS485_PROTOCOL_MODBUS_ASCII,
            _RS485_PROTOCOL_COUNT
        }

        // Hardwarova verze
        public enum THwVersion : byte {
            HW_VERSION_GSM,           // GSM modul
            HW_VERSION_DACS_LW1,      // Emulace vahy LW1 pripojene k Dacs ACS pres RS-485
            HW_VERSION_MODBUS,        // Modbus pres RS-485
            HW_VERSION_DACS_ACS6,     // Vaha pripojena k Dacs ACS6 pres RS-485
            _HW_VERSION_COUNT
        }

        // ---------------------------------------------------------------------------------------------
        // Datum a cas
        // ---------------------------------------------------------------------------------------------

        // Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TLongDateTime {
            public byte Min;
            public byte Hour;
            public byte Day;
            public byte Month;
            public word Year;

            public void Swap() {
                Year = Veit.Endian.Endian.SwapUInt16(Year);
            }
        }  // 6 bajtu

        // ---------------------------------------------------------------------------------------------
        // Rustova krivka
        // ---------------------------------------------------------------------------------------------

        public const int CURVE_MAX_POINTS          = 30;          // Max pocet bodu s definici rustove krivky
        public const ushort CURVE_MAX_DAY          = 999;         // Max cislo dne
        public const ushort CURVE_MAX_WEIGHT       = 0xFFFF;      // Max hmotnost (aby se vlezla do 2 bajtu)
        public const int CURVE_END_WEIGHT          = 0;           // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)
        public const ushort CURVE_FIRST_DAY_NUMBER = 0;           // Cislo prvniho dne, od ktereho se zacina krmit (= minimalni cislo dne, ktere lze pri zahajeni vazeni zadat)

        // Format bodu rustove krivky
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TCurvePoint {
            public word Day;         // 0..CURVE_MAX_DAY (999)
            public word Weight;      // 0..CURVE_MAX_WEIGHT (0xFFFF)

            public void Swap() {
                Day    = Veit.Endian.Endian.SwapUInt16(Day);
                Weight = Veit.Endian.Endian.SwapUInt16(Weight);
            }
        }  // 4 bajty

        // Pole 1 krivky
        //typedef TCurvePoint TCurvePoints[CURVE_MAX_POINTS];  // 120 bajtu

        //-----------------------------------------------------------------------------
        // Hejna
        //-----------------------------------------------------------------------------

        public const int FLOCK_NAME_MAX_LENGTH     = 8;           // Maximalni delka
        public const byte FLOCK_EMPTY_NUMBER       = 0xFF;        // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane
        public const byte FLOCK_TIME_LIMIT_EMPTY   = 0xFF;        // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale
        public const int FLOCK_TIME_LIMIT_MAX_HOUR = 23;          // Maximalni hodnota hodiny pro omezeni (0-23hod)

        // Rozdeleni pohlavi
        public enum TGender : byte {
            GENDER_FEMALE = 0,                            // Pohlavi samice (pokud se pouziva jen 1 pohlavi, jsou vzdy platne parametry pro samice)
            GENDER_MALE,                                  // Pohlavi samec
            _GENDER_COUNT
        }
        const int GENDER_COUNT = (int)TGender._GENDER_COUNT;

        // Zahlavi hejna
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TFlockHeader {
            public byte Number;                                  // Cislo hejna - pokud je hejno definovane, je rovno indexu hejna (0 - 9)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FLOCK_NAME_MAX_LENGTH)]
            public byte[] Title;            // Nazev hejna (bez ukoncovaci nuly, prazdne znaky se doplni mezerami)
            public byte UseCurves;                               // Zda se maji pouzivat rustove krivky nebo automaticke zjistovani cilove hmotnosti bez pouziti rustovych krivek
                                                        // Pokud krivky nepouziva, jsou zadane pocatecni hmotnosti ulozene jako 1. bod ve krivce
            public byte UseBothGenders;                          // Flag, zda se ma pouzivat rozliseni na samce a samice
            public byte WeighFrom;                               // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se
            public byte WeighTill;                               // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se
        }  // 13 bajtu

        // Struktura jednoho hejna
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TFlock {
            public TFlockHeader  Header;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = CURVE_MAX_POINTS * GENDER_COUNT)]
            public TCurvePoint[] GrowthCurve;     // Rustova krivka pro samice (pripadne oboje) a pro samce

            public void Swap() {
                for (int i = 0; i < GrowthCurve.Length; i++) {
                    GrowthCurve[i].Swap();
                }
            }

        }  // 253 bajtu

        public const int FLOCKS_MAX_COUNT = 10;          // Pocet hejn, ktere muze zadat
        //typedef TFlock TFlocks[FLOCKS_MAX_COUNT];       // Cisla hejn odpovidaji pozici, tj. na pozici 0 je hejno cislo 0, na pozici 1 hejno cislo 1 atd.

        // Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
        // veci, ktere jsou stale potreba a v prubehu dne se nemeni, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake
        // zmene v EEPROM, pri prechodu na dalsi den atd.
        // Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TWeighing {
            public TFlockHeader  Header;
            public word          DayNumber;                      // Cislo dne od pocatku vykrmu
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public word[]          TargetWeight;    // Normovana hmotnost nactena z rustove krivky pro samice a samce
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public TRawValue[]     MarginAbove;     // Horni mez hmotnosti samic a samcu
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public TRawValue[]     MarginBelow;     // Dolni mez hmotnosti samic a samcu
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public word[]          ComparisonWeight;// Hmotnost pro porovnani nactena z rustove porovnavaci krivky pro samice a samce

            public void Swap() {
                DayNumber    = Veit.Endian.Endian.SwapUInt16(DayNumber);
                for (int i = 0; i < GENDER_COUNT; i++) {
                    TargetWeight[i] = Veit.Endian.Endian.SwapUInt16(TargetWeight[i]);
                    MarginAbove[i] = Veit.Endian.Endian.SwapFloat(MarginAbove[i]);
                    MarginBelow[i] = Veit.Endian.Endian.SwapFloat(MarginBelow[i]);
                    ComparisonWeight[i] = Veit.Endian.Endian.SwapUInt16(ComparisonWeight[i]);
                }
            }
        }  // 273 bajtu

        //-----------------------------------------------------------------------------
        // Kalibrace
        //-----------------------------------------------------------------------------

        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TCalibration {
            public int  ZeroCalibration;                    // Kalibrace nuly
            public int  RangeCalibration;                   // Kalibrace rozsahu
            public int  Range;                              // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
            public byte Division;                           // Dilek vah

            public void Swap() {
                ZeroCalibration  = Veit.Endian.Endian.SwapInt32(ZeroCalibration);
                RangeCalibration = Veit.Endian.Endian.SwapInt32(RangeCalibration);
                Range            = Veit.Endian.Endian.SwapInt32(Range);
            }
        }  // 13 bajtu

        //-----------------------------------------------------------------------------
        // Konfiguracni data
        //-----------------------------------------------------------------------------

        // Rychle zadani parametru vykrmu rovnou pri startu, bez pouziti nektereho preddefinovaneho hejna.
        // Pri pouziti hejna se tyto parametry berou primo z hejna.
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TQuickWeighing {
            public byte UseBothGenders;                  // Flag, zda se ma pouzivat rozliseni na samce a samice
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public word[] InitialWeight;    // Zadana normovana hmotnost pro pocatecni den vykrmu pro samice a samce

            public void Swap() {
                for (int i = 0; i < GENDER_COUNT; i++) {
                    InitialWeight[i] = Veit.Endian.Endian.SwapUInt16(InitialWeight[i]);
                }
            }
        }  // 5 bajtu

        // Parametry, ktere zadal pri zahajeni vykrmu. Tyto parametry se v celem prubehu vazeni nemeni.
        // Po skonceni vykrmu se zde drzi vsechny parametry posledniho vykrmu az do doby odstartovani dalsiho vykrmu. Parametry odtut muze tedy vyuzivat
        // historie i po ukonceni vykrmu.
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TWeighingStart {
            public byte           Running;            // Flag, ze prave probiha krmeni
            public byte           WaitingForStart;    // Flag, ze se prave ceka na opozdeny start krmeni. Ostatni polozky TWeighingStart jsou vyplneny, staci inicializovat archiv atd.
            public byte           UseFlock;           // Flag, zda zvolil krmeni podle urciteho hejna. Pokud ne, zadal zakladni parametry primo a vazi se bez pouziti hejna, za pouziti parametru v TRychlyVykrm.
            public byte           CurrentFlock;       // Pokud se krmi podle hejna, je zde cislo hejna, podle ktereho se prave krmi. Pri ukonceni vykrmu zde zustane cislo hejna, podle ktereho se krmilo.
            public word           CurveDayShift;      // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.
            public TLongDateTime  DateTime;           // Datum zahajeni vykrmu
            public TQuickWeighing QuickWeighing;      // Zde jsou parametry vykrmu pri PouzivatHejno=NO
            public byte           UniformityRange;    // Rozsah uniformity v +- %
            public byte           Online;             // Flag, ze probiha online mereni

            public void Swap() {
                CurveDayShift = Veit.Endian.Endian.SwapUInt16(CurveDayShift);
                DateTime.Swap();
                QuickWeighing.Swap();
            }
        }  // 19 bajtu

        // Parametry GSM
        public const int GSM_NUMBER_MAX_LENGTH = 15;        // Maximalni delka telefonniho cisla bez zakoncovaci nuly, pozor, musi byt mensi nez GSM_MAX_NUMBER v Hardware.h
        public const int GSM_NUMBER_MAX_COUNT  = 5;         // Maximalni pocet definovanych telefonnich cisel
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TGsm {
            public byte Use;                 // Flag, zda se GSM modul vyuziva (zda je pripojen)
            public byte SendMidnight;        // Flag, zda se ma posilat o pulnoci statistika
            public byte PeriodMidnight;      // Perioda ve dnech, kdy se ma posilat pulnocni statistika
            public byte SendOnRequest;       // Flag, zda se ma odpovidat na zaslane SMS
            public byte ExecuteOnRequest;    // Flag, zda se ma provadet cinnost podle zaslanych SMS (zatim nevyuzito)
            public byte CheckNumbers;        // Flag, zda se ma pri prijeti prikazu kontrolovat cislo (odpovi se jen na prikazy odeslane z cisla, ktere je v seznamu)
            public byte NumberCount;         // Pocet definovanych telefonnich cisel, na ktere se vysila o pulnoci
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GSM_NUMBER_MAX_COUNT * GSM_NUMBER_MAX_LENGTH)]        // Delka pole = X_COUNT * Y_COUNT
            public byte[] Numbers;  // Definovana telefonni cisla
        }   // 82 bajtu

        // Parametry RS-485
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TRs485 {
            public byte      Address;        // Adresa vahy v siti RS-485
            public dword     Speed;          // Rychlost komunikace v Baudech za sekundu
            public TParity   Parity;         // Typ parity
            public word      ReplyDelay;     // Zpozdeni mezi prijmem prikazu a odpovedi v milisekundach, kvantovani s TIMER0_PERIOD
            public byte      SilentInterval; // Mezera mezi dvema pakety MODBUS v milisekundach
            public TProtocol Protocol;       // Typ komunikacniho protokolu

            public void Swap() {
                Speed      = Veit.Endian.Endian.SwapUInt32(Speed);
                ReplyDelay = Veit.Endian.Endian.SwapUInt16(ReplyDelay);
            }
        }   // 10 bajtu

        // Korekcni krivka
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TWeightCorrection {
            public word Day1;                // Den prvniho zlomu, do tohoto dne je korekce nulova
            public word Day2;                // Den druheho zlomu, v tomto dni a dale se uplatni zadana korekce <Correction>
            public byte Correction;          // Korekce v desetinach procenta (max. 25.5%) v den Day2. Pokud je hodnota 0, korekce se neuplatnuje.

            public void Swap() {
                Day1 = Veit.Endian.Endian.SwapUInt16(Day1);
                Day2 = Veit.Endian.Endian.SwapUInt16(Day2);
            }
        }  // 5 bajtu

        public const int CONFIG_RESERVED_SIZE = 11;                  // Pocet rezervovanych bajtu v TConfig

        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TConfig {
            // Interni data, pro ulozeni v EEPROM
            public word              Version;                        // VERSION
            public byte              Build;                          // BUILD
            public THwVersion        HwVersion;                      // Verze hardware (GSM, Dacs LW1, Modbus, ...)
            public word              IdentificationNumber;           // identifikace zarizeni
            public byte              Language;                       // Jazykova verze + jazyk

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public byte[]              MarginAbove;     // Okoli nad prumerem pro samice a samce
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public byte[]              MarginBelow;     // Okoli pod prumerem pro samice a samce
            public byte              Filter;                         // Filtr prevodniku
            public byte              StabilizationRange;             // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
            public byte              StabilizationTime;              // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
            public TAutoMode         AutoMode;                       // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)
            public TJumpMode         JumpMode;                       // Typ naskoku/seskoku na vahu, ktery se vyhodnocuje

            public TUnits            Units;                          // Zobrazovane jednotky

            public byte              HistogramRange;                 // Rozsah histogramu v +- % stredni hodnoty
            public byte              UniformityRange;                // Rozsah uniformity v +- %

            public TWeighingStart    WeighingStart;                  // Parametry vazeni

            public TGsm              Gsm;                            // Parametry GSM modulu

            public TBacklight        Backlight;                      // Nastaveny rezim podsvitu
            public byte              Battery;                        // Vaha je napajena z baterie - zatim se nepouziva

            public TRs485            Rs485;                          // Parametry linky RS-485

            public byte              ComparisonFlock;                // Cislo hejna, se kterym se porovnavaji vysledky, pripadne FLOCK_EMPTY_NUMBER pokud se neporovnava

            public TWeightCorrection WeightCorrection;               // Korekce hmotnosti

            [MarshalAs(UnmanagedType.ByValArray, SizeConst = CONFIG_RESERVED_SIZE)]
            public byte[]              Reserved; // Pro budouci pouziti

            public byte              Checksum;                       // Kontrolni soucet

            public void Swap() {
                Version = Veit.Endian.Endian.SwapUInt16(Version);
                IdentificationNumber = Veit.Endian.Endian.SwapUInt16(IdentificationNumber);
                WeighingStart.Swap();
                Rs485.Swap();
                WeightCorrection.Swap();
            }
        }   // 150 bajtu, pri provedeni zmen zkontrolovat fci CfgSaveConfigFromModule()

        //-----------------------------------------------------------------------------
        // Struktura archivu
        //-----------------------------------------------------------------------------

        // narazniky :

        public const byte FL_ARCHIVE_EMPTY = 0xFE;
        public const byte FL_ARCHIVE_FULL  = 0xFF;

        // Popisovac statistiky :
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TStatistic {
            public TNumber XSuma;
            public TNumber X2Suma;
            public word    Count;

            public void Swap() {
                XSuma  = Veit.Endian.Endian.SwapFloat(XSuma);
                X2Suma = Veit.Endian.Endian.SwapFloat(X2Suma);
                Count  = Veit.Endian.Endian.SwapUInt16(Count);
            }
        }   // 10 bajtu

        public const int HISTOGRAM_SLOTS = 39;       // pocet sloupcu - sude i liche cislo (max.254)
            
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct THistogram {
            public THistogramValue Center;                  // Stredni hodnota, kolem ktere se histogram vytvari - je treba zadat pri inicializaci
            public THistogramValue Step;                    // Krok histogramu - je treba zadat pri inicializaci
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = HISTOGRAM_SLOTS)]
            public THistogramCount[] Slot;

            public void Swap() {
                Center  = Veit.Endian.Endian.SwapInt32(Center);
                Step = Veit.Endian.Endian.SwapInt32(Step);
                for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
                    Slot[i]  = Veit.Endian.Endian.SwapUInt16(Slot[i]);
                }
            }
        }    // 86 bajtu

        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TArchive {
            public word          DayNumber;                     // Cislo dne od pocatku vykrmu
            public TLongDateTime DateTime;                      // datum a cas porizeni
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public TStatistic[]    Stat;           // statistika samic a samcu
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public THistogram[]    Hist;           // histogram samic a samcu
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public word[]          LastAverage;    // Prumerna vcerejsi hmotnost samic a samcu (pro vypocet daily gain)
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public word[]          TargetWeight;   // Normovana hmotnost samic a samcu pro tento den
            public byte          RealUniformityUsed;            // YES/NO, zda se pouzil presny vypocet uniformity, ktery je v promennych RealnUniformitaSamice a RealnUniformitaSamci
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = GENDER_COUNT)]
            public byte[]          RealUniformity; // Uniformita samic a samcu vypoctena presne z jednotlivych vzorku

            public void Swap() {
                DayNumber = Veit.Endian.Endian.SwapUInt16(DayNumber);
                DateTime.Swap();
                for (int i = 0; i < GENDER_COUNT; i++) {
                    Stat[i].Swap();
                    Hist[i].Swap();
                    LastAverage[i]  = Veit.Endian.Endian.SwapUInt16(LastAverage[i]);
                    TargetWeight[i] = Veit.Endian.Endian.SwapUInt16(TargetWeight[i]);
                }
            }
        }         // 211 bajtu - pozor, pri prekroceni 255 bajtu predefinovat promennou v SaveContext() a LoadContext()

        //-----------------------------------------------------------------------------
        // Zaznam vzorku
        //-----------------------------------------------------------------------------

        // Naraznikove znaky :

        public const byte LG_SAMPLE_EMPTY      = 0x7F;        // nenaplnene FIFO
        public const byte LG_SAMPLE_FULL       = 0xFF;        // naplnene FIFO
        public const byte LG_SAMPLE_VALUE      = 0x20;        // zaznam hodnoty
        public const byte LG_SAMPLE_GENDER     = 0x10;        // bit pohlavi
        public const byte LG_SAMPLE_WEIGHT_MSB = 0x01;        // 17. bit hmotnosti ve flagu

        public const uint LG_SAMPLE_MASK_WEIGHT    = 0x01FFFF;    // Hmotnost je ulozena ve spodnich 17bit, MSB byte je flag (hmotnost tedy zabira 1 spodni bit z flagu)
        public const uint LG_SAMPLE_MASK_HI_WEIGHT = 0x010000;    // 17. bit hmotnosti
        public const ushort LG_SAMPLE_MASK_HOUR    = 0x001F;      // Hodina je ulozena ve spodnich 5 bitech

        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TLoggerSample {
            public byte  Flag;        // naraznik
            public word  Value;       // vaha nebo hodina (vaha je na 17 bitu)
            public void Swap() {
                Value = Veit.Endian.Endian.SwapUInt16(Value);
            }
        }       // 3 bajty

        // zaznam jednotlivych vazeni :
        public const int FL_LOGGER_SAMPLES = 1801;
        public const int FL_LOGGER_SAMPLE_SIZE = 3;                       // 3 bajty

        // struktura dne (jen pro pro Builder a vypocet velikosti) :
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TArchiveDailyInfo {
            public TArchive      Archive;                                                   // 211  bytu
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FL_LOGGER_SAMPLES)]
            public TLoggerSample[] Samples;                               // 5403 bytu

            public void Swap() {
                Archive.Swap();
                for (int i = 0; i < FL_LOGGER_SAMPLES; i++) {
                    Samples[i].Swap();
                }
            }
        }    // 5614 bytu

        //-----------------------------------------------------------------------------
        // Online ukladani
        //-----------------------------------------------------------------------------

        // Naraznikove znaky :
        public const byte ON_SAMPLE_EMPTY      = 0x7F;        // nenaplnene FIFO
        public const byte ON_SAMPLE_FULL       = 0xFF;        // naplnene FIFO
        public const byte ON_SAMPLE_WEIGHT     = 0x08;        // zaznam hmotnosti
        public const byte ON_SAMPLE_SAVED      = 0x20;        // Vaha by prave ulozila vzorek do pameti
        public const byte ON_SAMPLE_STABLE     = 0x10;        // Bit ustalene hmotnosti
        public const byte ON_SAMPLE_SIGN       = 0x04;        // Znamenko hmotnosti
        public const byte ON_SAMPLE_WEIGHT_MSB = 0x03;        // 17. a 18. bit hmotnosti ve flagu

        public const uint ON_SAMPLE_MASK_WEIGHT = 0x03FFFF;    // Hmotnost je ulozena ve spodnich 18bit, MSB byte je flag (hmotnost tedy zabira 2 spodni bity z flagu)
        public const ushort ON_SAMPLE_MASK_HOUR = 0x001F;      // Hodina je ulozena ve spodnich 5 bitech

        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TOnlineSample {
            public byte  Flag;        // Naraznik
            public word  Value;       // Hmotnost nebo hodina

            public void Swap() {
                Value = Veit.Endian.Endian.SwapUInt16(Value);
            }
        }       // 3 bajty

        // zaznam jednotlivych vazeni :
        public const int FL_ONLINE_SAMPLES = 694265;
        public const int FL_ONLINE_SAMPLE_SIZE = 3;    // 3 bajty

        //-----------------------------------------------------------------------------
        // Struktura FLASH
        //-----------------------------------------------------------------------------

        // rozdeleni pameti na sekce
        // U AT45BD161B mam k dispozici 4096 x 512 = 2 097 152 bajtu

        // konfiguracni sekce :
        public const int FL_CONFIG_BASE       = 0;
        public const int FL_CONFIG_SIZE       = 4096;

        // archiv statistik & histogramu :
        public const int FL_ARCHIVE_BASE      = (FL_CONFIG_BASE + FL_CONFIG_SIZE);
        public const int FL_ARCHIVE_DAY_SIZE  = 5614;               // sizeof( TArchiveDailyInfo) velikost polozky
        public const int FL_ARCHIVE_DAYS      = 371;                                         // pocet polozek
        public const int FL_ARCHIVE_SIZE      = (FL_ARCHIVE_DAY_SIZE * FL_ARCHIVE_DAYS);      // celkem bytu (2082794), konec pameti 2086890

        // Online mereni :
        public const int FL_ONLINE_BASE       = FL_ARCHIVE_BASE;                              // Stejne jako archiv
        public const int FL_ONLINE_SIZE       = (FL_ONLINE_SAMPLE_SIZE * FL_ONLINE_SAMPLES);  // celkem bytu (2082795), konec pameti 2086891

        // struktura konfiguracni sekce :
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TConfigSection {
            public TConfig       Config;         // Konfigurace
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FLOCKS_MAX_COUNT)]
            public TFlock[]       Flocks;         // Definice jednotlivych hejn
            public TCalibration  Calibration;    // Kalibrace zkopirovana z interni EEPROM

            public void Swap() {
                Config.Swap();
                for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
                    Flocks[i].Swap();
                }
                Calibration.Swap();
            }
        }  // 2693 bajtu


        // Struktura cele Flash
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct TFlash {
            public TConfigSection ConfigSection;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FL_CONFIG_SIZE - 2693)]
            public byte[] Reserved;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = FL_ARCHIVE_DAYS)]
            public TArchiveDailyInfo[] ArchiveDailyInfos;

            public void Swap() {
                ConfigSection.Swap();
                for (int i = 0; i < FL_ARCHIVE_DAYS; i++) {
                    ArchiveDailyInfos[i].Swap();
                }
            }
        }

    }
}