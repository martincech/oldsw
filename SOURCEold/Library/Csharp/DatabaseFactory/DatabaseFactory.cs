﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;


namespace Veit.Database {
    public class DatabaseFactory {

        /// <summary>
        /// Database provider
        /// </summary>
        private DbProviderFactory provider;

        /// <summary>
        /// Database connection
        /// </summary>
        public DbConnection Connection { get { return connection; } }
        private DbConnection connection;

        /// <summary>
        /// Connection string
        /// </summary>
        private string connectionString;

        /// <summary>
        /// Database transaction
        /// </summary>
        private DbTransaction transaction;

        /// <summary>
        /// 8 bits signed integer
        /// </summary>
        public string TypeByte { get { return typeByte; } }
        private string typeByte;

        /// <summary>
        /// 16 bits signed integer
        /// </summary>
        public string TypeSmallInt { get { return typeSmallInt; } }
        private string typeSmallInt;

        /// <summary>
        /// 32 bits signed integer
        /// </summary>
        public string TypeInteger { get { return typeInteger; } }
        private string typeInteger;

        /// <summary>
        /// 64 bits signed integer
        /// </summary>
        public string TypeLong { get { return typeLong; } }
        private string typeLong;

        /// <summary>
        /// 32 bitf float
        /// </summary>
        public string TypeFloat { get { return typeFloat; } }
        private string typeFloat;

        /// <summary>
        /// 64 bits double
        /// </summary>
        public string TypeDouble { get { return typeDouble; } }
        private string typeDouble;

        /// <summary>
        /// Cahracter
        /// </summary>
        public string TypeChar { get { return typeChar; } }
        private string typeChar;

        /// <summary>
        /// National character
        /// </summary>
        public string TypeNChar { get { return typeNChar; } }
        private string typeNChar;

        /// <summary>
        /// National variable character
        /// </summary>
        public string TypeNVarChar { get { return typeNVarChar; } }
        private string typeNVarChar;

        /// <summary>
        /// Date / time
        /// </summary>
        public string TypeTimestamp { get { return typeTimestamp; } }
        private string typeTimestamp;

        /// <summary>
        /// Constructor without a need to list the provider in machine.config
        /// </summary>
        /// <param name="provider">Database provider to use</param>
        /// <param name="connectionString">Database connection string</param>
        public DatabaseFactory(DbProviderFactory provider, string connectionString) {
            this.provider = provider;
            this.connectionString = connectionString;
            SetTypesFirebird();
        }

        /// <summary>
        /// Constructor. Database provider must be listed in machine.config or app.config.
        /// </summary>
        /// <param name="providerName">Provider name (for example "FirebirdSql.Data.FirebirdClient")</param>
        /// <param name="connectionString">Database connection string</param>
        public DatabaseFactory(string providerName, string connectionString)
            : this(DbProviderFactories.GetFactory(providerName), connectionString) {
        }

        /// <summary>
        /// Create new connection
        /// </summary>
        private void CreateConnection() {
            connection = provider.CreateConnection();
            connection.ConnectionString = connectionString;
        }

        /// <summary>
        /// Open connection
        /// </summary>
        public void OpenConnection() {
            if (transaction == null) {
                // Neprobiha zadna transakce, zalozim nove spojeni
                CreateConnection();
                connection.Open();
                return;
            }

            // Probiha transakce, spojeni uz je otevrene
        }

        /// <summary>
        /// Close connection
        /// </summary>
        public void CloseConnection() {
            if (transaction == null) {
                // Neprobiha zadna transakce, spojeni muzu zavrit a zrusit
                connection.Close();
                connection.Dispose();
                connection = null;
                return;
            }

            // Probiha transakce, spojeni ponecham otevrene dokud transakci neukonci
        }

        /// <summary>
        /// Begin transaction and open connection if needed
        /// </summary>
        public void BeginTransaction() {
            if (connection == null) {
                // Musim vytvorit a otevrit spojeni
                OpenConnection();
            }
            transaction = connection.BeginTransaction();
        }

        /// <summary>
        /// Commit transaction and close connection
        /// </summary>
        public void CommitTransaction() {
            transaction.Commit();
            transaction.Dispose();
            transaction = null;
            CloseConnection();
        }

        /// <summary>
        /// Rollback transaction and close connection
        /// </summary>
        public void RollbackTransaction() {
            transaction.Rollback();
            transaction.Dispose();
            transaction = null;
            CloseConnection();
        }

        /// <summary>
        /// Create new command
        /// </summary>
        /// <returns>New command instance</returns>
        public DbCommand CreateCommand() {
            DbCommand command = provider.CreateCommand();
            command.Connection = connection;
            if (transaction != null) {
                // Probiha transakce, ulozim ji do prikazu
                command.Transaction = transaction;
            }
            return command;
        }

        /// <summary>
        /// Create new command with specified command text
        /// </summary>
        /// <param name="commandText">Command text to run</param>
        /// <returns></returns>
        public DbCommand CreateCommand(string commandText) {
            DbCommand command = CreateCommand();
            command.CommandText = commandText;
            return command;
        }

        /// <summary>
        /// Create new parameter
        /// </summary>
        /// <returns>Parameter</returns>
        public DbParameter CreateParameter() {
            return provider.CreateParameter();
        }

        /// <summary>
        /// Create new parameter
        /// </summary>
        /// <param name="name">Parameter name</param>
        /// <param name="value">Parameter value</param>
        /// <returns>Parameter</returns>
        public DbParameter CreateParameter(string name, object value) {
            DbParameter parameter = CreateParameter();
            parameter.ParameterName = name;
            parameter.Value         = value;
            return parameter;
        }

        /// <summary>
        /// Calculate Julian date
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>Julian date</returns>
        public double ToJulianDay(DateTime dateTime) {
            return dateTime.ToOADate();         // Neodpovida SQLite, opravit !!!
        }

        /// <summary>
        /// Calculate DateTime from Julian day
        /// </summary>
        /// <param name="julianDay">Julian day</param>
        /// <returns>DateTime</returns>
        public DateTime FromJulianDay(double julianDay) {
            return DateTime.FromOADate(julianDay);      // Neodpovida SQLite, opravit !!!
        }

        /// <summary>
        /// Set all data types to Firebird
        /// </summary>
        public void SetTypesFirebird() {
            typeByte      = "SMALLINT";      // Nejmensi je 2 bajtovy integer
            typeSmallInt  = "SMALLINT";
            typeInteger   = "INTEGER";
            typeLong      = "INT64";
            typeFloat     = "FLOAT";
            typeDouble    = "DOUBLE PRECISION";
            typeChar      = "CHAR";
            typeNChar     = "NCHAR";
            typeNVarChar  = "VARCHAR";
            typeTimestamp = "TIMESTAMP";
        }

        /// <summary>
        /// Set all data types to MS SQL Server Compact Edition
        /// </summary>
        public void SetTypesSqlCe() {
            typeByte      = "TINYINT";
            typeSmallInt  = "SMALLINT";
            typeInteger   = "INTEGER";
            typeLong      = "BIGINT";
            typeFloat     = "REAL";
            typeDouble    = "FLOAT";
            typeChar      = "NCHAR";
            typeNChar     = "NCHAR";
            typeNVarChar  = "NVARCHAR";
            typeTimestamp = "DATETIME";
        }

        /// <summary>
        /// Set all data types to SQLite
        /// </summary>
        public void SetTypesSqlite() {
            // SQLite sice podporuje jen 5 typu, ale ADO.NET provider jich rozlisuje vic (napr. REAL bere vzdy jako float a ne double)
            // Viz. http://sqlite.phxsoftware.com/forums/t/31.aspx
            typeByte      = "TINYINT";
            typeSmallInt  = "SMALLINT";
            typeInteger   = "INT";
            typeLong      = "INTEGER";      // Musi byt "INTEGER", jinak nefunguji primary keys
            typeFloat     = "REAL";
            typeDouble    = "DOUBLE";
            typeChar      = "TEXT";
            typeNChar     = "TEXT";
            typeNVarChar  = "TEXT";        // Pozor, nepouzivat STRING, ten prevadi napr. "0005" na integer 5. TEXT funguje v poradku.
            typeTimestamp = "TIMESTAMP";   // SQLite sice nativne nepodporuje Timestamp, .NET provider uklada automaticky pomoci fce SQLiteConvert.ToJulianDay(DateTime)
        }
    }
}
