﻿//******************************************************************************
//
//   Sample.cs      One sample stored in the Bat1 scale
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    
    /// <summary>
    /// Flag stored at each sample
    /// </summary>
    public enum Flag {
        NONE = 0,       // Flags not used during weighing
        LIGHT,          // Weight lower than low limit
        OK,             // Weight within low and high limits
        HEAVY,          // Weight higher than high limit
        MALE,           // Male
        FEMALE,         // Female
        _COUNT,
        ALL             // Use all flags (for searching only)
    };
    
    /// <summary>
    /// One stored sample
    /// </summary>
    public struct Sample {
        /// <summary>
        /// Date and time when the sample was saved
        /// </summary>
        public DateTime dateTime;

        /// <summary>
        /// Weight in kg/g/lb
        /// </summary>
        public float weight;

        /// <summary>
        /// Flag
        /// </summary>
        public Flag flag;

        /// <summary>
        /// Convert flag to string
        /// </summary>
        /// <param name="flag">Flag to convert</param>
        /// <returns>String representation</returns>
        public static string FlagToString(Flag flag) {
            switch (flag) {
                case Flag.NONE:   return Properties.Resources.FLAG_NONE;
                case Flag.LIGHT:  return Properties.Resources.FLAG_LIGHT;
                case Flag.OK:     return Properties.Resources.FLAG_OK;
                case Flag.HEAVY:  return Properties.Resources.FLAG_HEAVY;
                case Flag.MALE:   return Properties.Resources.FLAG_MALE;
                case Flag.FEMALE: return Properties.Resources.FLAG_FEMALE;
                default:          return Properties.Resources.FLAG_ALL;
            }
        }

        private static int WeightComparison(Sample x, Sample y) {
            if (x.weight < y.weight) {
                return -1;
            } else if (x.weight > y.weight) {
                return 1;
            }
            return 0;
        }

        private static int FlagComparison(Sample x, Sample y) {
            if (x.flag < y.flag) {
                return -1;
            } else if (x.flag > y.flag) {
                return 1;
            }
            return 0;
        }
        
        /// <summary>
        /// Compare function for List(Sample).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByTime(Sample x, Sample y) {
            int result = DateTime.Compare(x.dateTime, y.dateTime);
            if (result != 0) {
                return result;      // Datumy jsou rozdilne
            }

            // Datumy jsou shodne, rozhodne hmotnost
            result = WeightComparison(x, y);
            if (result != 0) {
                return result;      // Hmotnosti jsou rozdilne
            }

            // Musi rozhodnout flag
            return FlagComparison(x, y);
        }

        /// <summary>
        /// Compare function for List(Sample).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByWeight(Sample x, Sample y) {
            int result = WeightComparison(x, y);
            if (result != 0) {
                return result;      // Hmotnosti jsou rozdilne
            }

            // Hmotnosti jsou shodne, rozhodne datum
            result = DateTime.Compare(x.dateTime, y.dateTime);
            if (result != 0) {
                return result;      // Datumy jsou rozdilne
            }

            // Musi rozhodnout flag
            return FlagComparison(x, y);
        }

    
    }
}
