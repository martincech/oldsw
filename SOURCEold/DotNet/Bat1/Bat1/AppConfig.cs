﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Bat1 {
    public static class AppConfig {
        public static readonly string ApplicationName        = ConfigurationManager.AppSettings["ApplicationName"].ToString();
        public static readonly string ApplicationNameSmall   = ConfigurationManager.AppSettings["ApplicationNameSmall"].ToString();
        public static readonly string DatabaseFileExtension  = ConfigurationManager.AppSettings["DatabaseFileExtension"].ToString();
        public static readonly string BackupFileExtension    = ConfigurationManager.AppSettings["BackupFileExtension"].ToString();
        public static readonly string HardcopyFileExtension  = ConfigurationManager.AppSettings["HardcopyFileExtension"].ToString();
        public static readonly string ExportFileExtension    = ConfigurationManager.AppSettings["ExportFileExtension"].ToString();
        public static readonly string CompanyDirectory       = ConfigurationManager.AppSettings["CompanyDirectory"].ToString();
        public static readonly string CompanyName            = ConfigurationManager.AppSettings["CompanyName"].ToString();
        public static readonly string CompanyAddress;
        public static readonly string CompanyMail            = ConfigurationManager.AppSettings["CompanyMail"].ToString();
        public static readonly string CompanyMailSubject     = ConfigurationManager.AppSettings["CompanyMailSubject"].ToString();
        public static readonly string CompanyWeb             = ConfigurationManager.AppSettings["CompanyWeb"].ToString();
        public static readonly string CompanyWebAddress      = ConfigurationManager.AppSettings["CompanyWebAddress"].ToString();

        static AppConfig() {
            // Vytvorim viceradkovy label s adresou, prvni prazdny radek konci
            StringBuilder address = new StringBuilder();            
            for (int i = 1; i <= 6; i++) {
                string line = ConfigurationManager.AppSettings["CompanyAddress" + i.ToString()].ToString();
                if (line == "") {
                    break;      // Prvni prazdny radek konci
                }
                if (i >= 2) {
                    address.Append("\n");     // Pridam CRLF
                }
                address.Append(line);
            }

            CompanyAddress = address.ToString();
        }
    }
}
