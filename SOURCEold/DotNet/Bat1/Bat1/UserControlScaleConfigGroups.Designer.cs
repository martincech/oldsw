﻿namespace Bat1 {
    partial class UserControlScaleConfigGroups {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigGroups));
            this.listBoxGroups = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkedListBoxFiles = new System.Windows.Forms.CheckedListBox();
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonRename = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.groupBoxDetails = new System.Windows.Forms.GroupBox();
            this.groupBoxDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxGroups
            // 
            this.listBoxGroups.AccessibleDescription = null;
            this.listBoxGroups.AccessibleName = null;
            resources.ApplyResources(this.listBoxGroups, "listBoxGroups");
            this.listBoxGroups.BackgroundImage = null;
            this.listBoxGroups.Font = null;
            this.listBoxGroups.FormattingEnabled = true;
            this.listBoxGroups.Name = "listBoxGroups";
            this.listBoxGroups.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxGroups_MouseDoubleClick);
            this.listBoxGroups.SelectedIndexChanged += new System.EventHandler(this.listBoxGroups_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            // 
            // textBoxNote
            // 
            this.textBoxNote.AccessibleDescription = null;
            this.textBoxNote.AccessibleName = null;
            resources.ApplyResources(this.textBoxNote, "textBoxNote");
            this.textBoxNote.BackgroundImage = null;
            this.textBoxNote.Font = null;
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxNote_KeyDown);
            this.textBoxNote.Leave += new System.EventHandler(this.textBoxNote_Leave);
            this.textBoxNote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNote_KeyPress);
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Font = null;
            this.label2.Name = "label2";
            // 
            // checkedListBoxFiles
            // 
            this.checkedListBoxFiles.AccessibleDescription = null;
            this.checkedListBoxFiles.AccessibleName = null;
            resources.ApplyResources(this.checkedListBoxFiles, "checkedListBoxFiles");
            this.checkedListBoxFiles.BackgroundImage = null;
            this.checkedListBoxFiles.Font = null;
            this.checkedListBoxFiles.FormattingEnabled = true;
            this.checkedListBoxFiles.Name = "checkedListBoxFiles";
            this.checkedListBoxFiles.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListBoxFiles_ItemCheck);
            // 
            // buttonNew
            // 
            this.buttonNew.AccessibleDescription = null;
            this.buttonNew.AccessibleName = null;
            resources.ApplyResources(this.buttonNew, "buttonNew");
            this.buttonNew.BackgroundImage = null;
            this.buttonNew.Font = null;
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonRename
            // 
            this.buttonRename.AccessibleDescription = null;
            this.buttonRename.AccessibleName = null;
            resources.ApplyResources(this.buttonRename, "buttonRename");
            this.buttonRename.BackgroundImage = null;
            this.buttonRename.Font = null;
            this.buttonRename.Name = "buttonRename";
            this.buttonRename.UseVisualStyleBackColor = true;
            this.buttonRename.Click += new System.EventHandler(this.buttonRename_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.AccessibleDescription = null;
            this.buttonDelete.AccessibleName = null;
            resources.ApplyResources(this.buttonDelete, "buttonDelete");
            this.buttonDelete.BackgroundImage = null;
            this.buttonDelete.Font = null;
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // groupBoxDetails
            // 
            this.groupBoxDetails.AccessibleDescription = null;
            this.groupBoxDetails.AccessibleName = null;
            resources.ApplyResources(this.groupBoxDetails, "groupBoxDetails");
            this.groupBoxDetails.BackgroundImage = null;
            this.groupBoxDetails.Controls.Add(this.checkedListBoxFiles);
            this.groupBoxDetails.Controls.Add(this.label1);
            this.groupBoxDetails.Controls.Add(this.textBoxNote);
            this.groupBoxDetails.Controls.Add(this.label2);
            this.groupBoxDetails.Font = null;
            this.groupBoxDetails.Name = "groupBoxDetails";
            this.groupBoxDetails.TabStop = false;
            // 
            // UserControlScaleConfigGroups
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.groupBoxDetails);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonRename);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.listBoxGroups);
            this.Font = null;
            this.Name = "UserControlScaleConfigGroups";
            this.VisibleChanged += new System.EventHandler(this.UserControlScaleConfigFileGroups_VisibleChanged);
            this.groupBoxDetails.ResumeLayout(false);
            this.groupBoxDetails.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxGroups;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox checkedListBoxFiles;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonRename;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.GroupBox groupBoxDetails;
    }
}
