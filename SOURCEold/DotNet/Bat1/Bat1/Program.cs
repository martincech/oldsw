﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using Veit.SFolders;
using SingleInstance;
using System.Drawing.Printing;



namespace Bat1 {
    public static class Program {
        
        /// <summary>
        /// Database instance
        /// </summary>
        public static Database Database;

        /// <summary>
        /// Application setup
        /// </summary>
        public static Setup Setup;
        
        /// <summary>
        /// Data folder name in user's profile
        /// </summary>
        public static string DataFolder {
            get { return dataFolder; }
        }
        private static string dataFolder;

        /// <summary>
        /// Folder for temporary files in user's profile
        /// </summary>
        public static string TempFolder {
            get { return tempFolder; }
        }
        private static string tempFolder;

        /// <summary>
        /// Database file name without path
        /// </summary>
        public static readonly string DatabaseFileName = AppConfig.ApplicationNameSmall + "." + FileExtension.DATABASE;

        /// <summary>
        /// Full database file name including path
        /// </summary>
        public static string FullDatabaseFileName {
            get { return dataFolder + @"\" + DatabaseFileName; }
        }

        /// <summary>
        /// Application name
        /// </summary>
        public static readonly string ApplicationName = AppConfig.ApplicationName;

        /// <summary>
        /// Global printer settings used for all printing jobs (so the user doesn't have to select
        /// a printer every time)
        /// </summary>
        public static PrinterSettings PrinterSettings = new PrinterSettings();

        /// <summary>
        /// Prepare working directories
        /// </summary>
        public static void PrepareDirectories() {
            // Nactu nazev pracovniho adresare (struktura "Veit\Bat1\V7")
            SFolders sFolders = new SFolders(AppConfig.CompanyDirectory + @"\" + AppConfig.ApplicationNameSmall + @"\V" + SwVersion.MAJOR.ToString());
            sFolders.PrepareData(null);             // Pokud neexistuje, vytvorim pracovni adresar
            dataFolder = sFolders.DataFolder;       // Zapamatuju si pracovni adresar
            
            // Adresar pro docasne soubory
            tempFolder = dataFolder + @"\Temp";     // Zapamatuju si adresar pro docasne soubory
            if (!Directory.Exists(tempFolder)) {
                // Pokud neexistuje, vytvorim
                Directory.CreateDirectory(tempFolder);  
            }
        }
        
        /// <summary>
        /// Database initialization
        /// </summary>
        private static bool InitDatabase(FormSplash splash) {
            // Zalozim objekt databaze
            Database = new Database(FullDatabaseFileName);

            // Zkontroluju databazi
            if (Database.Exists()) {
                // Zkontroluju integritu dat v databazi
                if (!Database.CheckIntegrity()) {
                    // Korupce dat, musi obnovit ze zalohy
                    if (splash != null) {
                        splash.Hide();          // Schovam splash screen
                    }
                    try {
                        MessageBox.Show(Properties.Resources.DATABASE_CORRUPTED, ApplicationName);
                        FormRestoreBackup form = new FormRestoreBackup();
                        if (form.ShowDialog() != DialogResult.OK) {
                            return false;
                        }
                    } finally {
                        if (splash != null) {
                            splash.Show();      // Na zaver opet zobrazim
                        }
                    }
                }

                // Zkontroluji platnost nastaveni
                Database.CheckSetup();
                
                // Zkontroluji verzi databaze (az po pripadne obnove ze zalohy)
                if (!Database.Update()) {
                    // Databaze je novejsi nez SW, musi upgradovat SW
                    if (splash != null) {
                        splash.Hide();          // Schovam splash screen
                    }
                    MessageBox.Show(Properties.Resources.DATABASE_NOT_SUPPORTED, ApplicationName);
                    return false;
                }

                // Provedu automatickou zalohu databaze po startu programu
                StartupBackup.Backup(true);     // Integritu jsem uz otestoval, neni treba znovu

                // Periodicka zaloha kazdych 7 dni
                PeriodicBackup.Execute();
            
            } else {
                // Database neexistuje
                Database.CreateDatabase();      // Vytvorim prazdnou databazi
                Database.CreateTables();        // Vytvorim vsechny tabulky
            }

            return true;
        }

        /// <summary>
        /// Data directory and database initialization
        /// </summary>
        public static bool InitData(FormSplash splash) {
            // Priprava adresarove struktury - trva kratce, text ve splashi ani nezobrazuju
            PrepareDirectories();

            // Inicializuji databazi
            if (!InitDatabase(splash)) {
                return false;
            }

            return true;
        }

        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            // Pokud uz program jednou bezi, nespoustim ho podruhe, ale prepnu na jiz spusteny
            if (SingleApplication.IsAlreadyRunning()) {
				SingleApplication.SwitchToCurrentInstance();
				return;
			}
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Zobrazim splash
            FormSplash splash = new FormSplash();
            splash.Show();
            splash.Refresh();       // Aby se hned vykreslilo

            try {
                // Inicializuji adresarovou strukturu a databazi
                if (!InitData(splash)) {
                    Application.Exit();
                    return;
                }

                // Nactu nastaveni programu
                Database.LoadSetup();
            } finally {
                splash.Close();
                splash.Dispose();
            }

            // Pokud zatim neni zvoleny zadny jazyk, necham uzivatele vybrat
            if (Setup.Language == SwLanguage.UNDEFINED) {
                new FormLanguage().ShowDialog();    // Bud vybere, nebo zustane UNDEFINED (vykresli se anglictina)
            }
            
            // Nastavim jazyk
            SwLanguageClass.SetLanguage(Setup.Language);

            Application.Run(new FormMain());
        }
    }
}
