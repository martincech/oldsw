﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Bat1 {
    /// <summary>
    /// Small shape of histogram
    /// </summary>
    static class HistogramMiniGraph {
        /// <summary>
        /// Width of one slot in pixels
        /// </summary>
        private const int SLOT_WIDTH = 2;

        /// <summary>
        /// Default slot color for white backgorund
        /// </summary>
        private static Color defaultSlotColor = Color.FromArgb(110, 110, 110);
        
        /// <summary>
        /// Default bottom axis color for white backgorund
        /// </summary>
        private static Color defaultAxisColor = Color.FromArgb(140, 140, 140);
        
        /// <summary>
        /// Draw histogram to graphics
        /// </summary>
        /// <param name="counts">Array of slot heights</param>
        /// <param name="graphics">Graphics to draw to</param>
        /// <param name="slotColor">Slot color</param>
        /// <param name="axisColor">Bottom axis color</param>
        /// <param name="maxHeight">Maximum height of histogram in pixels</param>
        /// <param name="isNormalized">True if slot heights are normalized to maxHeight or 1 sample = 1 pixel</param>
        public static void Draw(int[] counts, Graphics graphics, Color slotColor, Color axisColor, int maxHeight, int topMargin, bool isNormalized) {
            // Nejprve najdu maximalni vysku sloupce
            int maxCount = 0;
            foreach (int count in counts) {
                if (count > maxCount) {
                    maxCount = count;
                }
            }

            // Vykreslim spodni osu a zmensim o 1px celkovou vysku nahledu
            graphics.DrawLine(new Pen(axisColor), 0, topMargin + maxHeight - 1, counts.Length * SLOT_WIDTH, topMargin + maxHeight - 1);
            maxHeight--;
            
            // Vypoctu jmenovatel pro normalizaci
            float denominator;
            if (isNormalized) {
                // Jmenovatel bude desetinne cislo
                denominator = (float)maxCount / (float)maxHeight;
            } else {
                // Jmenovatel bude integer
                if (maxCount % maxHeight > 0) {
                    denominator = maxCount / maxHeight + 1;
                } else {
                    denominator = maxCount / maxHeight;
                }
            }
            if (denominator == 0) {
                denominator = 1;        // Nulou nemuzu delit
            }
            
            // Vykreslim sloupce
            int x = 0;
            float height;
            foreach (int count in counts) {
                height = (float)count / (float)denominator;
                
                // Celociselnou cast vysky vykreslim plnou barvou
                graphics.FillRectangle(new SolidBrush(slotColor), x, maxHeight - (int)height + topMargin, SLOT_WIDTH, (int)height);
                
                // Pokud mam jeste misto, vykreslim desetinnou cast vysky svetlejsi barvou (pomoci pruhlednosti)
                if (count > 0 && (int)height < maxHeight - topMargin) {
                    int alpha = (int)(255.0 * (float)((height - (int)height)));
                    graphics.FillRectangle(new SolidBrush(Color.FromArgb(alpha, slotColor)), x, maxHeight - (int)height - 1 + topMargin, SLOT_WIDTH, 1);
                }

                x += SLOT_WIDTH;
            }
        }

        /// <summary>
        /// Draw histogram to graphics with default colors for white backgorund
        /// </summary>
        /// <param name="counts">Array of slot heights</param>
        /// <param name="graphics">Graphics to draw to</param>
        /// <param name="maxHeight">Maximum height of histogram in pixels</param>
        /// <param name="isNormalized">True if slot heights are normalized to maxHeight or 1 sample = 1 pixel</param>
        public static void Draw(int[] counts, Graphics graphics, int maxHeight, int topMargin, bool isNormalized) {
            Draw(counts, graphics, defaultSlotColor, defaultAxisColor, maxHeight, topMargin, isNormalized);
//            Draw(counts, graphics, Color.Yellow, Color.Red, maxHeight, topMargin, isNormalized);
        }

        /// <summary>
        /// Calculate total bitmap width
        /// </summary>
        /// <param name="slotsCount">Number of slots in histogram</param>
        /// <returns>Width in pixels</returns>
        public static int GetTotalWidth(int slotsCount) {
            return slotsCount * SLOT_WIDTH;
        }
    }
}
