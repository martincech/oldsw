﻿namespace Bat1 {
    partial class UserControlScaleConfigPassword {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigPassword));
            this.radioButtonEnable = new System.Windows.Forms.RadioButton();
            this.radioButtonDisable = new System.Windows.Forms.RadioButton();
            this.buttonChange = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBoxNumber1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNumber2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNumber3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNumber4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxKey1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxKey2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxKey3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxKey4 = new System.Windows.Forms.PictureBox();
            this.imageListKey = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey4)).BeginInit();
            this.SuspendLayout();
            // 
            // radioButtonEnable
            // 
            this.radioButtonEnable.AccessibleDescription = null;
            this.radioButtonEnable.AccessibleName = null;
            resources.ApplyResources(this.radioButtonEnable, "radioButtonEnable");
            this.radioButtonEnable.BackgroundImage = null;
            this.radioButtonEnable.Font = null;
            this.radioButtonEnable.Name = "radioButtonEnable";
            this.radioButtonEnable.TabStop = true;
            this.radioButtonEnable.UseVisualStyleBackColor = true;
            this.radioButtonEnable.CheckedChanged += new System.EventHandler(this.radioButtonEnable_CheckedChanged);
            // 
            // radioButtonDisable
            // 
            this.radioButtonDisable.AccessibleDescription = null;
            this.radioButtonDisable.AccessibleName = null;
            resources.ApplyResources(this.radioButtonDisable, "radioButtonDisable");
            this.radioButtonDisable.BackgroundImage = null;
            this.radioButtonDisable.Font = null;
            this.radioButtonDisable.Name = "radioButtonDisable";
            this.radioButtonDisable.TabStop = true;
            this.radioButtonDisable.UseVisualStyleBackColor = true;
            this.radioButtonDisable.CheckedChanged += new System.EventHandler(this.radioButtonEnable_CheckedChanged);
            // 
            // buttonChange
            // 
            this.buttonChange.AccessibleDescription = null;
            this.buttonChange.AccessibleName = null;
            resources.ApplyResources(this.buttonChange, "buttonChange");
            this.buttonChange.BackgroundImage = null;
            this.buttonChange.Font = null;
            this.buttonChange.Name = "buttonChange";
            this.buttonChange.UseVisualStyleBackColor = true;
            this.buttonChange.Click += new System.EventHandler(this.buttonChange_Click);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            // 
            // pictureBoxNumber1
            // 
            this.pictureBoxNumber1.AccessibleDescription = null;
            this.pictureBoxNumber1.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber1, "pictureBoxNumber1");
            this.pictureBoxNumber1.BackgroundImage = null;
            this.pictureBoxNumber1.Font = null;
            this.pictureBoxNumber1.Image = global::Bat1.Properties.Resources.PasswordKey1;
            this.pictureBoxNumber1.ImageLocation = null;
            this.pictureBoxNumber1.Name = "pictureBoxNumber1";
            this.pictureBoxNumber1.TabStop = false;
            // 
            // pictureBoxNumber2
            // 
            this.pictureBoxNumber2.AccessibleDescription = null;
            this.pictureBoxNumber2.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber2, "pictureBoxNumber2");
            this.pictureBoxNumber2.BackgroundImage = null;
            this.pictureBoxNumber2.Font = null;
            this.pictureBoxNumber2.Image = global::Bat1.Properties.Resources.PasswordKey2;
            this.pictureBoxNumber2.ImageLocation = null;
            this.pictureBoxNumber2.Name = "pictureBoxNumber2";
            this.pictureBoxNumber2.TabStop = false;
            // 
            // pictureBoxNumber3
            // 
            this.pictureBoxNumber3.AccessibleDescription = null;
            this.pictureBoxNumber3.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber3, "pictureBoxNumber3");
            this.pictureBoxNumber3.BackgroundImage = null;
            this.pictureBoxNumber3.Font = null;
            this.pictureBoxNumber3.Image = global::Bat1.Properties.Resources.PasswordKey3;
            this.pictureBoxNumber3.ImageLocation = null;
            this.pictureBoxNumber3.Name = "pictureBoxNumber3";
            this.pictureBoxNumber3.TabStop = false;
            // 
            // pictureBoxNumber4
            // 
            this.pictureBoxNumber4.AccessibleDescription = null;
            this.pictureBoxNumber4.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber4, "pictureBoxNumber4");
            this.pictureBoxNumber4.BackgroundImage = null;
            this.pictureBoxNumber4.Font = null;
            this.pictureBoxNumber4.Image = global::Bat1.Properties.Resources.PasswordKey4;
            this.pictureBoxNumber4.ImageLocation = null;
            this.pictureBoxNumber4.Name = "pictureBoxNumber4";
            this.pictureBoxNumber4.TabStop = false;
            // 
            // pictureBoxKey1
            // 
            this.pictureBoxKey1.AccessibleDescription = null;
            this.pictureBoxKey1.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey1, "pictureBoxKey1");
            this.pictureBoxKey1.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey1.BackgroundImage = null;
            this.pictureBoxKey1.Font = null;
            this.pictureBoxKey1.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey1.ImageLocation = null;
            this.pictureBoxKey1.Name = "pictureBoxKey1";
            this.pictureBoxKey1.TabStop = false;
            // 
            // pictureBoxKey2
            // 
            this.pictureBoxKey2.AccessibleDescription = null;
            this.pictureBoxKey2.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey2, "pictureBoxKey2");
            this.pictureBoxKey2.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey2.BackgroundImage = null;
            this.pictureBoxKey2.Font = null;
            this.pictureBoxKey2.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey2.ImageLocation = null;
            this.pictureBoxKey2.Name = "pictureBoxKey2";
            this.pictureBoxKey2.TabStop = false;
            // 
            // pictureBoxKey3
            // 
            this.pictureBoxKey3.AccessibleDescription = null;
            this.pictureBoxKey3.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey3, "pictureBoxKey3");
            this.pictureBoxKey3.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey3.BackgroundImage = null;
            this.pictureBoxKey3.Font = null;
            this.pictureBoxKey3.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey3.ImageLocation = null;
            this.pictureBoxKey3.Name = "pictureBoxKey3";
            this.pictureBoxKey3.TabStop = false;
            // 
            // pictureBoxKey4
            // 
            this.pictureBoxKey4.AccessibleDescription = null;
            this.pictureBoxKey4.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey4, "pictureBoxKey4");
            this.pictureBoxKey4.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey4.BackgroundImage = null;
            this.pictureBoxKey4.Font = null;
            this.pictureBoxKey4.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey4.ImageLocation = null;
            this.pictureBoxKey4.Name = "pictureBoxKey4";
            this.pictureBoxKey4.TabStop = false;
            // 
            // imageListKey
            // 
            this.imageListKey.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListKey.ImageStream")));
            this.imageListKey.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListKey.Images.SetKeyName(0, "KeyNull.bmp");
            this.imageListKey.Images.SetKeyName(1, "KeyEnter.bmp");
            this.imageListKey.Images.SetKeyName(2, "KeyLeft.bmp");
            this.imageListKey.Images.SetKeyName(3, "KeyEsc.bmp");
            this.imageListKey.Images.SetKeyName(4, "KeyUp.bmp");
            this.imageListKey.Images.SetKeyName(5, "KeyRight.bmp");
            this.imageListKey.Images.SetKeyName(6, "KeyDown.bmp");
            // 
            // UserControlScaleConfigPassword
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.pictureBoxKey4);
            this.Controls.Add(this.pictureBoxKey3);
            this.Controls.Add(this.pictureBoxKey2);
            this.Controls.Add(this.pictureBoxKey1);
            this.Controls.Add(this.pictureBoxNumber4);
            this.Controls.Add(this.pictureBoxNumber3);
            this.Controls.Add(this.pictureBoxNumber2);
            this.Controls.Add(this.pictureBoxNumber1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.radioButtonDisable);
            this.Controls.Add(this.radioButtonEnable);
            this.Controls.Add(this.buttonChange);
            this.Font = null;
            this.Name = "UserControlScaleConfigPassword";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButtonEnable;
        private System.Windows.Forms.RadioButton radioButtonDisable;
        private System.Windows.Forms.Button buttonChange;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxNumber1;
        private System.Windows.Forms.PictureBox pictureBoxNumber2;
        private System.Windows.Forms.PictureBox pictureBoxNumber3;
        private System.Windows.Forms.PictureBox pictureBoxNumber4;
        private System.Windows.Forms.PictureBox pictureBoxKey1;
        private System.Windows.Forms.PictureBox pictureBoxKey2;
        private System.Windows.Forms.PictureBox pictureBoxKey3;
        private System.Windows.Forms.PictureBox pictureBoxKey4;
        private System.Windows.Forms.ImageList imageListKey;
    }
}
