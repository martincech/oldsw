﻿namespace Bat1 {
    partial class FormSelectWeighing {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSelectWeighing));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanelAvailable = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewAvailableWeighings = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonDetailsAvailable = new System.Windows.Forms.Button();
            this.labelAvailable = new System.Windows.Forms.Label();
            this.panelStatistics = new System.Windows.Forms.Panel();
            this.tableLayoutPanelStatistics = new System.Windows.Forms.TableLayoutPanel();
            this.dataGridViewStatisticsWeighings = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonDetailsStatistics = new System.Windows.Forms.Button();
            this.labelStatistics = new System.Windows.Forms.Label();
            this.panelArrows = new System.Windows.Forms.Panel();
            this.buttonRemoveAll = new System.Windows.Forms.Button();
            this.buttonAddAll = new System.Windows.Forms.Button();
            this.buttonRemove = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.comboBoxScale = new System.Windows.Forms.ComboBox();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.comboBoxFile = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxFilter = new System.Windows.Forms.GroupBox();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.panelBottom = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanelBottom = new System.Windows.Forms.FlowLayoutPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tableLayoutPanelAvailable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAvailableWeighings)).BeginInit();
            this.panelStatistics.SuspendLayout();
            this.tableLayoutPanelStatistics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatisticsWeighings)).BeginInit();
            this.panelArrows.SuspendLayout();
            this.groupBoxFilter.SuspendLayout();
            this.panelBottom.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.AccessibleDescription = null;
            this.splitContainer1.AccessibleName = null;
            resources.ApplyResources(this.splitContainer1, "splitContainer1");
            this.splitContainer1.BackgroundImage = null;
            this.splitContainer1.Font = null;
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AccessibleDescription = null;
            this.splitContainer1.Panel1.AccessibleName = null;
            resources.ApplyResources(this.splitContainer1.Panel1, "splitContainer1.Panel1");
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel1.BackgroundImage = null;
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanelAvailable);
            this.splitContainer1.Panel1.Font = null;
            this.toolTip1.SetToolTip(this.splitContainer1.Panel1, resources.GetString("splitContainer1.Panel1.ToolTip"));
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AccessibleDescription = null;
            this.splitContainer1.Panel2.AccessibleName = null;
            resources.ApplyResources(this.splitContainer1.Panel2, "splitContainer1.Panel2");
            this.splitContainer1.Panel2.BackgroundImage = null;
            this.splitContainer1.Panel2.Controls.Add(this.panelStatistics);
            this.splitContainer1.Panel2.Controls.Add(this.panelArrows);
            this.splitContainer1.Panel2.Font = null;
            this.toolTip1.SetToolTip(this.splitContainer1.Panel2, resources.GetString("splitContainer1.Panel2.ToolTip"));
            this.splitContainer1.TabStop = false;
            this.toolTip1.SetToolTip(this.splitContainer1, resources.GetString("splitContainer1.ToolTip"));
            // 
            // tableLayoutPanelAvailable
            // 
            this.tableLayoutPanelAvailable.AccessibleDescription = null;
            this.tableLayoutPanelAvailable.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanelAvailable, "tableLayoutPanelAvailable");
            this.tableLayoutPanelAvailable.BackgroundImage = null;
            this.tableLayoutPanelAvailable.Controls.Add(this.dataGridViewAvailableWeighings, 0, 1);
            this.tableLayoutPanelAvailable.Controls.Add(this.buttonDetailsAvailable, 0, 2);
            this.tableLayoutPanelAvailable.Controls.Add(this.labelAvailable, 0, 0);
            this.tableLayoutPanelAvailable.Font = null;
            this.tableLayoutPanelAvailable.Name = "tableLayoutPanelAvailable";
            this.toolTip1.SetToolTip(this.tableLayoutPanelAvailable, resources.GetString("tableLayoutPanelAvailable.ToolTip"));
            // 
            // dataGridViewAvailableWeighings
            // 
            this.dataGridViewAvailableWeighings.AccessibleDescription = null;
            this.dataGridViewAvailableWeighings.AccessibleName = null;
            this.dataGridViewAvailableWeighings.AllowUserToAddRows = false;
            this.dataGridViewAvailableWeighings.AllowUserToDeleteRows = false;
            this.dataGridViewAvailableWeighings.AllowUserToResizeRows = false;
            resources.ApplyResources(this.dataGridViewAvailableWeighings, "dataGridViewAvailableWeighings");
            this.dataGridViewAvailableWeighings.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewAvailableWeighings.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewAvailableWeighings.BackgroundImage = null;
            this.dataGridViewAvailableWeighings.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAvailableWeighings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewAvailableWeighings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAvailableWeighings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewAvailableWeighings.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewAvailableWeighings.Font = null;
            this.dataGridViewAvailableWeighings.Name = "dataGridViewAvailableWeighings";
            this.dataGridViewAvailableWeighings.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewAvailableWeighings.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewAvailableWeighings.RowHeadersVisible = false;
            this.dataGridViewAvailableWeighings.RowTemplate.Height = 18;
            this.dataGridViewAvailableWeighings.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewAvailableWeighings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAvailableWeighings.StandardTab = true;
            this.toolTip1.SetToolTip(this.dataGridViewAvailableWeighings, resources.GetString("dataGridViewAvailableWeighings.ToolTip"));
            this.dataGridViewAvailableWeighings.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewAvailableWeighings_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.FillWeight = 40F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn5.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn6.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // buttonDetailsAvailable
            // 
            this.buttonDetailsAvailable.AccessibleDescription = null;
            this.buttonDetailsAvailable.AccessibleName = null;
            resources.ApplyResources(this.buttonDetailsAvailable, "buttonDetailsAvailable");
            this.buttonDetailsAvailable.BackgroundImage = null;
            this.buttonDetailsAvailable.Font = null;
            this.buttonDetailsAvailable.Name = "buttonDetailsAvailable";
            this.toolTip1.SetToolTip(this.buttonDetailsAvailable, resources.GetString("buttonDetailsAvailable.ToolTip"));
            this.buttonDetailsAvailable.UseVisualStyleBackColor = true;
            this.buttonDetailsAvailable.Click += new System.EventHandler(this.buttonDetailsAvailable_Click);
            // 
            // labelAvailable
            // 
            this.labelAvailable.AccessibleDescription = null;
            this.labelAvailable.AccessibleName = null;
            resources.ApplyResources(this.labelAvailable, "labelAvailable");
            this.labelAvailable.Name = "labelAvailable";
            this.toolTip1.SetToolTip(this.labelAvailable, resources.GetString("labelAvailable.ToolTip"));
            // 
            // panelStatistics
            // 
            this.panelStatistics.AccessibleDescription = null;
            this.panelStatistics.AccessibleName = null;
            resources.ApplyResources(this.panelStatistics, "panelStatistics");
            this.panelStatistics.BackgroundImage = null;
            this.panelStatistics.Controls.Add(this.tableLayoutPanelStatistics);
            this.panelStatistics.Font = null;
            this.panelStatistics.Name = "panelStatistics";
            this.toolTip1.SetToolTip(this.panelStatistics, resources.GetString("panelStatistics.ToolTip"));
            // 
            // tableLayoutPanelStatistics
            // 
            this.tableLayoutPanelStatistics.AccessibleDescription = null;
            this.tableLayoutPanelStatistics.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanelStatistics, "tableLayoutPanelStatistics");
            this.tableLayoutPanelStatistics.BackgroundImage = null;
            this.tableLayoutPanelStatistics.Controls.Add(this.dataGridViewStatisticsWeighings, 0, 1);
            this.tableLayoutPanelStatistics.Controls.Add(this.buttonDetailsStatistics, 0, 2);
            this.tableLayoutPanelStatistics.Controls.Add(this.labelStatistics, 0, 0);
            this.tableLayoutPanelStatistics.Font = null;
            this.tableLayoutPanelStatistics.Name = "tableLayoutPanelStatistics";
            this.toolTip1.SetToolTip(this.tableLayoutPanelStatistics, resources.GetString("tableLayoutPanelStatistics.ToolTip"));
            // 
            // dataGridViewStatisticsWeighings
            // 
            this.dataGridViewStatisticsWeighings.AccessibleDescription = null;
            this.dataGridViewStatisticsWeighings.AccessibleName = null;
            this.dataGridViewStatisticsWeighings.AllowUserToAddRows = false;
            this.dataGridViewStatisticsWeighings.AllowUserToDeleteRows = false;
            this.dataGridViewStatisticsWeighings.AllowUserToResizeRows = false;
            resources.ApplyResources(this.dataGridViewStatisticsWeighings, "dataGridViewStatisticsWeighings");
            this.dataGridViewStatisticsWeighings.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewStatisticsWeighings.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewStatisticsWeighings.BackgroundImage = null;
            this.dataGridViewStatisticsWeighings.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewStatisticsWeighings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewStatisticsWeighings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStatisticsWeighings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewStatisticsWeighings.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridViewStatisticsWeighings.Font = null;
            this.dataGridViewStatisticsWeighings.Name = "dataGridViewStatisticsWeighings";
            this.dataGridViewStatisticsWeighings.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewStatisticsWeighings.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridViewStatisticsWeighings.RowHeadersVisible = false;
            this.dataGridViewStatisticsWeighings.RowTemplate.Height = 18;
            this.dataGridViewStatisticsWeighings.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewStatisticsWeighings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewStatisticsWeighings.StandardTab = true;
            this.toolTip1.SetToolTip(this.dataGridViewStatisticsWeighings, resources.GetString("dataGridViewStatisticsWeighings.ToolTip"));
            this.dataGridViewStatisticsWeighings.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStatisticsWeighings_CellDoubleClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.FillWeight = 30F;
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // buttonDetailsStatistics
            // 
            this.buttonDetailsStatistics.AccessibleDescription = null;
            this.buttonDetailsStatistics.AccessibleName = null;
            resources.ApplyResources(this.buttonDetailsStatistics, "buttonDetailsStatistics");
            this.buttonDetailsStatistics.BackgroundImage = null;
            this.buttonDetailsStatistics.Font = null;
            this.buttonDetailsStatistics.Name = "buttonDetailsStatistics";
            this.toolTip1.SetToolTip(this.buttonDetailsStatistics, resources.GetString("buttonDetailsStatistics.ToolTip"));
            this.buttonDetailsStatistics.UseVisualStyleBackColor = true;
            this.buttonDetailsStatistics.Click += new System.EventHandler(this.buttonDetailsStatistics_Click);
            // 
            // labelStatistics
            // 
            this.labelStatistics.AccessibleDescription = null;
            this.labelStatistics.AccessibleName = null;
            resources.ApplyResources(this.labelStatistics, "labelStatistics");
            this.labelStatistics.Name = "labelStatistics";
            this.toolTip1.SetToolTip(this.labelStatistics, resources.GetString("labelStatistics.ToolTip"));
            // 
            // panelArrows
            // 
            this.panelArrows.AccessibleDescription = null;
            this.panelArrows.AccessibleName = null;
            resources.ApplyResources(this.panelArrows, "panelArrows");
            this.panelArrows.BackgroundImage = null;
            this.panelArrows.Controls.Add(this.buttonRemoveAll);
            this.panelArrows.Controls.Add(this.buttonAddAll);
            this.panelArrows.Controls.Add(this.buttonRemove);
            this.panelArrows.Controls.Add(this.buttonAdd);
            this.panelArrows.Font = null;
            this.panelArrows.Name = "panelArrows";
            this.toolTip1.SetToolTip(this.panelArrows, resources.GetString("panelArrows.ToolTip"));
            // 
            // buttonRemoveAll
            // 
            this.buttonRemoveAll.AccessibleDescription = null;
            this.buttonRemoveAll.AccessibleName = null;
            resources.ApplyResources(this.buttonRemoveAll, "buttonRemoveAll");
            this.buttonRemoveAll.BackgroundImage = null;
            this.buttonRemoveAll.Font = null;
            this.buttonRemoveAll.Name = "buttonRemoveAll";
            this.toolTip1.SetToolTip(this.buttonRemoveAll, resources.GetString("buttonRemoveAll.ToolTip"));
            this.buttonRemoveAll.UseVisualStyleBackColor = true;
            this.buttonRemoveAll.Click += new System.EventHandler(this.buttonRemoveAll_Click);
            // 
            // buttonAddAll
            // 
            this.buttonAddAll.AccessibleDescription = null;
            this.buttonAddAll.AccessibleName = null;
            resources.ApplyResources(this.buttonAddAll, "buttonAddAll");
            this.buttonAddAll.BackgroundImage = null;
            this.buttonAddAll.Font = null;
            this.buttonAddAll.Name = "buttonAddAll";
            this.toolTip1.SetToolTip(this.buttonAddAll, resources.GetString("buttonAddAll.ToolTip"));
            this.buttonAddAll.UseVisualStyleBackColor = true;
            this.buttonAddAll.Click += new System.EventHandler(this.buttonAddAll_Click);
            // 
            // buttonRemove
            // 
            this.buttonRemove.AccessibleDescription = null;
            this.buttonRemove.AccessibleName = null;
            resources.ApplyResources(this.buttonRemove, "buttonRemove");
            this.buttonRemove.BackgroundImage = null;
            this.buttonRemove.Font = null;
            this.buttonRemove.Name = "buttonRemove";
            this.toolTip1.SetToolTip(this.buttonRemove, resources.GetString("buttonRemove.ToolTip"));
            this.buttonRemove.UseVisualStyleBackColor = true;
            this.buttonRemove.Click += new System.EventHandler(this.buttonRemove_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.AccessibleDescription = null;
            this.buttonAdd.AccessibleName = null;
            resources.ApplyResources(this.buttonAdd, "buttonAdd");
            this.buttonAdd.BackgroundImage = null;
            this.buttonAdd.Font = null;
            this.buttonAdd.Name = "buttonAdd";
            this.toolTip1.SetToolTip(this.buttonAdd, resources.GetString("buttonAdd.ToolTip"));
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // comboBoxScale
            // 
            this.comboBoxScale.AccessibleDescription = null;
            this.comboBoxScale.AccessibleName = null;
            resources.ApplyResources(this.comboBoxScale, "comboBoxScale");
            this.comboBoxScale.BackgroundImage = null;
            this.comboBoxScale.DropDownHeight = 400;
            this.comboBoxScale.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxScale.Font = null;
            this.comboBoxScale.FormattingEnabled = true;
            this.comboBoxScale.Name = "comboBoxScale";
            this.toolTip1.SetToolTip(this.comboBoxScale, resources.GetString("comboBoxScale.ToolTip"));
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.AccessibleDescription = null;
            this.dateTimePickerTo.AccessibleName = null;
            resources.ApplyResources(this.dateTimePickerTo, "dateTimePickerTo");
            this.dateTimePickerTo.BackgroundImage = null;
            this.dateTimePickerTo.CalendarFont = null;
            this.dateTimePickerTo.CustomFormat = null;
            this.dateTimePickerTo.Font = null;
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.ShowCheckBox = true;
            this.toolTip1.SetToolTip(this.dateTimePickerTo, resources.GetString("dateTimePickerTo.ToolTip"));
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Font = null;
            this.label2.Name = "label2";
            this.toolTip1.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // label5
            // 
            this.label5.AccessibleDescription = null;
            this.label5.AccessibleName = null;
            resources.ApplyResources(this.label5, "label5");
            this.label5.Font = null;
            this.label5.Name = "label5";
            this.toolTip1.SetToolTip(this.label5, resources.GetString("label5.ToolTip"));
            // 
            // label3
            // 
            this.label3.AccessibleDescription = null;
            this.label3.AccessibleName = null;
            resources.ApplyResources(this.label3, "label3");
            this.label3.Font = null;
            this.label3.Name = "label3";
            this.toolTip1.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.AccessibleDescription = null;
            this.dateTimePickerFrom.AccessibleName = null;
            resources.ApplyResources(this.dateTimePickerFrom, "dateTimePickerFrom");
            this.dateTimePickerFrom.BackgroundImage = null;
            this.dateTimePickerFrom.CalendarFont = null;
            this.dateTimePickerFrom.CustomFormat = null;
            this.dateTimePickerFrom.Font = null;
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.ShowCheckBox = true;
            this.toolTip1.SetToolTip(this.dateTimePickerFrom, resources.GetString("dateTimePickerFrom.ToolTip"));
            // 
            // comboBoxFile
            // 
            this.comboBoxFile.AccessibleDescription = null;
            this.comboBoxFile.AccessibleName = null;
            resources.ApplyResources(this.comboBoxFile, "comboBoxFile");
            this.comboBoxFile.BackgroundImage = null;
            this.comboBoxFile.DropDownHeight = 400;
            this.comboBoxFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFile.Font = null;
            this.comboBoxFile.FormattingEnabled = true;
            this.comboBoxFile.Name = "comboBoxFile";
            this.toolTip1.SetToolTip(this.comboBoxFile, resources.GetString("comboBoxFile.ToolTip"));
            // 
            // label4
            // 
            this.label4.AccessibleDescription = null;
            this.label4.AccessibleName = null;
            resources.ApplyResources(this.label4, "label4");
            this.label4.Font = null;
            this.label4.Name = "label4";
            this.toolTip1.SetToolTip(this.label4, resources.GetString("label4.ToolTip"));
            // 
            // groupBoxFilter
            // 
            this.groupBoxFilter.AccessibleDescription = null;
            this.groupBoxFilter.AccessibleName = null;
            resources.ApplyResources(this.groupBoxFilter, "groupBoxFilter");
            this.groupBoxFilter.BackgroundImage = null;
            this.groupBoxFilter.Controls.Add(this.comboBoxFile);
            this.groupBoxFilter.Controls.Add(this.comboBoxScale);
            this.groupBoxFilter.Controls.Add(this.label4);
            this.groupBoxFilter.Controls.Add(this.dateTimePickerTo);
            this.groupBoxFilter.Controls.Add(this.dateTimePickerFrom);
            this.groupBoxFilter.Controls.Add(this.label2);
            this.groupBoxFilter.Controls.Add(this.label3);
            this.groupBoxFilter.Controls.Add(this.label5);
            this.groupBoxFilter.Font = null;
            this.groupBoxFilter.Name = "groupBoxFilter";
            this.groupBoxFilter.TabStop = false;
            this.toolTip1.SetToolTip(this.groupBoxFilter, resources.GetString("groupBoxFilter.ToolTip"));
            // 
            // buttonOk
            // 
            this.buttonOk.AccessibleDescription = null;
            this.buttonOk.AccessibleName = null;
            resources.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.BackgroundImage = null;
            this.buttonOk.Font = null;
            this.buttonOk.Name = "buttonOk";
            this.toolTip1.SetToolTip(this.buttonOk, resources.GetString("buttonOk.ToolTip"));
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.AccessibleDescription = null;
            this.buttonCancel.AccessibleName = null;
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.BackgroundImage = null;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Font = null;
            this.buttonCancel.Name = "buttonCancel";
            this.toolTip1.SetToolTip(this.buttonCancel, resources.GetString("buttonCancel.ToolTip"));
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // panelBottom
            // 
            this.panelBottom.AccessibleDescription = null;
            this.panelBottom.AccessibleName = null;
            resources.ApplyResources(this.panelBottom, "panelBottom");
            this.panelBottom.BackgroundImage = null;
            this.panelBottom.Controls.Add(this.tableLayoutPanel1);
            this.panelBottom.Font = null;
            this.panelBottom.Name = "panelBottom";
            this.toolTip1.SetToolTip(this.panelBottom, resources.GetString("panelBottom.ToolTip"));
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AccessibleDescription = null;
            this.tableLayoutPanel1.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.BackgroundImage = null;
            this.tableLayoutPanel1.Controls.Add(this.groupBoxFilter, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.splitContainer1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanelBottom, 0, 2);
            this.tableLayoutPanel1.Font = null;
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.toolTip1.SetToolTip(this.tableLayoutPanel1, resources.GetString("tableLayoutPanel1.ToolTip"));
            // 
            // flowLayoutPanelBottom
            // 
            this.flowLayoutPanelBottom.AccessibleDescription = null;
            this.flowLayoutPanelBottom.AccessibleName = null;
            resources.ApplyResources(this.flowLayoutPanelBottom, "flowLayoutPanelBottom");
            this.flowLayoutPanelBottom.BackgroundImage = null;
            this.flowLayoutPanelBottom.Controls.Add(this.buttonCancel);
            this.flowLayoutPanelBottom.Controls.Add(this.buttonOk);
            this.flowLayoutPanelBottom.Font = null;
            this.flowLayoutPanelBottom.Name = "flowLayoutPanelBottom";
            this.toolTip1.SetToolTip(this.flowLayoutPanelBottom, resources.GetString("flowLayoutPanelBottom.ToolTip"));
            // 
            // FormSelectWeighing
            // 
            this.AcceptButton = this.buttonOk;
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.CancelButton = this.buttonCancel;
            this.Controls.Add(this.panelBottom);
            this.Font = null;
            this.Icon = null;
            this.Name = "FormSelectWeighing";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.Load += new System.EventHandler(this.FormSelectWeighing_Load);
            this.Shown += new System.EventHandler(this.FormSelectWeighing_Shown);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.tableLayoutPanelAvailable.ResumeLayout(false);
            this.tableLayoutPanelAvailable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAvailableWeighings)).EndInit();
            this.panelStatistics.ResumeLayout(false);
            this.tableLayoutPanelStatistics.ResumeLayout(false);
            this.tableLayoutPanelStatistics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStatisticsWeighings)).EndInit();
            this.panelArrows.ResumeLayout(false);
            this.groupBoxFilter.ResumeLayout(false);
            this.groupBoxFilter.PerformLayout();
            this.panelBottom.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanelBottom.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxScale;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.ComboBox comboBoxFile;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button buttonRemove;
        internal System.Windows.Forms.DataGridView dataGridViewStatisticsWeighings;
        private System.Windows.Forms.GroupBox groupBoxFilter;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panelArrows;
        private System.Windows.Forms.Panel panelStatistics;
        private System.Windows.Forms.Button buttonDetailsStatistics;
        private System.Windows.Forms.Panel panelBottom;
        private System.Windows.Forms.Button buttonRemoveAll;
        private System.Windows.Forms.Button buttonAddAll;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelStatistics;
        private System.Windows.Forms.Label labelStatistics;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelAvailable;
        internal System.Windows.Forms.DataGridView dataGridViewAvailableWeighings;
        private System.Windows.Forms.Button buttonDetailsAvailable;
        private System.Windows.Forms.Label labelAvailable;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBottom;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    }
}