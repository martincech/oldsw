﻿//******************************************************************************
//
//   SampleList.cs      List of samples
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    /// <summary>
    /// List of samples
    /// </summary>
    public class SampleList {
        /// <summary>
        /// List of samples
        /// </summary>
        private List<Sample> list = new List<Sample>();

        /// <summary>
        /// Minimum date/time in the list (undefined when the list is empty)
        /// </summary>
        public DateTime MinDateTime { get { return minDateTime; } }
        private DateTime minDateTime;

        /// <summary>
        /// Maximum date/time in the list (undefined when the list is empty)
        /// </summary>
        public DateTime MaxDateTime { get { return maxDateTime; } }
        private DateTime maxDateTime;

        /// <summary>
        /// Position during searching
        /// </summary>
        private int findPosition;

        /// <summary>
        /// Sample read when searching. Call Read() method before reading this property.
        /// </summary>
        public Sample Sample { get { return sample; } }
        private Sample sample;

        /// <summary>
        /// Constructor
        /// </summary>
        public SampleList() {
            Clear();
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        public SampleList(SampleList source) {
            // Zkopiruju vzorky
            Clear();
            list.AddRange(source.list);

            // Min a max
            minDateTime  = source.minDateTime;
            maxDateTime  = source.maxDateTime;
        }

        /// <summary>
        /// Clear min and max
        /// </summary>
        private void ClearMinMaxDateTime() {
            minDateTime = DateTime.MaxValue;
            maxDateTime = DateTime.MinValue;
        }
        
        /// <summary>
        /// Test sample date/time and update min and max
        /// </summary>
        /// <param name="sample">Sample</param>
        private void UpdateMinMaxDateTime(Sample sample) {
            if (sample.dateTime < minDateTime) {
                minDateTime = sample.dateTime;
            }
            if (sample.dateTime > maxDateTime) {
                maxDateTime = sample.dateTime;
            }
        }
        
        /// <summary>
        /// Add sample to list
        /// </summary>
        /// <param name="sample">Sample to add</param>
        public void Add(Sample sample) {
            // Pridam vzorek. Trideni podle data je treba udelat externe (v databazi muzou byt vzorky zprehazene,
            // napr. po importu)
            list.Add(sample);
            
            // Pokud jsem pridal prvni vzorek, nastavim minimalni a maximalni datum/cas na tento vzorek
            if (list.Count == 1) {
                minDateTime = sample.dateTime;
                maxDateTime = sample.dateTime;
                return;
            }

            // Obnovim min/max datum a cas
            UpdateMinMaxDateTime(sample);
        }

        public void SortByTime() {
            list.Sort(Sample.CompareByTime);
        }

        public void SortByWeight() {
            list.Sort(Sample.CompareByWeight);
        }

        /// <summary>
        /// Remove sample from the list
        /// </summary>
        /// <param name="index">Sample index</param>
        public void RemoveAt(int index) {
            // Smazu vzorek ze seznamu
            list.RemoveAt(index);

            // Prepoctu min a max datumy
            if (list.Count == 0) {
                Clear();
                return;
            }

            // Updatuju min a max datumy
            ClearMinMaxDateTime();
            foreach (Sample sample in list) {
                UpdateMinMaxDateTime(sample);
            }
        }
        
        /// <summary>
        /// Go through samples with specified flag and remove Nth sample from the list
        /// </summary>
        /// <param name="index">Sample index</param>
        /// <param name="flag">Sample flag</param>
        public void RemoveAt(int index, Flag flag) {
            if (index < 0 || index >= list.Count) {
                return;     // Mimo rozsah
            }

            if (flag == Flag.ALL) {
                // Muzu jednoduse odstranit vzorek ze seznamu nehlede na flag
                RemoveAt(index);
                return;
            }

            // Najdu index pozadovaneho vzorku v ramci celeho seznamu. Hledam <index>-ty vzorek s flagem <flag>.
            int globalIndex = 0;        // Index v celem seznamu
            First();
            while (Read()) {
                if (sample.flag == flag) {
                    if (index == 0) {
                        // Dorazil jsem na pozadovany vzorek (index-ty se zadanym flagem), smazu ho
                        RemoveAt(globalIndex);
                        return;
                    }
                    if (index < 0) {
                        return;     // Uz jsem mimo
                    }
                    index--;        // Postupne odpocitavam
                }
                globalIndex++;
            }
        }

        /// <summary>
        /// Clear all samples
        /// </summary>
        public void Clear() {
            list.Clear();
            ClearMinMaxDateTime();
            findPosition = -1;
        }

        /// <summary>
        /// Get number of samples in the list
        /// </summary>
        public int Count {
            get {
                return list.Count;
            }
        }

        /// <summary>
        /// Returns true if [flag] corresponds to [sampleFlag]
        /// </summary>
        /// <param name="sampleFlag">Flag of the sample</param>
        /// <param name="flag">Flag that I am looking for</param>
        /// <returns></returns>
        private bool CheckFlag(Flag sampleFlag, Flag flag) {
            if (flag == Flag.ALL) {
                return true;        // Beru vsechny flagy, vzorek urcite vyhovi
            }
            return (sampleFlag == flag);
        }
        
        /// <summary>
        /// Move to the first sample
        /// </summary>
        /// <param name="flag">Filter samples with specified flag</param>
        public void First(Flag flag) {
            findPosition = -1;

            // Pokusim se najit prvni vzorek se zadanym flagem
            for (int i = 0; i < list.Count; i++) {
                if (CheckFlag(list[i].flag, flag)) {
                    findPosition = i;
                    return;
                }
            }
        }

        /// <summary>
        /// Move to the first sample
        /// </summary>
        public void First() {
            First(Flag.ALL);
        }

        /// <summary>
        /// Move to next sample
        /// </summary>
        /// <param name="flag">Filter samples with specified flag</param>
        private void Next(Flag flag) {
            if (findPosition < 0) {
                return;
            }
            // Pokusim se najit dalsi vzorek se zadanym flagem
            for (findPosition++; findPosition < list.Count; findPosition++) {
                if (CheckFlag(list[findPosition].flag, flag)) {
                    return;         // Nasel jsem
                }
            }
            // Nenasel jsem, oznacim konec seznamu
            findPosition = -1;
        }

        /// <summary>
        /// Move to next sample
        /// </summary>
        private void Next() {
            Next(Flag.ALL);
        }

        /// <summary>
        /// Read current sample to Sample property and move to next sample
        /// </summary>
        /// <param name="flag">Filter samples with specified flag</param>
        /// <returns>true if the sample was read successfuly</returns>
        public bool Read(Flag flag) {
            if (findPosition < 0) {
                return false;           // Vzorek nelze nacist (prazdny seznam, konec seznamu)
            }

            // Nactu vzorek na aktualni pozici
            sample = list[findPosition];

            // Posunu se na dalsi vzorek
            Next(flag);

            return true;
        }

        /// <summary>
        /// Read current sample to Sample property and move to next sample
        /// </summary>
        /// <returns>true if the sample was read successfuly</returns>
        public bool Read() {
            return Read(Flag.ALL);
        }

        /// <summary>
        /// Get list of weights with specified flag
        /// </summary>
        /// <param name="flag">Flag filter</param>
        /// <returns>List of weights</returns>
        public List<double> GetWeights(Flag flag) {
            List<double> weightList = new List<double>();
            First(flag);
            while (Read(flag)) {
                weightList.Add(sample.weight);
            }
            return weightList;
        }

        /// <summary>
        /// Get list of weights with specified flag
        /// </summary>
        /// <returns>List of weights</returns>
        public List<double> GetWeights() {
            return GetWeights(Flag.ALL);
        }

        /// <summary>
        /// Convert all weights from one units to another
        /// </summary>
        /// <param name="oldUnits">Old units</param>
        /// <param name="newUnits">New units</param>
        public void Convert(Units oldUnits, Units newUnits) {
            if (oldUnits == newUnits) {
                return;     // Neni treba prepocitavat
            }
            
            // Vytvorim novy seznam s prepoctenymi hmotnostmi
            List<Sample> newList = new List<Sample>(list.Count);
            foreach (Sample sample in list) {
                Sample newSample = sample;
                newSample.weight = (float)ConvertWeight.Convert(sample.weight, oldUnits, newUnits);
                newList.Add(newSample);
            }

            // Nahradim puvodni seznam novym
            list = newList;
        }

        /// <summary>
        /// Move decimal point of weight
        /// </summary>
        /// <param name="oldUnits">Old units</param>
        /// <param name="newUnits">New units</param>
        public void MoveDecimalPoint(Units oldUnits, Units newUnits) {
            // Osetrim pripady, kdy neni treba prepocet - provadim pouze pri prevodu z gramu nebo na gramy, mezi kg-lb ne
            if (oldUnits == newUnits) {
                return;
            }
            if (oldUnits == Units.KG && newUnits == Units.LB
             || oldUnits == Units.LB && newUnits == Units.KG) {
                return;
            }

            // Vytvorim novy seznam s prepoctenymi hmotnostmi
            List<Sample> newList = new List<Sample>(list.Count);
            foreach (Sample sample in list) {
                Sample newSample = sample;
                newSample.weight = ConvertWeight.MoveDecimalPoint(sample.weight, oldUnits, newUnits);
                newList.Add(newSample);
            }

            // Nahradim puvodni seznam novym
            list = newList;
        }

        /// <summary>
        /// Set new weighing start (all samples will be moved in time)
        /// </summary>
        /// <param name="started">Date and time when the first sample was weighed</param>
        public void SetWeighingStart(DateTime started) {
            if (started == minDateTime) {
                return;         // Neni treba posouvat
            }
            
            // Vypoctu casovy rozdil, o ktery budu posouvat vsechny vzorky
            TimeSpan timeSpan = started - minDateTime;

            // Zalohuju si stavajici seznam
            List<Sample> oldSampleList = new List<Sample>();
            oldSampleList.AddRange(list);

            // Naplnim seznam znovu s posunutym casem
            Clear();
            foreach (Sample sample in oldSampleList) {
                Sample newSample = sample;
                newSample.dateTime += timeSpan;
                Add(newSample);
            }
        }

        /// <summary>
        /// Divide weight in all samples
        /// </summary>
        /// <param name="divideBy">Divide by number</param>
        /// <param name="units">Units used</param>
        public void DivideWeights(float divideBy, Units units) {
            if (divideBy == 1.0F) {
                return;         // Neni treba delit
            }
            
            if (divideBy == 0.0F) {
                return;         // Deleni nulou
            }

            // Zalohuju si stavajici seznam
            List<Sample> oldSampleList = new List<Sample>();
            oldSampleList.AddRange(list);

            // Naplnim seznam znovu s vydelenou hmotnosti
            Clear();
            foreach (Sample sample in oldSampleList) {
                Sample newSample = sample;
                newSample.weight /= divideBy;
                
                // Hmotnost nesmi klesnout na nulu (podle pouzitych jednotek)
                float minWeight = (float)ConvertWeight.MinWeight(units);
                if (newSample.weight < minWeight) {
                    newSample.weight = minWeight;
                }

                // Hmotnost nesmi byt vyssi nez vazivost (podle pouzitych jednotek)
                float maxWeight = (float)ConvertWeight.MaxWeight(units, WeighingCapacity.EXTENDED); // Povolim 50kg
                if (newSample.weight > maxWeight) {
                    newSample.weight = maxWeight;
                }

                Add(newSample);
            }
        }

    }
}
