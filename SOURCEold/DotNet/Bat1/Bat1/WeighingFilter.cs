﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public class WeighingFilter {
        /// <summary>
        /// All weighings available for filtration (read from database)
        /// </summary>
        public WeighingSearchInfoFilter WeighingInfoFilter { get { return weighingInfoFilter; } }
        private WeighingSearchInfoFilter weighingInfoFilter;

        /// <summary>
        /// Show or hide seconds in the Date/time column
        /// </summary>
        public bool ShowSeconds = false;

        // Controly v rodicovskem okne
        private Control parentControl;
        private DateTimePicker dateTimePickerFrom;
        private DateTimePicker dateTimePickerTo;
        private ComboBox comboBoxFile;
        private ComboBox comboBoxScale;
        private DataGridView dataGridViewAvailableWeighings;

        // Prave se nahravaji data do seznamu, neobnovuju zobrazeni
        private bool isLoading = false;                         

        /// <summary>
        /// List of available weighings changed event delegate
        /// </summary>
        /// <param name="sender"></param>
        public delegate void ChangedEventHandler(object sender);

        /// <summary>
        /// List of available weighings changed event
        /// </summary>
        public event ChangedEventHandler ChangedEvent;
        
        public WeighingFilter(Control parentControl,
                              DateTimePicker dateTimePickerFrom,
                              DateTimePicker dateTimePickerTo,
                              ComboBox comboBoxFile,
                              ComboBox comboBoxScale,
                              DataGridView dataGridViewAvailableWeighings) {
            // Vytvorim seznam, nactu ho z databaze az pri vykreslovani
            weighingInfoFilter = new WeighingSearchInfoFilter();
            
            // Preberu controly
            this.parentControl                  = parentControl;
            this.dateTimePickerFrom             = dateTimePickerFrom;
            this.dateTimePickerTo               = dateTimePickerTo;
            this.comboBoxFile                   = comboBoxFile;
            this.comboBoxScale                  = comboBoxScale;
            this.dataGridViewAvailableWeighings = dataGridViewAvailableWeighings;

            // Nastavim eventy
            comboBoxFile.SelectionChangeCommitted  += new EventHandler(comboBoxFile_SelectionChangeCommitted);
            comboBoxScale.SelectionChangeCommitted += new EventHandler(comboBoxScale_SelectionChangeCommitted);
            dateTimePickerFrom.ValueChanged        += new EventHandler(dateTimePickerFrom_ValueChanged);
            dateTimePickerFrom.EnabledChanged      += new EventHandler(dateTimePickerFrom_ValueChanged);
            dateTimePickerTo.ValueChanged          += new EventHandler(dateTimePickerTo_ValueChanged);
            dateTimePickerTo.EnabledChanged        += new EventHandler(dateTimePickerTo_ValueChanged);
        }

        /// <summary>
        /// Load available weighings into the controls
        /// </summary>
        public void LoadWeighings() {
            try {
                // Pro zrychleni
                comboBoxFile.BeginUpdate();
                comboBoxScale.BeginUpdate();
                
                // Vyprazdnim comboboxy a jako prvni polozku dam <all>
                comboBoxFile.Items.Clear();
                comboBoxFile.Items.Add(Properties.Resources.SELECT_WEIGHINGS_ALL_FILES);
                comboBoxScale.Items.Clear();
                comboBoxScale.Items.Add(Properties.Resources.SELECT_WEIGHINGS_ALL_SCALES);

                // Nahraju seznam vazeni
                weighingInfoFilter.Load(Program.Database.ReadWeighingSearchInfo());

                // Naplnim comboboxy vah a souboru
                foreach (string file in weighingInfoFilter.FilesList) {
                    comboBoxFile.Items.Add(file);
                }
                foreach (string scale in weighingInfoFilter.ScalesList) {
                    comboBoxScale.Items.Add(scale);
                }
            } finally {
                comboBoxFile.EndUpdate();
                comboBoxScale.EndUpdate();
            }
        }

        /// <summary>
        /// Show weighings that match to the specified filter
        /// </summary>
        public void ShowAvailableWeighings() {
            // Zobrazim seznam vazeni
            dataGridViewAvailableWeighings.Rows.Clear();

            weighingInfoFilter.Execute();
            foreach (WeighingSearchInfo weighingInfo in weighingInfoFilter.FilteredInfoList) {
                string dateTimeString;
                if (ShowSeconds) {
                    dateTimeString = weighingInfo.minDateTime.ToString("G");    // Vcetne sekund
                } else {
                    dateTimeString = weighingInfo.minDateTime.ToString("g");    // Bez sekund
                }
                dataGridViewAvailableWeighings.Rows.Add(dateTimeString, weighingInfo.fileName,
                                                        weighingInfo.scaleName, weighingInfo.note);
            }
            
            // Nahodim event
            if (ChangedEvent != null) {
                ChangedEvent(this);
            }
        }

        public void Load() {
            // Nahraju vazeni
            Cursor.Current = Cursors.WaitCursor;
            isLoading = true;       // Zakazu obnovu zobrazeni
            
            try {
                // Nahraju vsechna vazeni z databaze 
                LoadWeighings();

                // Default zvolim vsechny vahy i soubory
                comboBoxFile.SelectedIndex  = 0;
                comboBoxScale.SelectedIndex = 0;

                // Casovy filtr zakazu a datumy nastavim na dnesek
                dateTimePickerFrom.Value   = DateTime.Now;
                dateTimePickerFrom.Checked = false;
                dateTimePickerTo.Value     = DateTime.Now;
                dateTimePickerTo.Checked   = false;

                // Zobrazim seznam dostupnych vazeni
                ShowAvailableWeighings();
            } finally {
                isLoading = false;          // Data uz jsou nahrana
                Cursor.Current = Cursors.Default;
            }
        }

        public void BeginUpdate() {
            isLoading = true;
        }

        public void EndUpdate() {
            isLoading = false;
        }

        private void comboBoxFile_SelectionChangeCommitted(object sender, EventArgs e) {
            // Nastavim filtr souboru
            weighingInfoFilter.FileFilter = comboBoxFile.SelectedIndex == 0 ? null : comboBoxFile.Text;

            // Zobrazim seznam vazeni
            ShowAvailableWeighings();
        }

        private void comboBoxScale_SelectionChangeCommitted(object sender, EventArgs e) {
            // Nastavim filtr vahy
            weighingInfoFilter.ScaleFilter = comboBoxScale.SelectedIndex == 0 ? null : comboBoxScale.Text;

            // Zobrazim seznam vazeni
            ShowAvailableWeighings();
        }

        private void dateTimePickerFrom_ValueChanged(object sender, EventArgs e) {
            if (isLoading) {
                // Seznamy se obnovuji, nezobrazuju
                return;
            }

            // Nastavim casovy filtr od
            weighingInfoFilter.MinDateFilter = dateTimePickerFrom.Checked ? dateTimePickerFrom.Value : DateTime.MinValue;

            // Zobrazim seznam vazeni
            ShowAvailableWeighings();
        }

        private void dateTimePickerTo_ValueChanged(object sender, EventArgs e) {
            if (isLoading) {
                // Seznamy se obnovuji, nezobrazuju
                return;
            }

            // Nastavim casovy filtr do
            weighingInfoFilter.MaxDateFilter = dateTimePickerTo.Checked ? dateTimePickerTo.Value : DateTime.MaxValue;

            // Zobrazim seznam vazeni
            ShowAvailableWeighings();
        }


    }
}
