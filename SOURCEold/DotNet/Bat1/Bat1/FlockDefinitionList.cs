﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// List of flock definitions
    /// </summary>
    public class FlockDefinitionList {
        /// <summary>
        /// List of flock definitions sorted by name
        /// </summary>
        public List<FlockDefinition> List { get { return list; } }
        private List<FlockDefinition> list = new List<FlockDefinition>();

        /// <summary>
        /// Constructor
        /// </summary>
        public FlockDefinitionList() {
        }

        /// <summary>
        /// Check if flock index is valid within the list
        /// </summary>
        /// <param name="index">Curve index</param>
        /// <returns>True if valid</returns>
        public bool CheckIndex(int index) {
          return (bool)(index >= 0 && index < list.Count);
        }

        /// <summary>
        /// Check if flock definition exists
        /// </summary>
        /// <param name="name">Flock definition name</param>
        /// <returns>True if exists</returns>
        public bool Exists(string name) {
            return list.Exists(delegate(FlockDefinition flockDefinition) { return flockDefinition.Name == name; });
        }
        
        /// <summary>
        /// Sort the list by name
        /// </summary>
        public void SortByName() {
            list.Sort(FlockDefinition.CompareByName);
        }
        
        /// <summary>
        /// Sort the list by time of flock start
        /// </summary>
        public void SortByTime() {
            list.Sort(FlockDefinition.CompareByTime);
        }

        /// <summary>
        /// Check if a flock definiton name is valid
        /// </summary>
        /// <param name="name">Flock definiton name</param>
        /// <returns>True if valid</returns>
        public bool CheckName(string name) {
            // Kontrola delky nazvu
            if (name.Length == 0) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Add flock definiton to the list
        /// </summary>
        /// <param name="file">Flock definiton to add</param>
        /// <returns>True if successful</returns>
        public bool Add(FlockDefinition flockDefinition) {
            // Kontrola jmena
            if (!CheckName(flockDefinition.Name)) {
                return false;       // Jmeno ma neplatny format
            }
            
            // Zkontroluju, zda uz definice se zadanym jmenem v seznamu neexistuje
            if (Exists(flockDefinition.Name)) {
                return false;
            }

            // Pridam do seznamu a setridim podle jmena
            list.Add(flockDefinition);
            SortByName();

            return true;
        }
        
        /// <summary>
        /// Delete flock definiton from the list
        /// </summary>
        /// <param name="index">Flock definiton index</param>
        public void Delete(int index) {
            if (!CheckIndex(index)) {
                return;
            }
            list.RemoveAt(index);
        }

        /// <summary>
        /// Rename flock definiton
        /// </summary>
        /// <param name="index">Flock definiton index</param>
        /// <param name="newName">New name</param>
        /// <returns>True if successful</returns>
        public bool Rename(int index, string newName) {
            if (!CheckIndex(index)) {
                return false;
            }

            // Zkontroluju platnost jmena
            if (newName == "") {
                return false;
            }
            
            // Zkontroluju, zda vubec je treba zmena
            if (list[index].Name == newName) {
                return true;        // Neni treba prejmenovavat, vratim true jako bych prejmenoval
            }
            
            // Zkontroluju, zda uz jmeno v seznamu neni na jine pozici
            if (Exists(newName)) {
                return false;       // Definice se zadanym jmenem uz existuje
            }
            
            // Zmenim jmeno a znovu setridim
            list[index].Name = newName;
            SortByName();

            return true;
        }

        /// <summary>
        /// Find flock definiton index
        /// </summary>
        /// <param name="name">Flock name</param>
        /// <returns>Index in the list</returns>
        public int GetIndex(string name) {
            for (int i = 0; i < list.Count; i++) {
                if (list[i].Name == name) {
                    return i;
                }
            }

            // Nenasel jsem
            return -1;
        }

        /// <summary>
        /// Get flock definition with specified name or null if doesn't exist
        /// </summary>
        /// <param name="name">Flock name</param>
        /// <returns>FlockDefinition</returns>
        public FlockDefinition GetFlockDefinition(string name) {
            int index = GetIndex(name);
            if (index < 0) {
                return null;
            }
            return List[index];
        }

    }
}