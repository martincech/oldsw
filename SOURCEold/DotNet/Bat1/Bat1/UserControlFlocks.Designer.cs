﻿namespace Bat1 {
    partial class UserControlFlocks {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlFlocks));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageFlocks = new System.Windows.Forms.TabPage();
            this.userControlEditFlocks = new Bat1.UserControlEditFlocks();
            this.tabPageCurrentFlocks = new System.Windows.Forms.TabPage();
            this.tabPageSelectedFlocks = new System.Windows.Forms.TabPage();
            this.tabPageCurves = new System.Windows.Forms.TabPage();
            this.userControlEditCurves = new Bat1.UserControlEditCurves();
            this.tabControl.SuspendLayout();
            this.tabPageFlocks.SuspendLayout();
            this.tabPageCurves.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.AccessibleDescription = null;
            this.tabControl.AccessibleName = null;
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.BackgroundImage = null;
            this.tabControl.Controls.Add(this.tabPageFlocks);
            this.tabControl.Controls.Add(this.tabPageCurrentFlocks);
            this.tabControl.Controls.Add(this.tabPageSelectedFlocks);
            this.tabControl.Controls.Add(this.tabPageCurves);
            this.tabControl.Font = null;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tabPageFlocks
            // 
            this.tabPageFlocks.AccessibleDescription = null;
            this.tabPageFlocks.AccessibleName = null;
            resources.ApplyResources(this.tabPageFlocks, "tabPageFlocks");
            this.tabPageFlocks.BackgroundImage = null;
            this.tabPageFlocks.Controls.Add(this.userControlEditFlocks);
            this.tabPageFlocks.Font = null;
            this.tabPageFlocks.Name = "tabPageFlocks";
            this.tabPageFlocks.UseVisualStyleBackColor = true;
            // 
            // userControlEditFlocks
            // 
            this.userControlEditFlocks.AccessibleDescription = null;
            this.userControlEditFlocks.AccessibleName = null;
            resources.ApplyResources(this.userControlEditFlocks, "userControlEditFlocks");
            this.userControlEditFlocks.BackgroundImage = null;
            this.userControlEditFlocks.Font = null;
            this.userControlEditFlocks.Name = "userControlEditFlocks";
            // 
            // tabPageCurrentFlocks
            // 
            this.tabPageCurrentFlocks.AccessibleDescription = null;
            this.tabPageCurrentFlocks.AccessibleName = null;
            resources.ApplyResources(this.tabPageCurrentFlocks, "tabPageCurrentFlocks");
            this.tabPageCurrentFlocks.BackgroundImage = null;
            this.tabPageCurrentFlocks.Font = null;
            this.tabPageCurrentFlocks.Name = "tabPageCurrentFlocks";
            this.tabPageCurrentFlocks.UseVisualStyleBackColor = true;
            // 
            // tabPageSelectedFlocks
            // 
            this.tabPageSelectedFlocks.AccessibleDescription = null;
            this.tabPageSelectedFlocks.AccessibleName = null;
            resources.ApplyResources(this.tabPageSelectedFlocks, "tabPageSelectedFlocks");
            this.tabPageSelectedFlocks.BackgroundImage = null;
            this.tabPageSelectedFlocks.Font = null;
            this.tabPageSelectedFlocks.Name = "tabPageSelectedFlocks";
            this.tabPageSelectedFlocks.UseVisualStyleBackColor = true;
            // 
            // tabPageCurves
            // 
            this.tabPageCurves.AccessibleDescription = null;
            this.tabPageCurves.AccessibleName = null;
            resources.ApplyResources(this.tabPageCurves, "tabPageCurves");
            this.tabPageCurves.BackgroundImage = null;
            this.tabPageCurves.Controls.Add(this.userControlEditCurves);
            this.tabPageCurves.Font = null;
            this.tabPageCurves.Name = "tabPageCurves";
            this.tabPageCurves.UseVisualStyleBackColor = true;
            // 
            // userControlEditCurves
            // 
            this.userControlEditCurves.AccessibleDescription = null;
            this.userControlEditCurves.AccessibleName = null;
            resources.ApplyResources(this.userControlEditCurves, "userControlEditCurves");
            this.userControlEditCurves.BackgroundImage = null;
            this.userControlEditCurves.Font = null;
            this.userControlEditCurves.Name = "userControlEditCurves";
            // 
            // UserControlFlocks
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tabControl);
            this.Font = null;
            this.Name = "UserControlFlocks";
            this.tabControl.ResumeLayout(false);
            this.tabPageFlocks.ResumeLayout(false);
            this.tabPageCurves.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageFlocks;
        private System.Windows.Forms.TabPage tabPageCurrentFlocks;
        private System.Windows.Forms.TabPage tabPageSelectedFlocks;
        private UserControlEditFlocks userControlEditFlocks;
        private System.Windows.Forms.TabPage tabPageCurves;
        private UserControlEditCurves userControlEditCurves;
    }
}
