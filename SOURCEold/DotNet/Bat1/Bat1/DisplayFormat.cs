﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Scale;

namespace Bat1 {
    /// <summary>
    /// Formatting of numbers for display, so the display is the same as in the scale
    /// </summary>
    public static class DisplayFormat {
        /// <summary>
        /// Return weight with cut additional digits in format to display. Used only for displaying statistic values, so it match display in the scale.
        /// Never use for single sample weight.
        /// </summary>
        /// <param name="weight">Weight</param>
        /// <param name="units">Units</param>
        /// <returns>String to display</returns>
        public static string CutWeight(float weight, Units units) {
            int i;
            if (units == Units.G){
                // U gramu oseknu desetinnou cast
                i = (int)weight;
                return i.ToString("N0");        // N0 zajisti oddeleni tisicu mezerou nebo jinym oddelovacem
            } else {
                // Kg i Lb beru nastejno, oseknu cast za gramy
                i = (int)((double)weight * 1000.0);    // Presnost musim na double
                double d = (double)i / 1000.0;
                return d.ToString("N3");
            }
        }

        /// <summary>
        /// Return rounded weight in format to display.
        /// </summary>
        /// <param name="weight">Weight</param>
        /// <param name="units">Units</param>
        /// <returns>String to display</returns>
        public static string RoundWeight(double weight, Units units) {
            if (units == Units.G){
                // Zaorkouhlim na cele gramy
                int i = (int)Math.Round(weight, 0);
                return i.ToString("N0");        // N0 zajisti oddeleni tisicu mezerou nebo jinym oddelovacem
            } else {
                // Kg i Lb beru nastejno, zaokrouhlim na 3 desetinna mista
                return Math.Round(weight, 3).ToString("N3");
            }
        }

        /// <summary>
        /// Return CV in format for display
        /// </summary>
        /// <param name="cv">CV</param>
        /// <returns>String to display</returns>
        public static string Cv(float cv) {
            // CV zobrazuju na desetiny a zbytek useknu
            // CV musim jeste pred vypoctem zaokrouhlit, jinak pocita spatne. Musim zaorkouhlit na vice des. mist nez 1.
            double d = Math.Round(cv, 2) * 10.0;    
            int i = (int)d;
            d = (double)i / 10.0;
            return d.ToString("N1");
        }

        /// <summary>
        /// Return uniformity in format for display
        /// </summary>
        /// <param name="cv">Uniformity</param>
        /// <returns>String to display</returns>
        public static string Uniformity(float uniformity) {
            return Cv(uniformity);      // Format je stejny jako u CV
        }
    }
}
