﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Csv;
using System.Globalization;             // Decimal separator
using Veit.Scale;

namespace Bat1 {
    /// <summary>
    /// Export data into CSV file
    /// </summary>
    public class ExportCsv {
        /// <summary>
        /// File name to save to
        /// </summary>
        private string fileName;

        /// <summary>
        /// CSV instance
        /// </summary>
        private Csv csv;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">File name to save to</param>
        public ExportCsv(string fileName) {
            // Preberu nazev souboru
            this.fileName = fileName;

            // Vytvorim instanci CSV s parametry
            string delimiter;
            switch (Program.Setup.CsvConfig.Delimiter) {
                case CsvDelimiter.COMMA:
                    delimiter = ",";
                    break;
                case CsvDelimiter.SEMICOLON:
                    delimiter = ";";
                    break;
                default: // CsvDelimiter.TAB:
                    delimiter = "\t";
                    break;
            }

            string decimalSeparator;
            switch (Program.Setup.CsvConfig.DecimalSeparator) {
                case CsvDecimalSeparator.COMMA:
                    decimalSeparator = ",";
                    break;
                case CsvDecimalSeparator.DOT:
                    decimalSeparator = ".";
                    break;
                default: // CsvDecimalSeparator.DEFAULT:
                    decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                    break;
            }

            Encoding encoding;
            switch (Program.Setup.CsvConfig.Encoding) {
                case CsvEncoding.DEFAULT:
                    encoding = Encoding.Default;
                    break;
                case CsvEncoding.UTF16:
                    encoding = Encoding.Unicode;
                    break;
                default: // CsvEncoding.UTF8:
                    encoding = Encoding.UTF8;
                    break;
            }

            csv = new Csv(delimiter, decimalSeparator, encoding);
        }

        /// <summary>
        /// Adds flag to the file name
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="flag">Used flag</param>
        /// <returns></returns>
        private string AddFlag(string fileName, Flag flag) {
            if (flag != Flag.ALL) {
                fileName += " (" + Sample.FlagToString(flag) + ")";     // Pridam flag
            }
            return fileName;
        }

        /// <summary>
        /// Add weighing to the CSV
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        private void AddWeighing(Weighing weighing, Flag flag) {
            // Nactu vysledek
            StatisticResult result = weighing.WeighingResults.GetResult(flag);
            if (result == null) {
                return;     // Tento flag ve vysledku neni
            }

            Units units = weighing.WeighingData.ScaleConfig.Units.Units;
            string unitsString = ConvertWeight.UnitsToString(units);
            
            // Hlavicka
            csv.ClearLine();
            csv.AddString( 0, Properties.Resources.FILE + ":");
            csv.AddString( 1, AddFlag(weighing.WeighingData.File.Name, flag));       
            csv.AddString( 3, Properties.Resources.COUNT + ":");
            csv.AddInteger(4, result.Count);
            csv.AddString( 6, Properties.Resources.CV + " [%]:");
            csv.AddDouble( 7, double.Parse(DisplayFormat.Cv(result.Cv)));
            csv.SaveLine();

            csv.ClearLine();
            csv.AddString( 0, Properties.Resources.SCALE + ":");
            csv.AddString( 1, weighing.WeighingData.ScaleConfig.ScaleName);
            csv.AddString( 3, Properties.Resources.AVERAGE + " [" + unitsString + "]:");
            csv.AddDouble( 4, double.Parse(DisplayFormat.RoundWeight(result.Average, units)));
            csv.AddString( 6, Properties.Resources.UNIFORMITY + " [%]:");
            csv.AddDouble( 7, double.Parse(DisplayFormat.Uniformity(result.Uniformity)));
            csv.SaveLine();

            csv.ClearLine();
            csv.AddString( 0, Properties.Resources.NOTE + ":");
            csv.AddString( 1, weighing.WeighingData.Note);
            csv.AddString( 3, Properties.Resources.ST_DEVIATION + " [" + unitsString + "]:");
            csv.AddDouble( 4, double.Parse(DisplayFormat.RoundWeight(result.Sigma, units)));
            csv.AddString( 6, Properties.Resources.SPEED + " [1/" + Properties.Resources.HOUR + "]:");
            csv.AddInteger(7, (int)Math.Round(result.WeighingSpeed));
            csv.SaveLine();

            csv.ClearLine();
            csv.SaveLine();

            // Hlavicka tabulky vzorku
            csv.ClearLine();
            csv.AddString(0, Properties.Resources.FILE);
            csv.AddString(1, Properties.Resources.NUMBER);
            csv.AddString(2, Properties.Resources.DATE_AND_TIME);
            csv.AddString(3, Properties.Resources.WEIGHT + " [" + unitsString + "]");
            csv.AddString(4, Properties.Resources.SEX_LIMIT);
            csv.SaveLine();

            // Vzorky pouze u vysledku stazenych z vahy (u rucne zadanych vzorky nejsou)
            if (weighing.WeighingData.ResultType != ResultType.MANUAL) {
                int sampleIndex = 1;
                weighing.WeighingData.SampleList.First(flag);
                while (weighing.WeighingData.SampleList.Read(flag)) {
                    csv.ClearLine();
                    csv.AddString(  0, weighing.WeighingData.File.Name);
                    csv.AddInteger( 1, sampleIndex);
                    csv.AddDateTime(2, weighing.WeighingData.SampleList.Sample.dateTime);
                    csv.AddDouble(  3, double.Parse(DisplayFormat.RoundWeight(weighing.WeighingData.SampleList.Sample.weight, units)));
                    csv.AddString(  4, Sample.FlagToString(weighing.WeighingData.SampleList.Sample.flag));
                    csv.SaveLine();
                    sampleIndex++;
                }
            }
        }

        /// <summary>
        /// Add header of statistics table to the CSV (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="units">Units used</param>
        private void AddStatisticsHeader(Units units) {
            string unitsString = ConvertWeight.UnitsToString(units);
            
            csv.ClearLine();
            csv.AddString(0, Properties.Resources.DATE_AND_TIME);
            csv.AddString(1, Properties.Resources.FILE);
            csv.AddString(2, Properties.Resources.COUNT);
            csv.AddString(3, Properties.Resources.AVERAGE + " [" + unitsString + "]");
            csv.AddString(4, Properties.Resources.ST_DEVIATION + " [" + unitsString + "]");
            csv.AddString(5, Properties.Resources.CV + " [%]");
            csv.AddString(6, Properties.Resources.UNIFORMITY + " [%]");
            csv.AddString(7, Properties.Resources.SPEED + " [1/" + Properties.Resources.HOUR + "]");
            csv.SaveLine();
        }

        /// <summary>
        /// Fills columns of statistics
        /// </summary>
        /// <param name="result">Statistic result</param>
        private void AddStatisticsColumns(StatisticResult result, Units units, ResultType resultType) {
            csv.AddInteger(2, result.Count);
            csv.AddDouble( 3, double.Parse(DisplayFormat.RoundWeight(result.Average, units)));
            csv.AddDouble( 4, double.Parse(DisplayFormat.RoundWeight(result.Sigma, units)));
            csv.AddDouble( 5, double.Parse(DisplayFormat.Cv(result.Cv)));
            csv.AddDouble( 6, double.Parse(DisplayFormat.Uniformity(result.Uniformity)));
            csv.AddInteger(7, (int)Math.Round(result.WeighingSpeed));
        }

        /// <summary>
        /// Add weighing statistics to the CSV (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        private void AddStatistics(Weighing weighing, Flag flag) {
            // Nactu vysledek
            StatisticResult result = weighing.WeighingResults.GetResult(flag);

            Units units = weighing.WeighingData.ScaleConfig.Units.Units;
            string unitsString = ConvertWeight.UnitsToString(units);
            
            csv.ClearLine();
            if (result == null) {
                // Tento flag ve vysledku neni, ulozim jen nazev souboru
                csv.AddString(  1, AddFlag(weighing.WeighingData.File.Name, flag));       
            } else {
                csv.AddDateTime(0, weighing.GetMinDateTime());
                csv.AddString(  1, AddFlag(weighing.WeighingData.File.Name, flag));
                AddStatisticsColumns(result, units, weighing.WeighingData.ResultType);
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Add statistics total to the CSV (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        private void AddStatisticsTotal(WeighingList weighingList, Flag flag) {
            // Nactu vysledek
            StatisticResult result = weighingList.TotalstatisticResults.GetResult(flag);
            Units units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;
            
            csv.ClearLine();
            csv.AddString(0, Properties.Resources.TOTAL + " (" + weighingList.List.Count.ToString() + ")");
            if (result != null) {
                // Tento flag je ve vysledku (pokud ne, ulozim pouze nazev souboru)
                AddStatisticsColumns(result, units, ResultType.MANUAL);     // Celkove vysledky vzdy zaokrouhluju
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Add header of flock statistics table to the CSV (layout corresponding to icon Flocks)
        /// </summary>
        /// <param name="units">Units used</param>
        private void AddFlocksHeader(Units units) {
            string unitsString = ConvertWeight.UnitsToString(units);
            
            csv.ClearLine();
            csv.AddString(0, Properties.Resources.DATE_AND_TIME);
            csv.AddString(1, Properties.Resources.DAY);
            csv.AddString(2, Properties.Resources.FLOCK);
            csv.AddString(3, Properties.Resources.COUNT);
            csv.AddString(4, Properties.Resources.AVERAGE      + " [" + unitsString + "]");
            csv.AddString(5, Properties.Resources.TARGET       + " [" + unitsString + "]");
            csv.AddString(6, Properties.Resources.DIFFERENCE   + " [" + unitsString + "]");
            csv.AddString(7, Properties.Resources.ABOVE);
            csv.AddString(8, Properties.Resources.BELOW);
            csv.AddString(9, Properties.Resources.GAIN         + " [" + unitsString + "]");
            csv.AddString(10,Properties.Resources.ST_DEVIATION + " [" + unitsString + "]");
            csv.AddString(11,Properties.Resources.CV + " [%]");
            csv.AddString(12,Properties.Resources.UNIFORMITY + " [%]");
            csv.AddString(13,Properties.Resources.SPEED + " [1/" + Properties.Resources.HOUR + "]");
            csv.SaveLine();
        }

        /// <summary>
        /// Fills columns of flock
        /// </summary>
        /// <param name="result">Statistic result</param>
        private void AddFlockColumns(StatisticResult result, Units units, ResultType resultType) {
            csv.AddInteger( 3, result.Count);
            csv.AddDouble(  4, double.Parse(DisplayFormat.RoundWeight(result.Average, units)));
            csv.AddDouble( 10, double.Parse(DisplayFormat.RoundWeight(result.Sigma, units)));
            csv.AddDouble( 11, double.Parse(DisplayFormat.Cv(result.Cv)));
            csv.AddDouble( 12, double.Parse(DisplayFormat.Uniformity(result.Uniformity)));
            csv.AddInteger(13, (int)Math.Round(result.WeighingSpeed));
        }

        /// <summary>
        /// Add flock statistics to the CSV (layout corresponding to icon Flocks)
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        private void AddFlocks(Weighing weighing, Flag flag) {
            // Nactu vysledek
            StatisticResult result = weighing.WeighingResults.GetResult(flag);

            Units units = weighing.WeighingData.ScaleConfig.Units.Units;
            string unitsString = ConvertWeight.UnitsToString(units);
            
            csv.ClearLine();
            if (result == null) {
                // Tento flag ve vysledku neni, ulozim jen nazev hejna
                csv.AddString(  2, AddFlag(weighing.WeighingData.File.Name, flag));       
            } else {
                csv.AddDateTime(0, weighing.GetMinDateTime());
                csv.AddInteger( 1, weighing.FlockData.Day);
                csv.AddString(  2, AddFlag(weighing.WeighingData.File.Name, flag));       
                AddFlockColumns(result, units, weighing.WeighingData.ResultType);
                csv.AddDouble(  5, double.Parse(DisplayFormat.RoundWeight(weighing.FlockData.Target, units)));
                csv.AddDouble(  6, double.Parse(DisplayFormat.RoundWeight(weighing.FlockData.Difference, units)));
                csv.AddInteger( 7, weighing.FlockData.CountAbove);
                csv.AddInteger( 8, weighing.FlockData.CountBelow);
                csv.AddDouble(  9, double.Parse(DisplayFormat.RoundWeight(weighing.FlockData.Gain, units)));
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Add flocks total to the CSV (layout corresponding to icon Flocks)
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        private void AddFlocksTotal(WeighingList weighingList, Flag flag) {
            // Nactu vysledek
            StatisticResult result = weighingList.TotalstatisticResults.GetResult(flag);
            Units units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;
            
            csv.ClearLine();
            csv.AddString(0, Properties.Resources.TOTAL + " (" + weighingList.List.Count.ToString() + ")");
            if (result != null) {
                // Tento flag je ve vysledku (pokud ne, ulozim pouze nazev souboru)
                AddFlockColumns(result, units, ResultType.MANUAL);     // Celkove vysledky vzdy zaokrouhluju
            }
            csv.SaveLine();
        }

        /// <summary>
        /// Save current CSV content to the file
        /// </summary>
        private void Save() {
            try {
                // Pokud soubor existuje, prepisu
                if (System.IO.File.Exists(fileName)) {
                    System.IO.File.Delete(fileName);
                }
                
                // Ulozim do souboru
                csv.SaveToFile(fileName);
            } catch {
                // Soubor jiz ma otevreny v Excelu
            }
        }

        /// <summary>
        /// Export one weighing
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Flag</param>
        public void ExportWeighing(Weighing weighing, Flag flag) {
            // Nactu vysledek
            StatisticResult result = weighing.WeighingResults.GetResult(flag);
            if (result == null) {
                return;     // Tento flag ve vysledku neni
            }
            
            // Smazu cele CSV
            csv.Clear();

            // Pridam vazeni
            AddWeighing(weighing, flag);

            // Ulozim do souboru
            csv.SaveToFile(fileName);
        }

        /// <summary>
        /// Export a list of weighings
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        public void ExportWeighingList(List<Weighing> weighingList, Flag flag) {
            // Smazu cele CSV
            csv.Clear();

            foreach (Weighing weighing in weighingList) {
                // Nactu vysledek
                StatisticResult result = weighing.WeighingResults.GetResult(flag);
                if (result == null) {
                    continue;   // Tento flag ve vysledku neni
                }

                // Pridam vazeni
                AddWeighing(weighing, flag);

                // Prazdny radek
                csv.ClearLine();
                csv.SaveLine();
                csv.SaveLine();
                csv.SaveLine();
            }

            // Ulozim do souboru
            Save();
        }

        /// <summary>
        /// Export table with statistics (layout corresponding to icon Statistics)
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Flag</param>
        /// <param name="isFlocks">True if exporting flocks table</param>
        public void ExportStatisticsTable(WeighingList weighingList, Flag flag, bool isFlocks) {
            if (weighingList.List.Count == 0) {
                return;     // Seznam je prazdny
            }
            
            // Smazu cele CSV
            csv.Clear();

            Units units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;

            if (isFlocks) {
                AddFlocksHeader(units);
            } else {
                AddStatisticsHeader(units);
            }
            
            // Pridam vsechna vazeni
            foreach (Weighing weighing in weighingList.List) {
                if (isFlocks) {
                    AddFlocks(weighing, flag);
                } else {
                    AddStatistics(weighing, flag);
                }
            }

            // Pridam radek TOTAL, pouze pokud je v tabulce vic jak 1 radek
            if (weighingList.List.Count > 1) {
                if (isFlocks) {
                    AddFlocksTotal(weighingList, flag);
                } else {
                    AddStatisticsTotal(weighingList, flag);
                }
            }

            // Ulozim do souboru
            Save();
        }

    }
}
