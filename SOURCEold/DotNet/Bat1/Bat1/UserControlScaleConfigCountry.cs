﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;

namespace Bat1 {
    struct CountryData {
        public string             Title;
        public Country            Country;
        public Language           Language;
        public DateFormat         DateFormat;
        public char               DateSeparator1;
        public char               DateSeparator2;
        public TimeFormat         TimeFormat;
        public char               TimeSeparator;
        public DaylightSavingMode DaylightSavingMode;

        public CountryData(string title, Country country, Language language, DateFormat dateFormat, char dateSeparator1,
                           char dateSeparator2, TimeFormat timeFormat, char timeSeparator, DaylightSavingMode daylightSavingMode) {
            Title              = title;
            Country            = country;
            Language           = language;
            DateFormat         = dateFormat;
            DateSeparator1     = dateSeparator1;
            DateSeparator2     = dateSeparator2;
            TimeFormat         = timeFormat;
            TimeSeparator      = timeSeparator;
            DaylightSavingMode = daylightSavingMode;
        }
    };

    public partial class UserControlScaleConfigCountry : UserControlScaleConfigBase {

        /// <summary>
        /// Array of country data, see http://en.wikipedia.org/wiki/Date_and_time_notation_by_country, http://answers.yahoo.com/question/index?qid=20060806094106AAcW27n,
        /// ftp://ftp.software.ibm.com/software/globalization/locales/
        /// </summary>
        private CountryData[] CountryDataArray;

        /// <summary>
        /// Date separators the user can choose from
        /// </summary>
        private char[] DATE_SEPARATOR = {'.', '-', '/', ',', ' '};
        
        /// <summary>
        /// Time separators the user can choose from
        /// </summary>
        private char[] TIME_SEPARATOR = {':', '.'};

        public UserControlScaleConfigCountry(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Seznam zemi je primarne ulozen v DLL. Vytvorim si lokalni seznam, se kterym dal pracuju
            int countryCount = Enum.GetValues(typeof(Veit.Bat1.Country)).Length;
            CountryDataArray = new CountryData[countryCount];
            for (int i = 0; i < countryCount; i++) {
                CountryDataArray[i].Title              =                     Veit.Bat1.Locales.GetCountryName(i);
                try {
                    CountryDataArray[i].Language           = (Language)          Veit.Bat1.Locales.GetLanguage(i);
                    CountryDataArray[i].DateFormat         = (DateFormat)        Veit.Bat1.Locales.GetDateFormat(i);
                    CountryDataArray[i].DateSeparator1     =                     Veit.Bat1.Locales.GetDateSeparator1(i);
                    CountryDataArray[i].DateSeparator2     =                     Veit.Bat1.Locales.GetDateSeparator2(i);
                    CountryDataArray[i].TimeFormat         = (TimeFormat)        Veit.Bat1.Locales.GetTimeFormat(i);
                    CountryDataArray[i].TimeSeparator      =                     Veit.Bat1.Locales.GetTimeSeparator(i);
                    CountryDataArray[i].DaylightSavingMode = (DaylightSavingMode)Veit.Bat1.Locales.GetDaylightSavingType(i);
                } catch {
                    // Ovladac vahy neni nainstalovan
                    continue;
                }
            }

            // Naplnim country
            comboBoxCountry.Items.Clear();
            foreach (CountryData countryData in CountryDataArray) {
                comboBoxCountry.Items.Add(countryData.Title);
            }
            
            // Naplnim separatory
            FillSeparators(comboBoxDateSeparator1, DATE_SEPARATOR);
            FillSeparators(comboBoxDateSeparator2, DATE_SEPARATOR);
            FillSeparators(comboBoxTimeSeparator,  TIME_SEPARATOR);
            
            // Preberu Read-only
            this.readOnly = readOnly;
            
            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        private void FillSeparators(ComboBox comboBox, char[] separatorArray) {
            comboBox.Items.Clear();
            foreach (char ch in separatorArray) {
                comboBox.Items.Add(ch);
            }
        }
        
        private void RedrawSeparator(ComboBox comboBox, char[] separatorArray, char separator) {
            // Pokud je pozadovany separator v seznamu, vykreslim ho. Jinak vykreslim prvni separator jako default.
            for (int index = 0; index < separatorArray.Length; index++) {
                if (separatorArray[index] == separator) {
                    // Nasel jsem separator v seznamu
                    comboBox.SelectedIndex = index;
                    return;
                }
            }
            // Separator v seznamu neni, zvolim prvni
            comboBox.SelectedIndex = 0;
        }

        private string CreateDate(string string1, string separator1, string string2, string separator2, string string3) {
            return string1 + separator1 + string2 + separator2 + string3;
        }
        
        private string CreateDate(int number1, int number2, int number3) {
            return CreateDate(number1.ToString(), comboBoxDateSeparator1.Text, number2.ToString(), comboBoxDateSeparator2.Text, number3.ToString());
        }

        private string CreateDate(string string1, string string2, string string3) {
            return CreateDate(string1, comboBoxDateSeparator1.Text, string2, comboBoxDateSeparator2.Text, string3);
        }

        private void RedrawDateExample() {
            string str = "";
            switch ((DateFormat)comboBoxDateFormat.SelectedIndex) {
                case DateFormat.DDMMYYYY:
                    str = CreateDate(30, 12, 2008);
                    break;
                case DateFormat.MMDDYYYY:
                    str = CreateDate(12, 30, 2008);
                    break;
                case DateFormat.YYYYMMDD:
                    str = CreateDate(2008, 12, 30);
                    break;
                case DateFormat.YYYYDDMM:
                    str = CreateDate(2008, 30, 12);
                    break;
                case DateFormat.DDMMMYYYY:
                    str = CreateDate("30", "Dec", "2008");
                    break;
                case DateFormat.MMMDDYYYY:
                    str = CreateDate("Dec", "30", "2008");
                    break;
                case DateFormat.YYYYMMMDD:
                    str = CreateDate("2008", "Dec", "30");
                    break;
                case DateFormat.YYYYDDMMM:
                    str = CreateDate("2008", "30", "Dec");
                    break;
            }
            labelDateExample.Text = str;
        }
        
        private void RedrawTimeExample() {
            string str = "";
            switch ((TimeFormat)comboBoxTimeFormat.SelectedIndex) {
                case TimeFormat.HOUR12:
                    str = "08" + comboBoxTimeSeparator.Text + "59pm";
                    break;
                case TimeFormat.HOUR24:
                    str = "20" + comboBoxTimeSeparator.Text + "59";
                    break;
            }
            labelTimeExample.Text = str;
        }

        private void RedrawCountry(CountryData countryData) {
            comboBoxLanguage.SelectedIndex = (int)countryData.Language;

            comboBoxDaylightSaving.SelectedIndex = (int)countryData.DaylightSavingMode;

            comboBoxDateFormat.SelectedIndex = (int)countryData.DateFormat;
            RedrawSeparator(comboBoxDateSeparator1, DATE_SEPARATOR, countryData.DateSeparator1);
            RedrawSeparator(comboBoxDateSeparator2, DATE_SEPARATOR, countryData.DateSeparator2);
            RedrawDateExample();

            comboBoxTimeFormat.SelectedIndex = (int)countryData.TimeFormat;
            RedrawSeparator(comboBoxTimeSeparator, TIME_SEPARATOR, countryData.TimeSeparator);
            RedrawTimeExample();
        }
        
        private void RedrawCountry() {
            isLoading = true;

            try {
                RedrawCountry(CountryDataArray[comboBoxCountry.SelectedIndex]);
            } finally {
                isLoading = false;
            }
        }

        public override void Redraw() {
            isLoading = true;

            try {
                comboBoxCountry.SelectedIndex  = (int)scaleConfig.Country;
                
                // Poradi jazyku v comboboxu je podle abecedy, tj. odpovida nejnovejsi verzi vah
                comboBoxLanguage.SelectedIndex = ScaleLanguage.ConvertToScale(scaleConfig.LanguageInDatabase, ScaleVersion.MINOR);

                comboBoxDaylightSaving.SelectedIndex = (int)scaleConfig.DaylightSavingMode;

                comboBoxDateFormat.SelectedIndex = (int)scaleConfig.DateFormat;
                RedrawSeparator(comboBoxDateSeparator1, DATE_SEPARATOR, scaleConfig.DateSeparator1);
                RedrawSeparator(comboBoxDateSeparator2, DATE_SEPARATOR, scaleConfig.DateSeparator2);
                RedrawDateExample();

                comboBoxTimeFormat.SelectedIndex = (int)scaleConfig.TimeFormat;
                RedrawSeparator(comboBoxTimeSeparator, TIME_SEPARATOR, scaleConfig.TimeSeparator);
                RedrawTimeExample();
            } finally {
                isLoading = false;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.Country = (Country)comboBoxCountry.SelectedIndex;
            
            // Vybrany jazyk prepoctu zpet na kod pouzity v DB
            // Poradi jazyku v comboboxu je podle abecedy, tj. odpovida nejnovejsi verzi vah
            scaleConfig.LanguageInDatabase = ScaleLanguage.ConvertToDatabase(comboBoxLanguage.SelectedIndex, ScaleVersion.MINOR);
            
            scaleConfig.DaylightSavingMode = (DaylightSavingMode)comboBoxDaylightSaving.SelectedIndex;

            scaleConfig.DateFormat = (DateFormat)comboBoxDateFormat.SelectedIndex;
            scaleConfig.DateSeparator1 = comboBoxDateSeparator1.Text[0];
            scaleConfig.DateSeparator2 = comboBoxDateSeparator2.Text[0];
            RedrawDateExample();

            scaleConfig.TimeFormat = (TimeFormat)comboBoxTimeFormat.SelectedIndex;
            scaleConfig.TimeSeparator = comboBoxTimeSeparator.Text[0];
            RedrawTimeExample();
        }

        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e) {
            ControlsToConfig();
        }

        private void comboBoxCountry_SelectionChangeCommitted(object sender, EventArgs e) {
            // Country musim obslouzit zvlast
            if (readOnly || isLoading) {
                return;
            }
            RedrawCountry();
            ControlsToConfig();     // Az po vykresleni
        }
    }
}
