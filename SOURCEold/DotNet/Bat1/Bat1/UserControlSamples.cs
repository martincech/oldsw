﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Scale;

namespace Bat1 {
    
    public partial class UserControlSamples : UserControl {
        /// <summary>
        /// Sort mode of the sample list
        /// </summary>
        enum SortMode {
            /// <summary>
            /// Sort by date and time
            /// </summary>
            TIME,

            /// <summary>
            /// Sort by weight
            /// </summary>
            WEIGHT
        }

        /// <summary>
        /// If false, samples can be deleted from the table
        /// </summary>
        public bool ReadOnly { get { return readOnly; } set { readOnly = value; flowLayoutPanelBottom.Visible = !readOnly; } }   // Schovam nebo zobrazim tlacitko pro mazani
        private bool readOnly;

        /// <summary>
        /// If false, samples can be deleted from the table
        /// </summary>
        public bool ShowDivide { get { return showDivide; } set { showDivide = value; buttonDivide.Visible = buttonUnits.Visible = showDivide; } }    // Schovam nebo zobrazim tlacitko pro deleni
        private bool showDivide;

        /// <summary>
        /// Sample list
        /// </summary>
        private SampleList sampleList;

        /// <summary>
        /// Show only samples with this flag
        /// </summary>
        private Flag flag;
        
        /// <summary>
        /// Units used
        /// </summary>
        private Units units;
        public Units Units { get { return units; } }
        
        /// <summary>
        /// Sorting mode defualt by time
        /// </summary>
        private SortMode sortMode = SortMode.TIME;

        /// <summary>
        /// Weights have been divided by a specified number
        /// </summary>
        public bool IsWeightsDivided { get { return isWeightsDivided; } }
        private bool isWeightsDivided = false;

        /// <summary>
        /// Sample deleted event delegate
        /// </summary>
        /// <param name="sender"></param>
        public delegate void DeletedEventHandler(object sender);

        /// <summary>
        /// Sample deleted event
        /// </summary>
        public event DeletedEventHandler DeletedEvent;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public UserControlSamples() {
            InitializeComponent();

            // Default je readonly
            ReadOnly   = true;    // Zaroven schova nebo zobrazi
            ShowDivide = true;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sampleList">Sample list to show/edit</param>
        /// <param name="flag">Show only samples with this flag</param>
        /// <param name="units">Units used</param>
        /// <param name="readOnly">If false, samples can be deleted from the list</param>
        public UserControlSamples(SampleList sampleList, Flag flag, Units units, bool readOnly, bool showDivide) : this() {
            // Preberu data
            ReadOnly        = readOnly;     // Zaroven schova nebo zobrazi
            ShowDivide      = showDivide;   // Zaroven schova nebo zobrazi
            this.sampleList = sampleList;
            this.flag       = flag;
            this.units      = units;

            // Setridim podle aktualne zvoleneho sloupce a vykreslim vzorky
            Sort();
        }

        /// <summary>
        /// Set samples
        /// </summary>
        /// <param name="sampleList">Sample list to show/edit</param>
        /// <param name="flag">Show only samples with this flag</param>
        /// <param name="units">Units used</param>
        public void SetData(SampleList sampleList, Flag flag, Units units) {
            // Preberu data
            this.sampleList = sampleList;
            this.flag       = flag;
            this.units      = units;

            // Setridim podle aktualne zvoleneho sloupce a vykreslim vzorky
            Sort();
        }

        /// <summary>
        /// Redraw all data
        /// </summary>
        private void Redraw() {
            dataGridViewSamples.SuspendLayout();
            try {
                dataGridViewSamples.Rows.Clear();
                if (sampleList == null) {
                    return;
                }
                
                sampleList.First(flag);
                string unitsString = " " + ConvertWeight.UnitsToString(units);
                while (sampleList.Read(flag)) {
                    dataGridViewSamples.Rows.Add(sampleList.Sample.dateTime,
                                                 DisplayFormat.RoundWeight(sampleList.Sample.weight, units) + unitsString,
                                                 Sample.FlagToString(sampleList.Sample.flag));
                }
            } finally {
                dataGridViewSamples.ResumeLayout();
            }
        }

        /// <summary>
        /// Delete current row
        /// </summary>
        private void Delete() {
            if (readOnly) {
                return;     // Mazat vzorky neni povoleno
            }

            if (dataGridViewSamples.RowCount <= 1) {
                return;     // V tabulce musi zbyt aspon 1 vzorek
            }

            if (dataGridViewSamples.SelectedRows.Count == 0) {
                return;     // Neni vybrany zadny radek
            }
            
            DataGridViewRow selectedRow = dataGridViewSamples.SelectedRows[0];

            // Zeptam se, zda chce smazat aktualni vzorek
            string sampleString = selectedRow.Cells["ColumnDateTime"].Value + "; " + selectedRow.Cells["ColumnWeight"].Value;
            if (MessageBox.Show(String.Format(Properties.Resources.DELETE_WEIGHT, sampleString), Program.ApplicationName, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            // Vymazu vzorek na aktualni pozici a prekreslim
            sampleList.RemoveAt(selectedRow.Index, flag);
            dataGridViewSamples.Rows.RemoveAt(selectedRow.Index);

            // Nahodim event
            DeletedEvent(this);
        }

        /// <summary>
        /// Set new weighing start date and time
        /// </summary>
        /// <param name="startDateTime">New start DateTime</param>
        public void SetWeighingStart(DateTime startDateTime) {
            sampleList.SetWeighingStart(startDateTime);
            Redraw();
        }

        /// <summary>
        /// Clear sorting glyphs in all columns
        /// </summary>
        private void ClearSortGlyphs() {
            foreach (DataGridViewColumn column in dataGridViewSamples.Columns) {
                column.HeaderCell.SortGlyphDirection = SortOrder.None;
            }
        }

        /// <summary>
        /// Sort sample list by time and redraw
        /// </summary>
        private void SortByTime() {
            // Setridim
            if (sampleList != null) {
                sampleList.SortByTime();
            }

            // Prekreslim
            ClearSortGlyphs();
            dataGridViewSamples.Columns[0].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        }

        /// <summary>
        /// Sort sample list by weight and redraw
        /// </summary>
        private void SortByWeight() {
            // Setridim
            if (sampleList != null) {
                sampleList.SortByWeight();
            }

            // Prekreslim
            ClearSortGlyphs();
            dataGridViewSamples.Columns[1].HeaderCell.SortGlyphDirection = SortOrder.Ascending;
        }

        private void Sort() {
            switch (sortMode) {
                case SortMode.TIME:
                    // Setridim podle data
                    SortByTime();
                    break;

                case SortMode.WEIGHT:
                    // Setridim podle hmotnosti
                    SortByWeight();
                    break;
            }
        
            // Prekreslim
            Redraw();
        }

        private void dataGridViewSamples_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Delete) {
                return;     // Klavesa Del maze
            }

            // Smazu
            Delete();

            // Klavesu Del jsem zpracoval
            e.Handled = true;
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            Delete();
            dataGridViewSamples.Focus();
        }

        private void dataGridViewSamples_CellClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex >= 0) {
                return;     // Kliknul na bunku, ne na hlavicku
            }

            switch (e.ColumnIndex) {
                case 0:
                    // Setridim podle data
                    if (sortMode == SortMode.TIME) {
                        return;     // Tabulka uz je spravne setridena
                    }
                    sortMode = SortMode.TIME;
                    break;

                case 1:
                    // Setridim podle hmotnosti
                    if (sortMode == SortMode.WEIGHT) {
                        return;     // Tabulka uz je spravne setridena
                    }
                    sortMode = SortMode.WEIGHT;
                    break;

                default:
                    // Podle jineho sloupce netridim
                    return;
            }
            Cursor.Current = Cursors.WaitCursor;
            try {
                Sort();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonDivide_Click(object sender, EventArgs e) {
            if (readOnly) {
                return;     // Modifikovat vzorky neni povoleno
            }

            if (!showDivide) {
                return;     // Nemuze delit
            }

            // Zeptam se na jmenovatel
            FormName form = new FormName(buttonDivide.Text, Properties.Resources.DIVIDE_ALL_WEIGHTS, "", false);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Prevedu zadane cislo
            float divideBy;
            if (!float.TryParse(form.EnteredName, out divideBy)) {
                CheckValue.InvalidValueMessage(null);
                return;
            }

            // Zkontroluju rozsah jmenovatele, maximalne 50ks (prevod mozny tam i zpet, tj. delit i nasobit)
            if (divideBy < 1.0F/50.0F || divideBy > 50.0F) {
                CheckValue.InvalidValueMessage(null);
                return;
            }

            // Vydelim vsechny hmotnosti a prekreslim
            sampleList.DivideWeights(divideBy, units);
            Redraw();

            // Zapamatuju si, ze jsem delil
            isWeightsDivided = true;
        }

        private void buttonUnits_Click(object sender, EventArgs e) {
            if (readOnly) {
                return;     // Modifikovat vzorky neni povoleno
            }

/*            if (!showDivide) {
                return;     // Nemuze delit
            }*/

            // Zeptam se na nove jednotky
            FormUnits form = new FormUnits(units);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            if (form.SelectedUnits == units) {
                return;     // Zadna zmena
            }
            
            // Prepoctu vsechna vazeni
            sampleList.Convert(units, form.SelectedUnits);

            // Ulozim si nove jednotky (pokud uzivatel ulozi zmeny, musim prepocist jednotky u vsech vazeni s timto ScaleConfig)
            units = form.SelectedUnits;

            // Prekreslim s novymi jednotkami
            Redraw();

            // Zapamatuju si, ze jsem delil
//            isWeightsDivided = true;
        }

    }
}
