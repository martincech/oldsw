﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;

namespace Bat1 {
    public partial class FormName : Form {

        /// <summary>
        /// Entered name
        /// </summary>
        public string EnteredName { get { return textBoxName.Text; } }

        private bool filterScaleName;

        public FormName(string windowTitle, string name, bool filterScaleName) {
            InitializeComponent();

            Text = windowTitle;
            this.filterScaleName  = filterScaleName;
            textBoxName.Text      = name;
            textBoxName.MaxLength = filterScaleName ? Const.TEXT_LENGTH_MAX : 0;      // Pri editaci textu vahy omezim na 15 znaku
        }

        public FormName(string windowTitle, string labelText, string name, bool filterScaleName) 
               : this(windowTitle, name, filterScaleName){
            labelName.Text = labelText;
        }

        private void FormNameNote_Shown(object sender, EventArgs e) {
            textBoxName.Focus();
            textBoxName.SelectAll();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Zkontroluju jmeno - pokud zadava obecny text, kontroluju jen zda neni text prazdny
            if (filterScaleName && !CheckValue.CheckScaleName(textBoxName.Text) || textBoxName.Text == "") {
                MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
                textBoxName.Focus();
                textBoxName.SelectAll();
                return;
            }

            // Vse v poradku
            DialogResult = DialogResult.OK;
        }

        private void textBoxName_KeyPress(object sender, KeyPressEventArgs e) {
            if (!filterScaleName) {
                return;         // Beru vsechen text
            }

            // Filtruju jen text pouzitelny ve vaze
            e.KeyChar = Char.ToUpper(e.KeyChar);
            if (!KeyFilter.IsScaleChar(e.KeyChar)) {
                e.Handled = true;
            }
        }

    }
}
