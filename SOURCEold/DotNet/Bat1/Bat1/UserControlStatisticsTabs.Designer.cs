﻿namespace Bat1 {
    partial class UserControlStatisticsTabs {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlStatisticsTabs));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageLastWeighings = new System.Windows.Forms.TabPage();
            this.userControlStatisticsLast = new Bat1.UserControlStatisticsTable();
            this.userControlStatisticsTopLast = new Bat1.UserControlStatisticsTop();
            this.tabPageSelectedWeighings = new System.Windows.Forms.TabPage();
            this.userControlStatisticsSelected = new Bat1.UserControlStatisticsTable();
            this.userControlStatisticsTopSelected = new Bat1.UserControlStatisticsTop();
            this.tabControl.SuspendLayout();
            this.tabPageLastWeighings.SuspendLayout();
            this.tabPageSelectedWeighings.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.AccessibleDescription = null;
            this.tabControl.AccessibleName = null;
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.BackgroundImage = null;
            this.tabControl.Controls.Add(this.tabPageLastWeighings);
            this.tabControl.Controls.Add(this.tabPageSelectedWeighings);
            this.tabControl.Font = null;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tabPageLastWeighings
            // 
            this.tabPageLastWeighings.AccessibleDescription = null;
            this.tabPageLastWeighings.AccessibleName = null;
            resources.ApplyResources(this.tabPageLastWeighings, "tabPageLastWeighings");
            this.tabPageLastWeighings.BackgroundImage = null;
            this.tabPageLastWeighings.Controls.Add(this.userControlStatisticsLast);
            this.tabPageLastWeighings.Controls.Add(this.userControlStatisticsTopLast);
            this.tabPageLastWeighings.Font = null;
            this.tabPageLastWeighings.Name = "tabPageLastWeighings";
            this.tabPageLastWeighings.UseVisualStyleBackColor = true;
            // 
            // userControlStatisticsLast
            // 
            this.userControlStatisticsLast.AccessibleDescription = null;
            this.userControlStatisticsLast.AccessibleName = null;
            resources.ApplyResources(this.userControlStatisticsLast, "userControlStatisticsLast");
            this.userControlStatisticsLast.BackgroundImage = null;
            this.userControlStatisticsLast.Font = null;
            this.userControlStatisticsLast.Name = "userControlStatisticsLast";
            // 
            // userControlStatisticsTopLast
            // 
            this.userControlStatisticsTopLast.AccessibleDescription = null;
            this.userControlStatisticsTopLast.AccessibleName = null;
            resources.ApplyResources(this.userControlStatisticsTopLast, "userControlStatisticsTopLast");
            this.userControlStatisticsTopLast.BackgroundImage = null;
            this.userControlStatisticsTopLast.Font = null;
            this.userControlStatisticsTopLast.Name = "userControlStatisticsTopLast";
            // 
            // tabPageSelectedWeighings
            // 
            this.tabPageSelectedWeighings.AccessibleDescription = null;
            this.tabPageSelectedWeighings.AccessibleName = null;
            resources.ApplyResources(this.tabPageSelectedWeighings, "tabPageSelectedWeighings");
            this.tabPageSelectedWeighings.BackgroundImage = null;
            this.tabPageSelectedWeighings.Controls.Add(this.userControlStatisticsSelected);
            this.tabPageSelectedWeighings.Controls.Add(this.userControlStatisticsTopSelected);
            this.tabPageSelectedWeighings.Font = null;
            this.tabPageSelectedWeighings.Name = "tabPageSelectedWeighings";
            this.tabPageSelectedWeighings.UseVisualStyleBackColor = true;
            // 
            // userControlStatisticsSelected
            // 
            this.userControlStatisticsSelected.AccessibleDescription = null;
            this.userControlStatisticsSelected.AccessibleName = null;
            resources.ApplyResources(this.userControlStatisticsSelected, "userControlStatisticsSelected");
            this.userControlStatisticsSelected.BackgroundImage = null;
            this.userControlStatisticsSelected.Font = null;
            this.userControlStatisticsSelected.Name = "userControlStatisticsSelected";
            // 
            // userControlStatisticsTopSelected
            // 
            this.userControlStatisticsTopSelected.AccessibleDescription = null;
            this.userControlStatisticsTopSelected.AccessibleName = null;
            resources.ApplyResources(this.userControlStatisticsTopSelected, "userControlStatisticsTopSelected");
            this.userControlStatisticsTopSelected.BackgroundImage = null;
            this.userControlStatisticsTopSelected.Font = null;
            this.userControlStatisticsTopSelected.Name = "userControlStatisticsTopSelected";
            // 
            // UserControlStatisticsTabs
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tabControl);
            this.Font = null;
            this.Name = "UserControlStatisticsTabs";
            this.tabControl.ResumeLayout(false);
            this.tabPageLastWeighings.ResumeLayout(false);
            this.tabPageSelectedWeighings.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageLastWeighings;
        private System.Windows.Forms.TabPage tabPageSelectedWeighings;
        private UserControlStatisticsTable userControlStatisticsLast;
        private UserControlStatisticsTop userControlStatisticsTopLast;
        private UserControlStatisticsTable userControlStatisticsSelected;
        private UserControlStatisticsTop userControlStatisticsTopSelected;
    }
}
