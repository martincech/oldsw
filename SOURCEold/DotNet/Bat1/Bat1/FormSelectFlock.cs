﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormSelectFlock : Form {

        /// <summary>
        /// Available flocks
        /// </summary>
        private FlockDefinitionList availableFlockList;

        /// <summary>
        /// Flocks selected for statistics
        /// </summary>
        public FlockDefinitionList  SelectedFlockList { get { return selectedFlockList; } }
        private FlockDefinitionList selectedFlockList;

        public FormSelectFlock() {
            InitializeComponent();

            // Nastavim multiselect
            userControlAvailableFlockList.MultiSelect = true;
            userControlSelectedFlockList.MultiSelect  = true;

            // Zaregistruju eventy
            userControlAvailableFlockList.DoubleClickEvent += AvailableDoubleClickEventHandler;
            userControlSelectedFlockList.DoubleClickEvent  += SelectedDoubleClickEventHandler;
            
            // Nahraju dostupna hejna z databaze
            availableFlockList = Program.Database.LoadFlockList();

            // Vybrana hejna zatim zadna, zalozim prazdny seznam
            selectedFlockList = new FlockDefinitionList();

            // Predam seznamy a prekreslim
            userControlAvailableFlockList.SetData(availableFlockList);
            userControlSelectedFlockList.SetData(selectedFlockList);

            // Zobrazim pocet dostupnych hejn (ten zustava konstantni)
            labelAvailable.Text = string.Format(Properties.Resources.SELECT_FLOCKS_AVAILABLE + " ({0:N0}):", userControlAvailableFlockList.FlockDefinitionList.List.Count);
        }

        /// <summary>
        /// Load list of flocks into the list of selected flocks
        /// </summary>
        /// <param name="flockList">Flock list to load</param>
        public void LoadSelectedFlocks(List<Flock> flockList) {
            selectedFlockList = new FlockDefinitionList();
            foreach (Flock flock in flockList) {
                selectedFlockList.Add(flock.Definition);
            }
            userControlSelectedFlockList.SetData(selectedFlockList);
            RedrawSelectedFlocksCount();
        }

        private void RedrawSelectedFlocksCount() {
            // Pocet hejn v seznamu
            labelStatistics.Text = string.Format(Properties.Resources.SELECT_FLOCKS_STATISTICS + " ({0:N0}):", userControlSelectedFlockList.FlockDefinitionList.List.Count);
        }

        private void DeselectAll(ListBox listBox) {
            for (int i = 0; i < listBox.Items.Count; i++) {
                listBox.SetSelected(i, false);
            }
        }
        
        private void AvailableDoubleClickEventHandler(FlockDefinition flockDefinition) {
            userControlSelectedFlockList.AddFlock(flockDefinition);
            RedrawSelectedFlocksCount();
        }

        private void SelectedDoubleClickEventHandler(FlockDefinition flockDefinition) {
            userControlSelectedFlockList.RemoveSelectedFlocks();
            RedrawSelectedFlocksCount();
        }

        private void FormSelectFlock_Shown(object sender, EventArgs e) {
            // Nastavim focus na seznam dostupnych hejn
            userControlAvailableFlockList.Focus();
        }

        private void buttonAdd_Click(object sender, EventArgs e) {
            // Pridam do statistiky vsechny vybrane polozky
            FlockDefinitionList selectedList = userControlAvailableFlockList.GetSelectedFlocks();
            if (selectedList == null) {
                return;     // Nevybral nic
            }
            userControlSelectedFlockList.AddFlocks(selectedList.List);
            RedrawSelectedFlocksCount();
        }

        private void buttonRemove_Click(object sender, EventArgs e) {
            // Odstranim ze statistiky vsechny vybrane polozky
            userControlSelectedFlockList.RemoveSelectedFlocks();
            RedrawSelectedFlocksCount();
        }

        private void buttonAddAll_Click(object sender, EventArgs e) {
            // Pridam do statistiky vsechny polozky
            userControlSelectedFlockList.AddFlocks(availableFlockList.List);
            RedrawSelectedFlocksCount();
        }

        private void buttonRemoveAll_Click(object sender, EventArgs e) {
            userControlSelectedFlockList.RemoveAllFlocks();
            RedrawSelectedFlocksCount();
        }

    }
}
