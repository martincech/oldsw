﻿namespace Bat1 {
    partial class FormScalePassword {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormScalePassword));
            this.buttonEsc = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonEnter = new System.Windows.Forms.Button();
            this.buttonLeft = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonRight = new System.Windows.Forms.Button();
            this.pictureBoxKey4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxKey3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxKey2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxKey1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNumber4 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNumber3 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNumber2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxNumber1 = new System.Windows.Forms.PictureBox();
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxKeyboard = new System.Windows.Forms.GroupBox();
            this.groupBoxPassword = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber1)).BeginInit();
            this.groupBoxKeyboard.SuspendLayout();
            this.groupBoxPassword.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonEsc
            // 
            this.buttonEsc.AccessibleDescription = null;
            this.buttonEsc.AccessibleName = null;
            resources.ApplyResources(this.buttonEsc, "buttonEsc");
            this.buttonEsc.BackgroundImage = global::Bat1.Properties.Resources.KeyEsc;
            this.buttonEsc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonEsc.Font = null;
            this.buttonEsc.Name = "buttonEsc";
            this.buttonEsc.UseVisualStyleBackColor = true;
            this.buttonEsc.Click += new System.EventHandler(this.buttonEsc_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.AccessibleDescription = null;
            this.buttonUp.AccessibleName = null;
            resources.ApplyResources(this.buttonUp, "buttonUp");
            this.buttonUp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonUp.Font = null;
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.buttonEsc_Click);
            // 
            // buttonEnter
            // 
            this.buttonEnter.AccessibleDescription = null;
            this.buttonEnter.AccessibleName = null;
            resources.ApplyResources(this.buttonEnter, "buttonEnter");
            this.buttonEnter.BackgroundImage = global::Bat1.Properties.Resources.KeyEnter;
            this.buttonEnter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonEnter.Font = null;
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.UseVisualStyleBackColor = true;
            this.buttonEnter.Click += new System.EventHandler(this.buttonEsc_Click);
            // 
            // buttonLeft
            // 
            this.buttonLeft.AccessibleDescription = null;
            this.buttonLeft.AccessibleName = null;
            resources.ApplyResources(this.buttonLeft, "buttonLeft");
            this.buttonLeft.BackgroundImage = global::Bat1.Properties.Resources.KeyLeft;
            this.buttonLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLeft.Font = null;
            this.buttonLeft.Name = "buttonLeft";
            this.buttonLeft.UseVisualStyleBackColor = true;
            this.buttonLeft.Click += new System.EventHandler(this.buttonEsc_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.AccessibleDescription = null;
            this.buttonDown.AccessibleName = null;
            resources.ApplyResources(this.buttonDown, "buttonDown");
            this.buttonDown.BackgroundImage = global::Bat1.Properties.Resources.KeyDown;
            this.buttonDown.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDown.Font = null;
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.buttonEsc_Click);
            // 
            // buttonRight
            // 
            this.buttonRight.AccessibleDescription = null;
            this.buttonRight.AccessibleName = null;
            resources.ApplyResources(this.buttonRight, "buttonRight");
            this.buttonRight.BackgroundImage = global::Bat1.Properties.Resources.KeyRight;
            this.buttonRight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRight.Font = null;
            this.buttonRight.Name = "buttonRight";
            this.buttonRight.UseVisualStyleBackColor = true;
            this.buttonRight.Click += new System.EventHandler(this.buttonEsc_Click);
            // 
            // pictureBoxKey4
            // 
            this.pictureBoxKey4.AccessibleDescription = null;
            this.pictureBoxKey4.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey4, "pictureBoxKey4");
            this.pictureBoxKey4.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey4.BackgroundImage = null;
            this.pictureBoxKey4.Font = null;
            this.pictureBoxKey4.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey4.ImageLocation = null;
            this.pictureBoxKey4.Name = "pictureBoxKey4";
            this.pictureBoxKey4.TabStop = false;
            // 
            // pictureBoxKey3
            // 
            this.pictureBoxKey3.AccessibleDescription = null;
            this.pictureBoxKey3.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey3, "pictureBoxKey3");
            this.pictureBoxKey3.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey3.BackgroundImage = null;
            this.pictureBoxKey3.Font = null;
            this.pictureBoxKey3.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey3.ImageLocation = null;
            this.pictureBoxKey3.Name = "pictureBoxKey3";
            this.pictureBoxKey3.TabStop = false;
            // 
            // pictureBoxKey2
            // 
            this.pictureBoxKey2.AccessibleDescription = null;
            this.pictureBoxKey2.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey2, "pictureBoxKey2");
            this.pictureBoxKey2.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey2.BackgroundImage = null;
            this.pictureBoxKey2.Font = null;
            this.pictureBoxKey2.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey2.ImageLocation = null;
            this.pictureBoxKey2.Name = "pictureBoxKey2";
            this.pictureBoxKey2.TabStop = false;
            // 
            // pictureBoxKey1
            // 
            this.pictureBoxKey1.AccessibleDescription = null;
            this.pictureBoxKey1.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxKey1, "pictureBoxKey1");
            this.pictureBoxKey1.BackColor = System.Drawing.Color.White;
            this.pictureBoxKey1.BackgroundImage = null;
            this.pictureBoxKey1.Font = null;
            this.pictureBoxKey1.Image = global::Bat1.Properties.Resources.KeyDown;
            this.pictureBoxKey1.ImageLocation = null;
            this.pictureBoxKey1.Name = "pictureBoxKey1";
            this.pictureBoxKey1.TabStop = false;
            // 
            // pictureBoxNumber4
            // 
            this.pictureBoxNumber4.AccessibleDescription = null;
            this.pictureBoxNumber4.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber4, "pictureBoxNumber4");
            this.pictureBoxNumber4.BackgroundImage = null;
            this.pictureBoxNumber4.Font = null;
            this.pictureBoxNumber4.Image = global::Bat1.Properties.Resources.PasswordKey4;
            this.pictureBoxNumber4.ImageLocation = null;
            this.pictureBoxNumber4.Name = "pictureBoxNumber4";
            this.pictureBoxNumber4.TabStop = false;
            // 
            // pictureBoxNumber3
            // 
            this.pictureBoxNumber3.AccessibleDescription = null;
            this.pictureBoxNumber3.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber3, "pictureBoxNumber3");
            this.pictureBoxNumber3.BackgroundImage = null;
            this.pictureBoxNumber3.Font = null;
            this.pictureBoxNumber3.Image = global::Bat1.Properties.Resources.PasswordKey3;
            this.pictureBoxNumber3.ImageLocation = null;
            this.pictureBoxNumber3.Name = "pictureBoxNumber3";
            this.pictureBoxNumber3.TabStop = false;
            // 
            // pictureBoxNumber2
            // 
            this.pictureBoxNumber2.AccessibleDescription = null;
            this.pictureBoxNumber2.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber2, "pictureBoxNumber2");
            this.pictureBoxNumber2.BackgroundImage = null;
            this.pictureBoxNumber2.Font = null;
            this.pictureBoxNumber2.Image = global::Bat1.Properties.Resources.PasswordKey2;
            this.pictureBoxNumber2.ImageLocation = null;
            this.pictureBoxNumber2.Name = "pictureBoxNumber2";
            this.pictureBoxNumber2.TabStop = false;
            // 
            // pictureBoxNumber1
            // 
            this.pictureBoxNumber1.AccessibleDescription = null;
            this.pictureBoxNumber1.AccessibleName = null;
            resources.ApplyResources(this.pictureBoxNumber1, "pictureBoxNumber1");
            this.pictureBoxNumber1.BackgroundImage = null;
            this.pictureBoxNumber1.Font = null;
            this.pictureBoxNumber1.Image = global::Bat1.Properties.Resources.PasswordKey1;
            this.pictureBoxNumber1.ImageLocation = null;
            this.pictureBoxNumber1.Name = "pictureBoxNumber1";
            this.pictureBoxNumber1.TabStop = false;
            // 
            // buttonNew
            // 
            this.buttonNew.AccessibleDescription = null;
            this.buttonNew.AccessibleName = null;
            resources.ApplyResources(this.buttonNew, "buttonNew");
            this.buttonNew.BackgroundImage = null;
            this.buttonNew.Font = null;
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.AccessibleDescription = null;
            this.buttonOk.AccessibleName = null;
            resources.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.BackgroundImage = null;
            this.buttonOk.Font = null;
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.AccessibleDescription = null;
            this.buttonCancel.AccessibleName = null;
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.BackgroundImage = null;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Font = null;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // groupBoxKeyboard
            // 
            this.groupBoxKeyboard.AccessibleDescription = null;
            this.groupBoxKeyboard.AccessibleName = null;
            resources.ApplyResources(this.groupBoxKeyboard, "groupBoxKeyboard");
            this.groupBoxKeyboard.BackgroundImage = null;
            this.groupBoxKeyboard.Controls.Add(this.buttonEsc);
            this.groupBoxKeyboard.Controls.Add(this.buttonRight);
            this.groupBoxKeyboard.Controls.Add(this.buttonDown);
            this.groupBoxKeyboard.Controls.Add(this.buttonLeft);
            this.groupBoxKeyboard.Controls.Add(this.buttonEnter);
            this.groupBoxKeyboard.Controls.Add(this.buttonUp);
            this.groupBoxKeyboard.Font = null;
            this.groupBoxKeyboard.Name = "groupBoxKeyboard";
            this.groupBoxKeyboard.TabStop = false;
            // 
            // groupBoxPassword
            // 
            this.groupBoxPassword.AccessibleDescription = null;
            this.groupBoxPassword.AccessibleName = null;
            resources.ApplyResources(this.groupBoxPassword, "groupBoxPassword");
            this.groupBoxPassword.BackgroundImage = null;
            this.groupBoxPassword.Controls.Add(this.buttonNew);
            this.groupBoxPassword.Controls.Add(this.pictureBoxKey1);
            this.groupBoxPassword.Controls.Add(this.pictureBoxKey3);
            this.groupBoxPassword.Controls.Add(this.pictureBoxNumber4);
            this.groupBoxPassword.Controls.Add(this.pictureBoxNumber1);
            this.groupBoxPassword.Controls.Add(this.pictureBoxKey2);
            this.groupBoxPassword.Controls.Add(this.pictureBoxKey4);
            this.groupBoxPassword.Controls.Add(this.pictureBoxNumber2);
            this.groupBoxPassword.Controls.Add(this.pictureBoxNumber3);
            this.groupBoxPassword.Font = null;
            this.groupBoxPassword.Name = "groupBoxPassword";
            this.groupBoxPassword.TabStop = false;
            // 
            // FormScalePassword
            // 
            this.AcceptButton = this.buttonOk;
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.CancelButton = this.buttonCancel;
            this.Controls.Add(this.groupBoxPassword);
            this.Controls.Add(this.groupBoxKeyboard);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Font = null;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = null;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormScalePassword";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxKey1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNumber1)).EndInit();
            this.groupBoxKeyboard.ResumeLayout(false);
            this.groupBoxPassword.ResumeLayout(false);
            this.groupBoxPassword.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonEsc;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonEnter;
        private System.Windows.Forms.Button buttonLeft;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonRight;
        private System.Windows.Forms.PictureBox pictureBoxKey4;
        private System.Windows.Forms.PictureBox pictureBoxKey3;
        private System.Windows.Forms.PictureBox pictureBoxKey2;
        private System.Windows.Forms.PictureBox pictureBoxKey1;
        private System.Windows.Forms.PictureBox pictureBoxNumber4;
        private System.Windows.Forms.PictureBox pictureBoxNumber3;
        private System.Windows.Forms.PictureBox pictureBoxNumber2;
        private System.Windows.Forms.PictureBox pictureBoxNumber1;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBoxKeyboard;
        private System.Windows.Forms.GroupBox groupBoxPassword;
    }
}