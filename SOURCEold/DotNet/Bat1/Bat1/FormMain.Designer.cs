﻿namespace Bat1 {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.toolStripMain = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonScale = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonWeighings = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonStatistics = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonFlocks = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonMaintenance = new System.Windows.Forms.ToolStripButton();
            this.panelPages = new System.Windows.Forms.Panel();
            this.toolStripMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMain
            // 
            this.toolStripMain.AccessibleDescription = null;
            this.toolStripMain.AccessibleName = null;
            resources.ApplyResources(this.toolStripMain, "toolStripMain");
            this.toolStripMain.BackgroundImage = null;
            this.toolStripMain.Font = null;
            this.toolStripMain.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonScale,
            this.toolStripButtonWeighings,
            this.toolStripButtonStatistics,
            this.toolStripButtonFlocks,
            this.toolStripButtonMaintenance});
            this.toolStripMain.Name = "toolStripMain";
            // 
            // toolStripButtonScale
            // 
            this.toolStripButtonScale.AccessibleDescription = null;
            this.toolStripButtonScale.AccessibleName = null;
            resources.ApplyResources(this.toolStripButtonScale, "toolStripButtonScale");
            this.toolStripButtonScale.BackgroundImage = null;
            this.toolStripButtonScale.Image = global::Bat1.Properties.Resources.IconScale;
            this.toolStripButtonScale.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripButtonScale.Name = "toolStripButtonScale";
            this.toolStripButtonScale.Padding = new System.Windows.Forms.Padding(5, 5, 5, 1);
            this.toolStripButtonScale.Click += new System.EventHandler(this.toolStripButtonReadData_Click);
            // 
            // toolStripButtonWeighings
            // 
            this.toolStripButtonWeighings.AccessibleDescription = null;
            this.toolStripButtonWeighings.AccessibleName = null;
            resources.ApplyResources(this.toolStripButtonWeighings, "toolStripButtonWeighings");
            this.toolStripButtonWeighings.BackgroundImage = null;
            this.toolStripButtonWeighings.Image = global::Bat1.Properties.Resources.IconWeighings;
            this.toolStripButtonWeighings.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripButtonWeighings.Name = "toolStripButtonWeighings";
            this.toolStripButtonWeighings.Padding = new System.Windows.Forms.Padding(5, 5, 5, 1);
            this.toolStripButtonWeighings.Click += new System.EventHandler(this.toolStripButtonWeighings_Click);
            // 
            // toolStripButtonStatistics
            // 
            this.toolStripButtonStatistics.AccessibleDescription = null;
            this.toolStripButtonStatistics.AccessibleName = null;
            resources.ApplyResources(this.toolStripButtonStatistics, "toolStripButtonStatistics");
            this.toolStripButtonStatistics.BackgroundImage = null;
            this.toolStripButtonStatistics.Image = global::Bat1.Properties.Resources.IconStatistics;
            this.toolStripButtonStatistics.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripButtonStatistics.Name = "toolStripButtonStatistics";
            this.toolStripButtonStatistics.Padding = new System.Windows.Forms.Padding(5, 5, 5, 1);
            this.toolStripButtonStatistics.Click += new System.EventHandler(this.toolStripButtonStatistics_Click);
            // 
            // toolStripButtonFlocks
            // 
            this.toolStripButtonFlocks.AccessibleDescription = null;
            this.toolStripButtonFlocks.AccessibleName = null;
            resources.ApplyResources(this.toolStripButtonFlocks, "toolStripButtonFlocks");
            this.toolStripButtonFlocks.BackgroundImage = null;
            this.toolStripButtonFlocks.Image = global::Bat1.Properties.Resources.IconFlocks;
            this.toolStripButtonFlocks.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripButtonFlocks.Name = "toolStripButtonFlocks";
            this.toolStripButtonFlocks.Padding = new System.Windows.Forms.Padding(5, 5, 5, 1);
            this.toolStripButtonFlocks.Click += new System.EventHandler(this.toolStripButtonFlocks_Click);
            // 
            // toolStripButtonMaintenance
            // 
            this.toolStripButtonMaintenance.AccessibleDescription = null;
            this.toolStripButtonMaintenance.AccessibleName = null;
            resources.ApplyResources(this.toolStripButtonMaintenance, "toolStripButtonMaintenance");
            this.toolStripButtonMaintenance.BackgroundImage = null;
            this.toolStripButtonMaintenance.Image = global::Bat1.Properties.Resources.IconMaintenance;
            this.toolStripButtonMaintenance.Margin = new System.Windows.Forms.Padding(5, 0, 10, 0);
            this.toolStripButtonMaintenance.Name = "toolStripButtonMaintenance";
            this.toolStripButtonMaintenance.Padding = new System.Windows.Forms.Padding(5, 5, 5, 1);
            this.toolStripButtonMaintenance.Click += new System.EventHandler(this.toolStripButtonMaintenance_Click);
            // 
            // panelPages
            // 
            this.panelPages.AccessibleDescription = null;
            this.panelPages.AccessibleName = null;
            resources.ApplyResources(this.panelPages, "panelPages");
            this.panelPages.BackgroundImage = null;
            this.panelPages.Font = null;
            this.panelPages.Name = "panelPages";
            // 
            // FormMain
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.panelPages);
            this.Controls.Add(this.toolStripMain);
            this.Font = null;
            this.Icon = null;
            this.Name = "FormMain";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.toolStripMain.ResumeLayout(false);
            this.toolStripMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMain;
        private System.Windows.Forms.Panel panelPages;
        private System.Windows.Forms.ToolStripButton toolStripButtonScale;
        private System.Windows.Forms.ToolStripButton toolStripButtonWeighings;
        private System.Windows.Forms.ToolStripButton toolStripButtonStatistics;
        private System.Windows.Forms.ToolStripButton toolStripButtonFlocks;
        private System.Windows.Forms.ToolStripButton toolStripButtonMaintenance;
    }
}

