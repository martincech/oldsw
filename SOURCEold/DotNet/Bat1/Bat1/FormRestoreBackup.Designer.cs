﻿namespace Bat1 {
    partial class FormRestoreBackup {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRestoreBackup));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxManual = new System.Windows.Forms.TextBox();
            this.radioButtonLastWorking = new System.Windows.Forms.RadioButton();
            this.radioButtonPeriodic = new System.Windows.Forms.RadioButton();
            this.radioButtonManual = new System.Windows.Forms.RadioButton();
            this.labelCreated = new System.Windows.Forms.Label();
            this.buttonChooseManual = new System.Windows.Forms.Button();
            this.comboBoxAuto = new System.Windows.Forms.ComboBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AccessibleDescription = null;
            this.groupBox1.AccessibleName = null;
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.BackgroundImage = null;
            this.groupBox1.Controls.Add(this.textBoxManual);
            this.groupBox1.Controls.Add(this.radioButtonLastWorking);
            this.groupBox1.Controls.Add(this.radioButtonPeriodic);
            this.groupBox1.Controls.Add(this.radioButtonManual);
            this.groupBox1.Controls.Add(this.labelCreated);
            this.groupBox1.Controls.Add(this.buttonChooseManual);
            this.groupBox1.Controls.Add(this.comboBoxAuto);
            this.groupBox1.Font = null;
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // textBoxManual
            // 
            this.textBoxManual.AccessibleDescription = null;
            this.textBoxManual.AccessibleName = null;
            resources.ApplyResources(this.textBoxManual, "textBoxManual");
            this.textBoxManual.BackgroundImage = null;
            this.textBoxManual.Font = null;
            this.textBoxManual.Name = "textBoxManual";
            // 
            // radioButtonLastWorking
            // 
            this.radioButtonLastWorking.AccessibleDescription = null;
            this.radioButtonLastWorking.AccessibleName = null;
            resources.ApplyResources(this.radioButtonLastWorking, "radioButtonLastWorking");
            this.radioButtonLastWorking.BackgroundImage = null;
            this.radioButtonLastWorking.Name = "radioButtonLastWorking";
            this.radioButtonLastWorking.TabStop = true;
            this.radioButtonLastWorking.UseVisualStyleBackColor = true;
            this.radioButtonLastWorking.CheckedChanged += new System.EventHandler(this.radioButtonLastWorking_CheckedChanged);
            // 
            // radioButtonPeriodic
            // 
            this.radioButtonPeriodic.AccessibleDescription = null;
            this.radioButtonPeriodic.AccessibleName = null;
            resources.ApplyResources(this.radioButtonPeriodic, "radioButtonPeriodic");
            this.radioButtonPeriodic.BackgroundImage = null;
            this.radioButtonPeriodic.Name = "radioButtonPeriodic";
            this.radioButtonPeriodic.TabStop = true;
            this.radioButtonPeriodic.UseVisualStyleBackColor = true;
            this.radioButtonPeriodic.CheckedChanged += new System.EventHandler(this.radioButtonPeriodic_CheckedChanged);
            // 
            // radioButtonManual
            // 
            this.radioButtonManual.AccessibleDescription = null;
            this.radioButtonManual.AccessibleName = null;
            resources.ApplyResources(this.radioButtonManual, "radioButtonManual");
            this.radioButtonManual.BackgroundImage = null;
            this.radioButtonManual.Name = "radioButtonManual";
            this.radioButtonManual.TabStop = true;
            this.radioButtonManual.UseVisualStyleBackColor = true;
            this.radioButtonManual.CheckedChanged += new System.EventHandler(this.radioButtonManual_CheckedChanged);
            // 
            // labelCreated
            // 
            this.labelCreated.AccessibleDescription = null;
            this.labelCreated.AccessibleName = null;
            resources.ApplyResources(this.labelCreated, "labelCreated");
            this.labelCreated.Font = null;
            this.labelCreated.Name = "labelCreated";
            // 
            // buttonChooseManual
            // 
            this.buttonChooseManual.AccessibleDescription = null;
            this.buttonChooseManual.AccessibleName = null;
            resources.ApplyResources(this.buttonChooseManual, "buttonChooseManual");
            this.buttonChooseManual.BackgroundImage = null;
            this.buttonChooseManual.Font = null;
            this.buttonChooseManual.Name = "buttonChooseManual";
            this.buttonChooseManual.UseVisualStyleBackColor = true;
            this.buttonChooseManual.Click += new System.EventHandler(this.buttonChooseManual_Click);
            // 
            // comboBoxAuto
            // 
            this.comboBoxAuto.AccessibleDescription = null;
            this.comboBoxAuto.AccessibleName = null;
            resources.ApplyResources(this.comboBoxAuto, "comboBoxAuto");
            this.comboBoxAuto.BackgroundImage = null;
            this.comboBoxAuto.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAuto.Font = null;
            this.comboBoxAuto.FormattingEnabled = true;
            this.comboBoxAuto.Name = "comboBoxAuto";
            // 
            // buttonCancel
            // 
            this.buttonCancel.AccessibleDescription = null;
            this.buttonCancel.AccessibleName = null;
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.BackgroundImage = null;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Font = null;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            this.buttonOk.AccessibleDescription = null;
            this.buttonOk.AccessibleName = null;
            resources.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.BackgroundImage = null;
            this.buttonOk.Font = null;
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // FormRestoreBackup
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.CancelButton = this.buttonCancel;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Font = null;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = null;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormRestoreBackup";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.FormRestoreBackup_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxManual;
        private System.Windows.Forms.RadioButton radioButtonLastWorking;
        private System.Windows.Forms.RadioButton radioButtonPeriodic;
        private System.Windows.Forms.RadioButton radioButtonManual;
        private System.Windows.Forms.Label labelCreated;
        private System.Windows.Forms.Button buttonChooseManual;
        private System.Windows.Forms.ComboBox comboBoxAuto;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;


    }
}