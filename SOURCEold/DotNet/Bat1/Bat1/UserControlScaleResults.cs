﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlScaleResults : UserControlResultsBase {
        /// <summary>
        /// List of displayed weighings
        /// </summary>
        private WeighingList weighingList = new WeighingList();

        /// <summary>
        /// Config of the scale
        /// </summary>
        private ScaleConfig scaleConfig;

        /// <summary>
        /// Scale name
        /// </summary>
        public string ScaleName { get { return scaleConfig.ScaleName; } }
        
        /// <summary>
        /// Data are dowloaded from old scale version
        /// </summary>
        private bool isOldVersion;

        /// <summary>
        /// Constructor
        /// </summary>
        public UserControlScaleResults() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Nactu seznamy vah a souboru z databaze
            LoadLists();

            // Zacinam plnit data do controlu
            isLoading = true;

            try {
                // Nahraju z databaze naposledy pouzite jednotky - staci jednou pri startu programu
                LoadUnits();
            } finally {
                isLoading = false;
            }
        }

        public bool ReadFromScale() {
            FormScaleWait form = new FormScaleWait(ParentForm);
            Cursor.Current = Cursors.WaitCursor;
            try {
                form.Show();
                form.Refresh();
                return Read();
            } finally {
                form.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Load last used units from database
        /// </summary>
        private void LoadUnits() {
            comboBoxUnits.SelectedIndex = (int)Program.Setup.ScaleVersion6Units;
        }

        /// <summary>
        /// Save selected units to database
        /// </summary>
        private void SaveUnits() {
            Program.Setup.ScaleVersion6Units = (Units)comboBoxUnits.SelectedIndex;
            Program.Database.SaveSetup();
        }

        /// <summary>
        /// Show or hide units controls
        /// </summary>
        /// <param name="show"></param>
        private void ShowUnitsControls(bool show) {
            labelUnits.Visible    = show;
            comboBoxUnits.Visible = show;
        }

        private void AddWeighingToDataGrid(Weighing weighing) {
            // Zobrazuju pro vsechny flagy
            StatisticResult result = weighing.WeighingResults.GetResult(Flag.ALL);

            // Vytvorim nahled histogramu
            Bitmap bitmap = new Bitmap(HistogramMiniGraph.GetTotalWidth(result.Histogram.Counts.Length), customControlHistogramDataGridViewWeighings.RowTemplate.Height - 3);
            HistogramMiniGraph.Draw(result.Histogram.Counts, Graphics.FromImage(bitmap), bitmap.Height - 1, 1, true);

            // Pridam radek do tabulky
            int index = customControlHistogramDataGridViewWeighings.Rows.Add(weighing.WeighingData.File.ToString(),                         // Vcetne poznamky
                                                                             weighing.WeighingData.SampleList.MinDateTime.ToString("G"),    // Cas vcetne sekund
                                                                             result.Count.ToString("N0"),                                   // N0 oddeli tisicovky
                                                                             DisplayFormat.RoundWeight(result.Average, weighing.WeighingData.ScaleConfig.Units.Units) + " " + ConvertWeight.UnitsToString(weighing.WeighingData.ScaleConfig.Units.Units),
                                                                             "1",       // Default nedelim
                                                                             DisplayFormat.Uniformity(result.Uniformity) + " %",
                                                                             bitmap,
                                                                             weighing.WeighingData.Note
                                                                             );
            
            // Nastavim tooltip na sloupec deleni
            customControlHistogramDataGridViewWeighings.Rows[index].Cells[4].ToolTipText = Properties.Resources.TOOLTIP_SELECT_NUMBER_OF_BIRDS;
        }

        private void RedrawFiles() {
            // Seznam vazeni
            customControlHistogramDataGridViewWeighings.Rows.Clear();
            for (int i = 0; i < weighingList.List.Count; i++) {
                AddWeighingToDataGrid(weighingList.List[i]);
            }
        }

        private void Redraw() {
            isLoading = true;

            try {
                // Jmeno vahy
                labelScaleName.Text = scaleConfig.ScaleName;
                
                // Seznam vazeni
                RedrawFiles();

                // Jednotky u stare vahy
                ShowUnitsControls(isOldVersion);
            } finally {
                isLoading = false;
            }
        }

        private void OldScaleChangeUnits(Units newUnits) {
            Units oldUnits = scaleConfig.Units.Units;
            
            // Pokud neni zadna zmena, nic neprepocitavam
            if (newUnits == oldUnits) {
                return;
            }
            
            // Ulozim nove jednotky do configu. zaroven prepoctu vsechny hmotnosti v configu
            scaleConfig.MoveDecimalPoint(newUnits);

            // Prepoctu hmotnosti u vsech zobrazenych vazeni ze starych jednotek na nove
            foreach (Weighing weighing in weighingList.List) {
                weighing.WeighingData.SampleList.MoveDecimalPoint(oldUnits, newUnits);
                weighing.RecalculateResults();      // Zaroven prepoctu statistiku
            }
        }
        
        private bool ContainsData(List<WeighingData> weighingDataList) {
            foreach (WeighingData weighingData in weighingDataList) {
                if (weighingData.SampleList.Count > 0) {
                    return true;
                }
            }
            return false;
        }

        private void SynchronizeTime(Bat1Version7 newBat1) {
            // Zeptam se, zda chce nastavit
            if (MessageBox.Show(Properties.Resources.SCALE_DATE_TIME_DIFFERENT, Properties.Resources.DATE_AND_TIME, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            // Nastavim cas ve vaze
            newBat1.SetDateTime();
        }

        private void TrySynchronizeTime(Bat1Version7 newBat1) {
            // Nactu cas vahy
            DateTime scaleDateTime;
            try {
                if (!newBat1.GetDateTime(out scaleDateTime)) {
                    return;
                }
            } catch {
                // Cas je v neplatnem formatu (napr. po prvnim naprogramovani vahy)
                SynchronizeTime(newBat1);
                return;
            }
            
            // Vypoctu rozdil casu ve vaze a v PC
            TimeSpan span = scaleDateTime - DateTime.Now;
            if (Math.Abs(span.TotalMinutes) <= 20) {
                return;     // Rozdil je do 20 minut, nic nenastavuju
            }

            // Rozdil je veliky, zeptam se, zda chce synchronizovat a pripadne nastavim cas
            SynchronizeTime(newBat1);
        }
        
        /// <summary>
        /// Read data from connected scale and display it
        /// </summary>
        /// <returns>True if successful</returns>
        private bool Read() {
            bool isDriverInstalled = true;
            List<WeighingData> weighingDataList;
            ScaleConfig scaleConfig;

            // Zkusim stahnout data z nove vahy
            Bat1Version7 newBat1 = null;
            try {
                newBat1 = new Bat1Version7();
            } catch {
                // Ovladac vahy neni nainstalovan
                isDriverInstalled = false;
            }
            if (isDriverInstalled && newBat1.ReadAll(out scaleConfig, out weighingDataList)) {
                // Je pripojena nova vaha
                isOldVersion = false;

                // Zkontroluju, zda vaha neni novejsi nez SW
                if (!newBat1.IsSupported()) {
                    MessageBox.Show(Properties.Resources.SCALE_NOT_SUPPORTED, Program.ApplicationName);
                    return false;
                }

                // Zkontroluju datum a cas ve vaze. Pokud je moc rozjety oproti PC, umoznim synchronizaci.
                // Pokud se nepodari nacist z vahy datum a cas, chybu nezobrazuju a synchronizaci vubec neprovadim.
                TrySynchronizeTime(newBat1);
            } else {
                // Nova vaha neni pripojena, zkontroluju, zda neni pripojena starsi verze vah
                Bat1Version6 oldBat1 = new Bat1Version6(Program.Setup.ScaleVersion6ComPort);
                if (!oldBat1.ReadAll(out scaleConfig, out weighingDataList)) {
                    // Neni pripojena zadna vaha
                    MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                    return false;
                }
                isOldVersion = true;
            }

            // Pokud vaha neobsahuje zadne vzorky, koncim
            if (!ContainsData(weighingDataList)) {
                MessageBox.Show(Properties.Resources.SCALE_CONTAINS_NO_DATA, Program.ApplicationName);
                return false;
            }

            // Preberu config vahy
            this.scaleConfig = scaleConfig;

            // Ulozim si nactena data, zaroven se vypoctou statistiky
            weighingList.Clear();
            foreach (WeighingData weighingData in weighingDataList) {
                if (weighingData.SampleList.Count > 0) {
                    // Ukladam jen soubory, kde jsou vzorky. Prazdne soubory nezobrazuju, ulozit stejne nejdou.
                    weighingList.Add(new Weighing(weighingData));
                }
            }

            // Pokud stahnul data ze stare vahy, nastavim u vsech vzorku vybrane jednotky
            if (isOldVersion) {
                OldScaleChangeUnits(Program.Setup.ScaleVersion6Units);
            }

            // Pridam prave nactene jmeno vahy do seznamu vah (pokud v seznamu jeste neni). Pokud bude nacitat postupne vice vah,
            // seznam se bude pouze rozrustat (tj. predchozi nactene vahy v seznamu zustanou).
            AddScale(scaleConfig.ScaleName);

            // Pridam nactene soubory do seznamu souboru (pridam i prazdne soubory, ktere normalne nezobrazuju). Seznam
            // souboru nevyuzim primo zde, ale az pri pripadne editaci souboru. Zde pouze plnim kumulovany seznam souboru.
            foreach (File file in scaleConfig.FileList.List) {
                AddFile(new NameNote(file.Name, file.Note));
            }

            // Zobrazim
            Redraw();

            return true;
        }

        /// <summary>
        /// Return currently selected weighing
        /// </summary>
        /// <returns>Weighing</returns>
        private Weighing GetSelectedWeighing() {
            if (customControlHistogramDataGridViewWeighings.Rows.Count == 0) {
                return null;        // Tabulka je prazdna
            }

            return weighingList.List[customControlHistogramDataGridViewWeighings.SelectedRows[0].Index];
        }

        private void Edit() {
            Weighing weighing = GetSelectedWeighing();
            if (weighing == null) {
                return;         // Tabulka je prazdna
            }

            // Predani 12tis vzorku trva, dam presypaci hodiny
            FormEditWeighing form;
            string oldFileName = weighing.WeighingData.File.Name;
            Cursor.Current = Cursors.WaitCursor;
            try {
                // Deleni nepovolim, to delam az pi ukladani
                form = new FormEditWeighing(weighing.WeighingData, fileList, scaleConfig, false, false);
            } finally {
                Cursor.Current = Cursors.Default;
            }
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }
            
            // weighing.WeighingData.File je ukazatel primo na definici souboru v configu vahy, tj. pokud zmenil
            // jmeno nebo poznamku souboru, zmena se provedla primo v configu. Neni tak uz treba delat nic rucne.
            
            // Pokud pridal novy soubor nebo zmenil u souboru poznamku, updatuju tento soubor i v seznamu
            AddFile(new NameNote(weighing.WeighingData.File.Name, weighing.WeighingData.File.Note));

            // Ulozim si vybrany radek
            int selectedRow = customControlHistogramDataGridViewWeighings.SelectedRows[0].Index;

            // Prepoctu statistiku
            weighing.RecalculateResults();
            
            // Prekreslim seznam vazeni
            RedrawFiles();

            // Vratim se zpet na puvodni radek - musim osetrit i pripad, kdy je v seznamu moc vazeni a je odscrolovany dolu
            customControlHistogramDataGridViewWeighings.FirstDisplayedScrollingRowIndex = customControlHistogramDataGridViewWeighings.Rows[selectedRow].Index;
            customControlHistogramDataGridViewWeighings.Refresh();      // Prekreslim s vybranym radkem na prvni pozici
            customControlHistogramDataGridViewWeighings.CurrentCell = customControlHistogramDataGridViewWeighings.Rows[selectedRow].Cells[0];
            customControlHistogramDataGridViewWeighings.Rows[selectedRow].Selected = true;
        }

        private void SaveWeighings(List<Weighing> selectedWeighingList, List<int> divideByList) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                // Ulozim vybrana vazeni
                List<Weighing> saveWeighingList = new List<Weighing>();
                List<SampleList> originalSampleListList = new List<SampleList>();
                for (int i = 0; i < selectedWeighingList.Count; i++) {
                    Weighing weighing = selectedWeighingList[i];

                    // Zkontroluju, zda uz vazeni v databazi neexistuje (kombinace soubor - start vazeni)
                    DialogResult dialogResult = CanSaveWeighing(weighing.WeighingData.File.Name, weighing.WeighingData.SampleList.MinDateTime);
                    if (dialogResult == DialogResult.No) {
                        continue;       // Vazeni uz existuje, nechce ho prepsat, ale chce pokracovat v ukladani
                    } else if (dialogResult == DialogResult.Cancel) {
                        return;         // Vazeni uz existuje, chce ukoncit cele ukladani
                    }

                    // Vazeni je mozne do databaze ulozit

                    // Ulozim si kopii puvodniho seznamu vzorku
                    originalSampleListList.Add(new SampleList(weighing.WeighingData.SampleList));

                    // Vydelim vsechny vzorky tohoto vazeni
                    weighing.WeighingData.SampleList.DivideWeights(divideByList[i],
                                                                   weighing.WeighingData.ScaleConfig.Units.Units);

                    // Pridam vazeni do seznamu pro ulozeni
                    saveWeighingList.Add(weighing);
                }

                // Pokud je seznam vazeni pro ulozeni prazdny, koncim
                if (saveWeighingList.Count == 0) {
                    return;
                }

                // Ulozim seznam vazeni naraz do DB
                Program.Database.SaveWeighingList(saveWeighingList);

                // Obnovim puvodni, nevydelene seznamy vzorku u vsech vazeni
                for (int i = 0; i < saveWeighingList.Count; i++) {
                    saveWeighingList[i].WeighingData.SampleList = new SampleList(originalSampleListList[i]);
                }

                // Zobrazim info, ze se ulozilo
                Cursor.Current = Cursors.Default;
                MessageBox.Show(Properties.Resources.WEIGHINGS_SAVED, Program.ApplicationName);

            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void customControlHistogramDataGridViewWeighings_CurrentCellDirtyStateChanged(object sender, EventArgs e) {
            // Event CellValueChanged se vyvola az po opusteni bunky => po kliknuti na Save hodnotu okamzite potvrdim
            // a vyvolam tak CellValueChanged rucne.
            if (customControlHistogramDataGridViewWeighings.IsCurrentCellDirty) { 
                customControlHistogramDataGridViewWeighings.CommitEdit(DataGridViewDataErrorContexts.Commit); 
            } 
        }

        private void comboBoxUnits_SelectionChangeCommitted(object sender, EventArgs e) {
            if (!isOldVersion) {
                return;
            }

            // Ulozim si vybrane jednotky
            SaveUnits();

            // Pokud jsou stazena nejaka data, nastavim je a prekreslim
            if (scaleConfig != null) {
                OldScaleChangeUnits(Program.Setup.ScaleVersion6Units);
                Redraw();
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e) {
            Edit();
        }

        private void UserControlScaleResults_Load(object sender, EventArgs e) {
            // Prehodim focus na tabulku
            customControlHistogramDataGridViewWeighings.Focus();
        }

        private void customControlHistogramDataGridViewWeighings_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) {
                return;     // Header
            }
            Edit();
        }

        private void linkLabelManual_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e) {
            new FormManualResults().ShowDialog();
        }

        private void customControlHistogramDataGridViewWeighings_CellClick(object sender, DataGridViewCellEventArgs e) {
            // Po kliknuti na combobox jej rovnou rozbalim, jinak jsou treba az 3 kliknuti.
            
            if (e.RowIndex < 0) {
                return;     // Header
            }
            if (e.ColumnIndex != 4) {
                return;     // Chci index sloupce s comboboxem
            }

            // Kod viz: http://stackoverflow.com/questions/241100/how-to-manually-drop-down-a-datagridviewcomboboxcolumn
            customControlHistogramDataGridViewWeighings.BeginEdit(true);
            ComboBox comboBox = (ComboBox)customControlHistogramDataGridViewWeighings.EditingControl;
            comboBox.DroppedDown = true;
        }

        private void buttonSaveAll_Click(object sender, EventArgs e) {
            if (weighingList.List.Count == 0) {
                return;     // Seznam je prazdny
            }

            // Sestavim seznam poctu kusu, kterym se bude delit hmotnost.
            List<int> divideByList = new List<int>();
            for (int i = 0; i < weighingList.List.Count; i++) {
                string divideByString = (string)customControlHistogramDataGridViewWeighings.Rows[i].Cells["ColumnDivide"].Value;
                int divideBy;
                if (!int.TryParse(divideByString, out divideBy)) {
                    divideBy = 1;
                }
                divideByList.Add(divideBy);
            }

            // Ulozim vybrana vazeni
            SaveWeighings(weighingList.List, divideByList);

            // Zapamatuju si, ze jsem nactena data ulozil
            SaveStatus.Saved = true;
        }

        private void buttonSaveSelected_Click(object sender, EventArgs e) {
            if (weighingList.List.Count == 0) {
                return;     // Seznam je prazdny
            }

            // Sestavim seznam vazeni, ktere chce ulozit.
            // Zaroven sestavim seznam poctu kusu, kterym se bude delit hmotnost.
            List<Weighing> selectedWeighingList = new List<Weighing>();
            List<int> divideByList = new List<int>();
            for (int i = 0; i < weighingList.List.Count; i++) {
                if (!customControlHistogramDataGridViewWeighings.Rows[i].Selected) {
                    continue;       // Toto vazeni nechce ulozit
                }
                selectedWeighingList.Add(weighingList.List[i]);
                string divideByString = (string)customControlHistogramDataGridViewWeighings.Rows[i].Cells["ColumnDivide"].Value;
                int divideBy;
                if (!int.TryParse(divideByString, out divideBy)) {
                    divideBy = 1;
                }
                divideByList.Add(divideBy);
            }

            // Ulozim vybrana vazeni
            SaveWeighings(selectedWeighingList, divideByList);

            // Zapamatuju si, ze jsem nactena data ulozil - zde aspon vybrana vazeni, dal to neresim.
            SaveStatus.Saved = true;
        }

        private void buttonExportSelected_Click(object sender, EventArgs e) {
            if (weighingList.List.Count == 0) {
                return;     // Seznam je prazdny
            }

            // Sestavim seznam vazeni, ktere chce ulozit.
            List<Weighing> selectedWeighingList = new List<Weighing>();
            for (int i = 0; i < weighingList.List.Count; i++) {
                if (!customControlHistogramDataGridViewWeighings.Rows[i].Selected) {
                    continue;       // Toto vazeni nechce ulozit
                }
                selectedWeighingList.Add(weighingList.List[i]);
            }
            
            new Export(this).ExportWeighingList(selectedWeighingList, Flag.ALL, true);
        }

        private void buttonExportAll_Click(object sender, EventArgs e) {
            if (weighingList.List.Count == 0) {
                return;     // Seznam je prazdny
            }

            new Export(this).ExportWeighingList(weighingList.List, Flag.ALL, true);
        }

    }
}
