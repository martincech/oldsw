﻿using System;
using System.Collections.Generic;
using System.Text;
using ZedGraph;
using System.IO;
using System.Drawing;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Veit.Scale;

namespace Bat1 {
    public class PrintWeighing {
        public bool IsFlock;
        public bool IsTotal;

        // Data pro vsechny pripady:
        public StatisticResult StatisticResult;
        public ZedGraphControl Graph;
        public Flag Flag;
        public Units Units;

        // Data pouze pro vazeni souboru (ne pro TOTAL)
        public string FileName;
        public DateTime From;
        public DateTime To;

        // Data pouze pro hejna
        public int Day;
        public float Target;
        public float Difference;
        public int CountAbove;
        public int CountBelow;
        public float Gain;

        private Graphics graphics;
        private Font font;

        public PrintWeighing(Graphics graphics) {
            this.graphics = graphics;
        }

        public void Print() {
            font = new Font("Arial", 10);
            
            PrintDocument printDocument = new PrintDocument();
            printDocument.PrinterSettings = Program.PrinterSettings;    // Preberu globalni parametry tiskarny

            printDocument.DefaultPageSettings.Landscape = false;        // Tisknu na vysku
            
            printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(PrintPage);
        
            // Preview
/*            PrintPreviewDialog ppvw;
            ppvw = new PrintPreviewDialog();
            ppvw.Document = printDocument;
            if (ppvw.ShowDialog() != DialogResult.OK) {
                return;
            }*/

            PrintDialog printDialog = new PrintDialog();
		    printDialog.Document = printDocument;
		    if (printDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            printDocument.Print();
        }

        private void PrintText(Graphics graphics, float x, float y, string text) {
            graphics.DrawString(text, font, Brushes.Black, x, y, new StringFormat());
        }

        private string RoundWeight(float weight) {
            return DisplayFormat.RoundWeight(weight, Units) + " " + ConvertWeight.UnitsToString(Units);
        }

        private void PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e) {
            float leftMargin = e.MarginBounds.Left;
            float topMargin  = e.MarginBounds.Top;
            float lineHeight = 1.2F * font.GetHeight(e.Graphics);        // Vyska radku textu
            float y = topMargin;        // Souradnice Y, zacinam na hornim okraji
            float x1 = leftMargin;
            float x2 = leftMargin + e.MarginBounds.Width / 2.0F;

            try {
                // Soubor + pripadny flag
                PrintText(e.Graphics, x1, y, Properties.Resources.FILE + ": " + FileName);
                if (Flag != Flag.ALL) {
                    PrintText(e.Graphics, x2, y, Properties.Resources.SEX_LIMIT + ": " + Sample.FlagToString(Flag));
                }
                y += 1.5F * lineHeight;

                // From-To
                if (!IsTotal) {
                    PrintText(e.Graphics, x1, y, Properties.Resources.FROM + ": " + From.ToString("G"));
                    PrintText(e.Graphics, x2, y, Properties.Resources.TO   + ": " + To.ToString("G"));
                    y += lineHeight;
                }

                // Cislo dne, cilova hmotnost, prirustek a rozdil u hejna
                if (IsFlock) {
                    y += 0.5F * lineHeight;
                    PrintText(e.Graphics, x1, y, Properties.Resources.DAY        + ": " + Day);
                    PrintText(e.Graphics, x2, y, Properties.Resources.GAIN       + ": " + RoundWeight(Gain));       // Zaokrouhluju
                    y += lineHeight;
                    PrintText(e.Graphics, x1, y, Properties.Resources.TARGET     + ": " + RoundWeight(Target));     // Zaokrouhluju
                    PrintText(e.Graphics, x2, y, Properties.Resources.ABOVE      + ": " + CountAbove.ToString("N0"));
                    y += lineHeight;
                    PrintText(e.Graphics, x1, y, Properties.Resources.DIFFERENCE + ": " + RoundWeight(Difference));
                    PrintText(e.Graphics, x2, y, Properties.Resources.BELOW      + ": " + CountBelow.ToString("N0"));
                    y += lineHeight;
                }

                y += 0.5F * lineHeight;

                // Pocet + prumer
                PrintText(e.Graphics, x1, y, Properties.Resources.COUNT   + ": " + StatisticResult.Count.ToString("N0"));
                PrintText(e.Graphics, x2, y, Properties.Resources.AVERAGE + ": " + RoundWeight(StatisticResult.Average));
                y += lineHeight;

                // Sigma + CV
                PrintText(e.Graphics, x1, y, Properties.Resources.ST_DEVIATION + ": " + RoundWeight(StatisticResult.Sigma));
                PrintText(e.Graphics, x2, y, Properties.Resources.CV           + ": " + DisplayFormat.Cv(StatisticResult.Cv) + " %");
                y += lineHeight;
                
                // Uniformita + rychlost
                PrintText(e.Graphics, x1, y, Properties.Resources.UNIFORMITY + ": " + DisplayFormat.Cv(StatisticResult.Uniformity) + " %");
                PrintText(e.Graphics, x2, y, Properties.Resources.SPEED      + ": " + StatisticResult.WeighingSpeed.ToString("N0") + " " + Properties.Resources.PER_HOUR);
                y += lineHeight;
                
                // Prazdny radek
                y += lineHeight;

                // Zapamatuju si puvodni rozmery a umisteni
                RectangleF originalRect = Graph.MasterPane.Rect;
			    
                // Vykreslim graf
                Graph.MasterPane.ReSize(e.Graphics, new RectangleF(leftMargin, y,
				                        e.MarginBounds.Width, e.MarginBounds.Height / 2 - (y - topMargin) - lineHeight));
			    Graph.MasterPane.Draw(e.Graphics);

			    // Vratim rozmery
                Graph.MasterPane.Rect = originalRect;
    		    Graph.MasterPane.ReSize(graphics, originalRect);

            } catch (Exception exception) {
                MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
	        }
        }



    }
}
