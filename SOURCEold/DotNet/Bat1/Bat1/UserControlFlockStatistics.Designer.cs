﻿namespace Bat1 {
    partial class UserControlFlockStatistics {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlFlockStatistics));
            this.tabControlCurrent = new System.Windows.Forms.TabControl();
            this.tabPageStatistics = new System.Windows.Forms.TabPage();
            this.userControlStatisticsTable = new Bat1.UserControlStatisticsTable();
            this.tabPageGraphs = new System.Windows.Forms.TabPage();
            this.userControlGraphs = new Bat1.UserControlGraphs();
            this.flowLayoutPanelBottom = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonExportGraph = new System.Windows.Forms.Button();
            this.buttonPrintGraph = new System.Windows.Forms.Button();
            this.userControlStatisticsTop = new Bat1.UserControlStatisticsTop();
            this.tabControlCurrent.SuspendLayout();
            this.tabPageStatistics.SuspendLayout();
            this.tabPageGraphs.SuspendLayout();
            this.flowLayoutPanelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlCurrent
            // 
            this.tabControlCurrent.AccessibleDescription = null;
            this.tabControlCurrent.AccessibleName = null;
            resources.ApplyResources(this.tabControlCurrent, "tabControlCurrent");
            this.tabControlCurrent.BackgroundImage = null;
            this.tabControlCurrent.Controls.Add(this.tabPageStatistics);
            this.tabControlCurrent.Controls.Add(this.tabPageGraphs);
            this.tabControlCurrent.Font = null;
            this.tabControlCurrent.Name = "tabControlCurrent";
            this.tabControlCurrent.SelectedIndex = 0;
            // 
            // tabPageStatistics
            // 
            this.tabPageStatistics.AccessibleDescription = null;
            this.tabPageStatistics.AccessibleName = null;
            resources.ApplyResources(this.tabPageStatistics, "tabPageStatistics");
            this.tabPageStatistics.BackgroundImage = null;
            this.tabPageStatistics.Controls.Add(this.userControlStatisticsTable);
            this.tabPageStatistics.Font = null;
            this.tabPageStatistics.Name = "tabPageStatistics";
            this.tabPageStatistics.UseVisualStyleBackColor = true;
            // 
            // userControlStatisticsTable
            // 
            this.userControlStatisticsTable.AccessibleDescription = null;
            this.userControlStatisticsTable.AccessibleName = null;
            resources.ApplyResources(this.userControlStatisticsTable, "userControlStatisticsTable");
            this.userControlStatisticsTable.BackgroundImage = null;
            this.userControlStatisticsTable.Font = null;
            this.userControlStatisticsTable.Name = "userControlStatisticsTable";
            // 
            // tabPageGraphs
            // 
            this.tabPageGraphs.AccessibleDescription = null;
            this.tabPageGraphs.AccessibleName = null;
            resources.ApplyResources(this.tabPageGraphs, "tabPageGraphs");
            this.tabPageGraphs.BackgroundImage = null;
            this.tabPageGraphs.Controls.Add(this.userControlGraphs);
            this.tabPageGraphs.Controls.Add(this.flowLayoutPanelBottom);
            this.tabPageGraphs.Font = null;
            this.tabPageGraphs.Name = "tabPageGraphs";
            this.tabPageGraphs.UseVisualStyleBackColor = true;
            // 
            // userControlGraphs
            // 
            this.userControlGraphs.AccessibleDescription = null;
            this.userControlGraphs.AccessibleName = null;
            resources.ApplyResources(this.userControlGraphs, "userControlGraphs");
            this.userControlGraphs.BackgroundImage = null;
            this.userControlGraphs.Font = null;
            this.userControlGraphs.Name = "userControlGraphs";
            // 
            // flowLayoutPanelBottom
            // 
            this.flowLayoutPanelBottom.AccessibleDescription = null;
            this.flowLayoutPanelBottom.AccessibleName = null;
            resources.ApplyResources(this.flowLayoutPanelBottom, "flowLayoutPanelBottom");
            this.flowLayoutPanelBottom.BackgroundImage = null;
            this.flowLayoutPanelBottom.Controls.Add(this.buttonExportGraph);
            this.flowLayoutPanelBottom.Controls.Add(this.buttonPrintGraph);
            this.flowLayoutPanelBottom.Font = null;
            this.flowLayoutPanelBottom.Name = "flowLayoutPanelBottom";
            // 
            // buttonExportGraph
            // 
            this.buttonExportGraph.AccessibleDescription = null;
            this.buttonExportGraph.AccessibleName = null;
            resources.ApplyResources(this.buttonExportGraph, "buttonExportGraph");
            this.buttonExportGraph.BackgroundImage = null;
            this.buttonExportGraph.Font = null;
            this.buttonExportGraph.Name = "buttonExportGraph";
            this.buttonExportGraph.UseVisualStyleBackColor = true;
            this.buttonExportGraph.Click += new System.EventHandler(this.buttonExportGraph_Click);
            // 
            // buttonPrintGraph
            // 
            this.buttonPrintGraph.AccessibleDescription = null;
            this.buttonPrintGraph.AccessibleName = null;
            resources.ApplyResources(this.buttonPrintGraph, "buttonPrintGraph");
            this.buttonPrintGraph.BackgroundImage = null;
            this.buttonPrintGraph.Font = null;
            this.buttonPrintGraph.Name = "buttonPrintGraph";
            this.buttonPrintGraph.UseVisualStyleBackColor = true;
            this.buttonPrintGraph.Click += new System.EventHandler(this.buttonPrintGraph_Click);
            // 
            // userControlStatisticsTop
            // 
            this.userControlStatisticsTop.AccessibleDescription = null;
            this.userControlStatisticsTop.AccessibleName = null;
            resources.ApplyResources(this.userControlStatisticsTop, "userControlStatisticsTop");
            this.userControlStatisticsTop.BackgroundImage = null;
            this.userControlStatisticsTop.Font = null;
            this.userControlStatisticsTop.Name = "userControlStatisticsTop";
            // 
            // UserControlFlockStatistics
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tabControlCurrent);
            this.Controls.Add(this.userControlStatisticsTop);
            this.Font = null;
            this.Name = "UserControlFlockStatistics";
            this.tabControlCurrent.ResumeLayout(false);
            this.tabPageStatistics.ResumeLayout(false);
            this.tabPageGraphs.ResumeLayout(false);
            this.tabPageGraphs.PerformLayout();
            this.flowLayoutPanelBottom.ResumeLayout(false);
            this.flowLayoutPanelBottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private UserControlStatisticsTop userControlStatisticsTop;
        private System.Windows.Forms.TabControl tabControlCurrent;
        private System.Windows.Forms.TabPage tabPageStatistics;
        private UserControlStatisticsTable userControlStatisticsTable;
        private System.Windows.Forms.TabPage tabPageGraphs;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBottom;
        private System.Windows.Forms.Button buttonExportGraph;
        private System.Windows.Forms.Button buttonPrintGraph;
        private UserControlGraphs userControlGraphs;

    }
}
