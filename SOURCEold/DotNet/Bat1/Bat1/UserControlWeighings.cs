﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlWeighings : UserControl {
        /// <summary>
        /// Filtering of available weighings
        /// </summary>
        private WeighingFilter weighingFilter;

        /// <summary>
        /// Selected weighing
        /// </summary>
        public Weighing SelectedWeighing { get { return selectedWeighing; } }
        private Weighing selectedWeighing;

        private bool selectOneWeighing;

        public UserControlWeighings(bool selectOneWeighing) {
            InitializeComponent();

            buttonAdd.Visible            = !selectOneWeighing;
            buttonEdit.Visible           = !selectOneWeighing;
            buttonDelete.Visible         = !selectOneWeighing;
            buttonImport.Visible         = !selectOneWeighing;
            buttonExportAll.Visible      = !selectOneWeighing;
            buttonExportSelected.Visible = !selectOneWeighing;
            buttonOk.Visible             = selectOneWeighing;
            buttonCancel.Visible         = selectOneWeighing;
            dataGridViewWeighings.MultiSelect = !selectOneWeighing;

            this.selectOneWeighing = selectOneWeighing;
        }

        private void SelectLastRow() {
            if (dataGridViewWeighings.Rows.Count <= 1) {
                return;     // Bud zadny nebo 1 radek, nedelam nic
            }
            int index = dataGridViewWeighings.Rows.Count - 1;
            dataGridViewWeighings.FirstDisplayedScrollingRowIndex = index;
            dataGridViewWeighings.Refresh();      // Prekreslim s vybranym radkem na prvni pozici
            dataGridViewWeighings.CurrentCell = dataGridViewWeighings.Rows[index].Cells[0];
            dataGridViewWeighings.Rows[index].Selected = true;
        }

        /// <summary>
        /// Available list changed event handler
        /// </summary>
        /// <param name="sender"></param>
        private void AvailableChangedEventHandler(object sender) {
            // Pocet vazeni v seznamu
            labelAvailableWeighings.Text = string.Format(Properties.Resources.SELECT_WEIGHINGS_AVAILABLE + " ({0:N0}):", dataGridViewWeighings.Rows.Count);

            // Prejdu na posledni (nejnovejsi) zaznam
            SelectLastRow();
        }

        /// <summary>
        /// Return currently selected weighing
        /// </summary>
        /// <returns>Weighing</returns>
        private Weighing GetFirstSelectedWeighing() {
            if (dataGridViewWeighings.Rows.Count == 0) {
                return null;        // Tabulka je prazdna
            }

            if (dataGridViewWeighings.SelectedRows.Count == 0) {
                return null;
            }
            
            return Program.Database.LoadWeighing(weighingFilter.WeighingInfoFilter.FilteredInfoList[dataGridViewWeighings.SelectedRows[0].Index].id);
        }

        private void Edit() {
            Weighing weighing = GetFirstSelectedWeighing();
            if (weighing == null) {
                return;         // Tabulka je prazdna
            }

            if (weighing.WeighingData.ResultType == ResultType.MANUAL) {
                // Rucni vazeni
                FormManualResults formManual = new FormManualResults(weighing);
                formManual.ShowDialog();
            } else {
                // Vazeni z vahy

                // Predani 12tis vzorku trva, dam presypaci hodiny
                FormEditWeighing form;
                Cursor.Current = Cursors.WaitCursor;
                try {
                    // Nahraju z databaze seznam souboru
                    NameNoteUniqueList fileList = new NameNoteUniqueList();
                    foreach (NameNote file in Program.Database.LoadFileNameNoteList()) {
                        fileList.Add(file);
                    }

                    // Predam data oknu
                    form = new FormEditWeighing(weighing.WeighingData, fileList, weighing.WeighingData.ScaleConfig, false, true);
                } finally {
                    Cursor.Current = Cursors.Default;
                }
                if (form.ShowDialog() != DialogResult.OK) {
                    return;
                }

                // Updatuju vazeni. Teoreticky tak muze vzniknout duplicitni vazeni, ale to neresim.
                Program.Database.UpdateWeighing(weighing.WeighingData.Id,
                                                weighing.WeighingData.File.Name,
                                                weighing.WeighingData.File.Note,
                                                weighing.WeighingData.Note,
                                                weighing.WeighingData.SampleList,
                                                weighing.WeighingData.ScaleConfig);
            }

            // Ulozim si vybrany radek
            int selectedRow = dataGridViewWeighings.SelectedRows[0].Index;

            // Zapamatuju si prave vybrany soubor a vahu. U rucne zadaneho vazeni muze editovat i jmeno vahy,
            // takze musim oboje. Datumy nemusim, ty zustanou.
            string selectedFileName = "";
            if (comboBoxFile.SelectedIndex > 0) {
                selectedFileName = comboBoxFile.Text;
            }
            string selectedScaleName = "";
            if (comboBoxScale.SelectedIndex > 0) {
                selectedScaleName = comboBoxScale.Text;
            }
            
            // Nactu novy seznam souboru a vah do comboboxu (stacil by jen seznam souboru)
            weighingFilter.LoadWeighings();

            // Obnovim puvodne vybrany soubor a vahu
            int fileIndex = comboBoxFile.Items.IndexOf(selectedFileName);
            if (fileIndex < 0) {
                comboBoxFile.SelectedIndex = 0;     // Vyberu all
            } else {
                comboBoxFile.SelectedIndex = fileIndex;
            }
            int scaleIndex = comboBoxScale.Items.IndexOf(selectedScaleName);
            if (scaleIndex < 0) {
                comboBoxScale.SelectedIndex = 0;    // Vyberu all
            } else {
                comboBoxScale.SelectedIndex = scaleIndex;
            }

            // Nastavim filtr souboru a vahy, datumy zustavaji beze zmeny
            weighingFilter.WeighingInfoFilter.FileFilter  = comboBoxFile.SelectedIndex == 0  ? null : comboBoxFile.Text;
            weighingFilter.WeighingInfoFilter.ScaleFilter = comboBoxScale.SelectedIndex == 0 ? null : comboBoxScale.Text;

            // Prekreslim seznam vazeni
            weighingFilter.ShowAvailableWeighings();

            // Vratim se zpet na puvodni radek - musim osetrit i pripad, kdy je v seznamu moc vazeni a je odscrolovany dolu
            if (selectedRow < dataGridViewWeighings.Rows.Count) {
                dataGridViewWeighings.FirstDisplayedScrollingRowIndex = dataGridViewWeighings.Rows[selectedRow].Index;
                dataGridViewWeighings.Refresh();      // Prekreslim s vybranym radkem na prvni pozici
                dataGridViewWeighings.CurrentCell = dataGridViewWeighings.Rows[selectedRow].Cells[0];
                dataGridViewWeighings.Rows[selectedRow].Selected = true;
            }
        }

        /// <summary>
        /// Reload weighing list from the database and try to keep currently selected filter.
        /// Call after a weighing has been deleted or imported.
        /// </summary>
        public void ReloadWeighingList() {
            // Zapamatuju si aktualni nastaveny filtr
            DateTime fromDateTime = dateTimePickerFrom.Value;
            bool fromChecked      = dateTimePickerFrom.Checked;
            DateTime toDateTime   = dateTimePickerTo.Value;
            bool toChecked        = dateTimePickerTo.Checked;
            string selectedFile   = comboBoxFile.Text;
            string selectedScale  = comboBoxScale.Text;

            // Nahraju vazeni z databaze a naplnim seznamy. Controly filtru se nastavi na default hodnoty.
            weighingFilter.Load();

            weighingFilter.BeginUpdate();

            // Pokud v seznamu souboru zustal puvodne vybrany soubor, vyberu ho. Pokud se vybrany soubor smazal,
            // vyberu vsechny soubory. Zaroven nastavim i filtr.
            if (weighingFilter.WeighingInfoFilter.FilesList.Contains(selectedFile)) {
                comboBoxFile.Text                            = selectedFile;
                weighingFilter.WeighingInfoFilter.FileFilter = selectedFile;
            } else {
                comboBoxFile.SelectedIndex                   = 0;
                weighingFilter.WeighingInfoFilter.FileFilter = null;
            }

            // Stejne tak vahu
            if (weighingFilter.WeighingInfoFilter.ScalesList.Contains(selectedScale)) {
                comboBoxScale.Text                            = selectedScale;
                weighingFilter.WeighingInfoFilter.ScaleFilter = selectedScale;
            } else {
                comboBoxScale.SelectedIndex                   = 0;
                weighingFilter.WeighingInfoFilter.ScaleFilter = null;
            }

            // Obnovim casovy filtr
            dateTimePickerFrom.Value   = fromDateTime;
            dateTimePickerFrom.Checked = false;     // Musim rucne, aby se vyvolal event. Jinak checkbox blbne.
            dateTimePickerFrom.Checked = true;
            dateTimePickerFrom.Checked = fromChecked;
            dateTimePickerTo.Value     = toDateTime;
            dateTimePickerTo.Checked   = false;
            dateTimePickerTo.Checked   = true;
            dateTimePickerTo.Checked   = toChecked;

            weighingFilter.EndUpdate();

            // Zobrazim seznam vazeni
            weighingFilter.ShowAvailableWeighings();
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            if (dataGridViewWeighings.Rows.Count == 0) {
                return;
            }

            if (dataGridViewWeighings.SelectedRows.Count == 0) {
                return;
            }
            
            if (MessageBox.Show(Properties.Resources.DELETE_WEIGHINGS, buttonDelete.Text, MessageBoxButtons.YesNo) == DialogResult.No) {
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            try {
                // Smazu vsechny vybrane polozky z databaze
                List<long> weighingIdList = new List<long>();
                foreach (DataGridViewRow row in dataGridViewWeighings.SelectedRows) {
                    weighingIdList.Add(weighingFilter.WeighingInfoFilter.FilteredInfoList[row.Index].id);
                }
                Program.Database.DeleteWeighings(weighingIdList);

                // Nactu znovu vsechna vazeni
                ReloadWeighingList();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonEdit_Click(object sender, EventArgs e) {
            Edit();
        }

        private void dataGridViewWeighings_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (selectOneWeighing) {
                // Pouze vybira jedno vazeni bez moznosti jakekoliv editace, doubleclick = OK
                buttonOk_Click(this, null);
                return;
            }
            if (e.RowIndex < 0) {
                return;     // Header
            }
            Edit();
        }

        private void FormWeighingManager_Shown(object sender, EventArgs e) {
            // Nastavim focus na tabulku
            dataGridViewWeighings.Focus();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            if (dataGridViewWeighings.SelectedRows.Count == 0) {
                return;     // Nema vybrane zadne vazeni
            }

            // Nactu prvni vybrane vazeni
            selectedWeighing = Program.Database.LoadWeighing(weighingFilter.WeighingInfoFilter.FilteredInfoList[dataGridViewWeighings.SelectedRows[0].Index].id);

            ParentForm.DialogResult = DialogResult.OK;
        }

        private void buttonImport_Click(object sender, EventArgs e) {
            // Zeptam se na jmeno souboru
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = FileExtension.EXPORT_BAT1;      // Importuju pouze format BAT1
            openFileDialog.Filter     = Export.FilterBat1;
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            Refresh();
            
            // Nactu seznam vazeni, ktere soubor exportu obsahuje
            Cursor.Current = Cursors.WaitCursor;
            List<Weighing> weighingList;
            try {
                ExportBat1 exportBat1 = new ExportBat1(openFileDialog.FileName);
                weighingList = exportBat1.ImportWeighingList();
                if (weighingList == null) {
                    return;     // Neplatny soubor exportu
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }

            // Zeptam se uzivatele, ktera vazeni chce importovat
            FormImportWeighings form = new FormImportWeighings(weighingList);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            Refresh();

            // Sestavim seznam vazeni, ktere chce ulozit
            List<Weighing> saveWeighingList = new List<Weighing>();
            foreach (Weighing weighing in form.SelectedWeighingList) {
                // Zkontroluju, zda uz vazeni v databazi neexistuje (kombinace soubor - start vazeni)
                DialogResult dialogResult = UserControlResultsBase.CanSaveWeighing(weighing.WeighingData.File.Name, weighing.GetMinDateTime());
                if (dialogResult == DialogResult.No) {
                    continue;       // Vazeni uz existuje, nechce ho prepsat, ale chce pokracovat v ukladani
                } else if (dialogResult == DialogResult.Cancel) {
                    return;         // Vazeni uz existuje, chce ukoncit cele ukladani
                }

                // Vazeni je mozne ulozit, zmenim zdroj na IMPORT
                weighing.WeighingData.RecordSource = RecordSource.IMPORT;
                
                // Pridam ho do seznamu
                saveWeighingList.Add(weighing);
            }

            // Pokud je seznam vazeni pro ulozeni prazdny, koncim
            if (saveWeighingList.Count == 0) {
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            try {
                // Ulozim seznam vazeni do DB (kazde vazeni s vlastnim configem)
                foreach (Weighing weighing in saveWeighingList) {
                    Program.Database.SaveWeighing(weighing);
                }

                // Zobrazim info, ze se ulozilo
                Cursor.Current = Cursors.Default;
                MessageBox.Show(Properties.Resources.WEIGHINGS_SAVED, Program.ApplicationName);

                // Nactu znovu vsechna vazeni
                ReloadWeighingList();

            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e) {
            new FormManualResults().ShowDialog();

            // Nactu znovu vsechna vazeni
            ReloadWeighingList();
        }

        private void UserControlWeighings_Load(object sender, EventArgs e) {
            // Nahraju a zobrazim vazeni
            Cursor.Current = Cursors.WaitCursor;
            try {
                // Dostupna vazeni nahraju z DB  zobrazim
                weighingFilter = new WeighingFilter(this, dateTimePickerFrom, dateTimePickerTo, comboBoxFile, comboBoxScale, dataGridViewWeighings);
                weighingFilter.ShowSeconds = true;      // Cas vcetne sekund

                // Zaregistruju event handler po zmene seznamu dostupnych vazeni
                weighingFilter.ChangedEvent += AvailableChangedEventHandler;

                // Nahraju seznam (musim az pote, co mam zaregistrovan event)
                weighingFilter.Load();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonExportAll_Click(object sender, EventArgs e) {
            if (dataGridViewWeighings.Rows.Count == 0) {
                return;        // Tabulka je prazdna
            }

            // Nactu seznam vsech vazeni v tabulce - muze to trvat
            Cursor.Current = Cursors.WaitCursor;
            List<Weighing> weighingList = new List<Weighing>();
            try {
                foreach (WeighingSearchInfo info in weighingFilter.WeighingInfoFilter.FilteredInfoList) {
                    weighingList.Add(Program.Database.LoadWeighing(info.id));
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }

            // Exportuju
            new Export(this).ExportWeighingList(weighingList, Flag.ALL, true);
        }

        private void buttonExportSelected_Click(object sender, EventArgs e) {
            if (dataGridViewWeighings.SelectedRows.Count == 0) {
                return;     // Nema vybrane zadne vazeni
            }

            // Nactu seznam vybranych vazeni - muze to trvat
            Cursor.Current = Cursors.WaitCursor;
            List<Weighing> weighingList = new List<Weighing>();
            try {
                foreach (DataGridViewRow row in dataGridViewWeighings.SelectedRows) {
                    weighingList.Add(Program.Database.LoadWeighing(weighingFilter.WeighingInfoFilter.FilteredInfoList[row.Index].id));
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }

            // Exportuju
            new Export(this).ExportWeighingList(weighingList, Flag.ALL, true);
        }
    }
}
