﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    /// <summary>
    /// Weight conversions (kg-g-lb)
    /// </summary>
    public static class ConvertWeight {
        
        /// <summary>
        /// Convert weight from one units to another
        /// </summary>
        /// <param name="weight">Weight in old units</param>
        /// <param name="oldUnits">Old units</param>
        /// <param name="newUnits">New units</param>
        /// <returns>Weight converted to new units</returns>
        public static double Convert(double weight, Units oldUnits, Units newUnits) {
            switch (oldUnits) {
                case Units.G:
                    switch (newUnits) {
                        case Units.KG:
                            return weight / 1000.0;

                        case Units.LB:
                            return weight / 453.59237;
                    }
                    break;
                
                case Units.KG:
                    switch (newUnits) {
                        case Units.G:
                            return weight * 1000.0;

                        case Units.LB:
                            return weight / 0.45359237;
                    }
                    break;

                case Units.LB:
                    switch (newUnits) {
                        case Units.KG:
                            return weight * 0.45359237;

                        case Units.G:
                            return weight * 453.59237;
                    }
                    break;
            }
            return weight;      // Bez prevodu
        }

        /// <summary>
        /// Convert integer value sent from scale to weight in specified units
        /// </summary>
        /// <param name="value">Integer value</param>
        /// <param name="units">Units</param>
        /// <returns></returns>
        public static double IntToWeight(int value, Units units) {
            // Vaha posila hodnotu ve zvolenych jednotkach, ale bez desetinne carky
            if (units == Units.G) {
                return (double)value;
            }
            return (double)value / 1000.0;
        }

        /// <summary>
        /// Convert weight in specified units to integer value sent to the scale
        /// </summary>
        /// <param name="weight">Weight</param>
        /// <param name="units">Units</param>
        /// <returns></returns>
        public static int WeightToInt(double weight, Units units) {
            // Vaha posila hodnotu ve zvolenych jednotkach, ale bez desetinne carky.
            // Radeji delam i zaokrouhleni, v SW je mozne delat prepocty jednotek, tj. aby to 0,999999 ulozilo jako 1.000kg
            if (units != Units.G) {
                weight *= 1000.0;       // Kg a Lb posilam jako tisicinasobek
            }
            return (int)Math.Round(weight, 0);
        }

        /// <summary>
        /// Get minimum non-zero weight in specified units
        /// </summary>
        /// <param name="units">Units</param>
        /// <returns>Weight</returns>
        public static double MinWeight(Units units) {
            switch (units) {
                case Units.G:
                    return 1;
                
                case Units.KG:
                    return 0.001;

                default: // Units.LB
                    return 0.001;
            }
        }

        /// <summary>
        /// Get maximum weight in specified units and weighing capacity
        /// </summary>
        /// <param name="units">Units</param>
        /// <param name="weighingCapacity">Weighing capacity 30 or 50kg</param>
        /// <returns>Weight</returns>
        public static double MaxWeight(Units units, WeighingCapacity weighingCapacity) {
            switch (units) {
                case Units.G:
                    if (weighingCapacity == WeighingCapacity.NORMAL) {
                        return (double)Const.UNITS_G_RANGE;
                    } else {
                        return (double)Const.UNITS_G_EXT_RANGE;
                    }
                
                case Units.KG:
                    if (weighingCapacity == WeighingCapacity.NORMAL) {
                        return (double)Const.UNITS_KG_RANGE / 1000.0;
                    } else {
                        return (double)Const.UNITS_KG_EXT_RANGE / 1000.0;
                    }

                default: // Units.LB
                    if (weighingCapacity == WeighingCapacity.NORMAL) {
                        return (double)Const.UNITS_LB_RANGE / 1000.0;
                    } else {
                        return (double)Const.UNITS_LB_EXT_RANGE / 1000.0;
                    }
            }
        }

        /// <summary>
        /// Convert units to string
        /// </summary>
        /// <param name="units">Units to convers</param>
        /// <returns>String representation</returns>
        public static string UnitsToString(Units units) {
            switch (units) {
                case Units.G:
                    return Properties.Resources.G;
                
                case Units.KG:
                    return Properties.Resources.KG;

                default: // Units.LB
                    return Properties.Resources.LB;
            }
        }

        /// <summary>
        /// Move decimal point of weight
        /// </summary>
        /// <param name="weight">Original weight</param>
        /// <param name="oldUnits">Original units</param>
        /// <param name="newUnits">New units</param>
        /// <returns>New weight</returns>
        public static float MoveDecimalPoint(float weight, Units oldUnits, Units newUnits) {
            if (oldUnits == Units.G) {
                if (newUnits == Units.KG || newUnits == Units.LB) {
                    // Z gramu na kila nebo libry
                    return weight / 1000.0F;
                }
            } else {
                if (newUnits == Units.G) {
                    // Z kil nebo liber na gramy
                    return weight * 1000.0F;
                }
            }
            
            // Beze zmeny
            return weight;
        }

    }
}
