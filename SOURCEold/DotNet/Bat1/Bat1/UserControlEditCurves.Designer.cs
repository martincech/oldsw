﻿namespace Bat1 {
    partial class UserControlEditCurves {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlEditCurves));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxDefinition = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.userControlCurve = new Bat1.UserControlCurve();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonSave = new System.Windows.Forms.Button();
            this.groupBoxCurves = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listBoxCurves = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonRename = new System.Windows.Forms.Button();
            this.buttonCopy = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxDefinition.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.groupBoxCurves.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AccessibleDescription = null;
            this.tableLayoutPanel1.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.BackgroundImage = null;
            this.tableLayoutPanel1.Controls.Add(this.groupBoxDefinition, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxCurves, 0, 0);
            this.tableLayoutPanel1.Font = null;
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.toolTip1.SetToolTip(this.tableLayoutPanel1, resources.GetString("tableLayoutPanel1.ToolTip"));
            // 
            // groupBoxDefinition
            // 
            this.groupBoxDefinition.AccessibleDescription = null;
            this.groupBoxDefinition.AccessibleName = null;
            resources.ApplyResources(this.groupBoxDefinition, "groupBoxDefinition");
            this.groupBoxDefinition.BackgroundImage = null;
            this.groupBoxDefinition.Controls.Add(this.panel2);
            this.groupBoxDefinition.Controls.Add(this.flowLayoutPanel2);
            this.groupBoxDefinition.Font = null;
            this.groupBoxDefinition.Name = "groupBoxDefinition";
            this.groupBoxDefinition.TabStop = false;
            this.toolTip1.SetToolTip(this.groupBoxDefinition, resources.GetString("groupBoxDefinition.ToolTip"));
            // 
            // panel2
            // 
            this.panel2.AccessibleDescription = null;
            this.panel2.AccessibleName = null;
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.BackgroundImage = null;
            this.panel2.Controls.Add(this.userControlCurve);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Font = null;
            this.panel2.Name = "panel2";
            this.toolTip1.SetToolTip(this.panel2, resources.GetString("panel2.ToolTip"));
            // 
            // userControlCurve
            // 
            this.userControlCurve.AccessibleDescription = null;
            this.userControlCurve.AccessibleName = null;
            resources.ApplyResources(this.userControlCurve, "userControlCurve");
            this.userControlCurve.BackgroundImage = null;
            this.userControlCurve.Font = null;
            this.userControlCurve.Name = "userControlCurve";
            this.toolTip1.SetToolTip(this.userControlCurve, resources.GetString("userControlCurve.ToolTip"));
            // 
            // panel5
            // 
            this.panel5.AccessibleDescription = null;
            this.panel5.AccessibleName = null;
            resources.ApplyResources(this.panel5, "panel5");
            this.panel5.BackgroundImage = null;
            this.panel5.Controls.Add(this.textBoxNote);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Font = null;
            this.panel5.Name = "panel5";
            this.toolTip1.SetToolTip(this.panel5, resources.GetString("panel5.ToolTip"));
            // 
            // textBoxNote
            // 
            this.textBoxNote.AccessibleDescription = null;
            this.textBoxNote.AccessibleName = null;
            resources.ApplyResources(this.textBoxNote, "textBoxNote");
            this.textBoxNote.BackgroundImage = null;
            this.textBoxNote.Font = null;
            this.textBoxNote.Name = "textBoxNote";
            this.toolTip1.SetToolTip(this.textBoxNote, resources.GetString("textBoxNote.ToolTip"));
            // 
            // label4
            // 
            this.label4.AccessibleDescription = null;
            this.label4.AccessibleName = null;
            resources.ApplyResources(this.label4, "label4");
            this.label4.Font = null;
            this.label4.Name = "label4";
            this.toolTip1.SetToolTip(this.label4, resources.GetString("label4.ToolTip"));
            // 
            // panel4
            // 
            this.panel4.AccessibleDescription = null;
            this.panel4.AccessibleName = null;
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.BackgroundImage = null;
            this.panel4.Controls.Add(this.comboBoxUnits);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Font = null;
            this.panel4.Name = "panel4";
            this.toolTip1.SetToolTip(this.panel4, resources.GetString("panel4.ToolTip"));
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.AccessibleDescription = null;
            this.comboBoxUnits.AccessibleName = null;
            resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
            this.comboBoxUnits.BackgroundImage = null;
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.toolTip1.SetToolTip(this.comboBoxUnits, resources.GetString("comboBoxUnits.ToolTip"));
            this.comboBoxUnits.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUnits_SelectionChangeCommitted);
            // 
            // label3
            // 
            this.label3.AccessibleDescription = null;
            this.label3.AccessibleName = null;
            resources.ApplyResources(this.label3, "label3");
            this.label3.Font = null;
            this.label3.Name = "label3";
            this.toolTip1.SetToolTip(this.label3, resources.GetString("label3.ToolTip"));
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Font = null;
            this.label2.Name = "label2";
            this.toolTip1.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AccessibleDescription = null;
            this.flowLayoutPanel2.AccessibleName = null;
            resources.ApplyResources(this.flowLayoutPanel2, "flowLayoutPanel2");
            this.flowLayoutPanel2.BackgroundImage = null;
            this.flowLayoutPanel2.Controls.Add(this.buttonSave);
            this.flowLayoutPanel2.Font = null;
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.toolTip1.SetToolTip(this.flowLayoutPanel2, resources.GetString("flowLayoutPanel2.ToolTip"));
            // 
            // buttonSave
            // 
            this.buttonSave.AccessibleDescription = null;
            this.buttonSave.AccessibleName = null;
            resources.ApplyResources(this.buttonSave, "buttonSave");
            this.buttonSave.BackgroundImage = null;
            this.buttonSave.Font = null;
            this.buttonSave.Name = "buttonSave";
            this.toolTip1.SetToolTip(this.buttonSave, resources.GetString("buttonSave.ToolTip"));
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // groupBoxCurves
            // 
            this.groupBoxCurves.AccessibleDescription = null;
            this.groupBoxCurves.AccessibleName = null;
            resources.ApplyResources(this.groupBoxCurves, "groupBoxCurves");
            this.groupBoxCurves.BackgroundImage = null;
            this.groupBoxCurves.Controls.Add(this.panel3);
            this.groupBoxCurves.Controls.Add(this.tableLayoutPanel2);
            this.groupBoxCurves.Controls.Add(this.panel1);
            this.groupBoxCurves.Font = null;
            this.groupBoxCurves.Name = "groupBoxCurves";
            this.groupBoxCurves.TabStop = false;
            this.toolTip1.SetToolTip(this.groupBoxCurves, resources.GetString("groupBoxCurves.ToolTip"));
            // 
            // panel3
            // 
            this.panel3.AccessibleDescription = null;
            this.panel3.AccessibleName = null;
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.BackgroundImage = null;
            this.panel3.Controls.Add(this.listBoxCurves);
            this.panel3.Font = null;
            this.panel3.Name = "panel3";
            this.toolTip1.SetToolTip(this.panel3, resources.GetString("panel3.ToolTip"));
            // 
            // listBoxCurves
            // 
            this.listBoxCurves.AccessibleDescription = null;
            this.listBoxCurves.AccessibleName = null;
            resources.ApplyResources(this.listBoxCurves, "listBoxCurves");
            this.listBoxCurves.BackgroundImage = null;
            this.listBoxCurves.Font = null;
            this.listBoxCurves.FormattingEnabled = true;
            this.listBoxCurves.Name = "listBoxCurves";
            this.toolTip1.SetToolTip(this.listBoxCurves, resources.GetString("listBoxCurves.ToolTip"));
            this.listBoxCurves.SelectedIndexChanged += new System.EventHandler(this.listBoxCurves_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.AccessibleDescription = null;
            this.tableLayoutPanel2.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.BackgroundImage = null;
            this.tableLayoutPanel2.Controls.Add(this.buttonDelete, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.buttonNew, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.buttonRename, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.buttonCopy, 2, 0);
            this.tableLayoutPanel2.Font = null;
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.toolTip1.SetToolTip(this.tableLayoutPanel2, resources.GetString("tableLayoutPanel2.ToolTip"));
            // 
            // buttonDelete
            // 
            this.buttonDelete.AccessibleDescription = null;
            this.buttonDelete.AccessibleName = null;
            resources.ApplyResources(this.buttonDelete, "buttonDelete");
            this.buttonDelete.BackgroundImage = null;
            this.buttonDelete.Font = null;
            this.buttonDelete.Name = "buttonDelete";
            this.toolTip1.SetToolTip(this.buttonDelete, resources.GetString("buttonDelete.ToolTip"));
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.AccessibleDescription = null;
            this.buttonNew.AccessibleName = null;
            resources.ApplyResources(this.buttonNew, "buttonNew");
            this.buttonNew.BackgroundImage = null;
            this.buttonNew.Font = null;
            this.buttonNew.Name = "buttonNew";
            this.toolTip1.SetToolTip(this.buttonNew, resources.GetString("buttonNew.ToolTip"));
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonRename
            // 
            this.buttonRename.AccessibleDescription = null;
            this.buttonRename.AccessibleName = null;
            resources.ApplyResources(this.buttonRename, "buttonRename");
            this.buttonRename.BackgroundImage = null;
            this.buttonRename.Font = null;
            this.buttonRename.Name = "buttonRename";
            this.toolTip1.SetToolTip(this.buttonRename, resources.GetString("buttonRename.ToolTip"));
            this.buttonRename.UseVisualStyleBackColor = true;
            this.buttonRename.Click += new System.EventHandler(this.buttonRename_Click);
            // 
            // buttonCopy
            // 
            this.buttonCopy.AccessibleDescription = null;
            this.buttonCopy.AccessibleName = null;
            resources.ApplyResources(this.buttonCopy, "buttonCopy");
            this.buttonCopy.BackgroundImage = null;
            this.buttonCopy.Font = null;
            this.buttonCopy.Name = "buttonCopy";
            this.toolTip1.SetToolTip(this.buttonCopy, resources.GetString("buttonCopy.ToolTip"));
            this.buttonCopy.UseVisualStyleBackColor = true;
            this.buttonCopy.Click += new System.EventHandler(this.buttonCopy_Click);
            // 
            // panel1
            // 
            this.panel1.AccessibleDescription = null;
            this.panel1.AccessibleName = null;
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.BackgroundImage = null;
            this.panel1.Font = null;
            this.panel1.Name = "panel1";
            this.toolTip1.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
            // 
            // UserControlEditCurves
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = null;
            this.Name = "UserControlEditCurves";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.Load += new System.EventHandler(this.UserControlEditCurves_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBoxDefinition.ResumeLayout(false);
            this.groupBoxDefinition.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.groupBoxCurves.ResumeLayout(false);
            this.groupBoxCurves.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBoxDefinition;
        private System.Windows.Forms.Panel panel2;
        private UserControlCurve userControlCurve;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.GroupBox groupBoxCurves;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ListBox listBoxCurves;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonRename;
        private System.Windows.Forms.Button buttonCopy;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
