﻿namespace Bat1 {
    partial class UserControlScaleResults {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleResults));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.customControlHistogramDataGridViewWeighings = new Bat1.CustomControlHistogramDataGridView();
            this.ColumnFile = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAverage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDivide = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ColumnUniformity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnHistogram = new System.Windows.Forms.DataGridViewImageColumn();
            this.ColumnNote = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonEdit = new System.Windows.Forms.Button();
            this.buttonExportSelected = new System.Windows.Forms.Button();
            this.buttonSaveSelected = new System.Windows.Forms.Button();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.saveFileDialogCsv = new System.Windows.Forms.SaveFileDialog();
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonExportAll = new System.Windows.Forms.Button();
            this.buttonSaveAll = new System.Windows.Forms.Button();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.labelScaleName = new System.Windows.Forms.Label();
            this.labelUnits = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.customControlHistogramDataGridViewWeighings)).BeginInit();
            this.tableLayoutPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // customControlHistogramDataGridViewWeighings
            // 
            this.customControlHistogramDataGridViewWeighings.AccessibleDescription = null;
            this.customControlHistogramDataGridViewWeighings.AccessibleName = null;
            this.customControlHistogramDataGridViewWeighings.AllowUserToAddRows = false;
            this.customControlHistogramDataGridViewWeighings.AllowUserToDeleteRows = false;
            this.customControlHistogramDataGridViewWeighings.AllowUserToResizeRows = false;
            resources.ApplyResources(this.customControlHistogramDataGridViewWeighings, "customControlHistogramDataGridViewWeighings");
            this.customControlHistogramDataGridViewWeighings.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.customControlHistogramDataGridViewWeighings.BackgroundColor = System.Drawing.SystemColors.Window;
            this.customControlHistogramDataGridViewWeighings.BackgroundImage = null;
            this.customControlHistogramDataGridViewWeighings.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.customControlHistogramDataGridViewWeighings.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.customControlHistogramDataGridViewWeighings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.customControlHistogramDataGridViewWeighings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnFile,
            this.ColumnDateTime,
            this.ColumnCount,
            this.ColumnAverage,
            this.ColumnDivide,
            this.ColumnUniformity,
            this.ColumnHistogram,
            this.ColumnNote});
            this.tableLayoutPanel.SetColumnSpan(this.customControlHistogramDataGridViewWeighings, 4);
            this.customControlHistogramDataGridViewWeighings.Font = null;
            this.customControlHistogramDataGridViewWeighings.Name = "customControlHistogramDataGridViewWeighings";
            this.customControlHistogramDataGridViewWeighings.RowHeadersVisible = false;
            this.customControlHistogramDataGridViewWeighings.RowTemplate.Height = 18;
            this.customControlHistogramDataGridViewWeighings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.toolTip1.SetToolTip(this.customControlHistogramDataGridViewWeighings, resources.GetString("customControlHistogramDataGridViewWeighings.ToolTip"));
            this.customControlHistogramDataGridViewWeighings.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.customControlHistogramDataGridViewWeighings_CellDoubleClick);
            this.customControlHistogramDataGridViewWeighings.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.customControlHistogramDataGridViewWeighings_CellClick);
            this.customControlHistogramDataGridViewWeighings.CurrentCellDirtyStateChanged += new System.EventHandler(this.customControlHistogramDataGridViewWeighings_CurrentCellDirtyStateChanged);
            // 
            // ColumnFile
            // 
            this.ColumnFile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            resources.ApplyResources(this.ColumnFile, "ColumnFile");
            this.ColumnFile.Name = "ColumnFile";
            this.ColumnFile.ReadOnly = true;
            this.ColumnFile.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDateTime
            // 
            this.ColumnDateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            resources.ApplyResources(this.ColumnDateTime, "ColumnDateTime");
            this.ColumnDateTime.Name = "ColumnDateTime";
            this.ColumnDateTime.ReadOnly = true;
            this.ColumnDateTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnCount
            // 
            this.ColumnCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnCount.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.ColumnCount, "ColumnCount");
            this.ColumnCount.Name = "ColumnCount";
            this.ColumnCount.ReadOnly = true;
            this.ColumnCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnAverage
            // 
            this.ColumnAverage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnAverage.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this.ColumnAverage, "ColumnAverage");
            this.ColumnAverage.Name = "ColumnAverage";
            this.ColumnAverage.ReadOnly = true;
            this.ColumnAverage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnDivide
            // 
            this.ColumnDivide.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            resources.ApplyResources(this.ColumnDivide, "ColumnDivide");
            this.ColumnDivide.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20"});
            this.ColumnDivide.MaxDropDownItems = 20;
            this.ColumnDivide.Name = "ColumnDivide";
            // 
            // ColumnUniformity
            // 
            this.ColumnUniformity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnUniformity.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.ColumnUniformity, "ColumnUniformity");
            this.ColumnUniformity.Name = "ColumnUniformity";
            this.ColumnUniformity.ReadOnly = true;
            this.ColumnUniformity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ColumnHistogram
            // 
            this.ColumnHistogram.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle5.NullValue")));
            this.ColumnHistogram.DefaultCellStyle = dataGridViewCellStyle5;
            resources.ApplyResources(this.ColumnHistogram, "ColumnHistogram");
            this.ColumnHistogram.Name = "ColumnHistogram";
            this.ColumnHistogram.ReadOnly = true;
            // 
            // ColumnNote
            // 
            this.ColumnNote.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            resources.ApplyResources(this.ColumnNote, "ColumnNote");
            this.ColumnNote.Name = "ColumnNote";
            this.ColumnNote.ReadOnly = true;
            this.ColumnNote.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // buttonEdit
            // 
            this.buttonEdit.AccessibleDescription = null;
            this.buttonEdit.AccessibleName = null;
            resources.ApplyResources(this.buttonEdit, "buttonEdit");
            this.buttonEdit.BackgroundImage = null;
            this.buttonEdit.FlatAppearance.BorderSize = 0;
            this.buttonEdit.Name = "buttonEdit";
            this.buttonEdit.TabStop = false;
            this.toolTip1.SetToolTip(this.buttonEdit, resources.GetString("buttonEdit.ToolTip"));
            this.buttonEdit.UseVisualStyleBackColor = true;
            this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
            // 
            // buttonExportSelected
            // 
            this.buttonExportSelected.AccessibleDescription = null;
            this.buttonExportSelected.AccessibleName = null;
            resources.ApplyResources(this.buttonExportSelected, "buttonExportSelected");
            this.buttonExportSelected.BackgroundImage = null;
            this.buttonExportSelected.FlatAppearance.BorderSize = 0;
            this.buttonExportSelected.Name = "buttonExportSelected";
            this.buttonExportSelected.TabStop = false;
            this.toolTip1.SetToolTip(this.buttonExportSelected, resources.GetString("buttonExportSelected.ToolTip"));
            this.buttonExportSelected.UseVisualStyleBackColor = true;
            this.buttonExportSelected.Click += new System.EventHandler(this.buttonExportSelected_Click);
            // 
            // buttonSaveSelected
            // 
            this.buttonSaveSelected.AccessibleDescription = null;
            this.buttonSaveSelected.AccessibleName = null;
            resources.ApplyResources(this.buttonSaveSelected, "buttonSaveSelected");
            this.buttonSaveSelected.BackgroundImage = null;
            this.buttonSaveSelected.FlatAppearance.BorderSize = 0;
            this.buttonSaveSelected.Name = "buttonSaveSelected";
            this.buttonSaveSelected.TabStop = false;
            this.toolTip1.SetToolTip(this.buttonSaveSelected, resources.GetString("buttonSaveSelected.ToolTip"));
            this.buttonSaveSelected.UseVisualStyleBackColor = true;
            this.buttonSaveSelected.Click += new System.EventHandler(this.buttonSaveSelected_Click);
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.AccessibleDescription = null;
            this.comboBoxUnits.AccessibleName = null;
            resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
            this.comboBoxUnits.BackgroundImage = null;
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.toolTip1.SetToolTip(this.comboBoxUnits, resources.GetString("comboBoxUnits.ToolTip"));
            this.comboBoxUnits.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUnits_SelectionChangeCommitted);
            // 
            // saveFileDialogCsv
            // 
            this.saveFileDialogCsv.DefaultExt = "csv";
            resources.ApplyResources(this.saveFileDialogCsv, "saveFileDialogCsv");
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.AccessibleDescription = null;
            this.tableLayoutPanel.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanel, "tableLayoutPanel");
            this.tableLayoutPanel.BackgroundImage = null;
            this.tableLayoutPanel.Controls.Add(this.customControlHistogramDataGridViewWeighings, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.flowLayoutPanel1, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.flowLayoutPanel2, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.labelScaleName, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.labelUnits, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.comboBoxUnits, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel.Font = null;
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.toolTip1.SetToolTip(this.tableLayoutPanel, resources.GetString("tableLayoutPanel.ToolTip"));
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AccessibleDescription = null;
            this.flowLayoutPanel1.AccessibleName = null;
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.BackgroundImage = null;
            this.tableLayoutPanel.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.buttonExportSelected);
            this.flowLayoutPanel1.Controls.Add(this.buttonExportAll);
            this.flowLayoutPanel1.Controls.Add(this.buttonSaveSelected);
            this.flowLayoutPanel1.Controls.Add(this.buttonSaveAll);
            this.flowLayoutPanel1.Font = null;
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.toolTip1.SetToolTip(this.flowLayoutPanel1, resources.GetString("flowLayoutPanel1.ToolTip"));
            // 
            // buttonExportAll
            // 
            this.buttonExportAll.AccessibleDescription = null;
            this.buttonExportAll.AccessibleName = null;
            resources.ApplyResources(this.buttonExportAll, "buttonExportAll");
            this.buttonExportAll.BackgroundImage = null;
            this.buttonExportAll.Font = null;
            this.buttonExportAll.Name = "buttonExportAll";
            this.toolTip1.SetToolTip(this.buttonExportAll, resources.GetString("buttonExportAll.ToolTip"));
            this.buttonExportAll.UseVisualStyleBackColor = true;
            this.buttonExportAll.Click += new System.EventHandler(this.buttonExportAll_Click);
            // 
            // buttonSaveAll
            // 
            this.buttonSaveAll.AccessibleDescription = null;
            this.buttonSaveAll.AccessibleName = null;
            resources.ApplyResources(this.buttonSaveAll, "buttonSaveAll");
            this.buttonSaveAll.BackgroundImage = null;
            this.buttonSaveAll.Font = null;
            this.buttonSaveAll.Name = "buttonSaveAll";
            this.toolTip1.SetToolTip(this.buttonSaveAll, resources.GetString("buttonSaveAll.ToolTip"));
            this.buttonSaveAll.UseVisualStyleBackColor = true;
            this.buttonSaveAll.Click += new System.EventHandler(this.buttonSaveAll_Click);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AccessibleDescription = null;
            this.flowLayoutPanel2.AccessibleName = null;
            resources.ApplyResources(this.flowLayoutPanel2, "flowLayoutPanel2");
            this.flowLayoutPanel2.BackgroundImage = null;
            this.tableLayoutPanel.SetColumnSpan(this.flowLayoutPanel2, 2);
            this.flowLayoutPanel2.Controls.Add(this.buttonEdit);
            this.flowLayoutPanel2.Font = null;
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.toolTip1.SetToolTip(this.flowLayoutPanel2, resources.GetString("flowLayoutPanel2.ToolTip"));
            // 
            // labelScaleName
            // 
            this.labelScaleName.AccessibleDescription = null;
            this.labelScaleName.AccessibleName = null;
            resources.ApplyResources(this.labelScaleName, "labelScaleName");
            this.labelScaleName.Font = null;
            this.labelScaleName.Name = "labelScaleName";
            this.toolTip1.SetToolTip(this.labelScaleName, resources.GetString("labelScaleName.ToolTip"));
            // 
            // labelUnits
            // 
            this.labelUnits.AccessibleDescription = null;
            this.labelUnits.AccessibleName = null;
            resources.ApplyResources(this.labelUnits, "labelUnits");
            this.labelUnits.Font = null;
            this.labelUnits.Name = "labelUnits";
            this.toolTip1.SetToolTip(this.labelUnits, resources.GetString("labelUnits.ToolTip"));
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            this.toolTip1.SetToolTip(this.label1, resources.GetString("label1.ToolTip"));
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.tableLayoutPanel.SetColumnSpan(this.label2, 4);
            this.label2.Font = null;
            this.label2.Name = "label2";
            this.toolTip1.SetToolTip(this.label2, resources.GetString("label2.ToolTip"));
            // 
            // UserControlScaleResults
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tableLayoutPanel);
            this.Font = null;
            this.Name = "UserControlScaleResults";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.Load += new System.EventHandler(this.UserControlScaleResults_Load);
            ((System.ComponentModel.ISupportInitialize)(this.customControlHistogramDataGridViewWeighings)).EndInit();
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CustomControlHistogramDataGridView customControlHistogramDataGridViewWeighings;
        private System.Windows.Forms.Button buttonEdit;
        private System.Windows.Forms.Button buttonExportSelected;
        private System.Windows.Forms.Button buttonSaveSelected;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.SaveFileDialog saveFileDialogCsv;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonSaveAll;
        private System.Windows.Forms.Button buttonExportAll;
        private System.Windows.Forms.Label labelScaleName;
        private System.Windows.Forms.Label labelUnits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAverage;
        private System.Windows.Forms.DataGridViewComboBoxColumn ColumnDivide;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUniformity;
        private System.Windows.Forms.DataGridViewImageColumn ColumnHistogram;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNote;

    }
}
