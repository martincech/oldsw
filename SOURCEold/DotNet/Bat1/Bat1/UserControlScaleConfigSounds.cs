﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;

namespace Bat1 {
    public partial class UserControlScaleConfigSounds : UserControlScaleConfigBase {

        public UserControlScaleConfigSounds(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Read-only
            this.readOnly = readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        public override void Redraw() {
            isLoading = true;

            try {
                comboBoxSavingDefaultTone.SelectedIndex = (int)scaleConfig.Sounds.ToneDefault;
                comboBoxSavingLightTone.SelectedIndex   = (int)scaleConfig.Sounds.ToneLight;
                comboBoxSavingOkTone.SelectedIndex      = (int)scaleConfig.Sounds.ToneOk;
                comboBoxSavingHeavyTone.SelectedIndex   = (int)scaleConfig.Sounds.ToneHeavy;
                numericUpDownSavingVolume.Text          =      scaleConfig.Sounds.VolumeSaving.ToString();
                comboBoxKeyboardTone.SelectedIndex      = (int)scaleConfig.Sounds.ToneKeyboard;
                numericUpDownKeyboardVolume.Text        =      scaleConfig.Sounds.VolumeKeyboard.ToString();
                checkBoxSpecialSounds.Checked           =      scaleConfig.Sounds.EnableSpecial;
            } finally {
                isLoading = false;
            }
        }
        
        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.Sounds.ToneDefault    = (Tone)comboBoxSavingDefaultTone.SelectedIndex;
            scaleConfig.Sounds.ToneLight      = (Tone)comboBoxSavingLightTone.SelectedIndex;
            scaleConfig.Sounds.ToneOk         = (Tone)comboBoxSavingOkTone.SelectedIndex;
            scaleConfig.Sounds.ToneHeavy      = (Tone)comboBoxSavingHeavyTone.SelectedIndex;
            scaleConfig.Sounds.VolumeSaving   = (int) numericUpDownSavingVolume.Value;
            scaleConfig.Sounds.ToneKeyboard   = (Tone)comboBoxKeyboardTone.SelectedIndex;
            scaleConfig.Sounds.VolumeKeyboard = (int) numericUpDownKeyboardVolume.Value;
            scaleConfig.Sounds.EnableSpecial  =       checkBoxSpecialSounds.Checked;
        }

        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e) {
            ControlsToConfig();
        }
    }
}
