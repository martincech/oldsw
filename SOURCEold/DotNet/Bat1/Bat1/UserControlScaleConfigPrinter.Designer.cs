﻿namespace Bat1 {
    partial class UserControlScaleConfigPrinter {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigPrinter));
            this.comboBoxSpeed = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownPaperWidth = new System.Windows.Forms.NumericUpDown();
            this.comboBoxFormat = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPaperWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxSpeed
            // 
            this.comboBoxSpeed.AccessibleDescription = null;
            this.comboBoxSpeed.AccessibleName = null;
            resources.ApplyResources(this.comboBoxSpeed, "comboBoxSpeed");
            this.comboBoxSpeed.BackgroundImage = null;
            this.comboBoxSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpeed.Font = null;
            this.comboBoxSpeed.FormattingEnabled = true;
            this.comboBoxSpeed.Items.AddRange(new object[] {
            resources.GetString("comboBoxSpeed.Items"),
            resources.GetString("comboBoxSpeed.Items1"),
            resources.GetString("comboBoxSpeed.Items2"),
            resources.GetString("comboBoxSpeed.Items3"),
            resources.GetString("comboBoxSpeed.Items4")});
            this.comboBoxSpeed.Name = "comboBoxSpeed";
            this.comboBoxSpeed.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label10
            // 
            this.label10.AccessibleDescription = null;
            this.label10.AccessibleName = null;
            resources.ApplyResources(this.label10, "label10");
            this.label10.Font = null;
            this.label10.Name = "label10";
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Font = null;
            this.label2.Name = "label2";
            // 
            // numericUpDownPaperWidth
            // 
            this.numericUpDownPaperWidth.AccessibleDescription = null;
            this.numericUpDownPaperWidth.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownPaperWidth, "numericUpDownPaperWidth");
            this.numericUpDownPaperWidth.Font = null;
            this.numericUpDownPaperWidth.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownPaperWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPaperWidth.Name = "numericUpDownPaperWidth";
            this.numericUpDownPaperWidth.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownPaperWidth.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownPaperWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // comboBoxFormat
            // 
            this.comboBoxFormat.AccessibleDescription = null;
            this.comboBoxFormat.AccessibleName = null;
            resources.ApplyResources(this.comboBoxFormat, "comboBoxFormat");
            this.comboBoxFormat.BackgroundImage = null;
            this.comboBoxFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFormat.Font = null;
            this.comboBoxFormat.FormattingEnabled = true;
            this.comboBoxFormat.Items.AddRange(new object[] {
            resources.GetString("comboBoxFormat.Items"),
            resources.GetString("comboBoxFormat.Items1"),
            resources.GetString("comboBoxFormat.Items2"),
            resources.GetString("comboBoxFormat.Items3"),
            resources.GetString("comboBoxFormat.Items4")});
            this.comboBoxFormat.Name = "comboBoxFormat";
            this.comboBoxFormat.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            // 
            // UserControlScaleConfigPrinter
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.comboBoxFormat);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDownPaperWidth);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxSpeed);
            this.Controls.Add(this.label10);
            this.Font = null;
            this.Name = "UserControlScaleConfigPrinter";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPaperWidth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSpeed;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownPaperWidth;
        private System.Windows.Forms.ComboBox comboBoxFormat;
        private System.Windows.Forms.Label label1;
    }
}
