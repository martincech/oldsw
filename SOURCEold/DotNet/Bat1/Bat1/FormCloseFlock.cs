﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormCloseFlock : Form {
        /// <summary>
        /// Entered date
        /// </summary>
        public DateTime ClosedDateTime;

        private DateTime startDateTime;

        public FormCloseFlock(string flockName, DateTime startDateTime) {
            InitializeComponent();

            this.startDateTime = startDateTime;
            Text = Properties.Resources.FLOCK_CLOSE + ": " + flockName;
        }

        private void FormCloseFlock_Shown(object sender, EventArgs e) {
            if (DateTime.Now.Date >= startDateTime.Date) {
                dateTimePicker.Value = DateTime.Now;    // Datum uzavreni muze byt stejny jako zahajeni
            } else {
                dateTimePicker.Value = startDateTime;
            }

            dateTimePicker.Focus();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Datum uzavreni musi byt vetsi nebo roven datu zahajeni hejna
            if (dateTimePicker.Value.Date < startDateTime.Date) {
                CheckValue.InvalidValueMessage(dateTimePicker);
                return;
            }

            ClosedDateTime = dateTimePicker.Value.Date;
            DialogResult = DialogResult.OK;
        }
    }
}
