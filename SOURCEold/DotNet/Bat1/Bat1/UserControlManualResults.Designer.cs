﻿namespace Bat1 {
    partial class UserControlManualResults {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlManualResults));
            this.tabControlResults = new System.Windows.Forms.TabControl();
            this.tabPageResults = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultNormal = new Bat1.UserControlWeighingResult();
            this.tabPageLight = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultLight = new Bat1.UserControlWeighingResult();
            this.tabPageOk = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultOk = new Bat1.UserControlWeighingResult();
            this.tabPageHeavy = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultHeavy = new Bat1.UserControlWeighingResult();
            this.tabPageMales = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultMales = new Bat1.UserControlWeighingResult();
            this.tabPageFemales = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultFemales = new Bat1.UserControlWeighingResult();
            this.comboBoxFlag = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.dateTimePickerStartTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.comboBoxFileName = new System.Windows.Forms.ComboBox();
            this.comboBoxScaleName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.textBoxFileNote = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControlResults.SuspendLayout();
            this.tabPageResults.SuspendLayout();
            this.tabPageLight.SuspendLayout();
            this.tabPageOk.SuspendLayout();
            this.tabPageHeavy.SuspendLayout();
            this.tabPageMales.SuspendLayout();
            this.tabPageFemales.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlResults
            // 
            this.tabControlResults.AccessibleDescription = null;
            this.tabControlResults.AccessibleName = null;
            resources.ApplyResources(this.tabControlResults, "tabControlResults");
            this.tabControlResults.BackgroundImage = null;
            this.tabControlResults.Controls.Add(this.tabPageResults);
            this.tabControlResults.Controls.Add(this.tabPageLight);
            this.tabControlResults.Controls.Add(this.tabPageOk);
            this.tabControlResults.Controls.Add(this.tabPageHeavy);
            this.tabControlResults.Controls.Add(this.tabPageMales);
            this.tabControlResults.Controls.Add(this.tabPageFemales);
            this.tabControlResults.Name = "tabControlResults";
            this.tabControlResults.SelectedIndex = 0;
            // 
            // tabPageResults
            // 
            this.tabPageResults.AccessibleDescription = null;
            this.tabPageResults.AccessibleName = null;
            resources.ApplyResources(this.tabPageResults, "tabPageResults");
            this.tabPageResults.BackgroundImage = null;
            this.tabPageResults.Controls.Add(this.userControlWeighingResultNormal);
            this.tabPageResults.Font = null;
            this.tabPageResults.Name = "tabPageResults";
            this.tabPageResults.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultNormal
            // 
            this.userControlWeighingResultNormal.AccessibleDescription = null;
            this.userControlWeighingResultNormal.AccessibleName = null;
            resources.ApplyResources(this.userControlWeighingResultNormal, "userControlWeighingResultNormal");
            this.userControlWeighingResultNormal.BackgroundImage = null;
            this.userControlWeighingResultNormal.Font = null;
            this.userControlWeighingResultNormal.Name = "userControlWeighingResultNormal";
            // 
            // tabPageLight
            // 
            this.tabPageLight.AccessibleDescription = null;
            this.tabPageLight.AccessibleName = null;
            resources.ApplyResources(this.tabPageLight, "tabPageLight");
            this.tabPageLight.BackgroundImage = null;
            this.tabPageLight.Controls.Add(this.userControlWeighingResultLight);
            this.tabPageLight.Font = null;
            this.tabPageLight.Name = "tabPageLight";
            this.tabPageLight.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultLight
            // 
            this.userControlWeighingResultLight.AccessibleDescription = null;
            this.userControlWeighingResultLight.AccessibleName = null;
            resources.ApplyResources(this.userControlWeighingResultLight, "userControlWeighingResultLight");
            this.userControlWeighingResultLight.BackgroundImage = null;
            this.userControlWeighingResultLight.Font = null;
            this.userControlWeighingResultLight.Name = "userControlWeighingResultLight";
            // 
            // tabPageOk
            // 
            this.tabPageOk.AccessibleDescription = null;
            this.tabPageOk.AccessibleName = null;
            resources.ApplyResources(this.tabPageOk, "tabPageOk");
            this.tabPageOk.BackgroundImage = null;
            this.tabPageOk.Controls.Add(this.userControlWeighingResultOk);
            this.tabPageOk.Font = null;
            this.tabPageOk.Name = "tabPageOk";
            this.tabPageOk.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultOk
            // 
            this.userControlWeighingResultOk.AccessibleDescription = null;
            this.userControlWeighingResultOk.AccessibleName = null;
            resources.ApplyResources(this.userControlWeighingResultOk, "userControlWeighingResultOk");
            this.userControlWeighingResultOk.BackgroundImage = null;
            this.userControlWeighingResultOk.Font = null;
            this.userControlWeighingResultOk.Name = "userControlWeighingResultOk";
            // 
            // tabPageHeavy
            // 
            this.tabPageHeavy.AccessibleDescription = null;
            this.tabPageHeavy.AccessibleName = null;
            resources.ApplyResources(this.tabPageHeavy, "tabPageHeavy");
            this.tabPageHeavy.BackgroundImage = null;
            this.tabPageHeavy.Controls.Add(this.userControlWeighingResultHeavy);
            this.tabPageHeavy.Font = null;
            this.tabPageHeavy.Name = "tabPageHeavy";
            this.tabPageHeavy.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultHeavy
            // 
            this.userControlWeighingResultHeavy.AccessibleDescription = null;
            this.userControlWeighingResultHeavy.AccessibleName = null;
            resources.ApplyResources(this.userControlWeighingResultHeavy, "userControlWeighingResultHeavy");
            this.userControlWeighingResultHeavy.BackgroundImage = null;
            this.userControlWeighingResultHeavy.Font = null;
            this.userControlWeighingResultHeavy.Name = "userControlWeighingResultHeavy";
            // 
            // tabPageMales
            // 
            this.tabPageMales.AccessibleDescription = null;
            this.tabPageMales.AccessibleName = null;
            resources.ApplyResources(this.tabPageMales, "tabPageMales");
            this.tabPageMales.BackgroundImage = null;
            this.tabPageMales.Controls.Add(this.userControlWeighingResultMales);
            this.tabPageMales.Font = null;
            this.tabPageMales.Name = "tabPageMales";
            this.tabPageMales.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultMales
            // 
            this.userControlWeighingResultMales.AccessibleDescription = null;
            this.userControlWeighingResultMales.AccessibleName = null;
            resources.ApplyResources(this.userControlWeighingResultMales, "userControlWeighingResultMales");
            this.userControlWeighingResultMales.BackgroundImage = null;
            this.userControlWeighingResultMales.Font = null;
            this.userControlWeighingResultMales.Name = "userControlWeighingResultMales";
            // 
            // tabPageFemales
            // 
            this.tabPageFemales.AccessibleDescription = null;
            this.tabPageFemales.AccessibleName = null;
            resources.ApplyResources(this.tabPageFemales, "tabPageFemales");
            this.tabPageFemales.BackgroundImage = null;
            this.tabPageFemales.Controls.Add(this.userControlWeighingResultFemales);
            this.tabPageFemales.Font = null;
            this.tabPageFemales.Name = "tabPageFemales";
            this.tabPageFemales.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultFemales
            // 
            this.userControlWeighingResultFemales.AccessibleDescription = null;
            this.userControlWeighingResultFemales.AccessibleName = null;
            resources.ApplyResources(this.userControlWeighingResultFemales, "userControlWeighingResultFemales");
            this.userControlWeighingResultFemales.BackgroundImage = null;
            this.userControlWeighingResultFemales.Font = null;
            this.userControlWeighingResultFemales.Name = "userControlWeighingResultFemales";
            // 
            // comboBoxFlag
            // 
            this.comboBoxFlag.AccessibleDescription = null;
            this.comboBoxFlag.AccessibleName = null;
            resources.ApplyResources(this.comboBoxFlag, "comboBoxFlag");
            this.comboBoxFlag.BackgroundImage = null;
            this.comboBoxFlag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFlag.FormattingEnabled = true;
            this.comboBoxFlag.Items.AddRange(new object[] {
            resources.GetString("comboBoxFlag.Items"),
            resources.GetString("comboBoxFlag.Items1"),
            resources.GetString("comboBoxFlag.Items2"),
            resources.GetString("comboBoxFlag.Items3")});
            this.comboBoxFlag.Name = "comboBoxFlag";
            this.comboBoxFlag.SelectionChangeCommitted += new System.EventHandler(this.comboBoxFlag_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textBoxNote
            // 
            this.textBoxNote.AccessibleDescription = null;
            this.textBoxNote.AccessibleName = null;
            resources.ApplyResources(this.textBoxNote, "textBoxNote");
            this.textBoxNote.BackgroundImage = null;
            this.textBoxNote.Font = null;
            this.textBoxNote.Name = "textBoxNote";
            // 
            // dateTimePickerStartTime
            // 
            this.dateTimePickerStartTime.AccessibleDescription = null;
            this.dateTimePickerStartTime.AccessibleName = null;
            resources.ApplyResources(this.dateTimePickerStartTime, "dateTimePickerStartTime");
            this.dateTimePickerStartTime.BackgroundImage = null;
            this.dateTimePickerStartTime.CalendarFont = null;
            this.dateTimePickerStartTime.CustomFormat = null;
            this.dateTimePickerStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerStartTime.Name = "dateTimePickerStartTime";
            this.dateTimePickerStartTime.ShowUpDown = true;
            // 
            // dateTimePickerStartDate
            // 
            this.dateTimePickerStartDate.AccessibleDescription = null;
            this.dateTimePickerStartDate.AccessibleName = null;
            resources.ApplyResources(this.dateTimePickerStartDate, "dateTimePickerStartDate");
            this.dateTimePickerStartDate.BackgroundImage = null;
            this.dateTimePickerStartDate.CalendarFont = null;
            this.dateTimePickerStartDate.CustomFormat = null;
            this.dateTimePickerStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerStartDate.Name = "dateTimePickerStartDate";
            // 
            // comboBoxFileName
            // 
            this.comboBoxFileName.AccessibleDescription = null;
            this.comboBoxFileName.AccessibleName = null;
            resources.ApplyResources(this.comboBoxFileName, "comboBoxFileName");
            this.comboBoxFileName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxFileName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxFileName.BackgroundImage = null;
            this.comboBoxFileName.DropDownHeight = 300;
            this.comboBoxFileName.FormattingEnabled = true;
            this.comboBoxFileName.Name = "comboBoxFileName";
            this.comboBoxFileName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxScaleName_KeyPress);
            this.comboBoxFileName.TextChanged += new System.EventHandler(this.comboBoxFileName_TextChanged);
            // 
            // comboBoxScaleName
            // 
            this.comboBoxScaleName.AccessibleDescription = null;
            this.comboBoxScaleName.AccessibleName = null;
            resources.ApplyResources(this.comboBoxScaleName, "comboBoxScaleName");
            this.comboBoxScaleName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxScaleName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxScaleName.BackgroundImage = null;
            this.comboBoxScaleName.DropDownHeight = 300;
            this.comboBoxScaleName.FormattingEnabled = true;
            this.comboBoxScaleName.Name = "comboBoxScaleName";
            this.comboBoxScaleName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxScaleName_KeyPress);
            // 
            // label6
            // 
            this.label6.AccessibleDescription = null;
            this.label6.AccessibleName = null;
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label9
            // 
            this.label9.AccessibleDescription = null;
            this.label9.AccessibleName = null;
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label4
            // 
            this.label4.AccessibleDescription = null;
            this.label4.AccessibleName = null;
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label7
            // 
            this.label7.AccessibleDescription = null;
            this.label7.AccessibleName = null;
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.AccessibleDescription = null;
            this.comboBoxUnits.AccessibleName = null;
            resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
            this.comboBoxUnits.BackgroundImage = null;
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
            this.comboBoxUnits.Name = "comboBoxUnits";
            // 
            // textBoxFileNote
            // 
            this.textBoxFileNote.AccessibleDescription = null;
            this.textBoxFileNote.AccessibleName = null;
            resources.ApplyResources(this.textBoxFileNote, "textBoxFileNote");
            this.textBoxFileNote.BackgroundImage = null;
            this.textBoxFileNote.Font = null;
            this.textBoxFileNote.Name = "textBoxFileNote";
            this.textBoxFileNote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxScaleName_KeyPress);
            // 
            // label8
            // 
            this.label8.AccessibleDescription = null;
            this.label8.AccessibleName = null;
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // UserControlManualResults
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tabControlResults);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxFlag);
            this.Controls.Add(this.textBoxFileNote);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxUnits);
            this.Controls.Add(this.dateTimePickerStartDate);
            this.Controls.Add(this.dateTimePickerStartTime);
            this.Controls.Add(this.comboBoxFileName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBoxScaleName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxNote);
            this.Font = null;
            this.Name = "UserControlManualResults";
            this.tabControlResults.ResumeLayout(false);
            this.tabPageResults.ResumeLayout(false);
            this.tabPageLight.ResumeLayout(false);
            this.tabPageOk.ResumeLayout(false);
            this.tabPageHeavy.ResumeLayout(false);
            this.tabPageMales.ResumeLayout(false);
            this.tabPageFemales.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlResults;
        private System.Windows.Forms.TabPage tabPageResults;
        private UserControlWeighingResult userControlWeighingResultNormal;
        private System.Windows.Forms.TabPage tabPageFemales;
        private UserControlWeighingResult userControlWeighingResultFemales;
        private System.Windows.Forms.TabPage tabPageMales;
        private UserControlWeighingResult userControlWeighingResultMales;
        private System.Windows.Forms.TabPage tabPageLight;
        private UserControlWeighingResult userControlWeighingResultLight;
        private System.Windows.Forms.TabPage tabPageOk;
        private UserControlWeighingResult userControlWeighingResultOk;
        private System.Windows.Forms.TabPage tabPageHeavy;
        private UserControlWeighingResult userControlWeighingResultHeavy;
        private System.Windows.Forms.ComboBox comboBoxFlag;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartTime;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartDate;
        private System.Windows.Forms.ComboBox comboBoxFileName;
        private System.Windows.Forms.ComboBox comboBoxScaleName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxFileNote;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
    }
}
