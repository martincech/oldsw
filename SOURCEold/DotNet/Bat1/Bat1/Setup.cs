﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat1;
using Veit.Scale;
using System.Globalization;             // Decimal separator

namespace Bat1 {
    /// <summary>
    /// Export file format
    /// </summary>
    public enum ExportFormat {
        BAT1,       // Export do formatu Bat1
        CSV         // Export do souboru CSV
    }
    
    /// <summary>
    /// CSV file encoding
    /// </summary>
    public enum CsvEncoding {
        DEFAULT,    // Encoding.Default: ANSI kodovani, ktere je prave nastavene ve Windows
        UTF16,      // Encoding.Unicode: Unicode UTF-16, v Excelu funguje dobre
        UTF8        // Encoding.UTF8:    Unicode UTF-8, v Excelu nefunguje, v OpenOffice ano
    }

    /// <summary>
    /// CSV decimal separator
    /// </summary>
    public enum CsvDecimalSeparator {
        DEFAULT,    // Default nastavene ve Windows
        DOT,
        COMMA
    }

    /// <summary>
    /// CSV delimiter
    /// </summary>
    public enum CsvDelimiter {
        COMMA,
        SEMICOLON,
        TAB
    }

    /// <summary>
    /// Predefined CSV configs
    /// </summary>
    public enum CsvTemplate {
        EXCEL,
        OPEN_OFFICE,
        CUSTOM
    }
    
    /// <summary>
    /// CSV export config
    /// </summary>
    public struct CsvConfig {
        /// <summary>
        /// Predefined sets of parameters
        /// </summary>
        public CsvTemplate Template;
        
        /// <summary>
        /// Column delimiter
        /// </summary>
        public CsvDelimiter Delimiter;

        /// <summary>
        /// Decimal separator
        /// </summary>
        public CsvDecimalSeparator DecimalSeparator;

        /// <summary>
        /// File encoding 
        /// </summary>
        public CsvEncoding Encoding;
    }

    public class Setup {
        /// <summary>
        /// Selected SW language
        /// </summary>
        public SwLanguage Language;
        
        /// <summary>
        /// Units last used during entering manual results
        /// </summary>
        public Units ManualUnits;

        /// <summary>
        /// COM port for old scales
        /// </summary>
        public int ScaleVersion6ComPort;

        /// <summary>
        /// Manually selected units for old scales
        /// </summary>
        public Units ScaleVersion6Units;

        /// <summary>
        /// Units last used during entering a growth curve
        /// </summary>
        public Units CurveUnits;

        /// <summary>
        /// Export format used last time
        /// </summary>
        public ExportFormat ExportFormat;

        /// <summary>
        /// CSV export config
        /// </summary>
        public CsvConfig CsvConfig;

        /// <summary>
        /// Excel CSV template
        /// </summary>
        public readonly CsvConfig CsvConfigExcelTemplate;
        
        /// <summary>
        /// Open Office CSV template
        /// </summary>
        public readonly CsvConfig CsvConfigOpenOfficeTemplate;

        /// <summary>
        /// Constructor
        /// </summary>
        public Setup() {
            // Vytvorim sablonu CSV pro Excel
            CsvConfigExcelTemplate.Template         = CsvTemplate.EXCEL;
            CsvConfigExcelTemplate.DecimalSeparator = CsvDecimalSeparator.DOT;  // Excel chce jen tecku
            CsvConfigExcelTemplate.Delimiter        = CsvDelimiter.TAB;         // Excel chce jen tabulator
            CsvConfigExcelTemplate.Encoding         = CsvEncoding.UTF16;        // UTF8 nefunguje

            // Vytvorim sablonu CSV pro Open Office
            CsvConfigOpenOfficeTemplate.Template         = CsvTemplate.OPEN_OFFICE;
            CsvConfigOpenOfficeTemplate.DecimalSeparator = CsvDecimalSeparator.DEFAULT;
            CsvConfigOpenOfficeTemplate.Delimiter        = CsvDelimiter.COMMA;  // V OpenOffice je default carka
            CsvConfigOpenOfficeTemplate.Encoding         = CsvEncoding.UTF16;   // Funguje i UTF8, necham radsi UTF16

            // Nastavim defaulty
            SetDefaults();
        }
        
        /// <summary>
        /// Set default values
        /// </summary>
        public void SetDefaults() {
            Language             = SwLanguage.UNDEFINED;

            ManualUnits          = Units.KG;
            
            ScaleVersion6ComPort = 1;
            ScaleVersion6Units   = Units.KG;

            CurveUnits           = Units.KG;
            
            ExportFormat         = ExportFormat.BAT1;

            CsvConfig            = CsvConfigExcelTemplate;      // Struktury muzu kopirovat primo
        }
    }
}
