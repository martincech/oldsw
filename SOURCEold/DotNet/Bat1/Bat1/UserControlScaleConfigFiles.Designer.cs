﻿namespace Bat1 {
    partial class UserControlScaleConfigFiles {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigFiles));
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonRename = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.listBoxFiles = new System.Windows.Forms.ListBox();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxSorting = new System.Windows.Forms.GroupBox();
            this.numericUpDownSortingHighLimit = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSortingLowLimit = new System.Windows.Forms.NumericUpDown();
            this.labelHighLimitUnits = new System.Windows.Forms.Label();
            this.labelLowLimitUnits = new System.Windows.Forms.Label();
            this.labelHighLimit = new System.Windows.Forms.Label();
            this.comboBoxSortingMode = new System.Windows.Forms.ComboBox();
            this.labelSortingMode = new System.Windows.Forms.Label();
            this.labelLowLimit = new System.Windows.Forms.Label();
            this.groupBoxSaving = new System.Windows.Forms.GroupBox();
            this.numericUpDownSavingStabilizationRange = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSavingStabilizationTime = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSavingMinimumWeight = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSavingFilter = new System.Windows.Forms.NumericUpDown();
            this.labelStabilizationRangeUnits = new System.Windows.Forms.Label();
            this.labelStabilizationTimeSec = new System.Windows.Forms.Label();
            this.labelMinimumWeightUnits = new System.Windows.Forms.Label();
            this.labelFilterSec = new System.Windows.Forms.Label();
            this.labelStabilizationRange = new System.Windows.Forms.Label();
            this.labelStabilizationTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxSavingMode = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelMoreBirds = new System.Windows.Forms.Label();
            this.groupBoxDetails = new System.Windows.Forms.GroupBox();
            this.groupBoxMoreBirds = new System.Windows.Forms.GroupBox();
            this.checkBoxEnableMoreBirds = new System.Windows.Forms.CheckBox();
            this.numericUpDownNumberOfBirds = new System.Windows.Forms.NumericUpDown();
            this.buttonActive = new System.Windows.Forms.Button();
            this.groupBoxSorting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingHighLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingLowLimit)).BeginInit();
            this.groupBoxSaving.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingMinimumWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingFilter)).BeginInit();
            this.groupBoxDetails.SuspendLayout();
            this.groupBoxMoreBirds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfBirds)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonDelete
            // 
            this.buttonDelete.AccessibleDescription = null;
            this.buttonDelete.AccessibleName = null;
            resources.ApplyResources(this.buttonDelete, "buttonDelete");
            this.buttonDelete.BackgroundImage = null;
            this.buttonDelete.Font = null;
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonRename
            // 
            this.buttonRename.AccessibleDescription = null;
            this.buttonRename.AccessibleName = null;
            resources.ApplyResources(this.buttonRename, "buttonRename");
            this.buttonRename.BackgroundImage = null;
            this.buttonRename.Font = null;
            this.buttonRename.Name = "buttonRename";
            this.buttonRename.UseVisualStyleBackColor = true;
            this.buttonRename.Click += new System.EventHandler(this.buttonRename_Click);
            // 
            // buttonNew
            // 
            this.buttonNew.AccessibleDescription = null;
            this.buttonNew.AccessibleName = null;
            resources.ApplyResources(this.buttonNew, "buttonNew");
            this.buttonNew.BackgroundImage = null;
            this.buttonNew.Font = null;
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // listBoxFiles
            // 
            this.listBoxFiles.AccessibleDescription = null;
            this.listBoxFiles.AccessibleName = null;
            resources.ApplyResources(this.listBoxFiles, "listBoxFiles");
            this.listBoxFiles.BackgroundImage = null;
            this.listBoxFiles.Font = null;
            this.listBoxFiles.FormattingEnabled = true;
            this.listBoxFiles.Name = "listBoxFiles";
            this.listBoxFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxFiles_MouseDoubleClick);
            this.listBoxFiles.SelectedIndexChanged += new System.EventHandler(this.listBoxFiles_SelectedIndexChanged);
            // 
            // textBoxNote
            // 
            this.textBoxNote.AccessibleDescription = null;
            this.textBoxNote.AccessibleName = null;
            resources.ApplyResources(this.textBoxNote, "textBoxNote");
            this.textBoxNote.BackgroundImage = null;
            this.textBoxNote.Font = null;
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxNote_KeyDown);
            this.textBoxNote.Leave += new System.EventHandler(this.textBoxNote_Leave);
            this.textBoxNote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNote_KeyPress);
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Font = null;
            this.label2.Name = "label2";
            // 
            // groupBoxSorting
            // 
            this.groupBoxSorting.AccessibleDescription = null;
            this.groupBoxSorting.AccessibleName = null;
            resources.ApplyResources(this.groupBoxSorting, "groupBoxSorting");
            this.groupBoxSorting.BackgroundImage = null;
            this.groupBoxSorting.Controls.Add(this.numericUpDownSortingHighLimit);
            this.groupBoxSorting.Controls.Add(this.numericUpDownSortingLowLimit);
            this.groupBoxSorting.Controls.Add(this.labelHighLimitUnits);
            this.groupBoxSorting.Controls.Add(this.labelLowLimitUnits);
            this.groupBoxSorting.Controls.Add(this.labelHighLimit);
            this.groupBoxSorting.Controls.Add(this.comboBoxSortingMode);
            this.groupBoxSorting.Controls.Add(this.labelSortingMode);
            this.groupBoxSorting.Controls.Add(this.labelLowLimit);
            this.groupBoxSorting.Font = null;
            this.groupBoxSorting.Name = "groupBoxSorting";
            this.groupBoxSorting.TabStop = false;
            // 
            // numericUpDownSortingHighLimit
            // 
            this.numericUpDownSortingHighLimit.AccessibleDescription = null;
            this.numericUpDownSortingHighLimit.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownSortingHighLimit, "numericUpDownSortingHighLimit");
            this.numericUpDownSortingHighLimit.Font = null;
            this.numericUpDownSortingHighLimit.Name = "numericUpDownSortingHighLimit";
            this.numericUpDownSortingHighLimit.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSortingLowLimit
            // 
            this.numericUpDownSortingLowLimit.AccessibleDescription = null;
            this.numericUpDownSortingLowLimit.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownSortingLowLimit, "numericUpDownSortingLowLimit");
            this.numericUpDownSortingLowLimit.Font = null;
            this.numericUpDownSortingLowLimit.Name = "numericUpDownSortingLowLimit";
            this.numericUpDownSortingLowLimit.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelHighLimitUnits
            // 
            this.labelHighLimitUnits.AccessibleDescription = null;
            this.labelHighLimitUnits.AccessibleName = null;
            resources.ApplyResources(this.labelHighLimitUnits, "labelHighLimitUnits");
            this.labelHighLimitUnits.Font = null;
            this.labelHighLimitUnits.Name = "labelHighLimitUnits";
            // 
            // labelLowLimitUnits
            // 
            this.labelLowLimitUnits.AccessibleDescription = null;
            this.labelLowLimitUnits.AccessibleName = null;
            resources.ApplyResources(this.labelLowLimitUnits, "labelLowLimitUnits");
            this.labelLowLimitUnits.Font = null;
            this.labelLowLimitUnits.Name = "labelLowLimitUnits";
            // 
            // labelHighLimit
            // 
            this.labelHighLimit.AccessibleDescription = null;
            this.labelHighLimit.AccessibleName = null;
            resources.ApplyResources(this.labelHighLimit, "labelHighLimit");
            this.labelHighLimit.Font = null;
            this.labelHighLimit.Name = "labelHighLimit";
            // 
            // comboBoxSortingMode
            // 
            this.comboBoxSortingMode.AccessibleDescription = null;
            this.comboBoxSortingMode.AccessibleName = null;
            resources.ApplyResources(this.comboBoxSortingMode, "comboBoxSortingMode");
            this.comboBoxSortingMode.BackgroundImage = null;
            this.comboBoxSortingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSortingMode.Font = null;
            this.comboBoxSortingMode.FormattingEnabled = true;
            this.comboBoxSortingMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxSortingMode.Items"),
            resources.GetString("comboBoxSortingMode.Items1"),
            resources.GetString("comboBoxSortingMode.Items2")});
            this.comboBoxSortingMode.Name = "comboBoxSortingMode";
            this.comboBoxSortingMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelSortingMode
            // 
            this.labelSortingMode.AccessibleDescription = null;
            this.labelSortingMode.AccessibleName = null;
            resources.ApplyResources(this.labelSortingMode, "labelSortingMode");
            this.labelSortingMode.Font = null;
            this.labelSortingMode.Name = "labelSortingMode";
            // 
            // labelLowLimit
            // 
            this.labelLowLimit.AccessibleDescription = null;
            this.labelLowLimit.AccessibleName = null;
            resources.ApplyResources(this.labelLowLimit, "labelLowLimit");
            this.labelLowLimit.Font = null;
            this.labelLowLimit.Name = "labelLowLimit";
            // 
            // groupBoxSaving
            // 
            this.groupBoxSaving.AccessibleDescription = null;
            this.groupBoxSaving.AccessibleName = null;
            resources.ApplyResources(this.groupBoxSaving, "groupBoxSaving");
            this.groupBoxSaving.BackgroundImage = null;
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingStabilizationRange);
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingStabilizationTime);
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingMinimumWeight);
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingFilter);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationRangeUnits);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationTimeSec);
            this.groupBoxSaving.Controls.Add(this.labelMinimumWeightUnits);
            this.groupBoxSaving.Controls.Add(this.labelFilterSec);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationRange);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationTime);
            this.groupBoxSaving.Controls.Add(this.label6);
            this.groupBoxSaving.Controls.Add(this.comboBoxSavingMode);
            this.groupBoxSaving.Controls.Add(this.label7);
            this.groupBoxSaving.Controls.Add(this.label8);
            this.groupBoxSaving.Font = null;
            this.groupBoxSaving.Name = "groupBoxSaving";
            this.groupBoxSaving.TabStop = false;
            // 
            // numericUpDownSavingStabilizationRange
            // 
            this.numericUpDownSavingStabilizationRange.AccessibleDescription = null;
            this.numericUpDownSavingStabilizationRange.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownSavingStabilizationRange, "numericUpDownSavingStabilizationRange");
            this.numericUpDownSavingStabilizationRange.DecimalPlaces = 1;
            this.numericUpDownSavingStabilizationRange.Font = null;
            this.numericUpDownSavingStabilizationRange.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.Maximum = new decimal(new int[] {
            990,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.Name = "numericUpDownSavingStabilizationRange";
            this.numericUpDownSavingStabilizationRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSavingStabilizationTime
            // 
            this.numericUpDownSavingStabilizationTime.AccessibleDescription = null;
            this.numericUpDownSavingStabilizationTime.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownSavingStabilizationTime, "numericUpDownSavingStabilizationTime");
            this.numericUpDownSavingStabilizationTime.DecimalPlaces = 1;
            this.numericUpDownSavingStabilizationTime.Font = null;
            this.numericUpDownSavingStabilizationTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationTime.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownSavingStabilizationTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationTime.Name = "numericUpDownSavingStabilizationTime";
            this.numericUpDownSavingStabilizationTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationTime.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSavingMinimumWeight
            // 
            this.numericUpDownSavingMinimumWeight.AccessibleDescription = null;
            this.numericUpDownSavingMinimumWeight.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownSavingMinimumWeight, "numericUpDownSavingMinimumWeight");
            this.numericUpDownSavingMinimumWeight.Font = null;
            this.numericUpDownSavingMinimumWeight.Name = "numericUpDownSavingMinimumWeight";
            this.numericUpDownSavingMinimumWeight.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSavingFilter
            // 
            this.numericUpDownSavingFilter.AccessibleDescription = null;
            this.numericUpDownSavingFilter.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownSavingFilter, "numericUpDownSavingFilter");
            this.numericUpDownSavingFilter.DecimalPlaces = 1;
            this.numericUpDownSavingFilter.Font = null;
            this.numericUpDownSavingFilter.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.Name = "numericUpDownSavingFilter";
            this.numericUpDownSavingFilter.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelStabilizationRangeUnits
            // 
            this.labelStabilizationRangeUnits.AccessibleDescription = null;
            this.labelStabilizationRangeUnits.AccessibleName = null;
            resources.ApplyResources(this.labelStabilizationRangeUnits, "labelStabilizationRangeUnits");
            this.labelStabilizationRangeUnits.Font = null;
            this.labelStabilizationRangeUnits.Name = "labelStabilizationRangeUnits";
            // 
            // labelStabilizationTimeSec
            // 
            this.labelStabilizationTimeSec.AccessibleDescription = null;
            this.labelStabilizationTimeSec.AccessibleName = null;
            resources.ApplyResources(this.labelStabilizationTimeSec, "labelStabilizationTimeSec");
            this.labelStabilizationTimeSec.Font = null;
            this.labelStabilizationTimeSec.Name = "labelStabilizationTimeSec";
            // 
            // labelMinimumWeightUnits
            // 
            this.labelMinimumWeightUnits.AccessibleDescription = null;
            this.labelMinimumWeightUnits.AccessibleName = null;
            resources.ApplyResources(this.labelMinimumWeightUnits, "labelMinimumWeightUnits");
            this.labelMinimumWeightUnits.Font = null;
            this.labelMinimumWeightUnits.Name = "labelMinimumWeightUnits";
            // 
            // labelFilterSec
            // 
            this.labelFilterSec.AccessibleDescription = null;
            this.labelFilterSec.AccessibleName = null;
            resources.ApplyResources(this.labelFilterSec, "labelFilterSec");
            this.labelFilterSec.Font = null;
            this.labelFilterSec.Name = "labelFilterSec";
            // 
            // labelStabilizationRange
            // 
            this.labelStabilizationRange.AccessibleDescription = null;
            this.labelStabilizationRange.AccessibleName = null;
            resources.ApplyResources(this.labelStabilizationRange, "labelStabilizationRange");
            this.labelStabilizationRange.Font = null;
            this.labelStabilizationRange.Name = "labelStabilizationRange";
            // 
            // labelStabilizationTime
            // 
            this.labelStabilizationTime.AccessibleDescription = null;
            this.labelStabilizationTime.AccessibleName = null;
            resources.ApplyResources(this.labelStabilizationTime, "labelStabilizationTime");
            this.labelStabilizationTime.Font = null;
            this.labelStabilizationTime.Name = "labelStabilizationTime";
            // 
            // label6
            // 
            this.label6.AccessibleDescription = null;
            this.label6.AccessibleName = null;
            resources.ApplyResources(this.label6, "label6");
            this.label6.Font = null;
            this.label6.Name = "label6";
            // 
            // comboBoxSavingMode
            // 
            this.comboBoxSavingMode.AccessibleDescription = null;
            this.comboBoxSavingMode.AccessibleName = null;
            resources.ApplyResources(this.comboBoxSavingMode, "comboBoxSavingMode");
            this.comboBoxSavingMode.BackgroundImage = null;
            this.comboBoxSavingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSavingMode.Font = null;
            this.comboBoxSavingMode.FormattingEnabled = true;
            this.comboBoxSavingMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxSavingMode.Items"),
            resources.GetString("comboBoxSavingMode.Items1"),
            resources.GetString("comboBoxSavingMode.Items2")});
            this.comboBoxSavingMode.Name = "comboBoxSavingMode";
            this.comboBoxSavingMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.AccessibleDescription = null;
            this.label7.AccessibleName = null;
            resources.ApplyResources(this.label7, "label7");
            this.label7.Font = null;
            this.label7.Name = "label7";
            // 
            // label8
            // 
            this.label8.AccessibleDescription = null;
            this.label8.AccessibleName = null;
            resources.ApplyResources(this.label8, "label8");
            this.label8.Font = null;
            this.label8.Name = "label8";
            // 
            // labelMoreBirds
            // 
            this.labelMoreBirds.AccessibleDescription = null;
            this.labelMoreBirds.AccessibleName = null;
            resources.ApplyResources(this.labelMoreBirds, "labelMoreBirds");
            this.labelMoreBirds.Font = null;
            this.labelMoreBirds.Name = "labelMoreBirds";
            // 
            // groupBoxDetails
            // 
            this.groupBoxDetails.AccessibleDescription = null;
            this.groupBoxDetails.AccessibleName = null;
            resources.ApplyResources(this.groupBoxDetails, "groupBoxDetails");
            this.groupBoxDetails.BackgroundImage = null;
            this.groupBoxDetails.Controls.Add(this.groupBoxMoreBirds);
            this.groupBoxDetails.Controls.Add(this.label2);
            this.groupBoxDetails.Controls.Add(this.textBoxNote);
            this.groupBoxDetails.Controls.Add(this.groupBoxSorting);
            this.groupBoxDetails.Controls.Add(this.groupBoxSaving);
            this.groupBoxDetails.Font = null;
            this.groupBoxDetails.Name = "groupBoxDetails";
            this.groupBoxDetails.TabStop = false;
            // 
            // groupBoxMoreBirds
            // 
            this.groupBoxMoreBirds.AccessibleDescription = null;
            this.groupBoxMoreBirds.AccessibleName = null;
            resources.ApplyResources(this.groupBoxMoreBirds, "groupBoxMoreBirds");
            this.groupBoxMoreBirds.BackgroundImage = null;
            this.groupBoxMoreBirds.Controls.Add(this.checkBoxEnableMoreBirds);
            this.groupBoxMoreBirds.Controls.Add(this.numericUpDownNumberOfBirds);
            this.groupBoxMoreBirds.Controls.Add(this.labelMoreBirds);
            this.groupBoxMoreBirds.Font = null;
            this.groupBoxMoreBirds.Name = "groupBoxMoreBirds";
            this.groupBoxMoreBirds.TabStop = false;
            // 
            // checkBoxEnableMoreBirds
            // 
            this.checkBoxEnableMoreBirds.AccessibleDescription = null;
            this.checkBoxEnableMoreBirds.AccessibleName = null;
            resources.ApplyResources(this.checkBoxEnableMoreBirds, "checkBoxEnableMoreBirds");
            this.checkBoxEnableMoreBirds.BackgroundImage = null;
            this.checkBoxEnableMoreBirds.Font = null;
            this.checkBoxEnableMoreBirds.Name = "checkBoxEnableMoreBirds";
            this.checkBoxEnableMoreBirds.UseVisualStyleBackColor = true;
            this.checkBoxEnableMoreBirds.CheckedChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownNumberOfBirds
            // 
            this.numericUpDownNumberOfBirds.AccessibleDescription = null;
            this.numericUpDownNumberOfBirds.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownNumberOfBirds, "numericUpDownNumberOfBirds");
            this.numericUpDownNumberOfBirds.Font = null;
            this.numericUpDownNumberOfBirds.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownNumberOfBirds.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownNumberOfBirds.Name = "numericUpDownNumberOfBirds";
            this.numericUpDownNumberOfBirds.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownNumberOfBirds.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownNumberOfBirds.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // buttonActive
            // 
            this.buttonActive.AccessibleDescription = null;
            this.buttonActive.AccessibleName = null;
            resources.ApplyResources(this.buttonActive, "buttonActive");
            this.buttonActive.BackgroundImage = null;
            this.buttonActive.Font = null;
            this.buttonActive.Name = "buttonActive";
            this.buttonActive.UseVisualStyleBackColor = true;
            this.buttonActive.Click += new System.EventHandler(this.buttonActive_Click);
            // 
            // UserControlScaleConfigFiles
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.buttonActive);
            this.Controls.Add(this.groupBoxDetails);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonRename);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.listBoxFiles);
            this.Font = null;
            this.Name = "UserControlScaleConfigFiles";
            this.groupBoxSorting.ResumeLayout(false);
            this.groupBoxSorting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingHighLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingLowLimit)).EndInit();
            this.groupBoxSaving.ResumeLayout(false);
            this.groupBoxSaving.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingMinimumWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingFilter)).EndInit();
            this.groupBoxDetails.ResumeLayout(false);
            this.groupBoxDetails.PerformLayout();
            this.groupBoxMoreBirds.ResumeLayout(false);
            this.groupBoxMoreBirds.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfBirds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonRename;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.ListBox listBoxFiles;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxSorting;
        private System.Windows.Forms.ComboBox comboBoxSortingMode;
        private System.Windows.Forms.Label labelSortingMode;
        private System.Windows.Forms.Label labelLowLimit;
        private System.Windows.Forms.Label labelHighLimit;
        private System.Windows.Forms.GroupBox groupBoxSaving;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxSavingMode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelStabilizationRange;
        private System.Windows.Forms.Label labelStabilizationTime;
        private System.Windows.Forms.Label labelMoreBirds;
        private System.Windows.Forms.GroupBox groupBoxDetails;
        private System.Windows.Forms.Label labelHighLimitUnits;
        private System.Windows.Forms.Label labelLowLimitUnits;
        private System.Windows.Forms.Label labelStabilizationRangeUnits;
        private System.Windows.Forms.Label labelStabilizationTimeSec;
        private System.Windows.Forms.Label labelMinimumWeightUnits;
        private System.Windows.Forms.Label labelFilterSec;
        private System.Windows.Forms.NumericUpDown numericUpDownNumberOfBirds;
        private System.Windows.Forms.NumericUpDown numericUpDownSortingLowLimit;
        private System.Windows.Forms.NumericUpDown numericUpDownSortingHighLimit;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingFilter;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingMinimumWeight;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingStabilizationTime;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingStabilizationRange;
        private System.Windows.Forms.GroupBox groupBoxMoreBirds;
        private System.Windows.Forms.CheckBox checkBoxEnableMoreBirds;
        private System.Windows.Forms.Button buttonActive;
    }
}
