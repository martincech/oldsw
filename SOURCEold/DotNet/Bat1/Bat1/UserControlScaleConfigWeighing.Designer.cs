﻿namespace Bat1 {
    partial class UserControlScaleConfigWeighing {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigWeighing));
            this.labelDivisionUnits = new System.Windows.Forms.Label();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownDivision = new System.Windows.Forms.NumericUpDown();
            this.comboBoxCapacity = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDivision)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDivisionUnits
            // 
            this.labelDivisionUnits.AccessibleDescription = null;
            this.labelDivisionUnits.AccessibleName = null;
            resources.ApplyResources(this.labelDivisionUnits, "labelDivisionUnits");
            this.labelDivisionUnits.Font = null;
            this.labelDivisionUnits.Name = "labelDivisionUnits";
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.AccessibleDescription = null;
            this.comboBoxUnits.AccessibleName = null;
            resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
            this.comboBoxUnits.BackgroundImage = null;
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.Font = null;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.comboBoxUnits.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUnits_SelectionChangeCommitted);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            // 
            // label10
            // 
            this.label10.AccessibleDescription = null;
            this.label10.AccessibleName = null;
            resources.ApplyResources(this.label10, "label10");
            this.label10.Font = null;
            this.label10.Name = "label10";
            // 
            // numericUpDownDivision
            // 
            this.numericUpDownDivision.AccessibleDescription = null;
            this.numericUpDownDivision.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownDivision, "numericUpDownDivision");
            this.numericUpDownDivision.Font = null;
            this.numericUpDownDivision.Name = "numericUpDownDivision";
            this.numericUpDownDivision.ValueChanged += new System.EventHandler(this.numericUpDownDivision_ValueChanged);
            // 
            // comboBoxCapacity
            // 
            this.comboBoxCapacity.AccessibleDescription = null;
            this.comboBoxCapacity.AccessibleName = null;
            resources.ApplyResources(this.comboBoxCapacity, "comboBoxCapacity");
            this.comboBoxCapacity.BackgroundImage = null;
            this.comboBoxCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCapacity.Font = null;
            this.comboBoxCapacity.FormattingEnabled = true;
            this.comboBoxCapacity.Items.AddRange(new object[] {
            resources.GetString("comboBoxCapacity.Items"),
            resources.GetString("comboBoxCapacity.Items1")});
            this.comboBoxCapacity.Name = "comboBoxCapacity";
            this.comboBoxCapacity.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCapacity_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AccessibleDescription = null;
            this.label2.AccessibleName = null;
            resources.ApplyResources(this.label2, "label2");
            this.label2.Font = null;
            this.label2.Name = "label2";
            // 
            // UserControlScaleConfigWeighing
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.comboBoxCapacity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownDivision);
            this.Controls.Add(this.labelDivisionUnits);
            this.Controls.Add(this.comboBoxUnits);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label10);
            this.Font = null;
            this.Name = "UserControlScaleConfigWeighing";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDivision)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDivisionUnits;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownDivision;
        private System.Windows.Forms.ComboBox comboBoxCapacity;
        private System.Windows.Forms.Label label2;
    }
}
