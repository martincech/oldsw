﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.ScaleStatistics;
using Veit.Scale;

namespace Bat1 {
    /// <summary>
    /// List of weighings
    /// </summary>
    public class WeighingList {
        /// <summary>
        /// Weighings in the list
        /// </summary>
        public  List<Weighing> List { get { return list; } }
        private List<Weighing> list = new List<Weighing>();

        /// <summary>
        /// Sorted list of all flags used in all weighings
        /// </summary>
        public  List<Flag> FlagList { get { return flagList; } }
        private List<Flag> flagList = new List<Flag>();

        /// <summary>
        /// Sorted list of all files used in all weighings
        /// </summary>
        public  List<NameNote> FileList { get { return fileList; } }
        private List<NameNote> fileList = new List<NameNote>();

        /// <summary>
        /// Unsorted list of units used in all weighings
        /// </summary>
        public  List<Units> UnitsList { get { return unitsList; } }
        private List<Units> unitsList = new List<Units>();

        /// <summary>
        /// List of total results for all used flags
        /// </summary>
        public  WeighingResults TotalstatisticResults { get { return totalStatisticResults; } }
        private WeighingResults totalStatisticResults;

        /// <summary>
        /// True if the list contains manually entered results
        /// </summary>
        public  bool ContainsManualResults { get { return containsManualResults; } }
        private bool containsManualResults;

        /// <summary>
        ///  Constructor
        /// </summary>
        public WeighingList() {
            // Na zacatek seznamu flagu pridam bez rozliseni flagu, ten je tam vzdy
            AddFlag(Flag.ALL);
            containsManualResults = false;
        }

        /// <summary>
        /// If the flag doesn't exist, add it to the flag list
        /// </summary>
        /// <param name="flag">Flag to add</param>
        private void AddFlag(Flag flag) {
            if (!flagList.Contains(flag)) {
                flagList.Add(flag);
            }
        }
        
        /// <summary>
        /// If units doesn't exist, add it to the units list
        /// </summary>
        /// <param name="units">Units to add</param>
        private void AddUnits(Units units) {
            if (!unitsList.Contains(units)) {
                unitsList.Add(units);
            }
        }

        /// <summary>
        /// Add weighing to the list
        /// </summary>
        /// <param name="weighing">Weighing to add</param>
        public void Add(Weighing weighing) {
            if (weighing == null) {
                return;
            }

            // Pridam vazeni do seznamu, ponecham nesetridene. Pripadne trideni podle data delam extra, protoze
            // u hejn potrebuju mit vazeni setridene podle hejn a ne data.
            list.Add(weighing);

            // Pokud vazeni obsahuje flagy, ktere jeste nejsou v seznamu, pridam je tam
            foreach (Flag flag in weighing.WeighingResults.FlagList) {
                AddFlag(flag);
            }
            flagList.Sort();                // Na zaver setridim

            // Pokud je vazeni v jednotkach, ktere jeste nejsou v seznamu, pridam je tam
            AddUnits(weighing.WeighingData.ScaleConfig.Units.Units);

            // Pridam soubor vazeni do seznamu (pokud tam jeste neni)
            if (!fileList.Contains(weighing.WeighingData.File)) {
                fileList.Add(weighing.WeighingData.File);
            }
            fileList.Sort(NameNote.CompareByName);   // Setridim podle jmena

            if (weighing.WeighingData.ResultType == ResultType.MANUAL) {
                containsManualResults = true;
            }
        }

        /// <summary>
        /// Sort the list by time
        /// </summary>
        public void Sort() {
            list.Sort(Weighing.CompareByTime);
        }

        /// <summary>
        /// Sort the list by flock day
        /// </summary>
        public void SortByFlockDay() {
            list.Sort(Weighing.CompareByFlockDay);
        }

        /// <summary>
        /// Clear all results
        /// </summary>
        public void Clear() {
            list.Clear();
            flagList.Clear();
            unitsList.Clear();
            fileList.Clear();
            containsManualResults = false;
        }

        /// <summary>
        /// Calculate average weighing speed from all weighings
        /// </summary>
        /// <param name="flag">Flag to calculate from</param>
        /// <returns>Average weighing speed</returns>
        private double CalculateAverageWeighingSpeed(Flag flag) {
            int count = 0;
            double weighingSpeed = 0;
            foreach (Weighing weighing in list) {
                StatisticResult result = weighing.WeighingResults.GetResult(flag);
                if (result == null) {
                    continue;
                }
                weighingSpeed += result.WeighingSpeed;
                count++;
            }

            if (count == 0) {
                return 0;       // Deleni nulou
            }

            return weighingSpeed / (double)count;
        }
        
        /// <summary>
        /// Calculate total statistics for all used flags
        /// </summary>
        public void CalculateTotalStatistics() {
            if (list.Count == 0) {
                // V seznamu neni zadne vazeni
                return;
            }
            
            // Rozsah uniformity a krok histogramu beru z prvniho vazeni v seznamu
            // Beru jen zaznamy stazene z vahy, u rucne zadanych vysledku nastaveni neni
            int uniformityRange = 10;      // Nejake default hodnoty pro pripad, ze by v seznamu byly jen manualni vysledky
            HistogramConfig histogramConfig = new HistogramConfig();
            histogramConfig.Mode  = HistogramMode.RANGE;
            histogramConfig.Range = 30;
            Units units = Units.KG;
            foreach (Weighing weighing in list) {
                units = weighing.WeighingData.ScaleConfig.Units.Units;      // Jednotky muzu vzit i z rucniho vazeni
                if (weighing.WeighingData.ResultType != ResultType.MANUAL) {
                    // V seznamu je nejake vazeni stazene z vah, nactu
                    uniformityRange = weighing.WeighingData.ScaleConfig.StatisticConfig.UniformityRange;
                    histogramConfig = weighing.WeighingData.ScaleConfig.StatisticConfig.Histogram;
                    break;
                }
            }

            if (containsManualResults) {
                // V seznamu je nejake vazeni zadane rucne, celkovou statistiku pocitam zjednodusene
                // Muzu spocitat jen celkovy prumer a pocet vzorku pro jednotlive flagy
                List<StatisticResult> resultList = new List<StatisticResult>();
                foreach (Flag flag in FlagList) {
                    double average = 0;         // Celkovou statiustiku pocitam radeji jako double
                    int count      = 0;
                    DateTime weighingStarted = DateTime.MaxValue;
                    foreach (Weighing weighing in list) {
                        StatisticResult result = weighing.WeighingResults.GetResult(flag);
                        if (result == null) {
                            continue;           // Tento flag v tomto vazeni neni
                        }
                        average += (double)result.Count * result.ExactAverage;       // Sumuju prumerne hmotnosti, pouziju presny prumer
                        count   += result.Count;
                        if (result.WeighingStarted < weighingStarted) {
                            weighingStarted = result.WeighingStarted;
                        }
                    }
                    if (count != 0) {
                        average /= (double)count;
                    }
                    resultList.Add(new StatisticResult(flag, count, (float)average, 0, 0, 0, weighingStarted));
                }
                totalStatisticResults = new WeighingResults(resultList);
            } else {
                // V seznamu jsou jen vysledky z vah, pocitam vse ze vzorku
                // Sestavim seznam vzorku ze vsech vazeni
                SampleList sampleList = new SampleList();
                float minWeight = float.MaxValue;
                float maxWeight = float.MinValue;
                foreach (Weighing weighing in list) {
                    weighing.WeighingData.SampleList.First();
                    while (weighing.WeighingData.SampleList.Read()) {
                        sampleList.Add(weighing.WeighingData.SampleList.Sample);
                        float weight = weighing.WeighingData.SampleList.Sample.weight;
                        if (weight < minWeight) {
                            minWeight = weight;
                        }
                        if (weight > maxWeight) {
                            maxWeight = weight;
                        }
                    }
                }

                // Vypoctu rozsah histogramu tak, aby tam padly vsechna vazeni (mohl vybrat vazeni jednodennich kurat
                // a k tomu vazeni pred porazkou). Pokud vybere hejno, bude to v podstate pravidlem.
                if (sampleList.Count > 1) {
                    // Jen pokud je v seznamu vice vzorku
                    StatisticResult statisticResult = new StatisticResult(sampleList, Flag.ALL, uniformityRange, histogramConfig, units);
                    
                    // Vypoctu rozsah vzorku
                    float weightRange = (maxWeight - minWeight) / 2.0F;

                    // Vypoctu pozadovany rozsah histogramu (rezim uz je nastaven na Range)
                    histogramConfig.Range = (int)(100.0F * weightRange / statisticResult.Average);
                    if (histogramConfig.Range == 0) {
                        histogramConfig.Range = 30;     // Radeji obnovim standardnich 30%
                    }
                }
                
                // Vypoctu celkovou statistiku
                totalStatisticResults = new WeighingResults(sampleList, uniformityRange, histogramConfig, units);

                // Rychlost vazeni musim vypocitat rucne jako prumer rychlosti ze vsech vazeni
                foreach (Flag flag in flagList) {
                    totalStatisticResults.SetWeighingSpeed(flag, CalculateAverageWeighingSpeed(flag));
                }
            }
        }

        /// <summary>
        /// Recalculate results for all weighings in the list
        /// </summary>
        public void RecalculateResults() {
            foreach (Weighing weighing in list) {
                weighing.RecalculateResults();
            }
        }

        /// <summary>
        /// Recalculate all weighings to new units
        /// </summary>
        /// <param name="units">New units</param>
        public void ChangeUnits(Units units) {
            // Pokud jsou ve vazeni pouze jedny jednotky a to ty, ktere chce, neni treba prepocet
            if (unitsList.Count == 1 && unitsList[0] == units) {
                return;     
            }

            foreach (Weighing weighing in list) {
                weighing.ChangeUnits(units);
            }
        }

        /// <summary>
        /// Check if the weighing list contains only flag NONE
        /// </summary>
        /// <returns></returns>
        public bool ContainsOnlyFlagNone() {
            foreach (Flag flag in flagList) {
                if (flag != Flag.NONE && flag != Flag.ALL) {
                    return false;       // Jiny flag nez NONE nebo ALL
                }
            }
            return true;    // Vsechny flagy jsou NONE
        }

        public DateTime GetMinDateTime(Flag flag) {
            DateTime minDateTime = DateTime.MaxValue;
            foreach (Weighing weighing in list) {
                DateTime min = weighing.GetMinDateTime(flag);
                if (min == DateTime.MinValue) {     // Fce GetMaxDateTime() vraci trosku nesmyslne MinValue
                    continue;       // Vazeni neobsahuje zadany flag, preskocim
                }
                if (min < minDateTime) {
                    minDateTime = min;
                }
            }
            return minDateTime;
        }

        public DateTime GetMaxDateTime(Flag flag) {
            DateTime maxDateTime = DateTime.MinValue;
            foreach (Weighing weighing in list) {
                DateTime max = weighing.GetMaxDateTime(flag);
                if (max == DateTime.MinValue) {
                    continue;       // Vazeni neobsahuje zadany flag, preskocim
                }
                if (max > maxDateTime) {
                    maxDateTime = max;
                }
            }
            return maxDateTime;
        }
        

    } // WeighingList
}
