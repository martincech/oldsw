﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlScaleConfig : UserControl {
        /// <summary>
        /// Scale config that is edited
        /// </summary>
        public ScaleConfig ScaleConfig { get { return scaleConfig; } }
        private ScaleConfig scaleConfig;

        /// <summary>
        /// Read-only mode
        /// </summary>
        private bool readOnly;

        private UserControlScaleConfigFiles      userControlScaleConfigFiles;
        private UserControlScaleConfigGroups     userControlScaleConfigFileGroups;
        private UserControlScaleConfigSaving     userControlScaleConfigSaving;
        private UserControlScaleConfigDisplay    userControlScaleConfigDisplay;
        private UserControlScaleConfigSounds     userControlScaleConfigSounds;
        private UserControlScaleConfigStatistics userControlScaleConfigStatistics;
        private UserControlScaleConfigCountry    userControlScaleConfigCountry;
        private UserControlScaleConfigWeighing   userControlScaleConfigWeighing;
        private UserControlScaleConfigPrinter    userControlScaleConfigPrinter;
        private UserControlScaleConfigPower      userControlScaleConfigPower;
        private UserControlScaleConfigPassword   userControlScaleConfigPassword;

        private TreeNode lastSelectedNode = null;

        /// <summary>
        /// List of created details
        /// </summary>
        private List<UserControlScaleConfigBase> listCreatedDetails = new List<UserControlScaleConfigBase>();

        public UserControlScaleConfig(bool readOnly) {
            InitializeComponent();

            this.readOnly = readOnly;

            // Zobrazim vsechny polozky
            treeViewMenu.ExpandAll();
        }

        /// <summary>
        /// Set scale config to edit
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        public void SetScaleConfig(ScaleConfig scaleConfig) {
            this.scaleConfig = scaleConfig;

            // Nastavim ve vsech controlech
            foreach (UserControlScaleConfigBase control in listCreatedDetails) {
                control.SetScaleConfig(scaleConfig);
            }
        }

        /// <summary>
        /// Add control to list
        /// </summary>
        /// <param name="control"></param>
        private void AddDetails(UserControlScaleConfigBase control) {
            groupBoxDetails.Controls.Add(control);
            control.Left = 16;
            control.Top  = 24;
            listCreatedDetails.Add(control);
        }

        /// <summary>
        /// Show specified details
        /// </summary>
        /// <param name="visibleControl"></param>
        private void ShowDetails(UserControlScaleConfigBase visibleControl) {
            // Schovam vsechny detaily krome tech, ktere mam zobrazit
            foreach (UserControlScaleConfigBase control in listCreatedDetails) {
                if (control != visibleControl) {
                    control.Hide();
                }
            }

            if (visibleControl == null) {
                // Chce pouze schovat vsechny detaily, nic nezobrazuju
                return;
            }
            
            // Zobrazim zvolene detaily
            visibleControl.Show();
            visibleControl.Redraw();
        }
        
        /// <summary>
        /// Show files
        /// </summary>
        private void ShowFiles() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigFiles == null) {
                userControlScaleConfigFiles = new UserControlScaleConfigFiles(scaleConfig, groupBoxDetails.Text, readOnly);
                AddDetails(userControlScaleConfigFiles);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigFiles);
        }
        
        /// <summary>
        /// Show file groups
        /// </summary>
        private void ShowFileGroups() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigFileGroups == null) {
                userControlScaleConfigFileGroups = new UserControlScaleConfigGroups(scaleConfig, groupBoxDetails.Text, readOnly);
                AddDetails(userControlScaleConfigFileGroups);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigFileGroups);
        }

        /// <summary>
        /// Show display
        /// </summary>
        private void ShowDisplay() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigDisplay == null) {
                userControlScaleConfigDisplay = new UserControlScaleConfigDisplay(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigDisplay);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigDisplay);
        }

        /// <summary>
        /// Show sounds
        /// </summary>
        private void ShowSounds() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigSounds == null) {
                userControlScaleConfigSounds = new UserControlScaleConfigSounds(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigSounds);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigSounds);
        }

        /// <summary>
        /// Show saving
        /// </summary>
        private void ShowSaving() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigSaving == null) {
                userControlScaleConfigSaving = new UserControlScaleConfigSaving(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigSaving);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigSaving);
        }

        /// <summary>
        /// Show statistics
        /// </summary>
        private void ShowStatistics() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigStatistics == null) {
                userControlScaleConfigStatistics = new UserControlScaleConfigStatistics(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigStatistics);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigStatistics);
        }

        /// <summary>
        /// Show weighing
        /// </summary>
        private void ShowWeighing() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigWeighing == null) {
                userControlScaleConfigWeighing = new UserControlScaleConfigWeighing(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigWeighing);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigWeighing);
        }

        /// <summary>
        /// Show printer
        /// </summary>
        private void ShowPrinter() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigPrinter == null) {
                userControlScaleConfigPrinter = new UserControlScaleConfigPrinter(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigPrinter);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigPrinter);
        }

        /// <summary>
        /// Show auto power off
        /// </summary>
        private void ShowAutoPowerOff() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigPower == null) {
                userControlScaleConfigPower = new UserControlScaleConfigPower(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigPower);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigPower);
        }

        /// <summary>
        /// Show country
        /// </summary>
        private void ShowCountry() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigCountry == null) {
                userControlScaleConfigCountry = new UserControlScaleConfigCountry(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigCountry);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigCountry);
        }

        /// <summary>
        /// Show password
        /// </summary>
        private void ShowPassword() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScaleConfigPassword == null) {
                userControlScaleConfigPassword = new UserControlScaleConfigPassword(scaleConfig, readOnly);
                AddDetails(userControlScaleConfigPassword);
            }

            // Zobrazim
            ShowDetails(userControlScaleConfigPassword);
        }

        private void treeViewMenu_BeforeCollapse(object sender, TreeViewCancelEventArgs e) {
            // Nepovolim pripadny pokus o sbaleni
            e.Cancel = true;
        }

        private void treeViewMenu_AfterSelect(object sender, TreeViewEventArgs e) {
            // Nastavim nazev group boxu podle vybrane polozky
            groupBoxDetails.Text = treeViewMenu.SelectedNode.Text;

            // Podle vybrane polozky zobrazim prislusne detaily
            if (treeViewMenu.SelectedNode.Name == "NodeConfiguration") {
                // Preskocim na Files
                treeViewMenu.SelectedNode = treeViewMenu.Nodes["NodeConfiguration"].Nodes["NodeFiles"];
                return;
            } else if (treeViewMenu.SelectedNode.Name == "NodeFiles") {
                ShowFiles();
            } else if (treeViewMenu.SelectedNode.Name == "NodeFileGroups") {
                ShowFileGroups();
            } else if (treeViewMenu.SelectedNode.Name == "NodeSaving") {
                ShowSaving();
            } else if (treeViewMenu.SelectedNode.Name == "NodeDisplay") {
                ShowDisplay();
            } else if (treeViewMenu.SelectedNode.Name == "NodeSounds") {
                ShowSounds();
            } else if (treeViewMenu.SelectedNode.Name == "NodeStatistics") {
                ShowStatistics();
            } else if (treeViewMenu.SelectedNode.Name == "NodeMaintenance") {
                // Podle smeru odkud jdu prekocim na Weighing nebo Statistics
                if (lastSelectedNode != null && lastSelectedNode.Name == "NodeCountry") {
                    // Jdu zespodu
                    treeViewMenu.SelectedNode = treeViewMenu.Nodes["NodeConfiguration"].Nodes["NodeStatistics"];
                } else {
                    // Jdu shora nebo nahodne mysi
                    treeViewMenu.SelectedNode = treeViewMenu.Nodes["NodeMaintenance"].Nodes["NodeCountry"];
                }
                return;
            } else if (treeViewMenu.SelectedNode.Name == "NodeCountry") {
                ShowCountry();
            } else if (treeViewMenu.SelectedNode.Name == "NodeWeighing") {
                ShowWeighing();
            } else if (treeViewMenu.SelectedNode.Name == "NodePrinter") {
                ShowPrinter();
            } else if (treeViewMenu.SelectedNode.Name == "NodeAutoPowerOff") {
                ShowAutoPowerOff();
            } else if (treeViewMenu.SelectedNode.Name == "NodePassword") {
                ShowPassword();
            } else {
                // Vse schovam
                ShowDetails(null);
            }

            // Zapamatuju si naposledy vybranou polozku
            lastSelectedNode = treeViewMenu.SelectedNode;
        }

        private void UserControlScaleConfig_Load(object sender, EventArgs e) {
            // Vyberu Files
            treeViewMenu.SelectedNode = treeViewMenu.Nodes["NodeConfiguration"].Nodes["NodeFiles"];
            treeViewMenu.Focus();
        }
    }
}
