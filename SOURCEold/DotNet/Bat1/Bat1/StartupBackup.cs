﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bat1 {
    /// <summary>
    /// Automatic database backup at application startup
    /// </summary>
    public static class StartupBackup {
        /// <summary>
        /// Name of the direcotry for startup backup
        /// </summary>
        const string DIRECTORY = "StartupBackup";

        /// <summary>
        /// Full backup directory path
        /// </summary>
        private static string BackupDirectory {
            get { return Program.DataFolder + @"\" + DIRECTORY; }
        }

        /// <summary>
        /// Full backup file path
        /// </summary>
        private static string BackupFileName {
            get { return BackupDirectory + @"\" + Program.DatabaseFileName; }
        }

        /// <summary>
        /// Execute backup
        /// </summary>
        /// <param name="integrityChecked">If true, database integrity has been already checked and doesn't need to be checked again</param>
        public static void Backup(bool integrityChecked) {
            // Pokud jeste neni zkontrolovana integrita, zkontroluju, zda je databaze v poradku
            if (!integrityChecked && !Program.Database.CheckIntegrity()) {
                // Korupce dat, ponecham predchozi funkcni zalohu
                return;
            }

            // Pokud adresar neexistuje, vytvorim ho
            if (!Directory.Exists(BackupDirectory)) {
                Directory.CreateDirectory(BackupDirectory);
            }

            // Databaze je v poradku, zkopiruju ji
            Program.Database.Copy(BackupFileName);

            // Nastavim datum posledniho zapisu do souboru rucne na dnesek. Tento udaj se zachova i pri kopirovani jinam.
            System.IO.File.SetLastWriteTime(BackupFileName, DateTime.Now);
        }

        /// <summary>
        /// Execute backup with database integrity check
        /// </summary>
        public static void Backup() {
            Backup(false);
        }

        
        /// <summary>
        /// Check if the backup exists
        /// </summary>
        /// <returns>True if exists</returns>
        public static bool Exists() {
            if (!Directory.Exists(BackupDirectory)) {
                return false;
            }
            if (!System.IO.File.Exists(BackupFileName)) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Get date and time of the backup
        /// </summary>
        /// <returns>DateTime</returns>
        public static DateTime Created() {
            // Zkontroluju, zda zaloha existuje
            if (!Exists()) {
                throw new Exception("Backup doesn't exist");
            }
            // Beru datum posledniho zapisu, ktery se zachovava i pri kopirovani souboru jinam.
            return System.IO.File.GetLastWriteTime(BackupFileName);
        }
        
        /// <summary>
        /// Restore database from backup
        /// </summary>
        public static void Restore() {
            // Zkontroluju, zda zaloha existuje
            if (!Exists()) {
                throw new Exception("Backup doesn't exist");
            }

            // Obnovim, vcetne prepisu
            System.IO.File.Copy(BackupFileName, Program.FullDatabaseFileName, true);
        }
    }
}
