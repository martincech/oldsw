﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.ScaleStatistics;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlScaleConfigStatistics : UserControlScaleConfigBase {

        public UserControlScaleConfigStatistics(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Read-only
            this.readOnly = readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        private void SetUnits() {
            // Nastavim minimalni a maximalni hodnoty podle jednotek
            numericUpDownHistogramStep.Minimum = (decimal)ConvertWeight.MinWeight(scaleConfig.Units.Units);
            numericUpDownHistogramStep.Maximum = (decimal)ConvertWeight.MaxWeight(scaleConfig.Units.Units, scaleConfig.Units.WeighingCapacity);
            switch (scaleConfig.Units.Units) {
                case Units.G:
                    numericUpDownHistogramStep.DecimalPlaces = 0;
                    numericUpDownHistogramStep.Increment     = 1;
                    break;

                default:    // KG nebo LB
                    numericUpDownHistogramStep.DecimalPlaces = 3;
                    numericUpDownHistogramStep.Increment     = (decimal)0.001;
                    break;
            }
        }

        private void RedrawUnits() {
            labelHistogramStepUnits.Text = ConvertWeight.UnitsToString(scaleConfig.Units.Units);
        }

        private void EnableHistogramRange(bool enable) {
            labelHistogramRange.Enabled         = enable;
            numericUpDownHistogramRange.Enabled = enable;
            labelHistogramRangePercent.Enabled  = enable;
        }

        private void EnableHistogramStep(bool enable) {
            labelHistogramStep.Enabled         = enable;
            numericUpDownHistogramStep.Enabled = enable;
            labelHistogramStepUnits.Enabled    = enable;
        }
        
        private void SetHistogramMode() {
            bool rangeMode = (HistogramMode)comboBoxHistogramMode.SelectedIndex == HistogramMode.RANGE;
            EnableHistogramRange(rangeMode);
            EnableHistogramStep(!rangeMode);
        }

        public override void Redraw() {
            isLoading = true;
            
            try {
                // Nastavim minimalni a maximalni hodnoty podle jednotek
                SetUnits();

                numericUpDownUniformityRange.Value = scaleConfig.StatisticConfig.UniformityRange;
                comboBoxHistogramMode.SelectedIndex = (int)scaleConfig.StatisticConfig.Histogram.Mode;
                numericUpDownHistogramRange.Value   = scaleConfig.StatisticConfig.Histogram.Range;
                numericUpDownHistogramStep.Text     = DisplayFormat.RoundWeight(scaleConfig.StatisticConfig.Histogram.Step, scaleConfig.Units.Units);
                RedrawUnits();

                // Povoleni zobrazeni na zaklade nastaveni
                SetHistogramMode();
            } finally {
                isLoading = false;
            }
        }

        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.StatisticConfig.UniformityRange = (int)numericUpDownUniformityRange.Value;
            scaleConfig.StatisticConfig.Histogram.Mode  = (HistogramMode)comboBoxHistogramMode.SelectedIndex;
            SetHistogramMode();
            scaleConfig.StatisticConfig.Histogram.Range = (int)numericUpDownHistogramRange.Value;
            scaleConfig.StatisticConfig.Histogram.Step  = (double)numericUpDownHistogramStep.Value;
        }

        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e) {
            ControlsToConfig();
        }

    }
}
