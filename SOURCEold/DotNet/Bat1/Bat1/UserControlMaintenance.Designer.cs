﻿namespace Bat1 {
    partial class UserControlMaintenance {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlMaintenance));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageBackup = new System.Windows.Forms.TabPage();
            this.userControlBackup = new Bat1.UserControlBackup();
            this.tabPageOptions = new System.Windows.Forms.TabPage();
            this.userControlOptions = new Bat1.UserControlOptions();
            this.tabPageDiagnostics = new System.Windows.Forms.TabPage();
            this.userControlDiagnostics = new Bat1.UserControlDiagnostics();
            this.tabPageAbout = new System.Windows.Forms.TabPage();
            this.userControlAbout = new Bat1.UserControlAbout();
            this.tabControl.SuspendLayout();
            this.tabPageBackup.SuspendLayout();
            this.tabPageOptions.SuspendLayout();
            this.tabPageDiagnostics.SuspendLayout();
            this.tabPageAbout.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.AccessibleDescription = null;
            this.tabControl.AccessibleName = null;
            resources.ApplyResources(this.tabControl, "tabControl");
            this.tabControl.BackgroundImage = null;
            this.tabControl.Controls.Add(this.tabPageBackup);
            this.tabControl.Controls.Add(this.tabPageOptions);
            this.tabControl.Controls.Add(this.tabPageDiagnostics);
            this.tabControl.Controls.Add(this.tabPageAbout);
            this.tabControl.Font = null;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            // 
            // tabPageBackup
            // 
            this.tabPageBackup.AccessibleDescription = null;
            this.tabPageBackup.AccessibleName = null;
            resources.ApplyResources(this.tabPageBackup, "tabPageBackup");
            this.tabPageBackup.BackgroundImage = null;
            this.tabPageBackup.Controls.Add(this.userControlBackup);
            this.tabPageBackup.Font = null;
            this.tabPageBackup.Name = "tabPageBackup";
            this.tabPageBackup.UseVisualStyleBackColor = true;
            // 
            // userControlBackup
            // 
            this.userControlBackup.AccessibleDescription = null;
            this.userControlBackup.AccessibleName = null;
            resources.ApplyResources(this.userControlBackup, "userControlBackup");
            this.userControlBackup.BackgroundImage = null;
            this.userControlBackup.Font = null;
            this.userControlBackup.Name = "userControlBackup";
            // 
            // tabPageOptions
            // 
            this.tabPageOptions.AccessibleDescription = null;
            this.tabPageOptions.AccessibleName = null;
            resources.ApplyResources(this.tabPageOptions, "tabPageOptions");
            this.tabPageOptions.BackgroundImage = null;
            this.tabPageOptions.Controls.Add(this.userControlOptions);
            this.tabPageOptions.Font = null;
            this.tabPageOptions.Name = "tabPageOptions";
            this.tabPageOptions.UseVisualStyleBackColor = true;
            // 
            // userControlOptions
            // 
            this.userControlOptions.AccessibleDescription = null;
            this.userControlOptions.AccessibleName = null;
            resources.ApplyResources(this.userControlOptions, "userControlOptions");
            this.userControlOptions.BackgroundImage = null;
            this.userControlOptions.Font = null;
            this.userControlOptions.Name = "userControlOptions";
            // 
            // tabPageDiagnostics
            // 
            this.tabPageDiagnostics.AccessibleDescription = null;
            this.tabPageDiagnostics.AccessibleName = null;
            resources.ApplyResources(this.tabPageDiagnostics, "tabPageDiagnostics");
            this.tabPageDiagnostics.BackgroundImage = null;
            this.tabPageDiagnostics.Controls.Add(this.userControlDiagnostics);
            this.tabPageDiagnostics.Font = null;
            this.tabPageDiagnostics.Name = "tabPageDiagnostics";
            this.tabPageDiagnostics.UseVisualStyleBackColor = true;
            // 
            // userControlDiagnostics
            // 
            this.userControlDiagnostics.AccessibleDescription = null;
            this.userControlDiagnostics.AccessibleName = null;
            resources.ApplyResources(this.userControlDiagnostics, "userControlDiagnostics");
            this.userControlDiagnostics.BackgroundImage = null;
            this.userControlDiagnostics.Font = null;
            this.userControlDiagnostics.Name = "userControlDiagnostics";
            // 
            // tabPageAbout
            // 
            this.tabPageAbout.AccessibleDescription = null;
            this.tabPageAbout.AccessibleName = null;
            resources.ApplyResources(this.tabPageAbout, "tabPageAbout");
            this.tabPageAbout.BackgroundImage = null;
            this.tabPageAbout.Controls.Add(this.userControlAbout);
            this.tabPageAbout.Font = null;
            this.tabPageAbout.Name = "tabPageAbout";
            this.tabPageAbout.UseVisualStyleBackColor = true;
            // 
            // userControlAbout
            // 
            this.userControlAbout.AccessibleDescription = null;
            this.userControlAbout.AccessibleName = null;
            resources.ApplyResources(this.userControlAbout, "userControlAbout");
            this.userControlAbout.BackgroundImage = null;
            this.userControlAbout.Font = null;
            this.userControlAbout.Name = "userControlAbout";
            // 
            // UserControlMaintenance
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tabControl);
            this.Font = null;
            this.Name = "UserControlMaintenance";
            this.tabControl.ResumeLayout(false);
            this.tabPageBackup.ResumeLayout(false);
            this.tabPageOptions.ResumeLayout(false);
            this.tabPageDiagnostics.ResumeLayout(false);
            this.tabPageAbout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageBackup;
        private System.Windows.Forms.TabPage tabPageOptions;
        private System.Windows.Forms.TabPage tabPageDiagnostics;
        private System.Windows.Forms.TabPage tabPageAbout;
        private UserControlAbout userControlAbout;
        private UserControlOptions userControlOptions;
        private UserControlBackup userControlBackup;
        private UserControlDiagnostics userControlDiagnostics;
    }
}
