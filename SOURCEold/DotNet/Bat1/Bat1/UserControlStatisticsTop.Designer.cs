﻿namespace Bat1 {
    partial class UserControlStatisticsTop {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlStatisticsTop));
            this.buttonSelect = new System.Windows.Forms.Button();
            this.comboBoxFlag = new System.Windows.Forms.ComboBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.labelFrom = new System.Windows.Forms.Label();
            this.labelTo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonSelect
            // 
            this.buttonSelect.AccessibleDescription = null;
            this.buttonSelect.AccessibleName = null;
            resources.ApplyResources(this.buttonSelect, "buttonSelect");
            this.buttonSelect.BackgroundImage = null;
            this.buttonSelect.Font = null;
            this.buttonSelect.Name = "buttonSelect";
            this.toolTip1.SetToolTip(this.buttonSelect, resources.GetString("buttonSelect.ToolTip"));
            this.buttonSelect.UseVisualStyleBackColor = true;
            // 
            // comboBoxFlag
            // 
            this.comboBoxFlag.AccessibleDescription = null;
            this.comboBoxFlag.AccessibleName = null;
            resources.ApplyResources(this.comboBoxFlag, "comboBoxFlag");
            this.comboBoxFlag.BackgroundImage = null;
            this.comboBoxFlag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFlag.Font = null;
            this.comboBoxFlag.FormattingEnabled = true;
            this.comboBoxFlag.Name = "comboBoxFlag";
            this.toolTip1.SetToolTip(this.comboBoxFlag, resources.GetString("comboBoxFlag.ToolTip"));
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.AccessibleDescription = null;
            this.dateTimePickerTo.AccessibleName = null;
            resources.ApplyResources(this.dateTimePickerTo, "dateTimePickerTo");
            this.dateTimePickerTo.BackgroundImage = null;
            this.dateTimePickerTo.CalendarFont = null;
            this.dateTimePickerTo.CustomFormat = null;
            this.dateTimePickerTo.Font = null;
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.ShowCheckBox = true;
            this.toolTip1.SetToolTip(this.dateTimePickerTo, resources.GetString("dateTimePickerTo.ToolTip"));
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.AccessibleDescription = null;
            this.dateTimePickerFrom.AccessibleName = null;
            resources.ApplyResources(this.dateTimePickerFrom, "dateTimePickerFrom");
            this.dateTimePickerFrom.BackgroundImage = null;
            this.dateTimePickerFrom.CalendarFont = null;
            this.dateTimePickerFrom.CustomFormat = null;
            this.dateTimePickerFrom.Font = null;
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.ShowCheckBox = true;
            this.toolTip1.SetToolTip(this.dateTimePickerFrom, resources.GetString("dateTimePickerFrom.ToolTip"));
            // 
            // labelFrom
            // 
            this.labelFrom.AccessibleDescription = null;
            this.labelFrom.AccessibleName = null;
            resources.ApplyResources(this.labelFrom, "labelFrom");
            this.labelFrom.Font = null;
            this.labelFrom.Name = "labelFrom";
            this.toolTip1.SetToolTip(this.labelFrom, resources.GetString("labelFrom.ToolTip"));
            // 
            // labelTo
            // 
            this.labelTo.AccessibleDescription = null;
            this.labelTo.AccessibleName = null;
            resources.ApplyResources(this.labelTo, "labelTo");
            this.labelTo.Font = null;
            this.labelTo.Name = "labelTo";
            this.toolTip1.SetToolTip(this.labelTo, resources.GetString("labelTo.ToolTip"));
            // 
            // UserControlStatisticsTop
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.labelFrom);
            this.Controls.Add(this.dateTimePickerTo);
            this.Controls.Add(this.dateTimePickerFrom);
            this.Controls.Add(this.labelTo);
            this.Controls.Add(this.buttonSelect);
            this.Controls.Add(this.comboBoxFlag);
            this.Font = null;
            this.Name = "UserControlStatisticsTop";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSelect;
        private System.Windows.Forms.ComboBox comboBoxFlag;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label labelFrom;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label labelTo;
    }
}
