﻿namespace Bat1 {
    partial class FormEditFlock {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEditFlock));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.groupBoxFiles = new System.Windows.Forms.GroupBox();
            this.listBoxFiles = new System.Windows.Forms.ListBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonDeleteFile = new System.Windows.Forms.Button();
            this.buttonAddFile = new System.Windows.Forms.Button();
            this.groupBoxCurves = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxCurveFemales = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxCurveDefault = new System.Windows.Forms.ComboBox();
            this.panelNote = new System.Windows.Forms.Panel();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelDay = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBoxStart = new System.Windows.Forms.GroupBox();
            this.numericUpDownStartDay = new System.Windows.Forms.NumericUpDown();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonMore = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBoxFiles.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.groupBoxCurves.SuspendLayout();
            this.panelNote.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxStart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartDay)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Controls.Add(this.buttonCancel);
            this.flowLayoutPanel1.Controls.Add(this.buttonOk);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // buttonCancel
            // 
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // buttonOk
            // 
            resources.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // groupBoxFiles
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBoxFiles, 2);
            this.groupBoxFiles.Controls.Add(this.listBoxFiles);
            this.groupBoxFiles.Controls.Add(this.flowLayoutPanel3);
            resources.ApplyResources(this.groupBoxFiles, "groupBoxFiles");
            this.groupBoxFiles.Name = "groupBoxFiles";
            this.groupBoxFiles.TabStop = false;
            // 
            // listBoxFiles
            // 
            resources.ApplyResources(this.listBoxFiles, "listBoxFiles");
            this.listBoxFiles.FormattingEnabled = true;
            this.listBoxFiles.Name = "listBoxFiles";
            this.listBoxFiles.KeyDown += new System.Windows.Forms.KeyEventHandler(this.listBoxFiles_KeyDown);
            // 
            // flowLayoutPanel3
            // 
            resources.ApplyResources(this.flowLayoutPanel3, "flowLayoutPanel3");
            this.flowLayoutPanel3.Controls.Add(this.buttonDeleteFile);
            this.flowLayoutPanel3.Controls.Add(this.buttonAddFile);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            // 
            // buttonDeleteFile
            // 
            resources.ApplyResources(this.buttonDeleteFile, "buttonDeleteFile");
            this.buttonDeleteFile.Name = "buttonDeleteFile";
            this.buttonDeleteFile.UseVisualStyleBackColor = true;
            this.buttonDeleteFile.Click += new System.EventHandler(this.buttonDeleteFile_Click);
            // 
            // buttonAddFile
            // 
            resources.ApplyResources(this.buttonAddFile, "buttonAddFile");
            this.buttonAddFile.Name = "buttonAddFile";
            this.buttonAddFile.UseVisualStyleBackColor = true;
            this.buttonAddFile.Click += new System.EventHandler(this.buttonAddFile_Click);
            // 
            // groupBoxCurves
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBoxCurves, 2);
            this.groupBoxCurves.Controls.Add(this.label1);
            this.groupBoxCurves.Controls.Add(this.comboBoxCurveFemales);
            this.groupBoxCurves.Controls.Add(this.label2);
            this.groupBoxCurves.Controls.Add(this.comboBoxCurveDefault);
            resources.ApplyResources(this.groupBoxCurves, "groupBoxCurves");
            this.groupBoxCurves.Name = "groupBoxCurves";
            this.groupBoxCurves.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // comboBoxCurveFemales
            // 
            resources.ApplyResources(this.comboBoxCurveFemales, "comboBoxCurveFemales");
            this.comboBoxCurveFemales.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCurveFemales.FormattingEnabled = true;
            this.comboBoxCurveFemales.Name = "comboBoxCurveFemales";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // comboBoxCurveDefault
            // 
            resources.ApplyResources(this.comboBoxCurveDefault, "comboBoxCurveDefault");
            this.comboBoxCurveDefault.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCurveDefault.FormattingEnabled = true;
            this.comboBoxCurveDefault.Name = "comboBoxCurveDefault";
            // 
            // panelNote
            // 
            resources.ApplyResources(this.panelNote, "panelNote");
            this.tableLayoutPanel1.SetColumnSpan(this.panelNote, 2);
            this.panelNote.Controls.Add(this.textBoxNote);
            this.panelNote.Controls.Add(this.label4);
            this.panelNote.Name = "panelNote";
            // 
            // textBoxNote
            // 
            resources.ApplyResources(this.textBoxNote, "textBoxNote");
            this.textBoxNote.Name = "textBoxNote";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // labelDay
            // 
            resources.ApplyResources(this.labelDay, "labelDay");
            this.labelDay.Name = "labelDay";
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.groupBoxFiles, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxCurves, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.panelNote, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxStart, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonMore, 0, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // groupBoxStart
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBoxStart, 2);
            this.groupBoxStart.Controls.Add(this.numericUpDownStartDay);
            this.groupBoxStart.Controls.Add(this.dateTimePickerFrom);
            this.groupBoxStart.Controls.Add(this.label3);
            this.groupBoxStart.Controls.Add(this.labelDay);
            resources.ApplyResources(this.groupBoxStart, "groupBoxStart");
            this.groupBoxStart.Name = "groupBoxStart";
            this.groupBoxStart.TabStop = false;
            // 
            // numericUpDownStartDay
            // 
            resources.ApplyResources(this.numericUpDownStartDay, "numericUpDownStartDay");
            this.numericUpDownStartDay.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numericUpDownStartDay.Name = "numericUpDownStartDay";
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            resources.ApplyResources(this.dateTimePickerFrom, "dateTimePickerFrom");
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // buttonMore
            // 
            resources.ApplyResources(this.buttonMore, "buttonMore");
            this.buttonMore.Name = "buttonMore";
            this.buttonMore.UseVisualStyleBackColor = true;
            this.buttonMore.Click += new System.EventHandler(this.buttonMore_Click);
            // 
            // FormEditFlock
            // 
            this.AcceptButton = this.buttonOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimizeBox = false;
            this.Name = "FormEditFlock";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBoxFiles.ResumeLayout(false);
            this.groupBoxFiles.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.groupBoxCurves.ResumeLayout(false);
            this.groupBoxCurves.PerformLayout();
            this.panelNote.ResumeLayout(false);
            this.panelNote.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBoxStart.ResumeLayout(false);
            this.groupBoxStart.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStartDay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Panel panelNote;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCurveDefault;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.Label labelDay;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxFiles;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.Button buttonDeleteFile;
        private System.Windows.Forms.Button buttonAddFile;
        private System.Windows.Forms.GroupBox groupBoxCurves;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxCurveFemales;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox groupBoxStart;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox listBoxFiles;
        private System.Windows.Forms.NumericUpDown numericUpDownStartDay;
        private System.Windows.Forms.Button buttonMore;
    }
}