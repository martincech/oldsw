﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// Sorted list of indexes
    /// </summary>
    public class SortedIndexList : List<int> {
        /// <summary>
        /// Add index to the list
        /// </summary>
        /// <param name="index">Index to add</param>
        public new void Add(int index) {
            // Pokud index jeste v seznamu neni, pouze ho pridam a setridim seznam
            if (!Contains(index)) {
                base.Add(index);
                Sort();
                return;
            }

            // Index uz v seznamu je, musim udelat na nove vkladany index misto
            MakeRoom(index);

            // Pridam novy index a setridim
            base.Add(index);
            Sort();
        }

        /// <summary>
        /// Remove index from the list and move all following indexes up by 1
        /// </summary>
        /// <param name="index">Index to remove</param>
        public void RemoveAndMove(int index) {
            // Pokud seznam index obsahuje, smazu ho
            if (Contains(index)) {
                Remove(index);
            }

            // Vsechny indexy za smazanym indexem posunu o 1 nahoru, tridit seznam znovu nemusim
            for (int i = 0; i < Count; i++) {
                if (this[i] > index) {
                    this[i] = this[i] - 1;
                }
            }
        }

        /// <summary>
        /// Make room for new index
        /// </summary>
        /// <param name="index">New index</param>
        public void MakeRoom(int index) {
            // U prazdneho seznamu neni treba delat misto
            if (Count == 0) {
                return;
            }
            
            // Pokud je novy index vetsi nez posledni index v seznamu, nemusim delat nic
            if (index > this[Count - 1]) {
                return;
            }

            // Vsechny indexy >= newIndex posunu o 1 dolu
            for (int i = 0; i < Count; i++) {
                if (this[i] >= index) {
                    this[i] = this[i] + 1;
                }
            }
        }

        /// <summary>
        /// Replace index with a new value
        /// </summary>
        /// <param name="oldIndex">Index to change</param>
        /// <param name="newIndex">New index value</param>
        public void Replace(int oldIndex, int newIndex) {
            // U prazdneho seznamu neni treba delat nic
            if (Count == 0) {
                return;
            }
            
            bool containsOld = Contains(oldIndex);
            bool containsNew = Contains(newIndex);
            
            // Pokud seznam obsahuje starou i novou hodnotu, nedelam nic. Pozice by se akorat prohodily
            // a nasledne setridily, tj. stav by byl stejny jako puvodne.
            if (containsOld && containsNew) {
                return;
            }

            // Smazu starou hodnotu a posunu cely seznam
            RemoveAndMove(oldIndex);

            if (containsOld) {
                // Pokud seznam obsahoval starou hodnotu, vlozim tam novou
                Add(newIndex);
            } else {
                // Seznam puvodni hodnotu neobsahoval, udelam pouze misto na novou hodnotu
                MakeRoom(newIndex);
            }
        }

    }
}
