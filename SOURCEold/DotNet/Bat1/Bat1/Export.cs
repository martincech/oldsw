﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    /// <summary>
    /// Export data to CSV of Bat1 export format
    /// </summary>
    public class Export {
        private Control parentControl;

        /// <summary>
        /// BAT1 Filter text for Open/Save dialogs
        /// </summary>
        public static readonly string FilterBat1 = String.Format(Properties.Resources.FILE_FORMAT_BAT1_EXPORT, Program.ApplicationName) + " (*." + FileExtension.EXPORT_BAT1 + ")|*." + FileExtension.EXPORT_BAT1;
        
        /// <summary>
        /// CSV Filter text for Open/Save dialogs
        /// </summary>
        private readonly string FilterCsv = Properties.Resources.FILE_FORMAT_CSV + " (*." + FileExtension.EXPORT_CSV + ")|*." + FileExtension.EXPORT_CSV;

        public Export(Control parentControl) {
            this.parentControl = parentControl;
        }

        /// <summary>
        /// Export list of weighings
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Export only samples with specified flag</param>
        /// <param name="enableBat1Format">Enable export to Bat1 format</param>
        public void ExportWeighingList(List<Weighing> weighingList, Flag flag, bool enableBat1Format) {
            // Zeptam se na format exportu
            FormExportFormat form = new FormExportFormat(enableBat1Format);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Zeptam se na jmeno souboru
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            if (form.ExportFormat == ExportFormat.BAT1) {
                saveFileDialog.DefaultExt = FileExtension.EXPORT_BAT1;
                saveFileDialog.Filter     = FilterBat1;
            } else {
                saveFileDialog.DefaultExt = FileExtension.EXPORT_CSV;
                saveFileDialog.Filter     = FilterCsv;
            }
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            
            // Exportuju
            Cursor.Current = Cursors.WaitCursor;
            parentControl.Refresh();
            try {
                if (form.ExportFormat == ExportFormat.BAT1) {
                    // U formatu Bat1 exportuju vzdy vsechny flagy ve vazeni
                    new ExportBat1(saveFileDialog.FileName).ExportWeighingList(weighingList);
                } else {
                    // Do CSV pouze vybrany flag
                    new ExportCsv(saveFileDialog.FileName).ExportWeighingList(weighingList, flag);
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Export one weighing
        /// </summary>
        /// <param name="weighing">Weighing</param>
        /// <param name="flag">Export only samples with specified flag</param>
        /// <param name="enableBat1Format">Enable export to Bat1 format</param>
        public void ExportWeighing(Weighing weighing, Flag flag, bool enableBat1Format) {
            List<Weighing> weighingList = new List<Weighing>();
            weighingList.Add(weighing);
            ExportWeighingList(weighingList, flag, enableBat1Format);
        }

        /// <summary>
        /// Export table with statistics to CSV file
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        /// <param name="flag">Export only samples with specified flag</param>
        /// <param name="isFlocks">True if exporting flocks table</param>
        public void ExportStatisticsTable(WeighingList weighingList, Flag flag, bool isFlocks) {
            // Zeptam se na jmeno souboru
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = FileExtension.EXPORT_CSV;
            saveFileDialog.Filter     = FilterCsv;
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            
            // Exportuju
            Cursor.Current = Cursors.WaitCursor;
            parentControl.Refresh();
            try {
                new ExportCsv(saveFileDialog.FileName).ExportStatisticsTable(weighingList, flag, isFlocks);
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Export scale config
        /// </summary>
        /// <param name="scaleConfig">ScaleConfig to export</param>
        /// <param name="name">Predefined config name</param>
        public void ExportScaleConfig(ScaleConfig scaleConfig, string name) {
            // Zeptam se na jmeno souboru
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = FileExtension.EXPORT_BAT1;
            saveFileDialog.Filter     = Export.FilterBat1;
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            
            // Exportuju
            Cursor.Current = Cursors.WaitCursor;
            parentControl.Refresh();
            try {
                new ExportBat1(saveFileDialog.FileName).ExportScaleConfig(scaleConfig, name);
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Import scale config
        /// </summary>
        /// <returns>Imported ScaleConfig or null</returns>
        public ScaleConfig ImportScaleConfig() {
            // Zeptam se na jmeno souboru
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.DefaultExt = FileExtension.EXPORT_BAT1;
            openFileDialog.Filter     = FilterBat1;
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return null;
            }
            
            // Importuju
            Cursor.Current = Cursors.WaitCursor;
            parentControl.Refresh();
            try {
                return new ExportBat1(openFileDialog.FileName).ImportScaleConfig();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

    }
}
