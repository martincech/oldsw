﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    /// <summary>
    /// Setup table
    /// </summary>
    public class DatabaseTableSetup : DatabaseTable {

        // Tabulka s nastavenim programu.
        
        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableSetup(DatabaseFactory factory)
            : base("Setup", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "MajorVersion "         + factory.TypeSmallInt + ","     // Major cislo verze SW (1.xx.xx.xx)
                 + "MinorVersion "         + factory.TypeSmallInt + ","     // Minor cislo verze SW (x.01.xx.xx)
                 + "DatabaseVersion "      + factory.TypeSmallInt + ","     // Cislo verze databaze SW (x.xx.01.xx)
                 + "BuildVersion "         + factory.TypeSmallInt + ","     // Cislo buildu SW (x.xx.xx.01)
                 + "Language "             + factory.TypeSmallInt + ","     // Jazyk SW
                 + "ManualUnits "          + factory.TypeByte + ","         // Naposledy pouzite jednotky u rucniho zadavani vysledku
                 + "ScaleVersion6ComPort " + factory.TypeByte + ","         // COM port stare verze vah
                 + "ScaleVersion6Units "   + factory.TypeByte + ","         // Jednotky stare verze vah
                 + "CurveUnits "           + factory.TypeByte + ","         // Naposledy pouzite jednotky pri zadavani rustove krivky
                 + "ExportFormat "         + factory.TypeByte + ","         // Format exportniho souboru
                 + "CsvTemplate "          + factory.TypeByte + ","         // Prednastaveny set parametru CSV souboru
                 + "CsvDelimiter "         + factory.TypeByte + ","         // Oddelovac v CSV souboru
                 + "CsvDecimalSeparator "  + factory.TypeByte + ","         // Desetinna carka v CSV souboru
                 + "CsvEncoding "          + factory.TypeByte               // Kodovani CSV souboru
                 + ")");
            
            // Zaroven vyplnim default hodnoty
            SetDefaults();
        }

        /// <summary>
        /// Set default values
        /// </summary>
        private void SetDefaults() {
            Save(new Setup());          // Konstruktor zaroven vytvori default parametry
        }
        
        /// <summary>
        /// Update table from version 7.0.0 to 7.0.1.0
        /// </summary>
        private void UpdateToVersion_7_0_1_0() {
            // Ve verzi 7.0.1.0 jsem pridal sloupec verze databaze a jazyka SW
            if (ColumnExists("DatabaseVersion")) {
                return;     // Neni treba update
            }

            // Pridam sloupec DatabaseVersion
            AddColumn("DatabaseVersion", factory.TypeSmallInt);

            // Pridam sloupec Language
            AddColumn("Language", factory.TypeSmallInt);

            // V tabulce Setup verze 7.0.0 (tj. jeste bez verze databaze) jsou jeste sloupce:
            // UpdateScaleConfigFiles, UpdateScaleConfigCountry, UpdateScaleConfigPassword a UpdateScaleConfigOther
            // Tyto sloupce v tabulce ponecham, z duvodu sloziteho mazani sloupcu v SQLite
        }
        
        /// <summary>
        /// Update table definition
        /// </summary>
        public void Update() {
            UpdateToVersion_7_0_1_0();
        }

        /// <summary>
        /// Check if the table exists and if it contains just one row
        /// </summary>
        public void Check() {
            // Zkontroluju, zda tabulka existuje
            if (!Exists()) {
                Create();
                return;
            }

            // Tabulka musi obsahovat prave 1 radek
            if (RowCount() != 1) {
                // Vytvorim tabulku znovu a vyplnim default hodnoty
                // Radsi vytvorim tabulku znovu, kdyby byla databaze stara
                DropIfExists();     // Pokud uz tabulka existuje, smazu
                Create();           // Vytvorim a zaroven vyplnim defaults
                return;
            }
        }

        /// <summary>
        /// Add version data to a command
        /// </summary>
        private void AddVersionToCommand(DbCommand command) {
            command.Parameters.Add(factory.CreateParameter("@MajorVersion",    (short) SwVersion.MAJOR));
            command.Parameters.Add(factory.CreateParameter("@MinorVersion",    (short) SwVersion.MINOR));
            command.Parameters.Add(factory.CreateParameter("@DatabaseVersion", (short) SwVersion.DATABASE));
            command.Parameters.Add(factory.CreateParameter("@BuildVersion",    (short) SwVersion.BUILD));
        }

        /// <summary>
        /// Save version data to database
        /// </summary>
        public void SaveVersion() {
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "MajorVersion = @MajorVersion, "
                                                           + "MinorVersion = @MinorVersion, "
                                                           + "DatabaseVersion = @DatabaseVersion, "
                                                           + "BuildVersion = @BuildVersion")) {
                command.Parameters.Clear();
                AddVersionToCommand(command);
                command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Save setup to database including version info
        /// </summary>
        /// <param name="setup">Setup to save</param>
        public void Save(Setup setup) {
            // Smazu vsechny dosavadni radky v tabulce
            Clear();

            // Pridam novy radek s default hodnotami
            // Pozor, musim zadat i nazvy sloupcu! Behem beta testovani jsem mel v tabulce dalsi sloupce, ktere uz nepouzivam,
            // ale v ostre verzi jsem je ponechal. V databazich nekterych uzivatelu se jeste muzou pouzivat z dob beta testovani.
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " (MajorVersion, MinorVersion, DatabaseVersion, BuildVersion, Language, ManualUnits, "
                                                           + "ScaleVersion6ComPort, ScaleVersion6Units, CurveUnits, ExportFormat, CsvTemplate, CsvDelimiter, "
                                                           + "CsvDecimalSeparator, CsvEncoding) "
                                                           + "VALUES (@MajorVersion, @MinorVersion, @DatabaseVersion, @BuildVersion, @Language, @ManualUnits, "
                                                           + "@ScaleVersion6ComPort, @ScaleVersion6Units, @CurveUnits, @ExportFormat, @CsvTemplate, @CsvDelimiter, "
                                                           + "@CsvDecimalSeparator, @CsvEncoding)")) {
                command.Parameters.Clear();
                AddVersionToCommand(command);
                command.Parameters.Add(factory.CreateParameter("@Language",             (short)setup.Language));
                command.Parameters.Add(factory.CreateParameter("@ManualUnits",          (byte) setup.ManualUnits));
                command.Parameters.Add(factory.CreateParameter("@ScaleVersion6ComPort", (byte) setup.ScaleVersion6ComPort));
                command.Parameters.Add(factory.CreateParameter("@ScaleVersion6Units",   (byte) setup.ScaleVersion6Units));
                command.Parameters.Add(factory.CreateParameter("@CurveUnits",           (byte) setup.CurveUnits));
                command.Parameters.Add(factory.CreateParameter("@ExportFormat",         (byte) setup.ExportFormat));
                command.Parameters.Add(factory.CreateParameter("@CsvTemplate",          (byte) setup.CsvConfig.Template));
                command.Parameters.Add(factory.CreateParameter("@CsvDelimiter",         (byte) setup.CsvConfig.Delimiter));
                command.Parameters.Add(factory.CreateParameter("@CsvDecimalSeparator",  (byte) setup.CsvConfig.DecimalSeparator));
                command.Parameters.Add(factory.CreateParameter("@CsvEncoding",          (byte) setup.CsvConfig.Encoding));
                command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Load setup from database
        /// </summary>
        /// <returns>Setup instance</returns>
        public Setup Load() {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName)) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.HasRows) {
                        throw new Exception("Setup doesn't exist");
                    }
                    Setup setup = new Setup();
                    
                    reader.Read();
                    
                    // Jazyk
                    if (reader["Language"] == DBNull.Value) {
                        setup.Language = SwLanguage.UNDEFINED;      // Muze byt null, tento sloupec jsem pridaval v upgradu
                    } else {
                        setup.Language = (SwLanguage)(short)reader["Language"];
                        if (setup.Language < 0 || setup.Language >= SwLanguage._COUNT) {
                            setup.Language = SwLanguage.UNDEFINED;      // Neplatna hodnota
                        }
                    }

                    // Dalsi parametry
                    setup.ManualUnits                = (Units)(byte)       reader["ManualUnits"];
                    setup.ScaleVersion6ComPort       = (byte)              reader["ScaleVersion6ComPort"];
                    setup.ScaleVersion6Units         = (Units)(byte)       reader["ScaleVersion6Units"];
                    setup.CurveUnits                 = (Units)(byte)       reader["CurveUnits"];
                    setup.ExportFormat               = (ExportFormat)(byte)reader["ExportFormat"];
                    setup.CsvConfig.Template         = (CsvTemplate)(byte) reader["CsvTemplate"];
                    setup.CsvConfig.Delimiter        = (CsvDelimiter)(byte)reader["CsvDelimiter"];
                    setup.CsvConfig.DecimalSeparator = (CsvDecimalSeparator)(byte) reader["CsvDecimalSeparator"];
                    setup.CsvConfig.Encoding         = (CsvEncoding)(byte) reader["CsvEncoding"];

                    return setup;
                }
            }
        }

        /// <summary>
        /// Load version of SW that used the database last time
        /// </summary>
        /// <returns>SW version</returns>
        public DatabaseVersion LoadVersion() {
            using (DbCommand command = factory.CreateCommand("SELECT MajorVersion, MinorVersion, DatabaseVersion, BuildVersion FROM " + tableName)) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.HasRows) {
                        throw new Exception("Setup doesn't exist");
                    }
                    reader.Read();
                    
                    DatabaseVersion version = new DatabaseVersion();
                    version.Major    = (short)reader["MajorVersion"];
                    version.Minor    = (short)reader["MinorVersion"];
                    version.Database = (short)reader["DatabaseVersion"];
                    version.Build    = (short)reader["BuildVersion"];

                    return version;
                }
            }
        }
    }
}
