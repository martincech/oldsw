﻿namespace Bat1 {
    partial class UserControlScale {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScale));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonRead = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonSetName = new System.Windows.Forms.Button();
            this.buttonSetDateTime = new System.Windows.Forms.Button();
            this.buttonSetParameters = new System.Windows.Forms.Button();
            this.userControlScaleResults = new Bat1.UserControlScaleResults();
            this.tableLayoutPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.AccessibleDescription = null;
            this.tableLayoutPanel.AccessibleName = null;
            resources.ApplyResources(this.tableLayoutPanel, "tableLayoutPanel");
            this.tableLayoutPanel.BackgroundImage = null;
            this.tableLayoutPanel.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.userControlScaleResults, 0, 1);
            this.tableLayoutPanel.Font = null;
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AccessibleDescription = null;
            this.flowLayoutPanel1.AccessibleName = null;
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.BackgroundImage = null;
            this.flowLayoutPanel1.Controls.Add(this.buttonRead);
            this.flowLayoutPanel1.Controls.Add(this.buttonDelete);
            this.flowLayoutPanel1.Controls.Add(this.buttonSetName);
            this.flowLayoutPanel1.Controls.Add(this.buttonSetDateTime);
            this.flowLayoutPanel1.Controls.Add(this.buttonSetParameters);
            this.flowLayoutPanel1.Font = null;
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            // 
            // buttonRead
            // 
            this.buttonRead.AccessibleDescription = null;
            this.buttonRead.AccessibleName = null;
            resources.ApplyResources(this.buttonRead, "buttonRead");
            this.buttonRead.BackgroundImage = null;
            this.buttonRead.Font = null;
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.AccessibleDescription = null;
            this.buttonDelete.AccessibleName = null;
            resources.ApplyResources(this.buttonDelete, "buttonDelete");
            this.buttonDelete.BackgroundImage = null;
            this.buttonDelete.Font = null;
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonSetName
            // 
            this.buttonSetName.AccessibleDescription = null;
            this.buttonSetName.AccessibleName = null;
            resources.ApplyResources(this.buttonSetName, "buttonSetName");
            this.buttonSetName.BackgroundImage = null;
            this.buttonSetName.Font = null;
            this.buttonSetName.Name = "buttonSetName";
            this.buttonSetName.UseVisualStyleBackColor = true;
            this.buttonSetName.Click += new System.EventHandler(this.buttonSetName_Click);
            // 
            // buttonSetDateTime
            // 
            this.buttonSetDateTime.AccessibleDescription = null;
            this.buttonSetDateTime.AccessibleName = null;
            resources.ApplyResources(this.buttonSetDateTime, "buttonSetDateTime");
            this.buttonSetDateTime.BackgroundImage = null;
            this.buttonSetDateTime.Font = null;
            this.buttonSetDateTime.Name = "buttonSetDateTime";
            this.buttonSetDateTime.UseVisualStyleBackColor = true;
            this.buttonSetDateTime.Click += new System.EventHandler(this.buttonSetDateTime_Click);
            // 
            // buttonSetParameters
            // 
            this.buttonSetParameters.AccessibleDescription = null;
            this.buttonSetParameters.AccessibleName = null;
            resources.ApplyResources(this.buttonSetParameters, "buttonSetParameters");
            this.buttonSetParameters.BackgroundImage = null;
            this.buttonSetParameters.Font = null;
            this.buttonSetParameters.Name = "buttonSetParameters";
            this.buttonSetParameters.UseVisualStyleBackColor = true;
            this.buttonSetParameters.Click += new System.EventHandler(this.buttonSetParameters_Click);
            // 
            // userControlScaleResults
            // 
            this.userControlScaleResults.AccessibleDescription = null;
            this.userControlScaleResults.AccessibleName = null;
            resources.ApplyResources(this.userControlScaleResults, "userControlScaleResults");
            this.userControlScaleResults.BackgroundImage = null;
            this.userControlScaleResults.Font = null;
            this.userControlScaleResults.Name = "userControlScaleResults";
            // 
            // UserControlScale
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.tableLayoutPanel);
            this.Font = null;
            this.Name = "UserControlScale";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonSetName;
        private System.Windows.Forms.Button buttonSetDateTime;
        private UserControlScaleResults userControlScaleResults;
        private System.Windows.Forms.Button buttonSetParameters;
    }
}
