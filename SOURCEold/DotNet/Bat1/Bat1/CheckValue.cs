﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    /// <summary>
    /// Check entered values
    /// </summary>
    static class CheckValue {

        /// <summary>
        /// Allowable characters in text saved to the scale
        /// </summary>
        private static char[] validScaleChars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                                                 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                 ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                                                 '_', '+', '-', '*', '/', '=', '!', '?', '.', ',', ':' ,';' ,'%', '#', '&'};

        /// <summary>
        /// Check if character fits into the specified set of chars
        /// </summary>
        /// <param name="ch">Character to check</param>
        /// <param name="validChars">Array of valid characters</param>
        /// <returns>True if valid</returns>
        public static bool CheckScaleChar(char ch, char[] validChars) {
            foreach (char validChar in validChars) {
                if (ch == validChar) {
                    return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Check if character is valid for the scale
        /// </summary>
        /// <param name="ch">Character to check</param>
        /// <returns>True if valid</returns>
        public static bool CheckScaleChar(char ch) {
            return CheckScaleChar(ch, validScaleChars);
        }

        /// <summary>
        /// Check text saved to the scale. Text can be empty.
        /// </summary>
        /// <param name="text">Text to check</param>
        /// <returns>True if text is valid</returns>
        public static bool CheckScaleText(string text) {
            foreach (char ch in text) {
                if (!CheckScaleChar(ch)) {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Check name saved to the scale. Name cannot be empty.
        /// </summary>
        /// <param name="text">Text to check</param>
        /// <returns>True if text is valid</returns>
        public static bool CheckScaleName(string name) {
            if (name == "") {
                return false;       // Jmeno musi byt zadane
            }
            return CheckValue.CheckScaleText(name);
        }

        public static void SelectControl(Control control) {
            control.Focus();
            if (control.GetType().ToString().EndsWith("TextBox")) {
                TextBox textBox = (TextBox)control;
                textBox.SelectAll();
            } else if (control.GetType().ToString().EndsWith("ComboBox")) {
                ComboBox comboBox = (ComboBox)control;
                comboBox.SelectAll();
            }
        }

        /// <summary>
        /// Show invalid value message and focus the control
        /// </summary>
        /// <param name="control">Control to focus or null</param>
        public static void InvalidValueMessage(Control control) {
            MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
            if (control == null) {
                return;
            }
            SelectControl(control);
        }

        /// <summary>
        /// Get scale name from a control (combobox, textbox)
        /// </summary>
        /// <param name="control">Control with entered text</param>
        /// <param name="name">Returned text</param>
        /// <returns>True if successful</returns>
        public static bool GetScaleName(Control control, out string name) {
            if (!CheckScaleName(control.Text)) {
                InvalidValueMessage(control);
                name = "";
                return false;
            }
            name = control.Text;
            return true;
        }
    
        /// <summary>
        /// Get scale text from a control (combobox, textbox)
        /// </summary>
        /// <param name="control">Control with entered text</param>
        /// <param name="name">Returned text</param>
        /// <returns>True if successful</returns>
        public static bool GetScaleText(Control control, out string text) {
            if (!CheckScaleText(control.Text)) {
                InvalidValueMessage(control);
                text = "";
                return false;
            }
            text = control.Text;
            return true;
        }

        public static bool GetInt(Control control, out int i, int minValue, int maxValue) {
            i = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (!Int32.TryParse(control.Text, out i)) {
                InvalidValueMessage(control);
                return false;
            }
            if (i < minValue || i > maxValue) {
                InvalidValueMessage(control);
                return false;
            }
            return true;
        }

        public static bool GetDouble(Control control, out double d, double minValue, double maxValue) {
            d = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (!Double.TryParse(control.Text, out d)) {
                InvalidValueMessage(control);
                return false;
            }
            if (d < minValue || d > maxValue) {
                InvalidValueMessage(control);
                return false;
            }
            return true;
        }

    }
}
