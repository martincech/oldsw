﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Scale;
using Veit.Bat1;

namespace Bat1 {
    /// <summary>
    /// One point in a growth curve
    /// </summary>
    public struct CurvePoint {
        /// <summary>
        /// Day number
        /// </summary>
        public int Day;

        /// <summary>
        /// Weight
        /// </summary>
        public float Weight;

        /// <summary>
        /// Constrictor
        /// </summary>
        /// <param name="day"></param>
        /// <param name="weight"></param>
        public CurvePoint(int day, float weight) {
            Day    = day;
            Weight = weight;
        }

        /// <summary>
        /// Compare function for List(GrowthCurvePoint).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByDay(CurvePoint x, CurvePoint y) {
            if (x.Day < y.Day) {
                return -1;
            }
            if (x.Day > y.Day) {
                return 1;
            }
            return 0;
        }
    }
    
    /// <summary>
    /// Growth curve
    /// </summary>
    public class Curve {
        /// <summary>
        /// Maximum number of points in the curve
        /// </summary>
        public static int MAX_COUNT = 30;

        /// <summary>
        /// Minimum day number allowed in the curve
        /// </summary>
        private const int MIN_DAY = 0;

        /// <summary>
        ///  Maximum day number allowed in the curve
        /// </summary>
        private const int MAX_DAY = 999;

        /// <summary>
        /// Curve name
        /// </summary>
        public string Name;

        /// <summary>
        /// Curve note
        /// </summary>
        public string Note;

        /// <summary>
        /// Units used in the curve
        /// </summary>
        public Units Units;

        /// <summary>
        /// List of points defined in the curve
        /// </summary>
        public List<CurvePoint> PointList { get { return pointList; } }
        private List<CurvePoint> pointList;

        /// <summary>
        /// Curve ID in the database (for simple updates in the database)
        /// </summary>
        public long Id;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Curve name</param>
        /// <param name="units">Units used</param>
        public Curve() {
            Name      = "";
            Note      = "";
            Units     = Units.KG;
            pointList = new List<CurvePoint>();
            Id        = -1;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Curve name</param>
        /// <param name="units">Units used</param>
        public Curve(string name, Units units) : this() {
            Name  = name;
            Units = units;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="sourceCurve">Source curve to copy from</param>
        public Curve(Curve sourceCurve) {
            Name      = sourceCurve.Name;
            Note      = sourceCurve.Note;
            Units     = sourceCurve.Units;
            pointList = new List<CurvePoint>(sourceCurve.pointList);
            Id        = -1;
        }

        /// <summary>
        /// Check if point index is valid within the curve
        /// </summary>
        /// <param name="index">Point index</param>
        /// <returns>True if valid</returns>
        public bool CheckIndex(int index) {
          return (bool)(index >= 0 && index < pointList.Count);
        }

        /// <summary>
        /// Check if point with a specified day exists
        /// </summary>
        /// <param name="day">Day number</param>
        /// <returns>True if exists</returns>
        private bool DayExists(int day) {
            return pointList.Exists(delegate(CurvePoint point) { return point.Day == day; });
        }
        
        /// <summary>
        /// Check if a day number can be added to the curve
        /// </summary>
        /// <param name="day">Day number</param>
        /// <returns>True if the day can be added</returns>
        public bool CheckDay(int day) {
            // Kontrola rozsahu
            if (day < MIN_DAY || day > MAX_DAY) {
                return false;
            }

            // Kontrola, zda uz den v krivce neexistuje
            return (!DayExists(day));
        }

        /// <summary>
        /// Check weight range
        /// </summary>
        /// <param name="weight">Weight</param>
        /// <returns>True if the weight is valid</returns>
        public bool CheckWeight(float weight) {
            return (bool)(weight > 0 && weight <= (float)ConvertWeight.MaxWeight(Units, WeighingCapacity.EXTENDED));    // Povolim 50kg
        }

        /// <summary>
        /// Sort the list by name
        /// </summary>
        private void Sort() {
            pointList.Sort(CurvePoint.CompareByDay);
        }

        /// <summary>
        /// Add new point to the curve
        /// </summary>
        /// <param name="day">Day number</param>
        /// <param name="weight">Weight</param>
        /// <returns>True if successful</returns>
        public bool Add(int day, float weight) {
            // Kontrola rozsahu
            if (!CheckDay(day)) {
                return false;       // Den je mimo rozsah nebo uz v krivce je
            }
            if (!CheckWeight(weight)) {
                return false;       // Hmotnost je mimo rozsah
            }

            // Kontrola, zda je na novy bod misto
            if (pointList.Count >= MAX_COUNT) {
                return false;       // Krivka uz je plna
            }

            // Ulozim novy bod
            pointList.Add(new CurvePoint(day, weight));

            // Na zaver setridim seznam bodu podle dne
            Sort();

            return true;
        }

        /// <summary>
        /// Delete point from curve
        /// </summary>
        /// <param name="index">Point index</param>
        public void Delete(int index) {
            if (!CheckIndex(index)) {
                return;
            }
            pointList.RemoveAt(index);
        }

        /// <summary>
        /// Recalculate whole curve to specified units
        /// </summary>
        /// <param name="newUnits">New units</param>
        public void SetNewUnits(Units newUnits) {
            if (newUnits == Units) {
                return;     // Neni treba prepocet
            }

            // Zalozim novy seznam bodu a ulozim do nej prepoctene hmotnosti
            List<CurvePoint> newPointList = new List<CurvePoint>();
            foreach (CurvePoint point in pointList) {
                newPointList.Add(new CurvePoint(point.Day, (float)ConvertWeight.Convert(point.Weight, Units, newUnits)));
            }

            // Preberu novy seznam bodu
            pointList = newPointList;

            // Ulozim si nove pouzite jednotky
            Units = newUnits;
        }

        /// <summary>
        /// Get day of a curve point
        /// </summary>
        /// <param name="index">Point index</param>
        /// <returns>Day number</returns>
        public int GetDay(int index) {
            if (!CheckIndex(index)) {
                return -1;
            }
            return pointList[index].Day;
        }

        /// <summary>
        /// Get weight of a curve point
        /// </summary>
        /// <param name="index">Point index</param>
        /// <returns>Weight</returns>
        public double GetWeight(int index) {
            if (!CheckIndex(index)) {
                return 0;
            }
            return pointList[index].Weight;
        }

        /// <summary>
        /// Calculate weight
        /// </summary>
        /// <param name="day">Day number</param>
        /// <returns>Weight</returns>
        public double CalculateWeight(int day) {
            int lastIndex = 0;

            // Pokud ve krivce nic neni, vratim nulu
            if (pointList.Count == 0) {
                return 0;               // Krivka je prazdna
            }

            // Pokud je den mensi nez minimalni den ve krivce, vratim hmotnost prvniho dne
            if (day < GetDay(0)) {
                return GetWeight(0);    // Pod limitem vracime minimalni hmotnost
            }

            // Projizdim jednotlive body krivky
            for (int index = 0; index < pointList.Count; index++) {
                if (GetDay(index) == day) {
                    return GetWeight(index);    // Nasel jsem primo zadany den => vratim primo hmotnost ve krivce (kvuli 1. bodu zde tento test musi byt)
                }
                if (GetDay(index) > day) {
                    // Nasel jsem prvni den, ktery lezi za zadanym dnem => aproximuju z predchoziho a tohoto dne. LastPoint je uz v tomto miste urcite inicializovan.
                    // Vypoctu hmotnost podle vzorce primky:
                    //     Y2-Y1
                    // Y = ----- * (X-X1) + Y1
                    //     X2-X1
                    // Musim to pocitat long, protoze se tam muze nasobit 999 * 64000
                    // Rozdil hmotnosti (CurvePoint->Weight - LastPoint->Weight) muze pri klesajici krivce vyjit zaporny => obe hodnoty musim prevest zvlast na long
                    // Rozdil dnu (CurvePoint->Day - LastPoint->Day) i (Day - LastPoint->Day) vyjde vzdy kladny, protoze krivka je setridena podle dnu vzestupne
                    // => rozdil dnu muzu pocitat ve word
                    return (GetWeight(index) - GetWeight(lastIndex)) * (float)(day - GetDay(lastIndex))
                            / (float)(GetDay(index) - GetDay(lastIndex))
                            + GetWeight(lastIndex);
                }
                lastIndex = index;
            }

            // Nenasel jsem zadny bod krivky, ktery by byl roven nebo vetsi nez zadany den => den vypadl z krivky, vracim hmotnost posledniho dne v krivce
            return GetWeight(lastIndex);    // CurvePoint uz je neplatny (mimo definovanou krivku) - musim vzit predchozi bod
        }

        /// <summary>
        /// Compare function for List(Curve).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByName(Curve x, Curve y) {
            int result = String.Compare(x.Name, y.Name);
            if (result < 0) {
                return -1;
            }
            if (result > 0) {
                return 1;
            }
            return 0;
        }

    
    }
}
