﻿
using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using Veit.Bat1;
using Veit.Scale;
using Veit.ScaleStatistics;

namespace Bat1 {
    /// <summary>
    /// Bat1 version 6 connection
    /// </summary>
    public class Bat1Version6 {
        private SerialPort port;

        /// <summary>
        /// Timeout for checking the scale
        /// </summary>
        const int TIMEOUT_VERSION_CHECK = 1000;         // 500ms jeste blbne

        /// <summary>
        /// Timeout when downloading data
        /// </summary>
        const int TIMEOUT_DOWNLOAD = 14000;

        /// <summary>
        /// Allowable characters in text saved to the scale version 6
        /// </summary>
        private char[] validScaleChars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                                          'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                          ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        /// <summary>
        /// Write one byte to the scale
        /// </summary>
        /// <param name="b">Byte to write</param>
        private void Write(byte b) {
            port.Write(new byte[] {b}, 0, 1);
        }
        
        /// <summary>
        /// Read data from the scale
        /// </summary>
        /// <param name="buffer">Array of bytes</param>
        /// <param name="count">Number of bytes to read</param>
        /// <returns>True if all bytes have been read</returns>
        private bool Read(byte[] buffer, int count) {
            // SerialPort.Read bohuzel s timeoutem nefunguje, musim rucne pomoci ReadByte (tam to funguje)
            try {
                for (int i = 0; i < count; i++) {
                    buffer[i] = (byte)port.ReadByte();
                }
                return true;
            } catch {
                return false;       // Timeout
            }
        }
        
        /// <summary>
        /// Read one byte from the scale
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        private bool ReadByte(out byte b) {
            try {
                b = (byte)port.ReadByte();
                return true;
            } catch {
                b = 0;
                return false;       // Timeout
            }
        }

        /// <summary>
        /// Read scale version
        /// </summary>
        /// <param name="version">Version</param>
        /// <returns>True if successful</returns>
        private bool ReadVersion(out int version) {
            version = 0;

            port.ReadTimeout = TIMEOUT_VERSION_CHECK;       // Na test pritomnosti staci kratky timeout
            try {
                port.Open();
            } catch {
                // Zadany port na pocitaci neexistuje (napr. nema zasunuty USB-serial prevodnik)
                return false;
            }

            try {
                Write(0x06);

                byte[] buffer = new byte[5];
                if (!Read(buffer, 5)) {
                    return false;
                }
                
                // V bufferu je ted napr. " 3.02" => udelam z toho cislo 302
                version = 100 * (buffer[1] - 0x30) + 10 * (buffer[3] - 0x30) + buffer[4] - 0x30;
                return true;
            } catch {
                return false;
            } finally {
                port.Close();
            }
        }

        /// <summary>
        /// Read scale setup
        /// </summary>
        /// <param name="scaleConfig">Scale config to be read</param>
        /// <param name="version">Scale version</param>
        /// <returns></returns>
        private bool ReadSetup(out ScaleConfig scaleConfig, int version) {
            scaleConfig = null;

            // Podle verze posila vaha ruzne dlouhy setup
            int count;
            if (version >= 406) {
                // 11.7.2003: Od verze 4.06 vaha posila 41 bajtu (pribylo deleni hmotnosti)
                count = 41;
            } else if (version >= 401) {
                // 12.9.2002: Od verze 4.01 vaha posila 40 bajtu (pribyly zakladni parametry)
                count = 40;
            } else if (version == 400) {
                // Vaha verze 4.00 posila 36 bajtu
                count = 36;
            } else {
                return false;
            }
            
            // Otevru port
            port.ReadTimeout = TIMEOUT_DOWNLOAD;
            port.Open();

            try {
                Write(0x04);

                byte[] buffer = new byte[count];
                if (!Read(buffer, count)) {
                    return false;
                }
                
                // Nastaveni vahy
                scaleConfig = new ScaleConfig(Units.G);            // Zatim budu ukladat v gramech, prepoctu na zvolene jednotky az pozdeji

                // Prvnich 16 znaku je jmeno vahy
                byte[] asciiName = new byte[16];
                for (int i = 0; i < asciiName.Length; i++) {
                    asciiName[i] = buffer[i];
                }
                string scaleName = Encoding.ASCII.GetString(asciiName).TrimEnd(new char[] {' '});      // Zaroven odstranim mezery na konci
                scaleConfig.ScaleName = scaleName.ToUpper();        // Prevedu na velka pismena (stara vaha muze obsahovat i mala)

                // Verze vah
                scaleConfig.Version.Major = version / 100;
                scaleConfig.Version.Minor = version % 100;
                scaleConfig.Version.Build = 0;
                scaleConfig.Version.Hw    = 0;
                
                // Uniformita
                scaleConfig.StatisticConfig.UniformityRange = (int)buffer[30];
                
                // Pocet kurat vazenych naraz
                if (version >= 406) {
                    scaleConfig.WeighingConfig.Saving.NumberOfBirds = (int)buffer[40];
                } else {
                    scaleConfig.WeighingConfig.Saving.NumberOfBirds = 1;
                }
                
                // Rezim ukladani
                switch (buffer[32]) {
                    case 0:
                        scaleConfig.WeighingConfig.Saving.Mode = SavingMode.AUTOMATIC;
                        break;
                    case 1: 
                        scaleConfig.WeighingConfig.Saving.Mode = SavingMode.MANUAL;
                        break;
                    default:
                        scaleConfig.WeighingConfig.Saving.Mode = SavingMode.MANUAL_BY_SEX;
                        break;
                }

                // Mez zatim v gramech
                scaleConfig.WeighingConfig.WeightSorting.Mode     = WeightSorting.NONE;
                scaleConfig.WeighingConfig.WeightSorting.LowLimit = 10.0 * (double)(256 * buffer[20] + buffer[21]);
                if (buffer[25] == 1) {
                    // Pouziva flagy - pouziti podle rezimu ukladani
                    switch (scaleConfig.WeighingConfig.Saving.Mode) {
                        case SavingMode.AUTOMATIC:
                        case SavingMode.MANUAL:
                            scaleConfig.WeighingConfig.WeightSorting.Mode = WeightSorting.LIGHT_HEAVY;
                            break;
                        default:
                            scaleConfig.WeighingConfig.WeightSorting.Mode = WeightSorting.NONE;
                            break;
                    }
                }

                // Krok histogramu delam zatim v gramech
                scaleConfig.StatisticConfig.Histogram.Mode = HistogramMode.STEP;
                if (version >= 401) {
                    // Od verze 4.01 vypocitam krok z vazivosti
                    int capacity = 256 * buffer[36] + buffer[37];
                    if (capacity == 1000) {
                        scaleConfig.StatisticConfig.Histogram.Step = 40;       // Vazivost 10kg => krok 40g
                    } else {
                        scaleConfig.StatisticConfig.Histogram.Step = 100;      // Vazivost 30kg => krok 100g
                    }
                } else {
                    scaleConfig.StatisticConfig.Histogram.Step = 40;           // Pro starsi vahy krok default 40g
                }

                // Seznam souboru musim vytvorit pozdeji, az stahnu data

                // Seznam skupin vytvorim a necham prazdny
                scaleConfig.FileGroupList = new FileGroupList();

                return true;
            } catch {
                return false;
            } finally {
                port.Close();
            }
        }

        /// <summary>
        /// Read scale name
        /// </summary>
        /// <param name="scaleName">Scale name to be read</param>
        /// <returns></returns>
        public bool ReadName(out string scaleName) {
            scaleName = "";
            
            // Verze vah
            int version;
            if (!ReadVersion(out version)) {
                return false;
            }

            ScaleConfig scaleConfig;
            if (!ReadSetup(out scaleConfig, version)) {
                return false;
            }

            scaleName = scaleConfig.ScaleName;
            return true;
        }

        /// <summary>
        /// Write scale name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool WriteName(string name) {
            if (name.Length > 16) {
                name = name.Remove(16);        // Omezim
            }
            name = name.PadRight(16, ' ');     // Pripadne prodlouzim mezerami
            
            // Vsechny nepripustne znaky nahradim pomlckou
            for (int i = 0; i < name.Length; i++) {
                if (!CheckValue.CheckScaleChar(name[i], validScaleChars)) {
                    name = name.Replace(name[i], '-');
                }
            }

            // Prevedu na ASCII
            byte[] asciiName = Encoding.ASCII.GetBytes(name);
            

            port.ReadTimeout = TIMEOUT_VERSION_CHECK;
            port.Open();

            try {
                Write(0x03);

                // Vahy ted poslou STX a potom poslu jmeno - STX proto, abych to tam neposilal a vahy trewba nebyly ready
                byte b;
                if (!ReadByte(out b)) {
                    return false;
                }
                if (b != 0x02) {
                    return false;
                }

                // Nastavim delsi timeout
                port.ReadTimeout = TIMEOUT_DOWNLOAD;

                // Zapisu jmeno
                port.Write(asciiName, 0, 16);

                // Vahy mne poslou ulozene jmeno zpet pro kontrolu
                byte[] newName = new byte[16];
                if (!Read(newName, 16)) {
                    return false;
                }
                for (int i = 0; i < newName.Length; i++) {
                    if (newName[i] != asciiName[i]) {
                        return false;
                    }
                }
                
                return true;
            } catch {
                return false;
            } finally {
                port.Close();
            }
        }
        
        private void AddFile(List<WeighingData> list, int fileNumber, SampleList sampleList, ScaleConfig scaleConfig) {
            // Vytvorim soubor vcetne nastaveni (z globalniho nastaveni)
            File file = new File(fileNumber.ToString(), "", new FileConfig(scaleConfig));

            // Pridam soubor do configu vahy
            scaleConfig.FileList.Add(file);
            
            // Pridam vazeni do seznamu, soubor ukazuje na soubor v configu
            list.Add(new WeighingData(-1, ResultType.OLD_SCALE, RecordSource.SAVE, file, sampleList, scaleConfig, ""));
        }
        
        /// <summary>
        /// Read all samples
        /// </summary>
        /// <param name="fileList"></param>
        /// <param name="scaleSetup"></param>
        /// <returns></returns>
        private bool ReadSamples(out List<WeighingData> weighingDataList, ScaleConfig scaleConfig, int version) {
            weighingDataList = new List<WeighingData>();
            port.ReadTimeout = TIMEOUT_DOWNLOAD;
            port.Open();

            try {
                Write(0x02);

                byte[] buffer = new byte[8200];
                byte b;
                int count = 0;

                // Nactu vsechny data do bufferu
                do {
                    if (!ReadByte(out b)) {
                        return false;
                    }
                    buffer[count] = b;
                    count++;

                    if (count >= 2 + 16 + 2) {
                        // Uz jsem nacetl jmeno vah a nacitam uz data => kontroluju FF
                        // Sem jdu az kdyz mam nactene minimalne 2 bajty dat (protoze porovnavam prave nacteny bajt a predchozi bajt dat)
                        if (buffer[count - 2] == 0xFF && buffer[count - 1] == 0xFF) {
                            break;      // Poslal 2xFF => koncim
                        }
                    }
                } while (count < 8199);

                byte[] s = new byte[2];
                bool scaleWithLimits = false;
                int filesCount = 0;
                int fileNumber = 0;
                SampleList sampleList = null;
                DateTime started = DateTime.Now;        // Pocatek vazeni u kazdeho souboru stejny
                DateTime sampleDateTime = started;      // Datum a cas aktualniho vzorku
                scaleConfig.FileList = new FileList();
                for (int i = 0; i < count - 2; i += 2) {
                    s[0] = buffer[i];
                    s[1] = buffer[i + 1];
                    if ((s[0] == 0xFE) && (s[1] == 0xFE || s[1] == 0xFD)) {
                        // Zacatek noveho souboru
                        if (filesCount > 0) {
                            // Ulozim nacteny soubor
                            AddFile(weighingDataList, fileNumber, sampleList, scaleConfig);
                        }
                      
                        if (s[1] == 0xFD && i == 0) {  // Jen pokud jde o prvni 2 bajty, pak uz toto nedelam
                            // Jde o novou vahu s mezi, cislem souboru a jmenem vah
                            // Hned po prvnim FEFD se posila jmeno vah -- 16 znaku. Jmeno vah sice nacitam vzdy, ale ukladam jej jen do excelu
                            i += 16;  // Preskocim jmeno
                            scaleWithLimits = true;       // Nastavim flag, ze mam zpracovavat i mez
                        }
                        if (scaleWithLimits) {
                            // 26.7.2001: Verze 3.0 a vyssi, ktera po FEFD posila cislo souboru
                            fileNumber = buffer[i + 2];
                            i++;  // Preskocim cislo souboru, to uz mam nactene v poli CislaSouboru
                        } else {
                            fileNumber = filesCount + 1;
                        }
                        // Zalozim novy soubor
                        sampleList = new SampleList();
                        sampleDateTime = started;       // Zase od zacatku
                        filesCount++;
                    } else {
                        // Normalni zaznam
                        Sample sample = new Sample();
                        sample.dateTime = sampleDateTime;
                        sampleDateTime = sampleDateTime.AddSeconds(30);     // Jedno kure za 30 sekund
                        sample.flag = Flag.NONE;        // Default hodnota flagu
                        if (scaleWithLimits) {
                            // Flagy interpretuju podle rezimu ukladani a pouziti mezi
                            if ((s[0] & 0x80) == 0x80) {
                                // Flag je nastaven
                                if (scaleConfig.WeighingConfig.WeightSorting.Mode == WeightSorting.LIGHT_HEAVY) {
                                    sample.flag = Flag.HEAVY;
                                } else if (scaleConfig.WeighingConfig.Saving.Mode == SavingMode.MANUAL_BY_SEX) {
                                    sample.flag = Flag.MALE;
                                }
                            } else {
                                if (scaleConfig.WeighingConfig.WeightSorting.Mode == WeightSorting.LIGHT_HEAVY) {
                                    sample.flag = Flag.LIGHT;
                                } else if (scaleConfig.WeighingConfig.Saving.Mode == SavingMode.MANUAL_BY_SEX) {
                                    sample.flag = Flag.FEMALE;
                                }
                            }
                            s[0] &= 0x7F;   // Odfiltruju bit s mezi, aby se nepletl do hmotnosti
                        }
                        // Stara Bat1 ma dilek 10g, nactenou hmotnost nasobim deseti
                        if (version == 400 ) {
                            // Vaha verze 4.00 uklada hmotnost v BCD (tj. 0x0215 = 2.15kg)
                            sample.weight = (int)(10 * (1000 * (int)((s[0] & 0xF0) >> 4) + 100 * (int)(s[0] & 0x0F) + 10 * (int)((s[1] & 0xF0) >> 4) + (int)(s[1] & 0x0F)));
                        } else {
                            // Od verze 4.01 nahoru je hmotnost ulozena binarne
                            sample.weight = 10 * ((256 * (int)s[0]) + s[1]);
                        }
                        sampleList.Add(sample);     // Tridit neni treba, vzorky pridavam uz setridene podle data
                    }
                }
                
                // Ulozim posledni soubor, ten se mne v cyklu neulozi
                if (filesCount > 0) {
                    // Ulozim nacteny soubor
                    AddFile(weighingDataList, fileNumber, sampleList, scaleConfig);
                }

                return true;
            } catch {
                return false;
            } finally {
                port.Close();
            }
        }

        public bool ReadAll(out ScaleConfig scaleConfig, out List<WeighingData> weighingDataList) {
            scaleConfig      = null;
            weighingDataList = null;
            
            // Verze vah
            int version;
            if (!ReadVersion(out version)) {
                return false;
            }
            if (version < 400) {
                return false;       // SW spolupracuje pouze s verzi 4.00 a vyssi
            }

            // Nastaveni
            System.Threading.Thread.Sleep(100);     // Vahy 4.00 bez prodlevy nefunguji (6.00 uz ano)
            if (!ReadSetup(out scaleConfig, version)) {
                return false;
            }
            
            // Data, zaroven vyplnim seznam souboru
            System.Threading.Thread.Sleep(100);     // Vahy 4.00 bez prodlevy nefunguji (6.00 uz ano)
            if (!ReadSamples(out weighingDataList, scaleConfig, version)) {
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="portNumber">Serial port number</param>
        public Bat1Version6(int portNumber) {
            port = new SerialPort();
            port.PortName       = "COM" + portNumber.ToString();
            port.BaudRate       = 9600;
            port.DataBits       = 8;
            port.Parity         = Parity.Even;
            port.StopBits       = StopBits.One;
            port.ReadBufferSize = 8300;         // Vaha ma EEPROM 8kB
            port.ReadTimeout    = TIMEOUT_VERSION_CHECK;
        }
    }
}
