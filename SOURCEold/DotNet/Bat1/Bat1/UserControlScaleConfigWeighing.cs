﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlScaleConfigWeighing : UserControlScaleConfigBase {
        public UserControlScaleConfigWeighing(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Read-only
            this.readOnly = readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        private void SetUnits() {
            // Nastavim minimalni a maximalni hodnoty podle jednotek
            numericUpDownDivision.Minimum = (decimal)ConvertWeight.MinWeight(scaleConfig.Units.Units);
            // Max beru z default nastaveni, do databaze si to neukladam
            // Pozor, driver vahy nemusi byt nainstalovany
            try {
                numericUpDownDivision.Maximum = (decimal)ConvertWeight.IntToWeight(Bat1Version7.DefaultConfig.Units.MaxDivision, scaleConfig.Units.Units);
            } catch {
                numericUpDownDivision.Maximum = 999;    // Pouze pro prohlizeni, tj. na max nezalezi
            }
            switch (scaleConfig.Units.Units) {
                case Units.G:
                    numericUpDownDivision.DecimalPlaces = 0;
                    numericUpDownDivision.Increment     = 1;
                    break;

                default:    // KG nebo LB
                    numericUpDownDivision.DecimalPlaces = 3;
                    numericUpDownDivision.Increment     = (decimal)0.001;
                    break;
            }
        }

        private void RedrawUnits() {
            labelDivisionUnits.Text = ConvertWeight.UnitsToString(scaleConfig.Units.Units);
        }

        public override void Redraw() {
            isLoading = true;
            
            try {
                // Nastavim minimalni a maximalni hodnoty podle jednotek
                SetUnits();

                comboBoxUnits.SelectedIndex    = (int)scaleConfig.Units.Units;
                numericUpDownDivision.Text     = scaleConfig.Units.Division.ToString();
                comboBoxCapacity.SelectedIndex = (int)scaleConfig.Units.WeighingCapacity;
                RedrawUnits();
            } finally {
                isLoading = false;
            }
        }

        private void comboBoxUnits_SelectionChangeCommitted(object sender, EventArgs e) {
            if (readOnly || isLoading) {
                return;
            }
            
            scaleConfig.SetNewUnits((Units)comboBoxUnits.SelectedIndex);    // Zaroven ulozi jednotky
            
            // Dilek nastavim na default hodnotu pro vybrane jednotky
            switch (scaleConfig.Units.Units) {
                case Units.G:
                    scaleConfig.Units.Division = 1;
                    break;

                default:    // KG nebo LB
                    scaleConfig.Units.Division = 0.001;
                    break;
            }

            Redraw();
        }

        private void numericUpDownDivision_ValueChanged(object sender, EventArgs e) {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.Units.Division = (double)numericUpDownDivision.Value;
        }

        private void comboBoxCapacity_SelectionChangeCommitted(object sender, EventArgs e) {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.Units.WeighingCapacity = (WeighingCapacity)comboBoxCapacity.SelectedIndex;
        }
    }
}
