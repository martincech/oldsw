﻿namespace Bat1 {
    partial class UserControlScaleConfigDisplay {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigDisplay));
            this.groupBoxBacklight = new System.Windows.Forms.GroupBox();
            this.labelDurationSec = new System.Windows.Forms.Label();
            this.numericUpDownBacklightIntensity = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownBacklightDuration = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxBacklightMode = new System.Windows.Forms.ComboBox();
            this.labelIntensity = new System.Windows.Forms.Label();
            this.labelDuration = new System.Windows.Forms.Label();
            this.groupBoxDisplayMode = new System.Windows.Forms.GroupBox();
            this.comboBoxDisplayMode = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxBacklight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBacklightIntensity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBacklightDuration)).BeginInit();
            this.groupBoxDisplayMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxBacklight
            // 
            this.groupBoxBacklight.AccessibleDescription = null;
            this.groupBoxBacklight.AccessibleName = null;
            resources.ApplyResources(this.groupBoxBacklight, "groupBoxBacklight");
            this.groupBoxBacklight.BackgroundImage = null;
            this.groupBoxBacklight.Controls.Add(this.labelDurationSec);
            this.groupBoxBacklight.Controls.Add(this.numericUpDownBacklightIntensity);
            this.groupBoxBacklight.Controls.Add(this.numericUpDownBacklightDuration);
            this.groupBoxBacklight.Controls.Add(this.label1);
            this.groupBoxBacklight.Controls.Add(this.comboBoxBacklightMode);
            this.groupBoxBacklight.Controls.Add(this.labelIntensity);
            this.groupBoxBacklight.Controls.Add(this.labelDuration);
            this.groupBoxBacklight.Font = null;
            this.groupBoxBacklight.Name = "groupBoxBacklight";
            this.groupBoxBacklight.TabStop = false;
            // 
            // labelDurationSec
            // 
            this.labelDurationSec.AccessibleDescription = null;
            this.labelDurationSec.AccessibleName = null;
            resources.ApplyResources(this.labelDurationSec, "labelDurationSec");
            this.labelDurationSec.Font = null;
            this.labelDurationSec.Name = "labelDurationSec";
            // 
            // numericUpDownBacklightIntensity
            // 
            this.numericUpDownBacklightIntensity.AccessibleDescription = null;
            this.numericUpDownBacklightIntensity.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownBacklightIntensity, "numericUpDownBacklightIntensity");
            this.numericUpDownBacklightIntensity.Font = null;
            this.numericUpDownBacklightIntensity.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDownBacklightIntensity.Name = "numericUpDownBacklightIntensity";
            this.numericUpDownBacklightIntensity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBacklightIntensity.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownBacklightIntensity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // numericUpDownBacklightDuration
            // 
            this.numericUpDownBacklightDuration.AccessibleDescription = null;
            this.numericUpDownBacklightDuration.AccessibleName = null;
            resources.ApplyResources(this.numericUpDownBacklightDuration, "numericUpDownBacklightDuration");
            this.numericUpDownBacklightDuration.Font = null;
            this.numericUpDownBacklightDuration.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownBacklightDuration.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBacklightDuration.Name = "numericUpDownBacklightDuration";
            this.numericUpDownBacklightDuration.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownBacklightDuration.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownBacklightDuration.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label1
            // 
            this.label1.AccessibleDescription = null;
            this.label1.AccessibleName = null;
            resources.ApplyResources(this.label1, "label1");
            this.label1.Font = null;
            this.label1.Name = "label1";
            // 
            // comboBoxBacklightMode
            // 
            this.comboBoxBacklightMode.AccessibleDescription = null;
            this.comboBoxBacklightMode.AccessibleName = null;
            resources.ApplyResources(this.comboBoxBacklightMode, "comboBoxBacklightMode");
            this.comboBoxBacklightMode.BackgroundImage = null;
            this.comboBoxBacklightMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBacklightMode.Font = null;
            this.comboBoxBacklightMode.FormattingEnabled = true;
            this.comboBoxBacklightMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxBacklightMode.Items"),
            resources.GetString("comboBoxBacklightMode.Items1"),
            resources.GetString("comboBoxBacklightMode.Items2")});
            this.comboBoxBacklightMode.Name = "comboBoxBacklightMode";
            this.comboBoxBacklightMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelIntensity
            // 
            this.labelIntensity.AccessibleDescription = null;
            this.labelIntensity.AccessibleName = null;
            resources.ApplyResources(this.labelIntensity, "labelIntensity");
            this.labelIntensity.Font = null;
            this.labelIntensity.Name = "labelIntensity";
            // 
            // labelDuration
            // 
            this.labelDuration.AccessibleDescription = null;
            this.labelDuration.AccessibleName = null;
            resources.ApplyResources(this.labelDuration, "labelDuration");
            this.labelDuration.Font = null;
            this.labelDuration.Name = "labelDuration";
            // 
            // groupBoxDisplayMode
            // 
            this.groupBoxDisplayMode.AccessibleDescription = null;
            this.groupBoxDisplayMode.AccessibleName = null;
            resources.ApplyResources(this.groupBoxDisplayMode, "groupBoxDisplayMode");
            this.groupBoxDisplayMode.BackgroundImage = null;
            this.groupBoxDisplayMode.Controls.Add(this.comboBoxDisplayMode);
            this.groupBoxDisplayMode.Controls.Add(this.label6);
            this.groupBoxDisplayMode.Font = null;
            this.groupBoxDisplayMode.Name = "groupBoxDisplayMode";
            this.groupBoxDisplayMode.TabStop = false;
            // 
            // comboBoxDisplayMode
            // 
            this.comboBoxDisplayMode.AccessibleDescription = null;
            this.comboBoxDisplayMode.AccessibleName = null;
            resources.ApplyResources(this.comboBoxDisplayMode, "comboBoxDisplayMode");
            this.comboBoxDisplayMode.BackgroundImage = null;
            this.comboBoxDisplayMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDisplayMode.Font = null;
            this.comboBoxDisplayMode.FormattingEnabled = true;
            this.comboBoxDisplayMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxDisplayMode.Items"),
            resources.GetString("comboBoxDisplayMode.Items1"),
            resources.GetString("comboBoxDisplayMode.Items2")});
            this.comboBoxDisplayMode.Name = "comboBoxDisplayMode";
            this.comboBoxDisplayMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.AccessibleDescription = null;
            this.label6.AccessibleName = null;
            resources.ApplyResources(this.label6, "label6");
            this.label6.Font = null;
            this.label6.Name = "label6";
            // 
            // UserControlScaleConfigDisplay
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = null;
            this.Controls.Add(this.groupBoxDisplayMode);
            this.Controls.Add(this.groupBoxBacklight);
            this.Font = null;
            this.Name = "UserControlScaleConfigDisplay";
            this.groupBoxBacklight.ResumeLayout(false);
            this.groupBoxBacklight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBacklightIntensity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBacklightDuration)).EndInit();
            this.groupBoxDisplayMode.ResumeLayout(false);
            this.groupBoxDisplayMode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxBacklight;
        private System.Windows.Forms.ComboBox comboBoxBacklightMode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelIntensity;
        private System.Windows.Forms.Label labelDuration;
        private System.Windows.Forms.GroupBox groupBoxDisplayMode;
        private System.Windows.Forms.ComboBox comboBoxDisplayMode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelDurationSec;
        private System.Windows.Forms.NumericUpDown numericUpDownBacklightIntensity;
        private System.Windows.Forms.NumericUpDown numericUpDownBacklightDuration;
    }
}
