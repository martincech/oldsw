﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    public class Flock {
        /// <summary>
        /// Flock definition
        /// </summary>
        public FlockDefinition Definition { get { return definition; } }
        private FlockDefinition definition;

        /// <summary>
        /// Additional time filter (or DateTime.MinValue when NA)
        /// </summary>
        private DateTime fromDate;

        /// <summary>
        /// Additional time filter (or DateTime.MinValue when NA)
        /// </summary>
        private DateTime toDate;

        /// <summary>
        /// Results as a list of weighings
        /// </summary>
        public WeighingList WeighingList { get { return weighingList; } }
        private WeighingList weighingList;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="definition">Flock definition</param>
        /// <param name="fromDate">Additional time filter (or DateTime.MinValue when NA)</param>
        /// <param name="toDate">Additional time filter (or DateTime.MaxValue when NA)</param>
        public Flock(FlockDefinition definition, DateTime fromDate, DateTime toDate) {
            // Preberu definici
            this.definition = definition;
            this.fromDate   = fromDate;
            this.toDate     = toDate;

            // Nactu vysledky hejna z databaze
            LoadWeighingList(fromDate, toDate);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="definition">Flock definition</param>
        public Flock(FlockDefinition definition) : this(definition, DateTime.MinValue, DateTime.MaxValue) {
        }

        /// <summary>
        /// Create list of weighing lists sorted by day (one weighing list per day)
        /// </summary>
        /// <param name="idList">List of weighing IDs</param>
        /// <returns></returns>
        private List<List<Weighing>> CreateUniqueDayWeighingList(List<long> idList) {
            // Seskupim jednotliva vazeni podle dnu. Vazeni, ktera probihala v jeden den je treba zprumerovat do jednoho vazeni.
            List<Weighing> dayWeighingList = new List<Weighing>();                      // Seznam vazeni provedenych za 1 den
            List<List<Weighing>> dayUniqueWeighingList = new List<List<Weighing>>();    // Seznam seznamu vazeni podle dnu (za kazdy den jeden seznam)
            DateTime dayDateTime = DateTime.MinValue;                                   // Datum posledne nacteneho vazeni, default MinValue

            foreach (long weighingId in idList) {
                // Nactu vazeni z DB
                Weighing weighing = Program.Database.LoadWeighing(weighingId);

                // Pokud je datum vazeni shodny s datumem predchoziho vazeni, pridam vazeni do seznamu za tento den
                if (dayDateTime == DateTime.MinValue || dayDateTime.Date == weighing.GetMinDateTime().Date) {
                    // Bud uplne prvni vazeni v seznam, nebo dalsi vazeni v jednom dni => pridam vazeni do seznamu vazeni z tohoto dne
                    dayWeighingList.Add(weighing);
                } else {
                    // Toto vazeni je uz v jiny den, musim nejprve ulozit seznam vazeni patrici k predchozimu dni
                    dayUniqueWeighingList.Add(dayWeighingList);

                    // Zacinam vytvaret seznam vazeni pro jiny den
                    dayWeighingList = new List<Weighing>();
                    dayWeighingList.Add(weighing);
                }

                // Ulozim si datum vazeni
                dayDateTime = weighing.GetMinDateTime().Date;
            }

            // Pokud v seznamu aktualniho dne zustaly nejake vazeni, musim je ted ulozit rucne
            if (dayWeighingList.Count > 0) {
                dayUniqueWeighingList.Add(dayWeighingList);
            }

            return dayUniqueWeighingList;
        }
        
        /// <summary>
        /// Create a list of average weighings per day
        /// </summary>
        /// <param name="dayUniqueWeighingList">List of weighing lists (one list per day)</param>
        /// <returns>List of averaged weighings (one average weighing per day)</returns>
        private WeighingList CreateAveragedWeighingList(List<List<Weighing>> dayUniqueWeighingList) {
            WeighingList averagedWeighingList = new WeighingList();      // Seznam s prumery za jednotlive dny

            // Zprumeruju vazeni z jednotlivych dnu a vytvorim seznam s prumernymi vysledky. Pokud je v jeden den
            // vice vazeni, tato vazeni zprumeruju v jedno, takze bude v 1 den vzdy jen 1 vazeni.
            foreach (List<Weighing> list in dayUniqueWeighingList) {
                if (list.Count == 1) {
                    // V tento den se vazilo pouze jednou. Neni treba nic prumerovat, pouziju rovnou toto vazeni.

                    // Zmenim nazev souboru v configu na jmeno hejna + pocet vazeni v prumeru. Prumerne vazeni se nikam ukladat nebude,
                    // tj. muzu pouzit nazev s Unicode znaky a libovolne dlouhy
                    list[0].WeighingData.File.RenameUnicode(definition.Name);   // Bez poctu vazeni v prumeru
                    
                    averagedWeighingList.Add(list[0]);
                    continue;
                }

                // V tento den se vazilo vicekrat, musim prumerovat
                
                // Vytvorim seznam vazeni v tento den, pouziju WeighingList, ktery mi rovnou vyhodi seznam flagu a dalsi info.
                WeighingList dayWeighingList = new WeighingList();
                foreach (Weighing weighing in list) {
                    dayWeighingList.Add(weighing);
                }

                // Zkontroluju, zda maji vsechna vazeni stejne jendotky. Pokud je pouzito vice jednotek,
                // nastavim vsechna vazeni na jednotky pouzite v prvnim vazeni (nedotazuju se)
                dayWeighingList.ChangeUnits(dayWeighingList.UnitsList[0]);

                // Vyberu prvni vazeni v seznamu, ze ktereho beru config vahy atd.
                Weighing firstWeighing = dayWeighingList.List[0];

                // Zmenim nazev souboru v configu na jmeno hejna + pocet vazeni v prumeru. Prumerne vazeni se nikam ukladat nebude,
                // tj. muzu pouzit nazev s Unicode znaky a libovolne dlouhy
                firstWeighing.WeighingData.File.RenameUnicode(definition.Name + " (" + dayWeighingList.List.Count.ToString() + ")");

                // Pokud seznam vazeni za tento den obsahuje pouze vysledky z vahy, staci sloucit vzorky do 1 velkeho
                // seznamu a vytvorit nove vazeni s celkovym seznamem vzorku
                if (!dayWeighingList.ContainsManualResults) {
                    // Tento den obsahuje jen vazeni nactene z vahy, pracuji tedy primo se vzorky

                    // Vytvorim celkovy seznam vzorku ze vsech vazeni za tento den
                    SampleList totalSampleList = new SampleList();
                    foreach (Weighing weighing in dayWeighingList.List) {
                        weighing.WeighingData.SampleList.First();
                        while (weighing.WeighingData.SampleList.Read()) {
                            totalSampleList.Add(weighing.WeighingData.SampleList.Sample);
                        }
                        // Vzorky neni treba tridit
                    }
                    
                    // Vytvorim nove vazeni s celkovymi daty
                    WeighingData averageWeighingData = new WeighingData(-1,     // ID dam -1, podle toho poznam, ze nejde o vazeni nactene z databaze
                                                                        firstWeighing.WeighingData.ResultType,
                                                                        firstWeighing.WeighingData.RecordSource,
                                                                        firstWeighing.WeighingData.File,
                                                                        totalSampleList,
                                                                        firstWeighing.WeighingData.ScaleConfig,
                                                                        "");

                    // Pridam prumerne vazeni do seznamu
                    averagedWeighingList.Add(new Weighing(averageWeighingData));

                    // Pokracuju na dalsi den
                    continue;
                }

                // Tento den obsahuje nejake rucni vazeni, misto vzorku musim zkombinovat rovnou vysledky.
                // Muzu vypocitat pouze pocet vzorku a prumernou hmotnost, nic vic.
                // Pokud je mezi rucnimi vazenimi i nejake z vahy, beru ho stejne jako rucni vazeni, tj. i vysledne
                // prumerne vazeni bude rucni.
                List<StatisticResult> resultList = new List<StatisticResult>();
                foreach (Flag flag in dayWeighingList.FlagList) {
                    if (flag == Flag.ALL) {
                        continue;       // Pro flag ALL neni treba pocitat, vypocte se samo
                    }

                    // Projedu vsechna vazeni a vypoctu prumerne vysledky pro tento flag
                    float average = 0;
                    int count     = 0;
                    foreach (Weighing weighing in dayWeighingList.List) {
                        StatisticResult result = weighing.WeighingResults.GetResult(flag);
                        if (result == null) {
                            continue;       // Tento flag vazeni neobsahuje
                        }
                        count   += result.Count;
                        average += (float)result.Count * result.ExactAverage;   // Pocitam s presnym prumerem, at nevnasim chybu
                    }

                    // Vypoctu prumernou hmotnost
                    average /= (float)count;

                    // Ulozim vysledek pro tento flag
                    resultList.Add(new StatisticResult(flag, count, average, 0, 0, 0, dayWeighingList.List[0].GetMinDateTime()));
                }

                // Pridam prumerne vazeni do seznamu
                ScaleConfig config = new ScaleConfig(dayWeighingList.UnitsList[0]);
                File file = new File("A", "", new FileConfig(config));      // Prozatimni nazev A
                file.RenameUnicode(firstWeighing.WeighingData.File.Name);   // Prejmenovat musim az po vytvoreni
                config.FileList = new FileList();
                config.FileList.Add(file);
                WeighingData weighingData = new WeighingData(-1, ResultType.MANUAL, RecordSource.SAVE, file, null, config, "");
                averagedWeighingList.Add(new Weighing(weighingData, new WeighingResults(resultList)));
            }

            return averagedWeighingList;
        }
        



        /// <summary>
        /// Load list of weighings that belong to the flock
        /// </summary>
        /// <param name="fromDate">Additional time filter (or DateTime.MinValue when NA)</param>
        /// <param name="toDate">Additional time filter (or DateTime.MaxValue when NA)</param>
        private void LoadWeighingList(DateTime fromDate, DateTime toDate) {
            // Nactu z databaze seznam ID vazeni, ktere spadaji do hejna. Seznam bude setrideny podle data.
            List<long> idList = Program.Database.ReadWeighingIdList(definition, fromDate, toDate);

            // Seskupim jednotliva vazeni podle dnu. Vazeni, ktera probihala v jeden den je treba zprumerovat do jednoho vazeni.
            List<List<Weighing>> dayUniqueWeighingList = CreateUniqueDayWeighingList(idList);

            // Zprumeruju vazeni z jednotlivych dnu a vytvorim seznam s prumernymi vysledky
            weighingList = CreateAveragedWeighingList(dayUniqueWeighingList);
        }

        /// <summary>
        /// Calculate day number of a specified date
        /// </summary>
        /// <param name="dateTime">DateTime</param>
        /// <returns>Day number</returns>
        public int DateToDay(DateTime dateTime) {
            // Pokud v hejnu neni definovan ani jeden soubor, vratim nulu
            if (definition.FlockFileList.Count == 0) {
                return 0;
            }

            // Vypoctu cislo dne
            TimeSpan span = dateTime.Date - definition.Started.Date;       // Pouze datumy
            int day = definition.StartedDay + (int)span.TotalDays;

            // Pokud vysel den zaporny, vratim nulu
            if (day < 0) {
                day = 0;
            }

            return day;
        }
    }
}
