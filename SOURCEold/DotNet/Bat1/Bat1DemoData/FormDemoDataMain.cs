﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Bat1;

namespace Bat1Db {
    public partial class FormDemoDataMain : Form {
        public FormDemoDataMain() {
            InitializeComponent();

            // Inicializuji adresarovou strukturu a databazi
            if (!Bat1.Program.InitData(null)) {
                Application.Exit();
                return;
            }

            labelVersion.Text = SwVersion.ToString();
        }

        private void buttonEmpty_Click(object sender, EventArgs e) {
            if (MessageBox.Show("All data will be ERASED. Really continue?", buttonEmpty.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }
            
            // Vyprazdnim databazi
            Bat1.Program.Database.CreateDatabase();      // Vytvorim prazdnou databazi
            Bat1.Program.Database.CreateTables();        // Vytvorim vsechny tabulky
        }

        private void buttonBroilerDemo_Click(object sender, EventArgs e) {
            TestDemoData.CreateBroilerData();
        }

        private void buttonParentsDemo_Click(object sender, EventArgs e) {
            TestDemoData.CreateParentsData();
        }

        private void buttonSortingDemo_Click(object sender, EventArgs e) {
            TestDemoData.CreateSortingData();
        }
    }
}
