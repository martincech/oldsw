﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.ScaleStatistics;
using Veit.Scale;

namespace Bat1 {
    public class TestDemoData {
        private static void SaveWeighing(SampleList sampleList, string scaleName, string fileName) {
            // Ulozim nastaveni vahy
            ScaleConfig scaleConfig = new ScaleConfig(Bat1Version7.DefaultConfig);
            scaleConfig.ScaleName = scaleName;
            scaleConfig.StatisticConfig.Histogram.Mode = HistogramMode.RANGE;
            scaleConfig.StatisticConfig.Histogram.Range = 30;
            long scaleConfigId = Program.Database.ScaleConfigsTable.Add(scaleConfig);

            // Ulozim soubor
            long fileId = Program.Database.FilesTable.Add(new File(fileName, "", new FileConfig(Bat1Version7.DefaultConfig)), scaleConfigId);

            // Ulozim vazeni
            long weighingId = Program.Database.WeighingsTable.Add(fileId, scaleConfigId, ResultType.NEW_SCALE, RecordSource.SAVE, "", sampleList.MinDateTime, sampleList.MaxDateTime);

            // Ulozim vzorky
            sampleList.First();
            while (sampleList.Read()) {
                Program.Database.SamplesTable.Add(weighingId, sampleList.Sample.weight, (short)sampleList.Sample.flag, sampleList.Sample.dateTime);
            }
        }

        public static void SaveUniformWeighing(DateTime startDateTime, string scaleName, string fileName, int samplesCount) {
            // Vytvorim vzorky
            SampleList sampleList = new SampleList();
            for (int i = 0; i < samplesCount; i++) {
                Sample sample;
                sample.weight   = (float)i / 10.0F;
                sample.flag     = Flag.NONE;
                sample.dateTime = startDateTime;
                sampleList.Add(sample);
                startDateTime = startDateTime.AddSeconds(20);
            }
            
            SaveWeighing(sampleList, scaleName, fileName);
        }

        public static SampleList CreateHistogramSampleList(DateTime startDateTime, float average, float range, int count, Flag flag, Random randomNumber) {
            // Vytvorim vzorky
            SampleList sampleList = new SampleList();
            const int STEP_COUNT = 5;
            float weightBase = average * (1.0F - range / 100.0F);
            float weightRange = average * 2.0F * range / 100.0F;        // 2 krat rozsah
            float weightStep = weightRange / (2.0F * (float)STEP_COUNT);
            List<float> weightList = new List<float>();

            // Pridam nahodnou hodnotu k poctu vzorku
            count = (int)((double)count * (0.9 + 0.1 * randomNumber.NextDouble()));
            count += randomNumber.Next(count * 2 / 10);

            // Vytvorim seznam hmotnosti
            for (int step = 0; step < STEP_COUNT; step++) {
                for (int i = 0; i < count / (STEP_COUNT + step); i++) {
                    float weight = weightBase + (float)randomNumber.NextDouble() * weightRange;
                    weightList.Add(weight);
                }

                // Posunu zakladnu hmotnosti
                weightBase += weightStep;

                // Zuzim rozsah nahodnych cisel
                weightRange -= 2.0F * weightStep;
            }
            
            // Nahodne hmotnosti poprehazim, aby nebyly monotonne vzrustajici
            List<float> randomWeightList = new List<float>();
            Random random = new Random();
            while (weightList.Count > 0) {
                int index = random.Next(weightList.Count - 1);
                randomWeightList.Add(weightList[index]);
                weightList.RemoveAt(index);
            }

            // Vytvorim seznam vzorku
            foreach (float randomWeight in randomWeightList) {
                int speedBase = 15 + randomNumber.Next(15);

                Sample sample;
                sample.weight   = randomWeight;
                sample.flag     = flag;
                sample.dateTime = startDateTime;
                sampleList.Add(sample);
                startDateTime = startDateTime.AddSeconds(speedBase + randomNumber.Next(5));
            }

            return sampleList;
        }

        public static void SaveHistogramWeighing(DateTime startDateTime, string scaleName, string fileName, float average, Flag flag, Random randomNumber) {
            // Vytvorim vzorky
            SampleList sampleList = CreateHistogramSampleList(startDateTime, average, 28.0F, 300, flag, randomNumber);
            
            // Vytvorim a vratim vazeni
            SaveWeighing(sampleList, scaleName, fileName);
        }

        public static void SaveHistogramWeighingsParents(DateTime startDateTime, string scaleName, string fileName, float averageFemales, float averageMales, Random randomNumber) {
            // Vytvorim vzorky samic
            SampleList sampleList = CreateHistogramSampleList(startDateTime, averageFemales, 15.0F, 150, Flag.FEMALE, randomNumber);
            
            // Vytvorim vzorky samcu
            SampleList sampleListMales = CreateHistogramSampleList(startDateTime, averageMales, 10.0F, 30, Flag.MALE, randomNumber);      // Samcu bude malo

            // Spojim vzorky do jednoho seznamu
            sampleListMales.First();
            while (sampleListMales.Read()) {
                sampleList.Add(sampleListMales.Sample);
            }
            
            // Vytvorim a vratim vazeni
            SaveWeighing(sampleList, scaleName, fileName);
        }

        public static void SaveHistogramWeighingsSorting(DateTime startDateTime, string scaleName, string fileName, float averageLight, float averageOk, float averageHeavy, Random randomNumber) {
            // Vytvorim vzorky Light
            SampleList sampleList = CreateHistogramSampleList(startDateTime, averageLight, 15.0F, 150, Flag.LIGHT, randomNumber);
            
            // Vytvorim vzorky Ok
            SampleList sampleListOk = CreateHistogramSampleList(startDateTime, averageOk, 10.0F, 30, Flag.OK, randomNumber);      // OK bude malo

            // Vytvorim vzorky Heavy
            SampleList sampleListHeavy = CreateHistogramSampleList(startDateTime, averageHeavy, 5.0F, 50, Flag.HEAVY, randomNumber);

            // Spojim vzorky do jednoho seznamu
            sampleListOk.First();
            while (sampleListOk.Read()) {
                sampleList.Add(sampleListOk.Sample);
            }
            sampleListHeavy.First();
            while (sampleListHeavy.Read()) {
                sampleList.Add(sampleListHeavy.Sample);
            }
            
            // Vytvorim a vratim vazeni
            SaveWeighing(sampleList, scaleName, fileName);
        }

        private static float RandomWeight(float average, int percent) {
            // Hmotnost +/- percent %
            Random random = new Random();
            int randomValue = random.Next(2 * percent);
            float weight = average * ((float)(100 - percent) / 100.0F + (float)randomValue / 100.0F);
            return weight;
        }
        
        private static void SaveJohnBroilers(DateTime dateTime, float average, Random randomNumber) {
            Random random = new Random();
            SaveHistogramWeighing(dateTime, "JOHN", "HOUSE1", RandomWeight(average, 20), Flag.NONE, randomNumber);
            dateTime = dateTime.AddMinutes(52);
            SaveHistogramWeighing(dateTime, "JOHN", "HOUSE2", RandomWeight(average, 20), Flag.NONE, randomNumber);
        }
        
        private static void SaveKateBroilers(DateTime dateTime, float average, Random randomNumber) {
            Random random = new Random();
            SaveHistogramWeighing(dateTime, "KATE", "HOUSE3", RandomWeight(average, 20), Flag.NONE, randomNumber);
        }

        private static void SaveWeighingBroilers(DateTime dateTime, float average, Random randomNumber) {
            SaveJohnBroilers(dateTime, average, randomNumber);
            SaveKateBroilers(dateTime.AddMinutes(15), average, randomNumber);
        }

        private static void SaveJohnParents(DateTime dateTime, float averageFemales, float averageMales, Random randomNumber) {
            Random random = new Random();
            SaveHistogramWeighingsParents(dateTime, "JOHN", "HOUSE1", RandomWeight(averageFemales, 3), RandomWeight(averageMales, 3), randomNumber);
            dateTime = dateTime.AddMinutes(52);
            SaveHistogramWeighingsParents(dateTime, "JOHN", "HOUSE2", RandomWeight(averageFemales, 3), RandomWeight(averageMales, 3), randomNumber);
        }
        
        private static void SaveKateParents(DateTime dateTime, float averageFemales, float averageMales, Random randomNumber) {
            Random random = new Random();
            SaveHistogramWeighingsParents(dateTime, "KATE", "HOUSE3", RandomWeight(averageFemales, 3), RandomWeight(averageMales, 3), randomNumber);
        }

        private static void SaveWeighingParents(DateTime dateTime, float averageFemales, float averageMales, Random randomNumber) {
            SaveJohnParents(dateTime, averageFemales, averageMales, randomNumber);
            SaveKateParents(dateTime, averageFemales, averageMales, randomNumber);
        }

        private static void SaveJohnSorting(DateTime dateTime, float averageLight, float averageOk, float averageHeavy, Random randomNumber) {
            Random random = new Random();
            SaveHistogramWeighingsSorting(dateTime, "JOHN", "HOUSE1", RandomWeight(averageLight, 3), RandomWeight(averageOk, 3), RandomWeight(averageHeavy, 3), randomNumber);
            dateTime = dateTime.AddMinutes(52);
            SaveHistogramWeighingsSorting(dateTime, "JOHN", "HOUSE2", RandomWeight(averageLight, 3), RandomWeight(averageOk, 3), RandomWeight(averageHeavy, 3), randomNumber);
        }
        
        private static void SaveKateSorting(DateTime dateTime, float averageLight, float averageOk, float averageHeavy, Random randomNumber) {
            Random random = new Random();
            SaveHistogramWeighingsSorting(dateTime, "KATE", "HOUSE3", RandomWeight(averageLight, 3), RandomWeight(averageOk, 3), RandomWeight(averageHeavy, 3), randomNumber);
        }

        private static void SaveWeighingSorting(DateTime dateTime, float averageLight, float averageOk, float averageHeavy, Random randomNumber) {
            SaveJohnSorting(dateTime, averageLight, averageOk, averageHeavy, randomNumber);
            SaveKateSorting(dateTime, averageLight, averageOk, averageHeavy, randomNumber);
        }

        private static void SaveCompleteBroiler(DateTime start, Random randomNumber) {
            SaveWeighingBroilers(start,                0.050F, randomNumber);
            SaveWeighingBroilers(start.AddDays(7.01),  0.197F, randomNumber);
            SaveWeighingBroilers(start.AddDays(14.02), 0.479F, randomNumber);
            SaveWeighingBroilers(start.AddDays(21.03), 0.866F, randomNumber);
            SaveWeighingBroilers(start.AddDays(28.04), 1.317F, randomNumber);
            SaveWeighingBroilers(start.AddDays(35.05), 1.787F, randomNumber);
        }

        private static void SaveIncompleteBroiler(DateTime start, Random randomNumber) {
            SaveWeighingBroilers(start,                0.050F, randomNumber);
            SaveWeighingBroilers(start.AddDays(7.01),  0.197F, randomNumber);
            SaveWeighingBroilers(start.AddDays(14.02), 0.479F, randomNumber);
            SaveWeighingBroilers(start.AddDays(21.03), 0.866F, randomNumber);
        }

        private static void SaveIncompleteParents(DateTime start, Random randomNumber) {
            SaveWeighingParents(start, 3.626F, 4.498F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingParents(start, 3.629F, 4.502F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingParents(start, 3.635F, 4.499F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingParents(start, 3.632F, 4.510F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingParents(start, 3.641F, 4.512F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingParents(start, 3.645F, 4.518F, randomNumber);
            start = start.AddDays(7.01);
        
            SaveWeighingParents(start, 3.649F, 4.522F, randomNumber);
            start = start.AddDays(7.01);
        
            SaveWeighingParents(start, 3.650F, 4.523F, randomNumber);
            start = start.AddDays(7.01);
        
            SaveWeighingParents(start, 3.655F, 4.528F, randomNumber);
            start = start.AddDays(7.01);
        }

        private static void SaveIncompleteSorting(DateTime start, Random randomNumber) {
            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);

            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);
        
            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);
        
            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);
        
            SaveWeighingSorting(start, 2.626F, 3.198F, 3.566F, randomNumber);
            start = start.AddDays(7.01);
        }

        private static void SaveFlockDefinitionBroilers(string nameBase, DateTime start, DateTime to, Curve curve) {
            FlockDefinition flockDefinition = new FlockDefinition(start.ToString("yyyy-MM-dd") + " " + nameBase);
            flockDefinition.Add(nameBase, start.Date, to);
            flockDefinition.CurveDefault = curve;
            Program.Database.SaveFlock(ref flockDefinition);
        }

        private static void SaveFlocksBroilers(DateTime start, DateTime to, Curve curve) {
            SaveFlockDefinitionBroilers("HOUSE1", start, to, curve);
            SaveFlockDefinitionBroilers("HOUSE2", start, to, curve);
            SaveFlockDefinitionBroilers("HOUSE3", start, to, curve);

            // Celkove hejno za celou farmu
            FlockDefinition flockDefinition = new FlockDefinition(start.ToString("yyyy-MM-dd") + " FARM");
            flockDefinition.Add("HOUSE1", start.Date, to);
            flockDefinition.Add("HOUSE2", start.Date, to);
            flockDefinition.Add("HOUSE3", start.Date, to);
            flockDefinition.CurveDefault = curve;
            Program.Database.SaveFlock(ref flockDefinition);
        }

        private static Curve SaveBroilerGrowthCurve() {
            Curve curve = new Curve("Cobb 500", Units.KG);
            curve.Add(1,  0.049F);
            curve.Add(5,  0.117F);
            curve.Add(10, 0.290F);
            curve.Add(15, 0.543F);
            curve.Add(20, 0.862F);
            curve.Add(25, 1.230F);
            curve.Add(30, 1.631F);
            curve.Add(35, 2.049F);
            curve.Add(40, 2.469f);
            curve.Note = "COBB 500 broilers";

            Program.Database.SaveCurve(ref curve);
            return curve;
        }

        public static void CreateBroilerData() {
            // Vyprazdnim databazi
            Program.Database.CreateDatabase();      // Vytvorim prazdnou databazi
            Program.Database.CreateTables();        // Vytvorim vsechny tabulky

            // Vytvorim 2 kompletni hejna a jedno prave bezici
            DateTime startedIncomplete = DateTime.Now.AddDays(-21);
            DateTime startedComplete1  = startedIncomplete.AddDays(-100);
            DateTime startedComplete2  = startedIncomplete.AddDays(-50);

            Program.Database.Factory.BeginTransaction();
            try {
                Random randomNumber = new Random();
                SaveCompleteBroiler(startedComplete1, randomNumber);
                SaveCompleteBroiler(startedComplete2, randomNumber);
                SaveIncompleteBroiler(startedIncomplete, randomNumber);
                
                Program.Database.Factory.CommitTransaction();
            }  catch (Exception Exception) {
                Program.Database.Factory.RollbackTransaction();
                throw Exception;
            }

            // Rustova krivka
            Curve curve = SaveBroilerGrowthCurve();
            
            // Hejna
            SaveFlocksBroilers(startedComplete1,  startedComplete1.AddDays(35), curve);
            SaveFlocksBroilers(startedComplete2,  startedComplete2.AddDays(35), curve);
            SaveFlocksBroilers(startedIncomplete, DateTime.MaxValue, curve);
        }

        private static void SaveFlockDefinitionParents(string nameBase, DateTime start, Curve curveMales, Curve curveFemales) {
            FlockDefinition flockDefinition = new FlockDefinition(start.ToString("yyyy-MM-dd") + " " + nameBase);
            flockDefinition.Add(nameBase, start.Date, DateTime.MaxValue);
            flockDefinition.CurveDefault = curveMales;
            flockDefinition.CurveFemales = curveFemales;
            flockDefinition.StartedDay   = 252;
            Program.Database.SaveFlock(ref flockDefinition);
        }

        private static void SaveFlocksParents(DateTime start, Curve curveMales, Curve curveFemales) {
            SaveFlockDefinitionParents("HOUSE1", start, curveMales, curveFemales);
            SaveFlockDefinitionParents("HOUSE2", start, curveMales, curveFemales);
            SaveFlockDefinitionParents("HOUSE3", start, curveMales, curveFemales);

            // Celkove hejno za celou farmu
            FlockDefinition flockDefinition = new FlockDefinition(start.ToString("yyyy-MM-dd") + " FARM");
            flockDefinition.Add("HOUSE1", start.Date, DateTime.MaxValue);
            flockDefinition.Add("HOUSE2", start.Date, DateTime.MaxValue);
            flockDefinition.Add("HOUSE3", start.Date, DateTime.MaxValue);
            flockDefinition.CurveDefault = curveMales;
            flockDefinition.CurveFemales = curveFemales;
            flockDefinition.StartedDay   = 252;
            Program.Database.SaveFlock(ref flockDefinition);
        }

        private static Curve SaveParentsMalesGrowthCurve() {
            Curve curve = new Curve("Males", Units.KG);
            curve.Add(250,  4.500F);
            curve.Add(300,  4.600F);
            curve.Note = "Parents - males";

            Program.Database.SaveCurve(ref curve);
            return curve;
        }

        private static Curve SaveParentsFemalesGrowthCurve() {
            Curve curve = new Curve("Females", Units.KG);
            curve.Add(250,  3.600F);
            curve.Add(300,  3.700F);
            curve.Note = "Parents - females";

            Program.Database.SaveCurve(ref curve);
            return curve;
        }

        public static void CreateParentsData() {
            // Vyprazdnim databazi
            Program.Database.CreateDatabase();      // Vytvorim prazdnou databazi
            Program.Database.CreateTables();        // Vytvorim vsechny tabulky

            // Vytvorim 1 nekompletni hejno
            DateTime started = DateTime.Now.AddDays(-5 * 7);

            Program.Database.Factory.BeginTransaction();
            try {
                SaveIncompleteParents(started, new Random());
                
                Program.Database.Factory.CommitTransaction();
            }  catch (Exception Exception) {
                Program.Database.Factory.RollbackTransaction();
                throw Exception;
            }

            // Rustova krivka
            Curve curveMales   = SaveParentsMalesGrowthCurve();
            Curve curveFemales = SaveParentsFemalesGrowthCurve();
            
            // Hejno
            SaveFlocksParents(started, curveMales, curveFemales);
        }

        private static void SaveFlockDefinitionSorting(string nameBase, DateTime start) {
            FlockDefinition flockDefinition = new FlockDefinition(start.ToString("yyyy-MM-dd") + " " + nameBase);
            flockDefinition.Add(nameBase, start.Date, DateTime.MaxValue);
            flockDefinition.StartedDay   = 252;
            Program.Database.SaveFlock(ref flockDefinition);
        }

        private static void SaveFlocksSorting(DateTime start) {
            SaveFlockDefinitionSorting("HOUSE1", start);
            SaveFlockDefinitionSorting("HOUSE2", start);
            SaveFlockDefinitionSorting("HOUSE3", start);

            // Celkove hejno za celou farmu
            FlockDefinition flockDefinition = new FlockDefinition(start.ToString("yyyy-MM-dd") + " FARM");
            flockDefinition.Add("HOUSE1", start.Date, DateTime.MaxValue);
            flockDefinition.Add("HOUSE2", start.Date, DateTime.MaxValue);
            flockDefinition.Add("HOUSE3", start.Date, DateTime.MaxValue);
            flockDefinition.StartedDay   = 252;
            Program.Database.SaveFlock(ref flockDefinition);
        }

        public static void CreateSortingData() {
            // Vyprazdnim databazi
            Program.Database.CreateDatabase();      // Vytvorim prazdnou databazi
            Program.Database.CreateTables();        // Vytvorim vsechny tabulky

            // Vytvorim 1 nekompletni hejno
            DateTime started = DateTime.Now.AddDays(-5 * 7);

            Program.Database.Factory.BeginTransaction();
            try {
                SaveIncompleteSorting(started, new Random());
                
                Program.Database.Factory.CommitTransaction();
            }  catch (Exception Exception) {
                Program.Database.Factory.RollbackTransaction();
                throw Exception;
            }

            // Hejno
            SaveFlocksSorting(started);
        }


    }
}
