﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat2;
using Veit.Csv;

namespace Bat2SmsCsv {
    public static class SaveSms {
        public static bool Save(DecodedSms decodedSms, string fileName) {
            Csv csv = new Csv(",", ".", Encoding.Default);
            int column = 0;

            csv.AddDateTime(column++, DateTime.Now);
            csv.AddString(  column++, decodedSms.PhoneNumber);
            csv.AddInteger( column++, decodedSms.ScaleNumber);
            csv.AddInteger( column++, decodedSms.DayNumber);
            csv.AddDateTime(column++, decodedSms.Date);
            csv.AddInteger( column++, decodedSms.Count);
            csv.AddDouble(  column++, double.Parse(decodedSms.Average.ToString("0.000")));
            csv.AddDouble(  column++, double.Parse(decodedSms.Gain.ToString("0.000")));
            csv.AddDouble(  column++, double.Parse(decodedSms.Sigma.ToString("0.000")));
            csv.AddDouble(  column++, double.Parse(decodedSms.Cv.ToString("0.0")));
            csv.AddInteger( column++, decodedSms.Uniformity);
            csv.SaveLine();

            try {
                return csv.SaveToFile(fileName, true);      // Append
            } catch {
                return false;
            }
        }
    }
}
