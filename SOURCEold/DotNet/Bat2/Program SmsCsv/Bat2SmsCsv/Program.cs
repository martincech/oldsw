﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using SingleInstance;

namespace Bat2SmsCsv {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            // Pokud uz program jednou bezi, nespoustim ho podruhe, ale prepnu na jiz spusteny
            if (SingleApplication.IsAlreadyRunning()) {
				SingleApplication.SwitchToCurrentInstance();
				return;
			}

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormMain());
        }
    }
}
