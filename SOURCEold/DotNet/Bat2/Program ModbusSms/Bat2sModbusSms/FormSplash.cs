﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat2sModbusSms {
    public partial class FormSplash : Form {
        public FormSplash() {
            InitializeComponent();

            // Nahraju splash screen
            try {
                pictureBoxSplash.Load("Splash.png");
            } catch {
                // Soubor neexistuje, nic se nedeje
            }
        }
    }
}
