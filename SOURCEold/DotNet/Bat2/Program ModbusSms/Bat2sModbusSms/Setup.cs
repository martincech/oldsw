﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat2sModbusSms {
    
    /// <summary>
    /// Modbus scales connection
    /// </summary>
    public struct ModbusScalesConfig {
        /// <summary>
        /// COM port
        /// </summary>
        public int PortNumber;
        
        /// <summary>
        /// Address of first scale
        /// </summary>
        public int FirstAddress;
        
        /// <summary>
        /// Address of last scale
        /// </summary>
        public int LastAddress;
    }

    /// <summary>
    /// Modbus protocol type
    /// </summary>
    public enum ModbusProtocol {
        RTU,
        ASCII
    }
    
    /// <summary>
    /// Modbus parity
    /// </summary>
    public enum ModbusParity {
        NONE,
        EVEN,
        ODD,
        MARK
    }

    /// <summary>
    /// Modbus protocol
    /// </summary>
    public struct ModbusProtocolConfig {
        /// <summary>
        /// Protocol type
        /// </summary>
        public ModbusProtocol Protocol;

        /// <summary>
        /// Baud rate
        /// </summary>
        public int Speed;

        /// <summary>
        /// Parity
        /// </summary>
        public ModbusParity Parity;

        /// <summary>
        /// Reply timeout in miliseconds
        /// </summary>
        public int ReplyTimeout;

        /// <summary>
        /// Silent interval in miliseconds (only in RTU mode)
        /// </summary>
        public int SilentInterval;

        /// <summary>
        /// Max number of attampts in communication
        /// </summary>
        public int NumberOfAttempts;
    }

    /// <summary>
    /// GSM modem
    /// </summary>
    public struct GsmSetup {
        /// <summary>
        /// COM port of the modem
        /// </summary>
        public int PortNumber;

        /// <summary>
        /// Period of sending until DayMidnight1
        /// </summary>
        public int PeriodMidnight1;     // Perioda ve dnech, kdy se ma posilat pulnocni statistika do dne DayMidnight1

        /// <summary>
        /// After this day, SMS is sent with PeriodMidnight2
        /// </summary>
        public int DayMidnight1;

        /// <summary>
        /// Period of sending after DayMidnight1
        /// </summary>
        public int PeriodMidnight2;

        /// <summary>
        /// List of phone numbers
        /// </summary>
        public List<string> MidnightNumberCollection;
    }
    
    /// <summary>
    /// Complete setup
    /// </summary>
    public class Setup {
        /// <summary>
        /// Max lenght of a phone number
        /// </summary>
        public static int GSM_NUMBER_MAX_LENGTH = 15;

        /// <summary>
        /// Max number of phone numbers
        /// </summary>
        public static int GSM_NUMBER_MAX_COUNT = 5;

        public static int MIN_GSM_PERIOD_MIDNIGHT = 1;
        public static int MAX_GSM_PERIOD_MIDNIGHT = 99;

        public static int MIN_RS485_ADDRESS         = 1;
        public static int MAX_RS485_ADDRESS         = 247;
        public static int MIN_RS485_REPLY_DELAY     = 0;
        public static int MAX_RS485_REPLY_DELAY     = 9999;
        public static int MIN_RS485_SILENT_INTERVAL = 1;
        public static int MAX_RS485_SILENT_INTERVAL = 255;
        public static int MAX_DAY                   = 999;

        /// <summary>
        /// Selected SW language
        /// </summary>
        public SwLanguage Language;
        
        /// <summary>
        /// Parameters of the Modbus protocol
        /// </summary>
        public ModbusProtocolConfig ModbusProtocol;

        /// <summary>
        /// Scales connection
        /// </summary>
        public ModbusScalesConfig ModbusScales;

        /// <summary>
        /// GSM modem setup
        /// </summary>
        public GsmSetup Gsm;

        /// <summary>
        /// Constructor
        /// </summary>
        public Setup() {
            // Nastavim defaulty
            SetDefaults();
        }
        
        /// <summary>
        /// Set default values
        /// </summary>
        public void SetDefaults() {
            Language             = SwLanguage.UNDEFINED;

            ModbusProtocol.Protocol         = Bat2sModbusSms.ModbusProtocol.RTU;
            ModbusProtocol.Speed            = 9600;
            ModbusProtocol.Parity           = ModbusParity.EVEN;
            ModbusProtocol.ReplyTimeout     = 1000;
            ModbusProtocol.SilentInterval   = 50;
            ModbusProtocol.NumberOfAttempts = 2;

            ModbusScales.PortNumber   = 1;
            ModbusScales.FirstAddress = -1;
            ModbusScales.LastAddress  = -1;

            Gsm.PortNumber      = 2;
            Gsm.PeriodMidnight1 = 1;
            Gsm.DayMidnight1    = 999;
            Gsm.PeriodMidnight2 = 1;
            Gsm.MidnightNumberCollection = new List<string>();
        }
    }
}
