﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Veit.KeyFilter;
using Veit.CheckValue;

namespace Bat2sModbusSms {
    public partial class FormSetup : Form {

        /// <summary>
        /// Setup to edit
        /// </summary>
        private Setup setup;

        private List<TextBox> numberTextBoxCollection;

        private readonly List<int> speedCollection;
        
        public FormSetup(Setup setup) {
            InitializeComponent();

            // Preberu data
            this.setup = setup;

            // Vytvorim seznam TextBoxu
            numberTextBoxCollection = new List<TextBox>();
            numberTextBoxCollection.Add(textBoxNumber1);
            numberTextBoxCollection.Add(textBoxNumber2);
            numberTextBoxCollection.Add(textBoxNumber3);
            numberTextBoxCollection.Add(textBoxNumber4);
            numberTextBoxCollection.Add(textBoxNumber5);

            // Vytvorim seznam rychlosti
            speedCollection = new List<int>();
            speedCollection.Add(1200);
            speedCollection.Add(2400);
            speedCollection.Add(4800);
            speedCollection.Add(9600);
            speedCollection.Add(19200);
            speedCollection.Add(38400);
            speedCollection.Add(57600);
            speedCollection.Add(115200);

            Redraw();
        }

        private void Redraw() {
            // Jazyk
            comboBoxLanguage.SelectedIndex = (int)setup.Language;

            // RS-485
            comboBoxProtocol.SelectedIndex = (int)setup.ModbusProtocol.Protocol;
            comboBoxSpeed.SelectedIndex    = speedCollection.IndexOf(setup.ModbusProtocol.Speed);
            comboBoxParity.SelectedIndex   = (int)setup.ModbusProtocol.Parity;
            textBoxReplyTimeout.Text       = setup.ModbusProtocol.ReplyTimeout.ToString();
            textBoxSilentInterval.Text     = setup.ModbusProtocol.SilentInterval.ToString();
            textBoxAttempts.Text           = setup.ModbusProtocol.NumberOfAttempts.ToString();

            // Vahy
            comboBoxScalesComPort.SelectedIndex = setup.ModbusScales.PortNumber - 1;
            textBoxScalesFirst.Text             = setup.ModbusScales.FirstAddress >= 0 ? setup.ModbusScales.FirstAddress.ToString() : "";
            textBoxScalesLast.Text              = setup.ModbusScales.LastAddress  >= 0 ? setup.ModbusScales.LastAddress.ToString()  : "";

            // GSM
            comboBoxModemComPort.SelectedIndex = setup.Gsm.PortNumber - 1;
            textBoxPeriod1.Text                = setup.Gsm.PeriodMidnight1.ToString();
            textBoxDay1.Text                   = setup.Gsm.DayMidnight1.ToString();
            textBoxPeriod2.Text                = setup.Gsm.PeriodMidnight2.ToString();
            foreach (TextBox textBox in numberTextBoxCollection) {
                textBox.Clear();
            }
            for (int i = 0; i < setup.Gsm.MidnightNumberCollection.Count; i++) {
                numberTextBoxCollection[i].Text = setup.Gsm.MidnightNumberCollection[i];
            }
        }

        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Check entered values
        /// </summary>
        /// <returns>True if all values are valid</returns>
        private bool CheckData() {
            // RS-485
            if (!CheckValue.IsInt(textBoxReplyTimeout, 0, 2000)) {
                return false;
            }
            if (!CheckValue.IsInt(textBoxSilentInterval, Setup.MIN_RS485_SILENT_INTERVAL, Setup.MAX_RS485_SILENT_INTERVAL)) {
                return false;
            }
            if (!CheckValue.IsInt(textBoxAttempts, 1, 9)) {
                return false;
            }

            // Vahy
            if (!CheckValue.IsInt(textBoxScalesFirst, Setup.MIN_RS485_ADDRESS, Setup.MAX_RS485_ADDRESS)) {
                return false;
            }
            if (!CheckValue.IsInt(textBoxScalesLast, Setup.MIN_RS485_ADDRESS, Setup.MAX_RS485_ADDRESS)) {
                return false;
            }

            // Gsm
            if (!CheckValue.IsInt(textBoxPeriod1, Setup.MIN_GSM_PERIOD_MIDNIGHT, Setup.MAX_GSM_PERIOD_MIDNIGHT)) {
                return false;
            }
            if (!CheckValue.IsInt(textBoxDay1, 0, Setup.MAX_DAY)) {
                return false;
            }
            if (!CheckValue.IsInt(textBoxPeriod2, Setup.MIN_GSM_PERIOD_MIDNIGHT, Setup.MAX_GSM_PERIOD_MIDNIGHT)) {
                return false;
            }

            // Cisla
            char[] validNumberChars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
            foreach (TextBox textBox in numberTextBoxCollection) {
                textBox.Text = textBox.Text.Trim();
                if (textBox.Text == "") {
                    break;      // Prvni nezadane cislo konci
                }
                if (!CheckValue.CheckText(textBox, validNumberChars)) {
                    return false;
                }
            }

            return true;
        }

        private void SaveData() {
            // Jazyk
            setup.Language = (SwLanguage)comboBoxLanguage.SelectedIndex;

            // RS-485
            setup.ModbusProtocol.Protocol         = (ModbusProtocol)comboBoxProtocol.SelectedIndex;
            setup.ModbusProtocol.Speed            = speedCollection[comboBoxSpeed.SelectedIndex];
            setup.ModbusProtocol.Parity           = (ModbusParity)comboBoxParity.SelectedIndex;
            setup.ModbusProtocol.ReplyTimeout     = Int32.Parse(textBoxReplyTimeout.Text);
            setup.ModbusProtocol.SilentInterval   = Int32.Parse(textBoxSilentInterval.Text);
            setup.ModbusProtocol.NumberOfAttempts = Int32.Parse(textBoxAttempts.Text);

            // Vahy
            setup.ModbusScales.PortNumber   = comboBoxScalesComPort.SelectedIndex + 1;
            setup.ModbusScales.FirstAddress = Int32.Parse(textBoxScalesFirst.Text);
            setup.ModbusScales.LastAddress  = Int32.Parse(textBoxScalesLast.Text);

            // GSM
            setup.Gsm.PortNumber      = comboBoxModemComPort.SelectedIndex + 1;
            setup.Gsm.PeriodMidnight1 = Int32.Parse(textBoxPeriod1.Text);
            setup.Gsm.DayMidnight1    = Int32.Parse(textBoxDay1.Text);
            setup.Gsm.PeriodMidnight2 = Int32.Parse(textBoxPeriod2.Text);
            setup.Gsm.MidnightNumberCollection.Clear();
            foreach (TextBox textBox in numberTextBoxCollection) {
                string text = textBox.Text.Trim();
                if (text == "") {
                    break;      // Prvni nezadane cislo konci
                }
                setup.Gsm.MidnightNumberCollection.Add(text);
            }
        }
        
        private void buttonOk_Click(object sender, EventArgs e) {
            if (!CheckData()) {
                return;
            }
            SaveData();
            DialogResult = DialogResult.OK;
        }

    }
}
