﻿namespace Bat2sModbusSms {
    partial class FormSetup {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.groupBoxScalesVersion6 = new System.Windows.Forms.GroupBox();
            this.textBoxAttempts = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxSilentInterval = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxReplyTimeout = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxParity = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxSpeed = new System.Windows.Forms.ComboBox();
            this.labelUnits = new System.Windows.Forms.Label();
            this.comboBoxProtocol = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxLanguage = new System.Windows.Forms.GroupBox();
            this.comboBoxLanguage = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxScalesLast = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxScalesFirst = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxScalesComPort = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxNumber5 = new System.Windows.Forms.TextBox();
            this.textBoxNumber4 = new System.Windows.Forms.TextBox();
            this.textBoxNumber3 = new System.Windows.Forms.TextBox();
            this.textBoxNumber2 = new System.Windows.Forms.TextBox();
            this.textBoxNumber1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxPeriod2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxDay1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBoxPeriod1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxModemComPort = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBoxScalesVersion6.SuspendLayout();
            this.groupBoxLanguage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxScalesVersion6
            // 
            this.groupBoxScalesVersion6.Controls.Add(this.textBoxAttempts);
            this.groupBoxScalesVersion6.Controls.Add(this.label4);
            this.groupBoxScalesVersion6.Controls.Add(this.textBoxSilentInterval);
            this.groupBoxScalesVersion6.Controls.Add(this.label3);
            this.groupBoxScalesVersion6.Controls.Add(this.textBoxReplyTimeout);
            this.groupBoxScalesVersion6.Controls.Add(this.label2);
            this.groupBoxScalesVersion6.Controls.Add(this.comboBoxParity);
            this.groupBoxScalesVersion6.Controls.Add(this.label1);
            this.groupBoxScalesVersion6.Controls.Add(this.comboBoxSpeed);
            this.groupBoxScalesVersion6.Controls.Add(this.labelUnits);
            this.groupBoxScalesVersion6.Controls.Add(this.comboBoxProtocol);
            this.groupBoxScalesVersion6.Controls.Add(this.label6);
            this.groupBoxScalesVersion6.Location = new System.Drawing.Point(12, 92);
            this.groupBoxScalesVersion6.Name = "groupBoxScalesVersion6";
            this.groupBoxScalesVersion6.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxScalesVersion6.Size = new System.Drawing.Size(156, 269);
            this.groupBoxScalesVersion6.TabIndex = 60;
            this.groupBoxScalesVersion6.TabStop = false;
            this.groupBoxScalesVersion6.Text = "RS-485 network";
            // 
            // textBoxAttempts
            // 
            this.textBoxAttempts.Location = new System.Drawing.Point(14, 235);
            this.textBoxAttempts.MaxLength = 1;
            this.textBoxAttempts.Name = "textBoxAttempts";
            this.textBoxAttempts.Size = new System.Drawing.Size(130, 20);
            this.textBoxAttempts.TabIndex = 89;
            this.textBoxAttempts.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 88;
            this.label4.Text = "Number of attempts:";
            // 
            // textBoxSilentInterval
            // 
            this.textBoxSilentInterval.Location = new System.Drawing.Point(14, 196);
            this.textBoxSilentInterval.MaxLength = 3;
            this.textBoxSilentInterval.Name = "textBoxSilentInterval";
            this.textBoxSilentInterval.Size = new System.Drawing.Size(130, 20);
            this.textBoxSilentInterval.TabIndex = 87;
            this.textBoxSilentInterval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 86;
            this.label3.Text = "Silent interval [ms]:";
            // 
            // textBoxReplyTimeout
            // 
            this.textBoxReplyTimeout.Location = new System.Drawing.Point(14, 157);
            this.textBoxReplyTimeout.MaxLength = 4;
            this.textBoxReplyTimeout.Name = "textBoxReplyTimeout";
            this.textBoxReplyTimeout.Size = new System.Drawing.Size(130, 20);
            this.textBoxReplyTimeout.TabIndex = 61;
            this.textBoxReplyTimeout.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 61;
            this.label2.Text = "Reply timeout [ms]:";
            // 
            // comboBoxParity
            // 
            this.comboBoxParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxParity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxParity.FormattingEnabled = true;
            this.comboBoxParity.Items.AddRange(new object[] {
            "None",
            "Even",
            "Odd",
            "Mark"});
            this.comboBoxParity.Location = new System.Drawing.Point(14, 117);
            this.comboBoxParity.Name = "comboBoxParity";
            this.comboBoxParity.Size = new System.Drawing.Size(130, 21);
            this.comboBoxParity.TabIndex = 84;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(11, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 85;
            this.label1.Text = "Parity:";
            // 
            // comboBoxSpeed
            // 
            this.comboBoxSpeed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.comboBoxSpeed.FormattingEnabled = true;
            this.comboBoxSpeed.Items.AddRange(new object[] {
            "1 200 Bd",
            "2 400 Bd",
            "4 800 Bd",
            "9 600 Bd",
            "19 200 Bd",
            "38 400 Bd",
            "57 600 Bd",
            "115 200 Bd"});
            this.comboBoxSpeed.Location = new System.Drawing.Point(14, 77);
            this.comboBoxSpeed.Name = "comboBoxSpeed";
            this.comboBoxSpeed.Size = new System.Drawing.Size(130, 21);
            this.comboBoxSpeed.TabIndex = 82;
            // 
            // labelUnits
            // 
            this.labelUnits.AutoSize = true;
            this.labelUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelUnits.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelUnits.Location = new System.Drawing.Point(11, 61);
            this.labelUnits.Name = "labelUnits";
            this.labelUnits.Size = new System.Drawing.Size(114, 13);
            this.labelUnits.TabIndex = 83;
            this.labelUnits.Text = "Communication speed:";
            // 
            // comboBoxProtocol
            // 
            this.comboBoxProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProtocol.FormattingEnabled = true;
            this.comboBoxProtocol.Items.AddRange(new object[] {
            "MODBUS RTU",
            "MODBUS ASCII"});
            this.comboBoxProtocol.Location = new System.Drawing.Point(14, 37);
            this.comboBoxProtocol.Name = "comboBoxProtocol";
            this.comboBoxProtocol.Size = new System.Drawing.Size(130, 21);
            this.comboBoxProtocol.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label6.Location = new System.Drawing.Point(11, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 36;
            this.label6.Text = "Protocol:";
            // 
            // groupBoxLanguage
            // 
            this.groupBoxLanguage.Controls.Add(this.comboBoxLanguage);
            this.groupBoxLanguage.Location = new System.Drawing.Point(12, 12);
            this.groupBoxLanguage.Name = "groupBoxLanguage";
            this.groupBoxLanguage.Padding = new System.Windows.Forms.Padding(8);
            this.groupBoxLanguage.Size = new System.Drawing.Size(156, 70);
            this.groupBoxLanguage.TabIndex = 59;
            this.groupBoxLanguage.TabStop = false;
            this.groupBoxLanguage.Text = "Language";
            // 
            // comboBoxLanguage
            // 
            this.comboBoxLanguage.DropDownHeight = 160;
            this.comboBoxLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLanguage.FormattingEnabled = true;
            this.comboBoxLanguage.IntegralHeight = false;
            this.comboBoxLanguage.Items.AddRange(new object[] {
            "English"});
            this.comboBoxLanguage.Location = new System.Drawing.Point(14, 37);
            this.comboBoxLanguage.Name = "comboBoxLanguage";
            this.comboBoxLanguage.Size = new System.Drawing.Size(130, 21);
            this.comboBoxLanguage.TabIndex = 49;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxScalesLast);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxScalesFirst);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBoxScalesComPort);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(12, 367);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox1.Size = new System.Drawing.Size(156, 151);
            this.groupBox1.TabIndex = 61;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Scales";
            // 
            // textBoxScalesLast
            // 
            this.textBoxScalesLast.Location = new System.Drawing.Point(14, 116);
            this.textBoxScalesLast.MaxLength = 3;
            this.textBoxScalesLast.Name = "textBoxScalesLast";
            this.textBoxScalesLast.Size = new System.Drawing.Size(130, 20);
            this.textBoxScalesLast.TabIndex = 65;
            this.textBoxScalesLast.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 100);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 64;
            this.label8.Text = "Last address:";
            // 
            // textBoxScalesFirst
            // 
            this.textBoxScalesFirst.Location = new System.Drawing.Point(14, 77);
            this.textBoxScalesFirst.MaxLength = 3;
            this.textBoxScalesFirst.Name = "textBoxScalesFirst";
            this.textBoxScalesFirst.Size = new System.Drawing.Size(130, 20);
            this.textBoxScalesFirst.TabIndex = 63;
            this.textBoxScalesFirst.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 62;
            this.label5.Text = "First address:";
            // 
            // comboBoxScalesComPort
            // 
            this.comboBoxScalesComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxScalesComPort.FormattingEnabled = true;
            this.comboBoxScalesComPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15",
            "COM16",
            "COM17",
            "COM18",
            "COM19",
            "COM20"});
            this.comboBoxScalesComPort.Location = new System.Drawing.Point(14, 37);
            this.comboBoxScalesComPort.Name = "comboBoxScalesComPort";
            this.comboBoxScalesComPort.Size = new System.Drawing.Size(130, 21);
            this.comboBoxScalesComPort.TabIndex = 48;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label7.Location = new System.Drawing.Point(11, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Connection:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxNumber5);
            this.groupBox2.Controls.Add(this.textBoxNumber4);
            this.groupBox2.Controls.Add(this.textBoxNumber3);
            this.groupBox2.Controls.Add(this.textBoxNumber2);
            this.groupBox2.Controls.Add(this.textBoxNumber1);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.textBoxPeriod2);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.textBoxDay1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBoxPeriod1);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.comboBoxModemComPort);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(183, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox2.Size = new System.Drawing.Size(156, 331);
            this.groupBox2.TabIndex = 62;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "GSM modem";
            // 
            // textBoxNumber5
            // 
            this.textBoxNumber5.Location = new System.Drawing.Point(14, 298);
            this.textBoxNumber5.MaxLength = 15;
            this.textBoxNumber5.Name = "textBoxNumber5";
            this.textBoxNumber5.Size = new System.Drawing.Size(130, 20);
            this.textBoxNumber5.TabIndex = 77;
            this.textBoxNumber5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // textBoxNumber4
            // 
            this.textBoxNumber4.Location = new System.Drawing.Point(14, 272);
            this.textBoxNumber4.MaxLength = 15;
            this.textBoxNumber4.Name = "textBoxNumber4";
            this.textBoxNumber4.Size = new System.Drawing.Size(130, 20);
            this.textBoxNumber4.TabIndex = 75;
            this.textBoxNumber4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // textBoxNumber3
            // 
            this.textBoxNumber3.Location = new System.Drawing.Point(14, 246);
            this.textBoxNumber3.MaxLength = 15;
            this.textBoxNumber3.Name = "textBoxNumber3";
            this.textBoxNumber3.Size = new System.Drawing.Size(130, 20);
            this.textBoxNumber3.TabIndex = 73;
            this.textBoxNumber3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // textBoxNumber2
            // 
            this.textBoxNumber2.Location = new System.Drawing.Point(14, 220);
            this.textBoxNumber2.MaxLength = 15;
            this.textBoxNumber2.Name = "textBoxNumber2";
            this.textBoxNumber2.Size = new System.Drawing.Size(130, 20);
            this.textBoxNumber2.TabIndex = 71;
            this.textBoxNumber2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // textBoxNumber1
            // 
            this.textBoxNumber1.Location = new System.Drawing.Point(14, 194);
            this.textBoxNumber1.MaxLength = 15;
            this.textBoxNumber1.Name = "textBoxNumber1";
            this.textBoxNumber1.Size = new System.Drawing.Size(130, 20);
            this.textBoxNumber1.TabIndex = 69;
            this.textBoxNumber1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 178);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 68;
            this.label13.Text = "Phone numbers:";
            // 
            // textBoxPeriod2
            // 
            this.textBoxPeriod2.Location = new System.Drawing.Point(14, 155);
            this.textBoxPeriod2.MaxLength = 2;
            this.textBoxPeriod2.Name = "textBoxPeriod2";
            this.textBoxPeriod2.Size = new System.Drawing.Size(130, 20);
            this.textBoxPeriod2.TabIndex = 67;
            this.textBoxPeriod2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 139);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 13);
            this.label12.TabIndex = 66;
            this.label12.Text = "Period 2:";
            // 
            // textBoxDay1
            // 
            this.textBoxDay1.Location = new System.Drawing.Point(14, 116);
            this.textBoxDay1.MaxLength = 3;
            this.textBoxDay1.Name = "textBoxDay1";
            this.textBoxDay1.Size = new System.Drawing.Size(130, 20);
            this.textBoxDay1.TabIndex = 65;
            this.textBoxDay1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 100);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 64;
            this.label9.Text = "Day 1:";
            // 
            // textBoxPeriod1
            // 
            this.textBoxPeriod1.Location = new System.Drawing.Point(14, 77);
            this.textBoxPeriod1.MaxLength = 2;
            this.textBoxPeriod1.Name = "textBoxPeriod1";
            this.textBoxPeriod1.Size = new System.Drawing.Size(130, 20);
            this.textBoxPeriod1.TabIndex = 63;
            this.textBoxPeriod1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 61);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 62;
            this.label10.Text = "Period 1:";
            // 
            // comboBoxModemComPort
            // 
            this.comboBoxModemComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxModemComPort.FormattingEnabled = true;
            this.comboBoxModemComPort.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15",
            "COM16",
            "COM17",
            "COM18",
            "COM19",
            "COM20"});
            this.comboBoxModemComPort.Location = new System.Drawing.Point(14, 37);
            this.comboBoxModemComPort.Name = "comboBoxModemComPort";
            this.comboBoxModemComPort.Size = new System.Drawing.Size(130, 21);
            this.comboBoxModemComPort.TabIndex = 48;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.label11.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label11.Location = new System.Drawing.Point(11, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Connection:";
            // 
            // buttonOk
            // 
            this.buttonOk.Location = new System.Drawing.Point(264, 466);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 63;
            this.buttonOk.Text = "OK";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(264, 495);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 64;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // FormSetup
            // 
            this.AcceptButton = this.buttonOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(351, 532);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxScalesVersion6);
            this.Controls.Add(this.groupBoxLanguage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSetup";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Setup";
            this.groupBoxScalesVersion6.ResumeLayout(false);
            this.groupBoxScalesVersion6.PerformLayout();
            this.groupBoxLanguage.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxScalesVersion6;
        private System.Windows.Forms.ComboBox comboBoxSpeed;
        private System.Windows.Forms.Label labelUnits;
        private System.Windows.Forms.ComboBox comboBoxProtocol;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBoxLanguage;
        private System.Windows.Forms.ComboBox comboBoxLanguage;
        private System.Windows.Forms.ComboBox comboBoxParity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxReplyTimeout;
        private System.Windows.Forms.TextBox textBoxAttempts;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxSilentInterval;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxScalesComPort;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxScalesLast;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxScalesFirst;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxDay1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxPeriod1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxModemComPort;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxNumber5;
        private System.Windows.Forms.TextBox textBoxNumber4;
        private System.Windows.Forms.TextBox textBoxNumber3;
        private System.Windows.Forms.TextBox textBoxNumber2;
        private System.Windows.Forms.TextBox textBoxNumber1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxPeriod2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
    }
}