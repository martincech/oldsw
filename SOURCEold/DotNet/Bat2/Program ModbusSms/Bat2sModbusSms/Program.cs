﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using Veit.SFolders;
using SingleInstance;
using Veit.CheckValue;

namespace Bat2sModbusSms {
    static class Program {
        /// <summary>
        /// Application name
        /// </summary>
        public const string ApplicationName = "BAT2s ModbusSms";

        /// <summary>
        /// Application setup
        /// </summary>
        public static Setup Setup;
 
        /// <summary>
        /// Data folder name in user's profile
        /// </summary>
        public static string DataFolder { get { return dataFolder; } }
        private static string dataFolder;

        /// <summary>
        /// Folder for temporary files in user's profile
        /// </summary>
        public static string TempFolder { get { return tempFolder; } }
        private static string tempFolder;

        /// <summary>
        /// Database instance with config
        /// </summary>
        public static DatabaseSetup DatabaseSetup;

        /// <summary>
        /// Prepare working directories
        /// </summary>
        public static void PrepareDirectories() {
            // Nactu nazev pracovniho adresare (struktura "Veit\Bat1\V7")
            SFolders sFolders = new SFolders(@"Veit\Bat2sModbusSms\V" + SwVersion.MAJOR.ToString());
            sFolders.PrepareData(null);             // Pokud neexistuje, vytvorim pracovni adresar
            dataFolder = sFolders.DataFolder;       // Zapamatuju si pracovni adresar
            
            // Adresar pro docasne soubory
            tempFolder = dataFolder + @"\Temp";     // Zapamatuju si adresar pro docasne soubory
            if (!Directory.Exists(tempFolder)) {
                // Pokud neexistuje, vytvorim
                Directory.CreateDirectory(tempFolder);  
            }
        }

        /// <summary>
        /// Database initialization
        /// </summary>
        private static bool InitDatabase(FormSplash splash) {
            // Zalozim objekt databaze
            DatabaseSetup = new DatabaseSetup();

            // Zkontroluju databazi
            if (DatabaseSetup.Exists()) {
                // Zkontroluju integritu dat v databazi
                if (!DatabaseSetup.CheckIntegrity()) {
                    // Korupce dat, musi obnovit ze zalohy
                    if (splash != null) {
                        splash.Hide();          // Schovam splash screen
                    }
                    try {
/*                        MessageBox.Show(Properties.Resources.DATABASE_CORRUPTED, ApplicationName);
                        FormRestoreBackup form = new FormRestoreBackup();
                        if (form.ShowDialog() != DialogResult.OK) {
                            return false;
                        }*/
                    } finally {
                        if (splash != null) {
                            splash.Show();      // Na zaver opet zobrazim
                        }
                    }
                }

                // Zkontroluji platnost nastaveni
//                Database.CheckSetup();
                
                // Zkontroluji verzi databaze (az po pripadne obnove ze zalohy)
//                Database.Update();              

                // Provedu automatickou zalohu databaze po startu programu
//                StartupBackup.Backup(true);     // Integritu jsem uz otestoval, neni treba znovu

                // Periodicka zaloha kazdych 7 dni
//                PeriodicBackup.Execute();
            
            } else {
                // Database neexistuje
                DatabaseSetup.CreateDatabase();      // Vytvorim prazdnou databazi
                DatabaseSetup.CreateTables();        // Vytvorim vsechny tabulky
            }

            return true;
        }

        /// <summary>
        /// Data directory and database initialization
        /// </summary>
        public static bool InitData(FormSplash splash) {
            // Priprava adresarove struktury - trva kratce, text ve splashi ani nezobrazuju
            PrepareDirectories();

            // Inicializuji databazi
            if (!InitDatabase(splash)) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            // Pokud uz program jednou bezi, nespoustim ho podruhe, ale prepnu na jiz spusteny
            if (SingleApplication.IsAlreadyRunning()) {
				SingleApplication.SwitchToCurrentInstance();
				return;
			}
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Zobrazim splash
            FormSplash splash = new FormSplash();
            splash.Show();
            splash.Refresh();       // Aby se hned vykreslilo

            try {
                // Inicializuji adresarovou strukturu a databazi
                if (!InitData(splash)) {
                    Application.Exit();
                    return;
                }

                // Nactu nastaveni programu
                DatabaseSetup.LoadSetup();
            } finally {
                splash.Close();
                splash.Dispose();
            }

            // Pokud zatim neni zvoleny zadny jazyk, necham uzivatele vybrat
/*            if (Setup.Language == SwLanguage.UNDEFINED) {
                new FormLanguage().ShowDialog();    // Bud vybere, nebo zustane UNDEFINED (vykresli se anglictina)
            }
            
            // Nastavim jazyk
            SwLanguageClass.SetLanguage(Setup.Language);*/

            // Inicializace CHeckValue
            CheckValue.InvalidValueText = Properties.Resources.VALUE_NOT_VALID;
            CheckValue.ApplicationName  = Program.ApplicationName;
        

            Application.Run(new FormMain());
        }
    }
}
