﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat2Modbus;

namespace Bat2sModbusSms {
    public class ScaleState {
        /// <summary>
        /// Port number and address of the scale
        /// </summary>
        public Bat2Slave Slave;
        
        /// <summary>
        /// Scale is responding
        /// </summary>
        public bool IsOnline;

        /// <summary>
        /// Counter of all sent messages
        /// </summary>
        public int MessageCounter;

        /// <summary>
        /// Counter of wrong replies
        /// </summary>
        public int ErrorCounter;

        /// <summary>
        /// Current state of the scale
        /// </summary>
        public CurrentState State;

        public ScaleState(int portNumber, int address) {
            Slave.PortNumber = portNumber;
            Slave.Address    = address;
            IsOnline         = false;
            MessageCounter   = 0;
            ErrorCounter     = 0;
            State            = new CurrentState();
        }
    }
}
