﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat2sModbusSms {
    /// <summary>
    /// Formatting of numbers for display, so the display is the same as in the scale
    /// </summary>
    public static class DisplayFormat {
        /// <summary>
        /// Return rounded weight in format to display.
        /// </summary>
        /// <param name="weight">Weight</param>
        /// <returns>String to display</returns>
        public static string RoundWeight(double weight) {
            // Kg i Lb beru nastejno, zaokrouhlim na 3 desetinna mista
            return Math.Round(weight, 3).ToString("N3");
        }

        /// <summary>
        /// Return CV in format for display
        /// </summary>
        /// <param name="cv">CV</param>
        /// <returns>String to display</returns>
        public static string Cv(float cv) {
            // CV zobrazuju na desetiny a zbytek useknu
            // CV musim jeste pred vypoctem zaokrouhlit, jinak pocita spatne. Musim zaorkouhlit na vice des. mist nez 1.
            double d = Math.Round(cv, 2) * 10.0;    
            int i = (int)d;
            d = (double)i / 10.0;
            return d.ToString("N1");
        }
    }
}
