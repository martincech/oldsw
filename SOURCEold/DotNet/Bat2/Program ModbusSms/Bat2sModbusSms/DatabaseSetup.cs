﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat2sModbusSms {
    /// <summary>
    /// Database with setup data stored on local computer, not shared in the network.
    /// </summary>
    class DatabaseSetup : Database {
        /// <summary>
        /// Setup table
        /// </summary>
        public  DatabaseTableSetup SetupTable { get { return setupTable; } }
        private DatabaseTableSetup setupTable;

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseSetup()
            : base("Bat2sModbusSmsSetup") {

            // Vytvorim vsechny tabulky
            setupTable = new DatabaseTableSetup(factory);
        }

        /// <summary>
        /// Create all tables
        /// </summary>
        public void CreateTables() {
            // Otevru spojeni
            factory.OpenConnection();

            try {
                setupTable.Create();
            } finally {
                // Nakonec vzdy zavru spojeni
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load application setup from database
        /// </summary>
        public void LoadSetup() {
            factory.OpenConnection();
            try {
                Program.Setup = setupTable.Load();
            } finally {
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Save application setup to database
        /// </summary>
        public void SaveSetup() {
            factory.OpenConnection();
            try {
                setupTable.Save(Program.Setup);
            } finally {
                factory.CloseConnection();
            }
        }

    }
}
