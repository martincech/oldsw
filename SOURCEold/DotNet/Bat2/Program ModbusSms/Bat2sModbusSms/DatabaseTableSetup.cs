﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat2sModbusSms {
    /// <summary>
    /// Table with SW setup
    /// </summary>
    public class DatabaseTableSetup : DatabaseTable {
    
        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableSetup(DatabaseFactory factory)
            : base("Setup", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                + "MajorVersion "         + factory.TypeSmallInt + ","     // Major cislo verze SW (1.xx.xx.xx)
                + "MinorVersion "         + factory.TypeSmallInt + ","     // Minor cislo verze SW (x.01.xx.xx)
                + "DatabaseVersion "      + factory.TypeSmallInt + ","     // Cislo verze databaze SW (x.xx.01.xx)
                + "BuildVersion "         + factory.TypeSmallInt + ","     // Cislo buildu SW (x.xx.xx.01)
                + "Language "             + factory.TypeSmallInt + ","     // Jazyk SW
                + "ModbusProtocol "       + factory.TypeByte + ","         
                + "ModbusSpeed "          + factory.TypeInteger + ","         
                + "ModbusParity "         + factory.TypeByte + ","         
                + "ModbusReplyTimeout "   + factory.TypeSmallInt + ","         
                + "ModbusSilentInterval " + factory.TypeSmallInt + ","         
                + "ModbusNumberOfAttempts " + factory.TypeByte + ","
                + "ScalesPortNumber "     + factory.TypeByte + ","
                + "ScalesFirstAddress "   + factory.TypeByte + ","
                + "ScalesLastAddress "    + factory.TypeByte + ","
                + "GsmPortNumber "        + factory.TypeByte + ","
                + "GsmPeriodMidnight1 "   + factory.TypeByte + ","
                + "GsmDayMidnight1 "      + factory.TypeSmallInt + ","
                + "GsmPeriodMidnight2 "   + factory.TypeByte + ","
                + "GsmMidnightNumber1 "   + factory.TypeNVarChar + "(" + Setup.GSM_NUMBER_MAX_LENGTH + ") CHARACTER SET UTF8,"
                + "GsmMidnightNumber2 "   + factory.TypeNVarChar + "(" + Setup.GSM_NUMBER_MAX_LENGTH + ") CHARACTER SET UTF8,"
                + "GsmMidnightNumber3 "   + factory.TypeNVarChar + "(" + Setup.GSM_NUMBER_MAX_LENGTH + ") CHARACTER SET UTF8,"
                + "GsmMidnightNumber4 "   + factory.TypeNVarChar + "(" + Setup.GSM_NUMBER_MAX_LENGTH + ") CHARACTER SET UTF8,"
                + "GsmMidnightNumber5 "   + factory.TypeNVarChar + "(" + Setup.GSM_NUMBER_MAX_LENGTH + ") CHARACTER SET UTF8"
                + ")");

            // Zaroven vyplnim default hodnoty
            SetDefaults();
        }

        /// <summary>
        /// Set default values
        /// </summary>
        private void SetDefaults() {
            Save(new Setup());          // Konstruktor zaroven vytvori default parametry
        }

        /// <summary>
        /// Update table definition
        /// </summary>
        public void Update() {
        }

        /// <summary>
        /// Add version data to a command
        /// </summary>
        private void AddVersionToCommand(DbCommand command) {
            command.Parameters.Add(factory.CreateParameter("@MajorVersion",    (short) SwVersion.MAJOR));
            command.Parameters.Add(factory.CreateParameter("@MinorVersion",    (short) SwVersion.MINOR));
            command.Parameters.Add(factory.CreateParameter("@DatabaseVersion", (short) SwVersion.DATABASE));
            command.Parameters.Add(factory.CreateParameter("@BuildVersion",    (short) SwVersion.BUILD));
        }

        /// <summary>
        /// Save version data to database
        /// </summary>
        public void SaveVersion() {
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "MajorVersion = @MajorVersion, "
                                                           + "MinorVersion = @MinorVersion, "
                                                           + "DatabaseVersion = @DatabaseVersion, "
                                                           + "BuildVersion = @BuildVersion")) {
                command.Parameters.Clear();
                AddVersionToCommand(command);
                command.ExecuteScalar();
            }
        }

        private void SaveGsmNumber(DbCommand command, Setup setup, int index) {
            index++;
            if (setup.Gsm.MidnightNumberCollection.Count <= index - 1) {
                command.Parameters.Add(factory.CreateParameter("@GsmMidnightNumber" + index.ToString(), DBNull.Value));
                return;
            }
            command.Parameters.Add(factory.CreateParameter("@GsmMidnightNumber" + index.ToString(), setup.Gsm.MidnightNumberCollection[index - 1]));
        }

        /// <summary>
        /// Save setup to database including version info
        /// </summary>
        /// <param name="setup">Setup to save</param>
        public void Save(Setup setup) {
            // Smazu vsechny dosavadni radky v tabulce
            Clear();

            // Pridam novy radek s default hodnotami
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " (MajorVersion, MinorVersion, DatabaseVersion, BuildVersion, Language, "
                                                           + "ModbusProtocol, ModbusSpeed, ModbusParity, ModbusReplyTimeout, ModbusSilentInterval, ModbusNumberOfAttempts, "
                                                           + "ScalesPortNumber, ScalesFirstAddress, ScalesLastAddress, "
                                                           + "GsmPortNumber, GsmPeriodMidnight1, GsmDayMidnight1, GsmPeriodMidnight2, "
                                                           + "GsmMidnightNumber1, GsmMidnightNumber2, GsmMidnightNumber3, GsmMidnightNumber4, GsmMidnightNumber5) "
                                                           + "VALUES (@MajorVersion, @MinorVersion, @DatabaseVersion, @BuildVersion, @Language, "
                                                           + "@ModbusProtocol, @ModbusSpeed, @ModbusParity, @ModbusReplyTimeout, @ModbusSilentInterval, @ModbusNumberOfAttempts, "
                                                           + "@ScalesPortNumber, @ScalesFirstAddress, @ScalesLastAddress, "
                                                           + "@GsmPortNumber, @GsmPeriodMidnight1, @GsmDayMidnight1, @GsmPeriodMidnight2, "
                                                           + "@GsmMidnightNumber1, @GsmMidnightNumber2, @GsmMidnightNumber3, @GsmMidnightNumber4, @GsmMidnightNumber5)")) {
                command.Parameters.Clear();
                AddVersionToCommand(command);
                command.Parameters.Add(factory.CreateParameter("@Language",               (short)setup.Language));
                command.Parameters.Add(factory.CreateParameter("@ModbusProtocol",         (short)setup.ModbusProtocol.Protocol));
                command.Parameters.Add(factory.CreateParameter("@ModbusSpeed",            (int)  setup.ModbusProtocol.Speed));
                command.Parameters.Add(factory.CreateParameter("@ModbusParity",           (short)setup.ModbusProtocol.Parity));
                command.Parameters.Add(factory.CreateParameter("@ModbusReplyTimeout",     (short)setup.ModbusProtocol.ReplyTimeout));
                command.Parameters.Add(factory.CreateParameter("@ModbusSilentInterval",   (short)setup.ModbusProtocol.SilentInterval));
                command.Parameters.Add(factory.CreateParameter("@ModbusNumberOfAttempts", (short)setup.ModbusProtocol.NumberOfAttempts));
                command.Parameters.Add(factory.CreateParameter("@ScalesPortNumber",       (short)setup.ModbusScales.PortNumber));
                command.Parameters.Add(factory.CreateParameter("@ScalesFirstAddress",     (short)setup.ModbusScales.FirstAddress));
                command.Parameters.Add(factory.CreateParameter("@ScalesLastAddress",      (short)setup.ModbusScales.LastAddress));
                command.Parameters.Add(factory.CreateParameter("@GsmPortNumber",          (short)setup.Gsm.PortNumber));
                command.Parameters.Add(factory.CreateParameter("@GsmPeriodMidnight1",     (short)setup.Gsm.PeriodMidnight1));
                command.Parameters.Add(factory.CreateParameter("@GsmDayMidnight1",        (short)setup.Gsm.DayMidnight1));
                command.Parameters.Add(factory.CreateParameter("@GsmPeriodMidnight2",     (short)setup.Gsm.PeriodMidnight2));
                for (int i = 0; i < Setup.GSM_NUMBER_MAX_COUNT; i++ ) {
                    SaveGsmNumber(command, setup, i);
                }
                command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Load setup from database
        /// </summary>
        /// <returns>Setup instance</returns>
        public Setup Load() {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName)) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.HasRows) {
                        throw new Exception("Setup doesn't exist");
                    }
                    Setup setup = new Setup();
                    
                    reader.Read();
                    
                    // Jazyk
                    if (reader["Language"] == DBNull.Value) {
                        setup.Language = SwLanguage.UNDEFINED;      // Muze byt null, tento sloupec jsem pridaval v upgradu
                    } else {
                        setup.Language = (SwLanguage)(short)reader["Language"];
                        if (setup.Language < 0 || setup.Language >= SwLanguage._COUNT) {
                            setup.Language = SwLanguage.UNDEFINED;      // Neplatna hodnota
                        }
                    }

                    // Dalsi parametry
                    setup.ModbusProtocol.Protocol         = (ModbusProtocol)(short)reader["ModbusProtocol"];
                    setup.ModbusProtocol.Speed            =                 (int)  reader["ModbusSpeed"];
                    setup.ModbusProtocol.Parity           = (ModbusParity)  (short)reader["ModbusParity"];
                    setup.ModbusProtocol.ReplyTimeout     =                 (short)reader["ModbusReplyTimeout"];
                    setup.ModbusProtocol.SilentInterval   =                 (short)reader["ModbusSilentInterval"];
                    setup.ModbusProtocol.NumberOfAttempts =                 (short)reader["ModbusNumberOfAttempts"];

                    setup.ModbusScales.PortNumber         = (short)reader["ScalesPortNumber"];
                    setup.ModbusScales.FirstAddress       = (short)reader["ScalesFirstAddress"];
                    setup.ModbusScales.LastAddress        = (short)reader["ScalesLastAddress"];
                    
                    setup.Gsm.PortNumber                  = (short)reader["GsmPortNumber"];
                    setup.Gsm.PeriodMidnight1             = (short)reader["GsmPeriodMidnight1"];
                    setup.Gsm.DayMidnight1                = (short)reader["GsmDayMidnight1"];
                    setup.Gsm.PeriodMidnight2             = (short)reader["GsmPeriodMidnight2"];
                    setup.Gsm.MidnightNumberCollection = new List<string>();
                    for (int i = 1; i <= Setup.GSM_NUMBER_MAX_COUNT; i++ ) {
                        if (reader["GsmMidnightNumber" + i.ToString()] == DBNull.Value) {
                            break;      // Koncim, prvni nezadane cislo
                        }
                        setup.Gsm.MidnightNumberCollection.Add((string)reader["GsmMidnightNumber" + i.ToString()]);
                    }

                    return setup;
                }
            }
        }

        /// <summary>
        /// Load version of SW that used the database last time
        /// </summary>
        /// <returns>SW version</returns>
        public DatabaseVersion LoadVersion() {
            using (DbCommand command = factory.CreateCommand("SELECT MajorVersion, MinorVersion, DatabaseVersion, BuildVersion FROM " + tableName)) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.HasRows) {
                        throw new Exception("Setup doesn't exist");
                    }
                    reader.Read();
                    
                    DatabaseVersion version = new DatabaseVersion();
                    version.Major    = (short)reader["MajorVersion"];
                    version.Minor    = (short)reader["MinorVersion"];
                    version.Database = (short)reader["DatabaseVersion"];
                    version.Build    = (short)reader["BuildVersion"];

                    return version;
                }
            }
        }
 
    }
}
