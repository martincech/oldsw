﻿namespace Bat1Db {
    partial class FormDemoDataMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonEmpty = new System.Windows.Forms.Button();
            this.buttonBroilerDemo = new System.Windows.Forms.Button();
            this.buttonParentsDemo = new System.Windows.Forms.Button();
            this.labelVersion = new System.Windows.Forms.Label();
            this.buttonSortingDemo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonEmpty
            // 
            this.buttonEmpty.Location = new System.Drawing.Point(12, 35);
            this.buttonEmpty.Name = "buttonEmpty";
            this.buttonEmpty.Size = new System.Drawing.Size(205, 23);
            this.buttonEmpty.TabIndex = 1;
            this.buttonEmpty.Text = "Empty database";
            this.buttonEmpty.UseVisualStyleBackColor = true;
            this.buttonEmpty.Click += new System.EventHandler(this.buttonEmpty_Click);
            // 
            // buttonBroilerDemo
            // 
            this.buttonBroilerDemo.Location = new System.Drawing.Point(12, 64);
            this.buttonBroilerDemo.Name = "buttonBroilerDemo";
            this.buttonBroilerDemo.Size = new System.Drawing.Size(205, 23);
            this.buttonBroilerDemo.TabIndex = 4;
            this.buttonBroilerDemo.Text = "Create broiler demo data";
            this.buttonBroilerDemo.UseVisualStyleBackColor = true;
            this.buttonBroilerDemo.Click += new System.EventHandler(this.buttonBroilerDemo_Click);
            // 
            // buttonParentsDemo
            // 
            this.buttonParentsDemo.Location = new System.Drawing.Point(12, 93);
            this.buttonParentsDemo.Name = "buttonParentsDemo";
            this.buttonParentsDemo.Size = new System.Drawing.Size(205, 23);
            this.buttonParentsDemo.TabIndex = 5;
            this.buttonParentsDemo.Text = "Create parents demo data";
            this.buttonParentsDemo.UseVisualStyleBackColor = true;
            this.buttonParentsDemo.Click += new System.EventHandler(this.buttonParentsDemo_Click);
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(12, 9);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(64, 13);
            this.labelVersion.TabIndex = 6;
            this.labelVersion.Text = "labelVersion";
            // 
            // buttonSortingDemo
            // 
            this.buttonSortingDemo.Location = new System.Drawing.Point(12, 122);
            this.buttonSortingDemo.Name = "buttonSortingDemo";
            this.buttonSortingDemo.Size = new System.Drawing.Size(205, 23);
            this.buttonSortingDemo.TabIndex = 7;
            this.buttonSortingDemo.Text = "Create sorting demo data";
            this.buttonSortingDemo.UseVisualStyleBackColor = true;
            this.buttonSortingDemo.Click += new System.EventHandler(this.buttonSortingDemo_Click);
            // 
            // FormDemoDataMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 157);
            this.Controls.Add(this.buttonSortingDemo);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.buttonParentsDemo);
            this.Controls.Add(this.buttonBroilerDemo);
            this.Controls.Add(this.buttonEmpty);
            this.Name = "FormDemoDataMain";
            this.Text = "BAT1 demo data";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonEmpty;
        private System.Windows.Forms.Button buttonBroilerDemo;
        private System.Windows.Forms.Button buttonParentsDemo;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Button buttonSortingDemo;
    }
}

