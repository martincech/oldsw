﻿namespace Bat1 {
    partial class UserControlWeighingResult {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlWeighingResult));
            this.textBoxUniformity = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxCv = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBoxSigma = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxAverage = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.textBoxCount = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // textBoxUniformity
            // 
            resources.ApplyResources(this.textBoxUniformity, "textBoxUniformity");
            this.textBoxUniformity.Name = "textBoxUniformity";
            this.textBoxUniformity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxManualAverage_KeyPress);
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // textBoxCv
            // 
            resources.ApplyResources(this.textBoxCv, "textBoxCv");
            this.textBoxCv.Name = "textBoxCv";
            this.textBoxCv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxManualAverage_KeyPress);
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // textBoxSigma
            // 
            resources.ApplyResources(this.textBoxSigma, "textBoxSigma");
            this.textBoxSigma.Name = "textBoxSigma";
            this.textBoxSigma.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxManualAverage_KeyPress);
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // textBoxAverage
            // 
            resources.ApplyResources(this.textBoxAverage, "textBoxAverage");
            this.textBoxAverage.Name = "textBoxAverage";
            this.textBoxAverage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxManualAverage_KeyPress);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // textBoxCount
            // 
            resources.ApplyResources(this.textBoxCount, "textBoxCount");
            this.textBoxCount.Name = "textBoxCount";
            this.textBoxCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxManualCount_KeyPress);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // UserControlWeighingResult
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBoxUniformity);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.textBoxCv);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxSigma);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxAverage);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxCount);
            this.Controls.Add(this.label5);
            this.Name = "UserControlWeighingResult";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxUniformity;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxCv;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBoxSigma;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxAverage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxCount;
        private System.Windows.Forms.Label label5;
    }
}
