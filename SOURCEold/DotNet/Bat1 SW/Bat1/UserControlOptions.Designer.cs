﻿namespace Bat1 {
    partial class UserControlOptions {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlOptions));
            this.groupBoxLanguage = new System.Windows.Forms.GroupBox();
            this.comboBoxLanguage = new System.Windows.Forms.ComboBox();
            this.groupBoxScalesVersion6 = new System.Windows.Forms.GroupBox();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.labelUnits = new System.Windows.Forms.Label();
            this.comboBoxComPort = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxCsv = new System.Windows.Forms.GroupBox();
            this.comboBoxCsvEncoding = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxCsvDecimalSeparator = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxCsvDelimiter = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxCsvTemplate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxLanguage.SuspendLayout();
            this.groupBoxScalesVersion6.SuspendLayout();
            this.groupBoxCsv.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxLanguage
            // 
            resources.ApplyResources(this.groupBoxLanguage, "groupBoxLanguage");
            this.groupBoxLanguage.Controls.Add(this.comboBoxLanguage);
            this.groupBoxLanguage.Name = "groupBoxLanguage";
            this.groupBoxLanguage.TabStop = false;
            // 
            // comboBoxLanguage
            // 
            resources.ApplyResources(this.comboBoxLanguage, "comboBoxLanguage");
            this.comboBoxLanguage.DropDownHeight = 160;
            this.comboBoxLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLanguage.FormattingEnabled = true;
            this.comboBoxLanguage.Name = "comboBoxLanguage";
            this.comboBoxLanguage.SelectionChangeCommitted += new System.EventHandler(this.comboBoxLanguage_SelectionChangeCommitted);
            // 
            // groupBoxScalesVersion6
            // 
            resources.ApplyResources(this.groupBoxScalesVersion6, "groupBoxScalesVersion6");
            this.groupBoxScalesVersion6.Controls.Add(this.comboBoxUnits);
            this.groupBoxScalesVersion6.Controls.Add(this.labelUnits);
            this.groupBoxScalesVersion6.Controls.Add(this.comboBoxComPort);
            this.groupBoxScalesVersion6.Controls.Add(this.label6);
            this.groupBoxScalesVersion6.Name = "groupBoxScalesVersion6";
            this.groupBoxScalesVersion6.TabStop = false;
            // 
            // comboBoxUnits
            // 
            resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.comboBoxUnits.SelectionChangeCommitted += new System.EventHandler(this.comboBoxComPort_SelectionChangeCommitted);
            // 
            // labelUnits
            // 
            resources.ApplyResources(this.labelUnits, "labelUnits");
            this.labelUnits.Name = "labelUnits";
            // 
            // comboBoxComPort
            // 
            resources.ApplyResources(this.comboBoxComPort, "comboBoxComPort");
            this.comboBoxComPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxComPort.FormattingEnabled = true;
            this.comboBoxComPort.Items.AddRange(new object[] {
            resources.GetString("comboBoxComPort.Items"),
            resources.GetString("comboBoxComPort.Items1"),
            resources.GetString("comboBoxComPort.Items2"),
            resources.GetString("comboBoxComPort.Items3"),
            resources.GetString("comboBoxComPort.Items4"),
            resources.GetString("comboBoxComPort.Items5"),
            resources.GetString("comboBoxComPort.Items6"),
            resources.GetString("comboBoxComPort.Items7"),
            resources.GetString("comboBoxComPort.Items8"),
            resources.GetString("comboBoxComPort.Items9"),
            resources.GetString("comboBoxComPort.Items10"),
            resources.GetString("comboBoxComPort.Items11"),
            resources.GetString("comboBoxComPort.Items12"),
            resources.GetString("comboBoxComPort.Items13"),
            resources.GetString("comboBoxComPort.Items14"),
            resources.GetString("comboBoxComPort.Items15"),
            resources.GetString("comboBoxComPort.Items16"),
            resources.GetString("comboBoxComPort.Items17"),
            resources.GetString("comboBoxComPort.Items18"),
            resources.GetString("comboBoxComPort.Items19")});
            this.comboBoxComPort.Name = "comboBoxComPort";
            this.comboBoxComPort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxComPort_SelectionChangeCommitted);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // groupBoxCsv
            // 
            resources.ApplyResources(this.groupBoxCsv, "groupBoxCsv");
            this.groupBoxCsv.Controls.Add(this.comboBoxCsvEncoding);
            this.groupBoxCsv.Controls.Add(this.label4);
            this.groupBoxCsv.Controls.Add(this.comboBoxCsvDecimalSeparator);
            this.groupBoxCsv.Controls.Add(this.label3);
            this.groupBoxCsv.Controls.Add(this.comboBoxCsvDelimiter);
            this.groupBoxCsv.Controls.Add(this.label2);
            this.groupBoxCsv.Controls.Add(this.comboBoxCsvTemplate);
            this.groupBoxCsv.Controls.Add(this.label1);
            this.groupBoxCsv.Name = "groupBoxCsv";
            this.groupBoxCsv.TabStop = false;
            // 
            // comboBoxCsvEncoding
            // 
            resources.ApplyResources(this.comboBoxCsvEncoding, "comboBoxCsvEncoding");
            this.comboBoxCsvEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCsvEncoding.FormattingEnabled = true;
            this.comboBoxCsvEncoding.Items.AddRange(new object[] {
            resources.GetString("comboBoxCsvEncoding.Items"),
            resources.GetString("comboBoxCsvEncoding.Items1"),
            resources.GetString("comboBoxCsvEncoding.Items2")});
            this.comboBoxCsvEncoding.Name = "comboBoxCsvEncoding";
            this.comboBoxCsvEncoding.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCsvDelimiter_SelectionChangeCommitted);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // comboBoxCsvDecimalSeparator
            // 
            resources.ApplyResources(this.comboBoxCsvDecimalSeparator, "comboBoxCsvDecimalSeparator");
            this.comboBoxCsvDecimalSeparator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCsvDecimalSeparator.FormattingEnabled = true;
            this.comboBoxCsvDecimalSeparator.Items.AddRange(new object[] {
            resources.GetString("comboBoxCsvDecimalSeparator.Items"),
            resources.GetString("comboBoxCsvDecimalSeparator.Items1"),
            resources.GetString("comboBoxCsvDecimalSeparator.Items2")});
            this.comboBoxCsvDecimalSeparator.Name = "comboBoxCsvDecimalSeparator";
            this.comboBoxCsvDecimalSeparator.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCsvDelimiter_SelectionChangeCommitted);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // comboBoxCsvDelimiter
            // 
            resources.ApplyResources(this.comboBoxCsvDelimiter, "comboBoxCsvDelimiter");
            this.comboBoxCsvDelimiter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCsvDelimiter.FormattingEnabled = true;
            this.comboBoxCsvDelimiter.Items.AddRange(new object[] {
            resources.GetString("comboBoxCsvDelimiter.Items"),
            resources.GetString("comboBoxCsvDelimiter.Items1"),
            resources.GetString("comboBoxCsvDelimiter.Items2")});
            this.comboBoxCsvDelimiter.Name = "comboBoxCsvDelimiter";
            this.comboBoxCsvDelimiter.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCsvDelimiter_SelectionChangeCommitted);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // comboBoxCsvTemplate
            // 
            resources.ApplyResources(this.comboBoxCsvTemplate, "comboBoxCsvTemplate");
            this.comboBoxCsvTemplate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCsvTemplate.FormattingEnabled = true;
            this.comboBoxCsvTemplate.Items.AddRange(new object[] {
            resources.GetString("comboBoxCsvTemplate.Items"),
            resources.GetString("comboBoxCsvTemplate.Items1"),
            resources.GetString("comboBoxCsvTemplate.Items2")});
            this.comboBoxCsvTemplate.Name = "comboBoxCsvTemplate";
            this.comboBoxCsvTemplate.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCsvTemplate_SelectionChangeCommitted);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // UserControlOptions
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxCsv);
            this.Controls.Add(this.groupBoxScalesVersion6);
            this.Controls.Add(this.groupBoxLanguage);
            this.Name = "UserControlOptions";
            this.groupBoxLanguage.ResumeLayout(false);
            this.groupBoxScalesVersion6.ResumeLayout(false);
            this.groupBoxScalesVersion6.PerformLayout();
            this.groupBoxCsv.ResumeLayout(false);
            this.groupBoxCsv.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxLanguage;
        private System.Windows.Forms.ComboBox comboBoxLanguage;
        private System.Windows.Forms.GroupBox groupBoxScalesVersion6;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.Label labelUnits;
        private System.Windows.Forms.ComboBox comboBoxComPort;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBoxCsv;
        private System.Windows.Forms.ComboBox comboBoxCsvEncoding;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxCsvDecimalSeparator;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxCsvDelimiter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxCsvTemplate;
        private System.Windows.Forms.Label label1;
    }
}
