﻿using System;
using System.Collections.Generic;
using System.Text;

/*

 Verze 7.0.2.0:
   - 10.1.2009: Podpora vahy verze 7.0.1 s nastavenim vazivosti 30 / 50kg. SW funguje i s vahami verze 7.0.0,
                v configu se nedelala zadna zmena.

 Verze 7.0.2.1:
   - 30.1.2009: Finalni grafika od Ivose.
  
 Verze 7.0.2.2:
   - 31.1.2009: Vsechny texty "sample" zmeneny na "weight", stejne jako ve vaze,
                "Theoretical growth curve" zmeneno na "Standard growth curve".

 Verze 7.1.2.0:
   - 5.2.2009: Prechod na novou Bat1.dll odpovidajici vaze 7.01.0 s podporou CrashData a 199 souboru.
               Program neni zpetne kompatibilni s vahami verze 7.00.x (ty maji jen 64 souboru a jsou binarne nekompatiblini).
   - 5.2.2009: Prelozeno do cestiny.
 
 Verze 7.1.2.1:
   - 10.2.2009: U vsech projektu zmenen Target na x86. DLL od SQLite je 32bitova a pokud necham Target na Any CPU, tak pri
                spusteni na 64bit Viste to pri pokusu o nahrani DLL hodi vyjimku. Program musi byt komplet 32bit, pak beha
                i na 64bit Viste ve 32 bitovem modu. Viz. http://sqlite.phxsoftware.com/forums/t/1564.aspx
   - 10.2.2009: Prelozeno do portugalstiny.
   - 11.2.2009: Prelozeno do spanelstiny.

 Verze 7.1.2.2:
   - 4.4.2009: Prelozeno do nemciny.
 
 Verze 7.1.2.3:
   - 16.4.2009: Prelozeno do francouzstiny.
   - 17.4.2009: Do zalozky Diagnostics pridano zobrazeni umisteni souboru databaze a moznost zkopirovat databazi bez VACUUM.

 Verze 7.1.2.4:
   - 2.6.2009: Prelozeno do finstiny.
   - 2.6.2009: Prelozeno do rustiny.
   - 2.6.2009: Upgrade na SQLite 1.0.61

 Verze 7.1.2.5:
   - 13.6.2009: V exportu vazeni do CSV se rychlost vazeni chybne zaokrouhlovala dolu, takze se
                lisila od zobrazeni v tabulce (tam se zaokrouhluje spravne aritmeticky).
   - 13.6.2009: V exportu do CSV se datum a cas exportoval spatne. Vzdy se prevedl na 12hod format
                a v Excelu uz s tim neslo nic delat. Nyni se exportuje v 24hod formatu, v Excelu
                si to muze prevest na libovolny format. Oprava v Library/Csv.cs.
   - 14.6.2009: Pokud mel soubor CSV otevreny v Excelu a zkusil dalsi export do tohoto souboru,
                hodilo to vyjimku. Osetreno v ExportCsv.cs, pujde o beznou situaci.
   - 14.6.2009: Tabulku Statistics a Flocks lze nyni exportovat do CSV, nove tlacitko Export table.
   - 14.6.2009: U ikony Weighings v FormMain opraven ToolTipText v rustine (predtim chybel).
 
 Verze 7.1.2.6:
   - 17.6.2009: Celou dobu se pocitalo CV a UNI spatne, hodnoty nesedely s vahou.
   - 17.6.2009: Porad byly problemy se zaokrohlovanim: nakonec jsem to udelal tak, ze Average, Sigma a Target se rovnou pri 
                vypoctu zaokrouhli (oseknou) na gramy, tj. stejne jako je vypocet ve vaze. Prumer 4.235698 se tak zaokrouhli
                na 4.23500002 a s touto hodnotou pak program pracuje. Pred kazdym zobrazenim je treba hodnotu zaokrouhlit
                normalne aritmeticky (dosud se sachovalo ruzne s CutWeight a RoundWeight, byl v tom bordel).
   - 17.6.2009: Target ze Standard growth curve se pocital s chybou, upraveno zaokrouhleni.
   - 17.6.2009: V tabulce statistiky hejn pribyl sloupec Difference, kde je rozdil Average - Target.

 Verze 7.1.2.7:
   - 6.8.2009: Upgrade na SQLite 1.0.65
   - 11.8.2009: V panelu Weighings je pri editaci vazeni nyni mozne zmenit jednotky hmotnosti. Po zmene se prepocitaji vsechny
                vazeni, ktere sdili stejny config vahy. Melo by se asi zobrazit upozorneni, ze se prepocte vice vazeni, ale
                zatim o tom mlcim.

 Verze 7.1.2.8:
   - 19.8.2009: Prelozeno do turectiny.
   - 19.8.2009: Opraven jeden vyraz ve finstine.
   - 19.8.2009: Pridano vice barev v grafu hejn, napr. Rembrandt pouziva bezne 20 hejn naraz.
  
 Verze 7.1.2.9:
   - 7.9.2009: Prelozeno do japonstiny.

 Verze 7.2.2.0:
   - 27.10.2009: Kompatibilni s vahami verze 7.02.0, kde pribyla madarstina. Ta ve starsich verzich vah zcela chybela.
   - 27.10.2009: Nova verze ZedGraph 5.1.5.
   - 30.9.2009: Jazyk ve ScaleConfig i v DB zmenen na univerzalni kody jazyku, ktere jsou kompatibilni se starsimi
                vahami a starsimi verzemi DB bez nutnosti prepoctu. Ve vaze se bohuzel nove jazyky vsouvaji doprostred
                enumu, tj. cely enum se meni podle verze, pouziti 1:1 tak neni mozne. Nova trida v ScaleLanguage.cs.
   - 2.11.2009: Pri startu aplikace se kontroluje, zda databaze pochazi z novejsi verze SW. Pokud ano, program zobrazi
                chybu a nespusti se (uzivatel musi upgradovat svuj SW). Pokud by starsi SW pouzil DB z novejsiho SW,
                v DB by mohly byt nepodporovane hodnoty (jazyky vahy) nebo by DB mohla mit jinou strukturu. Stejna
                kontrola se provadi i pri obnove ze zalohy, importu dat a importu configu vahy. Text chybove hlasky
                prelozen do vsech jazyku krome japonstiny.
   - 2.11.2009: V okne vyberu jazyka po instalaci jsem vymenil combobox za listbox, aby byly vsechny jazyky hned videt.
  
 Verze 7.2.2.1:
   - 9.1.2010: Adaptace na FlexScale od Big Dutchman, zaveden app.config, zmeny v Resources, instalacni projekt rozdelen
               na Veit a Big Dutchman. Volba verze se dela pomoci extra bitmapy splash screen, bitmapy loga a app.config,
               vse se voli az v instalatoru. Soubor EXE tak je jen jeden.
   - 11.1.2010: Dwain: uzivatelum se stavalo, ze nacetli data z vahy, ale zapomneli je ulozit do DB. Pak smazali data z
                vahy a prili tak omylem o vazeni. Nyni delam kontrolu ulozeni, pokud po nacteni dat pomoci Read data vazeni
                neulozi, tak po opetovnem stisku Read data, Delete data nebo pri ukonceni programu se zobrazi upozorneni.
                Novy modul SaveStatus.cs.
   - 11.1.2010: Nova Bat1Dll.dll, ktera podporuje take FlexScale pro Big Dutchman + nastavuje default jmeno vahy "SCALE 1".
   - 13.1.2010: Prechod na podepsany ovladac FTDI verze 2.06.00. Program je kompatibilni se starsimi vahami a take stary SW
                je kompatibilni s novymi vahami (nastesti se s vahou komunikuje pomoci jmena, ne pomoci PID+VID, takze na
                verzi ovladace ani PID nezalezi). Zkousel jsem na cistych instalacich W2000, XP, Vista 32bit,
                Windows 7 32bit RC a Windows 7 64bit a vsude funguje.

 Verze 7.3.2.0:
   - 9.6.2010: Kompatibilni s vahami verze 7.03.0, kde pribyla polstina.
   - 11.6.2010: Pokud mel starsi SW a cetl data z novejsi vahy s vyssim minor cislem verze (napr. SW verze 7.2.2.0 a vaha
                verze 7.3.0), cteni dat skoncilo vyjimkou, misto toho aby se zobrazila zprava, ze vaha neni podporovana.
                Osetreno ve fci ScaleLanguage.ConvertToDatabase().

 Verze 7.3.3.0:
   - 11.4.2011: Vyber souboru v definici hejna nyni probiha z listboxu a je mozne vybrat vice souboru naraz.
   - 11.4.2011: V definici hejna lze soubor smazat klavesou Del.
   - 12.4.2011: Barvy v grafu rustove krivky rozsirene na prakticky nekonecno. Je definovanych 12 zakladnich barev a pokud
                nestaci, automaticky se vytvori dalsi podle aktualniho poctu krivek v grafu pomoci zesvetleni a ubrani saturace.
                Stejne to dela Excel 2010.
   - 12.4.2011: V definici hejn lze obe tabulky aktualnich i uzavrenych hejn tridit podle jmena a podle data zahajeni hejna.
                Default trideni je podle jmena, tj. jako doposud.
   - 12.4.2011: SQLite verze 1.0.66.0.
   - 13.4.2011: Vybrana hejna se nyni ukladaji do databaze a pamatuji se tak i po zavreni programu.
   - 13.4.2011: Nova verze Bat1Dll.dll s upravenym Uartem. Nema vliv na funkcnost, ani to driv neblblo.
   - 14.4.2011: V okne pro vyber sablony pri zalozeni noveho hejna je nyni pouzita tabulka misto pouheho listboxu se jmeny hejn.
   - 14.4.2011: V okne pro vyber hejna v Selected flocks je taky pouzita tabulka misto pouhych listboxu se jmeny hejn.
  
 Verze 7.3.3.1:
   - 11.7.2011: Pokud bylo v DB vytvorene prave 1 hejno, spustil znovu program a prepnul se do hejn, tak program
                hodil vyjimku. Chyba v ControlSelection.SelectRow().
   - 11.7.2011: V kodu jsou casti pro casove omezeni Selected flocks, ale kod jeste neni hotovy a casove
                omezeni je zatim zakazane.
 
 Verze 7.3.3.2:
   - 19.7.2011: V Selected flocks je dodatecny casovy filtr, kterym lze omezit zobrazena hejna. Zadany filtr se neuklada do DB.
   - 19.7.2011: V tabulce Current flocks a Selected flocks pribyly sloupce Above a Below, kde se zobrazuje pocet kusu nad a pod
                cilovou hmotnosti. Stejne tak je pocet nad a pod ve vsech exportech a vytiscich hejn.
   - 19.7.2011: Pri editaci configu vahy se v nazvu okna v zavorce zobrazuje jmeno prave otevreneho configu.
   - 20.7.2011: Pri editaci configu vahy se program pta, zda chce uzivatel ulozit zmeny v prave otevrenem configu.
   - 20.7.2011: Pri odeslani mailu primo z programu (ikona Maintenance - About) se v predmetu mailu posle i cislo verze SW
 
 Verze 7.3.3.3:
   - 8.4.2012: Japonstina zmenena z Katakany na Kanji.
 
 Verze 7.3.3.4:
   - 7.1.2013: Pokud bylo soucasti hejna vazeni s rucne zadanymi vysledky, tak pro zobreazeni tohoto hejna (at uz bylo Current
               nebo Selected) se zobrazila vyjimka. Chyba ve fci UserControlStatisticsTable.CalculateFlockData().

 Verze 7.4.3.0:
   - 19.6.2013: Pridana italstina do SW.
   - 2.7.2013: Podpora vahy 7.04.0, kde pribyla italstina




 */

namespace Bat1 {
    
    /// <summary>
    /// Scale version that the SW supports. Data from higher scale versions can be read, but the config cannot be changed.
    /// </summary>
    public static class ScaleVersion {
        /// <summary>
        /// Major version number
        /// </summary>
        public const int MAJOR = 7;
        
        /// <summary>
        /// Minor version number, changed always when config changes (including new languages)
        /// </summary>
        public const int MINOR = 4;
    }

    
    /// <summary>
    /// Current SW version
    /// </summary>
    public static class SwVersion {
        /// <summary>
        /// Major version number, corresponding to scale major version
        /// </summary>
        public const int MAJOR = ScaleVersion.MAJOR;
        
        /// <summary>
        /// Minor version number, corresponding to scale minor version
        /// </summary>
        public const int MINOR = ScaleVersion.MINOR;

        /// <summary>
        /// Database version number, changed when the database structure changes
        /// </summary>
        public const int DATABASE = 3;

        /// <summary>
        /// Build number
        /// </summary>
        public const int BUILD = 0;

        /// <summary>
        /// Get version string, for example "7.0.0.0"
        /// </summary>
        /// <returns>Version string</returns>
        public static new string ToString() {
            return MAJOR.ToString() + "." + MINOR.ToString() + "." + DATABASE.ToString() + "." + BUILD.ToString();
        }
    }


}
