﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlEditCurves : UserControl {
        private bool isLoading;
        
        private CurveList curveList;

        public UserControlEditCurves() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            isLoading = true;

            try {
                // Nahraju z databaze naposledy pouzite jednotky
                LoadUnits();
                RedrawUnits();

                // Nahraju krivky z databaze
                curveList = Program.Database.LoadCurveList();
                
                // Zobrazim seznam krivek
                RedrawCurveList();

                // Najedu na prvni krivku a zobrazim jeji obsah
                SelectFirstCurve();

                listBoxCurves.Focus();
            } finally {
                isLoading = false;
            }
        }

        private void RedrawCurveList() {
            listBoxCurves.Items.Clear();
            foreach (Curve curve in curveList.List) {
                listBoxCurves.Items.Add(curve.Name);
            }
        }
        
        private void RedrawCurve(int index) {
            if (index < 0) {
                userControlCurve.Clear();
                textBoxNote.Text           = "";
                groupBoxDefinition.Text    = "";
                groupBoxDefinition.Enabled = false;
                return;
            }
            
            Curve curve = curveList.List[index];

            // Nazev group boxu nastavim na jmeno lrivky
            groupBoxDefinition.Enabled = true;
            groupBoxDefinition.Text    = curve.Name;

            // Jednotky
            comboBoxUnits.SelectedIndex = (int)curve.Units;
            RedrawUnits();

            // Nahraju krivku do tabulky
            userControlCurve.LoadCurve(curve);

            // Poznamka
            textBoxNote.Text = curve.Note;
        }

        private void SelectFirstCurve() {
            if (listBoxCurves.Items.Count > 0) {
                listBoxCurves.SelectedIndex = 0;       // Nejaka krivka v seznamu je, zobrazim prvni (jinak zustane index -1)
            }
            RedrawCurve(listBoxCurves.SelectedIndex);
        }

        /// <summary>
        /// Load last used units from database
        /// </summary>
        private void LoadUnits() {
            comboBoxUnits.SelectedIndex = (int)Program.Setup.CurveUnits;
        }

        /// <summary>
        /// Save selected units to database
        /// </summary>
        private void SaveUnits() {
            Program.Setup.CurveUnits = (Units)comboBoxUnits.SelectedIndex;
            Program.Database.SaveSetup();
        }

        private void RedrawUnits() {
            userControlCurve.RedrawUnits((Units)comboBoxUnits.SelectedIndex);
        }

        private bool CanSave(string curveName) {
            // Kontrola jmena
            if (!curveList.CheckName(curveName)) {
                MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
                return false;
            }

            // Zkoktroluju, zda uz krivka neexistuje
            if (curveList.Exists(curveName)) {
                MessageBox.Show(String.Format(Properties.Resources.CURVE_EXISTS, curveName), Program.ApplicationName);
                return false;
            }

            return true;
        }

        private void SelectCurve(int index) {
            // Vyberu krivku v seznamu
            listBoxCurves.SelectedIndex = index;
            listBoxCurves.Focus();

            // Zobrazim obsah krivky do tabulky
            RedrawCurve(listBoxCurves.SelectedIndex);
        }

        private void SelectCurve(string name) {
            SelectCurve(curveList.GetIndex(name));
        }

        private bool EnterName(string windowTitle, string oldName, out string newName) {
            FormName form = new FormName(windowTitle, oldName, false);
            if (form.ShowDialog() != DialogResult.OK) {
                newName = null;
                return false;
            }
            
            // Zkopiruju jmeno, maximalni delka je NAME_MAX_LENGTH definovane v DatabaseTableCurves
            if (form.EnteredName.Length <= DatabaseTableCurves.NAME_MAX_LENGTH) {
                newName = form.EnteredName;
            } else {
                newName = form.EnteredName.Remove(DatabaseTableCurves.NAME_MAX_LENGTH);
            }

            return true;
        }

        private bool SaveCurve(int index) {
            // Nactu vybranou krivku
            if (index >= curveList.List.Count) {
                return false;
            }
            Curve curve = curveList.List[index];

            // Nastavim vybrane jednotky
            curve.Units = (Units)comboBoxUnits.SelectedIndex;

            // Zkopiruju zadane udaje z tabulky do krivky
            if (!userControlCurve.Save(ref curve)) {
                MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
                return false;
            }

            // Poznamka
            curve.Note = textBoxNote.Text;

            return true;
        }

        private void comboBoxUnits_SelectionChangeCommitted(object sender, EventArgs e) {
            // Vybrane jednotky pouze prekreslim, ulozim je do databaze az po ulozeni krivky
            RedrawUnits();
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            // Zeptam se na jmeno
            string newName;
            if (!EnterName(buttonNew.Text, "", out newName)) {
                return;
            }

            // Kontrola jmena
            if (!CanSave(newName)) {
                return;
            }

            // Zalozim novou krivku, beru naposledy pouzite jednotky
            Curve curve = new Curve(newName, Program.Setup.CurveUnits);
            if (!curveList.Add(curve)) {
                return;             // Nemelo by selhat, jmeno jsem uz kontroloval drive
            }

            // Pridam novou krivku do databaze
            Program.Database.SaveCurve(ref curve);
            
            // Obnovim zobrazeni seznamu krivek
            RedrawCurveList();

            // Vyberu prave zalozenou krivku v seznamu
            SelectCurve(newName);
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            // Smazu vybranou krivku
            int index = listBoxCurves.SelectedIndex;
            if (index < 0) {
                return;         // Zadna krivka neni vybrana
            }

            Curve curve = curveList.List[index];

            // Zkontroluju, zda neni tato krivka pouzita v nejakem hejne a zeptam se, zda chce smazat
            int flockCount = Program.Database.FlocksWithCurve(curve.Id);
            string text;
            if (flockCount == 0) {
                text = String.Format(Properties.Resources.CURVE_DELETE, curve.Name);
            } else {
                text = String.Format(Properties.Resources.CURVE_USED_IN_FLOCKS, curve.Name, flockCount);
            }
            if (MessageBox.Show(text, Program.ApplicationName, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;         // Nechce smazat
            }
            
            // Smazu stavajici krivku z databaze - driv nez ji smazu ze seznamu
            // Pokud je krivka pouzita v nejakych hejnech, zaroven ji smazu ze vsech techto hejn
            Program.Database.DeleteCurve(curve.Id);

            // Smazu vybranou krivku
            curveList.Delete(index);

            // Obnovim zobrazeni seznamu krivek
            RedrawCurveList();

            // Vyberu prvni krivku v seznamu
            SelectFirstCurve();
            listBoxCurves.Focus();
        }

        private void buttonRename_Click(object sender, EventArgs e) {
            // Prejmenuju vybranou krivku
            int index = listBoxCurves.SelectedIndex;
            if (index < 0) {
                return;         // Zadna krivka neni vybrana
            }

            string oldName = curveList.List[index].Name;

            // Zeptam se na nove jmeno
            string newName;
            if (!EnterName(buttonRename.Text, oldName, out newName)) {
                return;
            }

            if (newName == oldName) {
                return;         // Jmeno nezmenil
            }

            // Kontrola jmena
            if (!CanSave(newName)) {
                return;
            }

            // Nastavim nove jmeno
            if (!curveList.Rename(index, newName)) {
                return;
            }

            // Updatuju krivku v databazi
            Program.Database.UpdateCurve(curveList.List[index]);

            // Obnovim zobrazeni seznamu krivek
            RedrawCurveList();

            // Vyberu prave zalozenou krivku v seznamu
            SelectCurve(newName);
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            // Ulozi obsah vybrane krivky
            int index = listBoxCurves.SelectedIndex;
            if (index < 0) {
                return;         // Zadna krivka neni vybrana
            }

            // Ulozim z tabulky do krivky
            if (!SaveCurve(index)) {
                return;
            }

            // Updatuju krivku v databazi
            Program.Database.UpdateCurve(curveList.List[index]);

            // Zobrazim zformatovane hodnoty
            RedrawCurve(index);

            // Ulozim si naposledy pouzite jednotky
            SaveUnits();
        }

        private void listBoxCurves_SelectedIndexChanged(object sender, EventArgs e) {
            if (isLoading) {
                return;
            }
            
            // Zobrazim obsah vybrane krivky
            if (listBoxCurves.SelectedIndex < 0) {
                return;             // Zadna krivka neni vybrana
            }

            // Zobrazim obsah krivky do tabulky
            RedrawCurve(listBoxCurves.SelectedIndex);
        }

        private void buttonCopy_Click(object sender, EventArgs e) {
            // Zkopiruje obsah prave vybrane krivky do nove krivky
            int index = listBoxCurves.SelectedIndex;
            if (index < 0) {
                return;             // Zadna krivka neni vybrana
            }

            Curve selectedCurve = curveList.List[index];

            // Zeptam se na jmeno, default nabidnu stavajici jmeno, aby ho mohl pouze upravit
            string newName;
            if (!EnterName(buttonCopy.Text, selectedCurve.Name, out newName)) {
                return;
            }

            // Pokud zadal stejne jmeno, neni treba nic kopirovat
            if (newName == selectedCurve.Name) {
                return;
            }

            // Kontrola jmena
            if (!CanSave(newName)) {
                return;
            }

            // Vytvorim novou krivku zkopirovanim z puvodni
            Curve curve = new Curve(selectedCurve);
            curve.Name = newName;       // Prejmenuju
            if (!curveList.Add(curve)) {
                return;             // Nemelo by selhat, jmeno jsem uz kontroloval drive
            }

            // Pridam novou krivku do databaze
            Program.Database.SaveCurve(ref curve);

            // Obnovim zobrazeni seznamu krivek
            RedrawCurveList();

            // Vyberu prave zalozenou krivku v seznamu, rovnou ji tim i edituju
            SelectCurve(newName);
        }

        private void UserControlEditCurves_Load(object sender, EventArgs e) {
            listBoxCurves.Focus();
        }

    }
}
