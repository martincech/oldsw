﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    /// <summary>
    /// UserControlScaleConfig base class
    /// </summary>
    public class UserControlScaleConfigBase : UserControl {
        /// <summary>
        /// Scale config to edit
        /// </summary>
        protected ScaleConfig scaleConfig;

        /// <summary>
        /// Read-only mode
        /// </summary>
        protected bool readOnly;

        /// <summary>
        /// Values are being loaded into the controls
        /// </summary>
        protected bool isLoading = false;

        /// <summary>
        /// Set new config
        /// </summary>
        /// <param name="scaleConfig">Scale config to edit</param>
        public virtual void SetScaleConfig(ScaleConfig scaleConfig) {
            // Preberu config
            this.scaleConfig = scaleConfig;

            // Prekreslim
            Redraw();
        }

        /// <summary>
        /// Redraw control
        /// </summary>
        public virtual void Redraw() {
        }

    }
}
