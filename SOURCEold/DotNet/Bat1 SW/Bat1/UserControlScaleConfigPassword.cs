﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlScaleConfigPassword : UserControlScaleConfigBase {
        /// <summary>
        /// Array of displayed keys
        /// </summary>
        private PictureBox [] pictureBoxKeyArray;

        /// <summary>
        /// Edited password
        /// </summary>
        private byte [] password;
        
        public UserControlScaleConfigPassword(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Vytvorim pole obrazku s tlacitky
            pictureBoxKeyArray = new PictureBox[4] {pictureBoxKey1, pictureBoxKey2, pictureBoxKey3, pictureBoxKey4};

            // Read-only
            this.readOnly = readOnly;

            // Preberu config a prekreslim
            SetScaleConfig(scaleConfig);
        }

        private void EnableControls() {
            buttonChange.Enabled = radioButtonEnable.Checked;
        }

        private void RedrawPassword() {
            for (int keyIndex = 0; keyIndex < password.Length; keyIndex++) {
                pictureBoxKeyArray[keyIndex].Image = imageListKey.Images[password[keyIndex]];
            }
        }
        
        public override void Redraw() {
            isLoading = true;

            try {
                if (scaleConfig.PasswordConfig.Enable) {
                    radioButtonEnable.Checked = true;
                } else {
                    radioButtonDisable.Checked = true;
                }

                // Preberu heslo
                password = new byte[4];
                scaleConfig.PasswordConfig.Password.CopyTo(password, 0);

                // Kombinace klaves
                RedrawPassword();

                // Povoleni zobrazeni na zaklade nastaveni
                EnableControls();
            } finally {
                isLoading = false;
            }
        }

        private bool IsPasswordValid() {
            foreach (byte key in password) {
                if (key == (byte)Veit.Bat1.Keys.K_NULL) {
                    return false;       // Zadna klavesa nesmi byt K_NULL
                }
            }
            return true;
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }

            // Povoleni hesla
            if (IsPasswordValid()) {
                scaleConfig.PasswordConfig.Enable = radioButtonEnable.Checked;
            } else {
                // Heslo neni zadane
                scaleConfig.PasswordConfig.Enable = false;
            }
            
            // Heslo samotne
            if (scaleConfig.PasswordConfig.Enable) {
                // Zkoupiruju zadane heslo
                password.CopyTo(scaleConfig.PasswordConfig.Password, 0);
            } else {
                // Nastavim prazdne heslo
                scaleConfig.PasswordConfig.Password = new byte[4] {(byte)Veit.Bat1.Keys.K_NULL, (byte)Veit.Bat1.Keys.K_NULL, (byte)Veit.Bat1.Keys.K_NULL, (byte)Veit.Bat1.Keys.K_NULL};
            }
        }

        private void radioButtonEnable_CheckedChanged(object sender, EventArgs e) {
            EnableControls();
            ControlsToConfig();
        }

        private void buttonChange_Click(object sender, EventArgs e) {
            FormScalePassword form = new FormScalePassword(imageListKey);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Preberu nove heslo a prekreslim
            form.Password.CopyTo(password, 0);
            RedrawPassword();
            ControlsToConfig();
        }
    }
}
