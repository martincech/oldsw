﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormScaleWait : Form {
        Form parentForm;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public FormScaleWait(Form parentForm) {
            InitializeComponent();

            this.parentForm = parentForm;
        }

        private void FormScaleWait_Shown(object sender, EventArgs e) {
            parentForm.Enabled = false;
        }

        private void FormScaleProgress_FormClosing(object sender, FormClosingEventArgs e) {
            parentForm.Enabled = true;
        }
    }
}
