﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {

    /// <summary>
    /// Info about one weighing for searching
    /// </summary>
    public struct WeighingSearchInfo {
        /// <summary>
        /// Weighing Id in the database
        /// </summary>
        public long id;

        /// <summary>
        /// Timestamp of the first sample
        /// </summary>
        public DateTime minDateTime;

        /// <summary>
        /// Timestamp of the last sample
        /// </summary>
        public DateTime maxDateTime;

        /// <summary>
        /// File name
        /// </summary>
        public string fileName;

        /// <summary>
        /// Scale name
        /// </summary>
        public string scaleName;

        /// <summary>
        /// Weighing note
        /// </summary>
        public string note;
    }

    public class WeighingSearchInfoList {
        /// <summary>
        /// List of weighing info
        /// </summary>
        public  List<WeighingSearchInfo> WeighingInfoList { get { return weighingInfoList; } }
        private List<WeighingSearchInfo> weighingInfoList;

        /// <summary>
        /// Constructor
        /// </summary>
        public WeighingSearchInfoList() {
            weighingInfoList = new List<WeighingSearchInfo>();
        }

        /// <summary>
        /// Compare function for List(WeighingSearchInfo).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        private int CompareByTime(WeighingSearchInfo x, WeighingSearchInfo y) {
            if (x.minDateTime < y.minDateTime) {
                return -1;
            }
            if (x.minDateTime > y.minDateTime) {
                return 1;
            }
            
            // Datumy jsou shodne, porovnam soubory
            int result = x.fileName.CompareTo(y.fileName);
            if (result < 0) {
                return -1;
            }
            if (result > 0) {
                return 1;
            }
            
            // Soubory jsou taky shodne, porovnam jeste vahy
            result = x.scaleName.CompareTo(y.scaleName);
            if (result < 0) {
                return -1;
            }
            if (result > 0) {
                return 1;
            }

            // Vse je shodne
            return 0;
        }

        /// <summary>
        /// Add weighing info into the list
        /// </summary>
        /// <param name="weighingInfo">Weighing info to add</param>
        public void Add(WeighingSearchInfo weighingInfo) {
            if (weighingInfoList.Exists(delegate(WeighingSearchInfo i) { return i.id == weighingInfo.id; })) {
                // Uz v seznamu je
                return;
            }
            // Pridam a setridim seznam
            weighingInfoList.Add(weighingInfo);
        }

        /// <summary>
        /// Sort the list by time
        /// </summary>
        public void Sort() {
            weighingInfoList.Sort(CompareByTime);
        }

        /// <summary>
        /// Delete item at index
        /// </summary>
        /// <param name="index">Index</param>
        public void Delete(int index) {
            weighingInfoList.RemoveAt(index);
        }

        /// <summary>
        /// Delete all items
        /// </summary>
        public void Clear() {
            weighingInfoList.Clear();
        }
    }
    
    /// <summary>
    /// Filter during weighing selection
    /// </summary>
    public class WeighingSearchInfoFilter {
        /// <summary>
        /// Complete sorted list of weighing info
        /// </summary>
        private List<WeighingSearchInfo> weighingInfoList;

        /// <summary>
        /// List of weighing info that fit the filter
        /// </summary>
        public  List<WeighingSearchInfo> FilteredInfoList { get { return filteredInfoList; } }
        private List<WeighingSearchInfo> filteredInfoList = new List<WeighingSearchInfo>();

        /// <summary>
        /// List of all files
        /// </summary>
        public  List<string> FilesList { get { return filesList; } }
        private List<string> filesList = new List<string>();

        /// <summary>
        /// List of all scales
        /// </summary>
        public  List<string> ScalesList { get { return scalesList; } }
        private List<string> scalesList = new List<string>();

        /// <summary>
        /// File name for filtration (or NULL for all files)
        /// </summary>
        public  string FileFilter { get { return fileFilter; } set { fileFilter = value; } }
        private string fileFilter = null;

        /// <summary>
        /// Scale name for filtration (or NULL for all scales)
        /// </summary>
        public  string ScaleFilter { get { return scaleFilter; } set { scaleFilter = value; } }
        private string scaleFilter = null;

        /// <summary>
        /// Minimum date of weighing for filtration (or DateTime.MinValue when not limited)
        /// </summary>
        public  DateTime MinDateFilter { get { return minDateFilter.Date; } set { minDateFilter = value.Date; } }
        private DateTime minDateFilter = DateTime.MinValue.Date;

        /// <summary>
        /// Maximum date of weighing for filtration (or DateTime.MaxValue when not limited)
        /// </summary>
        public  DateTime MaxDateFilter { get { return maxDateFilter.Date; } set { maxDateFilter = value.Date; } }
        private DateTime maxDateFilter = DateTime.MaxValue.Date;

        /// <summary>
        /// Constructor
        /// </summary>
        public WeighingSearchInfoFilter() {
        }

        /// <summary>
        /// Load weighing info
        /// </summary>
        /// <param name="weighingInfoList">Complete sorted list of weighing info</param>
        public void Load(List<WeighingSearchInfo> weighingInfoList) {
            this.weighingInfoList = weighingInfoList;
            
            // Default je filtr vypnuty, zverejnim vsechny polozky
            filteredInfoList.AddRange(weighingInfoList);

            // Sestavim seznamy souboru a vah
            filesList.Clear();
            scalesList.Clear();
            foreach (WeighingSearchInfo weighingInfo in weighingInfoList) {
                if (!filesList.Exists(delegate(string s) { return s == weighingInfo.fileName; })) {
                    filesList.Add(weighingInfo.fileName);
                }
                if (!scalesList.Exists(delegate(string s) { return s == weighingInfo.scaleName; })) {
                    scalesList.Add(weighingInfo.scaleName);
                }
            }

            // Na zaver seznamy setridim podle abecedy
            filesList.Sort();
            scalesList.Sort();
        }

        /// <summary>
        /// Execute the filter and fill FilteredInfoList
        /// </summary>
        public void Execute() {
            filteredInfoList.Clear();
            foreach (WeighingSearchInfo weighingInfo in weighingInfoList) {
                if (MinDateFilter != DateTime.MinValue.Date && weighingInfo.maxDateTime.Date < MinDateFilter) {
                    // Datum posledniho vzorku je mensi nez minimalni datum ve filtru
                    continue;
                }
                if (MaxDateFilter != DateTime.MaxValue.Date && weighingInfo.minDateTime.Date > MaxDateFilter) {
                    // Datum prvniho vzorku je vetsi nez maximalni datum ve filtru
                    continue;
                }
                if (FileFilter != null && weighingInfo.fileName != FileFilter) {
                    continue;
                }
                if (ScaleFilter != null && weighingInfo.scaleName != ScaleFilter) {
                    continue;
                }
                // Vazeni vyhovuje, pridam ho do seznamu
                filteredInfoList.Add(weighingInfo);
            }
        }

    }

}
