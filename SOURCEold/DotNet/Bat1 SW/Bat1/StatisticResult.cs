﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Scale;
using Veit.ScaleStatistics;

namespace Bat1 {
    /// <summary>
    /// Results of one statistic calculation
    /// </summary>
    public class StatisticResult {
        
        /// <summary>
        /// Number of histogram slots
        /// </summary>
        public static int HISTOGRAM_SLOT_COUNT = 39;
        
        /// <summary>
        /// Flag used for calculation
        /// </summary>
        public readonly Flag Flag;
        
        /// <summary>
        /// Number of samples
        /// </summary>
        public readonly int Count;
        
        /// <summary>
        /// Average weight rounded to grams (i.e. the same as in the scale)
        /// </summary>
        public float Average;

        /// <summary>
        /// Exact average weight (not rounded) used for calculations like Count * Average
        /// </summary>
        public float ExactAverage;
        
        /// <summary>
        /// Standard deviation rounded to grams (i.e. the same as in the scale)
        /// </summary>
        public float Sigma;

        /// <summary>
        /// Exact Sigma (not rounded) used for units conversions
        /// </summary>
        public float ExactSigma;

        /// <summary>
        /// Coeficient of variation
        /// </summary>
        public readonly float Cv;

        /// <summary>
        /// Uniformity
        /// </summary>
        public readonly float Uniformity;

        /// <summary>
        /// Weighing speed in chicks per hour
        /// </summary>
        public double WeighingSpeed;

        /// <summary>
        /// Histogram
        /// </summary>
        public readonly Histogram Histogram;

        /// <summary>
        /// Date and time when the weighing was started
        /// </summary>
        public readonly DateTime WeighingStarted;

        /// <summary>
        /// Constructor used with data downloaded from scales
        /// </summary>
        /// <param name="sampleList">List of samples</param>
        /// <param name="flag">Filter by flag</param>
        /// <param name="uniformityRange">Uniformity range in percents</param>
        public StatisticResult(SampleList sampleList, Flag flag, int uniformityRange, HistogramConfig histogramConfig, Units units) {
            Statistics statistics = new Statistics();
            
            // Ulozim si flag
            Flag = flag;
            
            // Naplnim data do vypoctu statistiky
            sampleList.First(flag);
            while (sampleList.Read(flag)) {
                statistics.Add(sampleList.Sample.weight);
            }

            // Vypoctu vsechny statisticke hodnoty
            // Average a Sigma vypoctu rovnou jako ve vaze, tj. na gramy. Prumer 3.26598 se tak prevede napr. na
            // 3.26500002, tuto hodnotu musim jeste pred zobrazenim zaokrouhlit normalne aritmeticky.
            Count         = statistics.Count();

            ExactAverage  = statistics.Average();
            Average       = float.Parse(DisplayFormat.CutWeight(ExactAverage, units));      // Jako ve vaze
            ExactSigma    = statistics.Sigma();
            Sigma         = float.Parse(DisplayFormat.CutWeight(ExactSigma, units));        // Jako ve vaze

            Cv            = statistics.Variation(Average, Sigma);               // Pocitam pomoci Average i Sigma zaokrouhleneho na gramy, aby to sedelo s vahou
            Uniformity    = statistics.Uniformity(uniformityRange, Average);    // Pocitam pomoci Average zaokrouhleneho na gramy, aby to sedelo s vahou
            TimeSpan span = sampleList.MaxDateTime - sampleList.MinDateTime;    // Delka vazeni
            if (span.TotalHours != 0) {
                WeighingSpeed = (double)Count / span.TotalHours;
            } else {
                WeighingSpeed = 1;
            }

            // Histogram
            Histogram = statistics.Histogram(histogramConfig, units, HISTOGRAM_SLOT_COUNT);

            // Zacatek vazeni beru z prvniho vzorku
            WeighingStarted = sampleList.MinDateTime;
        }

        /// <summary>
        /// Constructor used with manually entered results
        /// </summary>
        /// <param name="flag"></param>
        /// <param name="count"></param>
        /// <param name="average"></param>
        /// <param name="sigma"></param>
        /// <param name="cv"></param>
        /// <param name="uniformity"></param>
        public StatisticResult(Flag flag, int count, float average, float sigma, float cv, float uniformity, DateTime weighingStarted) {
            // Preberu zadane hodnoty
            Flag       = flag;
            Count      = count;
            ExactAverage = Average = average;
            ExactSigma = Sigma = sigma;
            Cv         = cv;
            Uniformity = uniformity;

            // Ostatni hodnoty vynuluju
            Histogram     = null;
            WeighingSpeed = 0;

            // Zacatek vazeni zadal rucne
            WeighingStarted = weighingStarted;
        }

        /// <summary>
        /// Recalculate result to new units. Used only for manual results.
        /// </summary>
        /// <param name="units">New units</param>
        public void ChangeUnits(Units oldUnits, Units newUnits) {
            if (oldUnits == newUnits) {
                return;     
            }

            // Vypocet presnych hodnot
            ExactAverage = (float)ConvertWeight.Convert(ExactAverage, oldUnits, newUnits);
            ExactSigma   = (float)ConvertWeight.Convert(ExactSigma,   oldUnits, newUnits);

            // Zaokrouhleni na gramy jako ve vaze, podle novych jednotek
            Average = float.Parse(DisplayFormat.CutWeight(ExactAverage, newUnits));
            Sigma   = float.Parse(DisplayFormat.CutWeight(ExactSigma, newUnits));
        }

    }
}
