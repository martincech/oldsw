﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat1 {
    
    /// <summary>
    /// Table with manually entered results
    /// </summary>
    public class DatabaseTableManualResults : DatabaseTable {

        // Tabulka s rucne zadanymi vysledky.

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableManualResults(DatabaseFactory factory)
            : base("ManualResults", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "WeighingId "      + factory.TypeInteger   + " NOT NULL,"       // Id vazeni, ke kteremu vysledek patri
                 + "StartedDateTime " + factory.TypeTimestamp + ","       // Datum a cas zahajeni vazeni
                 + "Flag "            + factory.TypeByte      + ","       
                 + "Count "           + factory.TypeSmallInt  + ","       
                 + "Average "         + factory.TypeFloat     + ","       
                 + "Sigma "           + factory.TypeFloat     + ","       
                 + "Cv "              + factory.TypeFloat     + ","       
                 + "Uniformity "      + factory.TypeByte      + ","       
                 + "CONSTRAINT FK_P_Weighings FOREIGN KEY (WeighingId) REFERENCES Weighings(WeighingId)"
                 + ")");
        }

        /// <summary>
        /// Add new row
        /// </summary>
        /// <param name="weighingId">Weighing ID</param>
        /// <param name="weight">Weight</param>
        /// <param name="flag">Flag</param>
        /// <param name="savedDateTime">Saved timestamp</param>
        public void Add(long weighingId, DateTime startedDateTime, Flag flag, int count,
                        double average, double sigma, double cv, int uniformity) {
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (@WeighingId, @StartedDateTime, @Flag, @Count, @Average, @Sigma, @Cv, @Uniformity)")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId",      (int)weighingId));       // Id vazeni ukladam jako 32bit integer kvuli mistu
                command.Parameters.Add(factory.CreateParameter("@StartedDateTime", startedDateTime));
                command.Parameters.Add(factory.CreateParameter("@Flag",            flag));
                command.Parameters.Add(factory.CreateParameter("@Count",           count));
                command.Parameters.Add(factory.CreateParameter("@Average",         average));
                command.Parameters.Add(factory.CreateParameter("@Sigma",           sigma));
                command.Parameters.Add(factory.CreateParameter("@Cv",              cv));
                command.Parameters.Add(factory.CreateParameter("@Uniformity",      uniformity));
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Delete results
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long weighingId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE WeighingId = @WeighingId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId", weighingId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Load results form database
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>List of StatisticResult</returns>
        public List<StatisticResult> Load(long weighingId) {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE WeighingId = @WeighingId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId", weighingId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.HasRows) {
                        throw new Exception(String.Format("Manual results with weighing Id {0} don't exist", weighingId));
                    }
                    List<StatisticResult> resultList = new List<StatisticResult>();
                    while (reader.Read()) {
                        StatisticResult result = new StatisticResult((Flag)(byte)reader["Flag"], (short)reader["Count"], (float)reader["Average"],
                                                                     (float)reader["Sigma"], (float)reader["Cv"], (byte)reader["Uniformity"],
                                                                     (DateTime)reader["StartedDateTime"]);
                        resultList.Add(result);
                    }
                    return resultList;
                }
            }
        }

    }
}
