﻿namespace Bat1 {
    partial class UserControlScaleConfigWeighing {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigWeighing));
            this.labelDivisionUnits = new System.Windows.Forms.Label();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownDivision = new System.Windows.Forms.NumericUpDown();
            this.comboBoxCapacity = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDivision)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDivisionUnits
            // 
            resources.ApplyResources(this.labelDivisionUnits, "labelDivisionUnits");
            this.labelDivisionUnits.Name = "labelDivisionUnits";
            // 
            // comboBoxUnits
            // 
            resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.comboBoxUnits.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUnits_SelectionChangeCommitted);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // numericUpDownDivision
            // 
            resources.ApplyResources(this.numericUpDownDivision, "numericUpDownDivision");
            this.numericUpDownDivision.Name = "numericUpDownDivision";
            this.numericUpDownDivision.ValueChanged += new System.EventHandler(this.numericUpDownDivision_ValueChanged);
            // 
            // comboBoxCapacity
            // 
            resources.ApplyResources(this.comboBoxCapacity, "comboBoxCapacity");
            this.comboBoxCapacity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCapacity.FormattingEnabled = true;
            this.comboBoxCapacity.Items.AddRange(new object[] {
            resources.GetString("comboBoxCapacity.Items"),
            resources.GetString("comboBoxCapacity.Items1")});
            this.comboBoxCapacity.Name = "comboBoxCapacity";
            this.comboBoxCapacity.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCapacity_SelectionChangeCommitted);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // UserControlScaleConfigWeighing
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.comboBoxCapacity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numericUpDownDivision);
            this.Controls.Add(this.labelDivisionUnits);
            this.Controls.Add(this.comboBoxUnits);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label10);
            this.Name = "UserControlScaleConfigWeighing";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownDivision)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDivisionUnits;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownDivision;
        private System.Windows.Forms.ComboBox comboBoxCapacity;
        private System.Windows.Forms.Label label2;
    }
}
