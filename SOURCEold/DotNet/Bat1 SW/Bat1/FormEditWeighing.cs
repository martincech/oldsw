﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormEditWeighing : Form {
        /// <summary>
        /// Weighing to edit
        /// </summary>
        private WeighingData weighingData;

        /// <summary>
        /// Copy of sample list to edit
        /// </summary>
        private SampleList sampleList;

        /// <summary>
        /// List with all file names and notes
        /// </summary>
        private NameNoteUniqueList fileList;

        /// <summary>
        /// Scale config to show (not edit)
        /// </summary>
        private ScaleConfig scaleConfig;
        
        /// <summary>
        /// Table with samples
        /// </summary>
        private UserControlSamples userControlSamples;

        /// <summary>
        /// Data is loading to the lists
        /// </summary>
        private bool isLoading = false;

        public FormEditWeighing(WeighingData weighingData, NameNoteUniqueList fileList, ScaleConfig scaleConfig, bool readOnly, bool showDivide) {
            InitializeComponent();

            // Preberu data
            this.weighingData = weighingData;
            sampleList        = new SampleList(weighingData.SampleList);        // Udelam si kopii, kterou budu editovat
            this.fileList     = fileList;
            this.scaleConfig  = scaleConfig;        // Config zde needituju, neni treba delat kopii

            // Vytvorim novy control pro vzorky
            userControlSamples = new UserControlSamples(sampleList,             // Edituju kopii, original necham puvodni
                                                        Flag.ALL,
                                                        weighingData.ScaleConfig.Units.Units,
                                                        readOnly,
                                                        showDivide);
            groupBoxSamples.Controls.Add(userControlSamples);
            userControlSamples.Dock = DockStyle.Fill;

            // Zaregistruju event handler po smazani vzorku
            userControlSamples.DeletedEvent += SampleDeletedEventHandler;

            // Config zobrazuju jen u vazeni z nove vahy
            if (weighingData.ResultType == ResultType.NEW_SCALE) {
                // Vytvorim control pro config vahy
                userControlScaleConfig = new UserControlScaleConfig(true);      // COnfig je zde vzdy Read-only
                userControlScaleConfig.Location = new Point(11, 11);
                userControlScaleConfig.Size     = new Size(696, 365);
                userControlScaleConfig.TabIndex = 0;
                userControlScaleConfig.Dock = DockStyle.Fill;
                tabPageConfig.Controls.Add(userControlScaleConfig);
                
                // Predam config vahy
                userControlScaleConfig.SetScaleConfig(scaleConfig);
            } else {
                // U stare vahy nemuzu zobrazit config, neni kompletni a hazi vyjimky
                tabControl.TabPages.Clear();
                tabControl.TabPages.Add(tabPageWeighing);
            }

            // Podle rezimu read-only nastavim tlacitka
            if (readOnly) {
                buttonCancel.Text = Properties.Resources.CLOSE;
                AcceptButton = buttonCancel;
                buttonOk.Visible = false;
            }

            // Vykreslim
            Redraw();
        }

        /// <summary>
        /// Calculate start date time
        /// </summary>
        /// <returns>DateTime</returns>
        private DateTime GetStartDateTime() {
            return new DateTime(dateTimePickerStartDate.Value.Year, dateTimePickerStartDate.Value.Month, dateTimePickerStartDate.Value.Day,
                                dateTimePickerStartTime.Value.Hour, dateTimePickerStartTime.Value.Minute, dateTimePickerStartTime.Value.Second);
        }
        
        private void RedrawEndOfWeighing() {
            labelEndOfWeighing.Text = sampleList.MaxDateTime.ToString();
        }

        private void ChangeStartDateTime(DateTime startDateTime) {
            userControlSamples.SetWeighingStart(startDateTime);
            RedrawEndOfWeighing();
        }

        private void LoadFileComboBox() {
            comboBoxFileName.Items.Clear();
            foreach (NameNote file in fileList) {
                comboBoxFileName.Items.Add(file.Name);
            }
        }

        private void Redraw() {
            isLoading = true;
            try {
                LoadFileComboBox();
                comboBoxFileName.Text         = weighingData.File.Name;
                textBoxFileNote.Text          = weighingData.File.Note;
                dateTimePickerStartDate.Value = sampleList.MinDateTime;
                dateTimePickerStartTime.Value = sampleList.MinDateTime;
                RedrawEndOfWeighing();
                textBoxNote.Text              = weighingData.Note;
            } finally {
                isLoading = false;
            }
        }
        
        private string GetFileNote(string fileName) {
            foreach (NameNote file in fileList) {
                if (file.Name == fileName) {
                    return file.Note;
                }
            }
            return "";
        }

        /// <summary>
        /// Sample deleted event handler
        /// </summary>
        /// <param name="sender"></param>
        public void SampleDeletedEventHandler(object sender) {
            Redraw();
        }
        
        private void comboBoxFileName_KeyPress(object sender, KeyPressEventArgs e) {
            e.KeyChar = Char.ToUpper(e.KeyChar);
            if (!KeyFilter.IsScaleChar(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void textBoxFileNote_TextChanged(object sender, EventArgs e) {
            if (isLoading) {
                return;         // Data se nahravaji, nevykresluju
            }

            // Pokusim se nacist z databaze poznamku k prave zadanemu souboru
            textBoxFileNote.Text = GetFileNote(comboBoxFileName.Text);
        }

        private void dateTimePickerStartDate_ValueChanged(object sender, EventArgs e) {
            if (isLoading) {
                return;         // Data se nahravaji, nevykresluju
            }

            ChangeStartDateTime(GetStartDateTime());
        }

        private void FormWeighingParameters_Shown(object sender, EventArgs e) {
            comboBoxFileName.Focus();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Kontrola zadanych hodnot
            string fileName;
            if (!CheckValue.GetScaleName(comboBoxFileName, out fileName)) {
                return;
            }
            
            string fileNote;
            if (!CheckValue.GetScaleText(textBoxFileNote, out fileNote)) {
                return;
            }

            DateTime startDateTime = GetStartDateTime();

            string note = textBoxNote.Text;

            // Vse je v poradku. Pokud nezmenil ani jeden parametr, ukoncim dialog jakoby stisknul Cancel,
            // tj. volajici okno nebude muset nic prekreslovat.
            if (fileName == weighingData.File.Name
             && fileNote == weighingData.File.Note
             && sampleList.MinDateTime == weighingData.SampleList.MinDateTime
             && note == weighingData.Note
             && sampleList.Count == weighingData.SampleList.Count
             && !userControlSamples.IsWeightsDivided    // Deleni hmotnosti musim hlidat zvlast, pocet vzorku ani datum se nezmeni
             && userControlSamples.Units == weighingData.ScaleConfig.Units.Units) {
                DialogResult = DialogResult.Cancel;
                return;
            }

            // Ulozim nove zadane hodnoty
            
            // Soubor, updatuje se automaticky i v configu vahy. Prejmenovanim se muzou rozdrbat skupiny souboru,
            // ale to zatim neresim (jde o okrajovou zalezitost).
            weighingData.File.Rename(fileName);
            weighingData.File.SetNote(fileNote);

            // Poznamka k vazeni
            weighingData.Note = note;

            // Vzorky vcetne pripadneho casoveho posunu
            weighingData.SampleList = new SampleList(sampleList);

            // Jednotky - pozor, nasledne musim prepocitat jednotky u vsech vazeni, ktere vazil soucasne s timto vazenim (tj. vsechna vazeni,
            // ktera sdili tento ScaleConfig)
            weighingData.ScaleConfig.SetNewUnits(userControlSamples.Units);

            DialogResult = DialogResult.OK;
        }

        private void comboBoxFileName_TextChanged(object sender, EventArgs e) {
            if (isLoading) {
                return;         // Data se nahravaji, nevykresluju
            }

            // Pokusim se nacist z databaze poznamku k prave zadanemu souboru
            textBoxFileNote.Text = fileList.GetNote(comboBoxFileName.Text);
        }
    }
}
