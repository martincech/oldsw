﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    /// <summary>
    /// Base class for reading results from a scale and entering results manually
    /// </summary>
    public class UserControlResultsBase : UserControl {
        /// <summary>
        /// Items are loading to their lists
        /// </summary>
        protected bool isLoading = false;

        /// <summary>
        /// List with scale names
        /// </summary>
        protected List<string> scaleList;
        
        /// <summary>
        /// List with file names and notes (name is unique)
        /// </summary>
        protected NameNoteUniqueList fileList;

        /// <summary>
        /// ComboBox used for scale names in the form
        /// </summary>
        private ComboBox comboBoxScaleNameBase;

        public UserControlResultsBase() {
        }

        /// <summary>
        /// Set combo box with scale names
        /// </summary>
        /// <param name="comboBoxScaleName">ComboBox with scale names</param>
        protected void SetScaleComboBox(ComboBox comboBox) {
            this.comboBoxScaleNameBase = comboBox;
        }

        private void LoadScaleComboBox() {
            if (comboBoxScaleNameBase == null) {
                return;     // Combobox pro jmeno vahy nepouziva (napr. v UserControlScaleResults)
            }
            
            isLoading = true;
            try {
                comboBoxScaleNameBase.Items.Clear();
                comboBoxScaleNameBase.Items.AddRange(scaleList.ToArray());
            } finally {
                isLoading = false;
            }
        }

        /// <summary>
        /// Load scale and file lists
        /// </summary>
        protected void LoadLists() {
            // Nahraju seznamy vah a souboru z databaze
            List<NameNote> fileList = Program.Database.LoadFileNameNoteList();
            this.fileList = new NameNoteUniqueList();
            foreach (NameNote file in fileList) {
                this.fileList.Add(file);
            }
            scaleList = Program.Database.LoadScaleNameList();
            
            // Ulozim seznam vah do controlu
            LoadScaleComboBox();
        }

        /// <summary>
        /// Add scale to the list
        /// </summary>
        /// <param name="scaleName">Scale name</param>
        protected void AddScale(string scaleName) {
            if (scaleList.Contains(scaleName)) {
                return;         // Vaha uz v seznamu je
            }

            // Pridam vahu do seznamu
            string oldText = null;
            if (comboBoxScaleNameBase != null) {
                oldText = comboBoxScaleNameBase.Text;
            }
            scaleList.Add(scaleName);
            scaleList.Sort();
            if (comboBoxScaleNameBase != null) {
                LoadScaleComboBox();
                comboBoxScaleNameBase.Text = oldText;           // Obnovim vybranou polozku
            }
        }

        /// <summary>
        /// Add file to the list
        /// </summary>
        /// <param name="file">File name and note</param>
        protected void AddFile(NameNote file) {
            // Zkontroluju, zda zadal nove jmeno souboru
            foreach (NameNote f in fileList) {
                if (f.Name == file.Name) {
                    // Soubor s timto nazvem uz v seznamu je, updatuju u nej jen poznamku
                    f.SetNote(file.Note);
                    return;
                }
            }

            // Pridam soubor do seznamu, obnovu controlu je treba udelat rucne
            fileList.Add(file);
            fileList.Sort(NameNote.CompareByName);
        }

        /// <summary>
        /// Check if specified weighing can be saved to the database
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="startDateTime">DateTime of first sample</param>
        /// <returns>True if the weighing can be saved</returns>
        public static DialogResult CanSaveWeighing(string fileName, DateTime startDateTime) {
            // Zkontroluju, zda uz vazeni v databazi neexistuje (kombinace soubor - start vazeni). Pokud existuje,
            // musi se z databaze nejprve smazat.
            long weighingId;
            if (!Program.Database.WeighingExists(fileName, startDateTime, out weighingId)) {
                return DialogResult.Yes;    // Vazeni neexistuje, je mozne ulozit
            }

            // Vazeni uz existuje, zeptam se, zda chce prepsat
            Cursor oldCursor = Cursor.Current;     // Ulozim si aktualni kurzor a zmenim na default
            Cursor.Current = Cursors.Default;
            DialogResult dialogResult = MessageBox.Show(String.Format(Properties.Resources.WEIGHING_EXISTS, fileName + ", " + startDateTime.ToString()),
                                                        Program.ApplicationName, MessageBoxButtons.YesNoCancel);
            Cursor.Current = oldCursor;            // Obnovim puvodni kurzor
                            
            if (dialogResult != DialogResult.Yes) {
                return dialogResult;        // Nechce prepsat, neni mozne ulozit
            }
            
            // Smazu stare vazeni, nasledne bude mozne vazeni ulozit
            Program.Database.DeleteWeighing(weighingId);
            return DialogResult.Yes;
        }

    }
}
