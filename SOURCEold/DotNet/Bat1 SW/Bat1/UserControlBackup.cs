﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Bat1 {
    public partial class UserControlBackup : UserControl {
        public UserControlBackup() {
            InitializeComponent();
        }

        private void buttonBackup_Click(object sender, EventArgs e) {
            // Zeptam se na umisteni zalohy
            // Zeptam se na jmeno souboru
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = FileExtension.BACKUP;
            saveFileDialog.Filter     = String.Format(Properties.Resources.FILE_FORMAT_BACKUP, Program.ApplicationName) + " (*." + FileExtension.BACKUP + ")|*." + FileExtension.BACKUP;
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Nastavim spravnou priponu
            string fileName = Path.ChangeExtension(saveFileDialog.FileName, FileExtension.BACKUP);
            
            // Zalohuju
            Refresh();
            Cursor.Current = Cursors.WaitCursor;
            try {
                Backup.BackupWithCompaction(fileName);
                Cursor.Current = Cursors.Default;
                MessageBox.Show(Properties.Resources.BACKUP_SUCCESSFUL, Program.ApplicationName);
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonRestore_Click(object sender, EventArgs e) {
            new FormRestoreBackup().ShowDialog();
        }
    }
}
