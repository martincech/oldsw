﻿namespace Bat1 {
    partial class UserControlScaleConfig {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfig));
            this.treeViewMenu = new System.Windows.Forms.TreeView();
            this.groupBoxDetails = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // treeViewMenu
            // 
            resources.ApplyResources(this.treeViewMenu, "treeViewMenu");
            this.treeViewMenu.Name = "treeViewMenu";
            this.treeViewMenu.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            ((System.Windows.Forms.TreeNode)(resources.GetObject("treeViewMenu.Nodes"))),
            ((System.Windows.Forms.TreeNode)(resources.GetObject("treeViewMenu.Nodes1")))});
            this.treeViewMenu.ShowRootLines = false;
            this.treeViewMenu.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeViewMenu_BeforeCollapse);
            this.treeViewMenu.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewMenu_AfterSelect);
            // 
            // groupBoxDetails
            // 
            resources.ApplyResources(this.groupBoxDetails, "groupBoxDetails");
            this.groupBoxDetails.Name = "groupBoxDetails";
            this.groupBoxDetails.TabStop = false;
            // 
            // UserControlScaleConfig
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxDetails);
            this.Controls.Add(this.treeViewMenu);
            this.Name = "UserControlScaleConfig";
            this.Load += new System.EventHandler(this.UserControlScaleConfig_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeViewMenu;
        private System.Windows.Forms.GroupBox groupBoxDetails;
    }
}
