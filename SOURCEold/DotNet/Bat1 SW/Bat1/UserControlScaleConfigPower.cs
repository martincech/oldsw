﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlScaleConfigPower : UserControlScaleConfigBase {

        public UserControlScaleConfigPower(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Preberu Read-only
            this.readOnly = readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        public override void Redraw() {
            isLoading = true;

            try {
                numericUpDownPeriod.Value = scaleConfig.PowerOffTimeout / 60;   // Je ulozeno v sekundach
            } finally {
                isLoading = false;
            }
        }

        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.PowerOffTimeout = (int)numericUpDownPeriod.Value * 60;      // Prepocitam na sekundy
        }

        private void numericUpDownPeriod_ValueChanged(object sender, EventArgs e) {
            ControlsToConfig();
        }

    }
}
