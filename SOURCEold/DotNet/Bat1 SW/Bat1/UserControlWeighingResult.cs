﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Scale;
using Veit.Bat1;

namespace Bat1 {
    public partial class UserControlWeighingResult : UserControl {

        private void SelectControl(Control control, TabControl tabControl, TabPage tabPage) {
            if (tabControl != null) {
                tabControl.SelectedTab = tabPage;
            }
            CheckValue.SelectControl(control);
        }

        private bool SaveInt(TextBox textBox, out int i, bool canBeEmpty, int min, int max, TabControl tabControl, TabPage tabPage) {
            i = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (canBeEmpty && textBox.Text == "") {
                return true;
            }
            if (!Int32.TryParse(textBox.Text, out i)) {
                MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
                SelectControl(textBox, tabControl, tabPage);
                return false;
            }
            if (i < min || i > max) {
                MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
                SelectControl(textBox, tabControl, tabPage);
                return false;
            }
            return true;
        }

        private bool SaveDouble(TextBox textBox, out double d, bool canBeEmpty, double min, double max, TabControl tabControl, TabPage tabPage) {
            d = 0;      // Pokud je textbox brazdny, hodnota je 0
            if (canBeEmpty && textBox.Text == "") {
                return true;
            }
            if (!Double.TryParse(textBox.Text, out d)) {
                MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
                tabControl.SelectedTab = tabPage;
                textBox.Focus();
                return false;
            }
            if (d < min || d > max) {
                MessageBox.Show(Properties.Resources.VALUE_NOT_VALID, Program.ApplicationName);
                tabControl.SelectedTab = tabPage;
                textBox.Focus();
                return false;
            }
            return true;
        }
        
        public bool GetResult(Units units, Flag flag, out StatisticResult result, TabControl tabControl, TabPage tabPage) {
            result = null;
            
            // Zkontroluju zadane hodnoty - pocet a prumer musi zadat, ostatni nemusi
            int count;
            if (!SaveInt(textBoxCount, out count, false, 1, 99999, tabControl, tabPage)) {
                return false;
            }

            // Maximalni a minimalni hmotnost podle jednotek
            double minWeight = ConvertWeight.MinWeight(units);
            double maxWeight = ConvertWeight.MaxWeight(units, WeighingCapacity.EXTENDED);     // Povolim 50kg

            double average;
            if (!SaveDouble(textBoxAverage, out average, false, minWeight, maxWeight, tabControl, tabPage)) {
                return false;
            }

            double sigma;
            if (!SaveDouble(textBoxSigma, out sigma, true, 0, maxWeight,tabControl, tabPage)) {
                return false;
            }

            double cv;
            if (!SaveDouble(textBoxCv, out cv, true, 0, 99.9, tabControl, tabPage)) {
                return false;
            }

            double uniformity;
            if (!SaveDouble(textBoxUniformity, out uniformity, true, 0, 100.0, tabControl, tabPage)) {
                return false;
            }
            
            result = new StatisticResult(flag, count, (float)average, (float)sigma, (float)cv, (float)uniformity, DateTime.Now);
            return true;
        }
        
        public void SetResult(StatisticResult result, Units units) {
            textBoxCount.Text      = result.Count.ToString();
            textBoxAverage.Text    = DisplayFormat.RoundWeight(result.Average, units);
            if (result.Sigma != 0) {
                textBoxSigma.Text  = DisplayFormat.RoundWeight(result.Sigma, units);
            }
            if (result.Cv != 0) {
                textBoxCv.Text     = DisplayFormat.Cv(result.Cv);
            }
            if (result.Uniformity != 0) {
                textBoxUniformity.Text = DisplayFormat.Uniformity(result.Uniformity);
            }
        }

        public UserControlWeighingResult() {
            InitializeComponent();
        }

        private void textBoxManualCount_KeyPress(object sender, KeyPressEventArgs e) {
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void textBoxManualAverage_KeyPress(object sender, KeyPressEventArgs e) {
            if (!KeyFilter.IsFloat(e.KeyChar)) {
                e.Handled = true;
            }
        }
    }
}
