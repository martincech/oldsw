﻿namespace Bat1 {
    partial class UserControlFlockStatistics {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlFlockStatistics));
            this.tabControlCurrent = new System.Windows.Forms.TabControl();
            this.tabPageStatistics = new System.Windows.Forms.TabPage();
            this.userControlStatisticsTable = new Bat1.UserControlStatisticsTable();
            this.tabPageGraphs = new System.Windows.Forms.TabPage();
            this.userControlGraphs = new Bat1.UserControlGraphs();
            this.flowLayoutPanelBottom = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonExportGraph = new System.Windows.Forms.Button();
            this.buttonPrintGraph = new System.Windows.Forms.Button();
            this.userControlStatisticsTop = new Bat1.UserControlStatisticsTop();
            this.tabControlCurrent.SuspendLayout();
            this.tabPageStatistics.SuspendLayout();
            this.tabPageGraphs.SuspendLayout();
            this.flowLayoutPanelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlCurrent
            // 
            resources.ApplyResources(this.tabControlCurrent, "tabControlCurrent");
            this.tabControlCurrent.Controls.Add(this.tabPageStatistics);
            this.tabControlCurrent.Controls.Add(this.tabPageGraphs);
            this.tabControlCurrent.Name = "tabControlCurrent";
            this.tabControlCurrent.SelectedIndex = 0;
            // 
            // tabPageStatistics
            // 
            resources.ApplyResources(this.tabPageStatistics, "tabPageStatistics");
            this.tabPageStatistics.Controls.Add(this.userControlStatisticsTable);
            this.tabPageStatistics.Name = "tabPageStatistics";
            this.tabPageStatistics.UseVisualStyleBackColor = true;
            // 
            // userControlStatisticsTable
            // 
            resources.ApplyResources(this.userControlStatisticsTable, "userControlStatisticsTable");
            this.userControlStatisticsTable.Name = "userControlStatisticsTable";
            // 
            // tabPageGraphs
            // 
            resources.ApplyResources(this.tabPageGraphs, "tabPageGraphs");
            this.tabPageGraphs.Controls.Add(this.userControlGraphs);
            this.tabPageGraphs.Controls.Add(this.flowLayoutPanelBottom);
            this.tabPageGraphs.Name = "tabPageGraphs";
            this.tabPageGraphs.UseVisualStyleBackColor = true;
            // 
            // userControlGraphs
            // 
            resources.ApplyResources(this.userControlGraphs, "userControlGraphs");
            this.userControlGraphs.Name = "userControlGraphs";
            // 
            // flowLayoutPanelBottom
            // 
            resources.ApplyResources(this.flowLayoutPanelBottom, "flowLayoutPanelBottom");
            this.flowLayoutPanelBottom.Controls.Add(this.buttonExportGraph);
            this.flowLayoutPanelBottom.Controls.Add(this.buttonPrintGraph);
            this.flowLayoutPanelBottom.Name = "flowLayoutPanelBottom";
            // 
            // buttonExportGraph
            // 
            resources.ApplyResources(this.buttonExportGraph, "buttonExportGraph");
            this.buttonExportGraph.Name = "buttonExportGraph";
            this.buttonExportGraph.UseVisualStyleBackColor = true;
            this.buttonExportGraph.Click += new System.EventHandler(this.buttonExportGraph_Click);
            // 
            // buttonPrintGraph
            // 
            resources.ApplyResources(this.buttonPrintGraph, "buttonPrintGraph");
            this.buttonPrintGraph.Name = "buttonPrintGraph";
            this.buttonPrintGraph.UseVisualStyleBackColor = true;
            this.buttonPrintGraph.Click += new System.EventHandler(this.buttonPrintGraph_Click);
            // 
            // userControlStatisticsTop
            // 
            resources.ApplyResources(this.userControlStatisticsTop, "userControlStatisticsTop");
            this.userControlStatisticsTop.Name = "userControlStatisticsTop";
            // 
            // UserControlFlockStatistics
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlCurrent);
            this.Controls.Add(this.userControlStatisticsTop);
            this.Name = "UserControlFlockStatistics";
            this.tabControlCurrent.ResumeLayout(false);
            this.tabPageStatistics.ResumeLayout(false);
            this.tabPageGraphs.ResumeLayout(false);
            this.tabPageGraphs.PerformLayout();
            this.flowLayoutPanelBottom.ResumeLayout(false);
            this.flowLayoutPanelBottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private UserControlStatisticsTop userControlStatisticsTop;
        private System.Windows.Forms.TabControl tabControlCurrent;
        private System.Windows.Forms.TabPage tabPageStatistics;
        private UserControlStatisticsTable userControlStatisticsTable;
        private System.Windows.Forms.TabPage tabPageGraphs;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBottom;
        private System.Windows.Forms.Button buttonExportGraph;
        private System.Windows.Forms.Button buttonPrintGraph;
        private UserControlGraphs userControlGraphs;

    }
}
