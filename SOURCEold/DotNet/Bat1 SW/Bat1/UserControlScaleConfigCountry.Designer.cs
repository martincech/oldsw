﻿namespace Bat1 {
    partial class UserControlScaleConfigCountry {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigCountry));
            this.comboBoxDaylightSaving = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxCountry = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxLanguage = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxDate = new System.Windows.Forms.GroupBox();
            this.labelDateExample = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxDateSeparator2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDateSeparator1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxDateFormat = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxTime = new System.Windows.Forms.GroupBox();
            this.labelTimeExample = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxTimeSeparator = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxTimeFormat = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBoxDate.SuspendLayout();
            this.groupBoxTime.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxDaylightSaving
            // 
            this.comboBoxDaylightSaving.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDaylightSaving.FormattingEnabled = true;
            this.comboBoxDaylightSaving.Items.AddRange(new object[] {
            resources.GetString("comboBoxDaylightSaving.Items"),
            resources.GetString("comboBoxDaylightSaving.Items1"),
            resources.GetString("comboBoxDaylightSaving.Items2")});
            resources.ApplyResources(this.comboBoxDaylightSaving, "comboBoxDaylightSaving");
            this.comboBoxDaylightSaving.Name = "comboBoxDaylightSaving";
            this.comboBoxDaylightSaving.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // comboBoxCountry
            // 
            this.comboBoxCountry.DropDownHeight = 250;
            this.comboBoxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCountry.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxCountry, "comboBoxCountry");
            this.comboBoxCountry.Items.AddRange(new object[] {
            resources.GetString("comboBoxCountry.Items"),
            resources.GetString("comboBoxCountry.Items1"),
            resources.GetString("comboBoxCountry.Items2"),
            resources.GetString("comboBoxCountry.Items3"),
            resources.GetString("comboBoxCountry.Items4")});
            this.comboBoxCountry.Name = "comboBoxCountry";
            this.comboBoxCountry.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCountry_SelectionChangeCommitted);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // comboBoxLanguage
            // 
            this.comboBoxLanguage.DropDownHeight = 250;
            this.comboBoxLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLanguage.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxLanguage, "comboBoxLanguage");
            this.comboBoxLanguage.Items.AddRange(new object[] {
            resources.GetString("comboBoxLanguage.Items"),
            resources.GetString("comboBoxLanguage.Items1"),
            resources.GetString("comboBoxLanguage.Items2"),
            resources.GetString("comboBoxLanguage.Items3"),
            resources.GetString("comboBoxLanguage.Items4"),
            resources.GetString("comboBoxLanguage.Items5"),
            resources.GetString("comboBoxLanguage.Items6"),
            resources.GetString("comboBoxLanguage.Items7"),
            resources.GetString("comboBoxLanguage.Items8"),
            resources.GetString("comboBoxLanguage.Items9"),
            resources.GetString("comboBoxLanguage.Items10"),
            resources.GetString("comboBoxLanguage.Items11"),
            resources.GetString("comboBoxLanguage.Items12"),
            resources.GetString("comboBoxLanguage.Items13")});
            this.comboBoxLanguage.Name = "comboBoxLanguage";
            this.comboBoxLanguage.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // groupBoxDate
            // 
            this.groupBoxDate.Controls.Add(this.labelDateExample);
            this.groupBoxDate.Controls.Add(this.label5);
            this.groupBoxDate.Controls.Add(this.comboBoxDateSeparator2);
            this.groupBoxDate.Controls.Add(this.label3);
            this.groupBoxDate.Controls.Add(this.comboBoxDateSeparator1);
            this.groupBoxDate.Controls.Add(this.label4);
            this.groupBoxDate.Controls.Add(this.comboBoxDateFormat);
            this.groupBoxDate.Controls.Add(this.label7);
            resources.ApplyResources(this.groupBoxDate, "groupBoxDate");
            this.groupBoxDate.Name = "groupBoxDate";
            this.groupBoxDate.TabStop = false;
            // 
            // labelDateExample
            // 
            resources.ApplyResources(this.labelDateExample, "labelDateExample");
            this.labelDateExample.Name = "labelDateExample";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // comboBoxDateSeparator2
            // 
            this.comboBoxDateSeparator2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDateSeparator2.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxDateSeparator2, "comboBoxDateSeparator2");
            this.comboBoxDateSeparator2.Name = "comboBoxDateSeparator2";
            this.comboBoxDateSeparator2.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // comboBoxDateSeparator1
            // 
            this.comboBoxDateSeparator1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDateSeparator1.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxDateSeparator1, "comboBoxDateSeparator1");
            this.comboBoxDateSeparator1.Name = "comboBoxDateSeparator1";
            this.comboBoxDateSeparator1.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // comboBoxDateFormat
            // 
            this.comboBoxDateFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDateFormat.FormattingEnabled = true;
            this.comboBoxDateFormat.Items.AddRange(new object[] {
            resources.GetString("comboBoxDateFormat.Items"),
            resources.GetString("comboBoxDateFormat.Items1"),
            resources.GetString("comboBoxDateFormat.Items2"),
            resources.GetString("comboBoxDateFormat.Items3"),
            resources.GetString("comboBoxDateFormat.Items4"),
            resources.GetString("comboBoxDateFormat.Items5"),
            resources.GetString("comboBoxDateFormat.Items6"),
            resources.GetString("comboBoxDateFormat.Items7")});
            resources.ApplyResources(this.comboBoxDateFormat, "comboBoxDateFormat");
            this.comboBoxDateFormat.Name = "comboBoxDateFormat";
            this.comboBoxDateFormat.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // groupBoxTime
            // 
            this.groupBoxTime.Controls.Add(this.labelTimeExample);
            this.groupBoxTime.Controls.Add(this.label11);
            this.groupBoxTime.Controls.Add(this.comboBoxTimeSeparator);
            this.groupBoxTime.Controls.Add(this.label6);
            this.groupBoxTime.Controls.Add(this.comboBoxTimeFormat);
            this.groupBoxTime.Controls.Add(this.label9);
            resources.ApplyResources(this.groupBoxTime, "groupBoxTime");
            this.groupBoxTime.Name = "groupBoxTime";
            this.groupBoxTime.TabStop = false;
            // 
            // labelTimeExample
            // 
            resources.ApplyResources(this.labelTimeExample, "labelTimeExample");
            this.labelTimeExample.Name = "labelTimeExample";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // comboBoxTimeSeparator
            // 
            this.comboBoxTimeSeparator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTimeSeparator.FormattingEnabled = true;
            resources.ApplyResources(this.comboBoxTimeSeparator, "comboBoxTimeSeparator");
            this.comboBoxTimeSeparator.Name = "comboBoxTimeSeparator";
            this.comboBoxTimeSeparator.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // comboBoxTimeFormat
            // 
            this.comboBoxTimeFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTimeFormat.FormattingEnabled = true;
            this.comboBoxTimeFormat.Items.AddRange(new object[] {
            resources.GetString("comboBoxTimeFormat.Items"),
            resources.GetString("comboBoxTimeFormat.Items1")});
            resources.ApplyResources(this.comboBoxTimeFormat, "comboBoxTimeFormat");
            this.comboBoxTimeFormat.Name = "comboBoxTimeFormat";
            this.comboBoxTimeFormat.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // UserControlScaleConfigCountry
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxTime);
            this.Controls.Add(this.groupBoxDate);
            this.Controls.Add(this.comboBoxLanguage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxDaylightSaving);
            this.Controls.Add(this.comboBoxCountry);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label1);
            this.Name = "UserControlScaleConfigCountry";
            this.groupBoxDate.ResumeLayout(false);
            this.groupBoxDate.PerformLayout();
            this.groupBoxTime.ResumeLayout(false);
            this.groupBoxTime.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxDaylightSaving;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxCountry;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxLanguage;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxDate;
        private System.Windows.Forms.ComboBox comboBoxDateSeparator2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxDateSeparator1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxDateFormat;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBoxTime;
        private System.Windows.Forms.ComboBox comboBoxTimeSeparator;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxTimeFormat;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label labelDateExample;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelTimeExample;
        private System.Windows.Forms.Label label11;
    }
}
