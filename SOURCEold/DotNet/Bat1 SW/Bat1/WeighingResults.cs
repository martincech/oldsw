﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Scale;
using Veit.ScaleStatistics;

namespace Bat1 {
    /// <summary>
    /// Complete statistic results of one weighing
    /// </summary>
    public class WeighingResults {
        /// <summary>
        /// Sorted list of all flags in the weighing
        /// </summary>
        public  List<Flag> FlagList { get { return flagList; } }
        private List<Flag> flagList = new List<Flag>();
        
        /// <summary>
        /// List of results for all flags in the weighing
        /// </summary>
        public  List<StatisticResult> StatisticResultList { get { return statisticResultList; } }
        private List<StatisticResult> statisticResultList = new List<StatisticResult>();

        /// <summary>
        /// Constructor used with data downloaded from scales
        /// </summary>
        /// <param name="sampleList">List of samples</param>
        /// <param name="uniformityRange">Uniformity range</param>
        /// <param name="histogramStep">Histogram step</param>
        public WeighingResults(SampleList sampleList, int uniformityRange, HistogramConfig histogramConfig, Units units) {
            // Sestavim a setridim seznam flagu, ktere vazeni obsahuje
            sampleList.First();
            while (sampleList.Read()) {
                if (!flagList.Contains(sampleList.Sample.flag)) {
                    flagList.Add(sampleList.Sample.flag);
                }
            }
            flagList.Add(Flag.ALL);    // Na konec seznamu bez rozliseni flagu
            flagList.Sort();                // Na zaver setridim

            // Vypoctu statistiky zvlast pro vsechny flagy, nakonci seznamu flagu je i ALL, tj. vypoctou se i celkove vysledky
            foreach (Flag flag in flagList) {
                statisticResultList.Add(new StatisticResult(sampleList, flag, uniformityRange, histogramConfig, units));
            }
        }

        /// <summary>
        /// Constructor used with manually entered results
        /// </summary>
        /// <param name="statisticResultList">List of results</param>
        public WeighingResults(List<StatisticResult> statisticResultList) {
            // Zkopiruju si vsechny vysledky
            this.statisticResultList.AddRange(statisticResultList);

            // Sestavim a setridim seznam flagu, ktere vazeni obsahuje
            foreach (StatisticResult result in statisticResultList) {
                if (!flagList.Contains(result.Flag)) {
                    flagList.Add(result.Flag);
                }
            }
            flagList.Add(Flag.ALL);    // Na konec seznamu bez rozliseni flagu
            flagList.Sort();           // Na zaver setridim

            // Na zaver pridam celkovou statistiku pro vsechny flagy
            
            // Pokud je ve vazeni pouze jeden vysledek (tj. byl pouzity pouze 1 flag), zkopiruju jednoduse tento vysledek
            // jako celkovy vysledek, cimz ziskam vsechny udaje vcetne sigma, CV a uniformity.
            // Pokud je ve vazeni vice vysledku (vice flagu), muzu vypocitat jen celkovou prumernou hmotnost a pocet vzorku, vse ostatni vynuluju.
            if (statisticResultList.Count == 1) {
                // Jednoduse zkopiruju vysledky
                StatisticResult result = statisticResultList[0];
                this.statisticResultList.Add(new StatisticResult(Flag.ALL, result.Count, result.Average, result.Sigma, result.Cv, result.Uniformity, result.WeighingStarted));
                return;
            }

            // Ve vazeni je vice flagu, celkovy vysledek musim spocitat
            int count     = 0;
            float average = 0;
            DateTime weighingStarted = DateTime.MaxValue;
            foreach (StatisticResult result in statisticResultList) {
                count   += result.Count;
                average += result.ExactAverage * (float)result.Count;   // Sumuju prumerne hmotnosti, pouziju presny prumer
                if (result.WeighingStarted < weighingStarted) {
                    weighingStarted = result.WeighingStarted;
                }
            }
            if (count != 0) {
                average /= (float)count;
            }
            this.statisticResultList.Add(new StatisticResult(Flag.ALL, count, average, 0, 0, 0, weighingStarted));
        }

        /// <summary>
        /// Get result with specified flag
        /// </summary>
        /// <param name="flag">Flag</param>
        /// <returns>StatisticResult</returns>
        public StatisticResult GetResult(Flag flag) {
            if (!flagList.Contains(flag)) {
                // Tento flag zde neni
                return null;
            }
            foreach (StatisticResult result in statisticResultList) {
                if (result.Flag == flag) {
                    return result;
                }
            }
            return null;
        }

        /// <summary>
        /// Manually set weighing speed
        /// </summary>
        /// <param name="flag">Flag</param>
        /// <param name="weighingSpeed">New weighing speed</param>
        public void SetWeighingSpeed(Flag flag, double weighingSpeed) {
            GetResult(flag).WeighingSpeed = weighingSpeed;
        }


        /// <summary>
        /// Recalculate all results to new units. Used only for manual results.
        /// </summary>
        /// <param name="units">New units</param>
        public void ChangeUnits(Units oldUnits, Units newUnits) {
            foreach (StatisticResult result in statisticResultList) {
                result.ChangeUnits(oldUnits, newUnits);
            }
        }

    }
}
