﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat1 {
    
    /// <summary>
    /// Weighings table
    /// </summary>
    public class DatabaseTableWeighings : DatabaseTable {

        // Tabulka s jednotlivymi vazenimi stazenymi z vahy. Jedno vazeni odpovida vysledkum jednoho souboru
        // ve vaze. Pri kazdem stazeni jednoho nebo vice souboru z vahy se vytvori:
        //   - Jeden kompletni globalni config vahy, vcetne seznamu souboru, nastaveni souboru a skupin
        //   - Jeden zaznam v tabulce Weighings pro kazdy stazeny soubor. Jedno vazeni tak odpovida jednomu
        //     souboru ve vaze, vazeni se odkazuje na globalni config.
        // Pokud se z jedne vahy stahnou data nadvakrat (napr. nejprve nektere soubory, podruhe jine), vytvori
        // se 2 globalni configy, i kdyz budou shodne.

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableWeighings(DatabaseFactory factory)
            : base("Weighings", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            // Vytvorim tabulku
            Create("CREATE TABLE Weighings ("
                 + "WeighingId "         + factory.TypeLong      + " PRIMARY KEY AUTOINCREMENT,"
                 + "FileId "             + factory.TypeLong      + " NOT NULL,"
                 + "ScaleConfigId "      + factory.TypeLong      + " NOT NULL,"
                 + "ResultType "         + factory.TypeByte      + ","
                 + "RecordSource "       + factory.TypeByte      + ","
                 + "DownloadedDateTime " + factory.TypeTimestamp + ","
                 + "Note "               + factory.TypeNVarChar  + "(100),"         // Poznamka k vazeni v PC muze byt dlouha
                 + "SwMajorVersion "     + factory.TypeSmallInt  + ","              // Major cislo verze SW behem stahovani (1.xx.xx)
                 + "SwMinorVersion "     + factory.TypeSmallInt  + ","              // Minor cislo verze SW behem stahovani (x.01.xx)
                 + "SwBuildVersion "     + factory.TypeSmallInt  + ","              // Cislo buildu SW behem stahovani (x.xx.01)
                 + "SamplesMinDateTime " + factory.TypeTimestamp + ","              // Datum a cas prvniho vzorku (predpoctena hodnota z Samples)
                 + "SamplesMaxDateTime " + factory.TypeTimestamp + ","              // Datum a cas posledniho vzorku (predpoctena hodnota z Samples)
                 + "CONSTRAINT FK_P_Files FOREIGN KEY (FileId) REFERENCES Files(FileId),"
                 + "CONSTRAINT FK_P_ScaleConfigs FOREIGN KEY (ScaleConfigId) REFERENCES ScaleConfigs(ScaleConfigId)"
                 + ")");

            // Index na sloupec SamplesMinDateTime (podle nej se vysledky radi)
            using (DbCommand command = factory.CreateCommand("CREATE INDEX IDX_WeighingsSamplesMinDateTime ON Weighings (SamplesMinDateTime)")) {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Update table from version 7.0.0 to 7.0.1.0
        /// </summary>
        private void UpdateToVersion_7_0_1_0() {
            // Ve verzi 7.0.1.0 jsem pridal sloupec rozliseni zdroje vazeni (ulozeni v SW nebo import)
            if (ColumnExists("RecordSource")) {
                return;     // Neni treba update
            }

            // Pridam sloupec RecordSource
            AddColumn("RecordSource", factory.TypeByte);

            // Vyplnim ve vsech radcich RecordSource na SAVE (tj. ulozene v SW)
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET RecordSource = @RecordSource")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@RecordSource", RecordSource.SAVE));
                command.ExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// Update table definition
        /// </summary>
        public void Update() {
            UpdateToVersion_7_0_1_0();
        }

        /// <summary>
        /// Add new row
        /// </summary>
        /// <param name="fileId">File ID</param>
        /// <param name="scaleConfigId">ScaleConfig ID</param>
        /// <returns>Id of new item</returns>
        public long Add(long fileId, long scaleConfigId, ResultType resultType, RecordSource recordSource, string note, DateTime minDateTime, DateTime maxDateTime) {
            // Ulozim novou vahu
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " "
                                                           + "(WeighingId, FileId, ScaleConfigId, ResultType, "
                                                           + "RecordSource, DownloadedDateTime, Note, SwMajorVersion, "
                                                           + "SwMinorVersion, SwBuildVersion, SamplesMinDateTime, "
                                                           + "SamplesMaxDateTime) "
                                                           + "VALUES (NULL, @FileId, @ScaleConfigId, @ResultType, "
                                                           + "@RecordSource, @DownloadedDateTime, @Note, "
                                                           + "@SwMajorVersion, @SwMinorVersion, @SwBuildVersion, "
                                                           + "@SamplesMinDateTime, @SamplesMaxDateTime); "
                                                           + "SELECT last_insert_rowid()")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FileId",             fileId));
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId",      scaleConfigId));
                command.Parameters.Add(factory.CreateParameter("@ResultType",         resultType));
                command.Parameters.Add(factory.CreateParameter("@RecordSource",       recordSource));
                command.Parameters.Add(factory.CreateParameter("@DownloadedDateTime", DateTime.Now));
                command.Parameters.Add(factory.CreateParameter("@Note",               note));
                command.Parameters.Add(factory.CreateParameter("@SwMajorVersion",     SwVersion.MAJOR));
                command.Parameters.Add(factory.CreateParameter("@SwMinorVersion",     SwVersion.MINOR));
                command.Parameters.Add(factory.CreateParameter("@SwBuildVersion",     SwVersion.BUILD));
                command.Parameters.Add(factory.CreateParameter("@SamplesMinDateTime", minDateTime));
                command.Parameters.Add(factory.CreateParameter("@SamplesMaxDateTime", maxDateTime));
                return (long)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Delete weighing
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long weighingId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE WeighingId = @WeighingId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId", weighingId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Read Ids of file and scale config, result type and note
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>True when successful</returns>
        public bool ReadInfo(long weighingId, out long fileId, out long scaleConfigId, 
                             out ResultType resultType, out RecordSource recordSource, out string note) {
            using (DbCommand command = factory.CreateCommand("SELECT FileId, ScaleConfigId, ResultType, "
                                                           + "RecordSource, Note "
                                                           + "FROM " + tableName + " "
                                                           + "WHERE WeighingId = @WeighingId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId", weighingId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        // Vazeni neexistuje
                        fileId = scaleConfigId = 0;
                        resultType = ResultType.NEW_SCALE;
                        recordSource = RecordSource.SAVE;
                        note = "";
                        return false;
                    }
                    fileId        = (long)               reader["FileId"];
                    resultType    = (ResultType)(byte)   reader["ResultType"];
                    recordSource  = (RecordSource)(byte) reader["RecordSource"];
                    scaleConfigId = (long)               reader["ScaleConfigId"];
                    note          = (string)             reader["Note"];
                    return true;
                }
            }
        }

        /// <summary>
        /// Read Ids of file and scale config
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>True when successful</returns>
        public bool ReadIds(long weighingId, out long fileId, out long scaleConfigId) {
            ResultType resultType;
            RecordSource recordSource;
            string note;
            
            return ReadInfo(weighingId, out fileId, out scaleConfigId, out resultType, out recordSource, out note);
        }

        /// <summary>
        /// Read File ID
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>File Id or -1</returns>
        public long ReadFileId(long weighingId) {
            ResultType resultType;
            RecordSource recordSource;
            string note;
            long fileId;
            long scaleConfigId;
            
            if (!ReadInfo(weighingId, out fileId, out scaleConfigId, out resultType, out recordSource, out note)) {
                return -1;
            }

            return fileId;
        }

        /// <summary>
        /// Read list of all weighing IDs sorted by date/time of weighing start
        /// </summary>
        /// <returns>List of IDs</returns>
        public List<long> ReadIdList() {
            using (DbCommand command = factory.CreateCommand("SELECT WeighingId FROM " + tableName + " "
                                                           + "ORDER BY SamplesMinDateTime")) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    List<long> idList = new List<long>();
                    while (reader.Read()) {
                        idList.Add((long)reader["WeighingId"]);
                        
                    }
                    return idList;
                }
            }
        }

        /// <summary>
        /// Read list of all weighing IDs that share the specified config
        /// </summary>
        /// <param name="scaleConfigId"></param>
        /// <returns>List of IDs</returns>
        public List<long> ReadIdList(long scaleConfigId) {
            using (DbCommand command = factory.CreateCommand("SELECT WeighingId FROM " + tableName + " "
                                                           + "WHERE ScaleConfigId = @ScaleConfigId "
                                                           + "ORDER BY SamplesMinDateTime")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    List<long> idList = new List<long>();
                    while (reader.Read()) {
                        idList.Add((long)reader["WeighingId"]);
                        
                    }
                    return idList;
                }
            }
        }

        /// <summary>
        /// Check if specified file exists in any weighing
        /// </summary>
        /// <param name="fileId">File Id</param>
        /// <returns>True if exists</returns>
        public bool FileExists(long fileId) {
            return ValueExists("FileId", fileId);
        }

        /// <summary>
        /// Check if specified scale config exists in any weighing
        /// </summary>
        /// <param name="scaleConfigId">Scale config Id</param>
        /// <returns>True if exists</returns>
        public bool ScaleConfigExists(long scaleConfigId) {
            return ValueExists("ScaleConfigId", scaleConfigId);
        }

        /// <summary>
        /// Update weighing data
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <param name="note">New note</param>
        /// <param name="samplesMinDateTime">DateTime of first sample</param>
        /// <param name="samplesMaxDateTime">DateTime of last sample</param>
        public void Update(long weighingId, string note, DateTime samplesMinDateTime, DateTime samplesMaxDateTime) {
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "Note = @Note, "
                                                           + "SamplesMinDateTime = @SamplesMinDateTime, "
                                                           + "SamplesMaxDateTime = @SamplesMaxDateTime "
                                                           + "WHERE WeighingId = @WeighingId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId",         weighingId));
                command.Parameters.Add(factory.CreateParameter("@SamplesMinDateTime", samplesMinDateTime));
                command.Parameters.Add(factory.CreateParameter("@SamplesMaxDateTime", samplesMaxDateTime));
                command.Parameters.Add(factory.CreateParameter("@Note",               note));
                command.ExecuteNonQuery();
            }
        }

    }
}