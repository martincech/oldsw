﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Ionic.Utils.Zip;

namespace Bat1 {

    /// <summary>
    /// Export data to Bat1 format
    /// </summary>
    public class ExportBat1 {
        /// <summary>
        /// File name to save to
        /// </summary>
        private string fileName;

        /// <summary>
        /// Database used for export
        /// </summary>
        private Database database;

        private string databaseFileName;

        private string infoFileName;

        private const string EXPORT_WEIGHINGS = "WEIGHINGS";
        private const string EXPORT_CONFIG    = "SCALE CONFIG";

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">File name to save to</param>
        public ExportBat1(string fileName) {
            // Preberu data
            this.fileName = fileName;

            // Umisteni souboru
            databaseFileName = Program.TempFolder + @"\Export." + FileExtension.DATABASE;
            infoFileName     = Program.TempFolder + @"\Info.txt";
        }

        private void CreateTemporaryDatabase() {
            string tempFileName = Program.TempFolder + @"\ExportTemp." + FileExtension.DATABASE;    // Nasledne budu kopirovat jinam
            if (System.IO.File.Exists(tempFileName)) {
                System.IO.File.Delete(tempFileName);    // Pokud existuje, prepisu
            }
            database = new Database(tempFileName);
            database.CreateDatabase();      // Vytvorim prazdnou databazi
            database.CreateTables();        // Vytvorim vsechny tabulky

            // Ulozim do databaze exportu nastaveni programu, v budoucnu to mozna vyuziju
            database.SaveSetup();
        }
        
        private bool OpenDatabase() {
            database = new Database(databaseFileName);
            return database.Update();      // Pokud je import ze starsi verze, updatuju databazi
        }

        private void CreateInfoFile(string exportType) {
            if (System.IO.File.Exists(infoFileName)) {
                System.IO.File.Delete(infoFileName);    // Pokud existuje, prepisu
            }
            using (StreamWriter writer = new StreamWriter(new FileStream(infoFileName, FileMode.CreateNew), Encoding.UTF8)) {
                writer.WriteLine(DateTime.Now.ToString("s"));       // Datum a cas exportu, SortableDateTimePattern
                writer.WriteLine(SwVersion.ToString());       // Verze SW, ze ktereho export pochazi
                writer.WriteLine(exportType);                       // Typ exportu
                writer.Close();
            }
        }
        
        private bool ReadInfoFile(out DateTime exportDateTime, out string version, out string exportType) {
            if (!System.IO.File.Exists(infoFileName)) {
                // Info soubor neexistuje
                exportDateTime = DateTime.Now;
                version        = SwVersion.ToString();
                exportType     = "";
                return false;
            }
            using (StreamReader reader = new StreamReader(new FileStream(infoFileName, FileMode.Open), Encoding.UTF8)) {
                exportDateTime = DateTime.Parse(reader.ReadLine());
                version        = reader.ReadLine();
                exportType     = reader.ReadLine();
                reader.Close();
            }
            return true;
        }

        private void CopyDatabase() {
            if (System.IO.File.Exists(databaseFileName)) {
                System.IO.File.Delete(databaseFileName);    // Pokud existuje, prepisu
            }
            database.Copy(databaseFileName);
        }
        
        private void SaveExportFile() {
            if (System.IO.File.Exists(fileName)) {
                System.IO.File.Delete(fileName);    // Pokud existuje, prepisu
            }
            ZipFile zipFile = new ZipFile(fileName);
            zipFile.TempFileFolder = Path.GetTempPath();    // Standardni docasny adresar, jinak vytvari soubory v adresari EXE
            zipFile.AddFile(databaseFileName, "");          // Bez cesty
            zipFile.AddFile(infoFileName, "");              // Bez cesty
            zipFile.Save();
        }

        private void CreateExportFile(string exportType) {
            // Bezpecne zkopiruju databazi na jine misto
            CopyDatabase();
            
            // Vytvorim textovy soubor s informacemi o exportu
            CreateInfoFile(exportType);

            // Zazipuju do jednoho souboru
            SaveExportFile();
        }

        private bool ExtractAndCheckType(string type) {
            // Rozbalim obsah ZIPu
            ZipFile zipFile = new ZipFile(fileName);
            zipFile.TempFileFolder = Path.GetTempPath();    // Standardni docasny adresar, jinak vytvari soubory v adresari EXE
            zipFile.ExtractAll(Program.TempFolder, true);   // Do Temp adresare, vcetne prepisu

            // Nactu info soubor
            DateTime exportDateTime;
            string version;
            string exportType;
            if (!ReadInfoFile(out exportDateTime, out version, out exportType)) {
                return false;
            }

            // Zkontroluju, zda je v exportnim souboru ulozeny export, ktery ocekavam
            if (exportType != type) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Export list of weighings
        /// </summary>
        /// <param name="weighingList">Weighing list</param>
        public void ExportWeighingList(List<Weighing> weighingList) {
            // Vytvorim docasnou databazi pro exportovana data - pred exportem ji zkopiruju jinam
            CreateTemporaryDatabase();

            // Ulozim seznam vazeni do exportni DB - ukladam kazde vazeni s vlastnim configem
            foreach (Weighing weighing in weighingList) {
                database.SaveWeighing(weighing);
            }

            // Exportuju
            CreateExportFile(EXPORT_WEIGHINGS);
        }

        /// <summary>
        /// Import list of weighings
        /// </summary>
        /// <returns>Weighing list</returns>
        public List<Weighing> ImportWeighingList() {
            // Rozbalim obsah ZIPu a zkontroluju obsah exportu
            if (!ExtractAndCheckType(EXPORT_WEIGHINGS)) {
                return null;
            }

            // Nactu a vratim seznam vazeni z databaze exportu
            if (!OpenDatabase()) {
                // Import pochazi z novejsi verze SW, data nelze pouzit (DB by mohla mit jinou strukturu)
                // Spravne bych mel zobrazit upozorneni, ale zatim to nedelam
                return null;
            }
            return database.ReadWeighingIdList();
        }

        /// <summary>
        /// Export scale config
        /// </summary>
        /// <param name="scaleConfig">ScaleConfig to export</param>
        /// <param name="name">Predefined config name</param>
        public void ExportScaleConfig(ScaleConfig scaleConfig, string name) {
            // Vytvorim docasnou databazi pro exportovana data - pred exportem ji zkopiruju jinam
            CreateTemporaryDatabase();

            // Ulozim config do exportni DB
            database.SaveScaleConfig(scaleConfig, name);

            // Exportuju
            CreateExportFile(EXPORT_CONFIG);
        }

        /// <summary>
        /// Import scale config
        /// </summary>
        /// <returns>ScaleConfig or null</returns>
        public ScaleConfig ImportScaleConfig() {
            // Rozbalim obsah ZIPu a zkontroluju obsah exportu
            if (!ExtractAndCheckType(EXPORT_CONFIG)) {
                return null;
            }

            // Nactu a vratim config z databaze exportu
            if (!OpenDatabase()) {
                // Import pochazi z novejsi verze SW, data nelze pouzit (DB by mohla mit jinou strukturu)
                // Spravne bych mel zobrazit upozorneni, ale zatim to nedelam
                return null;
            }
            return database.LoadScaleConfig(1);     // Export obsahuje pouze 1 config s ID = 1
        }

    }
}
