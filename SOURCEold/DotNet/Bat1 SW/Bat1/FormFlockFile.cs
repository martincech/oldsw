﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormFlockFile : Form {
        private List<string> fileList;
        public List<string> selectedFileList;

        public FormFlockFile(List<string> fileList) {
            InitializeComponent();

            // Preberu seznam
            this.fileList = fileList;
            
            // Ulozim soubory do listboxu
            listBoxFiles.Items.Clear();
            foreach (string file in fileList) {
                listBoxFiles.Items.Add(file);
            }

            // Vyberu prvni soubor
            if (listBoxFiles.Items.Count > 0) {
                listBoxFiles.SelectedIndex = 0;
            }
        }

        private void FormFlockFile_Shown(object sender, EventArgs e) {
            // Nastavim focus
            listBoxFiles.Focus();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Sestavim seznam vybranych souboru
            selectedFileList = new List<string>();
            for (int i = 0; i < listBoxFiles.Items.Count; i++) {
                if (listBoxFiles.GetSelected(i)) {
                    selectedFileList.Add(fileList[i]);
                }
            }

            // Hotovo
            DialogResult = DialogResult.OK;
        }

        private void listBoxFiles_DoubleClick(object sender, EventArgs e) {
            if (listBoxFiles.SelectedIndex < 0) {
                return;
            }

            buttonOk_Click(null, null);
        }

    }
}
