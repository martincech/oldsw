﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlManualResults : UserControlResultsBase {
        enum WeighingType {
            NORMAL,
            MIXED_SEX,
            SORTING_2,
            SORTING_3
        }

        private Weighing editWeighing = null;

        public UserControlManualResults() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Predam combobox s vahami, soubor musim obsluhovat zvlast
            SetScaleComboBox(comboBoxScaleName);

            // Zacinam plnit data do controlu
            isLoading = true;

            try {
                // Nactu seznamy vah a souboru z databaze, zaroven se vyplni combobox vah
                LoadLists();

                // Vyplnim combobox souboru
                LoadFileComboBox();

                // Default zvolim normalni vazeni
                comboBoxFlag.SelectedIndex = (int)WeighingType.NORMAL;
                SetWeighingType(WeighingType.NORMAL);
                
                // Nahraju z databaze naposledy pouzite jednotky - staci jednou pri startu programu
                LoadUnits();
            } finally {
                isLoading = false;
            }
        }

        public void Save() {
            // Vaha
            string scaleName;
            if (!CheckValue.GetScaleName(comboBoxScaleName, out scaleName)) {
                return;
            }

            // Soubor
            string name;
            if (!CheckValue.GetScaleName(comboBoxFileName, out name)) {
                return;
            }
            NameNote file = new NameNote(name, textBoxFileNote.Text);
            
            // Start vazeni
            DateTime startDateTime = new DateTime(dateTimePickerStartDate.Value.Year, dateTimePickerStartDate.Value.Month, dateTimePickerStartDate.Value.Day,
                                                  dateTimePickerStartTime.Value.Hour, dateTimePickerStartTime.Value.Minute, dateTimePickerStartTime.Value.Second);

            // Poznamka
            string note = textBoxNote.Text;

            // Jednotky
            Units units = (Units)comboBoxUnits.SelectedIndex;
            
            // Statistika
            List<StatisticResult> resultList;
            if (!GetResults(out resultList, units)) {
                return;
            }

            if (editWeighing == null) {
                // Zkontroluju, zda uz vazeni v databazi neexistuje (kombinace soubor - start vazeni)
                if (CanSaveWeighing(file.Name, startDateTime) != DialogResult.Yes) {
                    return;     // Vazeni uz existuje a nechce ho prepsat
                }
            } else {
                // Pokud edituje vazeni, nejprve smazu z databaze puvodni vazeni
                Program.Database.DeleteWeighing(editWeighing.WeighingData.Id);
            }
            
            // Ulozim vazeni do databaze
            Program.Database.SaveWeighing(scaleName, file, RecordSource.SAVE, startDateTime, note, units, resultList);

            // Ulozim do databaze posledne pouzite jednotky
            SaveUnits();

            // Pokud zadal novou vahu, musim ji pridat do seznamu
            AddScale(scaleName);

            // Pokud zadal novy soubor, musim ho pridat do seznamu
            AddFile(file);

            // Novy seznam souboru musim nahrat rucne
            string oldText = comboBoxFileName.Text;
            LoadFileComboBox();
            comboBoxFileName.Text = oldText;           // Obnovim vybranou polozku

            // Zobrazim info, ze se ulozilo (pouze pokud zaklada nove vazeni, tj. ne pri editaci)
            if (editWeighing == null) {
                MessageBox.Show(Properties.Resources.WEIGHING_SAVED, Program.ApplicationName);
            }
        }

        public void LoadWeighing(Weighing weighing) {
            // Ulozim si
            editWeighing = weighing;

            // Vaha
            comboBoxScaleName.Text = weighing.WeighingData.ScaleConfig.ScaleName;

            // Soubor
            comboBoxFileName.Text = weighing.WeighingData.File.Name;
            textBoxFileNote.Text  = weighing.WeighingData.File.Note;
            
            // Start vazeni
            dateTimePickerStartDate.Value = weighing.GetMinDateTime().Date;
            dateTimePickerStartTime.Value = weighing.GetMinDateTime();

            // Poznamka
            textBoxNote.Text = weighing.WeighingData.Note;

            // Jednotky
            Units units = weighing.WeighingData.ScaleConfig.Units.Units;
            comboBoxUnits.SelectedIndex = (int)units;
            
            // Statistika podle flagu, ktere vazeni obsahuje
            if (weighing.WeighingResults.FlagList.Contains(Flag.FEMALE)) {
                // Manual by sex
                SetWeighingType(WeighingType.MIXED_SEX);
                comboBoxFlag.SelectedIndex = (int)WeighingType.MIXED_SEX;
                userControlWeighingResultMales.SetResult(  weighing.WeighingResults.StatisticResultList[0], units);
                userControlWeighingResultFemales.SetResult(weighing.WeighingResults.StatisticResultList[1], units);
            } else if (weighing.WeighingResults.FlagList.Contains(Flag.OK)) {
                // Sorting into 3 groups
                SetWeighingType(WeighingType.SORTING_3);
                comboBoxFlag.SelectedIndex = (int)WeighingType.SORTING_3;
                userControlWeighingResultLight.SetResult(weighing.WeighingResults.StatisticResultList[0], units);
                userControlWeighingResultOk.SetResult(   weighing.WeighingResults.StatisticResultList[1], units);
                userControlWeighingResultHeavy.SetResult(weighing.WeighingResults.StatisticResultList[2], units);
            } else if (weighing.WeighingResults.FlagList.Contains(Flag.LIGHT)) {
                // Sorting into 2 groups
                SetWeighingType(WeighingType.SORTING_2);
                comboBoxFlag.SelectedIndex = (int)WeighingType.SORTING_2;
                userControlWeighingResultLight.SetResult(weighing.WeighingResults.StatisticResultList[0], units);
                userControlWeighingResultHeavy.SetResult(weighing.WeighingResults.StatisticResultList[1], units);
            } else {
                // Normal
                SetWeighingType(WeighingType.NORMAL);
                comboBoxFlag.SelectedIndex = (int)WeighingType.NORMAL;
                userControlWeighingResultNormal.SetResult(weighing.WeighingResults.StatisticResultList[0], units);
            }
        }

        private void SetWeighingType(WeighingType type) {
            tabControlResults.TabPages.Clear();
            switch (type) {
                case WeighingType.NORMAL:
                    tabControlResults.TabPages.Add(tabPageResults);
                    break;

                case WeighingType.MIXED_SEX:
                    tabControlResults.TabPages.Add(tabPageMales);
                    tabControlResults.TabPages.Add(tabPageFemales);
                    break;

                case WeighingType.SORTING_2:
                    tabControlResults.TabPages.Add(tabPageLight);
                    tabControlResults.TabPages.Add(tabPageHeavy);
                    break;

                case WeighingType.SORTING_3:
                    tabControlResults.TabPages.Add(tabPageLight);
                    tabControlResults.TabPages.Add(tabPageOk);
                    tabControlResults.TabPages.Add(tabPageHeavy);
                    break;
            }
        }

        /// <summary>
        /// Load last used units from database
        /// </summary>
        private void LoadUnits() {
            comboBoxUnits.SelectedIndex = (int)Program.Setup.ManualUnits;
        }

        /// <summary>
        /// Save selected units to database
        /// </summary>
        private void SaveUnits() {
            Program.Setup.ManualUnits = (Units)comboBoxUnits.SelectedIndex;
            Program.Database.SaveSetup();
        }

        private void LoadFileComboBox() {
            comboBoxFileName.Items.Clear();
            foreach (NameNote file in fileList) {
                comboBoxFileName.Items.Add(file.Name);
            }
        }

        private bool AddResult(UserControlWeighingResult control, List<StatisticResult> resultList, Units units, Flag flag,
                               TabControl tabControl, TabPage tabPage) {
            StatisticResult result;
            if (!control.GetResult(units, flag, out result, tabControl, tabPage)) {
                return false;
            }
            resultList.Add(result);
            return true;
        }

        private bool GetResults(out List<StatisticResult> resultList, Units units) {
            resultList = new List<StatisticResult>();
            
            // Statistika podle vybraneho typu vazeni
            switch ((WeighingType)comboBoxFlag.SelectedIndex) {
                case WeighingType.NORMAL:
                    if (!AddResult(userControlWeighingResultNormal, resultList, units, Flag.NONE, tabControlResults, tabPageResults)) {
                        return false;
                    }
                    break;

                case WeighingType.MIXED_SEX:
                    if (!AddResult(userControlWeighingResultMales, resultList, units, Flag.MALE, tabControlResults, tabPageMales)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultFemales, resultList, units, Flag.FEMALE, tabControlResults, tabPageFemales)) {
                        return false;
                    }
                    break;

                case WeighingType.SORTING_2:
                    if (!AddResult(userControlWeighingResultLight, resultList, units, Flag.LIGHT, tabControlResults, tabPageLight)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultHeavy, resultList, units, Flag.HEAVY, tabControlResults, tabPageHeavy)) {
                        return false;
                    }
                    break;

                case WeighingType.SORTING_3:
                    if (!AddResult(userControlWeighingResultLight, resultList, units, Flag.LIGHT, tabControlResults, tabPageLight)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultOk, resultList, units, Flag.OK, tabControlResults, tabPageOk)) {
                        return false;
                    }
                    if (!AddResult(userControlWeighingResultHeavy, resultList, units, Flag.HEAVY, tabControlResults, tabPageHeavy)) {
                        return false;
                    }
                    break;
            }

            return true;
        }
        
        private void comboBoxScaleName_KeyPress(object sender, KeyPressEventArgs e) {
            e.KeyChar = Char.ToUpper(e.KeyChar);
            if (!KeyFilter.IsScaleChar(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void comboBoxFileName_TextChanged(object sender, EventArgs e) {
            if (isLoading) {
                return;         // Data se nahravaji, nevykresluju
            }

            // Pokusim se nacist z databaze poznamku k prave zadanemu souboru
            textBoxFileNote.Text = fileList.GetNote(comboBoxFileName.Text);
        }

        private void comboBoxFlag_SelectionChangeCommitted(object sender, EventArgs e) {
            SetWeighingType((WeighingType)comboBoxFlag.SelectedIndex);
            comboBoxFlag.Focus();     // Vratim focus
        }
    }
}
