﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat1 {
    public class DatabaseTableSelectedFlocks : DatabaseTable {

        // Tabulka se seznamem vybranych hejn, aby je uzivatel nemusel vzdy vybirat znovu.

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableSelectedFlocks(DatabaseFactory factory)
            : base("SelectedFlocks", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "FlockId " + factory.TypeLong + " PRIMARY KEY,"  // ID hejna
                 + "CONSTRAINT FK_P_Flocks FOREIGN KEY (FlockId) REFERENCES Flocks(FlockId)"
                 + ")");
        }

        /// <summary>
        /// Delete flock
        /// </summary>
        /// <param name="flockId">Flock Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long flockId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE FlockId = @FlockId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FlockId", flockId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Save list of selected flocks
        /// </summary>
        /// <param name="flockDefinitionList">Flock list</param>
        public void Save(FlockDefinitionList flockDefinitionList) {
            Clear();
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (@FlockId)")) {
                foreach (FlockDefinition flockDefinition in flockDefinitionList.List) {
                    command.Parameters.Clear();
                    command.Parameters.Add(factory.CreateParameter("@FlockId", flockDefinition.Id));
                    command.ExecuteNonQuery();
                }
            }
        }
    
    }
}
