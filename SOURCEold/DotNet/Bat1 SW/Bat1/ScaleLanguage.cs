﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// List of unique language codes used in the scale. These codes are used only for saving the scale language
    /// to the DB. The order of languages corresponds to scale version 7.01.x and must NOT be changed. New languages
    /// in higher scale versions are always added to the end of the list. Thanks to this, the list of languages
    /// stays always compatible with older SW versions.
    /// </summary>
    public enum ScaleLanguagesInDatabase {
        CZECH,
        DUTCH,
        ENGLISH,
        FINNISH,
        FRENCH,
        GERMAN,
        JAPANESE,
        PORTUGUESE,
        RUSSIAN,
        SPANISH,
        TURKISH,        // Tady konci puvodni verze vahy 7.00.0 a 7.01.0 (obe maji shodne jazyky)
        HUNGARIAN,      // Madarstina pridana ve vaze 7.02.0
        POLISH,         // Polstina pridana ve vaze 7.03.0
        ITALIAN         // Italstina pridana ve vaze 7.04.0
    }

    public static class ScaleLanguage {

        /// <summary>
        /// Total number of languages in the newest scale version
        /// </summary>
        private const int LANGUAGES_COUNT = 14;
        
        /// <summary>
        /// Languages used in various versions of the scale, sorted as in the scale. Minor scale version is the first dimension
        /// of the array. Undefined languages are filled with English.
        /// </summary>
        private static ScaleLanguagesInDatabase[,] languages = new ScaleLanguagesInDatabase[5, LANGUAGES_COUNT]
            {
               // Vaha 7.00.x
               {ScaleLanguagesInDatabase.CZECH,
                ScaleLanguagesInDatabase.DUTCH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.FINNISH,
                ScaleLanguagesInDatabase.FRENCH,
                ScaleLanguagesInDatabase.GERMAN,
                ScaleLanguagesInDatabase.JAPANESE,
                ScaleLanguagesInDatabase.PORTUGUESE,
                ScaleLanguagesInDatabase.RUSSIAN,
                ScaleLanguagesInDatabase.SPANISH,
                ScaleLanguagesInDatabase.TURKISH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.ENGLISH },

               // Vaha 7.01.x - stejna jako 7.00.x
               {ScaleLanguagesInDatabase.CZECH,
                ScaleLanguagesInDatabase.DUTCH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.FINNISH,
                ScaleLanguagesInDatabase.FRENCH,
                ScaleLanguagesInDatabase.GERMAN,
                ScaleLanguagesInDatabase.JAPANESE,
                ScaleLanguagesInDatabase.PORTUGUESE,
                ScaleLanguagesInDatabase.RUSSIAN,
                ScaleLanguagesInDatabase.SPANISH,
                ScaleLanguagesInDatabase.TURKISH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.ENGLISH },

               // Vaha 7.02.x
               {ScaleLanguagesInDatabase.CZECH,
                ScaleLanguagesInDatabase.DUTCH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.FINNISH,
                ScaleLanguagesInDatabase.FRENCH,
                ScaleLanguagesInDatabase.GERMAN,
                ScaleLanguagesInDatabase.HUNGARIAN,     // Ve vaze 7.02.x pridana madarstina
                ScaleLanguagesInDatabase.JAPANESE,
                ScaleLanguagesInDatabase.PORTUGUESE,
                ScaleLanguagesInDatabase.RUSSIAN,
                ScaleLanguagesInDatabase.SPANISH,
                ScaleLanguagesInDatabase.TURKISH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.ENGLISH },

               // Vaha 7.03.x
               {ScaleLanguagesInDatabase.CZECH,
                ScaleLanguagesInDatabase.DUTCH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.FINNISH,
                ScaleLanguagesInDatabase.FRENCH,
                ScaleLanguagesInDatabase.GERMAN,
                ScaleLanguagesInDatabase.HUNGARIAN,
                ScaleLanguagesInDatabase.JAPANESE,
                ScaleLanguagesInDatabase.POLISH,        // Ve vaze 7.03.x pridana polstina
                ScaleLanguagesInDatabase.PORTUGUESE,
                ScaleLanguagesInDatabase.RUSSIAN,
                ScaleLanguagesInDatabase.SPANISH,
                ScaleLanguagesInDatabase.TURKISH,
                ScaleLanguagesInDatabase.ENGLISH },

               // Vaha 7.04.x
               {ScaleLanguagesInDatabase.CZECH,
                ScaleLanguagesInDatabase.DUTCH,
                ScaleLanguagesInDatabase.ENGLISH,
                ScaleLanguagesInDatabase.FINNISH,
                ScaleLanguagesInDatabase.FRENCH,
                ScaleLanguagesInDatabase.GERMAN,
                ScaleLanguagesInDatabase.HUNGARIAN,
                ScaleLanguagesInDatabase.ITALIAN,        // Ve vaze 7.04.x pridana italstina
                ScaleLanguagesInDatabase.JAPANESE,
                ScaleLanguagesInDatabase.POLISH,  
                ScaleLanguagesInDatabase.PORTUGUESE,
                ScaleLanguagesInDatabase.RUSSIAN,
                ScaleLanguagesInDatabase.SPANISH,
                ScaleLanguagesInDatabase.TURKISH }
            };

        /// <summary>
        /// Convert language code from database to language code used in the scale of specified version.
        /// </summary>
        /// <param name="languageInDatabase">Language code used in the DB</param>
        /// <param name="minorScaleVersion">Minor scale version</param>
        /// <returns>Language code used in the scale</returns>
        public static int ConvertToScale(ScaleLanguagesInDatabase languageInDatabase, int minorScaleVersion) {
            // Projedu pole jazyku odpovidajici cislu verze vahy a hledam pozadovany jazyk
            for (int i = 0; i < LANGUAGES_COUNT; i++) {
                if (languages[minorScaleVersion, i] == languageInDatabase) {
                    return i;       // Nasel jsem, vratim pozici jazyka ve vaze
                }
            }

            // Pokud dosel az sem, jazyk ve vaze neni definovan. Vratim index anglictiny, volam rekurzivne tuto fci.
            return ConvertToScale(ScaleLanguagesInDatabase.ENGLISH, minorScaleVersion);
        }

        /// <summary>
        /// Convert language code from scale of specified version to language code used in the database.
        /// </summary>
        /// <param name="languageInScale">Language code used in the scale</param>
        /// <param name="minorScaleVersion">Minor scale version</param>
        /// <returns>Language code used in the DB</returns>
        public static ScaleLanguagesInDatabase ConvertToDatabase(int languageInScale, int minorScaleVersion) {
            if (languageInScale >= LANGUAGES_COUNT) {
                return ScaleLanguagesInDatabase.ENGLISH;
            }

            // Pokud je minorScaleVersion pripojene vahy novejsi nez verze SW, hodilo by to vyjimku (pole languages[] neni pro minorScaleVersion definovano)
            if (minorScaleVersion > ScaleVersion.MINOR) {
                return ScaleLanguagesInDatabase.ENGLISH;
            }

            // Vratim jazyk z pole odpovidajici cislu verze vahy
            return languages[minorScaleVersion, languageInScale];
        }

        /// <summary>
        /// Convert language code from one scale version (source) to another scale version (target)
        /// </summary>
        /// <param name="sourceLanguage">Language in the source scale</param>
        /// <param name="sourceScaleVersion">Version of the source scale</param>
        /// <param name="targetScaleVersion">Version of the target scale</param>
        /// <returns>Language in the target scale</returns>
        public static int ConvertFromScaleToScale(int sourceLanguage, ScaleConfigVersion sourceScaleVersion, ScaleConfigVersion targetScaleVersion) {
            // Pokud se verze rovnaji, neni treba zadny prepocet
            if (sourceScaleVersion.Minor == targetScaleVersion.Minor) {
                return sourceLanguage;
            }

            // Prevedu kod jazyka na jazyk pouzivany v DB
            ScaleLanguagesInDatabase languageInDatabase = ConvertToDatabase(sourceLanguage, sourceScaleVersion.Minor);

            // Jazyk prevedu zpet na kod pouzity v cilove vaze
            return ConvertToScale(languageInDatabase, targetScaleVersion.Minor);
        }

    }
}
