﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat1 {
    
    /// <summary>
    /// Samples table
    /// </summary>
    public class DatabaseTableSamples : DatabaseTable {

        // Tabulka se vzorky v ulozenych vazenich.

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableSamples(DatabaseFactory factory)
            : base("Samples", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "WeighingId "    + factory.TypeInteger   + ","       // Id vazeni, ke kteremu vzorek patri
                 + "Weight "        + factory.TypeFloat     + ","       // Hmotnost
                 + "Flag "          + factory.TypeByte      + ","       // Flag
                 + "SavedDateTime " + factory.TypeTimestamp + ","       // Datum a cas ulozeni vzorku
                 + "CONSTRAINT FK_P_Weighings FOREIGN KEY (WeighingId) REFERENCES Weighings(WeighingId)"
                 + ")");

            // Index - zatim ne
/*            using (DbCommand command = factory.CreateCommand("CREATE INDEX IDX_SamplesSaved ON Samples (SavedDateTime)")) {
                command.ExecuteNonQuery();
            }*/
        }

        /// <summary>
        /// Add new row
        /// </summary>
        /// <param name="weighingId">Weighing ID</param>
        /// <param name="weight">Weight</param>
        /// <param name="flag">Flag</param>
        /// <param name="savedDateTime">Saved timestamp</param>
        public void Add(long weighingId, double weight, short flag, DateTime savedDateTime) {
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (@WeighingId, @Weight, @Flag, @SavedDateTime)")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId",    (int)weighingId));         // Id vazeni ukladam jako 32bit integer kvuli mistu
                command.Parameters.Add(factory.CreateParameter("@Weight",        weight));
                command.Parameters.Add(factory.CreateParameter("@Flag",          flag));
                command.Parameters.Add(factory.CreateParameter("@SavedDateTime", savedDateTime));
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Delete samples
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long weighingId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE WeighingId = @WeighingId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId", weighingId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Load samples form database
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>SampleList instance</returns>
        public SampleList Load(long weighingId) {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE WeighingId = @WeighingId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingId", weighingId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.HasRows) {
                        throw new Exception(String.Format("Samples with weighing Id {0} don't exist", weighingId));
                    }
                    SampleList sampleList = new SampleList();
                    while (reader.Read()) {
                        Sample sample;
                        sample.weight   = (float)      reader["Weight"];
                        sample.flag     = (Flag)(byte) reader["Flag"];
                        sample.dateTime = (DateTime)   reader["SavedDateTime"];
                        sampleList.Add(sample);
                    }
                    sampleList.SortByTime();  // Setridim podle data, v databazi muzou byt vzorky zprehazene

                    return sampleList;
                }
            }
        }
    }
}