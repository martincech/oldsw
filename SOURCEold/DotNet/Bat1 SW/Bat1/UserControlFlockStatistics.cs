﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Bat1 {
    public partial class UserControlFlockStatistics : UserControl {
        private FlockDefinitionList selectedFlockList;

        // Prave se nahravaji data do seznamu, neobnovuju zobrazeni
        private bool isLoading = false;

        public UserControlFlockStatistics(bool selectedFlocksMode) {
            InitializeComponent();

            // Skryju nebo zobrazim tlacitko pro rucni vyber, zaroven nastavim text tlacitka
            userControlStatisticsTop.ButtonSelect.Visible = selectedFlocksMode;
            if (selectedFlocksMode) {
                userControlStatisticsTop.ButtonSelect.Text = Properties.Resources.SELECT_FLOCKS + "...";
            }

            // Zobrazim casovy filtr
            userControlStatisticsTop.ShowTimeFilter(selectedFlocksMode);

            // Zaregistruju eventy
            if (selectedFlocksMode) {
                userControlStatisticsTop.ButtonSelect.Click                += new EventHandler(buttonSelect_Click);
                userControlStatisticsTop.DateTimePickerFrom.ValueChanged   += new EventHandler(dateTimePickerFrom_ValueChanged);
                userControlStatisticsTop.DateTimePickerFrom.EnabledChanged += new EventHandler(dateTimePickerFrom_ValueChanged);
                userControlStatisticsTop.DateTimePickerTo.ValueChanged     += new EventHandler(dateTimePickerTo_ValueChanged);
                userControlStatisticsTop.DateTimePickerTo.EnabledChanged   += new EventHandler(dateTimePickerTo_ValueChanged);
            }
            userControlStatisticsTop.ComboBoxFlag.SelectionChangeCommitted += new EventHandler(comboBoxFlag_SelectionChangeCommitted);

            // Default casovy filtr zakazu a datumy nastavim na dnesek
            isLoading = true;       // Zakazu obnovu zobrazeni
            try {
                userControlStatisticsTop.DateTimePickerFrom.Value   = DateTime.Now;
                userControlStatisticsTop.DateTimePickerFrom.Checked = false;
                userControlStatisticsTop.DateTimePickerTo.Value     = DateTime.Now;
                userControlStatisticsTop.DateTimePickerTo.Checked   = false;
            } finally {
                isLoading = false;          // Data uz jsou nahrana
            }
        }

        private void RedrawTimeGraph() {
            if (userControlStatisticsTable.FlockList == null) {
                return;
            }
            userControlGraphs.SetData(userControlStatisticsTable.FlockList, userControlStatisticsTable.DisplayedFlag);
        }

        public void RedrawCurrentFlocks() {
            // Prekreslim statistiku i graf
            userControlStatisticsTable.RedrawCurrentFlocks();
            RedrawTimeGraph();

            // Naplnim seznam flagu, default vybere ALL
            userControlStatisticsTop.FillFlags(userControlStatisticsTable.WeighingList);
        }

        /// <summary>
        /// Reload definition of selected flocks from the database
        /// </summary>
        public void ReloadSelectedFlocks() {
            selectedFlockList = Program.Database.LoadSelectedFlocks();
        }
        
        public void RedrawSelectedFlocks() {
            // Pokud uz ma vybrana nejaka hejna, ulozim si casove omezeni
            DateTime from = DateTime.MinValue;
            DateTime to   = DateTime.MaxValue;
            if (selectedFlockList.List.Count > 0) {
                // Casove omezeni jen pokud vybral nejaka hejna, jinak necham bez omezeni
                from = userControlStatisticsTop.DateTimePickerFrom.Checked ? userControlStatisticsTop.DateTimePickerFrom.Value : DateTime.MinValue;
                to   = userControlStatisticsTop.DateTimePickerTo.Checked   ? userControlStatisticsTop.DateTimePickerTo.Value   : DateTime.MaxValue;
                // Umazu cas, ponecham jen datum
                from = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0);
                to   = new DateTime(to.Year, to.Month, to.Day, 23, 59, 59);
            }
            
            // Prekreslim statistiku i graf
            userControlStatisticsTable.RedrawStatistics(selectedFlockList, from, to);
            RedrawTimeGraph();

            // Naplnim seznam flagu, default vybere ALL
            userControlStatisticsTop.FillFlags(userControlStatisticsTable.WeighingList);

            // Zobrazim casovy filtr - musim az tady, protoze teprve po zavolani userControlStatisticsTable.RedrawStatistics() se vyplni
            // prave zobrazeny flag v userControlStatisticsTable.
            isLoading = true;       // Zakazu obnovu zobrazeni
            try {
                // Pokud je casovy filtr zaskrtnuty (aktivni), ponecham zadany datum beze zmeny
                // Pokud je casovy filtr zakazany (neaktivni), vyplnim datumy podle vybranych hejn
                if (!userControlStatisticsTop.DateTimePickerFrom.Checked) {
                    DateTime minDateTime = userControlStatisticsTable.WeighingList.GetMinDateTime(userControlStatisticsTable.DisplayedFlag);
                    userControlStatisticsTop.DateTimePickerFrom.Value = minDateTime == DateTime.MaxValue ? DateTime.Now : minDateTime;
                    userControlStatisticsTop.DateTimePickerFrom.Checked = false;        // Zmenou data se automaticky zaskrtne, musim odskrtnout
                }
                if (!userControlStatisticsTop.DateTimePickerTo.Checked) {
                    DateTime maxDateTime = userControlStatisticsTable.WeighingList.GetMaxDateTime(userControlStatisticsTable.DisplayedFlag);
                    userControlStatisticsTop.DateTimePickerTo.Value = maxDateTime == DateTime.MinValue ? DateTime.Now : maxDateTime;
                    userControlStatisticsTop.DateTimePickerTo.Checked = false;          // Zmenou data se automaticky zaskrtne, musim odskrtnout
                }
            } finally {
                isLoading = false;          // Data uz jsou nahrana
            }

        }

        private void buttonSelect_Click(object sender, EventArgs e) {
            FormSelectFlock form = new FormSelectFlock();
            if (userControlStatisticsTable.FlockList != null) {
                form.LoadSelectedFlocks(userControlStatisticsTable.FlockList);     // Naplnim dosud vybrana hejna
            }
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }
            
            isLoading = true;       // Zakazu obnovu zobrazeni
            
            try {
                // Prekreslim okno, jinak je to hnusne
                this.ParentForm.Refresh();

                // Ulozim si
                selectedFlockList = form.SelectedFlockList;

                // Ulozim vybrany seznam hejn do DB
                Program.Database.SaveSelectedFlocks(selectedFlockList);
                
                // Prekreslim
                RedrawSelectedFlocks();
            } finally {
                isLoading = false;          // Data uz jsou nahrana
            }
        }

        private void dateTimePickerFrom_ValueChanged(object sender, EventArgs e) {
            if (isLoading) {
                // Seznamy se obnovuji, nezobrazuju
                return;
            }

            // Zobrazim vybrana hejna
            RedrawSelectedFlocks();
        }

        private void dateTimePickerTo_ValueChanged(object sender, EventArgs e) {
            if (isLoading) {
                // Seznamy se obnovuji, nezobrazuju
                return;
            }

            // Zobrazim vybrana hejna
            RedrawSelectedFlocks();
        }

        private void comboBoxFlag_SelectionChangeCommitted(object sender, EventArgs e) {
            userControlStatisticsTable.SetFlag(userControlStatisticsTop.ComboBoxFlag.SelectedIndex);
            RedrawTimeGraph();
        }

        private void buttonPrintGraph_Click(object sender, EventArgs e) {
            PrintDocument printDocument = userControlGraphs.Graph.PrintDocument;
            printDocument.PrinterSettings = Program.PrinterSettings;    // Preberu globalni parametry tiskarny
			PrintDialog printDialog = new PrintDialog();
			printDialog.Document = printDocument;
			if (printDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
                
            try {
                // Docasne nastavim graf na tisk
                string title = "";
                if (userControlStatisticsTable.DisplayedFlag != Flag.ALL) {
                    title = Properties.Resources.SEX_LIMIT + ": " + Sample.FlagToString(userControlStatisticsTable.DisplayedFlag);
                }
                userControlGraphs.SetForPrinting(title);

                printDocument.DefaultPageSettings.Landscape = true;     // Na sirku
			    printDocument.Print();
            } finally {
                // Nastavim zpet na monitor
                userControlGraphs.SetForScreen();
            }
        }

        private void buttonExportGraph_Click(object sender, EventArgs e) {
            userControlGraphs.Graph.SaveAs();
        }

    }
}
