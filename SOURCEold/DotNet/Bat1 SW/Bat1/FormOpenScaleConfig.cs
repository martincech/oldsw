﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormOpenScaleConfig : Form {
        /// <summary>
        /// Name of the selected config
        /// </summary>
        public string SelectedConfigName { get { return listBoxNames.Text; } }
        
        public FormOpenScaleConfig() {
            InitializeComponent();

            // Nahraju jmena z databaze
            RedrawList();
        }

        private void RedrawList() {
            listBoxNames.Items.Clear();
            List<string> list = Program.Database.LoadPredefinedScaleConfigNameList();
            list.Sort();        // Seznam zobrazim setrideny
            foreach (string name in list) {
                listBoxNames.Items.Add(name);
            }
        }

        private void FormLoadScaleConfig_Shown(object sender, EventArgs e) {
            // Prejdu na listbox
            listBoxNames.Focus();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            if (listBoxNames.SelectedIndex < 0) {
                return;
            }

            DialogResult = DialogResult.OK;
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            if (listBoxNames.SelectedIndex < 0) {
                return;
            }

            if (MessageBox.Show(String.Format(Properties.Resources.DELETE_TEMPLATE, listBoxNames.Text), Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            Program.Database.DeleteScaleConfig(listBoxNames.Text);
            RedrawList();
        }

    }
}
