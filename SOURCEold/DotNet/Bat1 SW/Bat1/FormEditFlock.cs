﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormEditFlock : Form {
        /// <summary>
        /// Edited flock definition
        /// </summary>
        public FlockDefinition FlockDefinition { get { return flockDefinition; } }
        private FlockDefinition flockDefinition;
        
        /// <summary>
        /// List of file names defined in the flock
        /// </summary>
        private List<string> flockFileNameList;

        private List<string> fileList;

        private CurveList curveList;

        private bool isMoreShown = true;

        public FormEditFlock(FlockDefinition flockDefinition) {
            InitializeComponent();

            // Puvodne mel kazdy soubor svoje vlastni casove obdobi, tj. v ramci hejna slo preskakovat z jednoho
            // souboru na jiny. Nakonec jsem to zrusil, takze hejno ma vzdy jen jedno globalni casove obdobi platne
            // pro vsechny soubory v hejnu. Interne (datove struktury, databaze, vypocty ve statistice) stale pracuji
            // tak, ze je preskakovani souboru mozne (co kdyby to do budoucna nekdo chtel). Pouze zde pri editaci hejna
            // pracuji tak, ze vsem souborum nastavim stejne casove obdobi, takze se ve statistice chovaji jakoby
            // preskakovani nebylo mozne. Kod je zde proto zbytecne slozity.
            
            // Preberu data, pracuju s kopii (kopiruju i ID)
            this.flockDefinition = new FlockDefinition(flockDefinition, true);

            // Preberu seznam jmen souboru, ten budu editovat zvlast
            flockFileNameList = new List<string>();
            foreach (FlockFile flockFile in flockDefinition.FlockFileList) {
                flockFileNameList.Add(flockFile.FileName);
            }

            // Nahraju seznam dostupnych souboru z databaze
            fileList = Program.Database.LoadFileNameList();
            
            // Nahraju krivky z databaze
            LoadCurves();

            // Nastavim nazev okna na jmeno hejna
            Text = Properties.Resources.FLOCK + ": " + flockDefinition.Name;

            // Vykreslim hejno
            
            // Datum zahajeni vezmu z prvniho souboru v hejnu (pro budouci pouziti maji casove obdobi
            // vsechny soubory, zatim nevyuzivam)
            if (flockDefinition.FlockFileList.Count > 0) {
                dateTimePickerFrom.Value = flockDefinition.FlockFileList[0].From;
            } else {
                dateTimePickerFrom.Value = DateTime.Now;
            }

            // Start vazeni
            numericUpDownStartDay.Text = flockDefinition.StartedDay.ToString();

            // Seznam souboru
            RedrawFiles();

            // Rustove krivky
            RedrawCurve(flockDefinition.CurveDefault, comboBoxCurveDefault);
            RedrawCurve(flockDefinition.CurveFemales, comboBoxCurveFemales);

            // Poznamka
            textBoxNote.Text = flockDefinition.Note;

            // Pokud hejno nema definovane rustove krivky ani neni zadana poznamka, skryju detaily.
            // Jinak zustavaji default zobrazene.
            if (flockDefinition.CurveDefault == null && flockDefinition.CurveFemales == null
                && flockDefinition.Note == "") {
                ShowDetails(false);
            }
        }

        private void ShowDetails(bool show) {
            isMoreShown            = show;
            groupBoxCurves.Visible = show;
            panelNote.Visible      = show;
            buttonMore.Text        = show ? Properties.Resources.HIDE_DETAILS : Properties.Resources.SHOW_DETAILS;

            // Zvetsim nebo zmensim okno
            int difference = groupBoxCurves.Height + groupBoxCurves.Margin.Top + groupBoxCurves.Margin.Bottom
                           + panelNote.Height + panelNote.Margin.Top + panelNote.Margin.Bottom;
            if (show) {
                Height += difference;
            } else {
                Height -= difference;
            }
        }
        
        private bool IsRowSelected() {
            if (listBoxFiles.Items.Count <= 0) {
                return false;     // V tabulce uz neni zadny soubor
            }

            if (listBoxFiles.SelectedIndex < 0) {
                return false;     // Neni vybrany zadny soubor
            }

            return true;
        }
        
        private void LoadCurvesToComboBox(ComboBox comboBox) {
            comboBox.Items.Clear();
            comboBox.Items.Add(Properties.Resources.CURVE_NONE);   // <none> na prvni pozici
            foreach (Curve curve in curveList.List) {
                comboBox.Items.Add(curve.Name);
            }
        }

        private void LoadCurves() {
            curveList = Program.Database.LoadCurveList();
            LoadCurvesToComboBox(comboBoxCurveDefault);
            LoadCurvesToComboBox(comboBoxCurveFemales);
        }

        private void RedrawCurve(Curve curve, ComboBox comboBox) {
            if (curve == null) {
                comboBox.SelectedIndex = 0;             // Vyberu <none>
                return;
            }

            int curveIndex = curveList.GetIndex(curve.Name);
            if (curveIndex < 0) {
                comboBox.SelectedIndex = 0;             // Vyberu <none>
                return;
            }
            comboBox.SelectedIndex = curveIndex + 1;    // Na 1. pozici je <none>
        }
        
        private void RedrawFiles() {
            listBoxFiles.Items.Clear();
            foreach (string fileName in flockFileNameList) {
                listBoxFiles.Items.Add(fileName);
            }
        }

        private Curve GetCurve(ComboBox comboBox) {
            if (comboBox.SelectedIndex <= 0) {
                return null;
            } else {
                // Nedelam kopii, aby se mne udrzelo ID krivky
                return curveList.List[comboBox.SelectedIndex - 1];
            }
        }
        
        private bool SaveFlock() {
            // Kontrola hodnot

            // Musi definovat aspon jeden soubor
            if (flockFileNameList.Count == 0) {
                CheckValue.InvalidValueMessage(listBoxFiles);
                return false;
            }

            // Soubory, datum Od nastavim u vsech souboru stejny, datum Do dam na maximum (default pro otevrene hejno)
            flockDefinition.FlockFileList.Clear();
            foreach (string fileName in flockFileNameList) {
                flockDefinition.FlockFileList.Add(new FlockFile(fileName, dateTimePickerFrom.Value, DateTime.MaxValue));
            }

            // Rustove krivky
            flockDefinition.CurveDefault = GetCurve(comboBoxCurveDefault);
            flockDefinition.CurveFemales = GetCurve(comboBoxCurveFemales);

            // Start vazeni
            flockDefinition.StartedDay = (int)numericUpDownStartDay.Value;

            // Poznamka
            flockDefinition.Note = textBoxNote.Text;

            return true;
        }

        private string GetSelectedCurveName(ComboBox comboBox) {
            if (comboBox.SelectedIndex <= 0) {
                return "";
            }
            return  comboBox.Text;
        }

        private void SelectCurve(ComboBox comboBox, string curveName) {
            comboBox.SelectedIndex = 0;            // Default vyberu <none>
            for (int i = 0; i < curveList.List.Count; i++) {
                if (curveList.List[i].Name == curveName) {
                    comboBox.SelectedIndex = i + 1;
                    return;
                }
            }
        }

        private void DeleteSelectedFile() {
            if (!IsRowSelected()) {
                return;     // Neni vybrany zadny radek
            }
            
            int selectedIndex = listBoxFiles.SelectedIndex;

            // Zeptam se, zda chce smazat aktualni soubor
            if (MessageBox.Show(String.Format(Properties.Resources.DELETE_FILE, listBoxFiles.Text), Program.ApplicationName, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            // Vymazu soubor na aktualni pozici a prekreslim
            flockFileNameList.RemoveAt(selectedIndex);
            listBoxFiles.Items.RemoveAt(selectedIndex);

            // Vyberu soubor pod prave vymazanym souborem (tj. zachovam index radku)
            if (selectedIndex < listBoxFiles.Items.Count) {
                listBoxFiles.SelectedIndex = selectedIndex;
            } else if (listBoxFiles.Items.Count > 0) {
                listBoxFiles.SelectedIndex = listBoxFiles.Items.Count - 1;
            }
            listBoxFiles.Focus();
        }

        private void buttonAddFile_Click(object sender, EventArgs e) {
            FormFlockFile form = new FormFlockFile(fileList);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Pridam do hejna vybrane soubory
            foreach (string selectedFile in form.selectedFileList) {
                if (flockFileNameList.Contains(selectedFile)) {
                    continue;   // Soubor uz v seznamu je
                }
                flockFileNameList.Add(selectedFile);
            }

            // Setridim podle nazvu
            flockFileNameList.Sort();

            // Prekreslim
            RedrawFiles();
        }

        private void buttonDeleteFile_Click(object sender, EventArgs e) {
            DeleteSelectedFile();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Ulozim data do hejna
            if (!SaveFlock()) {
                return;
            }
            DialogResult = DialogResult.OK;
        }

        private void buttonMore_Click(object sender, EventArgs e) {
            ShowDetails(!isMoreShown);
        }

        private void listBoxFiles_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode != Keys.Delete) {
                return;     // Klavesa Del maze
            }

            // Smazu
            DeleteSelectedFile();

            // Klavesu Del jsem zpracoval
            e.Handled = true;
        }

    }
}
