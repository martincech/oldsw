﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Bat1 {
    public partial class FormRestoreBackup : Form {
        private List<PeriodicBackupFile> periodicBackupFilelist;
        
        private void EnableAuto(bool enable) {
            comboBoxAuto.Enabled = enable;
        }
        
        private void EnableManual(bool enable) {
            textBoxManual.Enabled      = enable;
            buttonChooseManual.Enabled = enable;
        }

        private bool Restore() {
            if (radioButtonLastWorking.Checked) {
                StartupBackup.Restore();
                return true;
            } else if (radioButtonPeriodic.Checked) {
                return Backup.Restore(periodicBackupFilelist[comboBoxAuto.SelectedIndex].Name);
            } else {   // radioButtonManual.Checked
                return Backup.Restore(textBoxManual.Text);
            }
        }
        
        public FormRestoreBackup() {
            InitializeComponent();
        }

        private void FormRestoreBackup_Load(object sender, EventArgs e) {
            // Zakazu ovladaci prvky u jednotlivych voleb (povolim az volbu vybere)
            EnableAuto(false);
            EnableManual(false);
            
            // Posledni funkcni databazi povolim jen pokud existuje
            if (StartupBackup.Exists()) {
                radioButtonLastWorking.Enabled = true;
                labelCreated.Visible           = true;
                labelCreated.Text              = StartupBackup.Created().ToString();
            } else {
                radioButtonLastWorking.Enabled = false;
                labelCreated.Visible           = false;
            }

            // Automatickou zalohu povolim jen pokud nejaka existuje
            periodicBackupFilelist = PeriodicBackup.GetFileList();
            comboBoxAuto.Items.Clear();
            if (periodicBackupFilelist.Count > 0) {
                radioButtonPeriodic.Enabled = true;
/*                if (!radioButtonLastWorking.Enabled) {
                    // Pokud je posledni kopie zakazana, vyberu jako default
                    radioButtonPeriodic.Checked = true;
                    radioButtonPeriodic.Focus();
                    EnableAuto(true);
                }*/

                // Naplnim seznam
                foreach (PeriodicBackupFile file in periodicBackupFilelist) {
                    comboBoxAuto.Items.Add(file.Created.ToString());
                }

                // Vyberu posledni zalohu
                comboBoxAuto.SelectedIndex = comboBoxAuto.Items.Count - 1;
            } else {
                radioButtonPeriodic.Enabled = false;
            }

            // Rucni zalohu povolim vzdy a vyberu ji jako default volbu
            radioButtonManual.Enabled = true;
            radioButtonManual.Checked = true;
            radioButtonManual.Focus();
            EnableManual(true);
        }

        private void buttonChooseManual_Click(object sender, EventArgs e) {
            // Filtr pro rucni volbu zalohy
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter   = String.Format(Properties.Resources.FILE_FORMAT_BACKUP, Program.ApplicationName) + " (*." + FileExtension.BACKUP + ")|*." + FileExtension.BACKUP;
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Zapisu nazev souboru do TextBoxu
            textBoxManual.Text = openFileDialog.FileName;
        }

        private void radioButtonLastWorking_CheckedChanged(object sender, EventArgs e) {
            EnableAuto(false);
            EnableManual(false);
        }

        private void radioButtonPeriodic_CheckedChanged(object sender, EventArgs e) {
            EnableAuto(true);
            EnableManual(false);
        }

        private void radioButtonManual_CheckedChanged(object sender, EventArgs e) {
            EnableAuto(false);
            EnableManual(true);
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Pokud vybral rucni zalohu, zkontroluju, zda soubor existuje
            if (radioButtonManual.Checked && !System.IO.File.Exists(textBoxManual.Text)) {
                MessageBox.Show(Properties.Resources.FILE_DOESNT_EXIST, Text);
                textBoxManual.Focus();
                return;
            }

            if (MessageBox.Show(Properties.Resources.RESTORE_BACKUP_OVERWRITE_WARNING, Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            Refresh();
            
            // Obnovim podle vybrane zalohy
            Cursor.Current = Cursors.WaitCursor;
            try {
                bool success = Restore();

                Cursor.Current = Cursors.Default;
                if (success) {
                    // Obnova probehla v poradku
                    MessageBox.Show(Properties.Resources.RESTORE_BACKUP_SUCCESSFUL, Text);
                } else {
                    // Databaze je novejsi nez SW, musi upgradovat SW
                    MessageBox.Show(Properties.Resources.DATABASE_NOT_SUPPORTED, Text);
                }
                DialogResult = DialogResult.OK;
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
