﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlCurve : UserControl {
        /// <summary>
        /// Read-only mode or edit mode
        /// </summary>
        public bool ReadOnly;

        /// <summary>
        /// Constructor
        /// </summary>
        public UserControlCurve() {
            InitializeComponent();

            // Vytvorim radky v tabulce
            Clear();
        }

        /// <summary>
        /// Clear table
        /// </summary>
        public void Clear() {
            dataGridView.Rows.Clear();
            for (int i = 0; i < Curve.MAX_COUNT; i++) {
                dataGridView.Rows.Add();
            }
        }

        /// <summary>
        /// Load curve into the table
        /// </summary>
        /// <param name="curve">Curve to load</param>
        public void LoadCurve(Curve curve) {
            dataGridView.SuspendLayout();

            try {
                // Vytvorim prazdne bunky
                Clear();

                // Vyplnim krivku
                int rowIndex = 0;
                foreach (CurvePoint point in curve.PointList) {
                    dataGridView.Rows[rowIndex].Cells["ColumnDay"].Value    = point.Day.ToString();
                    dataGridView.Rows[rowIndex].Cells["ColumnWeight"].Value = DisplayFormat.RoundWeight((float)point.Weight, curve.Units);
                    rowIndex++;
                }
            } finally {
                dataGridView.ResumeLayout();
            }
        }

        /// <summary>
        /// Get number of dayes entered in the table
        /// </summary>
        /// <returns>Number of days</returns>
        private int GetNumberOfDays() {
            int count = 0;

            foreach (DataGridViewRow row in dataGridView.Rows) {
                if ((string)row.Cells["ColumnDay"].Value == "" || row.Cells["ColumnDay"].Value == null) {
                    break;
                }
                count++;
            }
            return count;
        }

        private void SelectCell(int row, int column) {
            dataGridView.CurrentCell.Selected = false; 
            dataGridView.Rows[row].Cells[column].Selected = true; 
            dataGridView.CurrentCell = dataGridView.SelectedCells[0]; 
        }

        public void RedrawUnits(Units units) {
            dataGridView.Columns[1].HeaderText = Properties.Resources.WEIGHT + " [" + ConvertWeight.UnitsToString(units) + "]";
        }
        
        /// <summary>
        /// Check values in the table
        /// </summary>
        /// <returns>True if all values are valid</returns>
        private bool CheckData(Units units, out int daysCount) {
            int day;
            float weight;
            Curve curve;        // Budu vytvaret jakoby novou krivku, aby probihala i kontrola existence dne ve krivce

            // Zjistim pocet dnu zadanych v tabulce
            daysCount = GetNumberOfDays();

            // Zalozim novou testovaci krivku
            curve = new Curve("Test", units);

            // Projizdim jednotlive radky
            for (int row = 0; row < daysCount; row++) {
                // Kontrola dne
                if (!int.TryParse((string)dataGridView.Rows[row].Cells[0].Value, out day)) {
                    // Neplatny format
                    SelectCell(row, 0);
                    return false;
                }
                if (!curve.CheckDay(day)) {
                    // Den ma bud spatny rozsah, nebo uz v krivce existuje
                    SelectCell(row, 0);
                    return false;
                }
                
                // Kontrola hmotnosti - gramy zadava na cele cislo, kg a lb na desetiny
                if (!float.TryParse((string)dataGridView.Rows[row].Cells[1].Value, out weight)) {
                    // Neplatny format
                    SelectCell(row, 1);
                    return false;
                }
                if (units == Units.G) {
                    weight = (int)weight;   // U gramu useknu desetinnou cast
                }
                if (!curve.CheckWeight(weight)) {
                    // Den ma bud spatny rozsah, nebo uz v krivce existuje
                    SelectCell(row, 1);
                    return false;
                }

                // Pridam bod do testovaci krivky (nutne pro kontrolu existence dne ve krivce)
                if (!curve.Add(day, weight)) {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Save entered curve
        /// </summary>
        /// <param name="curve">Curve to add points to</param>
        /// <returns>True if successful</returns>
        public bool Save(ref Curve curve) {
            // Zkontroluju, zda je tabulka zadana korektne
            int daysCount;
            if (!CheckData(curve.Units, out daysCount)) {
                return false;
            }

            // Smazu vsechny dosavadni body v krivce
            curve.PointList.Clear();

            // Ulozim body z tabulky, hodnoty v tabulce jsou zadane spravne (testoval jsem vyse)
            for (int row = 0; row < daysCount; row++) {
                curve.Add(int.Parse(  (string)dataGridView.Rows[row].Cells[0].Value),
                          float.Parse((string)dataGridView.Rows[row].Cells[1].Value));
            }

            return true;
        }


        private void dataGridView_KeyDown(object sender, KeyEventArgs e) {
            if (ReadOnly) {
                return;
            }

            if (e.KeyCode == Keys.Delete) {
                // Smazu obsah aktualni bunky
                if (dataGridView.CurrentCell == null) {
                    return;
                }
                dataGridView.CurrentCell.Value = "";

                // Zpracoval jsem
                e.Handled = true;
                return;
            }
        }



    }
}
