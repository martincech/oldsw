﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// One file and date range defined in a flock
    /// </summary>
    public struct FlockFile {
        /// <summary>
        /// File name
        /// </summary>
        public string FileName;

        /// <summary>
        /// From what date
        /// </summary>
        public DateTime From;

        /// <summary>
        /// To what date
        /// </summary>
        public DateTime To;

        public FlockFile(string fileName, DateTime from, DateTime to) {
            FileName = fileName;
            From     = from;
            To       = to;
        }

        /// <summary>
        /// Compare function for List(FlockFile).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByFrom(FlockFile x, FlockFile y) {
            int result = DateTime.Compare(x.From, y.From);
            if (result != 0) {
                return result;      // Datumy jsou rozdilne
            }

            // Datumy jsou shodne, rozhodne jmeno souboru
            result = string.Compare(x.FileName, y.FileName);
            if (result != 0) {
                return result;      // Jmena souboru jsou rozdilne
            }

            // Musi rozhodnout datum To
            return DateTime.Compare(x.To, y.To);
        }

    }
    
    /// <summary>
    /// Flock definition
    /// </summary>
    public class FlockDefinition {

        // Kazde hejno muze obsahovat vice souboru, cimz se pokryje situace, kdy se kurata presouvaji z jedne haly/farmy do jine.

        /// <summary>
        /// Flock name
        /// </summary>
        public string Name;

        /// <summary>
        /// Flock note
        /// </summary>
        public string Note;

        /// <summary>
        /// Default standard growth curve of the flock (curve for males with mixed sex)
        /// </summary>
        public Curve CurveDefault;

        /// <summary>
        /// Standard growth curve of females, when mixed sex is used
        /// </summary>
        public Curve CurveFemales;

        /// <summary>
        /// Date and time when the flock was started
        /// </summary>
        public DateTime Started { get { return flockFileList[0].From; } }   // Datum zacatku prvniho souboru

        /// <summary>
        /// Day number corresponding to the date when the flock was started
        /// </summary>
        public int StartedDay;

        /// <summary>
        /// List of files and date ranges included in the flock. Sorted by From date.
        /// </summary>
        public List<FlockFile> FlockFileList { get { return flockFileList; } }
        private List<FlockFile> flockFileList;

        /// <summary>
        /// Flock ID in the database (for simple updates in the database)
        /// </summary>
        public long Id;

        /// <summary>
        /// Constructor
        /// </summary>
        public FlockDefinition() {
            Name          = "";
            Note          = "";
            CurveDefault  = null;
            CurveFemales  = null;
            StartedDay    = 1;
            flockFileList = new List<FlockFile>();
            Id            = -1;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Flock name</param>
        public FlockDefinition(string name) : this() {
            Name  = name;
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="sourceCurve">Source flock to copy from</param>
        /// <param name="copyId">Copy flock ID as well</param>
        public FlockDefinition(FlockDefinition sourceFlockDefinition, bool copyId) {
            Name          = sourceFlockDefinition.Name;
            Note          = sourceFlockDefinition.Note;
            CurveDefault  = CopyCurve(sourceFlockDefinition.CurveDefault);
            CurveFemales  = CopyCurve(sourceFlockDefinition.CurveFemales);
            StartedDay    = sourceFlockDefinition.StartedDay;
            flockFileList = new List<FlockFile>(sourceFlockDefinition.flockFileList);
            if (copyId) {
                Id        = sourceFlockDefinition.Id;
            } else {
                Id        = -1;
            }
        }

        /// <summary>
        /// Copy growth curve
        /// </summary>
        /// <param name="sourceCurve">Source curve</param>
        /// <returns>Curve copy</returns>
        private Curve CopyCurve(Curve sourceCurve) {
            if (sourceCurve == null) {
                return null;       // Zadna krivka neni zadana
            } else {
                Curve curve = new Curve(sourceCurve);
                curve.Id  = sourceCurve.Id;     // Preberu i ID v databazi
                return curve;
            }
        }
        
        /// <summary>
        /// Check if file index is valid within the flock
        /// </summary>
        /// <param name="index">File index</param>
        /// <returns>True if valid</returns>
        public bool CheckIndex(int index) {
          return (bool)(index >= 0 && index < flockFileList.Count);
        }

        /// <summary>
        /// Sort the list by from date
        /// </summary>
        private void Sort() {
            flockFileList.Sort(FlockFile.CompareByFrom);
        }

        /// <summary>
        /// Add new file to the flock
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="from">From date</param>
        /// <param name="to">To date</param>
        /// <returns>True if successful</returns>
        public bool Add(string fileName, DateTime from, DateTime to) {
            // Zkontroluju platnost jmena
            if (!CheckValue.CheckScaleName(fileName)) {
                return false;
            }

            // Od-Do musi jit postupne
            if (from > to) {
                return false;
            }

            // Ulozim novy bod
            flockFileList.Add(new FlockFile(fileName, from, to));

            // Na zaver setridim seznam podle dne
            Sort();

            return true;
        }

        /// <summary>
        /// Delete file from the flock
        /// </summary>
        /// <param name="index">File index</param>
        public void Delete(int index) {
            if (!CheckIndex(index)) {
                return;
            }
            flockFileList.RemoveAt(index);
        }

        /// <summary>
        /// Compare function for List(FlockDefinition).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByName(FlockDefinition x, FlockDefinition y) {
            int result = String.Compare(x.Name, y.Name);
            if (result < 0) {
                return -1;
            }
            if (result > 0) {
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// Compare function for List(FlockDefinition).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByTime(FlockDefinition x, FlockDefinition y) {
            int result = DateTime.Compare(x.Started, y.Started);
            if (result != 0) {
                return result;      // Datumy jsou rozdilne
            }

            // Datumy jsou shodne, rozhodne nazev
            return CompareByName(x, y);
        }

        /// <summary>
        /// Get curve for a specified flag
        /// </summary>
        /// <param name="flag">Flag</param>
        /// <returns>Curve</returns>
        public Curve GetCurve(Flag flag) {
            if (flag == Flag.FEMALE) {
                // Samice maji specialni krivku
                return CurveFemales;
            }
            // Samci nebo vse ostatni pouzivaji krivku default
            return CurveDefault;
        }

        /// <summary>
        /// Check if the flock is currently running (i.e. the flock is not closed)
        /// </summary>
        /// <returns>True if the flock is current</returns>
        public bool IsCurrent() {
            // Projedu vsechny soubory a pokud je datum Do u libovolneho z nich roven DateTime.MaxValue, hejno bezi
            foreach (FlockFile flockFile in flockFileList) {
                if (flockFile.To.Date == DateTime.MaxValue.Date) {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Set flock To date
        /// </summary>
        /// <param name="closedDateTime">To date</param>
        private void SetToDate(DateTime toDateTime) {
            // U vsech souboru nastavim datum Do na toDateTime
            // FlockFile je struct, tj. musim vytvorit seznam znovu.
            List<FlockFile> newFlockFileList = new List<FlockFile>();
            foreach (FlockFile flockFile in flockFileList) {
                newFlockFileList.Add(new FlockFile(flockFile.FileName, flockFile.From, toDateTime));
            }

            // Preberu novy seznam
            flockFileList = newFlockFileList;
        }

        /// <summary>
        /// Set flock as current
        /// </summary>
        public void SetCurrent() {
            // U vsech souboru nastavim datum Do na DateTime.MaxValue
            SetToDate(DateTime.MaxValue);
        }

        /// <summary>
        /// Set flock as closed
        /// </summary>
        /// <param name="closedDateTime">Closed date</param>
        public void SetClosed(DateTime closedDateTime) {
            // U vsech souboru nastavim datum Do na closedDateTime
            SetToDate(closedDateTime);
        }

    }
}
