﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlScaleConfigGroups : UserControlScaleConfigBase {
        private string windowTitle;

        public UserControlScaleConfigGroups(ScaleConfig scaleConfig, string windowTitle, bool readOnly) {
            InitializeComponent();

            // Read-only
            this.readOnly = readOnly;
            buttonDelete.Enabled = !readOnly;
            buttonNew.Enabled    = !readOnly;
            buttonRename.Enabled = !readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
            this.windowTitle = windowTitle;
        }

        private void RedrawGroupList() {
            listBoxGroups.Items.Clear();
            foreach (FileGroup group in scaleConfig.FileGroupList.List) {
                listBoxGroups.Items.Add(group.ToString());
            }
        }

        private void RedrawGroup(int index) {
            isLoading = true;

            try {
                checkedListBoxFiles.Items.Clear();
                if (index < 0) {
                    // Neni vybrana zadna skupina, vse vymazu (seznam souboru uz smazany je)
                    groupBoxDetails.Enabled = false;
                    groupBoxDetails.Text    = "";
                    textBoxNote.Text        = "";
                    return;
                }

                FileGroup group = scaleConfig.FileGroupList.List[index];
                groupBoxDetails.Enabled = true;
                groupBoxDetails.Text    = group.Name;

                textBoxNote.Text = group.Note;
                
                // Seznam souboru zaroven zaskrtnuti
                for (int i = 0; i < scaleConfig.FileList.List.Count; i++) {
                    checkedListBoxFiles.Items.Add(scaleConfig.FileList.List[i].ToString(),
                                                  scaleConfig.FileGroupList.List[index].FileIndexList.Contains(i));
                }
            } finally {
                isLoading = false;
            }
        }

        public override void Redraw() {
            RedrawGroup(listBoxGroups.SelectedIndex);
        }

        public override void SetScaleConfig(ScaleConfig scaleConfig) {
            // Preberu config
            this.scaleConfig = scaleConfig;

            // Vyberu prvni polozku a prekreslim
            isLoading = true;
            try {
                RedrawGroupList();
                if (scaleConfig.FileGroupList.List.Count > 0) {
                    listBoxGroups.SelectedIndex = 0;
                }
                RedrawGroup(listBoxGroups.SelectedIndex);
            } finally {
                isLoading = false;
            } 
        }

        private void SaveNote() {
            if (readOnly || isLoading) {
                return;
            }
            
            if (listBoxGroups.SelectedIndex < 0) {
                return;     // Neni vybrana zadna skupina (nemelo by nastat)
            }
            FileGroup group = scaleConfig.FileGroupList.List[listBoxGroups.SelectedIndex];

            // Radsi projedu zadany text a vyhazim neplatne znaky (pomoci Ctrl+V muze vlozit cokoliv)
            string validNote = "";
            foreach (char ch in textBoxNote.Text) {
                if (!KeyFilter.IsScaleChar(ch)) {
                    continue;       // Neplatny znak vynecham
                }
                validNote += ch;
            }
            textBoxNote.Text = validNote;

            // Ulozim poznamku
            if (validNote == group.Note) {
                return;     // Poznamka zustala stejna
            }
            
            // Poznamka je zmenena, ulozim do skupiny
            group.SetNote(validNote);

            // Prekreslim seznam skupin (ukazde skupiny je v zavorce i poznamka) a preskocim zpet na aktualni skupinu
            int index = listBoxGroups.SelectedIndex;
            RedrawGroupList();
            listBoxGroups.SelectedIndex = index;
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            listBoxGroups.Focus();      // Default dam focus zpet na seznam

            FormName form = new FormName(windowTitle, "", true);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }
            if (scaleConfig.FileGroupList.Exists(form.EnteredName)) {
                MessageBox.Show(String.Format(Properties.Resources.FILE_GROUP_ALREADY_EXISTS, form.EnteredName), windowTitle);
                return;
            }
            scaleConfig.FileGroupList.Add(form.EnteredName);
            RedrawGroupList();
            
            // Preskocim na prave vytvorenou skupinu
            listBoxGroups.SelectedIndex = scaleConfig.FileGroupList.GetIndex(form.EnteredName);
            listBoxGroups.Focus();
        }

        private void buttonRename_Click(object sender, EventArgs e) {
            int index = listBoxGroups.SelectedIndex;
            listBoxGroups.Focus();      // Default dam focus zpet na seznam

            if (index < 0) {
                // Nevybral zadnou polozku
                return;
            }
            FormName form = new FormName(windowTitle, scaleConfig.FileGroupList.List[index].Name, true);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            if (form.EnteredName == scaleConfig.FileGroupList.List[index].Name) {
                // Jmeno beze zmeny
                return;
            }

            // Zmenil jmeno
            if (scaleConfig.FileGroupList.Exists(form.EnteredName)) {
                // Nove zadane jmeno uz existuje
                MessageBox.Show(String.Format(Properties.Resources.FILE_GROUP_ALREADY_EXISTS, form.EnteredName), windowTitle);
                return;
            }

            // Prejmenuju, zaroven dojde k novemu setrideni seznamu
            scaleConfig.FileGroupList.Rename(index, form.EnteredName);

            // Prekreslim a preskocim na editovany soubor
            RedrawGroupList();
            listBoxGroups.SelectedIndex = scaleConfig.FileGroupList.GetIndex(form.EnteredName);
            listBoxGroups.Focus();
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            int index = listBoxGroups.SelectedIndex;
            listBoxGroups.Focus();      // Default dam focus zpet na seznam
            if (index < 0) {
                return;
            }

            if (MessageBox.Show(String.Format(Properties.Resources.DELETE_FILE_GROUP,
                                scaleConfig.FileGroupList.List[index].Name),
                                windowTitle, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            scaleConfig.FileGroupList.Delete(index);
            
            // Prekreslim a focus nastavim na nejblizsi polozku
            RedrawGroupList();
            if (scaleConfig.FileGroupList.List.Count == 0) {
                // Seznam uz je prazdny
                index = -1;
            } else if (index >= scaleConfig.FileGroupList.List.Count) {
                // Smazal posledni polozku v seznamu, prejdu na predposledni
                index = scaleConfig.FileGroupList.List.Count - 1;
            } // else ponecham na polozce za smazanou polozkou
            listBoxGroups.SelectedIndex = index;
            listBoxGroups.Focus();
            RedrawGroup(index);
        }

        private void listBoxGroups_SelectedIndexChanged(object sender, EventArgs e) {
            if (isLoading) {
                return;
            }

            isLoading = true;
            try {
                RedrawGroup(listBoxGroups.SelectedIndex);
            } finally {
                isLoading = false;
            }
        }

        private void UserControlScaleConfigFileGroups_VisibleChanged(object sender, EventArgs e) {
            if (!Visible) {
                return;
            }

            // Prave detaily zobrazil - mezitim mohl zmenit seznam souboru, tj. prekreslim i aktualne vybranou skupinu
            isLoading = true;
            try {
                RedrawGroup(listBoxGroups.SelectedIndex);
            } finally {
                isLoading = false;
            } 

        }

        private void textBoxNote_KeyPress(object sender, KeyPressEventArgs e) {
            e.KeyChar = Char.ToUpper(e.KeyChar);
            if (!KeyFilter.IsScaleChar(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void textBoxNote_Leave(object sender, EventArgs e) {
            SaveNote();
        }

        private void textBoxNote_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                SaveNote();     // Po stisku Enter ulozim
            }
        }

        private void checkedListBoxFiles_ItemCheck(object sender, ItemCheckEventArgs e) {
            if (readOnly || isLoading) {
                return;
            }

            if (listBoxGroups.SelectedIndex < 0) {
                return;     // Neni vybrana zadna skupina (nemelo by nastat)
            }
            FileGroup group = scaleConfig.FileGroupList.List[listBoxGroups.SelectedIndex];
            if (e.NewValue == CheckState.Checked) {
                // Pridam index do seznamu
                group.FileIndexList.Add(e.Index);
            } else {
                // Odstranim index ze seznamu
                group.FileIndexList.Remove(e.Index);
            }
        }

        private void listBoxGroups_MouseDoubleClick(object sender, MouseEventArgs e) {
            if (readOnly) {
                return;
            }
            buttonRename_Click(sender, null);
        }
    }
}
