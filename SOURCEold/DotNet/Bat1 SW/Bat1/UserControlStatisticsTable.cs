﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using Veit.ScaleStatistics;
using Veit.Scale;
using System.Drawing.Printing;
using ZedGraph;
using PrintDataGrid;

namespace Bat1 {
    public partial class UserControlStatisticsTable : UserControl {
        /// <summary>
        /// List of displayed weighings
        /// </summary>
        public WeighingList WeighingList { get { return weighingList; } }
        private WeighingList weighingList;

        /// <summary>
        /// List of displayed flocks
        /// </summary>
        public List<Flock> FlockList { get { return flockList; } }
        private List<Flock> flockList;

        /// <summary>
        /// Displayed flag
        /// </summary>
        public Flag DisplayedFlag { get { return displayedFlag; } }
        private Flag displayedFlag = Flag.ALL;

        /// <summary>
        /// True when loading weighings from the database
        /// </summary>
        private bool isLoadingWeighings = false;

        /// <summary>
        /// Units that are used for all weighings
        /// </summary>
        private Units displayedUnits;

        public UserControlStatisticsTable() {
            InitializeComponent();

            // Zobrazim prazdny obsah
            ClearData();
        }

        /// <summary>
        /// Clear all statistic results
        /// </summary>
        public void ClearData() {
            // Smazu seznamy
            weighingList = new WeighingList();
            flockList    = null;

            // Smazu zobrazeny obsah
            DisplayWeighingList(false);                 // Tabulka vazeni
            ClearWeighingDetails();                     // Histogram a vzorky
            HideFlocks();                               // Hejna
        }

        public void RedrawStatistics(List<WeighingSearchInfo> weighingSearchInfoList) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                weighingList = CreateWeighingList(weighingSearchInfoList);
                weighingList.Sort();        // Setridim podle data

                // Pokud je v seznamu vazeni pouzito vice jednotek, musim vsechny vazeni prepocist na jedny jednotky
                if (!CheckUnits()) {
                    weighingList.Clear();       // Smazu seznam vazeni
                    ClearWeighingDetails();
                    // Pokracuju dal v prekresleni
                }

                RedrawStatistics(weighingList);

                // Schovam hejna
                HideFlocks();
            } finally {
                isLoadingWeighings = false;
                Cursor.Current = Cursors.Default;
            }
        }

        private void RedrawStatistics(WeighingList weighingList) {
            // Pozor, vsechna vazeni uz musi byt prepoctena na 1 jednotky. Pouzite jednotky uz jsou ulozene
            // v displayedUnits

            // Vypoctu celkove statistiky
            weighingList.CalculateTotalStatistics();

            // Zacinam plnit data do controlu
            isLoadingWeighings = true;

            ClearWeighingDetails();

            // Default zobrazim vsechny flagy
            displayedFlag = Flag.ALL;
            
            // Zobrazim seznam vazeni
            DisplayWeighingList(true);

            // Skocim do tabulky s vazenimi
            dataGridWeighings.Focus();
            
            // Vykreslim histogram a vzorky vybraneho vazeni
            DisplaySelectedWeighing();
        }

        public void RedrawLastWeighings() {
            // Nactu vybrana vazeni do pameti, jsou uz setridena podle datumu, vahy a souboru
            RedrawStatistics(Program.Database.ReadLastWeighings());
        }

        public void RedrawCurrentFlocks() {
            // Default zobrazim vsechny flagy - musim rucne jiz zde, pred vypocem prirustku. Pokud by mel vybrany
            // najaky flag a neprepnul bych zde na vsechny flagy, prirustky se vypoctou pro vybrany flag, ale
            // po vypoctu se zobrazi vsechny flagy. Zde nastavim jen promennou displayedFlag, combobox az pozdeji,
            // protoze jeste neznam seznam flagu.
            displayedFlag = Flag.ALL;

            // Aktivni hejna vykresluju vzdy bez casoveho omezeni
            RedrawStatistics(Program.Database.LoadCurrentFlockList());
        }

        public void SetFlag(int flagComboBoxIndex) {
            // Nactu vybrany flag
            if (flagComboBoxIndex == 0) {
                displayedFlag = Flag.ALL;       // Na prvni pozici je text "Sex/Limit..."
            } else {
                displayedFlag = weighingList.FlagList[flagComboBoxIndex - 1];   
            }
            
            // Pokud zobrazuju hejna, musim prepocist cilove hmotnosti (male/female muzou mit jine krivky)
            if (flockList != null) {
                CalculateFlockData(flockList, displayedFlag);

                // Setridim seznam vazeni podle cisla dne (az po vypoctu dne, cilove hmotnosti a prirustku)
                weighingList.SortByFlockDay();
            }
            
            // Zapamatuju si prave vybrane vazeni (bude chtit asi porovanavat)
            int selectedIndex = dataGridWeighings.SelectedRows[0].Index;
            
            // Zobrazim seznam vazeni
            if (flockList != null) {
                DisplayWeighingList(true);
            } else {
                DisplayWeighingList(false);
            }

            // Vratim se na puvodne vybrany radek
            dataGridWeighings.Rows[selectedIndex].Selected = true;

            // Vykreslim histogram a vzorky vybraneho vazeni
            DisplaySelectedWeighing();
        }

        private void ShowFlockColumns(bool isVisible) {
            if (isVisible) {
                dataGridWeighings.Columns["ColumnFile"].HeaderText = Properties.Resources.FLOCK;
            } else {
                dataGridWeighings.Columns["ColumnFile"].HeaderText = Properties.Resources.FILE;
            }
            dataGridWeighings.Columns["ColumnDay"].Visible        = isVisible;
            dataGridWeighings.Columns["ColumnTarget"].Visible     = isVisible;
            dataGridWeighings.Columns["ColumnDifference"].Visible = isVisible;
            dataGridWeighings.Columns["ColumnAbove"].Visible      = isVisible;
            dataGridWeighings.Columns["ColumnBelow"].Visible      = isVisible;
            dataGridWeighings.Columns["ColumnGain"].Visible       = isVisible;
        }

        private void HideFlocks() {
            flockList = null;

            // Zakazu menu hejn
            ShowFlockColumns(false);
        }
        
        private void ShowFlocks() {
            // Povolim menu hejn
            ShowFlockColumns(true);
        }

        private void AddWeighingToDataGrid(string started, string day, string fileName, string target, string difference,
                                           string above, string below, string gain, StatisticResult result, ResultType resultType) {
            // Pokud je result == null, vazeni neobsahuje zobrazeny flag
            
            // Vytvorim nahled histogramu
            Bitmap bitmap;
            if (result == null || result.Histogram == null) {
                bitmap = new Bitmap(1, 1);      // Vytvorim prazdnou bitmapu, jinak se v DataGridu zobrazuje krizek
            } else {
                bitmap = new Bitmap(HistogramMiniGraph.GetTotalWidth(result.Histogram.Counts.Length), dataGridWeighings.RowTemplate.Height - 2);
                HistogramMiniGraph.Draw(result.Histogram.Counts, Graphics.FromImage(bitmap), bitmap.Height - 1, 1, true);
            }

            // Pridam radek do tabulky
            if (result == null) {
                dataGridWeighings.Rows.Add("",
                                           "",
                                           fileName,
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           "",
                                           bitmap
                                           );
            } else {
                dataGridWeighings.Rows.Add(started,
                                           day,
                                           fileName,
                                           result.Count.ToString("N0"),
                                           DisplayFormat.RoundWeight(result.Average, displayedUnits),
                                           target,
                                           difference,
                                           above,
                                           below,
                                           gain,
                                           DisplayFormat.RoundWeight(result.Sigma, displayedUnits),
                                           DisplayFormat.Cv(result.Cv),
                                           DisplayFormat.Uniformity(result.Uniformity),
                                           result.WeighingSpeed.ToString("N0"),
                                           bitmap
                                           );
            }
        }

        private void DisplayWeighingList(bool showFlocks) {
            // Zobrazim seznam vazeni
            dataGridWeighings.Rows.Clear();
            int index = 0;
            foreach (Weighing weighing in weighingList.List) {
                // Vytvorim stringy pro extra hodnoty hejna
                string day = "", target = "", difference = "", above = "", below = "", gain = "";
                if (showFlocks) {
                    day        = weighing.FlockData.Day.ToString("N0");
                    target     = DisplayFormat.RoundWeight(weighing.FlockData.Target,     displayedUnits);
                    difference = DisplayFormat.RoundWeight(weighing.FlockData.Difference, displayedUnits);
                    above      = weighing.FlockData.CountAbove.ToString("N0");
                    below      = weighing.FlockData.CountBelow.ToString("N0");
                    gain       = DisplayFormat.RoundWeight(weighing.FlockData.Gain,       displayedUnits);
                }

                // Pridam data do tabulky
                AddWeighingToDataGrid(weighing.GetMinDateTime(displayedFlag).ToString("g"),
                                      day,
                                      weighing.WeighingData.File.Name,
                                      target,
                                      difference,
                                      above,
                                      below,
                                      gain,
                                      weighing.WeighingResults.GetResult(displayedFlag),
                                      weighing.WeighingData.ResultType);
                index++;
            }
            
            // Celkove vysledky zobrazim jen pokud vybral vice vazeni
            if (weighingList.List.Count > 1) {
                AddWeighingToDataGrid(Properties.Resources.TOTAL + " (" + weighingList.List.Count.ToString() + ")",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      "",
                                      weighingList.TotalstatisticResults.GetResult(displayedFlag),
                                      ResultType.MANUAL);       // Celkove vysledky vzdy zaokrouhluju
            }
        }
        
        private void DisplaySamples(WeighingData weighingData, Flag flag) {
            if (weighingData == null) {
                userControlSamples.SetData(null, flag, Units.KG);
            } else {
                userControlSamples.SetData(weighingData.SampleList, flag, weighingData.ScaleConfig.Units.Units);
            }
        }
        
        private void DisplayHistogram(WeighingResults weighingResults, WeighingData weighingData,
                                      Flag flag, int uniformityRange) {
            // Nactu vysledky pro zadany flag
            StatisticResult result = weighingResults.GetResult(flag);
            if (result == null) {
                // Tento flag ve vazeni neni
                userControlHistogram.ClearData();
                return;
            }

            // Vykreslim
            userControlHistogram.SetData(result.Histogram, result.Average, uniformityRange);
        }
        
        private void DisplaySelectedWeighing() {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Zjistim vybrane vazeni
            int selectedIndex = dataGridWeighings.SelectedRows[0].Index;
            WeighingResults weighingResults;
            WeighingData weighingData;
            int uniformityRange;
            if (dataGridWeighings.Rows.Count == 1 || selectedIndex < dataGridWeighings.Rows.Count - 1) {
                // Vybral nektere vazeni (pokud je zobrazeno pouze 1 vazeni, radek TOTAL se nezobrazuje)
                weighingResults = weighingList.List[selectedIndex].WeighingResults;
                weighingData    = weighingList.List[selectedIndex].WeighingData;
                uniformityRange = weighingData.ScaleConfig.StatisticConfig.UniformityRange;
            } else {
                // Vybral celkem (posledni radek)
                weighingResults = weighingList.TotalstatisticResults;
                weighingData    = null;
                uniformityRange = weighingList.List[0].WeighingData.ScaleConfig.StatisticConfig.UniformityRange;
            }

            Cursor.Current = Cursors.WaitCursor;
            try {
                // Pro zrychleni vykresluju jen to, co je prave zobrazene
                switch (tabControlBottom.SelectedIndex) {
                    case 0:
                        DisplayHistogram(weighingResults, weighingData, displayedFlag, uniformityRange);
                        break;
                    case 1:
                        DisplaySamples(weighingData, displayedFlag);
                        break;
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private bool CheckUnits() {
            // Pokud jsou v seznamu vazeni jen jedny jednotky, je vse v poradku
            if (weighingList.UnitsList.Count == 0) {
                return true;
            } 
            if (weighingList.UnitsList.Count == 1) {
                displayedUnits = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;     // Ulozim si
                return true;
            }

            // V seznamu vazeni je pouzito vice jednotek, musim vsechna vazeni prepocist na jedny jednotky
            FormUnits form = new FormUnits(weighingList.UnitsList[0]);      // Default vyberu jednotky z 1. vazeni
            if (form.ShowDialog() != DialogResult.OK) {
                return false;
            }

            // Prepoctu vsechna vazeni
            weighingList.ChangeUnits(form.SelectedUnits);

            // Ulozim si pouzite jednotky
            displayedUnits = form.SelectedUnits;

            return true;
        }

        private void ClearWeighingDetails() {
            userControlHistogram.ClearData();
            DisplaySamples(null, displayedFlag);
        }

        private WeighingList CreateWeighingList(List<WeighingSearchInfo> weighingSearchInfoList) {
            WeighingList weighingList = new WeighingList();
            
            foreach (WeighingSearchInfo searchinfo in weighingSearchInfoList) {
                weighingList.Add(Program.Database.LoadWeighing(searchinfo.id));
            }

            return weighingList;
        }

        private WeighingList CreateWeighingList(List<Flock> flockList, Flag flag) {
            // Ze seznamu hejn vytvorim seznam jednotlivych vazeni
            WeighingList weighingList = new WeighingList();
            foreach (Flock flock in flockList) {
                foreach (Weighing weighing in flock.WeighingList.List) {
                    weighingList.Add(weighing);
                }
            }
            return weighingList;
        }

        private void CalculateFlockData(List<Flock> flockList, Flag flag) {
            // U vsech vazeni v seznamu hejn vypoctu cislo dne, cilovou hmotnost a prirustek.
            // Musim delat zvlast, az mam zajisteno, ze vsechna vazeni maji nastavene stejne jednotky
            foreach (Flock flock in flockList) {
                // Teoreticke krivky prepoctu na zobrazene jednotky (jsou uz v displayedUnits)
                Curve curve = flock.Definition.GetCurve(flag);
                if (curve != null) {
                    curve.SetNewUnits(displayedUnits);
                }

                // Z jednotlivych vazeni v hejnu vypoctu potrebne udaje
                float lastAverage = float.MinValue;        // Pocatecni hodnota pro vypocet prirustku
                foreach (Weighing weighing in flock.WeighingList.List) {
                    // Cislo dne v hejnu
                    int flockDay = flock.DateToDay(weighing.GetMinDateTime(flag));
                    weighing.FlockData.Day = flockDay;
                    
                    // Cilova hmotnost z teoreticke rustove krivky, uz ji mam nactenou a prepoctenou na spravne jednotky
                    if (curve == null) {
                        weighing.FlockData.Target = 0;
                    } else {
                        // Vypoctu rovnou zaokrouhlenou hodnotu v gramech
                        weighing.FlockData.Target = float.Parse(DisplayFormat.RoundWeight((float)curve.CalculateWeight(flockDay),
                                                                                          weighing.WeighingData.ScaleConfig.Units.Units));
                    }

                    // Rozdil prumeru od cilove hmotnosti a prirustek od minuleho vazeni
                    // Average i Target jsou uz zaokrouhlene na gramy, tj. nemusim ted nic zaokrouhlovat
                    StatisticResult result = weighing.WeighingResults.GetResult(flag);
                    if (result == null) {
                        weighing.FlockData.Difference = 0;  // Vazeni tento flag neobsahuje
                        weighing.FlockData.Gain = 0;        
                    } else {
                        weighing.FlockData.Difference = result.Average - weighing.FlockData.Target;
                        if (lastAverage == float.MinValue) {
                            weighing.FlockData.Gain = 0;    // Prvni bod v krivce ma nulovy prirustek
                        } else {
                            weighing.FlockData.Gain = result.Average - lastAverage;
                        }
                        lastAverage = result.Average;
                    }

                    // Pocet vazeni nad a pod cilovou hmotnosti
                    weighing.FlockData.CountAbove = 0;
                    weighing.FlockData.CountBelow = 0;
                    if (weighing.WeighingData.SampleList != null && result != null) {
                        // U rucne zadaneho vazeni nejsou zadne vzorky (SampleList = NULL)
                        // Vazeni obsahuje tento flag (pokud neobsahuje, ponecham pocty nulove)
                        weighing.WeighingData.SampleList.First(flag);
                        while (weighing.WeighingData.SampleList.Read(flag)) {
                            if (weighing.WeighingData.SampleList.Sample.weight >= weighing.FlockData.Target) {
                                weighing.FlockData.CountAbove++;
                            } else {
                                weighing.FlockData.CountBelow++;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Redraws statistics
        /// </summary>
        /// <param name="flockDefinitionList">List of flock definitions</param>
        /// <param name="fromDate">Additional time filter (or DateTime.MinValue when NA)</param>
        /// <param name="toDate">Additional time filter (or DateTime.MaxValue when NA)</param>
        public void RedrawStatistics(FlockDefinitionList flockDefinitionList, DateTime fromDate, DateTime toDate) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                // Sestavim seznam hejn vcetne vysledku
                flockList = new List<Flock>();
                foreach (FlockDefinition flockDefinition in flockDefinitionList.List) {
                    flockList.Add(new Flock(flockDefinition, fromDate, toDate));
                }

                // Ze seznamu hejn vytvorim seznam jednotlivych vazeni. Seznam prozatim nebude setriden podle data,
                // ale podle hejn (to je treba pro vypocet hodnot hejna jako Day, Target, Difference a Gain).
                // Setrideni podle data provedu az na zaver.
                weighingList = CreateWeighingList(flockList, displayedFlag);

                // Pokud je v seznamu vazeni pouzito vice jednotek, musim vsechna vazeni prepocist na jedny jednotky
                if (!CheckUnits()) {
                    weighingList.Clear();       // Smazu seznam vazeni
                    flockList.Clear();          // Smazu seznam hejn
                    ClearWeighingDetails();
                    // Pokracuju dal v prekresleni
                }

                // Az po prepoctu jednotek muzu sestavit seznamy extra udaju pro hejna
                CalculateFlockData(flockList, displayedFlag);

                // Setridim seznam vazeni podle cisla dne (az po vypoctu dne, cilove hmotnosti a prirustku)
                weighingList.SortByFlockDay();
                
                // Prekreslim
                RedrawStatistics(weighingList);

                // Zobrazim hejna
                ShowFlocks();
            } finally {
                isLoadingWeighings = false;
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Redraws statistics
        /// </summary>
        /// <param name="flockDefinitionList">List of flock definitions</param>
        private void RedrawStatistics(FlockDefinitionList flockDefinitionList) {
            RedrawStatistics(flockDefinitionList, DateTime.MinValue, DateTime.MaxValue);    // Bez casoveho omezeni
        }

        private void DrawTriangle(Graphics graphics, int x, int y, bool isDown) {
            Color triangleColor = Color.FromArgb(80, Color.Black);
            Pen pen = new Pen(triangleColor);
            int yIncrement;
            if (isDown) {
                yIncrement = 1;
            } else {
                yIncrement = -1;
            }
            // FillRectangle nevykresluje presne, radsi kreslim rucne jenotlive cary
            graphics.DrawLine(pen, x, y, x + 4, y);
            y += yIncrement;
            graphics.DrawLine(pen, x + 1, y, x + 3, y);
            y += yIncrement;
            Bitmap pt = new Bitmap(1, 1);
            pt.SetPixel(0, 0, triangleColor);
            graphics.DrawImageUnscaled(pt, x + 2, y);
        }

        private void dataGridWeighings_SelectionChanged(object sender, EventArgs e) {
            if (isLoadingWeighings) {
                // Data se nahravaji, nevykresluju
                return;
            }
            
            // Vykreslim histogram a vzorky vybraneho vazeni
            DisplaySelectedWeighing();
        }

        private void tabControlBottom_Selected(object sender, TabControlEventArgs e) {
            DisplaySelectedWeighing();
        }

        private void splitContainerBottom_Paint(object sender, PaintEventArgs e) {
            // Vykreslim caru a sipky, aby bylo patrne, ze se da graf zvetsit
            SplitContainer splitContainer = (SplitContainer)sender;
            int lineY = splitContainer.SplitterDistance + splitContainer.SplitterWidth / 2;
            Color lineColor = Color.FromArgb(30, Color.Black);
            e.Graphics.DrawLine(new Pen(lineColor), 0, lineY, splitContainer.Width, lineY);
            DrawTriangle(e.Graphics, 0, lineY - 2, false);
            DrawTriangle(e.Graphics, 0, lineY + 2, true);
            DrawTriangle(e.Graphics, splitContainer.Width - 5, lineY - 2, false);
            DrawTriangle(e.Graphics, splitContainer.Width - 5, lineY + 2, true);
        }

        private void splitContainerBottom_SplitterMoved(object sender, SplitterEventArgs e) {
            // Po posunu splitteru musim prekreslit, jinak v nekterych situacich focus zmrsi obsah
            SplitContainer splitContainer = (SplitContainer)sender;
            splitContainer.Invalidate();
        }

        private void splitContainerBottom_Resize(object sender, EventArgs e) {
            // Po zvetseni splitteru musim taky prekreslit
            SplitContainer splitContainer = (SplitContainer)sender;
            splitContainer.Invalidate();
        }

        private void buttonPrintSelectedWeighing_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Pred tiskem musim preskocit na stranku s histogramem, aby se graf inicializoval a spravne vykreslil
            tabControlBottom.SelectedTab = tabPageHistogram;
            tabControlBottom.Refresh();

            // Nactu vysledek prave vybraneho vazeni. Nemuzu predavat primo Weighing, protoze radek TOTAL obsahuje
            // jen vysledek, ne cele vazeni. Je treba mit moznost vytisknout i celkove vysledky.
            int index = dataGridWeighings.SelectedRows[0].Index;
            StatisticResult statisticResult;
            Weighing weighing = null;
            if (index < weighingList.List.Count) {
                statisticResult = weighingList.List[index].WeighingResults.GetResult(displayedFlag);
                weighing = weighingList.List[index];
            } else {
                statisticResult = weighingList.TotalstatisticResults.GetResult(displayedFlag);
            }
            if (statisticResult == null) {
                return;     // Zobrazeny flag v tomto vazeni neni
            }
            
            // Tisknu
            using (Graphics graphics = this.CreateGraphics()) {
	            PrintWeighing printWeighing = new PrintWeighing(graphics);
	            
                // Spolecna data pro vsechny pripady
                printWeighing.StatisticResult = statisticResult;
                printWeighing.Flag = displayedFlag;
                printWeighing.Graph = userControlHistogram.Graph;
                printWeighing.Units = weighingList.List[0].WeighingData.ScaleConfig.Units.Units;    // Z 1. vazeni

                // Data podle toho, zda jde o vazeni nebo TOTAL
                if (index < weighingList.List.Count) {
                    // Vazeni
                    printWeighing.IsTotal  = false;
                    printWeighing.FileName = weighing.WeighingData.File.Name;
                    printWeighing.From     = weighing.WeighingData.SampleList.MinDateTime;
                    printWeighing.To       = weighing.WeighingData.SampleList.MaxDateTime;
                } else {
                    // Total
                    printWeighing.IsTotal = true;
                    printWeighing.FileName = Properties.Resources.TOTAL;
                }

                // Flock nebo vazeni zvlast
                if (flockList != null && !printWeighing.IsTotal) {
                    // Vazeni je soucasti hejna
                    printWeighing.IsFlock    = true;
                    printWeighing.Day        = weighing.FlockData.Day;
                    printWeighing.Target     = weighing.FlockData.Target;
                    printWeighing.Difference = weighing.FlockData.Difference;
                    printWeighing.CountAbove = weighing.FlockData.CountAbove;
                    printWeighing.CountBelow = weighing.FlockData.CountBelow;
                    printWeighing.Gain       = weighing.FlockData.Gain;
                } else {
                    // Bez hejna nebo radek TOTAL (ten nema ani u hejn vyplnene udaje hejna)
                    printWeighing.IsFlock = false;
                }
                
                userControlHistogram.SetForPrinting();
                try {
                    printWeighing.Print();
                } finally {
                    userControlHistogram.SetForScreen();
                }
            }

            // Prekreslim, graf menil rozmery atd.
            userControlHistogram.Refresh();
        }

        private void buttonPrintTable_Click(object sender, EventArgs e) {
            string title = "";
            if (displayedFlag != Flag.ALL) {
                title = Properties.Resources.SEX_LIMIT + ": " + Sample.FlagToString(displayedFlag);
            }
            PrintDGV.Print(dataGridWeighings, Program.PrinterSettings, title, true, true, false);
        }

        private void buttonExportSelectedWeighing_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            int index = dataGridWeighings.SelectedRows[0].Index;
            if (index >= weighingList.List.Count) {
                return;         // Radek total nemuzu exportovat
            }

            // Nactu prave vybrane vazeni
            Weighing weighing = weighingList.List[index];

            // Exportuju, zprumerovane vazeni z hejna nelze exportovat do formatu Bat1
            bool isFlocks = (bool)(weighing.WeighingData.Id < 0);
            new Export(this).ExportWeighing(weighing, displayedFlag, !isFlocks);   
        }

        private void buttonExportAllWeighings_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Ze seznamu vazeni musim vyhazet prumerovana vazeni hejn, ty nelz exportovat.
            // Zalozim novy seznam vazeni, do ktereho nakopiruju pouze vazeni nactene z DB
            List<Weighing> databaseWeighingList = new List<Weighing>();
            bool isFlocks = false;
            foreach (Weighing weighing in weighingList.List) {
                if (weighing.WeighingData.Id < 0) {
                    isFlocks = true;    // Zprumerovane vazeni z hejna nelze exportovat, nastavim flag
                }
                databaseWeighingList.Add(weighing);
            }
            if (databaseWeighingList.Count == 0) {
                return;         // V seznamu nezbylo zadne vazeni
            }
            
            // Exportuju, vysledky z hejn nelze exportovat do formatu Bat1
            new Export(this).ExportWeighingList(databaseWeighingList, displayedFlag, !isFlocks);
        }

        private void buttonExportTable_Click(object sender, EventArgs e) {
            if (dataGridWeighings.Rows.Count == 0) {
                return;         // V tabulce neni zadne vazeni
            }

            if (dataGridWeighings.SelectedCells.Count == 0) {
                return;         // Neni nic vybrane, nemelo by nastat
            }

            // Exportuju
            new Export(this).ExportStatisticsTable(weighingList, displayedFlag, flockList != null);
        }

    }
}
