//*****************************************************************************
//
//    Fonts.c - project fonts
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#include "Fonts.h"

#include "../Moduly/Font/Font8x8.c"
#include "Arial.c"
#include "Courier.c"

const TFontDescriptor const *Fonts[] = {
   /* FONT_8x8 */   &Font8x8,
   /* Arial    */   &FontArial,
   /* Courier  */   &FontCourier,
   /* last     */   0
};   
