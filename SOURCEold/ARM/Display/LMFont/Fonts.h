//*****************************************************************************
//
//    Fonts.h      Project fonts
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Fonts_H__
   #define __Fonts_H__

#ifndef __Font_H__   
   #include "../inc/Font.h"
#endif   

// Fonts enumeration :
typedef enum {
   FONT_8x8,
   FONT_ARIAL,
   FONT_COURIER,
   _FONT_LAST
} TProjectFonts;

// fonts descriptors :
extern const TFontDescriptor const *Fonts[];

#endif
