//*****************************************************************************
//
//   Courier.c  Font Courier
//   Version 1.0
//
//*****************************************************************************

#include "../inc/Font.h"

const byte CourierArray [ 15 * 16]= {
/* 0x00 */ 0x08,0x00,0xC0,0x1F,0xFE,0x73,0x70,0x03,0x76,0x70,0xFE,0xC3,0x1F,0x00,0x00,0x00,
/* 0x01 */ 0x09,0x00,0x60,0x60,0x06,0x66,0x60,0xFF,0xF7,0x7F,0x00,0x06,0x60,0x00,0x06,0x00,
/* 0x02 */ 0x08,0x00,0xC0,0x60,0x0E,0x37,0x78,0xC3,0x36,0x66,0x3E,0xC6,0x61,0x00,0x00,0x00,
/* 0x03 */ 0x08,0x00,0x63,0x70,0x07,0x36,0x63,0x33,0x36,0x67,0xFF,0xE3,0x1C,0x00,0x00,0x00,
/* 0x04 */ 0x08,0xC0,0x00,0x0F,0xF8,0xE0,0x6D,0xC7,0xF6,0x7F,0xFF,0x07,0x6C,0x00,0x00,0x00,
/* 0x05 */ 0x08,0x00,0xF3,0x73,0x3F,0x36,0x63,0x33,0x36,0x63,0xE3,0x03,0x1C,0x00,0x00,0x00,
/* 0x06 */ 0x08,0x00,0x80,0x1F,0xFC,0xE3,0x66,0x37,0x36,0x63,0xF3,0x37,0x3E,0x00,0x00,0x00,
/* 0x07 */ 0x08,0x00,0x70,0x00,0x07,0x30,0x00,0x03,0x37,0x7F,0xFF,0xF0,0x00,0x00,0x00,0x00,
/* 0x08 */ 0x08,0x00,0xE0,0x3C,0xFF,0x37,0x63,0x33,0x36,0x63,0xFF,0xE7,0x3C,0x00,0x00,0x00,
/* 0x09 */ 0x08,0x00,0xE0,0x63,0x7F,0x36,0x66,0x63,0x37,0x3B,0xFE,0xC1,0x0F,0x00,0x00,0x00,
/* 0x0A */ 0x04,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
/* 0x0B */ 0x09,0x00,0x00,0x00,0x60,0x00,0x06,0x60,0x00,0x06,0x60,0x00,0x06,0x60,0x00,0x00,
/* 0x0C */ 0x07,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x06,0x60,0x00,0x06,0x00,0x00,0x00,0x00,
/* 0x0D */ 0x07,0x00,0x00,0x00,0x00,0x00,0x00,0x18,0x86,0x61,0x18,0x06,0x00,0x00,0x00,0x00,
/* 0x0E */ 0x0A,0x60,0x00,0x06,0x60,0x00,0x06,0xFE,0xE7,0x7F,0x60,0x00,0x06,0x60,0x00,0x06,
};

const TFontDescriptor FontCourier = {
   /* Type     */ FONT_NUMERIC,
   /* Width    */ 10,
   /* Height   */ 12,
   /* Offset   */ 48,
   /* Size     */ 16,
   /* AvgWidth */ 8,
   /* HSpace   */ 1,
   /* VSpace   */ 0,
   /* Array    */ CourierArray
};

