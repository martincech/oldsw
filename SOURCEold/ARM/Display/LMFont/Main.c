//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Olimex
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard

#include "../inc/Graphic.h"   // graphic
#include "Fonts.h"            // seznam fontu
#include "../inc/conio.h"     // Display

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s


static void LedFlash( native ms);
// Flash led for <ms> miliseconds

static int GetEvent( void);
// Returns keyboard event

static void GpuTest( native Mode);
// Test GPU

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   KbdInit();
   GInit();
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   GSetFont( FONT_8x8);
   i = 0;
   forever {
      switch( GetEvent()){
         case K_ENTER :
         case K_ENTER | K_REPEAT :
            GpuTest( i);
            GFlush();
            i++;
            if( i > 2){
               i = 0;
            }
            break;              
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LedsOn();
   SysDelay( ms);
   LedsOff();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Get event
//------------------------------------------------------------------------------

static int GetEvent( void)
// Returns keyboard event
{
int Key;
static TYesNo Toggle = NO;

   forever {
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         if( Toggle){
            Led0On();
         } else {
            Led0Off();
         }
         Toggle = !Toggle;
      }
      Key = KbdGet();
      if( Key != K_IDLE){
         return( Key);
      }
   }
} // GetEvent

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

static const char *DemoText = "Ahoj my !\nAhoj ja !\nAhoj on !";

static void GpuTest( native Mode)
// Test GPU
{
   switch( Mode){
      case 0 :
         GClear();
         GSetFont( FONT_ARIAL);
         cgoto( 0, 0);
         cputs( "Ahoj ty!\n");
         cputs( "Ahoj vy!\n");
         cputs( "Ahoj oni!\n");
         GSetFixedPitch();
         cputs( "Ahoj ty!\n");
         cputs( "Ahoj vy!\n");
         cputs( "Ahoj oni!\n");
         GSetNormalPitch();
         return;

      case 1 :
         GClear();
         GSetFont( FONT_COURIER);
         cgoto( 0, 0);
         cputs( "0123456789\n");
         cputs( "0 1-2.3:4+5\n");
         GSetFixedPitch();
         cputs( "0123456789\n");
         cputs( "0 1-2.3:4+5\n");
         GSetNormalPitch();
         return;

      case 2 :
         GClear();
         GSetFont( FONT_ARIAL);
         GTextAt( G_WIDTH / 2  - GTextWidth( DemoText) / 2, 
                  G_HEIGHT / 2 - GTextHeight( DemoText) / 2);
         cputs( DemoText);
         return;
   
      default :
         return;
   }
} // GpuTest
