//*****************************************************************************
//
//    Sed1335.c - SED 1335F LCD display services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#define __DEBUG__

#include "..\inc\System.h"
#include "..\inc\Sed1335.h"
#include <stdio.h>

// Prikazy pro radic SED 1335F :

//-----------------------------------------------------------------------------
// SYSTEM_SET
//-----------------------------------------------------------------------------
#define DISPLAY_CMD_SYSTEM_SET 0x40          // inicializace a zakladni nastaveni
// P1
#define DISPLAY_MASK_P1         0x10          // zakladni maska, povinna !
#define DISPLAY_MASK_M0         0x01          // external CG ROM
#define DISPLAY_MASK_M1         0x02          // 64 char CG RAM/bit 6 correction on
#define DISPLAY_MASK_M2         0x04          // 16 pixel char height
#define DISPLAY_MASK_WS         0x08          // dual-panel display
#define DISPLAY_MASK_IV         0x20          // No screen top-line correction
#define DISPLAY_MASK_TL         0x40          // TV mode
#define DISPLAY_MASK_DR         0x80          // additional shift-clock cycle
// P2
#define DISPLAY_MASK_FX         0x07          // sirka znaku - 1
#define DISPLAY_MASK_WF         0x80          // two-frame AC drive waveform
// P3
#define DISPLAY_MASK_FY         0x0F          // vyska znaku - 1
// P4
#define DISPLAY_MASK_CR         0xFF          // bytu na radku - 1, 0..239
// P5
#define DISPLAY_MASK_TCR        0xFF          // celkova delka radku v "bytech" - 1, 0..255
// P6
#define DISPLAY_MASK_LF         0xFF          // pocet grafickych radku - 1, 0..255
// P7
#define DISPLAY_MASK_APL        0xFF          // dolni byte poctu bytu na virtualni radek
// P8
#define DISPLAY_MASK_APH        0xFF          // horni byte poctu bytu na virtualni radek

//-----------------------------------------------------------------------------
// SLEEP IN
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_SLEEP_IN    0x53          // uspani displeje

// vzbuzeni pomoci prvnich 2 bytu SYSTEM_SET

//-----------------------------------------------------------------------------
// DISP ON/OFF
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_DISP_OFF    0x58            // vypinani displeje, nastaveni kurzoru a rovin
#define DISPLAY_CMD_DISP_ON     0x59            // zapinani displeje, -"-
// P1
#define DISPLAY_MASK_CURSOR_OFF    0x00         // vypnuti kurzoru
#define DISPLAY_MASK_CURSOR_ON     0x01         // svitici kurzor
#define DISPLAY_MASK_CURSOR_FLASH2 0x02         // kurzor blikajici 2Hz
#define DISPLAY_MASK_CURSOR_FLASH1 0x03         // kurzor blikajici 1Hz

#define DISPLAY_MASK_SAD1_OFF      0x00         // vypnuti roviny SAD1
#define DISPLAY_MASK_SAD1_ON       0x04         // zapnuti roviny SAD1
#define DISPLAY_MASK_SAD1_FLASH2   0x08         // blikani roviny SAD1 2Hz
#define DISPLAY_MASK_SAD1_FLASH16  0x0C         // blikani roviny SAD1 16Hz

#define DISPLAY_MASK_SAD2_OFF      0x00         // vypnuti roviny SAD2
#define DISPLAY_MASK_SAD2_ON       0x10         // zapnuti roviny SAD2
#define DISPLAY_MASK_SAD2_FLASH2   0x20         // blikani roviny SAD2 2Hz
#define DISPLAY_MASK_SAD2_FLASH16  0x30         // blikani roviny SAD2 16Hz

#define DISPLAY_MASK_SAD3_OFF      0x00         // vypnuti roviny SAD3
#define DISPLAY_MASK_SAD3_ON       0x40         // zapnuti roviny SAD3
#define DISPLAY_MASK_SAD3_FLASH2   0x80         // blikani roviny SAD3 2Hz
#define DISPLAY_MASK_SAD3_FLASH16  0xC0         // blikani roviny SAD3 16Hz

//-----------------------------------------------------------------------------
// SCROLL
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_SCROLL         0x44         // definice rolovacich oblasti
// P1
#define DISPLAY_MASK_SAD1L         0xFF         // dolni adresa oblasti SAD1
// P2
#define DISPLAY_MASK_SAD1H         0xFF         // horni adresa oblasti SAD1
// P3
#define DISPLAY_MASK_SL1           0xFF         // pocet radku oblasti SAD1 - 1

// P4
#define DISPLAY_MASK_SAD2L         0xFF         // dolni adresa oblasti SAD2
// P5
#define DISPLAY_MASK_SAD2H         0xFF         // horni adresa oblasti SAD2
// P6
#define DISPLAY_MASK_SL2           0xFF         // pocet radku oblasti SAD2 - 1

// P7
#define DISPLAY_MASK_SAD3L         0xFF         // dolni adresa oblasti SAD3
// P8
#define DISPLAY_MASK_SAD3H         0xFF         // horni adresa oblasti SAD3

// jen pro WS = 1 a dve roviny :
// P9
#define DISPLAY_MASK_SAD4L         0xFF         // dolni adresa oblasti SAD4
// P10
#define DISPLAY_MASK_SAD4H         0xFF         // horni adresa oblasti SAD4

//-----------------------------------------------------------------------------
// CSRFORM
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CSRFORM        0x5D        // nastavi velikost a tvar kurzoru
// P1
#define DISPLAY_MASK_CRX           0x0F        // sirka kurzoru v pixelech - 1
// P2
#define DISPLAY_MASK_CRY           0x0F        // vyska kurzoru v pixelech - 1
#define DISPLAY_MASK_CM            0x80        // blokovy kurzor, v grafice vzdy nastavit

//-----------------------------------------------------------------------------
// CGRAM_ADR
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CGRAM_ADR      0x5C        // nastaveni pocatecni adresy generatoru znaku
// P1
#define DISPLAY_MASK_SAGL          0xFF        // dolni cast adresy
// P2
#define DISPLAY_MASK_SAGH          0xFF        // horni cast adresy
// POZOR, adresa plati pro znak 0x00, prvni uzivatelsky znak je 0x80
// ofset prvniho uzivatelskeho znaku je 0x80 krat pocet radku na znak

//-----------------------------------------------------------------------------
// HDOT_SCR
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_HDOT_SCR       0x5A        // horizontalni rolovani po pixelech
// P1
#define DISPLAY_MASK_HDOT_PIXEL    0x07        // pocet pixelu horizontalniho posunu

//-----------------------------------------------------------------------------
// OVLAY
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_OVLAY          0x5B        // rizeni prekryti rovin
// P1
#define DISPLAY_MASK_OR            0x00        // OR  rovin 1 a 2, OR 3
#define DISPLAY_MASK_XOR           0x01        // XOR rovin 1 a 2, OR 3
#define DISPLAY_MASK_AND           0x02        // AND rovin 1 a 2, OR 3
#define DISPLAY_MASK_POR           0x03        // Prioritni OR rovin 1,2 a 3

#define DISPLAY_MASK_TEXT          0x00        // rovina 1 v textovem modu
#define DISPLAY_MASK_GRAPHICS      0x0C        // rovina 1 v grafickem modu
#define DISPLAY_MASK_GRAPHICS3     0x1C        // tri graficke roviny

//-----------------------------------------------------------------------------
// CSRW
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CSRW           0x46        // zapis pozice ukazatele do videopameti
// P1
#define DISPLAY_MASK_CSRL          0xFF        // dolni cast adresy ukazatele
// P2
#define DISPLAY_MASK_CSRH          0xFF        // horni cast adresy ukazatele (nepovinna)

//-----------------------------------------------------------------------------
// CSRR
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_CSRR           0x47        // cteni pozice ukazatele do videopameti
// P1 viz CSRW
// P2 viz CSRW
// oba parametry se ctou (!) v datovem modu

//-----------------------------------------------------------------------------
// MREAD
//-----------------------------------------------------------------------------

#define DISPLAY_CMD_MREAD          0x43      // cteni videopameti
// P1..PN ctene byty, inkrement/dekrement adresy probiha podle CSRDIR

//-----------------------------------------------------------------------------


// Konstanty pro displej POWERTIP PG320240 :

//#define DISPLAY_WIDTH   320                   // graficke rozliseni v pixelech vodorovne
//#define DISPLAY_HEIGHT  240                   // graficke rozliseni v pixelech svisle
#define DISPLAY_X_BYTE  (DISPLAY_WIDTH / 8)   // pocet bytu videoram na radek
#define DISPLAY_CR_SPACE 18                   // pocet bytu horizontalniho zatemneni


#define DISPLAY_CHAR_WIDTH  8                                  // sirka pole pro znak
#define DISPLAY_CHAR_HEIGHT 8                                  // vyska pole pro znak

// Rozdeleni oblasti videopameti :

#define DISPLAY_SAD1_START  0                   // startovaci adresa prvni roviny
#define DISPLAY_SAD1_HEIGHT DISPLAY_HEIGHT      // vyska prvni roviny

#elif DISPLAY_MODE_GRAPHICS2
   #define DISPLAY_SAD2_START (DISPLAY_X_BYTE * DISPLAY_HEIGHT) // pocet bytu na strance
   #define DISPLAY_SAD3_START  0                                // nepouzito
   #define DISPLAY_SAD4_START  0                                // nepouzito
#elif DISPLAY_MODE_GRAPHICS3
   #define DISPLAY_SAD2_START (DISPLAY_X_BYTE * DISPLAY_HEIGHT)      // pocet bytu na strance
   #define DISPLAY_SAD3_START (2 * DISPLAY_X_BYTE * DISPLAY_HEIGHT)  // pocet bytu na predchozich strankach
   #define DISPLAY_SAD4_START  0                                     // nepouzito
#else
   #error "Undefined graphics mode (DISPLAY_MODE_...)"
#endif
#define DISPLAY_SAD2_HEIGHT DISPLAY_HEIGHT

// Kontextove promenne pro kresleni :
bit    _plane;                         // cislo graficke roviny
static word   _offset;                 // ofset do videopameti, odpovidajici <x,y,plane>
static TFontDescriptor code *_font;    // pracovni font
static bit plane1_on;
static bit plane2_on;

// Lokalni funkce :

#define WriteData( d) DisplayWriteData( d)
// zapise do displeje data

static void ClearRAM( void);
// Smaze vsechny roviny RAM

static void VideoRAMSet( word start, word length, char ch);
// Nastavi oblast video RAM na zadanou hodnotu. POZOR, pred volanim musi byt
// nastaven smysl pohybu kurzoru

//-----------------------------------------------------------------------------
// Implementacni poznamky :
// - Vodic A0 je trvale drzen v 0 (datovy mod), jen po dobu vyslani prikazu
//   se nastavuje do 1. Bude-li vadit, muze se shozeni A0 schovat do makra
//   DisplayEnable() a nahozeni do DisplayDisable()
// - Vyberovy vodic /CS displeje je trvale v 1 (neaktivni), po dobu volani
//   funkce se aktivuje
// - Implicitni pohyb kurzoru je vpravo, pokud jej funkce zmeni, je treba jej
//   vratit

//-----------------------------------------------------------------------------
// Inicializace displeje
//-----------------------------------------------------------------------------

void DisplayInit( void)
// inicializuje display
{
   DisplayReset();      // shodit /RES
   SysDelay( 1);        // delka impulsu min 200us
   DisplayRun();        // nahodit /RES
   SysDelay( 3);        // cekani na pripravenost 3ms
   DisplayEnable();     // shodit /CS
   //------------- SYSTEM_SET :
   DisplayWriteCommand( DISPLAY_CMD_SYSTEM_SET);
   WriteData( DISPLAY_MASK_P1 |
              DISPLAY_MASK_M1 |
              DISPLAY_MASK_IV
            );         // P1 - Internal ROM, 64 CG RAM, Height 8, single-panel,
                       // no top-line correction, LCD mode, no additional cycles
   WriteData( DISPLAY_MASK_WF |
             (DISPLAY_MASK_FX  & (DISPLAY_CHAR_WIDTH - 1))
            );         // P2 - sirka znaku, two frame waveform period
   WriteData( DISPLAY_MASK_FY  & (DISPLAY_CHAR_HEIGHT - 1)
            );         // P3 - vyska znaku
   WriteData( DISPLAY_MASK_CR  & (DISPLAY_X_BYTE - 1));
                       // P4 - sirka radku v bytech - 1
   WriteData( DISPLAY_MASK_TCR & (DISPLAY_X_BYTE + DISPLAY_CR_SPACE - 1));
                       // P5 - celkova sirka radku vcetne zatemneni
   WriteData( DISPLAY_MASK_LF  & (DISPLAY_HEIGHT - 1));
                       // P6 - vyska displeje v pixelech - 1
   WriteData( DISPLAY_MASK_APL & DISPLAY_X_BYTE);
                       // P7 - LSB sirka virtualniho radku v bytech
   WriteData( DISPLAY_MASK_APH & (DISPLAY_X_BYTE >> 8));
                       // P8 - MSB -"-
   //------------- SCROLL :
   DisplayWriteCommand( DISPLAY_CMD_SCROLL);
   WriteData( DISPLAY_MASK_SAD1L & DISPLAY_SAD1_START);
                       // P1 - SAD1L
   WriteData( DISPLAY_MASK_SAD1H & (DISPLAY_SAD1_START >> 8));
                       // P2 - SAD1H
   WriteData( DISPLAY_MASK_SL1   & (DISPLAY_SAD1_HEIGHT - 1));
                       // P3 - SL1
   WriteData( DISPLAY_MASK_SAD2L & DISPLAY_SAD2_START);
                       // P4 - SAD2L
   WriteData( DISPLAY_MASK_SAD2H & (DISPLAY_SAD2_START >> 8));
                       // P5 - SAD2H
   WriteData( DISPLAY_MASK_SL2   & (DISPLAY_SAD2_HEIGHT - 1));
                       // P6 - SL2
   WriteData( DISPLAY_MASK_SAD3L & DISPLAY_SAD3_START);
                       // P7 - SAD3L
   WriteData( DISPLAY_MASK_SAD3H & (DISPLAY_SAD3_START >> 8));
                       // P8 - SAD3H
   WriteData( DISPLAY_MASK_SAD4L & DISPLAY_SAD4_START);
                       // P9 - SAD4L
   WriteData( DISPLAY_MASK_SAD4H & (DISPLAY_SAD4_START >> 8));
                       // P10 - SAD4H
   //------------- HDOT_SCR :
   DisplayWriteCommand( DISPLAY_CMD_HDOT_SCR);
   WriteData( DISPLAY_MASK_HDOT_PIXEL & 0);
                        // P1 - pocet pixelu horizontalniho posunu
   #elif DISPLAY_MODE_GRAPHICS2
      //------------- OVLAY :
      DisplayWriteCommand( DISPLAY_CMD_OVLAY);
      WriteData( DISPLAY_MASK_GRAPHICS |
                 DISPLAY_MASK_XOR);
                                 // P1 - SAD1 grafika, grafika XORovana se SAD1, 2 roviny
   #elif DISPLAY_MODE_GRAPHICS3
      //------------- OVLAY :
      DisplayWriteCommand( DISPLAY_CMD_OVLAY);
      WriteData( DISPLAY_MASK_GRAPHICS3 |
                 DISPLAY_MASK_XOR);
                                 // P1 - SAD1 grafika, grafika XORovana se SAD1, 3 roviny
   #endif
   //------------- DISP_OFF
   DisplayWriteCommand( DISPLAY_CMD_DISP_OFF);
   //------------- CSRDIR :
   DisplayWriteCommand(DISPLAY_CMD_CSR_DOWN);     // Pohyb kurzoru dolu
//   DisplayWriteCommand( DISPLAY_CMD_CSR_RIGHT);
   ClearRAM();
   #elif DISPLAY_MODE_GRAPHICS2
      //------------- DISP_ON :
      DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
      WriteData( DISPLAY_MASK_CURSOR_OFF |
                 DISPLAY_MASK_SAD1_ON  |
                 DISPLAY_MASK_SAD2_ON  |
                 DISPLAY_MASK_SAD3_OFF
               );
                                 // P1 - Vypnuty kurzor, zapnuty roviny SAD1, SAD2
   #elif DISPLAY_MODE_GRAPHICS3
      //------------- DISP_ON :
      DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
      WriteData( DISPLAY_MASK_CURSOR_OFF |
                 DISPLAY_MASK_SAD1_ON  |
                 DISPLAY_MASK_SAD2_ON  |
                 DISPLAY_MASK_SAD3_ON
               );
                                 // P1 - Vypnuty kurzor, zapnuty roviny SAD1, SAD2, SAD3
   #endif

   // Nastaveni implicitniho grafickeho fontu :
   DisplaySetFont( 0);
   DisplaySetPlane( DISPLAY_PLANE2);
   DisplayPlane( YES);
   DisplaySetPlane( DISPLAY_PLANE1);
   DisplayPlane( YES);
   // zapnuty obe roviny :
   plane1_on = 1;
   plane2_on = 1;

   DisplayDisable();    // nahodit /CS
} // DisplayInit

//-----------------------------------------------------------------------------
// Offset adresy podle souradnic
//-----------------------------------------------------------------------------

#ifdef DISPLAY_OFFSET

word DisplayOffset(byte X, byte Y) {
  // Offset adresy podle souradnic
  return (word)(X + (_plane ? DISPLAY_SAD2_START : DISPLAY_SAD1_START) + Y * DISPLAY_X_BYTE);
}

#endif

//-----------------------------------------------------------------------------
// Nastaveni roviny
//-----------------------------------------------------------------------------

void DisplaySetPlane( bit plane)
// nastavi pracovni zobrazovaci rovinu
{
   _plane = plane;
} // DisplaySetPlane


//-----------------------------------------------------------------------------
// Nastaveni fontu
//-----------------------------------------------------------------------------

void DisplaySetFont( byte font)
// nastavi pracovni font
{
//   _font = &Font[ font];
   switch (font) {
#ifdef DISPLAY_FONT_SYSTEM
     case FONT_SYSTEM: {
       _font = &FontSystem;
       break;
     }
#endif
#ifdef DISPLAY_MODE_MIXED
     case FONT_FRAME_8x8: {
       _font = &FontFrame;
       break;
     }
#endif
#ifdef DISPLAY_FONT_NUM_MYRIAD40
     case FONT_NUM_MYRIAD40: {
       _font = &FontNumericMyriad40;
       break;
     }
#endif
#ifdef DISPLAY_FONT_NUM_MYRIAD32
     case FONT_NUM_MYRIAD32: {
       _font = &FontNumericMyriad32;
       break;
     }
#endif
#ifdef DISPLAY_FONT_NUM_TAHOMA18
     case FONT_NUM_TAHOMA18: {
       _font = &FontNumericTahoma18;
       break;
     }
#endif
   }//switch
} // DisplaySetFont

//-----------------------------------------------------------------------------
// Nastaveni pozice
//-----------------------------------------------------------------------------
#ifdef DISPLAY_MOVE_TO

void DisplayMoveTo( byte x, byte y)
// presun pozice kurzoru <x> je pocet osmic pixelu, <y> je pocet pixelu
{
#elif DISPLAY_MODE_GRAPHICS2
   _offset  = (plane ? DISPLAY_SAD2_START : DISPLAY_SAD1_START) // zacatek roviny
               + y * DISPLAY_X_BYTE;                            // zacatek radku y
   _offset += x;                                                // sloupec x
#elif DISPLAY_MODE_GRAPHICS3
#endif
} // DisplayMoveTo

#endif // DISPLAY_MOVE_TO

//-----------------------------------------------------------------------------
// Mazani roviny
//-----------------------------------------------------------------------------
#ifdef DISPLAY_CLEAR

void DisplayClear( void)
// Smaze nastavenou rovinu
{
   DisplayEnable();
#elif DISPLAY_MODE_GRAPHICS2
      // Maze obe roviny!
      // nastaveni graficke roviny SAD1 na 0 :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
#elif DISPLAY_MODE_GRAPHICS3
#endif
   DisplayDisable();
} // DisplayClear
#endif  // DISPLAY_CLEAR

//-----------------------------------------------------------------------------
// Zobrazeni roviny
//-----------------------------------------------------------------------------
#ifdef DISPLAY_PLANE

void DisplayPlane( bit on)
// Podle promenne <on> zapne/vypne nastavenou rovinu
{

   DisplayEnable();
   if( !plane){
      plane1_on = on;
   } else {
      plane2_on = on;
   }
   //------------- DISP_ON :
   DisplayWriteCommand( DISPLAY_CMD_DISP_ON);
   WriteData( DISPLAY_MASK_CURSOR_OFF |         // v grafice vypnuty
              (plane1_on ? DISPLAY_MASK_SAD1_ON : DISPLAY_MASK_SAD1_OFF) |
              (plane2_on ? DISPLAY_MASK_SAD2_ON : DISPLAY_MASK_SAD2_OFF) |
              DISPLAY_MASK_SAD3_OFF
            );
   DisplayDisable();
} // DisplayPlane

#endif // DISPLAY_PLANE

//-----------------------------------------------------------------------------
// Obdelnik
//-----------------------------------------------------------------------------
#ifdef DISPLAY_FILL_BOX

void DisplayFillBox( byte w, byte h, byte value)
// Nakresli obdelnik do nastavene roviny od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech, <value>
// je hodnota osmice
{
byte i, j;

   DisplayEnable();
//   DisplayWriteCommand( DISPLAY_CMD_CSR_DOWN);                 // pohyb kurzoru dolu
   for( j = 0; j < w; j++){
      // jeden sloupec
      SetCursor( _offset);
      DisplayWriteCommand( DISPLAY_CMD_MWRITE);
      for( i = 0; i < h; i++){
         WriteData( value);
      }
      _offset++;                                         // dalsi sloupec
   }
//   DisplayWriteCommand( DISPLAY_CMD_CSR_RIGHT);                 // standardni smer pohybu
   DisplayDisable();
} // DisplayBox

#endif // DISPLAY_FILL_BOX

//-----------------------------------------------------------------------------
// Zapis obdelnikoveho vzoru
//-----------------------------------------------------------------------------
#ifdef DISPLAY_PATTERN

void DisplayPattern( byte code *pattern, byte w, byte h)
// Nakresli obdelnik vyplneny vzorem z adresy <pattern>
// do nastavene roviny od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech
{
byte i, j;

   DisplayEnable();
//   DisplayWriteCommand( DISPLAY_CMD_CSR_DOWN);                 // pohyb kurzoru dolu
   for( j = 0; j < w; j++){
      // jeden sloupec
      SetCursor( _offset);
      DisplayWriteCommand( DISPLAY_CMD_MWRITE);
      for( i = 0; i < h; i++){
         WriteData( *pattern);
         pattern++;
      }
      _offset++;                                         // dalsi sloupec
   }
//   DisplayWriteCommand( DISPLAY_CMD_CSR_RIGHT);                 // standardni smer pohybu
   DisplayDisable();
} // DisplayPattern

#endif // DISPLAY_PATTERN


//-----------------------------------------------------------------------------
// Zapis prikazu
//-----------------------------------------------------------------------------

void DisplayWriteCommand( byte cmd)
// zapise do displeje prikaz
{
   DisplayAddressCommand();                    // nahodit A0
   DisplayWriteData( cmd);
   DisplayAddressData();                       // shodit A0, nasleduji parametry
} // WriteCommand

//-----------------------------------------------------------------------------
// Mazani RAM
//-----------------------------------------------------------------------------

static void ClearRAM( void)
// Smaze vsechny roviny RAM
{
   #elif DISPLAY_MODE_GRAPHICS2
      // nastaveni graficke roviny SAD1 na 0 :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
   #elif DISPLAY_MODE_GRAPHICS3
      // nastaveni graficke roviny SAD1 na 0 :
      VideoRAMSet( DISPLAY_SAD1_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD2 na 0 :
      VideoRAMSet( DISPLAY_SAD2_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
      // nastaveni graficke roviny SAD3 na 0 :
      VideoRAMSet( DISPLAY_SAD3_START, DISPLAY_X_BYTE * DISPLAY_HEIGHT, 0);
   #endif
} // ClearRAM

//-----------------------------------------------------------------------------
// Nastaveni ukazatele RAM
//-----------------------------------------------------------------------------

void SetCursor( word address)
// Nastavi adresu do video RAM
{
   DisplayWriteCommand( DISPLAY_CMD_CSRW);
   WriteData( DISPLAY_MASK_CSRL & address);
   WriteData( DISPLAY_MASK_CSRH & (address >> 8));
} // SetCursor

//-----------------------------------------------------------------------------
// Nastaveni RAM na hodnotu
//-----------------------------------------------------------------------------

static void VideoRAMSet( word start, word length, char ch)
// Nastavi oblast video RAM na zadanou hodnotu. POZOR, pred volanim musi byt
// nastaven smysl pohybu kurzoru
{
word i;

   DisplayWriteCommand(DISPLAY_CMD_CSR_RIGHT);
   SetCursor( start);
   //------- MWRITE
   DisplayWriteCommand( DISPLAY_CMD_MWRITE);
   for( i = 0; i < length; i++){
      WriteData( ch);
   }
   DisplayWriteCommand(DISPLAY_CMD_CSR_DOWN);
} // VideoRAMSet

