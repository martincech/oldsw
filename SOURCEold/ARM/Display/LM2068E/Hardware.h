//*****************************************************************************
//
//    Hardware.h - Olimex hardware definitions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "../inc/Uni.h"
#include "../inc/Lpc213x.h"
#include "../inc/Lpc.h"

#define __LPC213x__

//-----------------------------------------------------------------------------
//   Prirazeni portu
//-----------------------------------------------------------------------------

//------------------------------------------------------------------------------
//   Parametry CPU
//------------------------------------------------------------------------------

#define FXTAL    14745600              // XTAL frequency 14.7456 MHz

#define FCPU     FXTAL                 // system frequency
#define FBUS     (FCPU / 4)            // peripheral bus frequency

#define MAM_MODE MAMCR_DISABLED        // Disable flash cache

//------------------------------------------------------------------------------
//   Parametry systemu
//------------------------------------------------------------------------------

#define WATCHDOG_INTERVAL 2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]

// mapa preruseni :
#define SYS_TIMER_IRQ     IRQ0         // System timer IRQ number
#define UART0_IRQ         IRQ1         // UART0 IRQ number
#define UART1_IRQ         IRQ2         // UART1 IRQ number

//------------------------------------------------------------------------------
//   LED
//------------------------------------------------------------------------------

// Port :
#define LED_DIR  GPIO0_IODIR
#define LED_PIN  GPIO0_IOPIN
#define LED_SET  GPIO0_IOSET
#define LED_CLR  GPIO0_IOCLR 
// Piny :
#define LED_D0   (1 << 12)
#define LED_D1   (1 << 13)

#define LedInit() LED_DIR |=  LED_D0 | LED_D1; LedsOff()
#define LedsOff() LED_SET  =  LED_D0 | LED_D1
#define LedsOn()  LED_CLR  =  LED_D0 | LED_D1

#define Led0On()  GPIO_IOCLR = LED_D0
#define Led0Off() GPIO_IOSET = LED_D0
#define Led1On()  GPIO_IOCLR = LED_D1
#define Led1Off() GPIO_IOSET = LED_D1

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_BAUD           9600       // Rychlost linky v baudech

//#define UART0_PARITY_EVEN  1          // Suda parita
//#define UART0_PARITY_ODD   1          // Licha parita

#define UART0_RX_TIMEOUT     10         // Meziznakovy timeout [ms]

// POZOR, nasledujici konstanty musi byt v binarni rade (1,2,4,8...) :
#define UART0_RX_BUFFER_SIZE    16      // prijimaci buffer
#define UART0_TX_BUFFER_SIZE    16      // vysilaci buffer

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define UART1_BAUD           9600       // Rychlost linky v baudech

//#define UART1_PARITY_EVEN  1          // Suda parita
//#define UART1_PARITY_ODD   1          // Licha parita

#define UART1_RX_TIMEOUT     10         // Meziznakovy timeout [ms]

// POZOR, nasledujici konstanty musi byt v binarni rade (1,2,4,8...) :
#define UART1_RX_BUFFER_SIZE    32      // prijimaci buffer
#define UART1_TX_BUFFER_SIZE    32      // vysilaci buffer

//------------------------------------------------------------------------------
// Klavesnice
//------------------------------------------------------------------------------

#define KBD_DIR          GPIO0_IODIR   // port direction
#define KBD_PIN          GPIO0_IOPIN   // port input data

#define KBD_K1           (1 << 15)     // K1

// definice klaves :
typedef enum {
   // systemove klavesy :
   K_NULL  = 0,                        // vyhrazeno k pouziti pro menu & okna
   _K_FIRSTUSER,

   // klavesy, v zavislosti na konstrukci klavesnice :
   K_ENTER = _K_FIRSTUSER,             // Enter

   // systemove klavesy :
   K_REPEAT       = 0x80,              // opakovani klavesy OR ke klavese
   K_RELEASED     = 0xFE,              // pusteni klavesy (jednotlive i autorepeat)
   K_IDLE         = 0xFF               // vyhrazeno k internimu pouziti, prazdny cyklus cteni
} TKeys;

// definice casovych konstant [ms] :
#define KBD_DEBOUNCE           20      // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START   500     // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED   200     // kadence autorepeat

//------------------------------------------------------------------------------
// Radic displeje S1D13700
//------------------------------------------------------------------------------

// porty :
#define GPU_DIR          GPIO0_IODIR   // port direction
#define GPU_PIN          GPIO0_IOPIN   // port input data
#define GPU_SET          GPIO0_IOSET   // port output set
#define GPU_CLR          GPIO0_IOCLR   // port output clear

#define GPU_DATA_DIR     GPIO0_IODIR   // port direction
#define GPU_DATA_PIN     GPIO0_IOPIN   // port input data
#define GPU_DATA_SET     GPIO0_IOSET   // port output set
#define GPU_DATA_CLR     GPIO0_IOCLR   // port output clear

// piny :
#define GPU_DATA_OFFSET  16            // LSB sbernice D0..D7
#define GPU_CS           (1 << 26)     // /CS
#define GPU_RES          (1 << 28)     // /RES
#define GPU_RD           (1 << 29)     // /RD
#define GPU_WR           (1 << 30)     // /WR
#define GPU_A0           (1 << 31)     // A0

#define GpuInitPorts()  GPU_DIR |= GPU_CS | GPU_RES | GPU_RD | GPU_WR | GPU_A0
#define GpuClearAll()   GpuClrRES(); GPU_SET  = GPU_CS | GPU_RD | GPU_WR

// chipselect :
#define GpuSelect()     GPU_CLR  = GPU_CS
#define GpuDeselect()   GPU_SET  = GPU_CS
// control signals :
#define GpuSetRES()     GPU_SET = GPU_RES
#define GpuClrRES()     GPU_CLR = GPU_RES
#define GpuSetRD()      GPU_SET = GPU_RD
#define GpuClrRD()      GPU_CLR = GPU_RD
#define GpuSetWR()      GPU_SET = GPU_WR
#define GpuClrWR()      GPU_CLR = GPU_WR
#define GpuSetA0()      GPU_SET = GPU_A0
#define GpuClrA0()      GPU_CLR = GPU_A0

#define GPU_DISABLE_BUFFER      1      // nepouzivat buffer

//------------------------------------------------------------------------------
// Grafika
//------------------------------------------------------------------------------

#define G_WIDTH          320           // sirka displeje (X)
#define G_HEIGHT         240           // vyska displeje (Y)

//#define GRAPHIC_CONIO      1           // preklad funkci conio.h

//------------------------------------------------------------------------------
#endif
