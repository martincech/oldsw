//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Olimex
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard

#include "../inc/Lm2068e.h"   // Display

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

static void LedFlash( native ms);
// Flash led for <ms> miliseconds

static void GpuTest( native Mode);
// Test GPU

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
TYesNo Toggle;
byte   Char;
TYesNo Draw;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   KbdInit();
   GpuInit();
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   Toggle = NO;
   Draw   = YES;
   forever {
      Char = 3;
      if( kbhit()){
         switch( getch()){
            case K_ENTER :
               if( Draw){
                  GpuTest( Char);
               } else {
                  GpuClear();
               }
/*
               if( Draw){
                  GpuOn();
               } else {
                  GpuOff();
               }
*/
               Draw = !Draw;
               break;              
         }
      }
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         if( Toggle){
            Led0On();
         } else {
            Led0Off();
         }
         Toggle = !Toggle;
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LedsOn();
   SysDelay( ms);
   LedsOff();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

static void GpuTest( native Mode)
// Test GPU
{
native Row, Col;
native i;

   switch( Mode){
      case 0 :
         for( i = 0; i < 100; i++){
             for( Row = 0; Row < GPU_MAX_ROW; Row++){
                GpuGoto( Row, 0);
                for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                   if( Col & 1){
                      GpuWrite( 0xAA);
                   } else {
                      GpuWrite( 0x55);
                   }
                }
             }
             GpuDone();
             for( Row = 0; Row < GPU_MAX_ROW; Row++){
                GpuGoto( Row, 0);
                for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                   if( Col & 1){
                      GpuWrite( 0x55);
                   } else {
                      GpuWrite( 0xAA);
                   }
                }
             }
             GpuDone();
             WatchDog();
          }
          GpuClear();
          return;
   
       case 1 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( 0xFF);
             }
          }
          GpuDone();
          return;
          
       case 2 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                if( Col & 1){
                   GpuWrite( 0xAA);
                } else {
                   GpuWrite( 0x55);
                }
             }
          }
          GpuDone();
          return;
          
       case 3 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN / 2; Col++){
                GpuWrite( 0xFF);
             }
          }
          GpuDone();
          return;
          
       case 4 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, GPU_MAX_COLUMN / 2);
             for( Col = GPU_MAX_COLUMN / 2; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( 0xFF);
             }
          }
          GpuDone();
          return;
          
       case 5 :
          for( Row = GPU_MAX_ROW / 4; Row < (GPU_MAX_ROW * 3) / 4; Row++){
             GpuGoto( Row, GPU_MAX_COLUMN / 4);
             for( Col = GPU_MAX_COLUMN / 4; Col < (GPU_MAX_COLUMN * 3) / 4; Col++){
                GpuWrite( 0xFF);
             }
          }
          GpuDone();
          return;         
       
       default :
          return;
   }
} // GpuTest
