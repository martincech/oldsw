//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Olimex
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard

#include "../inc/Graphic.h"   // graphic
#include "Fonts.h"            // seznam fontu
#include "../inc/conio.h"     // Display

#define CONTRAST_MIN           0x0D0
#define CONTRAST_BASE          0x0F0               // electronic contrast base
#define CONTRAST_MAX           0x110               // max contrast

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

static void LedFlash( native ms);
// Flash led for <ms> miliseconds

static int GetEvent( void);
// Returns keyboard event

static void GpuTest( native Mode);
// Test GPU

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   KbdInit();
   GInit();
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   GSetFont( FONT_8x8);
   i = 0;
   forever {
      switch( GetEvent()){
         case K_ENTER :
         case K_ENTER | K_REPEAT :
            GpuTest( i);
            GFlush();
            i++;
            if( i > 9){
               i = 0;
            }
            break;              
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LedsOn();
   SysDelay( ms);
   LedsOff();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Get event
//------------------------------------------------------------------------------

static int GetEvent( void)
// Returns keyboard event
{
int Key;
static TYesNo Toggle = NO;

   forever {
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         if( Toggle){
            Led0On();
         } else {
            Led0Off();
         }
         Toggle = !Toggle;
      }
      Key = KbdGet();
      if( Key != K_IDLE){
         return( Key);
      }
   }
} // GetEvent

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

#define P_HEIGHT  (G_HEIGHT / 2)

static void GpuTest( native Mode)
// Test GPU
{
int i;

   switch( Mode){
      case 0 :
         GClear();
         GBox( 0, 0, G_WIDTH, G_HEIGHT);
         return;

      case 1 :
         GClear();
         return;
   
      case 2 :
         GBox( 0, 0, 8, 8);
         return;
         
      case 3 :
         GClear();
         GBox( 0, 0, 10, 10);
         return;

      case 4 :
         GClear();
         cgoto( 0, 0);
         GSetColor( COLOR_BLACK);
         cputs( "Ahoj ty!");

         cgoto( 1, 0);
         GSetColor( COLOR_DARKGRAY);
         cputs( "Ahoj ty!");

         cgoto( 2, 0);
         GSetColor( COLOR_LIGHTGRAY);
         cputs( "Ahoj ty!");

         cgoto( 3, 0);
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");

         cgoto( 4, 0);
         GSetColor( COLOR_LIGHTGRAY);
         GBox( 0, GCharHeight() * 4, G_WIDTH, GCharHeight());
         GSetColor( COLOR_BLACK);
         cputs( "Ahoj ty!");

         cgoto( 5, 0);
         GSetColor( COLOR_DARKGRAY);
         GBox( 0, GCharHeight() * 5, G_WIDTH, GCharHeight());
         GSetColor( COLOR_BLACK);
         cputs( "Ahoj ty!");

         cgoto( 6, 0);
         GSetColor( COLOR_BLACK);
         GBox( 0, GCharHeight() * 6, G_WIDTH, GCharHeight());
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");

         cgoto( 7, 0);
         GSetColor( COLOR_DARKGRAY);
         GBox( 0, GCharHeight() * 7, G_WIDTH, GCharHeight());
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");

         cgoto( 8, 0);
         GSetColor( COLOR_LIGHTGRAY);
         GBox( 0, GCharHeight() * 8, G_WIDTH, GCharHeight());
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");
         GSetColor( COLOR_BLACK);
         return;

      case 5 :
         GClear();
         for( i = 0; i < 20; i++){
            GLine( 0, i * 8, G_MAX_X, i * 8);
         }
         return;

      case 6 :
         GClear();
         for( i = 0; i < 20; i++){
            GLine( i * 16, 0, i * 16, G_MAX_Y);
         }
         return;

      case 7 :
         GClear();
         for( i = 0; i < 20; i++){
            GLine( 0, 0, G_MAX_X, i * 8);
         }
         return;

      case 8 :
         GClear();
         GFlush();
         GLine( 0, 0, 0, G_HEIGHT - 1);
         GFlush();
         GLine( G_WIDTH - 1, 0, G_WIDTH - 1, G_HEIGHT - 1);
         GFlush();
         GLine( 0, 0, G_WIDTH - 1, 0);
         GFlush();
         GLine( 0, G_HEIGHT - 1, G_WIDTH - 1, G_HEIGHT - 1);
         return;

      case 9 :
         GClear();
         GSetColor( COLOR_WHITE);
         GBox( 0,               0, G_WIDTH / 4, P_HEIGHT);
         GSetColor( COLOR_LIGHTGRAY);
         GBox( G_WIDTH / 4,     0, G_WIDTH / 4, P_HEIGHT);
         GSetColor( COLOR_DARKGRAY);
         GBox( G_WIDTH / 2,     0, G_WIDTH / 4, P_HEIGHT);
         GSetColor( COLOR_BLACK);
         GBox((G_WIDTH * 3)/ 4, 0, G_WIDTH / 4, P_HEIGHT);
         return;
                  
      default :
         return;
   }
} // GpuTest
