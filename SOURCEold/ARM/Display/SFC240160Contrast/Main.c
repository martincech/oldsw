//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Olimex
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard

#include "../inc/St7259.h"   // Display

/*
#define CONTRAST_MIN           0x0AF
#define CONTRAST_BASE          0x0C4               // electronic contrast base
#define CONTRAST_MAX           0x0DF               // max contrast
*/

#define CONTRAST_MIN           0x0B0
#define CONTRAST_BASE          0x0D8               // electronic contrast base
#define CONTRAST_MAX           0x110               // max contrast

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

static void LedFlash( native ms);
// Flash led for <ms> miliseconds

static int GetEvent( void);
// Returns keyboard event

static void GpuTest( native Mode);
// Test GPU

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
unsigned Contrast;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   KbdInit();
   GpuInit();
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   Contrast = CONTRAST_MIN;
   GpuContrast( CONTRAST_BASE); 
   GpuTest( 5);
   forever {
      switch( GetEvent()){
         case K_ENTER :
         case K_ENTER | K_REPEAT :
            GpuContrast( Contrast); 
            Contrast += 1;
            if( Contrast > CONTRAST_MAX){
               Contrast = CONTRAST_MIN;
            }
            break;              
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LedsOn();
   SysDelay( ms);
   LedsOff();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Get event
//------------------------------------------------------------------------------

static int GetEvent( void)
// Returns keyboard event
{
int Key;
static TYesNo Toggle = NO;

   forever {
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         if( Toggle){
            Led0On();
         } else {
            Led0Off();
         }
         Toggle = !Toggle;
      }
      Key = KbdGet();
      if( Key != K_IDLE){
         return( Key);
      }
   }
} // GetEvent

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

static void GpuTest( native Mode)
// Test GPU
{
int Row, Col;

   switch( Mode){
      case 0 :
          GpuClear();
          return;
   
       case 1 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;
          
       case 2 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( GPU_COLOR_GRAY);
                GpuWrite( GPU_COLOR_GRAY);
                GpuWrite( GPU_COLOR_GRAY);
             }
          }
          GpuDone();
          return;
          
       case 3 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN / 2; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;
          
       case 4 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, GPU_MAX_COLUMN / 2);
             for( Col = GPU_MAX_COLUMN / 2; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;
          
       case 5 :
          GpuClear();
          for( Row = GPU_MAX_ROW / 4; Row < (GPU_MAX_ROW * 3) / 4; Row++){
             GpuGoto( Row, GPU_MAX_COLUMN / 4);
             for( Col = GPU_MAX_COLUMN / 4; Col < (GPU_MAX_COLUMN * 3) / 4; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;         

       case 6 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row += 8){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( GPU_COLOR_GRAY);
                GpuWrite( GPU_COLOR_GRAY);
                GpuWrite( GPU_COLOR_GRAY);
             }
          }
          GpuDone();
          return;
       
       default :
          return;
   }
} // GpuTest
