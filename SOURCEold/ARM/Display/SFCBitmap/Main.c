//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Olimex
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard

#include "../inc/Graphic.h"   // graphic
#include "Fonts.h"            // seznam fontu
#include "../inc/conio.h"     // Display

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

#if G_PLANES < 2
   #define COLOR_LIGHTGRAY COLOR_BLACK
   #define COLOR_DARKGRAY  COLOR_BLACK
#endif

//------------------------------------------------------------------------------
DefineBitmap( SquareBitmap, 2, 32, 32)
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,

   0xFF, 0xFF, 0xFF, 0x00,
   0xFF, 0xFF, 0xFF, 0x00,
   0xFF, 0xFF, 0xFF, 0x00,
   0xFF, 0xFF, 0xFF, 0x00,
   0xFF, 0xFF, 0xFF, 0x00,
   0xFF, 0xFF, 0xFF, 0x00,
   0xFF, 0xFF, 0xFF, 0x00,
   0xFF, 0xFF, 0xFF, 0x00,

   0x00, 0x00, 0xFF, 0x00,
   0x00, 0x00, 0xFF, 0x00,
   0x00, 0x00, 0xFF, 0x00,
   0x00, 0x00, 0xFF, 0x00,
   0x00, 0x00, 0xFF, 0x00,
   0x00, 0x00, 0xFF, 0x00,
   0x00, 0x00, 0xFF, 0x00,
   0x00, 0x00, 0xFF, 0x00,

   0xFF, 0x00, 0xFF, 0x00,
   0xFF, 0x00, 0xFF, 0x00,
   0xFF, 0x00, 0xFF, 0x00,
   0xFF, 0x00, 0xFF, 0x00,
   0xFF, 0x00, 0xFF, 0x00,
   0xFF, 0x00, 0xFF, 0x00,
   0xFF, 0x00, 0xFF, 0x00,
   0xFF, 0x00, 0xFF, 0x00,
//---

   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,

   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00,

   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,

   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
   0xFF, 0xFF, 0x00, 0x00,
EndBitmap()
//------------------------------------------------------------------------------
#include "Bmp4.c"  // Bmp4 bitmap
#include "Bat1.c"  // Bat1 bitmap


static void LedFlash( native ms);
// Flash led for <ms> miliseconds

static int GetEvent( void);
// Returns keyboard event

static void GpuTest( native Mode);
// Test GPU

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   KbdInit();
   GInit();
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   GSetFont( FONT_8x8);
   i = 0;
   forever {
      switch( GetEvent()){
         case K_ENTER :
         case K_ENTER | K_REPEAT :
            GpuTest( i);
            GFlush();
            i++;
            if( i > 11){
               i = 0;
            }
            break;              
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LedsOn();
   SysDelay( ms);
   LedsOff();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Get event
//------------------------------------------------------------------------------

static int GetEvent( void)
// Returns keyboard event
{
int Key;
static TYesNo Toggle = NO;

   forever {
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         if( Toggle){
            Led0On();
         } else {
            Led0Off();
         }
         Toggle = !Toggle;
      }
      Key = KbdGet();
      if( Key != K_IDLE){
         return( Key);
      }
   }
} // GetEvent

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

#define P_HEIGHT  (G_HEIGHT / 2)

static void GpuTest( native Mode)
// Test GPU
{
int i;

   switch( Mode){
      case 0 :
         GClear();
         GBox( 0, 0, G_WIDTH, G_HEIGHT);
         return;

      case 1 :
         GClear();
         return;
   
      case 2 :
         GClear();
         GSetColor( COLOR_LIGHTGRAY);
         GBox( 10, 10, 64, 12);
         GSetColor( COLOR_WHITE);
         GBox( 8, 8, 64, 12);
         GSetColor( COLOR_BLACK);
         GRectangle( 8, 8, 64, 12);
         GTextAt( 12, 10);
         cputs( "Cancel");
         return;
         
      case 3 :
         GClear();

         GSetColor( COLOR_BLACK);
         GBox( 20, 0, 2, G_HEIGHT);
         GBox( 57, 0, 2, G_HEIGHT);
         GBox( 93, 0, 2, G_HEIGHT);

         GSetMode( GMODE_REPLACE);
         GRectangle( 3, 3, 34, 34);
         GBitmap( 4, 4,   &SquareBitmap);

         GSetMode( GMODE_OR);
         GRectangle( 40, 40, 34, 34);
         GBitmap( 41, 41, &SquareBitmap);

         GSetMode( GMODE_XOR);
         GRectangle( 75, 75, 34, 34);
         GBitmap( 76, 76, &SquareBitmap);

         GSetMode( GMODE_REPLACE);
         return;

      case 4 :
         GClear();
         cgoto( 0, 0);
         GSetColor( COLOR_BLACK);
         cputs( "Ahoj ty!");

         cgoto( 1, 0);
         GSetColor( COLOR_DARKGRAY);
         cputs( "Ahoj ty!");

         cgoto( 2, 0);
         GSetColor( COLOR_LIGHTGRAY);
         cputs( "Ahoj ty!");

         cgoto( 3, 0);
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");

         cgoto( 4, 0);
         GSetColor( COLOR_LIGHTGRAY);
         GBox( 0, GCharHeight() * 4, G_WIDTH, GCharHeight());
         GSetColor( COLOR_BLACK);
         cputs( "Ahoj ty!");

         cgoto( 5, 0);
         GSetColor( COLOR_DARKGRAY);
         GBox( 0, GCharHeight() * 5, G_WIDTH, GCharHeight());
         GSetColor( COLOR_BLACK);
         cputs( "Ahoj ty!");

         cgoto( 6, 0);
         GSetColor( COLOR_BLACK);
         GBox( 0, GCharHeight() * 6, G_WIDTH, GCharHeight());
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");

         cgoto( 7, 0);
         GSetColor( COLOR_DARKGRAY);
         GBox( 0, GCharHeight() * 7, G_WIDTH, GCharHeight());
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");

         cgoto( 8, 0);
         GSetColor( COLOR_LIGHTGRAY);
         GBox( 0, GCharHeight() * 8, G_WIDTH, GCharHeight());
         GSetColor( COLOR_WHITE);
         cputs( "Ahoj ty!");
         GSetColor( COLOR_BLACK);
         return;

      case 5 :
         GClear();
         for( i = 0; i < 20; i++){
            GLine( 0, i * 8, G_MAX_X, i * 8);
         }
         return;

      case 6 :
         GClear();
         for( i = 0; i < 20; i++){
            GLine( i * 16, 0, i * 16, G_MAX_Y);
         }
         return;

      case 7 :
         GClear();
         for( i = 0; i < 20; i++){
            GLine( 0, 0, G_MAX_X, i * 8);
         }
         return;

      case 8 :
         GClear();
         GFlush();
         GLine( 0, 0, 0, G_HEIGHT - 1);
         GFlush();
         GLine( G_WIDTH - 1, 0, G_WIDTH - 1, G_HEIGHT - 1);
         GFlush();
         GLine( 0, 0, G_WIDTH - 1, 0);
         GFlush();
         GLine( 0, G_HEIGHT - 1, G_WIDTH - 1, G_HEIGHT - 1);
         return;

      case 9 :
         GClear();
         GSetColor( COLOR_WHITE);
         GBox( 0,               0, G_WIDTH / 4, P_HEIGHT);
         GSetColor( COLOR_LIGHTGRAY);
         GBox( G_WIDTH / 4,     0, G_WIDTH / 4, P_HEIGHT);
         GSetColor( COLOR_DARKGRAY);
         GBox( G_WIDTH / 2,     0, G_WIDTH / 4, P_HEIGHT);
         GSetColor( COLOR_BLACK);
         GBox((G_WIDTH * 3)/ 4, 0, G_WIDTH / 4, P_HEIGHT);
         return;

      case 10 :
         GClear();
         GRectangle( 3, 3, Bmp4.Width + 2, Bmp4.Height + 2);
         GBitmap( 4, 4, &Bmp4);
         return;

      case 11 :
         GClear();
         GBitmap( 0, 0, &Bat1);
         return;
                  
      default :
         return;
   }
} // GpuTest
