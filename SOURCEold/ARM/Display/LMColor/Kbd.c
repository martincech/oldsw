//*****************************************************************************
//
//    Kbd.c -  User keyboard
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../inc/Kbd.h"         // Keyboard prototypes
#include "../Moduly/Kbd/Kbd.c"  // Keyboard template

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void KbdInit( void)
// Inicializace
{
   KBD_DIR &= ~KBD_K1;
} // KbdInit

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

static int ReadKey( void)
// Cteni klavesy
{
dword Value;

   Value = KBD_PIN;
   if( !(Value & KBD_K1)){
      return( K_ENTER);
   }
   return( K_RELEASED);         // nic neni stisknuto
} // ReadKey
