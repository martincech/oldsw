Programovani Olimex ARM


1. vyndat propojku vedle JTAG konektoru (vedle RS232/ICSP)
2. Do RS232/ICSP zapojit kabel do PC
3. DIL switch prepnout do polohy ON (obe sekce),
   zapnout napajeni
4. Spustit LPC2000 Flash Utility
   zkontrolovat :
   Filename - vypalovany soubor
   Device LPC2138
   XTAL Freq 14745
   vpravo : Connected to Port COM1 (podle pripojeni)
   Use Baud Rate 19200
   Use DTR/RTS zatrhnout
5. Po stisknuti Read Device ID by melo vypsat udaje o CPU
6. Erase
7. Upload To Flash
8. Compare Flash (nekdy nefunguje spravne)
9. Vratit DIL switch do polohy OFF (obe sekce)
10. Stisknout tlacitko RST na desce
   - bliknou obe LED
   - tlacitkem pod zlutou LED se voli funkce
