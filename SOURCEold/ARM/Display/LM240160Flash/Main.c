//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Olimex
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard

#include "../inc/St7259.h"    // Display

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

#define BMP_WIDTH   56
#define BMP_HEIGHT 160

extern const byte Cisla1[ BMP_HEIGHT][ BMP_WIDTH / 8];
extern const byte Cisla2[ BMP_HEIGHT][ BMP_WIDTH / 8];

static void LedFlash( native ms);
// Flash led for <ms> miliseconds

static void DrawBitmap( const byte Bitmap[BMP_HEIGHT][BMP_WIDTH / 8]);
// Kresleni mapy

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
TYesNo Toggle;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   KbdInit();
   GpuInit();
   GpuContrast( GPU_EC_BASE);
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   Toggle = NO;
   forever {
      WatchDog();                   // refresh watchdog
      if( Toggle){
         Led0On();
         DrawBitmap( Cisla1);
      } else {
         Led0Off();
         DrawBitmap( Cisla2);
      }
      Toggle = !Toggle;
      SysDelay( 500);
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LedsOn();
   SysDelay( ms);
   LedsOff();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"


//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------


#include "Cisla1.c"
#include "Cisla2.c"

static void DrawBitmap( const byte Bitmap[BMP_HEIGHT][BMP_WIDTH / 8])
// Kresleni mapy
{
int Row, Col;
int i;
byte Value;

   for( Row = 0; Row < BMP_HEIGHT; Row++){
      GpuGoto( Row, 0);
      for( Col = 0; Col < BMP_WIDTH; Col += 8){
         Value = Bitmap[ Row][ Col / 8];
         for( i = 0; i < 8; i++){
            if( Value & (1 << i)){
               GpuWrite( GPU_COLOR_BLACK);
            } else {
               GpuWrite( GPU_COLOR_WHITE);
            }
         }
       }
    }
    GpuDone();
} // DrawBitmap
