//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Olimex
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard

#include "../inc/Graphic.h"   // graphic
#include "Fonts.h"            // seznam fontu
#include "../inc/conio.h"     // Display

#include "../Inc/Wgt/Dinput.h"
#include "Str.h"

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s


static void LedFlash( native ms);
// Flash led for <ms> miliseconds

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   KbdInit();
   GInit();
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   GSetFont( FONT_8x8);
   i = 50;
   forever {
      switch( SysWaitEvent()){
         case K_ENTER :
//            DInputEnum( "Caption", "Text", &i, STR_BACKLIGHT_MODE_AUTO, 3);
            DInputNumber( "Caption", "Text", &i, 1, 1, 100, "kg");
            break;              
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LedsOn();
   SysDelay( ms);
   LedsOff();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//  Yield
//------------------------------------------------------------------------------

int SysYield( void) 
// Background process & check for events
{
int           Key;
static TYesNo Toggle = NO;

   if( Timer1s){
      // uplynula 1s
      WatchDog();                   // refresh watchdog
      Timer1s = NO;                 // zrus priznak
      if( Toggle){
         Led0On();
      } else {
         Led0Off();
      }
      Toggle = !Toggle;
   }
   Key = KbdGet();
   if( Key != K_IDLE){
      return( Key);
   }
   return( K_IDLE);
} // SysYield

//------------------------------------------------------------------------------
//  Wait Event
//------------------------------------------------------------------------------

int SysWaitEvent( void) 
// Wait for event
{
int Key;

   forever {
      Key = SysYield();
      if( Key != K_IDLE) {
          return( Key);                // nonempty event
      }
   }
} // SysWaitEvent

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();



#include "../Moduly/LPC/SysTimer.c"
