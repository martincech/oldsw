//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

#define STR_NULL (char *)0 // undefined string
// Message Box :
#define STR_OK               (char *)1	
#define STR_CANCEL           (char *)2	
#define STR_YES              (char *)3	
#define STR_NO               (char *)4	

// Standard title :
#define STR_ERROR            (char *)5	
#define STR_INFO             (char *)6	
#define STR_CONFIRMATION     (char *)7	

// Input box :
#define STR_INPUT            (char *)8	
#define STR_OUT_OF_LIMITS    (char *)9	

// Directory box :
#define STR_DIRECTORY_EMPTY  (char *)10	

// DData/DHistory box :
#define STR_REALLY_DELETE_RECORD (char *)11	

// Text box :
#define STR_STRING_EMPTY     (char *)12	

// Backlight mode enum :
#define STR_BACKLIGHT_MODE_AUTO (char *)13	
#define STR_BACKLIGHT_MODE_ON (char *)14	
#define STR_BACKLIGHT_MODE_OFF (char *)15	

// system :
#define _STR_LAST (char *)16 // strings count

#endif
