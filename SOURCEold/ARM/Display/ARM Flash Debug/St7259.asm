	.file	"St7259.c"
	.section	.debug_abbrev,"",%progbits
.Ldebug_abbrev0:
	.section	.debug_info,"",%progbits
.Ldebug_info0:
	.section	.debug_line,"",%progbits
.Ldebug_line0:
	.text
.Ltext0:
	.align	2
	.global	GpuDone
	.type	GpuDone, %function
GpuDone:
.LFB8:
	.file 1 "C:/SOURCE/ARM/Display/../Moduly/ST7259/St7259.c"
	.loc 1 300 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 301 0
	mov	r3, #-536870912
	mov	r2, #67108864
	add	r3, r3, #163840
	.loc 1 300 0
	@ lr needed for prologue
	.loc 1 301 0
	str	r2, [r3, #4]
	.loc 1 302 0
	bx	lr
.LFE8:
	.size	GpuDone, .-GpuDone
	.align	2
	.global	GpuInitBuffer
	.type	GpuInitBuffer, %function
GpuInitBuffer:
.LFB11:
	.loc 1 368 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	.loc 1 369 0
	ldr	r3, .L5
	.loc 1 371 0
	mov	r2, #160	@ movhi
	strh	r2, [r3, #0]	@ movhi
	.loc 1 370 0
	mov	r2, #0	@ movhi
	.loc 1 368 0
	@ lr needed for prologue
	.loc 1 370 0
	strh	r2, [r3, #2]	@ movhi
	.loc 1 373 0
	bx	lr
.L6:
	.align	2
.L5:
	.word	GBuffer
.LFE11:
	.size	GpuInitBuffer, .-GpuInitBuffer
	.align	2
	.type	SetData, %function
SetData:
.LFB13:
	.loc 1 398 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
.LVL0:
	.loc 1 403 0
	mov	r0, r0, asl #16
.LVL1:
	.loc 1 402 0
	mov	r3, #-536870912
	add	r3, r3, #163840
	.loc 1 403 0
	and	r0, r0, #16711680
	.loc 1 402 0
	mov	r2, #16711680
	str	r2, [r3, #12]
	.loc 1 398 0
	@ lr needed for prologue
	.loc 1 403 0
	str	r0, [r3, #4]
	.loc 1 404 0
	bx	lr
.LFE13:
	.size	SetData, .-SetData
	.align	2
	.type	WriteCommand, %function
WriteCommand:
.LFB12:
	.loc 1 384 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL2:
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI0:
	.loc 1 385 0
	mov	r4, #-536870912
	add	r4, r4, #163840
	mov	r6, #-2147483648
	.loc 1 386 0
	mov	r5, #1073741824
	.loc 1 385 0
	str	r6, [r4, #12]
	.loc 1 384 0
	and	r0, r0, #255
	.loc 1 386 0
	str	r5, [r4, #12]
	.loc 1 387 0
	bl	SetData
.LVL3:
	.loc 1 388 0
	str	r5, [r4, #4]
	.loc 1 389 0
	str	r6, [r4, #4]
	.loc 1 390 0
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE12:
	.size	WriteCommand, .-WriteCommand
	.align	2
	.global	GpuOff
	.type	GpuOff, %function
GpuOff:
.LFB4:
	.loc 1 252 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI1:
	.loc 1 253 0
	mov	r4, #-536870912
	add	r4, r4, #163840
	mov	r5, #67108864
	.loc 1 254 0
	mov	r0, #174
	.loc 1 253 0
	str	r5, [r4, #12]
	.loc 1 254 0
	bl	WriteCommand
	.loc 1 255 0
	str	r5, [r4, #4]
	.loc 1 256 0
	ldmfd	sp!, {r4, r5, pc}
.LFE4:
	.size	GpuOff, .-GpuOff
	.align	2
	.global	GpuOn
	.type	GpuOn, %function
GpuOn:
.LFB3:
	.loc 1 240 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI2:
	.loc 1 241 0
	mov	r4, #-536870912
	add	r4, r4, #163840
	mov	r5, #67108864
	.loc 1 242 0
	mov	r0, #175
	.loc 1 241 0
	str	r5, [r4, #12]
	.loc 1 242 0
	bl	WriteCommand
	.loc 1 243 0
	str	r5, [r4, #4]
	.loc 1 244 0
	ldmfd	sp!, {r4, r5, pc}
.LFE3:
	.size	GpuOn, .-GpuOn
	.align	2
	.global	GpuWrite
	.type	GpuWrite, %function
GpuWrite:
.LFB7:
	.loc 1 288 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL4:
	stmfd	sp!, {r4, r5, lr}
.LCFI3:
	.loc 1 289 0
	mov	r4, #-536870912
	add	r4, r4, #163840
	mov	r5, #1073741824
	str	r5, [r4, #12]
	.loc 1 290 0
	bl	SetData
.LVL5:
	.loc 1 291 0
	str	r5, [r4, #4]
	.loc 1 292 0
	ldmfd	sp!, {r4, r5, pc}
.LFE7:
	.size	GpuWrite, .-GpuWrite
	.align	2
	.global	GpuContrast
	.type	GpuContrast, %function
GpuContrast:
.LFB9:
	.loc 1 310 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL6:
	stmfd	sp!, {r4, r5, r6, lr}
.LCFI4:
	.loc 1 311 0
	mov	r5, #-536870912
	add	r5, r5, #163840
	mov	r6, #67108864
	.loc 1 310 0
	mov	r4, r0
	.loc 1 311 0
	str	r6, [r5, #12]
	.loc 1 312 0
	mov	r0, #129
.LVL7:
	bl	WriteCommand
	.loc 1 313 0
	and	r0, r4, #63
	.loc 1 314 0
	mov	r4, r4, lsr #6
.LVL8:
	.loc 1 313 0
	bl	GpuWrite
	.loc 1 314 0
	and	r0, r4, #7
	bl	GpuWrite
	.loc 1 315 0
	str	r6, [r5, #4]
	.loc 1 316 0
	ldmfd	sp!, {r4, r5, r6, pc}
.LFE9:
	.size	GpuContrast, .-GpuContrast
	.align	2
	.type	SetArea, %function
SetArea:
.LFB15:
	.loc 1 432 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL9:
	stmfd	sp!, {r4, r5, r6, r7, lr}
.LCFI5:
	.loc 1 432 0
	mov	r4, r0
	.loc 1 433 0
	mov	r0, #117
.LVL10:
	.loc 1 432 0
	mov	r5, r1
	mov	r6, r2
	mov	r7, r3
	.loc 1 433 0
	bl	WriteCommand
.LVL11:
	.loc 1 434 0
	mov	r0, r4
	bl	GpuWrite
	.loc 1 435 0
	mov	r0, r5
	bl	GpuWrite
	.loc 1 437 0
	mov	r0, #21
	bl	WriteCommand
	.loc 1 438 0
	mov	r0, r6
	bl	GpuWrite
	.loc 1 439 0
	mov	r0, r7
	.loc 1 440 0
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	.loc 1 439 0
	b	GpuWrite
.LFE15:
	.size	SetArea, .-SetArea
	.align	2
	.type	ClearRam, %function
ClearRam:
.LFB14:
	.loc 1 412 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	.loc 1 415 0
	mov	r0, #0
	.loc 1 412 0
	stmfd	sp!, {r4, r5, lr}
.LCFI6:
	.loc 1 415 0
	mov	r2, r0
	mov	r1, #159
	mov	r3, #79
	bl	SetArea
	.loc 1 416 0
	mov	r0, #92
	bl	WriteCommand
	mov	r5, #0
.LVL12:
.LVL13:
.L22:
	.loc 1 424 0
	mov	r4, #0
.L23:
	.loc 1 419 0
	mov	r0, #248
	bl	GpuWrite
	.loc 1 420 0
	mov	r0, #248
	bl	GpuWrite
	.loc 1 418 0
	add	r4, r4, #1
	.loc 1 421 0
	mov	r0, #248
	bl	GpuWrite
	.loc 1 418 0
	cmp	r4, #80
	bne	.L23
	.loc 1 417 0
	add	r5, r5, #1
	cmp	r5, #160
	bne	.L22
	ldmfd	sp!, {r4, r5, pc}
.LFE14:
	.size	ClearRam, .-ClearRam
	.align	2
	.global	GpuClear
	.type	GpuClear, %function
GpuClear:
.LFB5:
	.loc 1 264 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI7:
	.loc 1 265 0
	mov	r4, #-536870912
	add	r4, r4, #163840
	mov	r5, #67108864
	str	r5, [r4, #12]
	.loc 1 266 0
	bl	ClearRam
	.loc 1 267 0
	str	r5, [r4, #4]
	.loc 1 268 0
	ldmfd	sp!, {r4, r5, pc}
.LFE5:
	.size	GpuClear, .-GpuClear
	.align	2
	.global	GpuFlush
	.type	GpuFlush, %function
GpuFlush:
.LFB10:
	.loc 1 333 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
.LCFI8:
	.loc 1 340 0
	ldr	r9, .L46
	ldr	r0, .L46+4
	.loc 1 342 0
	ldrh	r3, [r9, #2]
	.loc 1 340 0
	ldrh	ip, [r9, #0]
	.loc 1 342 0
	umull	r2, r1, r0, r3
	.loc 1 340 0
	umull	r3, r2, r0, ip
	.loc 1 342 0
	mov	r1, r1, asl #15
	.loc 1 340 0
	mov	r2, r2, asl #15
	mov	r2, r2, lsr #16
.LVL14:
	.loc 1 342 0
	mov	r1, r1, lsr #16
.LVL15:
	.loc 1 341 0
	mov	r3, r2, asl #1
	.loc 1 343 0
	mov	r0, r1, asl #1
	add	r6, r0, r1
.LVL16:
	.loc 1 341 0
	add	sl, r3, r2
.LVL17:
	.loc 1 344 0
	ldrh	r0, [r9, #4]
	mov	r3, r6
	ldrh	r1, [r9, #6]
.LVL18:
	mov	r2, sl
.LVL19:
	bl	SetArea
	.loc 1 345 0
	ldrh	r8, [r9, #4]
.LVL20:
	ldrh	r3, [r9, #6]
	cmp	r8, r3
	bgt	.L33
	add	fp, r9, #8
.LVL21:
.L35:
	.loc 1 346 0
	and	r2, r8, #7
	mov	r3, #1
	.loc 1 348 0
	cmp	sl, r6
	.loc 1 346 0
	mov	r7, r3, asl r2
	.loc 1 348 0
	bgt	.L36
	.loc 1 347 0
	mov	r2, r8, asr #3
	mov	r3, r2, asl #8
	sub	r3, r3, r2, asl #4
	add	r3, sl, r3
	add	r5, r3, fp
	mov	r4, sl
.L38:
	.loc 1 349 0
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	tst	r7, r3
	.loc 1 350 0
	mov	r0, #0
	.loc 1 348 0
	add	r4, r4, #1
	.loc 1 352 0
	moveq	r0, #248
	bl	GpuWrite
	.loc 1 348 0
	cmp	r6, r4
	.loc 1 354 0
	add	r5, r5, #1
	.loc 1 348 0
	bge	.L38
.L36:
	.loc 1 345 0
	ldrh	r3, [r9, #6]
	add	r8, r8, #1
	cmp	r3, r8
	bge	.L35
.L33:
	.loc 1 357 0
	bl	GpuDone
	.loc 1 359 0
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.loc 1 358 0
	b	GpuInitBuffer
.L47:
	.align	2
.L46:
	.word	GBuffer
	.word	-1431655765
.LFE10:
	.size	GpuFlush, .-GpuFlush
	.align	2
	.global	GpuGoto
	.type	GpuGoto, %function
GpuGoto:
.LFB6:
	.loc 1 276 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
.LVL22:
	.loc 1 277 0
	mov	ip, #-536870912
	.loc 1 276 0
	stmfd	sp!, {r4, lr}
.LCFI9:
	.loc 1 277 0
	add	ip, ip, #163840
	mov	r4, #67108864
	.loc 1 276 0
	mov	r2, r1
	.loc 1 278 0
	mov	r3, #79
	mov	r1, #159
.LVL23:
	.loc 1 277 0
	str	r4, [ip, #12]
	.loc 1 278 0
	bl	SetArea
.LVL24:
	.loc 1 279 0
	mov	r0, #92
	.loc 1 280 0
	ldmfd	sp!, {r4, lr}
	.loc 1 279 0
	b	WriteCommand
.LFE6:
	.size	GpuGoto, .-GpuGoto
	.align	2
	.global	GpuInit
	.type	GpuInit, %function
GpuInit:
.LFB2:
	.loc 1 158 0
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
.LCFI10:
	.loc 1 159 0
	mov	r4, #-536870912
	add	r4, r4, #163840
	mov	r5, #268435456
	mov	r2, #1677721600
	str	r5, [r4, #12]
	str	r2, [r4, #4]
	.loc 1 160 0
	ldr	r3, [r4, #8]
	.loc 1 161 0
	add	r2, r2, #469762048
	.loc 1 160 0
	orr	r3, r3, #-201326592
	str	r3, [r4, #8]
	.loc 1 161 0
	str	r2, [r4, #4]
	.loc 1 162 0
	ldr	r3, [r4, #8]
	orr	r3, r3, #16711680
	str	r3, [r4, #8]
	.loc 1 165 0
	mov	r0, #5
	.loc 1 164 0
	str	r5, [r4, #12]
	.loc 1 165 0
	bl	SysUDelay
	.loc 1 166 0
	str	r5, [r4, #4]
	.loc 1 167 0
	mov	r0, #5
	.loc 1 168 0
	add	r5, r5, #-201326592
	.loc 1 167 0
	bl	SysUDelay
	.loc 1 170 0
	mov	r0, #48
	.loc 1 168 0
	str	r5, [r4, #12]
	.loc 1 170 0
	bl	WriteCommand
	.loc 1 175 0
	mov	r0, #148
	bl	WriteCommand
	.loc 1 176 0
	mov	r0, #209
	bl	WriteCommand
	.loc 1 178 0
	mov	r0, #32
	bl	WriteCommand
	.loc 1 179 0
	mov	r0, #8
	bl	GpuWrite
	.loc 1 181 0
	mov	r0, #2
	bl	SysDelay
	.loc 1 183 0
	mov	r0, #32
	bl	WriteCommand
	.loc 1 184 0
	mov	r0, #11
	bl	GpuWrite
	.loc 1 186 0
	mov	r0, #129
	bl	WriteCommand
	.loc 1 187 0
	mov	r0, #48
	bl	GpuWrite
	.loc 1 188 0
	mov	r0, #3
	bl	GpuWrite
	.loc 1 190 0
	mov	r0, #202
	bl	WriteCommand
	.loc 1 191 0
	mov	r0, #4
	bl	GpuWrite
	.loc 1 192 0
	mov	r0, #39
	bl	GpuWrite
	.loc 1 193 0
	mov	r0, #0
	bl	GpuWrite
	.loc 1 195 0
	mov	r0, #166
	bl	WriteCommand
	.loc 1 197 0
	mov	r0, #187
	bl	WriteCommand
	.loc 1 198 0
	mov	r0, #1
	bl	GpuWrite
	.loc 1 200 0
	mov	r0, #188
	bl	WriteCommand
	.loc 1 201 0
	mov	r0, #1
	bl	GpuWrite
	.loc 1 202 0
	mov	r0, #0
	bl	GpuWrite
	.loc 1 203 0
	mov	r0, #2
	bl	GpuWrite
	.loc 1 205 0
	mov	r0, #49
	bl	WriteCommand
	.loc 1 206 0
	mov	r0, #50
	bl	WriteCommand
	.loc 1 207 0
	mov	r0, #5
	bl	GpuWrite
	.loc 1 208 0
	mov	r0, #0
	bl	GpuWrite
	.loc 1 209 0
	mov	r0, #4
	bl	GpuWrite
	.loc 1 210 0
	mov	r0, #48
	bl	WriteCommand
	.loc 1 225 0
	bl	ClearRam
	.loc 1 226 0
	mov	r0, #175
	bl	WriteCommand
	.loc 1 228 0
	str	r5, [r4, #4]
	.loc 1 232 0
	ldmfd	sp!, {r4, r5, lr}
	.loc 1 230 0
	b	GpuInitBuffer
.LFE2:
	.size	GpuInit, .-GpuInit
	.comm	TYesNoEnum,4,4
	.comm	TOkErrEnum,4,4
	.comm	GBuffer,4808,4
	.section	.debug_frame,"",%progbits
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0xe
	.byte	0xc
	.uleb128 0xd
	.uleb128 0x0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB13
	.4byte	.LFE13-.LFB13
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB12
	.4byte	.LFE12-.LFB12
	.byte	0x4
	.4byte	.LCFI0-.LFB12
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x4
	.4byte	.LCFI1-.LFB4
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE8:
.LSFDE10:
	.4byte	.LEFDE10-.LASFDE10
.LASFDE10:
	.4byte	.Lframe0
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x4
	.4byte	.LCFI2-.LFB3
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE10:
.LSFDE12:
	.4byte	.LEFDE12-.LASFDE12
.LASFDE12:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI3-.LFB7
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE12:
.LSFDE14:
	.4byte	.LEFDE14-.LASFDE14
.LASFDE14:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI4-.LFB9
	.byte	0xe
	.uleb128 0x10
	.byte	0x8e
	.uleb128 0x1
	.byte	0x86
	.uleb128 0x2
	.byte	0x85
	.uleb128 0x3
	.byte	0x84
	.uleb128 0x4
	.align	2
.LEFDE14:
.LSFDE16:
	.4byte	.LEFDE16-.LASFDE16
.LASFDE16:
	.4byte	.Lframe0
	.4byte	.LFB15
	.4byte	.LFE15-.LFB15
	.byte	0x4
	.4byte	.LCFI5-.LFB15
	.byte	0xe
	.uleb128 0x14
	.byte	0x8e
	.uleb128 0x1
	.byte	0x87
	.uleb128 0x2
	.byte	0x86
	.uleb128 0x3
	.byte	0x85
	.uleb128 0x4
	.byte	0x84
	.uleb128 0x5
	.align	2
.LEFDE16:
.LSFDE18:
	.4byte	.LEFDE18-.LASFDE18
.LASFDE18:
	.4byte	.Lframe0
	.4byte	.LFB14
	.4byte	.LFE14-.LFB14
	.byte	0x4
	.4byte	.LCFI6-.LFB14
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE18:
.LSFDE20:
	.4byte	.LEFDE20-.LASFDE20
.LASFDE20:
	.4byte	.Lframe0
	.4byte	.LFB5
	.4byte	.LFE5-.LFB5
	.byte	0x4
	.4byte	.LCFI7-.LFB5
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE20:
.LSFDE22:
	.4byte	.LEFDE22-.LASFDE22
.LASFDE22:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI8-.LFB10
	.byte	0xe
	.uleb128 0x24
	.byte	0x8e
	.uleb128 0x1
	.byte	0x8b
	.uleb128 0x2
	.byte	0x8a
	.uleb128 0x3
	.byte	0x89
	.uleb128 0x4
	.byte	0x88
	.uleb128 0x5
	.byte	0x87
	.uleb128 0x6
	.byte	0x86
	.uleb128 0x7
	.byte	0x85
	.uleb128 0x8
	.byte	0x84
	.uleb128 0x9
	.align	2
.LEFDE22:
.LSFDE24:
	.4byte	.LEFDE24-.LASFDE24
.LASFDE24:
	.4byte	.Lframe0
	.4byte	.LFB6
	.4byte	.LFE6-.LFB6
	.byte	0x4
	.4byte	.LCFI9-.LFB6
	.byte	0xe
	.uleb128 0x8
	.byte	0x8e
	.uleb128 0x1
	.byte	0x84
	.uleb128 0x2
	.align	2
.LEFDE24:
.LSFDE26:
	.4byte	.LEFDE26-.LASFDE26
.LASFDE26:
	.4byte	.Lframe0
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x4
	.4byte	.LCFI10-.LFB2
	.byte	0xe
	.uleb128 0xc
	.byte	0x8e
	.uleb128 0x1
	.byte	0x85
	.uleb128 0x2
	.byte	0x84
	.uleb128 0x3
	.align	2
.LEFDE26:
	.file 2 "./../inc/Uni.h"
	.file 3 "C:/SOURCE/ARM/Display/../Moduly/ST7259/../../inc/Gpu.h"
	.text
.Letext0:
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
.LLST3:
	.4byte	.LVL0-.Ltext0
	.4byte	.LVL1-.Ltext0
	.2byte	0x1
	.byte	0x50
	.4byte	0x0
	.4byte	0x0
.LLST4:
	.4byte	.LFB12-.Ltext0
	.4byte	.LCFI0-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI0-.Ltext0
	.4byte	.LFE12-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0x0
	.4byte	0x0
.LLST5:
	.4byte	.LVL2-.Ltext0
	.4byte	.LVL3-.Ltext0
	.2byte	0x1
	.byte	0x50
	.4byte	0x0
	.4byte	0x0
.LLST6:
	.4byte	.LFB4-.Ltext0
	.4byte	.LCFI1-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI1-.Ltext0
	.4byte	.LFE4-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0x0
	.4byte	0x0
.LLST7:
	.4byte	.LFB3-.Ltext0
	.4byte	.LCFI2-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI2-.Ltext0
	.4byte	.LFE3-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0x0
	.4byte	0x0
.LLST8:
	.4byte	.LFB7-.Ltext0
	.4byte	.LCFI3-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI3-.Ltext0
	.4byte	.LFE7-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0x0
	.4byte	0x0
.LLST9:
	.4byte	.LVL4-.Ltext0
	.4byte	.LVL5-.Ltext0
	.2byte	0x1
	.byte	0x50
	.4byte	0x0
	.4byte	0x0
.LLST10:
	.4byte	.LFB9-.Ltext0
	.4byte	.LCFI4-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI4-.Ltext0
	.4byte	.LFE9-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 16
	.4byte	0x0
	.4byte	0x0
.LLST11:
	.4byte	.LVL6-.Ltext0
	.4byte	.LVL7-.Ltext0
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL7-.Ltext0
	.4byte	.LVL8-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	0x0
	.4byte	0x0
.LLST12:
	.4byte	.LFB15-.Ltext0
	.4byte	.LCFI5-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI5-.Ltext0
	.4byte	.LFE15-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 20
	.4byte	0x0
	.4byte	0x0
.LLST13:
	.4byte	.LVL9-.Ltext0
	.4byte	.LVL10-.Ltext0
	.2byte	0x1
	.byte	0x50
	.4byte	.LVL10-.Ltext0
	.4byte	.LFE15-.Ltext0
	.2byte	0x1
	.byte	0x54
	.4byte	0x0
	.4byte	0x0
.LLST14:
	.4byte	.LVL9-.Ltext0
	.4byte	.LVL11-.Ltext0
	.2byte	0x1
	.byte	0x51
	.4byte	.LVL11-.Ltext0
	.4byte	.LFE15-.Ltext0
	.2byte	0x1
	.byte	0x55
	.4byte	0x0
	.4byte	0x0
.LLST15:
	.4byte	.LVL9-.Ltext0
	.4byte	.LVL11-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL11-.Ltext0
	.4byte	.LFE15-.Ltext0
	.2byte	0x1
	.byte	0x56
	.4byte	0x0
	.4byte	0x0
.LLST16:
	.4byte	.LVL9-.Ltext0
	.4byte	.LVL11-.Ltext0
	.2byte	0x1
	.byte	0x53
	.4byte	.LVL11-.Ltext0
	.4byte	.LFE15-.Ltext0
	.2byte	0x1
	.byte	0x57
	.4byte	0x0
	.4byte	0x0
.LLST17:
	.4byte	.LFB14-.Ltext0
	.4byte	.LCFI6-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI6-.Ltext0
	.4byte	.LFE14-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0x0
	.4byte	0x0
.LLST18:
	.4byte	.LFB5-.Ltext0
	.4byte	.LCFI7-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI7-.Ltext0
	.4byte	.LFE5-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0x0
	.4byte	0x0
.LLST19:
	.4byte	.LFB10-.Ltext0
	.4byte	.LCFI8-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI8-.Ltext0
	.4byte	.LFE10-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 36
	.4byte	0x0
	.4byte	0x0
.LLST20:
	.4byte	.LVL14-.Ltext0
	.4byte	.LVL17-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	.LVL17-.Ltext0
	.4byte	.LVL19-.Ltext0
	.2byte	0x1
	.byte	0x5a
	.4byte	0x0
	.4byte	0x0
.LLST21:
	.4byte	.LVL15-.Ltext0
	.4byte	.LVL16-.Ltext0
	.2byte	0x1
	.byte	0x51
	.4byte	.LVL16-.Ltext0
	.4byte	.LVL18-.Ltext0
	.2byte	0x1
	.byte	0x56
	.4byte	0x0
	.4byte	0x0
.LLST22:
	.4byte	.LFB6-.Ltext0
	.4byte	.LCFI9-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI9-.Ltext0
	.4byte	.LFE6-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 8
	.4byte	0x0
	.4byte	0x0
.LLST23:
	.4byte	.LVL22-.Ltext0
	.4byte	.LVL24-.Ltext0
	.2byte	0x1
	.byte	0x50
	.4byte	0x0
	.4byte	0x0
.LLST24:
	.4byte	.LVL22-.Ltext0
	.4byte	.LVL23-.Ltext0
	.2byte	0x1
	.byte	0x51
	.4byte	.LVL23-.Ltext0
	.4byte	.LVL24-.Ltext0
	.2byte	0x1
	.byte	0x52
	.4byte	0x0
	.4byte	0x0
.LLST25:
	.4byte	.LFB2-.Ltext0
	.4byte	.LCFI10-.Ltext0
	.2byte	0x1
	.byte	0x5d
	.4byte	.LCFI10-.Ltext0
	.4byte	.LFE2-.Ltext0
	.2byte	0x2
	.byte	0x7d
	.sleb128 12
	.4byte	0x0
	.4byte	0x0
	.section	.debug_info
	.4byte	0x3b3
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.4byte	.Ldebug_line0
	.4byte	.Letext0
	.4byte	.Ltext0
	.4byte	.LASF43
	.byte	0x1
	.4byte	.LASF44
	.4byte	.LASF45
	.uleb128 0x2
	.4byte	.LASF0
	.byte	0x2
	.byte	0xc
	.4byte	0x30
	.uleb128 0x3
	.4byte	.LASF2
	.byte	0x1
	.byte	0x8
	.uleb128 0x2
	.4byte	.LASF1
	.byte	0x2
	.byte	0xd
	.4byte	0x42
	.uleb128 0x3
	.4byte	.LASF3
	.byte	0x2
	.byte	0x7
	.uleb128 0x3
	.4byte	.LASF4
	.byte	0x4
	.byte	0x7
	.uleb128 0x3
	.4byte	.LASF5
	.byte	0x8
	.byte	0x7
	.uleb128 0x3
	.4byte	.LASF6
	.byte	0x1
	.byte	0x6
	.uleb128 0x3
	.4byte	.LASF7
	.byte	0x2
	.byte	0x5
	.uleb128 0x4
	.ascii	"int\000"
	.byte	0x4
	.byte	0x5
	.uleb128 0x3
	.4byte	.LASF8
	.byte	0x8
	.byte	0x5
	.uleb128 0x5
	.4byte	0x87
	.byte	0x4
	.byte	0x2
	.byte	0x25
	.uleb128 0x6
	.ascii	"NO\000"
	.sleb128 0
	.uleb128 0x6
	.ascii	"YES\000"
	.sleb128 1
	.byte	0x0
	.uleb128 0x5
	.4byte	0x9b
	.byte	0x4
	.byte	0x2
	.byte	0x2a
	.uleb128 0x6
	.ascii	"OK\000"
	.sleb128 0
	.uleb128 0x6
	.ascii	"ERR\000"
	.sleb128 1
	.byte	0x0
	.uleb128 0x3
	.4byte	.LASF9
	.byte	0x4
	.byte	0x7
	.uleb128 0x7
	.4byte	0xf2
	.2byte	0x12c8
	.byte	0x3
	.byte	0x11
	.uleb128 0x8
	.4byte	.LASF10
	.byte	0x3
	.byte	0x12
	.4byte	0x37
	.byte	0x2
	.byte	0x23
	.uleb128 0x0
	.uleb128 0x8
	.4byte	.LASF11
	.byte	0x3
	.byte	0x13
	.4byte	0x37
	.byte	0x2
	.byte	0x23
	.uleb128 0x2
	.uleb128 0x8
	.4byte	.LASF12
	.byte	0x3
	.byte	0x14
	.4byte	0x37
	.byte	0x2
	.byte	0x23
	.uleb128 0x4
	.uleb128 0x8
	.4byte	.LASF13
	.byte	0x3
	.byte	0x15
	.4byte	0x37
	.byte	0x2
	.byte	0x23
	.uleb128 0x6
	.uleb128 0x8
	.4byte	.LASF14
	.byte	0x3
	.byte	0x16
	.4byte	0xf2
	.byte	0x2
	.byte	0x23
	.uleb128 0x8
	.byte	0x0
	.uleb128 0x9
	.4byte	0x108
	.4byte	0x25
	.uleb128 0xa
	.4byte	0x9b
	.byte	0x13
	.uleb128 0xa
	.4byte	0x9b
	.byte	0xef
	.byte	0x0
	.uleb128 0x2
	.4byte	.LASF15
	.byte	0x3
	.byte	0x17
	.4byte	0xa2
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF16
	.byte	0x1
	.2byte	0x12c
	.byte	0x1
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x1
	.byte	0x5d
	.uleb128 0xb
	.byte	0x1
	.4byte	.LASF17
	.byte	0x1
	.2byte	0x170
	.byte	0x1
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x1
	.byte	0x5d
	.uleb128 0xc
	.4byte	0x16f
	.4byte	.LASF18
	.byte	0x1
	.2byte	0x18e
	.byte	0x1
	.4byte	.LFB13
	.4byte	.LFE13
	.byte	0x1
	.byte	0x5d
	.uleb128 0xd
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x18c
	.4byte	0x49
	.4byte	.LLST3
	.uleb128 0xe
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x18f
	.4byte	0x49
	.byte	0x0
	.uleb128 0xf
	.4byte	0x199
	.4byte	.LASF19
	.byte	0x1
	.2byte	0x180
	.byte	0x1
	.4byte	.LFB12
	.4byte	.LFE12
	.4byte	.LLST4
	.uleb128 0x10
	.ascii	"Cmd\000"
	.byte	0x1
	.2byte	0x17e
	.4byte	0x25
	.4byte	.LLST5
	.byte	0x0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF21
	.byte	0x1
	.byte	0xfc
	.byte	0x1
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	.LLST6
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF22
	.byte	0x1
	.byte	0xf0
	.byte	0x1
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LLST7
	.uleb128 0x12
	.4byte	0x1ee
	.byte	0x1
	.4byte	.LASF24
	.byte	0x1
	.2byte	0x120
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.4byte	.LLST8
	.uleb128 0xd
	.4byte	.LASF23
	.byte	0x1
	.2byte	0x11e
	.4byte	0x49
	.4byte	.LLST9
	.byte	0x0
	.uleb128 0x12
	.4byte	0x219
	.byte	0x1
	.4byte	.LASF25
	.byte	0x1
	.2byte	0x136
	.byte	0x1
	.4byte	.LFB9
	.4byte	.LFE9
	.4byte	.LLST10
	.uleb128 0xd
	.4byte	.LASF20
	.byte	0x1
	.2byte	0x134
	.4byte	0x49
	.4byte	.LLST11
	.byte	0x0
	.uleb128 0xf
	.4byte	0x273
	.4byte	.LASF26
	.byte	0x1
	.2byte	0x1b0
	.byte	0x1
	.4byte	.LFB15
	.4byte	.LFE15
	.4byte	.LLST12
	.uleb128 0xd
	.4byte	.LASF27
	.byte	0x1
	.2byte	0x1ae
	.4byte	0x65
	.4byte	.LLST13
	.uleb128 0xd
	.4byte	.LASF28
	.byte	0x1
	.2byte	0x1ae
	.4byte	0x65
	.4byte	.LLST14
	.uleb128 0xd
	.4byte	.LASF29
	.byte	0x1
	.2byte	0x1ae
	.4byte	0x65
	.4byte	.LLST15
	.uleb128 0xd
	.4byte	.LASF30
	.byte	0x1
	.2byte	0x1ae
	.4byte	0x65
	.4byte	.LLST16
	.byte	0x0
	.uleb128 0xf
	.4byte	0x2a5
	.4byte	.LASF31
	.byte	0x1
	.2byte	0x19c
	.byte	0x1
	.4byte	.LFB14
	.4byte	.LFE14
	.4byte	.LLST17
	.uleb128 0x13
	.ascii	"r\000"
	.byte	0x1
	.2byte	0x19d
	.4byte	0x65
	.byte	0x1
	.byte	0x55
	.uleb128 0x13
	.ascii	"c\000"
	.byte	0x1
	.2byte	0x19d
	.4byte	0x65
	.byte	0x1
	.byte	0x54
	.byte	0x0
	.uleb128 0x14
	.byte	0x1
	.4byte	.LASF33
	.byte	0x1
	.2byte	0x108
	.byte	0x1
	.4byte	.LFB5
	.4byte	.LFE5
	.4byte	.LLST18
	.uleb128 0x12
	.4byte	0x32a
	.byte	0x1
	.4byte	.LASF34
	.byte	0x1
	.2byte	0x14d
	.byte	0x1
	.4byte	.LFB10
	.4byte	.LFE10
	.4byte	.LLST19
	.uleb128 0x13
	.ascii	"x\000"
	.byte	0x1
	.2byte	0x14e
	.4byte	0x65
	.byte	0x1
	.byte	0x54
	.uleb128 0x13
	.ascii	"y\000"
	.byte	0x1
	.2byte	0x14e
	.4byte	0x65
	.byte	0x1
	.byte	0x58
	.uleb128 0x15
	.4byte	.LASF35
	.byte	0x1
	.2byte	0x14f
	.4byte	0x65
	.4byte	.LLST20
	.uleb128 0x15
	.4byte	.LASF36
	.byte	0x1
	.2byte	0x14f
	.4byte	0x65
	.4byte	.LLST21
	.uleb128 0x16
	.4byte	.LASF32
	.byte	0x1
	.2byte	0x150
	.4byte	0x49
	.byte	0x1
	.byte	0x57
	.uleb128 0x16
	.4byte	.LASF14
	.byte	0x1
	.2byte	0x151
	.4byte	0x32a
	.byte	0x1
	.byte	0x55
	.byte	0x0
	.uleb128 0x17
	.byte	0x4
	.4byte	0x25
	.uleb128 0x12
	.4byte	0x36b
	.byte	0x1
	.4byte	.LASF37
	.byte	0x1
	.2byte	0x114
	.byte	0x1
	.4byte	.LFB6
	.4byte	.LFE6
	.4byte	.LLST22
	.uleb128 0x10
	.ascii	"Row\000"
	.byte	0x1
	.2byte	0x112
	.4byte	0x65
	.4byte	.LLST23
	.uleb128 0xd
	.4byte	.LASF38
	.byte	0x1
	.2byte	0x112
	.4byte	0x65
	.4byte	.LLST24
	.byte	0x0
	.uleb128 0x11
	.byte	0x1
	.4byte	.LASF39
	.byte	0x1
	.byte	0x9e
	.byte	0x1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LLST25
	.uleb128 0x18
	.4byte	.LASF40
	.byte	0x2
	.byte	0x28
	.4byte	0x73
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	TYesNoEnum
	.uleb128 0x18
	.4byte	.LASF41
	.byte	0x2
	.byte	0x2d
	.4byte	0x87
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	TOkErrEnum
	.uleb128 0x18
	.4byte	.LASF42
	.byte	0x1
	.byte	0x84
	.4byte	0x108
	.byte	0x1
	.byte	0x5
	.byte	0x3
	.4byte	GBuffer
	.byte	0x0
	.section	.debug_abbrev
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x10
	.uleb128 0x6
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x25
	.uleb128 0xe
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x1b
	.uleb128 0xe
	.byte	0x0
	.byte	0x0
	.uleb128 0x2
	.uleb128 0x16
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x24
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.byte	0x0
	.byte	0x0
	.uleb128 0x4
	.uleb128 0x24
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.byte	0x0
	.byte	0x0
	.uleb128 0x5
	.uleb128 0x4
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0x0
	.byte	0x0
	.uleb128 0x6
	.uleb128 0x28
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0x0
	.byte	0x0
	.uleb128 0x7
	.uleb128 0x13
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x5
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.byte	0x0
	.byte	0x0
	.uleb128 0x8
	.uleb128 0xd
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x38
	.uleb128 0xa
	.byte	0x0
	.byte	0x0
	.uleb128 0x9
	.uleb128 0x1
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.uleb128 0x49
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0xa
	.uleb128 0x21
	.byte	0x0
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2f
	.uleb128 0xb
	.byte	0x0
	.byte	0x0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0x0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0x0
	.byte	0x0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.byte	0x0
	.byte	0x0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0xe
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0xf
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x10
	.uleb128 0x5
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x12
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x1
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0x0
	.byte	0x0
	.uleb128 0x14
	.uleb128 0x2e
	.byte	0x0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0x6
	.byte	0x0
	.byte	0x0
	.uleb128 0x16
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0x0
	.byte	0x0
	.uleb128 0x17
	.uleb128 0xf
	.byte	0x0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0x0
	.byte	0x0
	.uleb128 0x18
	.uleb128 0x34
	.byte	0x0
	.uleb128 0x3
	.uleb128 0xe
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x2
	.uleb128 0xa
	.byte	0x0
	.byte	0x0
	.byte	0x0
	.section	.debug_pubnames,"",%progbits
	.4byte	0xba
	.2byte	0x2
	.4byte	.Ldebug_info0
	.4byte	0x3b7
	.4byte	0x113
	.ascii	"GpuDone\000"
	.4byte	0x127
	.ascii	"GpuInitBuffer\000"
	.4byte	0x199
	.ascii	"GpuOff\000"
	.4byte	0x1ae
	.ascii	"GpuOn\000"
	.4byte	0x1c3
	.ascii	"GpuWrite\000"
	.4byte	0x1ee
	.ascii	"GpuContrast\000"
	.4byte	0x2a5
	.ascii	"GpuClear\000"
	.4byte	0x2bb
	.ascii	"GpuFlush\000"
	.4byte	0x330
	.ascii	"GpuGoto\000"
	.4byte	0x36b
	.ascii	"GpuInit\000"
	.4byte	0x380
	.ascii	"TYesNoEnum\000"
	.4byte	0x392
	.ascii	"TOkErrEnum\000"
	.4byte	0x3a4
	.ascii	"GBuffer\000"
	.4byte	0x0
	.section	.debug_aranges,"",%progbits
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0x0
	.2byte	0x0
	.2byte	0x0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0x0
	.4byte	0x0
	.section	.debug_str,"MS",%progbits,1
.LASF36:
	.ascii	"maxX\000"
.LASF40:
	.ascii	"TYesNoEnum\000"
.LASF7:
	.ascii	"short int\000"
.LASF25:
	.ascii	"GpuContrast\000"
.LASF34:
	.ascii	"GpuFlush\000"
.LASF26:
	.ascii	"SetArea\000"
.LASF8:
	.ascii	"long long int\000"
.LASF44:
	.ascii	"C:/SOURCE/ARM/Display/../Moduly/ST7259/St7259.c\000"
.LASF12:
	.ascii	"MinY\000"
.LASF45:
	.ascii	"C:\\SOURCE\\ARM\\Display\000"
.LASF43:
	.ascii	"GNU C 4.1.1\000"
.LASF38:
	.ascii	"Column\000"
.LASF28:
	.ascii	"RowHi\000"
.LASF39:
	.ascii	"GpuInit\000"
.LASF16:
	.ascii	"GpuDone\000"
.LASF35:
	.ascii	"minX\000"
.LASF22:
	.ascii	"GpuOn\000"
.LASF32:
	.ascii	"Mask\000"
.LASF0:
	.ascii	"byte\000"
.LASF27:
	.ascii	"RowLo\000"
.LASF15:
	.ascii	"TGraphicBuffer\000"
.LASF2:
	.ascii	"unsigned char\000"
.LASF31:
	.ascii	"ClearRam\000"
.LASF6:
	.ascii	"signed char\000"
.LASF5:
	.ascii	"long long unsigned int\000"
.LASF17:
	.ascii	"GpuInitBuffer\000"
.LASF20:
	.ascii	"Value\000"
.LASF41:
	.ascii	"TOkErrEnum\000"
.LASF3:
	.ascii	"short unsigned int\000"
.LASF42:
	.ascii	"GBuffer\000"
.LASF23:
	.ascii	"Pattern\000"
.LASF30:
	.ascii	"ColHi\000"
.LASF9:
	.ascii	"long unsigned int\000"
.LASF37:
	.ascii	"GpuGoto\000"
.LASF18:
	.ascii	"SetData\000"
.LASF24:
	.ascii	"GpuWrite\000"
.LASF33:
	.ascii	"GpuClear\000"
.LASF10:
	.ascii	"MinX\000"
.LASF19:
	.ascii	"WriteCommand\000"
.LASF21:
	.ascii	"GpuOff\000"
.LASF1:
	.ascii	"word\000"
.LASF4:
	.ascii	"unsigned int\000"
.LASF14:
	.ascii	"Buffer\000"
.LASF11:
	.ascii	"MaxX\000"
.LASF13:
	.ascii	"MaxY\000"
.LASF29:
	.ascii	"ColLo\000"
	.ident	"GCC: (GNU) 4.1.1"
