//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Bastl
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/conio.h"     // Display
#include "Fonts.h"            // seznam fontu
#include "Backlight.h"        // Backlight

#include "../inc/Rx8025.h"    // RTC

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   CpuInit();   

   KbdInit();
   BacklightInit();
   GInit();

   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   BacklightSet( 10);
   BacklightOn();
   GSetFont( FONT_8x8);

   // hlavni smycka :
   // clock :
   if( !RtcInit()){
      cputs( "Error\n");
      GFlush();
   }
   RtcSetSec( 0x50);
   RtcSetMin( 0x59);
   RtcSetHour( 0x23);
   RtcSetDay( 0x04);
   RtcSetMonth( 0x02);
   RtcSetWday( DT_MONDAY);
   RtcSetYear(  0x2008);

   forever {
      if( kbhit()){
         cclear();
         switch( getch()){
            case K_ENTER :
               RtcSetAlarm( RtcHour(), RtcMin() + 1);
               break;

            case K_ESC :
               RtcAlarmOff();
               break;

            case K_RIGHT :
               break;

            case K_LEFT :
               break;

            case K_UP :
               break;

            case K_DOWN :
               break;
         }
      }
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak

         cclear();
         cgoto( 2, 0);
         cprintf( "%02x:%02x:%02x\n",     RtcHour(), RtcMin(), RtcSec());
         cprintf( "%02x.%02x.%04x %02x\n", RtcDay(), RtcMonth(), RtcYear(), RtcWday() + 1);
         if( RtcCheckAlarm()){
            cputs( "* Alarm !");
         } else {
            cputs( "* Chrrr  ");
         }
      }
   }
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();                       



#include "../Moduly/LPC/SysTimer.c"
