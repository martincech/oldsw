//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Bastl
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/conio.h"     // Display
#include "../inc/Sound.h"     // Sound
#include "Fonts.h"            // seznam fontu
#include "Backlight.h"        // Backlight

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   CpuInit();   

   KbdInit();
   BacklightInit();
   GInit();
   SndInit();

   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   BacklightSet( 10);
   BacklightOn();
   GSetFont( FONT_8x8);

   // hlavni smycka :
   forever {
      if( kbhit()){
         cclear();
         switch( getch()){
            case K_ENTER :
               SndBeep( NOTE_C1, VOL_MAX, 100);     // async. pipnuti
               break;

            case K_ESC :
               SndBeep( NOTE_D1, VOL_MAX, 100);     // async. pipnuti
               break;

            case K_RIGHT :
               SndBeep( NOTE_E1, VOL_MAX, 100);     // async. pipnuti
               break;

            case K_LEFT :
               SndBeep( NOTE_F1, VOL_MAX, 100);     // async. pipnuti
               break;

            case K_UP :
               SndBeep( NOTE_G1, 1, 100);     // async. pipnuti
               break;

            case K_DOWN :
               SndBeep( NOTE_A1, 1, 100);     // async. pipnuti
               break;
         }
      }
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
      }
   }
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();                       \
   SndTrigger();                       



#include "../Moduly/LPC/SysTimer.c"
