//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo final
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "..\inc\Cpu.h"       // CPU startup
#include "..\inc\System.h"    // Operating system

#define __COM1__
#include "..\inc\Com.h"       // RS232

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

static void LedFlash( native ms);
// Flash led for <ms> miliseconds

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
native Toggle;
byte   ch;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   ComInit();
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   Toggle = NO;
   forever {
      if( ComRxChar( &ch)){
         ComTxChar( ch);
      }
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         if( Toggle){
            Led0On();
         } else {
            Led0Off();
         }
         Toggle = !Toggle;
         ComTxChar( 'A');
         ComTxChar( 'B');
         ComTxChar( 'C');
         ComTxChar( '\r');
         ComTxChar( '\n');
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   Led0On();
   SysDelay( ms);
   Led0Off();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }


#include "../Moduly/LPC/SysTimer.c"
