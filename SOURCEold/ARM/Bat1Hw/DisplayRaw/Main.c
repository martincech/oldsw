//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Bastl
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/St7259.h"    // Display

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

static void LedFlash( native ms);
// Flash led for <ms> miliseconds

static void GpuTest( native Mode);
// Test GPU

static void DrawBitmap( void);
// Kresleni mapy

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
unsigned Toggle;

   CpuInit();   
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti

   GpuInit();
   GpuContrast( GPU_EC_BASE);
   PwmInitPorts();
   BltOn();

   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   // hlavni smycka :
   Toggle = NO;
   forever {
      GpuTest( 1);
      SysDelay( 1000);
      GpuTest( 0);
      SysDelay( 1000);
      GpuTest( 3);
      SysDelay( 1000);
      GpuTest( 4);
      SysDelay( 1000);
      GpuTest( 9);
      SysDelay( 1000);
      GpuTest( 7);
      SysDelay( 1000);
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         if( Toggle){
            Led0On();
         } else {
            Led0Off();
         }
         Toggle = !Toggle;
      }
   }
} // main

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   Led0On();
   SysDelay( ms);
   Led0Off();
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
//   KbdTrigger();                       



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

static void GpuTest( native Mode)
// Test GPU
{
int Row, Col;

   switch( Mode){
      case 0 :
          GpuClear();
          return;
   
       case 1 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;
          
       case 2 :
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                if( Col < GPU_MAX_COLUMN / 2){
                   GpuWrite( GPU_COLOR_MIDDLEGRAY);
                   GpuWrite( GPU_COLOR_MIDDLEGRAY);
                   GpuWrite( GPU_COLOR_MIDDLEGRAY);
                } else {
                   GpuWrite( GPU_COLOR_WHITE);
                   GpuWrite( GPU_COLOR_WHITE);
                   GpuWrite( GPU_COLOR_WHITE);
                }
             }
          }
          GpuDone();
          return;
          
       case 3 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN / 2; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;
          
       case 4 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, GPU_MAX_COLUMN / 2);
             for( Col = GPU_MAX_COLUMN / 2; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;
          
       case 5 :
          GpuClear();
          for( Row = GPU_MAX_ROW / 4; Row < (GPU_MAX_ROW * 3) / 4; Row++){
             GpuGoto( Row, GPU_MAX_COLUMN / 4);
             for( Col = GPU_MAX_COLUMN / 4; Col < (GPU_MAX_COLUMN * 3) / 4; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;         

       case 6 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row += 8){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
                GpuWrite( GPU_COLOR_BLACK);
             }
          }
          GpuDone();
          return;

       case 7 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
/*
                GpuWrite( ( (Col << 3) & 0xC0) | 0x3F);
                GpuWrite( ( (Col << 3) & 0xC0) | 0x3F);
                GpuWrite( ( (Col << 3) & 0xC0) | 0x3F);
*/
                GpuWrite( Col << 3);
                GpuWrite( Col << 3);
                GpuWrite( Col << 3);
             }
          }
          GpuDone();
          return;

       case 8 :
          GpuClear();
          for( Row = 0; Row < GPU_MAX_ROW; Row++){
             GpuGoto( Row, 0);
             if( Row % 8 == 0){
                for( Col = 0; Col < GPU_MAX_COLUMN; Col++){
                   GpuWrite( GPU_COLOR_BLACK);
                   GpuWrite( GPU_COLOR_BLACK);
                   GpuWrite( GPU_COLOR_BLACK);
                }
             } else {
                for( Col = 0; Col < GPU_MAX_COLUMN / 2; Col++){
                   GpuWrite( GPU_COLOR_BLACK);
                   GpuWrite( GPU_COLOR_WHITE);
                   GpuWrite( GPU_COLOR_WHITE);
                   GpuWrite( GPU_COLOR_WHITE);
                   GpuWrite( GPU_COLOR_WHITE);
                   GpuWrite( GPU_COLOR_WHITE);
                }
             }
          }
          GpuDone();
          return;

       case 9 :
          DrawBitmap();
          return;
       
       default :
          return;
   }
} // GpuTest

//------------------------------------------------------------------------------
//   Test
//------------------------------------------------------------------------------

#include "Demo.c"

static void DrawBitmap( void)
// Kresleni mapy
{
int Row, Col;
int i;
byte Value;

   for( Row = 0; Row < GPU_MAX_ROW; Row++){
      GpuGoto( Row, 0);
      for( Col = 0; Col < G_WIDTH; Col += 8){
         Value = Bitmap[ Row][ Col / 8];
         for( i = 0; i < 8; i++){
            if( Value & (1 << i)){
               GpuWrite( GPU_COLOR_BLACK);
            } else {
               GpuWrite( GPU_COLOR_WHITE);
            }
         }
       }
    }
    GpuDone();
} // DrawBitmap
