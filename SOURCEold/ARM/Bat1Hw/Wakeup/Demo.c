//******************************************************************************
//                                                                            
//  Demo.c         Olimex ARM demo                                                                 
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/LPC213x.h"
#include "../inc/Lpc.h"

// LED on P1.23 klavesnice K0
#define LEDPIN  23

//------------------------------------------------------------------------------
//   LED init
//------------------------------------------------------------------------------

static void LedInit( void)
{
   GPIO1_IODIR |=  (1<<LEDPIN);	// define LED-Pin as output
   GPIO1_IOSET  =  (1<<LEDPIN);	// set Bit = LED off
} // LedInit

//------------------------------------------------------------------------------
//   Delay
//------------------------------------------------------------------------------

static void Delay( void)
{
	volatile int i,j;

	for( i = 0; i < 100; i++){
		for( j = 0; j < 1000; j++);
	}
} // Delay


//******************************************************************************
//   Main
//******************************************************************************

int main( void)
{
	
	LedInit();
	while( 1){
		GPIO1_IOCLR=(1<<LEDPIN);	// set all outputs in mask to 0 -> LED on
		Delay();
		GPIO1_IOSET=(1<<LEDPIN);	// set all outputs in mask to 1 -> LED off
		Delay();
	}
} // main
