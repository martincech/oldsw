//*****************************************************************************
//
//    Kbd.c -  User keyboard
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../inc/Kbd.h"         // Keyboard prototypes
#include "../Moduly/Kbd/Kbd.c"  // Keyboard template

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void KbdInit( void)
// Inicializace
{
   KBD_DIR &= ~( KBD_K0 | KBD_K1 | KBD_K2 | KBD_ON);
} // KbdInit

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

static int ReadKey( void)
// Cteni klavesy
{
dword Value;

   // dvojstisk Enter/Esc :
   if( !(KBD_PIN & KBD_ON) && !(KBD_PIN & KBD_K2)){
      return( K_BOOT);
   }
   // On/Esc :
   if( !(KBD_PIN & KBD_ON)){
      return( K_ESC);
   }
   // prvni rada :
   Value = KBD_PIN;
   // jednotlive klavesy :
   if( !(Value & KBD_K0)){
      return( K_LEFT);
   }
   if( !(Value & KBD_K1)){
      return( K_UP);
   }
   if( !(Value & KBD_K2)){
      return( K_ENTER);
   }
   // prvni sloupec :
   DisableInts();
   KBD_DIR |= KBD_K0;
   KBD_CLR  = KBD_K0;
   SysUDelay( 1);
   Value     = KBD_PIN;
   KBD_DIR  &= ~KBD_K0;
   EnableInts();
   if( !(Value & KBD_K1)){
      return( K_DOWN);
   }
   if( !(Value & KBD_K2)){
      return( K_RIGHT);
   }
   return( K_RELEASED);         // nic neni stisknuto
} // ReadKey
