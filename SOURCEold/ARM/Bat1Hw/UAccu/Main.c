//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Bastl
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/conio.h"     // Display
#include "Fonts.h"            // seznam fontu
#include "Backlight.h"        // Backlight

#include "../Inc/IAdc.h"     // Internal A/D convertor

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

// Accu voltage measuring :

#define GetAccuVoltage( RawValue) ((PACCU_RANGE * (RawValue)) / IADC_RANGE)


#define ADC_SHIFT  2
#define ADC_READS  (1 << ADC_SHIFT)

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
dword  Value;
int    i;

   CpuInit();   

   KbdInit();
   BacklightInit();
   GInit();

   PManInitPorts();
   PManSetVYP();
   IAdcInit();

   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   BacklightSet( 10);
   BacklightOn();
   GSetFont( FONT_8x8);

   // hlavni smycka :
   forever {
      // PPR status :
      cgoto( 0, 0);
      GSetColor( COLOR_WHITE);
      GBox( 0, 0, 8, 8);
      GSetColor( COLOR_BLACK);
      if( PManGetPPR()){
         cputch( '1');
      } else {
         cputch( '0');
      }
      // CHG status :
      cgoto( 1, 0);
      GSetColor( COLOR_WHITE);
      GBox( 0, 8, 8, 8);
      GSetColor( COLOR_BLACK);
      if( PManGetCHG()){
         cputch( 'C');
      } else {
         cputch( '-');
      }
      // USB status :
      cgoto( 2, 0);
      GSetColor( COLOR_WHITE);
      GBox( 0, 16, 8, 8);
      GSetColor( COLOR_BLACK);
      if( PManGetUSBON()){
         cputch( 'U');
      } else {
         cputch( '-');
      }
      // keyboard :
      if( kbhit()){
         cclear();
         switch( getch()){
            case K_ENTER :
               break;

            case K_ESC :
               break;

            case K_RIGHT :
               break;

            case K_LEFT :
               break;

            case K_UP :
               break;

            case K_DOWN :
               PManClrVYP();
               break;
         }
      }
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak

         // read A/D with averaging :
         Value = 0;
         for( i = 0; i < ADC_READS; i++){
            Value += IAdcRead( ACCU_ADC_VOLTAGE);
         }
         Value >>= ADC_SHIFT;
         cgoto( 3, 0);
         cclreol();
         cprintf( "%d mV",  GetAccuVoltage( Value));
      }
   }
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();                       



#include "../Moduly/LPC/SysTimer.c"
