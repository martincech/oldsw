//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Bastl
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/conio.h"     // Display
#include "../inc/Eep.h"       // EEPROM
#include "Fonts.h"            // seznam fontu
#include "Backlight.h"        // Backlight

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

void WriteTest( void);
void VerifyTest( void);

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   CpuInit();   

   KbdInit();
   BacklightInit();
   GInit();
   EepInit();

   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   BacklightSet( 10);
   BacklightOn();
   GSetFont( FONT_8x8);

   // hlavni smycka :
   forever {
      if( kbhit()){
         cclear();
         switch( getch()){
            case K_ENTER :
               WriteTest();
               break;

            case K_ESC :
               VerifyTest();
               break;

            case K_RIGHT :
               break;

            case K_LEFT :
               break;

            case K_UP :
               break;

            case K_DOWN :
               break;
         }
      }
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
      }
   }
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();                       



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------

#define MAGIC_OFFSET 2
void WriteTest( void)
{
int i, j;

   cgoto( 0, 0);
   cputs( "Start...\n");
   for( i = 0; i < EEP_SIZE; i += EEP_PAGE_SIZE){
      EepPageWriteStart( i);
      for( j = 0; j < EEP_PAGE_SIZE; j++){
         EepPageWriteData( (i + MAGIC_OFFSET) & 0xFF);
      }
      EepPageWritePerform();
   }
   cputs( "Done...");
} // WriteTest

void VerifyTest( void)
{
int i, j;
byte Value;

   cgoto( 2, 0);
   cputs( "Start...\n");
   for( i = 0; i < EEP_SIZE; i += EEP_PAGE_SIZE){
      EepBlockReadStart( i);
      for( j = 0; j < EEP_PAGE_SIZE; j++){
         Value = EepBlockReadData();
         if( Value != ((i + MAGIC_OFFSET) & 0xFF)){
            cprintf( "Error @%04x : %02x\n", i, Value);
            cputs( "Error...");
            return;
         }
      }
      EepBlockReadStop();
   }
   cputs( "Done...");
} // VerifyTest
