//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Bastl
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/conio.h"     // Display
#include "../Inc/St7259.h"   // Display
#include "Fonts.h"            // seznam fontu
#include "Backlight.h"        // Backlight

#define BACKLIGHT_MAX 9

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i, j;

   CpuInit();   

   KbdInit();
   BacklightInit();
   GInit();

   PManInitPorts();
   PManSetVYP();

   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   BacklightSet( BACKLIGHT_MAX);
   BacklightOn();
   GSetFont( FONT_8x8);

   PManClrENNAB();
   KbdPowerUpRelease();                // wait for key release

   // main loop :
   cputs( "START");
   i = 0;
   forever {
      if( kbhit()){
         switch( getch()){
            case K_ENTER :
               cgoto( 1, i);
               cputch( 'X');
               i++;
               if( i > 20){
                  cgoto( 1, 0);
                  cclreol();
                  i = 0;
               }
               break;

            case K_ESC :
               cgoto( 2, 0);
               cclreol();
               cputch( 'S');
               SysDelay( 3000);
               cputch( 'E');
               break;

            case K_RIGHT :
               cgoto( 4, 0);
               cclreol();
               cputch( 'S');
               forever {
                  WatchDog();
               }
               cputch( 'E');
               break;

            case K_LEFT :
               cgoto( 3, 0);
               cclreol();
               cputch( 'S');
//               j = FCPU / 8;
               j = FCPU / (8 + 3 * 14);
               WatchDog();
               do {
                  WatchDog();
               } while( --j);
               cputch( 'E');
               break;

            case K_UP :
               forever {
               };
               break;

            case K_DOWN :
               GpuShutdown();                      // display off
               PManClrVYP();
               break;
         }
      }
      if( Timer1s){
         // 1s expiration
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // clear flag
      }
   }
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();                       



#include "../Moduly/LPC/SysTimer.c"
