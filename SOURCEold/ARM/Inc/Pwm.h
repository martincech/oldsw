//******************************************************************************
//                                                                            
//   Pwm.c          Simple PWM
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __Pwm1_H__
   #define __Pwm1_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void PwmInit( void);
// Initialisation

void PwmStart( int Percent);
// Start PWM with <Percent> duty cycle

void PwmStop( void);
// Stop PWM

#endif
