//*****************************************************************************
//
//    Spi.h - SPI communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Spi_H__
   #define __Spi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void SpiInit( void);
// Interface initialisation

void SpiAttach( void);
// Bus setup and chipselect

void SpiRelease( void);
// Deselect and bus release

byte SpiReadByte( void);
// Returns readed byte

void SpiWriteByte( byte Value);
// Write <value> byte

#endif
