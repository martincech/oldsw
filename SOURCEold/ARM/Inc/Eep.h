//*****************************************************************************
//
//    Eep.h        Common SPI EEPROM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Eep_H__
   #define __Eep_H__

#ifdef __WIN32__
   #ifndef __Uni_H__
      #include "../../Library/Unisys/Uni.h"
   #endif
#else
   #ifndef __Spi_H__
      #include "Spi.h"
   #endif
#endif

void EepInit( void);
// Nastavi klidove hodnoty na sbernici, inicializuje pamet

byte EepReadByte( unsigned Address);
// Precte byte z EEPROM <Address>

TYesNo EepWriteByte( unsigned Address, byte Value);
// Zapise byte <Value> na <Address> v EEPROM
// Vraci NO neni-li zapis mozny

//------ Blokove cteni -----------------------------

void EepBlockReadStart( unsigned Address);
// Zahaji blokove cteni z EEPROM <Address>

#ifdef __WIN32__
   byte EepBlockReadData( void);
   // Cteni bytu bloku

   void EepBlockReadStop( void);
   // Ukonceni cteni bloku
#else
   #define EepBlockReadData() SpiReadByte()
   // Cteni bytu bloku

   #define EepBlockReadStop() SpiRelease()
   // Ukonceni cteni bloku
#endif


//------ Strankovy zapis -----------------------------
// Pozor, hranice stranek hlida uzivatel API
// EEP_PAGE_SIZE je v Hardware.h

TYesNo EepPageWriteStart( unsigned Address);
// Zahaji zapis stranky od <Address> v EEPROM.
// Vraci NO neni-li zapis mozny

#ifdef __WIN32__
   void EepPageWriteData( byte Value);
   // Zapis bytu do stranky

   void EepPageWritePerform( void);
   // Odstartuje fyzicky zapis stranky do EEPROM
#else
   #define EepPageWriteData( Value) SpiWriteByte( Value)
   // Zapis bytu do stranky

   #define EepPageWritePerform()    SpiRelease()
   // Odstartuje fyzicky zapis stranky do EEPROM
#endif

//------ Utility -------------------------------------

TYesNo EepSaveData( unsigned Address, const void *Data, int Size);
// Zapise <Data> o velikosti <Size> na <Address>

void EepLoadData( unsigned Address, void *Data, int Size);
// Nacte <Data> o velikosti <Size> z <Address>

TYesNo EepFillData( unsigned Address, byte Pattern, int Size);
// Vyplni prostor od <Address> v delce <Size> pomoci <Pattern>

TYesNo EepMatchData( unsigned Address, void *Data, int Size);
// Komparuje <Data> o velikosti <Size> na <Address>

#endif
