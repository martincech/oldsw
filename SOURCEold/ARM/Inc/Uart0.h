//*****************************************************************************
//
//    Uart0.h - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Uart0_H__
   #define __Uart0_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __ComDef_H__
   #include "ComDef.h"
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void Uart0Init( void);
// Communication initialisation

void Uart0Disconnect( void);
// Disconnect port pins

void Uart0Setup( unsigned Baud, unsigned Format, word Timeout);
// Set communication parameters <Format> is TComFormat enum

TYesNo Uart0TxBusy( void);
// Returns YES if transmitter is busy

void Uart0TxChar( byte Char);
// Transmit <Char> byte

TYesNo Uart0RxChar( byte *Char);
// Returns YES and received byte on <Char>,
// or NO on timeout

TYesNo Uart0RxWait( native Timeout);
// Waits for receive up to <Timeout> miliseconds

void Uart0FlushChars( void);
// Skips all Rx chars up to intercharacter timeout

TYesNo Uart0IsBreak( void);
// Testuje Tx break

#endif
