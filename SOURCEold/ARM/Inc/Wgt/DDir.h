//******************************************************************************
//                                                                            
//  DDir.h         Display file directory
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DDir_H__
   #define __DDir_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FdirDef_H__
   #include "../File/FdirDef.h"
#endif

#ifndef __DsDef_H__
   #include "../File/DsDef.h"
#endif

#ifndef __StrDef_H__
   #include "../Inc/Wgt/StrDef.h"
#endif

TFdirHandle DDirSelect( TUniStr Caption, TFdirHandle Handle);
// Select file from directory. Set cursor to <Handle>

void DDirSelectSet( TUniStr Caption, TFDataSet DataSet);
// Select file set from directory

#endif


