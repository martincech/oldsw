//******************************************************************************
//                                                                            
//   DEnterList.h   Enter string list
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DEnterList_H__
   #define __DEnterList_H__

#ifndef __StrDef_H__
   #include "StrDef.h"
#endif

#ifndef __DCallback_H__
   #include "DCallback.h"
#endif

#ifndef __DLabel_H__
   #include "DLabel.h"   // Center type
#endif

// List definition :
#define DefList( list) const TUniStr list[] = {
#define EndList()      STR_NULL};

// Use :
// DefList( DemoList)
//    STR_ITEM1,
//    STR_ITEM2,
//    ...
// EndList()


//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo DEnterList( int *InitialValue, const TUniStr *List, int x, int y, TCenterType Center);
// Display and edit <InitialValue>

int DEnterListWidth( const TUniStr *List);
// Returns pixel width of <List>

TYesNo DEnterSpin( int *InitialValue, int MinValue, int MaxValue, TAction *OnChange, int x, int y);
// Enter value by spinner

int DEnterSpinWidth( int MaxValue);
// Returns pixel width of spinner field

#endif
