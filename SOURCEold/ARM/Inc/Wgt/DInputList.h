//******************************************************************************
//                                                                            
//   DInputList.h   Input dialog for string list
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DInputList_H__
   #define __DInputList_H__

#ifndef __DEnterList_H__
   #include "DEnterList.h"
#endif

#ifndef __DCallback_H__
   #include "DCallback.h"
#endif


TYesNo DInputList( TUniStr Caption, TUniStr Text, int *Value, 
                   const TUniStr *List);
// Input list

TYesNo DInputSpin( TUniStr Caption, TUniStr Text, int *Value, 
                   int MaxValue, TAction *OnChange);
// Input value by spinner

#endif
