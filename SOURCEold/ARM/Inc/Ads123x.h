//*****************************************************************************
//
//    Ads123x.h  -  A/D convertor ADS1230/ADS1232 services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ads123x_H__
   #define __Ads123x_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef ADC_INTERRUPT                    // prevodnik v prerusovacim modu

#define ADC_STATUS_READY  0x80
#define ADC_STATUS_MASK   0x7F

// interni data :
extern volatile byte _AdcStatus;  // stav mereni
#endif // ADC_INTERRUPT

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void AdcInit( void);
// Inicializace prevodniku

#define AdcIsReady()         (!AdcGetDOUT())   // DRDY muze byt na EINT, pouzij DOUT
// Prevodnik ma pripravena data

#ifndef ADC_INTERRUPT

void AdcSetup( void);
// Nastaveni prevodu, autokalibrace

int32 AdcReadValue( void);
// Precteni namerene hodnoty

#endif // ADC_INTERRUPT

//-----------------------------------------------------------------------------
#ifdef ADC_INTERRUPT       // prevodnik v prerusovacim modu

// POZOR, od AdcStart do prvniho nastaveni AdcDataReady()
// uplyne 800ms + 100ms * n kde <n> je pocet prumerovanych vzorku

void AdcStart( void);
// Zahajeni periodickeho mereni

void AdcStop( void);
// Zastaveni periodickeho mereni

#define AdcDataReady()   (_AdcStatus & ADC_STATUS_READY)
// Test na nove zmerenou hodnotu

int32 AdcRead( void);
// Cteni merene hodnoty

#endif // ADC_INTERRUPT
//-----------------------------------------------------------------------------

#endif
