//*****************************************************************************
//
//    Rtc.h  -  Real time clock basic services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Rtc_H__
   #define __Rtc_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __DtDef_H__
   #include "DtDef.h"        // day of week constants
#endif

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

TYesNo RtcInit( void);
// Initialisation

//-----------------------------------------------------------------------------
// Read time
//-----------------------------------------------------------------------------

byte RtcSec( void);
// Returns seconds in BCD

byte RtcMin( void);
// Returns minutes in BCD

byte RtcHour( void);
// Returns hours in BCD

//-----------------------------------------------------------------------------
// Read date
//-----------------------------------------------------------------------------

byte RtcDay( void);
// Returns day of month in BCD

byte RtcMonth( void);
// Returns month in BCD

word RtcYear( void);
// Returns year in BCD

byte RtcWday( void);
// Returns day of week as TDtDow enum

//-----------------------------------------------------------------------------
// Write time
//-----------------------------------------------------------------------------

void RtcSetSec( byte Bcd);
// Set seconds to <Bcd>

void RtcSetMin( byte Bcd);
// Set minutes to <Bcd>

void RtcSetHour( byte Bcd);
// Set hours to <Bcd>

//-----------------------------------------------------------------------------
// Write date
//-----------------------------------------------------------------------------

void RtcSetDay( byte Bcd);
// Set day of month to <Bcd>

void RtcSetMonth( byte Bcd);
// Set month to <Bcd>

void RtcSetYear( word Bcd);
// Set year to <Bcd>

void RtcSetWday( byte Dow);
// Set day of week to <Dow> TDtDow enum

//-----------------------------------------------------------------------------
// Alarm
//-----------------------------------------------------------------------------

void RtcSetAlarm(byte HourBcd, byte MinuteBcd);
// Set alarm to <HourBcd>:<MinuteBcd>

TYesNo RtcCheckAlarm( void);
// Returns YES if alarm active (resets alarm also)

void RtcAlarmOff( void);
// Switch alarm off

//-----------------------------------------------------------------------------
// Utilities
//-----------------------------------------------------------------------------

TTimestamp RtcLoad( void);
// Returns RTC time

void RtcSave( TTimestamp Time);
// Set RTC time by <Time>

#endif
