//******************************************************************************
//
//   GSM.h        GSM services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __SMS_H__
   #define __SMS_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#define SMS_MAX_OPERATOR   16         // max. delka nazvu operatora
#define SMS_MAX_NUMBER     16         // max. delka cisla
#define SMS_MAX_LENGTH    160         // max. delka prijate zpravy

// navratove kody operace SmsRead :

typedef enum {
   SMS_EMPTY_MESSAGE = 0,
   SMS_FORMAT_ERROR  = 0xFE,
   SMS_READ_ERROR    = 0xFF
} TGsmReadError;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

TYesNo GsmReset( void);
// Inicializace modemu

TYesNo GsmRegistered( void);
// Vraci YES, je-li modem registrovan v siti

TYesNo GsmOperator( char *Name);
// Vrati retezec s nazvem operatora,
// POZOR delka <Name> musi byt min SMS_MAX_OPERATOR+1

native GsmSignalStrength( void);
// Vrati relativni silu signalu 0..31 nebo 99 nejde-li zjistit


//-- SMS ----------------------------------------------------------------------

TYesNo SmsSend( const char *Number, const char *Text, native MessageLength, native RefNumber);
// Odeslani zpravy. <RefNumber> je referencni cislo zpravy, 0=automaticky

TYesNo SmsSendOk( void);
// Testuje potvrzeni odeslani

native SmsRead( native Index, char *Number, char *Text);
// Precte prijatou zpravu z pozice <Index>, vrati jeji delku

TYesNo SmsDelete( native Index);
// Smaze prijatou zpravu z pozice <Index>


//--Datove sluzby -------------------------------------------------------------

TYesNo GsmIsRinging( void);
// Kontrola prichoziho hovoru

void GsmOffHook( void);
// Zvedne telefon

TYesNo GsmConnect( void);
// Kontrola navazani spojeni

void GsmCommandMode( void);
// Prepne na prikazovy rezim (vysle ESC)

void GsmHangUp( void);
// Vysle prikaz zaveseni

#endif
