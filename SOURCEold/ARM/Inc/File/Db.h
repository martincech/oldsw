//******************************************************************************
//
//   Db.h          Database utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Db_H__
   #define __Db_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FdirDef_H__
   #include "FdirDef.h"                // directory entry
#endif

#ifndef __DbRecord_H__
   #include "DbRecord.h"               // project specific data definition
#endif

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Sequential access, read only
//------------------------------------------------------------------------------

TYesNo DbCreate( const char *Name, TFdirClass Class, void *Configuration);
// Create table with <Name>

void DbOpen( TFdirHandle Handle, TYesNo ReadOnly);
// Open table with <Handle>, set <ReadOnly>

void DbEmpty( void);
// Delete all records

#define DbRename( Handle, Name)        FsRename( Handle, Name)
// Rename database

#define DbDelete()                     FsDelete()
// Delete current table

void DbClose( void);
// Close current table

#define DbInfo()                       FsInfo()
// Returns directory entry of the database

void DbLoadConfig( TDbConfig *Configuration);
// Load configuration record <Configuration>

void DbSaveConfig( TDbConfig *Configuration);
// Save configuration record <Configuration>

void DbBegin( void);
// Set before start of the table

TYesNo DbNext( TDbRecord *Record);
// Get next record

//------------------------------------------------------------------------------
// Random Access
//------------------------------------------------------------------------------

int DbCount( void);
// Returns items count

TYesNo DbMoveAt( int RecordIndex);
// Move current record pointer at <RecordIndex>

//------------------------------------------------------------------------------
// Enable write
//------------------------------------------------------------------------------

void DbDeleteRecord( int RecordIndex);
// Delete record at <RecordIndex> position

TYesNo DbDeleteLastRecord( void);
// Delete last record

TYesNo DbAppend( TDbRecord *Record);
// Append <Record> at end of databease

#ifdef __cplusplus
}
#endif

#endif
