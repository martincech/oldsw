//*****************************************************************************
//
//    Uni.h  - universal definitions (GNU ARM based)
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Uni_H__
   #define __Uni_H__

// zakladni typy dat :
typedef unsigned char      byte;
typedef unsigned short     word;
typedef unsigned int       dword;
typedef unsigned long long qword;            
// zakladni typy dat (se znamenkem) :
typedef signed char        int8;       // znamenkovy integer - byte
typedef short              int16;      // znamenkovy integer - word
typedef int                int32;      // znamenkovy integer - dword
typedef long long          int64;      // znamenkovy integer - qword
// zakladni typy dat (bez znamenka) :
typedef unsigned char      uint8;      // byte
typedef unsigned short     uint16;     // word
typedef unsigned int       uint32;     // dword
typedef unsigned long long uint64;     // qword

// prirozeny format (delka registru procesoru) :
typedef int          snative;          // se znamenkem
typedef unsigned int native;           // bez znamenka

// pomocne typy dat :
typedef unsigned bool;                  // bit jako vstupni parametr
typedef unsigned TYesNo;                // bezne funkce vraceji YES=v poradku, NO=chyba
typedef unsigned TOkErr;                // nektere funkce vraceji 0=OK, jinak cislo chyby


enum {
   NO,
   YES
} TYesNoEnum;

enum {
   OK,
   ERR
} TOkErrEnum;

#define if_ok( c)   if( !(c))
#define if_err( c)  if( c)
#define forever     for(;;)

// union na predavani velicin jako dword :

typedef union {
   dword dw;           // array[ 0] = LSB ... array[ 3] = MSB
   word  w;            // array[ 0] = LSB ... array[ 1] = MSB
   byte  b;            // array[ 0]
   byte  array[ 4];
} TDataConvertor;

// Funkce :

#define strequ( s1, s2)    !strcmp( s1, s2)
#define strnequ( s1, s2, n)!strncmp( s1, s2, n)
#define memequ( m1, m2, l) !memcmp( m1, m2, l)

// structure/union alignment :
#define __packed __attribute__ ((packed))

#endif
