//*****************************************************************************
//
//    sputchar.h   string putchar
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __sputchar_H__
   #define __sputchar_H__


void sputcharbuffer( char *buffer);
// set sputchar buffer

int sputchar( int c);
// write character to buffer

#endif
