//******************************************************************************
//                                                                            
//   MenuConfiguration.h  Configuration menu
//   Version 1.0          (c) VymOs
//
//******************************************************************************

#ifndef __MenuConfiguration_H__
   #define __MenuConfiguration_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MenuConfiguration( void);
// Configuration menu

#endif
