//*****************************************************************************
//
//    Backlight.h   Backlight functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Backlight_H__
   #define __Backlight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void BacklightInit( void);
// Initialisation

void BacklightCharger( void);
// Set charger mode

void BacklightNormal( void);
// Set normal mode

void BacklightBoot( void);
// Set backlight mode to boot

void BacklightOn( void);
// Conditionaly on

void BacklightOff( void);
// Conditionaly off

void BacklightShutdown( void);
// Power off

void BacklightTest( int Intensity);
// Test backlight intensity

//--- boot mode only :

void BacklightDark( void);
// Unconditionaly Backlight off

void BacklightLight( void);
// Unconditionaly Backlight on

void BacklightFlash( void);
// Unconditionaly flash backlight

#endif
