//******************************************************************************
//                                                                            
//   MenuWUnits.c    Weighing units menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuWUnits.h"

#include "../Inc/Graphic.h"       // graphic
#include "../Inc/File/Fd.h"       // File directory
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "../Inc/Cpu.h"           // WatchDog

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DWeight.h"          // Display weight

#include "Str.h"                  // Strings
#include "MenuCalibrate.h"        // Menu Calibration
#include "Bat1.h"                 // Bat1 Definitions
#include "Cal.h"                  // Calibration
#include "Sdb.h"                  // Samples database

static DefMenu( ServiceWeighingMenu)
   STR_UNITS,
   STR_DIVISION,
   STR_RANGE,
   STR_CALIBRATION,
EndMenu()

typedef enum {
   SW_UNITS,
   SW_DIVISION,
   SW_RANGE,
   SW_CALIBRATION,
} TServiceWeighingMenuEnum;

// Local functions :

static void WeighingUnitsParameters( int Index, int y, void *UserData);
// Menu weighing units parameters

static void SetUnits( int Units);
// Set weighing units

static int EnumToCapacity( int Code);
// Decode capacity from <Code>

static int CapacityToEnum( int Capacity);
// Encode <Capacity> to enum

static void FilesSetUnits( int Units);
// Set weighing units for all files

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuWeighingUnits( void)
// Menu weighing units
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_WEIGHING, ServiceWeighingMenu, WeighingUnitsParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case SW_UNITS :
            i = Config.Units.Units;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_UNITS_KG, _UNITS_COUNT)){
               break;
            }
            SetUnits( i);
            ConfigSaveSection( Units);
            break;

         case SW_DIVISION :
            i = Config.Units.Division;
            if( !DEditWeight( DMENU_EDIT_X, MData.y, &i, 1, Config.Units.MaxDivision)){
               break;
            }
            Config.Units.Division = i;
            ConfigSaveSection( Units.Division);
            break;

         case SW_RANGE :
            i = CapacityToEnum( Config.Units.Range);
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_CAPACITY_NORMAL, _CAPACITY_COUNT)){
               break;
            }
            Config.Units.Range = EnumToCapacity( i);
            ConfigSaveSection( Units.Range);
            break;

         case SW_CALIBRATION :
            MenuCalibrate();
            break;

      }
   }
} // MenuWeighingUnits

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void WeighingUnitsParameters( int Index, int y, void *UserData)
// Menu weighing units parameters
{
   switch( Index){
      case SW_UNITS :
         DLabelEnum( Config.Units.Units, STR_UNITS_KG, DMENU_PARAMETERS_X, y);
         break;

      case SW_DIVISION :
         DWeightWithUnitsNarrow( DMENU_PARAMETERS_X, y, Config.Units.Division);
         break;

      case SW_RANGE :
         DLabelEnum( CapacityToEnum( Config.Units.Range), STR_CAPACITY_NORMAL, DMENU_PARAMETERS_X, y);
         break;
   }
} // WeighingUnitsParameters

//------------------------------------------------------------------------------
//   Units
//------------------------------------------------------------------------------

static void SetUnits( int Units)
// Set weighing units
{
int Capacity;

   Capacity = CapacityToEnum( Config.Units.Range);    // save old capacity as enum
   Config.Units.Units = Units;                        // set new units
   Config.Units.Range = EnumToCapacity( Capacity);    // set new capacity
   switch( Units){
      case UNITS_KG :
         Config.Units.Decimals    = UNITS_KG_DECIMALS;
         Config.Units.MaxDivision = UNITS_KG_MAX_DIVISION;
         Config.Units.Division    = UNITS_KG_DIVISION;
         // saving parameters :
         Config.WeighingParameters.Saving.MinimumWeight = SAVING_MINIMUM_KG;
         break;
      case UNITS_G :
         Config.Units.Decimals    = UNITS_G_DECIMALS;
         Config.Units.MaxDivision = UNITS_G_MAX_DIVISION;
         Config.Units.Division    = UNITS_G_DIVISION;
         // saving parameters :
         Config.WeighingParameters.Saving.MinimumWeight = SAVING_MINIMUM_G;
         break;
      case UNITS_LB :
         Config.Units.Decimals    = UNITS_LB_DECIMALS;
         Config.Units.MaxDivision = UNITS_LB_MAX_DIVISION;
         Config.Units.Division    = UNITS_LB_DIVISION;
         // saving parameters :
         Config.WeighingParameters.Saving.MinimumWeight = SAVING_MINIMUM_LB;
         break;
   }
   FilesSetUnits( Units);
} // SetUnits

//------------------------------------------------------------------------------
//   Enum to capacity
//------------------------------------------------------------------------------

static int EnumToCapacity( int Code)
// Decode capacity from <Code>
{
   switch( Config.Units.Units){
      case UNITS_KG :
         if( Code == CAPACITY_EXTENDED){
            return( UNITS_KG_EXT_RANGE);
         } else {
            return( UNITS_KG_RANGE);
         }
      case UNITS_G :
         if( Code == CAPACITY_EXTENDED){
            return( UNITS_G_EXT_RANGE);
         } else {
            return( UNITS_G_RANGE);
         }
      case UNITS_LB :
         if( Code == CAPACITY_EXTENDED){
            return( UNITS_LB_EXT_RANGE);
         } else {
            return( UNITS_LB_RANGE);
         }
   }
   return( 0);
} // EnumToCapacity

//------------------------------------------------------------------------------
//   Capacity to enum
//------------------------------------------------------------------------------

static int CapacityToEnum( int Capacity)
// Encode <Capacity> to enum
{
   switch( Config.Units.Units){
      case UNITS_KG :
         if( Capacity == UNITS_KG_EXT_RANGE){
            return( CAPACITY_EXTENDED);
         } else {
            return( CAPACITY_NORMAL);
         }
      case UNITS_G :
         if( Capacity == UNITS_G_EXT_RANGE){
            return( CAPACITY_EXTENDED);
         } else {
            return( CAPACITY_NORMAL);
         }
      case UNITS_LB :
         if( Capacity == UNITS_LB_EXT_RANGE){
            return( CAPACITY_EXTENDED);
         } else {
            return( CAPACITY_NORMAL);
         }
   }
   return( 0);
} // CapacityToEnum

//------------------------------------------------------------------------------
//   File Units
//------------------------------------------------------------------------------

static void FilesSetUnits( int Units)
// Set weighing units for all files
{
TFdirHandle Handle;

   FdSetClass( SDB_CLASS);
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      SdbOpen( Handle, NO);
      SdbConfig()->Saving.MinimumWeight = Config.WeighingParameters.Saving.MinimumWeight;
      SdbClose();
      WatchDog();
   }
} // FilesSetUnits
