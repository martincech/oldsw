//******************************************************************************
//                                                                            
//   Password.c     Password checking
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Password.h"

#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DMsg.h"             // Display message

#include "Str.h"                  // Strings
#include "Beep.h"                 // Sounds

#include <string.h>

#define PWD_FLAG_DISABLED  0x80        // trial expired
#define PWD_FLAG_ENABLED   0x40        // password already passed
#define PWD_FLAG_COUNT     0x0F        // trials counter
#define PASSWORD_TRIALS       5        // max. trials

static byte PasswordFlag;

#define PasswordDisable()    memset( Config.Password, 0, PASSWORD_LENGTH)

//------------------------------------------------------------------------------
//  Check
//------------------------------------------------------------------------------

TYesNo PasswordCheck( void)
// Password checking, returns YES if passed
{
char Password[ PASSWORD_LENGTH];

   if( PasswordInactive()){
      return( YES);                              // always passed
   }
   // check for password :
   if( PasswordFlag & PWD_FLAG_DISABLED){
      BeepError();                               // no trial
      return( NO);
   }
   if( PasswordFlag & PWD_FLAG_ENABLED){
      return( YES);                              // already passed
   }
   DInputPassword( STR_PASSWORD, STR_ENTER_PASSWORD, Password, PASSWORD_LENGTH);
   if( memequ( Password, Config.Password, PASSWORD_LENGTH)){
      PasswordFlag = PWD_FLAG_ENABLED;           // passed
      return( YES);
   }
   PasswordFlag++;
   if( (PasswordFlag & PWD_FLAG_COUNT) >= PASSWORD_TRIALS){
      PasswordFlag |= PWD_FLAG_DISABLED;         // trials expired
   }
   DMsgOk( STR_ERROR, STR_INVALID_PASSWORD, 0);
   return( NO);
} // PasswordCheck

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

TYesNo PasswordInactive( void)
// Password is inactive
{
   return( Config.Password[ 0] == 0);
} // PasswordInactive

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

void PasswordEdit( void)
// Edit new password
{
char Password[ PASSWORD_LENGTH];
char Password2[ PASSWORD_LENGTH];

   // protect by password :
   if( !DMsgYesNo( STR_PASSWORD, STR_PROTECT_BY_PASSWORD, 0)){
      PasswordInactivate();
      return;
   }
   // input password :
   DInputPassword( STR_PASSWORD, STR_ENTER_NEW_PASSWORD, Password,  PASSWORD_LENGTH);
   DInputPassword( STR_PASSWORD, STR_CONFIRM_PASSWORD,   Password2, PASSWORD_LENGTH);
   if( !memequ( Password, Password2, PASSWORD_LENGTH)){
      DMsgOk( STR_ERROR, STR_PASSWORDS_DONT_MATCH, 0);
      PasswordInactivate();
      return;
   }
   // save password
   memcpy( Config.Password, Password, PASSWORD_LENGTH);
   ConfigSaveSection( Password);
   PasswordClear();                                        // activate now
} // PasswordEdit

//------------------------------------------------------------------------------
//  Inactivate
//------------------------------------------------------------------------------

void PasswordInactivate( void)
// Inactivate password checking
{
   PasswordDisable();
   ConfigSaveSection( Password);
} // PasswordInactivate

//------------------------------------------------------------------------------
//  Clear
//------------------------------------------------------------------------------

void PasswordClear( void)
// Clear password trials
{
   PasswordFlag = 0;
} // PasswordClear
