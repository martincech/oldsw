//******************************************************************************
//
//   MenuSounds.c    Sounds configuration menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuSounds.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Wgt/DMsg.h"             // Display message
#include "Str.h"                  // Strings
#include "Beep.h"                 // Beep

static DefMenu( SoundsMenu)
   STR_SAVING,
   STR_KEYBOARD,
   STR_SPECIAL_SOUNDS,
EndMenu()

typedef enum {
   SM_SAVING,
   SM_KEYBOARD,
   SM_SPECIAL_SOUNDS,
} TSoundsMenuEnum;

// Local functions :

static void SoundsParameters( int Index, int y, void *UserData);
// Menu Sounds parameters

static void MenuSoundsSaving( void);
// Sounds/Saving submenu

static void SavingParameters( int Index, int y, void *UserData);
// Menu Saving parameters

static void MenuSoundsKeyboard( void);
// Sounds/Keyboard submenu

static void KeyboardParameters( int Index, int y, void *UserData);
// Menu Saving parameters

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuSounds( void)
// Sounds configuration menu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_SOUNDS, SoundsMenu, SoundsParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case SM_SAVING :
            MenuSoundsSaving();
            break;

         case SM_KEYBOARD :
            MenuSoundsKeyboard();
            break;

         case SM_SPECIAL_SOUNDS :
            i = Config.Sounds.EnableSpecial;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_NO, 2)){
               break;
            }
            Config.Sounds.EnableSpecial = i;
            break;
      }
   }
} // MenuSounds

//------------------------------------------------------------------------------
//   Sounds parameters
//------------------------------------------------------------------------------

static void SoundsParameters( int Index, int y, void *UserData)
// Menu Sounds parameters
{
   switch( Index){
      case SM_SAVING :
         break;

      case SM_KEYBOARD :
         break;

      case SM_SPECIAL_SOUNDS :
         DLabelEnum( Config.Sounds.EnableSpecial, STR_NO, DMENU_PARAMETERS_X, y);
         break;
   }
} // SoundsParameters

//******************************************************************************

//------------------------------------------------------------------------------
//   Saving submenu
//------------------------------------------------------------------------------

static DefMenu( SoundsSavingMenu)
   STR_DEFAULT_TONE,
   STR_TONE_LIGHT,
   STR_TONE_OK,
   STR_TONE_HEAVY,
   STR_VOLUME,
EndMenu()

typedef enum {
   SS_DEFAULT_TONE,
   SS_TONE_LIGHT,
   SS_TONE_OK,
   SS_TONE_HEAVY,
   SS_VOLUME,
} TSoundsSavingMenuEnum;

static void MenuSoundsSaving( void)
// Sounds/Saving submenu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_SAVING_SOUNDS, SoundsSavingMenu, SavingParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case SS_DEFAULT_TONE :
            i = Config.Sounds.ToneDefault;
            if( !DEditEnumCallback( DMENU_EDIT_X, MData.y, &i, STR_BEEP_TONE1, BEEP_TONE_COUNT, BeepToneTest)){
               break;
            }
            Config.Sounds.ToneDefault = i;
            break;

         case SS_TONE_LIGHT :
            i = Config.Sounds.ToneLight;
            if( !DEditEnumCallback( DMENU_EDIT_X, MData.y, &i, STR_BEEP_TONE1, BEEP_TONE_COUNT, BeepToneTest)){
               break;
            }
            Config.Sounds.ToneLight = i;
            break;

         case SS_TONE_OK :
            i = Config.Sounds.ToneOk;
            if( !DEditEnumCallback( DMENU_EDIT_X, MData.y, &i, STR_BEEP_TONE1, BEEP_TONE_COUNT, BeepToneTest)){
               break;
            }
            Config.Sounds.ToneOk = i;
            break;

         case SS_TONE_HEAVY :
            i = Config.Sounds.ToneHeavy;
            if( !DEditEnumCallback( DMENU_EDIT_X, MData.y, &i, STR_BEEP_TONE1, BEEP_TONE_COUNT, BeepToneTest)){
               break;
            }
            Config.Sounds.ToneHeavy = i;
            break;

         case SS_VOLUME :
            i = Config.Sounds.VolumeSaving;
            if( !DEditVolume( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Config.Sounds.VolumeSaving = i;
            break;
      }
   }
} // MenuSoundsSaving

//------------------------------------------------------------------------------
//   Saving submenu parameters
//------------------------------------------------------------------------------

static void SavingParameters( int Index, int y, void *UserData)
// Menu Saving parameters
{
   switch( Index){
      case SS_DEFAULT_TONE :
         DLabelEnum( Config.Sounds.ToneDefault, STR_BEEP_TONE1, DMENU_PARAMETERS_X, y);
         break;

      case SS_TONE_LIGHT :
         DLabelEnum( Config.Sounds.ToneLight, STR_BEEP_TONE1, DMENU_PARAMETERS_X, y);
         break;

      case SS_TONE_OK :
         DLabelEnum( Config.Sounds.ToneOk, STR_BEEP_TONE1, DMENU_PARAMETERS_X, y);
         break;

      case SS_TONE_HEAVY :
         DLabelEnum( Config.Sounds.ToneHeavy, STR_BEEP_TONE1, DMENU_PARAMETERS_X, y);
         break;

      case SS_VOLUME :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Sounds.VolumeSaving);
         break;
   }
} // SavingParameters

//------------------------------------------------------------------------------
//   Keyboard submenu
//------------------------------------------------------------------------------

static DefMenu( SoundsKeyboardMenu)
   STR_TONE,
   STR_VOLUME,
EndMenu()

typedef enum {
   SK_TONE,
   SK_VOLUME,
} TSoundsKeyboardMenuEnum;


static void MenuSoundsKeyboard( void)
// Sounds/Keyboard submenu
{
TMenuData MData;
int       i;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_KEYBOARD_SOUNDS, SoundsKeyboardMenu, KeyboardParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case SK_TONE :
            i = Config.Sounds.ToneKeyboard;
            if( !DEditEnumCallback( DMENU_EDIT_X, MData.y, &i, STR_BEEP_TONE1, BEEP_SINGLE_TONE_COUNT, BeepToneTest)){
               break;
            }
            Config.Sounds.ToneKeyboard = i;
            break;

         case SK_VOLUME :
            i = Config.Sounds.VolumeKeyboard;
            if( !DEditVolume( DMENU_EDIT_X, MData.y, &i)){
               break;
            }
            Config.Sounds.VolumeKeyboard = i;
            break;
      }
   }
} // MenuSoundsKeyboard

//------------------------------------------------------------------------------
//   Keyboard submenu parameters
//------------------------------------------------------------------------------

static void KeyboardParameters( int Index, int y, void *UserData)
// Menu Saving parameters
{
   switch( Index){
      case SK_TONE :
         DLabelEnum( Config.Sounds.ToneKeyboard, STR_BEEP_TONE1, DMENU_PARAMETERS_X, y);
         break;

      case SK_VOLUME :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Sounds.VolumeKeyboard);
         break;
   }
} // KeyboardParameters
