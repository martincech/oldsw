//******************************************************************************
//                                                                            
//   Menu.c         Bat1 menu
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Menu.h"

#include "../Inc/System.h"        // Operating system
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/Kbd.h"           // Keyboard
#include "../Inc/File/Fd.h"       // File directory
#include "../Inc/Wgt/DEvent.h"    // Display events

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DWeight.h"          // Display weight

#include "Str.h"                  // Strings
#include "Beep.h"                 // Sound
#include "PMan.h"                 // Power management
#include "Backlight.h"            // Backlight
#include "Weighing.h"             // Weighing utility
#include "Screen.h"               // Basic screen
#include "ScreenStatistics.h"     // Statistics screen
#include "Printer.h"              // Printer
#include "Sdb.h"                  // Samples database
#include "MenuWeighing.h"         // Weighing menu
#include "MenuStatistics.h"       // Statistics menu
#include "MenuSettings.h"         // Settings menu
#include "MenuConfiguration.h"    // Configuration menu
#include "MenuMaintenance.h"      // Maintenance menu

// Local functions :

static void MenuMain( void);
// Main menu

static void ShowStatistics( void);
// Display statistics of the working file

//------------------------------------------------------------------------------
//   Charger loop
//------------------------------------------------------------------------------

void MenuCharger( void)
// Display charging
{
   forever {
      switch( SysWaitEvent()){
         case K_ESC :
            PManSetPowerOn();          // key ESC pressed - power on
            return;                    // run device

         case K_FLASH1 :
            ScreenCharger();
            break;

         case K_SHUTDOWN :
            // OFF key pressed or charger removed or power fail
            PManClrShutdown();         // confirm shutdown
            if( PManAccuFail()){
               // charger failure
               ScreenPowerFail();      // display status
               SysDelay( 1000);        // show status
               BacklightShutdown();    // backlight off
               PManPowerOff();         // no return
            }
            if( !PManCharger() && !PManUsb()){
               // charger removed 
               ScreenChargerOff();     // display status
               SysDelay( 1000);        // show status
               BacklightShutdown();    // backlight off
               PManPowerOff();         // no return
            }
            // OFF button pressed
            break;
      }
      GFlush();
   }
} // MenuCharger

//------------------------------------------------------------------------------
//   Main loop
//------------------------------------------------------------------------------

void MenuExecute( void)
// Main menu loop
{
char Buffer[ DWEIGHT_MAX_LENGTH];
static byte EscDone;

   WeighingStart();                    // power up default action
   forever {
      DEventDiscard();                 // stop timeout chain
      switch( SysWaitEvent()){
         case K_DOWN :
            BeepKey();
            WeighingSuspend();         // stop weighing
            MenuMain();                // execute main menu
            WeighingRelease();         // start weighing
            break;

         case K_UP :
            if( !SdbInfo()){
               break;                  // no working file
            }
            BeepKey();
            WeighingSuspend();         // stop weighing
            ShowStatistics();          // show statistics & histogram
            WeighingRelease();         // start weighing
            break;
               
         case K_ENTER :
            WeighingManualMale();
            break;

         case K_RIGHT :
            WeighingManualFemale();
            break;

         case K_LEFT :   
            if( WeighingLastFlag() == FLAG_INVALID){
               break;                  // no last sample
            }
            BeepKey();
            // format last weight :
            DWeightWithUnitsFormat( Buffer, WeighingLastWeight());
            // display confirmation :
            if( !DMsgYesNo( STR_DELETE_SAMPLE, STR_DELETE_LAST_SAMPLE, Buffer)){
               break;
            }
            WeighingDelete();
            break;

         case K_ESC :
            EscDone = NO;              // wait for first repeat
            break;

         case K_ESC | K_REPEAT :
            if( EscDone){
               break;                  // skip next repeat
            }
            EscDone = YES;             // first repeat done
            BeepKey();
            WeighingTara();
            break;

         case K_FLASH1 :
            WeighingExecute();
            ScreenRedraw();
            break;

         case K_FLASH2 :
            ScreenFlash();
            break;

         case K_SHUTDOWN :
            // OFF key pressed or power fail
            PManClrShutdown();         // confirm shutdown
            WeighingStop();            // stop weighing
            if( PManAccuFail()){
               ScreenPowerFail();      // display status
               SysDelay( 1000);        // show status
               PManPowerOff();         // no return
            }
            // OFF button pressed
            ScreenShutdown();          // display status
            BeepBye();
            SysDelay( 1000);           // show status
            BacklightShutdown();       // backlight off
            PManSetPowerOff();         // power off request
            return;                    // check for charger or power off
      }
      GFlush();
   }
} // MenuExecute

//------------------------------------------------------------------------------
//   Main Menu
//------------------------------------------------------------------------------

static DefMenu( MainMenu)
   STR_WEIGHING,
   STR_STATISTICS,
   STR_SETTINGS,
   STR_CONFIGURATION,
   STR_MAINTENANCE,
EndMenu()

typedef enum {
   MM_WEIGHING,
   MM_STATISTICS,
   MM_SETTINGS,
   MM_CONFIGURATION,
   MM_MAINTENANCE,
} TMainMenuEnum;

static void MenuMain( void)
// Main menu
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_MAIN_MENU, MainMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MM_WEIGHING :
            MenuWeighing();
            break;

         case MM_STATISTICS :
            MenuStatistics();
            break;

         case MM_SETTINGS :
            MenuSettings();
            break;

         case MM_CONFIGURATION :
            MenuConfiguration();
            break;

         case MM_MAINTENANCE :
            MenuMaintenance();
            break;
      }
   }
} // MenuMain

//------------------------------------------------------------------------------
//   Show statistics
//------------------------------------------------------------------------------

static void ShowStatistics( void)
// Display statistics of the working file
{
char Name[ FDIR_NAME_LENGTH + 1];

   // closed database, uses frozen values
   FdGetName( Config.LastFile, Name);
   if( SdbConfig()->Saving.Mode == SAVING_MODE_MANUAL_BY_SEX){
      ScreenStatisticsBySex( Name);  
   } else {
      ScreenStatisticsByWeight( Name, 
                                SdbConfig()->WeightSorting.Mode, 
                                SdbConfig()->WeightSorting.LowLimit, 
                                SdbConfig()->WeightSorting.HighLimit);
   }
} // ShowStatistics
