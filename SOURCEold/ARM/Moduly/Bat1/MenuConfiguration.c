//******************************************************************************
//                                                                            
//   MenuConfiguration.c  Configuration menu
//   Version 1.0          (c) VymOs
//
//******************************************************************************

#include "MenuConfiguration.h"

#include "Wgt/DMenu.h"            // Display menu

#include "Str.h"                  // Strings
#include "Password.h"             // Password checking
#include "MenuFiles.h"            // Files menu
#include "MenuGroups.h"           // File groups menu
#include "MenuDisplay.h"          // Display configration menu
#include "MenuSounds.h"           // Sounds configuration menu
#include "MenuSaving.h"           // Saving parameters
#include "MenuStatParameters.h"   // Statistic parameters

static DefMenu( ConfigurationMenu)
   STR_FILES,
   STR_FILE_GROUPS,
   STR_SAVING,
   STR_DISPLAY,
   STR_SOUNDS,
   STR_STATISTICS,
EndMenu()

typedef enum {
   CM_FILES,
   CM_FILE_GROUPS,
   CM_SAVING,
   CM_DISPLAY,
   CM_SOUNDS,
   CM_STATISTICS,
} TConfigurationMenuEnum;

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuConfiguration( void)
// Configuration menu
{
TMenuData MData;

   // check for password :
   if( !PasswordCheck()){
      return;
   }
   // run menu :
   DMenuClear( MData);
   forever {
      if( !DMenu( STR_CONFIGURATION, ConfigurationMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case CM_FILES :
            MenuFiles();
            break;

         case CM_FILE_GROUPS :
            MenuFileGroups();
            break;

         case CM_SAVING :
            MenuSavingParameters( &Config.WeighingParameters, YES);
            ConfigSaveSection( WeighingParameters);
            break;

         case CM_DISPLAY :
            MenuDisplay();
            ConfigSaveSection( Display);
            break;

         case CM_SOUNDS :
            MenuSounds();
            ConfigSaveSection( Sounds);
            break;

         case CM_STATISTICS :
            MenuStatisticParameters( &Config.StatisticParameters);
            ConfigSaveSection( StatisticParameters);
            break;
      }
   }
} // MenuConfiguration
