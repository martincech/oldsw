//******************************************************************************
//
//   Printer.c    Printer services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "Printer.h"
#include "../Inc/System.h"
#include "../Inc/uconio.h"             // UART conio
#include "../Inc/Cpu.h"                // WatchDog
#include "Wgt/DMsg.h"

#include "Str.h"
#include "Sdb.h"                       // Samples database
#include "Group.h"                     // Files Group
#include "../Inc/File/Fd.h"            // File directory
#include "../Inc/xTime.h"              // format date/time
#include "xWeight.h"                   // format weight

#ifdef UCONIO_COM0
   #define __COM0__
      #include "../inc/Com.h"
   #define COM_BAUD    UART0_BAUD
   #define COM_FORMAT  UART0_FORMAT
   #define COM_TIMEOUT UART0_TIMEOUT
#else // UCONIO_COM1
   #define __COM1__
      #include "../inc/Com.h"
   #define COM_BAUD    UART1_BAUD
   #define COM_FORMAT  UART1_FORMAT
   #define COM_TIMEOUT UART1_TIMEOUT
#endif

#define PRINTER_LINE_FAST    250         // fast line print duration
#define PRINTER_LINE_SLOW    500         // slow line print duration
#define SAMPLE_WIDTH         8           // sample column width

// Variables :
static word _LineDelay;                  // line print duration

// Macros :
#define WaitOk() if( !PrintWait()) return( NO)

// Local functions :

static TYesNo PrintProlog( const char *Name, TPrintSet Set);
// Print header of the protocol

static TYesNo PrintEpilog( void);
// Print footer of the protocol

static TYesNo PrintGroupRange( TGroup Group);
// Print group samples date/time range

static TYesNo PrintFileRange( TFdirHandle Handle);
// Print file samples date/time range

static TYesNo GetFileRange( TFdirHandle Handle, TTimestamp *From, TTimestamp *To);
// Get samples date/time range

static TYesNo PrintFilesList( TGroup Group);
// Print files list

static TYesNo PrintGroupSamples( TGroup Group, TPrintSet Set);
// Print samples by <Group>

static TYesNo PrintProtocol( const char *Name, TGroup Group, TPrintSet Set, TCalcMode Mode, TPrinterProtocol Protocol);
// Print standard protocol

static TYesNo PrintSamples( TFdirHandle Handle);
// Print file samples

static TYesNo PrintStatisticsAndHistogram( TCalcMode Mode, TPrinterProtocol Protocol);
// Print statistics and histogram

static TYesNo PrintStatistics( TCalc *Statistics);
// Print statistics

static TYesNo PrintHistogram( THistogram *Histogram);
// Print histogram

//------------------------------------------------------------------------------

static TYesNo PrintWait( void);
// Wait for the line output

static void PLine( void);
// Print separation line

static void PDoubleLine( void);
// Print separation line

static void PCharLine( char ch);
// Print separation line with <ch>

static void PCharacters( char ch, int Count);
// Print character string

static void PWeight( TWeight Weight);
// Print weight

static void PWeightWithUnits( TWeight Weight);
// Print weight with units

void PWeighingUnits( void);
// Print current units

static void PDateTime( TTimestamp Now);
// Print date & time

//------------------------------------------------------------------------------
// Printer setup
//------------------------------------------------------------------------------

void PrinterSetup( void)
// Set printer communication parameters
{
   ComSetup( Config.Printer.CommunicationSpeed, Config.Printer.CommunicationFormat, COM_TIMEOUT);
} // PrinterSetup

//------------------------------------------------------------------------------
// Print
//------------------------------------------------------------------------------

void PrinterPrint( const char *Name, TGroup Group, TPrintSet Set, TCalcMode Mode, TPrinterProtocol Protocol)
// Print <Protocol> of the <Group>/<Mode> with title <Name>
{
   PrinterSetup();
   // set line delay :
   if( Config.Printer.CommunicationSpeed >= 9600){
      _LineDelay = PRINTER_LINE_FAST;
   } else {
      _LineDelay = PRINTER_LINE_SLOW;
   }
   // confirm printing :
   if( !DMsgYesNo( STR_PRINT, STR_START_PRINTING, 0)){
      return;
   }
   // start printing :
   SysDisableTimeout();                          // disable keyboard timeout
   DMsgCancel( STR_PRINT, STR_PRINTING, 0);      // show cancel window
   if( !PrintProlog( Name, Set)){                // print header
      SysEnableTimeout();                        // enable keyboard timeout
      return;                                    // print canceled
   }
   if( (Protocol == PROTOCOL_TOTAL_STATISTICS) && (Set != PRINT_FILE)){
      if( !PrintFilesList( Group)){
         SysEnableTimeout();                     // enable keyboard timeout
         return;                                 // print canceled
      }
   }
   if( Protocol == PROTOCOL_SAMPLES){
      PrintGroupSamples( Group, Set);
   } else {
      PrintProtocol( Name, Group, Set, Mode, Protocol);
   }
   SysEnableTimeout();                 // enable keyboard timeout
} // PrinterPrint

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

void PrinterWeight( int Index, TWeight Weight)
// Print single <Weight> with <Index>
{
   uprintf( "%5d   ", Index);
   PWeight( Weight);
   uputchar( '\n');
} // PrinterWeight

#ifdef PROJECT_PRINTER_ACTUAL_WEIGHT
//------------------------------------------------------------------------------
// Actual Weight
//------------------------------------------------------------------------------

// Factor approximation for lb/kg recalculation (1lb = 0.453592kg)
// Factor is CAL_LB_MUL / (1 << CAL_LB_SHIFT) :
#define CAL_LB_MUL      475626   // 0.453592 * (1024 * 1024) rounded
#define CAL_LB_SHIFT    20       // 1024 * 1024

void PrinterActualWeight( TWeight Weight)
// Print single <Weight>
{
#ifdef PROJECT_PRINTER_TELXON
   // convert to grams :
   if( Config.Units.Units == UNITS_LB){
      Weight = (TWeight)(((TMulWeight)Weight * CAL_LB_MUL * 2) >> CAL_LB_SHIFT); // 2 * Weight
      if( Weight > 0){
         // round positive
         Weight++;
         Weight >>= 1;
      }
   }
   // saturate to range :
   if( Weight < 0){
      Weight = 0;
   }
   if( Weight > 9999){
      Weight = 9999;
   }
   uprintf( "+     %4d g  \n", Weight);
#else
   PWeight( Weight);
   uputchar( '\n');
#endif
} // PrinterActualWeight
#endif // PROJECT_PRINTER_ACTUAL_WEIGHT

//******************************************************************************

//------------------------------------------------------------------------------
// Prolog
//------------------------------------------------------------------------------

static TYesNo PrintProlog( const char *Name, TPrintSet Set)
// Print header of the protocol
{
   PDoubleLine();
   WaitOk();
   uprintf( "Scale  : %s\n", Config.ScaleName);
   WaitOk();
   switch( Set){
      case PRINT_FILE :
         break;
      case PRINT_GROUP :
         uprintf( "Group  : %s\n", Name);
         WaitOk();
         break;
      case PRINT_MORE_FILES :
         break;
   }
   uputs( "Print  : "); PDateTime( SysGetClock()); uputchar( '\n');
   WaitOk();
   return( YES);
} // PrintProlog

//------------------------------------------------------------------------------
// Epilog
//------------------------------------------------------------------------------

static TYesNo PrintEpilog( void)
// Print footer of the protocol
{
   PDoubleLine();
   WaitOk();
   uputchar( '\n');
   WaitOk();
   uputchar( '\n');
   WaitOk();
   uputchar( '\n');
   WaitOk();
   return( YES);
} // PrintEpilog

//------------------------------------------------------------------------------
// Group Range
//------------------------------------------------------------------------------

static TYesNo PrintGroupRange( TGroup Group)
// Print samples date/time range
{
TTimestamp TsMin, TsMax;
TTimestamp From, To;
int        Handle;
TYesNo     RangeOk;

   // get minimal, maximal timestamp from <Group>
   TsMin = 0x7FFFFFFF;
   TsMax = 0;
   // get every file
   RangeOk = NO;
   for( Handle = 1; Handle <= FDIR_SIZE; Handle++){
      if( !GroupIsMember( Group, Handle)){
         continue;
      }
      if( !GetFileRange( Handle, &From, &To)){
         continue;                     // empty file
      }
      RangeOk = YES;                   // at least one file
      if( From < TsMin){
         TsMin = From;
      }
      if( To > TsMax){
         TsMax = To;
      }
   }
   if( !RangeOk){
      return( YES);                    // all files empty
   }
   uprintf( "From   : "); PDateTime( TsMin); uputchar( '\n');
   WaitOk();
   uprintf( "To     : "); PDateTime( TsMax); uputchar( '\n');
   WaitOk();
   return( YES);
} // PrintGroupRange

//------------------------------------------------------------------------------
// File Range
//------------------------------------------------------------------------------

static TYesNo PrintFileRange( TFdirHandle Handle)
// Print file samples date/time range
{
TTimestamp From, To;

   if( !GetFileRange( Handle, &From, &To)){
      return( YES);                    // empty file
   }
   uprintf( "From   : "); PDateTime( From); uputchar( '\n');
   WaitOk();
   uprintf( "To     : "); PDateTime( To);   uputchar( '\n');
   WaitOk();
   return( YES);
} // PrintFileRange

//------------------------------------------------------------------------------
// Get File Range
//------------------------------------------------------------------------------

static TYesNo GetFileRange( TFdirHandle Handle, TTimestamp *From, TTimestamp *To)
// Get samples date/time range
{
TTimestamp TsMin, TsMax;
TSdbRecord Record;

   // get minimal, maximal timestamp from <Group>
   TsMin = 0x7FFFFFFF;
   TsMax = 0;
   SdbOpen( Handle, YES);       // open file
   if( !SdbCount()){
      *From = 0;
      *To   = 0;
      return( NO);
   }
   SdbBegin();                  // at first item
   while( SdbNext( &Record)){
      if( Record.Timestamp < TsMin){
         TsMin = Record.Timestamp;
      }
      if( Record.Timestamp > TsMax){
         TsMax = Record.Timestamp;
      }
      WatchDog();
   }
   SdbClose();
   *From = TsMin;
   *To   = TsMax;
   return( YES);
} // GetFileRange

//------------------------------------------------------------------------------
// Files list
//------------------------------------------------------------------------------

static TYesNo PrintFilesList( TGroup Group)
// Print files list
{
int  Handle;
char FileName[ FDIR_NAME_LENGTH + 1];

   uprintf( "Files  : \n");
   WaitOk();
   // print every file
   for( Handle = 1; Handle <= FDIR_SIZE; Handle++){
      if( !GroupIsMember( Group, Handle)){
         continue;
      }
      FdGetName( Handle, FileName); // get file name
      uprintf( "   %s\n", FileName);
      WaitOk();
   }
   return( YES);
} // PrintFilesList

//------------------------------------------------------------------------------
// Group Samples
//------------------------------------------------------------------------------

static TYesNo PrintGroupSamples( TGroup Group, TPrintSet Set)
// Print samples by <Group>
{
int  Handle;
char Name[ FDIR_NAME_LENGTH + 1];

   PLine();
   WaitOk();
   uputs( "Samples ["); PWeighingUnits(); uputs( "]\n");
   WaitOk();
   for( Handle = 1; Handle <= FDIR_SIZE; Handle++){
      if( !GroupIsMember( Group, Handle)){
         continue;
      }
      PLine();
      WaitOk();
      FdGetName( Handle, Name);        // get file name
      uprintf( "File   : %s\n", Name);
      WaitOk();
      if( !PrintFileRange( Handle)){
         return( NO);
      }
      if( !PrintSamples( Handle)){
         return( NO);
      }
   }
   return( PrintEpilog());
} // PrintGroupSamples

//------------------------------------------------------------------------------
// Protocol
//------------------------------------------------------------------------------

static TYesNo PrintProtocol( const char *Name, TGroup Group, TPrintSet Set, TCalcMode Mode, TPrinterProtocol Protocol)
// Print standard protocol
{
int    Handle;
char   FileName[ FDIR_NAME_LENGTH + 1];
TGroup FileGroup;

   if( Protocol != PROTOCOL_TOTAL_STATISTICS){
      // print every file
      for( Handle = 1; Handle <= FDIR_SIZE; Handle++){
         if( !GroupIsMember( Group, Handle)){
            continue;
         }
         PLine();
         WaitOk();
         FdGetName( Handle, FileName); // get file name
         uprintf( "File   : %s\n", FileName);
         WaitOk();
         if( !PrintFileRange( Handle)){
            return( NO);
         }
         SdbOpen( Handle, YES);       // open file
         // check for limits :
         if( Mode == CALC_LIGHT_HEAVY){
            uputs( "Limit :"); PWeightWithUnits( SdbConfig()->WeightSorting.LowLimit); uputchar( '\n');
         } else if( Mode == CALC_LIGHT_OK_HEAVY){
            uputs( "Low  Limit :"); PWeightWithUnits( SdbConfig()->WeightSorting.LowLimit);  uputchar( '\n');
            uputs( "High Limit :"); PWeightWithUnits( SdbConfig()->WeightSorting.HighLimit); uputchar( '\n');
         }
         uputchar( '\n');
         if( !PrintWait()){
            SdbClose();
            return( NO);
         }
         // check for empty :
         if( !SdbCount()){
            SdbClose();
            uputs( "   ** empty **\n");
            WaitOk();
            continue;
         }
         SdbClose();
         // create single file group :
         GroupClear( FileGroup);
         GroupAdd( FileGroup, Handle);   // add to group
         // calculate statistics & histogram :
         CalcMode( Mode);
         GroupSet( FileGroup);
         CalcStatistics();
         if( !PrintStatisticsAndHistogram( Mode, Protocol)){
             return( NO);
         }
      }
   }
   if( Set == PRINT_FILE && Protocol != PROTOCOL_TOTAL_STATISTICS){
      // single file only
      return( PrintEpilog());
   }
   // print total statistics :
   PLine();
   WaitOk();
   if( Set == PRINT_MORE_FILES){
      uprintf( "TOTAL\n");
      WaitOk();
   } else {
      uprintf( "TOTAL %s\n", Name);
      WaitOk();
   }
   if( !PrintGroupRange( Group)){      // from/to date time
      return( NO);
   }
   uputchar( '\n');
   WaitOk();
   // calculate statistics & histogram :
   CalcMode( Mode);
   GroupSet( Group);
   CalcStatistics();
   if( !PrintStatisticsAndHistogram( Mode, Protocol)){
      return( NO);
   }
   return( PrintEpilog());
} // PrintProtocol

//------------------------------------------------------------------------------
// Samples
//------------------------------------------------------------------------------

static TYesNo PrintSamples( TFdirHandle Handle)
// Print file samples
{
TSdbRecord Record;
int        Count;
int        SamplesPerLine;

   SamplesPerLine = Config.Printer.PaperWidth / SAMPLE_WIDTH;
   SdbOpen( Handle, YES);              // open database
   if( !SdbCount()){
      SdbClose();
      uputs( "   ** empty **\n");
      WaitOk();
      return( YES);
   }
   SdbBegin();                         // at first item
   Count = SamplesPerLine;
   while( SdbNext( &Record)){
      PWeight( Record.Weight);
      uputch( ' ');
      uputch( ' ');
      if( !--Count){
         Count = SamplesPerLine;
         uputch('\n');
         // wait for print
         if( !PrintWait()){
            SdbClose();
            return( NO);
         }
      }
   }
   SdbClose();
   if( Count != SamplesPerLine){
      uputch('\n');                    // terminate row
      WaitOk();
   }
   return( YES);
} // PrintSamples

//------------------------------------------------------------------------------
// Statistics & Histogram
//------------------------------------------------------------------------------

static TYesNo PrintStatisticsAndHistogram( TCalcMode Mode, TPrinterProtocol Protocol)
// Print statistics and histogram
{
   // print total statistics :
   if( !PrintStatistics( &Calc[ STATISTICS_TOTAL])){
      return( NO);
   }
   // print total histogram :
   if( Protocol != PROTOCOL_STATISTICS){
      if( !PrintHistogram( &Calc[ STATISTICS_TOTAL].Histogram)){
         return( NO);
      }
   }
   if( Mode == CALC_TOTAL){
      return( YES);                 // sorting off
   }
   if( Mode == CALC_BY_SEX){
      uprintf( "MALE :\n\n");
      WaitOk();
      // print statistics :
      if( !PrintStatistics( &Calc[ STATISTICS_MALE])){
         return( NO);
      }
      // print histogram :
      if( Protocol != PROTOCOL_STATISTICS){
         if( !PrintHistogram( &Calc[ STATISTICS_MALE].Histogram)){
            return( NO);
         }
      }
      uprintf( "FEMALE :\n\n");
      WaitOk();
      // print statistics :
      if( !PrintStatistics( &Calc[ STATISTICS_FEMALE])){
         return( NO);
      }
      // print histogram :
      if( Protocol != PROTOCOL_STATISTICS){
         if( !PrintHistogram( &Calc[ STATISTICS_FEMALE].Histogram)){
            return( NO);
         }
      }
   } else { // CALC_LIGHT_(OK)_HEAVY
      uprintf( "LIGHT :\n\n");
      WaitOk();
      // print statistics :
      if( !PrintStatistics( &Calc[ STATISTICS_LIGHT])){
         return( NO);
      }
      if( Mode == CALC_LIGHT_OK_HEAVY){
         uprintf( "OK :\n\n");
         WaitOk();
         // print statistics :
         if( !PrintStatistics( &Calc[ STATISTICS_OK])){
            return( NO);
         }
      }
      uprintf( "HEAVY :\n\n");
      WaitOk();
      // print statistics :
      if( !PrintStatistics( &Calc[ STATISTICS_HEAVY])){
         return( NO);
      }
   }
   return( YES);
} // PrintStatisticsAndHistogram

//------------------------------------------------------------------------------
// Statistics
//------------------------------------------------------------------------------

static TYesNo PrintStatistics( TCalc *Statistics)
// Print statistics
{
   uprintf( "Count      : %d\n", Statistics->Count);
   WaitOk();
   uputs(   "Average    :"); PWeightWithUnits( Statistics->Average); uputchar( '\n');
   WaitOk();
   uputs(   "St.Dev.    :"); PWeightWithUnits( Statistics->Sigma);   uputchar( '\n');
   WaitOk();
   uprintf( "CV         : %3.1f %%\n", Statistics->Cv);
   WaitOk();
   uprintf( "Uniformity : %3.1f %%\n", Statistics->Uniformity);
   WaitOk();
   uputchar( '\n');
   WaitOk();
   return( YES);
} // PrintStatistics

//------------------------------------------------------------------------------
// Histogram
//------------------------------------------------------------------------------

#define HISTOGRAM_Y        7         // histogram base y
#define HISTOGRAM_TITLE_W  11        // histogram title width
#define HISTOGRAM_COUNT_W  5         // histogram count width

static TYesNo PrintHistogram( THistogram *Histogram)
// Print histogram
{
int i;
int MaxValue;   // histogram maximum
int Height;     // histogram height
int Stars;      // stars count

   // title line :
   uputs( "Weight ["); PWeighingUnits(); uputchar( ']');
   PCharacters( ' ', Config.Printer.PaperWidth - HISTOGRAM_TITLE_W - HISTOGRAM_COUNT_W);
   uputs( "Count\n");
   WaitOk();
   // print histogram :
   Height   = Config.Printer.PaperWidth - HISTOGRAM_Y - HISTOGRAM_COUNT_W - 1;
   MaxValue = HistogramMaximum( Histogram);
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      PWeight( HistogramGetValue( Histogram, i));
      uputchar( ' ');
      if( Height > 0){
         // round stars count :
         Stars  = (HistogramNormalize( Histogram, MaxValue, i) * Height * 2) / HISTOGRAM_NORM;
         Stars += 1;
         Stars /= 2;
         // draw stars column :
         PCharacters( '*', Stars);
         PCharacters( ' ', Height - Stars);
         uputchar( ' ');
      }
      uprintf( "%d\n", Histogram->Slot[ i]);
      WaitOk();
   }
   uputchar( '\n');
   WaitOk();
   return( YES);
} // PrintHistogram

//******************************************************************************

//------------------------------------------------------------------------------
// Wait
//------------------------------------------------------------------------------

static TYesNo PrintWait( void)
// Wait for the line output
{
int   Key;
dword Now;

   Now = SysTime();
   do {
      Key = SysYield();
      if( Key == K_ESC || Key == K_SHUTDOWN){
         // Canceled by user or power failure
         return( NO);
      }
   } while( (SysTime() - Now) < _LineDelay);
   return( YES);
} // PrintWait

//------------------------------------------------------------------------------
// Line
//------------------------------------------------------------------------------

static void PLine( void)
// Print separation line
{
   PCharLine( '-');
} // PLine

//------------------------------------------------------------------------------
// Double Line
//------------------------------------------------------------------------------

static void PDoubleLine( void)
// Print separation line
{
   PCharLine( '=');
} // PDoubleLine

//------------------------------------------------------------------------------
// Character Line
//------------------------------------------------------------------------------

static void PCharLine( char ch)
// Print separation line
{
   PCharacters( ch, Config.Printer.PaperWidth);
   uputchar( '\n');
} // PLine

//------------------------------------------------------------------------------
// Character Line
//------------------------------------------------------------------------------

static void PCharacters( char ch, int Count)
// Print character string
{
int i;
   for( i = 0; i < Count; i++){
      uputchar( ch);
   }
} // PLine

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

static void PWeight( TWeight Weight)
// Print weight
{
   xWeight( uputchar, Weight);
} // PWeight

//------------------------------------------------------------------------------
// Weight with units
//------------------------------------------------------------------------------

static void PWeightWithUnits( TWeight Weight)
// Print weight with units
{
   xWeightWithUnits( uputchar, Weight);
} // PWeightWithUnits

//------------------------------------------------------------------------------
// Units
//------------------------------------------------------------------------------

void PWeighingUnits( void)
// Print current units
{
   xWeighingUnits( uputchar);
} // PWeighingUnits

//------------------------------------------------------------------------------
// Date & Time
//------------------------------------------------------------------------------

static void PDateTime( TTimestamp Now)
// Print date & time
{
   xDateTimeShort( uputchar, Now);
} // PDateTime
