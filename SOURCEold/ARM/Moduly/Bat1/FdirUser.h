//******************************************************************************
//
//   FdirUser.h   File directory user data definition
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __FdirUser_H__
   #define __FdirUser_H__

#ifndef __FileCfg_H__
   #include "FileCfg.h"
#endif
#ifndef __Config_H__
   #include "Config.h"
#endif

typedef TFileConfig TFdirUserData;               
// Directory entry user metadata

#define FdirUserDataDefaults( Data)  ConfigFillFileConfig( Data)
// Fill metadata with defaults

#endif
