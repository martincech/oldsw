//******************************************************************************
//                                                                            
//   ScreenStatistics.c  Display statistics
//   Version 1.0         (c) VymOs
//
//******************************************************************************

#include "ScreenStatistics.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DEvent.h"    // Display events
#include "Str.h"                  // Strings
#include "../inc/wgt/DHistogram.h"// Display histogram
#include "Calc.h"                 // Statistic calculations
#include "Wgt/DWeight.h"          // Display weight
#include "Bitmap.h"               // Bitmaps
#include "Fonts.h"                // Project fonts
#include "Wgt/DLayout.h"          // Display layout

// Local functions :

static TYesNo StatisticsBySex( TUniStr Caption);
// Display statistics by sex. Returns NO on Esc key

static TYesNo StatisticsByWeight( TUniStr Caption, TWeightSortingMode Mode);
// Display statistics by weight. Returns NO on Esc key

static TYesNo Histogram( TUniStr Caption, TStatisticsType Type, TYesNo TitleIcon, int LowLimit, int HighLimit, 
                         TUniStr RightText, const TBitmap *RightIcon);
// Display histogram. Returns NO on Esc key

static void DrawGrid( TUniStr Caption, TYesNo SumIcon);
// Draw screen grid

static void DrawColumn( int x, const TBitmap *Title, TCalc *Statistics);
// Draw statistics column

//------------------------------------------------------------------------------
//   Statistics
//------------------------------------------------------------------------------

void ScreenStatisticsBySex( TUniStr Caption)
// Display Statistics by sex
{
   forever {
      // group by sex
      if( !StatisticsBySex( Caption)){
         return;
      }
      // show total histogram
      if( !Histogram( Caption, STATISTICS_TOTAL,  YES, -1, -1, STR_BTN_HISTOGRAM,  &BmpIconMale)){
         return;
      }
      // show histogram by sex
      if( !Histogram( Caption, STATISTICS_MALE,   YES, -1, -1, STR_BTN_HISTOGRAM,  &BmpIconFemale)){
         return;
      }
      if( !Histogram( Caption, STATISTICS_FEMALE, YES, -1, -1, STR_BTN_STATISTICS, 0)){
         return;
      }
   }
} // ScreenStatisticsBySex

//------------------------------------------------------------------------------
//   Statistics
//------------------------------------------------------------------------------

void ScreenStatisticsByWeight( TUniStr Caption, TWeightSortingMode Mode, int LowLimit, int HighLimit)
// Display Statistics by weight
{
   // update limits for histogram :
   switch( Mode){
      case WEIGHT_SORTING_NONE :
         LowLimit  = 0;                // disable low limit
         HighLimit = 0;                // disable high limit
         break;
      case WEIGHT_SORTING_LIGHT_HEAVY :
         HighLimit = 0;                // disable high limit
         break;
      case WEIGHT_SORTING_LIGHT_OK_HEAVY :
         break;
      default :
         break;
   }
   // show statistics & histogram :
   forever {
      // group by weight
      if( !StatisticsByWeight( Caption, Mode)){
         return;
      }
      // show total histogram
      if( !Histogram( Caption, STATISTICS_TOTAL, NO, LowLimit, HighLimit, STR_BTN_STATISTICS, 0)){
         return;
      }
   }
} // ScreenStatisticsByWeight

//------------------------------------------------------------------------------
//   Sex
//------------------------------------------------------------------------------

#define SS_TOTAL_X  36
#define SS_MALE_X   89
#define SS_FEMALE_X 143

static TYesNo StatisticsBySex( TUniStr Caption)
// Display statistics by sex. Returns NO on Esc key
{
   GClear();
   // draw frame
   DrawGrid( Caption, YES);
   // draw columns
   DrawColumn( SS_TOTAL_X,  &BmpStatisticsTotal,  &Calc[ STATISTICS_TOTAL]);
   DrawColumn( SS_MALE_X,   &BmpStatisticsMale,   &Calc[ STATISTICS_MALE]);
   DrawColumn( SS_FEMALE_X, &BmpStatisticsFemale, &Calc[ STATISTICS_FEMALE]);
   // redraw & wait
   GFlush();
   return( DEventWaitForEnterEsc());
} // StatisticsBySex

//------------------------------------------------------------------------------
//   Weight
//------------------------------------------------------------------------------

#define SW_TOTAL_X  36
#define SW_LIGHT_X  89
#define SW_OK_X     143
#define SW_HEAVY_X  196

static TYesNo StatisticsByWeight( TUniStr Caption, TWeightSortingMode Mode)
// Display statistics by weight. Returns NO on Esc key
{
   GClear();
   // draw frame
   DrawGrid( Caption, NO);
   // draw columns
   DrawColumn( SW_TOTAL_X,  &BmpStatisticsTotal, &Calc[ STATISTICS_TOTAL]);
   if( Mode != WEIGHT_SORTING_NONE){
      DrawColumn( SW_LIGHT_X,  &BmpStatisticsLight, &Calc[ STATISTICS_LIGHT]);
      if( Mode == WEIGHT_SORTING_LIGHT_HEAVY){
         DrawColumn( SW_OK_X,  &BmpStatisticsHeavy, &Calc[ STATISTICS_HEAVY]);
      } else {
         // Light/OK/Heavy
         DrawColumn( SW_OK_X,     &BmpStatisticsOK,    &Calc[ STATISTICS_OK]);
         DrawColumn( SW_HEAVY_X,  &BmpStatisticsHeavy, &Calc[ STATISTICS_HEAVY]);
      }
   }
   // redraw & wait
   GFlush();
   return( DEventWaitForEnterEsc());
} // StatisticsByWeight

//------------------------------------------------------------------------------
//   Histogram
//------------------------------------------------------------------------------

static TYesNo Histogram( TUniStr Caption, TStatisticsType Type, TYesNo TitleIcon, int LowLimit, int HighLimit, 
                         TUniStr RightText, const TBitmap *RightIcon)
// Display histogram. Returns NO on Esc key
{
   GClear();
   // title
   DLayoutFileTitle( Caption);
   // title icon :
   if( TitleIcon){
      GSetColor( DCOLOR_TITLE);
      switch( Type){
         case STATISTICS_MALE :
            GBitmap( G_WIDTH - 18, 2, &BmpIconMale);
            break;
         case STATISTICS_FEMALE :
            GBitmap( G_WIDTH - 18, 2, &BmpIconFemale);
            break;
         case STATISTICS_TOTAL :
            GBitmap( G_WIDTH - 18, 2, &BmpIconTotal);
            break;
         default :
            break;
      }
      GSetColor( DCOLOR_DEFAULT);
   }
   // status line
   DLayoutStatus( STR_BTN_CANCEL, RightText, RightIcon);
   // histogram body
   return( DHistogram( &Calc[ Type].Histogram, LowLimit, HighLimit));
} // Histogram

//------------------------------------------------------------------------------
//   Grid
//------------------------------------------------------------------------------

#define SS_TITLE_X  4

static void DrawGrid( TUniStr Caption, TYesNo SumIcon)
// Draw screen grid
{
   // title
   DLayoutFileTitle( Caption);
   // status line
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_HISTOGRAM, SumIcon ? &BmpIconTotal : 0);
   // background
   GSetColor( COLOR_LIGHTGRAY);
   GBox( 0, 59, G_WIDTH, 19);
   GBox( 0, 97, G_WIDTH, 19);

   // grid
//!!!   GSetMode( GMODE_XOR);
   GLine( 0, 39, G_WIDTH, 39);
   GLine( 29, 22, 29, 135); 
   GSetMode( GMODE_REPLACE);

   // symbols
   GSetColor( DCOLOR_DEFAULT);
   GBitmap( 6, 41,  &BmpStatisticsCount);
   GBitmap( 8, 61,  &BmpStatisticsWeight);
   GBitmap( 9, 83,  &BmpStatisticsSigma);
   GBitmap( 2, 101, &BmpStatisticsCV);
   GBitmap( 2, 121, &BmpStatisticsUNI);
} // DrawGrid

//------------------------------------------------------------------------------
//   Column
//------------------------------------------------------------------------------

static void DrawColumn( int x, const TBitmap *Title, TCalc *Statistics)
// Draw statistics column
{
   GBitmap( x + 10, 21, Title);        // title
   // count
   GSetFont( TAHOMA16);
   GTextAt( x, 43);  
   cprintf( "%d", Statistics->Count);
   // average
   GTextAt( x, 62);  
   DWeightLeft( Statistics->Average);
   // sigma
   GTextAt( x, 81);  
   DWeightLeft( Statistics->Sigma);
   // variation
   GTextAt( x, 100);  
   cprintf( "%3.1f", Statistics->Cv);
   // uniformity
   GTextAt( x, 119);  
   cprintf( "%3.1f", Statistics->Uniformity);
} // DrawColumn
