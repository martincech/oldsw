//******************************************************************************
//                                                                            
//   DFormat.h      Display formats
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DFormat_H__
   #define __DFormat_H__

#ifndef __StrDef_H__
   #include "../../Inc/Wgt/StrDef.h"
#endif

void DFormatRange( char *Buffer, int Decimals, int LoLimit, int HiLimit, TUniStr Units);
// Format range data

#endif
