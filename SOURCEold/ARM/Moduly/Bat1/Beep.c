//******************************************************************************
//                                                                            
//  Beep.c         Sound processing
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Beep.h"
#include "../Inc/System.h"
#include "../Inc/Sound.h"
#include "ConfigDef.h"

// constants :
#define INVALID_NOTE  0xFF   // invalid note
#define PATTERN_MAX   10     // max. pattern size

// pattern note :
typedef struct {
   byte Note;                // note
   byte Duration;            // note duration [ms]
} TBeepNote;

// pattern definition macros :
#define DefPattern( p) static const TBeepNote p[] = {
#define EndPattern()   { INVALID_NOTE, 0}};

//------------------------------------------------------------------------------
//  Patterns
//------------------------------------------------------------------------------

// Greeting :
DefPattern( GreetingPattern)
   { NOTE_C1,    110},
   { NOTE_C1,     90},
   { NOTE_DIS1,  110},
   { NOTE_FIS1,  190},
EndPattern()

// Boot :
DefPattern( BootPattern)
   { NOTE_C1,    110},
   { NOTE_DIS1,   90},
   { NOTE_DIS1,   90},
EndPattern()

// Bye :
DefPattern( ByePattern)
   { NOTE_FIS1, 110},
   { NOTE_F1,   90},
   { NOTE_DIS1, 110},
   { NOTE_C1,   190},
EndPattern()

// Calibration :
DefPattern( CalibrationPattern)
   { NOTE_C1, 110},
   { NOTE_C1,  90},
EndPattern()

// Above :
DefPattern( AbovePattern)
   { NOTE_F3, 110},
   { NOTE_A3, 90},
   { NOTE_C4, 110},
EndPattern()

// Within :
DefPattern( WithinPattern)
   { NOTE_G3, 110},
   { NOTE_G3, 90},
   { NOTE_G3, 110},
EndPattern()

// Below :
DefPattern( BelowPattern)
   { NOTE_C3, 110},
   { NOTE_GIS2, 90},
   { NOTE_F2, 110},
EndPattern()

// Default :
DefPattern( DefaultPattern)
   { NOTE_C3, 110},
   { NOTE_E3, 90},
   { NOTE_C3, 110},
EndPattern()

// Tone to note conversion table :
static const byte _ToneNote[ BEEP_TONE_COUNT] = {
   NOTE_C1, NOTE_G1,
   NOTE_C2, NOTE_G2,
   NOTE_C3, NOTE_G3,
   NOTE_C4, NOTE_G4,
};

// Tone code to Pattern conversion table :
static const TBeepNote* _TonePattern[ BEEP_PATTERN_COUNT] = {
   DefaultPattern,
   BelowPattern,
   WithinPattern,   
   AbovePattern,
};
  

#define SoundVolume Config.Sounds.VolumeKeyboard  // standard volume
#define SaveVolume  Config.Sounds.VolumeSaving    // saving volume

// Local functions :

static void PlayPattern( const TBeepNote *Pattern, int Volume);
// Plays pattern

static void PlayTone( int Tone, int Volume, int Duration);
// Plays sound by <Tone> code with <Volume> and <Duration>

//------------------------------------------------------------------------------
//  Beep
//------------------------------------------------------------------------------

void BeepKey( void)
// Key press beep
{
   PlayTone( Config.Sounds.ToneKeyboard, SoundVolume, 20);
} // BeepKey

//------------------------------------------------------------------------------
//  Error Beep
//------------------------------------------------------------------------------

void BeepError( void)
// Error key press beep
{
   SndSBeep( NOTE_G3, SoundVolume, 10);
   SndSBeep( NOTE_C4, SoundVolume, 10);
} // BeepError

//------------------------------------------------------------------------------
//  Stop beep
//------------------------------------------------------------------------------

void BeepTimeout( void)
// User inactivity beep
{
   SndBeep( NOTE_C2, SoundVolume, 25);
} // BeepTimeout

//------------------------------------------------------------------------------
//  Greeting
//------------------------------------------------------------------------------

void BeepGreeting( void)
// Power on greeting
{
   if( !Config.Sounds.EnableSpecial){
      return;                          // special sounds disabled
   }
   PlayPattern( GreetingPattern, SoundVolume);
} // BeepGreeting

//------------------------------------------------------------------------------
//  Greeting
//------------------------------------------------------------------------------

void BeepBoot( void)
// Boot power on
{
   if( !Config.Sounds.EnableSpecial){
      return;                          // special sounds disabled
   }
   PlayPattern( BootPattern, SoundVolume);
} // BeepBoot

//------------------------------------------------------------------------------
//  Bye
//------------------------------------------------------------------------------

void BeepBye( void)
// Power off
{
   if( !Config.Sounds.EnableSpecial){
      return;                          // special sounds disabled
   }
   PlayPattern( ByePattern, SoundVolume);
} // BeepBye

//------------------------------------------------------------------------------
//  Weighing above
//------------------------------------------------------------------------------

void BeepWeighingHeavy( void)
// Weighing heavy done
{
   PlayTone( Config.Sounds.ToneHeavy, SaveVolume, 100);
} // BeepWeightingAbove

//------------------------------------------------------------------------------
//  Weighing below
//------------------------------------------------------------------------------

void BeepWeighingLight( void)
// Weighing light done
{
   PlayTone( Config.Sounds.ToneLight, SaveVolume, 100);
} // BeepWeighingBelow

//------------------------------------------------------------------------------
//  Weighing middle
//------------------------------------------------------------------------------

void BeepWeighingOk( void)
// Weighing OK done
{
   PlayTone( Config.Sounds.ToneOk, SaveVolume, 100);
} // BeepWeighingWithin

//------------------------------------------------------------------------------
//  Weighing default
//------------------------------------------------------------------------------

void BeepWeighingDefault( void)
// Weighing middle done
{
   PlayTone( Config.Sounds.ToneDefault, SaveVolume, 100);
} // BeepWeighingDefault

//------------------------------------------------------------------------------
//  Calibration
//------------------------------------------------------------------------------

void BeepCalibration( void)
// Calibration weighing done
{
   PlayPattern( CalibrationPattern, SoundVolume);
} // BeepCalibration

//------------------------------------------------------------------------------
//  Volume Test
//------------------------------------------------------------------------------

void BeepTest( int Volume)
// Test beep volume
{
   SndBeep( NOTE_F3, Volume, 50);
} // BeepTest

//------------------------------------------------------------------------------
//  Tone Test
//------------------------------------------------------------------------------

void BeepToneTest( int Tone)
// Test <Tone> selection
{
   PlayTone( Tone, SoundVolume, 50);
} // BeepToneTest

//------------------------------------------------------------------------------
//  Rollover
//------------------------------------------------------------------------------

void BeepRollover( void)
// Keyboard beep - list rollover
{
   SndSBeep( NOTE_C3, SoundVolume, 20);
   SndSBeep( NOTE_E3, SoundVolume, 20);
} // BeepRollover

//******************************************************************************

//------------------------------------------------------------------------------
//  Play pattern
//------------------------------------------------------------------------------

static void PlayPattern( const TBeepNote *Pattern, int Volume)
// Plays pattern
{
   SndStop();                          // stop async beep
   while( Pattern->Note != INVALID_NOTE){
      SndSBeep( Pattern->Note, Volume, Pattern->Duration);
      SysDelay( 10);
      Pattern++;
   }
} // PlayPattern

//------------------------------------------------------------------------------
//  Play tone
//------------------------------------------------------------------------------

static void PlayTone( int Tone, int Volume, int Duration)
// Plays sound by <Tone> code with <Volume> and <Duration>
{
   if( Tone < BEEP_SINGLE_TONE_COUNT){
      SndBeep( _ToneNote[ Tone], Volume, Duration);   
      return;
   }
   if( Tone < BEEP_TONE_COUNT){
      PlayPattern( _TonePattern[ Tone - BEEP_SINGLE_TONE_COUNT], Volume);
      return;
   }
   // wrong tone, play default :
   SndBeep( NOTE_C5, Volume, Duration);
} // PlayTone
