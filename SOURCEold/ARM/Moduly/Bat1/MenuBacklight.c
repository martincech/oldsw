//******************************************************************************
//                                                                            
//   MenuBacklight.c   Backlight menu
//   Version 1.0       (c) VymOs
//
//******************************************************************************

#include "MenuBacklight.h"
#include "../Inc/Graphic.h"       // graphic
#include "../Inc/conio.h"         // Display
#include "../Inc/Wgt/DLabel.h"    // Display label
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DEdit.h"            // Display edit value
#include "Str.h"                  // Strings
#include "Backlight.h"            // Backlight

static DefMenu( BacklightMenu)
   STR_MODE,
   STR_DURATION,
   STR_INTENSITY,
EndMenu()

typedef enum {
   BM_MODE,
   BM_DURATION,
   BM_INTENSITY,
} TBacklightMenuEnum;

// Local functions :

static void BacklightParameters( int Index, int y, void *UserData);
// Backlight menu parameters

//------------------------------------------------------------------------------
//   Backlight
//------------------------------------------------------------------------------

void MenuBacklight( void)
// Backlight menu
{
TMenuData MData;
int       i;
int       OldMode;

   DMenuClear( MData);
   forever {
      MData.Mask = 0;
      if( Config.Display.Backlight.Mode != BACKLIGHT_MODE_AUTO){
         MData.Mask = (1 << BM_DURATION);
      }
      // selection :
      if( !DMenu( STR_BACKLIGHT, BacklightMenu, BacklightParameters, 0, &MData)){
         BacklightNormal();      // return to normal conditions
         return;
      }
      switch( MData.Item){
         case BM_MODE :
            i = Config.Display.Backlight.Mode;
            if( !DEditEnum( DMENU_EDIT_X, MData.y, &i, STR_BACKLIGHT_MODE_AUTO, _BACKLIGHT_MODE_COUNT)){
               break;
            }
            Config.Display.Backlight.Mode = i;
            break;

         case BM_DURATION :
            i = Config.Display.Backlight.Duration;
            if( !DEditNumber( DMENU_EDIT_X, MData.y, &i, 0, 1, BACKLIGHT_DURATION_MAX, STR_SECONDS)){
               break;
            }
            Config.Display.Backlight.Duration = i;
            break;

         case BM_INTENSITY :
            // set backlight mode always on for testing :
            OldMode = Config.Display.Backlight.Mode;             // save old mode
            Config.Display.Backlight.Mode = BACKLIGHT_MODE_ON;   // set always on
            BacklightNormal();                                   // setup mode
            // test intensity :
            i = Config.Display.Backlight.Intensity;
            if( !DEditSpin( DMENU_EDIT_X, MData.y, &i, 0, BACKLIGHT_MAX, BacklightTest)){
               Config.Display.Backlight.Mode = OldMode;          // restore mode
               break;
            }
            Config.Display.Backlight.Mode = OldMode;             // restore mode
            Config.Display.Backlight.Intensity = i;
            break;
      }
   }
} // MenuBacklight

//******************************************************************************

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void BacklightParameters( int Index, int y, void *UserData)
// Backlight menu parameters
{
   switch( Index){
      case BM_MODE :
         DLabelEnum( Config.Display.Backlight.Mode, STR_BACKLIGHT_MODE_AUTO, DMENU_PARAMETERS_X, y);
         break;

      case BM_DURATION :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d %s", Config.Display.Backlight.Duration, STR_SECONDS);
         break;

      case BM_INTENSITY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Config.Display.Backlight.Intensity);
         break;
   }
} // BacklightParameters
