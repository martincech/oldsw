//******************************************************************************
//
//   Group.h      Group utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Group_H__
   #define __Group_H__

#ifndef __Fs_H__
   #include "../Inc/File/Fs.h"
#endif

#ifndef __Ds_H__
   #include "../Inc/File/Ds.h"
#endif

#ifndef __GroupDef_H__
   #include "GroupDef.h"
#endif

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

TYesNo GroupCreate( char *Name, TGroup Group);
// Create new group

void GroupLoad( TFdirHandle Handle, TGroup Group);
// Load group data into <Group>

void GroupSave( TFdirHandle Handle, TGroup Group);
// Save <Group> into group

void GroupLoadNote( TFdirHandle Handle, char *Note);
// Load group note

void GroupSaveNote( TFdirHandle Handle, char *Note);
// Save group note

#define GroupRename( Handle, Name)    FsRename( Handle, Name)
// Rename group

void GroupDelete( TFdirHandle Handle);
// Delete group

void GroupDeleteFile( TFdirHandle Handle);
// Remove <Handle> from all groups

//------------------------------------------------------------------------------
// Member manipulations
//------------------------------------------------------------------------------

#define GroupClear( Group)                 DsClear( Group)
// Remova all members

#define GroupIsEmpty( Group)               DsIsEmpty( Group)
// Empty group

#define GroupIsMember( Group, Handle)      DsContains( Group, Handle)
// File <Handle> is member

#define GroupAdd( Group, Handle)           DsAdd( Group, Handle)
// Add file <Handle>

#define GroupAddAll( Group)                DsSelectAll( Group)
// Add all files

#define GroupRemove( Group, Handle)        DsRemove( Group, Handle)
// Remove file <Handle>

#define GroupSet( Group)                   DsSet( Group)
// Set group as working data set

#endif
