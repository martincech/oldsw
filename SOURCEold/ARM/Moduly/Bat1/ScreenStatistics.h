//******************************************************************************
//                                                                            
//   ScreenStatistics.h  Display statistics
//   Version 1.0         (c) VymOs
//
//******************************************************************************

#ifndef __ScreenStatistics_H__
   #define __ScreenStatistics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"
#endif

#ifndef __StrDef_H__
   #include "../Inc/Wgt/StrDef.h"
#endif

void ScreenStatisticsBySex( TUniStr Caption);
// Display Statistics by sex

void ScreenStatisticsByWeight( TUniStr Caption, TWeightSortingMode Mode, int LowLimit, int HighLimit);
// Display Statistics by weight

#endif
