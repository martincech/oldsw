//******************************************************************************
//                                                                            
//   MenuMemory.c    Memory functions menu
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include "MenuMemory.h"

#include "../Inc/File/Fd.h"       // File directory
#include "../Inc/Wgt/DEvent.h"    // Display event
#include "../Inc/Cpu.h"           // WatchDog

#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DMsg.h"             // Display message

#include "Str.h"                  // Strings
#include "Sdb.h"                  // Samples database
#include "Group.h"                // File groups
#include "Screen.h"               // Display

static DefMenu( MemoryMenu)
   STR_CAPACITY,
   STR_FORMAT,
EndMenu()

typedef enum {
   MM_CAPACITY,
   MM_FORMAT,
} TMemoryMenuEnum;

// Local functions :

static void ShowCapacity( void);
// Show capacity screen

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuMemory( void)
// Memory configuration menu
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_MEMORY, MemoryMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case MM_CAPACITY :
            ShowCapacity();
            break;

         case MM_FORMAT :
            if( !DMsgYesNo( STR_FORMAT, STR_REALLY_FORMAT, 0)){
               break;
            }
            FsFormat();
            Config.LastFile = FDIR_INVALID;      // invalidate active file
            ConfigSaveSection( LastFile);           
            break;
      }
   }
} // MenuMemory

//******************************************************************************

//------------------------------------------------------------------------------
//  Capacity
//------------------------------------------------------------------------------

static void ShowCapacity( void)
// Show capacity screen
{
TFdirHandle Handle;
int         FilesCount;
int         GroupsCount;
int         FilesSize;
int         PercentUsed;

   // samples database :
   FilesCount = 0;
   FilesSize  = 0;
   FdSetClass( SDB_CLASS);
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      FilesCount++;
      FilesSize   += SdbFileSize( FdGetSize( Handle));
      WatchDog();
   }
   // groups :
   GroupsCount = 0;
   FdSetClass( GRP_CLASS);
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      GroupsCount++;
      WatchDog();
   }
   // memory used :
   PercentUsed = ((FS_CAPACITY - FsAvailable()) * 1000) / FS_CAPACITY;
   ScreenCapacity( FilesCount, FilesSize, GroupsCount, PercentUsed); // show screen
   DEventWaitForEnterEsc();                                          // wait for a key
} // ShowCapacity
