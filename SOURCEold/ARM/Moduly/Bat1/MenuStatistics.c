//******************************************************************************
//                                                                            
//   MenuStatistics.c   Statistics menu
//   Version 1.0       (c) VymOs
//
//******************************************************************************

#include "MenuStatistics.h"

#include "../Inc/File/Fd.h"       // File directory
#include "Wgt/DMenu.h"            // Display menu
#include "Wgt/DInput.h"           // Display input value
#include "Wgt/DMsg.h"             // Display message
#include "Wgt/DDir.h"             // Display file directory

#include "Str.h"                  // Strings
#include "Screen.h"               // ScreenWait
#include "ScreenStatistics.h"     // Display statistics
#include "ScreenFiles.h"          // Display files comparation
#include "Calc.h"                 // Statistics calculations
#include "Sdb.h"                  // Samples database
#include "Group.h"                // File groups
#include "Printer.h"              // Printer
#include <string.h>

#define TITLE_MAX    31           // maximum title length

static DefMenu( StatisticsMenu)
   STR_TOTAL_STATISTICS,
   STR_COMPARE_FILES,
   STR_PRINT,
EndMenu()

typedef enum {
   SM_TOTAL_STATISTICS,
   SM_COMPARE_FILES,
   SM_PRINT,
} TStatisticsMenuEnum;

// Selection menu :
static DefMenu( SelectionMenu)
   STR_ONE_FILE,
   STR_ONE_GROUP,
   STR_MORE_FILES,
   STR_ALL_FILES,
EndMenu()

typedef enum {
   SM_ONE_FILE,
   SM_ONE_GROUP,
   SM_MORE_FILES,
   SM_ALL_FILES,
   _SM_COUNT
} TSelectionMenuEnum;

// Local functions :

static void MenuTotalStatistics( void);
// Total statistics menu

static void MenuCompareFiles( void);
// Compare files menu

static void MenuPrint( void);
// Print menu

static int SelectFiles( TUniStr Caption, TGroup Group, char *Name, TYesNo Single, int Item);
// Select file/group

static TCalcMode GetCalcMode( TGroup Group);
// Returns Calculation mode by Group

static void GetLimits( TGroup Group, TWeightSortingMode *SortMode, int *LowLimit, int *HighLimit);
// Get <LowLimit> and <HighLimit>

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

void MenuStatistics( void)
// Statistics menu
{
TMenuData MData;

   DMenuClear( MData);
   forever {
      if( !DMenu( STR_STATISTICS, StatisticsMenu, 0, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case SM_TOTAL_STATISTICS :
            MenuTotalStatistics();
            break;

         case SM_COMPARE_FILES :
            MenuCompareFiles();
            break;

         case SM_PRINT :
            MenuPrint();
            break;

      }
   }
} // MenuStatistics

//******************************************************************************

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

static void MenuTotalStatistics( void)
// Total statistics menu
{
int       Item;
TGroup    Group;
TCalcMode Mode;
char      Name[ TITLE_MAX + 1];
TWeight   LowLimit, HighLimit;
TWeightSortingMode SortMode;

   Item = 0;
   forever {
      Item = SelectFiles( STR_TOTAL_STATISTICS, Group, Name, YES, Item);
      if( Item < 0){
         return;
      }
      ScreenWait();
      // get sorting mode :
      Mode = GetCalcMode( Group);
      // calculate statistics :
      CalcMode( Mode);
      GroupSet( Group);
      CalcStatistics();
      // display by mode :
      if( Mode == CALC_BY_SEX){
         ScreenStatisticsBySex( Name);
      } else {
         GetLimits( Group, &SortMode, &LowLimit, &HighLimit);
         ScreenStatisticsByWeight( Name, SortMode, LowLimit, HighLimit);
      }
   }
} // MenuTotalStatistics

//------------------------------------------------------------------------------
//  Compare files
//------------------------------------------------------------------------------

static void MenuCompareFiles( void)
// Compare files menu
{
int    Item;
TGroup Group;
char   Name[ TITLE_MAX + 1];

   Item = 1;                           // skip single file
   forever {
      Item = SelectFiles( STR_COMPARE_FILES, Group, Name, NO, Item);
      if( Item < 0){
         return;
      }
      ScreenCompareFiles( Name, Group, GetCalcMode( Group));
   }
} // MenuCompareFiles

//------------------------------------------------------------------------------
//  Print
//------------------------------------------------------------------------------

// selection mode to print mode table
static const byte _PrintMode[ _SM_COUNT] = { PRINT_FILE, PRINT_GROUP, PRINT_MORE_FILES, PRINT_MORE_FILES};

static void MenuPrint( void)
// Print menu
{
int         Item;
TGroup      Group;
TCalcMode   Mode;
char        Name[ TITLE_MAX + 1];
int         Protocol;

   Item = 0;
   forever {
      Item = SelectFiles( STR_PRINT, Group, Name, YES, Item);
      if( Item < 0){
         return;
      }
      // enter protocol type :
      Protocol = Config.PrintProtocol;           // enter last protocol type
      if( !DInputEnum( STR_PRINT, STR_ENTER_PROTOCOL_TYPE, &Protocol, STR_PROTOCOL_STATISTICS_HISTOGRAM, _PROTOCOL_TYPE_COUNT)){
         continue;
      }
      Config.PrintProtocol = Protocol;
      ConfigSaveSection( PrintProtocol);         // save protocol type
      // get sorting mode :
      Mode = GetCalcMode( Group);
      PrinterPrint( Name, Group, _PrintMode[ Item], Mode, Protocol);
   }
} // MenuPrint

//------------------------------------------------------------------------------
//  Select files
//------------------------------------------------------------------------------

static int SelectFiles( TUniStr Caption, TGroup Group, char *Name, TYesNo Single, int Item)
// Select file/group
{
TMenuData   MData;
TFdirHandle Handle;

   DMenuClear( MData);
   MData.Item = Item;
   MData.Mask = Single ? 0 : (1 << SM_ONE_FILE);
   forever {
      if( !DMenu( Caption, SelectionMenu, 0, 0, &MData)){
         return( -1);
      }
      GroupClear( Group);
      switch( MData.Item){
         case SM_ONE_FILE :
            FdSetClass( SDB_CLASS);
            Handle = DDirSelectFile( STR_SELECT_FILE, Config.LastFile);
            if( Handle == FDIR_INVALID){
               continue;                              // repeat menu
            }
            FdGetName( Handle, Name);                 // get name
            GroupAdd( Group, Handle);                 // add to group
            break;

         case SM_ONE_GROUP :
            FdSetClass( GRP_CLASS);
            Handle = DDirSelectGroup( STR_SELECT_GROUP, FDIR_INVALID);
            if( Handle == FDIR_INVALID){
               continue;                              // repeat menu
            }
            FdGetName( Handle, Name);                 // get name
            GroupLoad( Handle, Group);                // read group
            break;

         case SM_MORE_FILES :
            FdSetClass( SDB_CLASS);                   // samples database directory class
            if( !DDirSelectSet( STR_SELECT_FILES, Group)){  // edit file set
               continue;
            }
            strcpy( Name, StrGet( STR_MORE_FILES));   // "file name"
            break;

         case SM_ALL_FILES :
            strcpy( Name, StrGet( STR_ALL_FILES));    // "file name"
            GroupAddAll( Group);                      // insert all files
            break;
      }
      if( GroupIsEmpty( Group)){
         DMsgOk( STR_ERROR, STR_NO_FILES_SELECTED, 0);
         continue;
      }
      return( MData.Item);
   }
} // SelectFiles

//------------------------------------------------------------------------------
//  Get Calc Mode
//------------------------------------------------------------------------------

static TCalcMode GetCalcMode( TGroup Group)
// Returns Calculation mode by Group
{
TSavingMode        SavingMode;
TWeightSortingMode SortingMode;
int                i;

   SavingMode  = Config.WeighingParameters.Saving.Mode;
   SortingMode = Config.WeighingParameters.WeightSorting.Mode;
   if( Config.EnableFileParameters){
      // search for first file in the group :
      for( i = 1; i <= FDIR_SIZE; i++){
         if( GroupIsMember( Group, i)){
            SdbOpen( i, YES);
            SavingMode  = SdbConfig()->Saving.Mode;
            SortingMode = SdbConfig()->WeightSorting.Mode;
            SdbClose();
            break;
         }
      }
   }
   if( SavingMode == SAVING_MODE_MANUAL_BY_SEX){
      return( CALC_BY_SEX);            // group by sex
   }
   // automatic or manual 1 group
   switch( SortingMode){
      case WEIGHT_SORTING_NONE :
         return( CALC_TOTAL);
      case WEIGHT_SORTING_LIGHT_HEAVY :
         return( CALC_LIGHT_HEAVY);
      case WEIGHT_SORTING_LIGHT_OK_HEAVY :
         return( CALC_LIGHT_OK_HEAVY);
      default :
         return( CALC_TOTAL);
   }
} // GetCalcMode

//------------------------------------------------------------------------------
//  Get Limits
//------------------------------------------------------------------------------

static void GetLimits( TGroup Group, TWeightSortingMode *SortMode, int *LowLimit, int *HighLimit)
// Get <LowLimit> and <HighLimit>
{
TWeightSortingMode Mode;
int                i;

   *LowLimit  = -1;
   *HighLimit = -1;
   // search for first file in the group :
   for( i = 1; i <= FDIR_SIZE; i++){
      if( GroupIsMember( Group, i)){
         SdbOpen( i, YES);
         break;
      }
   }
   // get sorting mode
   if( Config.EnableFileParameters){
      Mode = SdbConfig()->WeightSorting.Mode;              // file parameters
   } else {
      Mode = Config.WeighingParameters.WeightSorting.Mode; // global parameters
   }
   *SortMode = Mode;
   // get limits
   if( Mode == WEIGHT_SORTING_LIGHT_HEAVY){
      *LowLimit  = SdbConfig()->WeightSorting.LowLimit;
   } else if( Mode == WEIGHT_SORTING_LIGHT_OK_HEAVY){
      *LowLimit  = SdbConfig()->WeightSorting.LowLimit;
      *HighLimit = SdbConfig()->WeightSorting.HighLimit;
   } // else no limits
   SdbClose();
} // GetLimits
