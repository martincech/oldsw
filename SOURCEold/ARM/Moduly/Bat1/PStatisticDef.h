//*****************************************************************************
//
//    PStatisticDef.h   Statistic parameters definition
//    Version 1.0       (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __PStatisticDef_H__
  #define __PStatisticDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

// Histogram mode :
typedef enum {
   HISTOGRAM_MODE_RANGE,               // histogram by range
   HISTOGRAM_MODE_STEP,                // histogram by absolute step
   _HISTOGRAM_MODE_COUNT
} THistogramMode;

// Histogram parameters
typedef struct {
   byte    Mode;                       // THistogramMode - mode of histogram
   byte    Range;		       // Histogram step in +/- perecents of average weight (or 0 if not used)
   word    _Spare;
   TWeight Step;		       // Histogram step in kg (or 0 if not used)
} THistogramParameters;

// Parameters for statistic calculations
typedef struct {
   byte                 UniformityRange;// Uniformity range in +/- percents of average weight
   byte                 _Spare[ 3];
   THistogramParameters Histogram;      // Histogram parameters
} TStatisticParameters;

#endif
