//*****************************************************************************
//
//    PAccu.h      Accumulator parameters utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __PAccu_H__
   #define __PAccu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __PAccuDef_H__
   #include "PAccuDef.h"
#endif

void PAccuLoad( void);
// Load maitenance data from EEPROM

void PAccuSave( void);
// Save maintenance data to EEPROM

unsigned PAccuVoltage( unsigned RawValue);
// Calculates voltage [mV] by <RawValue>

unsigned PAccuRawVoltage( unsigned Value);
// Calculate raw voltage [LSB] by <Value> [mV]

unsigned PAccuCapacity( unsigned RawValue);
// Calculate remaining capacity [%] by <RawValue>

void PAccuCalibrate( unsigned RawValue, unsigned Value);
// Calculate calibration data by <RawValue> [LSB] and <Value> [mV]

//-----------------------------------------------------------------------------

unsigned PAccuGetRange( void);
// Get physical range [mV]

void PAccuSetUMax( unsigned RawValue);
// Set Umax voltage

unsigned PAccuGetUMax( void);
// Get Umax voltage

void PAccuSetUMid( unsigned RawValue);
// Set Umid voltage

unsigned PAccuGetUMid( void);
// Get Umid voltage

void PAccuSetUMin( unsigned RawValue);
// Set Umin voltage

unsigned PAccuGetUMin( void);
// Get Umin voltage

void PAccuSetUWarn( unsigned RawValue);
// Set Uwarn voltage

unsigned PAccuGetUWarn( void);
// Get Uwarn voltage

void PAccuSetChargeTime( unsigned Min);
// Set charge time to <Min>

unsigned PAccuGetChargeTime( void);
// Get charge time [Min]

void PAccuSetDischargeTime( unsigned Min);
// Set discharge time to <Min>

unsigned PAccuGetDischargeTime( void);
// Get discharge time [Min]

#endif
