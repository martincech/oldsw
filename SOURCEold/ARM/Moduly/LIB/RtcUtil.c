//*****************************************************************************
//
//    RtcUtil.c  -  Real time clock utilities
//    Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "../../inc/Rtc.h"
#include "../../inc/Bcd.h"
#include "../../inc/Dt.h"

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

TTimestamp RtcLoad( void)
// Returns RTC time
{
TLocalTime TmpTime;

   TmpTime.Sec   = dbcd2bin( RtcSec());
   TmpTime.Min   = dbcd2bin( RtcMin());
   TmpTime.Hour  = dbcd2bin( RtcHour());
   TmpTime.Day   = dbcd2bin( RtcDay());
   TmpTime.Month = dbcd2bin( RtcMonth());
   TmpTime.Year  = dbcd2bin( RtcYear()) - 2000;
   return( DtCompose( &TmpTime));
} // RtcLoad

//-----------------------------------------------------------------------------
// Save
//-----------------------------------------------------------------------------

void RtcSave( TTimestamp Time)
// Set RTC time by <Time>
{
TLocalTime TmpTime;

   DtSplit( Time, &TmpTime);
   RtcSetSec(   dbin2bcd( TmpTime.Sec));
   RtcSetMin(   dbin2bcd( TmpTime.Min));
   RtcSetHour(  dbin2bcd( TmpTime.Hour));
   RtcSetDay(   dbin2bcd( TmpTime.Day));
   RtcSetMonth( dbin2bcd( TmpTime.Month));
   RtcSetYear(  dbin2bcd( TmpTime.Year + 2000));
} // RtcSave
