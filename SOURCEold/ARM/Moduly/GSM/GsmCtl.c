//*****************************************************************************
//
//    GsmCtl.c     - Demo of GSM control
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#include "../../inc/GsmCtl.h"
#include "../../inc/Gsm.h"                // GSM

#define __DEBUG__

#ifdef __DEBUG__
   #include "../../inc/conio.h"           // jednoduchy display

   #define TRACE( s)          cputs( s);putchar('\n');
   #define TRACEI( s, i)      cputs( s);cint8( i);putchar('\n');
#else
   #define TRACE( s)
   #define TRACEI( s, i)
#endif

#if defined( GSM_COM0)
   #define __COM0__
#elif defined( GSM_COM1)   
   #define __COM1__
#else
   #error "Unknown COM number"
#endif   
#include "../../inc/ComPkt.h"          // packet interface

TGsmCtl GsmCtl;

//-----------------------------------------------------------------------------
// Radic
//-----------------------------------------------------------------------------

void GsmExecute( void)
// Zpracovava udalosti GSM (volat 1x za sekundu)
{
native Cmd;
dword  Data;

   switch( GsmCtl.Status){
      case GSM_POWERED :
         TRACE( "GSM Powered");
         GsmCtl.Registered = NO;                 // Neni prihlasen
         if( !GsmReset()){
            // nereaguje, odloz dalsi reset
            GsmCtl.Status  = GSM_WAIT_POWERED;
            GsmCtl.Counter = GSM_POWERED_PERIOD;
            return;
         }
         GsmCtl.Status = GSM_CHECK;
         break;

      case GSM_WAIT_POWERED :
         TRACE( "GSM Wait Powered");
         if( --GsmCtl.Counter == 0){
            GsmCtl.Status = GSM_POWERED;         // znovu kontrola nabehu
            return;
         }
         break;

      case GSM_CHECK :
         TRACE( "GSM Check");
         if( !GsmRegistered()){
            GsmCtl.Status = GSM_POWERED;
            return;
         }
         GsmCtl.Registered = YES;                // Je prihlasen
         GsmCtl.Status  = GSM_WAIT_FOR_CALL;
         GsmCtl.Counter = GSM_CHECK_PERIOD;
         break;

      case GSM_WAIT_FOR_CALL :
         TRACEI( "GSM Ring ? ", GsmCtl.Counter);
         if( --GsmCtl.Counter == 0){
            GsmCtl.Status = GSM_CHECK;           // kontrola spojeni
            return;
         }
         if( !GsmIsRinging()){
            return;
         }
         // prichozi volani
         TRACE( "RING, Off Hook");
         GsmOffHook();
         GsmCtl.Status  = GSM_WAIT_FOR_CONNECT;
         GsmCtl.Counter = GSM_CONNECT_TIMEOUT;
         break;

      case GSM_WAIT_FOR_CONNECT :
         TRACEI( "GSM Connect ? ", GsmCtl.Counter);
         if( --GsmCtl.Counter == 0){
            TRACE( "HangUp");
            GsmCtl.Status  = GSM_HANG_UP;        // prikaz zaveseni
            GsmCtl.Counter = GSM_WAIT_ESC;       // cekej pred ESC
            return;
         }
         if( !GsmConnect()){
            return;
         }
         TRACE( "Connected...");
         GsmCtl.Status  = GSM_DATA_MODE;
         GsmCtl.Counter = GSM_HANGUP_TIMEOUT;
         GsmConnected();                         // callback pripojeni
         break;

      case GSM_DATA_MODE :
         TRACEI( "GSM data ? ", GsmCtl.Counter);
         if( --GsmCtl.Counter == 0){
            TRACE( "HangUp");
            GsmDisconnected();                   // callback odpojeni
            GsmCtl.Status  = GSM_HANG_UP;        // prikaz zaveseni
            GsmCtl.Counter = GSM_WAIT_ESC;       // cekej pred ESC
            return;
         }
         if( !ComRxPacket( &Cmd, &Data)){
            return;
         }
         TRACEI( "GSM Rx Cmd : ", Cmd);
         GsmCtl.Counter = GSM_HANGUP_TIMEOUT;    // znovu timeout
         GsmExecuteCommand( Cmd, Data);
         break;

      case GSM_USER_MODE :
         TRACE( "GSM User mode");
         break;

#ifdef GSM_SMS
      case GSM_SMS_SENDING :
         TRACEI( "GSM send ? ", GsmCtl.Counter);
         if( --GsmCtl.Counter == 0){
            TRACE( "Send ERR");
            GsmSmsSendTimeout();
            GsmCtl.Status = GSM_CHECK;           // zpatky na kontrolu spojeni
            return;
         }
         if( !SmsSendOk()){
            return;
         }
         TRACE( "Send OK");
         GsmSmsSendOk();
         GsmCtl.Status = GSM_CHECK;              // zpatky na kontrolu spojeni
         break;
#endif // GSM_SMS

      case GSM_HANG_UP :
         // prikaz zaveseni
         if( --GsmCtl.Counter){
            TRACE( "GSM Wait for ESC");
            break;                               // prodleva bez vysilani
         }
         TRACE( "GSM send +++");
         GsmCommandMode();                       // vyslani ESC
         GsmCtl.Status  = GSM_WAIT_HANG_UP;      // cekame na zaveseni
         GsmCtl.Counter = GSM_WAIT_COMMAND;      // prepnuti do prikazoveho rezimu
         break;
         
      case GSM_WAIT_HANG_UP :
         // cekani na prikazovy mod
         if( --GsmCtl.Counter){
            TRACE( "GSM Wait Hang Up");
            break;                               // prodleva po ESC
         }
         TRACE( "GSM Hang Up");
         GsmHangUp();                            // zaves
         GsmCtl.Status = GSM_CHECK;              // zpatky na kontrolu spojeni
         break;

      case GSM_IDLE :
      default :
         GsmCtl.Registered = NO;                 // Neni prihlasen
         TRACE( "GSM Idle");
         return;
   }
} // GsmExecute
