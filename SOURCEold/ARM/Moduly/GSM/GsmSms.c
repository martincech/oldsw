//******************************************************************************
//
//   GsmSms.c     GSM SMS messaging
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "../../inc/Gsm.h"
#include "../../inc/GsmCtl.h"   // GSM kontext
#include "../../inc/Bcd.h"      // conversion
#include "../../inc/System.h"   // SysDelay
#include "COM_util.h"
#include <string.h>

#if defined( GSM_COM0)
   #define __COM0__
#elif defined( GSM_COM1)   
   #define __COM1__
#else
   #error "Unknown COM number"
#endif   
#include "../../inc/Com.h"


// Preddefinovane retezce :

static const char PduSend[]    = "AT+CMGS=";             // Send SMS + <length><CR>
static const char Prompt[]     = "\r\n> ";               // input prompt
static const char SendOk[]     = "\r\n+CMGS: ";          // Send ok (+message reference, pduack)

static const char PduRead[]    = "AT+CMGR=";             // Read SMS <index><CR>
static const char ReadHeader[] = "\r\n+CMGR: ";          // Header of read message

static const char Delete[]     = "AT+CMGD=";             // Delete SMS <index><CR>

#define CTRL_Z  '\x1A'

#define PDU_FIXED_LENGTH      8        // konstatni delka telegramu bez SMSC
#define TIMESTAMP_LENGTH     14        // delka sekce cas+zona

#define SMS_PROMPT_TIMEOUT 1000        // Send prompt timeout [ms]
#define SMS_READ_TIMEOUT    500        // SMS read timeout


#define TxString( s) ComSecureTxString( s)    // pred vyslanim zrus Rx

//-----------------------------------------------------------------------------
// Send
//-----------------------------------------------------------------------------

TYesNo SmsSend( const char *Number, const char *Text, native MessageLength, native RefNumber)
// Odeslani zpravy. <RefNumber> je referencni cislo zpravy, 0=automaticky
{
native NumberLength;
native i;
byte   ch;
native Shift;

#ifdef GSM_CTL   
   if( !GsmReady()){
      return( NO);                     // kontextova cinnost, nelze vyslat
   }
#endif   
   // delka PDU :
   NumberLength  = strlen( Number);
   i             = (NumberLength + 1) / 2 + (byte)(((word)MessageLength * 7 - 1) / 8) + 1 +
                   PDU_FIXED_LENGTH;
   // vyslani AT prikazu Send :
   TxString( PduSend);
   ComTxDec( i);                       // delka PDU dekadicky
   ComTxChar( '\r');
   // cekani na prompt :
   if( !ComRxMatch( Prompt, SMS_PROMPT_TIMEOUT)){
      return( NO);
   }
   // retezec PDU :
   ComTxHex( 0);                       // default SMS center (nepocita se do delky PDU)
   ComTxHex( 0x11);                    // use expiration, send PDU
   ComTxHex( RefNumber);               // message reference set by ME
   ComTxHex( NumberLength);            // number length
   ComTxHex( 0x91);                    // international format
   // konvertuj cislo volaneho na lo-endian BCD :
   for( i = 0; i < NumberLength; i += 2){
      ComTxDigit( char2dec( Number[ i + 1])); // prvni je lo nibble
      ComTxDigit( char2dec( Number[ i]));     // druhy hi nibble
   }
   if( NumberLength & 0x01){
      // lichy pocet cislic, vysli jeste posledni
      ComTxChar( 'F');                                   // vypln je 0xF0
      ComTxDigit( char2dec( Number[ NumberLength - 1])); // posledni cislice
   }
   // priznaky :
   ComTxHex( 0);                       // no hi-level protocol
   ComTxHex( 0);                       // default 7-bit GSM aplhabet
   ComTxHex( 0xAA);                    // expiration 4 days
   ComTxHex( MessageLength);           // delka zpravy
   // transformace zpravy do 7-bitoveho proudu :
   Shift = 0;
   for( i = 0; i < MessageLength; i++){
      if( Shift == 7){
         Shift = 0;
         continue;                     // znak byl odeslan v predchozim kroku
      }
      ch  = (byte)Text[ i]     >> Shift;
      ch |= (byte)Text[ i + 1] << (7 - Shift);
      ComTxHex( ch);
      Shift++;
   }
   ComTxChar( CTRL_Z);                     // end of PDU input
#ifdef GSM_CTL   
   GsmCtl.Status  = GSM_SMS_SENDING;       // kontext radice
   GsmCtl.Counter = GSM_SMS_SEND_TIMEOUT;  // timeout potvrzeni
#endif   
   return( YES);
} // SmsSend

//-----------------------------------------------------------------------------
// Send OK
//-----------------------------------------------------------------------------

TYesNo SmsSendOk( void)
// Testuje potvrzeni odeslani
{
   if( !ComRxMatch( SendOk, 0)){
      return( NO);
   }
   return( YES);
} // SmsSendOk

//-----------------------------------------------------------------------------
// Receive
//-----------------------------------------------------------------------------

native SmsRead( native Index, char *Number, char *Text)
// Precte prijatou zpravu z pozice <Index>, vrati jeji delku
{
byte   Value;
native NumberLength;
native MessageLength;
native i;
byte   ch;
native Shift;

#ifdef GSM_CTL   
   if( !GsmReady()){
      return( SMS_READ_ERROR);         // kontextova cinnost, nelze cist
   }
#endif   
   Index++;                            // cislovano od 1
   // AT prikaz Read :
   TxString( PduRead);
   ComTxDec( Index);                   // pozice
   ComTxChar( '\r');
   if( !ComRxMatch( ReadHeader, SMS_READ_TIMEOUT)){
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   Value = ComRxDec();                 // typ zpravy
   if( Value != 0 && Value != 1){
      return( SMS_EMPTY_MESSAGE);      // neni to prijata zprava
   }
   // preskoc pole <alpha>
   if( !ComWaitChar( ',')){
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   Value = ComRxDec();                 // delka PDU zpravy
   if( !ComWaitChar( '\n')){           // konec stavoveho radku
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   if( Value == 0){
      return( SMS_EMPTY_MESSAGE);      // prazdna zprava
   }
   // SMS Center address :
   Value = ComRxHex();                 // SMSC length
   ComSkipChars( 2 * Value);           // preskoc hexa pary se SMSC
   // zacatek PDU :
   ComSkipChars( 2);                   // flag
   Value = ComRxHex();                 // sender number length
   Number[ Value] = '\0';              // nastav delku cisla prijemce
   NumberLength = Value;
   Value = ComRxHex();                 // format cisla
   if( Value != 0x91 && Value != 0x81 && Value != 0xA1){
//!!!       Value != 0x50){            // OSKAR
      return( SMS_FORMAT_ERROR);       // unknown number format
   }
   // cti BCD pary cisla
   for( i = 0; i < NumberLength; i += 2){
      Value = ComRxHex();
      Number[ i]     = nibble2dec( Value);
      Number[ i + 1] = nibble2dec( Value >> 4);
   }
   if( NumberLength & 0x01){
      // lichy pocet cislic
      Value = ComRxHex();
      Number[ NumberLength - 1] = nibble2dec( Value);
   }
   Value = ComRxHex();                 // skip protocol
   Value = ComRxHex();                 // aplhabet
   if( Value != 0){
//!!!       Value != 0x50){            // OSKAR
      return( SMS_FORMAT_ERROR);
   }
   ComSkipChars( TIMESTAMP_LENGTH);    // skip time & timezone
   Value = ComRxHex();                 // message length
   MessageLength = Value;
   if( MessageLength > SMS_MAX_LENGTH - 1){
      MessageLength = SMS_MAX_LENGTH - 1;  // zariznuti zpravy
   }
   // transformace zpravy ze 7-bitoveho kodovani :
   Value = ComRxHex();                 // prvni par
   ch    = Value;                      // predchozi znak
   Shift = 0;                          // shift aktivniho znaku
   for( i = 0; i < MessageLength; i++){
      ch |= Value << Shift;            // doplneni zbytku znaku
      ch &= 0x7F;                      // jen 7 bitu
      if( ch == 0){
         ch = '@';                     // GSM 0 = @
      }
      Text[ i] = ch;
      ch = Value >> (7 - Shift);       // priprav LSB druheho znaku
      Shift++;                         // posun zbytku druheho znaku
      if( Shift == 8){
         Shift = 0;                    // cteni vynechame ch == Value
      } else {
         Value = ComRxHex();           // nacti zbytek druheho znaku
      }
   }
   Text[ MessageLength] = 0;           // terminator
   return( MessageLength);
} // SmsRead

//-----------------------------------------------------------------------------
// Delete
//-----------------------------------------------------------------------------

TYesNo SmsDelete( native Index)
// Smaze prijatou zpravu z pozice <Index>
{
#ifdef GSM_CTL   
   if( !GsmReady()){
      return( NO);                     // kontextova cinnost, nelze mazat
   }
#endif   
   Index++;                            // cislovano od 1
   TxString( Delete);
   ComTxDec( Index);                   // pozice
   ComTxChar( '\r');
   return( YES);
} // SmsDelete

