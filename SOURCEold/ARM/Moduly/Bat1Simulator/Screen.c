//******************************************************************************
//
//   Screen.c     Basic screen drawings
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "Screen.h"
#include "../Inc/System.h"             // Operating system
#include "../Inc/Graphic.h"            // graphic
#include "../Inc/conio.h"              // Display
#include "../Inc/File/Fs.h"            // File system
#include "Str.h"                       // Strings
#include "../Inc/Wgt/DTime.h"          // Display Time
#include "../Inc/Wgt/DProgress.h"      // Display progress box
#include "../Inc/Wgt/DLabel.h"         // Display text label
#include "../Inc/Filter.h"             // Filter status
#include "../Inc/sputchar.h"           // buffer putchar
#include "../Inc/xTime.h"              // time formatting
#include "Fonts.h"                     // Font list
#include "Sdb.h"                       // Samples database
#include "PMan.h"                      // Power management
#include "Weighing.h"                  // Weighing
#include "Calc.h"                      // Statistics
#include "GLogo.h"                     // Graphic logo
#include "Bitmap.h"                    // Project Bitmaps
#include "Wgt/DMsg.h"                  // Display Message
#include "Wgt/DHistogram.h"            // Display Histogram
#include "Wgt/DWeight.h"               // Display weight
#include "Wgt/DLayout.h"               // Display layout
#include <string.h>

// Status line (top) :
#define STATUS_Y          0
#define STATUS_H          14
#define STATUS_LINE_Y     15
#define STATUS_COLOR      COLOR_LIGHTGRAY
#define STATUS_LINE_COLOR COLOR_DARKGRAY
#define STATUS_TEXT_Y     2                // date/time

// Basic :
#define BASIC_LAST_SAVED_X      116
#define BASIC_LAST_SAVED_Y      99
#define BASIC_LAST_SAVED_W      20
#define BASIC_LAST_SAVED_H      20
#define BASIC_FRAME_TOP_Y       95
#define BASIC_FRAME_BOTTOM_Y    122
#define BASIC_WEIGHT_X          0
#define BASIC_WEIGHT_Y          28
#define BASIC_WEIGHT_FONT       ARIAL_BOLD90

// Advanced :
#define ADVANCED_LAST_SAVED_X   113
#define ADVANCED_LAST_SAVED_Y   72
#define ADVANCED_LAST_SAVED_W   20
#define ADVANCED_LAST_SAVED_H   20
#define ADVANCED_WEIGHT_X       95
#define ADVANCED_WEIGHT_Y       27
#define ADVANCED_WEIGHT_FONT    ARIAL_BOLD55

// Large :
#define LARGE_FRAME_TOP_Y     93
#define LARGE_FRAME_BOTTOM_Y  134
#define LARGE_WEIGHT_X        BASIC_WEIGHT_X
#define LARGE_WEIGHT_Y        BASIC_WEIGHT_Y
#define LARGE_WEIGHT_FONT     BASIC_WEIGHT_FONT

// Accu symbol dimensions :
#define ACCU_X  0
#define ACCU_Y  2
#define ACCU_W  23
#define ACCU_H  10

// Memory symbol dimensions :
#define MEM_X   217
#define MEM_Y   2
#define MEM_W   23
#define MEM_H   10

// Big accu symbol dimensions :
#define BIG_ACCU_W          30
#define BIG_ACCU_H          63
#define BIG_ACCU_X         (G_WIDTH  / 2 - BIG_ACCU_W / 2)
#define BIG_ACCU_Y          47
#define BIG_ACCU_TEXT_Y     120
#define BIG_ACCU_TEXT_H     15

// Memory status :
#define MEM_FREE_BLOCKS    3           // free blocks for warning

#define ACCU_MAX_COUNT     6           // number of accu symbols

// Local variables :

static byte AccuCounter = 0;

// Local functions :

static void StatusLineFrame( void);
// Status line frame

static void StatusLine( void);
// Display status line

static void BasicScreen( void);
// Basic screen

static void AdvancedScreen( void);
// Advanced screen

static void LargeScreen( void);
// Large screen

static void DisplayDateTime( int x, int y);
// Display date & time

static void DisplayAccu( void);
// Display accu capacity

static void DisplayMemory( void);
// Display free memory

static void DisplayBigAccu( void);
// Display accu capacity (charger only)

static void DisplayFlag( int x, int y);
// Display flag symbol (advanced, basic)

static void DisplayLargeFlag( int x, int y);
// Display flag symbol (strong)

static int MemCapacity( void);
// Returns percent of the memory capacity

static TYesNo MemWarning( void);
// Returns on memory low

//------------------------------------------------------------------------------
// Logo
//------------------------------------------------------------------------------

void ScreenLogo( void)
// Display initial logo
{
   GLogo();
   // scale name
   SetFont( TAHOMA16);
   GSetMode( GMODE_XOR);               // always visible
   GTextAt( 0, G_HEIGHT - GCharHeight() - 1);
   cputs( Config.ScaleName);
   // version string (right aligned) :
#ifdef PROJECT_SCREEN_VERSION
   DLabelFormat( G_WIDTH - 2, G_HEIGHT - GCharHeight() - 1,
                 "%2d.%02d.%1d", Config.Version >> 8, Config.Version & 0xFF, Config.Build);
#endif
#ifdef PROJECT_SCREEN_VERSION_COBB
   DLabelFormat( G_WIDTH - 2, G_HEIGHT - GCharHeight() - 1,
                 "%2d.%02d.%1d Cobb", Config.Version >> 8, Config.Version & 0xFF, Config.Build);
#endif
#ifdef PROJECT_SCREEN_VERSION_AVIAGEN
   DLabelFormat( G_WIDTH - 2, G_HEIGHT - GCharHeight() - 1,
                 "%2d.%02d.%1d Aviagen", Config.Version >> 8, Config.Version & 0xFF, Config.Build);
#endif
   // redraw
   GSetMode( GMODE_REPLACE);          // default mode
   GFlush();
} // ScreenLogo

//------------------------------------------------------------------------------
// Boot
//------------------------------------------------------------------------------

void ScreenBoot( void)
// Display boot logo
{
   GClear();
   SetFont( TAHOMA16);
   cgoto( 0, 0);
   cputs( "(c) Veit electronics");
   GSetFixedPitch();
   cgoto( 2, 0);
   cputs( "BAT 1");
   GSetNormalPitch();
   cgoto( 4, 0);
   cputs( "DIAGNOSTICS");
   SetFont( TAHOMA16);
   GFlush();
} // ScreenBoot

//------------------------------------------------------------------------------
// Charger
//------------------------------------------------------------------------------

void ScreenCharger( void)
// Display charger screen
{
   // animation counter :
   AccuCounter++;
   if( AccuCounter >= ACCU_MAX_COUNT){
      AccuCounter = 0;
   }
   GClear();
   StatusLineFrame();
   // date/time :
   DisplayDateTime( 2, STATUS_TEXT_Y);
   // charger/USB symbol :
   if( PManCharger()){
      GBitmap( G_WIDTH - 20 - 1, 1, &BmpCharger);
   } else if( PManUsb()){
      GBitmap( G_WIDTH - 20 - 1, 1, &BmpPC);
   }
   // accumulator animation :
   DisplayBigAccu();
} // ScreenCharger

//------------------------------------------------------------------------------
// Redraw
//------------------------------------------------------------------------------

void ScreenRedraw( void)
// Redraw screen
{
   AccuCounter++;
   if( AccuCounter >= ACCU_MAX_COUNT){
      AccuCounter = 0;
   }
   GClear();
   StatusLine();
   switch( Config.Display.Mode){
      case DISPLAY_MODE_BASIC :
         BasicScreen();
         break;
      case DISPLAY_MODE_ADVANCED :
         AdvancedScreen();
         break;
      case DISPLAY_MODE_LARGE :
         LargeScreen();
         break;
   }
} // ScreenRedraw

//------------------------------------------------------------------------------
// Flash
//------------------------------------------------------------------------------

void ScreenFlash( void)
// Clear flashing objects
{
   GSetColor( STATUS_COLOR);
   // flash accu :
   if( !PManCharger() && !PManUsb() && PManAccuWarning()){
      GBox( ACCU_X, ACCU_Y, ACCU_W, ACCU_H);
   }
   // flash memory :
   if( MemWarning()){
      GBox( MEM_X, MEM_Y, MEM_W, MEM_H);
   }
   //---- file specific
   if( !SdbInfo()){
      return;
   }
   if( SdbConfig()->Saving.Mode != SAVING_MODE_AUTOMATIC){
      return;                // manual mode, don't display status
   }
   if( FilterRecord.Status != FILTER_WAIT_EMPTY){
      return;                // wait for charge/weighing
   }
   // wait for discharge : flash saved symbol
   GSetColor( DCOLOR_BACKGROUND);
   switch( Config.Display.Mode){
      case DISPLAY_MODE_BASIC :
         GBox( BASIC_LAST_SAVED_X, BASIC_LAST_SAVED_Y, BASIC_LAST_SAVED_W, BASIC_LAST_SAVED_H);
         break;
      case DISPLAY_MODE_ADVANCED :
         GBox( ADVANCED_LAST_SAVED_X, ADVANCED_LAST_SAVED_Y, ADVANCED_LAST_SAVED_W, ADVANCED_LAST_SAVED_H);
         break;
      case DISPLAY_MODE_LARGE :
         break;
   }
   GSetColor( DCOLOR_DEFAULT);
} // ScreenFlash

//------------------------------------------------------------------------------
// Sorting Flag
//------------------------------------------------------------------------------

void ScreenSorting( TSampleFlag Flag)
// Display sorting <Flag>
{
   switch( Config.Display.Mode){
      case DISPLAY_MODE_ADVANCED :
         GSetColor( DCOLOR_BACKGROUND);
         GBox( 96, 19, 144, 49);                 // clear area
         GSetColor( DCOLOR_DEFAULT);
         switch( Flag){
            case FLAG_NONE :
            case FLAG_MALE :
            case FLAG_FEMALE :
               SetFont( ADVANCED_WEIGHT_FONT);   // redraw actual weight
               DWeightNarrow( ADVANCED_WEIGHT_X, ADVANCED_WEIGHT_Y, WeighingWeight());
               GSetMode( GMODE_XOR);             // invert area
               GBitmap( 96, 19, &BmpSortAdvancedNone);
               GSetMode( GMODE_REPLACE);
               break;
            case FLAG_LIGHT :
               GBitmap( 96, 19, &BmpSortAdvancedLight);
               break;
            case FLAG_OK :
               GBitmap( 96, 19, &BmpSortAdvancedOK);
               break;
            case FLAG_HEAVY :
               GBitmap( 96, 19, &BmpSortAdvancedHeavy);
               break;
         }
         break;

      case DISPLAY_MODE_LARGE :
      case DISPLAY_MODE_BASIC :
         GSetColor( DCOLOR_BACKGROUND);
         GBox( 0, 20, 240, 71);                  // clear area
         GSetColor( DCOLOR_DEFAULT);
         switch( Flag){
            case FLAG_NONE :
            case FLAG_MALE :
            case FLAG_FEMALE :
               SetFont( BASIC_WEIGHT_FONT);      // redraw actual weight
               DWeightNarrow( BASIC_WEIGHT_X, BASIC_WEIGHT_Y, WeighingWeight());
               GSetMode( GMODE_XOR);             // invert area
               GBitmap( 0, 20, &BmpSortNone);
               GSetMode( GMODE_REPLACE);
               break;
            case FLAG_LIGHT :
               GBitmap( 0, 20, &BmpSortLight);
               break;
            case FLAG_OK :
               GBitmap( 0, 20, &BmpSortOK);
               break;
            case FLAG_HEAVY :
               GBitmap( 0, 20, &BmpSortHeavy);
               break;
         }
         break;
   }
   GFlush();
} // ScreenSorting

//------------------------------------------------------------------------------
// Power fail
//------------------------------------------------------------------------------

void ScreenPowerFail( void)
// Display Power fail screen
{
   GClear();
   GFlush();
} // ScreenPowerFail

//------------------------------------------------------------------------------
// Shutdown
//------------------------------------------------------------------------------

void ScreenShutdown( void)
// Display shutdown screen
{
   GClear();
   GBitmap( (G_WIDTH - 40) / 2, (G_HEIGHT - 40) / 2, &BmpOff);
   GSetColor( DCOLOR_DEFAULT);
   SetFont( TAHOMA16);
   DLabelCenter( STR_OFF, 0, 120, G_WIDTH, 0);
   GFlush();
} // ScreenShutdown

//------------------------------------------------------------------------------
// Charger Shutdown
//------------------------------------------------------------------------------

void ScreenChargerOff( void)
// Display charger shutdown
{
   GClear();
   GFlush();
} // ScreenChargerOff

//------------------------------------------------------------------------------
// Wait
//------------------------------------------------------------------------------

void ScreenWait( void)
// Display waiting
{
   GClear();
   GBitmap( 104, 55, &BmpWait);
   GSetColor( DCOLOR_DEFAULT);
   SetFont( TAHOMA16);
   DLabelCenter( STR_WAIT, 0, 122, G_WIDTH, 0);
   GFlush();
} // ScreenWait

//------------------------------------------------------------------------------
// No memory
//------------------------------------------------------------------------------

void ScreenNoMemory( void)
// Display no memory message
{
   DMsgOk( STR_ERROR, STR_NO_MEMORY, 0);
} // ScreenNoMemory

//------------------------------------------------------------------------------
// Capacity
//------------------------------------------------------------------------------

#define DG_LEFT_TEXT 5

void ScreenCapacity( int FilesCount, int FilesSize, int GroupsCount, int PercentUsed)
// Memory capacity screen
{
int y;

   SetFont( TAHOMA16);
   GClear();
   DLayoutTitle( STR_MEMORY_CAPACITY);
   DLayoutStatus( 0, STR_BTN_OK, 0);
   y = DLAYOUT_TITLE_H + 5;
   // files count
   GTextAt( DG_LEFT_TEXT, y);
   cputs( STR_FILES_COUNT);
   DLabelFormat( DMENU_PARAMETERS_X, y, "%d", FilesCount);
   y += GCharHeight();
   // files size
   GTextAt( DG_LEFT_TEXT, y);
   cputs( STR_FILES_SIZE);
   DLabelFormat( DMENU_PARAMETERS_X, y, "%d", FilesSize);
   y += GCharHeight();
   // groups count
   GTextAt( DG_LEFT_TEXT, y);
   cputs( STR_GROUPS_COUNT);
   DLabelFormat( DMENU_PARAMETERS_X, y, "%d", GroupsCount);
   y += GCharHeight();
   // memory used
   GTextAt( DG_LEFT_TEXT, y);
   cputs( STR_MEMORY_USED);
   DLabelFormat( DMENU_PARAMETERS_X, y, "%3.1f %%", PercentUsed);  // [0.1 %]
   y += GCharHeight();
   GFlush();
} // ScreenCapacity

//******************************************************************************

//------------------------------------------------------------------------------
//  Status Frame
//------------------------------------------------------------------------------

static void StatusLineFrame( void)
// Status line frame
{
   GSetColor( STATUS_COLOR);
   GBox( 0, STATUS_Y, G_WIDTH, STATUS_H);
   GSetColor( STATUS_LINE_COLOR);
   GLine( 0, STATUS_LINE_Y, G_WIDTH - 1, STATUS_LINE_Y);
   GSetColor( DCOLOR_DEFAULT);
} // StatusLineFrame

//------------------------------------------------------------------------------
// Info Line
//------------------------------------------------------------------------------

static void StatusLine( void)
// Display status line
{
   StatusLineFrame();
   // date/time :
   DisplayDateTime( 28, STATUS_TEXT_Y);
   // bargraphs :
   DisplayAccu();
   DisplayMemory();
   // charger/USB symbol :
   if( PManCharger()){
      GBitmap( 195, 1, &BmpCharger);
   } else if( PManUsb()){
      GBitmap( 195, 1, &BmpPC);
   }
   // working file data -------------------------------------------------------
   if( !SdbInfo()){
      return;                          // no active file
   }
   // number of birds :
   if( WeighingNumberOfBirds() > 0){
      GBitmap( 165, 0, &BmpMoreBirds);
      SetFont( ARIAL_BOLD14);
      GTextAt( 179, STATUS_TEXT_Y);
      cprintf( "%2d", WeighingNumberOfBirds());
   }
   // mode :
   if( WeighingTestActive()){
   switch( SdbConfig()->Saving.Mode){
      case SAVING_MODE_AUTOMATIC :
         GBitmap( 126, 0, &BmpModeAuto);
         break;
      case SAVING_MODE_MANUAL :
         GBitmap( 126, 0, &BmpModeManual);
         break;
      case SAVING_MODE_MANUAL_BY_SEX :
         GBitmap( 126, 0, &BmpModeManualBySex);
         break;
   }
   }
   // sort mode :
   if( SdbConfig()->Saving.Mode == SAVING_MODE_MANUAL_BY_SEX){
      return;                          // sort by sex only
   }
   switch( SdbConfig()->WeightSorting.Mode){
      case WEIGHT_SORTING_NONE :
         break;

      case WEIGHT_SORTING_LIGHT_HEAVY :
         GBitmap( 150, 0, &BmpLightHeavy);
         break;

      case WEIGHT_SORTING_LIGHT_OK_HEAVY :
         GBitmap( 150, 0, &BmpLightOKHeavy);
         break;
   }
} // StatusLine

//------------------------------------------------------------------------------
// Basic
//------------------------------------------------------------------------------

static void BasicScreen( void)
// Basic screen
{
TWeight Weight;

   // actual weight :
   Weight = WeighingWeight();          // set status too
   // actual status :
   switch( WeighingStatus()){
      case WEIGHING_OK :
         // actual weight
         SetFont( BASIC_WEIGHT_FONT);
         DWeightNarrow( BASIC_WEIGHT_X, BASIC_WEIGHT_Y, Weight);
         break;
      case WEIGHING_OVER :
         // overflow
         GBitmap( 64, 26, &BmpBasicOverflow);
         break;
      case WEIGHING_UNDER :
         // underflow
         GBitmap( 64, 26, &BmpBasicUnderflow);
         break;
   }
   // frame :
   GSetColor( COLOR_DARKGRAY);
   GLine(  2, BASIC_FRAME_TOP_Y,    G_WIDTH - 3, BASIC_FRAME_TOP_Y);     // Top
   GLine(  2, BASIC_FRAME_BOTTOM_Y, G_WIDTH - 3, BASIC_FRAME_BOTTOM_Y);  // Bottom
   GLine(  0,           BASIC_FRAME_TOP_Y + 2, 0,           BASIC_FRAME_BOTTOM_Y - 2); // Left
   GLine(  G_WIDTH - 1, BASIC_FRAME_TOP_Y + 2, G_WIDTH - 1, BASIC_FRAME_BOTTOM_Y - 2); // Right
   GPixel( 1,           BASIC_FRAME_TOP_Y + 1);     // Left Top
   GPixel( 1,           BASIC_FRAME_BOTTOM_Y - 1);  // Left Bottom
   GPixel( G_WIDTH - 2, BASIC_FRAME_TOP_Y + 1);     // Right Top
   GPixel( G_WIDTH - 2, BASIC_FRAME_BOTTOM_Y - 1);  // Right Bottom
   GSetColor( DCOLOR_DEFAULT);
   // file :
   GBitmap( 5, 138, &BmpFile);
   SetFont( ARIAL_BOLD18);
   GTextAt( 36, 142);
   // working file data -------------------------------------------------------
   if( !SdbInfo()){
      cputs( STR_NO_FILE);
      return;                          // no active file
   }
   cputs( SdbInfo()->Name);
   // last weight :
   if( WeighingLastFlag() != FLAG_INVALID){
      GBitmap( BASIC_LAST_SAVED_X, BASIC_LAST_SAVED_Y, &BmpLastSaved);
      SetFont( ARIAL_BOLD27);
      GTextAt( 140, 102);
      DWeightLeft( WeighingLastWeight());
      DisplayFlag( 208, 97);
   } // else empty database
   // statistics/count :
   SetFont( ARIAL_BOLD27);
   GBitmap( 5, 99, &BmpCount);
   GTextAt( 35, 102);
   cprintf( "%d", Calc[ STATISTICS_TOTAL].Count);
} // BasicScreen

//------------------------------------------------------------------------------
// Advanced
//------------------------------------------------------------------------------

static void AdvancedScreen( void)
// Advanced screen
{
TWeight Weight;

   // actual weight :
   Weight = WeighingWeight();          // set status too
   // actual status :
   switch( WeighingStatus()){
      case WEIGHING_OK :
         // actual weight
         SetFont( ADVANCED_WEIGHT_FONT);
         DWeightNarrow( ADVANCED_WEIGHT_X, ADVANCED_WEIGHT_Y, Weight);
         break;
      case WEIGHING_OVER :
         // overflow
         GBitmap( 130, 24, &BmpAdvancedOverflow);
         break;
      case WEIGHING_UNDER :
         // underflow
         GBitmap( 130, 24, &BmpAdvancedUnderflow);
         break;
   }
   // working file data -------------------------------------------------------
   // file :
   GBitmap( 5, 138, &BmpFile);
   SetFont( ARIAL_BOLD18);
   GTextAt( 36, 142);
   if( !SdbInfo()){
      cputs( STR_NO_FILE);
      return;                          // no active file
   }
   cputs( SdbInfo()->Name);
   // last weight :
   if( WeighingLastFlag() != FLAG_INVALID){
      GBitmap( ADVANCED_LAST_SAVED_X, ADVANCED_LAST_SAVED_Y, &BmpLastSaved);
      SetFont( ARIAL_BOLD27);
      GTextAt( 145, 74);
      DWeightLeft( WeighingLastWeight());
      DisplayFlag( 213, 71);
   } // else empty database
   // statistics/count :
   SetFont( ARIAL_BOLD27);
   GBitmap( 113, 104, &BmpCount);
   GTextAt( 145, 106);
   cprintf( "%d", Calc[ STATISTICS_TOTAL].Count);
   // statistics/average :
   SetFont( ARIAL_BOLD18);
   GBitmap( 19, 24, &BmpWeight);
   GTextAt( 40, 23);
   DWeightLeft( Calc[ STATISTICS_TOTAL].Average);
   // statistics/sigma :
   GBitmap( 19, 42, &BmpSigma);
   GTextAt( 40, 42);
   DWeightLeft( Calc[ STATISTICS_TOTAL].Sigma);
   // statistics/CV :
   GTextAt( 8, 61);
   cputs( "CV");
   GTextAt( 40, 61);
   cprintf( "%3.1f %%", Calc[ STATISTICS_TOTAL].Cv);
   // statistics/UNI :
   GTextAt( 2, 80);
   cputs( "UNI");
   GTextAt( 40, 80);
   cprintf( "%3.1f %%", Calc[ STATISTICS_TOTAL].Uniformity);
   // histogram :
   DHistogramIcon( &Calc[ STATISTICS_TOTAL].Histogram, 8, 103);
} // AdvancedScreen

//------------------------------------------------------------------------------
// Large
//------------------------------------------------------------------------------

static void LargeScreen( void)
// Large screen
{
TWeight Weight;

   // actual weight :
   Weight = WeighingWeight();          // set status too
   // actual status :
   switch( WeighingStatus()){
      case WEIGHING_OK :
         // actual weight
         SetFont( LARGE_WEIGHT_FONT);
         DWeightNarrow( LARGE_WEIGHT_X, LARGE_WEIGHT_Y, Weight);
         break;
      case WEIGHING_OVER :
         // overflow
         GBitmap( 64, 26, &BmpBasicOverflow);
         break;
      case WEIGHING_UNDER :
         // underflow
         GBitmap( 64, 26, &BmpBasicUnderflow);
         break;
   }
   // frame :
   GSetColor( COLOR_DARKGRAY);
   GLine(  2, LARGE_FRAME_TOP_Y,    G_WIDTH - 3, LARGE_FRAME_TOP_Y);     // Top
   GLine(  2, LARGE_FRAME_BOTTOM_Y, G_WIDTH - 3, LARGE_FRAME_BOTTOM_Y);  // Bottom
   GLine(  0,           LARGE_FRAME_TOP_Y + 2, 0,           LARGE_FRAME_BOTTOM_Y - 2); // Left
   GLine(  G_WIDTH - 1, LARGE_FRAME_TOP_Y + 2, G_WIDTH - 1, LARGE_FRAME_BOTTOM_Y - 2); // Right
   GPixel( 1,           LARGE_FRAME_TOP_Y + 1);     // Left Top
   GPixel( 1,           LARGE_FRAME_BOTTOM_Y - 1);  // Left Bottom
   GPixel( G_WIDTH - 2, LARGE_FRAME_TOP_Y + 1);     // Right Top
   GPixel( G_WIDTH - 2, LARGE_FRAME_BOTTOM_Y - 1);  // Right Bottom
   GSetColor( DCOLOR_DEFAULT);
   // working file data -------------------------------------------------------
   // file :
   SetFont( ARIAL_BOLD21);
   GTextAt( 2, 139);
   if( !SdbInfo()){
      cputs( STR_NO_FILE);
      return;                          // no active file
   }
   cputs( SdbInfo()->Name);
   if( SdbConfig()->Saving.Mode == SAVING_MODE_AUTOMATIC &&
       FilterRecord.Status == FILTER_WAIT_EMPTY){
      // wait for discharge, invert display
      GBoxRound( 0, LARGE_FRAME_TOP_Y, G_WIDTH, LARGE_FRAME_BOTTOM_Y - LARGE_FRAME_TOP_Y + 1, 2, 2);
      GSetMode( GMODE_XOR);            // color inversion
   }
   // last weight :
   if( WeighingLastFlag() != FLAG_INVALID){
      SetFont( ARIAL_BOLD45);
      GTextAt( 107, 100);
      DWeightLeft( WeighingLastWeight());
      DisplayLargeFlag( 202, 98);
   } // else empty database
   // statistics/count :
   SetFont( ARIAL_BOLD45);
   GTextAt( 4, 100);
   cprintf( "%d", Calc[ STATISTICS_TOTAL].Count);
   GSetMode( GMODE_REPLACE);
} // LargeScreen

//------------------------------------------------------------------------------
// Date & Time
//------------------------------------------------------------------------------

static void DisplayDateTime( int x, int y)
// Display date & time
{
char   Buffer[ 32];
int    i;
TYesNo IsPm;

   if( CountryGetTimeFormat() == TIME_FORMAT_24){
      GTextAt( x, y);
      SetFont( ARIAL_BOLD14);
      DDateTimeShort( SysGetClock());
      return;
   } // else am/pm
   sputcharbuffer( Buffer);
   xDateTimeShort( sputchar, SysGetClock());
   sputchar( '\0');
   i = strlen( Buffer);
   IsPm = Buffer[ i - 2] == 'p';       // remember suffix
   Buffer[ i - 2] = '\0';              // remove am/pm suffix
   // draw text
   GTextAt( x, y);
   SetFont( ARIAL_BOLD14);
   cputs( Buffer);
   // draw suffix
   i = GTextWidth( Buffer);
   if( IsPm){
      GBitmap( x + i, y + 4, &BmpPm);
   } else {
      GBitmap( x + i, y + 4, &BmpAm);
   }
} // DisplayDateTime

//------------------------------------------------------------------------------
// Accu
//------------------------------------------------------------------------------

static void DisplayAccu( void)
// Display accu capacity
{
int Percent;

   // accu symbol :
   GBitmap( ACCU_X, ACCU_Y, &BmpBattery);
   if( PManCharger() || PManUsb()){
      // charger active
      if( PManCharged()){
         Percent = 100;                // charging done
      } else {
         Percent = AccuCounter * 20;   // charging in process
      }
   } else {
      // no charger
      Percent = PManAccuCapacity();
   }
   GSetColor( COLOR_DARKGRAY);
   DProgressBarRight( Percent, ACCU_X + 3, ACCU_Y + 1, ACCU_W - 4, (ACCU_H - 2) / 2);
   GSetColor( COLOR_BLACK);
   DProgressBarRight( Percent, ACCU_X + 3, ACCU_Y + 5, ACCU_W - 4, (ACCU_H - 2) / 2);
} // DisplayAccu

//------------------------------------------------------------------------------
// Memory
//------------------------------------------------------------------------------

static void DisplayMemory( void)
// Display free memory
{
   // memory symbol :
   GBitmap( MEM_X, MEM_Y, &BmpMemory);
   GSetColor( COLOR_DARKGRAY);
   DProgressBar( MemCapacity(), MEM_X + 1, MEM_Y + 1, MEM_W - 2, (MEM_H - 2) / 2);
   GSetColor( COLOR_BLACK);
   DProgressBar( MemCapacity(), MEM_X + 1, MEM_Y + 5, MEM_W - 2, (MEM_H - 2) / 2);
} // DisplayMemory

//------------------------------------------------------------------------------
// Big accu
//------------------------------------------------------------------------------

static void DisplayBigAccu( void)
// Display accu capacity (charger only)
{
int Counter;

   GBitmap( BIG_ACCU_X, BIG_ACCU_Y, &BmpAccu);
   SetFont( TAHOMA16);
   if( PManCharged()){
      DLabelCenter( STR_CHARGED, 0, BIG_ACCU_TEXT_Y, G_WIDTH, BIG_ACCU_TEXT_H);
      Counter = ACCU_MAX_COUNT - 1;    // full accu
   } else {
      DLabelCenter( STR_CHARGING, 0, BIG_ACCU_TEXT_Y, G_WIDTH, BIG_ACCU_TEXT_H);
      Counter = AccuCounter;           // charging in process
   }
   // draw contents :
   if( Counter == 0){
      return;                          // empty accu
   }
   GSetColor( COLOR_BLACK);
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 13, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 13, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 1){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 24, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 24, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 2){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 35, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 35, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 3){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 46, 12, 10);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 46, 12, 10);
   GSetColor( COLOR_BLACK);
   if( Counter == 4){
      return;
   }
   GBox( BIG_ACCU_X + 3,  BIG_ACCU_Y + BIG_ACCU_H - 56, 12, 9);
   GSetColor( COLOR_DARKGRAY);
   GBox( BIG_ACCU_X + 15, BIG_ACCU_Y + BIG_ACCU_H - 56, 12, 9);
   GSetColor( COLOR_BLACK);
} // DisplayBigAccu

//------------------------------------------------------------------------------
// Flag
//------------------------------------------------------------------------------

static void DisplayFlag( int x, int y)
// Display flag symbol (advanced, basic)
{
   switch( WeighingLastFlag()){
      case FLAG_NONE :
         break;
      case FLAG_OK :
         GBitmap( x, y, &BmpFlagOK);
         break;
      case FLAG_LIGHT :
         GBitmap( x, y, &BmpFlagLight);
         break;
      case FLAG_HEAVY :
         GBitmap( x, y, &BmpFlagHeavy);
         break;
      case FLAG_MALE :
         GBitmap( x, y, &BmpFlagMale);
         break;
      case FLAG_FEMALE :
         GBitmap( x, y, &BmpFlagFemale);
         break;
   }
} // DisplayFlag

//------------------------------------------------------------------------------
// Large Flag
//------------------------------------------------------------------------------

static void DisplayLargeFlag( int x, int y)
// Display flag symbol (strong)
{
   switch( WeighingLastFlag()){
      case FLAG_NONE :
         break;
      case FLAG_OK :
         GBitmap( x, y, &BmpFlagLargeOK);
         break;
      case FLAG_LIGHT :
         GBitmap( x, y, &BmpFlagLargeLight);
         break;
      case FLAG_HEAVY :
         GBitmap( x, y, &BmpFlagLargeHeavy);
         break;
      case FLAG_MALE :
         GBitmap( x, y, &BmpFlagLargeMale);
         break;
      case FLAG_FEMALE :
         GBitmap( x, y, &BmpFlagLargeFemale);
         break;
   }
} // DisplayLargeFlag

//------------------------------------------------------------------------------
// Memory capacity
//------------------------------------------------------------------------------

static int MemCapacity( void)
// Returns percent of the memory capacity
{
   return((FsAvailable() * 100) / FS_CAPACITY);   // free [%]
} // MemCapacity

//------------------------------------------------------------------------------
// Memory warning
//------------------------------------------------------------------------------

static TYesNo MemWarning( void)
// Returns on memory low
{
 return( FsAvailable() < MEM_FREE_BLOCKS * FS_BLOCK_SIZE);  // free capacity warning
} // MemWarning
