//*****************************************************************************
//
//    Uart0.c - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/Uart0.h"
#include "../../inc/System.h"

#if   defined( __LPC210x__) || defined( __LPC213x__)
   // P0.0 - TxD, P0.1 - RxD
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK   ((3 << 0) | (3 << 2))   // PINSEL0, P0.0, P0.1 dibits
   #define PINSEL_UART   ((1 << 0) | (1 << 2))   // PINSEL0 function 01 on P0.0, P0.1

   #define GPIO_UART       (GPIO0_IODIR)
   #define GPIO_UART_MASK  ((1 << 0) | (1 << 1))
#else
   #error "Unknown processor model"
#endif

#define BAUD_DIVISOR  (FBUS / 16 / UART0_BAUD)

static word _Timeout;

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void Uart0Init( void)
// Communication initialisation
{
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_UART;  // select pin fuction
} // Uart0Init

//-----------------------------------------------------------------------------
// Odpojeni
//-----------------------------------------------------------------------------

void Uart0Disconnect( void)
// Disconnect port pins
{
   PINSEL     = (PINSEL & ~PINSEL_MASK);          // back to GPIO
   GPIO_UART &= ~GPIO_UART_MASK;                  // direction to input
} // Uart0Disconnect

//-----------------------------------------------------------------------------
// Setup
//-----------------------------------------------------------------------------

// format conversion table
static const byte _LcrFormat[ _COM_FORMAT_COUNT] = {
/* COM_8BITS       */  LCR_CHAR8 | LCR_STOP1 | LCR_NO_PARITY,
/* COM_8BITS_EVEN  */  LCR_CHAR8 | LCR_STOP1 | LCR_EVEN_PARITY,
/* COM_8BITS_ODD   */  LCR_CHAR8 | LCR_STOP1 | LCR_ODD_PARITY,
/* COM_8BITS_MARK  */  LCR_CHAR8 | LCR_STOP1 | LCR_MARK_PARITY,
/* COM_8BITS_SPACE */  LCR_CHAR8 | LCR_STOP1 | LCR_SPACE_PARITY,
/* COM_7BITS       */  LCR_CHAR7 | LCR_STOP1 | LCR_NO_PARITY,
/* COM_7BITS_EVEN  */  LCR_CHAR7 | LCR_STOP1 | LCR_EVEN_PARITY,
/* COM_7BITS_ODD   */  LCR_CHAR7 | LCR_STOP1 | LCR_ODD_PARITY,
/* COM_7BITS_MARK  */  LCR_CHAR7 | LCR_STOP1 | LCR_MARK_PARITY,
/* COM_7BITS_SPACE */  LCR_CHAR7 | LCR_STOP1 | LCR_SPACE_PARITY
};

void Uart0Setup( unsigned Baud, unsigned Format, word Timeout)
// Set communication parameters
{
unsigned BaudDivisor;

   UART0_IER = 0;                      // disable all interrupts
   UART0_IIR = 0;                      // clear interrupt ID register
   UART0_LSR = 0;                      // clear line status register
   // set divisor :
   #ifndef UART0_MULVAL
      BaudDivisor = (FBUS / 16) / Baud;     
   #else
      BaudDivisor = ((FBUS / 16) * UART0_MULVAL) / (UART0_MULVAL + UART0_DIVADDVAL) / Baud;
      UART0_FDR = FDR_DIVADDVAL( UART0_DIVADDVAL) | FDR_MULVAL( UART0_MULVAL);
   #endif
   UART0_LCR = LCR_DLAB;               // enable divisor latch access
   UART0_DLL = BaudDivisor & 0xFF;
   UART0_DLM = BaudDivisor >> 8;
   // set mode :
   UART0_LCR = _LcrFormat[ Format];
   UART0_FCR = 0;                      // disable FIFO
   _Timeout  = Timeout;
} // Uart0Setup

//-----------------------------------------------------------------------------
// Tx busy
//-----------------------------------------------------------------------------

TYesNo Uart0TxBusy( void)
// Returns YES if transmitter is busy
{
   return( !(UART0_LSR & LSR_THRE));
} // Uart0TxBusy

//-----------------------------------------------------------------------------
// Tx
//-----------------------------------------------------------------------------

void Uart0TxChar( byte Char)
// Transmit <Char> byte
{
   while( !(UART0_LSR & LSR_THRE));    // wait for THR empty
   UART0_THR = Char;
} // Uart0TxChar

//-----------------------------------------------------------------------------
// Rx
//-----------------------------------------------------------------------------

TYesNo Uart0RxChar( byte *Char)
// Returns YES and received byte on <Char>,
// or NO on timeout
{
native Now;
native Status;

   Now = SysTime();
   do {
      Status = UART0_LSR;
      if( Status & LSR_RDR){
         *Char = (byte)UART0_RBR;      // get character
         if( Status & LSR_LINE_MASK){
            return( NO);               // line error
         } else {
            return( YES);              // character ok
         }
      }
   } while( (SysTime() - Now) < _Timeout);
   return( NO);
} // Uart0RxChar

//-----------------------------------------------------------------------------
// Rx Wait
//-----------------------------------------------------------------------------

TYesNo Uart0RxWait( native Timeout)
// Waits for receive up to <Timeout> miliseconds
{
native Now;
native Status;

   Now = SysTime();
   do {
      Status = UART0_LSR;
      if( Status & LSR_RDR){
         if( Status & LSR_LINE_MASK){
            Status = UART0_RBR;        // discard character
            return( NO);               // line error
         } else {
            return( YES);              // character ok
         }
      }
   } while( (SysTime() - Now) < Timeout);
   return( NO);
} // Uart0RxWait

//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void Uart0FlushChars( void)
// Skips all Rx chars up to intercharacter timeout
{
native Now;
native Char;

   Now = SysTime();
   do {
      if( UART0_LSR & LSR_RDR){
         Char = UART0_RBR;             // get character
         Now  = SysTime();             // timeout again
      }
   } while( (SysTime() - Now) < _Timeout);
} // Uart0FlushChars
