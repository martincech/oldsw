//*****************************************************************************
//
//    SoundTmr1.c  Sound creation SW PWM via Timer 1
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../inc/Sound.h"
#include "../../inc/System.h"
#include "../../inc/Cpu.h"

#define SND_IR      T1_IR              // interrupt flag register
#define SND_TCR     T1_TCR             // timer control register
#define SND_CTCR    T1_CTCR            // counter/timer control register
#define SND_PR      T1_PR              // prescaler 
#define SND_MCR     T1_MCR             // match control register
#define SND_VIC     VIC_TIMER1         // VIC channel

#define SND_PERIOD_CHANNEL 0           // match channel
#define SND_PERIOD_MR      T1_MR0      // match value register
#define SND_DUTY_CHANNEL   1           // match channel
#define SND_DUTY_MR        T1_MR1      // match value register

volatile short _snd_duration;          // citac delky

// Local functions :

static void __irq SndTimerHandler( void);
// Sound PWM timer interrupt handler

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void SndInit( void)
// Inicializace modulu
{
   SndInitPorts();
   SndOff();                                          // reset output
   CpuIrqAttach( SND_IRQ, SND_VIC, SndTimerHandler);
   // initialize timer :
   SND_PR    = 0;                                     // Count every PCLK edge
   SND_MCR  |= MCR_INTERRUPT( SND_PERIOD_CHANNEL) | 
               MCR_RESET( SND_PERIOD_CHANNEL) |       // period interrupt & reset
               MCR_INTERRUPT( SND_DUTY_CHANNEL);      // duty interrupt
   SND_TCR   = TCR_RESET;                             // Reset timer
} // SndInit

//-----------------------------------------------------------------------------
// Pipnuti
//-----------------------------------------------------------------------------

void SndBeepRun( int count, int v, short duration)
// Asynchronne pipne periodou <count>, hlasitosti <v>, trvani <duration> milisekund
{
   _snd_duration = (duration) / TIMER_PERIOD; 
   SndSoundRun( count, v);
}  // SndBeepRun

//-----------------------------------------------------------------------------
// Synchronni pipnuti
//-----------------------------------------------------------------------------

void SndSBeepRun( int count, int v, short duration) 
// Synchronne pipne periodou <count>, hlasitosti <v>, trvani <duration> milisekund
{
   SndSoundRun( count, v); 
   SysDelay( duration); 
   SndNosound(); 
   SysDelay(10);
} // SndSBeep   

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void SndNosound( void)
// Zastavi zvuk (v preruseni)
{
   SND_TCR = TCR_RESET;                // disable & reset
   SndOff();
} // SndNosound

//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------

static const word Gain[ 11] = {
//0,44,63,88,125,176,250,353,500,707,1000};    // 3dB step
0,16,25,40,63,100,158,251,398,631,1000};       // 4dB step

void SndSoundRun( int count, int volume)
// Spusti zvuk s periodou <count> a  hlasitosti <volume>
{
int width;

   if( volume == 0){
      SndNosound();                    // sound off
      return;
   }
   width   = (count * Gain[ volume]) / 2000;     // pulse width
   SND_PERIOD_MR = count;              // match register to PWM period
   SND_DUTY_MR   = width;              // match register to PWM duty
   SND_TCR       = TCR_ENABLE;         // run timer
} // SndSoundRun

//------------------------------------------------------------------------------
//   Snd Timer handler
//------------------------------------------------------------------------------

static void __irq SndTimerHandler( void)
// Sound PWM timer interrupt handler
{
   if( SND_IR & IR_MR( SND_PERIOD_CHANNEL)){
      SND_IR = IR_MR( SND_PERIOD_CHANNEL);  // clear flag
      SndOn();                              // set signal
   }
   if( SND_IR & IR_MR( SND_DUTY_CHANNEL)){
      SND_IR = IR_MR( SND_DUTY_CHANNEL);   // clear flag
      SndOff();                            // reset signal
   }
   CpuIrqDone();
} // SndTimerHandler
