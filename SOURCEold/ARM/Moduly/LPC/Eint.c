//*****************************************************************************
//
//    Eint.c      -  external interrupt template
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

/*
   Use :

static void __irq EintHandler( void);
// External interrupt handler

#define EINT_PORT       16          // EINT port number (P0.EINT_PORT)
#define EINT_RISE_EDGE  1           // EINT rise edge sensitivity
 or #define EINT_FALL_EDGE  1       // EINT fall edge sensitivity
 or #define EINT_LOW_LEVEL  1       // EINT low  level sensitivity
 or #define EINT_HIGH_LEVEL 1       // EINT high level sensitivity

#include "../Moduly/LPC/Eint.c"     // template include

*/

#if defined( __LPC210x__) || defined( __LPC213x__)

#if   EINT_PORT == 1
   // P0.1, EINT0, function 11
   #define EINT  0
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 << 2)     // PINSEL0, P0.1 dibits
   #define PINSEL_EINT   (3 << 2)     // PINSEL0 function 11 on P0.1
   #define EINT_IRQ      EINT0_IRQ    // IRQ channel
#elif EINT_PORT == 3
   // P0.3, EINT1, function 11
   #define EINT  1
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 << 6)     // PINSEL0, P0.3 dibits
   #define PINSEL_EINT   (3 << 6)     // PINSEL0 function 11 on P0.3
   #define EINT_IRQ      EINT1_IRQ    // IRQ channel
#elif EINT_PORT == 7
   // P0.7, EINT2, function 11
   #define EINT  2
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 <<14)     // PINSEL0, P0.7 dibits
   #define PINSEL_EINT   (3 <<14)     // PINSEL0 function 11 on P0.7
   #define EINT_IRQ      EINT2_IRQ    // IRQ channel
#elif EINT_PORT == 9
   // P0.9, EINT3, function 11
   #define EINT  3
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 <<18)     // PINSEL0, P0.9 dibits
   #define PINSEL_EINT   (3 <<18)     // PINSEL0 function 11 on P0.9
   #define EINT_IRQ      EINT3_IRQ    // IRQ channel
#elif EINT_PORT == 14
   // P0.14, EINT1, function 10
   #define EINT  1
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 <<28)     // PINSEL0, P0.14 dibits
   #define PINSEL_EINT   (2 <<28)     // PINSEL0 function 10 on P0.14
   #define EINT_IRQ      EINT1_IRQ    // IRQ channel
#elif EINT_PORT == 15
   // P0.15, EINT2, function 10
   #define EINT  2
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   (3 <<30)     // PINSEL0, P0.15 dibits
   #define PINSEL_EINT   (2 <<30)     // PINSEL0 function 10 on P0.15
   #define EINT_IRQ      EINT2_IRQ    // IRQ channel
#elif EINT_PORT == 16
   // P0.16, EINT0, function 01
   #define EINT  0
   #define PINSEL        PCB_PINSEL1        
   #define PINSEL_MASK   (3 << 0)     // PINSEL1, P0.16 dibits
   #define PINSEL_EINT   (1 << 0)     // PINSEL1 function 01 on P0.16
   #define EINT_IRQ      EINT0_IRQ    // IRQ channel
#elif EINT_PORT == 20
   // P0.20, EINT3, function 11
   #define EINT  3
   #define PINSEL        PCB_PINSEL1        
   #define PINSEL_MASK   (3 << 8)     // PINSEL1, P0.16 dibits
   #define PINSEL_EINT   (3 << 8)     // PINSEL1 function 11 on P0.20
   #define EINT_IRQ      EINT3_IRQ    // IRQ channel
#elif EINT_PORT == 30
   // P0.30, EINT3, function 10
   #define EINT  3
   #define PINSEL        PCB_PINSEL1        
   #define PINSEL_MASK   (3 <<28)     // PINSEL1, P0.16 dibits
   #define PINSEL_EINT   (2 <<28)     // PINSEL1 function 10 on P0.30
   #define EINT_IRQ      EINT3_IRQ    // IRQ channel
#else
   #error "Wrong EINT_PORT number"
#endif

#else
   #error "Unknown processor model"
#endif

// Povoleni vstupu :
#define EintSelectPin()    PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_EINT
// Zakaz vstupu :
#define EintDeselectPin()  PINSEL = PINSEL & ~PINSEL_MASK

// Kanal VIC :
#define EINT_VIC_CHANNEL     (VIC_EINT0 + EINT)

// Mazani priznaku :
#define EintClearFlag()      SCB_EXTINT = EXTINT_EINT( EINT)

// Rezim cinnosti :
#if   defined( EINT_RISE_EDGE)
   #define EintMode()        SCB_EXTMODE  |=  EXTMODE_EINT( EINT)    // edge sensitive
   #define EintPolarity()    SCB_EXTPOLAR |=  EXTPOLAR_EINT( EINT)   // rising edge
#elif defined( EINT_FALL_EDGE)
   #define EintMode()        SCB_EXTMODE  |=  EXTMODE_EINT( EINT)    // edge sensitive
   #define EintPolarity()    SCB_EXTPOLAR &= ~EXTPOLAR_EINT( EINT)   // falling edge
#elif defined( EINT_LOW_LEVEL)
   #define EintMode()        SCB_EXTMODE  &= ~EXTMODE_EINT( EINT)    // level sensitive
   #define EintPolarity()    SCB_EXTPOLAR &= ~EXTPOLAR_EINT( EINT)   // low level
#elif defined( EINT_HIGH_LEVEL)
   #define EintMode()        SCB_EXTMODE  &= ~EXTMODE_EINT( EINT)    // level sensitive
   #define EintPolarity()    SCB_EXTPOLAR |=  EXTPOLAR_EINT( EINT)   // high level
#else
   #error "Unknown edge/level sensitivity"
#endif

static void EintSetup( void);
// Nastaveni externiho preruseni

static void EintDisableIrq( void);
// Zakaz preruseni

static void EintEnableIrq( void);
// Povoleni preruseni

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

static void EintSetup( void)
// Nastaveni externiho preruseni
{
   EintSelectPin();                    // select pin for EINT
   EintMode();                         // set EINT mode
   EintPolarity();                     // set EINT polarity
   EintClearFlag();                    // clear pending IRQ
   CpuIrqAttach( EINT_IRQ, EINT_VIC_CHANNEL, EintHandler); // install handler
   CpuIrqDisable( EINT_VIC_CHANNEL);                       // disable IRQ
} // EintSetup

//-----------------------------------------------------------------------------
// Zakaz preruseni
//-----------------------------------------------------------------------------

static void EintDisableIrq( void)
// Zakaz preruseni
{
   CpuIrqDisable( EINT_VIC_CHANNEL);   // disable IRQ
} // EintDisableIrq

//-----------------------------------------------------------------------------
// Povoleni preruseni
//-----------------------------------------------------------------------------

static void EintEnableIrq( void)
// Povoleni preruseni
{
   EintClearFlag();                    // clear pending IRQ
   CpuIrqEnable( EINT_VIC_CHANNEL);    // enable IRQ
} // EintEnableIrq
