//*****************************************************************************
//
//    Spi.c - SPI communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/Spi.h"
#include "../../inc/System.h"

#if   defined( __LPC210x__) || defined( __LPC213x__)
   // P0.4 - SCK, P0.5 - MISO, P0.6 - MOSI
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK  ((3 << 8) | (3 << 10) | (3 << 12)) // PINSEL0, P0.4..P0.6 dibits
   #define PINSEL_SPI0  ((1 << 8) | (1 << 10) | (1 << 12)) // PINSEL0 function 01 on P0.4..P0.6
#else
   #error "Unknown processor model"
#endif

#if !defined( SPI_CPOL) || !defined( SPI_CPHA)
   #error "Undefined SPI_CPOL, SPI_CPHA"
#endif

#if SPI_CPOL == 1
   #define SPI_POLARITY SPCR_POL
#else
   #define SPI_POLARITY 0
#endif

#if SPI_CPHA == 1
   #define SPI_STROB    SPCR_CPHA
#else
   #define SPI_STROB    0
#endif

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void SpiInit( void)
// Interface initialisation
{
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_SPI0; // select pin fuction
   SPI_SET   = SPI_CS;                             // deselect
   SPI_DIR  |= SPI_CS;                             // CS out 
   SPI_SPCCR = SPI_DIVIDIER;                       // SCK delic
   SPI_SPCR  = SPCR_MSTR | SPI_POLARITY | SPI_STROB;   // level, strob
} // SpiInit

//-----------------------------------------------------------------------------
// Select
//-----------------------------------------------------------------------------

void SpiAttach( void)
// Bus setup and chipselect
{
   SPI_CLR = SPI_CS;                   // CS select
   SysUDelay( 1);
} // SpiAttach

//-----------------------------------------------------------------------------
// Deselect
//-----------------------------------------------------------------------------

void SpiRelease( void)
// Deselect and bus release
{
   SysUDelay( 1);
   SPI_SET = SPI_CS;                   // CS deselect
} // SpiRelease

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

byte SpiReadByte( void)
// Returns readed byte
{
   SPI_SPDR = 0;                       // dummy write
   while( !(SPI_SPSR & SPSR_SPIF));    // wait for send
   return( SPI_SPDR);
} // SpiReadByte

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void SpiWriteByte( byte Value)
// Write <value> byte
{
   SPI_SPDR = Value;                   // write
   while( !(SPI_SPSR & SPSR_SPIF));    // wait for send
} // SpiWriteByte
