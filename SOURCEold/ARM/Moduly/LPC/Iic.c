//*****************************************************************************
//
//    Iic.c  -  I2C controller services
//    Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "../../inc/Iic.h"

#if   defined( __LPC210x__) || defined( __LPC213x__)
   // P0.2 - SCL, P0.3 - SDA
   #define PINSEL        PCB_PINSEL0        
   #define PINSEL_MASK  ((3 << 4) | (3 << 6)) // PINSEL0, P0.2,P0.3 dibits
   #define PINSEL_I2C   ((1 << 4) | (1 << 6)) // PINSEL0 function 01 on P0.2,P0.3
#else
   #error "Unknown processor model"
#endif

#define IIC_HALF_PERIOD  5      // half clock period [us]
#define WaitForReady()          while( !(I2C_I2CONSET & I2CONSET_SI))
#define WaitForStop()           while( I2C_I2CONSET & I2CONSET_STO)
#define ClrSI()                 I2C_I2CONCLR = I2CONCLR_SIC

#define CONCLR_CLEAR_ALL   (I2CONCLR_AAC | I2CONCLR_SIC | I2CONCLR_STAC | I2CONCLR_I2ENC)

//------------------------------------------------------------------------------
//  Inicializace
//------------------------------------------------------------------------------

void IicInit( void)
// Interface initialisation
{
dword Dividier;

   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_I2C; // select pin fuction
   // clock frequency :
   Dividier     = (IIC_HALF_PERIOD * FBUS) / 1000000;
   I2C_I2SCLH   = Dividier;
   I2C_I2SCLL   = Dividier;
   I2C_I2CONCLR = CONCLR_CLEAR_ALL;    // clear status
   I2C_I2CONSET = I2CONSET_I2EN;       // enable I2C
} // IicInit

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void IicStart( void)
// Sends start sequence
{
byte Status;

   ClrSI();
   I2C_I2CONSET = I2CONSET_STA;        // send START
   WaitForReady();
   Status = I2C_I2STAT;
   I2C_I2CONCLR = I2CONCLR_STAC;       // clear START
} // IicStart

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void IicStop( void)
// Sends stop sequence
{
   I2C_I2CONSET = I2CONSET_STO;
   ClrSI();
   WaitForStop();
//   I2C_I2CONCLR = CONCLR_CLEAR_ALL & ~I2CONSET_I2EN;
} // IicStop

//------------------------------------------------------------------------------
//  Send
//------------------------------------------------------------------------------

TYesNo IicSend( byte value)
// Sends <value>, returns YES if ACK
{
   I2C_I2DAT    = value;
   ClrSI();
   WaitForReady();
   switch( I2C_I2STAT){
      case I2STAT_SLAW_ACK :
      case I2STAT_TX_MDATA_ACK :
      case I2STAT_SLAR_ACK :
         return( YES);
      default :
         return( NO);   
   }
} // IicSend

//------------------------------------------------------------------------------
//  Receive
//------------------------------------------------------------------------------

byte IicReceive( TYesNo ack)
// Returns received byte, sends confirmation <ack>.
// <ack> = YES sends ACK, NO sends NOT ACK
{
   if( ack){
      I2C_I2CONSET = I2CONSET_AA;      // set ACK bit
   } else {
      I2C_I2CONCLR = I2CONCLR_AAC;     // clear ACK bit
   }
   ClrSI();
   WaitForReady();
   switch( I2C_I2STAT){
      case I2STAT_RX_MDATA_ACK :
      case I2STAT_RX_MDATA_NAK :  
         return( I2C_I2DAT);
      default :
         return( 0);
   }
} // IicReceive

#ifdef IIC_WRITE
//------------------------------------------------------------------------------
//  Read
//------------------------------------------------------------------------------

TYesNo IicWrite( void *Buffer, int Size)
// Sends <Size> bytes from <Buffer>
{
int   i;
byte *p;

   p = (byte *)Buffer;
   for( i = 0; i < Size; i++){
/*
      if( !IicSend( *p)){
         return( NO);
      }
*/
      IicSend( *p);
      p++;
   }
   return( YES);
} // IicWrite
#endif // IIC_WRITE

#ifdef IIC_READ
//------------------------------------------------------------------------------
//  Read
//------------------------------------------------------------------------------

void IicRead( void *Buffer, int Size)
// Receives of <Size> bytes into <Buffer>
{
int  i;
byte *p;

   p = (byte *)Buffer;
   for( i = 0; i < Size; i++){
      *p = IicReceive( i != Size - 1);
      p++;
   }
} // IicRead
#endif // IIC_READ
