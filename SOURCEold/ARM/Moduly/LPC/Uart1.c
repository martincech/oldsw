//*****************************************************************************
//
//    Uart1.c - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/Uart1.h"
#include "../../inc/System.h"

#if   defined( __LPC210x__) || defined( __LPC213x__)
   // P0.8 - TxD, P0.9 - RxD
   #define PINSEL        PCB_PINSEL0
   #define PINSEL_MASK   ((3 << 16) | (3 << 18))   // PINSEL0, P0.8, P0.9 dibits
   #define PINSEL_UART   ((1 << 16) | (1 << 18))   // PINSEL0 function 01 on P0.8, P0.9

   #define GPIO_UART       (GPIO0_IODIR)
   #define GPIO_UART_MASK  ((1 << 8) | (1 << 9))
#else
   #error "Unknown processor model"
#endif

#define BAUD_DIVISOR  (FBUS / 16 / UART1_BAUD)

static word _Timeout;

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void Uart1Init( void)
// Communication initialisation
{
   PINSEL = (PINSEL & ~PINSEL_MASK) | PINSEL_UART;  // select pin fuction
} // Uart1Init

//-----------------------------------------------------------------------------
// Odpojeni
//-----------------------------------------------------------------------------

void Uart1Disconnect( void)
// Disconnect port pins
{
   PINSEL     = (PINSEL & ~PINSEL_MASK);          // back to GPIO
   GPIO_UART &= ~GPIO_UART_MASK;                  // direction to input
} // Uart1Disconnect

//-----------------------------------------------------------------------------
// Setup
//-----------------------------------------------------------------------------

// format conversion table
static const byte _LcrFormat[ _COM_FORMAT_COUNT] = {
/* COM_8BITS       */  LCR_CHAR8 | LCR_STOP1 | LCR_NO_PARITY,
/* COM_8BITS_EVEN  */  LCR_CHAR8 | LCR_STOP1 | LCR_EVEN_PARITY,
/* COM_8BITS_ODD   */  LCR_CHAR8 | LCR_STOP1 | LCR_ODD_PARITY,
/* COM_8BITS_MARK  */  LCR_CHAR8 | LCR_STOP1 | LCR_MARK_PARITY,
/* COM_8BITS_SPACE */  LCR_CHAR8 | LCR_STOP1 | LCR_SPACE_PARITY,
/* COM_7BITS       */  LCR_CHAR7 | LCR_STOP1 | LCR_NO_PARITY,
/* COM_7BITS_EVEN  */  LCR_CHAR7 | LCR_STOP1 | LCR_EVEN_PARITY,
/* COM_7BITS_ODD   */  LCR_CHAR7 | LCR_STOP1 | LCR_ODD_PARITY,
/* COM_7BITS_MARK  */  LCR_CHAR7 | LCR_STOP1 | LCR_MARK_PARITY,
/* COM_7BITS_SPACE */  LCR_CHAR7 | LCR_STOP1 | LCR_SPACE_PARITY
};

void Uart1Setup( unsigned Baud, unsigned Format, word Timeout)
// Set communication parameters
{
unsigned BaudDivisor;

   UART1_IER = 0;                      // disable all interrupts
   UART1_IIR = 0;                      // clear interrupt ID register
   UART1_LSR = 0;                      // clear line status register
   // set divisor :
   #ifndef UART1_MULVAL
      BaudDivisor = (FBUS / 16) / Baud;
   #else
      BaudDivisor = ((FBUS / 16) * UART1_MULVAL) / (UART1_MULVAL + UART1_DIVADDVAL) / Baud;
      UART1_FDR = FDR_DIVADDVAL( UART1_DIVADDVAL) | FDR_MULVAL( UART1_MULVAL);
   #endif
   UART1_LCR = LCR_DLAB;               // enable divisor latch access
   UART1_DLL = BaudDivisor & 0xFF;
   UART1_DLM = BaudDivisor >> 8;
   // set mode :
   UART1_LCR = _LcrFormat[ Format];
   UART1_FCR = 0;                      // disable FIFO
   _Timeout  = Timeout;
} // Uart1Setup

//-----------------------------------------------------------------------------
// Tx busy
//-----------------------------------------------------------------------------

TYesNo Uart1TxBusy( void)
// Returns YES if transmitter is busy
{
   return( !(UART1_LSR & LSR_THRE));
} // Uart1TxBusy

//-----------------------------------------------------------------------------
// Tx
//-----------------------------------------------------------------------------

void Uart1TxChar( byte Char)
// Transmit <Char> byte
{
   while( !(UART1_LSR & LSR_THRE));    // wait for THR empty
   UART1_THR = Char;
} // Uart1TxChar

//-----------------------------------------------------------------------------
// Rx
//-----------------------------------------------------------------------------

TYesNo Uart1RxChar( byte *Char)
// Returns YES and received byte on <Char>,
// or NO on timeout
{
native Now;
native Status;

   Now = SysTime();
   do {
      Status = UART1_LSR;
      if( Status & LSR_RDR){
         *Char = (byte)UART1_RBR;      // get character
         if( Status & LSR_LINE_MASK){
            return( NO);               // line error
         } else {
            return( YES);              // character ok
         }
      }
   } while( (SysTime() - Now) < _Timeout);
   return( NO);
} // Uart1RxChar

//-----------------------------------------------------------------------------
// Rx Wait
//-----------------------------------------------------------------------------

TYesNo Uart1RxWait( native Timeout)
// Waits for receive up to <Timeout> miliseconds
{
native Now;
native Status;

   Now = SysTime();
   do {
      Status = UART1_LSR;
      if( Status & LSR_RDR){
         if( Status & LSR_LINE_MASK){
            Status = UART1_RBR;        // discard character
            return( NO);               // line error
         } else {
            return( YES);              // character ok
         }
      }
   } while( (SysTime() - Now) < Timeout);
   return( NO);
} // Uart1RxWait

//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void Uart1FlushChars( void)
// Skips all Rx chars up to intercharacter timeout
{
native Now;
native Char;

   Now = SysTime();
   do {
      if( UART1_LSR & LSR_RDR){
         Char = UART1_RBR;             // get character
         Now  = SysTime();             // timeout again
      }
   } while( (SysTime() - Now) < _Timeout);
} // Uart1FlushChars
