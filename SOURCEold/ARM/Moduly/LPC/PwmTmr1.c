//******************************************************************************
//                                                                            
//   PwmTmr1.c      Timer 1 software PWM
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/Pwm.h"
#include "../../inc/System.h"
#include "../../inc/Cpu.h"

#define SPWM_IR      T1_IR            // interrupt flag register
#define SPWM_TCR     T1_TCR           // timer control register
#define SPWM_CTCR    T1_CTCR          // counter/timer control register
#define SPWM_PR      T1_PR            // prescaler 
#define SPWM_MCR     T1_MCR           // match control register
#define SPWM_VIC     VIC_TIMER1       // VIC channel

#define SPWM_PERIOD_CHANNEL 0         // match channel
#define SPWM_PERIOD_MR      T1_MR0    // match value register
#define SPWM_DUTY_CHANNEL   1         // match channel
#define SPWM_DUTY_MR        T1_MR1    // match value register

#define SPWM_INCREMENT ((SPWM_PERIOD * (FBUS / 1000)) / 1000 - 1)

// Local functions :

static void __irq PwmTimerHandler( void);
// PWM timer interrupt handler

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void PwmInit( void)
// Initialisation
{
   PwmInitPorts();
   PwmClr();                                           // reset output
   CpuIrqAttach( SPWM_IRQ, SPWM_VIC, PwmTimerHandler);
   // initialize timer :
   SPWM_PR    = 0;                                     // Count every PCLK edge
   SPWM_MCR  |= MCR_INTERRUPT( SPWM_PERIOD_CHANNEL) | 
                MCR_RESET( SPWM_PERIOD_CHANNEL) |      // period interrupt & reset
                MCR_INTERRUPT( SPWM_DUTY_CHANNEL);     // duty interrupt
   SPWM_PERIOD_MR = SPWM_INCREMENT;                    // match register to PWM period
   SPWM_TCR = TCR_RESET;                               // Reset timer
} // PwmInit

//------------------------------------------------------------------------------
//  Start
//------------------------------------------------------------------------------

void PwmStart( int Percent)
// Start PWM with <Percent> duty cycle
{
   if( Percent == 0){
      PwmStop();
      PwmClr();
      return;      // always off
   }
   if( Percent == 100){
      PwmStop();
      PwmSet();    // always on
      return;
   }
   SPWM_DUTY_MR   = (SPWM_INCREMENT * Percent) / 100;   // match register to PWM duty
   SPWM_TCR       = TCR_ENABLE;         // Run timer
} // PwmStart

//------------------------------------------------------------------------------
//  Stop
//------------------------------------------------------------------------------

void PwmStop( void)
// Stop PWM
{
   SPWM_TCR = TCR_RESET;               // disable & reset
   PwmClr();
} // PwmStop

//------------------------------------------------------------------------------
//   Pwm Timer handler
//------------------------------------------------------------------------------

static void __irq PwmTimerHandler( void)
// PWM timer interrupt handler
{
   if( SPWM_IR & IR_MR( SPWM_PERIOD_CHANNEL)){
      SPWM_IR = IR_MR( SPWM_PERIOD_CHANNEL); // clear flag
      PwmSet();                              // set signal
   }
   if( SPWM_IR & IR_MR( SPWM_DUTY_CHANNEL)){
      SPWM_IR = IR_MR( SPWM_DUTY_CHANNEL);  // clear flag
      PwmClr();                             // reset signal
   }
   CpuIrqDone();
} // PwmTimerHandler
