//*****************************************************************************
//
//    Lm2068e.c      LM2068E / S1D13700 graphic controller services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/Lm2068e.h"
#include "../../inc/System.h"

#define GPU_BUS_MASK      (0xFF << GPU_DATA_OFFSET)        // D0..D7 bus mask

// operace na sbernici :
#define BusIn()      GPU_DATA_DIR &= ~GPU_BUS_MASK         // bus input
#define BusOut()     GPU_DATA_DIR |=  GPU_BUS_MASK         // bus output

// S1D13700 commands ----------------------------------------------------------

//-----------------------------------------------------------------------------
// SYSTEM SET
//-----------------------------------------------------------------------------

#define GPU_CMD_SYSTEM_SET  0x40          // initialisation & basic settings
// P1
#define GPU_MASK_P1         0x10          // basic mask
#define GPU_MASK_M0         0x01          // external CG ROM
#define GPU_MASK_M1         0x02          // 64 char CG RAM/bit 6 correction on
#define GPU_MASK_M2         0x04          // 16 pixel char height
#define GPU_MASK_WS         0x08          // dual-panel display
#define GPU_MASK_IV         0x20          // No screen top-line correction
// P2
#define GPU_MASK_FX         0x07          // char width - 1
#define GPU_MASK_WF         0x80          // two-frame AC drive waveform
// P3
#define GPU_MASK_FY         0x0F          // char height - 1
// P4
#define GPU_MASK_CR         0xFF          // bytes per row - 1, 0..239
// P5
#define GPU_MASK_TCR        0xFF          // total length of row "bytes" - 1, 0..255 incl. blanking
// P6
#define GPU_MASK_LF         0xFF          // graphical rows count - 1, 0..255
// P7
#define GPU_MASK_APL        0xFF          // LSB bytes per virtual row
// P8
#define GPU_MASK_APH        0xFF          // LSB bytes per virtual row

//-----------------------------------------------------------------------------
// SLEEP IN
//-----------------------------------------------------------------------------

#define GPU_CMD_SLEEP_IN    0x53          // sleep

//-----------------------------------------------------------------------------
// DISP ON/OFF
//-----------------------------------------------------------------------------

#define GPU_CMD_DISP_OFF    0x58            // display off, cursor & planes setting
#define GPU_CMD_DISP_ON     0x59            // display on, cursor & planes setting
// P1
#define GPU_MASK_CURSOR_OFF    0x00         // cursor off
#define GPU_MASK_CURSOR_ON     0x01         // cursor on
#define GPU_MASK_CURSOR_FLASH2 0x02         // cursor flash 2Hz
#define GPU_MASK_CURSOR_FLASH1 0x03         // cursor flash 1Hz

#define GPU_MASK_SAD1_OFF      0x00         // SAD1 plane off
#define GPU_MASK_SAD1_ON       0x04         // SAD1 plane on
#define GPU_MASK_SAD1_FLASH2   0x08         // SAD1 plane flash 2Hz
#define GPU_MASK_SAD1_FLASH16  0x0C         // SAD1 plane flash 16Hz

#define GPU_MASK_SAD2_OFF      0x00         // SAD2 plane off
#define GPU_MASK_SAD2_ON       0x10         // SAD2 plane on
#define GPU_MASK_SAD2_FLASH2   0x20         // SAD2 plane flash 2Hz
#define GPU_MASK_SAD2_FLASH16  0x30         // SAD2 plane flash 16Hz

#define GPU_MASK_SAD3_OFF      0x00         // SAD3 plane off
#define GPU_MASK_SAD3_ON       0x40         // SAD3 plane on
#define GPU_MASK_SAD3_FLASH2   0x80         // SAD3 plane flash 3Hz
#define GPU_MASK_SAD3_FLASH16  0xC0         // SAD3 plane flash 16Hz

//-----------------------------------------------------------------------------
// SCROLL
//-----------------------------------------------------------------------------

#define GPU_CMD_SCROLL         0x44         // scroll area definition
// P1
#define GPU_MASK_SAD1L         0xFF         // LSB area SAD1
// P2
#define GPU_MASK_SAD1H         0xFF         // MSB area SAD1
// P3
#define GPU_MASK_SL1           0xFF         // SAD1 area rows - 1

// P4
#define GPU_MASK_SAD2L         0xFF         // LSB area SAD2
// P5
#define GPU_MASK_SAD2H         0xFF         // MSB area SAD2
// P6
#define GPU_MASK_SL2           0xFF         // SAD2 area rows - 1

// P7
#define GPU_MASK_SAD3L         0xFF         // LSB area SAD3
// P8
#define GPU_MASK_SAD3H         0xFF         // MSB area SAD3

// P9
#define GPU_MASK_SAD4L         0xFF         // LSB area SAD4
// P10
#define GPU_MASK_SAD4H         0xFF         // MSB area SAD4

//-----------------------------------------------------------------------------
// CSRFORM
//-----------------------------------------------------------------------------

#define GPU_CMD_CSRFORM        0x5D        // cursor shape and size
// P1
#define GPU_MASK_CRX           0x0F        // cursor width - 1
// P2
#define GPU_MASK_CRY           0x0F        // cursor height - 1
#define GPU_MASK_CM            0x80        // block cursor

//-----------------------------------------------------------------------------
// CSRDIR
//-----------------------------------------------------------------------------

// address increment and cursor direction
#define GPU_CMD_CSR_RIGHT      0x4C        // right, increment
#define GPU_CMD_CSR_LEFT       0x4D        // left, decrement
#define GPU_CMD_CSR_UP         0x4E        // up, AP decrement
#define GPU_CMD_CSR_DOWN       0x4F        // down, AP increment

//-----------------------------------------------------------------------------
// OVLAY
//-----------------------------------------------------------------------------

#define GPU_CMD_OVLAY          0x5B        // rizeni prekryti rovin
// P1
#define GPU_MASK_OR            0x00        // OR L1 L2 L3
#define GPU_MASK_XOR           0x01        // L1 XOR L2, OR 3
#define GPU_MASK_AND           0x02        // L1 AND L2, OR 3
#define GPU_MASK_POR           0x03        // L1 > L2 > L3

#define GPU_MASK_TEXT1         0x00        // L1 text
#define GPU_MASK_GRAPHIC1      0x04        // L1 graphic
#define GPU_MASK_TEXT2         0x00        // L2 text
#define GPU_MASK_GRAPHIC2      0x08        // L2 graphic
#define GPU_MASK_OVERLAY       0x10        // three layer composition
#define GPU_MASK_GRAPHICS      0x0C        // L1 & L2 graphic
#define GPU_MASK_GRAPHICS3     0x1C        // three graphic layers

//-----------------------------------------------------------------------------
// CGRAM ADR
//-----------------------------------------------------------------------------

#define GPU_CMD_CGRAM_ADR      0x5C        // char generator start address
// P1
#define GPU_MASK_SAGL          0xFF        // LSB address
// P2
#define GPU_MASK_SAGH          0xFF        // MSB address

//-----------------------------------------------------------------------------
// HDOT SCR
//-----------------------------------------------------------------------------

#define GPU_CMD_HDOT_SCR       0x5A        // horizontal pixel scroll
// P1
#define GPU_MASK_HDOT_PIXEL    0x07        // pixels

//-----------------------------------------------------------------------------
// GRAY SCALE
//-----------------------------------------------------------------------------

#define GPU_CMD_GRAY_SCALE     0x60        // set gray resolution
// P1
#define GPU_MASK_1BPP          0x00        // 1 bpp
#define GPU_MASK_2BPP          0x01        // 2 bpp
#define GPU_MASK_4BPP          0x02        // 4 bpp

//-----------------------------------------------------------------------------
// CSRW
//-----------------------------------------------------------------------------

#define GPU_CMD_CSRW           0x46        // write cursor address
// P1
#define GPU_MASK_CSRL          0xFF        // LSB address
// P2
#define GPU_MASK_CSRH          0xFF        // MSB address

//-----------------------------------------------------------------------------
// CSRR
//-----------------------------------------------------------------------------

#define GPU_CMD_CSRR           0x47        // read cursor address
// P1 as CSRW                              // read LSB
// P2 as CSRW                              // read MSB

//-----------------------------------------------------------------------------
// MWRITE
//-----------------------------------------------------------------------------

#define GPU_CMD_MWRITE         0x42       // write to memory
// P1..Pn data

//-----------------------------------------------------------------------------
// MREAD
//-----------------------------------------------------------------------------

#define GPU_CMD_MREAD          0x43      // read memory
// P1..Pn read data

//-----------------------------------------------------------------------------

// LM2068E display data :

#define GPU_CR_SPACE  26                                   // horizontal blanking (bytes) fps 70Hz
#define GPU_MASK_ON  (GPU_MASK_CURSOR_OFF |\
                      GPU_MASK_SAD1_ON    |\
                      GPU_MASK_SAD2_OFF   |\
                      GPU_MASK_SAD3_OFF)

#define GPU_CHAR_WIDTH  8                                  // char width
#define GPU_CHAR_HEIGHT 8                                  // char height

// video memory partitioning :
#define GPU_SAD1_START  0                                  // first plane start address
#define GPU_SAD1_HEIGHT GPU_MAX_ROW                        // first plane height
#define GPU_SAD2_START (GPU_MAX_COLUMN * GPU_MAX_ROW)      // bytes per plane
#define GPU_SAD2_HEIGHT GPU_MAX_ROW                        // second plane height
#define GPU_SAD3_START (2 * GPU_MAX_COLUMN * GPU_MAX_ROW)  // 2 * bytes per plane
#define GPU_SAD3_HEIGHT GPU_MAX_ROW                        // third plane height
#define GPU_SAD4_START  0                                  // not used

//-----------------------------------------------------------------------------

// Local functions :

static void WriteData( byte Data);
// Write data

static void WriteCommand( byte Cmd);
// Write command

static void SetData( unsigned Value);
// Write data to bus D0..D7

static void ClearRam( void);
// Clear all RAM

static void VideoRamSet( unsigned Start, int Length, byte Pattern);
// Set video ram pattern

void SetCursor( unsigned Address);
// Set RAM address

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void GpuInit( void)
// Initialisation
{
   GpuClearAll();                      // all controls to inactive, A0 = L
   GpuInitPorts();                     // port directions
   GpuClrA0();                         // data default
   BusOut();                           // default direction

   GpuClrRES();                        // activate /RES
   SysDelay( 5);                       // reset pulse min 1.3ms
   GpuSetRES();                        // deactivate /RES
   SysDelay( 5);                       // wait for ready
   GpuSelect();                        // activate /CS
   //------------- SYSTEM_SET :
   WriteCommand( GPU_CMD_SYSTEM_SET);
   WriteData( GPU_MASK_P1 |
              GPU_MASK_IV
            );         // P1 - Internal ROM, single-panel,
                       // no top-line correction, LCD mode, no additional cycles
   WriteData( GPU_MASK_WF |
             (GPU_MASK_FX  & (GPU_CHAR_WIDTH - 1))
            );         // P2 - character width
   WriteData( GPU_MASK_FY  & (GPU_CHAR_HEIGHT - 1)
            );         // P3 - character height
   WriteData( GPU_MASK_CR  & (GPU_MAX_COLUMN - 1));
                       // P4 - line width - 1
   WriteData( GPU_MASK_TCR & (GPU_MAX_COLUMN + GPU_CR_SPACE - 1));
                       // P5 - line width with blanking
   WriteData( GPU_MASK_LF  & (GPU_MAX_ROW - 1));
                       // P6 - height - 1
   WriteData( GPU_MASK_APL & GPU_MAX_COLUMN);
                       // P7 - LSB virtual width
   WriteData( GPU_MASK_APH & (GPU_MAX_COLUMN >> 8));
                       // P8 - MSB -"-
   //------------- SCROLL :
   WriteCommand( GPU_CMD_SCROLL);
   WriteData( GPU_MASK_SAD1L & GPU_SAD1_START);
                       // P1 - SAD1L
   WriteData( GPU_MASK_SAD1H & (GPU_SAD1_START >> 8));
                       // P2 - SAD1H
   WriteData( GPU_MASK_SL1   & (GPU_SAD1_HEIGHT - 1));
                       // P3 - SL1
   WriteData( GPU_MASK_SAD2L & GPU_SAD2_START);
                       // P4 - SAD2L
   WriteData( GPU_MASK_SAD2H & (GPU_SAD2_START >> 8));
                       // P5 - SAD2H
   WriteData( GPU_MASK_SL2   & (GPU_SAD2_HEIGHT - 1));
                       // P6 - SL2
   WriteData( GPU_MASK_SAD3L & GPU_SAD3_START);
                       // P7 - SAD3L
   WriteData( GPU_MASK_SAD3H & (GPU_SAD3_START >> 8));
                       // P8 - SAD3H
   WriteData( GPU_MASK_SAD4L & GPU_SAD4_START);
                       // P9 - SAD4L
   WriteData( GPU_MASK_SAD4H & (GPU_SAD4_START >> 8));
                       // P10 - SAD4H
   //------------- HDOT_SCR :
   WriteCommand( GPU_CMD_HDOT_SCR);
   WriteData( GPU_MASK_HDOT_PIXEL & 0);
                        // P1 - horizotal shift
   //------------- OVLAY :
   WriteCommand( GPU_CMD_OVLAY);
   WriteData( GPU_MASK_GRAPHIC1 | GPU_MASK_XOR);
   //------------- GRAY_SCALE :
   WriteCommand( GPU_CMD_GRAY_SCALE);
   WriteData( GPU_MASK_1BPP);
   //------------- DISP_OFF
   WriteCommand( GPU_CMD_DISP_OFF);
   //------------- CSRDIR :
   WriteCommand( GPU_CMD_CSR_RIGHT);
   ClearRam();
   //------------- DISP_ON :
   WriteCommand( GPU_CMD_DISP_ON);
   WriteData( GPU_MASK_ON);

   GpuDeselect();    // deselect /CS
#ifndef GPU_DISABLE_BUFFER
   GpuInitBuffer();
#endif   
} // GpuInit

//-----------------------------------------------------------------------------
// On
//-----------------------------------------------------------------------------

void GpuOn( void)
// Display On
{
   GpuSelect();
   WriteCommand( GPU_CMD_DISP_ON);
   WriteData( GPU_MASK_ON);
   GpuDeselect();
} // GpuOn

//-----------------------------------------------------------------------------
// Off
//-----------------------------------------------------------------------------

void GpuOff( void)
// Display Off
{
   GpuSelect();
   WriteCommand( GPU_CMD_DISP_OFF);
   GpuDeselect();
} // GpuOff

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void GpuClear( void)
// Clear display
{
   GpuSelect();
   ClearRam();
   GpuDeselect();
} // GpuClear

//-----------------------------------------------------------------------------
// Goto
//-----------------------------------------------------------------------------

void GpuGoto( int Row, int Column)
// Set coordinate to <Row>,<Column>
{
   GpuSelect();
   SetCursor( Row * GPU_MAX_COLUMN + Column);
   WriteCommand( GPU_CMD_MWRITE);
} // GpuGoto

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void GpuWrite( unsigned Pattern)
// Write <Pattern> to current coordinate (increment <Column>)
{
   WriteData( Pattern);
} // GpuWrite

//-----------------------------------------------------------------------------
// Done
//-----------------------------------------------------------------------------

void GpuDone( void)
// Write done
{
   GpuDeselect();
} // GpuDone

//*****************************************************************************

//-----------------------------------------------------------------------------
// Write data
//-----------------------------------------------------------------------------

static void WriteData( byte Data)
// Write data
{
   GpuClrWR();
   SetData( Data);
   GpuSetWR();
} // WriteData

//-----------------------------------------------------------------------------
// Write command
//-----------------------------------------------------------------------------

static void WriteCommand( byte Cmd)
// Write command
{
   GpuSetA0();
   GpuClrWR();
   SetData( Cmd);
   GpuSetWR();
   GpuClrA0();
} // WriteCommand

//-----------------------------------------------------------------------------
// Bus out
//-----------------------------------------------------------------------------

static void SetData( unsigned Value)
// Write data to bus D0..D7
{
unsigned Mask;

   Mask = Value << GPU_DATA_OFFSET;
   GPU_DATA_CLR = GPU_BUS_MASK;
   GPU_DATA_SET = Mask & GPU_BUS_MASK;
} // SetData

//-----------------------------------------------------------------------------
// Clear RAM
//-----------------------------------------------------------------------------

static void ClearRam( void)
// Clear all RAM
{
   VideoRamSet( GPU_SAD1_START, GPU_MAX_COLUMN * GPU_MAX_ROW, 0);
//   VideoRamSet( GPU_SAD2_START, GPU_MAX_COLUMN * GPU_MAX_ROW, 0);
//   VideoRamSet( GPU_SAD3_START, GPU_MAX_COLUMN * GPU_MAX_ROW, 0);
} // ClearRam

//-----------------------------------------------------------------------------
// Set RAM
//-----------------------------------------------------------------------------

static void VideoRamSet( unsigned Start, int Length, byte Pattern)
// Set video RAM pattern
{
int i;

   SetCursor( Start);
   //------- MWRITE
   WriteCommand( GPU_CMD_MWRITE);
   for( i = 0; i < Length; i++){
      WriteData( Pattern);
   }
} // VideoRamSet

//-----------------------------------------------------------------------------
// Set cursor
//-----------------------------------------------------------------------------

void SetCursor( unsigned Address)
// Set RAM address
{
   WriteCommand( GPU_CMD_CSRW);
   WriteData( GPU_MASK_CSRL & Address);
   WriteData( GPU_MASK_CSRH & (Address >> 8));
} // SetCursor
