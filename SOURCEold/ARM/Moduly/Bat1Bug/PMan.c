//******************************************************************************
//
//  PMan.c         Power management utilities
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "PMan.h"
#include "../Inc/System.h"   // SysDelay
#include "../Inc/Cpu.h"      // CPU
#include "../Inc/Kbd.h"      // Keyboard
#include "../Inc/Eep.h"      // EEPROM
#include "../Inc/St7259.h"   // Display
#include "../Inc/Graphic.h"  // print
#include "../Inc/conio.h"    // print
#include "PAccu.h"           // Accumulator maitenance data
#include "Accu.h"            // Accumulator maitenance

#define PMAN_SHUTDOWN       0x01       // shutdown requested
#define PMAN_ON             0x02       // power on requested by key press
#define PMAN_USB            0x04       // USB communication
#define PMAN_CHARGER        0x08       // charger connected
#define PMAN_ACCU_WARNING   0x10       // accu voltage warning
#define PMAN_ACCU_FAIL      0x20       // accu undervoltage
#define PMAN_ACCU_CHARGED   0x40       // accu charged
#define PMAN_BOOT           0x80       // boot key pressed

#define PMAN_MAX_FAILS      5          // number of accu fails

static byte PManFlag;                  // PMan flags
static byte AccuFailCount;             // accu failure counter
static word AccuVoltage;               // actual accu voltage [LSB]
static word AccuVoltageSmooth;         // filtered accu voltage [LSB]

//------------------------------------------------------------------------------
// Initialize
//------------------------------------------------------------------------------

void PManInit( void)
// Initialize power management
{
int Key;

   PManInitPorts();
   PManClrENNAB();                     // enable charger
   EepInit();                          // initialize EEPROM
   AccuInit();                         // accumulator maitenance
   PAccuLoad();                        // load accumulator maitenance data
   PManFlag          = 0;              // initial flag
   AccuFailCount     = 0;              // prepare fail testing
   Key = KbdPowerUpKey();              // read key
   DisableInts();                      // read key enables interrupt
   if( Key == K_ESC){
      PManFlag |= PMAN_ON;             // key ON
   } else if( Key == K_BOOT || Key == PMAN_BOOT_KEY){
      PManFlag |= PMAN_BOOT;           // boot key
      return;                          // skip voltage checking
   }
   // measure & test low voltage :
   AccuVoltage = AccuRawVoltage();
   GTextAt( 0, G_HEIGHT / 2);
   cprintf( "Voltage: %d\n", AccuVoltage);
   GFlush();
   SysDelay( 3000);
   if( AccuVoltage < PAccuGetUMin()){
      // low accu voltage
      cputs( "LOW !\n");
      GFlush();
      if( !PManGetPPR()){
         // accu without charger
         PManClrVYP();                 // power off
         cputs( "NO CHARGER\n");
         GFlush();
         WatchDog();                   // refresh
         forever;                      // sorry, accu empty
      }
   }
   AccuVoltageSmooth = AccuVoltage;    // IIR filter initial value
   PManExecute();                      // initial status
} // PManInit

//------------------------------------------------------------------------------
// Execute
//------------------------------------------------------------------------------

void PManExecute( void)
// Main executive, call periodically
{
unsigned ChargedVoltage;

   ChargedVoltage = PAccuRawVoltage( ACCU_CHARGED_VOLTAGE);
   AccuVoltage = AccuRawVoltage();     // measure accumulator
   if( AccuVoltage < PAccuGetUMin()){
      // voltage under secure level
      if( PManGetPPR()){
         // charger connected don't set fail
         PManFlag |= PMAN_ACCU_WARNING;
         AccuFailCount = 0;            // reset failure counting
      } else {
         // accu only, check fail
         AccuFailCount++;
         if( AccuFailCount >= PMAN_MAX_FAILS){
            AccuFailCount = PMAN_MAX_FAILS;
            PManFlag |= PMAN_ACCU_FAIL;
         }
      }
   } else if( AccuVoltage < PAccuGetUWarn()){
      // voltage under warning level
      PManFlag |= PMAN_ACCU_WARNING;
      AccuFailCount = 0;               // reset failure counting
   } else {
      // voltage in secure range
      PManFlag &= ~(PMAN_ACCU_FAIL | PMAN_ACCU_WARNING);
      AccuFailCount = 0;               // reset failure counting
   }
   // test for external power :
   if( PManGetPPR()){
      // test for USB power :
      if( PManGetUSBON()){
         // powered by USB
         PManFlag |=  PMAN_USB;
         PManFlag &= ~PMAN_CHARGER;
      } else {
         // powered by charger
         PManFlag |=  PMAN_CHARGER;
         PManFlag &= ~PMAN_USB;
      }
      // test for charging done :
      PManFlag &= ~PMAN_ACCU_CHARGED;            // clear charged flag
      if( AccuVoltage > ChargedVoltage){
          // voltage over charged treshold
         if( PManGetCHG()){
            PManFlag |=  PMAN_ACCU_CHARGED;      // set charged flag
         }
      } // voltage under charged treshold
   } else {
      // no external power
      PManFlag &= ~PMAN_CHARGER;
      PManFlag &= ~PMAN_USB;
      PManFlag &= ~PMAN_ACCU_CHARGED;
      if( !(PManFlag & PMAN_ON)){
         // charger without Power On key
         PManFlag |= PMAN_SHUTDOWN;    // shutdown request
      }
   }
} // PManExecute

//------------------------------------------------------------------------------
// Execute
//------------------------------------------------------------------------------

void PManExecuteSlow( void)
// Main executive, call periodically by 1s
{
dword Tmp;

   // calculate IIR fitering :
   Tmp   = 3 * (dword)AccuVoltageSmooth;
   Tmp  += AccuVoltage;
   Tmp >>= 2;
   AccuVoltageSmooth = (word)Tmp;
} // PManExecuteSlow

//------------------------------------------------------------------------------
// Set Shutdown
//------------------------------------------------------------------------------

void PManSetShutdown( void)
// Prepare power off mode
{
   PManFlag |=  PMAN_SHUTDOWN;
} // PManSetShutdown

//------------------------------------------------------------------------------
// Clr Shutdown
//------------------------------------------------------------------------------

void PManClrShutdown( void)
// Leave power off mode
{
   PManFlag &= ~PMAN_SHUTDOWN;
} // PManClrSuutdown

//------------------------------------------------------------------------------
// Set Power On
//------------------------------------------------------------------------------

void PManSetPowerOn( void)
// Power on in charge/USB
{
   PManFlag |=  PMAN_ON;
} // PManSetPowerOn

//------------------------------------------------------------------------------
// Power Off
//------------------------------------------------------------------------------

void PManSetPowerOff( void)
// Power off request
{
   PManFlag &= ~PMAN_ON;
} // PManSetPowerOff

//------------------------------------------------------------------------------
// Power Off
//------------------------------------------------------------------------------

void PManPowerOff( void)
// Switch power off
{
   GpuShutdown();                      // display off
   EepReadByte( 0);                    // wait for EEPROM ready
   DisableInts();                      // stop interrupts
   PManClrVYP();                       // power off
   forever {
      SysDelay( 100);                  // delay & watchdog
   }
} // PManPowerOff

//------------------------------------------------------------------------------
// Boot
//------------------------------------------------------------------------------

TYesNo PManBoot( void)
// Returns YES on boot key
{
   return( (PManFlag & PMAN_BOOT) != 0);
} // PManBoot

//------------------------------------------------------------------------------
// Power On
//------------------------------------------------------------------------------

TYesNo PManPowerOn( void)
// Returns YES on key power on
{
   return( (PManFlag & PMAN_ON) != 0);
} // PManPowerOn

//------------------------------------------------------------------------------
// Shutdown
//------------------------------------------------------------------------------

TYesNo PManShutdown( void)
// Prepare power off mode
{
   return( (PManFlag & PMAN_SHUTDOWN) != 0);
} // PManShutdown

//------------------------------------------------------------------------------
// Accu warning
//------------------------------------------------------------------------------

TYesNo PManAccuWarning( void)
// Returns YES for low accu voltage
{
   return( (PManFlag & PMAN_ACCU_WARNING) != 0);
} // PManAccuWarning

//------------------------------------------------------------------------------
// Accu Off
//------------------------------------------------------------------------------

TYesNo PManAccuFail( void)
// Returns YES for discharged accu
{
   return( (PManFlag & PMAN_ACCU_FAIL) != 0);
} // PManAccuFail

//------------------------------------------------------------------------------
// USB
//------------------------------------------------------------------------------

TYesNo PManUsb( void)
// Returns YES for active USB
{
   return( (PManFlag & PMAN_USB) != 0);
} // PManUsb

//------------------------------------------------------------------------------
// Charger
//------------------------------------------------------------------------------

TYesNo PManCharger( void)
// Returns YES for connected charger
{
   return( (PManFlag & PMAN_CHARGER) != 0);
} // PManCharger

//------------------------------------------------------------------------------
// Charged
//------------------------------------------------------------------------------

TYesNo PManCharged( void)
// Returns YES for charging done
{
   return( (PManFlag & PMAN_ACCU_CHARGED) != 0);
} // PManCharged

//------------------------------------------------------------------------------
// Charge Only
//------------------------------------------------------------------------------

TYesNo PManChargeOnly( void)
// Returns YES for USB/charger (without power on button)
{
   if( PManFlag & PMAN_ON){
      return( NO);                     // power on by key
   }
   if( !(PManFlag & PMAN_USB) && !(PManFlag & PMAN_CHARGER)){
      // no external power
      PManPowerOff();                  // no return, key power off
   }
   return( YES);                       // charging
} // PManChargeOnly

//------------------------------------------------------------------------------
// Capacity
//------------------------------------------------------------------------------

int PManAccuCapacity( void)
// Returns accu capacity [%]
{
   return( PAccuCapacity( AccuVoltageSmooth));
} // PManCapacity

//------------------------------------------------------------------------------
// Voltage
//------------------------------------------------------------------------------

int PManAccuVoltage( void)
// Returns accu voltage [mV]
{
   return( PAccuVoltage( AccuVoltageSmooth));
} // PManAccuVoltage
