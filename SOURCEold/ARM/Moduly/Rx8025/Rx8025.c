//*****************************************************************************
//
//    Rx8025.c  -  Real time clock RX-8025 SA/NB services
//    Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "../../inc/Rx8025.h"
#include "../../inc/Iic.h"
#include "../../inc/System.h"   // SysUDelay

// Adresa I2C :

#define RTC_BASE_ADDRESS 0x64                         // zaklad adresy RTC
#define RTC_READ         (RTC_BASE_ADDRESS | 0x01)    // cteni z pameti
#define RTC_WRITE         RTC_BASE_ADDRESS            // zapis do pameti

//-----------------------------------------------------------------------------
// Adresy registru
//-----------------------------------------------------------------------------

// vysilaji se v hornim nibble :

#define RTC_SEC_ADDRESS           0x00       // sekundy v BCD kodovani
#define RTC_MIN_ADDRESS           0x10       // minuty v BCD kodovani
#define RTC_HOUR_ADDRESS          0x20       // hodiny v BCD kodovani
#define RTC_WDAY_ADDRESS          0x30       // den v tydnu
#define RTC_DAY_ADDRESS           0x40       // den v mesici
#define RTC_MONTH_ADDRESS         0x50       // mesic
#define RTC_YEAR_ADDRESS          0x60       // rok

#define RTC_OFFSET_ADDRESS        0x70       // digital offset - XTAL correction
#define RTC_ALARMW_MIN_ADDRESS    0x80       // Alarm W - minute
#define RTC_ALARMW_HOUR_ADDRESS   0x90       // Alarm W - hour
#define RTC_ALARMW_WDAY_ADDRESS   0xA0       // Alarm W - weekday
#define RTC_ALARMD_MIN_ADDRESS    0xB0       // Alarm D - minute
#define RTC_ALARMD_HOUR_ADDRESS   0xC0       // Alarm D - hour

#define RTC_CONTROL1_ADDRESS      0xE0       // Control 1
#define RTC_CONTROL2_ADDRESS      0xF0       // Control 2

#define RTC_SIMPLE_READ           0x04       // zjednodusene cteni registru (OR k adrese)

//-----------------------------------------------------------------------------
// Masky registru
//-----------------------------------------------------------------------------

// Registr CONTROL 1 :

#define RTC_INTA_OFF              0x00       // CT0-2 /INTA = HiZ
#define RTC_INTA_LOW              0x01       // CT0-2 /INTA = Fixed Low
#define RTC_INTA_PULSE_2HZ        0x02       // CT0-2 /INTA = Pulse mode 2 Hz
#define RTC_INTA_PULSE_1HZ        0x03       // CT0-2 /INTA = Pulse mode 1 Hz
#define RTC_INTA_LEVEL_SEC        0x04       // CT0-2 /INTA = Level mode 1 sec
#define RTC_INTA_LEVEL_MIN        0x05       // CT0-2 /INTA = Level mode 1 min
#define RTC_INTA_LEVEL_HOUR       0x06       // CT0-2 /INTA = Level mode 1 hour
#define RTC_INTA_LEVEL_MONTH      0x07       // CT0-2 /INTA = Level mode 1 month
#define RTC_CLEN2                 0x10       // /CLEN2 FOUT control
#define RTC_24                    0x20       // /12,24 24-hour clock
#define RTC_DALE                  0x40       // DALE alarm D enable
#define RTC_WALE                  0x80       // WALE alarm W enable

// Registr CONTROL 2 :

#define RTC_DAFG                  0x01       // DAFG alarm D flag
#define RTC_WAFG                  0x02       // WAFG alarm W flag
#define RTC_CTFG                  0x04       // CTFG periodic interrupt flag
#define RTC_CLEN1                 0x08       // /CLEN1 FOUT control
#define RTC_PON                   0x10       // PON  1 - power on reset detected
#define RTC_XST                   0x20       // /XST 0 - oscillator stop detected
#define RTC_VDET                  0x40       // VDET 1 - power drop detected
#define RTC_VDSL_13               0x80       // VDSL set power drop treshold 1.3V (0 - 2.1V)

// Implicitni hodnoty :

#define RTC_CONTROL1_DEFAULT     (RTC_INTA_OFF | RTC_24)
#define RTC_CONTROL2_DEFAULT     (RTC_XST)

// Lokalni funkce :

static void RtcWriteRegister( byte address, byte value);
// Zapise <value> na <address> do registru

static byte RtcReadRegister( byte address);
// Precte obsah registru <address> a vrati jej

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

TYesNo RtcInit( void)
// Inicializuje sbernici a RTC
{
TYesNo Ack;

   IicInit();
   IicStart();
   Ack = IicSend( RTC_WRITE);
   if( !Ack){
      IicStop();
      return( NO);                     // RTC neodpovida
   }
   // zapis do ridicich registru :
   IicSend( RTC_CONTROL1_ADDRESS);
   IicSend( RTC_CONTROL1_DEFAULT);     // na adresu CONTROL1
   IicSend( RTC_CONTROL2_DEFAULT);     // na adresu CONTROL2
   IicStop();
   SysUDelay( 65);                     // prodleva mezi stop a nasledujicim start
   return( YES);
} // RtcInit

//-----------------------------------------------------------------------------
// Cteni s
//-----------------------------------------------------------------------------

byte RtcSec( void)
// Vrati sekundy hodin v BCD
{
   return( RtcReadRegister( RTC_SEC_ADDRESS));
} // RtcSec

//-----------------------------------------------------------------------------
// Cteni min
//-----------------------------------------------------------------------------

byte RtcMin( void)
// Vrati minuty hodin v BCD
{
   return( RtcReadRegister( RTC_MIN_ADDRESS));
} // RtcSec

//-----------------------------------------------------------------------------
// Cteni h
//-----------------------------------------------------------------------------

byte RtcHour( void)
// Vrati hodiny hodin v BCD
{
   return( RtcReadRegister( RTC_HOUR_ADDRESS));
} // RtcHour

//-----------------------------------------------------------------------------
// Cteni dne
//-----------------------------------------------------------------------------

byte RtcDay( void)
// Vrati den v mesici v BCD
{
   return( RtcReadRegister( RTC_DAY_ADDRESS));
} // RtcDay

//-----------------------------------------------------------------------------
// Cteni mesice
//-----------------------------------------------------------------------------

byte RtcMonth( void)
// Vrati mesic hodin v BCD
{
   return( RtcReadRegister( RTC_MONTH_ADDRESS));
} // RtcMonth

//-----------------------------------------------------------------------------
// Cteni roku
//-----------------------------------------------------------------------------

word RtcYear( void)
// Vrati rok hodin v BCD
{
   return( RtcReadRegister( RTC_YEAR_ADDRESS) | 0x2000);
} // RtcYear

//-----------------------------------------------------------------------------
// Cteni dne v tydnu
//-----------------------------------------------------------------------------

byte RtcWday( void)
// Vrati den v tydnu hodin ve forme vyctu
{
byte Wday;

   Wday = RtcReadRegister( RTC_WDAY_ADDRESS);
   if( !Wday){
      return( DT_SUNDAY);
   }
   return( Wday - 1);
} // RtcWDay

//-----------------------------------------------------------------------------
// Zapis s
//-----------------------------------------------------------------------------

void RtcSetSec( byte Bcd)
// Nastavi sekundy hodin, <Bcd> je hodnota v BCD
{
#ifdef RTC_RANGE_CHECK
   if( Bcd > 0x59){
      Bcd = 0x59;
   }
#endif
   RtcWriteRegister( RTC_SEC_ADDRESS, Bcd);
} // RtcSetSec

//-----------------------------------------------------------------------------
// Zapis min
//-----------------------------------------------------------------------------

void RtcSetMin( byte Bcd)
// Nastavi minuty hodin, <Bcd> je hodnota v BCD
{
#ifdef RTC_RANGE_CHECK
   if( Bcd > 0x59){
      Bcd = 0x59;
   }
#endif
   RtcWriteRegister( RTC_MIN_ADDRESS, Bcd);
} // RtcSetMin

//-----------------------------------------------------------------------------
// Zapis h
//-----------------------------------------------------------------------------

void RtcSetHour( byte Bcd)
// Nastavi sekundy hodin, <Bcd> je hodnota v BCD
{
#ifdef RTC_RANGE_CHECK
   if( Bcd > 0x23){
      Bcd = 0x23;
   }
#endif
   RtcWriteRegister( RTC_HOUR_ADDRESS, Bcd);
} // RtcSetHour


//-----------------------------------------------------------------------------
// Zapis dne
//-----------------------------------------------------------------------------

void RtcSetDay( byte Bcd)
// Nastavi den v mesici hodin, <Bcd> je hodnota v BCD
{
#ifdef RTC_RANGE_CHECK
   if( Bcd < 0x01){
      Bcd = 0x01;
   }
   if( Bcd > 0x31){
      Bcd = 0x31;
   }
#endif
   RtcWriteRegister( RTC_DAY_ADDRESS, Bcd);
} // RtcSetDay

//-----------------------------------------------------------------------------
// Zapis mesice
//-----------------------------------------------------------------------------

void RtcSetMonth( byte Bcd)
// Nastavi mesic hodin, <Bcd> je hodnota v BCD
{
#ifdef RTC_RANGE_CHECK
   if( Bcd < 0x01){
      Bcd = 0x01;
   }
   if( Bcd > 0x12){
      Bcd = 0x12;
   }
#endif
   RtcWriteRegister( RTC_MONTH_ADDRESS, Bcd);
} // RtcSetMonth

//-----------------------------------------------------------------------------
// Zapis roku
//-----------------------------------------------------------------------------

void RtcSetYear( word Bcd)
// Nastavi rok hodin, <Bcd> je hodnota v BCD
{
#ifdef RTC_RANGE_CHECK
   if( Bcd < 0x2000){
      Bcd = 0x2000;
   }
#endif
   RtcWriteRegister( RTC_YEAR_ADDRESS, Bcd & 0xFF);     // dolni cast
} // RtcSetYear

//-----------------------------------------------------------------------------
// Zapis dne v tydnu
//-----------------------------------------------------------------------------

void RtcSetWday( byte Dow)
// Nastavi den v tydnu hodin, <Dow> je den v tydnu
{
   if( Dow >= DT_SUNDAY){
      Dow = 0;     // nedele = 0
   } else {
      Dow++;       // ostatni 1..6
   }
   RtcWriteRegister( RTC_WDAY_ADDRESS, Dow);
} // RtcSetWday

#ifdef RTC_USE_ALARM
//-----------------------------------------------------------------------------
// Nastaveni alarmu
//-----------------------------------------------------------------------------

void RtcSetAlarm( byte HourBcd, byte MinuteBcd)
// Nastavi alarm na cas Hour:Minute, cas se zadava v bcd
{
#ifdef RTC_RANGE_CHECK
   if( HourBcd > 0x23){
      HourBcd = 0x23;
   }
   if( MinuteBcd > 0x59){
      MinuteBcd = 0x59;
   }
#endif
   RtcWriteRegister( RTC_ALARMD_MIN_ADDRESS,  MinuteBcd);
   RtcWriteRegister( RTC_ALARMD_HOUR_ADDRESS, HourBcd);
   RtcWriteRegister( RTC_CONTROL1_ADDRESS, RTC_CONTROL1_DEFAULT | RTC_DALE); // povoleni
   RtcWriteRegister( RTC_CONTROL2_ADDRESS, RTC_CONTROL2_DEFAULT);            // mazani flagu
} // RtcSetAlarm

//-----------------------------------------------------------------------------
// Kontrola alarmu
//-----------------------------------------------------------------------------

TYesNo RtcCheckAlarm( void)
// Vrati YES, je-li alarm (zaroven shodi alarm)
{
   if( RtcReadRegister( RTC_CONTROL2_ADDRESS) & RTC_DAFG){
      RtcWriteRegister( RTC_CONTROL2_ADDRESS, RTC_CONTROL2_DEFAULT);         // mazani flagu
      return( YES);
   }
   return( NO);
} // RtcCheckAlarm

//-----------------------------------------------------------------------------
// Vypnuti alarmu
//-----------------------------------------------------------------------------

void RtcAlarmOff( void)
// Vypne alarm
{
   RtcWriteRegister( RTC_CONTROL1_ADDRESS, RTC_CONTROL1_DEFAULT);            // zakaz
   RtcWriteRegister( RTC_CONTROL2_ADDRESS, RTC_CONTROL2_DEFAULT);            // mazani flagu
} // RtcAlarmOff

#endif  // RTC_USE_ALARM

//-----------------------------------------------------------------------------
// Zapis do registru
//-----------------------------------------------------------------------------

static void RtcWriteRegister( byte Address, byte Value)
// Zapise <value> na <address> do registru
{
   IicStart();
   IicSend( RTC_WRITE);
   IicSend( Address);
   IicSend( Value);
   IicStop();
   SysUDelay( 65);                     // prodleva mezi stop a nasledujicim start
} // RtcWriteRegister

//-----------------------------------------------------------------------------
// Cteni registru
//-----------------------------------------------------------------------------

static byte RtcReadRegister( byte Address)
// Precte obsah registru <Address> a vrati jej
{
byte Value;

   IicStart();
   IicSend( RTC_WRITE);
   IicSend( Address);
   // Read data :
   IicStart();
   IicSend( RTC_READ);
   Value = IicReceive( NO);            // precti jeden byte a posli NAK
   IicStop();
   SysUDelay( 65);                     // prodleva mezi stop a nasledujicim start
   return( Value);
} // RtcReadRegister
