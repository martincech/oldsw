//*****************************************************************************
//
//    GpuFlush90.c  Flush display with rotation 90�
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../Inc/El12864.h"

#define GetRow( y)       ((y) >> 3)
#define GetCol( x)       ((x) >> 3)
#define ToX( c)          ((c) << 3)

static byte RectangleStrip( byte *Rectangle, byte y);
// Get rectangle strip from <y> coordinate

//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void GpuFlush( void)
// Zkopiruje aktivni oblast do radice
{
int r, c, y;
int minC, maxC;
int minR, maxR;

   minR = GetRow( GBuffer.MinY);
   maxR = GetRow( GBuffer.MaxY);
   minC = GetCol( GBuffer.MinX);
   maxC = GetCol( GBuffer.MaxX);
   // vertical block from left to right :
   for( c = minC; c <= maxC; c++){
      GpuGoto( GPU_MAX_ROW - 1 - c, 0);   // GPU coordinate from bottom to top
      // vertical squares from top to bottom :
      for( r = minR; r <= maxR; r++){
         // vertical square
         for( y = 0; y < 8; y++){
            // square strip from top to bottom
            GpuWrite( RectangleStrip( &GBuffer.Buffer[r][ ToX(c)], y));
         }
      }
   }
   GpuDone();
   GpuInitBuffer();
} // GpuFlush

//-----------------------------------------------------------------------------
// Strip
//-----------------------------------------------------------------------------

static byte RectangleStrip( byte *Rectangle, byte y)
// Get rectangle strip from <y> coordinate
{
byte DstMask;   // destination mask
byte YMask;     // source mask
byte Result;
int  x;

   YMask   = 1 << y;
   DstMask = 0x80;                     // GPU destination bit from MSB to LSB
   Result  = 0;
   for( x = 0; x < 8; x++){
      if( *Rectangle & YMask){
         Result |= DstMask;
      }
      DstMask >>= 1;
      Rectangle++;
   }
   return( Result);
} // RectangleStrip
