//*****************************************************************************
//
//    Console.c - EL12864 graphic display simple console
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../inc/conio.h"
#include "../../inc/El12864.h"

#define FONT_DISABLE_DESCRIPTOR 1      // zakaz generovani deskriptoru fontu

#include "../Font/Font8x8.c"

// Lokalni promenne :

static word Row;
static word Col;

//-----------------------------------------------------------------------------
// Kurzor
//-----------------------------------------------------------------------------

void ccursor( bool on)
// Je-li <on>=NO zhasne kurzor, jinak rozsviti
{
} // ccursor

//-----------------------------------------------------------------------------
// Mazani
//-----------------------------------------------------------------------------

void cclear( void)
// Smaze display
{
   GpuClear();
   Row = 0;
   Col = 0;
} // cclear

//-----------------------------------------------------------------------------
// Pozice
//-----------------------------------------------------------------------------

void cgoto( native r, native c)
// Presune kurzor na zadanou textovou pozici <r> radek <c> sloupec
{
   Row = r;
   Col = c;
} // cgoto

//-----------------------------------------------------------------------------
// Mazani radku
//-----------------------------------------------------------------------------

void cclreol( void)
// smaze az do konce radku, nemeni pozici kurzoru
{
native x, i;

   x = Col * 8;
   GpuGoto( Row, x);
   for( i = x; i < GPU_MAX_COLUMN; i++){
      GpuWrite( 0);
   }
   GpuDone();
} // cclreol

//-----------------------------------------------------------------------------
// Znak
//-----------------------------------------------------------------------------

int putchar( int c)
// standardni zapis znaku
{
native Index;
native i;

   if( c == '\n'){
      // newline character
      Row++;
      Col = 0;
      return( c);
   }
   Index = c * 8;               // zacatek znaku
   // kopie bitmapy :
   GpuGoto( Row, Col * 8);
   for( i = 0; i < 8; i++){
      GpuWrite( Font8x8Array[ Index++]);
   }
   GpuDone();
   Col++;
   return( c);
} // putchar
