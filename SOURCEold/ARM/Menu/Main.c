//******************************************************************************
//                                                                            
//  Main.c         Menu demo
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/St7259.h"    // Display (contrast)
#include "../inc/Sound.h"     // Sound
#include "Fonts.h"            // seznam fontu
#include "../inc/conio.h"     // Display

#include <string.h>
#include "../Inc/Wgt/DLabel.h"  // display enum
#include "../Inc/Wgt/DEnter.h"       // input value
#include "../Inc/Wgt/DEnterList.h"   // input enum
#include "../Inc/Wgt/DEvent.h"
#include "Wgt/DMenu.h"
#include "Wgt/DEdit.h"
#include "Wgt/DLayout.h"

#include "Backlight.h"        // Backlight
#include "Beep.h"             // Beep
#include "Str.h"

// start odpocitavani 1 s :

#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD
#define SetTimerFlash()   CounterFlash   = (TIMER_FLASH1 + TIMER_FLASH2) / TIMER_PERIOD

#define TIMER_FLAG_FLASH1  0x02
#define TIMER_FLAG_FLASH2  0x04

#define SetFlash1()  Flags |=  TIMER_FLAG_FLASH1
#define SetFlash2()  Flags |=  TIMER_FLAG_FLASH2

#define ClrFlash1()  Flags &= ~TIMER_FLAG_FLASH1
#define ClrFlash2()  Flags &= ~TIMER_FLAG_FLASH2

#define IsFlash1()  (Flags & TIMER_FLAG_FLASH1)
#define IsFlash2()  (Flags & TIMER_FLAG_FLASH2)

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s
volatile word   CounterFlash;            // flash countdown
volatile byte   Flags;                   // timer flags

//------------------------------------------------------------------------------

#define BACKLIGHT_MAX  20
#define CONTRAST_MIN   GPU_EC_BASE - 32
#define CONTRAST_MAX   GPU_EC_BASE + 32

static int CurrentContrast  = GPU_EC_BASE;

#include "Bat1.c"        // logo bitmap

//------------------------------------------------------------------------------

// Menu context :
#define TEXT_VALUE_SIZE  6
#define SPIN_VALUE_MAX   99

static int  WeightValue;
static int  NumberValue;
static char TextValue[ TEXT_VALUE_SIZE + 1];
static int  EnumValue;
static int  ListValue;
static int  SpinValue;

typedef enum {
   E_KOLO,
   E_ROLO,
   E_HOVNO,
   _E_COUNT
} TDemoEnum;

DefList( DemoList)
   "Kolo",
   "Rolo",
   "Hovno",
EndList()

// Local functions :

static void MenuDemo( void);
// Demo menu

static void DemoParameters( int Index, int y, void *UserData);
// Menu parameters

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   CpuInit();
   // power management :
   PManInitPorts();
   PManClrENNAB();                     // enable charger
   PManSetVYP();                       // hold power

   // basic peripherals init :
   KbdInit();
   SndInit();
   BacklightInit();
   GInit();

   // system run :   

   Flags = 0;
   SetTimerFlash();

   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   // hlavni smycka :
   BacklightSet( BACKLIGHT_MAX);
   BacklightOn();
   CurrentContrast  = GPU_EC_BASE;
   GpuContrast( CurrentContrast);
   GSetFont( TAHOMA16);

   // init menu context :
   WeightValue = 666;
   NumberValue = 333;
   strcpy( TextValue, "TEXT");
   EnumValue   = 0;
   ListValue   = 0;
   SpinValue   = SPIN_VALUE_MAX;

   GBitmap( 0, 0, &Bat1);  
   GFlush();
   forever {
      switch( SysWaitEvent()){
         case K_DOWN :
            BeepKey();
            MenuDemo();
            DEventDiscard();       // stop timeout chain
            break;

         case K_RIGHT :
            if( CurrentContrast == CONTRAST_MAX){
               continue;
            }
            CurrentContrast++;
            GpuContrast( CurrentContrast);
            continue;

         case K_LEFT :
            if( CurrentContrast == CONTRAST_MIN){
               continue;
            }
            CurrentContrast--;
            GpuContrast( CurrentContrast);
            continue;

         case K_ESC :
            CurrentContrast  = GPU_EC_BASE;
            GpuContrast( CurrentContrast);
            continue;

         case K_ESC | K_REPEAT :
            // power off
            GpuShutdown();                      // display off
            DisableInts();                      // stop interrupts
            PManClrVYP();                       // power off
            forever {
               SysDelay( 100);                  // watchdog space
               WatchDog();                      // wait for power failure
            }
            break;

         default :
            continue;
      }
      GBitmap( 0, 0, &Bat1);  
      GFlush();
   }
} // main

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

static DefMenu( DemoMenu)
   "Enter weight",
   "Enter text",
   "Enter enum",
   "Enter list",
   "Enter spin",
   "Enter number",
   "Item 7",
   "Item 8",
   "Enter any",
EndMenu()

typedef enum {
   DM_ENTER_WEIGHT,
   DM_ENTER_TEXT,
   DM_ENTER_ENUM,
   DM_ENTER_LIST,
   DM_ENTER_SPIN,
   DM_ENTER_NUMBER,
   DM_ITEM7,
   DM_ITEM8,
   DM_ENTER_ANY,
} TDemoMenuEnum;

#define DM_SUB_MASK  ((1 << DM_ITEM6) | (1 << DM_ITEM7) | (1 << DM_ITEM8))

static void MenuDemo( void)
// Demo menu
{
TMenuData MData;
char      NewText[ 32];

   DMenuClear( MData);
   MData.Mask    = (1 << 6);
   forever {
      if( !DMenu( "Demo menu", DemoMenu, DemoParameters, 0, &MData)){
         return;
      }
      switch( MData.Item){
         case DM_ENTER_WEIGHT :
            DEditNumber( DMENU_EDIT_X, MData.y, &WeightValue, 1, 1, 999, "kg" );
            break;

         case DM_ENTER_TEXT :
            strcpy( NewText, TextValue);
            if( !DEditText( DMENU_EDIT_X, MData.y, NewText, TEXT_VALUE_SIZE)){
               break;
            }
            strcpy( TextValue, NewText);
            break;

         case DM_ENTER_ENUM :
            DEditEnum( DMENU_EDIT_X, MData.y, &EnumValue, STR_BACKLIGHT_MODE_AUTO, 3);
            break;

         case DM_ENTER_LIST :
            DEditList( DMENU_EDIT_X, MData.y, &ListValue, DemoList);
            break;

         case DM_ENTER_SPIN :
            DEditSpin( DMENU_EDIT_X, MData.y, &SpinValue, 0, SPIN_VALUE_MAX, 0);
            break;

         case DM_ENTER_NUMBER :
            DEditNumber( DMENU_EDIT_X, MData.y, &NumberValue, 1, 1, 999, 0);
            break;

         case DM_ITEM7 :
         case DM_ITEM8 :
            DLayoutStatus( "Press a key", 0, 0);
            GFlush();
            DEventWaitForEnterEsc();
            break;
         case DM_ENTER_ANY :
            DEditSpin( DMENU_EDIT_X, MData.y, &SpinValue, 0, SPIN_VALUE_MAX, 0);
            break;
      }
   }
} // MenuDemo

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void DemoParameters( int Index, int y, void *UserData)
// Display menu parameters
{
   switch( Index){
      case DM_ENTER_WEIGHT :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%3.1f kg", WeightValue);
         break;
      case DM_ENTER_TEXT :
         DLabelRight( TextValue, DMENU_PARAMETERS_X, y);
         break;
      case DM_ENTER_ENUM :
         DLabelEnum( EnumValue, STR_BACKLIGHT_MODE_AUTO, DMENU_PARAMETERS_X, y);
         break;
      case DM_ENTER_LIST :
         DLabelList( ListValue, DemoList, DMENU_PARAMETERS_X, y);
         break;
      case DM_ENTER_SPIN :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", SpinValue);
         break;
      case DM_ENTER_NUMBER :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%3.1f", NumberValue);
         break;
      case DM_ITEM7 :
      case DM_ITEM8 :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", Index * 100);
         break;
      case DM_ENTER_ANY :
         DLabelFormat( DMENU_PARAMETERS_X, y, "%d", SpinValue);
         break;
   }
} // DemoParameters

//------------------------------------------------------------------------------
//  Yield
//------------------------------------------------------------------------------

int SysYield( void) 
// Background process & check for events
{
int           Key;

   if( Timer1s){
      // uplynula 1s
      WatchDog();                   // refresh watchdog
      Timer1s = NO;                 // zrus priznak
   }
   // check for flash :
   if( SysIsFlash1()){
      return( K_FLASH1);
   }
   if( SysIsFlash2()){
      return( K_FLASH2);
   }
   // check for key :
   Key = KbdGet();
   if( Key != K_IDLE){
      return( Key);
   }
   return( K_IDLE);
} // SysYield

//------------------------------------------------------------------------------
//  Wait Event
//------------------------------------------------------------------------------

int SysWaitEvent( void) 
// Wait for event
{
int Key;

   forever {
      Key = SysYield();
      if( Key != K_IDLE) {
          return( Key);                // nonempty event
      }
   }
} // SysWaitEvent

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   CounterFlash--;                     \
   if( CounterFlash == TIMER_FLASH1){  \
      SetFlash1();                     \
   }                                   \
   if( !CounterFlash){                 \
      SetTimerFlash();                 \
      SetFlash2();                     \
   }                                   \
   KbdTrigger();                       \
   SndTrigger();                       



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Flash timer
//------------------------------------------------------------------------------

TYesNo SysIsFlash1( void)
// Returns YES if flash 1 event occured
{
TYesNo Is;

   DisableInts();
   Is = IsFlash1();                    // read flag
   ClrFlash1();                        // clear flag
   EnableInts();
   return( Is);
} // SysIsFlash1

TYesNo SysIsFlash2( void)
// Returns YES if flash 2 event occured
{
TYesNo Is;

   DisableInts();
   Is = IsFlash2();                    // read flag
   ClrFlash2();                        // clear flag
   EnableInts();
   return( Is);
} // SysIsFlash1
