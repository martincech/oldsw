//******************************************************************************
//
//   CountryTable.c  Country & locale tables
//   Version 1.0     (c) VymOs
//
//******************************************************************************

// Locales :
typedef enum {
   LOCALE_DEFAULT,
   LOCALE_EU,
   LOCALE_US,
   LOCALE_JAPAN,
   _LOCALES_COUNT
} TLocalesEnum;


// Locale templates :
static const TLocale _Locales[ _LOCALES_COUNT] = {
/* DEFAULT */ { CPG_LATIN, DATE_FORMAT_DDMMYYYY, '.', '.', TIME_FORMAT_24, ':', DT_DST_TYPE_OFF},
/* EU */      { CPG_LATIN, DATE_FORMAT_DDMMYYYY, '.', '.', TIME_FORMAT_24, ':', DT_DST_TYPE_EU},
/* US */      { CPG_LATIN, DATE_FORMAT_MMDDYYYY, '-', '-', TIME_FORMAT_12, ':', DT_DST_TYPE_US},
/* JAPAN */   { CPG_JAPAN, DATE_FORMAT_DDMMYYYY, '.', '.', TIME_FORMAT_24, ':', DT_DST_TYPE_OFF}
};


static const TLocaleItem _LocaleIndex[ _COUNTRY_COUNT] = {
/* CTRY_INTERNATIONAL */ { LNG_ENGLISH, LOCALE_DEFAULT},
/* CTRY_US     */        { LNG_ENGLISH, LOCALE_US},
/* CTRY_CZECH  */        { LNG_CZECH,   LOCALE_EU},
/* CTRY_SLOVAK */        { LNG_SLOVAK,  LOCALE_EU},
/* CTRY_JAPAN  */        { LNG_JAPAN,   LOCALE_JAPAN}
};
