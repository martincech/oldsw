//******************************************************************************
//
//   CountrySet.h  Country set definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

// Country :
typedef enum {
   COUNTRY_INTERNATIONAL,
   COUNTRY_US,
   COUNTRY_CZECH,
   COUNTRY_SLOVAK,
   COUNTRY_JAPAN,
   _COUNTRY_COUNT
} TCountryEnum;

// Language :
typedef enum {
   LNG_ENGLISH,
   LNG_CZECH,
   LNG_SLOVAK,
   LNG_JAPAN,
   _LNG_COUNT
} TLanguageEnum;

// Code pages :
typedef enum {
   CPG_LATIN,
   CPG_JAPAN,
   _CPG_COUNT
} TCodePageEnum;
