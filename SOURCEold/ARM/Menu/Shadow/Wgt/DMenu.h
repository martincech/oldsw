//******************************************************************************
//                                                                            
//  DMenu.h         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DMenu_H__
   #define __DMenu_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

// Menu definition :
#define DefMenu( menu) const TUniStr menu[] = {
#define EndMenu()      STR_NULL};

// Use :
// DefMenu( MenuFile)
//    STR_NEW,
//    STR_OPEN,
//    ...
// EndMenu()

typedef void TMenuItemCb( int Index, int y, void *UserData);
// right side menu callback


int DMenu( TUniStr Caption, const TUniStr *Menu, unsigned Mask, TMenuItemCb *MenuItemCb, void *UserData, int CursorItem);
// Displays <Menu>, returns selected item or -1 on Esc

int DMenuGetY( unsigned Mask, int Item);
// Returns Y coordinate of <CursorItem>

#endif
