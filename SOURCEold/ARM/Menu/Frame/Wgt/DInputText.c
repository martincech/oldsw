//******************************************************************************
//                                                                            
//   DInputText.c   Display input text box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DInput.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DMsg.h"
#include "../../inc/Graphic.h"
#include "DLayout.h"
#include "../Str.h"                    // strings from project directory
#include <string.h>

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DInputText( TUniStr Caption, TUniStr Text, char *String, int Width)
// Input text up to <Width> letters
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterTextWidth( Width) / 2;
   forever {
      // draw widgets :
      GClear();
      DLayoutTitle( Caption);
      DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
      DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
      GFlush();
      // edit text :
      if( !DEnterText( String, Width, x, DINPUT_EDIT_Y)){
         return( NO);                  // escape
      }
      StrTrimRight( String);
      if( strlen( String) > 0){
         return( YES);         
      }
      if( Width == 1){
         // single character - accept space
         String[ 0] = ' ';
         String[ 1] = '\0';
         return( YES);
      }
      DMsgOk( STR_ERROR, STR_STRING_EMPTY, 0);
   }
} // DInputText
