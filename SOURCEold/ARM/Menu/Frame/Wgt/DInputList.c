//******************************************************************************
//                                                                            
//   DInputList.c   Input dialog for string list
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DInputList.h"
#include "../../inc/wgt/DEnterList.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DProgress.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "DLayout.h"
#include "../Str.h"                    // strings from project directory

//------------------------------------------------------------------------------
//  Input List
//------------------------------------------------------------------------------

TYesNo DInputList( TUniStr Caption, TUniStr Text, int *Value, 
                   const TUniStr *List)
// Input list
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterListWidth( List) / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterList( Value, List, x, DINPUT_EDIT_Y));
} // DInputList

//------------------------------------------------------------------------------
//  Spin
//------------------------------------------------------------------------------

TYesNo DInputSpin( TUniStr Caption, TUniStr Text, int *Value, 
                   int MaxValue, TAction *OnChange)
// Input value by spinner
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterSpinWidth( MaxValue) / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterSpin( Value, MaxValue, OnChange, x, DINPUT_EDIT_Y));
} // DInputSpin
