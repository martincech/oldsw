//******************************************************************************
//                                                                            
//   DHistogram.c   Display histogram
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DHistogram.h"
#include "../../Inc/Wgt/DEvent.h"
#include "../../Inc/Wgt/DLabel.h"
#include "../../Inc/Wgt/DProgress.h"
#include "../../Inc/Wgt/Beep.h"
#include "../../Inc/Graphic.h"
#include "../../Inc/conio.h"
#include "DWeight.h"
#include "../Fonts.h"
#include "../Bitmap.h"

// Histogram constatnts :
#define DHIST_CURSOR_Y    21                           // top cursor
#define DHIST_CEIL_Y      26                           // ceil line
#define DHIST_BASE_Y      112                          // base line
#define DHIST_BOTTOM_Y    138                          // bottom line
#define DHIST_INFO_H      (DHIST_BOTTOM_Y - DHIST_BASE_Y + 1)  // information area height
#define DHIST_WEIGHT_X    152                          // weight label x
#define DHIST_COLUMN_X    6                            // column distance
#define DHIST_COLUMN_W    5                            // column width

// Icon constants :
#define ICON_COLUMN_W   2                            // columns width
#define ICON_COLUMN_H   28                           // max. column height
#define ICON_BASE_Y     29                           // base line
#define ICON_W          (ICON_COLUMN_W * HISTOGRAM_SLOTS)

// Local functions :

static void DrawHistogram( THistogram *Histogram, int LowLimit, int HighLimit);
// Draw <Histogram>

static void DrawColumn( int Value, int SlotIndex);
// Draw column at <SlotIndex> with <Value>

static int GetSlotX( int SlotIndex);
// Returns <SlotIndex> x position

static void MarkerColumn( THistogram *Histogram, int SlotIndex);
// Draw marker column at <SlotIndex>

//------------------------------------------------------------------------------
//  Histogram
//------------------------------------------------------------------------------

TYesNo DHistogram( THistogram *Histogram,  int LowLimit, int HighLimit)
// Display histogram. Returns YES no Enter
{
int Cursor;
int OldCursor;

   SetFont( TAHOMA16);
   Cursor    = HISTOGRAM_SLOTS / 2;
   OldCursor = -1;
   DrawHistogram( Histogram, LowLimit, HighLimit);
   forever {
      if( Cursor != OldCursor){
         // clear status area
         GSetColor( DCOLOR_BACKGROUND);
         GBox( 0, DHIST_BASE_Y + 1, G_WIDTH, DHIST_INFO_H);
         GSetColor( DCOLOR_DEFAULT);
         // draw icons
         GBitmap( 3,   115, &BmpHistogramCount);
         GBitmap( 125, 115, &BmpHistogramWeight);
         // count
         GTextAt( 34, DHIST_BASE_Y + 4);
         cprintf( "%d", Histogram->Slot[ Cursor]);         // samples count
         // weight
         GTextAt( DHIST_WEIGHT_X,  DHIST_BASE_Y + 4);
         if( Cursor == 0){
            cputch( '<');                                  // samples under minimum
         } else if( Cursor == HISTOGRAM_SLOTS - 1){
            cputch( '>');                                  // samples above maximum
         } // else no prefix
         DWeightLeft( HistogramGetValue( Histogram, Cursor));  // weight at cursor
         // erase cursor marker
         if( OldCursor >= 0){
            GSetColor( DCOLOR_BACKGROUND);
            GBox( GetSlotX( OldCursor) - 3, DHIST_CURSOR_Y, 7, 5); // erase icon
            MarkerColumn( Histogram, OldCursor);                   // erase column
            GSetColor( DCOLOR_DEFAULT);
         }
         // draw cursor marker
         GBitmap( GetSlotX( Cursor) - 3, DHIST_CURSOR_Y, &BmpHistogramCursor);
         GSetColor( DCOLOR_HISTOGRAM_CURSOR);
         MarkerColumn( Histogram, Cursor);
         GSetColor( DCOLOR_DEFAULT);
/*
         GSetMode( GMODE_XOR);
         GLine( GetSlotX( Cursor), DHIST_BASE_Y + 1, GetSlotX( Cursor), DHIST_BOTTOM_Y);
         GSetMode( GMODE_REPLACE);
*/
         OldCursor = Cursor;           // remember value
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            if( Cursor == HISTOGRAM_SLOTS - 1){
               BeepError();
               break;
            }
            BeepKey();
            Cursor++;
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            if( Cursor == 0){
               BeepError();
               break;
            }
            BeepKey();
            Cursor--;
            break;

         case K_ENTER :
            BeepKey();
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DHistogram

//------------------------------------------------------------------------------
//  Histogram Icon
//------------------------------------------------------------------------------

void DHistogramIcon( THistogram *Histogram, int x, int y)
// Display histogram icon at <x,y>
{
int i;
int MaxValue;
int Height;

   GSetColor( DCOLOR_SHADOW);
   GBox( x, y, ICON_W, ICON_BASE_Y);                            // background box
   GSetColor( DCOLOR_DEFAULT);
   GLine( x, y + ICON_BASE_Y, x + ICON_W, y + ICON_BASE_Y);     // base line
   MaxValue = HistogramMaximum( Histogram);
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Height = (HistogramNormalize( Histogram, MaxValue, i) * ICON_COLUMN_H) / HISTOGRAM_NORM;
      GBox( x + i * ICON_COLUMN_W, y + ICON_BASE_Y - Height, ICON_COLUMN_W, Height);      
   }
} // DHistogramIcon

//------------------------------------------------------------------------------
//  Draw histogram
//------------------------------------------------------------------------------

static void DrawHistogram( THistogram *Histogram, int LowLimit, int HighLimit)
// Draw <Histogram>
{
int i;
int Value;
int MaxValue;

   GLine( 0, DHIST_BASE_Y, G_WIDTH - 1, DHIST_BASE_Y);     // base line
   MaxValue = HistogramMaximum( Histogram);
   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Value = HistogramNormalize( Histogram, MaxValue, i);
      DrawColumn( Value, i);
   }
   // marker settings
   GSetMode( GMODE_XOR);
   GSetPattern( PATTERN_DOTTED);
   // draw low limit marker
   if( LowLimit > 0){
      i = HistogramGetSlot( Histogram, LowLimit);
      if( i > 0 && i < HISTOGRAM_SLOTS - 1){
         GLine( GetSlotX( i) - 3, DHIST_CEIL_Y, GetSlotX( i) - 3, DHIST_BASE_Y - 1);
      } // else out of visible area
   }
   // draw high limit marker
   if( HighLimit > 0){
      i = HistogramGetSlot( Histogram, HighLimit);
      if( i > 0 && i < HISTOGRAM_SLOTS - 1){
         GLine( GetSlotX( i) + 3, DHIST_CEIL_Y, GetSlotX( i) + 3, DHIST_BASE_Y - 1);
      } // else out of visible area
   }
   // return to standard settings
   GSetMode( GMODE_REPLACE);
   GSetPattern( PATTERN_SOLID);
} // DrawHistogram

//------------------------------------------------------------------------------
//  Draw Column
//------------------------------------------------------------------------------

static void DrawColumn( int Value, int SlotIndex)
// Draw column at <SlotIndex> with <Value>
{
int Height;

   Height = (Value * (DHIST_BASE_Y - DHIST_CEIL_Y)) / HISTOGRAM_NORM;
   if( Value > 0 && Height == 0){
      Height = 1;                      // nonzero value - set 1pixel height
   }
   if( !Height){
      return;                          // zero column
   }
   GBox( GetSlotX( SlotIndex) - DHIST_COLUMN_W / 2, DHIST_BASE_Y - Height, DHIST_COLUMN_W, Height);
} // Draw column

//------------------------------------------------------------------------------
//  Slot X
//------------------------------------------------------------------------------

static int GetSlotX( int SlotIndex)
// Returns <SlotIndex> x position
{
   return( SlotIndex * DHIST_COLUMN_X + G_WIDTH / 2 - HISTOGRAM_SLOTS / 2 * DHIST_COLUMN_X);
} // GetSlotX

//------------------------------------------------------------------------------
//  Marker Column
//------------------------------------------------------------------------------

static void MarkerColumn( THistogram *Histogram, int SlotIndex)
// Draw marker column at <SlotIndex>
{
int MaxValue;
int Value;
int Height;

   MaxValue = HistogramMaximum( Histogram);
   Value = HistogramNormalize( Histogram, MaxValue, SlotIndex);
   Height = (Value * (DHIST_BASE_Y - DHIST_CEIL_Y)) / HISTOGRAM_NORM;
   if( Value > 0 && Height == 0){
      Height = 1;                      // nonzero value - set 1pixel height
   }
   GBox( GetSlotX( SlotIndex) - 2, DHIST_CEIL_Y, DHIST_COLUMN_W, DHIST_BASE_Y - DHIST_CEIL_Y - Height - 1);
} // MarkerColumn
