//******************************************************************************
//                                                                            
//  DMenu.c         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DMenu.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "DLayout.h"

#define DMENU_MAX_ROWS       32               // rows limit

#define DMENU_ITEM_HEIGHT    19               // menu item height
#define DMENU_TEXT_Y         25               // first item y

#define DMENU_NORMAL_COLOR   DCOLOR_DEFAULT   // standard menu item
#define DMENU_INACTIVE_COLOR COLOR_DARKGRAY   // shadowed menu item
#define DMENU_SELECTED_COLOR DCOLOR_DEFAULT   // selected item text
#define DMENU_SELECTED_BG    COLOR_LIGHTGRAY  // selected item background

// Local functions :

static void MenuDraw( const TUniStr *Menu, unsigned Mask, int CursorRow, TMenuItemCb *MenuItemCb, void *UserData,
                      TYesNo Inactive, int *RowCount);
// Draw menu, returns <RowCount>

static int GetRow( unsigned Mask, int CursorItem);
// Returns row number by item index

static int GetIndex( unsigned Mask, int CursorRow);
// Returns item index by Row

//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------

int DMenu( TUniStr Caption, const TUniStr *Menu, unsigned Mask, TMenuItemCb *MenuItemCb, void *UserData, int CursorItem)
// Displays <Menu>, returns selected item or -1 on Esc
{
int RowCount;                // menu rows
int LastItem;

   GClear();
   DLayoutTitle( Caption);
   CursorItem = GetRow( Mask, CursorItem);       // recalculate to visible rows
   LastItem   = -1;
   forever {
      if( LastItem != CursorItem){
         MenuDraw( Menu, Mask, CursorItem, MenuItemCb, UserData, NO, &RowCount); // active menu
         LastItem = CursorItem;        // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CursorItem == 0){
               BeepError();
               break;
            }
            BeepKey();
            CursorItem--;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CursorItem == RowCount - 1){
               BeepError();
               break;
            }
            BeepKey();
            CursorItem++;
            break;

         case K_ENTER :
            BeepKey();
            MenuDraw( Menu, Mask, CursorItem, MenuItemCb, UserData, YES, &RowCount); // inactive menu
            return( GetIndex( Mask, CursorItem));  // get item index by row

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( -1);
      }     
   }
} // DMenu

//------------------------------------------------------------------------------
//  MenuGetY
//------------------------------------------------------------------------------

int DMenuGetY( unsigned Mask, int Item)
// Returns Y coordinate of <CursorItem>
{
   return( GetRow( Mask, Item) * DMENU_ITEM_HEIGHT + DMENU_TEXT_Y);
} // DMenuGetY

//******************************************************************************

//------------------------------------------------------------------------------
//  Menu Draw
//------------------------------------------------------------------------------

static void MenuDraw( const TUniStr *Menu, unsigned Mask, int CursorRow, TMenuItemCb *MenuItemCb, void *UserData,
                      TYesNo Inactive, int *RowCount)
// Draw menu, returns <RowCount> and <ItemHeight>
{
int i;
int Count;
int y;
int Color;

   // clear data area
   GSetColor( DCOLOR_BACKGROUND);
   GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H);
   if( Inactive){
      Color = DMENU_INACTIVE_COLOR;    // inactive menu
   } else {
      Color = DMENU_NORMAL_COLOR;      // standard menu
   }
   // draw items
   i     = 0;
   Count = 0;
   y     = DMENU_TEXT_Y;
   while( Menu[ i]){
      if( Mask & (1 << i)){
         i++;                          // skip invisible item
         continue;
      }
      GSetColor( Color);                              // current text color
      if( Count == CursorRow){
         // selected item
         if( Inactive){
            GSetColor( DMENU_SELECTED_BG);
         } else {
            GSetColor( DCOLOR_CURSOR);                // cursor block color
         }
         GBoxRound( 0, y, G_WIDTH, DMENU_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         if( Inactive){
            GSetColor( DMENU_SELECTED_COLOR);         // selected item color
         } else {
            GSetColor( DCOLOR_BACKGROUND);            // cursor text color
         }
      }
      DLabel( Menu[ i], 6, y);
      if( MenuItemCb){
         (*MenuItemCb)( i, y, UserData);
      }
      
      y += DMENU_ITEM_HEIGHT;
      i++;
      Count++;
   }
   *RowCount   = Count;
   GSetColor( DCOLOR_DEFAULT);         // restore default color
} // MenuDraw

//------------------------------------------------------------------------------
//  Get Row
//------------------------------------------------------------------------------

int GetRow( unsigned Mask, int CursorItem)
// Returns Y coordinate of <CursorItem>
{
int Row;
int i;

   Row = CursorItem;
   for( i = 0; i <= CursorItem; i++){
      if( Mask & (1 << i)){
         Row--;                      // remove invisible
      }
   }
   return( Row);
} // GetRow

//------------------------------------------------------------------------------
//  Get Index
//------------------------------------------------------------------------------

static int GetIndex( unsigned Mask, int CursorRow)
// Returns item index by Row
{
int Index;
int i;

   // search for visible items :
   Index = -1;
   for( i = 0; i < DMENU_MAX_ROWS; i++){
      if( Mask & (1 << i)){
         continue;                     // invisible
      }
      Index++;                         // visible
      if( Index == CursorRow){
         return( i);                   // index found
      }
   }
   return( 0);                         // compiler only
} // GetIndex
