//*****************************************************************************
//
//    Fonts.c - project fonts
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#include "Fonts.h"
#include "../inc/Country.h"
#include "../inc/Graphic.h"

#include "../Moduly/Font/Tahoma8.c"
#include "../Moduly/Font/Tahoma16.c"
#include "../Moduly/Font/Arial30n.c"
#include "../Moduly/Font/ArialBlack85n.c"
#include "../Moduly/Font/Arial80n.c"

const TFontDescriptor const *Fonts[] = {
   /* Tahoma8      */   &Tahoma8,
   /* Tahoma16     */   &FontTahoma16,
   /* Arial30      */   &FontArial30n,
   /* ArialBlack85 */   &FontArialBlack85n,
   /* Arial80      */   &FontArial80n,
   /* last         */   0
};   

//------------------------------------------------------------------------------
//  Set font
//------------------------------------------------------------------------------

void SetFont( int FontNumber)
// Set font
{
   GSetFont( FontNumber);              // direct font
} // SetFont
