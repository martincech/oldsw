//*****************************************************************************
//
//    Bitmap.h - project bitmaps
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Bitmap_H__
   #define __Bitmap_H__

#ifndef __Graphic_H__
   #include "../Inc/Graphic.h"
#endif

//------------------------------------------------------------------------------
//  Logo
//------------------------------------------------------------------------------

extern const TBitmap BmpOff;

//------------------------------------------------------------------------------
//   Status Line
//------------------------------------------------------------------------------

extern const TBitmap BmpBattery;
extern const TBitmap BmpMemory;

extern const TBitmap BmpCharger;
extern const TBitmap BmpPC;

extern const TBitmap BmpLightHeavy;
extern const TBitmap BmpLightOKHeavy;

extern const TBitmap BmpModeAuto;
extern const TBitmap BmpModeManual;
extern const TBitmap BmpModeManualBySex;

extern const TBitmap BmpMoreBirds;

//------------------------------------------------------------------------------
//  Big accu
//------------------------------------------------------------------------------

extern const TBitmap BmpAccu;

//------------------------------------------------------------------------------
//  Advanced
//------------------------------------------------------------------------------

extern const TBitmap BmpWeight;
extern const TBitmap BmpSigma;
extern const TBitmap BmpFile;
extern const TBitmap BmpLastSaved;
extern const TBitmap BmpCount;
extern const TBitmap BmpFlagMale;
extern const TBitmap BmpFlagFemale;
extern const TBitmap BmpFlagLight;
extern const TBitmap BmpFlagOK;
extern const TBitmap BmpFlagHeavy;

extern const TBitmap BmpAdvancedUnderflow;
extern const TBitmap BmpAdvancedOverflow;

extern const TBitmap BmpSortAdvancedNone;
extern const TBitmap BmpSortAdvancedLight;
extern const TBitmap BmpSortAdvancedOK;
extern const TBitmap BmpSortAdvancedHeavy;

//------------------------------------------------------------------------------
//  Basic
//------------------------------------------------------------------------------

extern const TBitmap BmpBasicUnderflow;
extern const TBitmap BmpBasicOverflow;

extern const TBitmap BmpSortNone;
extern const TBitmap BmpSortLight;
extern const TBitmap BmpSortOK;
extern const TBitmap BmpSortHeavy;

//------------------------------------------------------------------------------
//  Large
//------------------------------------------------------------------------------

extern const TBitmap BmpFlagLargeMale;
extern const TBitmap BmpFlagLargeFemale;
extern const TBitmap BmpFlagLargeLight;
extern const TBitmap BmpFlagLargeOK;
extern const TBitmap BmpFlagLargeHeavy;

//------------------------------------------------------------------------------
//  Statistics
//------------------------------------------------------------------------------

extern const TBitmap BmpStatisticsCount;
extern const TBitmap BmpStatisticsWeight;
extern const TBitmap BmpStatisticsSigma;
extern const TBitmap BmpStatisticsCV;
extern const TBitmap BmpStatisticsUNI;

extern const TBitmap BmpStatisticsTotal;
extern const TBitmap BmpStatisticsMale;
extern const TBitmap BmpStatisticsFemale;
extern const TBitmap BmpStatisticsLight;
extern const TBitmap BmpStatisticsOK;
extern const TBitmap BmpStatisticsHeavy;

//------------------------------------------------------------------------------
//  Histogram
//------------------------------------------------------------------------------

extern const TBitmap BmpHistogramCount;
extern const TBitmap BmpHistogramWeight;
extern const TBitmap BmpHistogramCursor;

//------------------------------------------------------------------------------
//  Compare files
//------------------------------------------------------------------------------

#define BmpFilesCount BmpStatisticsCount
extern const TBitmap  BmpFilesWeight;
#define BmpFilesSigma BmpStatisticsSigma
extern const TBitmap  BmpFilesCV;
#define BmpFilesUNI   BmpStatisticsUNI
//------------------------------------------------------------------------------
//  Layout
//------------------------------------------------------------------------------

extern const TBitmap BmpFileBlack;

extern const TBitmap BmpButtonOK;
extern const TBitmap BmpButtonCancel;
extern const TBitmap BmpButtonUp;
extern const TBitmap BmpButtonDown;
extern const TBitmap BmpButtonLeft;
extern const TBitmap BmpButtonRight;

extern const TBitmap BmpIconTotal;
extern const TBitmap BmpIconMale;
extern const TBitmap BmpIconFemale;
extern const TBitmap BmpIconLight;
extern const TBitmap BmpIconOK;
extern const TBitmap BmpIconHeavy;

extern const TBitmap BmpFileChecked;
extern const TBitmap BmpFileUnchecked;
extern const TBitmap BmpFileArrow;

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

extern const TBitmap BmpEditUp;
extern const TBitmap BmpEditDown;

#endif
