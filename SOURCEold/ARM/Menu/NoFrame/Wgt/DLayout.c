//******************************************************************************
//                                                                            
//  DLayout.c      Display layout
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DLayout.h"
#include "../../Inc/Graphic.h"
#include "../../Inc/conio.h"
#include "../../Inc/Wgt/DLabel.h"
#include "../Bitmap.h"
#include "../Fonts.h"
#include "../Str.h"

#define RIGHT_SPACE         3                              // right side icon space
#define STATUS_ICON_X       3                              // left icon
#define STATUS_TEXT_X       30                             // left text
#define STATUS_LINE_Y       (G_HEIGHT - DLAYOUT_STATUS_H)  // status line
#define STATUS_BOX_H        (DLAYOUT_STATUS_H - 1)         // status box width
#define STATUS_ICON_Y       (STATUS_LINE_Y + 3)            // status icon
#define STATUS_TEXT_Y       (STATUS_LINE_Y + 3)            // status text

//------------------------------------------------------------------------------
//  Title
//------------------------------------------------------------------------------

void DLayoutTitle( TUniStr Caption)
// Display window <Title>
{
   SetFont( TAHOMA16);
   // draw frame
   GSetColor( DCOLOR_TITLE_BG);
   GBox( 0, 0, G_WIDTH, DLAYOUT_TITLE_H);
   // draw title
   if( Caption){
      GSetColor( DCOLOR_TITLE);
      DLabelCenter( Caption, 0, 0, G_WIDTH, DLAYOUT_TITLE_H);
   }
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutTitle

//------------------------------------------------------------------------------
//  File Title
//------------------------------------------------------------------------------

void DLayoutFileTitle( TUniStr FileName)
// Display window <FileName>
{
   SetFont( TAHOMA16);
   // draw frame
   GSetColor( DCOLOR_TITLE_BG);
   GBox( 0, 0, G_WIDTH, DLAYOUT_TITLE_H);
   // icon
   GBitmap( 4, 0, &BmpFileBlack);
   // draw title
   if( FileName){
      GSetColor( DCOLOR_TITLE);
      GTextAt( 27, 2);  
      cputs( FileName);
   }
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutFileTitle

//------------------------------------------------------------------------------
//  Status line
//------------------------------------------------------------------------------

void DLayoutStatus( TUniStr Left, TUniStr Right, const TBitmap *RightIcon)
// Draw status line
{
int Width;

   SetFont( TAHOMA16);
   // draw frame
   GSetColor( DCOLOR_STATUS_LINE);
   GLine( 0, STATUS_LINE_Y, G_WIDTH, STATUS_LINE_Y);
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, STATUS_LINE_Y + 1, G_WIDTH, STATUS_BOX_H);
   // left button
   GSetColor( DCOLOR_STATUS);
   if( Left){
      GBitmap( STATUS_ICON_X,  STATUS_ICON_Y, &BmpButtonCancel);
      GTextAt( STATUS_TEXT_X, STATUS_TEXT_Y);
      cputs( Left);
   }
   // right bitmap
   if( RightIcon){
      GBitmap( G_WIDTH - RightIcon->Width - RIGHT_SPACE, 142, RightIcon);
   }
   // right button
   if( Right){
      Width = GTextWidth( StrGet( Right));
      if( RightIcon){
         GBitmap( G_WIDTH - Width - 26 - RightIcon->Width - 2 * RIGHT_SPACE, STATUS_ICON_Y, &BmpButtonOK);
         GTextAt( G_WIDTH - Width - 2  - RightIcon->Width - 2 * RIGHT_SPACE, STATUS_TEXT_Y);
      } else {
         GBitmap( G_WIDTH - Width - 26, STATUS_ICON_Y, &BmpButtonOK);
         GTextAt( G_WIDTH - Width - 2,  STATUS_TEXT_Y);
      }
      cputs( Right);
   }
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutStatus

//------------------------------------------------------------------------------
//  Status line with cursor
//------------------------------------------------------------------------------

void DLayoutStatusMove( void)
// Draw status line with all cursor keys
{
   SetFont( TAHOMA16);
   // draw frame
   GSetColor( DCOLOR_STATUS_LINE);
   GLine( 0, STATUS_LINE_Y, G_WIDTH, STATUS_LINE_Y);
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, STATUS_LINE_Y + 1, G_WIDTH, STATUS_BOX_H);
   // left button
   GSetColor( DCOLOR_STATUS);
   GBitmap( STATUS_ICON_X,  STATUS_ICON_Y, &BmpButtonCancel);
   GTextAt( STATUS_TEXT_X, STATUS_TEXT_Y);
   cputs( STR_BTN_CANCEL);
   // right buttons
   GBitmap( G_WIDTH - 20, STATUS_ICON_Y, &BmpButtonRight);
   GBitmap( G_WIDTH - 40, STATUS_ICON_Y, &BmpButtonLeft);
   GBitmap( G_WIDTH - 60, STATUS_ICON_Y, &BmpButtonDown);
   GBitmap( G_WIDTH - 80, STATUS_ICON_Y, &BmpButtonUp);
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutStatusMove

//------------------------------------------------------------------------------
//  Status line with vertical cursor
//------------------------------------------------------------------------------

void DLayoutStatusMoveVertical( void)
// Draw status line with vertical cursor keys
{
   SetFont( TAHOMA16);
   // draw frame
   GSetColor( DCOLOR_STATUS_LINE);
   GLine( 0, STATUS_LINE_Y, G_WIDTH, STATUS_LINE_Y);
   GSetColor( DCOLOR_STATUS_BG);
   GBox( 0, STATUS_LINE_Y + 1, G_WIDTH, STATUS_BOX_H);
   // left button
   GSetColor( DCOLOR_STATUS);
   GBitmap( STATUS_ICON_X,  STATUS_ICON_Y, &BmpButtonCancel);
   GTextAt( STATUS_TEXT_X, STATUS_TEXT_Y);
   cputs( STR_BTN_CANCEL);
   // right buttons
   GBitmap( G_WIDTH - 20, STATUS_ICON_Y, &BmpButtonDown);
   GBitmap( G_WIDTH - 40, STATUS_ICON_Y, &BmpButtonUp);
   GSetColor( DCOLOR_DEFAULT);
} // DLayoutStatusMoveVertical
