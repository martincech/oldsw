//******************************************************************************
//                                                                            
//  DMsg.c         Display text message box
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DMsg.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/Graphic.h"
#include "DLayout.h"                    // Display layout
#include "Str.h"                        // strings from project directory

// local functions :
static void FormatText( TUniStr Text, TUniStr Text2);
// Draw texts

//------------------------------------------------------------------------------
//  Info
//------------------------------------------------------------------------------

void DMsgOk( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text> with title <Caption> with OK button
{
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( 0, STR_BTN_OK, 0);
   GFlush();
   // wait for user response :
   DEventWaitForEnter();
} // DMsgOk

//------------------------------------------------------------------------------
//  Nowait info
//------------------------------------------------------------------------------

void DMsgCancel( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays window with Cancel button. Warning : doesn't wait for a key
{
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( STR_BTN_CANCEL, 0, 0);
   GFlush();
} // DMsgCancel

//------------------------------------------------------------------------------
//  Confirmation
//------------------------------------------------------------------------------

TYesNo DMsgOkCancel( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text> with title <Caption> with OK/Cancel buttons
{
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // wait for user response :
   return( DEventWaitForEnterEsc());
} // DMsgOkCancel

//------------------------------------------------------------------------------
//  Selection
//------------------------------------------------------------------------------

TYesNo DMsgYesNo( TUniStr Caption, TUniStr Text, TUniStr Text2)
// Displays <Text>/<Text2> with title <Caption> with Yes/No buttons
{
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   FormatText( Text, Text2);
   DLayoutStatus( STR_BTN_NO, STR_BTN_YES, 0);
   GFlush();
   // wait for user response :
   return( DEventWaitForEnterEsc());
} // DMsgYesNo

//------------------------------------------------------------------------------
//  Format text
//------------------------------------------------------------------------------

static void FormatText( TUniStr Text, TUniStr Text2)
// Draw texts
{
   if( !Text2){
      DLabelCenter( Text,  0, 72, G_WIDTH, 0);
   } else {
      DLabelCenter( Text,  0, 55, G_WIDTH, 0);
      DLabelCenter( Text2, 0, 75, G_WIDTH, 0);
   }
} // FormatText
