//*****************************************************************************
//
//    Fonts.h - project fonts
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __Fonts_H__
   #define __Fonts_H__

#ifndef __Font_H__   
   #include "../inc/Font.h"
#endif   

// vycet fontu :

typedef enum {
   TAHOMA8,
   TAHOMA16,
   ARIAL30,
   ARIAL_BLACK85,
   ARIAL80,
   _FONT_LAST
} TProjectFonts;

// popisovace fontu :

extern const TFontDescriptor const *Fonts[];

void SetFont( int FontNumber);
// Set font

#endif
