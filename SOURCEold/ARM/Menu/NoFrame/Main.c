//******************************************************************************
//                                                                            
//  Main.c         Menu demo
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/St7259.h"    // Display (contrast)
#include "../inc/Sound.h"     // Sound
#include "Fonts.h"            // seznam fontu
#include "../inc/conio.h"     // Display

#include <string.h>
#include "../Inc/Wgt/DLabel.h"  // display enum
#include "../Inc/Wgt/DEnter.h"       // input value
#include "../Inc/Wgt/DEnterList.h"   // input enum
#include "../Inc/Wgt/DEvent.h"
#include "Wgt/DMenu.h"
#include "Wgt/DEdit.h"

#include "Backlight.h"        // Backlight
#include "Beep.h"             // Beep
#include "Str.h"

// start odpocitavani 1 s :

#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD
#define SetTimerFlash()   CounterFlash   = (TIMER_FLASH1 + TIMER_FLASH2) / TIMER_PERIOD

#define TIMER_FLAG_FLASH1  0x02
#define TIMER_FLAG_FLASH2  0x04

#define SetFlash1()  Flags |=  TIMER_FLAG_FLASH1
#define SetFlash2()  Flags |=  TIMER_FLAG_FLASH2

#define ClrFlash1()  Flags &= ~TIMER_FLAG_FLASH1
#define ClrFlash2()  Flags &= ~TIMER_FLAG_FLASH2

#define IsFlash1()  (Flags & TIMER_FLAG_FLASH1)
#define IsFlash2()  (Flags & TIMER_FLAG_FLASH2)

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s
volatile word   CounterFlash;            // flash countdown
volatile byte   Flags;                   // timer flags

//------------------------------------------------------------------------------

#define BACKLIGHT_MAX  20
#define CONTRAST_MIN   GPU_EC_BASE - 32
#define CONTRAST_MAX   GPU_EC_BASE + 32

static int CurrentContrast  = GPU_EC_BASE;

#include "Bat1.c"        // logo bitmap

//------------------------------------------------------------------------------

// Menu context :
#define TEXT_VALUE_SIZE  6

static int  NumberValue;
static char TextValue[ TEXT_VALUE_SIZE + 1];
static int  EnumValue;

typedef enum {
   E_KOLO,
   E_ROLO,
   E_HOVNO,
   _E_COUNT
} TDemoEnum;

DefList( DemoEnumList)
   "Kolo",
   "Rolo",
   "Hovno",
EndList()

// Local functions :

static void MenuDemo( void);
// Demo menu

static void DemoParameters( int Index, int y, void *UserData);
// Menu parameters

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   CpuInit();
   // power management :
   PManInitPorts();
   PManClrENNAB();                     // enable charger
   PManSetVYP();                       // hold power

   // basic peripherals init :
   KbdInit();
   SndInit();
   BacklightInit();
   GInit();

   // system run :   

   Flags = 0;
   SetTimerFlash();

   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   // hlavni smycka :
   BacklightSet( BACKLIGHT_MAX);
   BacklightOn();
   CurrentContrast  = GPU_EC_BASE;
   GpuContrast( CurrentContrast);
   GSetFont( TAHOMA16);

   // init menu context :
   NumberValue = 333;
   strcpy( TextValue, "TEXT");
   EnumValue   = 0;

   GBitmap( 0, 0, &Bat1);  
   GFlush();
   forever {
      switch( SysWaitEvent()){
         case K_DOWN :
            BeepKey();
            MenuDemo();
            DEventDiscard();       // stop timeout chain
            break;

         case K_RIGHT :
            if( CurrentContrast == CONTRAST_MAX){
               continue;
            }
            CurrentContrast++;
            GpuContrast( CurrentContrast);
            continue;

         case K_LEFT :
            if( CurrentContrast == CONTRAST_MIN){
               continue;
            }
            CurrentContrast--;
            GpuContrast( CurrentContrast);
            continue;

         case K_ESC :
            CurrentContrast  = GPU_EC_BASE;
            GpuContrast( CurrentContrast);
            continue;

         case K_ESC | K_REPEAT :
            // power off
            GpuShutdown();                      // display off
            DisableInts();                      // stop interrupts
            PManClrVYP();                       // power off
            forever {
               SysDelay( 100);                  // watchdog space
               WatchDog();                      // wait for power failure
            }
            break;

         default :
            continue;
      }
      GBitmap( 0, 0, &Bat1);  
      GFlush();
   }
} // main

//------------------------------------------------------------------------------
//   Menu
//------------------------------------------------------------------------------

static DefMenu( DemoMenu)
   "Enter number",
   "Enter text",
   "Enter enum",
   "Item 4",
   "Item 5",
   "Item 6",
   "Item 7",
   "Item 8",
EndMenu()

typedef enum {
   DM_ENTER_NUMBER,
   DM_ENTER_TEXT,
   DM_ENTER_ENUM,
   DM_ITEM4,
   DM_ITEM5,
   DM_ITEM6,
   DM_ITEM7,
   DM_ITEM8,
} TDemoMenuEnum;

#define DMENU_STATUS_Y  (G_HEIGHT - 16)      // status line Y
#define DMENU_INFO_X     4                   // status/info left margin

static void MenuDemo( void)
// Demo menu
{
int Item;
int Mask;
int y;

   Item = 0;
   Mask = (1 << 4);
   forever {
      Item = DMenu( "Demo menu", DemoMenu, Mask, DemoParameters, 0, Item);
      if( Item < 0){
         return;
      }
      y = DMenuGetY( Mask, Item);
      switch( Item){
         case DM_ENTER_NUMBER :
//            DEnterNumber( &NumberValue, 3, 0, 3, DMENU_PARAMETERS_X, DMenuGetY( Mask, Item));
            DEditNumber( DMENU_PARAMETERS_X, y, &NumberValue, 1, 1, 999, "kg" );
            break;

         case DM_ENTER_TEXT :
//            DEnterText( TextValue, TEXT_VALUE_SIZE, DMENU_PARAMETERS_X, DMenuGetY( Mask, Item));
            DEditText( DMENU_PARAMETERS_X, y, TextValue, TEXT_VALUE_SIZE);
            break;

         case DM_ENTER_ENUM :
//            DEnterList( &EnumValue, DemoEnumList, DMENU_PARAMETERS_X, DMenuGetY( Mask, Item));
            DEditEnum( DMENU_PARAMETERS_X, y, &EnumValue, STR_BACKLIGHT_MODE_AUTO, 3);
            break;

         case DM_ITEM4 :
         case DM_ITEM5 :
         case DM_ITEM6 :
         case DM_ITEM7 :
         case DM_ITEM8 :
            GSetColor( DCOLOR_STATUS);
            GTextAt( DMENU_INFO_X, DMENU_STATUS_Y);
            cputs( "Press a key");
            GSetColor( DCOLOR_DEFAULT);
            GFlush();
            DEventWaitForEnterEsc();
            break;
      }
   }
} // MenuDemo

//------------------------------------------------------------------------------
//   Parameters
//------------------------------------------------------------------------------

static void DemoParameters( int Index, int y, void *UserData)
// Display menu parameters
{
   GTextAt( DMENU_PARAMETERS_X, y);
   switch( Index){
      case DM_ENTER_NUMBER :
         cprintf( "%4.1f kg", NumberValue);
         break;
      case DM_ENTER_TEXT :
         cputs( TextValue);
         break;
      case DM_ENTER_ENUM :
//         DLabelList( EnumValue, DemoEnumList, DMENU_PARAMETERS_X, y);
         DLabelEnum( EnumValue, STR_BACKLIGHT_MODE_AUTO, DMENU_PARAMETERS_X, y);
         break;
      case DM_ITEM4 :
      case DM_ITEM5 :
      case DM_ITEM6 :
      case DM_ITEM7 :
      case DM_ITEM8 :
         cprintf( "%d", Index * 100);
         break;
   }
} // DemoParameters

//------------------------------------------------------------------------------
//  Yield
//------------------------------------------------------------------------------

int SysYield( void) 
// Background process & check for events
{
int           Key;

   if( Timer1s){
      // uplynula 1s
      WatchDog();                   // refresh watchdog
      Timer1s = NO;                 // zrus priznak
   }
   // check for flash :
   if( SysIsFlash1()){
      return( K_FLASH1);
   }
   if( SysIsFlash2()){
      return( K_FLASH2);
   }
   // check for key :
   Key = KbdGet();
   if( Key != K_IDLE){
      return( Key);
   }
   return( K_IDLE);
} // SysYield

//------------------------------------------------------------------------------
//  Wait Event
//------------------------------------------------------------------------------

int SysWaitEvent( void) 
// Wait for event
{
int Key;

   forever {
      Key = SysYield();
      if( Key != K_IDLE) {
          return( Key);                // nonempty event
      }
   }
} // SysWaitEvent

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   CounterFlash--;                     \
   if( CounterFlash == TIMER_FLASH1){  \
      SetFlash1();                     \
   }                                   \
   if( !CounterFlash){                 \
      SetTimerFlash();                 \
      SetFlash2();                     \
   }                                   \
   KbdTrigger();                       \
   SndTrigger();                       



#include "../Moduly/LPC/SysTimer.c"

//------------------------------------------------------------------------------
//   Flash timer
//------------------------------------------------------------------------------

TYesNo SysIsFlash1( void)
// Returns YES if flash 1 event occured
{
TYesNo Is;

   DisableInts();
   Is = IsFlash1();                    // read flag
   ClrFlash1();                        // clear flag
   EnableInts();
   return( Is);
} // SysIsFlash1

TYesNo SysIsFlash2( void)
// Returns YES if flash 2 event occured
{
TYesNo Is;

   DisableInts();
   Is = IsFlash2();                    // read flag
   ClrFlash2();                        // clear flag
   EnableInts();
   return( Is);
} // SysIsFlash1
