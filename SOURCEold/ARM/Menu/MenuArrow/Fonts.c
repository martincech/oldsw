//*****************************************************************************
//
//    Fonts.c - project fonts
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#include "Fonts.h"
#include "../inc/Country.h"
#include "../inc/Graphic.h"

#include "../Moduly/Font/Tahoma8.c"
#include "../Moduly/Font/Tahoma16.c"

const TFontDescriptor const *Fonts[] = {
   /* Tahoma8      */   &Tahoma8,
   /* Tahoma16     */   &FontTahoma16,
   /* last         */   0
};   

//------------------------------------------------------------------------------
//  Set font
//------------------------------------------------------------------------------

void SetFont( int FontNumber)
// Set font
{
   GSetFont( FontNumber);              // direct font
} // SetFont
