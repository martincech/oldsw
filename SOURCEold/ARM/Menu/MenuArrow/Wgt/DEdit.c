//******************************************************************************
//                                                                            
//   DEdit.c        Display edit box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DEdit.h"
#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DEnterList.h"
#include "../../inc/wgt/DMsg.h"
#include "../../inc/Graphic.h"
#include "../../inc/conio.h"
#include "../../inc/Bcd.h"
#include "DFormat.h"                    // Display formats
#include "../Str.h"                     // strings from project directory
#include "../Bitmap.h"                  // project bitmaps
#include "../Fonts.h"                   // project fonts
#include <string.h>                     // strlen

#define DEDIT_HEIGHT  19                // edit field height

static void DrawFrame( int x, int y, int Width, int Height);
// Draw edit area frame

//------------------------------------------------------------------------------
//  Integer
//------------------------------------------------------------------------------

TYesNo DEditNumber( int x, int y, int *Value, 
                    int Decimals, int LoLimit, int HiLimit, TUniStr Units)
// Edit number
{
int  NWidth;
int  EditWidth;
int  NewValue;
char RangeTxt[ 32];

   SetFont( DEDIT_FONT);
   EditWidth = BcdGetWidth( HiLimit);
   NWidth    = EditWidth;
   if( NWidth <= Decimals){
      NWidth = Decimals + 1;           // fraction only, add 0.000
   }
   NewValue  = *Value;
//   DrawFrame( x, y, DEnterNumberWidth( NWidth, Decimals), DEDIT_HEIGHT);
   // units
   if( Units){
      GTextAt( x + DEnterNumberWidth( NWidth, Decimals) + GLetterWidth( ' '), y);
      cputs( Units);
   }
   // edit value
   if( !DEnterNumber( &NewValue, NWidth, Decimals, EditWidth, x, y)){
      return( NO);                     // escape
   }
   // check for range
   if( NewValue >= LoLimit && NewValue <= HiLimit){
      *Value = NewValue;
      return( YES);                    // inside range
   }
   // format & show range
   DFormatRange( RangeTxt, Decimals, LoLimit, HiLimit, Units);
   DMsgOk( STR_ERROR, STR_OUT_OF_LIMITS, RangeTxt);
   return( NO);                        // number out of range
} // DEditNumber

//------------------------------------------------------------------------------
//  Enum
//------------------------------------------------------------------------------

TYesNo DEditEnum( int x, int y, int *Value, 
                  TUniStr Base, int EnumCount)
// Edit enum
{
   return( DEditEnumCallback( x, y, Value, Base, EnumCount, 0));
} // DEditEnum

//------------------------------------------------------------------------------
//  Enum with callback
//------------------------------------------------------------------------------

TYesNo DEditEnumCallback( int x, int y, int *Value, 
                          TUniStr Base, int EnumCount, TAction *OnChange)
// Edit enum with <OnChange> callback
{
   SetFont( DEDIT_FONT);
   DrawFrame( x, y, DEnterEnumWidth( Base, EnumCount), DEDIT_HEIGHT);
   // edit enum :
   return( DEnterEnum( Value, Base, EnumCount, OnChange, x, y));
} // DEditEnum

#ifdef DEDIT_TEXT
//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DEditText( int x, int y, char *String, int CharCount)
// Edit text up to <Width> letters
{
   SetFont( DEDIT_FONT);
//   DrawFrame( x, y, DEnterTextWidth( CharCount), DEDIT_HEIGHT);
   // edit text :
   if( !DEnterText( String, CharCount, x, y)){
      return( NO);                  // escape
   }
   StrTrimRight( String);
   if( strlen( String) > 0){
      return( YES);         
   }
   if( CharCount == 1){
      // single character - accept space
      String[ 0] = ' ';
      String[ 1] = '\0';
      return( YES);
   }
   DMsgOk( STR_ERROR, STR_STRING_EMPTY, 0);
   return( NO);
} // DEditText

#endif // DEDIT_TEXT

//------------------------------------------------------------------------------
//  List
//------------------------------------------------------------------------------

TYesNo DEditList( int x, int y, int *Value, const TUniStr *List)
// Edit list
{
   SetFont( DEDIT_FONT);
   DrawFrame( x, y, DEnterListWidth( List), DEDIT_HEIGHT);
   // edit enum :
   return( DEnterList( Value, List, x, y));
} // DEditList

//------------------------------------------------------------------------------
//  Spin
//------------------------------------------------------------------------------

TYesNo DEditSpin( int x, int y, int *Value, 
                  int MinValue, int MaxValue, TAction *OnChange)
// Edit value by spinner
{
   SetFont( DEDIT_FONT);
   DrawFrame( x, y, DEnterSpinWidth( MaxValue), DEDIT_HEIGHT);
   // edit enum :
   return( DEnterSpin( Value, MinValue, MaxValue, OnChange, x, y));
} // DEditSpin

//------------------------------------------------------------------------------
//  Frame
//------------------------------------------------------------------------------

static void DrawFrame( int x, int y, int Width, int Height)
// Draw edit area frame
{
   // draw arrows
   GSetColor( DCOLOR_DEFAULT);
   GBitmap( x + Width / 2 - 3, y - 5,          &BmpEditUp);
   GBitmap( x + Width / 2 - 3, y + Height + 1, &BmpEditDown);
} // DrawFrame
