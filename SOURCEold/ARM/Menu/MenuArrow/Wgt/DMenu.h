//******************************************************************************
//                                                                            
//  DMenu.h         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DMenu_H__
   #define __DMenu_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

#ifndef __string_H
   #include <string.h>
#endif

// Menu definition :
#define DefMenu( menu) const TUniStr menu[] = {
#define EndMenu()      STR_NULL};

// Use :
// DefMenu( MenuFile)
//    STR_NEW,
//    STR_OPEN,
//    ...
// EndMenu()

// Menu context data :
typedef struct {
   int      Item;            // active/selected item
   unsigned Mask;            // hidden items mask
   unsigned SubMask;         // submenu items mask
   int      y;               // selected item Y position
   int      FirstRow;        // scrollable menu - first row
} TMenuData;

typedef void TMenuItemCb( int Index, int y, void *UserData);
// right side menu callback


TYesNo DMenu( TUniStr Caption, const TUniStr *Menu, TMenuItemCb *MenuItemCb, void *UserData, TMenuData *MData);
// Displays <Menu>, returns NO on Esc

#define DMenuClear( MData)  memset( &MData, 0, sizeof( MData))
// Prepare menu data

#endif
