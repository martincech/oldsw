//******************************************************************************
//                                                                            
//  DMenu.c         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DMenu.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "../../inc/conio.h"
#include "DLayout.h"
#include "../Str.h"
#include "../Fonts.h"

#define DMENU_MAX_ROWS       32               // rows limit

#define DMENU_ITEM_HEIGHT    19               // menu item height
#define DMENU_TEXT_Y         24               // first item y

#define DMENU_HEIGHT  (G_HEIGHT - DMENU_TEXT_Y - DLAYOUT_STATUS_H) // items area height
#define DMENU_COUNT   (DMENU_HEIGHT / DMENU_ITEM_HEIGHT)           // items per page

#define DMENU_NUMBER_W       26               // line number width

#define DMENU_NORMAL_COLOR   DCOLOR_DEFAULT   // standard menu item
#define DMENU_INACTIVE_COLOR COLOR_LIGHTGRAY  // shadowed menu item
#define DMENU_SELECTED_COLOR DCOLOR_DEFAULT   // selected item text
#define DMENU_SELECTED_BG    COLOR_LIGHTGRAY  // selected item background
#define DMENU_NUMBER_COLOR   COLOR_LIGHTGRAY  // line number

// Local functions :

static void MenuDraw( const TUniStr *Menu, TMenuData *MData, int LastCursorRow, int CursorRow, 
                      TMenuItemCb *MenuItemCb, void *UserData, TYesNo Inactive);
// Draw menu

static int MenuRows( const TUniStr *Menu, unsigned Mask);
// Returns number of visible rows

static int GetRow( TMenuData *MData, int CursorItem);
// Returns row number by item index

static int GetIndex( unsigned Mask, int CursorRow);
// Returns item index by Row

//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------

TYesNo DMenu( TUniStr Caption, const TUniStr *Menu, TMenuItemCb *MenuItemCb, void *UserData, TMenuData *MData)
// Displays <Menu>, returns selected item or -1 on Esc
{
int RowsCount;             // all visible menu rows
int PageRows;              // menu rows at page
int LastItem;
int CursorItem;
int x;

   SetFont( DMENU_FONT);
   GClear();
   DLayoutTitle( Caption);
   DLayoutStatus( STR_BTN_EXIT, STR_BTN_SELECT, 0);
   CursorItem = GetRow( MData, MData->Item);     // recalculate to visible rows
   RowsCount  = MenuRows( Menu, MData->Mask);    // visible rows
   if( RowsCount <= DMENU_COUNT){
      PageRows = RowsCount;            // all items visible - nonscrollable menu
   } else {
      PageRows = DMENU_COUNT;          // scrollable menu
   }
   LastItem   = -1;
   forever {
      if( LastItem != CursorItem){
         x = G_WIDTH - DMENU_NUMBER_W;
         GSetColor( DCOLOR_TITLE_BG);
         GBox( x, 0, G_WIDTH - x, DLAYOUT_TITLE_H);
         GSetColor( DMENU_NUMBER_COLOR);
         GTextAt( x, 0);
         cprintf( "%d/%d", CursorItem + MData->FirstRow + 1, RowsCount);
         GSetColor( DCOLOR_DEFAULT);
         GFlush();
         MenuDraw( Menu, MData, LastItem, CursorItem, MenuItemCb, UserData, NO); // active menu
         LastItem = CursorItem;        // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CursorItem == 0){
               if( MData->FirstRow == 0){
                  BeepError();
                  break;
               }
               BeepKey();
               MData->FirstRow--;
               LastItem = -1;          // redraw menu
               break;
            }
            BeepKey();
            CursorItem--;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CursorItem == PageRows - 1){
               if( MData->FirstRow >= RowsCount - PageRows){
                  BeepError();
                  break;
               }
               BeepKey();
               MData->FirstRow++;
               LastItem = -1;          // redraw menu
               break;
            }
            BeepKey();
            CursorItem++;
            break;

         case K_ENTER :
            BeepKey();
            DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
            MenuDraw( Menu, MData, -1, CursorItem, MenuItemCb, UserData, YES); // inactive menu
            MData->y    = CursorItem * DMENU_ITEM_HEIGHT + DMENU_TEXT_Y;
            MData->Item = GetIndex( MData->Mask, CursorItem + MData->FirstRow);// get item index by row
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }     
   }
} // DMenu

//******************************************************************************

//------------------------------------------------------------------------------
//  Menu Draw
//------------------------------------------------------------------------------

static void MenuDraw( const TUniStr *Menu, TMenuData *MData, int LastCursorRow, int CursorRow, 
                      TMenuItemCb *MenuItemCb, void *UserData, TYesNo Inactive)
// Draw menu
{
int i;
int Count;
int y;
int Color;

   if( LastCursorRow == -1){
      // clear all menu area
      GSetColor( DCOLOR_BACKGROUND);
      GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
   }
   if( Inactive){
      Color = DMENU_INACTIVE_COLOR;    // inactive menu
   } else {
      Color = DMENU_NORMAL_COLOR;      // standard menu
   }
   // draw items
   i     = GetIndex( MData->Mask, MData->FirstRow); // skip at first visible
   Count = 0;
   y     = DMENU_TEXT_Y;
   while( Menu[ i]){
      if( MData->Mask & (1 << i)){
         i++;                          // skip invisible item
         continue;
      }
      if( LastCursorRow != -1){
         // draw partialy
         if( Count != LastCursorRow && Count != CursorRow){
            // dont't redraw other rows
            y += DMENU_ITEM_HEIGHT;
            i++;
            Count++;
            continue;
         }
         // erase row
         GSetColor( DCOLOR_BACKGROUND);
         GBox( 0, y, G_WIDTH, DMENU_ITEM_HEIGHT);
      }
      GSetColor( Color);                              // current text color
      if( Count == CursorRow){
         // selected item
         if( Inactive){
            GSetColor( DMENU_SELECTED_BG);
         } else {
            GSetColor( DCOLOR_CURSOR);                // cursor block color
         }
         GBoxRound( 0, y, G_WIDTH, DMENU_ITEM_HEIGHT, DCURSOR_R, 
                   (MData->SubMask & (1 << i)) ? DMENU_ITEM_HEIGHT / 2 : DCURSOR_R);
         if( Inactive){
            GSetColor( DMENU_SELECTED_COLOR);         // selected item color
         } else {
            GSetColor( DCOLOR_BACKGROUND);            // cursor text color
         }
      }
      DLabel( Menu[ i], 6, y);
      if( Count != CursorRow || !Inactive){
         if( MenuItemCb){
            (*MenuItemCb)( i, y, UserData);
         }
      } // else skip inactive cursor row callback      
      y += DMENU_ITEM_HEIGHT;
      i++;
      Count++;
      if( Count == DMENU_COUNT){
         break;                        // page done
      }
   }
   GSetColor( DCOLOR_DEFAULT);         // restore default color
} // MenuDraw

//------------------------------------------------------------------------------
//  Menu Draw
//------------------------------------------------------------------------------

static int MenuRows( const TUniStr *Menu, unsigned Mask)
// Returns number of visible rows
{
int i;
int Count;

   i     = 0;
   Count = 0;
   while( Menu[ i]){
      if( Mask & (1 << i)){
         i++;                          // skip invisible item
         continue;
      }
      i++;
      Count++;
   }
   return( Count);
} // MenuRows

//------------------------------------------------------------------------------
//  Get Row
//------------------------------------------------------------------------------

int GetRow( TMenuData *MData, int CursorItem)
// Returns row number by item index
{
int Row;
int i;

   Row = CursorItem;
   for( i = 0; i <= CursorItem; i++){
      if( MData->Mask & (1 << i)){
         Row--;                      // remove invisible
      }
   }
   return( Row - MData->FirstRow);
} // GetRow

//------------------------------------------------------------------------------
//  Get Index
//------------------------------------------------------------------------------

static int GetIndex( unsigned Mask, int CursorRow)
// Returns item index by Row
{
int Index;
int i;

   // search for visible items :
   Index = -1;
   for( i = 0; i < DMENU_MAX_ROWS; i++){
      if( Mask & (1 << i)){
         continue;                     // invisible
      }
      Index++;                         // visible
      if( Index == CursorRow){
         return( i);                   // index found
      }
   }
   return( 0);                         // compiler only
} // GetIndex
