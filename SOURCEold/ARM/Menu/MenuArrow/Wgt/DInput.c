//******************************************************************************
//                                                                            
//   DInput.c       Display input box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DInput.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DMsg.h"
#include "../../inc/Graphic.h"
#include "../../inc/Bcd.h"
#include "../Str.h"                     // strings from project directory
#include "DLayout.h"                    // Display layout
#include <string.h>

// Local functions :
#ifdef DINPUT_ENABLE_RANGE
   static void FormatRange( char *Buffer, int Decimals, int LoLimit, int HiLimit, TUniStr Units);
   // Format range data
#endif

//------------------------------------------------------------------------------
//  Integer
//------------------------------------------------------------------------------

TYesNo DInputNumber( TUniStr Caption, TUniStr Text, int *Value, 
                     int Decimals, int LoLimit, int HiLimit, TUniStr Units)
// Input number
{
int Width;
int EditWidth;
int NewValue;
int x;                    // edit field position
#ifdef DINPUT_ENABLE_RANGE
   char RangeTxt[ 32];
#endif

   EditWidth = BcdGetWidth( HiLimit);
   Width     = EditWidth;
   if( Width <= Decimals){
      Width = Decimals + 1;            // fraction only, add 0.000
   }
   NewValue  = *Value;
   x         = G_WIDTH / 2 - DEnterNumberWidth( Width, Decimals) / 2;
   forever {
      // draw widgets :
      GClear();
      DLayoutTitle( Caption);
#ifdef DINPUT_ENABLE_RANGE
      DLabelCenter( Text, 0, DINPUT_CAPTION_Y - 10, G_WIDTH, 0);
      FormatRange( RangeTxt, Decimals, LoLimit, HiLimit, Units);
      DLabelCenter( RangeTxt, 0, DINPUT_CAPTION_Y + 10, G_WIDTH, 0);
#else
      DLabelCenter( Text, DINPUT_CAPTION_Y, 40, G_WIDTH, 0);
#endif
      DLabel( Units, G_WIDTH / 2 + (DEnterNumberWidth( Width, Decimals) + GCharWidth()) / 2, DINPUT_EDIT_Y);
      DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
      GFlush();
      // edit number :
      if( !DEnterNumber( &NewValue, Width, Decimals, EditWidth, x, DINPUT_EDIT_Y)){
         return( NO);                  // escape
      }
      if( NewValue >= LoLimit && NewValue <= HiLimit){
         *Value = NewValue;
         return( YES);         
      }
#ifdef DINPUT_ENABLE_RANGE
      DMsgOk( STR_ERROR, STR_OUT_OF_LIMITS, RangeTxt);
#else
      DMsgOk( STR_ERROR, STR_OUT_OF_LIMITS, 0);
#endif
   }
} // DInputNumber

//------------------------------------------------------------------------------
//  Enum
//------------------------------------------------------------------------------

#ifdef DENTER_CALLBACK
TYesNo DInputEnum(   TUniStr Caption, TUniStr Text, int *Value, 
                     TUniStr Base, int EnumCount)
{
   return( DInputEnumCallback( Caption, Text, Value, Base, EnumCount, 0));
} // DInputEnum

TYesNo DInputEnumCallback( TUniStr Caption, TUniStr Text, int *Value, 
                           TUniStr Base, int EnumCount, TAction *OnChange)
// Input enum with <OnChange> callback

#else
TYesNo DInputEnum(   TUniStr Caption, TUniStr Text, int *Value, 
                     TUniStr Base, int EnumCount)
// Input enum
#endif
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterEnumWidth( Base, EnumCount) / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
#ifdef DENTER_CALLBACK
   return( DEnterEnum( Value, Base, EnumCount, OnChange, x, DINPUT_EDIT_Y));
#else
   return( DEnterEnum( Value, Base, EnumCount, x, DINPUT_EDIT_Y));
#endif
} // DInputEnum

#ifdef DINPUT_PROGRESS
//------------------------------------------------------------------------------
//  Progress
//------------------------------------------------------------------------------

TYesNo DInputProgress( TUniStr Caption, TUniStr Text, int *Value, 
                       int MaxValue, TAction *OnChange)
// Input value by progress bar
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterProgressWidth() / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
      DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterProgress( Value, MaxValue, OnChange, x, DINPUT_EDIT_Y));
} // DInputProgress

#endif // DINPUT_PROGRESS

//------------------------------------------------------------------------------
//  Password
//------------------------------------------------------------------------------

void DInputPassword(  TUniStr Caption, TUniStr Text, char *Password, int Width)
// Input password
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterPasswordWidth( Width) / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   DEnterPassword( Password, Width, x, DINPUT_EDIT_Y);
} // DInputPassword


#ifndef DINPUT_DISABLE_DATE
//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

TYesNo DInputDate( TUniStr Caption, TUniStr Text, TLocalTime *Local)
// Enter date 
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterDateWidth() / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterDate( Local, x, DINPUT_EDIT_Y));
} // DInputDate
#endif // DINPUT_DISABLE_DATE

#ifndef DINPUT_DISABLE_TIME
//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

TYesNo DInputTime( TUniStr Caption, TUniStr Text, TLocalTime *Local)
// Enter time
{
int x;                    // edit field position

   x = G_WIDTH / 2 - DEnterTimeWidth() / 2;
   // draw widgets :
   GClear();
   DLayoutTitle( Caption);
   DLabelCenter( Text, 0, DINPUT_CAPTION_Y, G_WIDTH, 0);
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_OK, 0);
   GFlush();
   // edit enum :
   return( DEnterTime( Local, x, DINPUT_EDIT_Y));
} // DInputTime
#endif // DINPUT_DISABLE_TIME

//------------------------------------------------------------------------------

#ifdef DINPUT_ENABLE_RANGE
//------------------------------------------------------------------------------
//  Format Range
//------------------------------------------------------------------------------

#include "../Inc/xprint.h"
#include "../Inc/sputchar.h"

static void FormatRange( char *Buffer, int Decimals, int LoLimit, int HiLimit, TUniStr Units)
// Format range data
{
dword Format;

   if( Decimals){
      // with decimal point
      Format = FmtPrecision( 1, Decimals);  // width plus dot
   } else {
      // integer number
      Format = FmtSetWidth( 0);             // width only
   }
   sputcharbuffer( Buffer);
   sputchar( '[');
   xprintdec( sputchar, LoLimit, Format);
   sputchar( ' ');
   sputchar( '.');
   sputchar( '.');
   sputchar( ' ');
   xprintdec( sputchar, HiLimit, Format);
   sputchar( ']');
   if( Units){
      sputchar( ' ');
      Units = StrGet( Units);
      while( *Units){
         sputchar( *Units);
         Units++;
      }
   }
   sputchar( 0);                            // string terminator
} // FormatRange
#endif // DINPUT_ENABLE_RANGE
