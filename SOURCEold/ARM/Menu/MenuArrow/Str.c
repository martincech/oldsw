//******************************************************************************
//
//  Str.c          Strings translations
//  Version 1.0    (c) Robot
//
//******************************************************************************

#include "Str.h"

//------------------------------------------------------------------------------
//   String definitions
//------------------------------------------------------------------------------

TAllStrings AllStrings = {
// system :
/* STR_NULL */   "",

// Message Box :
/* STR_BTN_OK           */ "OK",	
/* STR_BTN_CANCEL       */ "CANCEL",	
/* STR_BTN_YES          */ "YES",	
/* STR_BTN_NO           */ "NO",	
/* STR_BTN_EXIT         */ "EXIT",	
/* STR_BTN_SELECT       */ "SELECT",	

/* STR_YES              */ "Yes",	
/* STR_NO               */ "No",	

// Standard title :
/* STR_ERROR            */ "Error",	
/* STR_INFO             */ "Info",	
/* STR_CONFIRMATION     */ "Confirmation",	

// Input box :
/* STR_INPUT            */ "Input",	
/* STR_OUT_OF_LIMITS    */ "Out of limits",	

// Directory box :
/* STR_DIRECTORY_EMPTY  */ "Directory empty",	

// DData/DHistory box :
/* STR_REALLY_DELETE_RECORD */ "Really delete record ?",	

// Text box :
/* STR_STRING_EMPTY     */ "String empty",	

// Backlight mode enum :
/* STR_BACKLIGHT_MODE_AUTO */ "Automatic",	
/* STR_BACKLIGHT_MODE_ON */ "Always on",	
/* STR_BACKLIGHT_MODE_OFF */ "Always off",	
// system :
/* _STR_LAST */ "?",
};
