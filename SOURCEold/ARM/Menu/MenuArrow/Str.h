//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

#define STR_NULL (char *)0 // undefined string
// Message Box :
#define STR_BTN_OK           (char *)1	
#define STR_BTN_CANCEL       (char *)2	
#define STR_BTN_YES          (char *)3	
#define STR_BTN_NO           (char *)4	
#define STR_BTN_EXIT         (char *)5	
#define STR_BTN_SELECT       (char *)6	

#define STR_YES              (char *)7	
#define STR_NO               (char *)8	

// Standard title :
#define STR_ERROR            (char *)9	
#define STR_INFO             (char *)10	
#define STR_CONFIRMATION     (char *)11	

// Input box :
#define STR_INPUT            (char *)12	
#define STR_OUT_OF_LIMITS    (char *)13	

// Directory box :
#define STR_DIRECTORY_EMPTY  (char *)14	

// DData/DHistory box :
#define STR_REALLY_DELETE_RECORD (char *)15	

// Text box :
#define STR_STRING_EMPTY     (char *)16	

// Backlight mode enum :
#define STR_BACKLIGHT_MODE_AUTO (char *)17	
#define STR_BACKLIGHT_MODE_ON (char *)18	
#define STR_BACKLIGHT_MODE_OFF (char *)19	

// system :
#define _STR_LAST (char *)20 // strings count

#endif
