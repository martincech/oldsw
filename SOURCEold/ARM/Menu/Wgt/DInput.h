//******************************************************************************
//                                                                            
//   DInput.h       Display input box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DInput_H__
   #define __DInput_H__

#ifndef __StrDef_H__
   #include "../../Inc/Wgt/StrDef.h"
#endif

#ifndef __DtDef_H__
   #include "../../Inc/DtDef.h"
#endif

#ifndef __DCallback_H__
   #include "../../Inc/Wgt/DCallback.h"
#endif

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

TYesNo DInputNumber( TUniStr Caption, TUniStr Text, int *Value, 
                     int Decimals, int LoLimit, int HiLimit, TUniStr Units);
// Input number

TYesNo DInputText( TUniStr Caption, TUniStr Text, char *String, int Width);
// Input text up to <Width> letters

TYesNo DInputEnum( TUniStr Caption, TUniStr Text, int *Value, 
                   TUniStr Base, int EnumCount);
// Input enum

#ifdef DENTER_CALLBACK
   TYesNo DInputEnumCallback( TUniStr Caption, TUniStr Text, int *Value, 
                              TUniStr Base, int EnumCount, TAction *OnChange);
   // Input enum with <OnChange> callback
#endif

TYesNo DInputProgress( TUniStr Caption, TUniStr Text, int *Value, 
                       int MaxValue, TAction *OnChange);
// Input value by progress bar

void DInputPassword( TUniStr Caption, TUniStr Text, char *Password, int Width); 
// Input password

TYesNo DInputDate( TUniStr Caption, TUniStr Text, TLocalTime *Local);
// Enter date 

TYesNo DInputTime( TUniStr Caption, TUniStr Text, TLocalTime *Local);
// Enter time

#endif
