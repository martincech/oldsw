/*****************************************************************************/
/*                                                                           */
/*    Main.c      Timer demo                                                 */
/*    (c) VymOs   Version 1.0                                                */
/*                                                                           */
/*****************************************************************************/

#include <targets/LPC2000.h>
#include <targets/liblpc2000.h>
#include <ctl_api.h>

#define LED_PIN (1 << 12)

#define TIMER_COUNT 1000

static int TimerTick;
static int TimerCount;

//-----------------------------------------------------------------------------
//  Delay
//-----------------------------------------------------------------------------

static void Delay( int n)
{
  volatile int i;
  for (i = 0; i < n; ++i);
} // Delay

//-----------------------------------------------------------------------------
//  Timer ISR
//-----------------------------------------------------------------------------

void TimerIsr( void)
{
   if( !TimerCount--){
      TimerCount = TIMER_COUNT;        // count again
      TimerTick  = 1;                  // set flag
   }
} // TimerIsr

//-----------------------------------------------------------------------------
//  Main
//-----------------------------------------------------------------------------

int main( void)
{
int Toggle;

   TimerTick  = 0;
   TimerCount = 0;
   Toggle     = 0;
   IO0DIR |= LED_PIN;
   IO0CLR |= LED_PIN;                  // LED on
   Delay( 10000);
   IO0SET |= LED_PIN;                  // LED off
   ctl_start_timer( TimerIsr);         // install ISR
   ctl_global_interrupts_enable();     // enable ints
   while( 1){
      // wait for timer tick :
      if( !TimerTick){
         continue;
      }
      TimerTick = 0;
      if( Toggle){
         IO0SET |= LED_PIN;            // LED off
      } else {
         IO0CLR |= LED_PIN;            // LED on
      }
      Toggle = !Toggle;
   }
} // main
