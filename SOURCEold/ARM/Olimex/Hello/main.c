// Copyright (c) 2005 Rowley Associates Limited.
//
// This file may be distributed under the terms of the License Agreement
// provided with this software.
//
// THIS FILE IS PROVIDED AS IS WITH NO WARRANTY OF ANY KIND, INCLUDING THE
// WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
//
////////////////////////////////////////////////////////////////////////////////
//
//                    Philips LPC2000 - UART0 Example
//
// Description
// -----------
// This example demonstrates configuring and writing to a UART and generating
// an interrupt on arrival of data. It also demonstrates how to get printf
// output over the UART by implementing __putchar.
//
// To see output:
//   - Connect serial cable from UART0/COM0 connector on your target board to your
//     host computer.
//   - Open CrossStudio's "Terminal Emulator Window". Configure it to 9600 baud, 
//     8 data bits, no parity, 1 stop bits. Click "Connect" to start the
//     terminal emulator window.
//
////////////////////////////////////////////////////////////////////////////////

#include <targets/LPC2000.h>
#include <targets/liblpc2000.h>
#include <ctl_api.h>

static unsigned char rxchar;

static void
delay(int n)
{
  volatile int i;
  for (i = 0; i < n; ++i);
}

void
UARTWriteChar(unsigned char ch)
{
  while ((U0LSR & 0x20) == 0);
  U0THR = ch;
}

unsigned char
UARTReadChar(void)
{
  while ((U0LSR & 0x01) == 0);
  return U0RBR;
}

int
UARTReadAvailable(void)
{
  return U0LSR & 0x01;
}

static void
uartISR(void)
{
  /* Read IIR to clear interrupt and find out the cause */
  unsigned iir = U0IIR;

  /* Handle UART interrupt */
  switch ((iir >> 1) & 0x7)
    {
      case 1:
        /* THRE interrupt */
        break;
      case 2:
        /* RDA interrupt */
        rxchar = U0RBR;
        break;
      case 3:
        /* RLS interrupt */
        break;
      case 6:
        /* CTI interrupt */
        break;
   }
}

void
UARTInitialize(unsigned int baud)
{
  /* Configure UART */
  unsigned int divisor = liblpc2000_get_pclk(liblpc2000_get_cclk(OSCILLATOR_CLOCK_FREQUENCY)) / (16 * baud);
  U0LCR = 0x83; /* 8 bit, 1 stop bit, no parity, enable DLAB */
  U0DLL = divisor & 0xFF;
  U0DLM = (divisor >> 8) & 0xFF;
  U0LCR &= ~0x80; /* Disable DLAB */
  PINSEL0 = PINSEL0 & (~0xF)| 0x5;
  U0FCR = 1;

  /* Setup UART RX interrupt */
  ctl_set_isr(6, 0, CTL_ISR_TRIGGER_FIXED, uartISR, 0);
  ctl_unmask_isr(6);
  U0IER = 1;
}

void 
__putchar(int ch)
{
  if (ch == '\n')
    UARTWriteChar('\r');
  UARTWriteChar(ch);
}

int
main(void)
{
  int i;
  ctl_global_interrupts_enable();
  UARTInitialize(9600);
  delay(1000);
  for(i = 0; ; ++i)
    { 
      printf("Hello World (%d)\n", i);
      if (rxchar != 0)
        { 
          if (rxchar == 'q' || rxchar == 'Q')
            break;
          printf("Key \'%c\' pressed\n", rxchar);
          rxchar = 0;
        }
    }
  printf("Goodbye!\n");
  return 0;
}


