//******************************************************************************
//                                                                            
//  Demo.c         Olimex ARM demo                                                                 
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/LPC213x.h"
#include "../inc/Lpc.h"

// LED on P0.12 (active low)
#define LEDPIN  12

//------------------------------------------------------------------------------
//   LED init
//------------------------------------------------------------------------------

static void LedInit( void)
{
   GPIO_IODIR |=  (1<<LEDPIN);	// define LED-Pin as output
   GPIO_IOSET  =  (1<<LEDPIN);	// set Bit = LED off
} // LedInit

//------------------------------------------------------------------------------
//   Delay
//------------------------------------------------------------------------------

static void Delay( void)
{
	volatile int i,j;

	for( i = 0; i < 100; i++){
		for( j = 0; j < 1000; j++);
	}
} // Delay


//******************************************************************************
//   Main
//******************************************************************************

int main( void)
{
	
	MAM_MAMCR = 2;	// MAM functions fully enabled (?)
	
	LedInit();
	while( 1){
		GPIO_IOCLR=(1<<LEDPIN);	// set all outputs in mask to 0 -> LED on
		Delay();
		GPIO_IOSET=(1<<LEDPIN);	// set all outputs in mask to 1 -> LED off
		Delay();
	}
} // main
