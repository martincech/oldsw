//******************************************************************************
//                                                                            
//  Demo.c         Olimex ARM demo                                                                 
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include <targets/LPC2000.h>

// LED on P0.12 (active low)
#define LEDPIN  12

//------------------------------------------------------------------------------
//   LED init
//------------------------------------------------------------------------------

static void LedInit( void)
{
	IO0DIR |=  (1<<LEDPIN);	// define LED-Pin as output
	IO0SET  =  (1<<LEDPIN);	// set Bit = LED off
} // LedInit

//------------------------------------------------------------------------------
//   Delay
//------------------------------------------------------------------------------

static void Delay( void)
{
	volatile int i,j;

	for( i = 0; i < 100; i++){
		for( j = 0; j < 1000; j++);
	}
} // Delay


//******************************************************************************
//   Main
//******************************************************************************

int main( void)
{
	
	MAMCR = 2;	// MAM functions fully enabled (?)
	
	LedInit();
	while( 1){
		IO0CLR=(1<<LEDPIN);	// set all outputs in mask to 0 -> LED on
		Delay();
		IO0SET=(1<<LEDPIN);	// set all outputs in mask to 1 -> LED off
		Delay();
	}
} // main
