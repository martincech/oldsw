//******************************************************************************
//                                                                            
//  Main.c         LED demo                                                                 
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "..\inc\Cpu.h"       // CPU startup
#include "..\inc\System.h"    // Operating system

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

// Local functions :
static void LedInit( void);
// Initialize port

static void LedFlash( native ms);
// Flash led for <ms> miliseconds

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
native Toggle;

   CpuInit();                          // CPU inititalisation	
   // peripherals init :
   LedInit();
   LedFlash( 500);                     // uvodni bliknuti
   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog
   // hlavni smycka :
   Toggle = 0;
   forever {
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
         switch( Toggle){
            case 0 :
               LED_SET = LED_D1;       // LED off
               LED_CLR = LED_D0;       // LED on
               break;   
            case 1 :
               LED_SET = LED_D0;       // LED off
               LED_CLR = LED_D1;       // LED on
               break;   
         }
         Toggle++;
         if( Toggle == 2){
            Toggle = 0;
         }
      }
   }
} // main


//------------------------------------------------------------------------------
//   LED init
//------------------------------------------------------------------------------

static void LedInit( void)
{
   LED_DIR |=  LED_D0 | LED_D1;	// define LED as output
   LED_SET  =  LED_D0 | LED_D1;	// set Bit = LED off
} // LedInit

//------------------------------------------------------------------------------
//   LED flash
//------------------------------------------------------------------------------

static void LedFlash( native ms)
// Flash led for <ms> miliseconds
{
   LED_CLR  =  LED_D0 | LED_D1;	// LED on
   SysDelay( ms);
   LED_SET  =  LED_D0 | LED_D1;	// LED off
   SysDelay( 20);
} // LedFlash

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }


#include "../Moduly/LPC/SysTimer.c"
