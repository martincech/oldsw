//******************************************************************************
//
//  Main.c         LED demo
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "..\inc\Cpu.h"       // CPU startup
#include "..\inc\System.h"    // Operating system

// LED on P0.12
#define LEDPIN  12
// LED on P0.13
#define LED2PIN 13

//******************************************************************************
//   Main
//******************************************************************************

int main( void)
{
   CpuInit();
   // initialize all ports :
   GPIO0_IODIR = 0xFFFFFFFF;	       // define all-Pins as output
   GPIO0_IOCLR = 0xFFFFFFFF;	       // all L

   GPIO1_IODIR = 0xFFFFFFFF;	       // define all-Pins as output
   GPIO1_IOCLR = 0xFFFFFFFF;	       // all L
   GPIO0_IOSET = (1 << LED2PIN);       // LED2 OFF
//   GPIO0_IODIR = (1 << LEDPIN);
   // blink LED :
   GPIO0_IOCLR = (1 << LEDPIN);	// ON - active L
   SysDelay( 2000);
   GPIO0_IOSET = (1 << LEDPIN);	// OFF
   SysDelay( 500);
   StartWatchDog();                    // start watchdog
   while( 1){
      GPIO0_IOCLR = (1 << LEDPIN);	// ON - active L
      SysDelay( 500);
      GPIO0_IOSET = (1 << LEDPIN);	// OFF
      SysDelay( 500);
      WatchDog();                       // refresh
   }
} // main
