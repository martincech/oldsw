//*****************************************************************************
//
//    Hardware.h - Olimex hardware definitions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../inc/Uni.h"
#include "../inc/Lpc213x.h"
#include "../inc/Lpc.h"

#define __LPC213x__

//------------------------------------------------------------------------------
//   Prirazeni portu
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//   LED
//------------------------------------------------------------------------------
// Port :
#define LED_DIR  GPIO0_IODIR
#define LED_PIN  GPIO0_IOPIN
#define LED_SET  GPIO0_IOSET
#define LED_CLR  GPIO0_IOCLR 
// Piny :
#define LED_D0   (1 << 12)
#define LED_D1   (1 << 13)

//------------------------------------------------------------------------------
//   Parametry CPU
//------------------------------------------------------------------------------

#define FXTAL    14745600              // XTAL frequency 14.745 MHz

#define FCPU     FXTAL                 // system frequency
#define FBUS     (FCPU / 4)            // peripheral bus frequency

#define MAM_MODE MAMCR_DISABLED        // Disable flash cache

//------------------------------------------------------------------------------
//   Parametry systemu
//------------------------------------------------------------------------------

//#define ENABLE_WATCHDOG   1            // conditional compilation of the watchdog
#define WATCHDOG_INTERVAL 2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]

// mapa preruseni :
#define SYS_TIMER_IRQ     IRQ0         // System timer IRQ number
#define UART0_IRQ         IRQ1         // UART0 IRQ number
#define UART1_IRQ         IRQ2         // UART1 IRQ number
