//******************************************************************************
//                                                                            
//  DMenu.c         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DMenu.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "DLayout.h"

#define DMENU_MAX_ROWS       32               // rows limit

#define DMENU_ITEM_HEIGHT    19               // menu item height
#define DMENU_TEXT_Y         25               // first item y

#define DMENU_NORMAL_COLOR   DCOLOR_DEFAULT   // standard menu item
#define DMENU_INACTIVE_COLOR COLOR_DARKGRAY   // shadowed menu item
#define DMENU_SELECTED_COLOR DCOLOR_DEFAULT   // selected item background
#define DMENU_SELECTED_BG    COLOR_LIGHTGRAY  // selected item background

// Local functions :

static void MenuDraw( const TUniStr *Menu, unsigned Mask, int Highlight, TMenuItemCb *MenuItemCb, void *UserData,
                      int *RowCount);
// Draw menu, returns <RowCount>

static int MenuSelect( int CursorItem, int RowCount);
// Displays menu selection, returns selected item or -1 on Esc

static int GetRow( unsigned Mask, int CursorItem);
// Returns row number

//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------

int DMenu( TUniStr Caption, const TUniStr *Menu, unsigned Mask, TMenuItemCb *MenuItemCb, void *UserData, int CursorItem)
// Displays <Menu>, returns selected item or -1 on Esc
{
int RowCount;                // menu rows
int Row;                     // selected (visible) row
int i;
int Index;                   // visible index

   GClear();
   DLayoutTitle( Caption);
   MenuDraw( Menu, Mask, -1, MenuItemCb, UserData, &RowCount); // active menu
   // recalculate CursorItem to visible items :
   Index = GetRow( Mask, CursorItem);
   // selection :
   Row = MenuSelect( Index, RowCount);
   if( Row < 0){
      return( -1);                     // esc pressed
   }
   // search for visible items :
   Index = -1;
   for( i = 0; i < DMENU_MAX_ROWS; i++){
      if( Mask & (1 << i)){
         continue;                     // invisible
      }
      Index++;                         // visible
      if( Index == Row){
         MenuDraw( Menu, Mask, i, MenuItemCb, UserData, &RowCount); // inactive menu
         return( i);                   // index found
      }
   }
   return( -1);                        // compiler only
} // DMenu

//------------------------------------------------------------------------------
//  MenuGetY
//------------------------------------------------------------------------------

int DMenuGetY( unsigned Mask, int Item)
// Returns Y coordinate of <CursorItem>
{
   return( GetRow( Mask, Item) * DMENU_ITEM_HEIGHT + DMENU_TEXT_Y);
} // DMenuGetY

//******************************************************************************

//------------------------------------------------------------------------------
//  Menu Draw
//------------------------------------------------------------------------------

static void MenuDraw( const TUniStr *Menu, unsigned Mask, int Highlight, TMenuItemCb *MenuItemCb, void *UserData,
                      int *RowCount)
// Draw menu, returns <RowCount> and <ItemHeight>
{
int i;
int Count;
int y;
int Color;

   // clear data area
   GSetColor( DCOLOR_BACKGROUND);
   GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H);
   if( Highlight < 0){
      Color = DMENU_NORMAL_COLOR;      // standard menu
   } else {
      Color = DMENU_INACTIVE_COLOR;    // inactive menu
   }
   // draw items
   i     = 0;
   Count = 0;
   y     = DMENU_TEXT_Y;
   while( Menu[ i]){
      if( Mask & (1 << i)){
         i++;                          // skip invisible item
         continue;
      }
      GSetColor( Color);               // current color
      if( i == Highlight){
         // selected item
         GSetColor( DMENU_SELECTED_BG);
         GBox( 0, y, G_WIDTH, DMENU_ITEM_HEIGHT);     // background box
         GSetColor( DMENU_SELECTED_COLOR);            // selected item color
      }
      DLabel( Menu[ i], 6, y);
      if( MenuItemCb){
         (*MenuItemCb)( i, y, UserData);
      }
      
      y += DMENU_ITEM_HEIGHT;
      i++;
      Count++;
   }
   *RowCount   = Count;
   GSetColor( DCOLOR_DEFAULT);         // restore default color
} // MenuDraw

//------------------------------------------------------------------------------
//  Menu Select
//------------------------------------------------------------------------------

static int MenuSelect( int CursorItem, int RowCount)
// Displays menu selection, returns selected item or -1 on Esc
{
int LastItem;

   LastItem    = -1;
   GSetMode( GMODE_XOR);
   GSetColor( DCOLOR_CURSOR);
   forever {
      if( LastItem != CursorItem){
         // status changed - redraw
         if( LastItem >= 0){
            // deselect last
            GBoxRound( 0, DMENU_TEXT_Y + LastItem * DMENU_ITEM_HEIGHT, 
                       G_WIDTH, DMENU_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         }
         // select current
         GBoxRound( 0, DMENU_TEXT_Y + CursorItem * DMENU_ITEM_HEIGHT, 
                    G_WIDTH, DMENU_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         LastItem = CursorItem;       // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CursorItem == 0){
               BeepError();
               break;
            }
            BeepKey();
            CursorItem--;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CursorItem == RowCount - 1){
               BeepError();
               break;
            }
            BeepKey();
            CursorItem++;
            break;

         case K_ENTER :
            BeepKey();
            GSetMode( GMODE_REPLACE);
            GSetColor( DCOLOR_DEFAULT);
            return( CursorItem);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            GSetMode( GMODE_REPLACE);
            GSetColor( DCOLOR_DEFAULT);
            return( -1);
      }     
   }
} // MenuSelect

//------------------------------------------------------------------------------
//  MenuDraw
//------------------------------------------------------------------------------

int GetRow( unsigned Mask, int CursorItem)
// Returns Y coordinate of <CursorItem>
{
int Row;
int i;

   Row = CursorItem;
   for( i = 0; i <= CursorItem; i++){
      if( Mask & (1 << i)){
         Row--;                      // remove invisible
      }
   }
   return( Row);
} // GetRow
