//******************************************************************************
//                                                                            
//  DSamples.c     Display samples database
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "DSamples.h"
#include "../../Inc/Wgt/DEvent.h"
#include "../../Inc/Wgt/DMsg.h"
#include "../../Inc/Wgt/Beep.h"
#include "../../Inc/Wgt/DTime.h"
#include "../../Inc/Graphic.h"
#include "../../Inc/conio.h"
#include "../../Inc/File/Fd.h"
#include "../../Inc/File/Db.h"

#include "DLayout.h"                      // Display layout
#include "DWeight.h"                      // Display weight
#include "../Str.h"                       // project directory strings
#include "../Sdb.h"                       // Samples database
#include "../Screen.h"                    // Wait
#include "../Fonts.h"                     // Project fonts
#include "../Bitmap.h"                    // Bitmaps

// Constants :
#define DDB_TEXT_Y        25                // top of the text area
#define DDB_ITEM_HEIGHT   19                // database item height

#define DSAMPLES_HEIGHT    (G_HEIGHT - DDB_TEXT_Y - DLAYOUT_STATUS_H)  // items area height
#define DSAMPLES_COUNT     (DSAMPLES_HEIGHT / DDB_ITEM_HEIGHT)         // items per page

// Local functions :
static TYesNo NextPage( int *CurrentPage, int *RowCount);
// Move at next page

static TYesNo PreviousPage( int *CurrentPage, int *RowCount);
// Move at previous page

static void UpdatePage( int *CurrentPage, int *CurrentItem, int *RowCount);
// Update page settings

static void DisplayPage( int CurrentPage, int *RowCount);
// Display database page

static void DeleteItem( int Index);
// Delete database item

//------------------------------------------------------------------------------
//  Display
//------------------------------------------------------------------------------

void DSamplesDisplay( TFdirHandle Handle)
// Display samples database data
{
char Name[ FDIR_NAME_LENGTH + 1];
int  RowCount;                // rows count
int  CurrentItem;             // active row
int  LastItem;                // last active row
int  CurrentPage;             // start of page offset
int  LastPage;                // last start of page offset

   GClear();                           // clear display
   FdGetName( Handle, Name);           // get file name
   DLayoutFileTitle( Name);            // display title
   DLayoutStatus( STR_BTN_CANCEL, STR_BTN_DELETE, 0);     // display status line
   SdbOpen( Handle, NO);
   CurrentItem =  0;                   // first row
   LastItem    = -1;                   // force redraw
   CurrentPage =  0;                   // first page
   LastPage    = -1;                   // force redraw
   forever {
      if( CurrentPage != LastPage){
         // redraw page
         DisplayPage( CurrentPage, &RowCount);
         LastPage = CurrentPage;
         LastItem = -1;                // force cursor redraw
      }
      if( CurrentItem != LastItem){
         GSetMode( GMODE_XOR);
         GSetColor( DCOLOR_CURSOR);
         if( LastItem >= 0){
            // deselect last
            GBoxRound( 0, DDB_TEXT_Y + LastItem * DDB_ITEM_HEIGHT, G_WIDTH, DDB_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         }
         // select current
         GBoxRound( 0, DDB_TEXT_Y + CurrentItem * DDB_ITEM_HEIGHT, G_WIDTH, DDB_ITEM_HEIGHT, DCURSOR_R, DCURSOR_R);
         LastItem = CurrentItem;       // remember last
         GSetMode( GMODE_REPLACE);
         GSetColor( DCOLOR_DEFAULT);
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CurrentItem > 0){
               BeepKey();
               CurrentItem--;
               break;
            }
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = RowCount - 1;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CurrentItem < RowCount - 1){
               BeepKey();
               CurrentItem++;
               break;
            }
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = 0;
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            if( CurrentItem >= RowCount){
               CurrentItem = RowCount - 1;       // short page - move cursor at last item               
            }
            break;

         case K_ENTER :
            if( !DbCount()){
               break;                  // empty database
            }
            BeepKey();
            DeleteItem( CurrentPage + CurrentItem);
            UpdatePage( &CurrentPage, &CurrentItem, &RowCount);
            LastPage = -1;             // force redraw
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            SdbClose();
            return;
      }     
   }
} // DSamplesDisplay

//------------------------------------------------------------------------------
//  Next page
//------------------------------------------------------------------------------

static TYesNo NextPage( int *CurrentPage, int *RowCount)
// Move at next page
{
int TotalCount;
int NextCount;

   TotalCount = DbCount();
   if( *CurrentPage + DSAMPLES_COUNT >= TotalCount){
      return( NO);                     // last visible page
   }
   *CurrentPage += DSAMPLES_COUNT;     // move to next page
   NextCount    = TotalCount - *CurrentPage;
   if( NextCount >= DSAMPLES_COUNT){
      *RowCount = DSAMPLES_COUNT;      // whole page
   } else {
      *RowCount = NextCount;           // short page
   }
   return( YES);
} // NextPage

//------------------------------------------------------------------------------
//  Previous page
//------------------------------------------------------------------------------

static TYesNo PreviousPage( int *CurrentPage, int *RowCount)
// Move at previous page
{
int    TotalCount;
TYesNo MovePage;

   TotalCount = DbCount();
   MovePage   = YES;
   if( *CurrentPage == 0){
      MovePage = NO;                   // already at database start
   } else {
      *CurrentPage -= DSAMPLES_COUNT;  // previous page
   }
   if( TotalCount < DSAMPLES_COUNT){
      *RowCount = TotalCount;
   } else {
      *RowCount = DSAMPLES_COUNT;
   }
   return( MovePage);
} // PreviousPage

//------------------------------------------------------------------------------
//  Update
//------------------------------------------------------------------------------

static void UpdatePage( int *CurrentPage, int *CurrentItem, int *RowCount)
// Update page settings
{
int TotalCount;
int PageRows;

   TotalCount = DbCount();
   PageRows   = TotalCount - *CurrentPage;
   if( PageRows >= DSAMPLES_COUNT){
      // whole page visible
      *RowCount = DSAMPLES_COUNT;
      return;
   }
   if( PageRows <= 0){
      // no item visible
      PreviousPage( CurrentPage, RowCount);
      if( !*RowCount){
         *CurrentItem = 0;             // empty database
      } else {
         *CurrentItem = *RowCount - 1; // at last item on page
      }
      return;
   }
   if( *CurrentItem > PageRows - 1){
      *CurrentItem = PageRows - 1;     // move at last row
   }
   *RowCount = PageRows;
} // UpdatePage

//------------------------------------------------------------------------------
//  Display page
//------------------------------------------------------------------------------

#define FLAG_ICON_X  (G_WIDTH - 15)

static void DisplayPage( int CurrentPage, int *RowCount)
// Display database page
{
int        Count;
TSdbRecord Record;
int        RowY;

   SetFont( TAHOMA16);
   // clear data area
   GSetColor( DCOLOR_BACKGROUND);
   GBox( 0, DLAYOUT_TITLE_H, G_WIDTH, G_HEIGHT - DLAYOUT_TITLE_H - DLAYOUT_STATUS_H);
   GSetColor( DCOLOR_DEFAULT);
   // draw samples
   Count      = 0;
   if( SdbMoveAt( CurrentPage)){
      // succesfull seek
      while( SdbNext( &Record)){
         if( Count >= DSAMPLES_COUNT){
            // end of page
            break;
         }
         RowY = DDB_TEXT_Y + Count * DDB_ITEM_HEIGHT;
         // item number
         GTextAt( 3, RowY);
         cprintf( "%d", CurrentPage + Count + 1);
         // date/time
         GTextAt( 49, RowY);
         DDateTimeShort( Record.Timestamp);
         // weight
         GTextAt( 178, RowY);
         DWeight( Record.Weight);
         // flag
         switch( Record.Flag){
            case FLAG_NONE :
               break;
            case FLAG_MALE :
               GBitmap( FLAG_ICON_X, RowY, &BmpIconMale);
               break;
            case FLAG_FEMALE :
               GBitmap( FLAG_ICON_X, RowY, &BmpIconFemale);
               break;
            case FLAG_LIGHT :
               GBitmap( FLAG_ICON_X, RowY, &BmpIconLight);
               break;
            case FLAG_OK :
               GBitmap( FLAG_ICON_X, RowY, &BmpIconOK);
               break;
            case FLAG_HEAVY :
               GBitmap( FLAG_ICON_X, RowY, &BmpIconHeavy);
               break;
         }
         Count++;
      }
   } // else seek out of range
   *RowCount = Count;
   GSetColor( COLOR_LIGHTGRAY);
   GLine( 45, DLAYOUT_TITLE_H + 2, 45, G_HEIGHT - DLAYOUT_STATUS_H - 2);
   GSetColor( DCOLOR_DEFAULT);
} // DisplayPage

//------------------------------------------------------------------------------
//  Delete item
//------------------------------------------------------------------------------

static void DeleteItem( int Index)
// Delete database item
{
   if( !DMsgYesNo( STR_CONFIRMATION, STR_REALLY_DELETE_RECORD, 0)){
      return;
   }
   ScreenWait();
   SdbDeleteRecord( Index);
} // DeleteItem
