//******************************************************************************
//                                                                            
//   DInputText.c   Display input text box
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DInput.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DGrid.h"
#include "../../inc/wgt/DEnter.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/DMsg.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"
#include "../../inc/conio.h"
#include "Str.h"                        // strings from project directory
#include <string.h>

// Local functions :

static void DrawFrame( TUniStr Caption, TUniStr Text);
// Draw window frame

static void EditString( char *String, int *Position, int Width);
// Edit <String> cursor starts at <Position>

static void FindLetter( char Letter, int *Row, int *Col);
// Find letter

//------------------------------------------------------------------------------
//  Text
//------------------------------------------------------------------------------

TYesNo DInputText( TUniStr Caption, TUniStr Text, char *String, int Width)
// Input text up to <Width> letters
{
int    Position;      // cursor position
TYesNo Redraw;        // redraw text
TYesNo ShowCursor;    // show/hide cursor
TYesNo DrawCursor;    // draw/skip cursor
int    x, y;          // edit field position

   x = G_WIDTH / 2 - DEnterTextWidth( Width) / 2;
   y = DG_EDIT_Y;
   DrawFrame( Caption, Text);          // draw window frame
   Position   = 0;                     // at first character
   Redraw     = YES;                   // first draw
   ShowCursor = YES;                   // show cursor
   DrawCursor = NO;                    // wait for flash
   StrSetWidth( String, Width);        // fill with spaces
   forever {
      if( Redraw){
         // clear area :
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  Width      * GCharWidth(), GCharHeight());
         GSetColor( DCOLOR_ENTER);
         // draw text :
         GTextAt( x, y);
         GSetFixedPitch();             // set nonproportional font
         cputs( String);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         if( !ShowCursor || DrawCursor){
            GSetMode( GMODE_XOR);
            GSetColor( DCOLOR_CURSOR);
            GBox( x + Position * GCharWidth(), y, GCharWidth() - 1, GCharHeight());
            GSetMode( GMODE_REPLACE);
         }
         GSetColor( DCOLOR_DEFAULT);
         Redraw = NO;
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_DOWN :
            BeepKey();
            EditString( String, &Position, Width);
            DrawFrame( Caption, Text); // draw window frame
            Redraw = YES;              // redraw edit box
            break;

         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            if( Width < 2){
               break;
            }
            ShowCursor = NO;           // disable cursor
            if( Position == Width - 1){
               BeepError();
               break;
            }
            BeepKey();
            Position++;
            Redraw = YES;
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            if( Width < 2){
               break;
            }
            ShowCursor = NO;           // disable cursor
            if( Position == 0){
               BeepError();
               break;
            }
            BeepKey();
            Position--;
            Redraw = YES;
            break;

         case K_RELEASED :
            ShowCursor = YES;       // key released, redraw cursor
            Redraw     = YES;
            break;

         case K_FLASH1 :
            DrawCursor = YES;
            Redraw     = YES;
            break;

         case K_FLASH2 :
            DrawCursor = NO;
            Redraw     = YES;
            break;

         case K_ENTER :
            BeepKey();
            StrTrimRight( String);
            if( strlen( String) > 0){
               return( YES);         
            }
            if( Width == 1){
               // single character - accept space
               String[ 0] = ' ';
               String[ 1] = '\0';
               return( YES);
            }
            DMsgOk( STR_ERROR, STR_STRING_EMPTY);
            // continue run
            DrawFrame( Caption, Text); // draw window frame
            Redraw = YES;              // redraw edit box
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DInputText

//------------------------------------------------------------------------------
//  Text Width
//------------------------------------------------------------------------------

int DEnterTextWidth( int Width)
// Returns pixel width of text field
{
   return( Width * GCharWidth());
} // DEnterTextWidth

//******************************************************************************

//------------------------------------------------------------------------------
//  Frame
//------------------------------------------------------------------------------

static void DrawFrame( TUniStr Caption, TUniStr Text)
// Draw window frame
{
   // draw widgets :
   GClear();
   DGridTitle( Caption);
   DLabelCenter( Text, 0, DG_CAPTION_Y, G_WIDTH, 0);
#ifndef DG_RIGHT_OK
   DGridTwoButtons( STR_OK, STR_CANCEL);
#else
   DGridTwoButtons( STR_CANCEL, STR_OK);
#endif
   DGridFrame();
} // DrawFrame

//******************************************************************************

//------------------------------------------------------------------------------
//  Edit
//------------------------------------------------------------------------------

#define DSL_ROWS            5
#define DSL_COLUMNS        16

#define DSL_STRING_X        0                         // edit string start x
#define DSL_STRING_Y       (G_HEIGHT - GCharHeight()) // edit string y
#define DSL_STRING_H        GCharHeight()             // edit area height
#define DSL_STRING_W        G_WIDTH                   // edit area width

static const char Letters[ DSL_ROWS][ DSL_COLUMNS] = {
("ABCDEFGHIJKLMNOP"),
("QRSTUVWXYZabcdef"),
("ghijklmnopqrstuv"),
("wxyz _0123456789"),
("+-*/=!?.,:;%#@$&"),
};

static void EditString( char *String, int *Position, int Width)
// Edit <String> cursor starts at <Position>
{
int    Row;
int    Col;
int    OldRow;
int    OldCol;
TYesNo Redraw;
TYesNo CursorVisible;
TYesNo ShowCursor;    // show/hide cursor
TYesNo FlashCursor;   // draw/skip cursor
int    i, j;

   GClear();
   // draw character matrix :
   for( i = 0; i < DSL_ROWS; i++){
      for( j = 0; j < DSL_COLUMNS; j++){
         GTextAt( j * GCharWidth(), i * GCharHeight());
         cputch( Letters[ i][ j]);
      }
   }
   FindLetter( String[ *Position], &Row, &Col);  // position of the character
   OldRow = -1;                                  // don't erase cursor
   OldCol = -1;                                  // don't erase cursor
   Redraw = YES;                                 // force redraw
   CursorVisible = NO;                           // no cursor at this moment
   ShowCursor    = YES;                          // show cursor
   FlashCursor   = NO;                           // wait for flash
   forever {
      if( Row != OldRow || Col != OldCol || Redraw){
         GSetMode( GMODE_XOR);
         // erase old cursor :
         if( CursorVisible){
            GBox( OldCol * GCharWidth(), OldRow * GCharHeight(), 
                  GCharWidth() - 1, GCharHeight());
            CursorVisible = NO;
         }
         // draw cursor :
         if( !ShowCursor || FlashCursor){
            GBox( Col * GCharWidth(), Row * GCharHeight(), 
                  GCharWidth() - 1, GCharHeight());
            CursorVisible = YES;
         }
         GSetMode( GMODE_REPLACE);
         OldRow = Row;
         OldCol = Col;
         GFlush();                     // redraw cursor
         // update string :
         String[ *Position] = Letters[ Row][ Col];
         // erase edit area :
         GSetColor( DCOLOR_EDIT_BG);
         GBox( 0, DSL_STRING_Y, DSL_STRING_W, DSL_STRING_H);
         // draw string :
         GSetColor( DCOLOR_EDIT);
         GTextAt( DSL_STRING_X, DSL_STRING_Y);
         GSetFixedPitch();             // set nonproportional font
         cputs( String);
         GSetNormalPitch();            // restore font setting
         // draw cursor :
         GSetMode( GMODE_XOR);
         GSetColor( DCOLOR_CURSOR);
         GBox( DSL_STRING_X + (*Position) * GCharWidth(), DSL_STRING_Y, GCharWidth() - 1, GCharHeight());
         GSetMode( GMODE_REPLACE);
         GSetColor( DCOLOR_DEFAULT);
         Redraw = NO;                  // redraw done
         GFlush();                     // redraw edit area
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            ShowCursor = NO;                               // disable cursor
            if( Row == 0){
               BeepError();
               break;
            }
            BeepKey();
            Row--;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            ShowCursor = NO;                               // disable cursor
            if( Row == DSL_ROWS - 1){
               BeepError();
               break;
            }
            BeepKey();
            Row++;
            break;

         case K_RIGHT | K_REPEAT :
         case K_RIGHT :
            ShowCursor = NO;                               // disable cursor
            if( Col == DSL_COLUMNS - 1){
               // try next row
               if( Row == DSL_ROWS - 1){
                  BeepError();
                  break;
               }
               BeepKey();
               Row++;
               Col = 0;
               break;
            }
            BeepKey();
            Col++;
            break;

         case K_LEFT | K_REPEAT :
         case K_LEFT :
            ShowCursor = NO;                               // disable cursor
            if( Col == 0){
               // try previous row
               if( Row == 0){
                  BeepError();
                  break;
               }
               BeepKey();
               Row--;
               Col = DSL_COLUMNS - 1;
               break;
            }
            BeepKey();
            Col--;
            break;

         case K_RELEASED :
            ShowCursor = YES;                              // key released, enable cursor
            Redraw     = YES;                              // force redraw
            break;

         case K_FLASH1 :
            FlashCursor = YES;
            Redraw      = YES;                             // force redraw
            break;

         case K_FLASH2 :
            FlashCursor = NO;
            Redraw      = YES;                             // force redraw
            break;

         case K_ENTER :
            if( *Position == Width - 1){
               BeepError();
               break;
            }
            BeepKey();
            (*Position)++;
            FindLetter( String[ *Position], &Row, &Col);   // cursor to the new character
            Redraw = YES;                                  // force redraw
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }
   }
} // EditString

//------------------------------------------------------------------------------
//  Find
//------------------------------------------------------------------------------

static void FindLetter( char Letter, int *Row, int *Col)
// Find letter
{
int  i, j;

   *Row = 0;
   *Col = 0;
   for( i = 0; i < DSL_ROWS; i++){
      for( j = 0; j < DSL_COLUMNS; j++){
         if( Letter == Letters[ i][ j]){
            *Row = i;
            *Col = j;
            return;
         }
      }
   }
} // FindLetter
