//******************************************************************************
//                                                                            
//  DDb.c          Display database template
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DDb_H__
   #define __DDb_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef void TDrawPage( int CurrentPage, int *RowCount);
// Display data page

typedef void TEnterAction( int Index);
// Press enter action

typedef void TMoveAction( int Index);
// Cursor move action

typedef struct {
   TDrawPage    *DrawPage;         // Draw page callback
   TEnterAction *EnterAction;      // Enter callback
   TMoveAction  *MoveAction;       // Move callback
} TDDbInfo;

#define DefDDbInfo( Name) const TDDbInfo Name = {
#define EndDDbInfo()      };

void DDbDisplay( const TDDbInfo *Info, TYesNo EnableEdit);
// Display database data

#endif
