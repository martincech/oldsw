//******************************************************************************
//                                                                            
//  DDb.c          Display database template
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../Inc/Wgt/DDb.h"
#include "../Inc/Wgt/DEvent.h"
#include "../Inc/Wgt/DMsg.h"
#include "../Inc/Wgt/Beep.h"
#include "../Inc/File/Db.h"
#include "../Inc/Graphic.h"
#include "../Inc/conio.h"

// Constants :
#define DDB_COUNT  ((G_HEIGHT - DDB_TOP - DDB_BOTTOM) / DDB_ITEM_HEIGHT)   // items per page
#define DDB_WIDTH  (G_WIDTH - DDB_LEFT - DDB_RIGHT)                        // cursor width

// Local functions :

static TYesNo NextPage( int *CurrentPage, int *RowCount);
// Move at next page

static TYesNo PreviousPage( int *CurrentPage, int *RowCount);
// Move at previous page

static void UpdatePage( int *CurrentPage, int *CurrentItem, int *RowCount);
// Update page settings

//------------------------------------------------------------------------------
//  Display
//------------------------------------------------------------------------------

void DDbDisplay( const TDDbInfo *Info, TYesNo EnableEdit)
// Display database data
{
int RowCount;                // rows count
int CurrentItem;             // active row
int LastItem;                // last active row
int CurrentPage;             // start of page offset
int LastPage;                // last start of page offset

   CurrentItem =  0;                   // first row
   LastItem    = -1;                   // force redraw
   CurrentPage =  0;                   // first page
   LastPage    = -1;                   // force redraw
   forever {
      if( CurrentPage != LastPage){
         // redraw page
         (*Info->DrawPage)( CurrentPage, &RowCount);
         LastPage = CurrentPage;
         LastItem = -1;                // force cursor redraw
      }
      if( CurrentItem != LastItem){
         GSetMode( GMODE_XOR);
         GSetColor( DCOLOR_CURSOR);
         if( LastItem >= 0){
            // deselect last
            GBox( DDB_LEFT, DDB_TOP + LastItem * DDB_ITEM_HEIGHT, DDB_WIDTH, DDB_ITEM_HEIGHT);
         }
         // select current
         GBox( DDB_LEFT, DDB_TOP + CurrentItem * DDB_ITEM_HEIGHT, DDB_WIDTH, DDB_ITEM_HEIGHT);
         LastItem = CurrentItem;       // remember last
         GSetMode( GMODE_REPLACE);
         GSetColor( DCOLOR_DEFAULT);
         if( Info->MoveAction){
            (*Info->MoveAction)( CurrentPage + CurrentItem);
         }
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CurrentItem > 0){
               BeepKey();
               CurrentItem--;
               break;
            }
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = RowCount - 1;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CurrentItem < RowCount - 1){
               BeepKey();
               CurrentItem++;
               break;
            }
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            CurrentItem = 0;
            break;

         case K_LEFT :
         case K_LEFT | K_REPEAT :
            if( !PreviousPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            break;

         case K_RIGHT :
         case K_RIGHT | K_REPEAT :
            if( !NextPage( &CurrentPage, &RowCount)){
               BeepError();
               break;
            }
            BeepKey();
            if( CurrentItem >= RowCount){
               CurrentItem = RowCount - 1;       // short page - move cursor at last item               
            }
            break;

         case K_ENTER :
            if( !EnableEdit){
               break;
            }
            if( !DbCount()){
               break;
            }
            BeepKey();
            if( Info->EnterAction){
               (*Info->EnterAction)( CurrentPage + CurrentItem);
            }
            UpdatePage( &CurrentPage, &CurrentItem, &RowCount);
            LastPage = -1;             // force redraw
            break;

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return;
      }     
   }
} // DDbDisplay

//------------------------------------------------------------------------------
//  Next page
//------------------------------------------------------------------------------

static TYesNo NextPage( int *CurrentPage, int *RowCount)
// Move at next page
{
int TotalCount;
int NextCount;

   TotalCount = DbCount();
   if( *CurrentPage + DDB_COUNT >= TotalCount){
      return( NO);                     // last visible page
   }
   *CurrentPage += DDB_COUNT;          // move to next page
   NextCount    = TotalCount - *CurrentPage;
   if( NextCount >= DDB_COUNT){
      *RowCount = DDB_COUNT;           // whole page
   } else {
      *RowCount = NextCount;           // short page
   }
   return( YES);
} // NextPage

//------------------------------------------------------------------------------
//  Previous page
//------------------------------------------------------------------------------

static TYesNo PreviousPage( int *CurrentPage, int *RowCount)
// Move at previous page
{
int    TotalCount;
TYesNo MovePage;

   TotalCount = DbCount();
   MovePage   = YES;
   if( *CurrentPage == 0){
      MovePage = NO;                   // already at database start
   } else {
      *CurrentPage -= DDB_COUNT;       // previous page
   }
   if( TotalCount < DDB_COUNT){
      *RowCount = TotalCount;
   } else {
      *RowCount = DDB_COUNT;
   }
   return( MovePage);
} // PreviousPage

//------------------------------------------------------------------------------
//  Update
//------------------------------------------------------------------------------

static void UpdatePage( int *CurrentPage, int *CurrentItem, int *RowCount)
// Update page settings
{
int TotalCount;
int PageRows;

   TotalCount = DbCount();
   PageRows   = TotalCount - *CurrentPage;
   if( PageRows >= DDB_COUNT){
      // whole page visible
      *RowCount = DDB_COUNT;
      return;
   }
   if( PageRows <= 0){
      // no item visible
      PreviousPage( CurrentPage, RowCount);
      if( !*RowCount){
         *CurrentItem = 0;             // empty database
      } else {
         *CurrentItem = *RowCount - 1; // at last item on page
      }
      return;
   }
   if( *CurrentItem > PageRows - 1){
      *CurrentItem = PageRows - 1;     // move at last row
   }
   *RowCount = PageRows;
} // UpdatePage
