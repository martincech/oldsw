//******************************************************************************
//                                                                            
//  DMenu.c         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __DMenu_H__
   #define __DMenu_H__

#ifndef __StrDef_H__
   #include "StrDef.h"
#endif

// Menu definition :
#define DefMenu( menu) const TUniStr menu[] = {
#define EndMenu()      STR_NULL};

// Use :
// DefMenu( MenuFile)
//    STR_NEW,
//    STR_OPEN,
//    ...
// EndMenu


int DMenu( const TUniStr *Menu, int CursorItem);
// Displays <Menu>, returns selected item or -1 on Esc

int DMenuSelect( int CursorItem, int ItemHeight, int RowCount);
// Displays menu selection, returns selected item or -1 on Esc

#endif
