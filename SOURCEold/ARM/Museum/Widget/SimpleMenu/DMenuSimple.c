//******************************************************************************
//                                                                            
//  DMenu.c         Display menu
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../../inc/wgt/DMenu.h"
#include "../../inc/wgt/DLabel.h"
#include "../../inc/wgt/DEvent.h"
#include "../../inc/wgt/Beep.h"
#include "../../inc/Graphic.h"

//------------------------------------------------------------------------------
//  Menu
//------------------------------------------------------------------------------

int DMenu( const TUniStr *Menu, int CursorItem)
// Displays <Menu> returns selected item or -1 on Esc
{
int RowCount;
int ItemHeight;
int i;

   // count rows :
   RowCount = 0;
   while( Menu[ RowCount]){
      RowCount++;
   }
   // get vertical size :
   ItemHeight = GCharHeight();   // by font only
   // draw items :
   GClear();
   for( i = 0; i < RowCount; i++){
      DLabel( Menu[ i], DG_MENU_MARGIN, i * ItemHeight);
   }
   // selection :
   return( DMenuSelect( CursorItem, ItemHeight, RowCount));
} // DMenu

//------------------------------------------------------------------------------
//  Menu Select
//------------------------------------------------------------------------------

int DMenuSelect( int CursorItem, int ItemHeight, int RowCount)
// Displays menu selection, returns selected item or -1 on Esc
{
int LastItem;

   LastItem    = -1;
   GSetMode( GMODE_XOR);
   forever {
      if( LastItem != CursorItem){
         // status changed - redraw
         if( LastItem >= 0){
            // deselect last
            GBox( 0, LastItem * ItemHeight, G_WIDTH, ItemHeight);
         }
         // select current
         GBox( 0, CursorItem * ItemHeight, G_WIDTH, ItemHeight);
         LastItem = CursorItem;       // remember last
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP :
         case K_UP | K_REPEAT :
            if( CursorItem == 0){
               BeepError();
               break;
            }
            CursorItem--;
            break;

         case K_DOWN :
         case K_DOWN | K_REPEAT :
            if( CursorItem == RowCount - 1){
               BeepError();
               break;
            }
            CursorItem++;
            break;

         case K_ENTER :
            GSetMode( GMODE_REPLACE);
            return( CursorItem);

         case K_ESC :
         case K_TIMEOUT :
            GSetMode( GMODE_REPLACE);
            return( -1);

         default :
            BeepStop();          // silent other events
            break;
      }     
   }
} // DMenuSelect
