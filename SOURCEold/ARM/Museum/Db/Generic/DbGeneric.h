//******************************************************************************
//
//   Db.h          Database utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Db_H__
   #define __Db_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FdirDef_H__
   #include "FdirDef.h"                // directory entry
#endif

#ifndef __DbDef_H__
   #include "DbDefGeneric.h"
#endif

//------------------------------------------------------------------------------
// Sequential access, read only
//------------------------------------------------------------------------------

TYesNo DbCreate( const TDbDescriptor *Descriptor,
                 char *Name, TFdirClass Class, void *Configuration);
// Create table with <Name>

void DbOpen( const TDbDescriptor *Descriptor,
             TFdirHandle Handle, TYesNo RandomAccess, TYesNo ReadOnly);
// Open table with <Handle>. Enable <RandomAccess>, set <ReadOnly>

void DbEmpty( void);
// Delete all records

#define SdbRename( Handle, Name)       FsRename( Handle, Name)
// Rename database

#define DbDelete()                     FsDelete()
// Delete current table

void DbCompress( void *Record);
// Compress database. <Record> is record cache

void DbClose( void);
// Close current table

#define DbInfo()                       FsInfo()
// Returns directory entry of the database

void DbLoadConfig( void *Configuration);
// Load configuration record <Configuration>

void DbSaveConfig( void *Configuration);
// Save configuration record <Configuration>

void DbBegin( void);
// Set before start of the table

TYesNo DbNext( void *Record);
// Get next record

//------------------------------------------------------------------------------
// Random Access
//------------------------------------------------------------------------------

int DbCount( void);
// Returns items count

TYesNo DbMoveAt( int RecordIndex);
// Move current record pointer at <RecordIndex>

//------------------------------------------------------------------------------
// Enable write
//------------------------------------------------------------------------------

void DbDeleteRecord( int RecordIndex);
// Delete record at <RecordIndex> position

TYesNo DbDeleteLastRecord( void);
// Delete last record

TYesNo DbAppend( void *Record);
// Append <Record> at end of databease

#endif

