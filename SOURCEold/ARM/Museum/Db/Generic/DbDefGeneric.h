//******************************************************************************
//
//   DbDef.h      Database data definition
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DbDef_H__
   #define __DbDef_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

// Database descriptor :
typedef struct {
   word ConfigSize;          // config record size
   word RecordSize;          // data record size
   word FlagOffset;          // flag offset
   byte FlagDeleted;         // deleted flag mask
} TDbDescriptor;

#define DefDbDescriptor( Name) const TDbDescriptor Name = {
#define EndDbDescriptor()      };

#endif

