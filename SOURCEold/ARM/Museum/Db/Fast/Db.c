//******************************************************************************
//
//   Db.c          Database utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/File/Db.h"
#include "../../Inc/File/Fs.h"

static int _ItemsCount;                // undeleted items count

// Local functions :

static TYesNo RecordIsDeleted( TDbRecord *Record);
// Test for deleted <Record>

static int CountItems( void);
// Count undeleted items

static int RecordSeek( int RecordIndex);
// Returns seek of record at <RecordIndex>

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TYesNo DbCreate( char *Name, TFdirClass Class, void *Configuration)
// Create table with <Name>
{
   if( !FsCreate( Name, Class)){
      return( NO);
   }
   _ItemsCount = 0;
   if( !FsWrite( Configuration, DB_CONFIG_SIZE)){
      FsDelete();                      // not enough space
      return( NO);
   }
   return( YES);
} // DbCreate

//------------------------------------------------------------------------------
// Open
//------------------------------------------------------------------------------

void DbOpen( TFdirHandle Handle, TYesNo RandomAccess, TYesNo ReadOnly)
// Open table with <Handle>. Enable <RandomAccess>, set <ReadOnly>
{
   FsOpen( Handle, ReadOnly);
   _ItemsCount = 0;
   if( RandomAccess){
      _ItemsCount = CountItems();
   }
   if( !ReadOnly){
      FsWSeek( 0, FS_SEEK_END);
   }
} // DbOpen

//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------

void DbClose( void)
// Close current table
{
   FsClose();
} // DbClose

//------------------------------------------------------------------------------
// Empty
//------------------------------------------------------------------------------

void DbEmpty( void)
// Delete all records
{
   FsTruncate( DB_CONFIG_SIZE);
   FsWSeek( 0, FS_SEEK_END);
} // DbEmpty

//------------------------------------------------------------------------------
// Compress
//------------------------------------------------------------------------------

void DbCompress( void)
// Compress database
{
byte     *Flag;
TYesNo    FirstTime;
TDbRecord Record;

   Flag  = (byte *)&Record;
   Flag += DB_FLAG_OFFSET;                       // flag address
   FirstTime = YES;
   FsRSeek( DB_CONFIG_SIZE, FS_SEEK_SET);        // at start of records
   while( FsRead( &Record, DB_RECORD_SIZE)){
      if( DbIsDeleted( *Flag)){
         if( FirstTime){
            FirstTime = NO;
            FsWSeek( FsRTell() - DB_RECORD_SIZE, FS_SEEK_SET);  // before deleted record
         }
         continue;
      }
      if( FirstTime){
         continue;                               // continuously undeleted
      }
      FsWrite( &Record, DB_RECORD_SIZE);         // save undeleted
   }
   if( !FirstTime){
      FsTruncate( FsWTell());                    // truncate at last record
   } // else nothing to truncate
   FsWSeek( 0, FS_SEEK_END);
} // DbCompress

//------------------------------------------------------------------------------
// Load config
//------------------------------------------------------------------------------

void DbLoadConfig( TDbConfig *Configuration)
// Load configuration record <Configuration>
{
   FsRSeek( 0, FS_SEEK_SET);
   FsRead( Configuration, DB_CONFIG_SIZE);
} // DbLoadConfig

//******************************************************************************
// Save config
//******************************************************************************

void DbSaveConfig( TDbConfig *Configuration)
// Save configuration record <Configuration>
{
int    i;
TYesNo SameData;
byte   Data;
byte   *p;

   // check for old data :
   SameData = YES;
   p        = (byte *)Configuration;
   FsRSeek( 0, FS_SEEK_SET);
   for( i = 0; i < DB_CONFIG_SIZE; i++){
      if( !FsRead( &Data, 1)){
         SameData = NO;                // short file
         break;
      }
      if( Data != *p){
         SameData = NO;                // different data
         break;
      }
      p++;
   }
   if( SameData){
      return;                          // already saved
   }
   FsWSeek( 0, FS_SEEK_SET);
   FsWrite( Configuration, DB_CONFIG_SIZE);
   FsWSeek( 0, FS_SEEK_END);
} // DbSaveConfig

//------------------------------------------------------------------------------
// Begin
//------------------------------------------------------------------------------

void DbBegin( void)
// Set before start of the table
{
   FsRSeek( DB_CONFIG_SIZE, FS_SEEK_SET);        // at start of records
} // DbBegin

//------------------------------------------------------------------------------
// Next
//------------------------------------------------------------------------------

TYesNo DbNext( TDbRecord *Record)
// Get next record
{
   forever {
      if( !FsRead( Record, DB_RECORD_SIZE)){
         return( NO);
      }
      if( !RecordIsDeleted( Record) ){
         return( YES);
      }
   }
} // DbNext

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

int DbCount( void)
// Returns items count
{
   return( _ItemsCount);
} // DbCount

//------------------------------------------------------------------------------
// Move
//------------------------------------------------------------------------------

TYesNo DbMoveAt( int RecordIndex)
// Move current record pointer at <RecordIndex>
{
int Seek;

   if( RecordIndex < 0 || RecordIndex >= _ItemsCount){
      return( NO);
   }
   Seek  = RecordSeek( RecordIndex);
   FsRSeek( Seek, FS_SEEK_SET);           // at start of record
   return( YES);
} // DbMoveAt

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void DbDeleteRecord( int RecordIndex)
// Delete record at <RecordIndex> position
{
byte Flag;
int  Seek;

   Seek  = RecordSeek( RecordIndex);
   Flag = 0;                                     // prepare flag
   DbSetDeleted( Flag);                          // set flag as deleted
   FsWSeek( Seek + DB_FLAG_OFFSET, FS_SEEK_SET); // return to flag position
   FsWrite( &Flag, 1);                           // overwrite flag
   FsWSeek( 0, FS_SEEK_END);                     // back to end
   FsRSeek( Seek, FS_SEEK_SET);                  // back to start of deleted item
   _ItemsCount--;
} // DbDeleteRecord

//------------------------------------------------------------------------------
// Delete last
//------------------------------------------------------------------------------

TYesNo DbDeleteLastRecord( void)
// Delete last record
{
   if( !_ItemsCount){
      return( NO);                     // nothing to delete
   }
   _ItemsCount--;
   FsTruncate( FsWTell() - DB_RECORD_SIZE);
   FsWSeek( 0, FS_SEEK_END);
   return( YES);
} // DbDeleteLastRecord

//------------------------------------------------------------------------------
// Append
//------------------------------------------------------------------------------

TYesNo DbAppend( TDbRecord *Record)
// Append <Record> at end of databease
{
   _ItemsCount++;
   return( FsWrite( Record, DB_RECORD_SIZE));
} // DbAppend

//******************************************************************************

//------------------------------------------------------------------------------
// Record deleted
//------------------------------------------------------------------------------

static TYesNo RecordIsDeleted( TDbRecord *Record)
// Test for deleted <Record>
{
   return( DbIsDeleted( DbFlag( *Record)));
} // RecordIsDeleted

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

static int CountItems( void)
// Count undeleted items
{
TDbRecord Record;
int       Count;

   FsRSeek( DB_CONFIG_SIZE, FS_SEEK_SET);        // at first record
   Count = 0;
   while( FsRead( &Record, DB_RECORD_SIZE)){
      if( !DbIsDeleted( DbFlag( Record))){
         Count++;
      }
   }
   return( Count);
} // CountItems

//------------------------------------------------------------------------------
// Record Seek
//------------------------------------------------------------------------------

static int RecordSeek( int RecordIndex)
// Returns seek of record at <RecordIndex>
{
TDbRecord Record;
int       Index;

   FsRSeek( DB_CONFIG_SIZE, FS_SEEK_SET);        // at first record
   Index = -1;
   do {
      if( !FsRead( &Record, DB_RECORD_SIZE)){
         return( NO);                            // end of file
      }
      if( !DbIsDeleted( DbFlag( Record))){
         Index++;                                // valid item
      }
   } while( Index != RecordIndex);
   return( FsRTell() - DB_RECORD_SIZE);          // at start of record
} // RecordSeek
