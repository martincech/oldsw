//******************************************************************************
//                                                                            
//  Main.c         Align tests
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../Inc/Uni.h"
#include <stddef.h>           // macro offsetof

typedef struct {
   byte One;
   byte Two;
   byte Three;
   byte Four;
} TStructOne;

typedef struct {
   byte One;
   byte Two;
   word Three;
} TStructTwo;

typedef struct {
   byte Two;
   word Three;
} TStructThree;

typedef struct {
   word One;
   byte Two;
} TStructFour;

typedef struct {
   word One;
   byte Two;
   word Three;
   byte Four;
} TStructFive;

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i;

   i = sizeof( TStructOne);

   i = sizeof( TStructTwo);
   i = offsetof( TStructTwo, Three);

   i = sizeof( TStructThree);
   i = offsetof( TStructTwo, Three);

   i = sizeof( TStructFour);
   i = offsetof( TStructFour, Two);

   i = sizeof( TStructFive);
   i = offsetof( TStructFive, Two);
   i = offsetof( TStructFive, Three);
   i = offsetof( TStructFive, Four);
} // main
