//******************************************************************************
//                                                                            
//  Main.c         Align tests
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../Inc/Uni.h"
#include <stddef.h>           // macro offsetof

typedef struct {
   byte One;
   byte Two;
   byte Three;
} __packed TStructOne;

typedef struct {
   byte  One;
   byte  Two;
   dword Three;
} __packed TStructTwo;

typedef struct {
   byte  Two;
   dword Three;
} __packed TStructThree;

typedef struct {
   dword One;
   byte Two;
} __packed TStructFour;

typedef struct {
   dword One;
   dword Two;
   byte  Three;
} __packed TStructFive;

typedef struct {
   word Two;
   dword Three;
   byte Four;
   word Five;
   dword Six;
   byte Seven;
} __packed TStructSix;

typedef struct {
   byte  One;
   byte  Two;
   byte  Three;
   byte  Four;
   dword Five;
} __packed TStructSeven;

typedef struct {
   byte One;
} __packed TStructEight;

typedef struct {
   dword One;
} TStructNine;

typedef struct {
   word One;
}  TStructTen;

typedef struct {
   TStructOne One;
   TStructSix Two;
} TStructMixed;

typedef struct {
   TStructEight One;
   TStructEight Two;
   TStructEight Three;
} TStructByte;

typedef struct {
   TStructEight One;
   TStructTen   Two;
} TStructWord;

typedef struct {
   TStructEight One;
   TStructNine  Two;
} TStructDword;

typedef struct {
   TStructEight One;
   word         Two;
   TStructEight Three;
   TStructNine  Four;
} TStructAlign;

typedef int64 TRetVal;
//typedef int32 TRetVal;

static TRetVal Demo( void);

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i;
TStructSix Six;
TStructFive Five;
TStructSeven Seven;
TStructMixed Mixed;
TRetVal x;

   i = sizeof( TStructOne);

   i = sizeof( TStructTwo);
   i = offsetof( TStructTwo, Three);

   i = sizeof( TStructThree);
   i = offsetof( TStructThree, Three);

   i = sizeof( TStructFour);
   i = offsetof( TStructFour, Two);

   i = sizeof( TStructFive);
   i = offsetof( TStructFive, Two);
   i = offsetof( TStructFive, Three);

   i = sizeof( TStructSix);
   i = offsetof( TStructSix, Two);
   i = offsetof( TStructSix, Three);
   i = offsetof( TStructSix, Four);
   i = offsetof( TStructSix, Five);
   i = offsetof( TStructSix, Six);

   i = Six.Two;
   i = Six.Three;
   i = Six.Four;
   i = Six.Five;
   i = Six.Six;

   i = Five.One;
   i = Five.Two;
   i = Five.Three;

   i = Seven.One;
   i = Seven.Five;

   i = sizeof( TStructMixed);
   i = offsetof( TStructMixed, Two);
   i = offsetof( TStructMixed, Two.Seven);
   
   i = Mixed.One.One;
   i = Mixed.One.Three;
   i = Mixed.Two.Two;
   i = Mixed.Two.Three;
   i = Mixed.Two.Six;
   i = Mixed.Two.Seven;

   i = sizeof( TStructByte);          // 3 
   i = offsetof( TStructByte, Two);   // 1
   i = offsetof( TStructByte, Three); // 2

   i = sizeof( TStructWord);          // 4 
   i = offsetof( TStructWord, Two);   // 2

   i = sizeof( TStructDword);         // 8
   i = offsetof( TStructDword, Two);  // 4

   i = sizeof( TStructAlign);         // 0x0C
   i = offsetof( TStructAlign, Two);  // 2
   i = offsetof( TStructAlign, Three);// 4
   i = offsetof( TStructAlign, Four); // 8
   x = Demo();

   i = 5;
   i = (i * 10000) / 1024;
   return( 0);
} // main

static TRetVal Demo( void)
{
   return( ((TRetVal)0xFFFFFFFF << 32) | 0x55555555);
} // Demo
