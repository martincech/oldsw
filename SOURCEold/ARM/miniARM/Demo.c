//******************************************************************************
//
//  Demo.c         MiniARM demo
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../Inc/Cpu.h"      // CPU startup
#include "../Inc/System.h"   // SysDelay

// LED on P0.16 P0.17
#define LED1PIN  16
#define LED2PIN  17

//******************************************************************************
//   Main
//******************************************************************************

int main( void)
{
   PManSetVYP();                       // hold power
   CpuInit();
   // initialize all ports :
   GPIO0_IODIR = 0xFFFFFFFF;	       // define all-Pins as output
   GPIO0_IOCLR = 0xFFFFFFFF;	       // all L

   // blink LED :
   GPIO0_IOSET = (1 << LED1PIN);	// ON - active H
   SysDelay( 2000);
   GPIO0_IOCLR = (1 << LED1PIN);	// OFF
   SysDelay( 500);
   StartWatchDog();                    // start watchdog
   while( 1){
      GPIO0_IOSET = (1 << LED1PIN);	// ON - active H
      SysDelay( 500);
      GPIO0_IOCLR = (1 << LED1PIN);	// OFF
      SysDelay( 500);
      WatchDog();                       // refresh
   }
} // main
