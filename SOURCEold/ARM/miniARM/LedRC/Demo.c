//******************************************************************************
//
//  Demo.c         MiniARM demo
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/LPC210x.h"
#include "../inc/Lpc.h"

// LED on P0.16 P0.17
#define LED1PIN  16
#define LED2PIN  17


//------------------------------------------------------------------------------
//   LED init
//------------------------------------------------------------------------------

static void LedInit( void)
{
   GPIO0_IODIR |=  (1 << LED1PIN) | (1 << LED2PIN);	// define LED-Pin as output
   GPIO0_IOCLR  =  (1 << LED1PIN) | (1 << LED2PIN);	// clr Bit = LED off
} // LedInit

//------------------------------------------------------------------------------
//   Delay
//------------------------------------------------------------------------------

static void Delay( void)
{
volatile int i,j;

   for( i = 0; i < 400; i++){
      for( j = 0; j < 1000; j++);
   }
} // Delay


//******************************************************************************
//   Main
//******************************************************************************

int main( void)
{
   LedInit();
   GPIO0_IOSET = (1 << LED1PIN);	// ON - active H
   Delay();
   Delay();
   Delay();
   Delay();
   Delay();
   GPIO0_IOCLR = (1 << LED1PIN);	// OFF
   Delay();
   while( 1){
      GPIO0_IOSET = (1 << LED1PIN);	// ON - active H
      Delay();
      GPIO0_IOCLR = (1 << LED1PIN);	// OFF
      Delay();
   }
} // main
