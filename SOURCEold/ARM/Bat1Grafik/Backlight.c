//*****************************************************************************
//
//    Backlight.c   Backlight functions
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Backlight.h"
#include "../inc/System.h"
#include "../inc/Pwm.h"

int _Intensity;

// Local functions :

static int PwmPercent( int Intensity);
// PWM duty cycle by backlight <Intensity>

static int PwmIntensity( void);
// PWM duty by current intensity

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void BacklightInit( void)
// Initialisation
{
   PwmInitPorts();
   PwmInit();
   _Intensity = 10;
} // BacklightInit

//------------------------------------------------------------------------------
//  On
//------------------------------------------------------------------------------

void BacklightOn( void)
// Conditionaly on
{
   PwmStart( PwmIntensity());
} // BacklightOn

//------------------------------------------------------------------------------
//  Off
//------------------------------------------------------------------------------

void BacklightOff( void)
// Conditionaly off
{
   PwmStop();
} // BacklightOff

//------------------------------------------------------------------------------
//  Set
//------------------------------------------------------------------------------

void BacklightSet( int Intensity)
// Set intensity
{
   _Intensity = Intensity;
} // BacklightSet

//------------------------------------------------------------------------------
//  Test
//------------------------------------------------------------------------------

void BacklightTest( int Intensity)
// Test backlight intensity
{
   PwmStart( PwmPercent( Intensity));
} // BacklightTest

//*****************************************************************************

//------------------------------------------------------------------------------
//  Percent
//------------------------------------------------------------------------------

static const byte Percent[21] =
{ 0,1,2,3,4,5,6,8,10,12,14,17,21,25,31,38,46,56,68,82,100};

static int PwmPercent( int Intensity)
// PWM duty cycle by backlight intensity
{
   return( Percent[ Intensity]);
} // PwmPercent

//------------------------------------------------------------------------------
//  Intensity
//------------------------------------------------------------------------------

static int PwmIntensity( void)
// PWM duty by current intensity
{
   return( PwmPercent( _Intensity));
} // PwmIntensity
