//*****************************************************************************
//
//    Usb.h        USB utilities
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Usb_H__
   #define __Usb_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void UsbInit( void);
// Initialise

void UsbExecute( void);
// USB executive

#endif
