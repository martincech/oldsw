//*****************************************************************************
//
//    Usb.c        USB utilities
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Usb.h"
#include "UsbDef.h"
#include "../Inc/Gpu.h"
#include "../Inc/Graphic.h"
#include <string.h>

#ifdef USB_COM1
   #define __COM1__
      #include "../inc/Com.h"
      #include "../inc/ComPkt.h"    // packet communication

   #define PACKET_MAX_DATA PACKET1_MAX_DATA
   #define COM_BAUD    UART1_BAUD
   #define COM_FORMAT  UART1_FORMAT
   #define COM_TIMEOUT UART1_TIMEOUT

#else // USB_COM0
   #define __COM0__
      #include "../inc/Com.h"
      #include "../inc/ComPkt.h"    // packet communication

   #define PACKET_MAX_DATA PACKET0_MAX_DATA
   #define COM_BAUD    UART0_BAUD
   #define COM_FORMAT  UART0_FORMAT
   #define COM_TIMEOUT UART0_TIMEOUT
#endif

#if PACKET_MAX_DATA != USB_MAX_DATA
   #error "PACKET_MAX_DATA mismatch"
#endif

// Local data :

static byte     UsbBuffer[ PACKET_MAX_DATA];
static unsigned Address;

// Local functions :

static void ExecuteLong( int Size);
// Execute long command

static void SaveData( int Offset, byte *Data, int Size);
// Save data to video RAM

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void UsbInit( void)
// Initialise
{
   ComInit();
   ComSetup( COM_BAUD, COM_FORMAT, COM_TIMEOUT);
   Address     = 0;
} // UsbInit

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

void UsbExecute( void)
// USB executive
{
int Cmd, Data;
int Size;

   if( !ComRxPacket( &Cmd, &Data)){
      return;                          // no data received
   }
   if( Cmd == PACKET_DATA){
      // read remainder of the data packet
      if( !ComRxData( UsbBuffer, &Size)){
         return;                       // read error
      }
      ExecuteLong( Size);
      return;
   }
   switch( Cmd){
      case USB_CMD_VERSION :
         Data = 0x0100;
         break;

      case USB_CMD_ADDRESS :
         Address = Data;               // remember address
         break;

      case USB_CMD_GET_ADDRESS :
         Data = Address;               // return address
         break;

      case USB_CMD_FLUSH :
         GBuffer.MinX = 0;
         GBuffer.MaxX = G_MAX_X;
         GBuffer.MinY = 0;
         GBuffer.MaxY = G_MAX_Y;   
         GFlush();
         break;
      case USB_CMD_CLEAR :
         GClear();
         GFlush();
         break;

   }
   ComTxPacket( Cmd | USB_CMD_REPLY, Data);
} // UsbExecute

//*****************************************************************************

//------------------------------------------------------------------------------
//  Execute
//------------------------------------------------------------------------------

static void ExecuteLong( int Size)
// Execute long command
{
int          Data;
int          RepCmd;
TUsbCmdUnion *Cmd;

   Cmd    = (void *)&UsbBuffer;
   Data   = 0;                         // reply data
   RepCmd = Cmd->Cmd;                  // reply command
   switch( RepCmd){
      case USB_CMD_WRITE :
         // write EEPROM
         if( Size > USB_MAX_DATA){
            RepCmd = USB_CMD_ERROR;
            break;
         }
         Size -= USB_CMD_SIZE;
         SaveData( Address, Cmd->Write.Data, Size);
         Address += Size;
         Data = Address;
         break;

      default :
         break;
   }
   ComTxPacket( RepCmd | USB_CMD_REPLY, Data);
} // ExecuteLong

//------------------------------------------------------------------------------
//  Save data
//------------------------------------------------------------------------------

static void SaveData( int Offset, byte *Data, int Size)
// Save data to video RAM
{
byte *p;

   p  = (byte *)GBuffer.Buffer;
   p += Address;
   memcpy( p, Data, Size);
} // SaveData
