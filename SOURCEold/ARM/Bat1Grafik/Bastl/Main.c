//******************************************************************************
//                                                                            
//  Main.c         Bat1 demo Bastl
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../inc/Cpu.h"       // CPU startup
#include "../inc/System.h"    // Operating system
#include "../inc/Kbd.h"       // Keyboard
#include "../inc/Graphic.h"   // graphic
#include "../inc/conio.h"     // Display
#include "../inc/St7259.h"    // Display (contrast)
#include "Fonts.h"            // seznam fontu
#include "Backlight.h"        // Backlight
#include "Usb.h"

// start odpocitavani 1 s :
#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Lokalni promenne :

volatile native Counter1s   = 0;         // odpocitavani 1 s
volatile native Timer1s     = 0;         // priznak odpocitani 1 s

static int CurrentBacklight = 10;
static int CurrentContrast  = GPU_EC_BASE;

#define BACKLIGHT_MAX  20
#define CONTRAST_MIN   GPU_EC_BASE - 32
#define CONTRAST_MAX   GPU_EC_BASE + 32

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
   CpuInit();   

   KbdInit();
   BacklightInit();
   GInit();
   UsbInit();

   // system run :   
   EnableInts();                       // povoleni preruseni
   SetTimer1s();                       // zahajeni odpoctu 1 s
   SysStartTimer();                    // systemovy casovac
   StartWatchDog();                    // start watchdog

   // hlavni smycka :
   CurrentBacklight = BACKLIGHT_MAX;
   CurrentContrast  = GPU_EC_BASE;
   BacklightSet( CurrentBacklight);
   BacklightOn();
   GpuContrast( CurrentContrast);
   GSetFont( FONT_8x8);
   cgoto( 0, 0);
   cputs( "Ready...");
   forever {
      UsbExecute();
      if( kbhit()){
         cgoto( 0, 0);
         switch( getch()){
            case K_ENTER :
               cclear();
               break;

            case K_ESC :
               CurrentBacklight = BACKLIGHT_MAX;
               CurrentContrast  = GPU_EC_BASE;
               BacklightSet( CurrentBacklight);
               BacklightOn();
               GpuContrast( CurrentContrast);
               break;

            case K_RIGHT :
               if( CurrentBacklight == BACKLIGHT_MAX){
                  break;
               }
               CurrentBacklight++;
               BacklightSet( CurrentBacklight);
               BacklightOn();
               break;

            case K_LEFT :
               if( CurrentBacklight == 0){
                  break;
               }
               CurrentBacklight--;
               BacklightSet( CurrentBacklight);
               BacklightOn();
               break;

            case K_UP :
               if( CurrentContrast == CONTRAST_MAX){
                  break;
               }
               CurrentContrast++;
               GpuContrast( CurrentContrast);
               break;

            case K_DOWN :
               if( CurrentContrast == CONTRAST_MIN){
                  break;
               }
               CurrentContrast--;
               GpuContrast( CurrentContrast);
               break;
         }
      }
      if( Timer1s){
         // uplynula 1s
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // zrus priznak
      }
   }
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();                       



#include "../Moduly/LPC/SysTimer.c"

