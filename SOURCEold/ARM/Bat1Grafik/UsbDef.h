//*****************************************************************************
//
//    UsbDef.h     USB protocol definition
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __UsbDef_H__
   #define __UsbDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#define USB_MAX_DATA      128           // max. packet data

//------------------------------------------------------------------------------
// USB commands
//------------------------------------------------------------------------------

typedef enum {
//  Short packet --------------------------------------------------------------
   USB_CMD_UNDEFINED,

   USB_CMD_VERSION,           // get version (no data)

   USB_CMD_ADDRESS,           // set current address( data = address)
   USB_CMD_GET_ADDRESS,       // get current address (no data)

   USB_CMD_FLUSH,             // flush video RAM
   USB_CMD_CLEAR,             // clear display

//  Long packet ---------------------------------------------------------------
   USB_CMD_WRITE,             // write data
//-----------------------------------------------------------------------------
   USB_CMD_ERROR,             // error reply
   _USB_CMD_LAST
} TUsbCmdCode;
// long packet == write data

//------------------------------------------------------------------------------
// USB long commands
//------------------------------------------------------------------------------

typedef dword TUsbCmd;                                   // command code

#define USB_CMD_SIZE      sizeof( TUsbCmd)               // command code size
#define USB_WRITE_SIZE   (USB_MAX_DATA - USB_CMD_SIZE)   // write command data sizes

// Write data (USB_CMD_WRITE) :
typedef struct {
   TUsbCmd Cmd;
   byte    Data[ USB_WRITE_SIZE];
} TUsbCmdWrite;

// Command union :
typedef union {
   TUsbCmd           Cmd;
   TUsbCmdWrite      Write;     // write data
} TUsbCmdUnion;

//------------------------------------------------------------------------------
// USB reply
//------------------------------------------------------------------------------

#define USB_CMD_REPLY  0x80   // reply flag

// USB reply data :
// USB_CMD_VERSION         version number
// USB_CMD_ADDRESS         echo address
// USB_CMD_GET_ADDRESS     address
// USB_CMD_FLUSH           echo data

// USB_CMD_WRITE           last address

#endif
