//******************************************************************************
//
//  Demo.c         Bat1 ARM demo
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../Inc/Cpu.h"      // CPU startup
#include "../Inc/System.h"   // SysDelay

// LED on P1.27 JTAG
#define LEDPIN  27

//******************************************************************************
//   Main
//******************************************************************************

int main( void)
{
   PManSetVYP();                       // hold power
   CpuInit();
   // initialize all ports :
   GPIO0_IODIR = 0xFFFFFFFF;	       // define all-Pins as output
   GPIO0_IOCLR = 0xFFFFFFFF;	       // all L

   GPIO1_IODIR = 0xFFFFFFFF;	       // define all-Pins as output
   GPIO1_IOCLR = 0xFFFFFFFF;	       // all L
   // blink LED :
   GPIO1_IOCLR = (1 << LEDPIN);	// ON - active L
   SysDelay( 2000);
   GPIO1_IOSET = (1 << LEDPIN);	// OFF
   SysDelay( 500);
   StartWatchDog();                    // start watchdog
   while( 1){
      GPIO1_IOCLR = (1 << LEDPIN);	// ON - active L
      SysDelay( 500);
      GPIO1_IOSET = (1 << LEDPIN);	// OFF
      SysDelay( 500);
      WatchDog();                       // refresh
   }
} // main
