    Poznamky k prostredi CrossWorks¨

Instalace :

1. Instalovat packages (C:\source\arm\packages)
2. Do adresare ProgramFIles\Rowley..\CrossWorks..\targets\Philips_LPC210x\LPC21xx
   zkopirovat project_templates.xml z vyse uvedeneho adresare. 
   Editovat v tomto souboru umisteni C:\source\arm podle aktualni instalace
3. Olimex ARM-USB-TINY nastavit Properties :
   JTAG Clock Dividier 10
   nTRST Open Drain No

Nastaveni editoru :

1. Opravit nastaveni indentace editoru
   v nabidce Tools/Options/Languages
   Indent Size, Indent Mode

2. Opravit velikost pisma v nabidce Tools/Options/Text editor
   Visual Appearance, Text Edit Font

Nastaveni options kompilatoru (pro novy projekt) :

1. Projekt Properties/Preprocessor Options :
   User Include Directories $(ProjectDir)
   Pozor, vlevo zvolit ve stromu Solution 'xxx'

2. Projekt Properties/Compiler Options :
   Additional Compiler Options :
   -Wall;-Wstrict-prototypes;-Wcast-align;-Wcast-qual;-Wimplicit;-Wmissing-declarations;-Wmissing-prototypes;-Wnested-externs;-Wpointer-arith;-Wswitch;-Wredundant-decls;-Wreturn-type;-Wshadow;-Wunused;-Wfloat-equal;-Wsign-compare;-Winline
   Pozor, vlevo zvolit ve stromu Solution 'xxx'

3. Projekt Properties/Compiler Options :
   ARM/Thumb interworking No
   Pozor, vlevo zvolit ve stromu Solution 'xxx'

Finalizace :
1. Pred distribuci prepnout Build na ARM Flash Release
   (zapne se watchdog)

Generovani Hex :

Project/Properties sekce Linker options, radek Post Build Command :
$(StudioDir)/gcc/bin/objcopy -Oihex "$(OutDir)"/$(ProjectName)$(EXE) $(ProjectName).hex
