Aktualizace zeme/jazyka :

- doplnit seznam jazyku CountrySet.h
- doplnit seznam zemi CountrySet.h
- doplnit tabulky zemi/jazyku CountryTable.c
- doplnit prelozene retezce do Str.c,h
- Hardware.h upravit LANGUAGE_COUNT podle poctu jazyku


- zvolit fonty ve Fonts.c
- upravit nastavovani fontu ve Fonts.c (SetFont podle codepage)


Pridani jazyka :

- Hardware.h upravit LANGUAGE_COUNT
- CountrySet.h doplnit jazyk do enumu
- Doplnit retezec s nazvem jazyka do tabulky
  prekladu (Str.txt nebo Str.xls) (POZOR,
  v poradi podle enumu)
- opravit CountryTable.c - priradit jazyk zemim
