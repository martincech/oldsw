Testovani akumulatoru
----------------------


1. Testovani Bat1 na testeru ma destrukcni ucinky.
Je treba znovu vypalit firmware a provest kalibraci
vahy a kalibraci baterie. Zapis do EEPROM se
testuje pres celou kapacitu EEPROM ruznymi
vzory. Puvodni data se pochopitelne zcela smazou.

2.Testovani akumulatoru vychazi z vlastnosti
nabijeciho obvodu. Vypinaci napeti je dle katalogoveho
listu 4.20V. Touto hodnotou je kalibrovan A/D prevodnik
pro mereni napeti baterie.

3.Testovani akumulatoru :
- akumulator se plne nabije (signalizovano signalem CHG
  nabijece)
- zmeri se napeti akumulatoru v okamziku vypnuti nabijece 
  a toto  napeti se povazuje za 4.20V. Timto napetim je
  kalibrovan prevod A/D prevodniku (Range)
- zmeri se vybijeci krivka akumulatoru (vypnuti se vyhodnoti
  narustem rychlosti poklesu napeti, v nouzi se  vypne absolutne
  pri 3.35V). Vypocitaji se aproximacni vybijeci primky
  a ulozi do EEPROM (napeti Umax, Umid, Umin).
  Zapise se cas vybijeni (Discharge)
- akumulator se plne nabije do EEPROM se doplni
   nabijeci cas (Charge)
- informace o zmerenych udajich je mozne vyvolat
  v servisnim menu Bat1 prikazem Battery Data

4. Odhad kapacity akumulatoru :
  napeti > Umax 100%
  napeti Umax..Umid 100%-50%
  napeti Umid..Umin  50%-0%
  napeti < Uwarn blikajici symbol
  Napeti v Battery Data je ve V v zavorce LSB prevodniku.
  Napeti pro plny rozsah prevodniku (1024 LSB)
  je oznaceno jako Range). Udaj Charge je cas nabijeni
  dosazeny pri kalibraci. Discharge je cas vybijeni pri
  kalibraci

5. Po destrukci kalibracnich dat v EEPROM se udaje
baterie nastavi takto :
Range se vypocte staticky z nominalnich hodnot (odporovy
delic ma zname hodnoty, referencni napeti A/D se bere 3.3V).
Ostatni hodnoty jsou 0 ! Vzhledem k vyhodnocovani stavu
baterie (viz bod 4) se baterie jevi vzdy jako "plne nabita",
aby vahu slo alespon zapnout a spustit kalibraci baterie

6. Parametry nabijeciho obvodu se testuji testerem.
- testuje se detekce pripojeni sitoveho nabijece
- testuje se detekce pripojeni USB nabijece
- zkousi se vypinaci napeti nabijece pri napajeni
   ze site a z USB.
Uspesnost testovani je data presnosti testeru a jeho
spravnou kalibraci.

