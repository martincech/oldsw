//*****************************************************************************
//
//    Hardware.h   Bat1 hardware definitions
//    Version 7.4  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "../inc/Uni.h"
#include "../inc/Lpc213x.h"
#include "../inc/Lpc.h"
#include <stddef.h>           // macro offsetof

#define __LPC213x__

#define HARDWARE_REVISION   0x740           // hardware revision

//------------------------------------------------------------------------------
//   Project options
//------------------------------------------------------------------------------

#if defined( PROJECT_COBB)
   #define PROJECT_SCREEN_VERSION_COBB         1    // version string
   #define PROJECT_WEIGHING_PRINT_ACTUAL       1    // print actual weight
   #define PROJECT_PRINTER_ACTUAL_WEIGHT       1    // print actual weight
#elif  defined( PROJECT_AVIAGEN)
   #define PROJECT_SCREEN_VERSION_AVIAGEN      1    // version string
   #define PROJECT_WEIGHING_PRINT_LAST         1    // print last saved weight
   #define PROJECT_PRINTER_ACTUAL_WEIGHT       1    // print actual weight
   #define PROJECT_PRINTER_TELXON              1    // print actual weight in Telxon format
#else
   #define PROJECT_SCREEN_VERSION              1    // version string
   #define PROJECT_WEIGHING_PRINT_SAVED        1    // print saved weight
#endif

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#define FXTAL    18432000              // XTAL frequency 18.432 MHz

#define FCPU     FXTAL                 // system frequency
#define FBUS     FCPU                  // peripheral bus frequency

#define MAM_MODE MAMCR_DISABLED        // Disable flash cache

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

#define WATCHDOG_INTERVAL 2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]

#define WATCHDOG_ENABLE   1            // Enable watchdog
//#define SLEEP_ENABLE      1          // Enable power save

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// mapa preruseni :
#define SYS_TIMER_IRQ     IRQ0         // System timer IRQ number
#define UART0_IRQ         IRQ1         // UART0 IRQ number
#define UART1_IRQ         IRQ2         // UART1 IRQ number
#define EINT2_IRQ         IRQ3         // EINT2 IRQ number (A/D convertor)

//-----------------------------------------------------------------------------
// UART0 (USB)
//-----------------------------------------------------------------------------

#define UART0_BAUD           38400      // baud rate
#define UART0_FORMAT         COM_8BITS  // default format
#define UART0_TIMEOUT        10         // intercharacter timeout [ms]

#define UART0_RX_BUFFER_SIZE   256      // rx buffer
#define UART0_TX_BUFFER_SIZE   256      // tx buffer

//-----------------------------------------------------------------------------
// UART1 (Printer)
//-----------------------------------------------------------------------------

#define UART1_BAUD           9600       // baud rate
#define UART1_FORMAT         COM_8BITS  // default format
#define UART1_TIMEOUT        10         // intercharacter timeout [ms]

//-----------------------------------------------------------------------------
// Packet 0 (USB)
//-----------------------------------------------------------------------------

#define PACKET0_RX_TIMEOUT     0        // reply timeout [ms]
#define PACKET0_MAX_DATA     128        // max. Rx packet size

#define PACKET0_TX_BLOCK                // enable long packet Tx
#define PACKET0_RX_BLOCK                // enable long packet Rx

#define USB_COM0                        // USB connected on UART 0

//------------------------------------------------------------------------------
// Keyboard
//------------------------------------------------------------------------------

#define KBD_DIR         GPIO1_IODIR   // port direction
#define KBD_PIN         GPIO1_IOPIN   // port input data
#define KBD_CLR         GPIO1_IOCLR   // port output clear

#define KBD_K0           (1 << 23)     // K0
#define KBD_K1           (1 << 24)     // K1
#define KBD_K2           (1 << 25)     // K2
#define KBD_ON           (1 << 22)     // ON/TARA

// key/events definition :
typedef enum {
   // system events :
   K_NULL  = 0,                        // menu & window excluded
   _K_FIRSTUSER,

   // keyboard :
   K_ENTER = _K_FIRSTUSER,             // Enter
   K_LEFT,                             // Left arrow
   K_ESC,                              // Esc
   K_UP,                               // Up arrow
   K_RIGHT,                            // Right arrow
   K_DOWN,                             // Down arrow
   K_BOOT,                             // Boot composed key

   // events :
   _K_EVENTS    = 0x40,                // start events
   K_FLASH1     = _K_EVENTS,           // flashing 1
   K_FLASH2,                           // flashing 2
   K_REDRAW,                           // 1s redraw
   K_SHUTDOWN,                         // power shutdown
   K_TIMEOUT,                          // inactivity timeout

   // system keys/flags :
   K_REPEAT       = 0x80,              // repeat key (ored with key)
   K_RELEASED     = 0xFE,              // release key (single and repeat)
   K_IDLE         = 0xFF               // internal use, empty read cycle
} TKeys;

// timing constants [ms] :
#define KBD_DEBOUNCE           20      // delay after first touch
#define KBD_AUTOREPEAT_START   300     // autorepeat delay
#define KBD_AUTOREPEAT_SPEED   200     // autorepeat repeat speed

//------------------------------------------------------------------------------
// Display controller ST7529
//------------------------------------------------------------------------------

// ports :
#define GPU_DIR0         GPIO0_IODIR   // port direction
#define GPU_SET0         GPIO0_IOSET   // port output set
#define GPU_CLR0         GPIO0_IOCLR   // port output clear

#define GPU_DIR1         GPIO1_IODIR   // port direction
#define GPU_SET1         GPIO1_IOSET   // port output set
#define GPU_CLR1         GPIO1_IOCLR   // port output clear

#define GPU_DATA_DIR     GPIO0_IODIR   // port direction
#define GPU_DATA_PIN     GPIO0_IOPIN   // port input data
#define GPU_DATA_SET     GPIO0_IOSET   // port output set
#define GPU_DATA_CLR     GPIO0_IOCLR   // port output clear

// pins :
#define GPU_DATA_OFFSET  16            // LSB sbernice D0..D7
#define GPU_CS           (1 << 25)     // /CS
#define GPU_RES          (1 << 12)     // /RES
#define GPU_WR           (1 << 19)     // /WR
#define GPU_A0           (1 << 18)     // A0

#define GpuInitPorts()  GPU_DIR0 |= GPU_CS | GPU_RES; GPU_DIR1 |= GPU_WR | GPU_A0
#define GpuClearAll()   GpuClrRES(); GpuDeselect(); GpuSetWR()

// chipselect :
#define GpuSelect()     GPU_CLR0 = GPU_CS
#define GpuDeselect()   GPU_SET0 = GPU_CS
// control signals :
#define GpuSetRES()     GPU_SET0 = GPU_RES
#define GpuClrRES()     GPU_CLR0 = GPU_RES
#define GpuSetWR()      GPU_SET1 = GPU_WR
#define GpuClrWR()      GPU_CLR1 = GPU_WR
#define GpuSetA0()      GPU_SET1 = GPU_A0
#define GpuClrA0()      GPU_CLR1 = GPU_A0

// Settings :
#define GPU_LF                0x00               // line cycles
#define GPU_EC_BASE           0xEB               // electronic contrast base
#define GPU_ANASET1           GPU_ANASET1_193
#define GPU_ANASET2           GPU_ANASET2_3K
#define GPU_ANASET3           GPU_ANASET3_10
#define GPU_DATSDR1           GPU_DATSDR1_CI
#define GPU_DATSDR2           GPU_DATSDR2_CLR
//#define GPU_DATSDR1           GPU_DATSDR1_LI
//#define GPU_DATSDR2           GPU_DATSDR2_NORMAL

#define CPU_COLUMN_OFFSSET    5
//#define CPU_COLUMN_OFFSSET    0

// Color palette (31..0) :
#define G_INTENSITY_BLACK      31
#define G_INTENSITY_DARKGRAY   20
#define G_INTENSITY_LIGHTGRAY  10
#define G_INTENSITY_WHITE       0

//------------------------------------------------------------------------------
// Graphics
//------------------------------------------------------------------------------

#define G_WIDTH          240           // display width (X)
#define G_HEIGHT         160           // display height (Y)
#define G_PLANES           2           // color planes count

#define GRAPHIC_CONIO      1           // enable conio.h
#define PRINTF_STRING      1           // enable string resource
#define GRAPHIC_LETTER_CENTERING 1     // fixed text centering
//#define GRAPHIC_TEXT_INDENTATION 1     // indentation

//------------------------------------------------------------------------------
//   Sound
//------------------------------------------------------------------------------

#define SND_DIR          GPIO0_IODIR   // port direction
#define SND_SET          GPIO0_IOSET   // port output set
#define SND_CLR          GPIO0_IOCLR   // port output clear

#define SND_FORCE        (1 << 28)     // volume force

#define SndInitPorts()   SND_DIR |= SND_FORCE; SndSilent()

#define SndSilent()      SND_CLR  = SND_FORCE
#define SndStrong()      SND_SET  = SND_FORCE

#define SOUND_CHANNEL          2       // PWM sound channel (1..6)
#define SOUND_ACTIVE_L         1       // PWM active level
#define SOUND_PINSEL_INTERRUPT 1       // enable PINSEL access iside irq

#define SOUND_FORCE            1       // enable controlled sound force

//------------------------------------------------------------------------------
// SPI interface
//------------------------------------------------------------------------------

// port :
#define SPI_DIR          GPIO0_IODIR   // port direction
#define SPI_PIN          GPIO0_IOPIN   // port input data
#define SPI_SET          GPIO0_IOSET   // port output set
#define SPI_CLR          GPIO0_IOCLR   // port output clear
// piny :
#define SPI_CS           (1 << 31)     // CS (STE) chipselect

#define SPI_DIVIDIER      8  // divide ratio SCK = FBUS / SPI_DIVIDIER (even, min 8)
#define SPI_CPOL          1  // default SCK level = H
#define SPI_CPHA          1  // SCK strob on return

//------------------------------------------------------------------------------
// EEPROM M95M01
//------------------------------------------------------------------------------

#define SysAccuOk()    1
#define EEP_PAGE_SIZE  256
#define EEP_SIZE       131072

//-----------------------------------------------------------------------------
// RTC RX-8025 via I2C bus
//-----------------------------------------------------------------------------

#define IIC_READ        1       // I2C read buffer
#define IIC_WRITE       1       // I2C write buffer

//-----------------------------------------------------------------------------
// A/D converter ADS1230
//-----------------------------------------------------------------------------

#define __ADS1230__                    // typ prevodniku ADS1230

#define ADC_DIR          GPIO0_IODIR   // port direction
#define ADC_PIN          GPIO0_IOPIN   // port input data
#define ADC_SET          GPIO0_IOSET   // port output set
#define ADC_CLR          GPIO0_IOCLR   // port output clear

// piny :
#define ADC_DRDY_PORT       15         // DRDY port
#define ADC_SCLK      (1 << 29)        // SCLK clock
#define ADC_PDWN      (1 << 30)        // PDWN reset/power down
#define ADC_DRDY      (1 << ADC_DRDY_PORT)  // DRDY data ready
#define ADC_DOUT      ADC_DRDY              // DOUT/DRDY common data out

#define AdcInitPorts()   ADC_DIR |= ADC_SCLK | ADC_PDWN; ADC_DIR &= ~ADC_DOUT

#define AdcSetSCLK()     ADC_SET  = ADC_SCLK
#define AdcClrSCLK()     ADC_CLR  = ADC_SCLK
#define AdcSetPDWN()     ADC_SET  = ADC_PDWN
#define AdcClrPDWN()     ADC_CLR  = ADC_PDWN
#define AdcGetDOUT()    (ADC_PIN & ADC_DOUT)

#define ADC_PINSEL_INTERRUPT 1

//-----------------------------------------------------------------------------
// A/D filter
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window
#define FILTER_SLOW_RESTART    1       // slow stabilisation after restart

// Basic data types :
typedef byte    TSamplesCount;         // samples counter

//-----------------------------------------------------------------------------
// IADC internal A/D convertor
//-----------------------------------------------------------------------------

// Enable input AD0.x :
#define IADC_AD00 0

//-----------------------------------------------------------------------------
//  Power management
//-----------------------------------------------------------------------------

#define PMAN_DIR          GPIO0_IODIR   // port direction
#define PMAN_PIN          GPIO0_IOPIN   // port input data
#define PMAN_SET          GPIO0_IOSET   // port output set
#define PMAN_CLR          GPIO0_IOCLR   // port output clear

#define PMAN_DIR1         GPIO1_IODIR   // port direction
#define PMAN_PIN1         GPIO1_IOPIN   // port input data

#define PMAN_VYP    (1 << 10)
#define PMAN_PPR    (1 << 16)
#define PMAN_CHG    (1 << 17)
#define PMAN_ENNAB  (1 << 13)
#define PMAN_USBON  (1 << 11)

#define PManInitPorts() PMAN_DIR |=  PMAN_VYP | PMAN_ENNAB; PMAN_DIR1 &= ~(PMAN_PPR | PMAN_CHG);\
                        PMAN_DIR &= ~PMAN_USBON

#define PManSetVYP()     PMAN_SET  = PMAN_VYP
#define PManClrVYP()     PMAN_CLR  = PMAN_VYP
#define PManSetENNAB()   PMAN_SET  = PMAN_ENNAB
#define PManClrENNAB()   PMAN_CLR  = PMAN_ENNAB

#define PManGetPPR()  (!(PMAN_PIN1  & PMAN_PPR))   // active L
#define PManGetCHG()    (PMAN_PIN1  & PMAN_CHG)
#define PManGetUSBON()  (PMAN_PIN   & PMAN_USBON)

#define PMAN_BOOT_KEY   K_LEFT         // boot menu enter key

//-----------------------------------------------------------------------------
// Accumulator maintenance
//-----------------------------------------------------------------------------

#define R1         15                  // upper resistor [kOhm]
#define R2         47                  // lower resitor  [kOhm]
#define VREF     3300                  // voltage reference [mV]

#define PACCU_RANGE ((VREF * (R1 + R2)) / R2) // calibration = input voltage range

#define PACCU_LOGGER_SIZE  508         // logger records count

#define ACCU_ADC_VOLTAGE     IADC_AD00 // accu voltage internal ADC input
#define ACCU_LOGGER_PERIOD   300       // logger period [s]
#define ACCU_EMPTY_VOLTAGE   3350      // accu empty treshold [mV]
#define ACCU_DELTA           5         // discharge delta [LSB]
#define ACCU_DELTA_COUNT     4         // delta count
#define ACCU_DELTA_DELAY     3600      // delta resolving delay [s]
#define ACCU_UMAX_DELAY      600       // Umax  delay by discharging start
#define ACCU_UWARN_DELAY     3600      // Uwarn delay before discharging end
// CHG status fitering :
#define ACCU_PRECHARGE_DELAY  600      // charge time without CHG check [s]
#define ACCU_CHARGED_VOLTAGE 3800      // minimum voltage for CHG test

//-----------------------------------------------------------------------------
// Secure file / accumulator logger
//-----------------------------------------------------------------------------

#define SFILE_MARKER        0xFFFF                         // marker value
#define SFILE_MARKER_SIZE   sizeof( word)                  // marker size
#define SFILE_START         offsetof( TEeprom, AccuLogger) // start address
#define SFILE_RECORD_SIZE   sizeof( TPAccuRecord)          // record size
#define SFILE_COUNT         PACCU_LOGGER_SIZE              // max. records count

//-----------------------------------------------------------------------------
//  BackLight
//-----------------------------------------------------------------------------

#define BLT_DIR          GPIO1_IODIR   // port direction
#define BLT_SET          GPIO1_IOSET   // port output set
#define BLT_CLR          GPIO1_IOCLR   // port output clear

#define BLT_SVIT  (1 << 21)

#define PwmInitPorts() BLT_DIR |= BLT_SVIT

#define BltOn()        BLT_SET = BLT_SVIT
#define BltOff()       BLT_CLR = BLT_SVIT

// PWM control :

#define SPWM_PERIOD     537            // SW PWM period [us]

#define PwmSet()        BltOn()        // port signal on
#define PwmClr()        BltOff()       // port signal off

//-----------------------------------------------------------------------------
// Printer
//-----------------------------------------------------------------------------

#define UCONIO_COM1            // printer connected on UART 1

//-----------------------------------------------------------------------------
// Date/Time
//-----------------------------------------------------------------------------

#define DT_ENABLE_DST  1        // enable daylight saving time compilation
#define DtGetDstType()     CountryGetDstType()

//#define DATE_YEAR_YYYY 1        // display full year

//-----------------------------------------------------------------------------
// Country
//-----------------------------------------------------------------------------

#define CountryGet()       Config.Country

//-----------------------------------------------------------------------------
// Language
//-----------------------------------------------------------------------------

#define LANGUAGE_COUNT 13
#define ActiveLanguage     CountryGetLanguage()

//-----------------------------------------------------------------------------
// File System
//-----------------------------------------------------------------------------

#define	FDIR_SIZE         199                    // Maximum number of files (directory entries)
#define FS_BLOCK_SIZE     64                     // block size [byte]
#define FAT_SIZE          1652                   // FAT size (total blocks count)

#define FS_OFFSET         offsetof( TEeprom, Filesystem)  // filesystem offset (from EEP start)

//-----------------------------------------------------------------------------
// Statistics
//-----------------------------------------------------------------------------

typedef double TNumber;                 // basic data type
typedef word   TStatCount;              // statistical samples count

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

typedef word THistogramCount;           // samples count
typedef long THistogramValue;           // sample value, must be signed
#define HISTOGRAM_MIN_STEP    1         // min. step in THistogramValue

#define HISTOGRAM_SLOTS          39     // columns count even/odd (max.254)
#define HISTOGRAM_NORM           1000   // normalized value by max column
#define HISTOGRAM_NORMALIZE_STEP 1      // normalize step size

//-----------------------------------------------------------------------------
// Display colors
//-----------------------------------------------------------------------------

#define DCOLOR_BACKGROUND        COLOR_WHITE        // standard background
#define DCOLOR_DEFAULT           COLOR_BLACK        // default color
#define DCOLOR_TITLE             COLOR_WHITE        // title text
#define DCOLOR_TITLE_BG          COLOR_BLACK        // title background
#define DCOLOR_CURSOR            COLOR_DARKGRAY     // cursor color
#define DCOLOR_STATUS            COLOR_BLACK        // status line text
#define DCOLOR_STATUS_BG         COLOR_LIGHTGRAY    // status line background
#define DCOLOR_STATUS_LINE       COLOR_DARKGRAY     // status line separator line
#define DCOLOR_ENTER             COLOR_WHITE        // enter area text
#define DCOLOR_ENTER_BG          COLOR_BLACK        // enter area background
#define DCOLOR_ENTER_CURSOR      COLOR_BLACK        // enter area cursor
#define DCOLOR_PROGRESS          COLOR_BLACK        // progress bar frame
#define DCOLOR_PROGRESS_BAR      COLOR_DARKGRAY     // progress bar filler
#define DCOLOR_SHADOW            COLOR_LIGHTGRAY    // shadow color
#define DCOLOR_HISTOGRAM_CURSOR  COLOR_DARKGRAY     // histogram cursor

//-----------------------------------------------------------------------------
// Display commons
//-----------------------------------------------------------------------------

#define DCURSOR_R            2              // coursor round corners

#define DENTER_YEAR_YYYY     1              // use YYYY year for edit
#define DENTER_CALLBACK      1              // enable action callback
#define DENTER_H             19             // enter field height

#define DINPUT_CAPTION_Y     45             // input field caption
#define DINPUT_EDIT_Y        80             // input field
#define DINPUT_EDIT_H        19             // input field height
#define DINPUT_ENABLE_RANGE  1              // enable DInputNumber range display

#define DMENU_PARAMETERS_X   G_WIDTH - 5    // menu parameters start x
#define DMENU_EDIT_X         G_WIDTH - 5    // edit parameters start x

#define DMENU_FONT           TAHOMA16       // menu font
#define DINPUT_FONT          TAHOMA16       // input dialogs font
#define DEDIT_FONT           TAHOMA16       // edit field font
#define DDIR_FONT            TAHOMA16       // directory box font
#define DSAMPLES_FONT        TAHOMA16       // view samples font
#define DMSG_FONT            TAHOMA16       // message box font
#define DLAYOUT_FONT         TAHOMA16       // title/status line font
#define DHISTOGRAM_FONT      TAHOMA16       // histogram font

#define DALIGN_RIGHT         1              // edit fields aligned right
#define DLABEL_MAX_LENGTH    63             // max. label length

//------------------------------------------------------------------------------

#include "Config.h"
#include "../inc/Country.h"

#endif
