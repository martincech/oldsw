﻿namespace Bat1TestCmd {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         this.ComTextBox = new System.Windows.Forms.TextBox();
         this.ConnectButton = new System.Windows.Forms.Button();
         this.TestInterfaceButton = new System.Windows.Forms.Button();
         this.OnOffCheckBox = new System.Windows.Forms.CheckBox();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.K0CheckBox = new System.Windows.Forms.CheckBox();
         this.K1CheckBox = new System.Windows.Forms.CheckBox();
         this.K2CheckBox = new System.Windows.Forms.CheckBox();
         this.TestSoundButton = new System.Windows.Forms.Button();
         this.TestRtcButton = new System.Windows.Forms.Button();
         this.BacklightCheckbox = new System.Windows.Forms.CheckBox();
         this.AdcLabel = new System.Windows.Forms.Label();
         this.IadcLabel = new System.Windows.Forms.Label();
         this.TestDisplayButton = new System.Windows.Forms.Button();
         this.TestMemoryButton = new System.Windows.Forms.Button();
         this.SwitchOffButton = new System.Windows.Forms.Button();
         this.ChargerChecbox = new System.Windows.Forms.CheckBox();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.KeyboardContinuousCheckBox = new System.Windows.Forms.CheckBox();
         this.OutputTextBox = new System.Windows.Forms.TextBox();
         this.AdcContinuousCheckBox = new System.Windows.Forms.CheckBox();
         this.IadcContinuousCheckBox = new System.Windows.Forms.CheckBox();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.SuspendLayout();
         // 
         // ComTextBox
         // 
         this.ComTextBox.Location = new System.Drawing.Point(12, 12);
         this.ComTextBox.Name = "ComTextBox";
         this.ComTextBox.Size = new System.Drawing.Size(100, 20);
         this.ComTextBox.TabIndex = 0;
         this.ComTextBox.Text = "COM1";
         // 
         // ConnectButton
         // 
         this.ConnectButton.Location = new System.Drawing.Point(118, 12);
         this.ConnectButton.Name = "ConnectButton";
         this.ConnectButton.Size = new System.Drawing.Size(75, 23);
         this.ConnectButton.TabIndex = 1;
         this.ConnectButton.Text = "Connect";
         this.ConnectButton.UseVisualStyleBackColor = true;
         this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
         // 
         // TestInterfaceButton
         // 
         this.TestInterfaceButton.Location = new System.Drawing.Point(270, 10);
         this.TestInterfaceButton.Name = "TestInterfaceButton";
         this.TestInterfaceButton.Size = new System.Drawing.Size(123, 23);
         this.TestInterfaceButton.TabIndex = 8;
         this.TestInterfaceButton.Text = "Test interface";
         this.TestInterfaceButton.UseVisualStyleBackColor = true;
         this.TestInterfaceButton.Click += new System.EventHandler(this.TestInterfaceButton_Click);
         // 
         // OnOffCheckBox
         // 
         this.OnOffCheckBox.AutoSize = true;
         this.OnOffCheckBox.Enabled = false;
         this.OnOffCheckBox.Location = new System.Drawing.Point(2, 46);
         this.OnOffCheckBox.Name = "OnOffCheckBox";
         this.OnOffCheckBox.Size = new System.Drawing.Size(57, 17);
         this.OnOffCheckBox.TabIndex = 9;
         this.OnOffCheckBox.Text = "On/off";
         this.OnOffCheckBox.UseVisualStyleBackColor = true;
         // 
         // timer1
         // 
         this.timer1.Enabled = true;
         this.timer1.Interval = 1000;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // K0CheckBox
         // 
         this.K0CheckBox.AutoSize = true;
         this.K0CheckBox.Enabled = false;
         this.K0CheckBox.Location = new System.Drawing.Point(65, 46);
         this.K0CheckBox.Name = "K0CheckBox";
         this.K0CheckBox.Size = new System.Drawing.Size(39, 17);
         this.K0CheckBox.TabIndex = 10;
         this.K0CheckBox.Text = "K0";
         this.K0CheckBox.UseVisualStyleBackColor = true;
         // 
         // K1CheckBox
         // 
         this.K1CheckBox.AutoSize = true;
         this.K1CheckBox.Enabled = false;
         this.K1CheckBox.Location = new System.Drawing.Point(110, 46);
         this.K1CheckBox.Name = "K1CheckBox";
         this.K1CheckBox.Size = new System.Drawing.Size(39, 17);
         this.K1CheckBox.TabIndex = 11;
         this.K1CheckBox.Text = "K1";
         this.K1CheckBox.UseVisualStyleBackColor = true;
         // 
         // K2CheckBox
         // 
         this.K2CheckBox.AutoSize = true;
         this.K2CheckBox.Enabled = false;
         this.K2CheckBox.Location = new System.Drawing.Point(155, 46);
         this.K2CheckBox.Name = "K2CheckBox";
         this.K2CheckBox.Size = new System.Drawing.Size(39, 17);
         this.K2CheckBox.TabIndex = 12;
         this.K2CheckBox.Text = "K2";
         this.K2CheckBox.UseVisualStyleBackColor = true;
         // 
         // TestSoundButton
         // 
         this.TestSoundButton.Location = new System.Drawing.Point(318, 77);
         this.TestSoundButton.Name = "TestSoundButton";
         this.TestSoundButton.Size = new System.Drawing.Size(75, 23);
         this.TestSoundButton.TabIndex = 13;
         this.TestSoundButton.Text = "Test sound";
         this.TestSoundButton.UseVisualStyleBackColor = true;
         this.TestSoundButton.Click += new System.EventHandler(this.TestSoundButton_Click);
         // 
         // TestRtcButton
         // 
         this.TestRtcButton.Location = new System.Drawing.Point(318, 106);
         this.TestRtcButton.Name = "TestRtcButton";
         this.TestRtcButton.Size = new System.Drawing.Size(75, 23);
         this.TestRtcButton.TabIndex = 14;
         this.TestRtcButton.Text = "Test RTC";
         this.TestRtcButton.UseVisualStyleBackColor = true;
         this.TestRtcButton.Click += new System.EventHandler(this.TestRtcButton_Click);
         // 
         // BacklightCheckbox
         // 
         this.BacklightCheckbox.AutoSize = true;
         this.BacklightCheckbox.Location = new System.Drawing.Point(319, 193);
         this.BacklightCheckbox.Name = "BacklightCheckbox";
         this.BacklightCheckbox.Size = new System.Drawing.Size(74, 17);
         this.BacklightCheckbox.TabIndex = 15;
         this.BacklightCheckbox.Text = "BackLight";
         this.BacklightCheckbox.UseVisualStyleBackColor = true;
         this.BacklightCheckbox.CheckedChanged += new System.EventHandler(this.BacklightCheckbox_CheckedChanged);
         // 
         // AdcLabel
         // 
         this.AdcLabel.AutoSize = true;
         this.AdcLabel.Location = new System.Drawing.Point(42, 42);
         this.AdcLabel.Name = "AdcLabel";
         this.AdcLabel.Size = new System.Drawing.Size(13, 13);
         this.AdcLabel.TabIndex = 16;
         this.AdcLabel.Text = "?";
         // 
         // IadcLabel
         // 
         this.IadcLabel.AutoSize = true;
         this.IadcLabel.Location = new System.Drawing.Point(134, 42);
         this.IadcLabel.Name = "IadcLabel";
         this.IadcLabel.Size = new System.Drawing.Size(13, 13);
         this.IadcLabel.TabIndex = 17;
         this.IadcLabel.Text = "?";
         // 
         // TestDisplayButton
         // 
         this.TestDisplayButton.Location = new System.Drawing.Point(318, 135);
         this.TestDisplayButton.Name = "TestDisplayButton";
         this.TestDisplayButton.Size = new System.Drawing.Size(75, 23);
         this.TestDisplayButton.TabIndex = 18;
         this.TestDisplayButton.Text = "Test display";
         this.TestDisplayButton.UseVisualStyleBackColor = true;
         this.TestDisplayButton.Click += new System.EventHandler(this.TestDisplayButton_Click);
         // 
         // TestMemoryButton
         // 
         this.TestMemoryButton.Location = new System.Drawing.Point(318, 164);
         this.TestMemoryButton.Name = "TestMemoryButton";
         this.TestMemoryButton.Size = new System.Drawing.Size(75, 23);
         this.TestMemoryButton.TabIndex = 19;
         this.TestMemoryButton.Text = "Test memory";
         this.TestMemoryButton.UseVisualStyleBackColor = true;
         this.TestMemoryButton.Click += new System.EventHandler(this.TestMemoryButton_Click);
         // 
         // SwitchOffButton
         // 
         this.SwitchOffButton.Location = new System.Drawing.Point(318, 39);
         this.SwitchOffButton.Name = "SwitchOffButton";
         this.SwitchOffButton.Size = new System.Drawing.Size(75, 23);
         this.SwitchOffButton.TabIndex = 20;
         this.SwitchOffButton.Text = "Switch off";
         this.SwitchOffButton.UseVisualStyleBackColor = true;
         this.SwitchOffButton.Click += new System.EventHandler(this.TestOffButton_Click);
         // 
         // ChargerChecbox
         // 
         this.ChargerChecbox.AutoSize = true;
         this.ChargerChecbox.Location = new System.Drawing.Point(319, 216);
         this.ChargerChecbox.Name = "ChargerChecbox";
         this.ChargerChecbox.Size = new System.Drawing.Size(63, 17);
         this.ChargerChecbox.TabIndex = 21;
         this.ChargerChecbox.Text = "Charger";
         this.ChargerChecbox.UseVisualStyleBackColor = true;
         this.ChargerChecbox.CheckedChanged += new System.EventHandler(this.ChargerChecbox_CheckedChanged);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.label3);
         this.groupBox1.Controls.Add(this.KeyboardContinuousCheckBox);
         this.groupBox1.Controls.Add(this.OnOffCheckBox);
         this.groupBox1.Controls.Add(this.K0CheckBox);
         this.groupBox1.Controls.Add(this.K1CheckBox);
         this.groupBox1.Controls.Add(this.K2CheckBox);
         this.groupBox1.Location = new System.Drawing.Point(12, 267);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(200, 82);
         this.groupBox1.TabIndex = 22;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Keyboard";
         // 
         // KeyboardContinuousCheckBox
         // 
         this.KeyboardContinuousCheckBox.AutoSize = true;
         this.KeyboardContinuousCheckBox.Location = new System.Drawing.Point(6, 19);
         this.KeyboardContinuousCheckBox.Name = "KeyboardContinuousCheckBox";
         this.KeyboardContinuousCheckBox.Size = new System.Drawing.Size(15, 14);
         this.KeyboardContinuousCheckBox.TabIndex = 13;
         this.KeyboardContinuousCheckBox.UseVisualStyleBackColor = true;
         // 
         // OutputTextBox
         // 
         this.OutputTextBox.Location = new System.Drawing.Point(12, 41);
         this.OutputTextBox.Multiline = true;
         this.OutputTextBox.Name = "OutputTextBox";
         this.OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.OutputTextBox.Size = new System.Drawing.Size(301, 220);
         this.OutputTextBox.TabIndex = 23;
         // 
         // AdcContinuousCheckBox
         // 
         this.AdcContinuousCheckBox.AutoSize = true;
         this.AdcContinuousCheckBox.Location = new System.Drawing.Point(40, 19);
         this.AdcContinuousCheckBox.Name = "AdcContinuousCheckBox";
         this.AdcContinuousCheckBox.Size = new System.Drawing.Size(15, 14);
         this.AdcContinuousCheckBox.TabIndex = 24;
         this.AdcContinuousCheckBox.UseVisualStyleBackColor = true;
         // 
         // IadcContinuousCheckBox
         // 
         this.IadcContinuousCheckBox.AutoSize = true;
         this.IadcContinuousCheckBox.Location = new System.Drawing.Point(132, 19);
         this.IadcContinuousCheckBox.Name = "IadcContinuousCheckBox";
         this.IadcContinuousCheckBox.Size = new System.Drawing.Size(15, 14);
         this.IadcContinuousCheckBox.TabIndex = 25;
         this.IadcContinuousCheckBox.UseVisualStyleBackColor = true;
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.label2);
         this.groupBox2.Controls.Add(this.label1);
         this.groupBox2.Controls.Add(this.AdcContinuousCheckBox);
         this.groupBox2.Controls.Add(this.IadcContinuousCheckBox);
         this.groupBox2.Controls.Add(this.AdcLabel);
         this.groupBox2.Controls.Add(this.IadcLabel);
         this.groupBox2.Location = new System.Drawing.Point(218, 267);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(200, 82);
         this.groupBox2.TabIndex = 26;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Adc";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(100, 42);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(31, 13);
         this.label2.TabIndex = 27;
         this.label2.Text = "Iadc:";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(10, 42);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(29, 13);
         this.label1.TabIndex = 26;
         this.label1.Text = "Adc:";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(134, 19);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(60, 13);
         this.label3.TabIndex = 27;
         this.label3.Text = "Continuous";
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(489, 354);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.OutputTextBox);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.ChargerChecbox);
         this.Controls.Add(this.SwitchOffButton);
         this.Controls.Add(this.TestMemoryButton);
         this.Controls.Add(this.TestDisplayButton);
         this.Controls.Add(this.BacklightCheckbox);
         this.Controls.Add(this.TestRtcButton);
         this.Controls.Add(this.TestSoundButton);
         this.Controls.Add(this.TestInterfaceButton);
         this.Controls.Add(this.ConnectButton);
         this.Controls.Add(this.ComTextBox);
         this.Name = "Form1";
         this.Text = "Form1";
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox ComTextBox;
      private System.Windows.Forms.Button ConnectButton;
      private System.Windows.Forms.Button TestInterfaceButton;
      private System.Windows.Forms.CheckBox OnOffCheckBox;
      private System.Windows.Forms.Timer timer1;
      private System.Windows.Forms.CheckBox K0CheckBox;
      private System.Windows.Forms.CheckBox K1CheckBox;
      private System.Windows.Forms.CheckBox K2CheckBox;
      private System.Windows.Forms.Button TestSoundButton;
      private System.Windows.Forms.Button TestRtcButton;
      private System.Windows.Forms.CheckBox BacklightCheckbox;
      private System.Windows.Forms.Label AdcLabel;
      private System.Windows.Forms.Label IadcLabel;
      private System.Windows.Forms.Button TestDisplayButton;
      private System.Windows.Forms.Button TestMemoryButton;
      private System.Windows.Forms.Button SwitchOffButton;
      private System.Windows.Forms.CheckBox ChargerChecbox;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.TextBox OutputTextBox;
      private System.Windows.Forms.CheckBox KeyboardContinuousCheckBox;
      private System.Windows.Forms.CheckBox AdcContinuousCheckBox;
      private System.Windows.Forms.CheckBox IadcContinuousCheckBox;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label3;
   }
}

