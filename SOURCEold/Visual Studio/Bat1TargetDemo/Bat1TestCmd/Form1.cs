﻿using System;
using System.Windows.Forms;
using Device.Bat1;


namespace Bat1TestCmd {
   public partial class Form1 : Form {


      private Target TestInterface = new Target();

      private Crt InfoWindow;

      private struct ErrorCounter {
         public int TestsDone;
         public int ErrorsCount;

         public ErrorCounter(int TestsDoneInit, int ErrorsCountInit) : this() {
            TestsDone = TestsDoneInit;
            ErrorsCount = ErrorsCountInit;
         }
      }

      ErrorCounter KeyboardCounter = new ErrorCounter(0, 0);
      ErrorCounter AdcCounter = new ErrorCounter(0, 0);
      ErrorCounter IadcCounter = new ErrorCounter(0, 0);

      public Form1() {
         InitializeComponent();

         InfoWindow = new Crt(OutputTextBox);
      }



      private void ConnectButton_Click(object sender, EventArgs e) {
         if(!TestInterface.Open()) {
            InfoWindow.ShowStatus("Can't open port", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Port opened", Crt.StatusFlag.Info);
      }

      private void TestInterfaceButton_Click(object sender, EventArgs e) {
         if(!TestInterface.TestUart()) {
            InfoWindow.ShowStatus("Test interface FAIL", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Test interface OK", Crt.StatusFlag.Info);
      }

      private void timer1_Tick(object sender, EventArgs e) {
         
         int AdcValue = 0;

         if(AdcContinuousCheckBox.Checked) {
            AdcCounter.TestsDone++;

            if(!TestInterface.ReadAdc(ref AdcValue)) {
               AdcCounter.ErrorsCount++;
               InfoWindow.ShowStatus("Unable read ADC. Error counter " + AdcCounter.ErrorsCount.ToString() + "/" + AdcCounter.TestsDone.ToString(), Crt.StatusFlag.Error);
            } else {
               AdcLabel.Text = AdcValue.ToString();
            }
         }

         if(IadcContinuousCheckBox.Checked) {
            if(!TestInterface.ReadIadc(ref AdcValue)) {
               InfoWindow.ShowStatus("Unable read IADC", Crt.StatusFlag.Error);
            } else {
               IadcLabel.Text = AdcValue.ToString();
            }
         }

         if(KeyboardContinuousCheckBox.Checked) {
            Bat1Tester.Keyboard Keyboard = new Bat1Tester.Keyboard();

            if(!TestInterface.TestKeyboard(ref Keyboard)) {
               InfoWindow.ShowStatus("Unable read keyboard", Crt.StatusFlag.Error);
            } else {
               OnOffCheckBox.Checked = Keyboard.On.State == Bat1Tester.Keyboard.Key.KeyState.PUSHED ? true : false;
               K0CheckBox.Checked = Keyboard.K0.State == Bat1Tester.Keyboard.Key.KeyState.PUSHED ? true : false;
               K1CheckBox.Checked = Keyboard.K1.State == Bat1Tester.Keyboard.Key.KeyState.PUSHED ? true : false;
               K2CheckBox.Checked = Keyboard.K2.State == Bat1Tester.Keyboard.Key.KeyState.PUSHED ? true : false;
            }
         }


      }

      private void TestSoundButton_Click(object sender, EventArgs e) {
         if(!TestInterface.TestSound()) {
            InfoWindow.ShowStatus("Test sound", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Test sound done", Crt.StatusFlag.Info);
      }

      private void TestRtcButton_Click(object sender, EventArgs e) {
         if(!TestInterface.TestRtc()) {
            InfoWindow.ShowStatus("Test RTC", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Test RTC done", Crt.StatusFlag.Info);
      }

      private void BacklightCheckbox_CheckedChanged(object sender, EventArgs e) {
         
         if(!TestInterface.SetBacklight(((CheckBox)sender).Checked)) {
            InfoWindow.ShowStatus("Backlight", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Backlight switched on/off", Crt.StatusFlag.Info);
      }

      private void TestDisplayButton_Click(object sender, EventArgs e) {
         if(!TestInterface.TestDisplay()) {
            InfoWindow.ShowStatus("Display", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Display test done", Crt.StatusFlag.Info);
      }

      private void TestMemoryButton_Click(object sender, EventArgs e) {
         if(!TestInterface.TestMemory()) {
            InfoWindow.ShowStatus("Memory", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Memory test done", Crt.StatusFlag.Info);
      }

      private void TestOffButton_Click(object sender, EventArgs e) {
         if(!TestInterface.SwitchOff()) {
            InfoWindow.ShowStatus("Switch off", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Device switched off - please connect again", Crt.StatusFlag.Info);

         TestInterface.Close();
      }

      private void ChargerChecbox_CheckedChanged(object sender, EventArgs e) {
         if(!TestInterface.EnableCharger(((CheckBox)sender).Checked)) {
            InfoWindow.ShowStatus("Charger", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Charger switched on/off", Crt.StatusFlag.Info);
      }
   }
}
