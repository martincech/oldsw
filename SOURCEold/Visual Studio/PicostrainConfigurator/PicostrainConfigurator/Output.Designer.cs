﻿namespace PicostrainConfigurator {
   partial class Output {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.OutputTextBox = new System.Windows.Forms.TextBox();
         this.SuspendLayout();
         // 
         // OutputTextBox
         // 
         this.OutputTextBox.Location = new System.Drawing.Point(12, 12);
         this.OutputTextBox.Multiline = true;
         this.OutputTextBox.Name = "OutputTextBox";
         this.OutputTextBox.Size = new System.Drawing.Size(234, 379);
         this.OutputTextBox.TabIndex = 0;
         // 
         // Output
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(258, 404);
         this.Controls.Add(this.OutputTextBox);
         this.Name = "Output";
         this.Text = "Output";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox OutputTextBox;
   }
}