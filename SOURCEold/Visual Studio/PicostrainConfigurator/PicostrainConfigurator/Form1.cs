﻿using System;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Picostrain;

namespace PicostrainConfigurator {
   public partial class Form1 : Form {
      public class ValuePair {
         private string _Key;
         private int _Value;

         public ValuePair(string Key, int Value) {
            _Key = Key;
            _Value = Value;
         }

         public string Key {
            get { return _Key; }
         }
         public int Value {
            get { return _Value; }
         }
      }

      private int[] DefaultVals = new int[] { 0x28828A, 0xAFC287, 0x5284BE, 0x020219, 0x400000, 0x400000, 0x100000, 0x100000, 0x266666, 0x000000, 0x1400A0, 0xAA045A, 0xE8E728, 0x8045C0, 0x000028, 0x000000, 0, 0, 0, 0, 0, 0 ,0 ,0 ,0 };
      
      private Label[] BitLabel;
      private Label[] RegistersLabel;
      private TextBox[] IOTextBox;
      private Control[,] FieldsControl;
      private Label[,] FieldNameLabel;
      private ToolTip[,] FieldToolTip;
      RegistersSection Config;
      private int DeltaX;

      private int My2Dec(string Bin) {
         int Base = 10;
         
         if(Bin == null) {
            return 0;
         }

         if(Bin.Length >= 2) {
            if(Bin.Substring(0, 2) == "0b") {
               Bin = Bin.Substring(2, Bin.Length - 2);
               Base = 2;
            } else if(Bin.Substring(0, 2) == "0x") {
               Bin = Bin.Substring(2, Bin.Length - 2);
               Base = 16;
            }
         }
         try {
            return Convert.ToInt32(Bin, Base);
         } catch(Exception) {
            return 0;
         }
      }

      public int ComputeValue(int Reg, string DefaultVal) {
         int Bits;
         int BitStart;
         int Value;
         int Base;
         int RegValue;

         RegValue = My2Dec(DefaultVal);

         Control Cnt;

         for(int i = 0; i < Config.Bits; i++) {
            Cnt = FieldsControl[Reg, i];

            if(Cnt == null) {
               return RegValue;
            }

            Bits = Cnt.Size.Width / DeltaX;
            BitStart = -((Cnt.Location.X - ModelFieldsTextBox.Location.X) / ModelFieldsTextBox.Size.Width - (Config.Bits - 1)) - Bits + 1;

            if(Cnt.GetType() == typeof(TextBox)) {
               TextBox FieldTextBox = (TextBox) Cnt;

               Value = My2Dec(FieldTextBox.Text);
            } else {
               ComboBox FieldComboBox = (ComboBox)Cnt;
               Value = (int)FieldComboBox.SelectedValue;
            }

            System.Diagnostics.Debug.WriteLine("Mask: " + (((2 << (Bits - 1)) - 1) << BitStart));
            RegValue &= ~(((2 << (Bits - 1)) - 1) << BitStart);
            System.Diagnostics.Debug.WriteLine("Value: " + RegValue.ToString());
            RegValue |= (Value & ((2 << (Bits - 1)) - 1)) << BitStart;

            System.Diagnostics.Debug.WriteLine("Bits: " + Bits.ToString() + ", BitStart: " + BitStart.ToString() + ", Value: " + Value.ToString());

            System.Diagnostics.Debug.WriteLine("Value: " + RegValue.ToString());
         }

         return RegValue;
      }

      public Form1() {
         int i;
         InitializeComponent();

         Config = ConfigurationManager.GetSection("RegistersSection") as RegistersSection;

         BitLabel = new Label[Config.Bits];

         DeltaX = ModelBitLabel22.Location.X - ModelBitLabel23.Location.X;

         ModelFieldsComboBox.Size = new System.Drawing.Size(DeltaX, ModelFieldsTextBox.Size.Height);
         ModelFieldsTextBox.Size = new System.Drawing.Size(DeltaX, ModelFieldsTextBox.Size.Height);

         for(i = Config.Bits - 1; i >= 0; i--) {
            BitLabel[i] = new Label();
            BitLabel[i].AutoSize = ModelBitLabel23.AutoSize;
            BitLabel[i].Location = new System.Drawing.Point(ModelBitLabel23.Location.X + (Config.Bits - 1 - i) * DeltaX, ModelBitLabel23.Location.Y);
            BitLabel[i].Name = "Label" + i.ToString();
            BitLabel[i].Size = ModelBitLabel23.Size;
            BitLabel[i].TabIndex = ModelBitLabel23.TabIndex;
            BitLabel[i].Text = i.ToString();
            this.Controls.Add(BitLabel[i]);
         }


         FieldsControl = new Control[Config.Registers.Count, Config.Bits];
         FieldNameLabel = new Label[Config.Registers.Count, Config.Bits];
         FieldToolTip = new ToolTip[Config.Registers.Count, Config.Bits];
         RegistersLabel = new Label[Config.Registers.Count];
         IOTextBox = new TextBox[Config.Registers.Count];

         i = 0;
         int DeltaY = Math.Abs(ModelFieldNameLabel.Location.Y - ModelFieldsTextBox.Location.Y) + ModelFieldNameLabel.Size.Height + 15;

         foreach(RegistersSection.TRegistersCollection.TRegister Register in Config.Registers) {
            RegistersLabel[i] = new Label();
            RegistersLabel[i].AutoSize = ModelRegistersLabel.AutoSize;
            RegistersLabel[i].Location = new System.Drawing.Point(ModelRegistersLabel.Location.X, ModelRegistersLabel.Location.Y + i * DeltaY);
            RegistersLabel[i].Name = Register.Name + "Label";
            RegistersLabel[i].Size = ModelRegistersLabel.Size;
            RegistersLabel[i].TabIndex = ModelRegistersLabel.TabIndex;
            RegistersLabel[i].Text = Register.Name;
            this.Controls.Add(RegistersLabel[i]);

            IOTextBox[i] = new TextBox();
            IOTextBox[i].Location = new System.Drawing.Point(ModelIOTextBox.Location.X, ModelIOTextBox.Location.Y + i * DeltaY);
            IOTextBox[i].Name = Register.Name + "IO";
            IOTextBox[i].Size = new System.Drawing.Size(72, 20);
            IOTextBox[i].TabIndex = 29;
            IOTextBox[i].Text = String.Format("0x{0," + Config.Bits / 4 + ":X" + Config.Bits / 4 + "}", DefaultVals[i]);
            this.Controls.Add(IOTextBox[i]);

            int j = 0;

            foreach(RegistersSection.TRegistersCollection.TRegister.TFieldsCollection.TField Fields in Register.Fields) {
               FieldNameLabel[i, j] = new Label();
               FieldNameLabel[i, j].AutoSize = ModelFieldNameLabel.AutoSize;
               FieldNameLabel[i, j].Location = new System.Drawing.Point(ModelFieldNameLabel.Location.X + ModelFieldsTextBox.Size.Width * (Config.Bits - 1 - Fields.BitStart), ModelFieldNameLabel.Location.Y + i * DeltaY);
               FieldNameLabel[i, j].Name = Register.Name + Fields.Name + "Label";
               FieldNameLabel[i, j].Size = ModelFieldNameLabel.Size;
               FieldNameLabel[i, j].TabIndex = ModelFieldNameLabel.TabIndex;
               FieldNameLabel[i, j].Text = Fields.Name;
               
               this.Controls.Add(FieldNameLabel[i, j]);
               Controls.SetChildIndex(FieldNameLabel[i, j], Config.Bits - 1 - j);

               FieldToolTip[i, j] = new System.Windows.Forms.ToolTip();
               FieldToolTip[i, j].SetToolTip(FieldNameLabel[i, j], Fields.Description);



               if(Fields.Values.Count == 0) {
                  TextBox FieldTextBox;
                  FieldTextBox = new TextBox();
                  FieldTextBox.Location = new System.Drawing.Point(ModelFieldsTextBox.Location.X + ModelFieldsTextBox.Size.Width * (Config.Bits - 1 - Fields.BitStart), ModelFieldsTextBox.Location.Y + i * DeltaY);
                  FieldTextBox.Multiline = ModelFieldsTextBox.Multiline;
                  FieldTextBox.Name = Register.Name + Fields.Name + "TextBox";
                  FieldTextBox.ScrollBars = ModelFieldsTextBox.ScrollBars;
                  FieldTextBox.Size = new System.Drawing.Size(ModelFieldsTextBox.Size.Width * (Fields.BitStart - Fields.BitStop + 1), ModelFieldsTextBox.Size.Height);
                  FieldTextBox.TabIndex = ModelFieldsTextBox.TabIndex;
                  FieldTextBox.Enabled = Fields.Enable;

                  this.Controls.Add(FieldTextBox);

                  FieldToolTip[i, j].SetToolTip(FieldTextBox, Fields.Description);

                  FieldsControl[i, j] = (Control) FieldTextBox;
               } else {
                  ComboBox FieldComboBox;

                  FieldComboBox = new System.Windows.Forms.ComboBox();
                  FieldComboBox.FormattingEnabled = true;
                  FieldComboBox.Location = new System.Drawing.Point(ModelFieldsComboBox.Location.X + ModelFieldsComboBox.Size.Width * (Config.Bits - 1 - Fields.BitStart), ModelFieldsComboBox.Location.Y + i * DeltaY);
                  FieldComboBox.Name = Register.Name + Fields.Name + "ComboBox";
                  FieldComboBox.Size = new System.Drawing.Size(ModelFieldsComboBox.Size.Width * (Fields.BitStart - Fields.BitStop + 1), ModelFieldsComboBox.Size.Height);
                  FieldComboBox.TabIndex = 26;
                  FieldComboBox.DropDownWidth = ModelFieldsComboBox.DropDownWidth;

                  ArrayList List = new ArrayList();

                  foreach(RegistersSection.TRegistersCollection.TRegister.TFieldsCollection.TField.TValuesCollection.TValue Values in Fields.Values) {
                     List.Add(new ValuePair(Values.Description, My2Dec(Values.Value)));
                  }

                  FieldComboBox.BindingContext = new BindingContext();
                  FieldComboBox.DisplayMember = "Key";
                  FieldComboBox.ValueMember = "Value";
                  FieldComboBox.DataSource = List;
                  this.Controls.Add(FieldComboBox);

                  FieldToolTip[i, j].SetToolTip(FieldComboBox, Fields.Description);

                  FieldsControl[i, j] = (Control)FieldComboBox;
               }
               
               j++;
            }

            DoSet(i);
            i++;
            
         }

         this.PerformLayout();

         ModelFieldNameLabel.Visible = false;
         ModelBitLabel23.Visible = false;
         ModelBitLabel22.Visible = false;
         ModelFieldsComboBox.Visible = false;
         ModelFieldsTextBox.Visible = false;
         ModelRegistersLabel.Visible = false;
         ModelIOTextBox.Visible = false;
      }

      private void button1_Click(object sender, EventArgs e) {
         int i = 0;
         string Value;

         string Str = "";

         foreach(RegistersSection.TRegistersCollection.TRegister Register in Config.Registers) {
            Value = "0x" + String.Format("{0," + Config.Bits / 4 + ":X" + Config.Bits / 4 + "}", ComputeValue(i, Config.Registers[i].DefaultVal));
            IOTextBox[i].Text = Value;
            Str += "equal 0x" + Value + "   ; " + Config.Registers[i].Name + "\r\n";

            i++;
         }

         Output OutputForm = new Output(Str);
         OutputForm.ShowDialog();
      }

      private void DoSet(int Reg) {
         int Bits;
         int BitStart;
         int RegValue;
         int Value;

         RegValue = My2Dec(IOTextBox[Reg].Text);

         Control Cnt;

         for(int i = 0; i < Config.Bits; i++) {
            Cnt = FieldsControl[Reg, i];

            if(Cnt == null) {
               return;
            }

            Bits = Cnt.Size.Width / DeltaX;
            BitStart = -((Cnt.Location.X - ModelFieldsTextBox.Location.X) / ModelFieldsTextBox.Size.Width - (Config.Bits - 1)) - Bits + 1;
            Value = (RegValue >> BitStart) & ((2 << (Bits - 1)) - 1);

            if(Cnt.GetType() == typeof(TextBox)) {
               TextBox FieldTextBox = (TextBox) Cnt;

               FieldTextBox.Text = Value.ToString();
            } else {
               ComboBox FieldComboBox = (ComboBox)Cnt;

               FieldComboBox.SelectedValue = Value;
            }
         }
      }

      private void SetButton_Click(object sender, EventArgs e) {
         int i = 0;

         foreach(RegistersSection.TRegistersCollection.TRegister Register in Config.Registers) {
            DoSet(i);

            i++;
         }
         
      }
   }
}
