﻿namespace PicostrainConfigurator {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.ModelBitLabel23 = new System.Windows.Forms.Label();
         this.ModelBitLabel22 = new System.Windows.Forms.Label();
         this.ModelRegistersLabel = new System.Windows.Forms.Label();
         this.ModelFieldsTextBox = new System.Windows.Forms.TextBox();
         this.ModelFieldsComboBox = new System.Windows.Forms.ComboBox();
         this.ModelFieldNameLabel = new System.Windows.Forms.Label();
         this.button1 = new System.Windows.Forms.Button();
         this.ModelIOTextBox = new System.Windows.Forms.TextBox();
         this.SetButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // ModelBitLabel23
         // 
         this.ModelBitLabel23.AutoSize = true;
         this.ModelBitLabel23.Location = new System.Drawing.Point(99, 38);
         this.ModelBitLabel23.Name = "ModelBitLabel23";
         this.ModelBitLabel23.Size = new System.Drawing.Size(30, 13);
         this.ModelBitLabel23.TabIndex = 0;
         this.ModelBitLabel23.Text = "MSB";
         // 
         // ModelBitLabel22
         // 
         this.ModelBitLabel22.AutoSize = true;
         this.ModelBitLabel22.Location = new System.Drawing.Point(151, 38);
         this.ModelBitLabel22.Name = "ModelBitLabel22";
         this.ModelBitLabel22.Size = new System.Drawing.Size(39, 13);
         this.ModelBitLabel22.TabIndex = 1;
         this.ModelBitLabel22.Text = "MSB-1";
         // 
         // ModelRegistersLabel
         // 
         this.ModelRegistersLabel.AutoSize = true;
         this.ModelRegistersLabel.Location = new System.Drawing.Point(12, 60);
         this.ModelRegistersLabel.Name = "ModelRegistersLabel";
         this.ModelRegistersLabel.Size = new System.Drawing.Size(75, 13);
         this.ModelRegistersLabel.TabIndex = 24;
         this.ModelRegistersLabel.Text = "Register name";
         // 
         // ModelFieldsTextBox
         // 
         this.ModelFieldsTextBox.Location = new System.Drawing.Point(91, 57);
         this.ModelFieldsTextBox.Name = "ModelFieldsTextBox";
         this.ModelFieldsTextBox.Size = new System.Drawing.Size(35, 20);
         this.ModelFieldsTextBox.TabIndex = 25;
         // 
         // ModelFieldsComboBox
         // 
         this.ModelFieldsComboBox.DropDownWidth = 200;
         this.ModelFieldsComboBox.FormattingEnabled = true;
         this.ModelFieldsComboBox.Items.AddRange(new object[] {
            "Option one",
            "Option two"});
         this.ModelFieldsComboBox.Location = new System.Drawing.Point(91, 57);
         this.ModelFieldsComboBox.Name = "ModelFieldsComboBox";
         this.ModelFieldsComboBox.Size = new System.Drawing.Size(35, 21);
         this.ModelFieldsComboBox.TabIndex = 26;
         // 
         // ModelFieldNameLabel
         // 
         this.ModelFieldNameLabel.AutoSize = true;
         this.ModelFieldNameLabel.Location = new System.Drawing.Point(91, 81);
         this.ModelFieldNameLabel.Name = "ModelFieldNameLabel";
         this.ModelFieldNameLabel.Size = new System.Drawing.Size(57, 13);
         this.ModelFieldNameLabel.TabIndex = 27;
         this.ModelFieldNameLabel.Text = "FieldName";
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(12, 12);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(75, 23);
         this.button1.TabIndex = 28;
         this.button1.Text = "Generate";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // ModelIOTextBox
         // 
         this.ModelIOTextBox.Location = new System.Drawing.Point(15, 77);
         this.ModelIOTextBox.Name = "ModelIOTextBox";
         this.ModelIOTextBox.Size = new System.Drawing.Size(72, 20);
         this.ModelIOTextBox.TabIndex = 29;
         // 
         // SetButton
         // 
         this.SetButton.Location = new System.Drawing.Point(102, 12);
         this.SetButton.Name = "SetButton";
         this.SetButton.Size = new System.Drawing.Size(75, 23);
         this.SetButton.TabIndex = 30;
         this.SetButton.Text = "Set";
         this.SetButton.UseVisualStyleBackColor = true;
         this.SetButton.Click += new System.EventHandler(this.SetButton_Click);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.AutoScroll = true;
         this.ClientSize = new System.Drawing.Size(1381, 681);
         this.Controls.Add(this.SetButton);
         this.Controls.Add(this.ModelIOTextBox);
         this.Controls.Add(this.button1);
         this.Controls.Add(this.ModelFieldNameLabel);
         this.Controls.Add(this.ModelFieldsComboBox);
         this.Controls.Add(this.ModelFieldsTextBox);
         this.Controls.Add(this.ModelRegistersLabel);
         this.Controls.Add(this.ModelBitLabel22);
         this.Controls.Add(this.ModelBitLabel23);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label ModelBitLabel23;
      private System.Windows.Forms.Label ModelBitLabel22;
      private System.Windows.Forms.Label ModelRegistersLabel;
      private System.Windows.Forms.TextBox ModelFieldsTextBox;
      private System.Windows.Forms.ComboBox ModelFieldsComboBox;
      private System.Windows.Forms.Label ModelFieldNameLabel;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.TextBox ModelIOTextBox;
      private System.Windows.Forms.Button SetButton;
   }
}

