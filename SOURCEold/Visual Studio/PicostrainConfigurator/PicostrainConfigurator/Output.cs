﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PicostrainConfigurator {
   public partial class Output : Form {
      public Output(string Str) {
         InitializeComponent();
         OutputTextBox.Text = Str;
      }
   }
}
