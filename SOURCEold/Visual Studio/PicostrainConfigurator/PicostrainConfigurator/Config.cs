﻿using System;
using System.Configuration;

namespace Picostrain {
   public class RegistersSection : ConfigurationSection {
      public RegistersSection() {
      }

      [ConfigurationProperty("Bits")]
      public int Bits {
         get {
            return (int)this["Bits"];
         }
      }

      [ConfigurationProperty("Registers"),
      ConfigurationCollection(typeof(TRegistersCollection), ClearItemsName = "clearRegisters")]
      public TRegistersCollection Registers {
         get {
            return (TRegistersCollection)this["Registers"];
         }
         set {
            this["Registers"] = value;
         }
      }

      public class TRegistersCollection : ConfigurationElementCollection {
         public TRegister this[int index] {
            get { return (TRegister)BaseGet(index); }
         }

         public TRegister this[string index] {
            get { return (TRegister)BaseGet(index); }
         }

         protected override ConfigurationElement CreateNewElement() {
            return new TRegister();
         }

         protected override object GetElementKey(ConfigurationElement element) {
            return ((TRegister)element).Name;
         }

         public class TRegister : ConfigurationElement {
            [ConfigurationProperty("Name", IsRequired = true)]
            public string Name {
               get {
                  return (string)this["Name"];
               }
            }

            [ConfigurationProperty("Ram")]
            public int Ram {
               get {
                  return (int)this["Ram"];
               }
            }

            [ConfigurationProperty("EepromStart")]
            public int EepromStart {
               get {
                  return (int)this["EepromStart"];
               }
            }

            [ConfigurationProperty("EepromStop")]
            public int EepromStop {
               get {
                  return (int)this["EepromStop"];
               }
            }

            [ConfigurationProperty("DefaultVal")]
            public string DefaultVal {
               get {
                  return (string)this["DefaultVal"];
               }
            }

            [ConfigurationProperty("Fields"),
            ConfigurationCollection(typeof(TFieldsCollection), ClearItemsName = "clearFields")]
            public TFieldsCollection Fields {
               get {
                  return (TFieldsCollection)this["Fields"];
               }
               set {
                  this["Fields"] = value;
               }
            }

            public class TFieldsCollection : ConfigurationElementCollection {
               public TField this[int index] {
                  get { return (TField)BaseGet(index); }
               }

               public TField this[string index] {
                  get { return (TField)BaseGet(index); }
               }

               protected override ConfigurationElement CreateNewElement() {
                  return new TField();
               }

               protected override object GetElementKey(ConfigurationElement element) {
                  return ((TField)element).Name;
               }

               public class TField : ConfigurationElement {
                  [ConfigurationProperty("Name", IsRequired = true)]
                  public string Name {
                     get {
                        return (string)this["Name"];
                     }
                  }

                  [ConfigurationProperty("BitStart", IsRequired = true)]
                  public int BitStart {
                     get {
                        return (int)this["BitStart"];
                     }
                  }

                  [ConfigurationProperty("BitStop", IsRequired = true)]
                  public int BitStop {
                     get {
                        return (int)this["BitStop"];
                     }
                  }

                  [ConfigurationProperty("RecVal", IsRequired = true)]
                  public string RecVal {
                     get {
                        return (string)this["RecVal"];
                     }
                  }

                  [ConfigurationProperty("Enable", IsRequired = true)]
                  public bool Enable {
                     get {
                        return (bool)this["Enable"];
                     }
                  }

                  [ConfigurationProperty("Description", IsRequired = true)]
                  public string Description {
                     get {
                        return (string)this["Description"];
                     }
                  }

                  [ConfigurationProperty("Values"),
                  ConfigurationCollection(typeof(TValuesCollection), ClearItemsName = "clearValues")]
                  public TValuesCollection Values {
                     get {
                        return (TValuesCollection)this["Values"];
                     }
                     set {
                        this["Values"] = value;
                     }
                  }

                  public class TValuesCollection : ConfigurationElementCollection {
                     public TValue this[int index] {
                        get { return (TValue)BaseGet(index); }
                     }

                     public TValue this[string index] {
                        get { return (TValue)BaseGet(index); }
                     }

                     protected override ConfigurationElement CreateNewElement() {
                        return new TValue();
                     }

                     protected override object GetElementKey(ConfigurationElement element) {
                        return ((TValue)element).Value;
                     }

                     public class TValue : ConfigurationElement {
                        [ConfigurationProperty("Value", IsRequired = true)]
                        public string Value {
                           get {
                              return (string)this["Value"];
                           }
                        }

                        [ConfigurationProperty("Description", IsRequired = true)]
                        public string Description {
                           get {
                              return (string)this["Description"];
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }
}