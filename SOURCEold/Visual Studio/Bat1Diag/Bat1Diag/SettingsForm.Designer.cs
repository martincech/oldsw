﻿namespace Bat1Diag {
   partial class SettingsForm {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.PowerSupplyComComboBox = new System.Windows.Forms.ComboBox();
         this.PrinterComComboBox = new System.Windows.Forms.ComboBox();
         this.SaveButton = new System.Windows.Forms.Button();
         this.CancelButton = new System.Windows.Forms.Button();
         this.textBox1 = new System.Windows.Forms.TextBox();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 19);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(97, 13);
         this.label1.TabIndex = 1;
         this.label1.Text = "Power supply COM";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(45, 47);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(64, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Printer COM";
         // 
         // PowerSupplyComComboBox
         // 
         this.PowerSupplyComComboBox.FormattingEnabled = true;
         this.PowerSupplyComComboBox.Location = new System.Drawing.Point(151, 19);
         this.PowerSupplyComComboBox.Name = "PowerSupplyComComboBox";
         this.PowerSupplyComComboBox.Size = new System.Drawing.Size(121, 21);
         this.PowerSupplyComComboBox.TabIndex = 3;
         // 
         // PrinterComComboBox
         // 
         this.PrinterComComboBox.FormattingEnabled = true;
         this.PrinterComComboBox.Location = new System.Drawing.Point(151, 47);
         this.PrinterComComboBox.Name = "PrinterComComboBox";
         this.PrinterComComboBox.Size = new System.Drawing.Size(121, 21);
         this.PrinterComComboBox.TabIndex = 4;
         // 
         // SaveButton
         // 
         this.SaveButton.Location = new System.Drawing.Point(15, 221);
         this.SaveButton.Name = "SaveButton";
         this.SaveButton.Size = new System.Drawing.Size(75, 23);
         this.SaveButton.TabIndex = 5;
         this.SaveButton.Text = "Save";
         this.SaveButton.UseVisualStyleBackColor = true;
         this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
         // 
         // CancelButton
         // 
         this.CancelButton.Location = new System.Drawing.Point(197, 221);
         this.CancelButton.Name = "CancelButton";
         this.CancelButton.Size = new System.Drawing.Size(75, 23);
         this.CancelButton.TabIndex = 6;
         this.CancelButton.Text = "Cancel";
         this.CancelButton.UseVisualStyleBackColor = true;
         this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
         // 
         // textBox1
         // 
         this.textBox1.Location = new System.Drawing.Point(-11, 109);
         this.textBox1.Name = "textBox1";
         this.textBox1.Size = new System.Drawing.Size(100, 20);
         this.textBox1.TabIndex = 7;
         // 
         // SettingsForm
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(284, 262);
         this.Controls.Add(this.textBox1);
         this.Controls.Add(this.CancelButton);
         this.Controls.Add(this.SaveButton);
         this.Controls.Add(this.PrinterComComboBox);
         this.Controls.Add(this.PowerSupplyComComboBox);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Cursor = System.Windows.Forms.Cursors.Arrow;
         this.Name = "SettingsForm";
         this.ShowInTaskbar = false;
         this.Text = "SettingsForm";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.ComboBox PowerSupplyComComboBox;
      private System.Windows.Forms.ComboBox PrinterComComboBox;
      private System.Windows.Forms.Button SaveButton;
      private System.Windows.Forms.Button CancelButton;
      private System.Windows.Forms.TextBox textBox1;
   }
}