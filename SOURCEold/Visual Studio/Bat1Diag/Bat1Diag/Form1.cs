﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bat1Diag {
   public partial class Form1 : Form {
      private SettingsForm SettingsForm = new SettingsForm();

      public Form1() {
         InitializeComponent();
         SettingsForm.Visible = false;
      }

      private void SettingsButton_Click(object sender, EventArgs e) {
         SettingsForm.ShowDialog();
      }
   }
}
