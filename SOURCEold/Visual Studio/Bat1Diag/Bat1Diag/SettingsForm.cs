﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bat1Diag {
   public partial class SettingsForm : Form {
      Config AppConfig = new Config();

      public SettingsForm() {
         InitializeComponent();

         string[] SerialPorts = System.IO.Ports.SerialPort.GetPortNames();

         int i = 0;

         foreach(string SPort in SerialPorts) {
            PowerSupplyComComboBox.Items.Add(SPort);

            if(AppConfig["PowerSupplyCom"] == SPort) {
               PowerSupplyComComboBox.SelectedIndex = i;
            }

            PrinterComComboBox.Items.Add(SPort);

            if(AppConfig["PrinterCom"] == SPort) {
               PrinterComComboBox.SelectedIndex = i;
            }

            i++;
         }
      }

      private void SaveButton_Click(object sender, EventArgs e) {


         AppConfig["PowerSupplyCom"] = PowerSupplyComComboBox.SelectedItem.ToString();
         AppConfig["PrinterCom"] = PrinterComComboBox.SelectedItem.ToString();

         AppConfig.Save();

         this.DialogResult = DialogResult.OK;
      }

      private void CancelButton_Click(object sender, EventArgs e) {
         this.DialogResult = DialogResult.Cancel;
      }
   }
}
