Poznamky k USB HID
------------------

1. Do projektu doplnit knihovny :
   Linker/Input/Additional Depedencies 
   ../Library/USB/HID/HID.lib
   ../Library/USB/setupapi.lib
   
2. Upravit kodovani stringu :
   Configuration Properties/General/Character Set 
   Use Multi-Byte Character Set
   
3. Doplnit definici symbolu :
   C/C++/Preprocesor/Preprocessor Definitions  
   _CRT_SECURE_NO_WARNINGS
   
4. Doplnit cestu do adresare Library :
   C/C++/General/Additional Include directories 
   ../Library
   
Projekt HIDUsbTest je test vlastni HID knihovny
(UsbBase.cpp + Hid.cpp). Pracuje proti FW HIDDemo.
Projekt je analogicky k HIDTest (s knihovou Atmel)

Projekt HIDFlashTest je test HID knihovny pro FW
HIDMemory. Cteni Data Flash na donglu USB KEY.
