//******************************************************************************
//
//   UsbBase.cpp  USB basic operations
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef UsbBaseH
#define UsbBaseH

#include <setupapi.h>

//******************************************************************************
// TUsbBase
//******************************************************************************

class TUsbBase {
public :
   TUsbBase();
   // Constructor

   ~TUsbBase();
   // Destructor

   BOOL Locate( GUID *Guid, int Index = 0);
   // Locate class by <Guid> with device <Index>

   BOOL Open();
   // Open located device

   void Close();
   // Close opened device

   char *GetPath();
   // Returns device path

   HANDLE GetHandle();
   // Get opened handle

//------------------------------------------------------------------------------
protected :
   char   *Path;
   HANDLE Handle;
}; // TUsbBase

#endif
