//******************************************************************************
//
//   Hid.h        HID library class
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef HidH
#define HidH

#include "USB/UsbBase.h"
#include "hiddev.h"

#define HID_REPORT_SIZE        64  // maximum report size
#define HID_DEVICE_NAME_SIZE   64  // maximum device name size
#define HID_MANUFACTURER_SIZE  64  // maximum manufacturer size
#define HID_SERIAL_NUMBER_SIZE 16  // maximum serial number size

//******************************************************************************
// THid
//******************************************************************************

class THid : public TUsbBase {
public :
   THid();
   // Constructor

   ~THid();
   // Destructor

   BOOL Open( char *DeviceName);
   // Open device by <Name>

   BOOL OpenBy( int Vid, int Pid);
   // Open device by <Vid> & <Pid>

   void Close();
   // Close device

   BOOL WriteReport( void *Buffer);
   // Write single report <Buffer>

   BOOL ReadReport( void *Buffer);
   // Read single report <Buffer>

   BOOL Flush();
   // Flush input buffer

   BOOL SetInputBuffers( int Count);
   // Set input buffer as <Count> reports

   int GetInputBuffers();
   // Returns input buffer reports count

   int GetOutputReportSize();
   // Returns output report size

   int GetInputReportSize();
   // Returns input report size

   char *GetDeviceName();
   // Returns USB device name

   char *GetManufacturer();
   // Returns USB device manufacturer

   char *GetSerialNumber();
   // Returns USB device serial number

   int GetVid();
   // Returns USB device VID

   int GetPid();
   // Returns USB device PID

//---------------------------------------------------------------------------
protected :
   byte       *Report;                 // report buffer
   char       DeviceName[ HID_DEVICE_NAME_SIZE + 1];
   char       Manufacturer[ HID_MANUFACTURER_SIZE + 1];
   char       SerialNumber[ HID_SERIAL_NUMBER_SIZE + 1];
   int        DeviceVid;
   int        DevicePid;
	HIDP_CAPS  Capabilities;            // device capabilities
	HANDLE	  ReportEvent;             // event for overlapped operations
	OVERLAPPED HidOverlapped;           // overlapped operations data

   BOOL GetCapabilities();
   // Fill capabilities and strings 
}; // THid

#endif

