//******************************************************************************
//
//   hiddev.h     HID device dll stub
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef hiddevH
#define hiddevH

#include "sal.h"         // parameters semantic check macros


#ifdef __cplusplus
extern "C" {
#endif

#include "hidsdi.h"
#include "hidpi.h"

#ifdef __cplusplus
}
#endif

#endif

