﻿using System;
using System.Windows.Forms;
using System.Threading;
using AgilentInstruments;
using Device.Uart;

using System.Globalization;

namespace AgPowerSupplyE3631A {

   public partial class Form1 : Form {
      protected AgE3631A E3631A;

      public Form1() {
         InitializeComponent();

         E3631A = new AgE3631A();

         Array Outputs = Enum.GetValues(typeof(AgE3631A.OutputList)); 
 
         foreach( AgE3631A.OutputList Output in Outputs ) 
         { 
            OutputComboBox.Items.Add(Enum.GetName(typeof(AgE3631A.OutputList), Output));
         } 

         OutputComboBox.SelectedIndex = 0;
      }

      private AgE3631A.OutputList TranslateOutput(int Index) {
         switch(Index) {
            case 1:
               return AgE3631A.OutputList.P25V;

            case 2:
               return AgE3631A.OutputList.N25V;

            default:
               return AgE3631A.OutputList.P6V;
         }
      }

      private void ConnectButton_Click(object sender, EventArgs e) {
         ComUart.InterfaceComUartSettings ComSettings = new ComUart.InterfaceComUartSettings();

         ComSettings.PortName = "COM3";
         ComSettings.BaudRate = 9600;
         ComSettings.Parity = Uart.Parity.None;
         ComSettings.Bits = 8;
         ComSettings.StopBits = Uart.StopBit.One;
         ComSettings.Handshake = Uart.Handshake.None;

         E3631A.SetCom(ComSettings);

         if(!E3631A.Control(true)) {
            SetStatus("Unable to set remote");
            return;
         }

         if(!E3631A.Display("CONNECTED")) {
            SetStatus("Unable to display text");
            return;
         }

         Thread.Sleep(1000);

         if(!E3631A.ClearDisplay()) {
            SetStatus("Unable to clear display");
            return;
         }

         if(!E3631A.EnableOutput(false)) {
            SetStatus("Unable to enable/disable output");
            return;
         }

         SetStatus("OK");
      }

      private void SetStatus(string Status) {
         StatusLabel.Text = "Status: " + Status;
      }

      private void OutputCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(!E3631A.EnableOutput(OutputCheckBox.Checked)) {
            SetStatus("Unable to enable/disable output");
            return;
         }

         SetStatus("OK");
      }

      private void SetVoltageButton_Click(object sender, EventArgs e) {
         double Voltage;

         if(!Double.TryParse(VoltageTextBox.Text, out Voltage)) {
            SetStatus("Invalid voltage format");
            return;
         }

         if(!E3631A.SetVoltage(Voltage, TranslateOutput(OutputComboBox.SelectedIndex))) {
            SetStatus("Unable to set voltage");
            return;
         }

         SetStatus("OK");
      }

      private void SetCurrentButton_Click(object sender, EventArgs e) {
         double Current;

         if(!Double.TryParse(CurrentTextBox.Text, out Current)) {
            SetStatus("Invalid current format");
            return;
         }

         if(!E3631A.SetCurrent(Current, TranslateOutput(OutputComboBox.SelectedIndex))) {
            SetStatus("Unable to set current");
            return;
         }

         SetStatus("OK");
      }

      private void ReadButton_Click(object sender, EventArgs e) {
         double Voltage = 0, Current = 0;

         if(!E3631A.Get(ref Voltage, ref Current, TranslateOutput(OutputComboBox.SelectedIndex))) {
            SetStatus("Unable to get settings");
            return;
         }

         VoltageLabel.Text = Voltage.ToString();
         CurrentLabel.Text = Current.ToString();

         SetStatus("OK");
      }

      private void MeasureVoltageButton_Click(object sender, EventArgs e) {
         double Voltage = 0;

         if(!E3631A.MeasureVoltage(ref Voltage, TranslateOutput(OutputComboBox.SelectedIndex))) {
            SetStatus("Unable to measure voltage");
            return;
         }

         VoltageMeasureLabel.Text = Voltage.ToString();

         SetStatus("OK");
      }

      private void MeasureCurrentButton_Click(object sender, EventArgs e) {
         double Current = 0;

         if(!E3631A.MeasureCurrent(ref Current, TranslateOutput(OutputComboBox.SelectedIndex))) {
            SetStatus("Unable to measure current");
            return;
         }

         CurrentMeasureLabel.Text = Current.ToString();

         SetStatus("OK");
      }
   }
}
