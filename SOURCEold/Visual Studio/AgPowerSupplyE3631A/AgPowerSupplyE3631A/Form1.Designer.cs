﻿namespace AgPowerSupplyE3631A {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.CurrentLabel = new System.Windows.Forms.Label();
         this.VoltageLabel = new System.Windows.Forms.Label();
         this.ReadButton = new System.Windows.Forms.Button();
         this.OutputCheckBox = new System.Windows.Forms.CheckBox();
         this.CurrentTextBox = new System.Windows.Forms.TextBox();
         this.VoltageTextBox = new System.Windows.Forms.TextBox();
         this.SetCurrentButton = new System.Windows.Forms.Button();
         this.SetVoltageButton = new System.Windows.Forms.Button();
         this.StatusLabel = new System.Windows.Forms.Label();
         this.ConnectButton = new System.Windows.Forms.Button();
         this.ComLabel = new System.Windows.Forms.Label();
         this.ComTextBox = new System.Windows.Forms.TextBox();
         this.OutputComboBox = new System.Windows.Forms.ComboBox();
         this.MeasureCurrentButton = new System.Windows.Forms.Button();
         this.MeasureVoltageButton = new System.Windows.Forms.Button();
         this.CurrentMeasureLabel = new System.Windows.Forms.Label();
         this.VoltageMeasureLabel = new System.Windows.Forms.Label();
         this.OutputLabel = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // CurrentLabel
         // 
         this.CurrentLabel.AutoSize = true;
         this.CurrentLabel.Location = new System.Drawing.Point(126, 225);
         this.CurrentLabel.Name = "CurrentLabel";
         this.CurrentLabel.Size = new System.Drawing.Size(23, 13);
         this.CurrentLabel.TabIndex = 23;
         this.CurrentLabel.Text = "? A";
         // 
         // VoltageLabel
         // 
         this.VoltageLabel.AutoSize = true;
         this.VoltageLabel.Location = new System.Drawing.Point(126, 207);
         this.VoltageLabel.Name = "VoltageLabel";
         this.VoltageLabel.Size = new System.Drawing.Size(23, 13);
         this.VoltageLabel.TabIndex = 22;
         this.VoltageLabel.Text = "? V";
         // 
         // ReadButton
         // 
         this.ReadButton.Location = new System.Drawing.Point(196, 202);
         this.ReadButton.Name = "ReadButton";
         this.ReadButton.Size = new System.Drawing.Size(75, 23);
         this.ReadButton.TabIndex = 21;
         this.ReadButton.Text = "Read";
         this.ReadButton.UseVisualStyleBackColor = true;
         this.ReadButton.Click += new System.EventHandler(this.ReadButton_Click);
         // 
         // OutputCheckBox
         // 
         this.OutputCheckBox.AutoSize = true;
         this.OutputCheckBox.Location = new System.Drawing.Point(180, 52);
         this.OutputCheckBox.Name = "OutputCheckBox";
         this.OutputCheckBox.Size = new System.Drawing.Size(92, 17);
         this.OutputCheckBox.TabIndex = 20;
         this.OutputCheckBox.Text = "Enable output";
         this.OutputCheckBox.UseVisualStyleBackColor = true;
         this.OutputCheckBox.CheckedChanged += new System.EventHandler(this.OutputCheckBox_CheckedChanged);
         // 
         // CurrentTextBox
         // 
         this.CurrentTextBox.Location = new System.Drawing.Point(82, 158);
         this.CurrentTextBox.Name = "CurrentTextBox";
         this.CurrentTextBox.Size = new System.Drawing.Size(100, 20);
         this.CurrentTextBox.TabIndex = 19;
         this.CurrentTextBox.Text = "0,2";
         // 
         // VoltageTextBox
         // 
         this.VoltageTextBox.Location = new System.Drawing.Point(82, 132);
         this.VoltageTextBox.Name = "VoltageTextBox";
         this.VoltageTextBox.Size = new System.Drawing.Size(100, 20);
         this.VoltageTextBox.TabIndex = 18;
         this.VoltageTextBox.Text = "1,0";
         // 
         // SetCurrentButton
         // 
         this.SetCurrentButton.Location = new System.Drawing.Point(196, 159);
         this.SetCurrentButton.Name = "SetCurrentButton";
         this.SetCurrentButton.Size = new System.Drawing.Size(75, 23);
         this.SetCurrentButton.TabIndex = 17;
         this.SetCurrentButton.Text = "Set current";
         this.SetCurrentButton.UseVisualStyleBackColor = true;
         this.SetCurrentButton.Click += new System.EventHandler(this.SetCurrentButton_Click);
         // 
         // SetVoltageButton
         // 
         this.SetVoltageButton.Location = new System.Drawing.Point(196, 130);
         this.SetVoltageButton.Name = "SetVoltageButton";
         this.SetVoltageButton.Size = new System.Drawing.Size(75, 23);
         this.SetVoltageButton.TabIndex = 16;
         this.SetVoltageButton.Text = "Set voltage";
         this.SetVoltageButton.UseVisualStyleBackColor = true;
         this.SetVoltageButton.Click += new System.EventHandler(this.SetVoltageButton_Click);
         // 
         // StatusLabel
         // 
         this.StatusLabel.AutoSize = true;
         this.StatusLabel.Location = new System.Drawing.Point(28, 269);
         this.StatusLabel.Name = "StatusLabel";
         this.StatusLabel.Size = new System.Drawing.Size(49, 13);
         this.StatusLabel.TabIndex = 15;
         this.StatusLabel.Text = "Status: ?";
         // 
         // ConnectButton
         // 
         this.ConnectButton.Location = new System.Drawing.Point(82, 48);
         this.ConnectButton.Name = "ConnectButton";
         this.ConnectButton.Size = new System.Drawing.Size(75, 23);
         this.ConnectButton.TabIndex = 14;
         this.ConnectButton.Text = "Connect";
         this.ConnectButton.UseVisualStyleBackColor = true;
         this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
         // 
         // ComLabel
         // 
         this.ComLabel.AutoSize = true;
         this.ComLabel.Location = new System.Drawing.Point(12, 24);
         this.ComLabel.Name = "ComLabel";
         this.ComLabel.Size = new System.Drawing.Size(52, 13);
         this.ComLabel.TabIndex = 13;
         this.ComLabel.Text = "Com port:";
         // 
         // ComTextBox
         // 
         this.ComTextBox.Location = new System.Drawing.Point(82, 21);
         this.ComTextBox.Name = "ComTextBox";
         this.ComTextBox.Size = new System.Drawing.Size(100, 20);
         this.ComTextBox.TabIndex = 12;
         this.ComTextBox.Text = "COM1";
         // 
         // OutputComboBox
         // 
         this.OutputComboBox.FormattingEnabled = true;
         this.OutputComboBox.Location = new System.Drawing.Point(82, 93);
         this.OutputComboBox.Name = "OutputComboBox";
         this.OutputComboBox.Size = new System.Drawing.Size(100, 21);
         this.OutputComboBox.TabIndex = 24;
         // 
         // MeasureCurrentButton
         // 
         this.MeasureCurrentButton.Location = new System.Drawing.Point(399, 159);
         this.MeasureCurrentButton.Name = "MeasureCurrentButton";
         this.MeasureCurrentButton.Size = new System.Drawing.Size(100, 23);
         this.MeasureCurrentButton.TabIndex = 26;
         this.MeasureCurrentButton.Text = "Measure current";
         this.MeasureCurrentButton.UseVisualStyleBackColor = true;
         this.MeasureCurrentButton.Click += new System.EventHandler(this.MeasureCurrentButton_Click);
         // 
         // MeasureVoltageButton
         // 
         this.MeasureVoltageButton.Location = new System.Drawing.Point(399, 130);
         this.MeasureVoltageButton.Name = "MeasureVoltageButton";
         this.MeasureVoltageButton.Size = new System.Drawing.Size(100, 23);
         this.MeasureVoltageButton.TabIndex = 25;
         this.MeasureVoltageButton.Text = "Measure voltage";
         this.MeasureVoltageButton.UseVisualStyleBackColor = true;
         this.MeasureVoltageButton.Click += new System.EventHandler(this.MeasureVoltageButton_Click);
         // 
         // CurrentMeasureLabel
         // 
         this.CurrentMeasureLabel.AutoSize = true;
         this.CurrentMeasureLabel.Location = new System.Drawing.Point(316, 153);
         this.CurrentMeasureLabel.Name = "CurrentMeasureLabel";
         this.CurrentMeasureLabel.Size = new System.Drawing.Size(23, 13);
         this.CurrentMeasureLabel.TabIndex = 28;
         this.CurrentMeasureLabel.Text = "? A";
         // 
         // VoltageMeasureLabel
         // 
         this.VoltageMeasureLabel.AutoSize = true;
         this.VoltageMeasureLabel.Location = new System.Drawing.Point(316, 135);
         this.VoltageMeasureLabel.Name = "VoltageMeasureLabel";
         this.VoltageMeasureLabel.Size = new System.Drawing.Size(23, 13);
         this.VoltageMeasureLabel.TabIndex = 27;
         this.VoltageMeasureLabel.Text = "? V";
         // 
         // OutputLabel
         // 
         this.OutputLabel.AutoSize = true;
         this.OutputLabel.Location = new System.Drawing.Point(22, 96);
         this.OutputLabel.Name = "OutputLabel";
         this.OutputLabel.Size = new System.Drawing.Size(42, 13);
         this.OutputLabel.TabIndex = 29;
         this.OutputLabel.Text = "Output:";
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(532, 300);
         this.Controls.Add(this.OutputLabel);
         this.Controls.Add(this.CurrentMeasureLabel);
         this.Controls.Add(this.VoltageMeasureLabel);
         this.Controls.Add(this.MeasureCurrentButton);
         this.Controls.Add(this.MeasureVoltageButton);
         this.Controls.Add(this.OutputComboBox);
         this.Controls.Add(this.CurrentLabel);
         this.Controls.Add(this.VoltageLabel);
         this.Controls.Add(this.ReadButton);
         this.Controls.Add(this.OutputCheckBox);
         this.Controls.Add(this.CurrentTextBox);
         this.Controls.Add(this.VoltageTextBox);
         this.Controls.Add(this.SetCurrentButton);
         this.Controls.Add(this.SetVoltageButton);
         this.Controls.Add(this.StatusLabel);
         this.Controls.Add(this.ConnectButton);
         this.Controls.Add(this.ComLabel);
         this.Controls.Add(this.ComTextBox);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label CurrentLabel;
      private System.Windows.Forms.Label VoltageLabel;
      private System.Windows.Forms.Button ReadButton;
      private System.Windows.Forms.CheckBox OutputCheckBox;
      private System.Windows.Forms.TextBox CurrentTextBox;
      private System.Windows.Forms.TextBox VoltageTextBox;
      private System.Windows.Forms.Button SetCurrentButton;
      private System.Windows.Forms.Button SetVoltageButton;
      private System.Windows.Forms.Label StatusLabel;
      private System.Windows.Forms.Button ConnectButton;
      private System.Windows.Forms.Label ComLabel;
      private System.Windows.Forms.TextBox ComTextBox;
      private System.Windows.Forms.ComboBox OutputComboBox;
      private System.Windows.Forms.Button MeasureCurrentButton;
      private System.Windows.Forms.Button MeasureVoltageButton;
      private System.Windows.Forms.Label CurrentMeasureLabel;
      private System.Windows.Forms.Label VoltageMeasureLabel;
      private System.Windows.Forms.Label OutputLabel;
   }
}

