//******************************************************************************
//
//   BatDevice.h      Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef BatDeviceH
   #define BatDeviceH

#ifndef PktAdapterH
   #include "PktAdapter.h"
#endif

#ifndef UsbDefH
   #include "../../ARM/Bat1/UsbDef.h"
#endif

//******************************************************************************
// BatDevice
//******************************************************************************

class TBatDevice {
public :
   TBatDevice();
   // Constructor

   ~TBatDevice();
   // Destructor

   BOOL Check();
   // Check for device connected

   BOOL Close();
   // Check for device connected

   BOOL GetVersion( int &Version);
   // Get bat version

   BOOL GetStatus( BOOL &PowerOn);
   // Get power status

   BOOL PowerOff();
   // Switch power off

   BOOL ReloadConfig();
   // Reload configuration by EEPROM

   BOOL GetTime( TTimestamp &Timestamp);
   // Get device time

   BOOL SetTime( TTimestamp Timestamp);
   // Set device time

   BOOL ReadMemory( int Address, void *Buffer, int Size);
   // Read EEPROM

   BOOL WriteMemory( int Address, void *Buffer, int Size);
   // Write EEPROM data

   BOOL DirectoryBegin( int &Count);
   // Read directory begin. Returns <Count> of directory items

   BOOL DirectoryNext( TUsbDirectoryItem *Item);
   // Read directory entry. Fills <Item>
   
   BOOL FileOpen( int Handle);
   // Open file by <Handle>

   BOOL FileClose();
   // Close Current file

   BOOL FileCreate( char *Name, char *Note, int Class, int &Handle);
   // Create file by <Name>, <Note>, <Class>. Returns <Handle>

   BOOL FileRead( void *Buffer, int Size);
   // Read file

   BOOL FileWrite( void *Buffer, int Size);
   // Write file

   BOOL FormatFilesystem();
   // Format filesystem

   TPktAdapter *Adapter;
   BOOL         WriteVerification;
//------------------------------------------------------------------------------
protected :
   BOOL SendCommand( int &Command, int &Data);
   // Send command with error recovery

   BOOL WriteFragment( int WriteCommand, void *Buffer, int Size);
   // Write fragment of data with recovery
}; // BatDevice

#endif
