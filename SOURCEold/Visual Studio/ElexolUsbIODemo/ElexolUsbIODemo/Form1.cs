﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Device.UsbIO;

// upravit, ať se nezapistují/nečtou piny po jednom, ale všechny najednou

namespace ElexolUsbIODemo {
   public partial class Form1 : Form {
      ElexolUsbIO DigitalIO = new ElexolUsbIO();

      public Form1() {
         InitializeComponent();
         if(!DigitalIO.Open("USB I/O 24 R")) {
            SetStatus("Can't open device");
         }

         ElexolUsbIO.TPorts.TPortDirection Dir = new ElexolUsbIO.TPorts.TPortDirection();

         Dir[0] = ElexolUsbIO.PinDirection.Input;
         Dir[1] = ElexolUsbIO.PinDirection.Input;
         Dir[2] = ElexolUsbIO.PinDirection.Input;
         Dir[3] = ElexolUsbIO.PinDirection.Input;
         Dir[4] = ElexolUsbIO.PinDirection.Input;
         Dir[5] = ElexolUsbIO.PinDirection.Input;
         Dir[6] = ElexolUsbIO.PinDirection.Input;
         Dir[7] = ElexolUsbIO.PinDirection.Input;

         DigitalIO.Port["A"].SetIO(Dir);
         DigitalIO.Port["B"].SetIO(Dir);
         DigitalIO.Port["C"].SetIO(Dir);
      }

      private void SetStatus(string Status) {
         StatusLabel.Text = "Status: " + Status;
      }

      private void timer1_Tick(object sender, EventArgs e) {
         var CheckBoxes = GetAll(this, typeof(CheckBox)); 
         
         foreach(CheckBox CB in CheckBoxes) {
            switch(CB.Name.Substring(6, 2)) {
               case "IO":
                  if(CB.Checked == false) {
                     CheckBox StateCheckBox = (CheckBox)FindControlByName(CB.Name.Substring(0, 6) + "CheckBox");
                     
                     string Port = CB.Name.Substring(4, 1);
                        
                     int Pin = Convert.ToInt32(CB.Name.Substring(5, 1));

                     bool State = false;

                     DigitalIO.Port[Port].Pin[Pin].Read(ref State);

                     StateCheckBox.Checked = State;
                  }
                  break;
            }
         }
      }

      private void PortIO_CheckedChanged(object sender, EventArgs e) {
         System.Windows.Forms.CheckBox CheckBox = (System.Windows.Forms.CheckBox)sender;

         string Port = CheckBox.Name.Substring(4, 1);
         int Pin = Convert.ToInt32(CheckBox.Name.Substring(5, 1));

         DigitalIO.Port[Port].Pin[Pin].SetIO(CheckBox.Checked == true ? ElexolUsbIO.PinDirection.Output : ElexolUsbIO.PinDirection.Input);

         CheckBox StateCheckBox = (CheckBox)FindControlByName(CheckBox.Name.Substring(0, 6) + "CheckBox");
         StateCheckBox.Enabled = CheckBox.Checked;
      }

      private void PortState_CheckedChanged(object sender, EventArgs e) {
         System.Windows.Forms.CheckBox CheckBox = (System.Windows.Forms.CheckBox) sender;

         string Port = CheckBox.Name.Substring(4, 1);
         int Pin = Convert.ToInt32(CheckBox.Name.Substring(5, 1));

         DigitalIO.Port[Port].Pin[Pin].Set(CheckBox.Checked);
      }

      public IEnumerable<Control> GetAll(Control control, Type type) {
         var controls = control.Controls.Cast<Control>();

         return controls.SelectMany(ctrl => GetAll(ctrl, type)).Concat(controls).Where(c => c.GetType() == type);
      }

      private Control FindControlByName(string name) {
         foreach(Control c in this.Controls) {
            if(c.Name == name)
               return c;
         }
         
         return null;
      }
 

   }
}
