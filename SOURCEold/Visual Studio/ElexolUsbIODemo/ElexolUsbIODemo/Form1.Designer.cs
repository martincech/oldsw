﻿namespace ElexolUsbIODemo {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         this.PortA7CheckBox = new System.Windows.Forms.CheckBox();
         this.PortA6CheckBox = new System.Windows.Forms.CheckBox();
         this.PortA5CheckBox = new System.Windows.Forms.CheckBox();
         this.PortA4CheckBox = new System.Windows.Forms.CheckBox();
         this.PortA3CheckBox = new System.Windows.Forms.CheckBox();
         this.PortA2CheckBox = new System.Windows.Forms.CheckBox();
         this.PortA1CheckBox = new System.Windows.Forms.CheckBox();
         this.PortA0CheckBox = new System.Windows.Forms.CheckBox();
         this.PortALabel = new System.Windows.Forms.Label();
         this.PortBLabel = new System.Windows.Forms.Label();
         this.PortB0CheckBox = new System.Windows.Forms.CheckBox();
         this.PortB1CheckBox = new System.Windows.Forms.CheckBox();
         this.PortB2CheckBox = new System.Windows.Forms.CheckBox();
         this.PortB3CheckBox = new System.Windows.Forms.CheckBox();
         this.PortB4CheckBox = new System.Windows.Forms.CheckBox();
         this.PortB5CheckBox = new System.Windows.Forms.CheckBox();
         this.PortB6CheckBox = new System.Windows.Forms.CheckBox();
         this.PortB7CheckBox = new System.Windows.Forms.CheckBox();
         this.PortCLabel = new System.Windows.Forms.Label();
         this.PortC0CheckBox = new System.Windows.Forms.CheckBox();
         this.PortC1CheckBox = new System.Windows.Forms.CheckBox();
         this.PortC2CheckBox = new System.Windows.Forms.CheckBox();
         this.PortC3CheckBox = new System.Windows.Forms.CheckBox();
         this.PortC4CheckBox = new System.Windows.Forms.CheckBox();
         this.PortC5CheckBox = new System.Windows.Forms.CheckBox();
         this.PortC6CheckBox = new System.Windows.Forms.CheckBox();
         this.PortC7CheckBox = new System.Windows.Forms.CheckBox();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.textBox1 = new System.Windows.Forms.TextBox();
         this.StatusLabel = new System.Windows.Forms.Label();
         this.PortA7IO = new System.Windows.Forms.CheckBox();
         this.PortA6IO = new System.Windows.Forms.CheckBox();
         this.PortA5IO = new System.Windows.Forms.CheckBox();
         this.PortA4IO = new System.Windows.Forms.CheckBox();
         this.PortA3IO = new System.Windows.Forms.CheckBox();
         this.PortA2IO = new System.Windows.Forms.CheckBox();
         this.PortA1IO = new System.Windows.Forms.CheckBox();
         this.PortA0IO = new System.Windows.Forms.CheckBox();
         this.PortB0IO = new System.Windows.Forms.CheckBox();
         this.PortB1IO = new System.Windows.Forms.CheckBox();
         this.PortB2IO = new System.Windows.Forms.CheckBox();
         this.PortB3IO = new System.Windows.Forms.CheckBox();
         this.PortB4IO = new System.Windows.Forms.CheckBox();
         this.PortB5IO = new System.Windows.Forms.CheckBox();
         this.PortB6IO = new System.Windows.Forms.CheckBox();
         this.PortB7IO = new System.Windows.Forms.CheckBox();
         this.PortC0IO = new System.Windows.Forms.CheckBox();
         this.PortC1IO = new System.Windows.Forms.CheckBox();
         this.PortC2IO = new System.Windows.Forms.CheckBox();
         this.PortC3IO = new System.Windows.Forms.CheckBox();
         this.PortC4IO = new System.Windows.Forms.CheckBox();
         this.PortC5IO = new System.Windows.Forms.CheckBox();
         this.PortC6IO = new System.Windows.Forms.CheckBox();
         this.PortC7IO = new System.Windows.Forms.CheckBox();
         this.SuspendLayout();
         // 
         // PortA7CheckBox
         // 
         this.PortA7CheckBox.AutoSize = true;
         this.PortA7CheckBox.Location = new System.Drawing.Point(34, 39);
         this.PortA7CheckBox.Name = "PortA7CheckBox";
         this.PortA7CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA7CheckBox.TabIndex = 0;
         this.PortA7CheckBox.Text = "7";
         this.PortA7CheckBox.UseVisualStyleBackColor = true;
         this.PortA7CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortA6CheckBox
         // 
         this.PortA6CheckBox.AutoSize = true;
         this.PortA6CheckBox.Location = new System.Drawing.Point(34, 62);
         this.PortA6CheckBox.Name = "PortA6CheckBox";
         this.PortA6CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA6CheckBox.TabIndex = 1;
         this.PortA6CheckBox.Text = "6";
         this.PortA6CheckBox.UseVisualStyleBackColor = true;
         this.PortA6CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortA5CheckBox
         // 
         this.PortA5CheckBox.AutoSize = true;
         this.PortA5CheckBox.Location = new System.Drawing.Point(34, 85);
         this.PortA5CheckBox.Name = "PortA5CheckBox";
         this.PortA5CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA5CheckBox.TabIndex = 2;
         this.PortA5CheckBox.Text = "5";
         this.PortA5CheckBox.UseVisualStyleBackColor = true;
         this.PortA5CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortA4CheckBox
         // 
         this.PortA4CheckBox.AutoSize = true;
         this.PortA4CheckBox.Location = new System.Drawing.Point(34, 108);
         this.PortA4CheckBox.Name = "PortA4CheckBox";
         this.PortA4CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA4CheckBox.TabIndex = 3;
         this.PortA4CheckBox.Text = "4";
         this.PortA4CheckBox.UseVisualStyleBackColor = true;
         this.PortA4CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortA3CheckBox
         // 
         this.PortA3CheckBox.AutoSize = true;
         this.PortA3CheckBox.Location = new System.Drawing.Point(34, 131);
         this.PortA3CheckBox.Name = "PortA3CheckBox";
         this.PortA3CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA3CheckBox.TabIndex = 4;
         this.PortA3CheckBox.Text = "3";
         this.PortA3CheckBox.UseVisualStyleBackColor = true;
         this.PortA3CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortA2CheckBox
         // 
         this.PortA2CheckBox.AutoSize = true;
         this.PortA2CheckBox.Location = new System.Drawing.Point(34, 154);
         this.PortA2CheckBox.Name = "PortA2CheckBox";
         this.PortA2CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA2CheckBox.TabIndex = 5;
         this.PortA2CheckBox.Text = "2";
         this.PortA2CheckBox.UseVisualStyleBackColor = true;
         this.PortA2CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortA1CheckBox
         // 
         this.PortA1CheckBox.AutoSize = true;
         this.PortA1CheckBox.Location = new System.Drawing.Point(34, 177);
         this.PortA1CheckBox.Name = "PortA1CheckBox";
         this.PortA1CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA1CheckBox.TabIndex = 6;
         this.PortA1CheckBox.Text = "1";
         this.PortA1CheckBox.UseVisualStyleBackColor = true;
         this.PortA1CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortA0CheckBox
         // 
         this.PortA0CheckBox.AutoSize = true;
         this.PortA0CheckBox.Location = new System.Drawing.Point(34, 200);
         this.PortA0CheckBox.Name = "PortA0CheckBox";
         this.PortA0CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortA0CheckBox.TabIndex = 7;
         this.PortA0CheckBox.Text = "0";
         this.PortA0CheckBox.UseVisualStyleBackColor = true;
         this.PortA0CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortALabel
         // 
         this.PortALabel.AutoSize = true;
         this.PortALabel.Location = new System.Drawing.Point(31, 18);
         this.PortALabel.Name = "PortALabel";
         this.PortALabel.Size = new System.Drawing.Size(36, 13);
         this.PortALabel.TabIndex = 8;
         this.PortALabel.Text = "Port A";
         // 
         // PortBLabel
         // 
         this.PortBLabel.AutoSize = true;
         this.PortBLabel.Location = new System.Drawing.Point(102, 18);
         this.PortBLabel.Name = "PortBLabel";
         this.PortBLabel.Size = new System.Drawing.Size(36, 13);
         this.PortBLabel.TabIndex = 17;
         this.PortBLabel.Text = "Port B";
         // 
         // PortB0CheckBox
         // 
         this.PortB0CheckBox.AutoSize = true;
         this.PortB0CheckBox.Location = new System.Drawing.Point(105, 200);
         this.PortB0CheckBox.Name = "PortB0CheckBox";
         this.PortB0CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB0CheckBox.TabIndex = 16;
         this.PortB0CheckBox.Text = "0";
         this.PortB0CheckBox.UseVisualStyleBackColor = true;
         this.PortB0CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortB1CheckBox
         // 
         this.PortB1CheckBox.AutoSize = true;
         this.PortB1CheckBox.Location = new System.Drawing.Point(105, 177);
         this.PortB1CheckBox.Name = "PortB1CheckBox";
         this.PortB1CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB1CheckBox.TabIndex = 15;
         this.PortB1CheckBox.Text = "1";
         this.PortB1CheckBox.UseVisualStyleBackColor = true;
         this.PortB1CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortB2CheckBox
         // 
         this.PortB2CheckBox.AutoSize = true;
         this.PortB2CheckBox.Location = new System.Drawing.Point(105, 154);
         this.PortB2CheckBox.Name = "PortB2CheckBox";
         this.PortB2CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB2CheckBox.TabIndex = 14;
         this.PortB2CheckBox.Text = "2";
         this.PortB2CheckBox.UseVisualStyleBackColor = true;
         this.PortB2CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortB3CheckBox
         // 
         this.PortB3CheckBox.AutoSize = true;
         this.PortB3CheckBox.Location = new System.Drawing.Point(105, 131);
         this.PortB3CheckBox.Name = "PortB3CheckBox";
         this.PortB3CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB3CheckBox.TabIndex = 13;
         this.PortB3CheckBox.Text = "3";
         this.PortB3CheckBox.UseVisualStyleBackColor = true;
         this.PortB3CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortB4CheckBox
         // 
         this.PortB4CheckBox.AutoSize = true;
         this.PortB4CheckBox.Location = new System.Drawing.Point(105, 108);
         this.PortB4CheckBox.Name = "PortB4CheckBox";
         this.PortB4CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB4CheckBox.TabIndex = 12;
         this.PortB4CheckBox.Text = "4";
         this.PortB4CheckBox.UseVisualStyleBackColor = true;
         this.PortB4CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortB5CheckBox
         // 
         this.PortB5CheckBox.AutoSize = true;
         this.PortB5CheckBox.Location = new System.Drawing.Point(105, 85);
         this.PortB5CheckBox.Name = "PortB5CheckBox";
         this.PortB5CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB5CheckBox.TabIndex = 11;
         this.PortB5CheckBox.Text = "5";
         this.PortB5CheckBox.UseVisualStyleBackColor = true;
         this.PortB5CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortB6CheckBox
         // 
         this.PortB6CheckBox.AutoSize = true;
         this.PortB6CheckBox.Location = new System.Drawing.Point(105, 62);
         this.PortB6CheckBox.Name = "PortB6CheckBox";
         this.PortB6CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB6CheckBox.TabIndex = 10;
         this.PortB6CheckBox.Text = "6";
         this.PortB6CheckBox.UseVisualStyleBackColor = true;
         this.PortB6CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortB7CheckBox
         // 
         this.PortB7CheckBox.AutoSize = true;
         this.PortB7CheckBox.Location = new System.Drawing.Point(105, 39);
         this.PortB7CheckBox.Name = "PortB7CheckBox";
         this.PortB7CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortB7CheckBox.TabIndex = 9;
         this.PortB7CheckBox.Text = "7";
         this.PortB7CheckBox.UseVisualStyleBackColor = true;
         this.PortB7CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortCLabel
         // 
         this.PortCLabel.AutoSize = true;
         this.PortCLabel.Location = new System.Drawing.Point(170, 19);
         this.PortCLabel.Name = "PortCLabel";
         this.PortCLabel.Size = new System.Drawing.Size(36, 13);
         this.PortCLabel.TabIndex = 26;
         this.PortCLabel.Text = "Port C";
         // 
         // PortC0CheckBox
         // 
         this.PortC0CheckBox.AutoSize = true;
         this.PortC0CheckBox.Location = new System.Drawing.Point(173, 200);
         this.PortC0CheckBox.Name = "PortC0CheckBox";
         this.PortC0CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC0CheckBox.TabIndex = 25;
         this.PortC0CheckBox.Text = "0";
         this.PortC0CheckBox.UseVisualStyleBackColor = true;
         this.PortC0CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortC1CheckBox
         // 
         this.PortC1CheckBox.AutoSize = true;
         this.PortC1CheckBox.Location = new System.Drawing.Point(173, 177);
         this.PortC1CheckBox.Name = "PortC1CheckBox";
         this.PortC1CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC1CheckBox.TabIndex = 24;
         this.PortC1CheckBox.Text = "1";
         this.PortC1CheckBox.UseVisualStyleBackColor = true;
         this.PortC1CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortC2CheckBox
         // 
         this.PortC2CheckBox.AutoSize = true;
         this.PortC2CheckBox.Location = new System.Drawing.Point(173, 154);
         this.PortC2CheckBox.Name = "PortC2CheckBox";
         this.PortC2CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC2CheckBox.TabIndex = 23;
         this.PortC2CheckBox.Text = "2";
         this.PortC2CheckBox.UseVisualStyleBackColor = true;
         this.PortC2CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortC3CheckBox
         // 
         this.PortC3CheckBox.AutoSize = true;
         this.PortC3CheckBox.Location = new System.Drawing.Point(173, 131);
         this.PortC3CheckBox.Name = "PortC3CheckBox";
         this.PortC3CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC3CheckBox.TabIndex = 22;
         this.PortC3CheckBox.Text = "3";
         this.PortC3CheckBox.UseVisualStyleBackColor = true;
         this.PortC3CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortC4CheckBox
         // 
         this.PortC4CheckBox.AutoSize = true;
         this.PortC4CheckBox.Location = new System.Drawing.Point(173, 108);
         this.PortC4CheckBox.Name = "PortC4CheckBox";
         this.PortC4CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC4CheckBox.TabIndex = 21;
         this.PortC4CheckBox.Text = "4";
         this.PortC4CheckBox.UseVisualStyleBackColor = true;
         this.PortC4CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortC5CheckBox
         // 
         this.PortC5CheckBox.AutoSize = true;
         this.PortC5CheckBox.Location = new System.Drawing.Point(173, 85);
         this.PortC5CheckBox.Name = "PortC5CheckBox";
         this.PortC5CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC5CheckBox.TabIndex = 20;
         this.PortC5CheckBox.Text = "5";
         this.PortC5CheckBox.UseVisualStyleBackColor = true;
         this.PortC5CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortC6CheckBox
         // 
         this.PortC6CheckBox.AutoSize = true;
         this.PortC6CheckBox.Location = new System.Drawing.Point(173, 62);
         this.PortC6CheckBox.Name = "PortC6CheckBox";
         this.PortC6CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC6CheckBox.TabIndex = 19;
         this.PortC6CheckBox.Text = "6";
         this.PortC6CheckBox.UseVisualStyleBackColor = true;
         this.PortC6CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // PortC7CheckBox
         // 
         this.PortC7CheckBox.AutoSize = true;
         this.PortC7CheckBox.Location = new System.Drawing.Point(173, 39);
         this.PortC7CheckBox.Name = "PortC7CheckBox";
         this.PortC7CheckBox.Size = new System.Drawing.Size(32, 17);
         this.PortC7CheckBox.TabIndex = 18;
         this.PortC7CheckBox.Text = "7";
         this.PortC7CheckBox.UseVisualStyleBackColor = true;
         this.PortC7CheckBox.CheckedChanged += new System.EventHandler(this.PortState_CheckedChanged);
         // 
         // timer1
         // 
         this.timer1.Enabled = true;
         this.timer1.Interval = 500;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // textBox1
         // 
         this.textBox1.Location = new System.Drawing.Point(162, 230);
         this.textBox1.Name = "textBox1";
         this.textBox1.Size = new System.Drawing.Size(100, 20);
         this.textBox1.TabIndex = 27;
         // 
         // StatusLabel
         // 
         this.StatusLabel.AutoSize = true;
         this.StatusLabel.Location = new System.Drawing.Point(25, 237);
         this.StatusLabel.Name = "StatusLabel";
         this.StatusLabel.Size = new System.Drawing.Size(49, 13);
         this.StatusLabel.TabIndex = 28;
         this.StatusLabel.Text = "Status: ?";
         // 
         // PortA7IO
         // 
         this.PortA7IO.AutoSize = true;
         this.PortA7IO.Cursor = System.Windows.Forms.Cursors.Default;
         this.PortA7IO.Location = new System.Drawing.Point(12, 40);
         this.PortA7IO.Name = "PortA7IO";
         this.PortA7IO.Size = new System.Drawing.Size(15, 14);
         this.PortA7IO.TabIndex = 29;
         this.PortA7IO.UseVisualStyleBackColor = true;
         this.PortA7IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortA6IO
         // 
         this.PortA6IO.AutoSize = true;
         this.PortA6IO.Location = new System.Drawing.Point(12, 63);
         this.PortA6IO.Name = "PortA6IO";
         this.PortA6IO.Size = new System.Drawing.Size(15, 14);
         this.PortA6IO.TabIndex = 30;
         this.PortA6IO.UseVisualStyleBackColor = true;
         this.PortA6IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortA5IO
         // 
         this.PortA5IO.AutoSize = true;
         this.PortA5IO.Location = new System.Drawing.Point(12, 86);
         this.PortA5IO.Name = "PortA5IO";
         this.PortA5IO.Size = new System.Drawing.Size(15, 14);
         this.PortA5IO.TabIndex = 31;
         this.PortA5IO.UseVisualStyleBackColor = true;
         this.PortA5IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortA4IO
         // 
         this.PortA4IO.AutoSize = true;
         this.PortA4IO.Location = new System.Drawing.Point(12, 109);
         this.PortA4IO.Name = "PortA4IO";
         this.PortA4IO.Size = new System.Drawing.Size(15, 14);
         this.PortA4IO.TabIndex = 32;
         this.PortA4IO.UseVisualStyleBackColor = true;
         this.PortA4IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortA3IO
         // 
         this.PortA3IO.AutoSize = true;
         this.PortA3IO.Location = new System.Drawing.Point(12, 132);
         this.PortA3IO.Name = "PortA3IO";
         this.PortA3IO.Size = new System.Drawing.Size(15, 14);
         this.PortA3IO.TabIndex = 33;
         this.PortA3IO.UseVisualStyleBackColor = true;
         this.PortA3IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortA2IO
         // 
         this.PortA2IO.AutoSize = true;
         this.PortA2IO.Location = new System.Drawing.Point(12, 155);
         this.PortA2IO.Name = "PortA2IO";
         this.PortA2IO.Size = new System.Drawing.Size(15, 14);
         this.PortA2IO.TabIndex = 34;
         this.PortA2IO.UseVisualStyleBackColor = true;
         this.PortA2IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortA1IO
         // 
         this.PortA1IO.AutoSize = true;
         this.PortA1IO.Location = new System.Drawing.Point(12, 178);
         this.PortA1IO.Name = "PortA1IO";
         this.PortA1IO.Size = new System.Drawing.Size(15, 14);
         this.PortA1IO.TabIndex = 35;
         this.PortA1IO.UseVisualStyleBackColor = true;
         this.PortA1IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortA0IO
         // 
         this.PortA0IO.AutoSize = true;
         this.PortA0IO.Location = new System.Drawing.Point(12, 201);
         this.PortA0IO.Name = "PortA0IO";
         this.PortA0IO.Size = new System.Drawing.Size(15, 14);
         this.PortA0IO.TabIndex = 36;
         this.PortA0IO.UseVisualStyleBackColor = true;
         this.PortA0IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB0IO
         // 
         this.PortB0IO.AutoSize = true;
         this.PortB0IO.Location = new System.Drawing.Point(84, 201);
         this.PortB0IO.Name = "PortB0IO";
         this.PortB0IO.Size = new System.Drawing.Size(15, 14);
         this.PortB0IO.TabIndex = 44;
         this.PortB0IO.UseVisualStyleBackColor = true;
         this.PortB0IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB1IO
         // 
         this.PortB1IO.AutoSize = true;
         this.PortB1IO.Location = new System.Drawing.Point(84, 178);
         this.PortB1IO.Name = "PortB1IO";
         this.PortB1IO.Size = new System.Drawing.Size(15, 14);
         this.PortB1IO.TabIndex = 43;
         this.PortB1IO.UseVisualStyleBackColor = true;
         this.PortB1IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB2IO
         // 
         this.PortB2IO.AutoSize = true;
         this.PortB2IO.Location = new System.Drawing.Point(84, 155);
         this.PortB2IO.Name = "PortB2IO";
         this.PortB2IO.Size = new System.Drawing.Size(15, 14);
         this.PortB2IO.TabIndex = 42;
         this.PortB2IO.UseVisualStyleBackColor = true;
         this.PortB2IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB3IO
         // 
         this.PortB3IO.AutoSize = true;
         this.PortB3IO.Location = new System.Drawing.Point(84, 132);
         this.PortB3IO.Name = "PortB3IO";
         this.PortB3IO.Size = new System.Drawing.Size(15, 14);
         this.PortB3IO.TabIndex = 41;
         this.PortB3IO.UseVisualStyleBackColor = true;
         this.PortB3IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB4IO
         // 
         this.PortB4IO.AutoSize = true;
         this.PortB4IO.Location = new System.Drawing.Point(84, 109);
         this.PortB4IO.Name = "PortB4IO";
         this.PortB4IO.Size = new System.Drawing.Size(15, 14);
         this.PortB4IO.TabIndex = 40;
         this.PortB4IO.UseVisualStyleBackColor = true;
         this.PortB4IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB5IO
         // 
         this.PortB5IO.AutoSize = true;
         this.PortB5IO.Location = new System.Drawing.Point(84, 86);
         this.PortB5IO.Name = "PortB5IO";
         this.PortB5IO.Size = new System.Drawing.Size(15, 14);
         this.PortB5IO.TabIndex = 39;
         this.PortB5IO.UseVisualStyleBackColor = true;
         this.PortB5IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB6IO
         // 
         this.PortB6IO.AutoSize = true;
         this.PortB6IO.Location = new System.Drawing.Point(84, 63);
         this.PortB6IO.Name = "PortB6IO";
         this.PortB6IO.Size = new System.Drawing.Size(15, 14);
         this.PortB6IO.TabIndex = 38;
         this.PortB6IO.UseVisualStyleBackColor = true;
         this.PortB6IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortB7IO
         // 
         this.PortB7IO.AutoSize = true;
         this.PortB7IO.Location = new System.Drawing.Point(84, 40);
         this.PortB7IO.Name = "PortB7IO";
         this.PortB7IO.Size = new System.Drawing.Size(15, 14);
         this.PortB7IO.TabIndex = 37;
         this.PortB7IO.UseVisualStyleBackColor = true;
         this.PortB7IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC0IO
         // 
         this.PortC0IO.AutoSize = true;
         this.PortC0IO.Location = new System.Drawing.Point(152, 201);
         this.PortC0IO.Name = "PortC0IO";
         this.PortC0IO.Size = new System.Drawing.Size(15, 14);
         this.PortC0IO.TabIndex = 52;
         this.PortC0IO.UseVisualStyleBackColor = true;
         this.PortC0IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC1IO
         // 
         this.PortC1IO.AutoSize = true;
         this.PortC1IO.Location = new System.Drawing.Point(152, 178);
         this.PortC1IO.Name = "PortC1IO";
         this.PortC1IO.Size = new System.Drawing.Size(15, 14);
         this.PortC1IO.TabIndex = 51;
         this.PortC1IO.UseVisualStyleBackColor = true;
         this.PortC1IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC2IO
         // 
         this.PortC2IO.AutoSize = true;
         this.PortC2IO.Location = new System.Drawing.Point(152, 155);
         this.PortC2IO.Name = "PortC2IO";
         this.PortC2IO.Size = new System.Drawing.Size(15, 14);
         this.PortC2IO.TabIndex = 50;
         this.PortC2IO.UseVisualStyleBackColor = true;
         this.PortC2IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC3IO
         // 
         this.PortC3IO.AutoSize = true;
         this.PortC3IO.Location = new System.Drawing.Point(152, 132);
         this.PortC3IO.Name = "PortC3IO";
         this.PortC3IO.Size = new System.Drawing.Size(15, 14);
         this.PortC3IO.TabIndex = 49;
         this.PortC3IO.UseVisualStyleBackColor = true;
         this.PortC3IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC4IO
         // 
         this.PortC4IO.AutoSize = true;
         this.PortC4IO.Location = new System.Drawing.Point(152, 109);
         this.PortC4IO.Name = "PortC4IO";
         this.PortC4IO.Size = new System.Drawing.Size(15, 14);
         this.PortC4IO.TabIndex = 48;
         this.PortC4IO.UseVisualStyleBackColor = true;
         this.PortC4IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC5IO
         // 
         this.PortC5IO.AutoSize = true;
         this.PortC5IO.Location = new System.Drawing.Point(152, 86);
         this.PortC5IO.Name = "PortC5IO";
         this.PortC5IO.Size = new System.Drawing.Size(15, 14);
         this.PortC5IO.TabIndex = 47;
         this.PortC5IO.UseVisualStyleBackColor = true;
         this.PortC5IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC6IO
         // 
         this.PortC6IO.AutoSize = true;
         this.PortC6IO.Location = new System.Drawing.Point(152, 63);
         this.PortC6IO.Name = "PortC6IO";
         this.PortC6IO.Size = new System.Drawing.Size(15, 14);
         this.PortC6IO.TabIndex = 46;
         this.PortC6IO.UseVisualStyleBackColor = true;
         this.PortC6IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // PortC7IO
         // 
         this.PortC7IO.AutoSize = true;
         this.PortC7IO.Location = new System.Drawing.Point(152, 40);
         this.PortC7IO.Name = "PortC7IO";
         this.PortC7IO.Size = new System.Drawing.Size(15, 14);
         this.PortC7IO.TabIndex = 45;
         this.PortC7IO.UseVisualStyleBackColor = true;
         this.PortC7IO.CheckedChanged += new System.EventHandler(this.PortIO_CheckedChanged);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(284, 262);
         this.Controls.Add(this.PortC0IO);
         this.Controls.Add(this.PortC1IO);
         this.Controls.Add(this.PortC2IO);
         this.Controls.Add(this.PortC3IO);
         this.Controls.Add(this.PortC4IO);
         this.Controls.Add(this.PortC5IO);
         this.Controls.Add(this.PortC6IO);
         this.Controls.Add(this.PortC7IO);
         this.Controls.Add(this.PortB0IO);
         this.Controls.Add(this.PortB1IO);
         this.Controls.Add(this.PortB2IO);
         this.Controls.Add(this.PortB3IO);
         this.Controls.Add(this.PortB4IO);
         this.Controls.Add(this.PortB5IO);
         this.Controls.Add(this.PortB6IO);
         this.Controls.Add(this.PortB7IO);
         this.Controls.Add(this.PortA0IO);
         this.Controls.Add(this.PortA1IO);
         this.Controls.Add(this.PortA2IO);
         this.Controls.Add(this.PortA3IO);
         this.Controls.Add(this.PortA4IO);
         this.Controls.Add(this.PortA5IO);
         this.Controls.Add(this.PortA6IO);
         this.Controls.Add(this.PortA7IO);
         this.Controls.Add(this.StatusLabel);
         this.Controls.Add(this.textBox1);
         this.Controls.Add(this.PortCLabel);
         this.Controls.Add(this.PortC0CheckBox);
         this.Controls.Add(this.PortC1CheckBox);
         this.Controls.Add(this.PortC2CheckBox);
         this.Controls.Add(this.PortC3CheckBox);
         this.Controls.Add(this.PortC4CheckBox);
         this.Controls.Add(this.PortC5CheckBox);
         this.Controls.Add(this.PortC6CheckBox);
         this.Controls.Add(this.PortC7CheckBox);
         this.Controls.Add(this.PortBLabel);
         this.Controls.Add(this.PortB0CheckBox);
         this.Controls.Add(this.PortB1CheckBox);
         this.Controls.Add(this.PortB2CheckBox);
         this.Controls.Add(this.PortB3CheckBox);
         this.Controls.Add(this.PortB4CheckBox);
         this.Controls.Add(this.PortB5CheckBox);
         this.Controls.Add(this.PortB6CheckBox);
         this.Controls.Add(this.PortB7CheckBox);
         this.Controls.Add(this.PortALabel);
         this.Controls.Add(this.PortA0CheckBox);
         this.Controls.Add(this.PortA1CheckBox);
         this.Controls.Add(this.PortA2CheckBox);
         this.Controls.Add(this.PortA3CheckBox);
         this.Controls.Add(this.PortA4CheckBox);
         this.Controls.Add(this.PortA5CheckBox);
         this.Controls.Add(this.PortA6CheckBox);
         this.Controls.Add(this.PortA7CheckBox);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.CheckBox PortA7CheckBox;
      private System.Windows.Forms.CheckBox PortA6CheckBox;
      private System.Windows.Forms.CheckBox PortA5CheckBox;
      private System.Windows.Forms.CheckBox PortA4CheckBox;
      private System.Windows.Forms.CheckBox PortA3CheckBox;
      private System.Windows.Forms.CheckBox PortA2CheckBox;
      private System.Windows.Forms.CheckBox PortA1CheckBox;
      private System.Windows.Forms.CheckBox PortA0CheckBox;
      private System.Windows.Forms.Label PortALabel;
      private System.Windows.Forms.Label PortBLabel;
      private System.Windows.Forms.CheckBox PortB0CheckBox;
      private System.Windows.Forms.CheckBox PortB1CheckBox;
      private System.Windows.Forms.CheckBox PortB2CheckBox;
      private System.Windows.Forms.CheckBox PortB3CheckBox;
      private System.Windows.Forms.CheckBox PortB4CheckBox;
      private System.Windows.Forms.CheckBox PortB5CheckBox;
      private System.Windows.Forms.CheckBox PortB6CheckBox;
      private System.Windows.Forms.CheckBox PortB7CheckBox;
      private System.Windows.Forms.Label PortCLabel;
      private System.Windows.Forms.CheckBox PortC0CheckBox;
      private System.Windows.Forms.CheckBox PortC1CheckBox;
      private System.Windows.Forms.CheckBox PortC2CheckBox;
      private System.Windows.Forms.CheckBox PortC3CheckBox;
      private System.Windows.Forms.CheckBox PortC4CheckBox;
      private System.Windows.Forms.CheckBox PortC5CheckBox;
      private System.Windows.Forms.CheckBox PortC6CheckBox;
      private System.Windows.Forms.CheckBox PortC7CheckBox;
      private System.Windows.Forms.Timer timer1;
      private System.Windows.Forms.TextBox textBox1;
      private System.Windows.Forms.Label StatusLabel;
      private System.Windows.Forms.CheckBox PortA7IO;
      private System.Windows.Forms.CheckBox PortA6IO;
      private System.Windows.Forms.CheckBox PortA5IO;
      private System.Windows.Forms.CheckBox PortA4IO;
      private System.Windows.Forms.CheckBox PortA3IO;
      private System.Windows.Forms.CheckBox PortA2IO;
      private System.Windows.Forms.CheckBox PortA1IO;
      private System.Windows.Forms.CheckBox PortA0IO;
      private System.Windows.Forms.CheckBox PortB0IO;
      private System.Windows.Forms.CheckBox PortB1IO;
      private System.Windows.Forms.CheckBox PortB2IO;
      private System.Windows.Forms.CheckBox PortB3IO;
      private System.Windows.Forms.CheckBox PortB4IO;
      private System.Windows.Forms.CheckBox PortB5IO;
      private System.Windows.Forms.CheckBox PortB6IO;
      private System.Windows.Forms.CheckBox PortB7IO;
      private System.Windows.Forms.CheckBox PortC0IO;
      private System.Windows.Forms.CheckBox PortC1IO;
      private System.Windows.Forms.CheckBox PortC2IO;
      private System.Windows.Forms.CheckBox PortC3IO;
      private System.Windows.Forms.CheckBox PortC4IO;
      private System.Windows.Forms.CheckBox PortC5IO;
      private System.Windows.Forms.CheckBox PortC6IO;
      private System.Windows.Forms.CheckBox PortC7IO;

   }
}

