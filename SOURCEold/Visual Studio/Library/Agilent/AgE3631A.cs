﻿//******************************************************************************
//
//   AgE3631A.cs     Agilent E3631A power supply
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;

namespace AgilentInstruments {

   public class AgE3631A : PowerSupply {
      //***************************** Public ***********************************

      public enum OutputList {
         P6V,
         P25V,
         N25V
      }

      //***************************** Public ***********************************

      //************************************************************************
      // Constructor
      //************************************************************************

      public AgE3631A()
         : base() {
      }

      //************************************************************************
      // Set
      //************************************************************************

      public bool Set(double Voltage, double Current, OutputList Output)
         // Set voltage
      {
         return SendCommand(String.Format(PowerSupplyCultureInfo.NumberFormat, "APPL " + TranslateOutput(Output) + ", {0:0.00000}, {1:0.00000}", Voltage, Current));
      } // SetVoltage

      //************************************************************************
      // SetVoltage
      //************************************************************************

      public bool SetVoltage(double Voltage, OutputList Output)
         // Set voltage
      {
         return SendCommand(String.Format(PowerSupplyCultureInfo.NumberFormat, "APPL " + TranslateOutput(Output) + ", {0:0.00000}", Voltage));
      } // SetVoltage

      //************************************************************************
      // Set current
      //************************************************************************

      public bool SetCurrent(double Voltage, OutputList Output)
         // Set voltage
      {
         if(!SelectOutput(Output)) {
            return false;
         }

         return base.SetCurrent(Voltage);
      } // SetVoltage

      //************************************************************************
      // Get voltage & current
      //************************************************************************

      public bool Get(ref double Voltage, ref double Current, OutputList Output)
         // Get settings
      {
         if(!SelectOutput(Output)) {
            return false;
         }

         return base.Get(ref Voltage, ref Current);
      }

      //************************************************************************
      // Measure current
      //************************************************************************

      public bool MeasureCurrent(ref double Current, OutputList Output)
         // Get settings
      {
         if(!SendCommand("MEAS:CURR? " + TranslateOutput(Output))) {
            return false;
         }

         if(!ReadNumericValue(ref Current)) {
            return false;
         }

         return (true);
      } // Measure

      //************************************************************************
      // Measure voltage
      //************************************************************************

      public bool MeasureVoltage(ref double Voltage, OutputList Output)
         // Get settings
      {
         if(!SendCommand("MEAS:VOLT? " + TranslateOutput(Output))) {
            return false;
         }

         if(!ReadNumericValue(ref Voltage)) {
            return false;
         }

         return (true);
      } // Measure

      //*************************** Protected **********************************

      //************************************************************************
      // SelectOutput
      //************************************************************************

      protected bool SelectOutput(OutputList Output)
         // Set output
      {
         return SendCommand("INST " + TranslateOutput(Output));
      } // SetOutput

      protected string TranslateOutput(OutputList Output) {
         switch(Output) {
            case OutputList.P6V:
               return "P6V";

            case OutputList.P25V:
               return "P25V";

            case OutputList.N25V:
               return "N25V";

            default:
               throw new Exception("Unknown output");
         }
      }

   }

}