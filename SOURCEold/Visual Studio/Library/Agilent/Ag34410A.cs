﻿//******************************************************************************
//
//   Ag34410A.cs     Agilent 34410A multimeter
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.Threading;
using Agilent.Agilent34410.Interop;


namespace AgilentInstruments {

   public class Ag34410A {
      //***************************** Public ***********************************

      public DCVoltageRangeList DCVoltageRange {
         get {
            return _DCVoltageRange;
         }

         set {
            _DCVoltageRange = value;
         }
      }

      public DCCurrentRangeList DCCurrentRange {
         get {
            return _DCCurrentRange;
         }

         set {
            _DCCurrentRange = value;
         }
      }

      public Resolution DCVoltageResolution {
         get {
            return _DCVoltageResolution;
         }

         set {
            _DCVoltageResolution = value;
         }
      }

      public Resolution DCCurrentResolution {
         get {
            return _DCCurrentResolution;
         }

         set {
            _DCCurrentResolution = value;
         }
      }

      public bool IsOpen {
         get {
            return AgilentMultimeter.Initialized;
         }
      }

      //*************************** Protected **********************************

      protected Agilent34410 AgilentMultimeter;

      // Resolution & range default values
      protected const DCVoltageRangeList DEFAULT_DC_VOLT_RANGE = DCVoltageRangeList._10V;
      protected const DCCurrentRangeList DEFAULT_DC_CURR_RANGE = DCCurrentRangeList._1A;

      protected const Resolution DEFAULT_DC_VOLT_RES = Resolution.Default;
      protected const Resolution DEFAULT_DC_CURR_RES = Resolution.Default;

      // Resolution & range
      protected DCVoltageRangeList _DCVoltageRange;
      protected DCCurrentRangeList _DCCurrentRange;

      protected Resolution _DCVoltageResolution;
      protected Resolution _DCCurrentResolution;

      public enum DCVoltageRangeList {
         Auto,
         _100mV,
         _1V,
         _10V,
         _100V,
         _1000V
      }

      public enum DCCurrentRangeList {
         Auto,
         _100uA,
         _1mA,
         _10mA,
         _100mA,
         _1A,
         _3A
      }

      public enum Resolution {
         Best,
         Default,
         Least
      };

      //***************************** Public ***********************************

      //************************************************************************
      // Constructor
      //************************************************************************

      public Ag34410A() {
	       AgilentMultimeter = new Agilent34410();
      }

      //************************************************************************
      // Connect
      //************************************************************************

      public bool Connect(string Address) {
         try {
            if(IsOpen) {
               AgilentMultimeter.Close();
            }

            string[] AddressParts = Address.Split(':');
            
            // Make sure the address is valid
            if(AddressParts[0].Substring(0, 3) == "USB") {
               Address += "::0::INSTR";
            } else if(AddressParts[0].Substring(0, 3) == "LAN") {
               Address += "::inst0::INSTR";
            } else if(AddressParts[0].Substring(0, 4) == "GPIB") {
               Address += "::INSTR";
            } else {
               return false;
            }

            // Open
            AgilentMultimeter.Initialize(Address, false, true);

            if(!IsOpen) {
               return false;
            }

            // Set default ranges & resolutions
            DCVoltageRange = DEFAULT_DC_VOLT_RANGE;
            DCCurrentRange = DEFAULT_DC_CURR_RANGE;

            DCVoltageResolution = DEFAULT_DC_VOLT_RES;
            DCCurrentResolution = DEFAULT_DC_CURR_RES;

         } catch(Exception) {
            return false;
         }

         return true;
      }

      //************************************************************************
      // Connect
      //************************************************************************

      public void Close() {
         if(!AgilentMultimeter.Initialized) {
            return;
         }

         AgilentMultimeter.Close();
      }

      //************************************************************************
      // ReadDCVoltage
      //************************************************************************

      public bool ReadDCVoltage(ref double Voltage) {
         try {
            if(!IsOpen) {
               return false;
            }

            // Configure range & resolution
            AgilentMultimeter.Voltage.DCVoltage.Configure(TranslateVoltRangeToAgilent(DCVoltageRange), TranslateResolutionToAgilent(_DCVoltageResolution));
         
            // Do reading
            if(!DoReading(ref Voltage)) {
               return false;
            }

         } catch(Exception) {
            return false;
         }

         return true;
      }

      //************************************************************************
      // ReadDCCurrent
      //************************************************************************

      public bool ReadDCCurrent(ref double Current) {
         try {
            if(!IsOpen) {
               return false;
            }

            // Configure range & resolution
            AgilentMultimeter.Current.DCCurrent.Configure(TranslateCurrRangeToAgilent(DCCurrentRange), TranslateResolutionToAgilent(_DCCurrentResolution));

            // Do reading
            if(!DoReading(ref Current)) {
               return false;
            }

         } catch(Exception) {
            return false;
         }

         return true;
      }

      //************************************************************************
      // MaxDCCurrent
      //************************************************************************

      public double MaxDCCurrent {
          get {
            return AgilentMultimeter.Current.DCCurrent.Range;
          }
      }

      //************************************************************************
      // MaxDCVoltage
      //************************************************************************

      public double MaxDCVoltage {
          get {
            return AgilentMultimeter.Voltage.DCVoltage.Range;
          }
      }
      //*************************** Protected **********************************

      //************************************************************************
      // DoReading
      //************************************************************************

      protected bool DoReading(ref double Reading) {
         try {
            // Get one sample
            short SampleCount = 1;

            // Immediate trigger
            AgilentMultimeter.Trigger.TriggerSource = Agilent34410TriggerSourceEnum.Agilent34410TriggerSourceImmediate;

            // No delay
            AgilentMultimeter.Trigger.TriggerDelay = 0;

            // Number of readings
            AgilentMultimeter.Trigger.SampleCount = SampleCount;

            AgilentMultimeter.Measurement.Initiate();

            // Wait for readings complete
            while(AgilentMultimeter.Measurement.get_ReadingCount(Agilent34410MemoryTypeEnum.Agilent34410MemoryTypeReadingMemory) != SampleCount) ;

            Thread.Sleep(1); // multimetr občas hodil error - Query unterminated nebo Data Stale

            double[] Readings = new double[SampleCount];

            // Get Readings
            Readings = AgilentMultimeter.Measurement.RemoveReadings(SampleCount);

            Reading = Readings[0];

            // Overload
            if(Math.Abs(Reading) > 9e20) {
               return false;
            }

         } catch(Exception) {
            return false;
         }

         return true;
      }

      //************************************************************************
      // TranslateResolutionToAgilent
      //************************************************************************

      protected Agilent34410ResolutionEnum TranslateResolutionToAgilent(Resolution Res) {
         switch(Res) {
            case Resolution.Best:
               return Agilent34410ResolutionEnum.Agilent34410ResolutionBest;

            case Resolution.Least:
               return Agilent34410ResolutionEnum.Agilent34410ResolutionLeast;

            default:
               return Agilent34410ResolutionEnum.Agilent34410ResolutionDefault;
         }
      }

      //************************************************************************
      // TranslateVoltRangeToAgilent
      //************************************************************************

      protected double TranslateVoltRangeToAgilent(DCVoltageRangeList Range) {
         switch(Range) {
            case DCVoltageRangeList._100mV:
               return 0.1;

            case DCVoltageRangeList._1V:
               return 1;

            case DCVoltageRangeList._10V:
               return 10;

            case DCVoltageRangeList._100V:
               return 100;

            case DCVoltageRangeList._1000V:
               return 1000;

            default: // autoranging
               return -1;
         }
      }

      //************************************************************************
      // TranslateCurrRangeToAgilent
      //************************************************************************

      protected double TranslateCurrRangeToAgilent(DCCurrentRangeList Range) {
         switch(Range) {
            case DCCurrentRangeList._100uA:
               return 0.0001;

            case DCCurrentRangeList._1mA:
               return 0.001;

            case DCCurrentRangeList._10mA:
               return 0.01;

            case DCCurrentRangeList._100mA:
               return 0.1;

            case DCCurrentRangeList._1A:
               return 1;

            case DCCurrentRangeList._3A:
               return 3;

            default: // autoranging
               return -1;
         }
      }
   } // class

} // namespace