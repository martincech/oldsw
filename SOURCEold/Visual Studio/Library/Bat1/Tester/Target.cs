﻿//******************************************************************************
//
//   Target.cs     Target
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Uart;
using System.Globalization;
using Device.Ftxx;
using System.Threading;
using System.Diagnostics;

namespace Bat1.Tester {

   /// <summary>
   /// Target. Test firmware must be loaded in target in order to use this class.</summary>

   public class Target {
      private Config.Target Config;

      public TTest Test;
      public TUsb Usb;

      public Target(Config.Target _Config, Config.TargetCommon.TUsb _UsbConfig, Config.TargetCommon.TCom _ComConfig) {
         Config = _Config;

         Test = new TTest(Config.ComPortSettings, _ComConfig);
         Usb = new TUsb(Config.ComUsbSettings, _UsbConfig);
      }

      /// <summary>
      /// Close</summary>

      public bool Close() {
         Test.Close();
         Usb.Close();

         return true;
      }

      public class TUsb {
         //public static object Locker = Ftdi.Locker;

         private Config.TargetCommon.TUsb UsbConfig;
         private Device.Uart.Config.UsbLocUartSettings ComSettings;

         private Ft232 FtdiDevice = new Ft232();

         private UsbUart Com = new UsbUart();

         /// <summary>
         /// IsOpen</summary>

         public bool IsOpen {
            get {
               return FtdiDevice.IsOpen;
            }
         }

         /// <summary>
         /// Default constructor</summary>
         /// <param name="Configuration">Module configuration</param>

         public TUsb(Device.Uart.Config.UsbLocUartSettings _ComSettings, Config.TargetCommon.TUsb _UsbConfig) {
            ComSettings = _ComSettings; // COM config
            UsbConfig = _UsbConfig;     // Test Config
         }

         /// <summary>
         /// Open UART over USB</summary>

         public bool Open() {
            int Identifier = 0;

            if(!Com.Locate(ComSettings.LocId, ref Identifier)) {
               Trace.WriteLine("Target USB Com open: Can't locate");
               return false;
            }

            Thread.Sleep(500);

            if(!Com.Open(Identifier)) {
               Trace.WriteLine("Target Com open: Can't open");
               return false;
            }

            Thread.Sleep(500);

            if(!Com.SetParameters(ComSettings.BaudRate, ComSettings.Parity, ComSettings.Bits, ComSettings.StopBit, ComSettings.Handshake)) {
               Trace.WriteLine("Target Com open: Can't set parameters");
               return false;
            }

            Com.SetRxWait(UsbConfig.RxTimeout);
            Com.Rts = false;
            Com.Dtr = false;

            return true;
         }

         /// <summary>
         /// Test UART over USB</summary>

         public bool TestUart() {
            Com.Flush();

            byte[] Data = new byte[1];

            // prepare data :
            for(int i = 0; i < UsbConfig.TestDataSize; i++) {
               Data[0] = (byte)i;

               // send data :
               if(Com.Write(Data, 1) != 1) {
                  Trace.WriteLine("Target USB test: Can't write");
                  return (false);
               }

               // receive reply :
               if(Com.Read(Data, 1) != 1) {
                  Trace.WriteLine("Target USB test: Can't read");
                  return (false);
               }

               // check
               if(Data[0] != (byte)i) {
                  Trace.WriteLine("Target USB test: Check failed");
                  return (false);
               }
            }

            return true;
         }

         /// <summary>
         /// Close UART over USB</summary>

         public void Close() {
            Trace.WriteLine("Target: closing");
            Com.Close();
            FtdiDevice.Close();
         }

         /// <summary>
         /// Detect programmed or blank device</summary>
         /// <returns>
         /// True if device detected, false if timeout</returns>

         public bool DetectDevice() {
            return (Wait(UsbConfig.ConnectDelay));
         }

         /// <summary>
         /// Detect programmed device</summary>
         /// <returns>
         /// True if device detected, false if timeout</returns>

         public bool CompareUsbName(string NameToCompare) {
            if(!Detect()) {
               return false;
            }

            string Name;

            FtdiDevice.GetDescription(out Name);

            if(Name != NameToCompare) {
               FtdiDevice.Close();
               Trace.WriteLine("Target USB Compare: Name is not programmed");
               return false;
            }

            FtdiDevice.Close();

            return true;
         }

         /// <summary>
         /// Write FTDI EEPROM</summary>

         public bool WriteEeprom(Config.FactoryConfig.TFtdi ConfigData) {
            if(Com.IsOpen) {
               Trace.WriteLine("Target FTDI EEPROM: Com is open");
               return false; // Uart is open, user must close it
            }

            if(ConfigData.DeviceName.Length > 63) {
               Trace.WriteLine("Target FTDI EEPROM: Device name too long");
               return false;
            }
            if(ConfigData.ManufacturerName.Length > 31) {
               Trace.WriteLine("Target FTDI EEPROM: Manufacturer name too long");
               return false;
            }

            try {
               try {
                  Ftdi.FT_STATUS Status;

                  Status = FtdiDevice.OpenByLocation(ComSettings.LocId);

                  if(Status != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception("Target FTDI EEPROM: Can't open ftdi by location = " + ComSettings.LocId.ToString() + " Status " + Status);
                  }

                  if(!FtdiDevice.IsOpen) {
                     throw new Exception("Target FTDI EEPROM: Device not opened");
                  }

                  Ft232.EEPROM_STRUCTURE PgmData = new Ft232.EEPROM_STRUCTURE();

                  Status = FtdiDevice.ReadEEPROM(PgmData);

                  if(Status != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception("Target FTDI EEPROM: Can't read eeprom  Status " + Status);
                  }

                  PgmData.Cbus0 = 3;
                  PgmData.Cbus1 = 2;
                  PgmData.Cbus2 = 0;
                  PgmData.Cbus3 = 1;
                  PgmData.Cbus4 = 5;
                  PgmData.Description = ConfigData.DeviceName;
                  PgmData.EndpointSize = 64;
                  PgmData.HighDriveIOs = false;
                  PgmData.InvertCTS = false;
                  PgmData.InvertDCD = false;
                  PgmData.InvertDSR = false;
                  PgmData.InvertDTR = false;
                  PgmData.InvertRI = false;
                  PgmData.InvertRTS = false;
                  PgmData.InvertRXD = false;
                  PgmData.InvertTXD = false;
                  PgmData.Manufacturer = ConfigData.ManufacturerName;
                  PgmData.ManufacturerID = "00";
                  PgmData.MaxPower = ConfigData.MaxPower;
                  PgmData.ProductID = 0x6001;
                  PgmData.PullDownEnable = false;
                  PgmData.RemoteWakeup = false;
                  PgmData.RIsD2XX = true;
                  PgmData.SelfPowered = ConfigData.SelfPowered;
                  PgmData.SerialNumber = "00000000";
                  PgmData.SerNumEnable = false;
                  PgmData.UseExtOsc = false;
                  PgmData.VendorID = 0x0403;

                  Status = FtdiDevice.WriteEEPROM(PgmData);

                  if(Status != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception("Target FTDI EEPROM: Can't write eeprom Status " + Status);
                  }

               } finally {
                  FtdiDevice.Close();
               }
            } catch(Exception e) {
               Trace.WriteLine(e.Message);
               return false;
            }

            return true;
         }

         /// <summary>
         /// Wait for device</summary>
         /// True - device connected, false - timeout expires</returns>

         private bool Wait(int Timeout) {
            for(int i = 0; i < Timeout; i++) {
               Trace.WriteLine("Target USB wait: Retry " + i.ToString());

               if(Detect()) {
                  FtdiDevice.Close();
                  return (true);
               }

               FtdiDevice.Close();

               Thread.Sleep(1000);
            }

            return (false);
         }

         /// <summary>
         /// Wait untill device disconnects</summary>
         /// <returns>
         /// True - no device connected, false - timeout expires</returns>

         public bool WaitForDisconnect()
            // Wait for disconnected device (leave closed)
         {
            for(int i = 0; i < UsbConfig.ConnectDelay; i++) {
               Trace.WriteLine("Target USB disconnect wait: Retry " + i.ToString());

               if(!Detect()) {
                  FtdiDevice.Close();
                  return (true);
               }

               FtdiDevice.Close();

               Thread.Sleep(1000);
            }

            return false;
         } // WaitForDisconnect

         /// <summary>
         /// Search for device</summary>

         private bool Detect() {
            if(Com.IsOpen) {
               Trace.WriteLine("Target USB Detect: Com is open");
               Close();
               if(Com.IsOpen) {
                  Trace.WriteLine("Target USB Detect: still open");
               }
               return false;
            }

            try {
               FtdiDevice.Close(); // někdy hodí výjimku...
            } catch(Exception e) {
               Trace.WriteLine("Target USB Detect: FtdiDevice.Close() exception " + e.Message);
            }

            if(FtdiDevice.OpenByLocation(ComSettings.LocId) != Ftdi.FT_STATUS.FT_OK) {
               Trace.WriteLine("Target USB Detect: Can't open ftdi by location = " + ComSettings.LocId.ToString());
               return false;
            }

            return true;
         }
      }

























      public class TTest {
         private ComUart Port = new ComUart();

         private Config.TargetCommon.TCom ComConfig;

         private Device.Uart.Config.ComUartSettings Config;

         /// <summary>
         /// Target replies</summary>
         private const byte TEST_OK = (byte)'Y';
         private const byte TEST_ERROR = (byte)'N';

         private struct CmdStruct {
            public string Code;
            public int ReplyLength;
            public int ReplyTimeout;

            public CmdStruct(string code, int length, int timeout)
               : this() {
               Code = code;
               ReplyLength = length;
               ReplyTimeout = timeout;
            }
         }

         /// <summary>
         /// Commands definition</summary>

         private static class Commands {
            public static readonly CmdStruct TEST_ADC = new CmdStruct("A", 9, 3000);
            public static readonly CmdStruct TEST_CHARGER_ON = new CmdStruct("C", 1, 2000);
            public static readonly CmdStruct TEST_DISPLAY = new CmdStruct("D", 1, 3000);
            public static readonly CmdStruct TEST_OFF = new CmdStruct("F", 1, 1000);
            public static readonly CmdStruct TEST_IADC = new CmdStruct("I", 9, 3000);
            public static readonly CmdStruct TEST_MEMORY = new CmdStruct("M", 1, 24000);
            public static readonly CmdStruct TEST_CHARGER_OFF = new CmdStruct("O", 1, 2000);
            public static readonly CmdStruct TEST_POWER = new CmdStruct("P", 5, 500);
            public static readonly CmdStruct TEST_RTC = new CmdStruct("R", 1, 5000);
            public static readonly CmdStruct TEST_SOUND = new CmdStruct("S", 1, 2000);
            public static readonly CmdStruct TEST_USB = new CmdStruct("U", 1, 2000);
            public static readonly CmdStruct TEST_BACKLIGHT_ON = new CmdStruct("B", 1, 1000);
            public static readonly CmdStruct TEST_BACKLIGHT_OFF = new CmdStruct("G", 1, 1000);
            public static readonly CmdStruct TEST_KEYBOARD = new CmdStruct("K", 1 + Keyboard.KEYS_COUNT, 500);
         }

         private System.Text.ASCIIEncoding Enc = new System.Text.ASCIIEncoding();

         private byte[] Reply = new byte[10];

         /// <summary>
         /// IsOpen</summary>

         public bool IsOpen {
            get {
               return Port.IsOpen;
            }
         }

         /// <summary>
         /// Default constructor</summary>
         /// <param name="Configuration">Module configuration</param>

         public TTest(Device.Uart.Config.ComUartSettings _Config, Config.TargetCommon.TCom _ComConfig) {
            Config = _Config;
            ComConfig = _ComConfig;
         }

         /// <summary>
         /// Open</summary>

         public bool Open() {
            if(Port.IsOpen) {
               Port.Close();
            }

            int Identifier = 0;

            if(!Port.Locate(Config.PortName, ref Identifier)) {
               return (false);
            }

            if(!Port.Open(Identifier)) {
               return (false);
            }

            Port.SetParameters(Config.BaudRate, Config.Parity, Config.Bits, Config.StopBit, Config.Handshake);

            Port.SetRxNowait();
            Port.Flush();

            return (true);
         }

         /// <summary>
         /// Close</summary>

         public bool Close() {
            Port.Close();

            return true;

         }

         /// <summary>
         /// Communication test</summary>

         public bool TestUart() {
            byte[] Buffer = new byte[ComConfig.TestDataSize];

            Port.SetRxWait(ComConfig.RxTimeout);
            Port.Flush();

            byte[] DataRead = new byte[1];
            byte[] DataWrite = new byte[1];
            DataWrite[0] = (byte)'a';

            for(int i = 0; i < ComConfig.TestDataSize; i++) {
               if(Port.Write(DataWrite, 1) != 1) {
                  Trace.WriteLine("Target Test UART: Can't write");
                  return (false);
               }

               if(Port.Read(DataRead, 1) != 1) {
                  Trace.WriteLine("Target Test UART: Can't read");
                  return (false);
               }

               if(DataRead[0] != DataWrite[0]) {
                  Trace.WriteLine("Target Test UART: Check failed");
                  return false;
               }

               if(DataWrite[0] < (byte)'z') {
                  DataWrite[0]++;
               } else {
                  DataWrite[0] = (byte)'a';
               }
            }

            return (true);
         }

         /// <summary>
         /// Presence test</summary>
         /// <remarks>
         /// Target is present if responds when character is transmited over UART.</remarks>

         public bool TestPresence(ref bool Present) {
            Port.SetRxWait(ComConfig.RxTimeout / 10);
            Port.Flush();

            byte[] DataRead = new byte[1];
            byte[] DataWrite = new byte[1];

            DataWrite[0] = (byte)'a';

            if(Port.Write(DataWrite, 1) != 1) {
               Trace.WriteLine("Target Test presence: Can't write");
               return (false);
            }

            if(Port.Read(DataRead, 1) != 1) {
               Trace.WriteLine("Target Test presence: Can't read");
               Present = false;
               return (true);
            }

            if(DataRead[0] != DataWrite[0]) {
               Trace.WriteLine("Target Test presence: Check error");
               return false;
            }

            Present = true;
            return true;
         }

         /// <summary>
         /// Display test</summary>

         public bool TestDisplay() {
            return (Test(Commands.TEST_DISPLAY));
         }

         /// <summary>
         /// Memory test</summary>

         public bool TestMemory() {
            return (Test(Commands.TEST_MEMORY));
         }

         /// <summary>
         /// RTC test</summary>

         public bool TestRtc() {
            return (Test(Commands.TEST_RTC));
         }

         /// <summary>
         /// Sound test</summary>

         public bool TestSound() {
            return (Test(Commands.TEST_SOUND));
         }

         /// <summary>
         /// Read ADC value</summary>

         public bool ReadAdc(ref int Value) {
            if(!Test(Commands.TEST_ADC)) {
               return (false);
            }

            string HexString = (new CString(Reply, Commands.TEST_ADC.ReplyLength - 1)).ToString();

            if(!Int32.TryParse(HexString, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out Value)) {
               Trace.WriteLine("Target Test Adc: not valid int");
               return false;
            }

            return (true);
         }

         /// <summary>
         /// Read internal ADC value</summary>

         public bool ReadIadc(ref int Value) {
            if(!Test(Commands.TEST_IADC)) {
               return (false);
            }

            string HexString = (new CString(Reply, Commands.TEST_IADC.ReplyLength - 1)).ToString();

            if(!Int32.TryParse(HexString, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out Value)) {
               Trace.WriteLine("Target Test Iadc: not valid int");
               return false;
            }

            return (true);

         }

         /// <summary>
         /// Start USB test</summary>
         /// <remarks>
         /// Next, USB UART should be tested.</remarks>

         public bool StartUsb() {
            return (StartTest(Commands.TEST_USB));
         }

         /// <summary>
         /// Get USB test result</summary>

         public bool UsbResult() {
            return (TestResult(Commands.TEST_USB));
         }

         /// <summary>
         /// Enable/disable charger</summary>

         public bool EnableCharger(bool Enable) {
            if(Enable) {
               return (Test(Commands.TEST_CHARGER_ON));
            } else {
               return (Test(Commands.TEST_CHARGER_OFF));
            }
         }

         /// <summary>
         /// Test power</summary>

         public bool TestPower(ref bool PPR, ref bool CHG, ref bool USBON, ref bool ENNAB)
            // Read power status
          {
            if(!Test(Commands.TEST_POWER)) {
               return (false);
            }

            PPR = (Reply[0] == 'H');
            CHG = (Reply[1] == 'H');
            USBON = (Reply[2] == 'H');
            ENNAB = (Reply[3] == 'H');
            return (true);
         }

         /// <summary>
         /// Switch off</summary>

         public bool SwitchOff() {
            return (Test(Commands.TEST_OFF));
         }

         /// <summary>
         /// Switch on/off backlight</summary>

         public bool SetBacklight(bool On) {
            if(On) {
               return (Test(Commands.TEST_BACKLIGHT_ON));
            } else {
               return (Test(Commands.TEST_BACKLIGHT_OFF));
            }
         }

         /// <summary>
         /// Read keyboard state</summary>

         public bool ReadKeyboard(ref Keyboard Keyb) {
            if(!Test(Commands.TEST_KEYBOARD)) {
               return false;
            }

            for(int i = 0; i < Keyboard.KEYS_COUNT; i++) {
               Keyb[i] = Reply[i] == 'H' ? Keyboard.Key.KeyState.RELEASED : Keyboard.Key.KeyState.PRESSED;
            }

            return true;
         }

         /// <summary>
         /// Test</summary>

         private bool Test(CmdStruct Cmd) {
            if(!StartTest(Cmd)) {
               Trace.WriteLine("Target Start test: Failed");
               return false;
            }

            if(!TestResult(Cmd)) {
               Trace.WriteLine("Target Test result: Failed");
               return false;
            }

            return true;
         }

         /// <summary>
         /// Start test</summary>

         private bool StartTest(CmdStruct Cmd) {
            if(!IsOpen) {
               Trace.WriteLine("Target: StartTest " + Cmd.Code.ToString() + ": Not open, opening");
               Open();
               return false;
            }

            Port.Flush();

            if(Port.Write(Enc.GetBytes(Cmd.Code), 1) == 0) {
               Trace.WriteLine("Target: StartTest " + Cmd.Code.ToString() + ": Can't write");
               return false;
            }

            return true;
         }

         /// <summary>
         /// Test result</summary>

         private bool TestResult(CmdStruct Cmd) {
            Port.SetRxWait(Cmd.ReplyTimeout);

            int size = 0;

            if((size = Port.Read(Reply, Cmd.ReplyLength)) != Cmd.ReplyLength) {
               Trace.WriteLine("Target: TestResult " + Cmd.Code.ToString() + ": Read: expected, received " + size.ToString());
               return false;
            }

            if(Reply[Cmd.ReplyLength - 1] != TEST_OK) {
               Trace.WriteLine("Target: TestResult " + Cmd.Code.ToString() + ": Test failed");
               return false;
            }

            return true;
         }
      }

      public class Keyboard {
         public class Key {
            public enum KeyState {
               PRESSED,
               RELEASED
            }

            public KeyState State = KeyState.RELEASED;
         }

         public static readonly int KEYS_COUNT = 4;

         public Key On = new Key();
         public Key K0 = new Key();
         public Key K1 = new Key();
         public Key K2 = new Key();

         private Key[] _Keys = new Key[KEYS_COUNT];

         public Keyboard() {
            _Keys[0] = On;
            _Keys[1] = K0;
            _Keys[2] = K1;
            _Keys[3] = K2;
         }

         public Key.KeyState this[int Index] {
            get {
               if(Index < 0 || Index >= KEYS_COUNT) {
                  // exception;
                  return Key.KeyState.RELEASED;
               }

               return _Keys[Index].State;
            }

            set {
               if(Index < 0 || Index >= KEYS_COUNT) {
                  // exception;
                  return;
               }

               _Keys[Index].State = value;
            }
         }
      }
   }
}