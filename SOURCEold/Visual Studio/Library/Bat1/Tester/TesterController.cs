﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Threading;
using Device.Ftxx;
using System.Diagnostics;

namespace Bat1.Tester {
   public class TesterController
   {
      public Tester[] Testers;
      public TextWriterTraceListenerWithTime[] Listeners;

      public Bat1Dll Dll; // shared Dll

      public Multimeter Multimeter; // shared multimeter

      public object TestersLocker = new object();
      private object Locker = new object();
      private object TesterBlockingLocker = new object();

      public Config.TesterCommon TesterCommonConfig;
      public Config.TargetCommon TargetCommonConfig;
      public Config.FactoryConfig FactoryConfiguration;
      public Config.Tests TestsConfig;
      public Config.TestBatch.TTestBatchCollection TestBatches;

      private Tester LockingTester;

      public void LockAllTesters(Tester BlockingTester) {
         Trace.WriteLine("LockAllTesters");
         if(LockingTester == BlockingTester) {
            return;
         }

         Monitor.Enter(TesterBlockingLocker);
         LockingTester = BlockingTester;

         foreach(Tester Tester in Testers) {
            if(Tester == BlockingTester) { // don't lock itself
               Trace.WriteLine("Locking: skip myself " + Tester.ID.ToString());
               continue;
            }
            Trace.WriteLine("Locking " + Tester.ID.ToString());
            Monitor.Enter(Tester);
            Trace.WriteLine("Locked " + Tester.ID.ToString());
         }
      }
      
      public void ReleaseAllTesters(Tester BlockingTester) {
         Trace.WriteLine("ReleaseAllTesters");
         if(LockingTester == null) {
            return;
         }

         if(LockingTester != BlockingTester) {
            return;
         }

         foreach(Tester Tester in Testers) {
            if(Tester == BlockingTester) { // don't lock itself
               Trace.WriteLine("Unlocking: skip myself " + Tester.ID.ToString());
               continue;
            }

            Trace.WriteLine("Unlocking " + Tester.ID.ToString());
            Monitor.Exit(Tester);
            Trace.WriteLine("Unlocked " + Tester.ID.ToString());
         }

         LockingTester = null;
         Monitor.Exit(TesterBlockingLocker);
      }

      public void GetDll(ref Bat1Dll Resource, Tester Tester) {
         lock(Locker) {
            Resource = Dll;
            Monitor.Enter(Resource);

            Dll.FtdiLocId(Tester.MyDevicesConfig.Target.ComUsbSettings.LocId);
         }
      }

      public void GetMultimeter(ref Multimeter Resource, Tester Tester) {
         lock(Locker) {
            Resource = Multimeter;
            Monitor.Enter(Resource);

            if(!Multimeter.IsOpen) {
               if(!Multimeter.Open()) {
                  throw new Tester.TestException("Can't open multimeter");
               }
            }
         }
      }

      public void ReleaseDll(ref Bat1Dll Resource, Tester Tester) {
         Dll.Close();
         Monitor.Exit(Resource);
         Resource = null;
      }

      public void ReleaseMultimeter(ref Multimeter Resource, Tester Tester) {
         Monitor.Exit(Resource);
         Resource = null;
      }

      public TesterController()
	   {
         Config.Tester TesterConfig = ConfigurationManager.GetSection("Tester") as Config.Tester;
         TesterCommonConfig = ConfigurationManager.GetSection("TesterCommon") as Config.TesterCommon;
         TargetCommonConfig = ConfigurationManager.GetSection("TargetCommon") as Config.TargetCommon;
         TestBatches = (ConfigurationManager.GetSection("TestBatch") as Config.TestBatch).TestBatches;
         TestsConfig = ConfigurationManager.GetSection("Tests") as Config.Tests;
         FactoryConfiguration = ConfigurationManager.GetSection("FactoryConfig") as Config.FactoryConfig;
         
         if(TestBatches == null
            || TargetCommonConfig == null
            || TesterCommonConfig == null
            || TestsConfig == null
            || FactoryConfiguration == null
            || FactoryConfiguration.Ftdi.ManufacturerName == null) { // občas naskočila výjimka, že nějaký objekt z konfigurace není inicializovaný (=null)
            throw new ConfigurationErrorsException("Null sections");
         }

         Testers = new Tester[TesterConfig.Testers.Count];
         Listeners = new TextWriterTraceListenerWithTime[TesterConfig.Testers.Count];

         Dll = new Bat1Dll();
         Dll.CheckDevice();

         for(int i = 0; i < TesterConfig.Testers.Count; i++) {
            FileStreamWithBackup fs = new FileStreamWithBackup(i.ToString() + "TesterTrace.txt", 1000000, 10, System.IO.FileMode.Append);
            fs.CanSplitData = false;
            Listeners[i] = new TextWriterTraceListenerWithTime(fs);
            Trace.AutoFlush = true;
            Trace.Listeners.Add(Listeners[i]);

            AnnounceThread(Testers[i]);
            Testers[i] = new Tester(this, TesterConfig.Testers[i], TesterCommonConfig, TargetCommonConfig, FactoryConfiguration, TestsConfig, TestBatches);
            Testers[i].ID = TesterConfig.Testers[i].ID;
            RemoveThread(Testers[i]);
         }

         Multimeter = new Multimeter(TesterConfig.Testers[0].Instruments.Multimeter);
	   }

      public void AnnounceThread(Tester AnnouncingTester) {
         int i = 0;

         foreach(Tester Tester in Testers) {
            if(Tester == AnnouncingTester) {
               Listeners[i].ThreadIdentifier = Thread.CurrentThread.ManagedThreadId;
               continue;
               return;
            }
            i++;
         }
      }

      public void RemoveThread(Tester AnnouncingTester) {
         int i = 0;

         foreach(Tester Tester in Testers) {
            if(Tester == AnnouncingTester) {
               Listeners[i].ThreadIdentifier = -1;
               continue;
               return;
            }
            i++;
         }
      }

      public void Close() {
         foreach(Tester Tester in Testers) {
            Tester.Close();
         }

         Dll.Close();
         Multimeter.Close();
      }
   }
}