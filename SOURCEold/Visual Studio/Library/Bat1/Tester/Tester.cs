﻿//******************************************************************************
//
//   Tester.cs     Tester
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.Threading;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;

namespace Bat1.Tester {
   public class Tester : Worker {
      private class Report {
         private Crt Output;

         public Report(Crt Win) {
            Output = Win;
         }

         public void Clear() {
            Output.Clear();
         }

         public void H1(string String, params double[] NumValues) {
            H1(String, MakeUnitValues(NumValues));
         }

         public void H2(string String, params double[] NumValues) {
            H2(String, MakeUnitValues(NumValues));
         }

         public void H3(string String, params double[] NumValues) {
            H3(String, MakeUnitValues(NumValues));
         }

         public void Info(string String, params double[] NumValues) {
            Info(String, MakeUnitValues(NumValues));
         }

         public void H1(string String, params UnitValue[] Values) {
            H1(ParseValues(String, Values));
         }

         public void H2(string String, params UnitValue[] Values) {
            H2(ParseValues(String, Values));
         }

         public void H3(string String, params UnitValue[] Values) {
            H3(ParseValues(String, Values));
         }

         public void Info(string String, params UnitValue[] Values) {
            Info(ParseValues(String, Values));
         }

         public void H1(string String) {
            Output.WriteLine("--- " + String + " ---");
         }

         public void H2(string String) {
            Output.WriteLine("- " + String + " -");
         }

         public void H3(string String) {
            Output.WriteLine(String);
         }

         public void Info(string String) {
            Output.WriteLine(String);
         }

         public void Space() {
            Output.NewLine();
         }

         public void Value(UnitValue Value) {
            Output.WriteLine(FormatValue(Value));
         }

         public void Range(Config.Tests.Range Range) {
            Info("< % ; % >", Range.Min, Range.Max);
         }

         public void Value(double Val) {
            UnitValue Value = new UnitValue();

            Value.Value = Val;
            Value.Unit = Unit;

            Output.WriteLine(FormatValue(Value));
         }

         private UnitValue[] MakeUnitValues(params double[] NumValues) {
            UnitValue[] Values = new UnitValue[NumValues.Length];
            int i = 0;

            foreach(double NumValue in NumValues) {
               Values[i].Value = NumValue;
               Values[i].Unit = Unit;

               i++;
            }

            return Values;
         }

         private string ParseValues(string String, params UnitValue[] Values) {
            int LastPos = 0;
            int Pos = 0;
            int i = 0;

            string Output = "";

            while(true) {
               Pos = LastPos;

               LastPos = String.IndexOf('%', LastPos);

               if(LastPos < 0 || i >= Values.Length) {
                  Output += String.Substring(Pos, String.Length - Pos);
                  break;
               }

               if(LastPos != String.Length - 1) {
                  if(String[LastPos + 1] == '%') {
                     Output += String.Substring(Pos, LastPos);
                     LastPos += 2;
                     continue;
                  }
               }

               int a = LastPos - Pos - 1;

               if(Pos == 0 && LastPos == 0) {
                  a++;
               }

               Output += String.Substring(Pos, a);

               Output += FormatValue(Values[i]);

               i++;

               LastPos += 1;
            }

            return Output;
         }

         private string FormatValue(UnitValue Value) {
            String Prefix = "";
            if(Math.Abs(Value.Value) < 1e-12) {
               Value.Value = 0;
            } else if(Math.Abs(Value.Value) < 1e-4) {
               Value.Value *= 1000000;
               Prefix = "u";
            } else if(Math.Abs(Value.Value) < 1e-1) {
               Value.Value *= 1000;
               Prefix = "m";
            }

            return String.Format(" {0:0.000} " + Prefix + "{1}", Value.Value, TranslateUnits(Value.Unit));
         }

         private string TranslateUnits(Units Unit) {
            switch(Unit) {
               case Units.Volts:
                  return "V";

               case Units.Amperes:
                  return "A";

               default:
                  return "";
            }
         }

         public struct UnitValue {
            public double Value;
            public Units Unit;
         }

         public Units Unit;

         public enum Units {
            Volts,
            Amperes,
            Lsb
         }
      }

      public class TestException : System.Exception {
         public TestException() {
         }

         public TestException(string Message)
            : base(Message) {
         }
      }

      public class TestFatalException : TestException {
         public TestFatalException() {
         }

         public TestFatalException(string Message)
            : base(Message) {
         }
      }

      public class TestInitException : TestException {
         public TestInitException() {
         }

         public TestInitException(string Message)
            : base(Message) {
         }
      }

      public enum FirmwareType {
         PRODUCTION = 0,
         TEST,
         Unknown
      }

      public FirmwareType Firmware {
         get {
            return _Firmware;
         }

         private set {
            _Firmware = value;
         }
      }

      public enum ResourceType {
         Dll,
         Multimeter
      }

      // Controller
      TesterController MyController;

      // Configuration
      public Config.TesterCommon TesterCommonConfig; // general tester config
      public Config.TargetCommon TargetCommonConfig;
      public Config.FactoryConfig FactoryConfiguration;
      public Config.Tests TestsConfig; // konfiguraci testů, definici operací atd. je zbytečné mít v každém testeru zvlášť
      public Config.TestBatch.TTestBatchCollection TestBatches;

      // Devices
      private PowerSupply PowerSupply;
      private Target Target;
      private TesterBoard TesterBoard;

      // Devices config
      public Config.Tester.TTester MyDevicesConfig;

      // Shared devices
      private Multimeter Multimeter;
      private Bat1Dll Dll;

      public int ID = 0;

      public BackgroundOperation Operation = new BackgroundOperation();

      private FirmwareType _Firmware = FirmwareType.Unknown;

      private Report TestReport;

      // Tester current status
      public int TotalOperations = 0;
      public int CurrentOperation = 0;
      public Operations OperationInProgress;
      public bool InProgress = false;
      public bool Failed = false;
      public bool OperationRunning = false;
      public bool ForceStop = false;

      // Operations
      public enum Operations {
         SWITCH_ON = 0,
         SWITCH_OFF,
         LOAD_PRODUCTION_FIRMWARE,
         LOAD_TEST_FIRMWARE,
         GET_FIRMWARE,
         FTDI_PROGRAMMING,
         TEST_KEYBOARD,
         TEST_DISPLAY,
         TEST_MEMORY,
         TEST_RTC,
         TEST_ADC,
         TEST_IADC,
         TEST_SOUND,
         TEST_PRINTER_COM,
         TEST_USB,
         TEST_CHARGER_CONNECTION,
         TEST_CHARGER_CURRENT,
         TEST_CHARGER_SHUTDOWN,
         TEST_POWER_SHUTDOWN,
         TEST_POWER_BACKLIGHT_ON,
         TEST_POWER_BACKLIGHT_OFF,
         TEST_POWER_IDLE,
         TEST_POWER_INVERTED,
         WRITE_CONFIG
      }

      private struct OperationStruct {
         public string Name;
         public OperationDelegate Function;
         public ResourceType[] SharedResources;
         public bool AllowRetry;

         public OperationStruct(string _Name, OperationDelegate _Function, ResourceType[] _SharedResources, bool _AllowRetry)
            : this() {
            Name = _Name;
            Function = _Function;
            SharedResources = _SharedResources;
            AllowRetry = _AllowRetry;
         }
      }

      private OperationStruct[] OperationDetail = new OperationStruct[Enum.GetValues(typeof(Operations)).Length];

      private delegate void OperationDelegate();

      System.Resources.ResourceManager Resources;

      /// <summary>
      /// Constructor</summary>
      string textik;
      public void SetReportOutput(Crt Win) {
         TestReport = new Report(Win);
      }

      public Tester(TesterController _Controller,              // parent
                     Config.Tester.TTester _MyDevicesConfig,
                     Config.TesterCommon _TesterCommonConfig,
                     Config.TargetCommon _TargetCommonConfig,
                     Config.FactoryConfig _FactoryConfiguration,
                     Config.Tests _TestsConfig,
                     Config.TestBatch.TTestBatchCollection _TestBatches) {
         MyController = _Controller;

         Resources = new System.Resources.ResourceManager("Bat1.Tester.Tester", System.Reflection.Assembly.GetExecutingAssembly());

         // Struktura operací se zbytečně inicializuje a drží v paměti pro každý tester zvlášť
         OperationDetail[(int)Operations.SWITCH_ON] = new OperationStruct(Resources.GetString("SwitchOnTest"),
                                                                                             new OperationDelegate(_SwitchOn),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.SWITCH_OFF] = new OperationStruct("Switch OFF",
                                                                                             new OperationDelegate(_SwitchOff),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.LOAD_PRODUCTION_FIRMWARE] = new OperationStruct("Load Production Firmware",
                                                                                             new OperationDelegate(_LoadProductionFirmware),
                                                                                             null,
                                                                                             true);

         OperationDetail[(int)Operations.LOAD_TEST_FIRMWARE] = new OperationStruct("Load Test Firmware",
                                                                                             new OperationDelegate(_LoadTestFirmware),
                                                                                             null,
                                                                                             true);

         OperationDetail[(int)Operations.FTDI_PROGRAMMING] = new OperationStruct("FTDI Programming",
                                                                                             new OperationDelegate(_FtdiProgramming),
                                                                                             null,
                                                                                             true);

         OperationDetail[(int)Operations.TEST_KEYBOARD] = new OperationStruct("Keyboard test",
                                                                                             new OperationDelegate(_TestKeyboard),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_DISPLAY] = new OperationStruct("Display test",
                                                                                             new OperationDelegate(_TestDisplay),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_MEMORY] = new OperationStruct("Memory test",
                                                                                             new OperationDelegate(_TestMemory),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_RTC] = new OperationStruct("RTC test",
                                                                                             new OperationDelegate(_TestRtc),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_ADC] = new OperationStruct("ADC test",
                                                                                             new OperationDelegate(_TestAdc),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_IADC] = new OperationStruct("IADC test",
                                                                                             new OperationDelegate(_TestIadc),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_SOUND] = new OperationStruct("Sound test",
                                                                                             new OperationDelegate(_TestSound),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_PRINTER_COM] = new OperationStruct("Printer Com test",
                                                                                             new OperationDelegate(_TestPrinterCom),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_USB] = new OperationStruct("USB test",
                                                                                             new OperationDelegate(_TestUsb),
                                                                                             null,
                                                                                             true);

         OperationDetail[(int)Operations.TEST_CHARGER_CONNECTION] = new OperationStruct("Charger connection test",
                                                                                             new OperationDelegate(_TestChargerConnection),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_CHARGER_CURRENT] = new OperationStruct("Charger current test",
                                                                                             new OperationDelegate(_TestChargerCurrent),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_CHARGER_SHUTDOWN] = new OperationStruct("Charger shutdown test",
                                                                                             new OperationDelegate(_TestChargerShutdown),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_POWER_SHUTDOWN] = new OperationStruct("Power shutdown",
                                                                                             new OperationDelegate(_TestTargetShutdown),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_POWER_BACKLIGHT_ON] = new OperationStruct("Power backlight on test",
                                                                                             new OperationDelegate(_TestPowerBackLightOn),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_POWER_BACKLIGHT_OFF] = new OperationStruct("Power backlight off test",
                                                                                             new OperationDelegate(_TestPowerBackLightOff),
                                                                                             null,
                                                                                             false);

         OperationDetail[(int)Operations.TEST_POWER_IDLE] = new OperationStruct("Power idle test",
                                                                                             new OperationDelegate(_TestPowerIdle),
                                                                                             new ResourceType[] { ResourceType.Multimeter },
                                                                                             false);

         OperationDetail[(int)Operations.TEST_POWER_INVERTED] = new OperationStruct("Power inverted test",
                                                                                             new OperationDelegate(_TestPowerInverted),
                                                                                             new ResourceType[] { ResourceType.Multimeter },
                                                                                             false);

         OperationDetail[(int)Operations.WRITE_CONFIG] = new OperationStruct("Write Configuration",
                                                                                             new OperationDelegate(_WriteConfig),
                                                                                             new ResourceType[] { ResourceType.Dll },
                                                                                             true);

         // Store config
         MyDevicesConfig = _MyDevicesConfig;
         TesterCommonConfig = _TesterCommonConfig;
         TargetCommonConfig = _TargetCommonConfig;
         TestBatches = _TestBatches;
         TestsConfig = _TestsConfig;
         FactoryConfiguration = _FactoryConfiguration;

         // Obtain devices
         PowerSupply = new PowerSupply(MyDevicesConfig.Instruments.PowerSupply);
         Target = new Target(MyDevicesConfig.Target, TargetCommonConfig.Usb, TargetCommonConfig.Com);
         TesterBoard = new TesterBoard(MyDevicesConfig.TesterBoard);

         // Init Tester board led
         TesterBoard.Open();
         TesterBoard.LedReadyOn();
      }

      public void Close() {
         while(InProgress) { // User should ensure himself that tester is not testing otherwise his thread will be frozen
            Thread.Sleep(100);
         }

         TesterBoard.Close();
         PowerSupply.Close();
         Target.Close();
      }

      /// <summary>
      /// Do</summary>

      public void Do(bool Cyclic, params Operations[] Operations) {
         MyController.AnnounceThread(this);
         Trace.WriteLine("*****************************************************************************");
         Trace.WriteLine("***********************************  BATCH  *********************************");
         Trace.WriteLine("*****************************************************************************");
         ForceStop = false;

         do { // cyclic
            if(InProgress == true) { // only one thread can test at one time
               return;
            }

            InProgress = true;
            bool InternalPassed = true;
            int Retry = 0;

            List<Operations> NotPassedOperations = new List<Operations>(); // Used to store not passed operations

            CurrentOperation = 0;                  // number of current operation
            TotalOperations = Operations.Length;   // total number of operations to be done

            try {
               if(TotalOperations > 1) {         // Clear report if batch in progress
                  TestReport.Clear();
               }

               Operation.Worker = this;          // Tester is reponsible to pass progress data

               InitMyDevices();                  // Init power supply, tester board etc.

               // switch leds off
               TesterBoard.LedErrorOff();
               TesterBoard.LedReadyOff();

               for(int i = 0; i < Operations.Length; i++) { // for each operation to be done
                  Operations Oper = Operations[i];
                  
                  Trace.WriteLine("-----------------------------------------------------------------------------");
                  Trace.WriteLine(OperationDetail[(int)Oper].Name);
                  Trace.WriteLine("-----------------------------------------------------------------------------"); 

                  try {
                     Trace.WriteLine("Locking myself");
                     Monitor.Enter(this);                   // lock for the time the tester is testing and is using shared devices
                     Trace.WriteLine("Locked myself");

                     try {
                        OperationRunning = true;

                        GetSharedDevices(OperationDetail[(int)Oper].SharedResources);

                        TestReport.H1(OperationDetail[(int)Oper].Name);

                        OperationInProgress = Oper;

                        TestReport.Info(DateTime.Now.ToString());

                        if(ForceStop == true) {
                           throw new Exception(Resources.GetString("AbortedByUser"));
                        }

                        Trace.WriteLine("Starting operation");
                        OperationDetail[(int)Oper].Function();
                        Trace.WriteLine("Operation passed");
                        TestReport.H2(Resources.GetString("Passed"));
                        TestReport.Space();
                        TestReport.Space();
                     } finally {
                        ReleaseSharedDevices(OperationDetail[(int)Oper].SharedResources);
                        OperationRunning = false;
                        Monitor.Exit(this);              // unlock
                     }
                  } catch(Exception e) {
                     if(e.Message.Length > 0) {
                        TestReport.Info(e.Message);
                        Trace.WriteLine(e.Message);
                     }

                     if(ForceStop == false && Retry < TesterCommonConfig.Retry && OperationDetail[(int)Oper].AllowRetry) {
                        i--; // repeat current operation
                        Retry++; // count retries

                        // Retry will be done with other testers stopped
                        TestReport.Info("Locking other testers");
                        Trace.WriteLine("Locking other testers");
                        MyController.LockAllTesters(this);
                        Trace.WriteLine("Other testers locked");
                        TestReport.Info("Other testers locked, Retry " + Retry.ToString());

                        // Restart all devices
                        try {
                           //TesterBoard.Close();
                           //PowerSupply.Close();
                           //Target.Close();
                           Thread.Sleep(1000);
                           //Target.Test.Open();
                           //TesterBoard.Open();
                           //InitMyDevices();
                           
                        } catch(Exception) { // any exception now would prevent calling ReleaseAllTesters() - all the time lock
                           
                        }

                        continue; // Start test again
                     }

                     TestReport.H2("!!! " + Resources.GetString("NotPassed") + " !!!");
                     TestReport.Space();
                     TestReport.Space();

                     InternalPassed = false;

                     NotPassedOperations.Add(Oper);

                     // If we have locked testers previously, unlock them
                     // muze byt volano, i kdyz k zamknuti nedoslo - kontroler je zodpovedny za to, ze se nepokusi odemknout nezamknutý tester
                     MyController.ReleaseAllTesters(this);

                     break; // Stop batch
                  }

                  // If we have locked testers previously, unlock them
                  // muze byt volano, i kdyz k zamknuti nedoslo - kontroler je zodpovedny za to, ze se nepokusi odemknout nezamknutý tester
                  MyController.ReleaseAllTesters(this);

                  Retry = 0;
                  CurrentOperation++;
               }



            } catch(TestInitException e) {
               if(e.Message.Length > 0) {
                  TestReport.Info(e.Message);
                  Trace.WriteLine(e.Message);
               }

               InternalPassed = false;
            } finally {
               //Target.Close(); // Close USB and COM of Target - user is going to manipulate with connectors
               Trace.WriteLine("Batch END");
            }


            if(InternalPassed) {
               TesterBoard.LedReadyOn();
               Failed = false;
            } else {
               TesterBoard.LedErrorOn();
               Failed = true;

               if(Operations.Length > 1) {
                  foreach(Operations Oper in NotPassedOperations) {
                     TestReport.H2(OperationDetail[(int)Oper].Name + ": " + Resources.GetString("NotPassed"));
                  }
               }
            }

            InProgress = false;
            /* pokud jsme v cyclic modu tak nyni nastava okamzik, kdy opetovne volani teto funkce (Do(...)) z jineho threadu 
             * dostane prednost a tato "instance" Do se ukonci - nezadouci, ale nic kritickeho...
             */
         } while(Cyclic == true && ForceStop == false);
      }

      /// <summary>
      /// RunBatch</summary>

      public void RunBatch(bool Cyclic, string ID) {
         if(TestBatches[ID] == null) {
            return;
         }

         TestReport.Clear();

         Operations[] Batch = new Operations[TestBatches[ID].Tests.Count];

         int i = 0;

         foreach(Config.TestBatch.TTestBatch.TTest Test in TestBatches[ID].Tests) {
            Batch[i++] = Test.Name;
         }

         Do(Cyclic, Batch);
      }

      /*************************************************************************
      **************************** WITH INITIALIZATION *************************
      *************************************************************************/
      /*
            /// <summary>
            /// SwitchOn</summary>

            public void SwitchOn() {
               Do(Operations.SWITCH_ON);
            }

            /// <summary>
            /// SwitchOff</summary>

            public void SwitchOff() {
               Do(Operations.SWITCH_OFF);
            }

            /// <summary>
            /// LoadProductionFirmware</summary>

            public void LoadProductionFirmware() {
               Do(Operations.LOAD_PRODUCTION_FIRMWARE);
            }

            /// <summary>
            /// LoadTestFirmware</summary>

            public void LoadTestFirmware() {
               Do(Operations.LOAD_TEST_FIRMWARE);
            }

            /// <summary>
            /// FtdiProgramming</summary>

            public void FtdiProgramming() {
               Do(Operations.FTDI_PROGRAMMING);
            }

            /// <summary>
            /// TestKeyboard</summary>

            public void TestKeyboard() {
               Do(Operations.TEST_KEYBOARD);
            }

            /// <summary>
            /// TestDisplay</summary>

            public void TestDisplay() {
               Do(Operations.TEST_DISPLAY);
            }

            /// <summary>
            /// TestMemory</summary>

            public void TestMemory() {
               Do(Operations.TEST_MEMORY);
            }

            /// <summary>
            /// TestRtc</summary>

            public void TestRtc() {
               Do(Operations.TEST_RTC);
            }

            /// <summary>
            /// TestAdc</summary>

            public void TestAdc() {
               Do(Operations.TEST_ADC);
            }

            /// <summary>
            /// TestIadc</summary>

            public void TestIadc() {
               Do(Operations.TEST_IADC);
            }

            /// <summary>
            /// TestSound</summary>

            public void TestSound() {
               Do(Operations.TEST_SOUND);
            }

            /// <summary>
            /// TestPrinterCom</summary>

            public void TestPrinterCom() {
               Do(Operations.TEST_PRINTER_COM);
            }

            /// <summary>
            /// TestUsb</summary>

            public void TestUsb() {
               Do(Operations.TEST_USB);
            }

            /// <summary>
            /// TestChargerConnection</summary>

            public void TestChargerConnection() {
               Do(Operations.TEST_CHARGER_CONNECTION);
            }

            /// <summary>
            /// TestChargerCurrent</summary>

            public void TestChargerCurrent() {
               Do(Operations.TEST_CHARGER_CURRENT);
            }

            /// <summary>
            /// TestChargerShutdown</summary>

            public void TestChargerShutdown() {
               Do(Operations.TEST_CHARGER_SHUTDOWN);
            }

            /// <summary>
            /// TestPowerShutdown</summary>

            public void TestPowerShutdown() {
               Do(Operations.TEST_POWER_SHUTDOWN);
            }

            /// <summary>
            /// TestPowerBacklightOn</summary>

            public void TestPowerBacklightOn() {
               Do(Operations.TEST_POWER_BACKLIGHT_ON);
            }

            /// <summary>
            /// TestPowerBacklightOff</summary>

            public void TestPowerBacklightOff() {
               Do(Operations.TEST_POWER_BACKLIGHT_OFF);
            }

            /// <summary>
            /// TestPowerIdle</summary>

            public void TestPowerIdle() {
               Do(Operations.TEST_POWER_IDLE);
            }

            /// <summary>
            /// TestPowerInverted</summary>

            public void TestPowerInverted() {
               Do(Operations.TEST_POWER_INVERTED);
            }

            /// <summary>
            /// WriteConfig</summary>

            public void WriteConfig() {
               Do(Operations.WRITE_CONFIG);
            }
            */
      /*************************************************************************
      ************************** WITHOUT INITIALIZATION ************************
      *************************************************************************/

      /// <summary>
      /// Switch on</summary>

      private void _SwitchOn() {
         _TargetSwitchOnPowerNormal();
      }

      /// <summary>
      /// Switch off</summary>

      private void _SwitchOff() {
         _TargetSwitchOff();
      }


      /// <summary>
      /// Load production firmware to device</summary>

      private void _LoadProductionFirmware() {
         _LoadFirmware(FirmwareType.PRODUCTION);
      }

      /// <summary>
      /// Load test firmware to device</summary>

      private void _LoadTestFirmware() {
         _LoadFirmware(FirmwareType.TEST); ;
      }

      /// <summary>
      /// Load firmware</summary>
      /// <param name="FW">Type of the firmware</param>

      private void _LoadFirmware(FirmwareType FW) {
         LpcFlash Lpc = new LpcFlash();

         try {
            if(FW == FirmwareType.Unknown) {
               throw new TestException("Invalid firmware");
            }

            _VoltageSetNormalAndConnect();

            // Connect target USB to PC
            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectDevice()) {
               throw new TestException("Device not present. Check USB/tester board.");
            }

            // Firmware
            Hex FirmwareFile = new Hex();

            TestReport.Info("Parsing file");

            Operation.Worker = FirmwareFile;

            FirmwareFile.Load(FactoryConfiguration.Firmware[FW]);

            Device.Uart.Config.UsbLocUartSettings LpcComSettings = new Device.Uart.Config.UsbLocUartSettings();

            LpcComSettings.LocId = MyDevicesConfig.Target.ComUsbSettings.LocId;
            LpcComSettings.Bits = 8;
            LpcComSettings.Parity = Device.Uart.Uart.Parity.None;
            LpcComSettings.Handshake = Device.Uart.Uart.Handshake.None;
            LpcComSettings.StopBit = Device.Uart.Uart.StopBit.One;
            LpcComSettings.BaudRate = TesterCommonConfig.Lpc.BaudRate;

            // configure Lpc
            Lpc.SetCom(new Device.Uart.UsbUart(), LpcComSettings);

            Lpc.CrystalFrequency = TesterCommonConfig.Lpc.CrystalFrequency;

            Operation.Worker = Lpc;

            TestReport.Info("Contacting device");

            TesterBoard.JumpersConnect();

            Thread.Sleep(200);

            if(!Lpc.GetDeviceInfo()) {
               throw new TestException("Can't contact device. Jumpers?");
            }

            TestReport.Info("Writing code");

            if(!Lpc.WriteFlash(0, FirmwareFile.Code, (int)FirmwareFile.CodeSize)) {
               throw new TestException("Write failed");
            }

            TestReport.Info("Verifying code");

            if(!Lpc.CompareFlash(0, FirmwareFile.Code, (int)FirmwareFile.CodeSize)) {
               throw new TestException("Verify failed");
            }

            TestReport.Info("Resetting");

            Lpc.Reset();

            Firmware = FW;
         } finally {
            Lpc.Disconnect();
            TesterBoard.JumpersDisconnect();
         }
      }

      /// <summary>
      /// FTDI EEPROM programming</summary>

      private void _FtdiProgramming() {
         try {
            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectDevice()) {
               throw new TestException("No device to be programmed");
            }

            if(!Target.Usb.WriteEeprom(FactoryConfiguration.Ftdi)) {
               throw new TestException("Can't write EEPROM");
            }

            if(!TesterBoard.UsbDisconnect()) {
               throw new TestException("Can't disconnect USB");
            }

            if(!Target.Usb.WaitForDisconnect()) {
               throw new TestException("Can't disconnect USB");
            }

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectDevice()) {
               throw new TestException("No device");
            }

            if(!Target.Usb.CompareUsbName(FactoryConfiguration.Ftdi.DeviceName)) {
               throw new TestException("Write failed");
            }

         } finally {
            Target.Usb.Close();
         }
      }

      /// <summary>
      /// Test keyboard</summary>

      private void _TestKeyboard() {
         Target.Keyboard Kbd = new Target.Keyboard();
         Target.Keyboard ModelKbd = new Target.Keyboard();

         _TargetSwitchOnPowerNormal();

         //---- release all keys : ---------------------------------------------------
         if(!TesterBoard.OnOffRelease()) {
            throw new TestException("Unable release ON key");
         }

         ModelKbd.On.State = Target.Keyboard.Key.KeyState.RELEASED;

         if(!TesterBoard.K0Release()) {
            throw new TestException("Unable release K0 key");
         }

         ModelKbd.K0.State = Target.Keyboard.Key.KeyState.RELEASED;

         if(!TesterBoard.K1Release()) {
            throw new TestException("Unable release K1 key");
         }

         ModelKbd.K1.State = Target.Keyboard.Key.KeyState.RELEASED;

         if(!TesterBoard.K2Release()) {
            throw new TestException("Unable release K2 key");
         }

         ModelKbd.K2.State = Target.Keyboard.Key.KeyState.RELEASED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 0.25;

         //---- press ON key : -------------------------------------------------------
         if(!TesterBoard.OnOffPush()) {
            throw new TestException("Unable press ON key");
         }

         ModelKbd.On.State = Target.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);

         // release key :
         if(!TesterBoard.OnOffRelease()) {
            throw new TestException("Unable release ON key");
         }

         ModelKbd.On.State = Target.Keyboard.Key.KeyState.RELEASED;

         //---- press K0 key : -------------------------------------------------------
         if(!TesterBoard.K0Push()) {
            throw new TestException("Unable push K0 key");
         }

         ModelKbd.K0.State = Target.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 0.5;

         // release key :
         if(!TesterBoard.K0Release()) {
            throw new TestException("Unable release K0 key");
         }

         ModelKbd.K0.State = Target.Keyboard.Key.KeyState.RELEASED;

         //---- press K1 key : -------------------------------------------------------
         if(!TesterBoard.K1Push()) {
            throw new TestException("Unable push K1 key");
         }

         ModelKbd.K1.State = Target.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 0.75;

         // release key :
         if(!TesterBoard.K1Release()) {
            throw new TestException("Unable release K1 key");
         }

         ModelKbd.K1.State = Target.Keyboard.Key.KeyState.RELEASED;

         //---- press K2 key : -------------------------------------------------------
         if(!TesterBoard.K2Push()) {
            throw new TestException("Unable push K2 key");
         }

         ModelKbd.K2.State = Target.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 1;

         // release key :
         if(!TesterBoard.K2Release()) {
            throw new TestException("Unable release K2 key");
         }

         ModelKbd.K2.State = Target.Keyboard.Key.KeyState.RELEASED;
      }

      /// <summary>
      /// Test display</summary>

      private void _TestDisplay() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestDisplay()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test memory</summary>

      private void _TestMemory() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestMemory()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test RTC</summary>

      private void _TestRtc() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestRtc()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test sound</summary>

      private void _TestSound() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestSound()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test printer COM</summary>

      private void _TestPrinterCom() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestUart()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test ADC</summary>

      private void _TestAdc() {
         _TargetSwitchOnPowerNormal();

         TestReport.Unit = Report.Units.Lsb;

         TestReport.Info("Passed conditions: " + TestsConfig.Adc.SuccessfulTrials + "/" + TestsConfig.Adc.Trials + " readings in range; MaxFlicker <= " + TestsConfig.Adc.MaxFlicker.ToString());

         TestReport.H2("Min");

         TestReport.H3("Target range");

         TestReport.Range(TestsConfig.Adc.ZeroRange);

         if(!TesterBoard.SensorMin()) {
            throw new TestException("Unable control bridge");
         }

         TestReport.H3("Values:");

         Progress = 0.1;

         Thread.Sleep(TestsConfig.Adc.InitDelay);

         // read trials :
         int Count = 0;
         int[] Value = new int[TestsConfig.Adc.Trials];
         int[] Dif = new int[TestsConfig.Adc.Trials];
         long Mean = 0;

         for(int i = 0; i < TestsConfig.Adc.Trials; i++) {
            Thread.Sleep(TestsConfig.Adc.Delay);

            if(!Target.Test.ReadAdc(ref Value[i])) {
               throw new TestException("Unable read ADC");
            }

            TestReport.Value(Value[i]);

            if(InRange(Value[i], TestsConfig.Adc.ZeroRange)) {
               Count++;
            }

            Mean += Value[i];
            Progress += 0.4 / TestsConfig.Adc.Trials;
         }

         if(Count < TestsConfig.Adc.SuccessfulTrials) {
            throw new TestException("ADC zero out of range");
         }

         Mean /= TestsConfig.Adc.Trials;
         Count = 0;
         for(int i = 0; i < TestsConfig.Adc.Trials; i++) {
            if(Math.Abs((int)Mean - Value[i]) <= TestsConfig.Adc.MaxFlicker) {
               Count++;
            }
         }

         if(Count < TestsConfig.Adc.SuccessfulTrials) {
            throw new TestException("ADC flicker too high");
         }

         TestReport.Info("");
         TestReport.H2("Max");

         TestReport.H3("Target range");

         TestReport.Range(TestsConfig.Adc.MaxRange);

         //---------------------------------------------------------------------------
         // test max
         if(!TesterBoard.SensorMax()) {
            throw new TestException("Unable control bridge (TB)");
         }

         TestReport.H3("Values:");

         Progress = 0.6;

         Thread.Sleep(TestsConfig.Adc.InitDelay);

         // read trials :
         Count = 0;
         Mean = 0;

         for(int i = 0; i < TestsConfig.Adc.Trials; i++) {
            Thread.Sleep(TestsConfig.Adc.Delay);

            if(!Target.Test.ReadAdc(ref Value[i])) {
               throw new TestException("Unable read ADC (T)");
            }

            TestReport.Value(Value[i]);

            if(InRange(Value[i], TestsConfig.Adc.MaxRange)) {
               Count++;
            }

            Mean += Value[i];
            Progress += 0.4 / TestsConfig.Adc.Trials;
         }

         if(Count < TestsConfig.Adc.SuccessfulTrials) {
            throw new TestException("ADC max out of range");
         }

         Mean /= TestsConfig.Adc.Trials;
         Count = 0;
         for(int i = 0; i < TestsConfig.Adc.Trials; i++) {
            if(Math.Abs((int)Mean - Value[i]) <= TestsConfig.Adc.MaxFlicker) {
               Count++;
            }
         }

         if(Count < TestsConfig.Adc.SuccessfulTrials) {
            throw new TestException("ADC flicker too high");
         }
      }

      /// <summary>
      /// Test internal ADC</summary>

      private void _TestIadc() {
         _TargetSwitchOnPowerNormal();

         TesterBoard.UsbDisconnect();

         _SetVoltage(TestsConfig.Iadc.LowVoltage);

         // read trials :
         int Count = 0;
         int Value = 0;
         double Voltage = 0;

         TestReport.Unit = Report.Units.Volts;

         TestReport.Info(Resources.GetString("PassedConditions") + TestsConfig.Iadc.SuccessfulTrials + "/" + TestsConfig.Iadc.Trials + Resources.GetString("ReadingsInRange"));

         TestReport.H2(Resources.GetString("LowVoltage"));

         TestReport.H3("Target range");

         TestReport.Range(TestsConfig.Iadc.LowRange);

         TestReport.H3("Values:");

         Progress = 0.1;

         Thread.Sleep(TestsConfig.Iadc.InitDelay);

         for(int i = 0; i < TestsConfig.Iadc.Trials; i++) {
            Thread.Sleep(TestsConfig.Iadc.Delay);


            if(!Target.Test.ReadIadc(ref Value)) {
               throw new TestException("Can't read IADC");
            }

            Voltage = TestsConfig.Iadc.TransferFunction(Value);

            TestReport.Value(Voltage);

            if(InRange(Voltage, TestsConfig.Iadc.LowRange)) {
               Count++;
            }

            Progress += 0.4 / TestsConfig.Adc.Trials;
         }

         if(Count < TestsConfig.Iadc.SuccessfulTrials) {
            throw new TestException(Resources.GetString("ReadingsNotInRage"));
         }

         //---------------------------------------------------------------------------
         // test max
         _SetVoltage(TestsConfig.Iadc.HighVoltage);

         Thread.Sleep(TestsConfig.Iadc.Delay); // wait for stable data

         // read trials :
         Count = 0;

         TestReport.Space();

         TestReport.H2("High voltage");

         TestReport.H3("Target range");

         TestReport.Range(TestsConfig.Iadc.HighRange);

         TestReport.H3("Values:");

         Progress = 0.6;

         for(int i = 0; i < TestsConfig.Iadc.Trials; i++) {
            Thread.Sleep(TestsConfig.Iadc.Delay);

            if(!Target.Test.ReadIadc(ref Value)) {
               throw new TestException("Can't read IADC");
            }

            Voltage = TestsConfig.Iadc.TransferFunction(Value);

            TestReport.Value(Voltage);

            if(InRange(Voltage, TestsConfig.Iadc.HighRange)) {
               Count++;
            }

            Progress += 0.4 / TestsConfig.Adc.Trials;
         }
         if(Count < TestsConfig.Iadc.SuccessfulTrials) {
            throw new TestException("ADC max out of range");
         }

      }

      /// <summary>
      /// Test USB</summary>

      private void _TestUsb() {
         try {
            _TargetSwitchOnPowerNormal();

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Unable connect USB (TB)");
            }

            Progress = 0.1;

            if(!Target.Usb.DetectDevice()) {
               throw new TestException("No device present");
            }

            Progress = 0.6;

            Thread.Sleep(TesterCommonConfig.Target.InitTime);

            Progress = 0.7;

            if(!Target.Test.StartUsb()) {
               throw new TestException("Unable start USB (T)");
            }

            if(!Target.Usb.Open()) {
               throw new TestException("Unable open target communication");
            }

            Progress = 0.9;

            if(!Target.Usb.TestUart()) {
               throw new TestException("");
            }

            if(!Target.Test.UsbResult()) {
               throw new TestException("");
            }

            Progress = 1;
         } finally {
            Target.Usb.Close();
         }

      }

      /// <summary>
      /// Test charger signals</summary>

      private void _TestChargerConnection() {
         try {
            _TargetSwitchOnPowerNormal();

            TesterBoard.UsbDisconnect();

            _UsbChargerDisconnect();

            Thread.Sleep(TesterCommonConfig.Target.UsbPowerSettleTime);

            Progress = 0.5;

            bool PPR = false;
            bool CHG = false;
            bool USBON = false;
            bool ENNAB = false;

            if(!Target.Test.TestPower(ref PPR, ref CHG, ref USBON, ref ENNAB)) {
               throw new TestException("Unable read data");
            }

            if(!PPR) {
               throw new TestException("PPR status without USB is LOW");
            }
            if(USBON) {
               throw new TestException("USBON status without USB is HIGH");
            }

            _UsbChargerConnect();

            Thread.Sleep(TesterCommonConfig.Target.UsbPowerSettleTime);

            if(!Target.Test.TestPower(ref PPR, ref CHG, ref USBON, ref ENNAB)) {
               throw new TestException("Unable read data");
            }

            if(PPR) {
               throw new TestException("PPR status with USB is HIGH");
            }
            if(!USBON) {
               throw new TestException("USBON status with USB is LOW");
            }

            Progress = 1;
         } finally {
            _UsbChargerDisconnect();
         }

      }

      /// <summary>
      /// Test charger shutdown on full charge</summary>

      private void _TestChargerShutdown() {
         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current < %", TestsConfig.ChargerShutdown.Current);
            TestReport.Info("at voltage range");

            TestReport.Unit = Report.Units.Volts;
            TestReport.Range(TestsConfig.ChargerShutdown.Voltage);

            _VoltageSetNormalAndConnect();

            if(!TesterBoard.LoadConnect()) {
               throw new TestException("Can't connect load");
            }

            TesterBoard.UsbDisconnect();

            _UsbChargerConnect();

            bool Shutdown = false;

            Report.UnitValue CurrentU = new Report.UnitValue();
            CurrentU.Unit = Report.Units.Amperes;

            Report.UnitValue VoltageU = new Report.UnitValue();
            VoltageU.Unit = Report.Units.Volts;

            foreach(Config.Tests.Step Step in TestsConfig.ChargerShutdown.VoltageSteps) {
               _SetVoltage(Step.Voltage);

               Thread.Sleep(TesterCommonConfig.Target.UsbPowerSettleTime);

               CurrentU.Value = PowerSupply.Usb.Current;
               VoltageU.Value = Step.Voltage;

               TestReport.Info("% @ %", CurrentU, VoltageU);

               if(CurrentU.Value < TestsConfig.ChargerShutdown.Current && InRange(VoltageU.Value, TestsConfig.ChargerShutdown.Voltage)) {
                  Shutdown = true;
                  break;
               }

               Progress += 1.0 / TestsConfig.PowerShutdown.VoltageSteps.Count;
            }

            if(!Shutdown) {
               throw new TestException("");
            }
         } finally {
            TesterBoard.LoadDisconnect();

            _VoltageSetNormalAndConnect();
            _UsbChargerDisconnect();

            /* pokud je nahrán production firmware, váha po odpojení USB napájení
               za předpokladu, že odpojení USB napájení váhu nevypnulo, nějaký čas
               nereaguje na stisk ON/OFF tlačítka (nezapne se). Další testy, které
               mohou po tomto následovat, by tak nebyly schopné váhu zapnout.
            
               Lepší řešení - pokusit se váhu zapnout několikrát
             */

            Thread.Sleep(500);
         }
      }

      /// <summary>
      /// Test charger nominal current</summary>

      private void _TestChargerCurrent() {
         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current in range");

            TestReport.Range(TestsConfig.ChargerCurrent.Current);

            _SetVoltage(TestsConfig.ChargerCurrent.Voltage);

            TesterBoard.UsbDisconnect();

            _BatteryConnect();

            Progress = 0.2;

            if(!TesterBoard.LoadConnect()) {
               throw new TestException("Can't connect load");
            }

            _UsbChargerConnect();

            Thread.Sleep(TesterCommonConfig.Target.UsbPowerSettleTime);

            Progress = 0.7;

            double Current = PowerSupply.Usb.Current;

            TestReport.Space();

            TestReport.Unit = Report.Units.Amperes;

            TestReport.Value(Current);

            if(!InRange(Current, TestsConfig.ChargerCurrent.Current)) {
               throw new TestException("");
            }

            Progress = 1;
         } finally {
            _VoltageSetNormalAndConnect();
            _UsbChargerDisconnect();
            TesterBoard.LoadDisconnect();

            /* pokud je nahrán production firmware, váha po odpojení USB napájení
               za předpokladu, že odpojení USB napájení váhu nevypnulo, nějaký čas
               nereaguje na stisk ON/OFF tlačítka (nezapne se). Další testy, které
               mohou po tomto následovat, by tak nebyly schopné váhu zapnout.
            
               Lepší řešení - pokusit se váhu zapnout několikrát
             */

            Thread.Sleep(500);
         }
      }

      /// <summary>
      /// Test target shutdown on low voltage</summary>

      private void _TestTargetShutdown() {
         TestReport.Unit = Report.Units.Amperes;

         TestReport.Info("Passed conditions: current < %", TestsConfig.PowerShutdown.Current);
         TestReport.Info("at voltage range");

         TestReport.Unit = Report.Units.Volts;
         TestReport.Range(TestsConfig.PowerShutdown.Voltage);

         _TargetSwitchOnPowerNormal();

         TesterBoard.UsbDisconnect();

         bool Shutdown = false;

         Report.UnitValue CurrentU = new Report.UnitValue();
         CurrentU.Unit = Report.Units.Amperes;

         Report.UnitValue VoltageU = new Report.UnitValue();
         VoltageU.Unit = Report.Units.Volts;

         foreach(Config.Tests.Step Step in TestsConfig.PowerShutdown.VoltageSteps) {
            _SetVoltage(Step.Voltage);

            Thread.Sleep(TesterCommonConfig.Target.PowerSettleTime);

            CurrentU.Value = PowerSupply.Normal.Current;
            VoltageU.Value = Step.Voltage;

            TestReport.Info("% @ %", CurrentU, VoltageU);

            if(CurrentU.Value < TestsConfig.PowerShutdown.Current && InRange(VoltageU.Value, TestsConfig.PowerShutdown.Voltage)) {
               Shutdown = true;
               break;
            }

            Progress += 1.0 / TestsConfig.PowerShutdown.VoltageSteps.Count;
         }

         if(!Shutdown) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test backlight on current</summary>

      private void _TestPowerBackLightOn() {
         _TestPowerBackLight(true);
      }

      /// <summary>
      /// Test backlight of current</summary>

      private void _TestPowerBackLightOff() {
         _TestPowerBackLight(false);
      }

      private void _TestPowerBackLight(bool On) {
         Config.Tests.Range BacklightRange;

         if(On) {
            BacklightRange = TestsConfig.PowerBacklightOn.Current;
         } else {
            BacklightRange = TestsConfig.PowerBacklightOff.Current;
         }

         TestReport.Unit = Report.Units.Amperes;

         TestReport.Info("Passed conditions: current in range");
         TestReport.Range(BacklightRange);

         _TargetSwitchOnPowerNormal();

         TesterBoard.UsbDisconnect();

         switch(Firmware) {
            case FirmwareType.TEST:
               Target.Test.SetBacklight(On);
               break;

            case FirmwareType.PRODUCTION:
            case FirmwareType.Unknown:
               // Dll nelze využít - bylo by připojeno USB a tím i nabíječka, která by ovlivňovala proud
               if(On) {
                  _TargetDoSwitchOn();
               } else {
                  _TargetDoSwitchOn();
                  Thread.Sleep(23000);
               }
               break;
         }

         Thread.Sleep(TesterCommonConfig.Target.PowerSettleTime);

         double Current = PowerSupply.Normal.Current;

         TestReport.Space();

         TestReport.Unit = Report.Units.Amperes;

         TestReport.Value(Current);

         if(!InRange(Current, BacklightRange)) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test idle current</summary>

      private void _TestPowerIdle() {
         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current in range");

            TestReport.Range(TestsConfig.PowerIdle.Current);

            this.Progress = 0.2;

            _VoltageSetNormalAndConnect();

            TesterBoard.UsbDisconnect();

            _BatteryDisconnect(); // switch off

            Thread.Sleep(TesterCommonConfig.Target.PowerSettleTime);

            _BatteryConnect();

            _AmperemeterConnect();

            Thread.Sleep(TesterCommonConfig.Target.PowerSettleTime);

            this.Progress = 0.8;

            int SubSequentSamplesInRange = 0;
            double[] Samples = new double[10];
            int i = 0;
            double Sample;
            double Sum1 = 0;
            double Sum2 = 0;

            DateTime Start = DateTime.Now;

            while(true) {
               Multimeter.MeasureCurrent(out Sample);

               TestReport.Value(Sample);

               if(i >= Samples.Length) {
                  Array.Resize(ref Samples, Samples.Length + 10);
               }

               Samples[i] = Sample;

               /*
                Počítají se 2 plovoucí "průměry" z 50 vzorků.
                Jeden aktuální, druhý zpožděný o 30 vzorků
                Jestliže prvni > druhý - neprošel (proud neklesá)
                Jestli vypršel timeout - neprošel
               */

               Sum1 += Sample;

               if(i >= 10) {
                  Sum1 -= Samples[i - 10];
               }

               if(i >= 20) {
                  Sum2 += Samples[i - 20];

                  if(i >= 30) {
                     Sum2 -= Samples[i - 30];
                  }
               }

               if(!InRange(Sample, TestsConfig.PowerIdle.Current)) {
                  SubSequentSamplesInRange = 0;

                  if(Start.AddSeconds(TestsConfig.PowerIdle.Timeout) < DateTime.Now) {
                     throw new TestException("Timeout");
                  }

                  /*if(i > 30) {
                     if((Sum1 > Sum2)) {
                        throw new TestException("");
                     }
                  }*/
               } else {
                  SubSequentSamplesInRange++;
               }

               if(SubSequentSamplesInRange >= TestsConfig.PowerIdle.SubsequentSamples) {
                  break;
               }

               i++;

               Thread.Sleep(TestsConfig.PowerIdle.Period);
            }

            this.Progress = 1;
         } finally {
            TesterBoard.AmpermeterDisconnect();
         }
      }

      /// <summary>
      /// Test inverted voltage current</summary>

      private void _TestPowerInverted() {
         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current in range");

            TestReport.Range(TestsConfig.PowerInverted.Current);

            TesterBoard.UsbDisconnect();

            _BatteryConnectInverted();

            _AmperemeterConnect();

            TesterBoard.OnOffPush();

            Thread.Sleep(TestsConfig.PowerInverted.SettleTime);

            TesterBoard.OnOffRelease();

            double Current = 0;

            Multimeter.MeasureCurrent(out Current);

            if(!InRange(Current, TestsConfig.PowerInverted.Current)) {
               throw new TestException("");
            }
         } finally {
            _VoltageSetNormalAndConnect();
            Thread.Sleep(1000);
            TesterBoard.AmpermeterDisconnect();
         }
      }

      /// <summary>
      /// Write configuration data to device</summary>

      private void _WriteConfig() {
         try {
            _BatteryDisconnect();

            _FtdiProgramming();

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectDevice()) {
               throw new TestException("Device not present. Check USB/tester board.");
            }

            Thread.Sleep(1500); // Let PC close FTDI, Dll is going to use it



            Dll.EnableLogger(false);

            Bat1Version7 Scale = new Bat1Version7();

            if(!Scale.Check(Dll)) {
               throw new TestException("Device not present. Check USB/tester board. Production firmware programmed ???");
            }

            Bat1.ScaleConfig FactoryConfig = new ScaleConfig();


            FactoryConfig.ActiveFileIndex = FactoryConfiguration.ActiveFileIndex;
            FactoryConfig.Country = FactoryConfiguration.Country;
            FactoryConfig.DateFormat = FactoryConfiguration.DateFormat;
            FactoryConfig.DateSeparator1 = FactoryConfiguration.DateSeparator1;
            FactoryConfig.DateSeparator2 = FactoryConfiguration.DateSeparator2;
            FactoryConfig.DaylightSavingMode = FactoryConfiguration.DaylightSavingMode;
            FactoryConfig.Display.Backlight.Duration = FactoryConfiguration.Display.Backlight.Duration;
            FactoryConfig.Display.Backlight.Intensity = FactoryConfiguration.Display.Backlight.Intensity;
            FactoryConfig.Display.Backlight.Mode = FactoryConfiguration.Display.Backlight.Mode;
            FactoryConfig.Display.Contrast = FactoryConfiguration.Display.Contrast;
            FactoryConfig.Display.Mode = FactoryConfiguration.Display.Mode;

            FactoryConfig.EnableFileParameters = FactoryConfiguration.EnableFileParameters;

            FileConfig FConfig = new FileConfig();
            FConfig.WeighingConfig.Saving.EnableMoreBirds = FactoryConfiguration.Weighing.Saving.EnableMoreBirds;
            FConfig.WeighingConfig.Saving.MinimumWeight = FactoryConfiguration.Weighing.Saving.MinimumWeight;
            FConfig.WeighingConfig.Saving.Mode = FactoryConfiguration.Weighing.Saving.Mode;
            FConfig.WeighingConfig.Saving.NumberOfBirds = FactoryConfiguration.Weighing.Saving.NumberOfBirds;
            FConfig.WeighingConfig.Saving.StabilisationRange = FactoryConfiguration.Weighing.Saving.StabilisationRange;
            FConfig.WeighingConfig.Saving.StabilisationTime = FactoryConfiguration.Weighing.Saving.StabilisationTime;

            FactoryConfig.FileList = new Bat1.FileList();
            FactoryConfig.FileList.Add(new Bat1.File(FactoryConfiguration.DefaultFile.Name, "", FConfig));

            FactoryConfig.LanguageInDatabase = FactoryConfiguration.Language;

            FactoryConfig.PasswordConfig.Enable = FactoryConfiguration.Password.Enable;

            FactoryConfig.PowerOffTimeout = FactoryConfiguration.PowerOffTimeout;

            FactoryConfig.Printer.CommunicationFormat = FactoryConfiguration.Printer.CommunicationFormat;
            FactoryConfig.Printer.CommunicationSpeed = FactoryConfiguration.Printer.CommunicationSpeed;
            FactoryConfig.Printer.PaperWidth = FactoryConfiguration.Printer.PaperWidth;

            FactoryConfig.ScaleName = FactoryConfiguration.ScaleName;

            FactoryConfig.Sounds.EnableSpecial = FactoryConfiguration.Sounds.EnableSpecial;
            FactoryConfig.Sounds.ToneDefault = FactoryConfiguration.Sounds.ToneDefault;
            FactoryConfig.Sounds.ToneLight = FactoryConfiguration.Sounds.ToneLight;
            FactoryConfig.Sounds.ToneOk = FactoryConfiguration.Sounds.ToneOk;
            FactoryConfig.Sounds.ToneHeavy = FactoryConfiguration.Sounds.ToneHeavy;
            FactoryConfig.Sounds.ToneKeyboard = FactoryConfiguration.Sounds.ToneKeyboard;
            FactoryConfig.Sounds.VolumeKeyboard = FactoryConfiguration.Sounds.VolumeKeyboard;
            FactoryConfig.Sounds.VolumeSaving = FactoryConfiguration.Sounds.VolumeSaving;

            FactoryConfig.StatisticConfig.Histogram.Mode = FactoryConfiguration.Statistic.Histogram.Mode;
            FactoryConfig.StatisticConfig.Histogram.Range = FactoryConfiguration.Statistic.Histogram.Range;
            FactoryConfig.StatisticConfig.Histogram.Step = FactoryConfiguration.Statistic.Histogram.Step;

            FactoryConfig.StatisticConfig.UniformityRange = FactoryConfiguration.Statistic.UniformityRange;

            FactoryConfig.TimeFormat = FactoryConfiguration.TimeFormat;

            FactoryConfig.TimeSeparator = FactoryConfiguration.TimeSeparator;

            FactoryConfig.Units.Decimals = FactoryConfiguration.Units.Decimals;
            FactoryConfig.Units.Division = FactoryConfiguration.Units.Division;
            FactoryConfig.Units.MaxDivision = FactoryConfiguration.Units.MaxDivision;
            FactoryConfig.Units.Range = FactoryConfiguration.Units.Range;
            FactoryConfig.Units.Units = FactoryConfiguration.Units.Units;
            FactoryConfig.Units.WeighingCapacity = FactoryConfiguration.Units.WeighingCapacity;

            FactoryConfig.Version.Major = FactoryConfiguration.Version.Major;
            FactoryConfig.Version.Minor = FactoryConfiguration.Version.Minor;
            FactoryConfig.Version.Hw = FactoryConfiguration.Version.Hw;
            FactoryConfig.Version.Build = FactoryConfiguration.Version.Build;

            FactoryConfig.WeighingConfig.Saving.Filter = FactoryConfiguration.Weighing.Saving.Filter;
            FactoryConfig.WeighingConfig.Saving.EnableMoreBirds = FactoryConfiguration.Weighing.Saving.EnableMoreBirds;
            FactoryConfig.WeighingConfig.Saving.MinimumWeight = FactoryConfiguration.Weighing.Saving.MinimumWeight;
            FactoryConfig.WeighingConfig.Saving.Mode = FactoryConfiguration.Weighing.Saving.Mode;
            FactoryConfig.WeighingConfig.Saving.NumberOfBirds = FactoryConfiguration.Weighing.Saving.NumberOfBirds;
            FactoryConfig.WeighingConfig.Saving.StabilisationRange = FactoryConfiguration.Weighing.Saving.StabilisationRange;
            FactoryConfig.WeighingConfig.Saving.StabilisationTime = FactoryConfiguration.Weighing.Saving.StabilisationTime;

            FactoryConfig.WeighingConfig.WeightSorting.Mode = FactoryConfiguration.Weighing.WeightSorting.Mode;
            FactoryConfig.WeighingConfig.WeightSorting.LowLimit = FactoryConfiguration.Weighing.WeightSorting.LowLimit;
            FactoryConfig.WeighingConfig.WeightSorting.HighLimit = FactoryConfiguration.Weighing.WeightSorting.HighLimit;



            // Write config
            Scale.WriteConfig(Dll, FactoryConfig);

            // Save
            if(!Scale.SaveDevice(Dll)) {
               throw new TestException("Unable save config");
            }

            // Date and time
            if(!Scale.SetDateTime(Dll)) {
               throw new TestException("Unable write time"); ;
            }

            // Logo
            byte[] LogoData;
            if(!Veit.Bat1.Bat1Logo.ConvertImage(FactoryConfiguration.Logo, out LogoData)) {
               throw new TestException("Bad logo");
            }

            Scale.UploadLogo(Dll, LogoData);
         } finally {
            Target.Usb.Close();
            TesterBoard.UsbDisconnect();
         }
      }

      /*************************************************************************
      ************************** SUPPORTING METHODS ****************************
      *************************************************************************/

      private bool _UsbChargerConnect() {
         if(!TesterBoard.BatUsbPowerSourceSelectExternalPowerSupply() || !TesterBoard.BatUsbPowerSwitchOff()) {
            throw new TestException("Can't connect usb power");
         }

         Thread.Sleep(TesterCommonConfig.Target.UsbPowerSettleTime);

         return true;
      }

      private bool _UsbChargerDisconnect() {
         if(!TesterBoard.BatUsbPowerSwitchOff()) {
            throw new TestException("Can't disconnect usb power");
         }

         return true;
      }


      private bool _BatteryConnectInverted() {
         if(!TesterBoard.PowerSelectInverted() || !TesterBoard.PowerSwitchOn()) {
            throw new TestException("Tester board can't switch power");
         }

         return true;
      }

      private bool _BatteryConnect() {
         if(!TesterBoard.PowerSelectNormal() || !TesterBoard.PowerSwitchOn()) {
            throw new TestException("Tester board can't switch power");
         }

         Thread.Sleep(TesterCommonConfig.Target.PowerSettleTime);

         return true;
      }

      private bool _AmperemeterConnect() {
         if(!TesterBoard.AmpermeterConnect()) {
            throw new TestException("Tester board can't connect ampermeter");
         }

         // Power must be switched off - it goes through ampermeter now
         if(!TesterBoard.PowerSwitchOff()) {
            throw new TestException("Tester board can't switch power");
         }

         return true;
      }

      private bool _BatteryDisconnect() {
         if(!TesterBoard.PowerSwitchOff()) {
            throw new TestException("Tester board can't switch power");
         }

         return true;
      }

      private bool _VoltageSetNormalAndConnect() {
         _VoltageSetAndConnect(TesterCommonConfig.PowerSupply.Normal.Voltage);

         return true;
      }

      private bool _VoltageSetAndConnect(double Voltage) {
         _SetVoltage(Voltage);
         _BatteryConnect();

         return true;
      }

      private void _TargetSwitchOnPowerNormal() {
         if(!_VoltageSetNormalAndConnect()) {
            throw new TestException("Unable switch on");
         }

         _TargetSwitchOn();
      }

      private void _SetVoltage(double Voltage) {
         PowerSupply.Normal.Voltage = Voltage;

         Thread.Sleep(TesterCommonConfig.PowerSupply.SettleTime);
      }

      private void _TargetSwitchOn() {
         if(!_TargetSwitch(true)) {
            throw new TestException("Unable switch on");
         }
      }

      private void _TargetSwitchOff() {
         if(!_TargetSwitch(false)) {
            throw new TestException("Unable switch off");
         }
      }

      // dodělat korektní vypnutí pro production firmware
      private bool _TargetSwitch(bool On) {
         bool Present = false;

         /*if(Firmware == FirmwareType.TEST || Firmware == FirmwareType.Unknown) {
            if(Target.Test.TestPresence(ref Present)) {
               if(Present) {
                  Firmware = FirmwareType.TEST;
               }
            }
         }

         if(Firmware == FirmwareType.TEST) {
            if((Present && On) || (!Present && !On)) { // nothing to be done
               return true;
            }
         }*/

         if(On) { // switch on
            if(!_TargetDoSwitchOn()) {
               return false;
            }

            Thread.Sleep(TesterCommonConfig.Target.InitTime);
         } else { // switch off
            if(!Target.Test.SwitchOff()) {
               return false;
            }
            /*
            if(Firmware == FirmwareType.TEST) {
               if(!Target.Test.SwitchOff()) {
                  return false;
               }

               TesterBoard.UsbDisconnect();
            } else {
               if(!_UsbChargerDisconnect()) {
                  return false;
               }

               if(!_BatteryDisconnect()) {
                  return false;
               }

               Thread.Sleep(500);

               if(!_BatteryConnect()) {
                  return false;
               }
            }*/
         }

         /*if(Firmware == FirmwareType.TEST || Firmware == FirmwareType.Unknown) {
            if(!Target.Test.TestPresence(ref Present)) {
               Firmware = FirmwareType.Unknown;
            } else {
               if(Present) {
                  Firmware = FirmwareType.TEST;
               }
            }
         }

         if(Firmware == FirmwareType.TEST) {
            if((!Present && On) || (Present && !On)) {
               return false;
            }
         }
         */
         return true;
      }

      private bool _TargetDoSwitchOn() {
         return _TargetDoSwitch(true);

      }

      private bool _TargetDoSwitchOff() {
         return _TargetDoSwitch(false);
      }


      private bool _TargetDoSwitch(bool On) {
         if(!TesterBoard.OnOffPush()) {
            return false;
         }

         int PushTime;

         if(On) {
            PushTime = TesterCommonConfig.Target.SwitchOnTime;
         } else {
            PushTime = TesterCommonConfig.Target.SwitchOffTime;
         }

         Thread.Sleep(PushTime);

         if(!TesterBoard.OnOffRelease()) {
            return false;
         }

         return true;
      }

      private void ReleaseSharedDevices(ResourceType[] Devices) {
         if(Devices == null) {
            return;
         }

         foreach(ResourceType Device in Devices) {
            switch(Device) {
               case ResourceType.Dll:
                  if(Dll == null) {
                     break;
                  }

                  MyController.ReleaseDll(ref Dll, this);
                  break;

               case ResourceType.Multimeter:
                  if(Multimeter == null) {
                     break;
                  }

                  TesterBoard.AmpermeterDisconnect(); // Ampermeter must be disconnected otherwise it influences other testers
                  MyController.ReleaseMultimeter(ref Multimeter, this);
                  break;
            }
         }
      }

      private void GetSharedDevices(ResourceType[] Devices) {
         if(Devices == null) {
            return;
         }

         foreach(ResourceType Device in Devices) {
            switch(Device) {
               case ResourceType.Dll:
                  MyController.GetDll(ref Dll, this);
                  break;

               case ResourceType.Multimeter:
                  MyController.GetMultimeter(ref Multimeter, this);
                  break;
            }
         }
      }

      private void InitMyDevices() {
         Trace.WriteLine("InitMyDevices start");

         if(!PowerSupply.IsOpen) {
            Trace.WriteLine("Power supply not opened");
            if(!PowerSupply.Open()) {
               //throw new TestInitException(Resources.GetString("PowerSupplyOpenError"));
            }

            if(!PowerSupply.IsOpen) {
               //throw new TestInitException(Resources.GetString("PowerSupplyOpenError"));
            }

            PowerSupply.Inverted.Voltage = TesterCommonConfig.PowerSupply.Inverted.Voltage;
            PowerSupply.Inverted.Current = TesterCommonConfig.PowerSupply.Inverted.CurrentLimit;

            PowerSupply.Normal.Voltage = TesterCommonConfig.PowerSupply.Normal.Voltage;
            PowerSupply.Normal.Current = TesterCommonConfig.PowerSupply.Normal.CurrentLimit;

            PowerSupply.Usb.Voltage = TesterCommonConfig.PowerSupply.Usb.Voltage;
            PowerSupply.Usb.Current = TesterCommonConfig.PowerSupply.Usb.CurrentLimit;

            PowerSupply.On = true;
         }

         if(!TesterBoard.IsOpen) {
            Trace.WriteLine("Tester board not opened");
            if(!TesterBoard.Open()) {
               //throw new TestInitException(Resources.GetString("TesterBoardOpenError"));
            }
         }

         if(!Target.Test.IsOpen) {
            Trace.WriteLine("Target not opened");

            if(!Target.Test.Open()) {
               //throw new TestInitException(Resources.GetString("TargetCOMOpenError"));
            }
         }
      }

      /// <summary>
      /// Check value for being in range</summary>

      private bool InRange(double value, Config.Tests.Range _Range) {
         if(value < _Range.Min || value > _Range.Max) {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Match 2 keyboards</summary>

      private void MatchKeyboard(Target.Keyboard Kbd, Target.Keyboard ModelKbd) {
         if(Kbd.On.State != ModelKbd.On.State) {
            throw new TestException("Frozen ON key");
         }

         if(Kbd.K0.State != ModelKbd.K0.State) {
            throw new TestException("Frozen K0 key");
         }

         if(Kbd.K1.State != ModelKbd.K1.State) {
            throw new TestException("Frozen K1 key");
         }

         if(Kbd.K2.State != ModelKbd.K2.State) {
            throw new TestException("Frozen K2 key");
         }
      }

      //private static object UsbLocker = new object();
   }
}

