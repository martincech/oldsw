﻿using System;
using Veit.Bat1;
using System.Text;


namespace Bat1.Tester
{
//=============================================================================
// DLL interface
//=============================================================================

    public class Bat1Dll
    {
//-----------------------------------------------------------------------------
//   Helper functions
//-----------------------------------------------------------------------------

        public uint GetDllVersion() {
            return Dll.GetDllVersion();
        }
        // Returns DLL version

        public bool IsUsbLess() {
           return Dll.IsUsbLess();
        }

        public void EnableLogger( bool Enable ) {
           Dll.EnableLogger( Enable );
        }

        public void DecodeTime(int Timestamp, out int Day, out int Month, out int Year, out int Hour, out int Min, out int Sec) {
           Dll.DecodeTime(Timestamp, out Day, out Month, out Year, out Hour, out Min, out Sec);
        }
        // Decode <Timestamp> to items

        public int EncodeTime( int Day, int Month, int Year, int Hour, int Min, int Sec ) {
           return Dll.EncodeTime( Day, Month, Year, Hour, Min, Sec );
        }
        // Returns timestamp by items

        public double DecodeWeight( int Weight ) {
           return Dll.DecodeWeight( Weight );
        }
        // Decode <Weight> to number

        public int EncodeWeight( double Weight ) {
           return Dll.EncodeWeight( Weight );
        }
        // Encode <Weight> to internal representation

//-----------------------------------------------------------------------------
//   Device functions
//-----------------------------------------------------------------------------

        public bool CheckDevice() {
           return Dll.CheckDevice();
        }
        // Returns TRUE if device present

        public void Close() {
           Dll.Close();
        }
        // Close

        public void FtdiSerialNumber(int SerialNumber) {
           Dll.FtdiSerialNumber(SerialNumber);
        }
        // Close

        public void FtdiLocId(uint LocId) {
           Dll.FtdiLocId(LocId);
        }

        public bool DeviceIsOn( out bool PowerOn) {
           return Dll.DeviceIsOn( out PowerOn);
        }
        // Sets <PowerOn> TRUE on powered device

        public bool DevicePowerOff() {
           return Dll.DevicePowerOff();
        }
        // Switch power off

        public bool GetTime( out int Clock) {
           return Dll.GetTime( out Clock);
        }
        // Get device clock

        public bool SetTime( int Clock) {
           return Dll.SetTime( Clock);
        }
        // Set device clock

        public void NewDevice() {
           Dll.NewDevice();
        }
        // Create new empty device

        public bool LoadConfiguration() {
           return Dll.LoadConfiguration();
        }
        // Load configuration data only from device

        public bool LoadDevice() {
           return Dll.LoadDevice();
        }
        // Load data from device

        public bool SaveDevice() {
           return Dll.SaveDevice();
        }
        // Save data to device

        public bool ReloadConfiguration() {
           return Dll.ReloadConfiguration();
        }
        // Reload configuration from EEPROM to device

        public bool LoadEstimation( out int Promile) {
           return Dll.LoadEstimation( out Promile);
        }
        // Load size estimation [%%]

        public void SaveEstimation( out int Promile ) {
           Dll.SaveEstimation( out Promile );
        }
        // Save size estimation [%%]

        public bool LoadCrashInfo() {
           return Dll.LoadCrashInfo();
        }
        // Load device crash info

//-----------------------------------------------------------------------------
//   EEPROM access
//-----------------------------------------------------------------------------

        public bool ReadEeprom( int Address, byte [] Buffer, int Size) {
           return Dll.ReadEeprom( Address, Buffer, Size);
        }
        // Read data from device EEPROM

        public bool WriteEeprom( int Address, byte [] Buffer, int Size) {
           return Dll.WriteEeprom( Address, Buffer, Size);
        }
        // Write data to device EEPROM

        public int GetEepromSize() {
           return Dll.GetEepromSize();
        }
        // Returns device EEPROM size

        public bool DeviceByEeprom( byte [] Buffer) {
           return Dll.DeviceByEeprom( Buffer);
        }
        // Setup device by EEPROM contents

        public int GetLogoSize() {
           return Dll.GetLogoSize();
        }
        // Returns logo size

        public int GetLogoAddress() {
           return Dll.GetLogoAddress();
        }
        // Returns logo start address

//-----------------------------------------------------------------------------
//   Configuration data
//-----------------------------------------------------------------------------

//---- get/set version

        public uint GetDeviceVersion() {
           return Dll.GetDeviceVersion();
        }
        // Get device version

        public void SetDeviceVersion( uint Version) {
           Dll.SetDeviceVersion( Version);
        }
        // Set device version

        public uint GetBuild() {
           return Dll.GetBuild();
        }
        // Get device build

        public void SetBuild( uint Build) {
           Dll.SetBuild( Build);
        }
        // Set device build

        public uint GetHwVersion() {
           return Dll.GetHwVersion();
        }
        // Get hardware version

        public void SetHwVersion( uint Version) {
           Dll.SetHwVersion( Version);
        }
        // Set hardware version

//---- get/set scale parameters

        public void GetScaleName( StringBuilder Name) {
           Dll.GetScaleName( Name);
        }
        // Get scale name

        public void SetScaleName( string Name) {
           Dll.SetScaleName( Name);
        }
        // Set scale name

        public void ClearPassword() {
           Dll.ClearPassword();
        }
        // Disable password

        public bool ValidPassword() {
           return Dll.ValidPassword();
        }
        // Returns TRUE if password is in use

        public void GetPassword( byte [] Password) {
           Dll.GetPassword( Password);
        }
        // Get password

        public void SetPassword( byte [] Password) {
           Dll.SetPassword( Password);
        }
        // Set password

//---- get/set country data

        public int GetCountry() {
           return Dll.GetCountry();
        }
        // Get country

        public void SetCountry( int Country) {
           Dll.SetCountry( Country);
        }
        // Set country

        public int GetLanguage() {
           return Dll.GetLanguage();
        }
        // Get language

        public void SetLanguage( int Language) {
           Dll.SetLanguage( Language);
        }
        // Set language

        public int GetCodePage() {
           return Dll.GetCodePage();
        }
        // Get code page

        public void SetCodePage(int CodePage) {
           Dll.SetCodePage(CodePage);
        }
        // Set code page

        public int GetDeviceDateFormat() {
           return Dll.GetDeviceDateFormat();
        }
        // Get date format

        public void SetDeviceDateFormat( int Format) {
           Dll.SetDeviceDateFormat( Format);
        }
        // Set date format

        public char GetDateSeparator1() {
           return Dll.GetDateSeparator1();
        }
        // Get first date separator

        public void SetDateSeparator1( char Separator) {
           Dll.SetDateSeparator1( Separator);
        }
        // Set first date separator

        public char GetDateSeparator2() {
           return Dll.GetDateSeparator2();
        }
        // Get second date separator

        public void SetDateSeparator2( char Separator) {
           Dll.SetDateSeparator2( Separator);
        }
        // Set second date separator

        public int GetDeviceTimeFormat() {
           return Dll.GetDeviceTimeFormat();
        }
        // Get time format

        public void SetDeviceTimeFormat( int Format) {
           Dll.SetDeviceTimeFormat( Format);
        }
        // Set time format

        public char GetTimeSeparator() {
           return Dll.GetTimeSeparator();
        }
        // Get time separator

        public void SetTimeSeparator( char Separator) {
           Dll.SetTimeSeparator( Separator);
        }
        // Set time separator

        public int GetDaylightSavingType() {
           return Dll.GetDaylightSavingType();
        }
        // Get daylight saving time type

        public void SetDaylightSavingType( int DstType) {
           Dll.SetDaylightSavingType( DstType);
        }
        // Set daylight saving time type

//---- get/set weighing units

        public int GetWeighingUnits() {
           return Dll.GetWeighingUnits();
        }
        // Get weighing units

        public void SetWeighingUnits( int Units) {
           Dll.SetWeighingUnits( Units);
        }
        // Set weighing units

        public int GetWeighingCapacity() {
           return Dll.GetWeighingCapacity();
        }
        // Get weighing capacity

        public void SetWeighingCapacity( int Capacity ) {
           Dll.SetWeighingCapacity( Capacity );
        }
        // Set weighing capacity

        public int GetWeighingRange() {
           return Dll.GetWeighingRange();
        }
        // Get weighing range

        public int GetWeighingDecimals() {
           return Dll.GetWeighingDecimals();
        }
        // Get weighing decimals

        public int GetWeighingMaxDivision() {
           return Dll.GetWeighingMaxDivision();
        }
        // Get weighing max. division

        public int GetWeighingDivision() {
           return Dll.GetWeighingDivision();
        }
        // Get weighing division

        public void SetWeighingDivision( int Division) {
           Dll.SetWeighingDivision( Division);
        }
        // Set weighing division

//---- get/set sound settings

        public int GetToneDefault() {
           return Dll.GetToneDefault();
        }
        // Get default beep

        public void SetToneDefault( int Tone) {
           Dll.SetToneDefault( Tone);
        }
        // Set default beep

        public int GetToneLight() {
           return Dll.GetToneLight();
        }
        // Get below beep

        public void SetToneLight( int Tone) {
           Dll.SetToneLight( Tone);
        }
        // Set below beep

        public int GetToneOk() {
           return Dll.GetToneOk();
        }
        // Get within beep

        public void SetToneOk( int Tone) {
           Dll.SetToneOk( Tone);
        }
        // Set within beep

        public int GetToneHeavy() {
           return Dll.GetToneHeavy();
        }
        // Get above beep

        public void SetToneHeavy( int Tone) {
           Dll.SetToneHeavy( Tone);
        }
        // Set above beep

        public int GetToneKeyboard() {
           return Dll.GetToneKeyboard();
        }
        // Get keyboard beep

        public void SetToneKeyboard( int Tone) {
           Dll.SetToneKeyboard( Tone);
        }
        // Set keyboard beep

        public bool GetEnableSpecialSounds() {
           return Dll.GetEnableSpecialSounds();
        }
        // Get enable special sounds

        public void SetEnableSpecialSounds( bool Enable) {
           Dll.SetEnableSpecialSounds( Enable);
        }
        // Set enable special sounds

        public int GetVolumeSaving() {
           return Dll.GetVolumeSaving();
        }
        // Get saving volume

        public void SetVolumeSaving( int Volume) {
           Dll.SetVolumeSaving( Volume);
        }
        // Set saving volume

        public int GetVolumeKeyboard() {
           return Dll.GetVolumeKeyboard();
        }
        // Get keyboard volume

        public void SetVolumeKeyboard( int Volume) {
           Dll.SetVolumeKeyboard( Volume);
        }
        // Set keyboard volume

//---- get/set display settings

        public int GetDisplayMode() {
           return Dll.GetDisplayMode();
        }
        // Get display mode

        public void SetDisplayMode( int Mode) {
           Dll.SetDisplayMode( Mode) ;
        }
        // Set display mode

        public int GetDisplayContrast() {
           return Dll.GetDisplayContrast();
        }
        // Get display contrast

        public void SetDisplayContrast( int Contrast) {
           Dll.SetDisplayContrast( Contrast);
        }
        // Set display contrast

        public int GetBacklightMode() {
           return Dll.GetBacklightMode();
        }
        // Get backlight mode

        public void SetBacklightMode( int Mode) {
           Dll.SetBacklightMode( Mode);
        }
        // Set backlight mode

        public int GetBacklightIntensity() {
           return Dll.GetBacklightIntensity();
        }
        // Get backlight intensity

        public void SetBacklightIntensity( int Intensity) {
           Dll.SetBacklightIntensity( Intensity);
        }
        // Set backlight intensity

        public int GetBacklightDuration() {
           return Dll.GetBacklightDuration();
        }
        // Get backlight duration

        public void SetBacklightDuration( int Duration) {
           Dll.SetBacklightDuration( Duration);
        }
        // Set backlight duration

//---- get/set printer settings

        public int GetPrinterPaperWidth() {
           return Dll.GetPrinterPaperWidth();
        }
        // Get printer paper width

        public void SetPrinterPaperWidth( int Width) {
           Dll.SetPrinterPaperWidth( Width);
        }
        // Set printer paper width

        public int GetPrinterCommunicationFormat() {
           return Dll.GetPrinterCommunicationFormat();
        }
        // Get printer communication format

        public void SetPrinterCommunicationFormat( int Format) {
           Dll.SetPrinterCommunicationFormat( Format) ;
        }
        // Set printer communication format

        public int GetPrinterCommunicationSpeed() {
           return Dll.GetPrinterCommunicationSpeed();
        }
        // Get printer communication format

        public void SetPrinterCommunicationSpeed( int Speed) {
           Dll.SetPrinterCommunicationSpeed( Speed);
        }
        // Set printer communication format

//---- get/set global parameters

        public int GetKeyboardTimeout() {
           return Dll.GetKeyboardTimeout();
        }
        // Get keyboard timeout

        public void SetKeyboardTimeout( int Timeout) {
           Dll.SetKeyboardTimeout( Timeout);
        }
        // Set keyboard timeout

        public int GetPowerOffTimeout() {
           return Dll.GetPowerOffTimeout();
        }
        // Get power off timeout

        public void SetPowerOffTimeout( int Timeout) {
           Dll.SetPowerOffTimeout(  Timeout);
        }
        // Set power off timeout

        public bool GetEnableFileParameters() {
           return Dll.GetEnableFileParameters();
        }
        // Get enable file parameters

        public void SetEnableFileParameters( bool Enable) {
           Dll.SetEnableFileParameters( Enable);
        }
        // Set enable file parameters

//---- get/set weighing parameters

        public bool GetEnableMoreBirds() {
           return Dll.GetEnableMoreBirds();
        }
        // Get enable more birds

        public void SetEnableMoreBirds( bool Enable ) {
           Dll.SetEnableMoreBirds( Enable );
        }
        // Set enable more birds

        public int GetWeightSortingMode() {
           return Dll.GetWeightSortingMode();
        }
        // Get weight sorting mode

        public void SetWeightSortingMode( int Mode) {
           Dll.SetWeightSortingMode( Mode);
        }
        // Set weight sorting mode

        public int GetSavingMode() {
           return Dll.GetSavingMode();
        }
        // Get saving mode

        public void SetSavingMode( int Mode) {
           Dll.SetSavingMode( Mode);
        }
        // Set saving mode

        public int GetFilter() {
           return Dll.GetFilter();
        }
        // Get filter

        public void SetFilter( int Filter) {
           Dll.SetFilter( Filter);
        }
        // Set filter

        public int GetStabilisationTime() {
           return Dll.GetStabilisationTime();
        }
        // Get stabilisation time

        public void SetStabilisationTime( int StabilisationTime) {
           Dll.SetStabilisationTime( StabilisationTime);
        }
        // Set stabilisation time

        public int GetMinimumWeight() {
           return Dll.GetMinimumWeight();
        }
        // Get minimum weight

        public void SetMinimumWeight( int Weight) {
           Dll.SetMinimumWeight( Weight);
        }
        // Set minimum weight

        public int GetStabilisationRange() {
           return Dll.GetStabilisationRange();
        }
        // Get stabilisation range

        public void SetStabilisationRange( int Range) {
           Dll.SetStabilisationRange( Range);
        }
        // Set stabilisation range

//---- get/set statistics

        public int GetUniformityRange() {
           return Dll.GetUniformityRange();
        }
        // Get uniformity range

        public void SetUniformityRange( int Range) {
           Dll.SetUniformityRange( Range);
        }
        // Set uniformity range

        public int GetHistogramMode() {
           return Dll.GetHistogramMode();
        }
        // Get histogram mode

        public int GetHistogramRange() {
           return Dll.GetHistogramRange();
        }
        // Get histogram range

        public void SetHistogramRange( int Range) {
           Dll.SetHistogramRange( Range);
        }
        // Set histogram range

        public int GetHistogramStep() {
           return Dll.GetHistogramStep();
        }
        // Get histogram step

        public void SetHistogramStep( int Step) {
           Dll.SetHistogramStep( Step);
        }
        // Set histogram step

//-----------------------------------------------------------------------------
//  Crash data
//-----------------------------------------------------------------------------

        public int GetExceptionTimestamp() {
           return Dll.GetExceptionTimestamp();
        }
        // Get exception date & time

        public int GetExceptionAddress() {
           return Dll.GetExceptionAddress();
        }
        // Get exception address

        public int GetExceptionType() {
           return Dll.GetExceptionType();
        }
        // Get exception type

        public int GetExceptionStatus() {
           return Dll.GetExceptionStatus();
        }
        // Get exception status

        public int GetWatchDogTimestamp() {
           return Dll.GetWatchDogTimestamp();
        }
        // Get watchdog date & time

        public int GetWatchDogStatus() {
           return Dll.GetWatchDogStatus();
        }
        // Get watchdog status

//-----------------------------------------------------------------------------
//  Data files
//-----------------------------------------------------------------------------

//---- directory maitenance

        public int GetFilesCount() {
           return Dll.GetFilesCount();
        }
        // Get number of files

        public void FilesDeleteAll() {
           Dll.FilesDeleteAll();
        }
        // Delete all files

        public int FileCreate() {
           return Dll.FileCreate();
        }
        // Create new file, returns index

//---- get/set directory data

        public void GetFileName( int Index, StringBuilder Name) {
           Dll.GetFileName( Index, Name);
        }
        // Get file name

        public void SetFileName( int Index, string Name) {
           Dll.SetFileName( Index, Name);
        }
        // Set file name

        public void GetFileNote( int Index, StringBuilder Note) {
           Dll.GetFileNote( Index, Note);
        }
        // Get file note

        public void SetFileNote( int Index, string Note) {
           Dll.SetFileNote( Index, Note);
        }
        // Set file note

        public uint GetFileCreation( int Index) {
           return Dll.GetFileCreation(Index);
        }
        // Get file creation date

        public int GetFileRawSize( int Index) {
           return Dll.GetFileRawSize(Index);
        }
        // Get file size [bytes]

        public bool IsCurrentFile( int Index) {
           return Dll.IsCurrentFile(Index);
        }
        // Returns TRUE on current working file

        public void SetCurrentFile( int Index) {
           Dll.SetCurrentFile( Index);
        }
        // Set file on <Index> as current working file

//---- get/set file configuration

        public bool GetFileEnableMoreBirds( int Index) {
           return Dll.GetFileEnableMoreBirds( Index);
        }
        // Get enable more birds

        public void SetFileEnableMoreBirds( int Index, bool Enable ) {
           Dll.SetFileEnableMoreBirds( Index, Enable );
        }
        // Set enable more birds

        public int GetFileNumberOfBirds( int Index) {
           return Dll.GetFileNumberOfBirds( Index);
        }
        // Get number of birds

        public void SetFileNumberOfBirds( int Index, int NumberOfBirds) {
           Dll.SetFileNumberOfBirds( Index, NumberOfBirds);
        }
        // Set number of birds

        public int GetFileWeightSortingMode( int Index) {
           return Dll.GetFileWeightSortingMode( Index);
        }
        // Get weight sorting mode

        public void SetFileWeightSortingMode( int Index, int Mode) {
           Dll.SetFileWeightSortingMode( Index, Mode);
        }
        // Set weight sorting mode

        public int GetFileLowLimit( int Index) {
           return Dll.GetFileLowLimit( Index);
        }
        // Get weight sorting low limit

        public void SetFileLowLimit( int Index, int LowLimit) {
           Dll.SetFileLowLimit( Index, LowLimit);
        }
        // Set weight sorting low limit

        public int GetFileHighLimit( int Index) {
           return Dll.GetFileHighLimit( Index);
        }
        // Get weight sorting high limit

        public void SetFileHighLimit( int Index, int HighLimit) {
           Dll.SetFileHighLimit( Index, HighLimit);
        }
        // Set weight sorting high limit

        public int GetFileSavingMode( int Index) {
           return Dll.GetFileSavingMode( Index);
        }
        // Get saving mode

        public void SetFileSavingMode( int Index, int Mode) {
           Dll.SetFileSavingMode( Index, Mode);
        }
        // Set saving mode

        public int GetFileFilter( int Index) {
           return Dll.GetFileFilter( Index);
        }
        // Get filter

        public void SetFileFilter( int Index, int Filter) {
           Dll.SetFileFilter( Index, Filter);
        }
        // Set filter

        public int GetFileStabilisationTime( int Index) {
           return Dll.GetFileStabilisationTime( Index);
        }
        // Get stabilisation time

        public void SetFileStabilisationTime( int Index, int StabilisationTime) {
           Dll.SetFileStabilisationTime( Index, StabilisationTime);
        }
        // Set stabilisation time

        public int GetFileMinimumWeight( int Index) {
           return Dll.GetFileMinimumWeight( Index);
        }
        // Get minimum weight

        public void SetFileMinimumWeight( int Index, int Weight) {
           Dll.SetFileMinimumWeight( Index, Weight);
        }
        // Set minimum weight

        public int GetFileStabilisationRange( int Index) {
           return Dll.GetFileStabilisationRange( Index);
        }
        // Get stabilisation range

        public void SetFileStabilisationRange( int Index, int Range) {
           Dll.SetFileStabilisationRange( Index, Range);
        }
        // Set stabilisation range

//---- get/set samples

        public int GetFileSamplesCount( int Index ) {
           return Dll.GetFileSamplesCount( Index );
        }
        // Get file samples count

        public void FileClearSamples( int Index) {
           Dll.FileClearSamples( Index);
        }
        // Clear file samples

        public void FileAllocSamples( int Index, int SamplesCount) {
           Dll.FileAllocSamples( Index, SamplesCount);
        }
        // Allocate file samples

        public int GetSampleTimestamp( int Index, int SampleIndex) {
           return Dll.GetSampleTimestamp( Index, SampleIndex);
        }
        // Get file sample timestamp

        public void SetSampleTimestamp( int Index, int SampleIndex, int Timestamp) {
           Dll.SetSampleTimestamp( Index, SampleIndex, Timestamp);
        }
        // Set file sample timestamp

        public int GetSampleWeight( int Index, int SampleIndex) {
           return Dll.GetSampleWeight( Index, SampleIndex);
        }
        // Get file sample weight

        public void SetSampleWeight( int Index, int SampleIndex, int Weight) {
           Dll.SetSampleWeight( Index, SampleIndex, Weight);
        }
        // Set file sample weight

        public int GetSampleFlag( int Index, int SampleIndex) {
           return Dll.GetSampleFlag( Index, SampleIndex);
        }
        // Get file sample flag

        public void SetSampleFlag( int Index, int SampleIndex, int Flag) {
           Dll.SetSampleFlag( Index, SampleIndex, Flag);
        }
        // Set file sample flag

//-----------------------------------------------------------------------------
//  File Groups
//-----------------------------------------------------------------------------

//---- directory maitenance

        public int GetGroupsCount() {
           return Dll.GetGroupsCount();
        }
        // Get number of groups

        public void GroupsDeleteAll() {
           Dll.GroupsDeleteAll();
        }
        // Delete all groups

        public int GroupCreate() {
           return Dll.GroupCreate();
        }
        // Create new group, returns index


//---- get/set directory data

        public void GetGroupName( int Index, StringBuilder Name ) {
           Dll.GetGroupName( Index, Name );
        }
        // Get group name

        public void SetGroupName( int Index, string Name ) {
           Dll.SetGroupName( Index, Name );
        }
        // Set group name

        public void GetGroupNote( int Index, StringBuilder Note ) {
           Dll.GetGroupNote( Index, Note );
        }
        // Get group note

        public void SetGroupNote( int Index, string Note ) {
           Dll.SetGroupNote( Index, Note );
        }
        // Set group note

        public uint GetGroupCreation( int Index ) {
           return Dll.GetGroupCreation( Index );
        }
        // Get group creation date

//--- file list

        public int GetGroupFilesCount( int Index ) {
           return Dll.GetGroupFilesCount( Index );
        }
        // Returns number of files in the group

        public void GroupClearFiles( int Index ) {
           Dll.GroupClearFiles( Index );
        }
        // Clear group files

        public int GetGroupFile( int Index, int FileIndex ) {
           return Dll.GetGroupFile( Index, FileIndex );
        }
        // Returns index into file list from the group at position <FileIndex>

        public void AddGroupFile( int Index, int FileIndex ) {
           Dll.AddGroupFile( Index, FileIndex );
        }
        // Add <FileIndex> into group
    } // class Dll
} // namespace Bat1
