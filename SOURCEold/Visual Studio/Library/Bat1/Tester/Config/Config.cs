﻿//******************************************************************************
//
//   Config.cs     Tester Config Structure
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.Configuration;
using Device.Uart;
using Veit.Bat1;

namespace Bat1.Tester.Config {
   /// <summary>
   /// Tests configuration</summary>

   public class Tests : ConfigurationSection {
      public Tests() {
      }

      /// <summary>
      /// (Min, Max) range</summary>

      public class Range : ConfigurationElement {
         [ConfigurationProperty("Min", IsRequired = true)]
         public double Min {
            get {
               return (double)this["Min"];
            }
            set {
               this["Min"] = value;
            }
         }

         [ConfigurationProperty("Max", IsRequired = true)]
         public double Max {
            get {
               return (double)this["Max"];
            }
            set {
               this["Max"] = value;
            }
         }
      }

      /// <summary>
      /// Current range value</summary>

      public class TCurrentRange : ConfigurationElement {
         [ConfigurationProperty("Current", IsRequired = true)]
         public Range Current {
            get {
               return (Range)this["Current"];
            }
            set {
               this["Current"] = value;
            }
         }
      }

      /// <summary>
      /// Voltage range value</summary>
      /// 
      public class TVoltageRange : ConfigurationElement {
         [ConfigurationProperty("Voltage", IsRequired = true)]
         public Range Voltage {
            get {
               return (Range)this["Voltage"];
            }
            set {
               this["Voltage"] = value;
            }
         }
      }

      /// <summary>
      /// Current range with settle time</summary>

      public class TCurrentRangeSettle : TCurrentRange {
         [ConfigurationProperty("SettleTime", IsRequired = true)]
         public int SettleTime {
            get {
               return (int)this["SettleTime"];
            }
            set {
               this["SettleTime"] = value;
            }
         }
      }

      /// <summary>
      /// ADC test configuration</summary>

      [ConfigurationProperty("Adc")]
      public TAdc Adc {
         get {
            return (TAdc)this["Adc"];
         }
         set {
            this["Adc"] = value;
         }
      }

      public class AdcCommon : ConfigurationElement {

         [ConfigurationProperty("InitDelay", DefaultValue = "1500", IsRequired = true)]
         public Int32 InitDelay {
            get {
               return (Int32)this["InitDelay"];
            }
            set {
               this["InitDelay"] = value;
            }
         }

         [ConfigurationProperty("Delay", DefaultValue = "800", IsRequired = true)]
         public Int32 Delay {
            get {
               return (Int32)this["Delay"];
            }
            set {
               this["Delay"] = value;
            }
         }

         [ConfigurationProperty("Trials", DefaultValue = "5", IsRequired = true)]
         public Int32 Trials {
            get {
               return (Int32)this["Trials"];
            }
            set {
               this["Trials"] = value;
            }
         }

         [ConfigurationProperty("SuccessfulTrials", DefaultValue = "4", IsRequired = true)]
         public Int32 SuccessfulTrials {
            get {
               return (Int32)this["SuccessfulTrials"];
            }
            set {
               this["SuccessfulTrials"] = value;
            }
         }
      }

      public class TAdc : AdcCommon {
         [ConfigurationProperty("ZeroRange")]
         public Range ZeroRange {
            get {
               return (Range)this["ZeroRange"];
            }
            set {
               this["ZeroRange"] = value;
            }
         }

         [ConfigurationProperty("MaxRange")]
         public Range MaxRange {
            get {
               return (Range)this["MaxRange"];
            }
            set {
               this["MaxRange"] = value;
            }
         }

         [ConfigurationProperty("MaxFlicker", DefaultValue = "30", IsRequired = true)]
         public Int32 MaxFlicker {
            get {
               return (Int32)this["MaxFlicker"];
            }
            set {
               this["MaxFlicker"] = value;
            }
         }
      }

      /// <summary>
      /// Internal ADC test configuration</summary>

      [ConfigurationProperty("Iadc")]
      public TIadc Iadc {
         get {
            return (TIadc)this["Iadc"];
         }
         set {
            this["Iadc"] = value;
         }
      }

      public class TIadc : AdcCommon {
         [ConfigurationProperty("HighRange")]
         public Range HighRange {
            get {
               return (Range)this["HighRange"];
            }
            set {
               this["HighRange"] = value;
            }
         }

         [ConfigurationProperty("LowRange")]
         public Range LowRange {
            get {
               return (Range)this["LowRange"];
            }
            set {
               this["LowRange"] = value;
            }
         }

         [ConfigurationProperty("HighVoltage")]
         public double HighVoltage {
            get {
               return (double)this["HighVoltage"];
            }
            set {
               this["HighVoltage"] = value;
            }
         }

         [ConfigurationProperty("LowVoltage")]
         public double LowVoltage {
            get {
               return (double)this["LowVoltage"];
            }
            set {
               this["LowVoltage"] = value;
            }
         }

         [ConfigurationProperty("Reference")]
         public double Reference {
            get {
               return (double)this["Reference"];
            }
            set {
               this["Reference"] = value;
            }
         }

         [ConfigurationProperty("R1")]
         public Int32 R1 {
            get {
               return (Int32)this["R1"];
            }
            set {
               this["R1"] = value;
            }
         }

         [ConfigurationProperty("R2")]
         public Int32 R2 {
            get {
               return (Int32)this["R2"];
            }
            set {
               this["R2"] = value;
            }
         }

         [ConfigurationProperty("AdcRange")]
         public double AdcRange {
            get {
               return (double)this["AdcRange"];
            }
            set {
               this["AdcRange"] = value;
            }
         }

         public double TransferFunction(double Value) {
            return (Value * (Reference * (R1 + R2) / R2)) / AdcRange;
         }
      }

      /// <summary>
      /// Charger current test configuration</summary>

      [ConfigurationProperty("ChargerCurrent")]
      public TChargerCurrent ChargerCurrent {
         get {
            return (TChargerCurrent)this["ChargerCurrent"];
         }
         set {
            this["ChargerCurrent"] = value;
         }

      }

      public class TChargerCurrent : TCurrentRange {
         [ConfigurationProperty("Voltage", IsRequired = true)]
         public double Voltage {
            get {
               return (double)this["Voltage"];
            }
            set {
               this["Voltage"] = value;
            }
         }
      }

      /// <summary>
      /// Charger shutdown test configuration</summary>

      [ConfigurationProperty("ChargerShutdown")]
      public ShutdownCommon ChargerShutdown {
         get {
            return (ShutdownCommon)this["ChargerShutdown"];
         }
         set {
            this["ChargerShutdown"] = value;
         }
      }

      public class ShutdownCommon : ConfigurationElement {

         [ConfigurationProperty("VoltageSteps"),
         ConfigurationCollection(typeof(Steps), ClearItemsName = "clearVoltageSteps")]
         public Steps VoltageSteps {
            get {
               return (Steps)this["VoltageSteps"];
            }
            set {
               this["VoltageSteps"] = value;
            }
         }

         [ConfigurationProperty("Voltage", IsRequired = true)]
         public Range Voltage {
            get {
               return (Range)this["Voltage"];
            }
            set {
               this["Voltage"] = value;
            }
         }

         [ConfigurationProperty("TargetCurrent", IsRequired = true)]
         public double Current {
            get {
               return (double)this["TargetCurrent"];
            }
            set {
               this["TargetCurrent"] = value;
            }
         }
      }

      public class Steps : ConfigurationElementCollection {
         public Step this[int index] {
            get { return (Step)BaseGet(index); }
         }

         protected override ConfigurationElement CreateNewElement() {
            return new Step();
         }

         protected override object GetElementKey(ConfigurationElement element) {
            return ((Step)element).Voltage;
         }
      }

      public class Step : ConfigurationElement {
         [ConfigurationProperty("Voltage", IsRequired = true)]
         public double Voltage {
            get {
               return (double)this["Voltage"];
            }
         }
      }

      /// <summary>
      /// Power shutdown test configuration</summary>

      [ConfigurationProperty("PowerShutdown")]
      public ShutdownCommon PowerShutdown {
         get {
            return (ShutdownCommon)this["PowerShutdown"];
         }
         set {
            this["PowerShutdown"] = value;
         }
      }

      /// <summary>
      /// Backligh on test configuration</summary>

      [ConfigurationProperty("PowerBacklightOn")]
      public TCurrentRange PowerBacklightOn {
         get {
            return (TCurrentRange)this["PowerBacklightOn"];
         }
         set {
            this["PowerBacklightOn"] = value;
         }
      }


      /// <summary>
      /// Backligh off test configuration</summary>

      [ConfigurationProperty("PowerBacklightOff")]
      public TCurrentRange PowerBacklightOff {
         get {
            return (TCurrentRange)this["PowerBacklightOff"];
         }
         set {
            this["PowerBacklightOff"] = value;
         }
      }

      /// <summary>
      /// Power idle test configuration</summary>

      [ConfigurationProperty("PowerIdle")]
      public TPowerIdle PowerIdle {
         get {
            return (TPowerIdle)this["PowerIdle"];
         }
         set {
            this["PowerIdle"] = value;
         }
      }

      /// <summary>
      /// Power idle</summary>

      public class TPowerIdle : ConfigurationElement {
         [ConfigurationProperty("Current", IsRequired = true)]
         public Range Current {
            get {
               return (Range)this["Current"];
            }
            set {
               this["Current"] = value;
            }
         }

         [ConfigurationProperty("Timeout")]
         public int Timeout {
            get {
               return (int)this["Timeout"];
            }
         }

         [ConfigurationProperty("Period")]
         public int Period {
            get {
               return (int)this["Period"];
            }
         }

         [ConfigurationProperty("SubsequentSamples")]
         public int SubsequentSamples {
            get {
               return (int)this["SubsequentSamples"];
            }
         }
      }

      /// <summary>
      /// Power inverted test configuration</summary>

      [ConfigurationProperty("PowerInverted")]
      public TCurrentRangeSettle PowerInverted {
         get {
            return (TCurrentRangeSettle)this["PowerInverted"];
         }
         set {
            this["PowerInverted"] = value;
         }
      }
   }

   /// <summary>
   /// Common tester configuration</summary>

   public class TesterCommon : ConfigurationSection {
      public TesterCommon() {
      }

      /// <summary>
      /// Max count of retries when test fails (USB tests only)</summary>

      [ConfigurationProperty("Retry")]
      public int Retry {
         get {
            return (int)this["Retry"];
         }
      }

      /// <summary>
      /// LPC configuration</summary>

      [ConfigurationProperty("Lpc")]
      public TLpc Lpc {
         get {
            return (TLpc)this["Lpc"];
         }
      }

      public class TLpc : ConfigurationElement {
         [ConfigurationProperty("CrystalFrequency", IsRequired = true)]
         public int CrystalFrequency {
            get {
               return (int)this["CrystalFrequency"];
            }
            set {
               this["CrystalFrequency"] = value;
            }
         }

         [ConfigurationProperty("BaudRate", IsRequired = true)]
         public int BaudRate {
            get {
               return (int)this["BaudRate"];
            }
            set {
               this["BaudRate"] = value;
            }
         }
      }

      /// <summary>
      /// Target configuration</summary>

      [ConfigurationProperty("Target")]
      public TTarget Target {
         get {
            return (TTarget)this["Target"];
         }
      }

      public class TTarget : ConfigurationElement {

         /// <summary>
         /// ON/OFF necessary press time to switch device on [ms]</summary>

         [ConfigurationProperty("SwitchOnTime", IsRequired = true)]
         public int SwitchOnTime {
            get {
               return (int)this["SwitchOnTime"];
            }
            set {
               this["SwitchOnTime"] = value;
            }
         }

         /// <summary>
         /// ON/OFF necessary press time to switch device off [ms]</summary>

         [ConfigurationProperty("SwitchOffTime", IsRequired = true)]
         public int SwitchOffTime {
            get {
               return (int)this["SwitchOffTime"];
            }
            set {
               this["SwitchOffTime"] = value;
            }
         }

         /// <summary>
         /// Firmware initialization time [ms]</summary>

         [ConfigurationProperty("InitTime", IsRequired = true)]
         public int InitTime {
            get {
               return (int)this["InitTime"];
            }
            set {
               this["InitTime"] = value;
            }
         }

         /// <summary>
         /// Power settle time [ms]</summary>

         [ConfigurationProperty("PowerSettleTime", IsRequired = true)]
         public int PowerSettleTime {
            get {
               return (int)this["PowerSettleTime"];
            }
            set {
               this["PowerSettleTime"] = value;
            }
         }

         /// <summary>
         /// USB power settle time [ms]</summary>

         [ConfigurationProperty("UsbPowerSettleTime", IsRequired = true)]
         public int UsbPowerSettleTime {
            get {
               return (int)this["UsbPowerSettleTime"];
            }
            set {
               this["UsbPowerSettleTime"] = value;
            }
         }
      }

      /// <summary>
      /// Power supply configuration</summary>

      [ConfigurationProperty("PowerSupply")]
      public TPowerSupply PowerSupply {
         get {
            return (TPowerSupply)this["PowerSupply"];
         }
      }

      public class TPowerSupply : ConfigurationElement {

         /// <summary>
         /// Normal output configuration</summary>

         [ConfigurationProperty("Normal", IsRequired = true)]
         public TOutput Normal {
            get {
               return (TOutput)this["Normal"];
            }
            set {
               this["Normal"] = value;
            }
         }

         /// <summary>
         /// Inverted output configuration</summary>

         [ConfigurationProperty("Inverted", IsRequired = true)]
         public TOutput Inverted {
            get {
               return (TOutput)this["Inverted"];
            }
            set {
               this["Inverted"] = value;
            }
         }

         /// <summary>
         /// USB output configuration</summary>

         [ConfigurationProperty("Usb", IsRequired = true)]
         public TOutput Usb {
            get {
               return (TOutput)this["Usb"];
            }
            set {
               this["Usb"] = value;
            }
         }

         /// <summary>
         /// Power supply output settle time [ms]</summary>

         [ConfigurationProperty("SettleTime", IsRequired = true)]
         public int SettleTime {
            get {
               return (int)this["SettleTime"];
            }
            set {
               this["SettleTime"] = value;
            }
         }
      }

      public class TOutput : ConfigurationElement {
         [ConfigurationProperty("Voltage", IsRequired = true)]
         public double Voltage {
            get {
               return (double)this["Voltage"];
            }
            set {
               this["Voltage"] = value;
            }
         }

         [ConfigurationProperty("CurrentLimit", IsRequired = true)]
         public double CurrentLimit {
            get {
               return (double)this["CurrentLimit"];
            }
            set {
               this["CurrentLimit"] = value;
            }
         }
      }
   }

   //***************************************************************************
   // Tester
   //***************************************************************************

   public class Tester : ConfigurationSection {
      public Tester() {
      }

      [ConfigurationProperty("", IsRequired = false, IsKey = false, IsDefaultCollection = true)]
      public TTesterCollection Testers {
         get {
            return (TTesterCollection)base[""];
         }
         set {
            base[""] = value;
         }
      }

      [ConfigurationCollection(typeof(TTester), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
      public class TTesterCollection : ConfigurationElementCollection {
         internal const string ItemPropertyName = "Tester";

         public override ConfigurationElementCollectionType CollectionType {
            get { return ConfigurationElementCollectionType.BasicMapAlternate; }
         }

         public TTester this[int index] {
            get { return (TTester)BaseGet(index); }
         }

         protected override string ElementName {
            get { return ItemPropertyName; }
         }

         protected override bool IsElementName(string elementName) {
            return (elementName == ItemPropertyName);
         }

         protected override object GetElementKey(ConfigurationElement element) {
            return ((TTester)element).ID;
         }

         protected override ConfigurationElement CreateNewElement() {
            return new TTester();
         }
      }

      public class TTester : ConfigurationElement {
         [ConfigurationProperty("ID", IsRequired = true)]
         public Int32 ID {
            get {
               return (Int32)this["ID"];
            }
         }

         [ConfigurationProperty("TesterBoard")]
         public TesterBoard TesterBoard {
            get {
               return (TesterBoard)this["TesterBoard"];
            }
         }

         [ConfigurationProperty("Target")]
         public Target Target {
            get {
               return (Target)this["Target"];
            }
         }

         [ConfigurationProperty("Instruments")]
         public Instruments Instruments {
            get {
               return (Instruments)this["Instruments"];
            }
         }
      }
   }

   /// <summary>
   /// Tester board configuration</summary>

   public class TesterBoard : ConfigurationElement {
      /*public TesterBoard() {
      }*/

      [ConfigurationProperty("ComSettings")]
      public Device.Uart.Config.UsbUartSettings ComSettings {
         get {
            return (Device.Uart.Config.UsbUartSettings)this["ComSettings"];
         }
      }
   }

   /// <summary>
   /// Target configuration</summary>

   public class Target : ConfigurationElement {
      [ConfigurationProperty("ComPortSettings")]
      public Device.Uart.Config.ComUartSettings ComPortSettings {
         get {
            return (Device.Uart.Config.ComUartSettings)this["ComPortSettings"];
         }
      }

      [ConfigurationProperty("ComUsbSettings")]
      public Device.Uart.Config.UsbLocUartSettings ComUsbSettings {
         get {
            return (Device.Uart.Config.UsbLocUartSettings)this["ComUsbSettings"];
         }
      }
   }

   /// <summary>
   /// Instruments configuration</summary>

   public class Instruments : ConfigurationElement {
      /*public Instruments() {
      }*/

      /// <summary>
      /// Power supply configuration</summary>

      [ConfigurationProperty("PowerSupply")]
      public TPowerSupply PowerSupply {
         get {
            return (TPowerSupply)this["PowerSupply"];
         }
      }

      public class TPowerSupply : ConfigurationElement {
         [ConfigurationProperty("ComSettings", IsRequired = true)]
         public Device.Uart.Config.ComUartSettings ComSettings {
            get {
               return (Device.Uart.Config.ComUartSettings)this["ComSettings"];
            }
         }
      }

      /// <summary>
      /// Multimeter configuration</summary>

      [ConfigurationProperty("Multimeter")]
      public TMultimeter Multimeter {
         get {
            return (TMultimeter)this["Multimeter"];
         }
      }

      public class TMultimeter : ConfigurationElement {
         [ConfigurationProperty("Address", IsRequired = true)]
         public string Address {
            get {
               return (string)this["Address"];
            }
         }
      }
   }

   /// <summary>
   /// Usb configuration</summary>
   public class TargetCommon : ConfigurationSection {
      public TargetCommon() {
      }

      [ConfigurationProperty("Com", IsRequired = true)]
      public TCom Com {
         get {
            return (TCom)this["Com"];
         }
      }

      public class TCom : ConfigurationElement {
         public TCom() {
         }

         /// <summary>
         /// Communication test data size</summary>

         [ConfigurationProperty("TestDataSize", IsRequired = true)]
         public Int32 TestDataSize {
            get {
               return (Int32)this["TestDataSize"];
            }
         }

         /// <summary>
         /// Communication test receive timeout [ms]</summary>

         [ConfigurationProperty("RxTimeout", IsRequired = true)]
         public Int32 RxTimeout {
            get {
               return (Int32)this["RxTimeout"];
            }
         }
      }

      [ConfigurationProperty("Usb", IsRequired = true)]
      public TUsb Usb {
         get {
            return (TUsb)this["Usb"];
         }
      }


      public class TUsb : ConfigurationElement {
         public TUsb() {
         }

         /// <summary>
         /// USB UART test data size</summary>

         [ConfigurationProperty("TestDataSize", IsRequired = true)]
         public Int32 TestDataSize {
            get {
               return (Int32)this["TestDataSize"];
            }
         }

         /// <summary>
         /// USB UART test receive timeout [ms]</summary>

         [ConfigurationProperty("RxTimeout", IsRequired = true)]
         public Int32 RxTimeout {
            get {
               return (Int32)this["RxTimeout"];
            }
         }

         /// <summary>
         /// Time to wait for device connection [s]</summary>

         [ConfigurationProperty("ConnectDelay", IsRequired = true)]
         public Int32 ConnectDelay {
            get {
               return (Int32)this["ConnectDelay"];
            }
         }
      }
   }

   //***************************************************************************
   // FactoryConfig
   //***************************************************************************

   public class FactoryConfig : ConfigurationSection {
      public FactoryConfig() {
      }

      [ConfigurationProperty("Version")]
      public TVersion Version {
         get {
            return (TVersion)this["Version"];
         }
      }

      public class TVersion : ConfigurationElement {
         [ConfigurationProperty("Major", IsRequired = true)]
         public int Major {
            get {
               return (int)this["Major"];
            }
         }

         [ConfigurationProperty("Minor", IsRequired = true)]
         public int Minor {
            get {
               return (int)this["Minor"];
            }
         }

         [ConfigurationProperty("Build", IsRequired = true)]
         public int Build {
            get {
               return (int)this["Build"];
            }
         }

         [ConfigurationProperty("Hw", IsRequired = true)]
         public int Hw {
            get {
               return (int)this["Hw"];
            }
         }
      }

      [ConfigurationProperty("ConfigName")]
      public string ConfigName {
         get {
            return (string)this["ConfigName"];
         }
      }

      [ConfigurationProperty("ScaleName")]
      public string ScaleName {
         get {
            return (string)this["ScaleName"];
         }
      }

      [ConfigurationProperty("Country")]
      public Veit.Bat1.Country Country {
         get {
            return (Veit.Bat1.Country)this["Country"];
         }
      }

      [ConfigurationProperty("CodePage")]
      public Veit.Bat1.CodePage CodePage {
         get {
            return (Veit.Bat1.CodePage)this["CodePage"];
         }
      }

      [ConfigurationProperty("Language")]
      public Bat1.ScaleLanguagesInDatabase Language {
         get {
            return (Bat1.ScaleLanguagesInDatabase)this["Language"];
         }
      }

      [ConfigurationProperty("DateFormat")]
      public Veit.Bat1.DateFormat DateFormat {
         get {
            return (Veit.Bat1.DateFormat)this["DateFormat"];
         }
      }

      [ConfigurationProperty("DateSeparator1")]
      public char DateSeparator1 {
         get {
            return (char)this["DateSeparator1"];
         }
      }

      [ConfigurationProperty("DateSeparator2")]
      public char DateSeparator2 {
         get {
            return (char)this["DateSeparator2"];
         }
      }

      [ConfigurationProperty("TimeFormat")]
      public Veit.Bat1.TimeFormat TimeFormat {
         get {
            return (Veit.Bat1.TimeFormat)this["TimeFormat"];
         }
      }

      [ConfigurationProperty("TimeSeparator")]
      public char TimeSeparator {
         get {
            return (char)this["TimeSeparator"];
         }
      }

      [ConfigurationProperty("DaylightSavingMode")]
      public Veit.Bat1.DaylightSavingMode DaylightSavingMode {
         get {
            return (Veit.Bat1.DaylightSavingMode)this["DaylightSavingMode"];
         }
      }

      [ConfigurationProperty("Units")]
      public TUnits Units {
         get {
            return (TUnits)this["Units"];
         }
      }

      public class TUnits : ConfigurationElement {
         [ConfigurationProperty("Range", IsRequired = true)]
         public int Range {
            get {
               return (int)this["Range"];
            }
         }

         [ConfigurationProperty("Units", IsRequired = true)]
         public Veit.Scale.Units Units {
            get {
               return (Veit.Scale.Units)this["Units"];
            }
         }

         [ConfigurationProperty("Decimals", IsRequired = true)]
         public int Decimals {
            get {
               return (int)this["Decimals"];
            }
         }

         [ConfigurationProperty("MaxDivision", IsRequired = true)]
         public int MaxDivision {
            get {
               return (int)this["MaxDivision"];
            }
         }

         [ConfigurationProperty("Division", IsRequired = true)]
         public int Division {
            get {
               return (int)this["Division"];
            }
         }

         [ConfigurationProperty("WeighingCapacity", IsRequired = true)]
         public Veit.Bat1.WeighingCapacity WeighingCapacity {
            get {
               return (Veit.Bat1.WeighingCapacity)this["WeighingCapacity"];
            }
         }
      }

      [ConfigurationProperty("Sounds")]
      public TSounds Sounds {
         get {
            return (TSounds)this["Sounds"];
         }
      }

      public class TSounds : ConfigurationElement {
         [ConfigurationProperty("ToneDefault", IsRequired = true)]
         public Veit.Bat1.Tone ToneDefault {
            get {
               return (Veit.Bat1.Tone)this["ToneDefault"];
            }
         }

         [ConfigurationProperty("ToneLight", IsRequired = true)]
         public Veit.Bat1.Tone ToneLight {
            get {
               return (Veit.Bat1.Tone)this["ToneLight"];
            }
         }

         [ConfigurationProperty("ToneOk", IsRequired = true)]
         public Veit.Bat1.Tone ToneOk {
            get {
               return (Veit.Bat1.Tone)this["ToneOk"];
            }
         }

         [ConfigurationProperty("ToneHeavy", IsRequired = true)]
         public Veit.Bat1.Tone ToneHeavy {
            get {
               return (Veit.Bat1.Tone)this["ToneHeavy"];
            }
         }

         [ConfigurationProperty("ToneKeyboard", IsRequired = true)]
         public Veit.Bat1.Tone ToneKeyboard {
            get {
               return (Veit.Bat1.Tone)this["ToneKeyboard"];
            }
         }

         [ConfigurationProperty("EnableSpecial", IsRequired = true)]
         public bool EnableSpecial {
            get {
               return (bool)this["EnableSpecial"];
            }
         }

         [ConfigurationProperty("VolumeKeyboard", IsRequired = true)]
         public int VolumeKeyboard {
            get {
               return (int)this["VolumeKeyboard"];
            }
         }

         [ConfigurationProperty("VolumeSaving", IsRequired = true)]
         public int VolumeSaving {
            get {
               return (int)this["VolumeSaving"];
            }
         }
      }

      [ConfigurationProperty("Display")]
      public TDisplay Display {
         get {
            return (TDisplay)this["Display"];
         }
      }

      public class TDisplay : ConfigurationElement {
         [ConfigurationProperty("Mode", IsRequired = true)]
         public Bat1.DisplayMode Mode {
            get {
               return (Bat1.DisplayMode)this["Mode"];
            }
         }

         [ConfigurationProperty("Contrast", IsRequired = true)]
         public int Contrast {
            get {
               return (int)this["Contrast"];
            }
         }

         [ConfigurationProperty("Backlight", IsRequired = true)]
         public TBacklight Backlight {
            get {
               return (TBacklight)this["Backlight"];
            }
         }

         public class TBacklight : ConfigurationElement {
            [ConfigurationProperty("Mode", IsRequired = true)]
            public Bat1.BacklightMode Mode {
               get {
                  return (Bat1.BacklightMode)this["Mode"];
               }
            }

            [ConfigurationProperty("Intensity", IsRequired = true)]
            public int Intensity {
               get {
                  return (int)this["Intensity"];
               }
            }

            [ConfigurationProperty("Duration", IsRequired = true)]
            public int Duration {
               get {
                  return (int)this["Duration"];
               }
            }
         }
      }

      [ConfigurationProperty("Printer")]
      public TPrinter Printer {
         get {
            return (TPrinter)this["Printer"];
         }
      }

      public class TPrinter : ConfigurationElement {
         [ConfigurationProperty("PaperWidth", IsRequired = true)]
         public int PaperWidth {
            get {
               return (int)this["PaperWidth"];
            }
         }

         [ConfigurationProperty("CommunicationFormat", IsRequired = true)]
         public Veit.Bat1.ComFormat CommunicationFormat {
            get {
               return (Veit.Bat1.ComFormat)this["CommunicationFormat"];
            }
         }

         [ConfigurationProperty("CommunicationSpeed", IsRequired = true)]
         public int CommunicationSpeed {
            get {
               return (int)this["CommunicationSpeed"];
            }
         }
      }

      [ConfigurationProperty("PowerOffTimeout")]
      public int PowerOffTimeout {
         get {
            return (int)this["PowerOffTimeout"];
         }
      }

      [ConfigurationProperty("KeyboardTimeout")]
      public int KeyboardTimeout {
         get {
            return (int)this["KeyboardTimeout"];
         }
      }

      [ConfigurationProperty("EnableFileParameters")]
      public bool EnableFileParameters {
         get {
            return (bool)this["EnableFileParameters"];
         }
      }

      [ConfigurationProperty("Password")]
      public TPassword Password {
         get {
            return (TPassword)this["Password"];
         }
      }

      public class TPassword : ConfigurationElement {
         [ConfigurationProperty("Enable", IsRequired = true)]
         public bool Enable {
            get {
               return (bool)this["Enable"];
            }
         }

         [ConfigurationProperty("Keys", IsRequired = true),
         ConfigurationCollection(typeof(Keys), ClearItemsName = "clearKeys")]
         public Keys ProgrammedNames {
            get {
               return (Keys)this["Keys"];
            }
         }

         public class Keys : ConfigurationElementCollection {
            public TKey this[int index] {
               get { return (TKey)BaseGet(index); }
            }

            protected override ConfigurationElement CreateNewElement() {
               return new TKey();
            }

            protected override object GetElementKey(ConfigurationElement element) {
               return ((TKey)element).Key;
            }

            public class TKey : ConfigurationElement {
               [ConfigurationProperty("Key", IsRequired = true)]
               public string Key {
                  get {
                     return (string)this["Key"];
                  }
               }
            }
         }
      }

      [ConfigurationProperty("Weighing")]
      public TWeighing Weighing {
         get {
            return (TWeighing)this["Weighing"];
         }
      }

      public class TWeighing : ConfigurationElement {
         [ConfigurationProperty("WeightSorting", IsRequired = true)]
         public TWeightSorting WeightSorting {
            get {
               return (TWeightSorting)this["WeightSorting"];
            }
         }

         public class TWeightSorting : ConfigurationElement {
            [ConfigurationProperty("Mode", IsRequired = true)]
            public Veit.Bat1.WeightSorting Mode {
               get {
                  return (Veit.Bat1.WeightSorting)this["Mode"];
               }
            }

            [ConfigurationProperty("LowLimit", IsRequired = true)]
            public int LowLimit {
               get {
                  return (int)this["LowLimit"];
               }
            }

            [ConfigurationProperty("HighLimit", IsRequired = true)]
            public int HighLimit {
               get {
                  return (int)this["HighLimit"];
               }
            }
         }

         [ConfigurationProperty("Saving", IsRequired = true)]
         public TSaving Saving {
            get {
               return (TSaving)this["Saving"];
            }
         }

         public class TSaving : ConfigurationElement {
            [ConfigurationProperty("Mode", IsRequired = true)]
            public Veit.Bat1.SavingMode Mode {
               get {
                  return (Veit.Bat1.SavingMode)this["Mode"];
               }
            }

            [ConfigurationProperty("EnableMoreBirds", IsRequired = true)]
            public bool EnableMoreBirds {
               get {
                  return (bool)this["EnableMoreBirds"];
               }
            }

            [ConfigurationProperty("NumberOfBirds", IsRequired = true)]
            public int NumberOfBirds {
               get {
                  return (int)this["NumberOfBirds"];
               }
            }

            [ConfigurationProperty("Filter", IsRequired = true)]
            public int Filter {
               get {
                  return (int)this["Filter"];
               }
            }

            [ConfigurationProperty("StabilisationTime", IsRequired = true)]
            public double StabilisationTime {
               get {
                  return (double)this["StabilisationTime"];
               }
            }

            [ConfigurationProperty("MinimumWeight", IsRequired = true)]
            public int MinimumWeight {
               get {
                  return (int)this["MinimumWeight"];
               }
            }

            [ConfigurationProperty("StabilisationRange", IsRequired = true)]
            public int StabilisationRange {
               get {
                  return (int)this["StabilisationRange"];
               }
            }
         }
      }

      [ConfigurationProperty("Statistic")]
      public TStatistic Statistic {
         get {
            return (TStatistic)this["Statistic"];
         }
      }

      public class TStatistic : ConfigurationElement {
         [ConfigurationProperty("UniformityRange", IsRequired = true)]
         public int UniformityRange {
            get {
               return (int)this["UniformityRange"];
            }
         }

         [ConfigurationProperty("Histogram", IsRequired = true)]
         public THistogram Histogram {
            get {
               return (THistogram)this["Histogram"];
            }
         }

         public class THistogram : ConfigurationElement {
            [ConfigurationProperty("Mode", IsRequired = true)]
            public Veit.ScaleStatistics.HistogramMode Mode {
               get {
                  return (Veit.ScaleStatistics.HistogramMode)this["Mode"];
               }
            }

            [ConfigurationProperty("Range", IsRequired = true)]
            public int Range {
               get {
                  return (int)this["Range"];
               }
            }

            [ConfigurationProperty("Step", IsRequired = true)]
            public int Step {
               get {
                  return (int)this["Step"];
               }
            }
         }
      }

      [ConfigurationProperty("ActiveFileIndex", IsRequired = true)]
      public int ActiveFileIndex {
         get {
            return (int)this["ActiveFileIndex"];
         }
      }

      [ConfigurationProperty("Logo", IsRequired = true)]
      public string Logo {
         get {
            return (string)this["Logo"];
         }
      }

      [ConfigurationProperty("DefaultFile", IsRequired = true)]
      public TDefaultFile DefaultFile {
         get {
            return (TDefaultFile)this["DefaultFile"];
         }
      }

      public class TDefaultFile : ConfigurationElement {
         [ConfigurationProperty("Name", IsRequired = true)]
         public string Name {
            get {
               return (string)this["Name"];
            }
         }
      }

      /// <summary>
      /// FTDI configuration</summary>

      [ConfigurationProperty("Ftdi", IsRequired = true)]
      public TFtdi Ftdi {
         get {
            return (TFtdi)this["Ftdi"];
         }
      }

      public class TFtdi : ConfigurationElement {
         /// <summary>
         /// Name to be programmed</summary>

         [ConfigurationProperty("DeviceName", IsRequired = true)]
         public string DeviceName {
            get {
               return (string)this["DeviceName"];
            }
         }

         /// <summary>
         /// Manufacturer to be programmed</summary>

         [ConfigurationProperty("ManufacturerName", IsRequired = true)]
         public string ManufacturerName {
            get {
               return (string)this["ManufacturerName"];
            }
         }

         /// <summary>
         /// MaxPower to be programmed</summary>

         [ConfigurationProperty("MaxPower", IsRequired = true)]
         public ushort MaxPower {
            get {
               return (ushort)this["MaxPower"];
            }
         }

         /// <summary>
         /// SelfPowered?</summary>

         [ConfigurationProperty("SelfPowered", IsRequired = true)]
         public bool SelfPowered {
            get {
               return (bool)this["SelfPowered"];
            }
         }
      }

      [ConfigurationProperty("Firmware"),
      ConfigurationCollection(typeof(TFirmwares), ClearItemsName = "clearFirmware")]
      public TFirmwares Firmware {
         get {
            return (TFirmwares)this["Firmware"];
         }
         set {
            this["Firmware"] = value;
         }
      }

      public class TFirmwares : ConfigurationElementCollection {
         public TFirmware this[int index] {
            get { return (TFirmware)BaseGet(index); }
         }

         public string this[Bat1.Tester.Tester.FirmwareType Firmware] {
            get {
               return ((TFirmware)BaseGet(Firmware)).Path;
            }
         }

         protected override ConfigurationElement CreateNewElement() {
            return new TFirmware();
         }

         protected override object GetElementKey(ConfigurationElement element) {
            return ((TFirmware)element).Type;
         }

         public class TFirmware : ConfigurationElement {
            [ConfigurationProperty("Type", IsRequired = true)]
            public Bat1.Tester.Tester.FirmwareType Type {
               get {
                  return (Bat1.Tester.Tester.FirmwareType)this["Type"];
               }
            }

            [ConfigurationProperty("Path", IsRequired = true)]
            public string Path {
               get {
                  return (string)this["Path"];
               }
            }
         }
      }
   }

   //***************************************************************************
   // TestBatch
   //***************************************************************************

   public class TestBatch : ConfigurationSection {
      public TestBatch() {
      }

      [ConfigurationProperty("", IsRequired = false, IsKey = false, IsDefaultCollection = true)]
      public TTestBatchCollection TestBatches {
         get {
            return (TTestBatchCollection)base[""];
         }
         set {
            base[""] = value;
         }
      }

      [ConfigurationCollection(typeof(TTestBatch), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
      public class TTestBatchCollection : ConfigurationElementCollection {
         internal const string ItemPropertyName = "TestBatch";

         public override ConfigurationElementCollectionType CollectionType {
            get { return ConfigurationElementCollectionType.BasicMapAlternate; }
         }

         public TTestBatch this[int index] {
            get { return (TTestBatch)BaseGet(index); }
         }

         new public TTestBatch this[string index] {
            get { return (TTestBatch)BaseGet(index); }
         }

         protected override string ElementName {
            get { return ItemPropertyName; }
         }

         protected override bool IsElementName(string elementName) {
            return (elementName == ItemPropertyName);
         }

         protected override object GetElementKey(ConfigurationElement element) {
            return ((TTestBatch)element).ID;
         }

         protected override ConfigurationElement CreateNewElement() {
            return new TTestBatch();
         }
      }

      public class TTestBatch : ConfigurationElement {
         [ConfigurationProperty("ID", IsRequired = true)]
         public string ID {
            get {
               return (string)this["ID"];
            }
         }

         [ConfigurationProperty("", IsRequired = false, IsKey = false, IsDefaultCollection = true)]
         public TTestsCollection Tests {
            get {
               return (TTestsCollection)base[""];
            }
            set {
               base[""] = value;
            }
         }

         [ConfigurationCollection(typeof(TTest), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
         public class TTestsCollection : ConfigurationElementCollection {
            internal const string ItemPropertyName = "Test";

            public override ConfigurationElementCollectionType CollectionType {
               get { return ConfigurationElementCollectionType.BasicMapAlternate; }
            }

            public TTest this[int index] {
               get { return (TTest)BaseGet(index); }
            }

            new public TTest this[string index] {
               get { return (TTest)BaseGet(index); }
            }

            protected override string ElementName {
               get { return ItemPropertyName; }
            }

            protected override bool IsElementName(string elementName) {
               return (elementName == ItemPropertyName);
            }

            protected override object GetElementKey(ConfigurationElement element) {
               return ((TTest)element).ID;
            }

            protected override ConfigurationElement CreateNewElement() {
               return new TTest();
            }
         }

         public class TTest : ConfigurationElement {
            // ID - enable same test multiple times in the collection
            public static int IDGlobal = 0;
            public int ID;

            public TTest() {
               ID = IDGlobal;

               IDGlobal++;
            }

            [ConfigurationProperty("Name", IsRequired = true)]
            public Bat1.Tester.Tester.Operations Name {
               get {
                  return (Bat1.Tester.Tester.Operations)this["Name"];
               }
            }
         }
      }
   }
}// namespace








