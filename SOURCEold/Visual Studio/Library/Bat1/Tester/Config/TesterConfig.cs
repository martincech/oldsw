﻿using System;
using System.Configuration;
using Device.Uart;

//***************************************************************************
// Tester
//***************************************************************************

public class Tester : ConfigurationSection {
   public Tester() {
   }

   [ConfigurationProperty("", IsRequired = false, IsKey = false, IsDefaultCollection = true)]
   public TTesterCollection Testers {
      get {
         return (TTesterCollection)base[""];
      }
      set {
         base[""] = value;
      }
   }

   [ConfigurationCollection(typeof(TTester), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
   public class TTesterCollection : ConfigurationElementCollection {
      internal const string ItemPropertyName = "Tester";

      public override ConfigurationElementCollectionType CollectionType {
         get { return ConfigurationElementCollectionType.BasicMapAlternate; }
      }

      public TTester this[int index] {
         get { return (TTester)BaseGet(index); }
      }

      protected override string ElementName {
         get { return ItemPropertyName; }
      }

      protected override bool IsElementName(string elementName) {
         return (elementName == ItemPropertyName);
      }

      protected override object GetElementKey(ConfigurationElement element) {
         return ((TTester)element).ID;
      }

      protected override ConfigurationElement CreateNewElement() {
         return new TTester();
      }
   }

   public class TTester : ConfigurationElement {
      [ConfigurationProperty("ID", IsRequired = true)]
      public Int32 ID {
         get {
            return (Int32)this["ID"];
         }
      }

      [ConfigurationProperty("TesterBoard")]
      public TesterBoard TesterBoard {
         get {
            return (TesterBoard)this["TesterBoard"];
         }
      }

      [ConfigurationProperty("Target")]
      public Target Target {
         get {
            return (Target)this["Target"];
         }
      }

      [ConfigurationProperty("Instruments")]
      public Instruments Instruments {
         get {
            return (Instruments)this["Instruments"];
         }
      }
   }
}

/// <summary>
/// Tester board configuration</summary>

public class TesterBoard : ConfigurationElement {
   /*public TesterBoard() {
   }*/

   [ConfigurationProperty("ComSettings")]
   public Device.Uart.Config.UsbUartSettings ComSettings {
      get {
         return (Device.Uart.Config.UsbUartSettings)this["ComSettings"];
      }
   }
}

/// <summary>
/// Target configuration</summary>

public class Target : ConfigurationElement {
   [ConfigurationProperty("ComPortSettings")]
   public Device.Uart.Config.ComUartSettings ComPortSettings {
      get {
         return (Device.Uart.Config.ComUartSettings)this["ComPortSettings"];
      }
   }

   [ConfigurationProperty("ComUsbSettings")]
   public Device.Uart.Config.UsbLocUartSettings ComUsbSettings {
      get {
         return (Device.Uart.Config.UsbLocUartSettings)this["ComUsbSettings"];
      }
   }
}

/// <summary>
/// Instruments configuration</summary>

public class Instruments : ConfigurationElement {
   /*public Instruments() {
   }*/

   /// <summary>
   /// Power supply configuration</summary>

   [ConfigurationProperty("PowerSupply")]
   public TPowerSupply PowerSupply {
      get {
         return (TPowerSupply)this["PowerSupply"];
      }
   }

   public class TPowerSupply : ConfigurationElement {
      [ConfigurationProperty("ComSettings", IsRequired = true)]
      public Device.Uart.Config.ComUartSettings ComSettings {
         get {
            return (Device.Uart.Config.ComUartSettings)this["ComSettings"];
         }
      }
   }

   /// <summary>
   /// Multimeter configuration</summary>

   [ConfigurationProperty("Multimeter")]
   public TMultimeter Multimeter {
      get {
         return (TMultimeter)this["Multimeter"];
      }
   }

   public class TMultimeter : ConfigurationElement {
      [ConfigurationProperty("Address", IsRequired = true)]
      public string Address {
         get {
            return (string)this["Address"];
         }
      }
   }
}

/// <summary>
/// Usb configuration</summary>
public class TargetCommon : ConfigurationSection {
   public TargetCommon() {
   }

   [ConfigurationProperty("Com", IsRequired = true)]
   public TCom Com {
      get {
         return (TCom)this["Com"];
      }
   }

   public class TCom : ConfigurationElement {
      public TCom() {
      }

      /// <summary>
      /// Communication test data size</summary>

      [ConfigurationProperty("TestDataSize", IsRequired = true)]
      public Int32 TestDataSize {
         get {
            return (Int32)this["TestDataSize"];
         }
      }

      /// <summary>
      /// Communication test receive timeout [ms]</summary>

      [ConfigurationProperty("RxTimeout", IsRequired = true)]
      public Int32 RxTimeout {
         get {
            return (Int32)this["RxTimeout"];
         }
      }
   }

   [ConfigurationProperty("Usb", IsRequired = true)]
   public TUsb Usb {
      get {
         return (TUsb)this["Usb"];
      }
   }


   public class TUsb : ConfigurationElement {
      public TUsb() {
      }

      /// <summary>
      /// USB UART test data size</summary>

      [ConfigurationProperty("TestDataSize", IsRequired = true)]
      public Int32 TestDataSize {
         get {
            return (Int32)this["TestDataSize"];
         }
      }

      /// <summary>
      /// USB UART test receive timeout [ms]</summary>

      [ConfigurationProperty("RxTimeout", IsRequired = true)]
      public Int32 RxTimeout {
         get {
            return (Int32)this["RxTimeout"];
         }
      }

      /// <summary>
      /// Time to wait for device connection [s]</summary>

      [ConfigurationProperty("ConnectDelay", IsRequired = true)]
      public Int32 ConnectDelay {
         get {
            return (Int32)this["ConnectDelay"];
         }
      }
   }
}
