﻿using System;
using Device.Uart;

namespace Device.Bat1 {
   public class InstrumentsConfig : TesterConfig {
      public InstrumentsConfig() {
      }
   }

   public class MultimeterConfig : InstrumentsConfig {
      public string Address = "USB0::2391::1543::MY47022753";

      public MultimeterConfig() {
      }
   }


   public class PowerSupplyConfig : InstrumentsConfig {
      public Uart.ComUart.InterfaceComUartSettings ComSettings = new Uart.ComUart.InterfaceComUartSettings();

      public PowerSupplyConfig() {
         ComSettings.PortName = "COM3";
         ComSettings.BaudRate = 9600;
         ComSettings.Parity = Uart.Uart.Parity.None;
         ComSettings.Bits = 8;
         ComSettings.StopBits = Uart.Uart.StopBit.One;
         ComSettings.Handshake = Uart.Uart.Handshake.None;
      }
   }

}