﻿using System;
using Device.Uart;

namespace Device.Bat1 {
   public class UsbConfig : TesterConfig {
      public Uart.UsbUart.InterfaceUsbUartSettings ComSettings = new Uart.UsbUart.InterfaceUsbUartSettings();

      public string UsbDeviceName = TargetUsbName;
      public string UsbDeviceManufacturer = "VEIT";

      public string[] FtdiNamesProgrammed = new string[] { TargetUsbName, "FlexScale" };
      public string[] FtdiNamesBlank = new string[] { "FT232R USB UART", "USB HS Serial Converter"};


      public int TestDataSize = 256;


      public int ConnectDelay = 7;
      public int Timeout = 2000;


      public UsbConfig() {
         ComSettings.DeviceName = TargetUsbName;
         ComSettings.BaudRate = 38400;
         ComSettings.Parity = Uart.Uart.Parity.None;
         ComSettings.Bits = 8;
         ComSettings.StopBits = Uart.Uart.StopBit.One;
         ComSettings.Handshake = Uart.Uart.Handshake.None;
      }
   }


}