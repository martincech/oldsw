﻿using System;
using Device.Uart;

namespace Device.Bat1 {
   public class TargetConfig : TesterConfig {
      public Uart.ComUart.InterfaceComUartSettings ComSettings = new Uart.ComUart.InterfaceComUartSettings();

      public TargetConfig() {
         ComSettings.PortName = "COM1";
         ComSettings.BaudRate = 9600;
         ComSettings.Parity = Uart.Uart.Parity.None;
         ComSettings.Bits = 8;
         ComSettings.StopBits = Uart.Uart.StopBit.One;
         ComSettings.Handshake = Uart.Uart.Handshake.None;
      }
   }


}