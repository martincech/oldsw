﻿//******************************************************************************
//
//   LpcFlash.cs     LpcFlash
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Uart;
using System.Threading; // Thread.Sleep()

public class LpcFlash : LpcIsp {

   //******************************* Public ************************************

   public string Version;
   public string Device;

   public bool EnableFlashComparation;  // compare flash segment after burn

   //****************************** Protected **********************************

   protected int FPartId;                        // LPC model number
   protected int FBootVersion;                   // LPC boot version
   protected int FTotalSize;                     // total read/write size
   protected int FDoneSize;                      // read/write size alrady done

   //******************************* Private ***********************************

   private const int LPC_READ_CHUNK = 900;

   //******************************* Public ************************************

   //***************************************************************************
   // Constructor
   //***************************************************************************

   public LpcFlash()
      // Constructor
   {
      Version = "?";
      Device = "?";

      EnableFlashComparation = false;
   } // TLpcFlash

   //***************************************************************************
   // Get Device info
   //***************************************************************************

   public bool GetDeviceInfo()
      // Get CPU informations
   {
      bool Synchronized = false;
      bool EnteredProgramming = false;

      StartTime = DateTime.Now;

      for(int i = 0; i < 3; i++) {
         EnteredProgramming = false;

         if(!EnterProgramming()) {
            Thread.Sleep(100);
            continue;
         }

         EnteredProgramming = true;

         if(!Synchronize()) {
            Thread.Sleep(100);
            continue;
         }

         Synchronized = true;

         break;
      }

      if(!EnteredProgramming) {
         return false;
      }

      if(!Synchronized) {
         ResetTarget();

         return false;
      }

      Progress = 0.75;

      // read device info :
      if(!ReadPartId(ref FPartId)) {
         return false;
      }

      switch(FPartId) {
         case (int)PartIdentification.LPC2131:
            Device = "LPC2131";
            break;
         case (int)PartIdentification.LPC2132:
            Device = "LPC2132";
            break;
         case (int)PartIdentification.LPC2134:
            Device = "LPC2134";
            break;
         case (int)PartIdentification.LPC2136:
            Device = "LPC2136";
            break;
         case (int)PartIdentification.LPC2138:
            Device = "LPC2138";
            break;
         default:
            return false;
      }

      if(!ReadBootVersion(ref FBootVersion)) {
         return false;
      }

      Version = ((FBootVersion >> 4).ToString() + "." + (FBootVersion & 0x0F).ToString());

      return (true);
   } // GetDeviceInfo

   //***************************************************************************
   // Write flash
   //***************************************************************************

   public bool WriteFlash(int Address, byte[] Data, int Size)
      // Write <Data> to flash memory
   {
      StartTime = DateTime.Now;

      if((Address + Size) > GetFlashSize()) {
         return (false);
      }

      if(Address != 0) {
         return (false);                          // not implemented yet
      }

      // enable flash write :
      if(!Unlock()) {
         return (false);
      }

      // erase flash :
      int LastSector = GetLastSector();

      if(!PrepareSector(0, LastSector)) {
         return (false);
      }

      if(!EraseSector(0, LastSector)) {
         return (false);
      }

      if(!BlankCheck(0, LastSector)) {
         return (false);
      }

      // progress data :
      FTotalSize = Size;
      FDoneSize = 0;
      Progress = 0;

      // write sectors :
      int Sector = 0;
      int DataPos = 0;
      byte[] DataChunk = new byte[32 * 1024];
      int SectorSize;

      do {
         SectorSize = Size;

         if(SectorSize > GetSectorSize(Sector)) {
            SectorSize = GetSectorSize(Sector);
         }

         Array.Copy(Data, DataPos, DataChunk, 0, SectorSize);

         if(!WriteFlashSector(Sector, Address, DataChunk, SectorSize)) {
            return (false);
         }

         Address += SectorSize;
         DataPos += SectorSize;

         Size -= SectorSize;
         Sector++;
      } while(Size > 0);

      return (true);
   } // WriteFlash

   //***************************************************************************
   // Read memory
   //***************************************************************************

   public bool ReadFlash(int Address, ref byte[] Data, int Size)
      // Read <Data> from memory <Address>
   {
      StartTime = DateTime.Now;

      if((Address + Size) > GetFlashSize()) {
         return (false);
      }

      // progress data :
      FTotalSize = Size;
      FDoneSize = 0;
      Progress = 0;

      // read chunks :
      byte[] DataChunk = new byte[LPC_READ_CHUNK];
      int ChunkSize;
      int DataPos = 0;

      do {
         ChunkSize = Size;

         if(ChunkSize > LPC_READ_CHUNK) {
            ChunkSize = LPC_READ_CHUNK;
         }

         if(!ReadMemory(Address, ref DataChunk, ChunkSize)) {
            return (false);
         }

         Array.Copy(DataChunk, 0, Data, DataPos, ChunkSize);

         Address += ChunkSize;
         DataPos += ChunkSize;
         Size -= ChunkSize;

         // update progress :
         FDoneSize += ChunkSize;
         Progress = (double)FDoneSize / FTotalSize;
      } while(Size > 0);

      return (true);
   } // ReadFlash

   //***************************************************************************
   // Compare flash
   //***************************************************************************

   public bool CompareFlash(int Address, byte[] Data, int Size)
      // Compare flash memory with <Data>
   {
      StartTime = DateTime.Now;

      byte[] Buffer = new byte[LPC_READ_CHUNK];

      if((Address + Size) > GetFlashSize()) {
         return (false);
      }

      int Offset = 0;

      if(Address < LPC_VECTOR_SIZE) {
         // skip vector area
         Offset = LPC_VECTOR_SIZE - Address;
         Address = LPC_VECTOR_SIZE;
         Size -= Offset;
      }

      // progress data :
      FTotalSize = Size;
      FDoneSize = 0;
      Progress = 0;

      int DataPos = Offset;

      // read chunks :
      byte[] DataChunk = new byte[LPC_READ_CHUNK];
      int ChunkSize;
      do {
         ChunkSize = Size;

         if(ChunkSize > LPC_READ_CHUNK) {
            ChunkSize = LPC_READ_CHUNK;
         }

         if(!ReadMemory(Address, ref Buffer, ChunkSize)) {
            return (false);
         }


         // compare :
         for(int i = 0; i < ChunkSize; i++) {
            if(Buffer[i] != Data[i + DataPos]) {
               return (false);
            }
         }
         // next chunk :
         Address += ChunkSize;
         Size -= ChunkSize;
         DataPos += ChunkSize;

         // update progress :
         FDoneSize += ChunkSize;
         Progress = (double)FDoneSize / FTotalSize;
      } while(Size > 0);

      return (true);
   } // CompareFlash

   //***************************************************************************
   // Rest
   //***************************************************************************

   public bool Reset()
      // Restart device
   {
      return (ResetTarget());
   } // Reset

   //****************************************************************************
   // Get flash size
   //****************************************************************************

   public int GetFlashSize()
      // Returns flash size
   {
      return (512 * 1024 - 12 * 1024);    // total size - boot sector size
      // model specific
   } // GetFlashSize

   //***************************************************************************
   // Get last sector
   //***************************************************************************

   public int GetLastSector()
      // Returns last sector number
   {
      return (26);
      // model specific
   } // GetLastSector

   //***************************************************************************
   // Get RAM size
   //***************************************************************************

   public int GetRamSize()
      // Return RAM size
   {
      return (32 * 1024);
      // model specific
   } // GetRamSize

   //***************************************************************************
   // Get segment size
   //***************************************************************************

   public int GetSectorSize(int Sector)
      // Returns segment size
   {
      if(Sector < 8) {
         return (4 * 1024);
      }
      if(Sector < 22) {
         return (32 * 1024);
      }
      return (4 * 1024);
      // model specific
   } // GetSectorSize

   //***************************************************************************
   // Get Copy size
   //***************************************************************************

   public int GetCopySize(int Size)
      // Returns maximal copy size
   {
      if(Size <= 256) {
         return (256);
      }

      if(Size <= 512) {
         return (512);
      }

      if(Size <= 1024) {
         return (1024);
      }

      return (4096);
      // model specific
      // compare copy size with available RamSize - (LPC_RAM_ADDRESS - LPC_RAM_START)
   } // GetCopySize

   //***************************************************************************
   // Write flash segment
   //***************************************************************************

   public bool WriteFlashSector(int Sector, int Address, byte[] Data, int Size)
      // Write <Data> to <Sector>
   {
      // copy RAM to flash :
      int CopySize;
      byte[] DataChunk = new byte[GetCopySize(Size)];
      int DataPos = 0;

      do {
         CopySize = GetCopySize(Size);

         Array.Copy(Data, DataPos, DataChunk, 0, CopySize);

         if(!WriteToRam(LPC_RAM_ADDRESS, DataChunk, CopySize)) {
            return (false);
         }

         if(!PrepareSector(Sector, Sector)) {
            return (false);
         }

         if(!CopyRamToFlash(Address, LPC_RAM_ADDRESS, CopySize)) {
            return (false);
         }

         if(EnableFlashComparation) {
            if(!Compare(Address, LPC_RAM_ADDRESS, CopySize)) {
               return (false);
            }
         }

         Address += CopySize;
         DataPos += CopySize;

         if(Size > CopySize) {
            Size -= CopySize;
            FDoneSize += CopySize;
         } else {
            // copy size may be greater than remainder
            FDoneSize += Size;            // take true remainder
            Size = 0;                     // terminate loop
         }

         Progress = (double)FDoneSize / FTotalSize;
      } while(Size > 0);

      return (true);
   } // WriteFlashSector

}
