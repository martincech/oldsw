﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

public static class Trace
{
   public static bool Debug = false;
   public static string[] List = new string[10000];
   static int ListPtr = 0;
   static bool MaxReached = false;

   public static TextBox Window = null;

   private delegate void EventArgsDelegate(string str);

   public static void WriteLine(string Str) {
      if(Debug == true) {
         WriteInt(DateTime.Now.Second + ": " + Str + "\r\n");
      }
   }

   public static void Write(byte[] Arr) {
      if(Debug == true) {
         string Str = "";
         foreach(byte b in Arr) {
            Str += String.Format("{0,2:X2}", b);
         }

         WriteInt(Str + "\r\n");
      }
   }

   public static void Write(byte[] Arr, uint Length) {
      if(Debug == true) {
         string Str = "";
         uint a = 0;
         foreach(byte b in Arr) {
            Str += String.Format("{0,2:X2}", b);
            a++;

            if(a == Length) {
               break;
            }
         }

         WriteInt(Str + "\r\n");
      }
   }

   private static void WriteInt(string str) {
      if(Window != null) {
         if(Window.InvokeRequired) {
            object[] parametersArray = new object[] { str };
            Window.Invoke(new EventArgsDelegate(WriteInt), parametersArray);
            return;
         }
         if(Window.Text.Length > 5000) {
            Window.Text = Window.Text.Substring(500, Window.Text.Length - 500);
         }

         Window.Text += str;
      } else {
         List[ListPtr] = str;

         ListPtr++;

         if(ListPtr >= List.Length) {
            MaxReached = true;
            ListPtr = 0;
         }
      }
   }

   public static void Erase() {
      ListPtr = 0;
      MaxReached = false;
   }

   public static string Get() {
      string DebugStr = "";

      if(MaxReached) {
         for(int i = ListPtr ; i < List.Length ; i++) {
            if(List[i] != null) {
               DebugStr += List[i];
            }
         }
      }

      for(int i = 0; i < ListPtr; i++) {
         if(List[i] != null) {
            DebugStr += List[i];
         }
      }

      return DebugStr;
   }
}
