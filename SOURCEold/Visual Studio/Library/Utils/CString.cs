﻿//******************************************************************************
//
//   CString.cs     CString
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;

public class CString {

   //******************************* Private ***********************************

   private byte[] CStr;

   private System.Text.ASCIIEncoding Enc = new System.Text.ASCIIEncoding();

   //******************************* Public ************************************

   public int Length {
      get {
         int Pos = 0;

         do {
            // the CString is not null terminated
            if(Pos >= CStr.Length) {
               return -1;
            }
         }
         while(CStr[Pos++] != (byte)'\0');

         return Pos - 1;
      }
   }

   //***************************************************************************
   // Indexer - int
   //***************************************************************************

   public byte this[int i] {
      get {
         return CStr[i];
      }
      set {
         CStr[i] = (byte)value;
      }
   }

   //***************************************************************************
   // Constructor - string
   //***************************************************************************

   public CString(string Str) {
      // allocate CStr
      CStr = new byte[Str.Length + 1];

      // copy to CStr
      Array.Copy(Enc.GetBytes(Str), 0, CStr, 0, Str.Length);

      // null termination
      CStr[Str.Length] = (byte)'\0';
   }

   //***************************************************************************
   // Constructor - byte[]
   //***************************************************************************

   public CString(byte[] Str) {
      int Pos = 0;

      // go through byte array till the end or '\0' is reached
      while(true) {
         if(Pos >= Str.Length) {
            break;
         }

         if(Str[Pos] == (byte)'\0') {
            break;
         }

         Pos++;
      }

      // allocate CStr
      CStr = new byte[Pos + 1];

      // copy to CStr
      Array.Copy(Str, 0, CStr, 0, Pos);

      // null termination
      CStr[Pos] = (byte)'\0';
   }

   //***************************************************************************
   // Constructor - byte[], length, adds '\0' at the end
   //***************************************************************************

   public CString(byte[] Str, int Length) {
      int Pos = 0;

      if(Length > Str.Length) {
         Length = Str.Length;
      }

      // go through byte array till the end '\0' is reached
      while(true) {
         if(Pos >= Length) {
            break;
         }

         Pos++;
      }

      // allocate CStr
      CStr = new byte[Pos + 1];

      // copy to CStr
      Array.Copy(Str, 0, CStr, 0, Pos);

      // null termination
      CStr[Pos] = (byte)'\0';
   }

   //***************************************************************************
   // Constructor - void CString with length
   //***************************************************************************

   public CString(int Length) {
      CStr = new byte[Length];
   }

   //***************************************************************************
   // Constructor - int
   //***************************************************************************

   public CString(int Number, byte dummy) : this(Number.ToString()) {}

   //***************************************************************************
   // (CString)int operator
   //***************************************************************************

   public static explicit operator CString(int Number) {
      return new CString(Number, 1);
   }

   //***************************************************************************
   // (int)CString operator
   //***************************************************************************

   public static explicit operator int(CString CStr) {
      return CStr.ToInt();
   }

   //***************************************************************************
   // (CString)string operator
   //***************************************************************************

   public static explicit operator CString(string Str) {
      return new CString(Str);
   }

   //***************************************************************************
   // (string)CString operator
   //***************************************************************************

   public static explicit operator string(CString CStr) {
      return CStr.ToString();
   }

   //***************************************************************************
   // (CString)byte[] operator
   //***************************************************************************

   public static explicit operator CString(byte[] Str) {
      return new CString(Str);
   }

   //***************************************************************************
   // (byte[])CString operator
   //***************************************************************************

   public static explicit operator byte[](CString CStr) {
      int Pos = 0;

      byte[] Array = new byte[CStr.Length];

      while(CStr.CStr[Pos] != (byte)'\0') {
         Array[Pos] = CStr.CStr[Pos];

         Pos++;
      }

      return Array;
   }

   //***************************************************************************
   // CopyTo
   //***************************************************************************

   public void CopyTo(byte[] Arr, int StartPos) {
      int Pos = StartPos;

      foreach(byte Ch in CStr) {
         Arr[Pos++] = Ch;
      }
   }

   //***************************************************************************
   // StrCmp
   //***************************************************************************

   public static int StrCmp(CString CStr1, CString CStr2)
      /*
       * 0  - equality
       * 1  - first character that does not match has a greater value in CStr1 than in CStr2
       * -1 - opposite
       */
   {
      int Pos = 0;

      // references can be null
      if(object.ReferenceEquals(CStr1, null) && object.ReferenceEquals(CStr2, null)) {
         return 0;
      } else if(object.ReferenceEquals(CStr2, null)) {
         return 1;
      } else if(object.ReferenceEquals(CStr1, null)) {
         return -1;
      }

      // compare CStrings
      int Length1 = CStr1.Length;
      int Length2 = CStr2.Length;

      while(true) {
         if(Pos >= Length1 || Pos >= Length2) {
            return 0;
         }

         if(CStr1[Pos] > CStr2[Pos]) {
            return 1;
         }

         if(CStr1[Pos] < CStr2[Pos]) {
            return -1;
         }

         if(CStr1[Pos] == (byte)'\0') {
            return 0;
         }

         Pos++;
      }
   }

   //***************************************************************************
   // ToInt
   //***************************************************************************

   public int ToInt() {
      int Number = 0;
      int Pos = 0;

      // ignore initial spaces
      while(CStr[Pos] == (byte)' ') {
         Pos++;
      }

      // parse number till foreign character appears
      while(CStr[Pos] >= (byte)'0' && CStr[Pos] <= (byte)'9') {
         Number *= 10;
         Number += CStr[Pos] - '0';
         Pos++;
      }

      return Number;
   }

   //***************************************************************************
   // ToString
   //***************************************************************************

   public override string ToString() {
      return Enc.GetString((byte[])this);
   }

   //***************************************************************************
   // GetHashCode - unimplemented!
   //***************************************************************************

   public override int GetHashCode() {
      return (0);
   }

   //***************************************************************************
   // Equals
   //***************************************************************************

   public override bool Equals(System.Object Obj) {
      // If parameter is null return false.
      if(Obj == null) {
         return false;
      }

      // If parameter cannot be cast to CString return false.
      CString CStr = Obj as CString;

      if((System.Object)CStr == null) {
         return false;
      }

      // Return true if the fields match.
      return (this == CStr);

   }

   //***************************************************************************
   // !=
   //***************************************************************************

   public static bool operator !=(CString CStr1, CString CStr2) {
      return (StrCmp(CStr1, CStr2) != 0);
   }

   //***************************************************************************
   // ==
   //***************************************************************************

   public static bool operator ==(CString CStr1, CString CStr2) {
      return (StrCmp(CStr1, CStr2) == 0);
   }

   //***************************************************************************
   // >
   //***************************************************************************

   public static bool operator >(CString CStr1, CString CStr2) {
      return (StrCmp(CStr1, CStr2) > 0);
   }

   //***************************************************************************
   // >=
   //***************************************************************************

   public static bool operator >=(CString CStr1, CString CStr2) {
      return (StrCmp(CStr1, CStr2) >= 0);
   }

   //***************************************************************************
   // <
   //***************************************************************************

   public static bool operator <(CString CStr1, CString CStr2) {
      return (StrCmp(CStr1, CStr2) < 0);
   }

   //***************************************************************************
   // <=
   //***************************************************************************

   public static bool operator <=(CString CStr1, CString CStr2) {
      return (StrCmp(CStr1, CStr2) <= 0);
   }

}
