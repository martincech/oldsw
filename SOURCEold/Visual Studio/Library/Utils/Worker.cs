﻿using System;
using System.Threading;

public class Worker {
   
   public DateTime StartTime;

   private double _Progress = 0;

   public double Progress {
      get {
         return _Progress;
      }

      protected set {
         _Progress = value;


      }
   }

   public Worker() {
   }
}

public class BackgroundOperation {
   public Thread Thread;
   public Worker Worker;
   public bool InProgress;
};