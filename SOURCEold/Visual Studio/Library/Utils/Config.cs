﻿using System;
using System.Configuration;

public class Config {
   private System.Configuration.Configuration config;

   public string this[string Key] {
      get {
         return config.AppSettings.Settings[Key].Value;
      }
      set {
         config.AppSettings.Settings.Remove(Key);
         config.AppSettings.Settings.Add(Key, value);

      }
   }

   public Config() {
      config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
   }

   public void Save() {
      config.Save(ConfigurationSaveMode.Modified);
      ConfigurationManager.RefreshSection("appSettings");
   }
}