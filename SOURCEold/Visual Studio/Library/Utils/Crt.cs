﻿using System;
using System.Windows.Forms;
using System.IO;

public class Crt {
   private System.Windows.Forms.TextBox OutputWindow;
   
   public Crt() {
   }

   public Crt(System.Windows.Forms.TextBox Out) {
      OutputWindow = Out;
   }

   public enum StatusFlag {
      Info,
      Error
   }

   private delegate void Deleg(string State);
   private delegate void Deleg2();

   public void Clear() {
      if(OutputWindow.InvokeRequired) {
         OutputWindow.Invoke(new Deleg2(Clear));
         return;
      }

      OutputWindow.Text = "";
      OutputWindow.Update();
   }

   public void NewLine() {
      Write("\r\n");
   }

   public void WriteLine(string Str) {
      Write(Str + "\r\n");
   }

   protected void ShowStatus(string Str, StatusFlag Flag) {
      Write(Flag.ToString() + ": " + Str + "\r\n");
   }

   public void Info(string Str) {
      ShowStatus(Str, StatusFlag.Info);
   }

   public void Error(string Str) {
      ShowStatus(Str, StatusFlag.Error);
   }

   public string Get() {
      return OutputWindow.Text;
   }

   public void Write(string Str) {
      if(OutputWindow.InvokeRequired) {
         OutputWindow.Invoke(new Deleg(Write), new object[] { Str });
         return;
      }

      OutputWindow.Text += Str;

      OutputWindow.SelectionStart = OutputWindow.Text.Length;
      OutputWindow.ScrollToCaret();
      OutputWindow.Update();
   }
}
