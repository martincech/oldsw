﻿//******************************************************************************
//
//   Ftdi.cs     FTD2XX_NET wrapper - some usefull functions added
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using FTD2XX_NET;
using System.Threading;

namespace Device.Ftxx {

   public class Ftdi : FTDI {
      public static object Locker = new object();

      //***************************** Public ***********************************

      //************************************************************************
      // Constructor
      //************************************************************************

      public Ftdi()
         : base() {
      }

      //************************************************************************
      // Open - arbitrary device type
      //************************************************************************

      public FT_STATUS Open(int Identifier)
         // Open device by <Identifier>
      {
         return Open(Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Open - fixed device type
      //************************************************************************

      public FT_STATUS Open(int Identifier, FT_DEVICE DeviceType)
         // Open device by <Identifier>
      {
         //lock(Locker) {
         if(IsOpen) {
            Close(); // close previous
         }

         FT_STATUS Status;

         // Check text data first (inaccessible after open) :
         uint ftdiDeviceCount = 0;

         Status = GetNumberOfDevices(ref ftdiDeviceCount);

         // Check status
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         if(ftdiDeviceCount == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);
         }

         Status = OpenByIndex((uint)Identifier);

         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Get device type
         FT_DEVICE OpenedDeviceType = FT_DEVICE.FT_DEVICE_UNKNOWN;

         this.GetDeviceType(ref OpenedDeviceType);

         // If neccessary, check device type
         if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
            if(OpenedDeviceType != DeviceType) {
               Close();
               return (FT_STATUS.FT_DEVICE_NOT_FOUND);
            }
         }

         return FT_STATUS.FT_OK;
         //}
      } // Open

      //************************************************************************
      // Locate - arbitrary device type
      //************************************************************************

      public FT_STATUS Locate(string Name, ref int Identifier)
         // Find device by <Name>, returns <Identifier>
      {
         return Locate(Name, ref Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Locate - arbitrary device type
      //************************************************************************

      public FT_STATUS Locate(string Name, string SerialNumber, ref int Identifier)
         // Find device by <Name>, returns <Identifier>
      {
         return Locate(Name, SerialNumber, ref Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************

      public FT_STATUS Locate(string Name, ref int Identifier, FT_DEVICE DeviceType)
         // Find device by <Name>, returns <Identifier>
      {
         //lock(Locker) {
         FT_STATUS Status;

         // Count of FTDI devices :
         uint NumDevices = 0;

         Status = GetNumberOfDevices(ref NumDevices);

         if(Status != FT_STATUS.FT_OK) {
            return (Status);
         }

         if(NumDevices == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);       // no devices
         }

         // Allocate storage for device info list
         FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FT_DEVICE_INFO_NODE[NumDevices + 5];

         Status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Search by device name
         int Count = 0;
         int UsbIndex = 0;
         int Index = -1;

         foreach(FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(Device == null) {
               continue;
            }

            System.Diagnostics.Debug.WriteLine(Device.LocId.ToString());
            System.Diagnostics.Debug.WriteLine("\r\n");

            if(Device.Description.ToString() != Name) {
               continue;
            }

            // If neccessary, check device type
            if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
               if(Device.Type != DeviceType) {
                  continue;
               }
            }

            UsbIndex = Index;
            Count++;
         }

         if(Count == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);  // no device
         }

         if(Count > 1) {
            return (FT_STATUS.FT_OTHER_ERROR);  // more devices
         }

         Identifier = UsbIndex;              // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************

      public FT_STATUS Locate(string Name, string SerialNumber, ref int Identifier, FT_DEVICE DeviceType)
         // Find device by <Name>, returns <Identifier>
      {
         //lock(Locker) {
         FT_STATUS Status;

         // Count of FTDI devices :
         uint NumDevices = 0;

         Status = GetNumberOfDevices(ref NumDevices);

         if(Status != FT_STATUS.FT_OK) {
            return (Status);
         }

         if(NumDevices == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);       // no devices
         }

         // Allocate storage for device info list
         FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FT_DEVICE_INFO_NODE[NumDevices + 5];

         Status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Search by device name
         int Count = 0;
         int UsbIndex = 0;
         int Index = -1;

         foreach(FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(Device == null) {
               continue;
            }

            if(Device.Description != Name && Device.SerialNumber != SerialNumber) {
               continue;
            }

            // If neccessary, check device type
            if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
               if(Device.Type != DeviceType) {
                  continue;
               }
            }

            UsbIndex = Index;
            Count++;
         }

         if(Count == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);  // no device
         }

         if(Count > 1) {
            return (FT_STATUS.FT_OTHER_ERROR);  // more devices
         }

         Identifier = UsbIndex;              // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************
      static int maxid;
      public FT_STATUS Locate(uint LocId, ref int Identifier, FT_DEVICE DeviceType)
         // Find device by <Name>, returns <Identifier>
      {
         int id = maxid++;
         //lock(Locker) {
         FT_STATUS Status;
         System.Diagnostics.Debug.WriteLine("Start " + id.ToString());
         // Count of FTDI devices :
         uint NumDevices = 0;

         Status = GetNumberOfDevices(ref NumDevices);
         System.Diagnostics.Debug.WriteLine("Number " + id.ToString());
         if(Status != FT_STATUS.FT_OK) {
            return (Status);
         }

         if(NumDevices == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);       // no devices
         }

         // Allocate storage for device info list
         FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FT_DEVICE_INFO_NODE[NumDevices + 5];
         System.Diagnostics.Debug.WriteLine("List " + id.ToString());
         Status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Search by device name
         int Count = 0;
         int UsbIndex = 0;
         int Index = -1;

         foreach(FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(Device == null) {
               continue;
            }

            if(Device.LocId != LocId) {
               continue;
            }

            UsbIndex = Index;
            Count++;
         }

         if(Count == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);  // no device
         }

         if(Count > 1) {
            return (FT_STATUS.FT_OTHER_ERROR);  // more devices
         }

         Identifier = UsbIndex;              // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Access - arbitrary device type
      //************************************************************************

      public FT_STATUS Access(int Identifier)
         // Check device access by <Identifier>
      {
         return Access(Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Access - fixed device type
      //************************************************************************

      public FT_STATUS Access(int Identifier, FT_DEVICE DeviceType)
         // Check device access by <Identifier>
      {
         //lock(Locker) {
         FT_STATUS Status = OpenByIndex((uint)Identifier);

         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Get device type
         FT_DEVICE OpenedDeviceType = FT_DEVICE.FT_DEVICE_UNKNOWN;

         this.GetDeviceType(ref OpenedDeviceType);

         // If neccessary, check device type
         if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
            if(OpenedDeviceType != DeviceType) {
               Close();
               return (FT_STATUS.FT_DEVICE_NOT_FOUND);
            }
         }

         return Close();
         //}
      } // Access

      // FTDI wrapper with exclusive access

      new public FT_STATUS GetDeviceList(FT_DEVICE_INFO_NODE[] _ftdiDeviceList) {
         lock(Locker) {
            return base.GetDeviceList(_ftdiDeviceList);
         }
      }

      new public FT_STATUS OpenByIndex(uint _Index) {
         lock(Locker) {
            return base.OpenByIndex(_Index);
         }
      }

      new public FT_STATUS OpenByLocation(uint _Location) {
         lock(Locker) {
            return base.OpenByLocation(_Location);
         }
      }

      new public FT_STATUS OpenByDescription(string _Description) {
         lock(Locker) {
            return base.OpenByDescription(_Description);
         }
      }

      new public FT_STATUS OpenBySerialNumber(string _SerialNumber) {
         lock(Locker) {
            return base.OpenBySerialNumber(_SerialNumber);
         }
      }

      new public FT_STATUS GetNumberOfDevices(ref uint ftdiDeviceCount) {
         lock(Locker) {
            return base.GetNumberOfDevices(ref ftdiDeviceCount);
         }
      }

      new public FT_STATUS Close() {
         lock(Locker) {
            return base.Close();
         }
      }

      new public FT_STATUS GetRxBytesAvailable(ref uint Available) {
         lock(Locker) {
            return base.GetRxBytesAvailable(ref Available);
         }
      }


      new public FT_STATUS Read(byte[] Data, uint Length, ref uint r) {
         //lock(Locker) {
            return base.Read(Data, Length, ref r);
         //}
      }

      new public FT_STATUS Write(byte[] Data, int Length, ref uint w) {
         //lock(Locker) {
            return base.Write(Data, Length, ref w);
         //}
      }

      new public FT_STATUS Purge(uint Mask) {
         lock(Locker) {
            return base.Purge(Mask);
         }
      }

      new public FT_STATUS SetTimeouts(uint Read, uint Write) {
         lock(Locker) {
            return base.SetTimeouts(Read, Write);
         }
      }

      new public FT_STATUS SetBaudRate(uint BaudRate) {
         lock(Locker) {
            return base.SetBaudRate(BaudRate);
         }
      }

      new public FT_STATUS SetDataCharacteristics(byte DataBits, byte StopBits, byte Parity) {
         lock(Locker) {
            return base.SetDataCharacteristics(DataBits, StopBits, Parity);
         }
      }

      new public FT_STATUS SetFlowControl(ushort Handshake, byte XonChar, byte XoffChar) {
         lock(Locker) {
            return base.SetFlowControl(Handshake, XonChar, XoffChar);
         }
      }

      new public bool IsOpen {
         get {
            lock(Locker) {
               return base.IsOpen;
            }
         }
      }

      new public FT_STATUS SetDTR(bool Enable) {
         lock(Locker) {
            return base.SetDTR(Enable);
         }
      }

      new public FT_STATUS SetRTS(bool Enable) {
         lock(Locker) {
            return base.SetRTS(Enable);
         }
      }

      new public FT_STATUS SetBreak(bool Enable) {
         lock(Locker) {
            return base.SetBreak(Enable);
         }
      }

      new public FT_STATUS GetDescription(out string Desc) {
         lock(Locker) {
            return base.GetDescription(out Desc);
         }
      }

      new public FT_STATUS GetSerialNumber(out string SN) {
         lock(Locker) {
            return base.GetSerialNumber(out SN);
         }
      }

      new public FT_STATUS SetLatency(byte Latency) {
         lock(Locker) {
            return base.SetLatency(Latency);
         }
      }

      new public FT_STATUS ReadFT232REEPROM(FTDI.FT232R_EEPROM_STRUCTURE ee) {
         lock(Locker) {
            return base.ReadFT232REEPROM(ee);
         }
      }

      public FT_STATUS WriteEEPROM(FTDI.FT232R_EEPROM_STRUCTURE ee) {
         lock(Locker) {
            return base.WriteFT232REEPROM(ee);
         }
      }
   }
}