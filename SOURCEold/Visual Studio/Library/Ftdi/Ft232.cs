﻿//******************************************************************************
//
//   Ft232.cs     FT232 device
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;

namespace Device.Ftxx {

   public class Ft232 : Ftdi {
      //***************************** Public ***********************************

      public class EEPROM_STRUCTURE : FT232R_EEPROM_STRUCTURE {
         public EEPROM_STRUCTURE()
            : base() {
         }
      }

      //***************************** Public ***********************************

      //************************************************************************
      // Constructor
      //************************************************************************

      public Ft232()
         : base() {
      }

      //************************************************************************
      // Open
      //************************************************************************

      new public FT_STATUS Open(int Identifier, FT_DEVICE DeviceType = FT_DEVICE.FT_DEVICE_232R) {
         return base.Open(Identifier, FT_DEVICE.FT_DEVICE_232R);
      }

      //************************************************************************
      // Locate
      //************************************************************************

      new public FT_STATUS Locate(string Name, ref int Identifier, FT_DEVICE DeviceType = FT_DEVICE.FT_DEVICE_232R) {
         return base.Locate(Name, ref Identifier, FT_DEVICE.FT_DEVICE_232R);
      }

      //************************************************************************
      // Locate
      //************************************************************************

      new public FT_STATUS Locate(string Name, string SerialNumber, ref int Identifier, FT_DEVICE DeviceType = FT_DEVICE.FT_DEVICE_232R) {
         return base.Locate(Name, SerialNumber, ref Identifier, FT_DEVICE.FT_DEVICE_232R);
      }

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************
      new public FT_STATUS Locate(uint LocId, ref int Identifier, FT_DEVICE DeviceType = FT_DEVICE.FT_DEVICE_232R) {
         return base.Locate(LocId, ref Identifier, FT_DEVICE.FT_DEVICE_232R);
      }

      //************************************************************************
      // Access
      //************************************************************************

      public FT_STATUS Access(string Name, ref int Identifier, FT_DEVICE DeviceType = FT_DEVICE.FT_DEVICE_232R) {
         return base.Locate(Name, ref Identifier, FT_DEVICE.FT_DEVICE_232R);
      }

      //************************************************************************
      // ReadEEPROM
      //************************************************************************

      public FT_STATUS ReadEEPROM(EEPROM_STRUCTURE ee) {
         return base.ReadFT232REEPROM(ee);
      }

      //************************************************************************
      // WriteEEPROM
      //************************************************************************

      public FT_STATUS WriteEEPROM(EEPROM_STRUCTURE ee) {
         return base.WriteFT232REEPROM(ee);
      }
   }
}