﻿//******************************************************************************
//
//   Ftdi.cs     FTD2XX_NET wrapper - some usefull functions added
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using FTD2XX_NET;
using System.Threading;

namespace Device.Ftxx {

   public class Ftdi : FTDI {
      //***************************** Public ***********************************

      //************************************************************************
      // Constructor
      //************************************************************************

      public Ftdi()
         : base() {
      }

      //************************************************************************
      // Open - arbitrary device type
      //************************************************************************

      public FT_STATUS Open(int Identifier)
         // Open device by <Identifier>
      {
         return Open(Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Open - fixed device type
      //************************************************************************

      public FT_STATUS Open(int Identifier, FT_DEVICE DeviceType)
         // Open device by <Identifier>
      {
         //lock(Locker) {
         if(IsOpen) {
            Close(); // close previous
         }

         FT_STATUS Status;

         // Check text data first (inaccessible after open) :
         uint ftdiDeviceCount = 0;

         Status = GetNumberOfDevices(ref ftdiDeviceCount);

         // Check status
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         if(ftdiDeviceCount == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);
         }

         Status = OpenByIndex((uint)Identifier);

         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Get device type
         FT_DEVICE OpenedDeviceType = FT_DEVICE.FT_DEVICE_UNKNOWN;

         this.GetDeviceType(ref OpenedDeviceType);

         // If neccessary, check device type
         if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
            if(OpenedDeviceType != DeviceType) {
               Close();
               return (FT_STATUS.FT_DEVICE_NOT_FOUND);
            }
         }

         return FT_STATUS.FT_OK;
         //}
      } // Open

      //************************************************************************
      // Locate - arbitrary device type
      //************************************************************************

      public FT_STATUS Locate(string Name, ref int Identifier)
         // Find device by <Name>, returns <Identifier>
      {
         return Locate(Name, ref Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Locate - arbitrary device type
      //************************************************************************

      public FT_STATUS Locate(string Name, string SerialNumber, ref int Identifier)
         // Find device by <Name>, returns <Identifier>
      {
         return Locate(Name, SerialNumber, ref Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************

      public FT_STATUS Locate(string Name, ref int Identifier, FT_DEVICE DeviceType)
         // Find device by <Name>, returns <Identifier>
      {
         //lock(Locker) {
         FT_STATUS Status;

         // Count of FTDI devices :
         uint NumDevices = 0;

         Status = GetNumberOfDevices(ref NumDevices);

         if(Status != FT_STATUS.FT_OK) {
            return (Status);
         }

         if(NumDevices == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);       // no devices
         }

         // Allocate storage for device info list
         FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FT_DEVICE_INFO_NODE[NumDevices + 5];

         Status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Search by device name
         int Count = 0;
         int UsbIndex = 0;
         int Index = -1;

         foreach(FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(Device == null) {
               continue;
            }

            System.Diagnostics.Debug.WriteLine(Device.LocId.ToString());
            System.Diagnostics.Debug.WriteLine("\r\n");

            if(Device.Description.ToString() != Name) {
               continue;
            }

            // If neccessary, check device type
            if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
               if(Device.Type != DeviceType) {
                  continue;
               }
            }

            UsbIndex = Index;
            Count++;
         }

         if(Count == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);  // no device
         }

         if(Count > 1) {
            return (FT_STATUS.FT_OTHER_ERROR);  // more devices
         }

         Identifier = UsbIndex;              // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************

      public FT_STATUS Locate(string Name, string SerialNumber, ref int Identifier, FT_DEVICE DeviceType)
         // Find device by <Name>, returns <Identifier>
      {
         //lock(Locker) {
         FT_STATUS Status;

         // Count of FTDI devices :
         uint NumDevices = 0;

         Status = GetNumberOfDevices(ref NumDevices);

         if(Status != FT_STATUS.FT_OK) {
            return (Status);
         }

         if(NumDevices == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);       // no devices
         }

         // Allocate storage for device info list
         FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FT_DEVICE_INFO_NODE[NumDevices + 5];

         Status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Search by device name
         int Count = 0;
         int UsbIndex = 0;
         int Index = -1;

         foreach(FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(Device == null) {
               continue;
            }

            if(Device.Description != Name && Device.SerialNumber != SerialNumber) {
               continue;
            }

            // If neccessary, check device type
            if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
               if(Device.Type != DeviceType) {
                  continue;
               }
            }

            UsbIndex = Index;
            Count++;
         }

         if(Count == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);  // no device
         }

         if(Count > 1) {
            return (FT_STATUS.FT_OTHER_ERROR);  // more devices
         }

         Identifier = UsbIndex;              // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Locate - fixed device type
      //************************************************************************
      static int maxid;
      public FT_STATUS Locate(uint LocId, ref int Identifier, FT_DEVICE DeviceType)
         // Find device by <Name>, returns <Identifier>
      {
         int id = maxid++;
         //lock(Locker) {
         FT_STATUS Status;
         System.Diagnostics.Debug.WriteLine("Start " + id.ToString());
         // Count of FTDI devices :
         uint NumDevices = 0;

         Status = GetNumberOfDevices(ref NumDevices);
         System.Diagnostics.Debug.WriteLine("Number " + id.ToString());
         if(Status != FT_STATUS.FT_OK) {
            return (Status);
         }

         if(NumDevices == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);       // no devices
         }

         // Allocate storage for device info list
         FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FT_DEVICE_INFO_NODE[NumDevices + 5];
         System.Diagnostics.Debug.WriteLine("List " + id.ToString());
         Status = GetDeviceList(ftdiDeviceList);

         // Populate our device list
         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Search by device name
         int Count = 0;
         int UsbIndex = 0;
         int Index = -1;

         foreach(FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(Device == null) {
               continue;
            }

            if(Device.LocId != LocId) {
               continue;
            }

            UsbIndex = Index;
            Count++;
         }

         if(Count == 0) {
            return (FT_STATUS.FT_DEVICE_NOT_FOUND);  // no device
         }

         if(Count > 1) {
            return (FT_STATUS.FT_OTHER_ERROR);  // more devices
         }

         Identifier = UsbIndex;              // one device only

         return (FT_STATUS.FT_OK);
         //}
      } // Locate

      //************************************************************************
      // Access - arbitrary device type
      //************************************************************************

      public FT_STATUS Access(int Identifier)
         // Check device access by <Identifier>
      {
         return Access(Identifier, FT_DEVICE.FT_DEVICE_UNKNOWN);
      }

      //************************************************************************
      // Access - fixed device type
      //************************************************************************

      public FT_STATUS Access(int Identifier, FT_DEVICE DeviceType)
         // Check device access by <Identifier>
      {
         //lock(Locker) {
         FT_STATUS Status = OpenByIndex((uint)Identifier);

         if(Status != FTDI.FT_STATUS.FT_OK) {
            return (Status);
         }

         // Get device type
         FT_DEVICE OpenedDeviceType = FT_DEVICE.FT_DEVICE_UNKNOWN;

         this.GetDeviceType(ref OpenedDeviceType);

         // If neccessary, check device type
         if(DeviceType != FT_DEVICE.FT_DEVICE_UNKNOWN) {
            if(OpenedDeviceType != DeviceType) {
               Close();
               return (FT_STATUS.FT_DEVICE_NOT_FOUND);
            }
         }

         return Close();
         //}
      } // Access
   }
}