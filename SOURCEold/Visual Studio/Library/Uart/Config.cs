﻿//******************************************************************************
//
//   Config.cs     Uart Configuration structure
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.Configuration;

namespace Device.Uart.Config {
   public class UsbLocUartSettings : UartSettings {

      /// <summary>
      /// DeviceName</summary>

      [ConfigurationProperty("LocId", IsRequired = true)]
      public uint LocId {
         get {
            return (uint)this["LocId"];
         }
         set {
            this["LocId"] = value;
         }
      }
   }

   public class UsbUartSettings : UartSettings {

      /// <summary>
      /// DeviceName</summary>

      [ConfigurationProperty("DeviceName", IsRequired = true)]
      public string DeviceName {
         get {
            return (string)this["DeviceName"];
         }
         set {
            this["DeviceName"] = value;
         }
      }
   }

   public class ComUartSettings : UartSettings {

      /// <summary>
      /// PortName</summary>
      
      [ConfigurationProperty("PortName", IsRequired = true)]
      public string PortName {
         get {
            return (string)this["PortName"];
         }
         set {
            this["PortName"] = value;
         }
      }
   }

   /// <summary>
   /// BaudRate</summary>

   public class UartSettings : ConfigurationElement {
      [ConfigurationProperty("BaudRate", IsRequired = true)]
      public Int32 BaudRate {
         get {
            return (Int32)this["BaudRate"];
         }
         set {
            this["BaudRate"] = value;
         }
      }

      /// <summary>
      /// StopBit</summary>

      [ConfigurationProperty("StopBit", IsRequired = true)]
      public Uart.StopBit StopBit {
         get {
            return (Uart.StopBit)this["StopBit"];
         }
         set {
            this["StopBit"] = value;
         }
      }

      /// <summary>
      /// Parity</summary>

      [ConfigurationProperty("Parity", IsRequired = true)]
      public Uart.Parity Parity {
         get {
            return (Uart.Parity)this["Parity"];
         }
         set {
            this["Parity"] = value;
         }
      }

      /// <summary>
      /// Handshake</summary>

      [ConfigurationProperty("Handshake", IsRequired = true)]
      public Uart.Handshake Handshake {
         get {
            return (Uart.Handshake)this["Handshake"];
         }
         set {
            this["Handshake"] = value;
         }
      }

      /// <summary>
      /// Bits</summary>

      [ConfigurationProperty("Bits", IsRequired = true)]
      public Int32 Bits {
         get {
            return (Int32)this["Bits"];
         }
         set {
            this["Bits"] = value;
         }
      }
   }
}