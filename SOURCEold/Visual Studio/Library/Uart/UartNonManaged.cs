﻿using System;

namespace Device.Uart {
   public abstract class UartNonManaged
   {
      public UartNonManaged()
	   {
	   }

      //***************************** Private **********************************

      private bool _Dtr = false;
      private bool _Rts = false;
      private bool _Txd = false;

      public bool IsOpen {
         get {
            return _IsOpen();
         }
      }

      public bool Dtr {
         get {
            return _Dtr;
         }
         set {
            _Dtr = !value;
            SetDtr(!value);
         }
      }

      public bool Rts {
         get {
            return _Rts;
         }
         set {
            _Rts = !value;
            SetRts(!value);
         }
      }

      public bool Txd {
         get {
            return _Txd;
         }
         set {
            _Txd = !value;
            SetTxd(!value);
         }
      }

      public abstract int Write(byte[] Data, int Length);
      public abstract int Read(byte[] Data, int Length);
      public abstract void Flush();
      public abstract void SetRxNowait();
      public abstract void SetRxWait(int Timeout);


      //**************************** Protected *********************************
      
      protected abstract bool _IsOpen();
      protected abstract void SetDtr(bool State);
      protected abstract void SetRts(bool State);
      protected abstract void SetTxd(bool State);
      
   }
}
