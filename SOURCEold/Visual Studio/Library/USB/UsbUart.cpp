//******************************************************************************
//
//   UsbUart.cpp  FTDI USB module
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <Windows.h>
#include <stdio.h>
#include <string.h>

#include "../Unisys/Uni.h"
#include "UsbUart.h"

// Check API return value :
#define if_fterr( status)  if( (status) != FT_OK)

#define DEFAULT_LATENCY 16          // default latency is 16ms
#define INVALID_INDEX   0xFFFFFFFF  // invalid device index

//******************************************************************************
// Constructor
//******************************************************************************

TUsbUart::TUsbUart()
// Constructor
{
   IsOpen                = NO;
   TxLatency             = DEFAULT_LATENCY;

   FParameters.BaudRate  = 9600;
   FParameters.DataBits  = 8;
   FParameters.StopBits  = 10;
   FParameters.Parity    = NO_PARITY;
   FParameters.Handshake = NO_HANDSHAKE;
   FHandle               = INVALID_HANDLE_VALUE;
   FImmediateRead        = YES;
} // TUsbUart

//******************************************************************************
// Destructor
//******************************************************************************

TUsbUart::~TUsbUart()
// Destructor
{
   Close();
} // ~TUsbUart

//******************************************************************************
// Access
//******************************************************************************

BOOL TUsbUart::Access( char *Name)
// Check for device <Name> present
{
DWORD DeviceIndex;

   DeviceIndex = Locate( Name);
   if( DeviceIndex == INVALID_INDEX){
      return( FALSE);
   }
   return( TRUE);
} // Access

//******************************************************************************
// Open
//******************************************************************************

BOOL TUsbUart::Open( char *Name)
// Open device by <Name>
{
DWORD DeviceIndex;

   if( IsOpen){
      Close();                         // close previous
   }
   DeviceIndex = Locate( Name);
   if( DeviceIndex == INVALID_INDEX){
      return( FALSE);
   }
   if_fterr( FT_Open( DeviceIndex, &FHandle)){
      return( FALSE);
   }
   FT_SetLatencyTimer( FHandle, TxLatency);    // device latency
   IsOpen = TRUE;
   return( TRUE);
} // Open

//******************************************************************************
// Open
//******************************************************************************

BOOL TUsbUart::Open( char *Name, char* SerialNumber)
// Open device by <Name>
{
DWORD DeviceIndex;

   if( IsOpen){
      Close();                         // close previous
   }
   DeviceIndex = Locate( Name, SerialNumber);
   if( DeviceIndex == INVALID_INDEX){
      return( FALSE);
   }
   if_fterr( FT_Open( DeviceIndex, &FHandle)){
      return( FALSE);
   }
   FT_SetLatencyTimer( FHandle, TxLatency);    // device latency
   IsOpen = TRUE;
   return( TRUE);
} // Open

//******************************************************************************
// Open
//******************************************************************************

BOOL TUsbUart::Open( unsigned int LocId)
// Open device by <Name>
{
DWORD DeviceIndex;

   if( IsOpen){
      Close();                         // close previous
   }
   DeviceIndex = Locate( LocId);
   if( DeviceIndex == INVALID_INDEX){
      return( FALSE);
   }

   if_fterr( FT_Open( DeviceIndex, &FHandle)){
      return( FALSE);
   }
   FT_SetLatencyTimer( FHandle, TxLatency);    // device latency
   IsOpen = TRUE;
   return( TRUE);
} // Open

//******************************************************************************
// Close
//******************************************************************************

void TUsbUart::Close()
// Close device
{
   if( IsOpen){
      FT_W32_CloseHandle( FHandle);
   }
   FHandle     = INVALID_HANDLE_VALUE;
   IsOpen      = FALSE;
} // Close

//******************************************************************************
// Write
//******************************************************************************

int TUsbUart::Write( void *Data, int Length)
// Write <Data> of size <Length>, returns written length (or 0 if error)
{
   if( !IsOpen){
      return( 0);
   }
   DWORD w;
   if( !FT_W32_WriteFile( FHandle, Data, Length, &w, NULL)){
      return( 0);   // timeout or error
   }
   return( w);
} // Write

//******************************************************************************
// Read
//******************************************************************************

int TUsbUart::Read( void *Data, int Length)
// Read data <Data> of size <Length>, returns true length (or 0 if error)
{
   if( !IsOpen){
      return( 0);
   }
   DWORD r;
   if( FImmediateRead){
      // RxNowait
      DWORD Available;
      if_fterr( FT_GetQueueStatus( FHandle, &Available)){
         return( 0);
      }
      if( Available == 0){
         return( 0);                   // queue empty
      }
      if( Available < (DWORD)Length){
         Length = Available;           // short read
      }
      if( !FT_W32_ReadFile( FHandle, Data, Length, &r, NULL)){
         return( 0);
      }
      return( r);
   }
   // RxWait, wait for first character :
   if( !FT_W32_ReadFile( FHandle, Data, 1, &r, NULL)){
      return( 0);
   }
   if( r != 1){
      return( 0);
   }
   if( Length == 1){
      return( 1);            // requested 1 byte only
   }
   // read remainder :
   Length--;
   byte *p = (byte *)Data;
   p++;
   if( !FT_W32_ReadFile( FHandle, p, Length, &r, NULL)){
      return( 0);
   }
   return( r+1);
} // Read

//******************************************************************************
// Flush
//******************************************************************************

void TUsbUart::Flush()
// Make input/output queue empty
{
   if( !IsOpen){
      return;
   }
   // Wait for data :
   for( int i = 0; i < 10; i++){
      FT_Purge( FHandle, FT_PURGE_RX | FT_PURGE_TX);
      DWORD Available;
      if_fterr( FT_GetQueueStatus( FHandle, &Available)){
         return;
      }
      if( Available == 0){
         return;
      }
      Sleep( 50);
   }
} // Flush

//******************************************************************************
// Set Rx Timing
//******************************************************************************

void TUsbUart::SetRxNowait()
// Set timing of receiver - return collected data immediately
{
   if( !IsOpen){
      return;
   }
   FImmediateRead = TRUE;
   FTTIMEOUTS  to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   to.ReadIntervalTimeout         =  MAXDWORD; // without waiting
   to.ReadTotalTimeoutConstant    =  0;
   to.ReadTotalTimeoutMultiplier  =  0;
   FT_W32_SetCommTimeouts( FHandle,&to);
} // SetRxNowait

void TUsbUart::SetRxWait( int TotalTime, int IntercharacterTime)
// Set timing of receiver - waits for first character up to <TotalTime>
// returns after <IntercharacterTime> break
{
   if( !IsOpen){
      return;
   }
   if( TotalTime == 0 && IntercharacterTime == 0){
      return;
   }
   FImmediateRead = FALSE;
   if( TotalTime <= IntercharacterTime){
      // too short for Windows OS
      TotalTime = 20 * IntercharacterTime;     // patch it
   }
   FTTIMEOUTS  to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   to.ReadIntervalTimeout         =  0;
   to.ReadTotalTimeoutMultiplier  =  IntercharacterTime;
   to.ReadTotalTimeoutConstant    =  TotalTime;
   FT_W32_SetCommTimeouts( FHandle,&to);
} // SetRxWait

//******************************************************************************
// Set Parameters
//******************************************************************************

BOOL  TUsbUart::SetParameters( const TParameters &Parameters)
// Set parameters of communication
{
   if( !IsOpen){
      return( FALSE);
   }
   if_fterr( FT_SetBaudRate( FHandle, Parameters.BaudRate)){
      return( FALSE);
   }
   // Translate Byte size :
   UCHAR WordBits;
   switch( Parameters.DataBits){
      case 7 :
         WordBits = FT_BITS_7;
         break;
      case 8 :
         WordBits = FT_BITS_8;
         break;
      default :
         return( FALSE); // invalid size
   }
   // Translate Stop Bits :
   UCHAR StopBits;
   switch( Parameters.StopBits){
      case 10 :
         StopBits = FT_STOP_BITS_1;
         break;
      case 20 :
         StopBits = FT_STOP_BITS_2;
         break;
      default :
         return( FALSE);  // invalid stop bits count
   }
   // Translate Parity :
   UCHAR Parity;
   switch( Parameters.Parity){
      case NO_PARITY :
         Parity = FT_PARITY_NONE;
         break;
      case ODD_PARITY :
         Parity = FT_PARITY_ODD;
         break;
      case EVEN_PARITY :
         Parity = FT_PARITY_EVEN;
         break;
      case MARK_PARITY :
         Parity = FT_PARITY_MARK;
         break;
      case SPACE_PARITY :
         Parity = FT_PARITY_SPACE;
         break;
      IDEFAULT
   }
   if_fterr( FT_SetDataCharacteristics( FHandle, WordBits, StopBits, Parity)){
      return( FALSE);
   }
   USHORT FlowControl;
   UCHAR  XonChar  = 11;
   UCHAR  XoffChar = 13;
   switch( Parameters.Handshake){
      case NO_HANDSHAKE :
         FlowControl = FT_FLOW_NONE;
         break;
      case HARDWARE_HANDSHAKE :
         FlowControl = FT_FLOW_RTS_CTS;
         break;
      case XON_XOFF_HANDSHAKE :
         FlowControl = FT_FLOW_XON_XOFF;
         break;
      case HALF_DUPLEX_HANDSHAKE :
         FlowControl = FT_FLOW_NONE;
         break;
      IDEFAULT
   }
   if_fterr( FT_SetFlowControl( FHandle, FlowControl, XonChar, XoffChar)){
      return( FALSE);
   }
   FParameters = Parameters;   // remember
   // Safe timimg :
   SafeTxTiming();
   SetRxNowait();
   return( TRUE);
} // SetParameters

//******************************************************************************
// Get Parameters
//******************************************************************************

void TUsbUart::GetParameters( TParameters &Parameters)
// Get parameters of communication
{
   Parameters = FParameters;
} // GetParameters

//******************************************************************************
// Property CTS
//******************************************************************************

BOOL TUsbUart::GetCTS()
{
   return( GetModemStatus() & MS_CTS_ON);
} // GetCTS

//******************************************************************************
// Property DSR
//******************************************************************************

BOOL TUsbUart::GetDSR()
{
   return( GetModemStatus() & MS_DSR_ON);
} // GetDSR

//******************************************************************************
// Property RI
//******************************************************************************

BOOL TUsbUart::GetRI()
{
   return( GetModemStatus() & MS_RING_ON);
} // GetRI

//******************************************************************************
// Property DCD
//******************************************************************************

BOOL TUsbUart::GetDCD()
{
   return( GetModemStatus() & MS_RLSD_ON);
} // GetDCD

//******************************************************************************
// Property DTR
//******************************************************************************

void TUsbUart::SetDTR( BOOL Status)
{

   if( !IsOpen){
      IERROR;
   }
   if( Status){
      FT_SetDtr( FHandle);
   } else {
      FT_ClrDtr( FHandle);
   }
} // SetDTR

//******************************************************************************
// Property RTS
//******************************************************************************

void TUsbUart::SetRTS( BOOL Status)
{
   if( !IsOpen){
      IERROR;
   }
   if( Status){
      FT_SetRts( FHandle);
   } else {
      FT_ClrRts( FHandle);
   }
} // SetRTS

//******************************************************************************
// Property TxD
//******************************************************************************

void TUsbUart::SetTxD( BOOL Status)
{
   if( !IsOpen){
      IERROR;
   }
   if( Status){
      FT_SetBreakOn( FHandle);
   } else {
      FT_SetBreakOff( FHandle);
   }
} // SetTxD


//-----------------------------------------------------------------------------

//******************************************************************************
// Locator
//******************************************************************************

DWORD TUsbUart::Locate( char *Name)
// Find device by <Name>, returns device index or INVALID_INDEX
{
   // Count of FTDI devices :
   DWORD      NumDevices;
   if_fterr( FT_ListDevices( &NumDevices, NULL, FT_LIST_NUMBER_ONLY)){
      return( FALSE);                  // unable get
   }
   if( NumDevices == 0){
      return( FALSE);                  // no devices
   }

   // Search by device name
   int   Count    = 0;                 // count of devices found
   char  DeviceName[ 256];             // device name
   DWORD UsbIndex = -1;                // index of last device
   for( DWORD i = 0; i < NumDevices; i++){
      if_fterr( FT_ListDevices( (PVOID)i, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
         continue;
      }
      if( !strequ( Name, DeviceName)){
         continue;
      }
      Count++;
      UsbIndex = i;
   }
   if( Count == 0){
      return( INVALID_INDEX);          // no device
   }
   if( Count > 1){
      return( INVALID_INDEX);          // more devices
   }
   return( UsbIndex);
} // Locate

//******************************************************************************
// Locator
//******************************************************************************

DWORD TUsbUart::Locate( char *Name, char *Number)
// Find device by <Name>, returns device index or INVALID_INDEX
{
   // Count of FTDI devices :
   DWORD      NumDevices;
   if_fterr( FT_ListDevices( &NumDevices, NULL, FT_LIST_NUMBER_ONLY)){
      return( FALSE);                  // unable get
   }
   if( NumDevices == 0){
      return( FALSE);                  // no devices
   }

   // Search by device name
   int   Count    = 0;                 // count of devices found
   char  DeviceName[ 256];             // device name
   char  SerialNumber[ 20];             // serial number
   DWORD UsbIndex = -1;                // index of last device
   for( DWORD i = 0; i < NumDevices; i++){
      if_fterr( FT_ListDevices( (PVOID)i, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
         continue;
      }
      if( !strequ( Name, DeviceName)){
         continue;
      }
     
      if_fterr( FT_ListDevices( (PVOID)i, SerialNumber, FT_LIST_BY_INDEX | FT_OPEN_BY_SERIAL_NUMBER)){
         continue;
      }
      if( !strequ( Number, SerialNumber)){
         continue;
      }

      Count++;
      UsbIndex = i;
   }
   if( Count == 0){
      return( INVALID_INDEX);          // no device
   }
   if( Count > 1){
      return( INVALID_INDEX);          // more devices
   }
   return( UsbIndex);
} // Locate

//******************************************************************************
// Locator
//******************************************************************************

DWORD TUsbUart::Locate( unsigned int LocId)
// Find device by <Name>, returns device index or INVALID_INDEX
{
   // Count of FTDI devices :
   DWORD      NumDevices;
   if_fterr( FT_ListDevices( &NumDevices, NULL, FT_LIST_NUMBER_ONLY)){
      return( FALSE);                  // unable get
   }
   if( NumDevices == 0){
      return( FALSE);                  // no devices
   }

   // Search by device name
   int   Count    = 0;                 // count of devices found
   unsigned int Loc;
   DWORD UsbIndex = -1;                // index of last device
   for( DWORD i = 0; i < NumDevices; i++){
      if_fterr( FT_ListDevices( (PVOID)i, &Loc, FT_LIST_BY_INDEX | FT_OPEN_BY_LOCATION)){
         continue;
      }
      if( Loc != LocId){
         continue;
      }

      Count++;
      UsbIndex = i;
   }
   if( Count == 0){
      return( INVALID_INDEX);          // no device
   }
   if( Count > 1){
      return( INVALID_INDEX);          // more devices
   }
   return( UsbIndex);
} // Locate

//******************************************************************************
// Tx timing
//******************************************************************************

void  TUsbUart::SafeTxTiming()
// Set safe timing of transmitter
{
   if( !IsOpen){
      return;
   }
   FTTIMEOUTS to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   // Calculate via us :
   int CharTime;
   if( FParameters.BaudRate > 0 && FParameters.DataBits > 0){
      CharTime = (1000000 / FParameters.BaudRate) * FParameters.DataBits + 3;
      if( CharTime < 1){
         CharTime = 1;
      }
   } else {
      CharTime = 1;
   }
   // convert to ms :
   CharTime /= 1000;
   if( CharTime <= 0){
      CharTime = 1;
   }
   CharTime *= 2;   // security multiplier

   // maximum time for send :
   to.WriteTotalTimeoutMultiplier = CharTime;
   to.WriteTotalTimeoutConstant   = 100;
   FT_W32_SetCommTimeouts( FHandle, &to);
} // SafeTxTiming

//******************************************************************************
// Modem Status
//******************************************************************************

DWORD TUsbUart::GetModemStatus()
// Read modem status
{
   if( !IsOpen){
      return( 0);
   }
   DWORD d = 0;
   if( !FT_W32_GetCommModemStatus( FHandle, &d)){
      return( 0);
   }
   //
   return( d);
} // GetModemStatus

