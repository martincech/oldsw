﻿//******************************************************************************
//
//   ElexolUsbIO.cs     Elexol 24 I/O board
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Uart;
using System.Diagnostics;

namespace Device.UsbIO {

   public class ElexolUsbIO {

      public class TPorts {
      
         public class TPort {

            public class TPin {
               private TPort _Port; // owner
               private int _Number;

               //************************** Public *****************************

               /// <summary>
               /// Default contructor</summary>
               /// <param name="Num">Pin number</param>
               /// <param name="Port">Port</param>

               public TPin(int Num, TPort Port) {
                  _Port = Port;
                  _Number = Num;
               }

               /// <summary>
               /// Set input</summary>

               public bool SetInput() {
                  return SetIO(PinDirection.Input);
               }

               /// <summary>
               /// Set output</summary>

               public bool SetOutput() {
                  return SetIO(PinDirection.Output);
               }

               /// <summary>
               /// Set input/output</summary>

               public bool SetIO(PinDirection Dir) {
                  return _Port.SetPinIO(_Number, Dir);
               }

               /// <summary>
               /// Get state</summary>

               public bool Read(ref bool State) {
                  if(!_Port.ReadPin(_Number, ref State)) {
                     return false;
                  }

                  return true;
               }

               /// <summary>
               /// Set pin</summary>

               public bool Set() {
                  return Set(true);
               }

               /// <summary>
               /// Clear pin</summary>

               public bool Clear() {
                  return Set(false);
               }

               /// <summary>
               /// Set/clear pin</summary>

               public bool Set(bool State) {
                  return _Port.SetPin(_Number, State);
               }
            } // TPin

            //*************************** Public *******************************

            public TPin[] Pin = new TPin[PinsPerPort];

            //************************** Protected *****************************

            protected ElexolUsbIO Board; // owner
            protected byte[] WriteCommand;
            protected byte[] ReadCommand;
            protected byte[] IOCommand;

            // there is no way to read port direction
            // current port direction have to be saved
            protected TPortDirection PortDirection;
            
            //*************************** Private ******************************

            private static System.Text.ASCIIEncoding Enc = new System.Text.ASCIIEncoding();

            //*************************** Public *******************************

            //******************************************************************
            // Constructor
            //******************************************************************

            public TPort(string WC, string RC, string IOC, ElexolUsbIO Brd) {
               WriteCommand = Enc.GetBytes(WC);
               ReadCommand = Enc.GetBytes(RC);
               IOCommand = Enc.GetBytes(IOC);

               Board = Brd;

               PortDirection = new TPortDirection();

               PortDirection[0] = PinDirection.Input;
               PortDirection[1] = PinDirection.Input;
               PortDirection[2] = PinDirection.Input;
               PortDirection[3] = PinDirection.Input;
               PortDirection[4] = PinDirection.Input;
               PortDirection[5] = PinDirection.Input;
               PortDirection[6] = PinDirection.Input;
               PortDirection[7] = PinDirection.Input;

               for(int i = 0 ; i < PinsPerPort ; i++) {
                  Pin[i] = new TPin(i, this);
               }
            }

            //******************************************************************
            // Write
            //******************************************************************

            public bool Write(byte PortData) {
               byte[] Data = WriteCommand;
               Array.Resize(ref Data, Data.Length + 1);
               Data[Data.Length - 1] = PortData;

               if(!Board.Write(Data)) {
                  return false;
               }

               return true;
            }

            //******************************************************************
            // Read
            //******************************************************************

            public bool Read(ref byte PortData) {
               if(!Board.Write(ReadCommand)) {
                  return false;
               }

               if(!Board.Read(ref PortData)) {
                  return false;
               }

               return true;
            }

            //******************************************************************
            // SetIO
            //******************************************************************

            public bool SetIO(TPortDirection Dir) {
               byte Direction = 0;

               for(int i = 0; i < PinsPerPort; i++) {
                  switch(Dir[i]) {
                     case PinDirection.Input:
                        if(PinInputValue) {
                           Direction |= (byte)(1 << i);
                        }
                        break;

                     case PinDirection.Output:
                        if(!PinInputValue) {
                           Direction |= (byte)(1 << i);
                        }
                        break;
                  }
               }


               byte[] Data = IOCommand;
               Array.Resize(ref Data, Data.Length + 1);
               Data[Data.Length - 1] = Direction;

               if(!Board.Write(Data)) {
                  return false;
               }

               PortDirection = Dir;

               return true;
            }

            //************************** Protected *****************************

            //******************************************************************
            // SetPin
            //******************************************************************

            protected bool SetPin(int Number, bool State) {
               byte PortData = 0;

               if(!Read(ref PortData)) {
                  return false;
               }

               if(State) {
                  PortData |= (byte)(1 << Number);
               } else {
                  PortData &= (byte)~(1 << Number);
               }

               if(!Write(PortData)) {
                  return false;
               }

               return true;
            }

            //******************************************************************
            // ReadPin
            //******************************************************************

            protected bool ReadPin(int Number, ref bool State) {
               byte PortData = 0;

               if(!Read(ref PortData)) {
                  return false;
               }

               State = (PortData & (byte)(1 << Number)) != 0 ? true : false;

               return true;
            }

            //******************************************************************
            // SetPinIO
            //******************************************************************

            protected bool SetPinIO(int Number, PinDirection Dir) {
               TPortDirection Direction = PortDirection;

               Direction[Number] = Dir;

               if(!SetIO(PortDirection)) {
                  return false;
               }

               return true;
            }

         } // TPort

         //***************************** Public ********************************

         public const int PinsPerPort = 8;

         public struct TPortDirection {
            PinDirection[] PinDir;

            public PinDirection this[int index] {
               get {
                  if(PinDir == null) {
                     Create();
                  }

                  return PinDir[index];
               }
               set {
                  if(PinDir == null) {
                     Create();
                  }

                  PinDir[index] = value;
               }
            }

            private void Create() {
               PinDir = new PinDirection[PinsPerPort];

               for(int i = 0 ; i < PinsPerPort; i++) {
                  PinDir[i] = PinDirection.Input;
               }
            }
         }

         public TPort this[string index] {
            get {
               switch(index) {
                  case "A":
                     return Ports[0];

                  case "B":
                     return Ports[1];

                  case "C":
                  default:
                     return Ports[2];
               }
            }
         }

         //***************************** Private *******************************

         private TPort[] Ports;

         //***************************** Public ********************************

         //********************************************************************
         // Constructor
         //********************************************************************

         public TPorts(int Count, string[,] Cmds, ElexolUsbIO Brd) {
            Ports = new TPort[Count];

            for(int i = 0; i < Count; i++) {
               Ports[i] = new TPort(Cmds[i, 0], Cmds[i, 1], Cmds[i, 2], Brd);
            }
         }

      } // TPorts

      //***************************** Public ***********************************

      public TPorts Port;

      public const int PortsPerBoard = 3;

      // Direction
      private const bool PinInputValue = true;

      private UsbUart Com = new UsbUart();

      private const int RX_TIMEOUT = 1000;
      
      /// <summary>
      /// IsOpen</summary>

      public bool IsOpen {
         get {
            return Com.IsOpen;
         }
      }

      /// <summary>
      /// Pin direction enum</summary>

      public enum PinDirection {
         Input,
         Output
      }

      /// <summary>
      /// Default contructor</summary>

      public ElexolUsbIO() {
         Port = new TPorts(PortsPerBoard, new string[,] { { "A", "a", "!A" }, { "B", "b", "!B" }, { "C", "c", "!C" } }, this);
      }

      /// <summary>
      /// Open</summary>

      public bool Open(Uart.Config.UartSettings ComSettings) {
         if(IsOpen) {
            return true;
         }

         int Identifier = 0;

         if(ComSettings.GetType() != typeof(Uart.Config.UsbUartSettings)) {
            return false;
         }

         Uart.Config.UsbUartSettings Settings = (Uart.Config.UsbUartSettings)ComSettings;

         if(!Com.Locate(Settings.DeviceName, ref Identifier)) {
            Trace.WriteLine("I/O Board Open: Failed to locate");
            return false;
         }

         if(!Com.Open(Identifier)) {
            Trace.WriteLine("I/O Board Open: Failed to open");
            return false;
         }

         // ať nastavím co chci, komunikace vždy funguje
         if(!Com.SetParameters(Settings.BaudRate, Settings.Parity, Settings.Bits, Settings.StopBit, Settings.Handshake)) {
            Trace.WriteLine("I/O Board Open: Failed to set parameters");
            return false;
         }

         Com.SetRxWait(RX_TIMEOUT);

         return true;
      }

      /// <summary>
      /// Close</summary>

      public bool Close() {
         if(!IsOpen) {
            return true;
         }

         Com.Close();

         return true;
      }

      /// <summary>
      /// Write</summary>

      private bool Write(byte[] Data) {
         if(!IsOpen) {
            Trace.WriteLine("I/O Board Write: Not open");
            return false;
         }

         if(Com.Write(Data, Data.Length) != Data.Length) {
            Trace.WriteLine("I/O Board Write: Can't write");
            return false;
         }

         return true;
      }

      /// <summary>
      /// Read</summary>

      private bool Read(ref byte Data) {
         if(!IsOpen) {
            Trace.WriteLine("I/O Board Read: Not open");
            return false;
         }

         byte[] DataArr = new byte[1];

         if(Com.Read(DataArr, 1) != 1) {
            Trace.WriteLine("I/O Board Write: Can't read");
            return false;
         }

         Data = DataArr[0];

         return true;
      }
   }
}