//******************************************************************************
//
//   ConsoleLogger.cpp  Raw data viewer
//   Version 1.0        (c) VymOs
//
//******************************************************************************

#include <windows.h>
#include <stdio.h>

#include "../../Library/Unisys/uni.h"

#include "ConsoleLogger.h"

#define BinaryColumns      16      // binary dump - numbers/line
#define TextColumns        32      // text dump   - charcters/line
#define MixedColumns       16      // mixed dump  - numeber & characters / line
#define UNKNOWN_CHARACTER  '.'     // nonprintable character

//---------------------------------------------------------------------------

TConsoleLogger *Logger;    // global pointer

//******************************************************************************
// Constructor
//******************************************************************************

TConsoleLogger::TConsoleLogger()
// Constructor
{
   Mode      = BINARY;
   Logger    = this;
} // TConsoleLogger

//******************************************************************************
// Write data
//******************************************************************************

void TConsoleLogger::Write( TDataType Type, void *Buffer, int Length)
// Write data
{
   if( Length <= 0){
      return;
   }
   if( !Buffer){
      IERROR;
   }
   switch( Type){
      case UNKNOWN :
         puts( "?? :");
         break;
      case RX_DATA :
         puts( "RX :");
         break;
      case TX_DATA :
         puts( "TX :");
         break;
      case RX_GARBAGE :
         puts( "RX GARBAGE :");
         break;
   }
   switch( Mode){
      case BINARY :
         BinaryWrite( (char *)Buffer, Length);
         break;
      case TEXT :
         TextWrite( (char *)Buffer, Length);
         break;
      case MIXED :
         MixedWrite( (char *)Buffer, Length);
         break;
      IDEFAULT
   }
} // Write

//******************************************************************************
// Write report
//******************************************************************************

void TConsoleLogger::Report( char *Format, ...)
// Write report
{
va_list Arg;

   va_start( Arg, Format);
   vprintf( Format, Arg);
} // Report

//******************************************************************************
// Binary dump
//******************************************************************************

void TConsoleLogger::BinaryWrite( char *Buffer, int Length)
// Binary dump
{
   int i = 0;
   while( 1){
      for( int j = 0; j < BinaryColumns; j++){
         printf( "%02X ", (unsigned char)Buffer[ i++]);
         if( i >= Length){
            goto next;
         }
      }
      printf( "\n");
   }
   next :
   printf( "\n");
} // BinaryWrite

//******************************************************************************
// Text dump
//******************************************************************************

void TConsoleLogger::TextWrite( char *Buffer, int Length)
// Text dump
{
char  Temp[ 128];

   int i = 0;
   while( 1){
      memset( Temp, 0, sizeof( Temp));
      for( int j = 0; j < TextColumns; j++){
         Temp[ j] = Buffer[ i++];
         if( i >= Length){
            goto next;
         }
      }
      TextLineWrite( Temp);
   }
   next :
   TextLineWrite( Temp);
} // TextWrite

//******************************************************************************
// Mixed dump
//******************************************************************************

void TConsoleLogger::MixedWrite( char *Buffer, int Length)
// Mixed dump
{
char  Temp[ 128];
int   LineLength;

   int i = 0;
   while( 1){
      memset( Temp, 0, sizeof( Temp));
      for( int j = 0; j < MixedColumns; j++){
         Temp[ j] = Buffer[ i++];
         if( i >= Length){
            LineLength = j + 1;
            goto next;
         }
      }
      MixedLineWrite( Temp, MixedColumns);
   }
   next :
   MixedLineWrite( Temp, LineLength);
} // MixedWrite

//******************************************************************************
// Text line
//******************************************************************************

void TConsoleLogger::TextLineWrite( char *Line)
// Write text line
{
   for( int i = 0; i < TextColumns; i++){
      if( Line[ i] == 0){
         Line[ i] = ' ';
      } else if( !isprint( Line[ i])){
         Line[ i] = UNKNOWN_CHARACTER;
      }
   }
   Line[ TextColumns] = '\0';
   puts( Line);
} // TextLineWrite

//******************************************************************************
// Write mixed line
//******************************************************************************

void TConsoleLogger::MixedLineWrite( char *Line, int LineLength)
// Write mixed line
{
   for( int i = 0; i < LineLength; i++){
      printf( "%02X ", (unsigned char)Line[ i]);
   }
   for( int i = 0; i < MixedColumns - LineLength; i++){
      printf( "   ");
   }
   printf( "   |   ");
   for( int i = 0; i < MixedColumns; i++){
      if( Line[ i] == 0){
         Line[ i] = ' ';
      } else if( !isprint( Line[ i])){
         Line[ i] = UNKNOWN_CHARACTER;
      }
   }
   Line[ MixedColumns] = '\0';
   printf( "%s\n", Line);
} // MixedLineWrite

