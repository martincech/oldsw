Poznamky k USB HID Test
-----------------------

Testovani knihovny Atmel AtUsbHid.dll
Volani pomoci LoadLibrary jak je uvedeno
v dokumentaci je komplikovane. Bylo upraveno
na prime volani funkci z AtUsbHid.dll.
Ke knihovne neni k dispozici AtUsbHid.lib.
Je proto vytvoren prazdny projekt AtUsbHid,
kde je pomoci DEF souboru a prazdnych definic
funkci zkompilovan soubor AtUsbHid.lib.
Vzapeti se z adresare Debug smaze AtUsbHid.dll
(ktere obsahuje prazdne funkce) a nahradi se
skutecnym AtUsbHid.dll od Atmelu. V solution
se musi projekt AtUsbHid musi zakazat
(Unload project v Solution Explorer), jinak
je po spusteni knihovna AtUsbHid.dll znovu
prelozena a smaze se spravna verze od Atmelu

1. Do projektu doplnit knihovny :
   Linker/Input/Additional Depedencies 
   ../Debug/AtUsbHid.lib
   
2. Upravit kodovani stringu :
   Configuration Properties/General/Character Set 
   Use Multi-Byte Character Set
   
3. Doplnit definici symbolu :
   C/C++/Preprocesor/Preprocessor Definitions  
   _CRT_SECURE_NO_WARNINGS
