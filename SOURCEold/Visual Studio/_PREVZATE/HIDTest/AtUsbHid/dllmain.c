#include "windows.h"

#define DLL_EXPORT        // export functions from DLL
#include "AtUsbHid.h"

BOOL APIENTRY DllMain( HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

BOOLEAN APIENTRY findHidDevice(const UINT VendorID, const UINT ProductID)
{
   return( TRUE);
}

void APIENTRY closeDevice(void)
{
}


BOOLEAN APIENTRY writeData(UCHAR* buf)
{
   return( TRUE);
}


BOOLEAN APIENTRY readData(UCHAR* buffer)
{
   return( TRUE);
}

int APIENTRY hidRegisterDeviceNotification(HWND hWnd)
{
   return( 0);
}

void APIENTRY hidUnregisterDeviceNotification(HWND hWnd)
{
}

int APIENTRY isMyDeviceNotification(DWORD dwData)
{
   return( 0);
}

BOOLEAN APIENTRY setFeature(UCHAR *buffer)
{
   return( TRUE);
}

int APIENTRY getFeatureReportLength(void)
{
   return( 0);
}

int APIENTRY getOutputReportLength(void)
{
   return( 0);
}

int APIENTRY getInputReportLength(void)
{
   return( 0);
}
