//******************************************************************************
//
//   AtUsbHid.h   Atmel USB HID library
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef _ATUSBHID_H_
#define _ATUSBHID_H_

// Error codes.
#define ERROR_USB_DEVICE_NOT_FOUND          0xE0000001
#define ERROR_USB_DEVICE_NO_CAPABILITIES    0xE0000002

#ifdef DLL_EXPORT
   #ifdef __cplusplus
      #define ATUSBHID_API extern "C" __declspec(dllexport)
   #else
      #define ATUSBHID_API extern __declspec(dllexport)
   #endif
#else
   #ifdef __cplusplus
      #define ATUSBHID_API extern "C" __declspec(dllimport)
   #else
      #define ATUSBHID_API extern __declspec(dllimport)
   #endif
#endif

//******************************************************************************
// Exported functions prototypes
//******************************************************************************

ATUSBHID_API BOOLEAN APIENTRY findHidDevice(const UINT VendorID, const UINT ProductID);

ATUSBHID_API void    APIENTRY closeDevice(void);

ATUSBHID_API BOOLEAN APIENTRY writeData(UCHAR* buf);

ATUSBHID_API BOOLEAN APIENTRY readData(UCHAR* buffer);

ATUSBHID_API int     APIENTRY hidRegisterDeviceNotification(HWND hWnd);

ATUSBHID_API void    APIENTRY hidUnregisterDeviceNotification(HWND hWnd);

ATUSBHID_API int     APIENTRY isMyDeviceNotification(DWORD dwData);

ATUSBHID_API BOOLEAN APIENTRY setFeature(UCHAR *buffer);

ATUSBHID_API int     APIENTRY getFeatureReportLength(void);

ATUSBHID_API int     APIENTRY getOutputReportLength(void);

ATUSBHID_API int     APIENTRY getInputReportLength(void);


#endif  // _ATUSBHID_H_
