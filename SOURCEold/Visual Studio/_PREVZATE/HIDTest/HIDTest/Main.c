//******************************************************************************
//
//   Main.c       HID Test main module Atmel Usb Hid library
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#define getch() _getch()                    // conio patch

#include "Performance.h"

#include "..\AtUsbHid\AtUsbHid.h"           // Atmel USB HID library

#define SINGLE_REPLY  1                     // single ping reply/else multiple reply, must match firmware !

// Atmel USB Key :
#define VID 0x03EB
#define PID 0x2013

// HID parameters :
#define HIDDEMO_REPORT_SIZE 64          // report packet size
#define REPLY_TIMEOUT       100         // wait for read with timeout [ms]
#define BURST_SIZE          100         // number of burst reports
#define ENABLE_TIMEOUT      1           // wait for read with timeout

#define MULTIPLE_BURST_SIZE  32         // watch firmware for value !

// HID report buffers :
BYTE OutBuffer[ HIDDEMO_REPORT_SIZE];   // Write Buffer
BYTE InBuffer[ HIDDEMO_REPORT_SIZE];    // Read Buffer

// Local functions :

static void PrintHelp( void);
// Print help

static void PrepareBuffer( int n);
// Prepare out buffer

static BOOL WaitForReply( void);
// Wait for reply

static void CompareBuffer( int n);
// Compare in/out buffer

static void BurstTest( void);
// Run burst test

static void DelayedBurstTest( void);
// Run delayed burst test

#ifndef SINGLE_REPLY
   static void MultipleReplyTest( void);
   // Run multiple reply test

   static void ReadOnlyTest( void);
   // Run read only test
#endif // !SINGLE_REPLY

//-----------------------------------------------------------------------------
// Main
//-----------------------------------------------------------------------------

void main( void)
{
   printf(">>> Opening USB HID device with Vendor ID= 0x%04X and Product ID=0x%04X\n", VID, PID);
	if( !findHidDevice( VID, PID)) {
		switch(GetLastError()) {
			case ERROR_USB_DEVICE_NOT_FOUND :
				printf("Error: Could not open the device\n");
				break;
			case ERROR_USB_DEVICE_NO_CAPABILITIES:
				printf("Error: Could not get USB device capabilities\n");
				break;
			default:
				printf("Error: While opening device\n");
				break;
		}
      return;
	}
   printf(">>> USB HID device  VID=0x%04X, PID=0x%04X opened\n", VID, PID);
   // print capability :
	printf( ">>> USB HID Input   Buffer size is %dByte\n", getInputReportLength());
	printf( ">>> USB HID Output  Buffer size is %dByte\n", getOutputReportLength());
	printf( ">>> USB HID Feature Buffer size is %dByte\n", getFeatureReportLength());
   PrintHelp();
   for(;;){
      switch( getch()){
         case 'e' :
            PrepareBuffer( 0);
            printf( "Write done\n");
            writeData( OutBuffer);
#ifdef ENABLE_TIMEOUT
            if( !WaitForReply()){
               printf( "ERROR : Timeout\n");
               break;
            }
#else
            while( !readData( InBuffer));
#endif
            printf( "Read done\n");
            CompareBuffer( 0);
            break;

         case 'b' :
            BurstTest();
            break;

         case 'd' :
            DelayedBurstTest();
            break;

#ifndef SINGLE_REPLY
         case 'm' :
            MultipleReplyTest();
            break;

         case 'r' :
            ReadOnlyTest();
            break;
#endif

         case 'x' :
          	// Close our USB HID device.
            closeDevice();
            printf( ">>> USB HID device VID=0x%04X PID=0x%04X closed\n", VID, PID);
            exit( 0);
            break;

         default :
            PrintHelp();
            break;
      }
   }
} // main

//******************************************************************************
// Help
//******************************************************************************

static void PrintHelp( void)
// Print help
{
   printf( "\nCommands :\n\n");
   printf( "  E - Echo\n");
   printf( "  B - Burst\n");
   printf( "  D - Delayed burst\n");
#ifndef SINGLE_REPLY
   printf( "  M - Multiple reply\n");
   printf( "  R - Read only\n");
#endif
   printf( "  X - Exit\n");
} // PrintHelp

//******************************************************************************
// Prepare buffer
//******************************************************************************

static void PrepareBuffer( int n)
// Prepare out buffer
{
int i;

   OutBuffer[ 0] = n;
   for( i = 1; i < HIDDEMO_REPORT_SIZE; i++){
      OutBuffer[ i] = i;
   }
} // PrepareBuffer

//******************************************************************************
// Compare buffer
//******************************************************************************

static void CompareBuffer( int n)
// Compare in/out buffer
{
int i;

   for( i = 0; i < HIDDEMO_REPORT_SIZE; i++){
      if( InBuffer[ i] != OutBuffer[ i]){
         printf( "COMPARE failed at offset %d value out %d in %d\n", i, OutBuffer[ i], InBuffer[ i]);
      }
   }
} // CompareBuffer

//******************************************************************************
// Wait for reply
//******************************************************************************

static BOOL WaitForReply( void)
// Wait for reply
{
int i;

   for( i = 0; i < REPLY_TIMEOUT; i++){
      if( readData( InBuffer)){
         return( TRUE);
      }
      Sleep( 1);
   }
   return( FALSE);
} // WaitForReply

//******************************************************************************
// Burst
//******************************************************************************

static void BurstTest( void)
// Run burst test
{
int   i;
int64 Timer;

   printf( "Start burst\n");
   Timer = TimerGet();
   for( i = 0; i < BURST_SIZE; i++){
      PrepareBuffer( i);
      writeData( OutBuffer);
#ifdef ENABLE_TIMEOUT
      if( !WaitForReply()){
         printf( "ERROR : Timeout\n");
         return;
      }
#else
      while( !readData( InBuffer));
#endif
      CompareBuffer( i);
   }
   Timer = TimerGet() - Timer;
   printf( "Burst done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // BurstTest

//******************************************************************************
// Delayed burst
//******************************************************************************

static void DelayedBurstTest( void)
// Run delayed burst test
{
int   i;
int64 Timer;

   printf( "Start write\n");
   Timer = TimerGet();
   for( i = 0; i < BURST_SIZE; i++){
      PrepareBuffer( i);
      writeData( OutBuffer);
   }

   Timer = TimerGet() - Timer;
   printf( "Write done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));

   printf( "Start read\n");
   Timer = TimerGet();
   for( i = 0; i < BURST_SIZE; i++){
      PrepareBuffer( i);
#ifdef ENABLE_TIMEOUT
      if( !WaitForReply()){
         printf( "ERROR : Timeout\n");
         return;
      }
#else
      while( !readData( InBuffer));
#endif
      CompareBuffer( i);
   }
   Timer = TimerGet() - Timer;
   printf( "Read done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // DelayedBurstTest

#ifndef SINGLE_REPLY
//******************************************************************************
// Multiple Reply
//******************************************************************************

static void MultipleReplyTest( void)
// Run multiple reply test
{
int   i;
int64 Timer;

   printf( "Start write\n");
   PrepareBuffer( 0);
   writeData( OutBuffer);
   printf( "Write done\n");

   printf( "Start read\n");
   Timer = TimerGet();
   PrepareBuffer( 0);
   for( i = 0; i < MULTIPLE_BURST_SIZE; i++){
      OutBuffer[ 0] = i;
#ifdef ENABLE_TIMEOUT
      if( !WaitForReply()){
         printf( "ERROR : Timeout\n");
         return;
      }
#else
      while( !readData( InBuffer));
#endif
      CompareBuffer( i);
   }
   Timer = TimerGet() - Timer;
   printf( "Read done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // MultipleReplyTest

//******************************************************************************
// Read only
//******************************************************************************

static void ReadOnlyTest( void)
// Run read only test
{
int   i;

   printf( "Start read\n");
   PrepareBuffer( 0);
   i = 0;
   forever {
      if( !WaitForReply()){
         printf( "ERROR : Timeout\n");
         return;
      }
      printf( "Index %d header %d\n", i, InBuffer[ 0]);
      i++;
   }
} // ReadOnlyTest
#endif // !SINGLE_REPLY
