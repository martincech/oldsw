//******************************************************************************
//
//   Performance.h   Performance time measuring
//   Version 1.0  (c) VymOs
//
//******************************************************************************


#ifndef PerformanceH
   #define PerformanceH

typedef long long int64;

#ifdef __cplusplus
   extern "C" {
#endif

int64 TimerGet( void);
// Get performance timer

double TimerSeconds( int64 Timer);
// Convert to seconds

#ifdef __cplusplus
   }
#endif

#endif

