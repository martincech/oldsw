//******************************************************************************
//
//   UsbUart.h    FTDI USB module
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef UsbUartH
   #define UsbUartH

#include "FTD2XX.h"

class TUsbUart
{
public :
   //---------------- UART parameters

   // Handshake Type :
   typedef enum {
      NO_HANDSHAKE,
      HARDWARE_HANDSHAKE,
      XON_XOFF_HANDSHAKE,
      HALF_DUPLEX_HANDSHAKE, // RS485 via RTS
   } THandshake;

   // Parity Type :
   typedef enum {
      NO_PARITY,
      ODD_PARITY,
      EVEN_PARITY,
      MARK_PARITY,
      SPACE_PARITY,
   } TParity;

   // Communication parameters :
   typedef struct {
      int        BaudRate;   // 300,600...115200
      int        DataBits;   // 5..8
      int        StopBits;   // stop bit count * 10 (10,15,20)
      TParity    Parity;     // Constants via TParity
      THandshake Handshake;  // Constants via THandshake
   } TParameters;

   //---------------- Functions

   TUsbUart();
   // Constructor

   ~TUsbUart();
   // Destructor

   static BOOL Access( char *Name);
   // Check for device <Name> present

   BOOL Open( char *Name);
   // Open device by <Name>

   BOOL Open( char *Name, char *SerialNumber);
   // Open device by <Name> and <SerialNumber>
   BOOL Open( unsigned int LocId);

   void Close();
   // Close device

   int Write( void *Data, int Length);
   // Write <Data> of size <Length>, returns written length (or 0 if error)

   int Read( void *Data, int Length);
   // Read data <Data> of size <Length>, returns true length (or 0 if error)

   void Flush();
   // Make input/output queue empty
   
   void SetRxNowait();
   // Set timing of receiver - returns collected data immediately

   void SetRxWait( int ReplyTime, int IntercharacterTime);
   // Set timing of receiver :
   // If <IntercharacterTime> = 0 waits <ReplyTime> for whole message
   // If <IntercharacterTime> > 0 waits <ReplyTime> for first
   // character, next waits <IntercharacterTime> * count of characters

   //------------------- Properties
   BOOL  SetParameters( const TParameters &Parameters);
   void  GetParameters( TParameters &Parameters);
   BOOL  GetCTS();   
   BOOL  GetDSR();
   BOOL  GetRI();
   BOOL  GetDCD();
   void  SetDTR( BOOL Status);
   void  SetRTS( BOOL Status);
   void  SetTxD( BOOL Status);

   BOOL  IsOpen;
   int   TxLatency;

//-----------------------------------------------------------------------------
protected :
   TParameters FParameters;
   FT_HANDLE   FHandle;            // USB device handle
   BOOL        FImmediateRead;     // RxNowait

   static DWORD Locate( char *Name);
   // Find device by <Name>, returns device index or INVALID_INDEX

   static DWORD Locate( char *Name, char *SerialNumber);
   static DWORD Locate( unsigned int LocId);

   void SafeTxTiming();
   // Set safe timing of the transmitter

   DWORD GetModemStatus();
   // Read modem status
}; // TUsbUart

#endif
