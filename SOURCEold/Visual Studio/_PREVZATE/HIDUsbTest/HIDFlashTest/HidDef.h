// HidDef.h

#ifndef HidDefH
   #define HidDefH

// Add Atmel native definitions for FW HidDef.h :
typedef unsigned char Byte;
typedef unsigned short Word;
typedef unsigned DWord;

#pragma pack( push, 1)                    // byte alignment
   #include "C:/WORK/AT90USB/UsbDemo/HIDMemory/at90usb128/demo/USBKEY_STK525-series6-hidio/HidDef.h"
#pragma pack( pop)                        // original alignment

#endif
