//******************************************************************************
//
//   Main.cpp     USB HID flash memory main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include <conio.h>
#include <stdio.h>

#include "USB/HID/Hid.h"
#include "Unisys/Performance.h"
#include "HidDef.h"          // packet structure

#define MY_VID 0x03EB
#define MY_PID 0x2013
#define MY_DEVICE "AVR USB HID DEMO"

#define HIDDEMO_REPORT_SIZE HID_REPORT_SIZE      // report packet size
#define HIDDEMO_BUFFERS     256                  // input buffers count

#define SECTOR_SIZE          512          // sector size
#define SECTOR_REPORTS       (SECTOR_SIZE / HID_REPORT_SIZE)

byte OutBuffer[ HIDDEMO_REPORT_SIZE];
byte InBuffer[ HIDDEMO_REPORT_SIZE];

byte Sector[ SECTOR_SIZE];

static THid *Hid;

// Local functions :

static void ReadSector( void);
// Read single sector

static void ReadMoreSectors( void);
// Read more sectors

static void RepeatReadSectors( void);
// Repeat read single sector

static void BurstReadSectors( void);
// Repeat read burst sector

static BOOL ReadSectors( dword Address, int Count);
// Read sectors

//******************************************************************************
// Main
//******************************************************************************

void main()
{
   printf( "Start...\n");
   Hid = new THid;

   if( !Hid->Open( MY_DEVICE)){
      printf( "Unable open device '%s'\n", MY_DEVICE);
      delete Hid;
      exit( 0);
   }
   printf( "USB HID Device '%s' opened\n", Hid->GetDeviceName());
   printf( "USB HID Device VID=0x%04X, PID=0x%04X\n", Hid->GetVid(), Hid->GetPid());
   // print capability :
	printf( "USB HID Input   Buffer size is %dByte\n", Hid->GetInputReportSize());
	printf( "USB HID Output  Buffer size is %dByte\n", Hid->GetOutputReportSize());
	printf( "USB HID Buffers count is       %dByte\n", Hid->GetInputBuffers());
   printf( "USB HID Device name is          '%s'\n",  Hid->GetDeviceName());
   printf( "USB HID Device manufacturer  is '%s'\n",  Hid->GetManufacturer());
   printf( "USB HID Device serial number is '%s'\n",  Hid->GetSerialNumber());
//	printf( "USB HID Feature Buffer size is %dByte\n", Hid->GetFeatureReportSize());
   if( !Hid->SetInputBuffers( HIDDEMO_BUFFERS)){
      printf( "Unable set input buffers count\n");
   } else {
      printf( "USB HID Buffers count is  now  %dByte\n", Hid->GetInputBuffers());
   }
   printf( "Commands :\n\n");
   printf( "  S - Read sector\n");
   printf( "  M - Read more sectors\n");
   printf( "  R - Repeat read sectors\n");
   printf( "  B - Burst read sectors\n");
   printf( "  F - Flush\n");
   printf( "  X - Exit\n");
   for(;;){
      switch( getch()){
         case 's' :
            ReadSector();
            break;

         case 'm' :
            ReadMoreSectors();
            break;

         case 'r' :
            RepeatReadSectors();
            break;

         case 'b' :
            BurstReadSectors();
            break;

         case 'f' :
            if( !Hid->Flush()){
               printf( "ERROR : Flush failed\n");
               break;
            }
            printf( "Flush O.K.\n");
            break;

         case 'x' :
          	// Close our USB HID device.
            delete Hid;
            printf( "USB HID device VID=0x%04X PID=0x%04X closed\n", MY_VID, MY_PID);
            exit( 0);
            break;

         default :
            break;
      }
   }
} // main

//******************************************************************************
// Single sector
//******************************************************************************

static void ReadSector( void)
// Read single sector
{
int64 Timer;

   memset( OutBuffer, 0, sizeof( OutBuffer));
   printf( "Start sector\n");
   Timer = TimerGet();
   if( !ReadSectors( 0, 1)){
      printf( "Unable read sectors\n");
      return;
   }
   Timer = TimerGet() - Timer;
   printf( "Sector done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // ReadSector

//******************************************************************************
// More sectors
//******************************************************************************

#define SECTORS_COUNT 1000

static void ReadMoreSectors( void)
// Read more sectors
{
int64 Timer;

   memset( OutBuffer, 0, sizeof( OutBuffer));
   printf( "Start more sectors\n");
   Timer = TimerGet();
   if( !ReadSectors( 0, SECTORS_COUNT)){
      printf( "Unable read sectors\n");
      return;
   }
   Timer = TimerGet() - Timer;
   printf( "Sectors done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // ReadMoreSectors

//******************************************************************************
// Repeat read
//******************************************************************************

static void RepeatReadSectors( void)
// Repeat read single sector
{
int64 Timer;
int   i;
dword Address;

   memset( OutBuffer, 0, sizeof( OutBuffer));
   printf( "Start repeat sectors\n");
   Timer = TimerGet();
   Address = 0;
   for( i = 0; i < SECTORS_COUNT; i++){
      if( !ReadSectors( Address, 1)){
         printf( "Unable read sectors\n");
         return;
      }
      Address += SECTOR_SIZE;
   }
   Timer = TimerGet() - Timer;
   printf( "Sectors done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // RepeatReadSectors

//******************************************************************************
// Burst read
//******************************************************************************

#define BURST_SIZE 10

static void BurstReadSectors( void)
// Repeat read burst sector
{
int64 Timer;
int   i;
dword Address;

   memset( OutBuffer, 0, sizeof( OutBuffer));
   printf( "Start burst sectors\n");
   Timer = TimerGet();
   Address = 0;
   for( i = 0; i < SECTORS_COUNT; i += BURST_SIZE){
      if( !ReadSectors( Address, BURST_SIZE)){
         printf( "Unable read sectors\n");
         return;
      }
      Address += SECTOR_SIZE;
   }
   Timer = TimerGet() - Timer;
   printf( "Sectors done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // BurstReadSectors

//******************************************************************************
// Read sectors
//******************************************************************************

static BOOL ReadSectors( dword Address, int Count)
// Read sectors
{
int   i, s;
THidRequest *Request;

   // request packet :
   Request = (THidRequest *)OutBuffer;
   Request->Command = HID_FLASH_READ;
   Request->Address = Address;
   Request->Count   = Count;
   // send request packet :
   if( !Hid->WriteReport( OutBuffer)){
      printf( "Unable write report\n");
      return( FALSE);
   }
   // read sectors :
   for( s = 0; s < Count; s++){
      // read one sector :
      for( i = 0; i < SECTOR_REPORTS; i++){
         if( !Hid->ReadReport( InBuffer)){
            printf( "Unable read report\n");
            return( FALSE);
         }
         memcpy( &Sector[ i * HID_REPORT_SIZE], InBuffer, HID_REPORT_SIZE);
      }
   }
   return( TRUE);
} // ReadSector

