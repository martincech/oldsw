//******************************************************************************
//
//   Main.cpp     USB HID library test main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include <conio.h>
#include <stdio.h>

#include "USB/HID/Hid.h"
#include "Unisys/Performance.h"

#define SINGLE_REPLY  1                     // single ping reply/else multiple reply, must match firmware !

#define MY_VID 0x03EB
#define MY_PID 0x2013
#define MY_DEVICE "AVR USB HID DEMO"

#define HIDDEMO_REPORT_SIZE HID_REPORT_SIZE      // report packet size
#define HIDDEMO_BUFFERS     256                  // input buffers count

#define BURST_SIZE           100         // number of burst reports
#define MULTIPLE_BURST_SIZE  100         // copy by firmware !

byte OutBuffer[ HIDDEMO_REPORT_SIZE];
byte InBuffer[ HIDDEMO_REPORT_SIZE];

static THid *Hid;

// Local functions :

static void PrintHelp( void);
// Print help

static void PrepareBuffer( int n);
// Prepare out buffer

static void CompareBuffer( int n);
// Compare in/out buffer

static void BurstTest( void);
// Run burst test

static void DelayedBurstTest( void);
// Run delayed burst test

#ifndef SINGLE_REPLY
   static void MultipleReplyTest( void);
   // Run multiple reply test

   static void ReadOnlyTest( void);
   // Run read only test
#endif

//******************************************************************************
// Main
//******************************************************************************

void main()
{
   printf( "Start...\n");
   Hid = new THid;
/*
   if( !Hid->Open( MY_VID, MY_PID)){
      printf( "Unable open device PID=%04X VID=%04X\n", MY_VID, MY_PID);
      delete Hid;
      exit( 0);
   }
*/
   if( !Hid->Open( MY_DEVICE)){
      printf( "Unable open device '%s'\n", MY_DEVICE);
      delete Hid;
      exit( 0);
   }
   printf( "USB HID Device '%s' opened\n", Hid->GetDeviceName());
   printf( "USB HID Device VID=0x%04X, PID=0x%04X\n", Hid->GetVid(), Hid->GetPid());
   // print capability :
	printf( "USB HID Input   Report size is %dByte\n", Hid->GetInputReportSize());
	printf( "USB HID Output  Report size is %dByte\n", Hid->GetOutputReportSize());
	printf( "USB HID Buffers count is       %d\n",     Hid->GetInputBuffers());
   printf( "USB HID Device name is          '%s'\n",  Hid->GetDeviceName());
   printf( "USB HID Device manufacturer  is '%s'\n",  Hid->GetManufacturer());
   printf( "USB HID Device serial number is '%s'\n",  Hid->GetSerialNumber());
//	printf( "USB HID Feature Buffer size is %dByte\n", Hid->GetFeatureReportSize());
   if( !Hid->SetInputBuffers( HIDDEMO_BUFFERS)){
      printf( "Unable set input buffers count\n");
   } else {
      printf( "USB HID Buffers count is  now  %d\n", Hid->GetInputBuffers());
   }
   PrintHelp();
   for(;;){
      switch( getch()){
         case 'e' :
            PrepareBuffer( 0);
            printf( "Write done\n");
            if( !Hid->WriteReport( OutBuffer)){
               printf( "Unable write report\n");
               break;
            }
            if( !Hid->ReadReport( InBuffer)){
               printf( "Unable read report\n");
               break;
            }
            printf( "Read done\n");
            CompareBuffer( 0);
            break;

         case 'b' :
            BurstTest();
            break;

         case 'd' :
            DelayedBurstTest();
            break;

#ifndef SINGLE_REPLY
         case 'm' :
            MultipleReplyTest();
            break;

         case 'r' :
            ReadOnlyTest();
            break;
#endif

         case 'f' :
            if( !Hid->Flush()){
               printf( "ERROR : Flush failed\n");
               break;
            }
            printf( "Flush O.K.\n");
            break;

         case 'x' :
          	// Close our USB HID device.
            delete Hid;
            printf( "USB HID device VID=0x%04X PID=0x%04X closed\n", MY_VID, MY_PID);
            exit( 0);
            break;

         default :
            PrintHelp();
            break;
      }
   }
} // main

//******************************************************************************
// Help
//******************************************************************************

static void PrintHelp( void)
// Print help
{
   printf( "Commands :\n\n");
   printf( "  E - Echo\n");
   printf( "  B - Burst\n");
   printf( "  D - Delayed burst\n");
#ifndef SINGLE_REPLY
   printf( "  M - Multiple reply\n");
   printf( "  R - Read only\n");
#endif
   printf( "  F - Flush\n");
   printf( "  X - Exit\n");
} // PrintHelp

//******************************************************************************
// Prepare buffer
//******************************************************************************

static void PrepareBuffer( int n)
// Prepare out buffer
{
int i;

   OutBuffer[ 0] = n;
   for( i = 1; i < HIDDEMO_REPORT_SIZE; i++){
      OutBuffer[ i] = i;
   }
} // PrepareBuffer

//******************************************************************************
// Compare buffer
//******************************************************************************

static void CompareBuffer( int n)
// Compare in/out buffer
{
int i;

   for( i = 0; i < HIDDEMO_REPORT_SIZE; i++){
      if( InBuffer[ i] != OutBuffer[ i]){
         printf( "COMPARE failed at offset %d value out %d in %d\n", i, OutBuffer[ i], InBuffer[ i]);
      }
   }
} // CompareBuffer

//******************************************************************************
// Burst
//******************************************************************************

static void BurstTest( void)
// Run burst test
{
int   i;
int64 Timer;

   printf( "Start burst\n");
   Timer = TimerGet();
   for( i = 0; i < BURST_SIZE; i++){
      PrepareBuffer( i);
      if( !Hid->WriteReport( OutBuffer)){
         printf( "Unable write report\n");
         return;
      }
      if( !Hid->ReadReport( InBuffer)){
         printf( "Unable read report\n");
         return;
      }
      CompareBuffer( i);
   }
   Timer = TimerGet() - Timer;
   printf( "Burst done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // BurstTest

//******************************************************************************
// Delayed burst
//******************************************************************************

static void DelayedBurstTest( void)
// Run delayed burst test
{
int   i;
int64 Timer;

   printf( "Start write\n");
   Timer = TimerGet();
   for( i = 0; i < BURST_SIZE; i++){
      PrepareBuffer( i);
      if( !Hid->WriteReport( OutBuffer)){
         printf( "Unable write report\n");
         return;
      }
   }

   Timer = TimerGet() - Timer;
   printf( "Write done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));

   printf( "Start read\n");
   Timer = TimerGet();
   for( i = 0; i < BURST_SIZE; i++){
      PrepareBuffer( i);
      if( !Hid->ReadReport( InBuffer)){
         printf( "Unable read report\n");
         return;
      }
      CompareBuffer( i);
   }
   Timer = TimerGet() - Timer;
   printf( "Read done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // DelayedBurstTest

#ifndef SINGLE_REPLY
//******************************************************************************
// Multiple Reply
//******************************************************************************

static void MultipleReplyTest( void)
// Run multiple reply test
{
int   i;
int64 Timer;

   printf( "Start write\n");
   PrepareBuffer( 0);
   if( !Hid->WriteReport( OutBuffer)){
      printf( "Unable write report\n");
      return;
   }
   printf( "Write done\n");

   printf( "Start read\n");
   Timer = TimerGet();
   PrepareBuffer( 0);
   for( i = 0; i < MULTIPLE_BURST_SIZE; i++){
      OutBuffer[ 0] = i;
      if( !Hid->ReadReport( InBuffer)){
         printf( "Unable read report\n");
         return;
      }
      CompareBuffer( i);
   }
   Timer = TimerGet() - Timer;
   printf( "Read done\n");
   printf( "Elapsed time %.3f\n", TimerSeconds( Timer));
} // MultipleReplyTest

//******************************************************************************
// Read only
//******************************************************************************

static void ReadOnlyTest( void)
// Run read only test
{
int   i;

   printf( "Start read\n");
   PrepareBuffer( 0);
   i = 0;
   forever {
      if( !Hid->ReadReport( InBuffer)){
         printf( "Unable read report\n");
         return;
      }
      printf( "Index %d header %d\n", i, InBuffer[ 0]);
      i++;
   }
} // ReadOnlyTest
#endif // !SINGLE_REPLY
