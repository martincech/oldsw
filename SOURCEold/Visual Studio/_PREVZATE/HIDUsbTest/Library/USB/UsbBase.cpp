//******************************************************************************
//
//   UsbBase.cpp  USB basic operations
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include "UsbBase.h"
#include "Unisys/Uni.h"

//******************************************************************************
// Constructor
//******************************************************************************

TUsbBase::TUsbBase()
// Constructor
{
   Path   = 0;
   Handle = INVALID_HANDLE_VALUE;
} // TUsbBase

//******************************************************************************
// Destructor
//******************************************************************************

TUsbBase::~TUsbBase()
// Destructor
{
   if( Path){
      free( Path);
   }
   if_okh( Handle){
      CloseHandle( Handle);
   }
} // ~TUsbBase

//******************************************************************************
// Locate
//******************************************************************************

BOOL TUsbBase::Locate( GUID *Guid, int Index)
// Locate class
{
HDEVINFO                         deviceInfo;
SP_DEVICE_INTERFACE_DATA         interfaceData;
PSP_DEVICE_INTERFACE_DETAIL_DATA detailData;

   // get list of present devices :
   deviceInfo = SetupDiGetClassDevs( Guid, NULL, NULL,
                                     DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
   if_errh( deviceInfo){
      return( FALSE);                  // unable get devices
   }
   // find first device interface :
   interfaceData.cbSize = sizeof( SP_DEVICE_INTERFACE_DATA);
   if( !SetupDiEnumDeviceInterfaces( deviceInfo,
                                     NULL,         // deviceInfoData
                                     Guid,
                                     (DWORD)Index, // MemberIndex
                                     &interfaceData)){
      SetupDiDestroyDeviceInfoList( deviceInfo);
      return( FALSE);
   }
   // get device detail structure size :
   ULONG requiredLength = 0;
   SetupDiGetDeviceInterfaceDetail( deviceInfo,
                                    &interfaceData,
                                    NULL, 0,
                                    &requiredLength,
                                    NULL);
   // allocate device details :
   detailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc( requiredLength);
   if( !detailData){
      SetupDiDestroyDeviceInfoList( deviceInfo);
      return( FALSE);
   }
   // get device details :
  detailData->cbSize = sizeof( SP_DEVICE_INTERFACE_DETAIL_DATA);
  ULONG length = requiredLength;
  if( !SetupDiGetDeviceInterfaceDetail( deviceInfo,
                                        &interfaceData,
                                        detailData,
                                        length,
                                        &requiredLength,
                                        NULL)){
      free( detailData);
      SetupDiDestroyDeviceInfoList( deviceInfo);
      return( FALSE);
   }
   // copy device path :
   if( Path){
      free( Path);
   }
   Path = (char *)malloc( strlen( detailData->DevicePath) + 1);
   if( !Path){
      free( detailData);
      SetupDiDestroyDeviceInfoList( deviceInfo);
      return( FALSE);
   }
   strcpy( Path, detailData->DevicePath);
   // cleanup :
   SetupDiDestroyDeviceInfoList( deviceInfo);
   free( detailData);
   return( TRUE);
} // Locate

//******************************************************************************
// Open
//******************************************************************************

BOOL TUsbBase::Open()
// Open located device
{
   Close();                            // close previous device
   Handle = CreateFile( Path,
                        GENERIC_WRITE | GENERIC_READ,
                        FILE_SHARE_WRITE | FILE_SHARE_READ,
                        NULL,
                        OPEN_EXISTING,
                        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED,
                        NULL);
   if_errh( Handle){
      return( FALSE);
   }
   return( TRUE);
} // Open

//******************************************************************************
// Close
//******************************************************************************

void TUsbBase::Close()
// Close opened device
{
   if_errh( Handle){
      return;                          // device not opened
   }
   CloseHandle( Handle);
   Handle = INVALID_HANDLE_VALUE;
} // Close

//******************************************************************************
// Get path
//******************************************************************************

char *TUsbBase::GetPath()
// Returns device path
{
   return( Path);
} // GetPath

//******************************************************************************
// Get handle
//******************************************************************************

HANDLE TUsbBase::GetHandle()
// Get opened handle
{
   return( Handle);
} // GetHandle

