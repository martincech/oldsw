//******************************************************************************
//
//   Hid.cpp      HID library class
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include "Hid.h"

#include "Unisys/Uni.h"

#define HID_MAX_DEVICES   100    // maximum number of checked devices
#define HID_WRITE_TIMEOUT 100    // write timeout
#define HID_READ_TIMEOUT  100    // read timeout

#define MAX_STRING        128

#define wcsequ( s1, s2)   (!wcscmp( s1, s2))

//******************************************************************************
// Constructor
//******************************************************************************

THid::THid() : TUsbBase()
// Constructor
{
   Report      = (byte *)malloc( HID_REPORT_SIZE + 1);
   // create event :
   ReportEvent = CreateEvent( NULL, TRUE, FALSE, NULL);
   HidOverlapped.hEvent     = ReportEvent;
	HidOverlapped.Offset     = 0;
	HidOverlapped.OffsetHigh = 0;
} // THid

//******************************************************************************
// Destructor
//******************************************************************************

THid::~THid()
// Destructor
{
   if_okh( ReportEvent){
      CloseHandle( ReportEvent);
   }
   if( Report){
      free( Report);
   }
} // ~THid

//******************************************************************************
// Open
//******************************************************************************

BOOL THid::Open( char *DeviceName)
// Open device by <Name>
{
   // convert name to wide character :
   wchar_t WName[ MAX_STRING + 1];
   if( !::MultiByteToWideChar( CP_ACP, 0, DeviceName, -1, WName, MAX_STRING)){
      return( FALSE);
   }
   // get HID GUID :
   GUID Guid;
   HidD_GetHidGuid( &Guid);
   // locate by name :
   wchar_t Name[ MAX_STRING + 1];
   BOOL    Found = FALSE;
   char   *Path;
   for( int i = 0; i < HID_MAX_DEVICES; i++){
      // locate GUID :
      if( !TUsbBase::Locate( &Guid, i)){
         return( FALSE);
      }
      // get device path :
      Path = TUsbBase::GetPath();
      if( !Path){
         return( FALSE);
      }
      // open device :
      if( !TUsbBase::Open()){
         continue;
      }
      // get product name :
      if( !HidD_GetProductString( Handle, Name, MAX_STRING + 1)){
         TUsbBase::Close();
         continue;
      }
      if( wcsequ( WName, Name)){
         Found = TRUE;
         break;
      }
      TUsbBase::Close();
   }
   if( !Found){
      return( FALSE);
   }
   // get device capabilities :
   if( !GetCapabilities()){
      TUsbBase::Close();
      return( FALSE);
   }
   return( TRUE);
} // Open


//******************************************************************************
// Open by VID & PID
//******************************************************************************

BOOL THid::OpenBy( int Vid, int Pid)
// Open device by <Vid> & <Pid>
{
   // get HID GUID :
   GUID Guid;
   HidD_GetHidGuid( &Guid);
   // locate by VID & PID :
   HIDD_ATTRIBUTES Attributes;
   BOOL            Found = FALSE;
   char           *Path;
   for( int i = 0; i < HID_MAX_DEVICES; i++){
      // locate GUID :
      if( !TUsbBase::Locate( &Guid, i)){
         return( FALSE);
      }
      // get device path :
      Path = TUsbBase::GetPath();
      if( !Path){
         return( FALSE);
      }
      // open device :
      if( !TUsbBase::Open()){
         continue;
      }
      // get HID attributes :
      Attributes.Size = sizeof( HIDD_ATTRIBUTES);
      if( !HidD_GetAttributes( TUsbBase::GetHandle(), &Attributes)){
         TUsbBase::Close();
         continue;
      }
      if( Attributes.VendorID  == Vid &&
          Attributes.ProductID == Pid){
         Found = TRUE;
         break;
      }
      TUsbBase::Close();
   }
   if( !Found){
      return( FALSE);
   }
   // get device capabilities :
   if( !GetCapabilities()){
      TUsbBase::Close();
      return( FALSE);
   }
   return( TRUE);
} // OpenBy

//******************************************************************************
// Close
//******************************************************************************

void THid::Close()
// Close device
{
   TUsbBase::Close();
} // Close

//******************************************************************************
// Write report
//******************************************************************************

BOOL THid::WriteReport( void *Buffer)
// Write single report <Buffer>
{
   Report[ 0] = 0;                     // Report ID
   memcpy( &Report[ 1], Buffer, Capabilities.OutputReportByteLength - 1);
   DWORD WrittenBytes;
   if( !WriteFile( Handle, Report, Capabilities.OutputReportByteLength, &WrittenBytes, &HidOverlapped)){
      if( GetLastError() != ERROR_IO_PENDING){
         return( FALSE);
      }
      DWORD Result = WaitForSingleObject( ReportEvent, HID_WRITE_TIMEOUT);
      if( Result == WAIT_TIMEOUT || Result == WAIT_ABANDONED){
         CancelIo( Handle);
         return( FALSE);
      }
      if( Result != WAIT_OBJECT_0){
         return( FALSE);
      }
      if( !GetOverlappedResult( Handle, &HidOverlapped, &WrittenBytes, FALSE)){
         return( FALSE);
      }
   } // else done immediately
   if( WrittenBytes != (DWORD)Capabilities.OutputReportByteLength){
      return( FALSE);
   }
   return( TRUE);
} // WriteReport

//******************************************************************************
// Read report
//******************************************************************************

BOOL THid::ReadReport( void *Buffer)
// Read single report <Buffer>
{
   DWORD ReadBytes;
   if( !ReadFile( Handle, Report, Capabilities.InputReportByteLength, &ReadBytes,  &HidOverlapped)){
      if( GetLastError() != ERROR_IO_PENDING){
         return( FALSE);
      }
      DWORD Result = WaitForSingleObject( ReportEvent, HID_READ_TIMEOUT);
      if( Result == WAIT_TIMEOUT || Result == WAIT_ABANDONED){
         CancelIo( Handle);
         return( FALSE);
      }
      if( Result != WAIT_OBJECT_0){
         return( FALSE);
      }
      if( !GetOverlappedResult( Handle, &HidOverlapped, &ReadBytes, FALSE)){
         return( FALSE);
      }
   } // else done immediately
   if( ReadBytes != (DWORD)Capabilities.InputReportByteLength){
      return( FALSE);
   }
   // copy report, skip report ID :
   memcpy( Buffer, &Report[ 1], Capabilities.InputReportByteLength - 1);
   return( TRUE);
} // ReadReport

//******************************************************************************
// Flush
//******************************************************************************

BOOL THid::Flush()
// Flush input buffer
{
   return( HidD_FlushQueue( Handle));
} // Flush

//******************************************************************************
// Set input buffers
//******************************************************************************

BOOL THid::SetInputBuffers( int Count)
// Set input buffer reports <Count>
{
   return( HidD_SetNumInputBuffers( Handle, (ULONG)Count));
} // SetInputBuffers

//******************************************************************************
// Get input buffers
//******************************************************************************

int THid::GetInputBuffers()
// Returns input buffer reports count
{
   ULONG Count;
   if( !HidD_GetNumInputBuffers( Handle, &Count)){
      return( 0);
   }
   return( (int)Count);
} // GetInputBuffers

//******************************************************************************
// Report size
//******************************************************************************

int THid::GetOutputReportSize()
// Returns output report size
{
   return( Capabilities.OutputReportByteLength - 1);  // without ID byte
} // GetOutputReportSize

int THid::GetInputReportSize()
// Returns input report size
{
   return( Capabilities.InputReportByteLength - 1);   // without ID byte
} // GetInputReportSize

//******************************************************************************
// Device name
//******************************************************************************

char *THid::GetDeviceName()
// Returns USB device name
{
   return( DeviceName);
} // GetDeviceName

//******************************************************************************
// Manufacturer
//******************************************************************************

char *THid::GetManufacturer()
// Returns USB device manufacturer
{
   return( Manufacturer);
} // GetManufacturer

//******************************************************************************
// Serial number
//******************************************************************************

char *THid::GetSerialNumber()
// Returns USB device serial number
{
   return( SerialNumber);
} // GetSerialNumber

//******************************************************************************
// Get VID
//******************************************************************************

int THid::GetVid()
// Returns USB device VID
{
   return( DeviceVid);
} // GetVid

//******************************************************************************
// Get PID
//******************************************************************************

int THid::GetPid()
// Returns USB device PID
{
   return( DevicePid);
} // GetPid

//------------------------------------------------------------------------------

//******************************************************************************
// Get capabilities
//******************************************************************************

BOOL THid::GetCapabilities()
// Fill capabilities and strings
{
wchar_t String[ MAX_STRING + 1];
char    Ansi[ MAX_STRING + 1];

   // get device attributes :
   HIDD_ATTRIBUTES Attributes;
   Attributes.Size = sizeof( HIDD_ATTRIBUTES);
   if( !HidD_GetAttributes( TUsbBase::GetHandle(), &Attributes)){
      return( FALSE);
   }
   DeviceVid = Attributes.VendorID;
   DevicePid = Attributes.ProductID;
   // get device capability :
	PHIDP_PREPARSED_DATA HidParsedData;
	if( !HidD_GetPreparsedData( Handle, &HidParsedData)){
      return( FALSE);
   }
	if( !HidP_GetCaps( HidParsedData, &Capabilities)){
      HidD_FreePreparsedData( HidParsedData);
      return( FALSE);
   }
   HidD_FreePreparsedData( HidParsedData);
   // get product string :
   if( !HidD_GetProductString( Handle, String, MAX_STRING + 1)){
      return( FALSE);
   }
   if( !::WideCharToMultiByte( CP_ACP, 0, String,  -1, Ansi, MAX_STRING, NULL, NULL)){
      return( FALSE);
   }
   strncpy( DeviceName, Ansi, HID_DEVICE_NAME_SIZE);
   DeviceName[ HID_DEVICE_NAME_SIZE] = '\0';
   // get manufacturer string :
   if( !HidD_GetManufacturerString( Handle, String, MAX_STRING + 1)){
      return( FALSE);
   }
   if( !::WideCharToMultiByte( CP_ACP, 0, String,  -1, Ansi, MAX_STRING, NULL, NULL)){
      return( FALSE);
   }
   strncpy( Manufacturer, Ansi, HID_MANUFACTURER_SIZE);
   DeviceName[ HID_MANUFACTURER_SIZE] = '\0';
   // get serial number string :
   if( !HidD_GetSerialNumberString( Handle, String, MAX_STRING + 1)){
      return( FALSE);
   }
   if( !::WideCharToMultiByte( CP_ACP, 0, String,  -1, Ansi, MAX_STRING, NULL, NULL)){
      return( FALSE);
   }
   strncpy( SerialNumber, Ansi, HID_SERIAL_NUMBER_SIZE);
   DeviceName[ HID_SERIAL_NUMBER_SIZE] = '\0';
   return( TRUE);
} // GetCapabilities

