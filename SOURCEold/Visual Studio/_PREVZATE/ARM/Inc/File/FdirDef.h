//******************************************************************************
//
//   FdirDef.h    File directory common definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __FdirDef_H__
   #define __FdirDef_H__

#ifndef __Uni_H__
   #include "../Uni.h"
#endif

#ifndef __DtDef_H__
   #include "../DtDef.h"               // date and time
#endif

#ifndef __FatDef_H__
   #include "FatDef.h"
#endif

typedef byte  TFdirHandle;             // directory entry identification
typedef byte  TFdirClass;              // directory entry class
typedef int32 TFsSize;                 // file size (byte)

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

#define	FDIR_NAME_LENGTH	        15	// Maximum length of file name
#define	FDIR_NOTE_LENGTH	        15	// Maximum length of file note

#define FDIR_CLASS_DELETED               0      // Deleted item class
#define FDIR_CLASS_FIRST                 1      // First defined class
#define FDIR_CLASS_WILDCARD           0xFF      // Any class (for search only)

#define FDIR_DELETED_FLAG               '\0'    // Deleted item flag

#define FdirIsDeletedItem(  Item) ((Item).Name[0] == FDIR_DELETED_FLAG)
#define FdirSetDeletedItem( Item)  (Item).Name[0] =  FDIR_DELETED_FLAG

//------------------------------------------------------------------------------
// Data
//------------------------------------------------------------------------------

// dictionary item :
typedef struct {
   TFdirClass  Class;                          // file class
   TFdirHandle Handle;                         // file handle
   word        _Spare;
} TFdirDictionary;

// directory entry item :
typedef struct {
   char          Name[FDIR_NAME_LENGTH + 1];   // File name + ending zero
   char          Note[FDIR_NOTE_LENGTH + 1];   // File note + ending zero
   TFatIndex     Start;                        // FAT first block
   word          _Spare;
   TFsSize       Size;                         // file size (bytes)
   TTimestamp    Created;                      // Creation Date/Time
} TFdirInfo;

#define FDIR_INVALID    0      // invalid handle

#endif
