//*****************************************************************************
//
//    MemoryDef.h  -  External EEPROM structure
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __MemoryDef_H__
  #define __MemoryDef_H__

#ifndef __PAccuDef_H__
   #include "PAccuDef.h"     // Accumulator parameters
#endif

#ifndef __CalDef_H__
   #include "CalDef.h"       // Calibration
#endif

#ifndef __ConfigDef_H__
   #include "ConfigDef.h"    // Device configuration
#endif

#ifndef __CrashDef_H__
   #include "CrashDef.h"     // Crash data
#endif

#ifndef __FsDef_H__
   #include "../inc/File/FsDef.h"        // Filesystem definition
#endif

//-----------------------------------------------------------------------------
// Logo definition
//-----------------------------------------------------------------------------

#define LOGO_WIDTH  240
#define LOGO_HEIGHT 160
#define LOGO_PLANES 2
#define LOGO_ROWS   (LOGO_HEIGHT / 8)
#define LOGO_SIZE   LOGO_WIDTH * LOGO_ROWS

typedef byte TLogo[ LOGO_PLANES][ LOGO_ROWS][ LOGO_WIDTH];

//-----------------------------------------------------------------------------
// Definition
//-----------------------------------------------------------------------------

// Spare bytes at the end of EEPROM
#define EEP_SPARE       ( EEP_SIZE              \
                        - sizeof(TPAccuData)    \
                        - sizeof(TPAccuLogger)  \
                        - sizeof(TLogo)         \
                        - sizeof(TCalibration)  \
                        - sizeof(TConfigUnion)  \
                        - sizeof(TFsData)       \
                        - sizeof(TExceptionInfo)\
                        - sizeof(TWatchDogInfo))

typedef struct {
   TPAccuData     AccuData;                      // Accumulator maintenance data
   TPAccuLogger   AccuLogger;                    // Accumulator discharge curve
   TLogo          Logo;                          // Display logo
   TCalibration   Calibration;                   // Calibration data
   TConfigUnion   Config;                        // Device configuration
   byte           Spare[ EEP_SPARE];             // Spare bytes
   TExceptionInfo Exception;                     // Exception information
   TWatchDogInfo  WatchDog;                      // Watchdog information
   TFsData        Filesystem;                    // Filesystem
} TEeprom;

#endif
