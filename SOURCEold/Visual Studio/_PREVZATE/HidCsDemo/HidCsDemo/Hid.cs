﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Veit {


public class Hid {
    public const int REPORT_SIZE = 64;

    [DllImport( "HidDll.dll" , EntryPoint = "HidOpen")]
    public static extern bool Open( string DeviceName);
    // Open device by <Name>

    [DllImport( "HidDll.dll" , EntryPoint = "HidOpenBy")]
    public static extern bool OpenBy( int Vid, int Pid);
    // Open device by <Vid> & <Pid>

    [DllImport( "HidDll.dll" , EntryPoint = "HidClose")]
    public static extern void Close();
    // Close device

    [DllImport( "HidDll.dll" , EntryPoint = "HidWriteReport")]
    public static extern bool WriteReport( byte[] Buffer);
    // Write single report <Buffer>

    [DllImport( "HidDll.dll" , EntryPoint = "HidReadReport")]
    public static extern bool ReadReport( byte[] Buffer);
    // Read single report <Buffer>

    [DllImport( "HidDll.dll" , EntryPoint = "HidFlush")]
    public static extern bool Flush();
    // Flush input buffer

    [DllImport( "HidDll.dll", EntryPoint = "HidSetInputBuffers")]
    public static extern bool SetInputBuffers( int Count);
    // Set input buffer as <Count> reports

    [DllImport( "HidDll.dll", EntryPoint = "HidGetInputBuffers")]
    public static extern int GetInputBuffers();
    // Returns input buffer reports count

    [DllImport( "HidDll.dll", EntryPoint = "HidGetOutputReportSize" )]
    public static extern int GetOutputReportSize();
    // Returns output report size

    [DllImport( "HidDll.dll", EntryPoint = "HidGetInputReportSize" )]
    public static extern int GetInputReportSize();
    // Returns input report size

    [DllImport( "HidDll.dll", EntryPoint = "HidGetDeviceName" )]
    public static extern string GetDeviceName();
    // Returns USB device name

    [DllImport( "HidDll.dll", EntryPoint = "HidGetManufacturer" )]
    public static extern string GetManufacturer();
    // Returns USB device manufacturer

    [DllImport( "HidDll.dll", EntryPoint = "HidGetSerialNumber" )]
    public static extern string GetSerialNumber();
    // Returns USB device serial number

    [DllImport( "HidDll.dll", EntryPoint = "HidGetVid")]
    public static extern int GetVid();
    // Returns USB device VID

    [DllImport( "HidDll.dll", EntryPoint = "HidGetPid" )]
    public static extern int GetPid();
    // Returns USB device PID
} // class Hid

} // namespace veit
