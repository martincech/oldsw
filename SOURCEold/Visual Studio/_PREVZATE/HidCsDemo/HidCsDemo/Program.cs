﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Veit;

namespace HidCsDemo {
    class Program {
        const int MY_VID = 0x03EB;
        const int MY_PID = 0x2013;
        const string MY_DEVICE = "AVR USB HID DEMO";

        const int HIDDEMO_BUFFERS = 256;                 // input buffers count

        static void Main( string[] args )
        {
            ConsoleKeyInfo ch;
            Console.WriteLine("Start...");
            if( !Veit.Hid.Open( MY_DEVICE)){
                Console.WriteLine( "Unable open device '{0}'", MY_DEVICE);
                return; 
            }
/*            if( !Veit.Hid.OpenBy( MY_VID, MY_PID)){
                Console.WriteLine( "Unable open device VID=0x{0:X}, PID=0x{1:X}", MY_VID, MY_PID);
                return; 
            }
*/
            Console.WriteLine( "USB HID Device '{0}' opened", Veit.Hid.GetDeviceName());
            Console.WriteLine( "USB HID Device VID=0x{0:X}, PID=0x{1:X}", Veit.Hid.GetVid(), Veit.Hid.GetPid());
            // print capability :
            Console.WriteLine( "USB HID Input   Report size is {0}Byte", Veit.Hid.GetInputReportSize());
            Console.WriteLine( "USB HID Output  Report size is {0}Byte", Veit.Hid.GetOutputReportSize());
            Console.WriteLine( "USB HID Buffers count is       {0}",     Veit.Hid.GetInputBuffers());
            Console.WriteLine( "USB HID Device name is          '{0}'",  Veit.Hid.GetDeviceName());
            Console.WriteLine( "USB HID Device manufacturer  is '{0}'",  Veit.Hid.GetManufacturer());
            Console.WriteLine( "USB HID Device serial number is '{0}'",  Veit.Hid.GetSerialNumber());
            if( !Veit.Hid.SetInputBuffers( HIDDEMO_BUFFERS)){
                Console.WriteLine( "Unable set input buffers count");
            } else {
                Console.WriteLine( "USB HID Buffers count is  now  {0}", Veit.Hid.GetInputBuffers());
            }
            while ( true)
            {
                ch = Console.ReadKey(true);
                switch (ch.Key)
                {
                    case ConsoleKey.E :
                        byte[] OutBuffer = new byte[Veit.Hid.REPORT_SIZE];
                        byte[] InBuffer = new byte[Veit.Hid.REPORT_SIZE];
                        for( int i = 0; i < Veit.Hid.REPORT_SIZE; i++){
                            OutBuffer[ i] = (byte)i;
                        }
                        Console.WriteLine( "Write report...");
                        if( !Veit.Hid.WriteReport( OutBuffer)){
                            Console.WriteLine( "Unable write report");
                            break;
                        }
                        Console.WriteLine( "Read report...");
                        if( !Veit.Hid.ReadReport( InBuffer)){
                            Console.WriteLine( "Unable write report");
                            break;
                        }
                        Console.WriteLine( "Read done...");
                        bool Match = true;
                        for( int i = 0; i < Veit.Hid.REPORT_SIZE; i++){
                            if( InBuffer[ i] != OutBuffer[ i]){
                                Match = false;
                                Console.WriteLine( "Comparation mismatch");
                                break;
                            }
                        }
                        if( Match){
                            Console.WriteLine( "Comparation O.K.");
                            break;
                        }
                        break;

                    case ConsoleKey.F :
                        Veit.Hid.Flush();
                        Console.WriteLine( "Flush O.K.");
                        break;

                    case ConsoleKey.X :
                        Veit.Hid.Close();
                        Console.WriteLine( "USB HID device VID=0x{0:X}, PID=0x{1:X} closed\n", MY_VID, MY_PID);
                        Console.WriteLine( "Press a key...");
                        ch = Console.ReadKey(true);
                        return;

                    default :
                        Console.WriteLine( "E - Echo" );
                        Console.WriteLine( "F - Flush" );
                        Console.WriteLine( "X - Exit" );
                        break;
                } // switch
            } // forever
       } // Main
    } // class Program
} // namespace
