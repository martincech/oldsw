DLL Knihovna pro HID
--------------------

HIDDll - projekt pro vytvoreni knihovny
HIDLibTest - projekt pro test knihovny

1. Upravit kodovani stringu :
   Configuration Properties/General/Character Set 
   Use Multi-Byte Character Set
   
2. Doplnit definici symbolu :
   C/C++/Preprocesor/Preprocessor Definitions  
   _CRT_SECURE_NO_WARNINGS

3. Doplnit cestu do adresare Library :
   C/C++/General/Additional Include directories 
   ../Library

4. HIDDll doplnit knihovny :
   Linker/Input/Additional Depedencies 
   ../../HidUsbTest/Library/USB/HID/HID.lib
   ../../HidUsbTest/Library/USB/setupapi.lib

5. HIDLibTest doplnit knihovnu :
   ../Debug/HidDll.lib
   