//******************************************************************************
//
//   HidDll.cpp   HID DLL library main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <windows.h>
#pragma hdrstop

#include <string.h>
#include <stdio.h>

#include "HidDll.h"
#include "USB/HID/HID.h"               // C++ HID object

// debug options :
#define __DEBUG__
#ifdef __DEBUG__
   #define TRACE( msg)       printf( msg "\n")
   #define TRACE1( msg, p1)  printf( msg "\n", p1)
#else
   #define TRACE( msg)
   #define TRACE1( msg, p1)
#endif

THid *Hid;        // library global object

//-----------------------------------------------------------------------------
//   Main
//-----------------------------------------------------------------------------

BOOL APIENTRY DllMain( HANDLE hModule, DWORD dwReason, LPVOID lpReserved)
{
   // process initialisation :
   if( dwReason == DLL_PROCESS_ATTACH){
      TRACE( "DllMain : initialisation");
      // Create object :
      Hid = new THid();
      return( 1);
   }
   // process shutdown :
   if( dwReason == DLL_PROCESS_DETACH){
      TRACE( "DllMain : terminate");
      delete Hid;
      return( 1);
   }
   // thread maipulations :
   if( dwReason == DLL_THREAD_ATTACH){
      TRACE( "DllMain : thread attach");
      return( 1);
   } 
   if( dwReason == DLL_THREAD_DETACH){
      TRACE( "DllMain : thread detach");
      return( 1);
   }
   // unknown reason :
   TRACE1( "DllMain : unknown reason %d", dwReason);
   return( 1);
} // DllMain

//-----------------------------------------------------------------------------
//   Functions
//-----------------------------------------------------------------------------

BOOL APIENTRY HidOpen( char *DeviceName)
// Open device by <Name>
{
   return( Hid->Open( DeviceName));
} // HidOpen

//-----------------------------------------------------------------------------
//   Open by PID & VID
//-----------------------------------------------------------------------------

BOOL APIENTRY HidOpenBy( int Vid, int Pid)
// Open device by <Vid> & <Pid>
{
   return( Hid->OpenBy( Vid, Pid));
} // HidOpenBy

//-----------------------------------------------------------------------------
//   Close
//-----------------------------------------------------------------------------

void APIENTRY HidClose( void)
// Close device
{
   Hid->Close();
} // HidClose

//-----------------------------------------------------------------------------
//   Write report
//-----------------------------------------------------------------------------

BOOL APIENTRY HidWriteReport( void *Buffer)
// Write single report <Buffer>
{
   return( Hid->WriteReport( Buffer));
} // HidWriteReport

//-----------------------------------------------------------------------------
//   Read report
//-----------------------------------------------------------------------------

BOOL APIENTRY HidReadReport( void *Buffer)
// Read single report <Buffer>
{
   return( Hid->ReadReport( Buffer));
} // HidReadReport

//-----------------------------------------------------------------------------
//   Flush
//-----------------------------------------------------------------------------

BOOL APIENTRY HidFlush( void)
// Flush input buffer
{
   return( Hid->Flush());
} // HidFlush

//-----------------------------------------------------------------------------
//   Set input buffers
//-----------------------------------------------------------------------------

BOOL APIENTRY HidSetInputBuffers( int Count)
// Set input buffer as <Count> reports
{
   return( Hid->SetInputBuffers( Count));
} // HidSetInputBuffers

//-----------------------------------------------------------------------------
//   Get input buffers
//-----------------------------------------------------------------------------

int APIENTRY HidGetInputBuffers( void)
// Returns input buffer reports count
{
   return( Hid->GetInputBuffers());
} // HidGetInputBuffers

//-----------------------------------------------------------------------------
//   Get output report size
//-----------------------------------------------------------------------------

int APIENTRY HidGetOutputReportSize( void)
// Returns output report size
{
   return( Hid->GetOutputReportSize());
} // HidGetOutputReportSize

//-----------------------------------------------------------------------------
//   Get input report size
//-----------------------------------------------------------------------------

int APIENTRY HidGetInputReportSize( void)
// Returns input report size
{
   return( Hid->GetInputReportSize());
} // HidGetInputReportSize

//-----------------------------------------------------------------------------
//   Get device name
//-----------------------------------------------------------------------------

char * APIENTRY HidGetDeviceName( void)
// Returns USB device name
{
   return( Hid->GetDeviceName());
} // HidGetDeviceName

//-----------------------------------------------------------------------------
//   Get manufacturer
//-----------------------------------------------------------------------------

char * APIENTRY HidGetManufacturer( void)
// Returns USB device manufacturer
{
   return( Hid->GetManufacturer());
} // HidGetManufacturer

//-----------------------------------------------------------------------------
//   Get serial number
//-----------------------------------------------------------------------------

char * APIENTRY HidGetSerialNumber( void)
// Returns USB device serial number
{
   return( Hid->GetSerialNumber());
} // HidGetSerialNumber

//-----------------------------------------------------------------------------
//   Get VID
//-----------------------------------------------------------------------------

int APIENTRY HidGetVid( void)
// Returns USB device VID
{
   return( Hid->GetVid());
} // HidGetVid

//-----------------------------------------------------------------------------
//   Get PID
//-----------------------------------------------------------------------------

int APIENTRY HidGetPid( void)
// Returns USB device PID
{
   return( Hid->GetPid());
} // HidGetPid
