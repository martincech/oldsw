//******************************************************************************
//
//   HidDll.h     HID DLL library
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef HidDllH
   #define HidDllH

#ifdef HIDDLL_EXPORTS
   #define DLL_IMPORT extern "C" __declspec( dllexport)
#else
   #define DLL_IMPORT extern "C" __declspec( dllimport)
#endif

#define HID_REPORT_SIZE        64  // maximum report size

//-----------------------------------------------------------------------------
//   Functions
//-----------------------------------------------------------------------------

DLL_IMPORT BOOL APIENTRY HidOpen( char *DeviceName);
// Open device by <Name>

DLL_IMPORT BOOL APIENTRY HidOpenBy( int Vid, int Pid);
// Open device by <Vid> & <Pid>

DLL_IMPORT void APIENTRY HidClose( void);
// Close device

DLL_IMPORT BOOL APIENTRY HidWriteReport( void *Buffer);
// Write single report <Buffer>

DLL_IMPORT BOOL APIENTRY HidReadReport( void *Buffer);
// Read single report <Buffer>

DLL_IMPORT BOOL APIENTRY HidFlush( void);
// Flush input buffer

DLL_IMPORT BOOL APIENTRY HidSetInputBuffers( int Count);
// Set input buffer as <Count> reports

DLL_IMPORT int APIENTRY HidGetInputBuffers( void);
// Returns input buffer reports count

DLL_IMPORT int APIENTRY HidGetOutputReportSize( void);
// Returns output report size

DLL_IMPORT int APIENTRY HidGetInputReportSize( void);
// Returns input report size

DLL_IMPORT char * APIENTRY HidGetDeviceName( void);
// Returns USB device name

DLL_IMPORT char * APIENTRY HidGetManufacturer( void);
// Returns USB device manufacturer

DLL_IMPORT char * APIENTRY HidGetSerialNumber( void);
// Returns USB device serial number

DLL_IMPORT int APIENTRY HidGetVid( void);
// Returns USB device VID

DLL_IMPORT int APIENTRY HidGetPid( void);
// Returns USB device PID

#endif
