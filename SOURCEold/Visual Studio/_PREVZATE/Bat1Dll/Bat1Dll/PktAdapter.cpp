//******************************************************************************
//
//   PktAdapter.cpp   Packet adapter
//   Version 0.0      (c) VymOs
//
//******************************************************************************

#include <windows.h>
#include <stdio.h>

#include "../../Library/Unisys/Uni.h"
#include "PktAdapter.h"
#include <typeinfo.h>
#include <string.h>

// Configuration names :

#define USB_DEVICE_NAME   "VEIT BAT1 Poultry Scale"        // USB adapter name
#define USB_DUTCHMAN_NAME "FlexScale"                      // USB adapter name

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( Logger){                                             \
      Logger->Write( TLogger::TX_DATA, Buffer, Length);     \
   }
#define RwdRx( Buffer, Length)                              \
   if( Logger){                                             \
      Logger->Write( TLogger::RX_DATA, Buffer, Length);     \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( Logger){                                             \
      Logger->Write( TLogger::RX_GARBAGE, Buffer, Length);  \
   }
#define RwdReport( Text)                                    \
   if( Logger){                                             \
      Logger->Report( Text"\n");                            \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TPktAdapter::TPktAdapter()
// Constructor
{
   RxTimeout               = 2000;
   RxIntercharacterTimeout = 50;
   RxPacketTimeout         = 1000;

   Logger     = 0;

   // default parameters :
   Parameters.BaudRate     = 38400;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUsbUart::NO_PARITY;
   Parameters.Handshake    = TUsbUart::NO_HANDSHAKE;
   strcpy( UsbDevice, "?");

   FtdiSerialNumber = 0;

   FtdiLocId = 0;

   FPort       = 0;
} // TPktAdapter

//******************************************************************************
// Destructor
//******************************************************************************

TPktAdapter::~TPktAdapter()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TPktAdapter

//******************************************************************************
// Send
//******************************************************************************

BOOL TPktAdapter::Send( int Command, int Data)
// Send message
{
TShortPacket Packet;

   if( !CheckConnect()){
      RwdReport( "CheckConnect failed");
      return( FALSE);
   }
   Packet.Start    = CPKT_SHORT_START;
   Packet.Cmd      = (byte)Command;
   Packet.Data     = (dword)Data;
   Packet.Crc      = CalcCrc( &Packet.Cmd, 5);
   Packet.End      = CPKT_SHORT_END;
   FPort->Flush();
   if( FPort->Write( &Packet, sizeof( Packet)) != sizeof( Packet)){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( FALSE);
   }
   RwdTx( &Packet, sizeof( Packet));
   return( TRUE);
} // Send

//******************************************************************************
// Send data
//******************************************************************************

BOOL TPktAdapter::SendData( void *Data, int Length)
// Send long data
{
   if( !CheckConnect()){
      return( FALSE);
   }
   if( Length > PACKET_MAX_DATA){
      IERROR;
   }
   TDataPacketStart *Start = (TDataPacketStart *)Buffer;
   Start->Start    = CPKT_DATA_START;
   Start->Size1    = (word)Length;
   Start->Size2    = Start->Size1;
   Start->Header   = CPKT_DATA_START;
   memcpy( Start+1, Data, Length);
   TDataPacketEnd   *End   = (TDataPacketEnd *)&Buffer[ sizeof(TDataPacketStart) + Length];
   End->Crc        = CalcCrc( (byte *)(Start + 1), Length);
   End->End        = CPKT_DATA_END;
   FPort->Flush();
   int ReqSize = CPKT_PACKET_SIZE( Length);
   if( FPort->Write( Start, ReqSize) != ReqSize){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( FALSE);
   }
   RwdTx( Start, ReqSize);
   return( TRUE);
} // SendData

//******************************************************************************
// Receive
//******************************************************************************

BOOL TPktAdapter::Receive( int &Command, int &Data)
// Receive message
{
int Size, ReqSize;

   if( !CheckConnect()){
      return( FALSE);
   }
   TShortPacket *Packet = (TShortPacket *)Buffer;
   FPort->SetRxWait( RxTimeout, 0);    // wait for first char
   if( FPort->Read( Packet, 1) != 1){
      return( FALSE);
   }
   if( Packet->Start != CPKT_SHORT_START &&
       Packet->Start != CPKT_DATA_START){
      RwdGarbage( &Packet, 1);
      FlushRxChars();
      return( FALSE);
   }
   FPort->SetRxWait( RxPacketTimeout, RxIntercharacterTimeout);
   if( Packet->Start == CPKT_SHORT_START){
      // short packet
      ReqSize = sizeof( *Packet) - 1;
      Size = FPort->Read( &Packet->Cmd, ReqSize);
      if(  Size != ReqSize){
         RwdGarbage( Packet, Size + 1);
         return( FALSE);
      }
      if( Packet->End != CPKT_SHORT_END){
         RwdGarbage( Packet, Size);
         return( FALSE);
      }
      if( Packet->Crc != CalcCrc( &Packet->Cmd, 5)){
         RwdGarbage( Packet, Size);
         return( FALSE);
      }
      Command  = Packet->Cmd;
      Data     = Packet->Data;
      RwdRx( Packet, sizeof( *Packet));
      return( TRUE);
   }
   // long packet
   TDataPacketStart *Start = (TDataPacketStart *)Buffer;
   ReqSize = sizeof( *Start) - 1;         // first char done
   Size = FPort->Read( &Start->Size1, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, Size + 1);
      return( FALSE);
   }
   if( Start->Header != CPKT_DATA_START){
      RwdGarbage( Start, Size);
      return( 0);
   }
   if( Start->Size1 != Start->Size2){
      RwdGarbage( Start, Size);
      return( FALSE);
   }
   ReqSize = (int)Start->Size1;
   byte *DataBuffer = &Buffer[ sizeof(*Start)];
   Size =  FPort->Read( DataBuffer, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, Size + sizeof( *Start));
      return( FALSE);
   }
   TDataPacketEnd *End = (TDataPacketEnd *)&Buffer[ sizeof( *Start) + Size];
   ReqSize = sizeof( *End);
   Size = FPort->Read( End, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( FALSE);
   }
   if( End->End != CPKT_DATA_END){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( 0);
   }
   if( End->Crc != CalcCrc( DataBuffer, Start->Size1)){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( FALSE);
   }
   Command = DATA_MESSAGE;
   Data    = 0;
   RwdRx( Start, sizeof( *Start) + Start->Size1 + sizeof( *End));
   return( TRUE);
} // Receive

//******************************************************************************
// Get long message
//******************************************************************************

void TPktAdapter::GetData( int &Length, void *Data)
// Get data of long message
{
   TDataPacketStart *Start = (TDataPacketStart *)Buffer;
   unsigned Size = Start->Size1;
   Length   = Size;
   if( Data){
      memcpy( Data, &Buffer[ sizeof(*Start)], Size);
   }
} // GetData

//******************************************************************************
// Property IsOpen
//******************************************************************************

BOOL TPktAdapter::GetIsOpen()
// returns true if device opened
{
   if( !FPort){
      return( FALSE);
   }
   if( !FPort->IsOpen){
      return( FALSE);
   }
   return( TRUE);
} // GetIsOpen()

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Flush
//******************************************************************************

#define FLUSH_DATA_SIZE 1024

void TPktAdapter::FlushRxChars()
// Flush chars up to intercharacter timeout
{
byte Data[ FLUSH_DATA_SIZE];

   FPort->SetRxWait( RxIntercharacterTimeout, RxIntercharacterTimeout);
   for( int i = 0; i < FLUSH_DATA_SIZE; i++){
      if( FPort->Read( &Data[ i], 1) == 0){
         // timeout
         if( i > 0){
            RwdGarbage( Data, i);
         }
         return;
      }
   }
   RwdGarbage( Data, FLUSH_DATA_SIZE);
} // FlushRxChars

//******************************************************************************
// Check Connection
//******************************************************************************

BOOL TPktAdapter::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( TRUE);
   }
   if( FPort){
      IERROR;                          // always opened !
   }

   TUsbUart *Usb = 0;
   // USB setup
   Usb = new TUsbUart;
   strcpy( UsbDevice, USB_DEVICE_NAME);          // default name

   if(FtdiLocId == 0) {
      if( !Usb->Open( USB_DEVICE_NAME)){
         // default name not found, try alternate name
         strcpy( UsbDevice, USB_DUTCHMAN_NAME);     // Dutchman name
         if( !Usb->Open( USB_DUTCHMAN_NAME)){
            strcpy( UsbDevice, "?");                // unknown name
            delete Usb;
            return( FALSE);                         // no devices found
         }
      }
   } else {
      if( !Usb->Open( FtdiLocId)){
            strcpy( UsbDevice, "?");                // unknown name
            delete Usb;
            return( FALSE);                         // no devices found
      }
   }

   Usb->SetParameters( Parameters);
   // common init :
   Usb->SetDTR( FALSE);
   Usb->SetRTS( FALSE);
   Usb->SetRxNowait();
   Usb->Flush();
   FPort = Usb;
   return( TRUE);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TPktAdapter::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

//******************************************************************************
// CRC
//******************************************************************************

byte TPktAdapter::CalcCrc( byte *Data, int Size)
// Calculate CRC
{
byte Crc = 0;
int  i;

   for( i = 0; i < Size; i++){
      Crc += *Data;
      Data++;
   }
   return( ~Crc);
} // CalcCrc
