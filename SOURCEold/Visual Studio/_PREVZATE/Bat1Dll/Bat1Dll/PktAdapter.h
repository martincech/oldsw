//******************************************************************************
//
//   PktAdapter.h     Packet adapter
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef PktAdapterH
   #define PktAdapterH

#ifndef UsbUartH
   #include "../../Library/USB/UsbUart.h"
#endif

#ifndef LoggerH
   #include "../../Library/Logger/Logger.h"
#endif

#include "../../ARM/Inc/CPktDef.h"                   // protocol definition

#define PACKET_MAX_DATA   1024                       // max. packet data size

//******************************************************************************
// TPktAdapter
//******************************************************************************

class TPktAdapter {
public :
   typedef enum {
      DATA_MESSAGE  = 0x8000,           // special command type - data message
   } TPacketType;

   TPktAdapter();
   // Constructor

   ~TPktAdapter();
   // Destructor

   BOOL Send( int Command, int Data);
   // Send message

   BOOL SendData( void *Data, int Lenght);
   // Send long data

   BOOL Receive( int &Command, int &Data);
   // Receive message

   void GetData( int &Length, void *Data);
   // Get data of long message
   
   void Disconnect();
   // Disconnect port

   //--------- Properties
   int RxTimeout;
   int RxIntercharacterTimeout;
   int RxPacketTimeout;
   BOOL GetIsOpen();        // returns true if device opened

   int FtdiSerialNumber;
   unsigned int FtdiLocId;

   TLogger               *Logger;
   TUsbUart::TParameters  Parameters;
   char                   UsbDevice[ 80]; // USB device name
//------------------------------------------------------------------------------

protected :
   TUsbUart *FPort;                       // connection port
   byte     Buffer[ CPKT_PACKET_SIZE( PACKET_MAX_DATA)]; // packet buffer

   void FlushRxChars();
   // Flush chars up to intercharacter timeout

   BOOL CheckConnect();
   // Check if adapter is ready

   byte CalcCrc( byte *Data, int Size);
   // Calculate CRC
};

#endif
