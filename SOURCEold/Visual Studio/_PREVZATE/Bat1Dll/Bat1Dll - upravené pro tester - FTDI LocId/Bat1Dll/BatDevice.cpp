//******************************************************************************
//
//   BatDevice.cpp    Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <windows.h>
#include <stdio.h>

#include "../../Library/Unisys/Uni.h"
#include "BatDevice.h"

//******************************************************************************
// Constructor
//******************************************************************************

TBatDevice::TBatDevice()
// Constructor
{
   Adapter   = 0;
   WriteVerification = FALSE;
} // TBatDevice

//******************************************************************************
// Destructor
//******************************************************************************

TBatDevice::~TBatDevice()
// Destructor
{
} // ~TBatDevice

//******************************************************************************
// Check
//******************************************************************************

BOOL TBatDevice::Check()
// Check for device
{
   int Version;
   return( GetVersion( Version));
} // Check

//******************************************************************************
// Close
//******************************************************************************

BOOL TBatDevice::Close()
// Close device
{
   Adapter->Disconnect();
} // Check

//******************************************************************************
// Version
//******************************************************************************

BOOL TBatDevice::GetVersion( int &Version)
// Get bat version
{
   int Cmd  = USB_CMD_VERSION;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   Version = Data;
   return( TRUE);
} // GetVersion

//******************************************************************************
// Status
//******************************************************************************

BOOL TBatDevice::GetStatus( BOOL &PowerOn)
// Get power status
{
   int Cmd  = USB_CMD_GET_STATUS;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   PowerOn = Data;
   return( TRUE);
} // GetStatus

//******************************************************************************
// Power off
//******************************************************************************

BOOL TBatDevice::PowerOff()
// Switch power off
{
   int Cmd  = USB_CMD_POWER_OFF;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   return( TRUE);
} // PowerOff

//******************************************************************************
// Reload Configuration
//******************************************************************************

BOOL TBatDevice::ReloadConfig()
// Reload configuration by EEPROM
{
   int Cmd  = USB_CMD_RELOAD;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   return( TRUE);
} // ReloadConfig

//******************************************************************************
// Get Time
//******************************************************************************

BOOL TBatDevice::GetTime( TTimestamp &Timestamp)
// Get device time
{
   int Cmd  = USB_CMD_GET_TIME;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   Timestamp = Data;
   return( TRUE);
} // GetTime

//******************************************************************************
// Set Time
//******************************************************************************

BOOL TBatDevice::SetTime( TTimestamp Timestamp)
// Set device time
{
   int Cmd  = USB_CMD_SET_TIME;
   int Data = Timestamp;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   return( TRUE);
} // SetTime

//******************************************************************************
// Read Memory
//******************************************************************************

BOOL TBatDevice::ReadMemory( int Address, void *Buffer, int Size)
// Read EEPROM
{
   if( !Adapter){
      return( FALSE);
   }
   int Command = USB_CMD_ADDRESS;
   int Data    = Address;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   // Split into packets :
   int Length = Size > USB_MAX_DATA ? USB_MAX_DATA : Size;
   int TotalLength = Size;
   int RLength;
   int Offset      = 0;
   do {
      Data    = Length;
      Command = USB_CMD_READ;
      if( !SendCommand( Command, Data)){
         return( FALSE);
      }
      if( Command != TPktAdapter::DATA_MESSAGE){
         return( FALSE);
      }
      // check for length :
      Adapter->GetData( RLength, 0);
      if( RLength != Length){
         return( FALSE);
      }
      // copy data :
      Adapter->GetData( RLength, &((char *)Buffer)[ Offset]);
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_MAX_DATA){
         Length = USB_MAX_DATA;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   if( Data != (Address + Size)){
      return( FALSE);
   }
   return( TRUE);
} // ReadMemory

//******************************************************************************
// Write Memory
//******************************************************************************

BOOL TBatDevice::WriteMemory( int Address, void *Buffer, int Size)
// Write EEPROM data
{
   if( !Adapter){
      return( FALSE);
   }
   // set address counter before write :
   int Command = USB_CMD_ADDRESS;
   int Data    = Address;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   // Split into packets :
   int Length = Size > USB_WRITE_SIZE ? USB_WRITE_SIZE : Size;
   int TotalLength = Size;
   int Offset      = 0;
   do {
      if( !WriteFragment( USB_CMD_WRITE, &((char *)Buffer)[ Offset], Length)){
         return( FALSE);
      }
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_WRITE_SIZE){
         Length = USB_WRITE_SIZE;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   if( Data != (Address + Size)){
      return( FALSE);
   }
   //----- verification :
   if( !WriteVerification){
      return( TRUE);          // without verification
   }
   byte *TmpBuffer  = new byte[ Size];
   if( !ReadMemory( Address, TmpBuffer, Size)){
      delete[] TmpBuffer;
      return( FALSE);
   }
   if( !memequ( Buffer, TmpBuffer, Size)){
      delete[] TmpBuffer;
      return( FALSE);        // different contents
   }
   delete[] TmpBuffer;
   return( TRUE);
} // WriteMemory

//******************************************************************************
// Directory Begin
//******************************************************************************

BOOL TBatDevice::DirectoryBegin( int &Count)
// Read directory begin. Returns <Count> of directory items
{
   if( !Adapter){
      return( FALSE);
   }
   int Cmd  = USB_CMD_DIRECTORY_BEGIN;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   Count = Data;
   return( TRUE);
} // DirectoryBegin

//******************************************************************************
// Directory Next
//******************************************************************************

BOOL TBatDevice::DirectoryNext( TUsbDirectoryItem *Item)
// Read directory entry. Fills <Item>
{
   if( !Adapter){
      return( FALSE);
   }
   int Command = USB_CMD_DIRECTORY_NEXT;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   if( Command != TPktAdapter::DATA_MESSAGE){
      return( FALSE);
   }
   // check for length :
   int RLength;
   Adapter->GetData( RLength, 0);
   if( RLength != sizeof( TUsbDirectoryItem)){
      return( FALSE);
   }
   // copy data :
   Adapter->GetData( RLength, Item);
   return( TRUE);
} // DirectoryNext

//******************************************************************************
// File Open
//******************************************************************************

BOOL TBatDevice::FileOpen( int Handle)
// Open file by <Handle>
{
   if( !Adapter){
      return( FALSE);
   }
   int Cmd  = USB_CMD_FILE_OPEN;
   int Data = Handle;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   return( TRUE);
} // FileOpen

//******************************************************************************
// File Close
//******************************************************************************

BOOL TBatDevice::FileClose()
// Close Current file
{
   if( !Adapter){
      return( FALSE);
   }
   int Cmd  = USB_CMD_FILE_CLOSE;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   return( TRUE);
} // FileClose

//******************************************************************************
// File Create
//******************************************************************************

BOOL TBatDevice::FileCreate( char *Name, char *Note, int Class, int &Handle)
// Create file by <Name>, <Note>, <Class>. Returns <Handle>
{
   int RCommand, RData;
   TUsbCmdUnion  CmdUnion;
   // send create data :
   CmdUnion.Create.Cmd   = USB_CMD_FILE_CREATE;
   CmdUnion.Create.Class = Class;
   strcpy( CmdUnion.Create.Name, Name);
   strcpy( CmdUnion.Create.Note, Note);
   if( !Adapter->SendData( &CmdUnion, sizeof( TUsbCmdFileCreate))){
      return( FALSE);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( FALSE);
   }
   if( !(RCommand & USB_CMD_REPLY)){
      return( FALSE);
   }
   if( (RCommand & ~USB_CMD_REPLY) != USB_CMD_FILE_CREATE){
      return( FALSE);
   }
   Handle = RData;
   return( TRUE);
} // FileCreate

//******************************************************************************
// File Read
//******************************************************************************

BOOL TBatDevice::FileRead( void *Buffer, int Size)
// Read file
{
   if( !Adapter){
      return( FALSE);
   }
   // clear address counter before read :
   int Command = USB_CMD_ADDRESS;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   // Split into packets :
   int Length = Size > USB_MAX_DATA ? USB_MAX_DATA : Size;
   int TotalLength = Size;
   int RLength;
   int Offset      = 0;
   do {
      Data    = Length;
      Command = USB_CMD_FILE_READ;
      if( !SendCommand( Command, Data)){
         return( FALSE);
      }
      if( Command != TPktAdapter::DATA_MESSAGE){
         return( FALSE);
      }
      // check for length :
      Adapter->GetData( RLength, 0);
      if( RLength != Length){
         return( FALSE);
      }
      // copy data :
      Adapter->GetData( RLength, &((char *)Buffer)[ Offset]);
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_MAX_DATA){
         Length = USB_MAX_DATA;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   if( Data != Size){
      return( FALSE);
   }
   return( TRUE);
} // FileRead

//******************************************************************************
// File Write
//******************************************************************************

BOOL TBatDevice::FileWrite( void *Buffer, int Size)
// Write file
{
   if( !Adapter){
      return( FALSE);
   }
   // clear address counter before write :
   int Command = USB_CMD_ADDRESS;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   // Split into packets :
   int Length = Size > USB_WRITE_SIZE ? USB_WRITE_SIZE : Size;
   int TotalLength = Size;
   int Offset      = 0;
   do {
      if( !WriteFragment( USB_CMD_FILE_WRITE, &((char *)Buffer)[ Offset], Length)){
         return( FALSE);
      }
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_WRITE_SIZE){
         Length = USB_WRITE_SIZE;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( FALSE);
   }
   if( Data != Size){
      return( FALSE);
   }
   return( TRUE);
} // FileWrite

//******************************************************************************
// Format
//******************************************************************************

BOOL TBatDevice::FormatFilesystem()
// Format filesystem
{
   int Cmd  = USB_CMD_FORMAT;
   int Data = USB_FORMAT_MAGIC;
   if( !SendCommand( Cmd, Data)){
      return( FALSE);
   }
   return( TRUE);
} // FormatFilesystem

//------------------------------------------------------------------------------

//******************************************************************************
// Send command
//******************************************************************************

BOOL TBatDevice::SendCommand( int &Command, int &Data)
// Send command with error recovery
{
   if( !Adapter){
      return( FALSE);
   }
   int RCommand, RData;
   if( !Adapter->Send( Command, Data)){
      return( FALSE);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( FALSE);
   }
   if( RCommand != TPktAdapter::DATA_MESSAGE &&
      (RCommand & ~USB_CMD_REPLY) != Command){
      return( FALSE);
   }
   Data    = RData;
   Command = RCommand;
   return( TRUE);
} // SendCommand

//******************************************************************************
// Write Fragment
//******************************************************************************

BOOL TBatDevice::WriteFragment( int WriteCommand, void *Buffer, int Size)
// Write fragment of data with recovery
{
   int RCommand, RData;
   TUsbCmdUnion  CmdUnion;
   // send data :
   CmdUnion.Write.Cmd = WriteCommand;
   memcpy( CmdUnion.Write.Data, Buffer, Size);
   if( !Adapter->SendData( &CmdUnion, Size + USB_CMD_SIZE)){
      return( FALSE);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( FALSE);
   }
   if( !(RCommand & USB_CMD_REPLY)){
      return( FALSE);
   }
   if( (RCommand & ~USB_CMD_REPLY) != WriteCommand){
      return( FALSE);
   }
   return( TRUE);
} // WriteFragment
