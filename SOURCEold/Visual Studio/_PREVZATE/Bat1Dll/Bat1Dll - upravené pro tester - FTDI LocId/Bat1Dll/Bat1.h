//******************************************************************************
//
//   Bat1.h           Bat1 advanced services
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef Bat1H
   #define Bat1H

#ifndef BatDeviceH
   #include "BatDevice.h"
#endif

#ifndef __MemoryDef_H__
   #include "../../ARM/Bat1/MemoryDef.h"
#endif

#ifndef __SdbDef_H__
   #include "../../ARM/Bat1/SdbDef.h"
#endif

#ifndef __GroupDef_H__
   #include "../../ARM/Bat1/GroupDef.h"
#endif

//******************************************************************************
// Bat1
//******************************************************************************

class TBat1 : public TBatDevice {
public :
   TBat1();
   // constructor
   ~TBat1();
   // destructor

   void NewDevice();
   // Create empty device

   BOOL Load( BOOL ReadSamples);
   // Load data from Bat1

   BOOL Save();
   // Save data to Bat1

   BOOL LoadEstimation( int &Promile);
   // Load size estimation [%%]

   void SaveEstimation( int &Promile);
   // Save size estimation [%%]

   //------------------- Crash info

   BOOL LoadCrashInfo();
   // Read exception & watchdog info

   //------------------- utility

   BOOL ReadLargeMemory( int Address, void *Buffer, int Size);
   // Read large chunk of the EEPROM

   BOOL WriteLargeMemory( int Address, void *Buffer, int Size);
   // Write large chunk of the EEPROM data

   void FilesDelete();
   // Delete all files

   void GroupsDelete();
   // Delete all groups

   void AllocSamples( int FileIndex, int SamplesCount);
   // Alloc memory for samples

   void ClearSamples( int FileIndex);
   // Clear all samples

   BOOL DeviceByEeprom( void *Buffer);
   // Load device by EEPROM contents in <Buffer>

   //------------------- properties
   BOOL TouchConfig;
   BOOL TouchFilesystem;

   // device configuration
   TConfigUnion   Config;                       // Device configuration
   TExceptionInfo ExceptionInfo;                // Crash exception info
   TWatchDogInfo  WatchDogInfo;                 // Crash watchdog info

   // file entry
   typedef struct {
      TUsbDirectoryItem Info;          // directory item
      TSdbConfig        Config;        // file config
      BOOL              Current;       // working file
      int               SamplesCount;  // file samples count
      TSdbRecord       *Sample;        // file samples
   } TFileEntry;

   TFileEntry File[ FDIR_SIZE];        // data files
   int        FilesCount;              // data files count

   // group entry
   typedef struct {
      TUsbDirectoryItem Info;          // directory item
      TGroup            Group;         // group contents
      int               FilesCount;    // number of files
      TFdirHandle       FileIndex[ FDIR_SIZE];  // file list
   } TGroupEntry;

   TGroupEntry Group[ FDIR_SIZE];      // groups
   int         GroupsCount;            // groups count

   //------------------- utility
   void DecodeTime( TTimestamp Timestamp, int &Day, int &Month, int &Year, int &Hour, int &Min, int &Sec);
   // Decode <Timestamp> to items

   TTimestamp EncodeTime( int Day, int Month, int Year, int Hour, int Min, int Sec);
   // Returns timestamp by items

//-----------------------------------------------------------------------------
protected :
   void Dispose();
   // Dispose allocated memory

   void CalcConfigCrc();
   // Recalculate Config CRC

   void LoadGroup( int GroupIndex);
   // Decode group data

   void SaveGroup( int GroupIndex);
   // Encode group data

   void GroupAdd( TGroup Group, int Handle);
   // Add <Handle> to <Group>

   BOOL GroupContains( TGroup Group, int Handle);
   // Returns TRUE if <Handle> is in the <Group>
}; // TBat1

#endif
