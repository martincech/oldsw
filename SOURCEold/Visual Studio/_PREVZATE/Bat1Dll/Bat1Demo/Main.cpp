/*****************************************************************************/
/*                                                                           */
/*  Main.cpp     Bat1 DLL demo main                                          */
/*  Version 1.0  (c) VymOs                                                   */
/*                                                                           */
/*****************************************************************************/

#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <string.h>
#include <direct.h>

#include "Hardware.h"
#include "../../Bat1Dll/Bat1Dll/Bat1Dll.h"
#include "../../ARM/Bat1/PWeighingDef.h"
#include "../../ARM/Inc/DtDef.h"
#include "../../ARM/Bat1/MemoryDef.h"

TEeprom Memory;

//-----------------------------------------------------------------------------
//  Main
//-----------------------------------------------------------------------------

void main( void)
{
int ch;
int Index;
int PowerOn;
int Ts, Day, Month, Year, Hour, Min, Sec;
FILE *f;
char Buf[ 255];

   EnableLogger( TRUE);
   printf( "Start...\n");
   forever {
      ch = _getch();
      switch( ch){
         case 'c' :
            if( !CheckDevice()){
               printf( "Device not present\n");
               break;
            }
            printf( "Device OK\n");
            break;

         case 'l' :
            // load
            if( !DeviceIsOn( &PowerOn)){
               printf( "ERROR : unable get status\n");
               break;
            }
            if( PowerOn){
               if( !DevicePowerOff()){
                  printf( "ERROR : unable switch power off\n");
                  break;
               }
            }
            if( !LoadDevice()){
               printf( "ERROR : unable load\n");
               break;
            }
            Ts = GetFileCreation( 0);
            DecodeTime( Ts, &Day, &Month, &Year, &Hour, &Min, &Sec);
            printf( "FILE created : %02d.%02d.%04d %02d:%02d:%02d\n", Day, Month, Year, Hour, Min, Sec);
            printf( "Loaded...\n");
            break;

         case 's' :
            // save
            SetScaleName( "SCALE");
            SetSavingMode( SAVING_MODE_MANUAL_BY_SEX);
            Index = GroupCreate();
            SetGroupName( Index, "MYGROUP");
            SetGroupNote( Index, "MYGROUP NOTE");
            AddGroupFile( Index, 0);
            AddGroupFile( Index, 1);
            AddGroupFile( Index, 2);       
            if( !SaveDevice()){
               printf( "ERROR : unable save\n");
               break;
            }
            printf( "Saved...\n");
            break;

         case 'u' :
            _getcwd( Buf, sizeof( Buf));
            printf( "Path : '%s'\n", Buf);
            f = fopen( "Test.bat1", "rb");
            if( !f){
               printf( "Unable open file\n");
               break;
            }
            if( fread( &Memory, sizeof( Memory), 1, f) != 1){
               printf( "Unable read file\n");
               break;
            }
            fclose( f);
            if( !DeviceByEeprom( (byte *)&Memory)){
               printf( "Unable set device EEPROM\n");
               break;
            }
            printf( "Files count  : %d\n", GetFilesCount());
            printf( "Groups count : %d\n", GetGroupsCount());
            printf( "File Directory :\n");
            for( int i = 0; i < GetFilesCount(); i++){
                GetFileName( i, Buf);
                printf( "   %s\n",  Buf);
                printf( "   Samples %d\n", GetFileSamplesCount( i));
            }
            printf( "Group Directory :\n");
            for( int i = 0; i < GetGroupsCount(); i++){
                GetGroupName( i, Buf);
                printf( "   %s\n",  Buf);
                printf( "   Files %d\n", GetGroupFilesCount( i));
            }
            break;

         case 'd' :
            _getcwd( Buf, sizeof( Buf));
            printf( "Path : '%s'\n", Buf);
            if( !ReadEeprom( 0, (byte *)&Memory, GetEepromSize())){
               printf( "Unable read\n");
               break;
            }
            f = fopen( "Test.bat1", "wb");
            if( !f){
               printf( "Unable open file\n");
               break;
            }
            if( fwrite( &Memory, sizeof( Memory), 1, f) != 1){
               printf( "Unable read file\n");
               break;
            }
            fclose( f);
            printf( "File Test.bat1 saved\n");
            break;

         case 't' :
            int Clock;
            if( !GetTime( &Clock)){
               printf( "Unable get time\n");
               break;
            }
            DecodeTime( Clock, &Day, &Month, &Year, &Hour, &Min, &Sec);
            printf( "Time : %02d.%02d.%4d %02d:%02d:%02d\n", Day, Month, Year, Hour, Min, Sec);
            break;

         case 'n' :
            if( !LoadCrashInfo()){
               printf( "Unable read\n");
               break;
            }
            printf( "Exception :\n");
            DecodeTime( GetExceptionTimestamp(), &Day, &Month, &Year, &Hour, &Min, &Sec);
            printf( "Time    : %02d.%02d.%4d %02d:%02d:%02d\n", Day, Month, Year, Hour, Min, Sec);
            printf( "Address : %08x\n", GetExceptionAddress());
            printf( "Type    : %d\n",   GetExceptionType());
            printf( "Status  : %02x\n", GetExceptionStatus());
            break;

         case 'w' :
            if( !LoadCrashInfo()){
               printf( "Unable read\n");
               break;
            }
            printf( "WatchDog :\n");
            DecodeTime( GetWatchDogTimestamp(), &Day, &Month, &Year, &Hour, &Min, &Sec);
            printf( "Time    : %02d.%02d.%4d %02d:%02d:%02d\n", Day, Month, Year, Hour, Min, Sec);
            printf( "Status  : %02x\n", GetWatchDogStatus());
            break;

         case 'x' :
            return;

         default :
            printf( "C - check\n");
            printf( "L - Load\n");
            printf( "S - Save\n");
            printf( "D - Download\n");
            printf( "U - Upload\n");
            printf( "T - Get time\n");
            printf( "N - Get exception\n");
            printf( "W - Get watchdog\n");
            printf( "X - Exit\n");
            break;
      }
   }
} // main
