﻿namespace AgMultimeter {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.ConnectButton = new System.Windows.Forms.Button();
         this.StatusLabel = new System.Windows.Forms.Label();
         this.VoltageButton = new System.Windows.Forms.Button();
         this.VoltageTextBox = new System.Windows.Forms.TextBox();
         this.ReadCurrentButton = new System.Windows.Forms.Button();
         this.CurrentTextBox = new System.Windows.Forms.TextBox();
         this.SuspendLayout();
         // 
         // ConnectButton
         // 
         this.ConnectButton.Location = new System.Drawing.Point(80, 13);
         this.ConnectButton.Name = "ConnectButton";
         this.ConnectButton.Size = new System.Drawing.Size(75, 23);
         this.ConnectButton.TabIndex = 0;
         this.ConnectButton.Text = "Connect";
         this.ConnectButton.UseVisualStyleBackColor = true;
         this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
         // 
         // StatusLabel
         // 
         this.StatusLabel.AutoSize = true;
         this.StatusLabel.Location = new System.Drawing.Point(50, 230);
         this.StatusLabel.Name = "StatusLabel";
         this.StatusLabel.Size = new System.Drawing.Size(49, 13);
         this.StatusLabel.TabIndex = 1;
         this.StatusLabel.Text = "Status: ?";
         // 
         // VoltageButton
         // 
         this.VoltageButton.Location = new System.Drawing.Point(147, 91);
         this.VoltageButton.Name = "VoltageButton";
         this.VoltageButton.Size = new System.Drawing.Size(101, 23);
         this.VoltageButton.TabIndex = 2;
         this.VoltageButton.Text = "Read Voltage";
         this.VoltageButton.UseVisualStyleBackColor = true;
         this.VoltageButton.Click += new System.EventHandler(this.VoltageButton_Click);
         // 
         // VoltageTextBox
         // 
         this.VoltageTextBox.Location = new System.Drawing.Point(28, 93);
         this.VoltageTextBox.Name = "VoltageTextBox";
         this.VoltageTextBox.Size = new System.Drawing.Size(100, 20);
         this.VoltageTextBox.TabIndex = 3;
         // 
         // ReadCurrentButton
         // 
         this.ReadCurrentButton.Location = new System.Drawing.Point(147, 120);
         this.ReadCurrentButton.Name = "ReadCurrentButton";
         this.ReadCurrentButton.Size = new System.Drawing.Size(101, 23);
         this.ReadCurrentButton.TabIndex = 4;
         this.ReadCurrentButton.Text = "Read Current";
         this.ReadCurrentButton.UseVisualStyleBackColor = true;
         this.ReadCurrentButton.Click += new System.EventHandler(this.ReadCurrentButton_Click);
         // 
         // CurrentTextBox
         // 
         this.CurrentTextBox.Location = new System.Drawing.Point(28, 122);
         this.CurrentTextBox.Name = "CurrentTextBox";
         this.CurrentTextBox.Size = new System.Drawing.Size(100, 20);
         this.CurrentTextBox.TabIndex = 5;
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(284, 262);
         this.Controls.Add(this.CurrentTextBox);
         this.Controls.Add(this.ReadCurrentButton);
         this.Controls.Add(this.VoltageTextBox);
         this.Controls.Add(this.VoltageButton);
         this.Controls.Add(this.StatusLabel);
         this.Controls.Add(this.ConnectButton);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button ConnectButton;
      private System.Windows.Forms.Label StatusLabel;
      private System.Windows.Forms.Button VoltageButton;
      private System.Windows.Forms.TextBox VoltageTextBox;
      private System.Windows.Forms.Button ReadCurrentButton;
      private System.Windows.Forms.TextBox CurrentTextBox;
   }
}

