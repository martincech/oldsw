﻿using System;
using System.Windows.Forms;
using AgilentInstruments;

namespace AgMultimeter {
   public partial class Form1 : Form {
      Ag34410A Multimeter;

      public Form1() {
         InitializeComponent();

         Multimeter = new Ag34410A();
      }

      private void SetStatus(string Status) {
         StatusLabel.Text = "Status: " + Status;
      }

      private void ConnectButton_Click(object sender, EventArgs e) {
         if(!Multimeter.Connect("USB0::2391::1543::MY47022753")) {
            SetStatus("Unable to connect");
            return;
         }

         SetStatus("OK");
      }

      private void VoltageButton_Click(object sender, EventArgs e) {
         double Voltage = 0;

         if(!Multimeter.ReadDCVoltage(ref Voltage)) {
            SetStatus("Unable to read voltage");
            return;
         }

         VoltageTextBox.Text = Voltage.ToString();

         SetStatus("OK");
      }

      private void ReadCurrentButton_Click(object sender, EventArgs e) {
         double Current = 0;

         if(!Multimeter.ReadDCCurrent(ref Current)) {
            SetStatus("Unable to read current");
            return;
         }

         CurrentTextBox.Text = Current.ToString();

         SetStatus("OK");
      }
   }
}
