﻿namespace Bat1TesterBoardDemo {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.ConnectButton = new System.Windows.Forms.Button();
         this.StatusLabel = new System.Windows.Forms.Label();
         this.UsbConnectDataCheckBox = new System.Windows.Forms.CheckBox();
         this.UsbPowerUsbRadioButton = new System.Windows.Forms.RadioButton();
         this.UsbPowerExternalRadioButton = new System.Windows.Forms.RadioButton();
         this.panel1 = new System.Windows.Forms.Panel();
         this.UsbPowerSwitchCheckBox = new System.Windows.Forms.CheckBox();
         this.OnOffButton = new System.Windows.Forms.Button();
         this.K0Button = new System.Windows.Forms.Button();
         this.K1Button = new System.Windows.Forms.Button();
         this.K2Button = new System.Windows.Forms.Button();
         this.SensorMaxCheckBox = new System.Windows.Forms.CheckBox();
         this.PowerPowerOnCheckBox = new System.Windows.Forms.CheckBox();
         this.panel2 = new System.Windows.Forms.Panel();
         this.PowerNormalRadioButton = new System.Windows.Forms.RadioButton();
         this.PowerInvertedCheckBox = new System.Windows.Forms.RadioButton();
         this.AmpermeterCheckBox = new System.Windows.Forms.CheckBox();
         this.LoadCheckBox = new System.Windows.Forms.CheckBox();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.groupBox3 = new System.Windows.Forms.GroupBox();
         this.groupBox4 = new System.Windows.Forms.GroupBox();
         this.panel1.SuspendLayout();
         this.panel2.SuspendLayout();
         this.groupBox1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.groupBox3.SuspendLayout();
         this.groupBox4.SuspendLayout();
         this.SuspendLayout();
         // 
         // ConnectButton
         // 
         this.ConnectButton.Location = new System.Drawing.Point(12, 12);
         this.ConnectButton.Name = "ConnectButton";
         this.ConnectButton.Size = new System.Drawing.Size(102, 23);
         this.ConnectButton.TabIndex = 7;
         this.ConnectButton.Text = "Connect";
         this.ConnectButton.UseVisualStyleBackColor = true;
         this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
         // 
         // StatusLabel
         // 
         this.StatusLabel.AutoSize = true;
         this.StatusLabel.Location = new System.Drawing.Point(151, 17);
         this.StatusLabel.Name = "StatusLabel";
         this.StatusLabel.Size = new System.Drawing.Size(49, 13);
         this.StatusLabel.TabIndex = 6;
         this.StatusLabel.Text = "Status: ?";
         // 
         // UsbConnectDataCheckBox
         // 
         this.UsbConnectDataCheckBox.AutoSize = true;
         this.UsbConnectDataCheckBox.Location = new System.Drawing.Point(6, 19);
         this.UsbConnectDataCheckBox.Name = "UsbConnectDataCheckBox";
         this.UsbConnectDataCheckBox.Size = new System.Drawing.Size(92, 17);
         this.UsbConnectDataCheckBox.TabIndex = 10;
         this.UsbConnectDataCheckBox.Text = "Connect Data";
         this.UsbConnectDataCheckBox.UseVisualStyleBackColor = true;
         this.UsbConnectDataCheckBox.CheckedChanged += new System.EventHandler(this.UsbConnectDataCheckBox_CheckedChanged);
         // 
         // UsbPowerUsbRadioButton
         // 
         this.UsbPowerUsbRadioButton.AutoSize = true;
         this.UsbPowerUsbRadioButton.Location = new System.Drawing.Point(98, 6);
         this.UsbPowerUsbRadioButton.Name = "UsbPowerUsbRadioButton";
         this.UsbPowerUsbRadioButton.Size = new System.Drawing.Size(47, 17);
         this.UsbPowerUsbRadioButton.TabIndex = 12;
         this.UsbPowerUsbRadioButton.TabStop = true;
         this.UsbPowerUsbRadioButton.Text = "USB";
         this.UsbPowerUsbRadioButton.UseVisualStyleBackColor = true;
         // 
         // UsbPowerExternalRadioButton
         // 
         this.UsbPowerExternalRadioButton.AutoSize = true;
         this.UsbPowerExternalRadioButton.Location = new System.Drawing.Point(29, 6);
         this.UsbPowerExternalRadioButton.Name = "UsbPowerExternalRadioButton";
         this.UsbPowerExternalRadioButton.Size = new System.Drawing.Size(63, 17);
         this.UsbPowerExternalRadioButton.TabIndex = 13;
         this.UsbPowerExternalRadioButton.TabStop = true;
         this.UsbPowerExternalRadioButton.Text = "External";
         this.UsbPowerExternalRadioButton.UseVisualStyleBackColor = true;
         this.UsbPowerExternalRadioButton.CheckedChanged += new System.EventHandler(this.UsbPowerExternalRadioButton_CheckedChanged);
         // 
         // panel1
         // 
         this.panel1.Controls.Add(this.UsbPowerExternalRadioButton);
         this.panel1.Controls.Add(this.UsbPowerUsbRadioButton);
         this.panel1.Location = new System.Drawing.Point(6, 65);
         this.panel1.Name = "panel1";
         this.panel1.Size = new System.Drawing.Size(158, 31);
         this.panel1.TabIndex = 14;
         // 
         // UsbPowerSwitchCheckBox
         // 
         this.UsbPowerSwitchCheckBox.AutoSize = true;
         this.UsbPowerSwitchCheckBox.Location = new System.Drawing.Point(6, 42);
         this.UsbPowerSwitchCheckBox.Name = "UsbPowerSwitchCheckBox";
         this.UsbPowerSwitchCheckBox.Size = new System.Drawing.Size(71, 17);
         this.UsbPowerSwitchCheckBox.TabIndex = 15;
         this.UsbPowerSwitchCheckBox.Text = "Power on";
         this.UsbPowerSwitchCheckBox.UseVisualStyleBackColor = true;
         this.UsbPowerSwitchCheckBox.CheckedChanged += new System.EventHandler(this.UsbPowerSwitchCheckBox_CheckedChanged);
         // 
         // OnOffButton
         // 
         this.OnOffButton.Location = new System.Drawing.Point(6, 19);
         this.OnOffButton.Name = "OnOffButton";
         this.OnOffButton.Size = new System.Drawing.Size(46, 23);
         this.OnOffButton.TabIndex = 17;
         this.OnOffButton.Text = "On/off";
         this.OnOffButton.UseVisualStyleBackColor = true;
         this.OnOffButton.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnOffButton_MouseDown);
         this.OnOffButton.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnOffButton_MouseUp);
         // 
         // K0Button
         // 
         this.K0Button.Location = new System.Drawing.Point(59, 19);
         this.K0Button.Name = "K0Button";
         this.K0Button.Size = new System.Drawing.Size(39, 23);
         this.K0Button.TabIndex = 18;
         this.K0Button.Text = "K0";
         this.K0Button.UseVisualStyleBackColor = true;
         this.K0Button.MouseDown += new System.Windows.Forms.MouseEventHandler(this.K0Button_MouseDown);
         this.K0Button.MouseUp += new System.Windows.Forms.MouseEventHandler(this.K0Button_MouseUp);
         // 
         // K1Button
         // 
         this.K1Button.Location = new System.Drawing.Point(104, 19);
         this.K1Button.Name = "K1Button";
         this.K1Button.Size = new System.Drawing.Size(39, 23);
         this.K1Button.TabIndex = 19;
         this.K1Button.Text = "K1";
         this.K1Button.UseVisualStyleBackColor = true;
         this.K1Button.MouseDown += new System.Windows.Forms.MouseEventHandler(this.K1Button_MouseDown);
         this.K1Button.MouseUp += new System.Windows.Forms.MouseEventHandler(this.K1Button_MouseUp);
         // 
         // K2Button
         // 
         this.K2Button.Location = new System.Drawing.Point(149, 19);
         this.K2Button.Name = "K2Button";
         this.K2Button.Size = new System.Drawing.Size(39, 23);
         this.K2Button.TabIndex = 20;
         this.K2Button.Text = "K2";
         this.K2Button.UseVisualStyleBackColor = true;
         this.K2Button.MouseDown += new System.Windows.Forms.MouseEventHandler(this.K2Button_MouseDown);
         this.K2Button.MouseUp += new System.Windows.Forms.MouseEventHandler(this.K2Button_MouseUp);
         // 
         // SensorMaxCheckBox
         // 
         this.SensorMaxCheckBox.AutoSize = true;
         this.SensorMaxCheckBox.Location = new System.Drawing.Point(13, 19);
         this.SensorMaxCheckBox.Name = "SensorMaxCheckBox";
         this.SensorMaxCheckBox.Size = new System.Drawing.Size(46, 17);
         this.SensorMaxCheckBox.TabIndex = 22;
         this.SensorMaxCheckBox.Text = "Max";
         this.SensorMaxCheckBox.UseVisualStyleBackColor = true;
         this.SensorMaxCheckBox.CheckedChanged += new System.EventHandler(this.SensorMaxCheckBox_CheckedChanged);
         // 
         // PowerPowerOnCheckBox
         // 
         this.PowerPowerOnCheckBox.AutoSize = true;
         this.PowerPowerOnCheckBox.Location = new System.Drawing.Point(6, 19);
         this.PowerPowerOnCheckBox.Name = "PowerPowerOnCheckBox";
         this.PowerPowerOnCheckBox.Size = new System.Drawing.Size(71, 17);
         this.PowerPowerOnCheckBox.TabIndex = 24;
         this.PowerPowerOnCheckBox.Text = "Power on";
         this.PowerPowerOnCheckBox.UseVisualStyleBackColor = true;
         this.PowerPowerOnCheckBox.CheckedChanged += new System.EventHandler(this.PowerPowerOnCheckBox_CheckedChanged);
         // 
         // panel2
         // 
         this.panel2.Controls.Add(this.PowerNormalRadioButton);
         this.panel2.Controls.Add(this.PowerInvertedCheckBox);
         this.panel2.Location = new System.Drawing.Point(6, 42);
         this.panel2.Name = "panel2";
         this.panel2.Size = new System.Drawing.Size(174, 31);
         this.panel2.TabIndex = 25;
         // 
         // PowerNormalRadioButton
         // 
         this.PowerNormalRadioButton.AutoSize = true;
         this.PowerNormalRadioButton.Location = new System.Drawing.Point(29, 6);
         this.PowerNormalRadioButton.Name = "PowerNormalRadioButton";
         this.PowerNormalRadioButton.Size = new System.Drawing.Size(58, 17);
         this.PowerNormalRadioButton.TabIndex = 13;
         this.PowerNormalRadioButton.TabStop = true;
         this.PowerNormalRadioButton.Text = "Normal";
         this.PowerNormalRadioButton.UseVisualStyleBackColor = true;
         this.PowerNormalRadioButton.CheckedChanged += new System.EventHandler(this.PowerNormalRadioButton_CheckedChanged);
         // 
         // PowerInvertedCheckBox
         // 
         this.PowerInvertedCheckBox.AutoSize = true;
         this.PowerInvertedCheckBox.Location = new System.Drawing.Point(98, 6);
         this.PowerInvertedCheckBox.Name = "PowerInvertedCheckBox";
         this.PowerInvertedCheckBox.Size = new System.Drawing.Size(64, 17);
         this.PowerInvertedCheckBox.TabIndex = 12;
         this.PowerInvertedCheckBox.TabStop = true;
         this.PowerInvertedCheckBox.Text = "Inverted";
         this.PowerInvertedCheckBox.UseVisualStyleBackColor = true;
         // 
         // AmpermeterCheckBox
         // 
         this.AmpermeterCheckBox.AutoSize = true;
         this.AmpermeterCheckBox.Location = new System.Drawing.Point(6, 80);
         this.AmpermeterCheckBox.Name = "AmpermeterCheckBox";
         this.AmpermeterCheckBox.Size = new System.Drawing.Size(124, 17);
         this.AmpermeterCheckBox.TabIndex = 26;
         this.AmpermeterCheckBox.Text = "Ampermeter connect";
         this.AmpermeterCheckBox.UseVisualStyleBackColor = true;
         this.AmpermeterCheckBox.CheckedChanged += new System.EventHandler(this.AmpermeterCheckBox_CheckedChanged);
         // 
         // LoadCheckBox
         // 
         this.LoadCheckBox.AutoSize = true;
         this.LoadCheckBox.Location = new System.Drawing.Point(6, 103);
         this.LoadCheckBox.Name = "LoadCheckBox";
         this.LoadCheckBox.Size = new System.Drawing.Size(92, 17);
         this.LoadCheckBox.TabIndex = 27;
         this.LoadCheckBox.Text = "Load connect";
         this.LoadCheckBox.UseVisualStyleBackColor = true;
         this.LoadCheckBox.CheckedChanged += new System.EventHandler(this.LoadCheckBox_CheckedChanged);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.UsbConnectDataCheckBox);
         this.groupBox1.Controls.Add(this.panel1);
         this.groupBox1.Controls.Add(this.UsbPowerSwitchCheckBox);
         this.groupBox1.Location = new System.Drawing.Point(12, 57);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(170, 125);
         this.groupBox1.TabIndex = 28;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "USB";
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.PowerPowerOnCheckBox);
         this.groupBox2.Controls.Add(this.panel2);
         this.groupBox2.Controls.Add(this.LoadCheckBox);
         this.groupBox2.Controls.Add(this.AmpermeterCheckBox);
         this.groupBox2.Location = new System.Drawing.Point(188, 57);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(200, 125);
         this.groupBox2.TabIndex = 29;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Power";
         // 
         // groupBox3
         // 
         this.groupBox3.Controls.Add(this.OnOffButton);
         this.groupBox3.Controls.Add(this.K0Button);
         this.groupBox3.Controls.Add(this.K1Button);
         this.groupBox3.Controls.Add(this.K2Button);
         this.groupBox3.Location = new System.Drawing.Point(12, 188);
         this.groupBox3.Name = "groupBox3";
         this.groupBox3.Size = new System.Drawing.Size(200, 49);
         this.groupBox3.TabIndex = 30;
         this.groupBox3.TabStop = false;
         this.groupBox3.Text = "Keyboard";
         // 
         // groupBox4
         // 
         this.groupBox4.Controls.Add(this.SensorMaxCheckBox);
         this.groupBox4.Location = new System.Drawing.Point(218, 188);
         this.groupBox4.Name = "groupBox4";
         this.groupBox4.Size = new System.Drawing.Size(170, 49);
         this.groupBox4.TabIndex = 31;
         this.groupBox4.TabStop = false;
         this.groupBox4.Text = "Sensor";
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(402, 246);
         this.Controls.Add(this.groupBox4);
         this.Controls.Add(this.groupBox3);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.ConnectButton);
         this.Controls.Add(this.StatusLabel);
         this.Name = "Form1";
         this.Text = "Form1";
         this.panel1.ResumeLayout(false);
         this.panel1.PerformLayout();
         this.panel2.ResumeLayout(false);
         this.panel2.PerformLayout();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox2.PerformLayout();
         this.groupBox3.ResumeLayout(false);
         this.groupBox4.ResumeLayout(false);
         this.groupBox4.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button ConnectButton;
      private System.Windows.Forms.Label StatusLabel;
      private System.Windows.Forms.CheckBox UsbConnectDataCheckBox;
      private System.Windows.Forms.RadioButton UsbPowerUsbRadioButton;
      private System.Windows.Forms.RadioButton UsbPowerExternalRadioButton;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.CheckBox UsbPowerSwitchCheckBox;
      private System.Windows.Forms.Button OnOffButton;
      private System.Windows.Forms.Button K0Button;
      private System.Windows.Forms.Button K1Button;
      private System.Windows.Forms.Button K2Button;
      private System.Windows.Forms.CheckBox SensorMaxCheckBox;
      private System.Windows.Forms.CheckBox PowerPowerOnCheckBox;
      private System.Windows.Forms.Panel panel2;
      private System.Windows.Forms.RadioButton PowerNormalRadioButton;
      private System.Windows.Forms.RadioButton PowerInvertedCheckBox;
      private System.Windows.Forms.CheckBox AmpermeterCheckBox;
      private System.Windows.Forms.CheckBox LoadCheckBox;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.GroupBox groupBox4;
   }
}

