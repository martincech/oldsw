﻿using System;
using System.Windows.Forms;
using Device.Bat1;

namespace Bat1TesterBoardDemo {
   public partial class Form1 : Form {

      private TesterBoardConfig TesterBoardConfig = new TesterBoardConfig();

      private TesterBoard TesterBoard;

      private void SetStatus(string Status) {
         StatusLabel.Text = "Status: " + Status;
      }

      public Form1() {
         InitializeComponent();

         TesterBoard = new TesterBoard();
      }

      private void ConnectButton_Click(object sender, EventArgs e) {
         if(!TesterBoard.Open()) {
            SetStatus("Can't open device");
            return;
         }

         // zaškrtnutí musí odpovídat tomu, jak je deska inicializovaná
         // ? Implemetovat do Bat1TesterBoard funkce zjišťující stav prvků ?
         UsbConnectDataCheckBox.Checked = false;
         UsbPowerSwitchCheckBox.Checked = false;
         UsbPowerUsbRadioButton.Checked = true;
         SensorMaxCheckBox.Checked = false;
         PowerPowerOnCheckBox.Checked = false;
         PowerNormalRadioButton.Checked = true;
         AmpermeterCheckBox.Checked = false;
         LoadCheckBox.Checked = false;

         SetStatus("OK");
      }

      private void UsbConnectDataCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(((CheckBox)sender).Checked == true) {
            TesterBoard.UsbDataConnect();
         } else {
            TesterBoard.UsbDataDisconnect();
         }
      }

      private void UsbPowerSwitchCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(((CheckBox)sender).Checked == true) {
            TesterBoard.UsbPowerSwitchOn();
         } else {
            TesterBoard.UsbPowerSwitchOff();
         }
      }

      private void UsbPowerExternalRadioButton_CheckedChanged(object sender, EventArgs e) {
         if(((RadioButton)sender).Checked == true) {
            TesterBoard.UsbPowerSelectExternal();
         } else {
            TesterBoard.UsbPowerSelectUsb();
         }
      }

      private void PowerPowerOnCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(((CheckBox)sender).Checked == true) {
            TesterBoard.PowerSwitchOn();
         } else {
            TesterBoard.PowerSwitchOff();
         }
      }

      private void PowerNormalRadioButton_CheckedChanged(object sender, EventArgs e) {
         if(((RadioButton)sender).Checked == true) {
            TesterBoard.PowerSelectNormal();
         } else {
            TesterBoard.PowerSelectInverted();
         }
      }

      private void AmpermeterCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(((CheckBox)sender).Checked == true) {
            TesterBoard.AmpermeterConnect();
         } else {
            TesterBoard.AmpermeterDisconnect();
         }
      }

      private void LoadCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(((CheckBox)sender).Checked == true) {
            TesterBoard.LoadConnect();
         } else {
            TesterBoard.LoadDisconnect();
         }
      }

      private void SensorMaxCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(((CheckBox)sender).Checked == true) {
            TesterBoard.SensorMax();
         } else {
            TesterBoard.SensorMin();
         }
      }

      private void OnOffButton_MouseDown(object sender, MouseEventArgs e) {
         TesterBoard.OnOffPush();
      }

      private void OnOffButton_MouseUp(object sender, MouseEventArgs e) {
         TesterBoard.OnOffRelease();
      }

      private void K0Button_MouseDown(object sender, MouseEventArgs e) {
         TesterBoard.K0Push();
      }

      private void K0Button_MouseUp(object sender, MouseEventArgs e) {
         TesterBoard.K0Release();
      }

      private void K1Button_MouseDown(object sender, MouseEventArgs e) {
         TesterBoard.K1Push();
      }

      private void K1Button_MouseUp(object sender, MouseEventArgs e) {
         TesterBoard.K1Release();
      }

      private void K2Button_MouseDown(object sender, MouseEventArgs e) {
         TesterBoard.K2Push();
      }

      private void K2Button_MouseUp(object sender, MouseEventArgs e) {
         TesterBoard.K2Release();
      }
   }
}
