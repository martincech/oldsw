﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bat1TestDemo {
   public partial class ConfigurationFiles : Form {
      public ConfigurationFiles() {
         InitializeComponent();
      }

      private void SaveButton_Click(object sender, EventArgs e) {
         this.DialogResult = DialogResult.OK;
      }

      private void CancelButton_Click(object sender, EventArgs e) {
         this.DialogResult = DialogResult.Cancel;
      }
   }
}
