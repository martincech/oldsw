﻿<configuration>
   <configSections>
      <section name="Tester" type="Bat1.Tester.Config.Tester, Bat1TestDemo" />
      <section name="Tests" type="Bat1.Tester.Config.Tests, Bat1TestDemo" />
      <section name="Usb" type="Bat1.Tester.Config.Usb, Bat1TestDemo" />
      <section name="TesterBoard" type="Bat1.Tester.Config.TesterBoard, Bat1TestDemo" />
      <section name="Target" type="Bat1.Tester.Config.Target, Bat1TestDemo" />
      <section name="Instruments" type="Bat1.Tester.Config.Instruments, Bat1TestDemo" />
      <section name="FactoryConfig" type="Bat1.Tester.Config.FactoryConfig, Bat1TestDemo" />
   </configSections>

   <Tester>
      <Lpc CrystalFrequency="18432000" BaudRate="57600" />
      <PowerSupply SettleTime="300">
         <Normal Voltage="4.2" CurrentLimit="0.8" />
         <Inverted Voltage="-4.2" CurrentLimit="0.05" />
         <Usb Voltage="5" CurrentLimit="0.5" />
      </PowerSupply>
      <Target SwitchOnTime="500" SwitchOffTime="3500" InitTime="1500" PowerSettleTime="500" UsbPowerSettleTime="500" AdcSettleTime="800">
      </Target>
   </Tester>

   <Tests>
      <Adc InitDelay="500" Delay="800" Trials="5" SuccessfulTrials="4">
         <ZeroRange Min="0" Max="100" />
         <MaxRange Min="80000" Max="96000" />
      </Adc>

      <Iadc InitDelay="500" Delay="800" Trials="5" SuccessfulTrials="4" LowVoltage="3.4" HighVoltage="4.2" Reference="3.3" R1="15000" R2="47000" AdcRange="1024">
         <LowRange Min="3.35" Max="3.45" />
         <HighRange Min="4.15" Max="4.25" />
      </Iadc>

      <ChargerCurrent Voltage="3.2">
         <Current Min="0.36" Max="0.4" />
      </ChargerCurrent>

      <ChargerShutdown TargetCurrent="0.005">
         <VoltageSteps>
            <clearVoltageSteps />
            <add Voltage="3.4" />
            <add Voltage="3.6" />
            <add Voltage="3.8" />
            <add Voltage="4.0" />
            <add Voltage="4.1" />
            <add Voltage="4.2" />
            <add Voltage="4.3" />
         </VoltageSteps>
      </ChargerShutdown>

      <PowerShutdown TargetCurrent="0.005">
         <VoltageSteps>
            <clearVoltageSteps />
            <add Voltage="4.0" />
            <add Voltage="3.8" />
            <add Voltage="3.6" />
            <add Voltage="3.5" />
            <add Voltage="3.4" />
            <add Voltage="3.3" />
            <add Voltage="3.2" />
            <add Voltage="3.1" />
            <add Voltage="3.0" />
         </VoltageSteps>
      </PowerShutdown>

      <PowerBacklightOn>
         <Current Min="0.08" Max="0.12" />
      </PowerBacklightOn>

      <PowerBacklightOff>
         <Current Min="0.03" Max="0.05" />
      </PowerBacklightOff>

      <PowerIdle>
         <Current Min="-5e-6" Max="5e-6" />
      </PowerIdle>

      <PowerInverted>
         <Current Min="-1e-6" Max="1e-6" />
      </PowerInverted>
   </Tests>


   <Usb TestDataSize="256" RxTimeout="2000" ConnectDelay="7">
      <ComSettings DeviceName="VEIT BAT1 Poultry Scale" BaudRate="38400" Bits ="8" StopBit="One" Parity="None" Handshake="None" />
      <Ftdi DeviceName="VEIT BAT1 Poultry Scale" ManufacturerName="VEIT" MaxPower="350">
         <ProgrammedNames>
            <clearProgrammedNames />
            <add Name="VEIT BAT1 Poultry Scale" />
            <add Name="FlexScale" />
         </ProgrammedNames>
         <BlankNames>
            <clearBlankNames />
            <add Name="FT232R USB UART" />
            <add Name="USB HS Serial Converter" />
         </BlankNames>
      </Ftdi>
   </Usb>

   <TesterBoard>
      <ComSettings DeviceName="USB I/O 24 R" BaudRate="9600" Bits ="8" StopBit="One" Parity="None" Handshake="None" />
   </TesterBoard>

   <Target TestDataSize="256" RxTimeout="2000" >
      <ComSettings PortName="COM1" BaudRate="9600" Bits ="8" StopBit="One" Parity="None" Handshake="None" />
   </Target>

   <Instruments>
      <PowerSupply>
         <ComSettings PortName="COM3" BaudRate="9600" Bits ="8" StopBit="One" Parity="None" Handshake="None" />
      </PowerSupply>
      <Multimeter Address="USB0::2391::1543::MY47022753" />
   </Instruments>




   <!--vytvorit skutecne-->
   <FactoryConfig
      ScaleName="VAHA"
      Country="CZECH"
      Language="CZECH"
      DateFormat="DDMMYYYY"
      DateSeparator1="."
      DateSeparator2="."
      TimeFormat="HOUR24"
      TimeSeparator=":"
      DaylightSavingMode="OFF"
      PowerOffTimeout="5940"
      EnableFileParameters="false"
      ActiveFileIndex="-1"
      Logo="D:\SOURCE\Borland\Bat1Tester\Data\Logo.bmp"
   >
      <Firmware>
         <clearFirmware />
         <add Type="TEST" Path="D:\Projekty\Tester Bat1\Data\Bat1.hex" />
         <add Type="PRODUCTION" Path="D:\Projekty\Tester Bat1\Data\Bat1test.hex" />
      </Firmware>

      <DefaultFile Name="FILE000"/>

      <Version Major="8" Minor="0" Build="0" Hw="0" />

      <Units Range="30000" Units="KG" Decimals="1" MaxDivision="1" Division="1" WeighingCapacity="EXTENDED" />
      <Sounds ToneDefault="MELODY1" ToneLight="MELODY2" ToneOk="MELODY3" ToneHeavy="MELODY4" ToneKeyboard="TONE7" EnableSpecial="true" VolumeKeyboard="2" VolumeSaving="9"/>
      <Display Mode="BASIC" Contrast="55">
         <Backlight Mode="AUTO" Intensity="9" Duration="5" />
      </Display>
      <Printer PaperWidth="99" CommunicationFormat="COM_8BITS" CommunicationSpeed="9600"/>

      <Password Enable="false">
         <Keys>
            <clearKeys />
            <add Key="ENTER" />
            <add Key="ENTER" />
            <add Key="ESC" />
            <add Key="UP" />
         </Keys>
      </Password>

      <Weighing>
         <WeightSorting Mode="NONE" LowLimit="1000" HighLimit="2000" />
         <Saving Mode="AUTOMATIC" EnableMoreBirds="false" NumberOfBirds="99" Filter="50" StabilisationTime="50" MinimumWeight="10" StabilisationRange="10" />
      </Weighing>

      <Statistic UniformityRange="1">
         <Histogram Mode="RANGE" Range="10" Step="0" />
      </Statistic>
   </FactoryConfig>




</configuration>