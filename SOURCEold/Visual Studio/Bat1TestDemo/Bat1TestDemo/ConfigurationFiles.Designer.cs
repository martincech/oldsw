﻿namespace Bat1TestDemo {
   partial class ConfigurationFiles {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.TesterTextBox = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.TestsTextBox = new System.Windows.Forms.TextBox();
         this.FactoryConfigurationTextBox = new System.Windows.Forms.TextBox();
         this.SaveButton = new System.Windows.Forms.Button();
         this.CancelButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // TesterTextBox
         // 
         this.TesterTextBox.Location = new System.Drawing.Point(137, 60);
         this.TesterTextBox.Name = "TesterTextBox";
         this.TesterTextBox.Size = new System.Drawing.Size(100, 20);
         this.TesterTextBox.TabIndex = 0;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(28, 66);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(37, 13);
         this.label1.TabIndex = 1;
         this.label1.Text = "Tester";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(28, 131);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(33, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Tests";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(28, 198);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(106, 13);
         this.label3.TabIndex = 3;
         this.label3.Text = "Factory configuration";
         // 
         // TestsTextBox
         // 
         this.TestsTextBox.Location = new System.Drawing.Point(137, 124);
         this.TestsTextBox.Name = "TestsTextBox";
         this.TestsTextBox.Size = new System.Drawing.Size(100, 20);
         this.TestsTextBox.TabIndex = 4;
         // 
         // FactoryConfigurationTextBox
         // 
         this.FactoryConfigurationTextBox.Location = new System.Drawing.Point(137, 191);
         this.FactoryConfigurationTextBox.Name = "FactoryConfigurationTextBox";
         this.FactoryConfigurationTextBox.Size = new System.Drawing.Size(100, 20);
         this.FactoryConfigurationTextBox.TabIndex = 5;
         // 
         // SaveButton
         // 
         this.SaveButton.Location = new System.Drawing.Point(81, 313);
         this.SaveButton.Name = "SaveButton";
         this.SaveButton.Size = new System.Drawing.Size(75, 23);
         this.SaveButton.TabIndex = 6;
         this.SaveButton.Text = "Save";
         this.SaveButton.UseVisualStyleBackColor = true;
         this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
         // 
         // CancelButton
         // 
         this.CancelButton.Location = new System.Drawing.Point(300, 313);
         this.CancelButton.Name = "CancelButton";
         this.CancelButton.Size = new System.Drawing.Size(75, 23);
         this.CancelButton.TabIndex = 7;
         this.CancelButton.Text = "Cancel";
         this.CancelButton.UseVisualStyleBackColor = true;
         this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
         // 
         // ConfigurationFiles
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(796, 395);
         this.Controls.Add(this.CancelButton);
         this.Controls.Add(this.SaveButton);
         this.Controls.Add(this.FactoryConfigurationTextBox);
         this.Controls.Add(this.TestsTextBox);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.TesterTextBox);
         this.Name = "ConfigurationFiles";
         this.Text = "ConfigurationFiles";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox TesterTextBox;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox TestsTextBox;
      private System.Windows.Forms.TextBox FactoryConfigurationTextBox;
      private System.Windows.Forms.Button SaveButton;
      private System.Windows.Forms.Button CancelButton;
   }
}