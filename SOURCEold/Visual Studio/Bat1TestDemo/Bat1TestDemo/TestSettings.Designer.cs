﻿namespace Bat1TestDemo {
   partial class TestSettings {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.GeneralPage = new System.Windows.Forms.TabPage();
         this.PowerTestsPage = new System.Windows.Forms.TabPage();
         this.StornoButton = new System.Windows.Forms.Button();
         this.SaveButton = new System.Windows.Forms.Button();
         this.OtherTestsPage = new System.Windows.Forms.TabPage();
         this.tabControl2 = new System.Windows.Forms.TabControl();
         this.BlOnPage = new System.Windows.Forms.TabPage();
         this.BlOff = new System.Windows.Forms.TabPage();
         this.IdlePage = new System.Windows.Forms.TabPage();
         this.InvertedPage = new System.Windows.Forms.TabPage();
         this.ShutdownPage = new System.Windows.Forms.TabPage();
         this.ChargerCurrentPage = new System.Windows.Forms.TabPage();
         this.ChargerShutdown = new System.Windows.Forms.TabPage();
         this.BlOnMinTextBox = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.BlOnMaxTextBox = new System.Windows.Forms.TextBox();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.BlOffMaxTextBox = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.BlOffMinTextBox = new System.Windows.Forms.TextBox();
         this.label8 = new System.Windows.Forms.Label();
         this.IdleMaxTextBox = new System.Windows.Forms.TextBox();
         this.label7 = new System.Windows.Forms.Label();
         this.InvertedMaxTextBox = new System.Windows.Forms.TextBox();
         this.label9 = new System.Windows.Forms.Label();
         this.ShutdownStepsTextBox = new System.Windows.Forms.TextBox();
         this.label10 = new System.Windows.Forms.Label();
         this.ShutdownCurrentTextBox = new System.Windows.Forms.TextBox();
         this.label11 = new System.Windows.Forms.Label();
         this.ChargerCurrentMaxTextBox = new System.Windows.Forms.TextBox();
         this.label12 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.ChargerCurrentMinTextBox = new System.Windows.Forms.TextBox();
         this.ChargerShutdownCurrentTextBox = new System.Windows.Forms.TextBox();
         this.label14 = new System.Windows.Forms.Label();
         this.label15 = new System.Windows.Forms.Label();
         this.ChargerShutdownStepsTextBox = new System.Windows.Forms.TextBox();
         this.tabControl3 = new System.Windows.Forms.TabControl();
         this.AdcPage = new System.Windows.Forms.TabPage();
         this.IadcPage = new System.Windows.Forms.TabPage();
         this.UsbPage = new System.Windows.Forms.TabPage();
         this.AdcTrialsTextBox = new System.Windows.Forms.TextBox();
         this.label16 = new System.Windows.Forms.Label();
         this.label17 = new System.Windows.Forms.Label();
         this.AdcSuccessfullTextBox = new System.Windows.Forms.TextBox();
         this.tabControl1.SuspendLayout();
         this.PowerTestsPage.SuspendLayout();
         this.OtherTestsPage.SuspendLayout();
         this.tabControl2.SuspendLayout();
         this.BlOnPage.SuspendLayout();
         this.BlOff.SuspendLayout();
         this.IdlePage.SuspendLayout();
         this.InvertedPage.SuspendLayout();
         this.ShutdownPage.SuspendLayout();
         this.ChargerCurrentPage.SuspendLayout();
         this.ChargerShutdown.SuspendLayout();
         this.tabControl3.SuspendLayout();
         this.AdcPage.SuspendLayout();
         this.SuspendLayout();
         // 
         // tabControl1
         // 
         this.tabControl1.Controls.Add(this.GeneralPage);
         this.tabControl1.Controls.Add(this.PowerTestsPage);
         this.tabControl1.Controls.Add(this.OtherTestsPage);
         this.tabControl1.Location = new System.Drawing.Point(167, 126);
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(641, 402);
         this.tabControl1.TabIndex = 5;
         // 
         // GeneralPage
         // 
         this.GeneralPage.Location = new System.Drawing.Point(4, 22);
         this.GeneralPage.Name = "GeneralPage";
         this.GeneralPage.Padding = new System.Windows.Forms.Padding(3);
         this.GeneralPage.Size = new System.Drawing.Size(633, 376);
         this.GeneralPage.TabIndex = 0;
         this.GeneralPage.Text = "General";
         this.GeneralPage.UseVisualStyleBackColor = true;
         // 
         // PowerTestsPage
         // 
         this.PowerTestsPage.Controls.Add(this.tabControl2);
         this.PowerTestsPage.Location = new System.Drawing.Point(4, 22);
         this.PowerTestsPage.Name = "PowerTestsPage";
         this.PowerTestsPage.Padding = new System.Windows.Forms.Padding(3);
         this.PowerTestsPage.Size = new System.Drawing.Size(633, 376);
         this.PowerTestsPage.TabIndex = 1;
         this.PowerTestsPage.Text = "Power Tests";
         this.PowerTestsPage.UseVisualStyleBackColor = true;
         // 
         // StornoButton
         // 
         this.StornoButton.Location = new System.Drawing.Point(666, 598);
         this.StornoButton.Name = "StornoButton";
         this.StornoButton.Size = new System.Drawing.Size(75, 23);
         this.StornoButton.TabIndex = 4;
         this.StornoButton.Text = "Storno";
         this.StornoButton.UseVisualStyleBackColor = true;
         this.StornoButton.Click += new System.EventHandler(this.StornoButton_Click);
         // 
         // SaveButton
         // 
         this.SaveButton.Location = new System.Drawing.Point(248, 598);
         this.SaveButton.Name = "SaveButton";
         this.SaveButton.Size = new System.Drawing.Size(75, 23);
         this.SaveButton.TabIndex = 3;
         this.SaveButton.Text = "Save";
         this.SaveButton.UseVisualStyleBackColor = true;
         this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
         // 
         // OtherTestsPage
         // 
         this.OtherTestsPage.Controls.Add(this.tabControl3);
         this.OtherTestsPage.Location = new System.Drawing.Point(4, 22);
         this.OtherTestsPage.Name = "OtherTestsPage";
         this.OtherTestsPage.Size = new System.Drawing.Size(633, 376);
         this.OtherTestsPage.TabIndex = 2;
         this.OtherTestsPage.Text = "Other Tests";
         this.OtherTestsPage.UseVisualStyleBackColor = true;
         // 
         // tabControl2
         // 
         this.tabControl2.Controls.Add(this.BlOnPage);
         this.tabControl2.Controls.Add(this.BlOff);
         this.tabControl2.Controls.Add(this.IdlePage);
         this.tabControl2.Controls.Add(this.InvertedPage);
         this.tabControl2.Controls.Add(this.ShutdownPage);
         this.tabControl2.Controls.Add(this.ChargerCurrentPage);
         this.tabControl2.Controls.Add(this.ChargerShutdown);
         this.tabControl2.Location = new System.Drawing.Point(6, 6);
         this.tabControl2.Name = "tabControl2";
         this.tabControl2.SelectedIndex = 0;
         this.tabControl2.Size = new System.Drawing.Size(564, 319);
         this.tabControl2.TabIndex = 0;
         // 
         // BlOnPage
         // 
         this.BlOnPage.Controls.Add(this.label3);
         this.BlOnPage.Controls.Add(this.BlOnMaxTextBox);
         this.BlOnPage.Controls.Add(this.label2);
         this.BlOnPage.Controls.Add(this.label1);
         this.BlOnPage.Controls.Add(this.BlOnMinTextBox);
         this.BlOnPage.Location = new System.Drawing.Point(4, 22);
         this.BlOnPage.Name = "BlOnPage";
         this.BlOnPage.Padding = new System.Windows.Forms.Padding(3);
         this.BlOnPage.Size = new System.Drawing.Size(556, 293);
         this.BlOnPage.TabIndex = 0;
         this.BlOnPage.Text = "BL On";
         this.BlOnPage.UseVisualStyleBackColor = true;
         // 
         // BlOff
         // 
         this.BlOff.Controls.Add(this.label4);
         this.BlOff.Controls.Add(this.BlOffMaxTextBox);
         this.BlOff.Controls.Add(this.label5);
         this.BlOff.Controls.Add(this.label6);
         this.BlOff.Controls.Add(this.BlOffMinTextBox);
         this.BlOff.Location = new System.Drawing.Point(4, 22);
         this.BlOff.Name = "BlOff";
         this.BlOff.Padding = new System.Windows.Forms.Padding(3);
         this.BlOff.Size = new System.Drawing.Size(556, 293);
         this.BlOff.TabIndex = 1;
         this.BlOff.Text = "BlOff";
         this.BlOff.UseVisualStyleBackColor = true;
         // 
         // IdlePage
         // 
         this.IdlePage.Controls.Add(this.label8);
         this.IdlePage.Controls.Add(this.IdleMaxTextBox);
         this.IdlePage.Location = new System.Drawing.Point(4, 22);
         this.IdlePage.Name = "IdlePage";
         this.IdlePage.Size = new System.Drawing.Size(556, 293);
         this.IdlePage.TabIndex = 2;
         this.IdlePage.Text = "Idle";
         this.IdlePage.UseVisualStyleBackColor = true;
         // 
         // InvertedPage
         // 
         this.InvertedPage.Controls.Add(this.label7);
         this.InvertedPage.Controls.Add(this.InvertedMaxTextBox);
         this.InvertedPage.Location = new System.Drawing.Point(4, 22);
         this.InvertedPage.Name = "InvertedPage";
         this.InvertedPage.Size = new System.Drawing.Size(556, 293);
         this.InvertedPage.TabIndex = 3;
         this.InvertedPage.Text = "Inverted";
         this.InvertedPage.UseVisualStyleBackColor = true;
         // 
         // ShutdownPage
         // 
         this.ShutdownPage.Controls.Add(this.ShutdownCurrentTextBox);
         this.ShutdownPage.Controls.Add(this.label10);
         this.ShutdownPage.Controls.Add(this.label9);
         this.ShutdownPage.Controls.Add(this.ShutdownStepsTextBox);
         this.ShutdownPage.Location = new System.Drawing.Point(4, 22);
         this.ShutdownPage.Name = "ShutdownPage";
         this.ShutdownPage.Size = new System.Drawing.Size(556, 293);
         this.ShutdownPage.TabIndex = 4;
         this.ShutdownPage.Text = "Shutdown";
         this.ShutdownPage.UseVisualStyleBackColor = true;
         // 
         // ChargerCurrentPage
         // 
         this.ChargerCurrentPage.Controls.Add(this.label11);
         this.ChargerCurrentPage.Controls.Add(this.ChargerCurrentMaxTextBox);
         this.ChargerCurrentPage.Controls.Add(this.label12);
         this.ChargerCurrentPage.Controls.Add(this.label13);
         this.ChargerCurrentPage.Controls.Add(this.ChargerCurrentMinTextBox);
         this.ChargerCurrentPage.Location = new System.Drawing.Point(4, 22);
         this.ChargerCurrentPage.Name = "ChargerCurrentPage";
         this.ChargerCurrentPage.Size = new System.Drawing.Size(556, 293);
         this.ChargerCurrentPage.TabIndex = 5;
         this.ChargerCurrentPage.Text = "Charger current";
         this.ChargerCurrentPage.UseVisualStyleBackColor = true;
         // 
         // ChargerShutdown
         // 
         this.ChargerShutdown.Controls.Add(this.ChargerShutdownCurrentTextBox);
         this.ChargerShutdown.Controls.Add(this.label14);
         this.ChargerShutdown.Controls.Add(this.label15);
         this.ChargerShutdown.Controls.Add(this.ChargerShutdownStepsTextBox);
         this.ChargerShutdown.Location = new System.Drawing.Point(4, 22);
         this.ChargerShutdown.Name = "ChargerShutdown";
         this.ChargerShutdown.Size = new System.Drawing.Size(556, 293);
         this.ChargerShutdown.TabIndex = 6;
         this.ChargerShutdown.Text = "Charger shutdown";
         this.ChargerShutdown.UseVisualStyleBackColor = true;
         // 
         // BlOnMinTextBox
         // 
         this.BlOnMinTextBox.Location = new System.Drawing.Point(42, 40);
         this.BlOnMinTextBox.Name = "BlOnMinTextBox";
         this.BlOnMinTextBox.Size = new System.Drawing.Size(100, 20);
         this.BlOnMinTextBox.TabIndex = 0;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 43);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(24, 13);
         this.label1.TabIndex = 1;
         this.label1.Text = "Min";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 74);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(27, 13);
         this.label2.TabIndex = 2;
         this.label2.Text = "Max";
         // 
         // BlOnMaxTextBox
         // 
         this.BlOnMaxTextBox.Location = new System.Drawing.Point(42, 71);
         this.BlOnMaxTextBox.Name = "BlOnMaxTextBox";
         this.BlOnMaxTextBox.Size = new System.Drawing.Size(100, 20);
         this.BlOnMaxTextBox.TabIndex = 3;
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(12, 12);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(97, 13);
         this.label3.TabIndex = 4;
         this.label3.Text = "Passed current [A]:";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(12, 14);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(97, 13);
         this.label4.TabIndex = 9;
         this.label4.Text = "Passed current [A]:";
         // 
         // BlOffMaxTextBox
         // 
         this.BlOffMaxTextBox.Location = new System.Drawing.Point(42, 73);
         this.BlOffMaxTextBox.Name = "BlOffMaxTextBox";
         this.BlOffMaxTextBox.Size = new System.Drawing.Size(100, 20);
         this.BlOffMaxTextBox.TabIndex = 8;
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(12, 76);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(27, 13);
         this.label5.TabIndex = 7;
         this.label5.Text = "Max";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(12, 45);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(24, 13);
         this.label6.TabIndex = 6;
         this.label6.Text = "Min";
         // 
         // BlOffMinTextBox
         // 
         this.BlOffMinTextBox.Location = new System.Drawing.Point(42, 42);
         this.BlOffMinTextBox.Name = "BlOffMinTextBox";
         this.BlOffMinTextBox.Size = new System.Drawing.Size(100, 20);
         this.BlOffMinTextBox.TabIndex = 5;
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point(14, 36);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(104, 13);
         this.label8.TabIndex = 11;
         this.label8.Text = "Idle current max [uA]";
         // 
         // IdleMaxTextBox
         // 
         this.IdleMaxTextBox.Location = new System.Drawing.Point(129, 33);
         this.IdleMaxTextBox.Name = "IdleMaxTextBox";
         this.IdleMaxTextBox.Size = new System.Drawing.Size(100, 20);
         this.IdleMaxTextBox.TabIndex = 10;
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(16, 30);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(126, 13);
         this.label7.TabIndex = 13;
         this.label7.Text = "Inverted current max [uA]";
         // 
         // InvertedMaxTextBox
         // 
         this.InvertedMaxTextBox.Location = new System.Drawing.Point(148, 27);
         this.InvertedMaxTextBox.Name = "InvertedMaxTextBox";
         this.InvertedMaxTextBox.Size = new System.Drawing.Size(100, 20);
         this.InvertedMaxTextBox.TabIndex = 12;
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point(31, 37);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(99, 13);
         this.label9.TabIndex = 15;
         this.label9.Text = "Shutdown steps [V]";
         // 
         // ShutdownStepsTextBox
         // 
         this.ShutdownStepsTextBox.Location = new System.Drawing.Point(163, 34);
         this.ShutdownStepsTextBox.Name = "ShutdownStepsTextBox";
         this.ShutdownStepsTextBox.Size = new System.Drawing.Size(100, 20);
         this.ShutdownStepsTextBox.TabIndex = 14;
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Location = new System.Drawing.Point(31, 78);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(115, 13);
         this.label10.TabIndex = 16;
         this.label10.Text = "Shutdown current [mA]";
         // 
         // ShutdownCurrentTextBox
         // 
         this.ShutdownCurrentTextBox.Location = new System.Drawing.Point(163, 75);
         this.ShutdownCurrentTextBox.Name = "ShutdownCurrentTextBox";
         this.ShutdownCurrentTextBox.Size = new System.Drawing.Size(100, 20);
         this.ShutdownCurrentTextBox.TabIndex = 17;
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(12, 23);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(97, 13);
         this.label11.TabIndex = 14;
         this.label11.Text = "Passed current [A]:";
         // 
         // ChargerCurrentMaxTextBox
         // 
         this.ChargerCurrentMaxTextBox.Location = new System.Drawing.Point(42, 82);
         this.ChargerCurrentMaxTextBox.Name = "ChargerCurrentMaxTextBox";
         this.ChargerCurrentMaxTextBox.Size = new System.Drawing.Size(100, 20);
         this.ChargerCurrentMaxTextBox.TabIndex = 13;
         // 
         // label12
         // 
         this.label12.AutoSize = true;
         this.label12.Location = new System.Drawing.Point(12, 85);
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size(27, 13);
         this.label12.TabIndex = 12;
         this.label12.Text = "Max";
         // 
         // label13
         // 
         this.label13.AutoSize = true;
         this.label13.Location = new System.Drawing.Point(12, 54);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(24, 13);
         this.label13.TabIndex = 11;
         this.label13.Text = "Min";
         // 
         // ChargerCurrentMinTextBox
         // 
         this.ChargerCurrentMinTextBox.Location = new System.Drawing.Point(42, 51);
         this.ChargerCurrentMinTextBox.Name = "ChargerCurrentMinTextBox";
         this.ChargerCurrentMinTextBox.Size = new System.Drawing.Size(100, 20);
         this.ChargerCurrentMinTextBox.TabIndex = 10;
         // 
         // ChargerShutdownCurrentTextBox
         // 
         this.ChargerShutdownCurrentTextBox.Location = new System.Drawing.Point(153, 68);
         this.ChargerShutdownCurrentTextBox.Name = "ChargerShutdownCurrentTextBox";
         this.ChargerShutdownCurrentTextBox.Size = new System.Drawing.Size(100, 20);
         this.ChargerShutdownCurrentTextBox.TabIndex = 21;
         // 
         // label14
         // 
         this.label14.AutoSize = true;
         this.label14.Location = new System.Drawing.Point(21, 71);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(115, 13);
         this.label14.TabIndex = 20;
         this.label14.Text = "Shutdown current [mA]";
         // 
         // label15
         // 
         this.label15.AutoSize = true;
         this.label15.Location = new System.Drawing.Point(21, 30);
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size(99, 13);
         this.label15.TabIndex = 19;
         this.label15.Text = "Shutdown steps [V]";
         // 
         // ChargerShutdownStepsTextBox
         // 
         this.ChargerShutdownStepsTextBox.Location = new System.Drawing.Point(153, 27);
         this.ChargerShutdownStepsTextBox.Name = "ChargerShutdownStepsTextBox";
         this.ChargerShutdownStepsTextBox.Size = new System.Drawing.Size(100, 20);
         this.ChargerShutdownStepsTextBox.TabIndex = 18;
         // 
         // tabControl3
         // 
         this.tabControl3.Controls.Add(this.AdcPage);
         this.tabControl3.Controls.Add(this.IadcPage);
         this.tabControl3.Controls.Add(this.UsbPage);
         this.tabControl3.Location = new System.Drawing.Point(34, 29);
         this.tabControl3.Name = "tabControl3";
         this.tabControl3.SelectedIndex = 0;
         this.tabControl3.Size = new System.Drawing.Size(564, 319);
         this.tabControl3.TabIndex = 1;
         // 
         // AdcPage
         // 
         this.AdcPage.Controls.Add(this.label17);
         this.AdcPage.Controls.Add(this.AdcSuccessfullTextBox);
         this.AdcPage.Controls.Add(this.label16);
         this.AdcPage.Controls.Add(this.AdcTrialsTextBox);
         this.AdcPage.Location = new System.Drawing.Point(4, 22);
         this.AdcPage.Name = "AdcPage";
         this.AdcPage.Padding = new System.Windows.Forms.Padding(3);
         this.AdcPage.Size = new System.Drawing.Size(556, 293);
         this.AdcPage.TabIndex = 0;
         this.AdcPage.Text = "ADC";
         this.AdcPage.UseVisualStyleBackColor = true;
         // 
         // IadcPage
         // 
         this.IadcPage.Location = new System.Drawing.Point(4, 22);
         this.IadcPage.Name = "IadcPage";
         this.IadcPage.Size = new System.Drawing.Size(556, 293);
         this.IadcPage.TabIndex = 1;
         this.IadcPage.Text = "IADC";
         this.IadcPage.UseVisualStyleBackColor = true;
         // 
         // UsbPage
         // 
         this.UsbPage.Location = new System.Drawing.Point(4, 22);
         this.UsbPage.Name = "UsbPage";
         this.UsbPage.Size = new System.Drawing.Size(556, 293);
         this.UsbPage.TabIndex = 2;
         this.UsbPage.Text = "Usb";
         this.UsbPage.UseVisualStyleBackColor = true;
         // 
         // AdcTrialsTextBox
         // 
         this.AdcTrialsTextBox.Location = new System.Drawing.Point(101, 23);
         this.AdcTrialsTextBox.Name = "AdcTrialsTextBox";
         this.AdcTrialsTextBox.Size = new System.Drawing.Size(100, 20);
         this.AdcTrialsTextBox.TabIndex = 0;
         // 
         // label16
         // 
         this.label16.AutoSize = true;
         this.label16.Location = new System.Drawing.Point(36, 26);
         this.label16.Name = "label16";
         this.label16.Size = new System.Drawing.Size(32, 13);
         this.label16.TabIndex = 1;
         this.label16.Text = "Trials";
         // 
         // label17
         // 
         this.label17.AutoSize = true;
         this.label17.Location = new System.Drawing.Point(36, 57);
         this.label17.Name = "label17";
         this.label17.Size = new System.Drawing.Size(59, 13);
         this.label17.TabIndex = 3;
         this.label17.Text = "Successful";
         // 
         // AdcSuccessfullTextBox
         // 
         this.AdcSuccessfullTextBox.Location = new System.Drawing.Point(101, 54);
         this.AdcSuccessfullTextBox.Name = "AdcSuccessfullTextBox";
         this.AdcSuccessfullTextBox.Size = new System.Drawing.Size(100, 20);
         this.AdcSuccessfullTextBox.TabIndex = 2;
         // 
         // TestSettings
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(974, 746);
         this.Controls.Add(this.tabControl1);
         this.Controls.Add(this.StornoButton);
         this.Controls.Add(this.SaveButton);
         this.Name = "TestSettings";
         this.Text = "TestSettings";
         this.tabControl1.ResumeLayout(false);
         this.PowerTestsPage.ResumeLayout(false);
         this.OtherTestsPage.ResumeLayout(false);
         this.tabControl2.ResumeLayout(false);
         this.BlOnPage.ResumeLayout(false);
         this.BlOnPage.PerformLayout();
         this.BlOff.ResumeLayout(false);
         this.BlOff.PerformLayout();
         this.IdlePage.ResumeLayout(false);
         this.IdlePage.PerformLayout();
         this.InvertedPage.ResumeLayout(false);
         this.InvertedPage.PerformLayout();
         this.ShutdownPage.ResumeLayout(false);
         this.ShutdownPage.PerformLayout();
         this.ChargerCurrentPage.ResumeLayout(false);
         this.ChargerCurrentPage.PerformLayout();
         this.ChargerShutdown.ResumeLayout(false);
         this.ChargerShutdown.PerformLayout();
         this.tabControl3.ResumeLayout(false);
         this.AdcPage.ResumeLayout(false);
         this.AdcPage.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage GeneralPage;
      private System.Windows.Forms.TabPage PowerTestsPage;
      private System.Windows.Forms.Button StornoButton;
      private System.Windows.Forms.Button SaveButton;
      private System.Windows.Forms.TabControl tabControl2;
      private System.Windows.Forms.TabPage BlOnPage;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox BlOnMaxTextBox;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox BlOnMinTextBox;
      private System.Windows.Forms.TabPage BlOff;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox BlOffMaxTextBox;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox BlOffMinTextBox;
      private System.Windows.Forms.TabPage IdlePage;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.TextBox IdleMaxTextBox;
      private System.Windows.Forms.TabPage InvertedPage;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.TextBox InvertedMaxTextBox;
      private System.Windows.Forms.TabPage ShutdownPage;
      private System.Windows.Forms.TextBox ShutdownCurrentTextBox;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TextBox ShutdownStepsTextBox;
      private System.Windows.Forms.TabPage ChargerCurrentPage;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.TextBox ChargerCurrentMaxTextBox;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.TextBox ChargerCurrentMinTextBox;
      private System.Windows.Forms.TabPage ChargerShutdown;
      private System.Windows.Forms.TextBox ChargerShutdownCurrentTextBox;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.TextBox ChargerShutdownStepsTextBox;
      private System.Windows.Forms.TabPage OtherTestsPage;
      private System.Windows.Forms.TabControl tabControl3;
      private System.Windows.Forms.TabPage AdcPage;
      private System.Windows.Forms.TabPage IadcPage;
      private System.Windows.Forms.TabPage UsbPage;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.TextBox AdcSuccessfullTextBox;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.TextBox AdcTrialsTextBox;
   }
}