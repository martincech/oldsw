﻿using System;
using System.Windows.Forms;
using Bat1.Tester;
namespace Bat1TestDemo {
   public partial class GeneralSettings : Form {
      Form1 ParentForm;

      public GeneralSettings(Form1 _ParentForm) {
         InitializeComponent();
         ParentForm = _ParentForm;
      }
      public int i = 0;


      private void StornoButton_Click(object sender, EventArgs e) {
         this.DialogResult = DialogResult.Cancel;
      }

      private void GeneralSettings_Load(object sender, EventArgs e) {
         /*// General
         ProductionFirmwarePathTextBox.Text = ParentForm.Tester.TesterConfigStatic.FirmwarePath[(int)TesterConfig.FirmwareType.Production];
         TestFirmwarePathTextBox.Text = ParentForm.Tester.TesterConfigStatic.FirmwarePath[(int)TesterConfig.FirmwareType.Test];

         // FTDI
         DeviceNameTextBox.Text = ParentForm.Tester.UsbConfig.UsbDeviceName;
         DeviceManufacturerTextBox.Text = ParentForm.Tester.UsbConfig.UsbDeviceManufacturer;
         DeviceMaxPowerTextBox.Text = ParentForm.Tester.UsbConfig.UsbMaxPower.ToString();

         ProgrammedNamesTextBox.Text = String.Join(" ; ", ParentForm.Tester.UsbConfig.FtdiNamesProgrammed);
         BlankNamesTextBox.Text = String.Join(" ; ", ParentForm.Tester.UsbConfig.FtdiNamesBlank);*/
      }

      private void SaveButton_Click(object sender, EventArgs e) {
         try {
           /* // General
            ParentForm.Tester.Config.FirmwarePath[(int)TesterConfig.FirmwareType.Production] = ProductionFirmwarePathTextBox.Text;
            ParentForm.Tester.Config.FirmwarePath[(int)TesterConfig.FirmwareType.Test] = TestFirmwarePathTextBox.Text;

            // FTDI
            ParentForm.Tester.UsbConfig.UsbDeviceName = DeviceNameTextBox.Text;
            ParentForm.Tester.UsbConfig.UsbDeviceManufacturer = DeviceManufacturerTextBox.Text;
        
            uint Value = 0;

            bool Done = UInt32.TryParse(DeviceMaxPowerTextBox.Text, out Value);

            if(!Done || Value > 500) {
               throw new Exception("USB max power invalid value");
            }

            ParentForm.Tester.UsbConfig.UsbMaxPower = (ushort)Value;
            */
            this.DialogResult = DialogResult.OK;
         } catch(Exception ex) {
            MessageBox.Show(ex.Message,
               "Bad data input",
               MessageBoxButtons.OK,
               MessageBoxIcon.Exclamation,
               MessageBoxDefaultButton.Button1);
         }
      }
   }
}
