﻿namespace Bat1TestDemo {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         this.ProductionFirmwareButton = new System.Windows.Forms.Button();
         this.OperationProgressBar = new System.Windows.Forms.ProgressBar();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.StatusTextBox = new System.Windows.Forms.TextBox();
         this.TestFirmwareButton = new System.Windows.Forms.Button();
         this.TestKeyboardButton = new System.Windows.Forms.Button();
         this.TestPowerIdleButton = new System.Windows.Forms.Button();
         this.SwitchTargetOnButton = new System.Windows.Forms.Button();
         this.SwitchOffButton = new System.Windows.Forms.Button();
         this.TestPowerInvertedButton = new System.Windows.Forms.Button();
         this.TestPowerBacklightOnButton = new System.Windows.Forms.Button();
         this.TestPowerBacklightOffButton = new System.Windows.Forms.Button();
         this.FTDIProgrammingButton = new System.Windows.Forms.Button();
         this.TestMemoryButton = new System.Windows.Forms.Button();
         this.TestSoundButton = new System.Windows.Forms.Button();
         this.TestRtcButton = new System.Windows.Forms.Button();
         this.TestPrinterComButton = new System.Windows.Forms.Button();
         this.TestAdcButton = new System.Windows.Forms.Button();
         this.TestInternalAdcButton = new System.Windows.Forms.Button();
         this.TestDisplayButton = new System.Windows.Forms.Button();
         this.TestUsbButton = new System.Windows.Forms.Button();
         this.ChargerConnectionTestButton = new System.Windows.Forms.Button();
         this.ChargerCurrentTextButton = new System.Windows.Forms.Button();
         this.ChargerShutdownTestButton = new System.Windows.Forms.Button();
         this.TargetShuwdownTestButton = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.FirmwareLabel = new System.Windows.Forms.Label();
         this.GetFirmwareButton = new System.Windows.Forms.Button();
         this.button1 = new System.Windows.Forms.Button();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.BatchButton = new System.Windows.Forms.Button();
         this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.WriteConfigButton = new System.Windows.Forms.Button();
         this.configurationFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.groupBox1.SuspendLayout();
         this.menuStrip1.SuspendLayout();
         this.SuspendLayout();
         // 
         // ProductionFirmwareButton
         // 
         this.ProductionFirmwareButton.Location = new System.Drawing.Point(157, 41);
         this.ProductionFirmwareButton.Name = "ProductionFirmwareButton";
         this.ProductionFirmwareButton.Size = new System.Drawing.Size(115, 23);
         this.ProductionFirmwareButton.TabIndex = 0;
         this.ProductionFirmwareButton.Text = "Production firmware";
         this.ProductionFirmwareButton.UseVisualStyleBackColor = true;
         this.ProductionFirmwareButton.Click += new System.EventHandler(this.ProductionFirmwareButton_Click);
         // 
         // OperationProgressBar
         // 
         this.OperationProgressBar.Location = new System.Drawing.Point(172, 12);
         this.OperationProgressBar.Name = "OperationProgressBar";
         this.OperationProgressBar.Size = new System.Drawing.Size(100, 23);
         this.OperationProgressBar.TabIndex = 1;
         // 
         // timer1
         // 
         this.timer1.Enabled = true;
         this.timer1.Interval = 500;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // StatusTextBox
         // 
         this.StatusTextBox.Location = new System.Drawing.Point(29, 94);
         this.StatusTextBox.Multiline = true;
         this.StatusTextBox.Name = "StatusTextBox";
         this.StatusTextBox.Size = new System.Drawing.Size(338, 128);
         this.StatusTextBox.TabIndex = 2;
         // 
         // TestFirmwareButton
         // 
         this.TestFirmwareButton.Location = new System.Drawing.Point(436, 152);
         this.TestFirmwareButton.Name = "TestFirmwareButton";
         this.TestFirmwareButton.Size = new System.Drawing.Size(115, 23);
         this.TestFirmwareButton.TabIndex = 3;
         this.TestFirmwareButton.Text = "Test firmware";
         this.TestFirmwareButton.UseVisualStyleBackColor = true;
         this.TestFirmwareButton.Click += new System.EventHandler(this.TestFirmwareButton_Click);
         // 
         // TestKeyboardButton
         // 
         this.TestKeyboardButton.Location = new System.Drawing.Point(6, 19);
         this.TestKeyboardButton.Name = "TestKeyboardButton";
         this.TestKeyboardButton.Size = new System.Drawing.Size(93, 23);
         this.TestKeyboardButton.TabIndex = 4;
         this.TestKeyboardButton.Text = "Test keyboard";
         this.TestKeyboardButton.UseVisualStyleBackColor = true;
         this.TestKeyboardButton.Click += new System.EventHandler(this.TestKeyboardButton_Click);
         // 
         // TestPowerIdleButton
         // 
         this.TestPowerIdleButton.Location = new System.Drawing.Point(765, 237);
         this.TestPowerIdleButton.Name = "TestPowerIdleButton";
         this.TestPowerIdleButton.Size = new System.Drawing.Size(116, 23);
         this.TestPowerIdleButton.TabIndex = 5;
         this.TestPowerIdleButton.Text = "Test power idle";
         this.TestPowerIdleButton.UseVisualStyleBackColor = true;
         this.TestPowerIdleButton.Click += new System.EventHandler(this.TestPowerIdleButton_Click);
         // 
         // SwitchTargetOnButton
         // 
         this.SwitchTargetOnButton.Location = new System.Drawing.Point(643, 108);
         this.SwitchTargetOnButton.Name = "SwitchTargetOnButton";
         this.SwitchTargetOnButton.Size = new System.Drawing.Size(116, 23);
         this.SwitchTargetOnButton.TabIndex = 6;
         this.SwitchTargetOnButton.Text = "Switch target on";
         this.SwitchTargetOnButton.UseVisualStyleBackColor = true;
         this.SwitchTargetOnButton.Click += new System.EventHandler(this.SwitchTargetOnButton_Click);
         // 
         // SwitchOffButton
         // 
         this.SwitchOffButton.Location = new System.Drawing.Point(765, 108);
         this.SwitchOffButton.Name = "SwitchOffButton";
         this.SwitchOffButton.Size = new System.Drawing.Size(116, 23);
         this.SwitchOffButton.TabIndex = 7;
         this.SwitchOffButton.Text = "Switch target off";
         this.SwitchOffButton.UseVisualStyleBackColor = true;
         this.SwitchOffButton.Click += new System.EventHandler(this.SwitchOffButton_Click);
         // 
         // TestPowerInvertedButton
         // 
         this.TestPowerInvertedButton.Location = new System.Drawing.Point(643, 295);
         this.TestPowerInvertedButton.Name = "TestPowerInvertedButton";
         this.TestPowerInvertedButton.Size = new System.Drawing.Size(124, 23);
         this.TestPowerInvertedButton.TabIndex = 8;
         this.TestPowerInvertedButton.Text = "Test power inverted";
         this.TestPowerInvertedButton.UseVisualStyleBackColor = true;
         this.TestPowerInvertedButton.Click += new System.EventHandler(this.TestPowerInvertedButton_Click);
         // 
         // TestPowerBacklightOnButton
         // 
         this.TestPowerBacklightOnButton.Location = new System.Drawing.Point(643, 266);
         this.TestPowerBacklightOnButton.Name = "TestPowerBacklightOnButton";
         this.TestPowerBacklightOnButton.Size = new System.Drawing.Size(116, 23);
         this.TestPowerBacklightOnButton.TabIndex = 9;
         this.TestPowerBacklightOnButton.Text = "Test power BL on";
         this.TestPowerBacklightOnButton.UseVisualStyleBackColor = true;
         this.TestPowerBacklightOnButton.Click += new System.EventHandler(this.TestPowerBacklightOnButton_Click);
         // 
         // TestPowerBacklightOffButton
         // 
         this.TestPowerBacklightOffButton.Location = new System.Drawing.Point(765, 266);
         this.TestPowerBacklightOffButton.Name = "TestPowerBacklightOffButton";
         this.TestPowerBacklightOffButton.Size = new System.Drawing.Size(116, 23);
         this.TestPowerBacklightOffButton.TabIndex = 10;
         this.TestPowerBacklightOffButton.Text = "Test power BLt off";
         this.TestPowerBacklightOffButton.UseVisualStyleBackColor = true;
         this.TestPowerBacklightOffButton.Click += new System.EventHandler(this.TestPowerBacklightOffButton_Click);
         // 
         // FTDIProgrammingButton
         // 
         this.FTDIProgrammingButton.Location = new System.Drawing.Point(12, 54);
         this.FTDIProgrammingButton.Name = "FTDIProgrammingButton";
         this.FTDIProgrammingButton.Size = new System.Drawing.Size(115, 23);
         this.FTDIProgrammingButton.TabIndex = 11;
         this.FTDIProgrammingButton.Text = "FTDI programming";
         this.FTDIProgrammingButton.UseVisualStyleBackColor = true;
         this.FTDIProgrammingButton.Click += new System.EventHandler(this.FTDIProgrammingButton_Click);
         // 
         // TestMemoryButton
         // 
         this.TestMemoryButton.Location = new System.Drawing.Point(6, 48);
         this.TestMemoryButton.Name = "TestMemoryButton";
         this.TestMemoryButton.Size = new System.Drawing.Size(93, 23);
         this.TestMemoryButton.TabIndex = 12;
         this.TestMemoryButton.Text = "Memory";
         this.TestMemoryButton.UseVisualStyleBackColor = true;
         this.TestMemoryButton.Click += new System.EventHandler(this.TestMemoryButton_Click);
         // 
         // TestSoundButton
         // 
         this.TestSoundButton.Location = new System.Drawing.Point(6, 105);
         this.TestSoundButton.Name = "TestSoundButton";
         this.TestSoundButton.Size = new System.Drawing.Size(93, 23);
         this.TestSoundButton.TabIndex = 13;
         this.TestSoundButton.Text = "Sound";
         this.TestSoundButton.UseVisualStyleBackColor = true;
         this.TestSoundButton.Click += new System.EventHandler(this.TestSoundButton_Click);
         // 
         // TestRtcButton
         // 
         this.TestRtcButton.Location = new System.Drawing.Point(105, 48);
         this.TestRtcButton.Name = "TestRtcButton";
         this.TestRtcButton.Size = new System.Drawing.Size(93, 23);
         this.TestRtcButton.TabIndex = 14;
         this.TestRtcButton.Text = "RTC";
         this.TestRtcButton.UseVisualStyleBackColor = true;
         this.TestRtcButton.Click += new System.EventHandler(this.TestRtcButton_Click);
         // 
         // TestPrinterComButton
         // 
         this.TestPrinterComButton.Location = new System.Drawing.Point(105, 105);
         this.TestPrinterComButton.Name = "TestPrinterComButton";
         this.TestPrinterComButton.Size = new System.Drawing.Size(93, 23);
         this.TestPrinterComButton.TabIndex = 15;
         this.TestPrinterComButton.Text = "Printer COM";
         this.TestPrinterComButton.UseVisualStyleBackColor = true;
         this.TestPrinterComButton.Click += new System.EventHandler(this.TestPrinterComButton_Click);
         // 
         // TestAdcButton
         // 
         this.TestAdcButton.Location = new System.Drawing.Point(6, 76);
         this.TestAdcButton.Name = "TestAdcButton";
         this.TestAdcButton.Size = new System.Drawing.Size(93, 23);
         this.TestAdcButton.TabIndex = 16;
         this.TestAdcButton.Text = "ADC";
         this.TestAdcButton.UseVisualStyleBackColor = true;
         this.TestAdcButton.Click += new System.EventHandler(this.TestAdcButton_Click);
         // 
         // TestInternalAdcButton
         // 
         this.TestInternalAdcButton.Location = new System.Drawing.Point(105, 77);
         this.TestInternalAdcButton.Name = "TestInternalAdcButton";
         this.TestInternalAdcButton.Size = new System.Drawing.Size(93, 23);
         this.TestInternalAdcButton.TabIndex = 17;
         this.TestInternalAdcButton.Text = "Internal ADC";
         this.TestInternalAdcButton.UseVisualStyleBackColor = true;
         this.TestInternalAdcButton.Click += new System.EventHandler(this.TestInternalAdcButton_Click);
         // 
         // TestDisplayButton
         // 
         this.TestDisplayButton.Location = new System.Drawing.Point(105, 19);
         this.TestDisplayButton.Name = "TestDisplayButton";
         this.TestDisplayButton.Size = new System.Drawing.Size(93, 23);
         this.TestDisplayButton.TabIndex = 18;
         this.TestDisplayButton.Text = "Display";
         this.TestDisplayButton.UseVisualStyleBackColor = true;
         this.TestDisplayButton.Click += new System.EventHandler(this.TestDisplayButton_Click);
         // 
         // TestUsbButton
         // 
         this.TestUsbButton.Location = new System.Drawing.Point(6, 134);
         this.TestUsbButton.Name = "TestUsbButton";
         this.TestUsbButton.Size = new System.Drawing.Size(93, 23);
         this.TestUsbButton.TabIndex = 19;
         this.TestUsbButton.Text = "USB";
         this.TestUsbButton.UseVisualStyleBackColor = true;
         this.TestUsbButton.Click += new System.EventHandler(this.TestUsbButton_Click);
         // 
         // ChargerConnectionTestButton
         // 
         this.ChargerConnectionTestButton.Location = new System.Drawing.Point(105, 134);
         this.ChargerConnectionTestButton.Name = "ChargerConnectionTestButton";
         this.ChargerConnectionTestButton.Size = new System.Drawing.Size(91, 23);
         this.ChargerConnectionTestButton.TabIndex = 20;
         this.ChargerConnectionTestButton.Text = "Charger con";
         this.ChargerConnectionTestButton.UseVisualStyleBackColor = true;
         this.ChargerConnectionTestButton.Click += new System.EventHandler(this.ChargerConnectionTestButton_Click);
         // 
         // ChargerCurrentTextButton
         // 
         this.ChargerCurrentTextButton.Location = new System.Drawing.Point(765, 152);
         this.ChargerCurrentTextButton.Name = "ChargerCurrentTextButton";
         this.ChargerCurrentTextButton.Size = new System.Drawing.Size(116, 23);
         this.ChargerCurrentTextButton.TabIndex = 21;
         this.ChargerCurrentTextButton.Text = "Charger current";
         this.ChargerCurrentTextButton.UseVisualStyleBackColor = true;
         this.ChargerCurrentTextButton.Click += new System.EventHandler(this.ChargerCurrentTextButton_Click);
         // 
         // ChargerShutdownTestButton
         // 
         this.ChargerShutdownTestButton.Location = new System.Drawing.Point(643, 152);
         this.ChargerShutdownTestButton.Name = "ChargerShutdownTestButton";
         this.ChargerShutdownTestButton.Size = new System.Drawing.Size(116, 23);
         this.ChargerShutdownTestButton.TabIndex = 22;
         this.ChargerShutdownTestButton.Text = "Charger shut down";
         this.ChargerShutdownTestButton.UseVisualStyleBackColor = true;
         this.ChargerShutdownTestButton.Click += new System.EventHandler(this.ChargerShutdownTestButton_Click);
         // 
         // TargetShuwdownTestButton
         // 
         this.TargetShuwdownTestButton.Location = new System.Drawing.Point(643, 237);
         this.TargetShuwdownTestButton.Name = "TargetShuwdownTestButton";
         this.TargetShuwdownTestButton.Size = new System.Drawing.Size(116, 23);
         this.TargetShuwdownTestButton.TabIndex = 23;
         this.TargetShuwdownTestButton.Text = "Target shut down";
         this.TargetShuwdownTestButton.UseVisualStyleBackColor = true;
         this.TargetShuwdownTestButton.Click += new System.EventHandler(this.TargetShuwdownTestButton_Click);
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(422, 108);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(92, 13);
         this.label1.TabIndex = 24;
         this.label1.Text = "Test firmware only";
         // 
         // FirmwareLabel
         // 
         this.FirmwareLabel.AutoSize = true;
         this.FirmwareLabel.Location = new System.Drawing.Point(537, 28);
         this.FirmwareLabel.Name = "FirmwareLabel";
         this.FirmwareLabel.Size = new System.Drawing.Size(53, 13);
         this.FirmwareLabel.TabIndex = 27;
         this.FirmwareLabel.Text = "Unknown";
         // 
         // GetFirmwareButton
         // 
         this.GetFirmwareButton.Location = new System.Drawing.Point(628, 23);
         this.GetFirmwareButton.Name = "GetFirmwareButton";
         this.GetFirmwareButton.Size = new System.Drawing.Size(115, 23);
         this.GetFirmwareButton.TabIndex = 28;
         this.GetFirmwareButton.Text = "Get firmware";
         this.GetFirmwareButton.UseVisualStyleBackColor = true;
         this.GetFirmwareButton.Click += new System.EventHandler(this.GetFirmwareButton_Click);
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(792, 18);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(177, 23);
         this.button1.TabIndex = 29;
         this.button1.Text = "Bat1dll device present";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.TestKeyboardButton);
         this.groupBox1.Controls.Add(this.TestMemoryButton);
         this.groupBox1.Controls.Add(this.TestSoundButton);
         this.groupBox1.Controls.Add(this.TestRtcButton);
         this.groupBox1.Controls.Add(this.TestPrinterComButton);
         this.groupBox1.Controls.Add(this.TestAdcButton);
         this.groupBox1.Controls.Add(this.TestInternalAdcButton);
         this.groupBox1.Controls.Add(this.TestDisplayButton);
         this.groupBox1.Controls.Add(this.TestUsbButton);
         this.groupBox1.Controls.Add(this.ChargerConnectionTestButton);
         this.groupBox1.Location = new System.Drawing.Point(386, 190);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(204, 168);
         this.groupBox1.TabIndex = 30;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Test firmware only";
         // 
         // BatchButton
         // 
         this.BatchButton.Location = new System.Drawing.Point(765, 403);
         this.BatchButton.Name = "BatchButton";
         this.BatchButton.Size = new System.Drawing.Size(75, 23);
         this.BatchButton.TabIndex = 31;
         this.BatchButton.Text = "Batch";
         this.BatchButton.UseVisualStyleBackColor = true;
         this.BatchButton.Click += new System.EventHandler(this.BatchButton_Click);
         // 
         // menuStrip1
         // 
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Size = new System.Drawing.Size(1010, 24);
         this.menuStrip1.TabIndex = 32;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // fileToolStripMenuItem
         // 
         this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
         this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
         this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
         this.fileToolStripMenuItem.Text = "File";
         // 
         // exitToolStripMenuItem
         // 
         this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
         this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
         this.exitToolStripMenuItem.Text = "Exit";
         this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
         // 
         // settingsToolStripMenuItem
         // 
         this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configurationFilesToolStripMenuItem});
         this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
         this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
         this.settingsToolStripMenuItem.Text = "Settings";
         // 
         // WriteConfigButton
         // 
         this.WriteConfigButton.Location = new System.Drawing.Point(89, 403);
         this.WriteConfigButton.Name = "WriteConfigButton";
         this.WriteConfigButton.Size = new System.Drawing.Size(233, 23);
         this.WriteConfigButton.TabIndex = 33;
         this.WriteConfigButton.Text = "Write configuration";
         this.WriteConfigButton.UseVisualStyleBackColor = true;
         this.WriteConfigButton.Click += new System.EventHandler(this.WriteConfigButton_Click);
         // 
         // configurationFilesToolStripMenuItem
         // 
         this.configurationFilesToolStripMenuItem.Name = "configurationFilesToolStripMenuItem";
         this.configurationFilesToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
         this.configurationFilesToolStripMenuItem.Text = "Configuration Files";
         this.configurationFilesToolStripMenuItem.Click += new System.EventHandler(this.configurationFilesToolStripMenuItem_Click);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(1010, 455);
         this.Controls.Add(this.WriteConfigButton);
         this.Controls.Add(this.BatchButton);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.button1);
         this.Controls.Add(this.GetFirmwareButton);
         this.Controls.Add(this.FirmwareLabel);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.TargetShuwdownTestButton);
         this.Controls.Add(this.ChargerShutdownTestButton);
         this.Controls.Add(this.ChargerCurrentTextButton);
         this.Controls.Add(this.FTDIProgrammingButton);
         this.Controls.Add(this.TestPowerBacklightOffButton);
         this.Controls.Add(this.TestPowerBacklightOnButton);
         this.Controls.Add(this.TestPowerInvertedButton);
         this.Controls.Add(this.SwitchOffButton);
         this.Controls.Add(this.SwitchTargetOnButton);
         this.Controls.Add(this.TestPowerIdleButton);
         this.Controls.Add(this.TestFirmwareButton);
         this.Controls.Add(this.StatusTextBox);
         this.Controls.Add(this.OperationProgressBar);
         this.Controls.Add(this.ProductionFirmwareButton);
         this.Controls.Add(this.menuStrip1);
         this.MainMenuStrip = this.menuStrip1;
         this.Name = "Form1";
         this.Text = "Form1";
         this.groupBox1.ResumeLayout(false);
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button ProductionFirmwareButton;
      private System.Windows.Forms.ProgressBar OperationProgressBar;
      private System.Windows.Forms.Timer timer1;
      private System.Windows.Forms.TextBox StatusTextBox;
      private System.Windows.Forms.Button TestFirmwareButton;
      private System.Windows.Forms.Button TestKeyboardButton;
      private System.Windows.Forms.Button TestPowerIdleButton;
      private System.Windows.Forms.Button SwitchTargetOnButton;
      private System.Windows.Forms.Button SwitchOffButton;
      private System.Windows.Forms.Button TestPowerInvertedButton;
      private System.Windows.Forms.Button TestPowerBacklightOnButton;
      private System.Windows.Forms.Button TestPowerBacklightOffButton;
      private System.Windows.Forms.Button FTDIProgrammingButton;
      private System.Windows.Forms.Button TestMemoryButton;
      private System.Windows.Forms.Button TestSoundButton;
      private System.Windows.Forms.Button TestRtcButton;
      private System.Windows.Forms.Button TestPrinterComButton;
      private System.Windows.Forms.Button TestAdcButton;
      private System.Windows.Forms.Button TestInternalAdcButton;
      private System.Windows.Forms.Button TestDisplayButton;
      private System.Windows.Forms.Button TestUsbButton;
      private System.Windows.Forms.Button ChargerConnectionTestButton;
      private System.Windows.Forms.Button ChargerCurrentTextButton;
      private System.Windows.Forms.Button ChargerShutdownTestButton;
      private System.Windows.Forms.Button TargetShuwdownTestButton;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label FirmwareLabel;
      private System.Windows.Forms.Button GetFirmwareButton;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button BatchButton;
      private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
      private System.Windows.Forms.Button WriteConfigButton;
      private System.Windows.Forms.ToolStripMenuItem configurationFilesToolStripMenuItem;
   }
}

