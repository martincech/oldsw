﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Bat1TestDemo {
   public partial class TestSettings : Form {
      public TestSettings() {
         InitializeComponent();
      }

      private void SaveButton_Click(object sender, EventArgs e) {
         try {

            this.DialogResult = DialogResult.OK;
         } catch(Exception ex) {
            MessageBox.Show(ex.Message,
               "Bad data input",
               MessageBoxButtons.OK,
               MessageBoxIcon.Exclamation,
               MessageBoxDefaultButton.Button1);
         }
      }

      private void StornoButton_Click(object sender, EventArgs e) {
         this.DialogResult = DialogResult.Cancel;
      }
   }
}
