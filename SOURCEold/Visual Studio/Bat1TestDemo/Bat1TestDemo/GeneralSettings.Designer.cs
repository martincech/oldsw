﻿namespace Bat1TestDemo {
   partial class GeneralSettings {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.SaveButton = new System.Windows.Forms.Button();
         this.StornoButton = new System.Windows.Forms.Button();
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.GeneralPage = new System.Windows.Forms.TabPage();
         this.label2 = new System.Windows.Forms.Label();
         this.label1 = new System.Windows.Forms.Label();
         this.ProductionFirmwarePathTextBox = new System.Windows.Forms.TextBox();
         this.TestFirmwarePathTextBox = new System.Windows.Forms.TextBox();
         this.FtdiPage = new System.Windows.Forms.TabPage();
         this.BlankNamesTextBox = new System.Windows.Forms.TextBox();
         this.label7 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.ProgrammedNamesTextBox = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.DeviceMaxPowerTextBox = new System.Windows.Forms.TextBox();
         this.DeviceManufacturerTextBox = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.DeviceNameTextBox = new System.Windows.Forms.TextBox();
         this.tabControl1.SuspendLayout();
         this.GeneralPage.SuspendLayout();
         this.FtdiPage.SuspendLayout();
         this.SuspendLayout();
         // 
         // SaveButton
         // 
         this.SaveButton.Location = new System.Drawing.Point(104, 500);
         this.SaveButton.Name = "SaveButton";
         this.SaveButton.Size = new System.Drawing.Size(75, 23);
         this.SaveButton.TabIndex = 0;
         this.SaveButton.Text = "Save";
         this.SaveButton.UseVisualStyleBackColor = true;
         this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
         // 
         // StornoButton
         // 
         this.StornoButton.Location = new System.Drawing.Point(522, 500);
         this.StornoButton.Name = "StornoButton";
         this.StornoButton.Size = new System.Drawing.Size(75, 23);
         this.StornoButton.TabIndex = 1;
         this.StornoButton.Text = "Storno";
         this.StornoButton.UseVisualStyleBackColor = true;
         this.StornoButton.Click += new System.EventHandler(this.StornoButton_Click);
         // 
         // tabControl1
         // 
         this.tabControl1.Controls.Add(this.GeneralPage);
         this.tabControl1.Controls.Add(this.FtdiPage);
         this.tabControl1.Location = new System.Drawing.Point(23, 28);
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(641, 402);
         this.tabControl1.TabIndex = 2;
         // 
         // GeneralPage
         // 
         this.GeneralPage.Controls.Add(this.label2);
         this.GeneralPage.Controls.Add(this.label1);
         this.GeneralPage.Controls.Add(this.ProductionFirmwarePathTextBox);
         this.GeneralPage.Controls.Add(this.TestFirmwarePathTextBox);
         this.GeneralPage.Location = new System.Drawing.Point(4, 22);
         this.GeneralPage.Name = "GeneralPage";
         this.GeneralPage.Padding = new System.Windows.Forms.Padding(3);
         this.GeneralPage.Size = new System.Drawing.Size(633, 376);
         this.GeneralPage.TabIndex = 0;
         this.GeneralPage.Text = "General";
         this.GeneralPage.UseVisualStyleBackColor = true;
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(16, 78);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(100, 13);
         this.label2.TabIndex = 3;
         this.label2.Text = "Production firmware";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(16, 39);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(70, 13);
         this.label1.TabIndex = 2;
         this.label1.Text = "Test firmware";
         // 
         // ProductionFirmwarePathTextBox
         // 
         this.ProductionFirmwarePathTextBox.Location = new System.Drawing.Point(122, 75);
         this.ProductionFirmwarePathTextBox.Name = "ProductionFirmwarePathTextBox";
         this.ProductionFirmwarePathTextBox.Size = new System.Drawing.Size(369, 20);
         this.ProductionFirmwarePathTextBox.TabIndex = 1;
         // 
         // TestFirmwarePathTextBox
         // 
         this.TestFirmwarePathTextBox.Location = new System.Drawing.Point(122, 36);
         this.TestFirmwarePathTextBox.Name = "TestFirmwarePathTextBox";
         this.TestFirmwarePathTextBox.Size = new System.Drawing.Size(369, 20);
         this.TestFirmwarePathTextBox.TabIndex = 0;
         // 
         // FtdiPage
         // 
         this.FtdiPage.Controls.Add(this.BlankNamesTextBox);
         this.FtdiPage.Controls.Add(this.label7);
         this.FtdiPage.Controls.Add(this.label6);
         this.FtdiPage.Controls.Add(this.ProgrammedNamesTextBox);
         this.FtdiPage.Controls.Add(this.label5);
         this.FtdiPage.Controls.Add(this.DeviceMaxPowerTextBox);
         this.FtdiPage.Controls.Add(this.DeviceManufacturerTextBox);
         this.FtdiPage.Controls.Add(this.label4);
         this.FtdiPage.Controls.Add(this.label3);
         this.FtdiPage.Controls.Add(this.DeviceNameTextBox);
         this.FtdiPage.Location = new System.Drawing.Point(4, 22);
         this.FtdiPage.Name = "FtdiPage";
         this.FtdiPage.Padding = new System.Windows.Forms.Padding(3);
         this.FtdiPage.Size = new System.Drawing.Size(633, 376);
         this.FtdiPage.TabIndex = 1;
         this.FtdiPage.Text = "FTDI";
         this.FtdiPage.UseVisualStyleBackColor = true;
         // 
         // BlankNamesTextBox
         // 
         this.BlankNamesTextBox.Location = new System.Drawing.Point(175, 223);
         this.BlankNamesTextBox.Name = "BlankNamesTextBox";
         this.BlankNamesTextBox.Size = new System.Drawing.Size(310, 20);
         this.BlankNamesTextBox.TabIndex = 9;
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(23, 226);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(110, 13);
         this.label7.TabIndex = 8;
         this.label7.Text = "Blank device\'s names";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point(23, 186);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(142, 13);
         this.label6.TabIndex = 7;
         this.label6.Text = "Programmed device\'s names";
         // 
         // ProgrammedNamesTextBox
         // 
         this.ProgrammedNamesTextBox.Location = new System.Drawing.Point(175, 183);
         this.ProgrammedNamesTextBox.Name = "ProgrammedNamesTextBox";
         this.ProgrammedNamesTextBox.Size = new System.Drawing.Size(310, 20);
         this.ProgrammedNamesTextBox.TabIndex = 6;
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point(23, 117);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(59, 13);
         this.label5.TabIndex = 5;
         this.label5.Text = "Max power";
         // 
         // DeviceMaxPowerTextBox
         // 
         this.DeviceMaxPowerTextBox.Location = new System.Drawing.Point(175, 114);
         this.DeviceMaxPowerTextBox.Name = "DeviceMaxPowerTextBox";
         this.DeviceMaxPowerTextBox.Size = new System.Drawing.Size(100, 20);
         this.DeviceMaxPowerTextBox.TabIndex = 4;
         // 
         // DeviceManufacturerTextBox
         // 
         this.DeviceManufacturerTextBox.Location = new System.Drawing.Point(175, 78);
         this.DeviceManufacturerTextBox.Name = "DeviceManufacturerTextBox";
         this.DeviceManufacturerTextBox.Size = new System.Drawing.Size(310, 20);
         this.DeviceManufacturerTextBox.TabIndex = 3;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(23, 78);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(106, 13);
         this.label4.TabIndex = 2;
         this.label4.Text = "Device manufacturer";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(23, 39);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(70, 13);
         this.label3.TabIndex = 1;
         this.label3.Text = "Device name";
         // 
         // DeviceNameTextBox
         // 
         this.DeviceNameTextBox.Location = new System.Drawing.Point(175, 39);
         this.DeviceNameTextBox.Name = "DeviceNameTextBox";
         this.DeviceNameTextBox.Size = new System.Drawing.Size(310, 20);
         this.DeviceNameTextBox.TabIndex = 0;
         // 
         // Settings
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(894, 629);
         this.Controls.Add(this.tabControl1);
         this.Controls.Add(this.StornoButton);
         this.Controls.Add(this.SaveButton);
         this.Name = "Settings";
         this.Text = "Settings";
         this.Load += new System.EventHandler(this.GeneralSettings_Load);
         this.tabControl1.ResumeLayout(false);
         this.GeneralPage.ResumeLayout(false);
         this.GeneralPage.PerformLayout();
         this.FtdiPage.ResumeLayout(false);
         this.FtdiPage.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.Button SaveButton;
      private System.Windows.Forms.Button StornoButton;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage GeneralPage;
      private System.Windows.Forms.TabPage FtdiPage;
      private System.Windows.Forms.TextBox TestFirmwarePathTextBox;
      private System.Windows.Forms.TextBox ProductionFirmwarePathTextBox;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.TextBox DeviceNameTextBox;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox DeviceMaxPowerTextBox;
      private System.Windows.Forms.TextBox DeviceManufacturerTextBox;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox BlankNamesTextBox;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.TextBox ProgrammedNamesTextBox;
   }
}