﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Configuration;
using Bat1.Tester;

namespace Bat1TestDemo {


   public partial class Form1 : Form {
      public Tester Tester;
      private BackgroundOperation Operation;

      private Crt StatusWindow;

      public Form1() {
         InitializeComponent();

         StatusWindow = new Crt(StatusTextBox);


        // Bat1.Tester.Config.ConfigurationFiless CFiles = ConfigurationManager.GetSection("ConfigurationFiless") as Bat1.Tester.Config.ConfigurationFiless;


         Tester = new Tester(StatusWindow);
         Operation = Tester.Operation;
      }




      private void ProductionFirmwareButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.LOAD_PRODUCTION_FIRMWARE);
         });

         Operation.Thread.Start();
      }




      private void timer1_Tick(object sender, EventArgs e) {
         if(Operation.Thread != null && Operation.Worker != null) {
            if(Operation.Thread.IsAlive) {
               OperationProgressBar.Value = Math.Min((int)(Operation.Worker.Progress * 100), 100);
               return;
            }
         }

         OperationProgressBar.Value = 0;

         string Firmware = "";

         switch(Tester.Firmware) {
            case FirmwareType.PRODUCTION:
               Firmware = "Production";
               break;
            case FirmwareType.TEST:
               Firmware = "Test";
               break;
            default:
               Firmware = "Unknown";
               break;
         }

         FirmwareLabel.Text = Firmware;

      }

      private void TestFirmwareButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.LOAD_TEST_FIRMWARE);
         });

         Operation.Thread.Start();
      }

      private void TestKeyboardButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_KEYBOARD);
         });

         Operation.Thread.Start();
      }

      private void SwitchTargetOnButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.SWITCH_ON);
         });

         Operation.Thread.Start();
      }

      private void SwitchOffButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.SWITCH_OFF);
         });

         Operation.Thread.Start();
      }



      private void TestPowerBacklightOnButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_POWER_BACKLIGHT_ON);
         });

         Operation.Thread.Start();
      }

      private void TestPowerBacklightOffButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_POWER_BACKLIGHT_OFF);
         });

         Operation.Thread.Start();
      }

      private void TestPowerIdleButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_POWER_IDLE);
         });

         Operation.Thread.Start();
      }

      private void TestPowerInvertedButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_POWER_INVERTED);
         });

         Operation.Thread.Start();
      }

      private void FTDIProgrammingButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.FTDI_PROGRAMMING);
         });

         Operation.Thread.Start();
      }

      private void TestMemoryButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_MEMORY);
         });

         Operation.Thread.Start();
      }

      private void TestSoundButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_SOUND);
         });

         Operation.Thread.Start();
      }

      private void TestRtcButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_RTC);
         });

         Operation.Thread.Start();
      }

      private void TestDisplayButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_DISPLAY);
         });

         Operation.Thread.Start();
      }

      private void TestUsbButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_USB);
         });

         Operation.Thread.Start();
      }

      private void TestPrinterComButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_PRINTER_COM);
         });

         Operation.Thread.Start();
      }

      private void TestAdcButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_ADC);
         });

         Operation.Thread.Start();
      }

      private void TestInternalAdcButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_IADC);
         });

         Operation.Thread.Start();
      }

      private void ChargerConnectionTestButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_CHARGER_CONNECTION);
         });

         Operation.Thread.Start();
      }

      private void ChargerCurrentTextButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_CHARGER_CURRENT);
         });

         Operation.Thread.Start();
      }

      private void ChargerShutdownTestButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_CHARGER_SHUTDOWN);
         });

         Operation.Thread.Start();
      }

      private void TargetShuwdownTestButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.TEST_POWER_SHUTDOWN);
         });

         Operation.Thread.Start();
      }

      private void GetFirmwareButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.GET_FIRMWARE);
         });

         Operation.Thread.Start();
      }

      private void button1_Click(object sender, EventArgs e) {
         if(Veit.Bat1.Dll.CheckDevice()) {
            StatusWindow.Info("Ready");
         } else {
            StatusWindow.Info("Not ready");
         }
      }

      private void BatchButton_Click(object sender, EventArgs e) {
         Tester.Operations[] Tests = new Tester.Operations[] { Tester.Operations.TEST_KEYBOARD, Tester.Operations.TEST_CHARGER_CURRENT };

         Operation.Thread = new Thread(delegate(object unused) {
            Tester.DoBatch(Tests);
         });

         Operation.Thread.Start();
      }

      private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
         this.Close();
      }



      private void generalToolStripMenuItem_Click(object sender, EventArgs e) {
         GeneralSettings GeneralSettings = new GeneralSettings(this);

         GeneralSettings.ShowDialog();
      }

      private void testsToolStripMenuItem_Click(object sender, EventArgs e) {
         TestSettings TestSettings = new TestSettings();

         TestSettings.ShowDialog();
      }

      private void WriteConfigButton_Click(object sender, EventArgs e) {
         Operation.Thread = new Thread(delegate(object unused) {
            Tester.Do(Tester.Operations.WRITE_CONFIG);
         });

         Operation.Thread.Start();
      }

      private void configurationFilesToolStripMenuItem_Click(object sender, EventArgs e) {
         ConfigurationFiles ConfigurationFiles = new ConfigurationFiles();

         ConfigurationFiles.ShowDialog();
      }

   }
}
