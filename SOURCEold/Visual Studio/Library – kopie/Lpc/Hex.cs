﻿//******************************************************************************
//
//   Hex.cs     Hex file parsing
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.IO;
using System.Text;
using System.Linq;
using System.ComponentModel;

public class Hex : Worker
{
   public class HexParseException : System.Exception {
      public HexParseException() {
      }
   }

   protected string _FileName = "";
   protected bool _IsValid = false;

   public string FileName {
      get { return _FileName; }
   }

   public bool IsValid {
      get { return _IsValid;}
   }

   protected enum HexRecordType:byte {
      HEX_DATA = 0,            // 0 - data record
      HEX_EOF,             // 1 - end of file
      HEX_SEGMENT,         // 2 - extended segment address record
      HEX_START_SEGMENT,   // 3 - start segment address record
      HEX_ADDRESS,         // 4 - extended linear address record
      HEX_START_ADDRESS    // 5 - start linear address record
   };

   public uint    CodeMinAddress;           // min. address of code
   public uint    CodeStartAddress;         // restart address
   public uint    CodeSize;                 // code size
   public byte[] Code;                     // code data

   protected const uint HEX_MAX_LINE = 600;
   protected byte[] Line = new byte[ HEX_MAX_LINE + 1];       // line buffer
   protected uint  LineLength;                    // line length
   protected uint  LineIndex;                     // line index
   protected byte LineChecksum;                  // line checksum
   protected uint  FMaxCodeSize;                  // prealocated code size

   //******************************************************************************
   // Constructor
   //******************************************************************************

   public Hex()
   // Constructor
   {
      
      CodeStartAddress = 0;
      CodeSize         = 0;
   } // THexFile

   //******************************************************************************
   // Load
   //******************************************************************************

   private string[] FileLines;
   private int LinesPtr = 0;

   public bool Load( string FileName)
   // Load HEX file
   {
      _IsValid = false;
      _FileName = "?";
      CodeSize = 0;

      byte Reclen = 0;
      uint LoadOffset = 0;
      HexRecordType RecordType = HexRecordType.HEX_DATA;
      byte[] Data = new byte[256];
      // code data :
      uint Address = 0;
      uint MaxAddress = 0;
      uint MinAddress;
      uint EndAddress;

      int Lines = 0;

      StartTime = DateTime.Now;
      
      try {
         FileLines = File.ReadAllLines(FileName);
      } catch(Exception) {
         throw new Exception("Can't read the file");
      }


      FileInfo f = new FileInfo(FileName);
      Code = new byte[f.Length];

      MinAddress = (uint)f.Length;

      try {
         LinesPtr = 0;

         int TotalLines = FileLines.Length;

         Code = Enumerable.Repeat((byte)0xFF, Code.Length).ToArray(); 


            CodeStartAddress = 0;
            // row data :

            while( true){
               Lines++;

               Progress = (double)Lines / TotalLines;
               /*
               System.Diagnostics.Debug.WriteLine("Total lines " + TotalLines.ToString());
               System.Diagnostics.Debug.WriteLine("Line " + Lines.ToString());*/
               //System.Diagnostics.Debug.WriteLine("Progress " + Progress.ToString());

               ReadLine( ref Reclen, ref LoadOffset, ref RecordType, ref Data);

               switch( RecordType){
                  case HexRecordType.HEX_DATA :
                     if( Reclen == 0){// zero length record   
                        throw new HexParseException(); 
                     }

                     Array.Copy(Data, 0, Code, Address + LoadOffset, Reclen);

                     // minimum address :
                     if( Address + LoadOffset < MinAddress){
                        MinAddress = Address + LoadOffset;
                     }
                     // maximum address :
                     EndAddress = (uint) (Address + LoadOffset + (Reclen - 1));
                     if( EndAddress > MaxAddress){
                        MaxAddress = EndAddress;
                     }
                     break;

                  case HexRecordType.HEX_EOF :
                     goto terminate;

                  case HexRecordType.HEX_SEGMENT :
                     Address = GetInteger( Data, Reclen) << 4; 
                     break;

                  case HexRecordType.HEX_START_SEGMENT :
                     Address = GetInteger( Data, Reclen) << 8;
                     break;

                  default :
                     // unknown record
                     throw new HexParseException(); 
               }
         }
      } catch(Exception) {
         throw new Exception("Can't parse the file");
      }

      

   terminate:
      CodeMinAddress = MinAddress;
      CodeSize = MaxAddress + 1;

      _IsValid = true;
      _FileName = FileName;

      return true;
   } // LoadHex

   //------------------------------------------------------------------------------

   //******************************************************************************
   // Read line
   //******************************************************************************


   protected void ReadLine( ref byte Reclen, ref uint LoadOffset, ref HexRecordType RecordType, ref byte[] Data)
   // read HEX file row
   {
      string LineStr = FileLines[LinesPtr++];

         LineLength = (uint)LineStr.Length;

         if( LineLength == 0 || LineLength > HEX_MAX_LINE){
            throw new HexParseException();               // zero length
         }

         ASCIIEncoding enc = new ASCIIEncoding();
         Line = enc.GetBytes(LineStr);

         // dekodovani radku :
         LineIndex    = 0;
         LineChecksum = 0;
         
         if( Line[ LineIndex++] != ':'){
            throw new HexParseException();                // invalid line
         }
         
         Reclen = ReadByte();
         LoadOffset = ReadWord();
         RecordType = (HexRecordType)ReadByte();

         for( uint i = 0; i < Reclen; i++){
            Data[ i] = ReadByte();
         }

         byte Checksum = ReadByte();

         if( LineChecksum != 0){
            throw new HexParseException();
         }
   } // ReadLine

   //******************************************************************************
   // Read nibble
   //******************************************************************************

   protected byte ReadNibble()
   // hex character conversion, returns 0xFF on error
   {
      if( LineIndex >= LineLength){
         throw new HexParseException();
      }

      byte c = Line[ LineIndex++];

      if( c >= '0' && c <= '9'){
         return( (byte)(c - '0'));
      }
      if( c >= 'A' && c <= 'F'){
         return( (byte)(c - 'A' + 10));
      }

      throw new HexParseException();
   } // ReadNibble

   //******************************************************************************
   // Read byte
   //******************************************************************************

   protected byte ReadByte()
   // read byte from line
   {
      byte Value = 0;

      byte x = ReadNibble();
      Value |= (byte)(x << 4);
      
      x = ReadNibble();
      Value |= x;

      LineChecksum += Value;

      return( Value);
   } // ReadByte

   //******************************************************************************
   // Read word
   //******************************************************************************

   protected uint ReadWord()
   // read word from line
   {
      byte HiByte = ReadByte();
      byte LoByte = ReadByte();

      return( (uint)(HiByte << 8) | LoByte);
   } // ReadWord

   //******************************************************************************
   // Get integer
   //******************************************************************************

   protected uint GetInteger( byte[] Data, uint Size)
   // compose integer from <Data> array
   {
      uint Value = 0;
      for( uint i = 0; i < Size; i++){
         Value <<= 8;
         Value  |= Data[ i];
      }
      return( Value);
   } // GetInteger
}
