﻿//******************************************************************************
//
//   Usb.cs     Usb
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Ftxx;
using System.Threading;
using Device.Uart;

namespace Bat1.Tester {
   /// <summary>
   /// Usb</summary>

   public class Usb {
      private Config.Usb Config;

      private Ft232 FtdiDevice = new Ft232();

      private UsbUart Com = new UsbUart();

      /// <summary>
      /// IsOpen</summary>
      
      public bool IsOpen {
         get {
            return FtdiDevice.IsOpen;
         }
      }

      /// <summary>
      /// Default constructor</summary>
      /// <param name="Configuration">Module configuration</param>
      
      public Usb(Config.Usb Configuration) {
         Config = Configuration;
      }

      /// <summary>
      /// Uart object</summary>

      public UsbUart Uart {
         get {
            return Com;
         }
      }

      /// <summary>
      /// Open UART over USB</summary>

      public bool Open() {
         if(FtdiDevice.IsOpen) {
            return false; // ftdidevice open - user must close it
         }

         int Identifier = 0;

         if(!Com.Locate(Config.ComSettings.DeviceName, ref Identifier)) {
            return false;
         }

         if(!Com.Open(Identifier)) {
            return false;
         }

         if(!Com.SetParameters(Config.ComSettings.BaudRate, Config.ComSettings.Parity, Config.ComSettings.Bits, Config.ComSettings.StopBit, Config.ComSettings.Handshake)) {
            return false;
         }

         Com.SetRxWait(Config.RxTimeout);
         Com.Rts = false;
         Com.Dtr = false;

         return true;
      }

      /// <summary>
      /// Test UART over USB</summary>

      public bool TestUart() {
         Com.Flush();

         byte[] Data = new byte[1];

         // prepare data :
         for(int i = 0; i < Config.TestDataSize; i++) {
            Data[0] = (byte)i;

            // send data :
            if(Com.Write(Data, 1) != 1) {
               return (false);
            }

            // receive reply :
            if(Com.Read(Data, 1) != 1) {
               return (false);
            }

            // check
            if(Data[0] != (byte)i) {
               return (false);
            }
         }

         return true;
      }

      /// <summary>
      /// Close UART over USB</summary>

      public void Close() {
         Com.Close();
      }

      private int DeviceDetectedIndex = -1;

      /// <summary>
      /// Detect programmed or blank device</summary>
      /// <returns>
      /// True if device detected, false if timeout</returns>
   
      public bool DetectDevice() {
         return (Wait(true, true, Config.ConnectDelay));
      }

      /// <summary>
      /// Detect programmed device</summary>
      /// <returns>
      /// True if device detected, false if timeout</returns>

      public bool DetectProgrammedDevice() {
         return (Wait(false, true, Config.ConnectDelay));
      }

      /// <summary>
      /// Write FTDI EEPROM</summary>

      public bool WriteEeprom() {
         if(Com.IsOpen) {
            return false; // Uart is open, user must close it
         }

         if(DeviceDetectedIndex == -1) {
            return false;
         }

         if(Config.Ftdi.DeviceName.Length > 63) {
            return false;
         }
         if(Config.Ftdi.ManufacturerName.Length > 31) {
            return false;
         }

         try {
            try {
               if(FtdiDevice.OpenByIndex((uint)DeviceDetectedIndex) != Ftdi.FT_STATUS.FT_OK) {
                  throw new Exception();
               }

               if(!FtdiDevice.IsOpen) {
                  throw new Exception();
               }

               Ft232.EEPROM_STRUCTURE PgmData = new Ft232.EEPROM_STRUCTURE();

               if(FtdiDevice.ReadEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                  throw new Exception();
               }

               PgmData.Cbus0 = 3;
               PgmData.Cbus1 = 2;
               PgmData.Cbus2 = 0;
               PgmData.Cbus3 = 1;
               PgmData.Cbus4 = 5;
               PgmData.Description = Config.Ftdi.DeviceName;
               PgmData.EndpointSize = 64;
               PgmData.HighDriveIOs = false;
               PgmData.InvertCTS = false;
               PgmData.InvertDCD = false;
               PgmData.InvertDSR = false;
               PgmData.InvertDTR = false;
               PgmData.InvertRI = false;
               PgmData.InvertRTS = false;
               PgmData.InvertRXD = false;
               PgmData.InvertTXD = false;
               PgmData.Manufacturer = Config.Ftdi.ManufacturerName;
               PgmData.ManufacturerID = "00";
               PgmData.MaxPower = Config.Ftdi.MaxPower;
               PgmData.ProductID = 0x6001;
               PgmData.PullDownEnable = false;
               PgmData.RemoteWakeup = false;
               PgmData.RIsD2XX = true;
               PgmData.SelfPowered = false;
               PgmData.SerialNumber = "00000000";//"00000000";
               PgmData.SerNumEnable = false;
               PgmData.UseExtOsc = false;
               PgmData.VendorID = 0x0403;




               /*
               PgmData.Signature1     = 0x00000000;
               PgmData.Signature2     = 0xffffffff;
               PgmData.Version        = 2;                  // FT232R extensions
               PgmData.Manufacturer   = ManufacturerBuf;
               PgmData.ManufacturerId = ManufacturerIdBuf;
               PgmData.Description    = DescriptionBuf;
               PgmData.SerialNumber   = SerialNumberBuf;
               // read EEPROM :
               if_fterr( FT_EE_Read( FHandle, &PgmData)){
                  FT_Close( FHandle);
                  FHandle     = INVALID_HANDLE_VALUE;
                  Status      = "FTDI read error";
                  AdapterName = "?";
                  return( false);
               }
               // new values :
               PgmData.Signature1   = 0x00000000;
               PgmData.Signature2   = 0xffffffff;
               PgmData.Version      = 2;			// FT232R extensions
               PgmData.VendorId     = 0x0403;
               PgmData.ProductId    = 0x6001;
               strcpy( PgmData.Manufacturer,   Manufacturer);
               strcpy( PgmData.ManufacturerId, "00");
               strcpy( PgmData.Description,    Device);
               strcpy( PgmData.SerialNumber,   "00000001");
               PgmData.MaxPower     = 0;
               PgmData.PnP          = 0;
               PgmData.SelfPowered  = 1;
               PgmData.RemoteWakeup = 0;
               // revision 4
               PgmData.Rev4              = 0;
               PgmData.IsoIn             = 0;
               PgmData.IsoOut            = 0;
               PgmData.PullDownEnable    = 0;
               PgmData.SerNumEnable      = 0;
               PgmData.USBVersionEnable  = 0;
               PgmData.USBVersion        = 0x0110;
               // FT2232C extensions
               PgmData.Rev5              = 0;
               PgmData.IsoInA            = 0;
               PgmData.IsoInB            = 0;
               PgmData.IsoOutA           = 0;
               PgmData.IsoOutB           = 0;
               PgmData.PullDownEnable5   = 0;
               PgmData.SerNumEnable5     = 0;
               PgmData.USBVersionEnable5 = 0;
               PgmData.USBVersion5       = 0x0110;
               PgmData.AIsHighCurrent    = 0;
               PgmData.BIsHighCurrent    = 0;
               PgmData.IFAIsFifo         = 0;
               PgmData.IFAIsFifoTar      = 0;
               PgmData.IFAIsFastSer      = 0;
               PgmData.AIsVCP            = 0;
               PgmData.IFBIsFifo         = 0;
               PgmData.IFBIsFifoTar      = 0;
               PgmData.IFBIsFastSer      = 0;
               PgmData.BIsVCP            = 0;
               // FT232R extensions
               PgmData.UseExtOsc         = 0;
               PgmData.HighDriveIOs      = 0;
               PgmData.EndpointSize      = 64;
               PgmData.PullDownEnableR   = 0;
               PgmData.SerNumEnableR     = 1;
               PgmData.InvertTXD         = 0;
               PgmData.InvertRXD         = 0;
               PgmData.InvertRTS         = 0;
               PgmData.InvertCTS         = 0;
               PgmData.InvertDTR         = 0;
               PgmData.InvertDSR         = 0;
               PgmData.InvertDCD         = 0;
               PgmData.InvertRI          = 0;
               PgmData.Cbus0             = 3;
               PgmData.Cbus1             = 2;
               PgmData.Cbus2             = 0;
               PgmData.Cbus3             = 1;
               PgmData.Cbus4             = 5;
               PgmData.RIsD2XX           = 1;*/



               if(FtdiDevice.WriteEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                  throw new Exception();
               }

            } finally {
               FtdiDevice.Close();
            }
         } catch(Exception) {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Wait for device</summary>
      /// True - device connected, false - timeout expires</returns>

      private bool Wait(bool NewDevice, bool ProgrammedDevice, int Timeout) {
         for(int i = 0; i < Timeout; i++) {

            if(Detect(NewDevice, ProgrammedDevice)) {
               return (true);
            }

            Thread.Sleep(1000);
         }

         return (false);
      }

      /// <summary>
      /// Wait untill device disconnects</summary>
      /// <returns>
      /// True - no device connected, false - timeout expires</returns>

      public bool WaitForDisconnect()
         // Wait for disconnected device (leave closed)
      {
         for(int i = 0; i < Config.ConnectDelay; i++) {
            if(!Detect(true, true)) {
               return (true);
            }

            Thread.Sleep(1000);
         }

         return false;
      } // WaitForDisconnect

      /// <summary>
      /// Search for device</summary>

      private bool Detect(bool BlankDevice, bool ProgrammedDevice) {
         if(Com.IsOpen) {
            return false; // Uart is open, user must close it
         }

         FtdiDevice.Close();

         // Count of FTDI devices :
         uint NumDevices = 0;

         if(FtdiDevice.GetNumberOfDevices(ref NumDevices) != Ftdi.FT_STATUS.FT_OK) {
            return false;
         }

         if(NumDevices == 0) {
            return false;       // no devices
         }

         // Allocate storage for device info list
         Ftdi.FT_DEVICE_INFO_NODE[] ftdiDeviceList = new Ftdi.FT_DEVICE_INFO_NODE[NumDevices];

         // Populate our device list
         if(FtdiDevice.GetDeviceList(ftdiDeviceList) != Ftdi.FT_STATUS.FT_OK) {
            return false;
         }

         // Search by device name
         DeviceDetectedIndex = -1;
         int Index = -1;

         foreach(Ftdi.FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(BlankDevice) {
               foreach(Config.Usb.TFtdi.Names.DeviceName Name in Config.Ftdi.BlankNames) {
                  if(Device.Description.ToString() == Name.Name) {
                     DeviceDetectedIndex = Index;
                     goto SearchEnd;
                  }
               }
            }

            if(ProgrammedDevice) {
               foreach(Config.Usb.TFtdi.Names.DeviceName Name in Config.Ftdi.ProgrammedNames) {
                  if(Device.Description.ToString() == Name.Name) {
                     DeviceDetectedIndex = Index;
                     goto SearchEnd;
                  }
               }
            }
         }

      SearchEnd:
         if(DeviceDetectedIndex == -1) {
            return false;  // no device
         }

         return true;
      }
   }
} // namespace