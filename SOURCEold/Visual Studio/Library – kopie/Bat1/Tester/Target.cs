﻿//******************************************************************************
//
//   Target.cs     Target
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Uart;
using System.Globalization;

namespace Bat1.Tester {

   /// <summary>
   /// Target. Test firmware must be loaded in target in order to use this class.</summary>
  
   public class Target {
      private Config.Target Config;

      private ComUart Port = new ComUart();

      /// <summary>
      /// Target replies</summary>
      private const byte TEST_OK = (byte)'Y';
      private const byte TEST_ERROR = (byte)'N';

      private struct CmdStruct {
         public string Code;
         public int ReplyLength;
         public int ReplyTimeout;

         public CmdStruct(string code, int length, int timeout)
            : this() {
            Code = code;
            ReplyLength = length;
            ReplyTimeout = timeout;
         }
      }

      /// <summary>
      /// Commands definition</summary>

      private static class Commands {
         public static readonly CmdStruct TEST_ADC = new CmdStruct("A", 9, 3000);
         public static readonly CmdStruct TEST_CHARGER_ON = new CmdStruct("C", 1, 2000);
         public static readonly CmdStruct TEST_DISPLAY = new CmdStruct("D", 1, 3000);
         public static readonly CmdStruct TEST_OFF = new CmdStruct("F", 1, 1000);
         public static readonly CmdStruct TEST_IADC = new CmdStruct("I", 9, 3000);
         public static readonly CmdStruct TEST_MEMORY = new CmdStruct("M", 1, 24000);
         public static readonly CmdStruct TEST_CHARGER_OFF = new CmdStruct("O", 1, 2000);
         public static readonly CmdStruct TEST_POWER = new CmdStruct("P", 5, 500);
         public static readonly CmdStruct TEST_RTC = new CmdStruct("R", 1, 5000);
         public static readonly CmdStruct TEST_SOUND = new CmdStruct("S", 1, 2000);
         public static readonly CmdStruct TEST_USB = new CmdStruct("U", 1, 2000);
         public static readonly CmdStruct TEST_BACKLIGHT_ON = new CmdStruct("B", 1, 1000);
         public static readonly CmdStruct TEST_BACKLIGHT_OFF = new CmdStruct("G", 1, 1000);
         public static readonly CmdStruct TEST_KEYBOARD = new CmdStruct("K", 1 + Tester.Keyboard.KEYS_COUNT, 500);
      }

      private System.Text.ASCIIEncoding Enc = new System.Text.ASCIIEncoding();

      private byte[] Reply = new byte[10];

      /// <summary>
      /// IsOpen</summary>
      
      public bool IsOpen {
         get {
            return Port.IsOpen;
         }
      }

      /// <summary>
      /// Default constructor</summary>
      /// <param name="Configuration">Module configuration</param>

      public Target(Config.Target _Config) {
         Config = _Config;
      }

      /// <summary>
      /// Open</summary>

      public bool Open() {
         if(Port.IsOpen) {
            Port.Close();
         }

         int Identifier = 0;

         if(!Port.Locate(Config.ComSettings.PortName, ref Identifier)) {
            return (false);
         }

         if(!Port.Open(Identifier)) {
            return (false);
         }

         Port.SetParameters(Config.ComSettings.BaudRate, Config.ComSettings.Parity, Config.ComSettings.Bits, Config.ComSettings.StopBit, Config.ComSettings.Handshake);

         Port.SetRxNowait();
         Port.Flush();

         return (true);
      }

      /// <summary>
      /// Close</summary>

      public bool Close() {
         Port.Close();

         return true;

      }

      /// <summary>
      /// Communication test</summary>

      public bool TestUart() {
         byte[] Buffer = new byte[Config.TestDataSize];

         Port.SetRxWait(Config.RxTimeout);
         Port.Flush();

         byte[] DataRead = new byte[1];
         byte[] DataWrite = new byte[1];
         DataWrite[0] = (byte)'a';

         for(int i = 0; i < Config.TestDataSize; i++) {
            if(Port.Write(DataWrite, 1) != 1) {
               return (false);
            }

            if(Port.Read(DataRead, 1) != 1) {
               return (false);
            }

            if(DataRead[0] != DataWrite[0]) {
               return false;
            }

            if(DataWrite[0] < (byte)'z') {
               DataWrite[0]++;
            } else {
               DataWrite[0] = (byte)'a';
            }
         }

         return (true);
      }

      /// <summary>
      /// Presence test</summary>
      /// <remarks>
      /// Target is present if responds when character is transmited over UART.</remarks>
      
      public bool TestPresence(ref bool Present) {
         Port.SetRxWait(Config.RxTimeout / 10);
         Port.Flush();

         byte[] DataRead = new byte[1];
         byte[] DataWrite = new byte[1];

         DataWrite[0] = (byte)'a';

         if(Port.Write(DataWrite, 1) != 1) {
            return (false);
         }

         if(Port.Read(DataRead, 1) != 1) {
            Present = false;
            return (true);
         }

         if(DataRead[0] != DataWrite[0]) {
            return false;
         }

         Present = true;
         return true;
      }

      /// <summary>
      /// Display test</summary>

      public bool TestDisplay() {
         return (Test(Commands.TEST_DISPLAY));
      }

      /// <summary>
      /// Memory test</summary>

      public bool TestMemory() {
         return (Test(Commands.TEST_MEMORY));
      }

      /// <summary>
      /// RTC test</summary>
      
      public bool TestRtc() {
         return (Test(Commands.TEST_RTC));
      }

      /// <summary>
      /// Sound test</summary>

      public bool TestSound() {
         return (Test(Commands.TEST_SOUND));
      }

      /// <summary>
      /// Read ADC value</summary>

      public bool ReadAdc(ref int Value) {
         if(!Test(Commands.TEST_ADC)) {
            return (false);
         }

         string HexString = (new CString(Reply, Commands.TEST_ADC.ReplyLength - 1)).ToString();

         if(!Int32.TryParse(HexString, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out Value)) {
            return false;
         }

         return (true);
      }

      /// <summary>
      /// Read internal ADC value</summary>

      public bool ReadIadc(ref int Value) {
         if(!Test(Commands.TEST_IADC)) {
            return (false);
         }

         string HexString = (new CString(Reply, Commands.TEST_IADC.ReplyLength - 1)).ToString();

         if(!Int32.TryParse(HexString, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out Value)) {
            return false;
         }

         return (true);

      }

      /// <summary>
      /// Start USB test</summary>
      /// <remarks>
      /// Next, USB UART should be tested.</remarks>
      
      public bool StartUsb() {
         return (StartTest(Commands.TEST_USB));
      }

      /// <summary>
      /// Get USB test result</summary>

      public bool UsbResult() {
         return (TestResult(Commands.TEST_USB));
      }

      /// <summary>
      /// Enable/disable charger</summary>
      
      public bool EnableCharger(bool Enable) {
         if(Enable) {
            return (Test(Commands.TEST_CHARGER_ON));
         } else {
            return (Test(Commands.TEST_CHARGER_OFF));
         }
      }

      /// <summary>
      /// Test power</summary>

      public bool TestPower(ref bool PPR, ref bool CHG, ref bool USBON, ref bool ENNAB)
         // Read power status
       {
         if(!Test(Commands.TEST_POWER)) {
            return (false);
         }

         PPR = (Reply[0] == 'H');
         CHG = (Reply[1] == 'H');
         USBON = (Reply[2] == 'H');
         ENNAB = (Reply[3] == 'H');
         return (true);
      }

      /// <summary>
      /// Switch off</summary>

      public bool SwitchOff() {
         return (Test(Commands.TEST_OFF));
      }

      /// <summary>
      /// Switch on/off backlight</summary>

      public bool SetBacklight(bool On) {
         if(On) {
            return (Test(Commands.TEST_BACKLIGHT_ON));
         } else {
            return (Test(Commands.TEST_BACKLIGHT_OFF));
         }
      }

      /// <summary>
      /// Read keyboard state</summary>

      public bool ReadKeyboard(ref Tester.Keyboard Keyboard) {
         if(!Test(Commands.TEST_KEYBOARD)) {
            return false;
         }

         for(int i = 0; i < Tester.Keyboard.KEYS_COUNT; i++) {
            Keyboard[i] = Reply[i] == 'H' ? Tester.Keyboard.Key.KeyState.RELEASED : Tester.Keyboard.Key.KeyState.PRESSED;
         }

         return true;
      }

      /// <summary>
      /// Test</summary>

      private bool Test(CmdStruct Cmd) {
         if(!StartTest(Cmd)) {
            return false;
         }

         if(!TestResult(Cmd)) {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Start test</summary>

      private bool StartTest(CmdStruct Cmd) {
         if(!IsOpen) {
            return false;
         }

         Port.Flush();

         if(Port.Write(Enc.GetBytes(Cmd.Code), 1) == 0) {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Test result</summary>

      private bool TestResult(CmdStruct Cmd) {
         Port.SetRxWait(Cmd.ReplyTimeout);

         int size = 0;

         if((size = Port.Read(Reply, Cmd.ReplyLength)) != Cmd.ReplyLength) {
            return false;
         }

         if(Reply[Cmd.ReplyLength - 1] != TEST_OK) {
            return false;
         }

         return true;
      }
   }
}