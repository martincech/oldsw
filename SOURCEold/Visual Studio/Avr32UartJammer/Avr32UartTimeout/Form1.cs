﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Device.Uart;

namespace Avr32UartTimeout {
   public partial class Form1 : Form {
      NativeUart Port = new NativeUart();
         Encoding Enc = Encoding.ASCII;

      public Form1() {
         InitializeComponent();
      }

      private void button1_Click(object sender, EventArgs e) {

            Port.Open("COM2");

         if(!Port.IsOpen) {
            StatusLabel.Text = "Nelze otevrit";
         } else {
            StatusLabel.Text = "OK";
         }
      }

 
      private void timer1_Tick(object sender, EventArgs e) {
         if(!Port.IsOpen) {
            return;
         }

         byte[] Data = new byte[10000];
         int Length = Port.JammerRead(Data, 10000);

         for(int i = 0; i < Length; i++) {
            OutputTextBox.Text += String.Format("{0,2:X} ", Data[i]);
         }
         
      }

      private void SendCharButton_Click(object sender, EventArgs e) {

      }

      private void DisconnectButton_Click(object sender, EventArgs e) {
         Port.Close();
      }

      private void CleartButton_Click(object sender, EventArgs e) {
         OutputTextBox.Text = "";
      }



   }
}
