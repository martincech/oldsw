﻿using System;
using System.Windows.Forms;
using Device.Bat1;
using System.Threading;

namespace Bat1UsbDemo {
   public partial class Form1 : Form {
      private Crt InfoWindow;

      private Usb Usb = new Usb();

      public Form1() {
         InitializeComponent();
         InfoWindow = new Crt(OutputTextBox);
      }

      private void WaitForDisconnectButton_Click(object sender, EventArgs e) {
         InfoWindow.ShowStatus("Waiting for disconnect", Crt.StatusFlag.Info);

         if(!Usb.WaitForDisconnect()) {
            InfoWindow.ShowStatus("Connected", Crt.StatusFlag.Error);
            return;
         }
            
         InfoWindow.ShowStatus("Disconnected", Crt.StatusFlag.Info);
      }

      private void WaitForNewDeviceButton_Click(object sender, EventArgs e) {
         InfoWindow.ShowStatus("Waiting for connect", Crt.StatusFlag.Info);

         if(!Usb.WaitForNewDevice()) {
            InfoWindow.ShowStatus("Disconnected", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Connected", Crt.StatusFlag.Info);
      }

      private void WaitForProgrammedDeviceButton_Click(object sender, EventArgs e) {
         InfoWindow.ShowStatus("Waiting for connect", Crt.StatusFlag.Info);

         if(!Usb.WaitForProgrammedDevice()) {
            InfoWindow.ShowStatus("Disconnected", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Connected", Crt.StatusFlag.Info);
      }

      private void ProgramEepromButton_Click(object sender, EventArgs e) {
         InfoWindow.ShowStatus("Programming EEPROM", Crt.StatusFlag.Info);

         if(!Usb.WriteEeprom()) {
            InfoWindow.ShowStatus("Programming error", Crt.StatusFlag.Error);
            return;
         }

         InfoWindow.ShowStatus("Programmed", Crt.StatusFlag.Info);
      }
   }
}
