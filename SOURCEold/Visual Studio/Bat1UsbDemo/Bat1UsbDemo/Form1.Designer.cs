﻿namespace Bat1UsbDemo {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.OutputTextBox = new System.Windows.Forms.TextBox();
         this.WaitForDisconnectButton = new System.Windows.Forms.Button();
         this.WaitForNewDeviceButton = new System.Windows.Forms.Button();
         this.WaitForProgrammedDeviceButton = new System.Windows.Forms.Button();
         this.ProgramEepromButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // OutputTextBox
         // 
         this.OutputTextBox.Location = new System.Drawing.Point(12, 138);
         this.OutputTextBox.Multiline = true;
         this.OutputTextBox.Name = "OutputTextBox";
         this.OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.OutputTextBox.Size = new System.Drawing.Size(301, 189);
         this.OutputTextBox.TabIndex = 26;
         // 
         // WaitForDisconnectButton
         // 
         this.WaitForDisconnectButton.Location = new System.Drawing.Point(12, 12);
         this.WaitForDisconnectButton.Name = "WaitForDisconnectButton";
         this.WaitForDisconnectButton.Size = new System.Drawing.Size(157, 23);
         this.WaitForDisconnectButton.TabIndex = 25;
         this.WaitForDisconnectButton.Text = "Wait for device disconnect";
         this.WaitForDisconnectButton.UseVisualStyleBackColor = true;
         this.WaitForDisconnectButton.Click += new System.EventHandler(this.WaitForDisconnectButton_Click);
         // 
         // WaitForNewDeviceButton
         // 
         this.WaitForNewDeviceButton.Location = new System.Drawing.Point(12, 41);
         this.WaitForNewDeviceButton.Name = "WaitForNewDeviceButton";
         this.WaitForNewDeviceButton.Size = new System.Drawing.Size(235, 23);
         this.WaitForNewDeviceButton.TabIndex = 27;
         this.WaitForNewDeviceButton.Text = "Wait for either programmed or blank device";
         this.WaitForNewDeviceButton.UseVisualStyleBackColor = true;
         this.WaitForNewDeviceButton.Click += new System.EventHandler(this.WaitForNewDeviceButton_Click);
         // 
         // WaitForProgrammedDeviceButton
         // 
         this.WaitForProgrammedDeviceButton.Location = new System.Drawing.Point(12, 70);
         this.WaitForProgrammedDeviceButton.Name = "WaitForProgrammedDeviceButton";
         this.WaitForProgrammedDeviceButton.Size = new System.Drawing.Size(235, 23);
         this.WaitForProgrammedDeviceButton.TabIndex = 28;
         this.WaitForProgrammedDeviceButton.Text = "Wait for programmed device";
         this.WaitForProgrammedDeviceButton.UseVisualStyleBackColor = true;
         this.WaitForProgrammedDeviceButton.Click += new System.EventHandler(this.WaitForProgrammedDeviceButton_Click);
         // 
         // ProgramEepromButton
         // 
         this.ProgramEepromButton.Location = new System.Drawing.Point(12, 99);
         this.ProgramEepromButton.Name = "ProgramEepromButton";
         this.ProgramEepromButton.Size = new System.Drawing.Size(235, 23);
         this.ProgramEepromButton.TabIndex = 29;
         this.ProgramEepromButton.Text = "Programm EEPROM";
         this.ProgramEepromButton.UseVisualStyleBackColor = true;
         this.ProgramEepromButton.Click += new System.EventHandler(this.ProgramEepromButton_Click);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(405, 324);
         this.Controls.Add(this.ProgramEepromButton);
         this.Controls.Add(this.WaitForProgrammedDeviceButton);
         this.Controls.Add(this.WaitForNewDeviceButton);
         this.Controls.Add(this.OutputTextBox);
         this.Controls.Add(this.WaitForDisconnectButton);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox OutputTextBox;
      private System.Windows.Forms.Button WaitForDisconnectButton;
      private System.Windows.Forms.Button WaitForNewDeviceButton;
      private System.Windows.Forms.Button WaitForProgrammedDeviceButton;
      private System.Windows.Forms.Button ProgramEepromButton;
   }
}

