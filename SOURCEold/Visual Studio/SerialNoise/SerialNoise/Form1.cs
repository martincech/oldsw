﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SerialNoise {
    public partial class FormMain : Form {
        private int charactersCount;
        private byte character;
        private bool random;
        private int period;
        private int randomPeriod;

        private int [] speeds = {1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};
        
        public FormMain() {
            InitializeComponent();
            toolStripStatusLabel.Text    = "Idle";
            comboBoxPort.SelectedIndex   = 0;
            comboBoxSpeed.SelectedIndex  = 3;
            comboBoxParity.SelectedIndex = 2;
        }

        private void buttonStart_Click(object sender, EventArgs e) {
            if (timer.Enabled) {
                // Vypnu
                timer.Enabled = false;
                serialPort.Close();
                buttonStart.Text = "Start";
                toolStripStatusLabel.Text = "Idle";
            } else {
                // Zapnu
                period          = int.Parse(textBoxPeriod.Text);
                charactersCount = int.Parse(textBoxCharacters.Text);
                character       = byte.Parse(textBoxCharacter.Text, System.Globalization.NumberStyles.HexNumber);
                random = checkBoxRandom.Checked;
                if (checkBoxRandomPeriod.Checked) {
                    randomPeriod = int.Parse(textBoxRandomPeriod.Text);
                } else {
                    randomPeriod = 0;
                }
                timer.Interval = period;

                serialPort.PortName = comboBoxPort.Text;
                serialPort.BaudRate = speeds[comboBoxSpeed.SelectedIndex];
                switch (comboBoxParity.SelectedIndex) {
                    case 0: serialPort.Parity = System.IO.Ports.Parity.None; break;
                    case 1: serialPort.Parity = System.IO.Ports.Parity.Odd; break;
                    case 2: serialPort.Parity = System.IO.Ports.Parity.Even; break;
                }
                try {
                    serialPort.Open();
                } catch (Exception Ex) {
                    MessageBox.Show(Ex.Message);
                    return;
                }

                buttonStart.Text = "Stop";
                timer.Enabled = true;
                toolStripStatusLabel.Text = "Started " + DateTime.Now;
            }
        }

        private void timer_Tick(object sender, EventArgs e) {
            Random randomNumber = new Random();
            byte[] buffer = new byte[charactersCount];
            
            for (int i = 0; i < charactersCount; i++) {
                if (random) {
                    buffer[i] = (byte)randomNumber.Next(255);
                } else {
                    buffer[i] = character;
                }
            }
            serialPort.Write(buffer, 0, charactersCount);

            // Dalsi perioda
            if (randomPeriod > 0) {
                timer.Interval = period + randomNumber.Next(randomPeriod);
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            timer.Enabled = false;
            if (serialPort.IsOpen) {
                serialPort.Close();
            }
        }

        private void checkBoxRandom_CheckedChanged(object sender, EventArgs e) {
            textBoxCharacter.Enabled = !checkBoxRandom.Checked;
        }
    }
}
