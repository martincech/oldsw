﻿namespace Serial
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           this.components = new System.ComponentModel.Container();
           this.FastTimer = new System.Windows.Forms.Timer(this.components);
           this.LoadHexButton = new System.Windows.Forms.Button();
           this.OperationProgressBar = new System.Windows.Forms.ProgressBar();
           this.Cancel1Button = new System.Windows.Forms.Button();
           this.WriteButton = new System.Windows.Forms.Button();
           this.OperationNameLabel = new System.Windows.Forms.Label();
           this.CpuTLabel = new System.Windows.Forms.Label();
           this.VersionTLabel = new System.Windows.Forms.Label();
           this.CpuLabel = new System.Windows.Forms.Label();
           this.VersionLabel = new System.Windows.Forms.Label();
           this.FileNameTLabel = new System.Windows.Forms.Label();
           this.CodeSizeTLabel = new System.Windows.Forms.Label();
           this.FileNameLabel = new System.Windows.Forms.Label();
           this.CodeSizeLabel = new System.Windows.Forms.Label();
           this.RemainingTimeLabel = new System.Windows.Forms.Label();
           this.SlowTimer = new System.Windows.Forms.Timer(this.components);
           this.RemainingTimeTLabel = new System.Windows.Forms.Label();
           this.StatusLabel = new System.Windows.Forms.Label();
           this.SuspendLayout();
           // 
           // FastTimer
           // 
           this.FastTimer.Enabled = true;
           this.FastTimer.Interval = 500;
           this.FastTimer.Tick += new System.EventHandler(this.FastTimer_Tick);
           // 
           // LoadHexButton
           // 
           this.LoadHexButton.Location = new System.Drawing.Point(12, 12);
           this.LoadHexButton.Name = "LoadHexButton";
           this.LoadHexButton.Size = new System.Drawing.Size(75, 23);
           this.LoadHexButton.TabIndex = 3;
           this.LoadHexButton.Text = "Load HEX";
           this.LoadHexButton.UseVisualStyleBackColor = true;
           this.LoadHexButton.Click += new System.EventHandler(this.LoadHexButton_Click);
           // 
           // OperationProgressBar
           // 
           this.OperationProgressBar.Location = new System.Drawing.Point(226, 43);
           this.OperationProgressBar.Name = "OperationProgressBar";
           this.OperationProgressBar.Size = new System.Drawing.Size(100, 23);
           this.OperationProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
           this.OperationProgressBar.TabIndex = 4;
           // 
           // Cancel1Button
           // 
           this.Cancel1Button.Cursor = System.Windows.Forms.Cursors.Default;
           this.Cancel1Button.Location = new System.Drawing.Point(226, 94);
           this.Cancel1Button.Name = "Cancel1Button";
           this.Cancel1Button.Size = new System.Drawing.Size(75, 23);
           this.Cancel1Button.TabIndex = 5;
           this.Cancel1Button.Text = "Cancel";
           this.Cancel1Button.UseVisualStyleBackColor = true;
           this.Cancel1Button.Click += new System.EventHandler(this.Cancel1Button_Click);
           // 
           // WriteButton
           // 
           this.WriteButton.Location = new System.Drawing.Point(12, 94);
           this.WriteButton.Name = "WriteButton";
           this.WriteButton.Size = new System.Drawing.Size(75, 23);
           this.WriteButton.TabIndex = 7;
           this.WriteButton.Text = "Write";
           this.WriteButton.UseVisualStyleBackColor = true;
           this.WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
           // 
           // OperationNameLabel
           // 
           this.OperationNameLabel.AutoSize = true;
           this.OperationNameLabel.Location = new System.Drawing.Point(223, 27);
           this.OperationNameLabel.Name = "OperationNameLabel";
           this.OperationNameLabel.Size = new System.Drawing.Size(13, 13);
           this.OperationNameLabel.TabIndex = 8;
           this.OperationNameLabel.Text = "?";
           // 
           // CpuTLabel
           // 
           this.CpuTLabel.AutoSize = true;
           this.CpuTLabel.Location = new System.Drawing.Point(12, 123);
           this.CpuTLabel.Name = "CpuTLabel";
           this.CpuTLabel.Size = new System.Drawing.Size(32, 13);
           this.CpuTLabel.TabIndex = 9;
           this.CpuTLabel.Text = "CPU:";
           // 
           // VersionTLabel
           // 
           this.VersionTLabel.AutoSize = true;
           this.VersionTLabel.Location = new System.Drawing.Point(12, 141);
           this.VersionTLabel.Name = "VersionTLabel";
           this.VersionTLabel.Size = new System.Drawing.Size(48, 13);
           this.VersionTLabel.TabIndex = 10;
           this.VersionTLabel.Text = "Version: ";
           // 
           // CpuLabel
           // 
           this.CpuLabel.AutoSize = true;
           this.CpuLabel.Location = new System.Drawing.Point(57, 123);
           this.CpuLabel.Name = "CpuLabel";
           this.CpuLabel.Size = new System.Drawing.Size(13, 13);
           this.CpuLabel.TabIndex = 11;
           this.CpuLabel.Text = "?";
           // 
           // VersionLabel
           // 
           this.VersionLabel.AutoSize = true;
           this.VersionLabel.Location = new System.Drawing.Point(57, 141);
           this.VersionLabel.Name = "VersionLabel";
           this.VersionLabel.Size = new System.Drawing.Size(13, 13);
           this.VersionLabel.TabIndex = 12;
           this.VersionLabel.Text = "?";
           // 
           // FileNameTLabel
           // 
           this.FileNameTLabel.AutoSize = true;
           this.FileNameTLabel.Location = new System.Drawing.Point(11, 41);
           this.FileNameTLabel.Name = "FileNameTLabel";
           this.FileNameTLabel.Size = new System.Drawing.Size(52, 13);
           this.FileNameTLabel.TabIndex = 13;
           this.FileNameTLabel.Text = "Filename:";
           // 
           // CodeSizeTLabel
           // 
           this.CodeSizeTLabel.AutoSize = true;
           this.CodeSizeTLabel.Location = new System.Drawing.Point(12, 58);
           this.CodeSizeTLabel.Name = "CodeSizeTLabel";
           this.CodeSizeTLabel.Size = new System.Drawing.Size(56, 13);
           this.CodeSizeTLabel.TabIndex = 14;
           this.CodeSizeTLabel.Text = "Code size:";
           // 
           // FileNameLabel
           // 
           this.FileNameLabel.AutoSize = true;
           this.FileNameLabel.Location = new System.Drawing.Point(74, 41);
           this.FileNameLabel.Name = "FileNameLabel";
           this.FileNameLabel.Size = new System.Drawing.Size(13, 13);
           this.FileNameLabel.TabIndex = 15;
           this.FileNameLabel.Text = "?";
           // 
           // CodeSizeLabel
           // 
           this.CodeSizeLabel.AutoSize = true;
           this.CodeSizeLabel.Location = new System.Drawing.Point(74, 58);
           this.CodeSizeLabel.Name = "CodeSizeLabel";
           this.CodeSizeLabel.Size = new System.Drawing.Size(13, 13);
           this.CodeSizeLabel.TabIndex = 16;
           this.CodeSizeLabel.Text = "?";
           // 
           // RemainingTimeLabel
           // 
           this.RemainingTimeLabel.AutoSize = true;
           this.RemainingTimeLabel.Location = new System.Drawing.Point(281, 74);
           this.RemainingTimeLabel.Name = "RemainingTimeLabel";
           this.RemainingTimeLabel.Size = new System.Drawing.Size(13, 13);
           this.RemainingTimeLabel.TabIndex = 17;
           this.RemainingTimeLabel.Text = "?";
           // 
           // SlowTimer
           // 
           this.SlowTimer.Enabled = true;
           this.SlowTimer.Interval = 2000;
           this.SlowTimer.Tick += new System.EventHandler(this.SlowTimer_Tick);
           // 
           // RemainingTimeTLabel
           // 
           this.RemainingTimeTLabel.AutoSize = true;
           this.RemainingTimeTLabel.Location = new System.Drawing.Point(223, 74);
           this.RemainingTimeTLabel.Name = "RemainingTimeTLabel";
           this.RemainingTimeTLabel.Size = new System.Drawing.Size(60, 13);
           this.RemainingTimeTLabel.TabIndex = 18;
           this.RemainingTimeTLabel.Text = "Remaining:";
           // 
           // StatusLabel
           // 
           this.StatusLabel.AutoSize = true;
           this.StatusLabel.Location = new System.Drawing.Point(134, 155);
           this.StatusLabel.Name = "StatusLabel";
           this.StatusLabel.Size = new System.Drawing.Size(49, 13);
           this.StatusLabel.TabIndex = 19;
           this.StatusLabel.Text = "Status: ?";
           // 
           // Form1
           // 
           this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
           this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           this.ClientSize = new System.Drawing.Size(349, 182);
           this.Controls.Add(this.StatusLabel);
           this.Controls.Add(this.RemainingTimeTLabel);
           this.Controls.Add(this.RemainingTimeLabel);
           this.Controls.Add(this.CodeSizeLabel);
           this.Controls.Add(this.FileNameLabel);
           this.Controls.Add(this.CodeSizeTLabel);
           this.Controls.Add(this.FileNameTLabel);
           this.Controls.Add(this.VersionLabel);
           this.Controls.Add(this.CpuLabel);
           this.Controls.Add(this.VersionTLabel);
           this.Controls.Add(this.CpuTLabel);
           this.Controls.Add(this.OperationNameLabel);
           this.Controls.Add(this.WriteButton);
           this.Controls.Add(this.Cancel1Button);
           this.Controls.Add(this.OperationProgressBar);
           this.Controls.Add(this.LoadHexButton);
           this.Name = "Form1";
           this.Text = "Form1";
           this.ResumeLayout(false);
           this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer FastTimer;
        private System.Windows.Forms.Button LoadHexButton;
        private System.Windows.Forms.ProgressBar OperationProgressBar;
        private System.Windows.Forms.Button Cancel1Button;
        private System.Windows.Forms.Button WriteButton;
        private System.Windows.Forms.Label CpuTLabel;
        private System.Windows.Forms.Label VersionTLabel;
        private System.Windows.Forms.Label CpuLabel;
        private System.Windows.Forms.Label VersionLabel;
        private System.Windows.Forms.Label FileNameTLabel;
        private System.Windows.Forms.Label CodeSizeTLabel;
        private System.Windows.Forms.Label FileNameLabel;
        private System.Windows.Forms.Label CodeSizeLabel;
        public System.Windows.Forms.Label OperationNameLabel;
        private System.Windows.Forms.Label RemainingTimeLabel;
        private System.Windows.Forms.Timer SlowTimer;
        private System.Windows.Forms.Label RemainingTimeTLabel;
        private System.Windows.Forms.Label StatusLabel;
    }
}

