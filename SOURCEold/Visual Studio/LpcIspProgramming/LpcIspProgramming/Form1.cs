﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.IO;

using Device.Uart;

namespace Serial {
   public partial class Form1 : Form {
      protected Hex HexFile;
      protected LpcFlash Lpc;

      protected BackgroundOperation Operation;

      private delegate void EventArgsDelegate();
      private delegate void SetStateDelegate(string State);

      //******************************************************************************
      // Form1
      //******************************************************************************

      public Form1() {
         InitializeComponent();

         Operation = new BackgroundOperation();
         Operation.InProgress = false;

         HexFile = new Hex(512 * 1024);

         Lpc = new LpcFlash();
         Lpc.BaudRate = 57600;
         Lpc.CrystalFrequency = 18432000;
         Lpc.DeviceName = "VEIT BAT1 Poultry Scale";
         Lpc.ViaUsb = true;

         Cancel1Button.Enabled = false;
         LoadHexButton.Enabled = true;
         WriteButton.Enabled = false;
      }

      //******************************************************************************
      // FastTimer_Tick
      //******************************************************************************

      private void FastTimer_Tick(object sender, EventArgs e) {
         if(Operation.InProgress) {
            //OperationNameLabel.Text = Operation.OperationName;
            OperationProgressBar.Value = Math.Min((int)(Operation.Worker.Progress * 100), 100);

            if(!Operation.Thread.IsAlive) {
               Operation.InProgress = false;
            }
         } else {
            OperationProgressBar.Value = 0;
         }
      }

      //******************************************************************************
      // SlowTimer_Tick
      //******************************************************************************

      private void SlowTimer_Tick(object sender, EventArgs e) {
         if(Operation.InProgress) {
            if (Operation.Worker.Progress != 0) {
               int SecsRemaining = (int)((DateTime.Now - Operation.Worker.StartTime).TotalSeconds * (1 / (Operation.Worker.Progress) - 1));

               RemainingTimeLabel.Text = (SecsRemaining / 60).ToString() + " min " + (SecsRemaining % 60).ToString() + " sec";
            }
         } else {
            RemainingTimeLabel.Text = "";
         }
      }

      //******************************************************************************
      // SetOperationName
      //******************************************************************************

      void SetOperationName(string Name) {
         // Make sure this callback is on the correct thread 
         if(this.InvokeRequired) {
            this.Invoke(new SetStateDelegate(SetOperationName), new object[] { Name });
            return;
         }

         OperationNameLabel.Text = Name;
      }

      //******************************************************************************
      // SetStatus
      //******************************************************************************

      private void SetStatus(string Status) {
         // Make sure this callback is on the correct thread 
         if(this.InvokeRequired) {
            this.Invoke(new SetStateDelegate(SetStatus), new object[] { Status });
            return;
         }

         StatusLabel.Text = "Status: " + Status;
         OperationNameLabel.Text = "";
      }

      //******************************************************************************
      // SetCpuInfo
      //******************************************************************************

      void SetCpuInfo() {
         // Make sure this callback is on the correct thread 
         if (this.InvokeRequired) {
            this.Invoke(new EventArgsDelegate(SetCpuInfo));
            return;
         }

         CpuLabel.Text = Lpc.Device;
         VersionLabel.Text = Lpc.Version;
      }

      //******************************************************************************
      // WriteEnd
      //******************************************************************************

      void WriteEnd() {
         // Make sure this callback is on the correct thread 
         if (this.InvokeRequired) {
            this.Invoke(new EventArgsDelegate(WriteEnd));
            return;
         }

         Cancel1Button.Enabled = false;
         LoadHexButton.Enabled = true;
         WriteButton.Enabled = true;
      }

      //******************************************************************************
      // LoadHexEnd
      //******************************************************************************

      void LoadHexEnd() {
         // Make sure this callback is on the correct thread 
         if (this.InvokeRequired) {
            this.Invoke(new EventArgsDelegate(LoadHexEnd));
            return;
         }

         FileNameLabel.Text = GetEllipsesPath(HexFile.FileName);
         CodeSizeLabel.Text = HexFile.CodeSize.ToString();
         WriteButton.Enabled = HexFile.IsValid;
         Cancel1Button.Enabled = false;
      }

      //******************************************************************************
      // LoadHexButton_Click
      //******************************************************************************

      private void LoadHexButton_Click(object sender, EventArgs e) {
         OpenFileDialog FileDialog = new OpenFileDialog();

         FileDialog.Filter = "HEX files (*.hex)|*.hex|All files (*.*)|*.*";

         if (FileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
            Operation.Thread = new Thread(delegate(object unused) {
               LoadHex(FileDialog.FileName);
            });

            Operation.Worker = HexFile;
            Operation.InProgress = true;
            SetOperationName("Parsing");

            Cancel1Button.Enabled = true;


            Operation.Thread.Start();
         }
      }

      //******************************************************************************
      // WriteButton_Click
      //******************************************************************************

      private void WriteButton_Click(object sender, EventArgs e) {
         if (!HexFile.IsValid) {
            return;
         }

         Operation.Thread = new Thread(delegate(object unused) {
            WriteFlash();
         });

         Operation.Worker = Lpc;
         Operation.InProgress = true;

         WriteButton.Enabled = false;
         Cancel1Button.Enabled = true;
         LoadHexButton.Enabled = false;

         Operation.Thread.Start();

      }

      //******************************************************************************
      // CancelButton_Click
      //******************************************************************************

      private void Cancel1Button_Click(object sender, EventArgs e) {
         if (Operation.InProgress) {
            Operation.Thread.Abort();
            Operation.InProgress = false;
         }

         WriteButton.Enabled = HexFile.IsValid;
         LoadHexButton.Enabled = true;
      }

      //******************************************************************************
      // LoadHex
      //******************************************************************************

      private void LoadHex(string Filename) {
         try {
            HexFile.Load(Filename);
            SetStatus("OK");
         } catch(Hex.HexParseException) {
            SetStatus("Invalid hex file");
         } finally {
            LoadHexEnd();
         }
      }

      //******************************************************************************
      // WriteFlash
      //******************************************************************************

      private void WriteFlash() {
         try {
            SetOperationName("Contacting CPU");
         
            if (!Lpc.GetDeviceInfo()) {
               throw new SystemException();
            }

            SetCpuInfo();

            SetOperationName("Writing");

            if (!Lpc.WriteFlash(0, HexFile.Code, (int)HexFile.CodeSize)) {
               throw new SystemException();
            }

            SetOperationName("Verifying");

            if (!Lpc.CompareFlash(0, HexFile.Code, (int)HexFile.CodeSize)) {
               throw new SystemException();
            }

            if (!Lpc.Reset()) {
               throw new SystemException();
            }

            SetStatus("OK");
         } catch(SystemException) {
            SetStatus("Can't write");
         } finally {
            Lpc.Disconnect();
            WriteEnd();
         }
      }

      //******************************************************************************
      // GetEllipsesPath - shorten path
      //******************************************************************************

      public string GetEllipsesPath(string fullPath) {
         string ellipsesPath = "";

         if (!string.IsNullOrEmpty(fullPath)) {
            string[] dirName = fullPath.Split('\\');
            string fileName = Path.GetFileName(fullPath);

            //Shorten the file name················
            if (fileName.Length > 25) {
               fileName = fileName.Substring(0, 15) + "..."
               + fileName.Substring(fileName.Length - 5, 5);
            }

            //Shortten the complete path
            ellipsesPath = Path.GetPathRoot(fullPath) + (dirName.Length > 2 ? dirName[1] + @"\..\" : "") + fileName;
         }

         return ellipsesPath;
      }

   }
}