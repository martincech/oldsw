﻿using System;
using System.Windows.Forms;
using FTD2XX_NET;

namespace Bat1Tester {
   public partial class DetectBat1 : Form {
      public DetectBat1() {
         InitializeComponent();

         Detect();
      }

      private void OkButton_Click(object sender, EventArgs e) {
         this.DialogResult = DialogResult.OK;
      }

      private void Detect() {
         OutputTextBox.Text = "";
         
         FTDI FtdiDevice = new FTDI();

         FTDI.FT_STATUS Status;

         // Count of FTDI devices :
         uint NumDevices = 0;
         try {
            Status = FtdiDevice.GetNumberOfDevices(ref NumDevices);

            if(Status != FTDI.FT_STATUS.FT_OK) {
               throw new Exception("Fatal error");
            }

            if(NumDevices == 0) {
               throw new Exception("No device");
            }

            // Allocate storage for device info list
            FTDI.FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FTDI.FT_DEVICE_INFO_NODE[NumDevices + 5];

            Status = FtdiDevice.GetDeviceList(ftdiDeviceList);

            // Populate our device list
            if(Status != FTDI.FT_STATUS.FT_OK) {
               throw new Exception("Fatal error");
            }

            foreach(FTDI.FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
               if(Device == null) {
                  continue;
               }

               OutputTextBox.Text += Device.LocId.ToString() + ", " + Device.Description + "\r\n";
            }
         } catch(Exception e) {
            OutputTextBox.Text = e.Message;
         }
      }

      private void RefreshButton_Click(object sender, EventArgs e) {
         Detect();
      }
   }
}
