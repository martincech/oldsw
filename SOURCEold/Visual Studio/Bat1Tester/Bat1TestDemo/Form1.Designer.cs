﻿namespace Bat1Tester {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
         this.ProductionFirmwareButton = new System.Windows.Forms.Button();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.TestFirmwareButton = new System.Windows.Forms.Button();
         this.TestKeyboardButton = new System.Windows.Forms.Button();
         this.TestPowerIdleButton = new System.Windows.Forms.Button();
         this.SwitchTargetOnButton = new System.Windows.Forms.Button();
         this.SwitchOffButton = new System.Windows.Forms.Button();
         this.TestPowerInvertedButton = new System.Windows.Forms.Button();
         this.TestPowerBacklightOnButton = new System.Windows.Forms.Button();
         this.TestPowerBacklightOffButton = new System.Windows.Forms.Button();
         this.FTDIProgrammingButton = new System.Windows.Forms.Button();
         this.TestMemoryButton = new System.Windows.Forms.Button();
         this.TestSoundButton = new System.Windows.Forms.Button();
         this.TestRtcButton = new System.Windows.Forms.Button();
         this.TestPrinterComButton = new System.Windows.Forms.Button();
         this.TestAdcButton = new System.Windows.Forms.Button();
         this.TestInternalAdcButton = new System.Windows.Forms.Button();
         this.TestDisplayButton = new System.Windows.Forms.Button();
         this.TestUsbButton = new System.Windows.Forms.Button();
         this.ChargerConnectionTestButton = new System.Windows.Forms.Button();
         this.ChargerCurrentTextButton = new System.Windows.Forms.Button();
         this.ChargerShutdownTestButton = new System.Windows.Forms.Button();
         this.TargetShuwdownTestButton = new System.Windows.Forms.Button();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.BatchButton = new System.Windows.Forms.Button();
         this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
         this.menuStrip1 = new System.Windows.Forms.MenuStrip();
         this.utilityToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
         this.debugToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
         this.detectBat1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
         this.testToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
         this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.utilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.detectBAT1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
         this.WriteConfigButton = new System.Windows.Forms.Button();
         this.TestBatchesComboBox = new System.Windows.Forms.ComboBox();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.ModelClearReportButton = new System.Windows.Forms.Button();
         this.ModelSaveReportButton = new System.Windows.Forms.Button();
         this.TesterGroupBox = new System.Windows.Forms.GroupBox();
         this.ModelTesterStatusImage = new System.Windows.Forms.PictureBox();
         this.ModelCompleteButton0 = new System.Windows.Forms.Button();
         this.ModelTesterOperationLabel = new System.Windows.Forms.Label();
         this.ModelTesterRadioButton1 = new System.Windows.Forms.RadioButton();
         this.ModelTesterRadioButton0 = new System.Windows.Forms.RadioButton();
         this.TesterLabel = new System.Windows.Forms.Label();
         this.ModelStatusTextBox = new System.Windows.Forms.TextBox();
         this.OperationProgressBar = new System.Windows.Forms.ProgressBar();
         this.label1 = new System.Windows.Forms.Label();
         this.ConfigNameLabel = new System.Windows.Forms.Label();
         this.groupBox3 = new System.Windows.Forms.GroupBox();
         this.LogoPictureBox = new System.Windows.Forms.PictureBox();
         this.CyclicCheckBox = new System.Windows.Forms.CheckBox();
         this.groupBox1.SuspendLayout();
         this.menuStrip1.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.TesterGroupBox.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.ModelTesterStatusImage)).BeginInit();
         this.groupBox3.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).BeginInit();
         this.SuspendLayout();
         // 
         // ProductionFirmwareButton
         // 
         this.ProductionFirmwareButton.Location = new System.Drawing.Point(485, 41);
         this.ProductionFirmwareButton.Name = "ProductionFirmwareButton";
         this.ProductionFirmwareButton.Size = new System.Drawing.Size(115, 23);
         this.ProductionFirmwareButton.TabIndex = 0;
         this.ProductionFirmwareButton.Text = "Production firmware";
         this.ProductionFirmwareButton.UseVisualStyleBackColor = true;
         this.ProductionFirmwareButton.Click += new System.EventHandler(this.ProductionFirmwareButton_Click);
         // 
         // timer1
         // 
         this.timer1.Interval = 250;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // TestFirmwareButton
         // 
         this.TestFirmwareButton.Location = new System.Drawing.Point(364, 41);
         this.TestFirmwareButton.Name = "TestFirmwareButton";
         this.TestFirmwareButton.Size = new System.Drawing.Size(115, 23);
         this.TestFirmwareButton.TabIndex = 3;
         this.TestFirmwareButton.Text = "Test firmware";
         this.TestFirmwareButton.UseVisualStyleBackColor = true;
         this.TestFirmwareButton.Click += new System.EventHandler(this.TestFirmwareButton_Click);
         // 
         // TestKeyboardButton
         // 
         this.TestKeyboardButton.Location = new System.Drawing.Point(105, 20);
         this.TestKeyboardButton.Name = "TestKeyboardButton";
         this.TestKeyboardButton.Size = new System.Drawing.Size(93, 23);
         this.TestKeyboardButton.TabIndex = 4;
         this.TestKeyboardButton.Text = "Test keyboard";
         this.TestKeyboardButton.UseVisualStyleBackColor = true;
         this.TestKeyboardButton.Click += new System.EventHandler(this.TestKeyboardButton_Click);
         // 
         // TestPowerIdleButton
         // 
         this.TestPowerIdleButton.Location = new System.Drawing.Point(128, 49);
         this.TestPowerIdleButton.Name = "TestPowerIdleButton";
         this.TestPowerIdleButton.Size = new System.Drawing.Size(116, 23);
         this.TestPowerIdleButton.TabIndex = 5;
         this.TestPowerIdleButton.Text = "Current idle";
         this.TestPowerIdleButton.UseVisualStyleBackColor = true;
         this.TestPowerIdleButton.Click += new System.EventHandler(this.TestPowerIdleButton_Click);
         // 
         // SwitchTargetOnButton
         // 
         this.SwitchTargetOnButton.Location = new System.Drawing.Point(364, 114);
         this.SwitchTargetOnButton.Name = "SwitchTargetOnButton";
         this.SwitchTargetOnButton.Size = new System.Drawing.Size(116, 23);
         this.SwitchTargetOnButton.TabIndex = 6;
         this.SwitchTargetOnButton.Text = "Switch ON";
         this.SwitchTargetOnButton.UseVisualStyleBackColor = true;
         this.SwitchTargetOnButton.Click += new System.EventHandler(this.SwitchTargetOnButton_Click);
         // 
         // SwitchOffButton
         // 
         this.SwitchOffButton.Location = new System.Drawing.Point(484, 114);
         this.SwitchOffButton.Name = "SwitchOffButton";
         this.SwitchOffButton.Size = new System.Drawing.Size(116, 23);
         this.SwitchOffButton.TabIndex = 7;
         this.SwitchOffButton.Text = "Switch target OFF";
         this.SwitchOffButton.UseVisualStyleBackColor = true;
         this.SwitchOffButton.Click += new System.EventHandler(this.SwitchOffButton_Click);
         // 
         // TestPowerInvertedButton
         // 
         this.TestPowerInvertedButton.Location = new System.Drawing.Point(6, 106);
         this.TestPowerInvertedButton.Name = "TestPowerInvertedButton";
         this.TestPowerInvertedButton.Size = new System.Drawing.Size(115, 23);
         this.TestPowerInvertedButton.TabIndex = 8;
         this.TestPowerInvertedButton.Text = "Current inverted";
         this.TestPowerInvertedButton.UseVisualStyleBackColor = true;
         this.TestPowerInvertedButton.Click += new System.EventHandler(this.TestPowerInvertedButton_Click);
         // 
         // TestPowerBacklightOnButton
         // 
         this.TestPowerBacklightOnButton.Location = new System.Drawing.Point(6, 77);
         this.TestPowerBacklightOnButton.Name = "TestPowerBacklightOnButton";
         this.TestPowerBacklightOnButton.Size = new System.Drawing.Size(116, 23);
         this.TestPowerBacklightOnButton.TabIndex = 9;
         this.TestPowerBacklightOnButton.Text = "Current BL on";
         this.TestPowerBacklightOnButton.UseVisualStyleBackColor = true;
         this.TestPowerBacklightOnButton.Click += new System.EventHandler(this.TestPowerBacklightOnButton_Click);
         // 
         // TestPowerBacklightOffButton
         // 
         this.TestPowerBacklightOffButton.Location = new System.Drawing.Point(128, 77);
         this.TestPowerBacklightOffButton.Name = "TestPowerBacklightOffButton";
         this.TestPowerBacklightOffButton.Size = new System.Drawing.Size(116, 23);
         this.TestPowerBacklightOffButton.TabIndex = 10;
         this.TestPowerBacklightOffButton.Text = "Current BL off";
         this.TestPowerBacklightOffButton.UseVisualStyleBackColor = true;
         this.TestPowerBacklightOffButton.Click += new System.EventHandler(this.TestPowerBacklightOffButton_Click);
         // 
         // FTDIProgrammingButton
         // 
         this.FTDIProgrammingButton.Location = new System.Drawing.Point(364, 70);
         this.FTDIProgrammingButton.Name = "FTDIProgrammingButton";
         this.FTDIProgrammingButton.Size = new System.Drawing.Size(115, 23);
         this.FTDIProgrammingButton.TabIndex = 11;
         this.FTDIProgrammingButton.Text = "FTDI programming";
         this.FTDIProgrammingButton.UseVisualStyleBackColor = true;
         this.FTDIProgrammingButton.Click += new System.EventHandler(this.FTDIProgrammingButton_Click);
         // 
         // TestMemoryButton
         // 
         this.TestMemoryButton.Location = new System.Drawing.Point(105, 49);
         this.TestMemoryButton.Name = "TestMemoryButton";
         this.TestMemoryButton.Size = new System.Drawing.Size(93, 23);
         this.TestMemoryButton.TabIndex = 12;
         this.TestMemoryButton.Text = "Memory";
         this.TestMemoryButton.UseVisualStyleBackColor = true;
         this.TestMemoryButton.Click += new System.EventHandler(this.TestMemoryButton_Click);
         // 
         // TestSoundButton
         // 
         this.TestSoundButton.Location = new System.Drawing.Point(105, 105);
         this.TestSoundButton.Name = "TestSoundButton";
         this.TestSoundButton.Size = new System.Drawing.Size(93, 23);
         this.TestSoundButton.TabIndex = 13;
         this.TestSoundButton.Text = "Sound";
         this.TestSoundButton.UseVisualStyleBackColor = true;
         this.TestSoundButton.Click += new System.EventHandler(this.TestSoundButton_Click);
         // 
         // TestRtcButton
         // 
         this.TestRtcButton.Location = new System.Drawing.Point(6, 105);
         this.TestRtcButton.Name = "TestRtcButton";
         this.TestRtcButton.Size = new System.Drawing.Size(93, 23);
         this.TestRtcButton.TabIndex = 14;
         this.TestRtcButton.Text = "RTC";
         this.TestRtcButton.UseVisualStyleBackColor = true;
         this.TestRtcButton.Click += new System.EventHandler(this.TestRtcButton_Click);
         // 
         // TestPrinterComButton
         // 
         this.TestPrinterComButton.Location = new System.Drawing.Point(6, 20);
         this.TestPrinterComButton.Name = "TestPrinterComButton";
         this.TestPrinterComButton.Size = new System.Drawing.Size(93, 23);
         this.TestPrinterComButton.TabIndex = 15;
         this.TestPrinterComButton.Text = "Printer COM";
         this.TestPrinterComButton.UseVisualStyleBackColor = true;
         this.TestPrinterComButton.Click += new System.EventHandler(this.TestPrinterComButton_Click);
         // 
         // TestAdcButton
         // 
         this.TestAdcButton.Location = new System.Drawing.Point(6, 76);
         this.TestAdcButton.Name = "TestAdcButton";
         this.TestAdcButton.Size = new System.Drawing.Size(93, 23);
         this.TestAdcButton.TabIndex = 16;
         this.TestAdcButton.Text = "ADC";
         this.TestAdcButton.UseVisualStyleBackColor = true;
         this.TestAdcButton.Click += new System.EventHandler(this.TestAdcButton_Click);
         // 
         // TestInternalAdcButton
         // 
         this.TestInternalAdcButton.Location = new System.Drawing.Point(105, 77);
         this.TestInternalAdcButton.Name = "TestInternalAdcButton";
         this.TestInternalAdcButton.Size = new System.Drawing.Size(93, 23);
         this.TestInternalAdcButton.TabIndex = 17;
         this.TestInternalAdcButton.Text = "Internal ADC";
         this.TestInternalAdcButton.UseVisualStyleBackColor = true;
         this.TestInternalAdcButton.Click += new System.EventHandler(this.TestInternalAdcButton_Click);
         // 
         // TestDisplayButton
         // 
         this.TestDisplayButton.Location = new System.Drawing.Point(6, 48);
         this.TestDisplayButton.Name = "TestDisplayButton";
         this.TestDisplayButton.Size = new System.Drawing.Size(93, 23);
         this.TestDisplayButton.TabIndex = 18;
         this.TestDisplayButton.Text = "Display";
         this.TestDisplayButton.UseVisualStyleBackColor = true;
         this.TestDisplayButton.Click += new System.EventHandler(this.TestDisplayButton_Click);
         // 
         // TestUsbButton
         // 
         this.TestUsbButton.Location = new System.Drawing.Point(6, 134);
         this.TestUsbButton.Name = "TestUsbButton";
         this.TestUsbButton.Size = new System.Drawing.Size(93, 23);
         this.TestUsbButton.TabIndex = 19;
         this.TestUsbButton.Text = "USB";
         this.TestUsbButton.UseVisualStyleBackColor = true;
         this.TestUsbButton.Click += new System.EventHandler(this.TestUsbButton_Click);
         // 
         // ChargerConnectionTestButton
         // 
         this.ChargerConnectionTestButton.Location = new System.Drawing.Point(105, 134);
         this.ChargerConnectionTestButton.Name = "ChargerConnectionTestButton";
         this.ChargerConnectionTestButton.Size = new System.Drawing.Size(91, 23);
         this.ChargerConnectionTestButton.TabIndex = 20;
         this.ChargerConnectionTestButton.Text = "Charger con";
         this.ChargerConnectionTestButton.UseVisualStyleBackColor = true;
         this.ChargerConnectionTestButton.Click += new System.EventHandler(this.ChargerConnectionTestButton_Click);
         // 
         // ChargerCurrentTextButton
         // 
         this.ChargerCurrentTextButton.Location = new System.Drawing.Point(6, 19);
         this.ChargerCurrentTextButton.Name = "ChargerCurrentTextButton";
         this.ChargerCurrentTextButton.Size = new System.Drawing.Size(116, 23);
         this.ChargerCurrentTextButton.TabIndex = 21;
         this.ChargerCurrentTextButton.Text = "Charger current";
         this.ChargerCurrentTextButton.UseVisualStyleBackColor = true;
         this.ChargerCurrentTextButton.Click += new System.EventHandler(this.ChargerCurrentTextButton_Click);
         // 
         // ChargerShutdownTestButton
         // 
         this.ChargerShutdownTestButton.Location = new System.Drawing.Point(128, 19);
         this.ChargerShutdownTestButton.Name = "ChargerShutdownTestButton";
         this.ChargerShutdownTestButton.Size = new System.Drawing.Size(116, 23);
         this.ChargerShutdownTestButton.TabIndex = 22;
         this.ChargerShutdownTestButton.Text = "Charger shut down";
         this.ChargerShutdownTestButton.UseVisualStyleBackColor = true;
         this.ChargerShutdownTestButton.Click += new System.EventHandler(this.ChargerShutdownTestButton_Click);
         // 
         // TargetShuwdownTestButton
         // 
         this.TargetShuwdownTestButton.Location = new System.Drawing.Point(6, 49);
         this.TargetShuwdownTestButton.Name = "TargetShuwdownTestButton";
         this.TargetShuwdownTestButton.Size = new System.Drawing.Size(116, 23);
         this.TargetShuwdownTestButton.TabIndex = 23;
         this.TargetShuwdownTestButton.Text = "Target shut down";
         this.TargetShuwdownTestButton.UseVisualStyleBackColor = true;
         this.TargetShuwdownTestButton.Click += new System.EventHandler(this.TargetShuwdownTestButton_Click);
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.TestKeyboardButton);
         this.groupBox1.Controls.Add(this.TestMemoryButton);
         this.groupBox1.Controls.Add(this.TestSoundButton);
         this.groupBox1.Controls.Add(this.TestRtcButton);
         this.groupBox1.Controls.Add(this.TestPrinterComButton);
         this.groupBox1.Controls.Add(this.TestAdcButton);
         this.groupBox1.Controls.Add(this.TestInternalAdcButton);
         this.groupBox1.Controls.Add(this.TestDisplayButton);
         this.groupBox1.Controls.Add(this.TestUsbButton);
         this.groupBox1.Controls.Add(this.ChargerConnectionTestButton);
         this.groupBox1.Location = new System.Drawing.Point(364, 159);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(204, 168);
         this.groupBox1.TabIndex = 30;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Test firmware";
         // 
         // BatchButton
         // 
         this.BatchButton.Location = new System.Drawing.Point(410, 527);
         this.BatchButton.Name = "BatchButton";
         this.BatchButton.Size = new System.Drawing.Size(75, 23);
         this.BatchButton.TabIndex = 31;
         this.BatchButton.Text = "Run batch";
         this.BatchButton.UseVisualStyleBackColor = true;
         this.BatchButton.Click += new System.EventHandler(this.BatchButton_Click);
         // 
         // menuStrip1
         // 
         this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.utilityToolStripMenuItem1});
         this.menuStrip1.Location = new System.Drawing.Point(0, 0);
         this.menuStrip1.Name = "menuStrip1";
         this.menuStrip1.Size = new System.Drawing.Size(619, 24);
         this.menuStrip1.TabIndex = 32;
         this.menuStrip1.Text = "menuStrip1";
         // 
         // utilityToolStripMenuItem1
         // 
         this.utilityToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debugToolStripMenuItem1,
            this.detectBat1ToolStripMenuItem1,
            this.testToolStripMenuItem1});
         this.utilityToolStripMenuItem1.Name = "utilityToolStripMenuItem1";
         this.utilityToolStripMenuItem1.Size = new System.Drawing.Size(50, 20);
         this.utilityToolStripMenuItem1.Text = "Utility";
         // 
         // debugToolStripMenuItem1
         // 
         this.debugToolStripMenuItem1.Name = "debugToolStripMenuItem1";
         this.debugToolStripMenuItem1.Size = new System.Drawing.Size(134, 22);
         this.debugToolStripMenuItem1.Text = "Debug";
         this.debugToolStripMenuItem1.Click += new System.EventHandler(this.debugToolStripMenuItem1_Click);
         // 
         // detectBat1ToolStripMenuItem1
         // 
         this.detectBat1ToolStripMenuItem1.Name = "detectBat1ToolStripMenuItem1";
         this.detectBat1ToolStripMenuItem1.Size = new System.Drawing.Size(134, 22);
         this.detectBat1ToolStripMenuItem1.Text = "Detect Bat1";
         this.detectBat1ToolStripMenuItem1.Click += new System.EventHandler(this.detectBat1ToolStripMenuItem1_Click);
         // 
         // testToolStripMenuItem1
         // 
         this.testToolStripMenuItem1.Name = "testToolStripMenuItem1";
         this.testToolStripMenuItem1.Size = new System.Drawing.Size(134, 22);
         this.testToolStripMenuItem1.Text = "Test";
         this.testToolStripMenuItem1.Click += new System.EventHandler(this.testToolStripMenuItem1_Click);
         // 
         // fileToolStripMenuItem
         // 
         this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
         this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
         this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
         this.fileToolStripMenuItem.Text = "File";
         // 
         // exitToolStripMenuItem
         // 
         this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
         this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
         this.exitToolStripMenuItem.Text = "Exit";
         this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
         // 
         // utilityToolStripMenuItem
         // 
         this.utilityToolStripMenuItem.Name = "utilityToolStripMenuItem";
         this.utilityToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
         // 
         // detectBAT1ToolStripMenuItem
         // 
         this.detectBAT1ToolStripMenuItem.Name = "detectBAT1ToolStripMenuItem";
         this.detectBAT1ToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
         // 
         // testToolStripMenuItem
         // 
         this.testToolStripMenuItem.Name = "testToolStripMenuItem";
         this.testToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
         // 
         // debugToolStripMenuItem
         // 
         this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
         this.debugToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
         // 
         // WriteConfigButton
         // 
         this.WriteConfigButton.Location = new System.Drawing.Point(485, 70);
         this.WriteConfigButton.Name = "WriteConfigButton";
         this.WriteConfigButton.Size = new System.Drawing.Size(115, 23);
         this.WriteConfigButton.TabIndex = 33;
         this.WriteConfigButton.Text = "Write configuration";
         this.WriteConfigButton.UseVisualStyleBackColor = true;
         this.WriteConfigButton.Click += new System.EventHandler(this.WriteConfigButton_Click);
         // 
         // TestBatchesComboBox
         // 
         this.TestBatchesComboBox.FormattingEnabled = true;
         this.TestBatchesComboBox.Location = new System.Drawing.Point(364, 499);
         this.TestBatchesComboBox.Name = "TestBatchesComboBox";
         this.TestBatchesComboBox.Size = new System.Drawing.Size(121, 21);
         this.TestBatchesComboBox.TabIndex = 34;
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.ChargerCurrentTextButton);
         this.groupBox2.Controls.Add(this.ChargerShutdownTestButton);
         this.groupBox2.Controls.Add(this.TargetShuwdownTestButton);
         this.groupBox2.Controls.Add(this.TestPowerIdleButton);
         this.groupBox2.Controls.Add(this.TestPowerBacklightOnButton);
         this.groupBox2.Controls.Add(this.TestPowerBacklightOffButton);
         this.groupBox2.Controls.Add(this.TestPowerInvertedButton);
         this.groupBox2.Location = new System.Drawing.Point(364, 333);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(250, 140);
         this.groupBox2.TabIndex = 35;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Power tests";
         // 
         // ModelClearReportButton
         // 
         this.ModelClearReportButton.Location = new System.Drawing.Point(93, 492);
         this.ModelClearReportButton.Name = "ModelClearReportButton";
         this.ModelClearReportButton.Size = new System.Drawing.Size(75, 23);
         this.ModelClearReportButton.TabIndex = 36;
         this.ModelClearReportButton.Text = "Clear";
         this.ModelClearReportButton.UseVisualStyleBackColor = true;
         this.ModelClearReportButton.Click += new System.EventHandler(this.ClearReportButton_Click);
         // 
         // ModelSaveReportButton
         // 
         this.ModelSaveReportButton.Location = new System.Drawing.Point(12, 492);
         this.ModelSaveReportButton.Name = "ModelSaveReportButton";
         this.ModelSaveReportButton.Size = new System.Drawing.Size(75, 23);
         this.ModelSaveReportButton.TabIndex = 37;
         this.ModelSaveReportButton.Text = "Save";
         this.ModelSaveReportButton.UseVisualStyleBackColor = true;
         this.ModelSaveReportButton.Click += new System.EventHandler(this.SaveReportButton_Click);
         // 
         // TesterGroupBox
         // 
         this.TesterGroupBox.Controls.Add(this.ModelTesterStatusImage);
         this.TesterGroupBox.Controls.Add(this.ModelCompleteButton0);
         this.TesterGroupBox.Controls.Add(this.ModelTesterOperationLabel);
         this.TesterGroupBox.Controls.Add(this.ModelTesterRadioButton1);
         this.TesterGroupBox.Controls.Add(this.ModelTesterRadioButton0);
         this.TesterGroupBox.Location = new System.Drawing.Point(12, 27);
         this.TesterGroupBox.Name = "TesterGroupBox";
         this.TesterGroupBox.Size = new System.Drawing.Size(203, 93);
         this.TesterGroupBox.TabIndex = 40;
         this.TesterGroupBox.TabStop = false;
         this.TesterGroupBox.Text = "Tester";
         // 
         // ModelTesterStatusImage
         // 
         this.ModelTesterStatusImage.Location = new System.Drawing.Point(44, 22);
         this.ModelTesterStatusImage.Name = "ModelTesterStatusImage";
         this.ModelTesterStatusImage.Size = new System.Drawing.Size(15, 15);
         this.ModelTesterStatusImage.TabIndex = 48;
         this.ModelTesterStatusImage.TabStop = false;
         this.ModelTesterStatusImage.Visible = false;
         // 
         // ModelCompleteButton0
         // 
         this.ModelCompleteButton0.Location = new System.Drawing.Point(6, 61);
         this.ModelCompleteButton0.Name = "ModelCompleteButton0";
         this.ModelCompleteButton0.Size = new System.Drawing.Size(75, 23);
         this.ModelCompleteButton0.TabIndex = 46;
         this.ModelCompleteButton0.Text = "Complete 0";
         this.ModelCompleteButton0.UseVisualStyleBackColor = true;
         this.ModelCompleteButton0.Visible = false;
         // 
         // ModelTesterOperationLabel
         // 
         this.ModelTesterOperationLabel.AutoSize = true;
         this.ModelTesterOperationLabel.Location = new System.Drawing.Point(7, 44);
         this.ModelTesterOperationLabel.Name = "ModelTesterOperationLabel";
         this.ModelTesterOperationLabel.Size = new System.Drawing.Size(13, 13);
         this.ModelTesterOperationLabel.TabIndex = 2;
         this.ModelTesterOperationLabel.Text = "?";
         this.ModelTesterOperationLabel.Visible = false;
         // 
         // ModelTesterRadioButton1
         // 
         this.ModelTesterRadioButton1.AutoSize = true;
         this.ModelTesterRadioButton1.Location = new System.Drawing.Point(112, 20);
         this.ModelTesterRadioButton1.Name = "ModelTesterRadioButton1";
         this.ModelTesterRadioButton1.Size = new System.Drawing.Size(31, 17);
         this.ModelTesterRadioButton1.TabIndex = 1;
         this.ModelTesterRadioButton1.TabStop = true;
         this.ModelTesterRadioButton1.Text = "1";
         this.ModelTesterRadioButton1.UseVisualStyleBackColor = true;
         this.ModelTesterRadioButton1.Visible = false;
         // 
         // ModelTesterRadioButton0
         // 
         this.ModelTesterRadioButton0.AutoSize = true;
         this.ModelTesterRadioButton0.Location = new System.Drawing.Point(7, 20);
         this.ModelTesterRadioButton0.Name = "ModelTesterRadioButton0";
         this.ModelTesterRadioButton0.Size = new System.Drawing.Size(31, 17);
         this.ModelTesterRadioButton0.TabIndex = 0;
         this.ModelTesterRadioButton0.TabStop = true;
         this.ModelTesterRadioButton0.Text = "0";
         this.ModelTesterRadioButton0.UseVisualStyleBackColor = true;
         this.ModelTesterRadioButton0.Visible = false;
         // 
         // TesterLabel
         // 
         this.TesterLabel.AutoSize = true;
         this.TesterLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         this.TesterLabel.Location = new System.Drawing.Point(221, 62);
         this.TesterLabel.Name = "TesterLabel";
         this.TesterLabel.Size = new System.Drawing.Size(23, 25);
         this.TesterLabel.TabIndex = 45;
         this.TesterLabel.Text = "?";
         // 
         // ModelStatusTextBox
         // 
         this.ModelStatusTextBox.Location = new System.Drawing.Point(12, 126);
         this.ModelStatusTextBox.Multiline = true;
         this.ModelStatusTextBox.Name = "ModelStatusTextBox";
         this.ModelStatusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.ModelStatusTextBox.Size = new System.Drawing.Size(338, 360);
         this.ModelStatusTextBox.TabIndex = 2;
         this.ModelStatusTextBox.Visible = false;
         // 
         // OperationProgressBar
         // 
         this.OperationProgressBar.Location = new System.Drawing.Point(12, 521);
         this.OperationProgressBar.Name = "OperationProgressBar";
         this.OperationProgressBar.Size = new System.Drawing.Size(338, 23);
         this.OperationProgressBar.TabIndex = 1;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(6, 16);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(40, 13);
         this.label1.TabIndex = 46;
         this.label1.Text = "Config:";
         // 
         // ConfigNameLabel
         // 
         this.ConfigNameLabel.AutoSize = true;
         this.ConfigNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
         this.ConfigNameLabel.Location = new System.Drawing.Point(46, 16);
         this.ConfigNameLabel.Name = "ConfigNameLabel";
         this.ConfigNameLabel.Size = new System.Drawing.Size(14, 13);
         this.ConfigNameLabel.TabIndex = 47;
         this.ConfigNameLabel.Text = "?";
         // 
         // groupBox3
         // 
         this.groupBox3.Controls.Add(this.ConfigNameLabel);
         this.groupBox3.Controls.Add(this.LogoPictureBox);
         this.groupBox3.Controls.Add(this.label1);
         this.groupBox3.Location = new System.Drawing.Point(492, 480);
         this.groupBox3.Name = "groupBox3";
         this.groupBox3.Size = new System.Drawing.Size(122, 99);
         this.groupBox3.TabIndex = 49;
         this.groupBox3.TabStop = false;
         // 
         // LogoPictureBox
         // 
         this.LogoPictureBox.Location = new System.Drawing.Point(8, 32);
         this.LogoPictureBox.Name = "LogoPictureBox";
         this.LogoPictureBox.Size = new System.Drawing.Size(100, 61);
         this.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
         this.LogoPictureBox.TabIndex = 48;
         this.LogoPictureBox.TabStop = false;
         // 
         // CyclicCheckBox
         // 
         this.CyclicCheckBox.AutoSize = true;
         this.CyclicCheckBox.Location = new System.Drawing.Point(296, 103);
         this.CyclicCheckBox.Name = "CyclicCheckBox";
         this.CyclicCheckBox.Size = new System.Drawing.Size(54, 17);
         this.CyclicCheckBox.TabIndex = 50;
         this.CyclicCheckBox.Text = "Cyclic";
         this.CyclicCheckBox.UseVisualStyleBackColor = true;
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(619, 582);
         this.Controls.Add(this.CyclicCheckBox);
         this.Controls.Add(this.groupBox3);
         this.Controls.Add(this.OperationProgressBar);
         this.Controls.Add(this.ModelStatusTextBox);
         this.Controls.Add(this.TesterLabel);
         this.Controls.Add(this.TesterGroupBox);
         this.Controls.Add(this.ModelSaveReportButton);
         this.Controls.Add(this.ModelClearReportButton);
         this.Controls.Add(this.SwitchOffButton);
         this.Controls.Add(this.SwitchTargetOnButton);
         this.Controls.Add(this.groupBox2);
         this.Controls.Add(this.TestBatchesComboBox);
         this.Controls.Add(this.WriteConfigButton);
         this.Controls.Add(this.BatchButton);
         this.Controls.Add(this.groupBox1);
         this.Controls.Add(this.FTDIProgrammingButton);
         this.Controls.Add(this.TestFirmwareButton);
         this.Controls.Add(this.ProductionFirmwareButton);
         this.Controls.Add(this.menuStrip1);
         this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
         this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
         this.MainMenuStrip = this.menuStrip1;
         this.Name = "Form1";
         this.Text = "Bat1 tester v2.0";
         this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
         this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
         this.groupBox1.ResumeLayout(false);
         this.menuStrip1.ResumeLayout(false);
         this.menuStrip1.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.TesterGroupBox.ResumeLayout(false);
         this.TesterGroupBox.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.ModelTesterStatusImage)).EndInit();
         this.groupBox3.ResumeLayout(false);
         this.groupBox3.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.LogoPictureBox)).EndInit();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button ProductionFirmwareButton;
      private System.Windows.Forms.Timer timer1;
      private System.Windows.Forms.Button TestFirmwareButton;
      private System.Windows.Forms.Button TestKeyboardButton;
      private System.Windows.Forms.Button TestPowerIdleButton;
      private System.Windows.Forms.Button SwitchTargetOnButton;
      private System.Windows.Forms.Button SwitchOffButton;
      private System.Windows.Forms.Button TestPowerInvertedButton;
      private System.Windows.Forms.Button TestPowerBacklightOnButton;
      private System.Windows.Forms.Button TestPowerBacklightOffButton;
      private System.Windows.Forms.Button FTDIProgrammingButton;
      private System.Windows.Forms.Button TestMemoryButton;
      private System.Windows.Forms.Button TestSoundButton;
      private System.Windows.Forms.Button TestRtcButton;
      private System.Windows.Forms.Button TestPrinterComButton;
      private System.Windows.Forms.Button TestAdcButton;
      private System.Windows.Forms.Button TestInternalAdcButton;
      private System.Windows.Forms.Button TestDisplayButton;
      private System.Windows.Forms.Button TestUsbButton;
      private System.Windows.Forms.Button ChargerConnectionTestButton;
      private System.Windows.Forms.Button ChargerCurrentTextButton;
      private System.Windows.Forms.Button ChargerShutdownTestButton;
      private System.Windows.Forms.Button TargetShuwdownTestButton;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Button BatchButton;
      private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
      private System.Windows.Forms.Button WriteConfigButton;
      private System.Windows.Forms.ComboBox TestBatchesComboBox;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button ModelClearReportButton;
      private System.Windows.Forms.Button ModelSaveReportButton;
      private System.Windows.Forms.GroupBox TesterGroupBox;
      private System.Windows.Forms.ToolStripMenuItem utilityToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem detectBAT1ToolStripMenuItem;
      private System.Windows.Forms.Label TesterLabel;
      private System.Windows.Forms.TextBox ModelStatusTextBox;
      private System.Windows.Forms.ProgressBar OperationProgressBar;
      private System.Windows.Forms.RadioButton ModelTesterRadioButton1;
      private System.Windows.Forms.RadioButton ModelTesterRadioButton0;
      private System.Windows.Forms.Button ModelCompleteButton0;
      private System.Windows.Forms.PictureBox ModelTesterStatusImage;
      private System.Windows.Forms.Label ModelTesterOperationLabel;
      private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label ConfigNameLabel;
      private System.Windows.Forms.PictureBox LogoPictureBox;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
      private System.Windows.Forms.CheckBox CyclicCheckBox;
      private System.Windows.Forms.ToolStripMenuItem utilityToolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem detectBat1ToolStripMenuItem1;
      private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem1;
   }
}

