﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Bat1.Tester;

namespace Bat1Tester {
   public partial class Test : Form {
      Form1 MainForm;

      public Test(Form1 _Parent) {
         InitializeComponent();

         MainForm = _Parent;

         Crt = new Crt(textBox1);
      }

      bool Run;

      Crt Crt;

      int[] FailedCount = new int[2];

      public void TestTester(int TesterId) {
         double[] Frequency = new double[4] {1, 0, 0, 0 };
         Random r = new Random();
         double diceRoll;
         double cumulative;
         int selectedElement;
         int MaxBreak;
         int MinBreak;
         int Break;

         while(Run) {
            diceRoll = r.NextDouble();
            cumulative = 0.0;
            selectedElement = 0;
            for(int i = 0; i < MainForm.Controller.Testers[0].TestBatches.Count; i++)
            {
               cumulative += Frequency[i];
                if (diceRoll < cumulative)
                {
                    selectedElement = i;
                    break;
                }
            }
            string a = MainForm.Controller.TestBatches[0].ID;
            MainForm.StartBatch(TesterId, MainForm.Controller.TestBatches[selectedElement].ID);

            Crt.WriteLine("Tester " + TesterId.ToString() + " : " + MainForm.Controller.Testers[TesterId].TestBatches[selectedElement].ID);

            Thread.Sleep(100);

            while(MainForm.Controller.Testers[TesterId].InProgress == true) {
               Thread.Sleep(1000);
            };


            if(MainForm.Controller.Testers[TesterId].Failed) {
               FailedCount[TesterId]++;
            }

            Crt.WriteLine("Tester " + TesterId.ToString() + " : Failed " + FailedCount[TesterId].ToString());

            if(!Int32.TryParse(MaxBreakTextBox.Text, out MaxBreak) || !Int32.TryParse(MinBreakTextBox.Text, out MinBreak)) {
               return;
            }

            Break = r.Next(MinBreak, MaxBreak) + TesterId;

            Crt.WriteLine("Tester " + TesterId.ToString() + " : Sleep " + Break.ToString() + " s");

            Thread.Sleep(Break * 1000);
         }

      }

      Thread[] Threads = new Thread[2];

      private void StartButton_Click(object sender, EventArgs e) {
        Run = true;
        Threads[0] = new Thread(delegate(object unused) {
           TestTester(0);
        });

        Threads[0].Start();

        StartButton.Enabled = false;
      }

      private void button1_Click(object sender, EventArgs e) {
         Run = false;

         StartButton.Enabled = true;
      }

      private void Start1Button_Click(object sender, EventArgs e) {
         Run = true;

         Threads[1] = new Thread(delegate(object unused) {
            TestTester(1);
         });

         Threads[1].Start();

         Start1Button.Enabled = false;
      }


   }
}
