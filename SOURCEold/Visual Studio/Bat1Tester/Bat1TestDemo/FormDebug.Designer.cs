﻿namespace Bat1Tester {
   partial class FormDebug {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.DebugCheckBox = new System.Windows.Forms.CheckBox();
         this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
         this.SaveButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // DebugCheckBox
         // 
         this.DebugCheckBox.AutoSize = true;
         this.DebugCheckBox.Location = new System.Drawing.Point(42, 31);
         this.DebugCheckBox.Name = "DebugCheckBox";
         this.DebugCheckBox.Size = new System.Drawing.Size(58, 17);
         this.DebugCheckBox.TabIndex = 0;
         this.DebugCheckBox.Text = "Debug";
         this.DebugCheckBox.UseVisualStyleBackColor = true;
         //this.DebugCheckBox.CheckedChanged += new System.EventHandler(this.DebugCheckBox_CheckedChanged);
         // 
         // SaveButton
         // 
         this.SaveButton.Location = new System.Drawing.Point(138, 27);
         this.SaveButton.Name = "SaveButton";
         this.SaveButton.Size = new System.Drawing.Size(75, 23);
         this.SaveButton.TabIndex = 1;
         this.SaveButton.Text = "Save";
         this.SaveButton.UseVisualStyleBackColor = true;
         //this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
         // 
         // FormDebug
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(284, 74);
         this.Controls.Add(this.SaveButton);
         this.Controls.Add(this.DebugCheckBox);
         this.Name = "FormDebug";
         this.Text = "FormDebug";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.CheckBox DebugCheckBox;
      private System.Windows.Forms.SaveFileDialog saveFileDialog1;
      private System.Windows.Forms.Button SaveButton;
   }
}