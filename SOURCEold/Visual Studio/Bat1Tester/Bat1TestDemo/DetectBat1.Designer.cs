﻿namespace Bat1Tester {
   partial class DetectBat1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.OkButton = new System.Windows.Forms.Button();
         this.OutputTextBox = new System.Windows.Forms.TextBox();
         this.RefreshButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // OkButton
         // 
         this.OkButton.Location = new System.Drawing.Point(39, 182);
         this.OkButton.Name = "OkButton";
         this.OkButton.Size = new System.Drawing.Size(75, 23);
         this.OkButton.TabIndex = 0;
         this.OkButton.Text = "OK";
         this.OkButton.UseVisualStyleBackColor = true;
         this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
         // 
         // OutputTextBox
         // 
         this.OutputTextBox.Location = new System.Drawing.Point(12, 12);
         this.OutputTextBox.Multiline = true;
         this.OutputTextBox.Name = "OutputTextBox";
         this.OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
         this.OutputTextBox.Size = new System.Drawing.Size(260, 164);
         this.OutputTextBox.TabIndex = 1;
         // 
         // RefreshButton
         // 
         this.RefreshButton.Location = new System.Drawing.Point(171, 182);
         this.RefreshButton.Name = "RefreshButton";
         this.RefreshButton.Size = new System.Drawing.Size(75, 23);
         this.RefreshButton.TabIndex = 2;
         this.RefreshButton.Text = "Refresh";
         this.RefreshButton.UseVisualStyleBackColor = true;
         this.RefreshButton.Click += new System.EventHandler(this.RefreshButton_Click);
         // 
         // DetectBat1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(284, 217);
         this.Controls.Add(this.RefreshButton);
         this.Controls.Add(this.OutputTextBox);
         this.Controls.Add(this.OkButton);
         this.Name = "DetectBat1";
         this.Text = "Detection";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button OkButton;
      private System.Windows.Forms.TextBox OutputTextBox;
      private System.Windows.Forms.Button RefreshButton;
   }
}