﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat1;
using Veit.Scale;
using Veit.ScaleStatistics;

namespace Bat1 {

    public struct ScaleConfigVersion {
        /// <summary>
        /// Major version number (1.xx.xx)
        /// </summary>
        public int Major;
        
        /// <summary>
        /// Minor version number (x.01.xx)
        /// </summary>
        public int Minor;

        /// <summary>
        /// Build number (x.xx.01)
        /// </summary>
        public int Build;

        /// <summary>
        /// Hardware version
        /// </summary>
        public int Hw;

        public ScaleConfigVersion(int minorVersion) {
            // Default vyplnim verzi vahy, kterou podporuje tato verze SW
            Major = ScaleVersion.MAJOR;
            Minor = minorVersion;
            Build = 0;
            Hw    = 0;
        }
    }
    
    /// <summary>
    /// Units descriptor
    /// </summary>
    public struct UnitsConfig {
        public int     Range;                      // weighing range
        public Units   Units;                      // weighing units TUnitsEnum
        public int     Decimals;                   // decimals count
        public int     MaxDivision;                // division limit
        public double  Division;                   // scale division
        public WeighingCapacity WeighingCapacity;  // Weighing capacity 30 or 50kg
    }

    /// <summary>
    /// Sounds configuration
    /// </summary>
    public struct SoundsConfig {
        public Tone ToneDefault;                   // Tone of the saving without limits
        public Tone ToneLight;                     // Tone of the below limit
        public Tone ToneOk;                        // Tone of the within limits
        public Tone ToneHeavy;                     // Tone of the above limits
        public Tone ToneKeyboard;                  // Tone of the keyboard click
        public bool EnableSpecial;                 // Enable special sounds
        public int  VolumeKeyboard;                // Volume of the keyboard click
        public int  VolumeSaving;                  // Volume of the saving beep
    }

    /// <summary>
    /// Display mode
    /// </summary>
    public enum DisplayMode {
        DISPLAY_MODE_BASIC,                 // Basic display mode
        DISPLAY_MODE_ADVANCED,              // Advanced display mode
        DISPLAY_MODE_STRONG,                // Strong display mode
        _DISPLAY_MODE_COUNT
    }


    /// <summary>
    /// Backlight mode
    /// </summary>
    public enum BacklightMode {
        BACKLIGHT_MODE_AUTO,                // Backlight in automatic mode
        BACKLIGHT_MODE_ON,	                // Backlight is always on
        BACKLIGHT_MODE_OFF,	                // Backlight is always off
        _BACKLIGHT_MODE_COUNT
    }

    /// <summary>
    /// Backlight configuration
    /// </summary>
    public struct BacklightConfig {
       public BacklightMode Mode;		               // TBacklightMode Backlight mode   
       public int           Intensity;	               // Backlight intensity
       public int           Duration;                   // Backlight duration [s]
    }

    /// <summary>
    /// Display configuration
    /// </summary>
    public struct DisplayConfig {
       public DisplayMode     Mode;              // TDisplayMode Display mode
       public int             Contrast;          // Display contrast
       public BacklightConfig Backlight;         // Backlight setup
    }

    /// <summary>
    /// Print configuration
    /// </summary>
    public struct PrintConfig {
       public int       PaperWidth;                    // Paper width [mm]
       public ComFormat CommunicationFormat;           // Serial format TComFormat enum
       public int       CommunicationSpeed;            // Serial speed [Bd]
    }

    /// <summary>
    /// Parameters for statistic calculations
    /// </summary>
    public struct StatisticConfig {
        public int             UniformityRange;// Uniformity range in +/- percents of average weight
        public HistogramConfig Histogram;      // Histogram parameters
    }

    /// <summary>
    /// Weight sorting
    /// </summary>
    public struct WeightSortingConfig {
        public WeightSorting Mode;	               // TWeightSortingMode - Mode of limits used
        public double        LowLimit;		       // Limit / Low limit
        public double        HighLimit;		       // High limit
    }

    /// <summary>
    /// Saving parameters
    /// </summary>
    public struct SavingConfig {
        public SavingMode Mode;                       // TSavingMode Saving mode
        public bool       EnableMoreBirds;            // Enable weighing more birds
        public int        NumberOfBirds;              // Number of birds
        public double     Filter;                     // Filter averaging time [sec]
        public double     StabilisationTime;          // Stable duration [sec]
        public double     MinimumWeight;              // Charge/discharge treshold [kg]
        public double     StabilisationRange;         // Stable range [%]
    }

    /// <summary>
    /// Weighing configuration
    /// </summary>
    public struct WeighingConfig {
       public WeightSortingConfig WeightSorting;        // Weight limits mode and weights
       public SavingConfig        Saving;               // Saving parameters
    }

    public struct PasswordConfig {
        public bool Enable;         // Enable password
        public byte [] Password;    // Keys kombination
    }

    /// <summary>
    /// Constants
    /// </summary>
    public class ScaleConfigConst {
        /// <summary>
        /// Maximum length of the scale name
        /// </summary>
        public const int SCALE_NAME_LENGTH	= 15;

        /// <summary>
        /// Number of password keys
        /// </summary>
        public const int PASSWORD_LENGTH = 4;
        
        /// <summary>
        /// volume 0..MAX
        /// </summary>
        public const int VOLUME_MAX = 10;

        /// <summary>
        /// backlight intensity 0..MAX
        /// </summary>
        public const int BACKLIGHT_MAX = 20;
        
        /// <summary>
        /// contrast intensity 0..MAX
        /// </summary>
        public const int CONTRAST_MAX = 64;
        
        /// <summary>
        /// backlight duration [s]
        /// </summary>
        public const int BACKLIGHT_DURATION_MAX = 300;

        /// <summary>
        /// Print constants - Max. paper width
        /// </summary>
        public const int PAPER_WIDTH_MAX = 99;

        /// <summary>
        /// Max. power off timeout in minutes
        /// </summary>
        public const int POWER_OFF_TIMEOUT_MAX = (99 * 60);

        /// <summary>
        /// maximal number of birds
        /// </summary>
        public const int NUMBER_OF_BIRDS_MAX = 99;

        /// <summary>
        /// Maximum filter * 0.1s
        /// </summary>
        public const int FILTER_MAX = 50;

        /// <summary>
        /// Maximum stabilisation * 0.1s
        /// </summary>
        public const int STABILISATION_TIME_MAX = 50;
    }

    /// <summary>
    /// Global config of the scale
    /// </summary>
    public class ScaleConfig {
        /// <summary>
        /// Scale version
        /// </summary>
        public ScaleConfigVersion Version;

        /// <summary>
        /// Scale name
        /// </summary>
        public string ScaleName;
        
        /// <summary>
        /// Country data
        /// </summary>
        public Country Country;

        /// <summary>
        /// Language in a universal format used in the DB
        /// </summary>
        public ScaleLanguagesInDatabase LanguageInDatabase;

        /// <summary>
        /// Date format
        /// </summary>
        public DateFormat DateFormat;
        
        /// <summary>
        /// First date separator
        /// </summary>
        public char DateSeparator1;

        /// <summary>
        /// Second date separator
        /// </summary>
        public char DateSeparator2;

        /// <summary>
        /// Time format
        /// </summary>
        public TimeFormat TimeFormat;

        /// <summary>
        /// Time separator
        /// </summary>
        public char TimeSeparator;

        /// <summary>
        /// DST
        /// </summary>
        public DaylightSavingMode DaylightSavingMode;

        /// <summary>
        /// Weighing units
        /// </summary>
        public UnitsConfig Units;
        
        /// <summary>
        /// Setup of sounds
        /// </summary>
        public SoundsConfig Sounds;
   
        /// <summary>
        /// Setup of display
        /// </summary>
        public DisplayConfig Display;
   
        /// <summary>
        /// Setup of printer
        /// </summary>
        public PrintConfig Printer;
   
        /// <summary>
        /// Auto power off timeout [s]
        /// </summary>
        public int PowerOffTimeout;

        /// <summary>
        /// File specific parameters (YES/NO)
        /// </summary>
        public bool EnableFileParameters;

        /// <summary>
        /// Password parameters
        /// </summary>
        public PasswordConfig PasswordConfig;

        /// <summary>
        /// Weighing parameters
        /// </summary>
        public WeighingConfig WeighingConfig;

        /// <summary>
        /// Statistic parameters
        /// </summary>
        public StatisticConfig StatisticConfig;

        /// <summary>
        /// List of defined files
        /// </summary>
        public FileList FileList;

        /// <summary>
        /// List of defined file groups
        /// </summary>
        //public FileGroupList FileGroupList;

        /// <summary>
        /// Index of an active file in the FileList or -1 if no file is active
        /// </summary>
        public int ActiveFileIndex;

        /// <summary>
        /// Constructor
        /// </summary>
        public ScaleConfig() {
            // Default config zde vytvaret nemuzu, musel bych volat Veit.Bat1.Dll.NewDevice(), coz je staticka
            // fce a rozdrbala by se tak vaha, kterou jsem treba zrovna nacetl a zpracovavam
            PasswordConfig.Password = new byte[Const.PASSWORD_LENGTH];
            FileList      = new FileList();
 //           FileGroupList = new FileGroupList();
            ActiveFileIndex = -1;
        }
        
        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="config">Source config to duplicate</param>
        public ScaleConfig(ScaleConfig config) {
            Version                 = config.Version;
            ScaleName               = config.ScaleName;
            Country                 = config.Country;
            LanguageInDatabase      = config.LanguageInDatabase;
            DateFormat              = config.DateFormat;
            DateSeparator1          = config.DateSeparator1;
            DateSeparator2          = config.DateSeparator2;
            TimeFormat              = config.TimeFormat;
            TimeSeparator           = config.TimeSeparator;
            DaylightSavingMode      = config.DaylightSavingMode;
            Units                   = config.Units;
            Sounds                  = config.Sounds;
            Display                 = config.Display;
            Printer                 = config.Printer;
            PowerOffTimeout         = config.PowerOffTimeout;
            EnableFileParameters    = config.EnableFileParameters;
            PasswordConfig.Enable   = config.PasswordConfig.Enable;
            PasswordConfig.Password = new byte[Const.PASSWORD_LENGTH];
            config.PasswordConfig.Password.CopyTo(PasswordConfig.Password, 0);
            WeighingConfig          = config.WeighingConfig;
            StatisticConfig         = config.StatisticConfig;
            FileList                = new FileList(config.FileList);
//            FileGroupList           = new FileGroupList(config.FileGroupList);
            ActiveFileIndex         = config.ActiveFileIndex;
        }
        
        /// <summary>
        /// Constructor for manually entered results
        /// </summary>
        /// <param name="units">Weighing units</param>
        public ScaleConfig(Units units) {
            Units.Units = units;
            ScaleName = "";         // Jmeno vahy nastavim na prazdny string, jinak by zustalo null a blblo by dale.
            ActiveFileIndex = -1;
        }

        private void SetNewUnitsSavingConfig(ref SavingConfig savingConfig, Units oldUnits, Units newUnits) {
            savingConfig.MinimumWeight = ConvertWeight.Convert(savingConfig.MinimumWeight, oldUnits, newUnits);
        }

        private void SetNewUnitsWeighingConfig(ref WeighingConfig weighingConfig, Units oldUnits, Units newUnits, bool setLimits) {
            weighingConfig.Saving.MinimumWeight = ConvertWeight.Convert(weighingConfig.Saving.MinimumWeight, oldUnits, newUnits);
            if (setLimits) {
                weighingConfig.WeightSorting.HighLimit = ConvertWeight.Convert(weighingConfig.WeightSorting.HighLimit, oldUnits, newUnits);
                weighingConfig.WeightSorting.LowLimit  = ConvertWeight.Convert(weighingConfig.WeightSorting.LowLimit,  oldUnits, newUnits);
            }
        }

        /// <summary>
        /// Convert all weight values in config to new units
        /// </summary>
        /// <param name="newUnits">New units</param>
        public void SetNewUnits(Units newUnits) {
            if (newUnits == Units.Units) {
                return;
            }

            // Prepoctu vsechny hmotnosti v configu
            
            // U globalniho configu ponecham meze na default hodnote 1.000 a 2.000 nehlede na jednotky.
            // Meze tedy neprepocitavam, pouze posouvam desetinnou carku
            SetNewUnitsSavingConfig(ref WeighingConfig.Saving, Units.Units, newUnits);
            WeighingConfig.WeightSorting.HighLimit = (float)ConvertWeight.MoveDecimalPoint((float)WeighingConfig.WeightSorting.HighLimit, Units.Units, newUnits);
            WeighingConfig.WeightSorting.LowLimit  = (float)ConvertWeight.MoveDecimalPoint((float)WeighingConfig.WeightSorting.LowLimit,  Units.Units, newUnits);
            StatisticConfig.Histogram.Step = ConvertWeight.Convert(StatisticConfig.Histogram.Step, Units.Units, newUnits);
            Units.Division                 = ConvertWeight.Convert(Units.Division, Units.Units, newUnits);
            
            // File-specific congig
            foreach (File file in FileList.List) {
                // Prepoctu i pokud nepouziva file-specific config, je to nutne kvuli pozdejsi zmene jendotek v Weighings
                SetNewUnitsSavingConfig(ref file.FileConfig.WeighingConfig.Saving, Units.Units, newUnits);
                file.FileConfig.WeighingConfig.WeightSorting.HighLimit = ConvertWeight.Convert(file.FileConfig.WeighingConfig.WeightSorting.HighLimit, Units.Units, newUnits);
                file.FileConfig.WeighingConfig.WeightSorting.LowLimit  = ConvertWeight.Convert(file.FileConfig.WeighingConfig.WeightSorting.LowLimit,  Units.Units, newUnits);
            }

            // Na zaver si jednotky ulozim
            Units.Units = newUnits;
        }

        /// <summary>
        /// Move decimal point in all weight values in config. Use only with old scale.
        /// </summary>
        /// <param name="newUnits">New units</param>
        public void MoveDecimalPoint(Units newUnits) {
            // Osetrim pripady, kdy neni treba prepocet - provadim pouze pri prevodu z gramu nebo na gramy, mezi kg-lb ne
            if (Units.Units == newUnits) {
                return;
            }
            if (Units.Units == Veit.Scale.Units.KG && newUnits == Veit.Scale.Units.LB
             || Units.Units == Veit.Scale.Units.LB && newUnits == Veit.Scale.Units.KG) {
                // Pouze si ulozim nove jednotky, prepocet hodnot neni treba
                Units.Units = newUnits;
                return;
            }

            // Stara vaha ma v configu nastavenou pouze mez a krok histogramu
            WeighingConfig.WeightSorting.LowLimit = ConvertWeight.MoveDecimalPoint((float)WeighingConfig.WeightSorting.LowLimit, Units.Units, newUnits);
            StatisticConfig.Histogram.Step        = ConvertWeight.MoveDecimalPoint((float)StatisticConfig.Histogram.Step,        Units.Units, newUnits);
            foreach (File file in FileList.List) {
                file.FileConfig.WeighingConfig.WeightSorting.LowLimit = ConvertWeight.MoveDecimalPoint((float)file.FileConfig.WeighingConfig.WeightSorting.LowLimit, Units.Units, newUnits);
            }

            // Na zaver si jednotky ulozim
            Units.Units = newUnits;
        }

        /// <summary>
        /// Read file with a specified name from file list in config
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>File instance</returns>
        public File GetFile(string fileName) {
            int index = FileList.GetIndex(fileName);
            if (index < 0) {
                return null;
            }
            return FileList.List[index];
        }
    }
}
