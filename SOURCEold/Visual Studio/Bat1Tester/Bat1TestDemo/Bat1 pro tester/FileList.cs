﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// List of files defined in the scale
    /// </summary>
    public class FileList {

        /// <summary>
        /// List of defined files sorted by file name
        /// </summary>
        public List<File> List { get { return list; } }
        private List<File> list = new List<File>();

        /// <summary>
        /// Constructor
        /// </summary>
        public FileList() {
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="fileList">Source file list</param>
        public FileList(FileList fileList) {
            foreach (File file in fileList.List) {
                File newFile = new File(file.Name, file.Note, file.FileConfig);
                list.Add(newFile);
            }
        }
        
        /// <summary>
        /// Check if file exists
        /// </summary>
        /// <param name="name">File name</param>
        /// <returns>True if exists</returns>
        public bool Exists(string name) {
            return list.Exists(delegate(File file) { return file.Name == name; });
        }
        
        /// <summary>
        /// Sort the list by name
        /// </summary>
        private void Sort() {
            list.Sort(File.CompareByName);
        }
        
        /// <summary>
        /// Add file to the list
        /// </summary>
        /// <param name="file">File to add</param>
        /// <returns>True if successful</returns>
        public bool Add(File file) {
            // Zkontroluju, zda uz soubor se zadanym jmenem v seznamu neexistuje
            if (Exists(file.Name)) {
                return false;
            }

            // Pridam do seznamu a setridim podle jmena
            list.Add(file);
            Sort();

            return true;
        }
        
        /// <summary>
        /// Add new file with specified default config to the list
        /// </summary>
        /// <param name="name">File name</param>
        /// <param name="note">File note</param>
        /// <param name="scaleConfig">Scale config to use</param>
        /// <returns>True if successful</returns>
        public bool Add(string name, string note, ScaleConfig scaleConfig) {
            // Zkontroluju platnost jmena i poznamky
            if (!CheckValue.CheckScaleName(name) || !CheckValue.CheckScaleText(note)) {
                return false;
            }

            // Pridam do seznamu zadany soubor se zadanym configem
            return Add(new File(name, note, new FileConfig(scaleConfig.WeighingConfig)));
        }

        /// <summary>
        /// Add new file with specified default config to the list
        /// </summary>
        /// <param name="name">File name</param>
        /// <param name="scaleConfig">Scale config to use</param>
        /// <returns>True if successful</returns>
        public bool Add(string name, ScaleConfig scaleConfig) {
            // Zkontroluju platnost jmena i poznamky
            if (!CheckValue.CheckScaleName(name)) {
                return false;
            }

            // Pridam do seznamu zadany soubor se zadanym configem
            return Add(new File(name, "", new FileConfig(scaleConfig.WeighingConfig)));
        }

        /// <summary>
        /// Delete file from the list
        /// </summary>
        /// <param name="index">File index</param>
        public void Delete(int index) {
            list.RemoveAt(index);
        }

        /// <summary>
        /// Rename file
        /// </summary>
        /// <param name="index">File index</param>
        /// <param name="newName">New name</param>
        /// <returns>True if successful</returns>
        public bool Rename(int index, string newName) {
            // Zkontroluju platnost jmena
            if (!CheckValue.CheckScaleName(newName)) {
                return false;
            }
            
            // Zkontroluju, zda vubec je treba zmena
            if (list[index].Name == newName) {
                return true;        // Neni treba prejmenovavat, vratim true jako bych prejmenoval
            }
            
            // Zkontroluju, zda uz jmeno v seznamu neni na jine pozici
            if (Exists(newName)) {
                return false;       // Soubor se zadanym jmenem uz existuje
            }
            
            // Zmenim jmeno a znovu setridim
            list[index].Rename(newName);
            Sort();

            return true;
        }

        /// <summary>
        /// Find file index
        /// </summary>
        /// <param name="name">File name</param>
        /// <returns>Index in the list</returns>
        public int GetIndex(string name) {
            for (int i = 0; i < list.Count; i++) {
                if (list[i].Name == name) {
                    return i;
                }
            }

            // Nenasel jsem
            return -1;
        }
    }
}
