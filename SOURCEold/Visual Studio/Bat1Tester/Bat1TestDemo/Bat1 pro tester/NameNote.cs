﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    public class NameNote {
        /// <summary>
        /// Item name
        /// </summary>
        public string Name { get { return name; } }
        private string name;

        /// <summary>
        /// Item note
        /// </summary>
        public string Note { get { return note; } }
        private string note;

        /// <summary>
        /// Empty contructor
        /// </summary>
        public NameNote() {
            name = note = "";
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="note">Note</param>
        public NameNote(string name, string note) {
            // Zkontroluju platnost jmena
            if (!CheckValue.CheckScaleName(name)) {
                throw new ArgumentException("Name is not valid");
            }

            // Zkontroluju platnost poznamky
            if (!CheckValue.CheckScaleText(note)) {
                throw new ArgumentException("Note is not valid");
            }
            
            // Ulozim
            this.name = name;
            this.note = note;
        }

        /// <summary>
        /// Rename
        /// </summary>
        /// <param name="newName">New name</param>
        public bool Rename(string newName) {
            // Zkontroluju platnost jmena
            if (!CheckValue.CheckScaleName(newName)) {
                return false;
            }
            name = newName;     // Jmeno je v poradku
            return true;
        }

        /// <summary>
        /// Rename with Unicode characters (use only with averaging in flocks)
        /// </summary>
        /// <param name="newName">New name</param>
        public void RenameUnicode(string newName) {
            // Platnost jmena nekontroluju
            name = newName;
        }

        /// <summary>
        /// Change note
        /// </summary>
        /// <param name="newNote">New note</param>
        public bool SetNote(string newNote) {
            // Zkontroluju platnost poznamky
            if (!CheckValue.CheckScaleText(newNote)) {
                return false;
            }
            note = newNote;
            return true;
        }

        /// <summary>
        /// Compare function for List(NameNote).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByName(NameNote x, NameNote y) {
            // Musim tridit podle ASCII hodnot, stejne jako ve vaze
            int result = String.CompareOrdinal(x.name, y.name);
            if (result < 0) {
                return -1;
            }
            if (result > 0) {
                return 1;
            }
            return 0;
        }

        /// <summary>
        /// Return NAME (NOTE). If note isn't specified, returns only NAME.
        /// </summary>
        /// <returns>Full string</returns>
        public override string ToString() {
            string str = name;
            if (note != "") {
                str += " (" + note + ")";
            }
            return str;
        }
    }
}
