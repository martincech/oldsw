﻿namespace Bat1Tester {
   partial class Test {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         this.textBox1 = new System.Windows.Forms.TextBox();
         this.MinBreakTextBox = new System.Windows.Forms.TextBox();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.MaxBreakTextBox = new System.Windows.Forms.TextBox();
         this.StartButton = new System.Windows.Forms.Button();
         this.button1 = new System.Windows.Forms.Button();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.Start1Button = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // textBox1
         // 
         this.textBox1.Location = new System.Drawing.Point(12, 12);
         this.textBox1.Multiline = true;
         this.textBox1.Name = "textBox1";
         this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.textBox1.Size = new System.Drawing.Size(226, 248);
         this.textBox1.TabIndex = 0;
         // 
         // MinBreakTextBox
         // 
         this.MinBreakTextBox.Location = new System.Drawing.Point(94, 266);
         this.MinBreakTextBox.Name = "MinBreakTextBox";
         this.MinBreakTextBox.Size = new System.Drawing.Size(100, 20);
         this.MinBreakTextBox.TabIndex = 1;
         this.MinBreakTextBox.Text = "1";
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(12, 273);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(54, 13);
         this.label1.TabIndex = 2;
         this.label1.Text = "Min break";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 300);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(57, 13);
         this.label2.TabIndex = 3;
         this.label2.Text = "Max break";
         // 
         // MaxBreakTextBox
         // 
         this.MaxBreakTextBox.Location = new System.Drawing.Point(94, 297);
         this.MaxBreakTextBox.Name = "MaxBreakTextBox";
         this.MaxBreakTextBox.Size = new System.Drawing.Size(100, 20);
         this.MaxBreakTextBox.TabIndex = 4;
         this.MaxBreakTextBox.Text = "10";
         // 
         // StartButton
         // 
         this.StartButton.Location = new System.Drawing.Point(12, 328);
         this.StartButton.Name = "StartButton";
         this.StartButton.Size = new System.Drawing.Size(75, 23);
         this.StartButton.TabIndex = 5;
         this.StartButton.Text = "Start 0";
         this.StartButton.UseVisualStyleBackColor = true;
         this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(119, 328);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(75, 23);
         this.button1.TabIndex = 6;
         this.button1.Text = "Stop";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // timer1
         // 
         this.timer1.Enabled = true;
         // 
         // Start1Button
         // 
         this.Start1Button.Location = new System.Drawing.Point(12, 357);
         this.Start1Button.Name = "Start1Button";
         this.Start1Button.Size = new System.Drawing.Size(75, 23);
         this.Start1Button.TabIndex = 7;
         this.Start1Button.Text = "Start 1";
         this.Start1Button.UseVisualStyleBackColor = true;
         this.Start1Button.Click += new System.EventHandler(this.Start1Button_Click);
         // 
         // Test
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(246, 394);
         this.Controls.Add(this.Start1Button);
         this.Controls.Add(this.button1);
         this.Controls.Add(this.StartButton);
         this.Controls.Add(this.MaxBreakTextBox);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.MinBreakTextBox);
         this.Controls.Add(this.textBox1);
         this.Name = "Test";
         this.Text = "Test";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox textBox1;
      private System.Windows.Forms.TextBox MinBreakTextBox;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox MaxBreakTextBox;
      private System.Windows.Forms.Button StartButton;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.Timer timer1;
      private System.Windows.Forms.Button Start1Button;

   }
}