﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Configuration;
using Bat1.Tester;
using System.IO;
using System.Drawing;
using System.Diagnostics;

namespace Bat1Tester {


   public partial class Form1 : Form {

      public TesterController Controller;
      private BackgroundOperation[] Operation;
      private Crt[] StatusWindow;
      private TextBox[] StatusTextBox;
      private RadioButton[] TesterRadioButton;
      private Button[] CompleteButton;
      private Label[] TesterOperationLabel;
      private PictureBox[] TesterStatusImage;

      private bool[] CrtSaved;
      private bool[] TesterLastOperation;
      private bool[] StatusUpdate;

      Image ReadyImage;
      Image ErrorImage;
      Image InProgressImage;

      public Form1() {
         InitializeComponent();
         //Trace.Debug = true;

         Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("cs-CZ");
         Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("cs-CZ");

         Graphics g;
         Pen p = new Pen(Color.Black, 1);

         Point ul = new Point(0, 0);
         Point ur = new Point(14, 0);
         Point br = new Point(14, 14);
         Point bl = new Point(0, 14);

         ReadyImage = new Bitmap(15, 15);
         g = Graphics.FromImage(ReadyImage);
         g.Clear(Color.Green);

         ErrorImage = new Bitmap(15, 15);
         g = Graphics.FromImage(ErrorImage) ;
         g.Clear(Color.Red);

         InProgressImage = new Bitmap(15, 15);
         g = Graphics.FromImage(InProgressImage);
         g.Clear(Color.Orange);
         g.DrawLine(p, ul, ur);
         g.DrawLine(p, ur, br);
         g.DrawLine(p, br, bl);
         g.DrawLine(p, bl, ul);

        try {
            Controller = new TesterController();
         } catch(Exception e) {
            MessageBox.Show("Error when initializing testers. Check config files.\r\n\r\n" + e.Message,
               "Error",
               MessageBoxButtons.OK,
               MessageBoxIcon.Exclamation);

            Close();
         }

         int DeltaX = ModelTesterRadioButton1.Location.X - ModelTesterRadioButton0.Location.X;
         int DeltaY = ModelTesterRadioButton1.Location.Y - ModelTesterRadioButton0.Location.Y;

         CrtSaved = new bool[Controller.Testers.Length];
         TesterLastOperation = new bool[Controller.Testers.Length];
         StatusUpdate = new bool[Controller.Testers.Length];
         CompleteButton = new Button[Controller.Testers.Length];
         TesterStatusImage = new System.Windows.Forms.PictureBox[Controller.Testers.Length];
         TesterOperationLabel = new Label[Controller.Testers.Length];
         TesterRadioButton = new RadioButton[Controller.Testers.Length];
         StatusTextBox = new TextBox[Controller.Testers.Length];
         StatusWindow = new Crt[Controller.Testers.Length];
         Operation = new BackgroundOperation[Controller.Testers.Length];

         int i = 0;
         foreach(Tester Tester in Controller.Testers) {
            CrtSaved[i] = true;
            TesterLastOperation[i] = true;

            CompleteButton[i] = new Button();
            CompleteButton[i].Location = new System.Drawing.Point(ModelCompleteButton0.Location.X + i * DeltaX, ModelCompleteButton0.Location.Y + i * DeltaY);
            CompleteButton[i].Name = i.ToString(); // Identifier
            CompleteButton[i].Size = ModelCompleteButton0.Size;
            CompleteButton[i].TabIndex = ModelCompleteButton0.TabIndex;
            CompleteButton[i].Text = "Complete " + i.ToString();
            CompleteButton[i].UseVisualStyleBackColor = ModelCompleteButton0.UseVisualStyleBackColor;
            CompleteButton[i].Click += new System.EventHandler(this.RunCompleteTest_Click);

            TesterStatusImage[i] = new System.Windows.Forms.PictureBox();
            TesterStatusImage[i].Image = ReadyImage;
            TesterStatusImage[i].Location = new System.Drawing.Point(ModelTesterStatusImage.Location.X + i * DeltaX, ModelTesterStatusImage.Location.Y + i * DeltaY);
            TesterStatusImage[i].Name = "ModelTesterStatusImage";
            TesterStatusImage[i].Size = new System.Drawing.Size(15, 15);
            TesterStatusImage[i].TabIndex = 48;
            TesterStatusImage[i].TabStop = false;

            TesterOperationLabel[i] = new Label();
            TesterOperationLabel[i].AutoSize = ModelTesterOperationLabel.AutoSize;
            TesterOperationLabel[i].Location = new System.Drawing.Point(ModelTesterOperationLabel.Location.X + i * DeltaX, ModelTesterOperationLabel.Location.Y + i * DeltaY);
            TesterOperationLabel[i].Name = "TesterOperationLabel" + i.ToString();
            TesterOperationLabel[i].Size = ModelTesterOperationLabel.Size;
            TesterOperationLabel[i].TabIndex = ModelTesterOperationLabel.TabIndex;
            TesterOperationLabel[i].Text = "?";

            TesterRadioButton[i] = new RadioButton();
            TesterRadioButton[i].AutoSize = ModelTesterRadioButton0.AutoSize;
            TesterRadioButton[i].Location = new System.Drawing.Point(ModelTesterRadioButton0.Location.X + i * DeltaX, ModelTesterRadioButton0.Location.Y + i * DeltaY);
            TesterRadioButton[i].Name = i.ToString();
            TesterRadioButton[i].Size = ModelTesterRadioButton0.Size;
            TesterRadioButton[i].TabIndex = ModelTesterRadioButton0.TabIndex;
            TesterRadioButton[i].TabStop = ModelTesterRadioButton0.TabStop;
            TesterRadioButton[i].Text = i.ToString();
            TesterRadioButton[i].UseVisualStyleBackColor = ModelTesterRadioButton0.UseVisualStyleBackColor;
            TesterRadioButton[i].CheckedChanged += new System.EventHandler(this.TesterRadioButton_CheckedChanged);

            this.TesterGroupBox.Controls.Add(TesterOperationLabel[i]);
            this.TesterGroupBox.Controls.Add(TesterRadioButton[i]);
            this.TesterGroupBox.Controls.Add(TesterStatusImage[i]);
            this.TesterGroupBox.Controls.Add(CompleteButton[i]);

            StatusTextBox[i] = new TextBox();
            StatusTextBox[i].Location = ModelStatusTextBox.Location;
            StatusTextBox[i].Multiline = ModelStatusTextBox.Multiline;
            StatusTextBox[i].Name = "StatusTextBox" + i.ToString();
            StatusTextBox[i].ScrollBars = ModelStatusTextBox.ScrollBars;
            StatusTextBox[i].Size = ModelStatusTextBox.Size; ;
            StatusTextBox[i].TabIndex = ModelStatusTextBox.TabIndex;
            StatusTextBox[i].Visible = false;

            this.Controls.Add(StatusTextBox[i]);

            StatusWindow[i] = new Crt(StatusTextBox[i]);

            Operation[i] = new BackgroundOperation();

            Tester.SetReportOutput(StatusWindow[i]);

            i++;
         }

         this.TesterGroupBox.ResumeLayout(false);
         this.TesterGroupBox.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

         foreach(Bat1.Tester.Config.TestBatch.TTestBatch Batch in Controller.TestBatches) {
            TestBatchesComboBox.Items.Add(Batch.ID.ToString());
         }

         if(TestBatchesComboBox.Items.Count > 0) {
            TestBatchesComboBox.SelectedIndex = 0;
         }
         

         LogoPictureBox.ImageLocation = Controller.FactoryConfiguration.Logo;
         ConfigNameLabel.Text = Controller.FactoryConfiguration.ConfigName;

         OperationProgressBar.Visible = false;
         FTDIProgrammingButton.Visible = false;

         TesterRadioButton[0].Checked = true;

         timer1.Enabled = true;
      }

      private int GetSelectedTester() {
         for(int i = 0; i < Controller.Testers.Length; i++) {
            if(TesterRadioButton[i].Checked) {
               return i;
            }
         }

         return 0;
      }

      // Periodical service
      private void timer1_Tick(object sender, EventArgs e) {
         for(int i = 0; i < Controller.Testers.Length; i++) {
            if(Controller.Testers[i].InProgress == true) {
               if(TesterStatusImage[i].Name == "1") {
                  TesterStatusImage[i].Name = "2";
                  TesterStatusImage[i].Image = InProgressImage;
               } else {
                  TesterStatusImage[i].Name = "1";
                  TesterStatusImage[i].Image = null;
               }

               if(Controller.Testers[i].OperationRunning && Controller.Testers[i].TotalOperations > 1) {
                  // save only if a test was inititated
                  CrtSaved[i] = false;
               }

               TesterOperationLabel[i].Text = "Operation " + Controller.Testers[i].CurrentOperation + " / " + Controller.Testers[i].TotalOperations;

               if(TesterLastOperation[i] == true) { // update only if neccessary
                  continue;
               }

               CompleteButton[i].Text = "STOP";
            } else {
               if(TesterLastOperation[i] == false) { // update only if neccessary
                  continue;
               }

               CompleteButton[i].Enabled = true;
               CompleteButton[i].Text = "Complete " + i.ToString();
               TesterOperationLabel[i].Text = "Idle";

               if(Controller.Testers[i].Failed == true) {
                  System.Media.SystemSounds.Hand.Play();
                  TesterStatusImage[i].Image = ErrorImage;
               } else {
                  System.Media.SystemSounds.Beep.Play();
                  TesterStatusImage[i].Image = ReadyImage;
               }

               if(CrtSaved[i] == false) {
                  string FileDir = @".\Reports\";
                  string FilePath;

                  try {
                     if(!Directory.Exists(FileDir)) {
                        Directory.CreateDirectory(FileDir);
                     }

                     string[] filePaths = Directory.GetFiles(FileDir, "*.txt");

                     string Name;
                     int MaxId = 0;
                     int Id = 0;
                     foreach(string Na in filePaths) {
                        Name = Na.Substring(Na.LastIndexOf(@"\") + 1);

                        if(Int32.TryParse(Name.Substring(0, Name.LastIndexOf(".")), out Id)) {
                           if(Id > MaxId) {
                              MaxId = Id;
                           }
                        }
                     }

                     MaxId++;
                     FilePath = FileDir + MaxId.ToString() + ".txt";

                     FileStream fileStream = new FileStream(FilePath, FileMode.Create);

                     try {
                        SaveStatusWindow(i, fileStream);
                        StatusWindow[i].WriteLine("Info: Report saved " + FilePath);
                     } finally {
                        fileStream.Close();
                     }
                  } catch {
                     StatusWindow[i].WriteLine("Info: Report not saved");
                  } finally {
                     CrtSaved[i] = true;
                  }
               }
            }

            TesterLastOperation[i] = Controller.Testers[i].InProgress;
         }
      }

      private void ProductionFirmwareButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.LOAD_PRODUCTION_FIRMWARE);
      }

      private void TestFirmwareButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.LOAD_TEST_FIRMWARE);
      }

      private void TestKeyboardButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_KEYBOARD);
      }

      private void SwitchTargetOnButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.SWITCH_ON);
      }

      private void SwitchOffButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.SWITCH_OFF);
      }

      private void TestPowerBacklightOnButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_POWER_BACKLIGHT_ON);
      }

      private void TestPowerBacklightOffButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_POWER_BACKLIGHT_OFF);
      }

      private void TestPowerIdleButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_POWER_IDLE);
      }

      private void TestPowerInvertedButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_POWER_INVERTED);
      }

      private void FTDIProgrammingButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.FTDI_PROGRAMMING);
      }

      private void TestMemoryButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_MEMORY);
      }

      private void TestSoundButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_SOUND);
      }

      private void TestRtcButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_RTC);
      }

      private void TestDisplayButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_DISPLAY);
      }

      private void TestUsbButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_USB);
      }

      private void TestPrinterComButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_PRINTER_COM);
      }

      private void TestAdcButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_ADC);
      }

      private void TestInternalAdcButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_IADC);
      }

      private void ChargerConnectionTestButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_CHARGER_CONNECTION);
      }

      private void ChargerCurrentTextButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_CHARGER_CURRENT);
      }

      private void ChargerShutdownTestButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_CHARGER_SHUTDOWN);
      }

      private void TargetShuwdownTestButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.TEST_POWER_SHUTDOWN);
      }


      private void WriteConfigButton_Click(object sender, EventArgs e) {
         StartOperation(Tester.Operations.WRITE_CONFIG);
      }

      private void BatchButton_Click(object sender, EventArgs e) {
         StartBatch(GetSelectedTester(), TestBatchesComboBox.Items[TestBatchesComboBox.SelectedIndex].ToString());
      }

      private void RunCompleteTest_Click(object sender, EventArgs e) {
         Button But = (Button)sender;

         int TesterId;

         if(!Int32.TryParse(But.Name, out TesterId)) {
            return;
         }

         if(But.Text == "STOP") {
            But.Text = "aborting ...";
            But.Enabled = false;
            Controller.Testers[TesterId].ForceStop = true;
         } else {
            try {
               StartBatch(TesterId, Controller.TestBatches["COMPLETE"].ID);
            } catch {
               MessageBox.Show("Batch Error. COMPLETE batch defined ?",
                  "Error",
                  MessageBoxButtons.OK,
                  MessageBoxIcon.Exclamation,
                  MessageBoxDefaultButton.Button1);
            }
         }
      }

      public void StartBatch(int TesterId, string Batch) {
         if(!Controller.Testers[TesterId].InProgress) {
            Operation[TesterId].Thread = new Thread(delegate(object unused) {
               Controller.Testers[TesterId].RunBatch(CyclicCheckBox.Checked, Batch);
            });

            Operation[TesterId].Thread.Start();
         }
      }

      public void StartOperation(Tester.Operations TesterOperation) {
         int TesterId = GetSelectedTester();

         if(!Controller.Testers[TesterId].InProgress) {
            Operation[TesterId].Thread = new Thread(delegate(object unused) {
               Controller.Testers[TesterId].Do(CyclicCheckBox.Checked, TesterOperation);
            });

            Operation[TesterId].Thread.Start();
         }
      }

      public void exitToolStripMenuItem_Click(object sender, EventArgs e) {
         this.Close();
      }

      private void ClearReportButton_Click(object sender, EventArgs e) {
         StatusWindow[CurrentTesterId].Clear();
      }

      private void SaveReportButton_Click(object sender, EventArgs e) {
         Stream myStream;
         SaveFileDialog saveFileDialog1 = new SaveFileDialog();

         saveFileDialog1.Filter = "Txt files (*.txt)|*.txt";
         saveFileDialog1.FilterIndex = 1;
         saveFileDialog1.RestoreDirectory = true;

         if(saveFileDialog1.ShowDialog() != DialogResult.OK) {
            return;
         }

         if((myStream = saveFileDialog1.OpenFile()) == null) {
            return;
         }

         try {
            SaveStatusWindow(CurrentTesterId, myStream);
         } catch(Exception) {
            MessageBox.Show("Can't write file.",
               "Error",
               MessageBoxButtons.OK,
               MessageBoxIcon.Exclamation,
               MessageBoxDefaultButton.Button1);
         } finally {
            myStream.Close();
         }
      }

      public void SaveStatusWindow(int TesterId, Stream myStream) {
         System.Text.ASCIIEncoding Enc = new System.Text.ASCIIEncoding();
         byte[] Data = Enc.GetBytes(StatusWindow[TesterId].Get());
         myStream.Write(Data, 0, Data.Length);

      }

      private void TesterRadioButton_CheckedChanged(object sender, EventArgs e) {
         RadioButton RB = (RadioButton)sender;

         int TesterId;

         if(Int32.TryParse(RB.Name, out TesterId)) {
            SwitchTester(TesterId);
         }
      }

      private int CurrentTesterId = 0;
      private void SwitchTester(int TesterId) {
         StatusTextBox[CurrentTesterId].Visible = false;

         StatusTextBox[TesterId].Visible = true;

         TesterLabel.Text = "Tester " + TesterId.ToString();

         CurrentTesterId = TesterId;
      }

      private void Form1_FormClosed(object sender, FormClosedEventArgs e) {
         Controller.Close();
      }

      private void debugToolStripMenuItem1_Click(object sender, EventArgs e) {
         FormDebug FormDebug = new FormDebug();

         FormDebug.Show();
      }

      private void detectBat1ToolStripMenuItem1_Click(object sender, EventArgs e) {
         DetectBat1 DetectBat1 = new DetectBat1();

         DetectBat1.ShowDialog();
      }

      private void testToolStripMenuItem1_Click(object sender, EventArgs e) {
         Test Test = new Test(this);

         Test.Show();
      }

      private void Form1_FormClosing(object sender, FormClosingEventArgs e) {
         foreach(Tester Tester in Controller.Testers) {
            if(Tester.InProgress == true) {
               MessageBox.Show("Can't exit application. First, stop all tests.", "Test in progress", MessageBoxButtons.OK);
               e.Cancel = true;
            }
         }
      }


   }





}

public sealed class FileStreamWithBackup : FileStream {
   public FileStreamWithBackup(string path, long maxFileLength, int maxFileCount, FileMode mode)
      : base(path, BaseFileMode(mode), FileAccess.Write) {
      Init(path, maxFileLength, maxFileCount, mode);
   }

   public FileStreamWithBackup(string path, long maxFileLength, int maxFileCount, FileMode mode, FileShare share)
      : base(path, BaseFileMode(mode), FileAccess.Write, share) {
      Init(path, maxFileLength, maxFileCount, mode);
   }

   public FileStreamWithBackup(string path, long maxFileLength, int maxFileCount, FileMode mode, FileShare share, int bufferSize)
      : base(path, BaseFileMode(mode), FileAccess.Write, share, bufferSize) {
      Init(path, maxFileLength, maxFileCount, mode);
   }

   public FileStreamWithBackup(string path, long maxFileLength, int maxFileCount, FileMode mode, FileShare share, int bufferSize, bool isAsync)
      : base(path, BaseFileMode(mode), FileAccess.Write, share, bufferSize, isAsync) {
      Init(path, maxFileLength, maxFileCount, mode);
   }

   public override bool CanRead { get { return false; } }

   public override void Write(byte[] array, int offset, int count) {
      int actualCount = System.Math.Min(count, array.GetLength(0));
      if(Position + actualCount <= m_maxFileLength) {
         base.Write(array, offset, count);
      } else {
         if(CanSplitData) {
            int partialCount = (int)(System.Math.Max(m_maxFileLength, Position) - Position);
            base.Write(array, offset, partialCount);
            offset += partialCount;
            count = actualCount - partialCount;
         } else {
            if(count > m_maxFileLength)
               throw new ArgumentOutOfRangeException("Buffer size exceeds maximum file length");
         }
         BackupAndResetStream();
         Write(array, offset, count);
      }
   }

   public long MaxFileLength { get { return m_maxFileLength; } }
   public int MaxFileCount { get { return m_maxFileCount; } }
   public bool CanSplitData { get { return m_canSplitData; } set { m_canSplitData = value; } }

   private void Init(string path, long maxFileLength, int maxFileCount, FileMode mode) {
      if(maxFileLength <= 0)
         throw new ArgumentOutOfRangeException("Invalid maximum file length");
      if(maxFileCount <= 0)
         throw new ArgumentOutOfRangeException("Invalid maximum file count");

      m_maxFileLength = maxFileLength;
      m_maxFileCount = maxFileCount;
      m_canSplitData = true;

      string fullPath = Path.GetFullPath(path);
      m_fileDir = Path.GetDirectoryName(fullPath);
      m_fileBase = Path.GetFileNameWithoutExtension(fullPath);
      m_fileExt = Path.GetExtension(fullPath);

      m_fileDecimals = 1;
      int decimalBase = 10;
      while(decimalBase < m_maxFileCount) {
         ++m_fileDecimals;
         decimalBase *= 10;
      }

      switch(mode) {
         case FileMode.Create:
         case FileMode.CreateNew:
         case FileMode.Truncate:
            // Delete old files
            for(int iFile = 0; iFile < m_maxFileCount; ++iFile) {
               string file = GetBackupFileName(iFile);
               if(File.Exists(file))
                  File.Delete(file);
            }
            break;

         default:
            // Position file pointer to the last backup file
            for(int iFile = 0; iFile < m_maxFileCount; ++iFile) {
               if(File.Exists(GetBackupFileName(iFile)))
                  m_nextFileIndex = iFile + 1;
            }
            if(m_nextFileIndex == m_maxFileCount)
               m_nextFileIndex = 0;
            Seek(0, SeekOrigin.End);
            break;
      }
   }

   private void BackupAndResetStream() {
      Flush();
      File.Copy(Name, GetBackupFileName(m_nextFileIndex), true);
      SetLength(0);

      ++m_nextFileIndex;
      if(m_nextFileIndex >= m_maxFileCount)
         m_nextFileIndex = 0;
   }

   private string GetBackupFileName(int index) {
      System.Text.StringBuilder format = new System.Text.StringBuilder();
      format.AppendFormat("D{0}", m_fileDecimals);
      System.Text.StringBuilder sb = new System.Text.StringBuilder();
      if(m_fileExt.Length > 0)
         sb.AppendFormat("{0}{1}{2}", m_fileBase, index.ToString(format.ToString()), m_fileExt);
      else
         sb.AppendFormat("{0}{1}", m_fileBase, index.ToString(format.ToString()));
      return Path.Combine(m_fileDir, sb.ToString());
   }

   private static FileMode BaseFileMode(FileMode mode) {
      return mode == FileMode.Append ? FileMode.OpenOrCreate : mode;
   }

   private long m_maxFileLength;
   private int m_maxFileCount;
   private string m_fileDir;
   private string m_fileBase;
   private string m_fileExt;
   private int m_fileDecimals;
   private bool m_canSplitData;
   private int m_nextFileIndex;
}

public class TextWriterTraceListenerWithTime : TextWriterTraceListener {
   public TextWriterTraceListenerWithTime()
      : base() {
   }

   public TextWriterTraceListenerWithTime(Stream stream)
      : base(stream) {
   }

   public TextWriterTraceListenerWithTime(string path)
      : base(path) {
   }

   public TextWriterTraceListenerWithTime(TextWriter writer)
      : base(writer) {
   }

   public TextWriterTraceListenerWithTime(Stream stream, string name)
      : base(stream, name) {
   }

   public TextWriterTraceListenerWithTime(string path, string name)
      : base(path, name) {
   }

   public TextWriterTraceListenerWithTime(TextWriter writer, string name)
      : base(writer, name) {
   }

   public int ThreadIdentifier = -1;

   public override void WriteLine(string message) {
      if(ThreadIdentifier != Thread.CurrentThread.ManagedThreadId) {
         return;
      }

      base.Write(DateTime.Now.ToString());
      base.Write(" ");
      base.Write(DateTime.Now.Millisecond.ToString());
      base.Write(" ");
      base.WriteLine(message);
   }
}