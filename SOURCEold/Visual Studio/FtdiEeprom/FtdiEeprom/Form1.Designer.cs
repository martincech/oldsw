﻿namespace FtdiEeprom {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.StatusLabel = new System.Windows.Forms.Label();
         this.ReadButton = new System.Windows.Forms.Button();
         this.DescriptionTextBox = new System.Windows.Forms.TextBox();
         this.WriteButton = new System.Windows.Forms.Button();
         this.DescriptionLabel = new System.Windows.Forms.Label();
         this.button1 = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // StatusLabel
         // 
         this.StatusLabel.AutoSize = true;
         this.StatusLabel.Location = new System.Drawing.Point(9, 145);
         this.StatusLabel.Name = "StatusLabel";
         this.StatusLabel.Size = new System.Drawing.Size(49, 13);
         this.StatusLabel.TabIndex = 1;
         this.StatusLabel.Text = "Status: ?";
         // 
         // ReadButton
         // 
         this.ReadButton.Location = new System.Drawing.Point(12, 12);
         this.ReadButton.Name = "ReadButton";
         this.ReadButton.Size = new System.Drawing.Size(102, 23);
         this.ReadButton.TabIndex = 2;
         this.ReadButton.Text = "Read EEPROM";
         this.ReadButton.UseVisualStyleBackColor = true;
         this.ReadButton.Click += new System.EventHandler(this.ReadButton_Click);
         // 
         // DescriptionTextBox
         // 
         this.DescriptionTextBox.Location = new System.Drawing.Point(97, 56);
         this.DescriptionTextBox.Name = "DescriptionTextBox";
         this.DescriptionTextBox.Size = new System.Drawing.Size(189, 20);
         this.DescriptionTextBox.TabIndex = 3;
         // 
         // WriteButton
         // 
         this.WriteButton.Location = new System.Drawing.Point(184, 12);
         this.WriteButton.Name = "WriteButton";
         this.WriteButton.Size = new System.Drawing.Size(102, 23);
         this.WriteButton.TabIndex = 4;
         this.WriteButton.Text = "Write EEPROM";
         this.WriteButton.UseVisualStyleBackColor = true;
         this.WriteButton.Click += new System.EventHandler(this.WriteButton_Click);
         // 
         // DescriptionLabel
         // 
         this.DescriptionLabel.AutoSize = true;
         this.DescriptionLabel.Location = new System.Drawing.Point(9, 59);
         this.DescriptionLabel.Name = "DescriptionLabel";
         this.DescriptionLabel.Size = new System.Drawing.Size(63, 13);
         this.DescriptionLabel.TabIndex = 5;
         this.DescriptionLabel.Text = "Description:";
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(38, 198);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(75, 23);
         this.button1.TabIndex = 6;
         this.button1.Text = "Start";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(311, 277);
         this.Controls.Add(this.button1);
         this.Controls.Add(this.DescriptionLabel);
         this.Controls.Add(this.WriteButton);
         this.Controls.Add(this.DescriptionTextBox);
         this.Controls.Add(this.ReadButton);
         this.Controls.Add(this.StatusLabel);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label StatusLabel;
      private System.Windows.Forms.Button ReadButton;
      private System.Windows.Forms.TextBox DescriptionTextBox;
      private System.Windows.Forms.Button WriteButton;
      private System.Windows.Forms.Label DescriptionLabel;
      private System.Windows.Forms.Button button1;

   }
}

