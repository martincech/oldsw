﻿using System;
using System.Windows.Forms;
using System.Threading;
using Device.Ftxx;
using FTD2XX_NET;





namespace FtdiEeprom {
   public partial class Form1 : Form {
      //private const string DeviceName = "VEIT BAT1 Poultry Scale";
      private const string DeviceName = "VEIT BAT1 Poultry Scale";


      public Form1() {
         InitializeComponent();
      }

      private void SetStatus(string Status) {
         StatusLabel.Text = "Status: " + Status;
      }

      private void ReadButton_Click(object sender, EventArgs e) {
         Ft232 FtdiDevice = new Ft232();

         int Identifier = 0;

         if(FtdiDevice.Locate(DeviceName, ref Identifier) != Ftdi.FT_STATUS.FT_OK) {
            SetStatus("Device not present");
            return;
         }

         if(FtdiDevice.Open(Identifier) != Ftdi.FT_STATUS.FT_OK) {
            SetStatus("Can't open device");
            return;
         }

         Ft232.EEPROM_STRUCTURE Data = new Ft232.EEPROM_STRUCTURE();

         if(FtdiDevice.ReadEEPROM(Data) != Ftdi.FT_STATUS.FT_OK) {
            SetStatus("Can't read EEPROM");
            return;
         }

         //Data.SerNumEnable

         DescriptionTextBox.Text = Data.Description;

         FtdiDevice.Close();

         SetStatus(Data.MaxPower.ToString() + " MAX");
      }

      private void WriteButton_Click(object sender, EventArgs e) {
         Ft232 FtdiDevice = new Ft232();

         int Identifier = 0;

         if(FtdiDevice.Locate(DeviceName, ref Identifier) != Ftdi.FT_STATUS.FT_OK) {
            SetStatus("Device not present");
            return;
         }

         if(FtdiDevice.Open(Identifier) != Ftdi.FT_STATUS.FT_OK) {
            SetStatus("Can't open device");
            return;
         }

         Ft232.EEPROM_STRUCTURE Data = new Ft232.EEPROM_STRUCTURE();

         Data.Description = DescriptionTextBox.Text;

         if(FtdiDevice.WriteEEPROM(Data) != Ftdi.FT_STATUS.FT_OK) {
            SetStatus("Can't write EEPROM");
            return;
         }

         FtdiDevice.Close();

         SetStatus("OK");
      }
      static int Counter = 0;

      public void TestFtdi() {
         int ID = Counter++;
         
         FTDI FtdiDevice = new FTDI();

         FTDI.FT_STATUS Status;
         System.Diagnostics.Debug.WriteLine("Start " + ID.ToString());
         // Count of FTDI devices :
         uint NumDevices = 0;

         Status = FtdiDevice.GetNumberOfDevices(ref NumDevices);

         System.Diagnostics.Debug.WriteLine("Number " + ID.ToString());


         Status = FtdiDevice.OpenByDescription("TesterBoard" + ID.ToString());
         System.Diagnostics.Debug.WriteLine("Open TesterBoard" + ID.ToString());
         if(Status == FTDI.FT_STATUS.FT_OK) {
            FtdiDevice.Close();
         } else {
            System.Diagnostics.Debug.WriteLine("Not open " + ID.ToString());
            return;
         }

         /*if(Status != FTDI.FT_STATUS.FT_OK) {
            System.Diagnostics.Debug.WriteLine("Failed " + Status.ToString());
            return;
         }

         if(NumDevices == 0) {
            System.Diagnostics.Debug.WriteLine("Failed 0");
            return;
         }

         // Allocate storage for device info list
         FTDI.FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FTDI.FT_DEVICE_INFO_NODE[NumDevices];

         Status = FtdiDevice.GetDeviceList(ftdiDeviceList);

         System.Diagnostics.Debug.WriteLine("List " + ID.ToString());

         // Populate our device list
         if(Status != FTDI.FT_STATUS.FT_OK) {
            System.Diagnostics.Debug.WriteLine("Failed " + Status.ToString());
            return;
         }*/
         /*
         // Search by device name
         int Count = 0;
         int UsbIndex = 0;
         int Index = -1;

         foreach(FTDI.FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
            Index++;

            if(Device.Description.ToString() != DeviceName) {
               continue;
            }

            UsbIndex = Index;
            Count++;
         }

         if(Count == 0) {
            System.Diagnostics.Debug.WriteLine("Failed");
            return;
         }

         if(Count > 1) {
            System.Diagnostics.Debug.WriteLine("Failed");
            return;
         }*/

         System.Diagnostics.Debug.WriteLine("Success");

      }

      // Zkouška paralelního používání dvou ftdi naráz
      private void button1_Click(object sender, EventArgs e) {
         Counter = 0;
         
         Thread Thread;
         
         Thread = new Thread(delegate(object unused) {
            TestFtdi();
         });

         Thread.Start();

         Thread.Sleep(0);

         Thread = new Thread(delegate(object unused) {
            TestFtdi();
         });

         Thread.Start();
      }

   }
}
