﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Device.Uart;

namespace Avr32UartTimeout {
   public partial class Form1 : Form {
      NativeUart Port = new NativeUart();
      ComUart DebugPort = new ComUart();
      Encoding Enc = Encoding.ASCII;

      public Form1() {
         InitializeComponent();
      }

      private void button1_Click(object sender, EventArgs e) {
         int ID = 0;

         Port.Open("COM2");

         if(DebugPort.Locate("COM3", ref ID)) {
            DebugPort.Open(ID);
         }


         DebugPort.SetParameters(57600, Uart.Parity.None, 8, Uart.StopBit.One, Uart.Handshake.None);
         DebugPort.SetRxNowait();

         if(!Port.IsOpen || !DebugPort.IsOpen) {
            StatusLabel.Text = "Nelze otevrit";
         } else {
            StatusLabel.Text = "OK";
         }

      }

      private void timer1_Tick(object sender, EventArgs e) {
         byte Address = 0;
         byte[] Data = new byte[1];

         if(Port.Read(ref Address, ref Data)) {
            System.Diagnostics.Debug.WriteLine(Enc.GetString(Data));
         }


      }

      private void SendFrame() {
         Port.Write(4, Enc.GetBytes("ABCD"));
      }

      private void SendCharButton_Click(object sender, EventArgs e) {
         SendFrame();
      }

      private void DisconnectButton_Click(object sender, EventArgs e) {
         Port.Close();
         DebugPort.Close();
      }

      private void CleartButton_Click(object sender, EventArgs e) {
         OutputTextBox.Text = "";
      }



   }
}
