﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Device.Uart;

namespace Avr32UartTimeout {
   public partial class Form1 : Form {
            ComUart Port = new ComUart();
         ComUart DebugPort = new ComUart();
         Encoding Enc = Encoding.ASCII;

      public Form1() {
         InitializeComponent();
      }

      private void button1_Click(object sender, EventArgs e) {
         int ID = 0;

         if(Port.Locate("COM2", ref ID)) {
            Port.Open(ID);
         }

         if(DebugPort.Locate("COM3", ref ID)) {
            DebugPort.Open(ID);
         }

         Port.SetParameters(57600, Uart.Parity.None, 8, Uart.StopBit.One, Uart.Handshake.None);
         Port.SetRxNowait();

         DebugPort.SetParameters(57600, Uart.Parity.None, 8, Uart.StopBit.One, Uart.Handshake.None);
         DebugPort.SetRxNowait();

         if(!Port.IsOpen || !DebugPort.IsOpen) {
            StatusLabel.Text = "Nelze otevrit";
         } else {
            StatusLabel.Text = "OK";
         }

         TimeoutTextBox.Text = "9";
         SetTimeout();
      }

      private int Timeout;

      private void SetTimeout() {
         int Time = 0;

         if(!Int32.TryParse(TimeoutTextBox.Text, out Time)) {
            StatusLabel.Text = "Chybny format";
            return;
         }
         
         if(Time < 0) {
            Time = 0;
         } else if(Time > 9) {
            Time = 9;
         }

         Timeout = Time * 100;

         if(DebugPort.Write(Enc.GetBytes(Time.ToString()), 1) != 1) {
            StatusLabel.Text = "Nelze poslat";
            return;
         }

         StatusLabel.Text = "OK";
      }

      private void SetTimeoutButton_Click(object sender, EventArgs e) {
         SetTimeout();
      }

   private void ReportEvent(string s) {
      int Time = (int)(DateTime.Now - LastEvent).TotalMilliseconds;
      
      if(Time > Timeout) {
         OutputTextBox.Text += "* ";
      }

      OutputTextBox.Text += s + ":\t" + Time + "\r\n";

      OutputTextBox.SelectionStart = OutputTextBox.Text.Length;
      OutputTextBox.ScrollToCaret();

      LastEvent = DateTime.Now;
   }

      private DateTime LastEvent;

      private void timer1_Tick(object sender, EventArgs e) {
         byte[] Data = new byte[1];

         if(DebugPort.Read(Data, 1) == 1) {
            string s = Enc.GetString(Data);

            switch(s) {
               case "T":
                  ReportEvent("Time");
                  break;

               default:
                  ReportEvent("Rec");
                  break;
            }


         }
      }

      private void SendChar() {
         if(Port.Write(Enc.GetBytes("X"), 1) != 1) {
            StatusLabel.Text = "Nelze poslat";
            return;
         }

         StatusLabel.Text = "OK";

         ReportEvent("Send");
      }

      private void SendCharButton_Click(object sender, EventArgs e) {
         SendChar();
      }

      private void DisconnectButton_Click(object sender, EventArgs e) {
         Port.Close();
         DebugPort.Close();
      }

      private void CleartButton_Click(object sender, EventArgs e) {
         OutputTextBox.Text = "";
      }



   }
}
