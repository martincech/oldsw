﻿namespace Avr32UartTimeout {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.components = new System.ComponentModel.Container();
         this.button1 = new System.Windows.Forms.Button();
         this.StatusLabel = new System.Windows.Forms.Label();
         this.OutputTextBox = new System.Windows.Forms.TextBox();
         this.TimeoutTextBox = new System.Windows.Forms.TextBox();
         this.SetTimeoutButton = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.timer1 = new System.Windows.Forms.Timer(this.components);
         this.SendCharButton = new System.Windows.Forms.Button();
         this.label2 = new System.Windows.Forms.Label();
         this.DisconnectButton = new System.Windows.Forms.Button();
         this.CleartButton = new System.Windows.Forms.Button();
         this.SuspendLayout();
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(194, 13);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(75, 23);
         this.button1.TabIndex = 0;
         this.button1.Text = "Connect";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Click += new System.EventHandler(this.button1_Click);
         // 
         // StatusLabel
         // 
         this.StatusLabel.AutoSize = true;
         this.StatusLabel.Location = new System.Drawing.Point(12, 18);
         this.StatusLabel.Name = "StatusLabel";
         this.StatusLabel.Size = new System.Drawing.Size(13, 13);
         this.StatusLabel.TabIndex = 1;
         this.StatusLabel.Text = "?";
         // 
         // OutputTextBox
         // 
         this.OutputTextBox.Location = new System.Drawing.Point(12, 58);
         this.OutputTextBox.Multiline = true;
         this.OutputTextBox.Name = "OutputTextBox";
         this.OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.OutputTextBox.Size = new System.Drawing.Size(121, 231);
         this.OutputTextBox.TabIndex = 2;
         this.OutputTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
         // 
         // TimeoutTextBox
         // 
         this.TimeoutTextBox.Location = new System.Drawing.Point(169, 152);
         this.TimeoutTextBox.Name = "TimeoutTextBox";
         this.TimeoutTextBox.Size = new System.Drawing.Size(100, 20);
         this.TimeoutTextBox.TabIndex = 3;
         // 
         // SetTimeoutButton
         // 
         this.SetTimeoutButton.Location = new System.Drawing.Point(194, 178);
         this.SetTimeoutButton.Name = "SetTimeoutButton";
         this.SetTimeoutButton.Size = new System.Drawing.Size(75, 23);
         this.SetTimeoutButton.TabIndex = 4;
         this.SetTimeoutButton.Text = "Set timeout";
         this.SetTimeoutButton.UseVisualStyleBackColor = true;
         this.SetTimeoutButton.Click += new System.EventHandler(this.SetTimeoutButton_Click);
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point(169, 136);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(114, 13);
         this.label1.TabIndex = 5;
         this.label1.Text = "0 = 0 ms ... 9 = 900 ms";
         // 
         // timer1
         // 
         this.timer1.Enabled = true;
         this.timer1.Interval = 5;
         this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
         // 
         // SendCharButton
         // 
         this.SendCharButton.Location = new System.Drawing.Point(194, 77);
         this.SendCharButton.Name = "SendCharButton";
         this.SendCharButton.Size = new System.Drawing.Size(75, 23);
         this.SendCharButton.TabIndex = 6;
         this.SendCharButton.Text = "Send char";
         this.SendCharButton.UseVisualStyleBackColor = true;
         this.SendCharButton.Click += new System.EventHandler(this.SendCharButton_Click);
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(12, 42);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(143, 13);
         this.label2.TabIndex = 7;
         this.label2.Text = "EVENT: ms_from_last_event";
         // 
         // DisconnectButton
         // 
         this.DisconnectButton.Location = new System.Drawing.Point(194, 245);
         this.DisconnectButton.Name = "DisconnectButton";
         this.DisconnectButton.Size = new System.Drawing.Size(75, 23);
         this.DisconnectButton.TabIndex = 8;
         this.DisconnectButton.Text = "Disconnect";
         this.DisconnectButton.UseVisualStyleBackColor = true;
         this.DisconnectButton.Click += new System.EventHandler(this.DisconnectButton_Click);
         // 
         // CleartButton
         // 
         this.CleartButton.Location = new System.Drawing.Point(194, 216);
         this.CleartButton.Name = "CleartButton";
         this.CleartButton.Size = new System.Drawing.Size(75, 23);
         this.CleartButton.TabIndex = 9;
         this.CleartButton.Text = "Clear";
         this.CleartButton.UseVisualStyleBackColor = true;
         this.CleartButton.Click += new System.EventHandler(this.CleartButton_Click);
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(281, 301);
         this.Controls.Add(this.CleartButton);
         this.Controls.Add(this.DisconnectButton);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.SendCharButton);
         this.Controls.Add(this.label1);
         this.Controls.Add(this.SetTimeoutButton);
         this.Controls.Add(this.TimeoutTextBox);
         this.Controls.Add(this.OutputTextBox);
         this.Controls.Add(this.StatusLabel);
         this.Controls.Add(this.button1);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.Label StatusLabel;
      private System.Windows.Forms.TextBox OutputTextBox;
      private System.Windows.Forms.TextBox TimeoutTextBox;
      private System.Windows.Forms.Button SetTimeoutButton;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Timer timer1;
      private System.Windows.Forms.Button SendCharButton;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Button DisconnectButton;
      private System.Windows.Forms.Button CleartButton;
   }
}

