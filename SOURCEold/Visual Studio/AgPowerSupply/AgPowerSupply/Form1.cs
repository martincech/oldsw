﻿using System;
using System.Windows.Forms;
using System.Threading;
using AgilentInstruments;

namespace AgPower {

   public partial class Form1 : Form {
      protected PowerSupply E3640A;

      public Form1() {
         InitializeComponent();

         E3640A = new PowerSupply();
      }

      private void ConnectButton_Click(object sender, EventArgs e) {
         E3640A.SetCom(ComTextBox.Text);

         if(!E3640A.Control(true)) {
            SetStatus("Unable set remote");
            return;
         }

         if(!E3640A.Display("CONNECTED")) {
            SetStatus("Unable display text");
            return;
         }

         Thread.Sleep(1000);

         if(!E3640A.ClearDisplay()) {
            SetStatus("Unable clear display");
            return;
         }

         if(!E3640A.EnableOutput(false)) {
            SetStatus("Unable enable/disable output");
            return;
         }

         SetStatus("OK");
      }

      private void SetStatus(string Status) {
         StatusLabel.Text = "Status: " + Status;
      }

      private void OutputCheckBox_CheckedChanged(object sender, EventArgs e) {
         if(!E3640A.EnableOutput(OutputCheckBox.Checked)) {
            SetStatus("Unable enable/disable output");
            return;
         }

         SetStatus("OK");
      }

      private void SetVoltageButton_Click(object sender, EventArgs e) {
         double Voltage;

         if(!Double.TryParse(VoltageTextBox.Text, out Voltage)) {
            SetStatus( "Invalid voltage format");
            return;
         }

         if(!E3640A.SetVoltage(Voltage)) {
            SetStatus("Unable set voltage");
            return;
         }

         SetStatus("OK");
      }

      private void SetCurrentButton_Click(object sender, EventArgs e) {
         double Current;

         if(!Double.TryParse(CurrentTextBox.Text, out Current)) {
            SetStatus("Invalid current format");
            return;
         }

         if(!E3640A.SetCurrent(Current)) {
            SetStatus("Unable set current");
            return;
         }

         SetStatus("OK");
      }

      private void ReadButton_Click(object sender, EventArgs e) {
         double Voltage = 0, Current = 0;

         if(!E3640A.Get(ref Voltage, ref Current)) {
            SetStatus("Unable get data");
            return;
         }

         VoltageLabel.Text = Voltage.ToString();
         CurrentLabel.Text = Current.ToString();

         SetStatus("OK");
      }
   }
}
