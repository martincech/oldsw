﻿namespace AgPower {
   partial class Form1 {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing) {
         if(disposing && (components != null)) {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent() {
         this.ComTextBox = new System.Windows.Forms.TextBox();
         this.ComLabel = new System.Windows.Forms.Label();
         this.ConnectButton = new System.Windows.Forms.Button();
         this.StatusLabel = new System.Windows.Forms.Label();
         this.SetVoltageButton = new System.Windows.Forms.Button();
         this.SetCurrentButton = new System.Windows.Forms.Button();
         this.VoltageTextBox = new System.Windows.Forms.TextBox();
         this.CurrentTextBox = new System.Windows.Forms.TextBox();
         this.OutputCheckBox = new System.Windows.Forms.CheckBox();
         this.ReadButton = new System.Windows.Forms.Button();
         this.VoltageLabel = new System.Windows.Forms.Label();
         this.CurrentLabel = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // ComTextBox
         // 
         this.ComTextBox.Location = new System.Drawing.Point(82, 29);
         this.ComTextBox.Name = "ComTextBox";
         this.ComTextBox.Size = new System.Drawing.Size(100, 20);
         this.ComTextBox.TabIndex = 0;
         this.ComTextBox.Text = "COM1";
         // 
         // ComLabel
         // 
         this.ComLabel.AutoSize = true;
         this.ComLabel.Location = new System.Drawing.Point(12, 32);
         this.ComLabel.Name = "ComLabel";
         this.ComLabel.Size = new System.Drawing.Size(52, 13);
         this.ComLabel.TabIndex = 1;
         this.ComLabel.Text = "Com port:";
         // 
         // ConnectButton
         // 
         this.ConnectButton.Location = new System.Drawing.Point(82, 56);
         this.ConnectButton.Name = "ConnectButton";
         this.ConnectButton.Size = new System.Drawing.Size(75, 23);
         this.ConnectButton.TabIndex = 2;
         this.ConnectButton.Text = "Connect";
         this.ConnectButton.UseVisualStyleBackColor = true;
         this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
         // 
         // StatusLabel
         // 
         this.StatusLabel.AutoSize = true;
         this.StatusLabel.Location = new System.Drawing.Point(28, 237);
         this.StatusLabel.Name = "StatusLabel";
         this.StatusLabel.Size = new System.Drawing.Size(49, 13);
         this.StatusLabel.TabIndex = 3;
         this.StatusLabel.Text = "Status: ?";
         // 
         // SetVoltageButton
         // 
         this.SetVoltageButton.Location = new System.Drawing.Point(171, 98);
         this.SetVoltageButton.Name = "SetVoltageButton";
         this.SetVoltageButton.Size = new System.Drawing.Size(75, 23);
         this.SetVoltageButton.TabIndex = 4;
         this.SetVoltageButton.Text = "Set voltage";
         this.SetVoltageButton.UseVisualStyleBackColor = true;
         this.SetVoltageButton.Click += new System.EventHandler(this.SetVoltageButton_Click);
         // 
         // SetCurrentButton
         // 
         this.SetCurrentButton.Location = new System.Drawing.Point(171, 127);
         this.SetCurrentButton.Name = "SetCurrentButton";
         this.SetCurrentButton.Size = new System.Drawing.Size(75, 23);
         this.SetCurrentButton.TabIndex = 5;
         this.SetCurrentButton.Text = "Set current";
         this.SetCurrentButton.UseVisualStyleBackColor = true;
         this.SetCurrentButton.Click += new System.EventHandler(this.SetCurrentButton_Click);
         // 
         // VoltageTextBox
         // 
         this.VoltageTextBox.Location = new System.Drawing.Point(57, 100);
         this.VoltageTextBox.Name = "VoltageTextBox";
         this.VoltageTextBox.Size = new System.Drawing.Size(100, 20);
         this.VoltageTextBox.TabIndex = 6;
         // 
         // CurrentTextBox
         // 
         this.CurrentTextBox.Location = new System.Drawing.Point(57, 126);
         this.CurrentTextBox.Name = "CurrentTextBox";
         this.CurrentTextBox.Size = new System.Drawing.Size(100, 20);
         this.CurrentTextBox.TabIndex = 7;
         // 
         // OutputCheckBox
         // 
         this.OutputCheckBox.AutoSize = true;
         this.OutputCheckBox.Location = new System.Drawing.Point(180, 60);
         this.OutputCheckBox.Name = "OutputCheckBox";
         this.OutputCheckBox.Size = new System.Drawing.Size(92, 17);
         this.OutputCheckBox.TabIndex = 8;
         this.OutputCheckBox.Text = "Enable output";
         this.OutputCheckBox.UseVisualStyleBackColor = true;
         this.OutputCheckBox.CheckedChanged += new System.EventHandler(this.OutputCheckBox_CheckedChanged);
         // 
         // ReadButton
         // 
         this.ReadButton.Location = new System.Drawing.Point(171, 170);
         this.ReadButton.Name = "ReadButton";
         this.ReadButton.Size = new System.Drawing.Size(75, 23);
         this.ReadButton.TabIndex = 9;
         this.ReadButton.Text = "Read";
         this.ReadButton.UseVisualStyleBackColor = true;
         this.ReadButton.Click += new System.EventHandler(this.ReadButton_Click);
         // 
         // VoltageLabel
         // 
         this.VoltageLabel.AutoSize = true;
         this.VoltageLabel.Location = new System.Drawing.Point(101, 175);
         this.VoltageLabel.Name = "VoltageLabel";
         this.VoltageLabel.Size = new System.Drawing.Size(23, 13);
         this.VoltageLabel.TabIndex = 10;
         this.VoltageLabel.Text = "? V";
         // 
         // CurrentLabel
         // 
         this.CurrentLabel.AutoSize = true;
         this.CurrentLabel.Location = new System.Drawing.Point(101, 193);
         this.CurrentLabel.Name = "CurrentLabel";
         this.CurrentLabel.Size = new System.Drawing.Size(23, 13);
         this.CurrentLabel.TabIndex = 11;
         this.CurrentLabel.Text = "? A";
         // 
         // Form1
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(284, 262);
         this.Controls.Add(this.CurrentLabel);
         this.Controls.Add(this.VoltageLabel);
         this.Controls.Add(this.ReadButton);
         this.Controls.Add(this.OutputCheckBox);
         this.Controls.Add(this.CurrentTextBox);
         this.Controls.Add(this.VoltageTextBox);
         this.Controls.Add(this.SetCurrentButton);
         this.Controls.Add(this.SetVoltageButton);
         this.Controls.Add(this.StatusLabel);
         this.Controls.Add(this.ConnectButton);
         this.Controls.Add(this.ComLabel);
         this.Controls.Add(this.ComTextBox);
         this.Name = "Form1";
         this.Text = "Form1";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.TextBox ComTextBox;
      private System.Windows.Forms.Label ComLabel;
      private System.Windows.Forms.Button ConnectButton;
      private System.Windows.Forms.Label StatusLabel;
      private System.Windows.Forms.Button SetVoltageButton;
      private System.Windows.Forms.Button SetCurrentButton;
      private System.Windows.Forms.TextBox VoltageTextBox;
      private System.Windows.Forms.TextBox CurrentTextBox;
      private System.Windows.Forms.CheckBox OutputCheckBox;
      private System.Windows.Forms.Button ReadButton;
      private System.Windows.Forms.Label VoltageLabel;
      private System.Windows.Forms.Label CurrentLabel;
   }
}

