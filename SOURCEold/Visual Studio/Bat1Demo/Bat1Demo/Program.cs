﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Veit.Bat1;

namespace Bat1CsDemo
{
    class Program
    {

//-----------------------------------------------------------------------------
//   Load
//-----------------------------------------------------------------------------

        static private bool Load() {
            bool PowerOn;
            if(!Veit.Bat1.Dll.DeviceIsOn( out PowerOn ))
            {
                Console.WriteLine( "ERROR : unable get status" );
                return( false);
            }
            if( PowerOn)
            {
                if(!Veit.Bat1.Dll.DevicePowerOff())
                {
                    Console.WriteLine( "ERROR : unable switch power off" );
                    return( false);
                }
            }
            if(!Veit.Bat1.Dll.LoadDevice())
            {
                Console.WriteLine( "ERROR : unable load" );
                return( false);
            }
            Console.WriteLine( "Device version : {0:X}", Veit.Bat1.Dll.GetDeviceVersion() );
            Console.WriteLine( "Device loaded..." );
            return ( true );
        } // Load

//-----------------------------------------------------------------------------
//   Main
//-----------------------------------------------------------------------------

        static void Main(string[] args)
        {
            int Day, Month, Year, Hour, Min, Sec;
            string FileName = "Test.bat1";
            ConsoleKeyInfo ch;
            Console.WriteLine("Start...");

            Veit.Bat1.Dll.EnableLogger( true);       // show communication
//            Veit.Bat1.Dll.EnableLogger(false);       // show communication

            while ( true)
            {
                ch = Console.ReadKey(true);
                switch (ch.Key)
                {
                    case ConsoleKey.C :
                        if( Veit.Bat1.Dll.CheckDevice()){
                            Console.WriteLine( "Device ready");
                        } else {
                            Console.WriteLine( "Device not present");
                        }
                        break;

                    case ConsoleKey.H :
                        while (true) {
                            if( !Veit.Bat1.Dll.CheckDevice()){
                                Console.WriteLine( "Device not present" );
                                Console.WriteLine( "STOPPED by error" );
                                break;
                            }
                        }
                        break;

                    case ConsoleKey.L :
                        Load();
                        break;

                    case ConsoleKey.K :
                        while (true) {
                            if( !Load()){
                                Console.WriteLine( "STOPPED by error" );
                                break;
                            }
                        }
                        break;

                    case ConsoleKey.P :
                        while (true) {
                            if( !Load()){
                                Console.WriteLine( "STOPPED by error" );
                                break;
                            }
                            if( !FillConfig()){
                                Console.WriteLine( "STOPPED by error" );
                                break;
                            }
                        }
                        break;
                    
                    case ConsoleKey.S :
                        // if You will create device from scratch, call :
                        // Veit.Bat1.Dll.NewDevice(); // default config, no Files, no Groups

                        // modify Config :
                        Veit.Bat1.Dll.SetScaleName( "BAT#" );
                        Veit.Bat1.Dll.SetSavingMode( (int)Veit.Bat1.SavingMode.MANUAL_BY_SEX );

                        // create File :
                        int FIndex = Veit.Bat1.Dll.FileCreate();

                        Veit.Bat1.Dll.SetFileName( FIndex, "MYFILE" );       // directory name
                        Veit.Bat1.Dll.SetFileNote( FIndex, "MYFILE NOTE" );  // directory note
                        Veit.Bat1.Dll.SetCurrentFile( FIndex );              // set as working file

                        Veit.Bat1.Dll.SetFileNumberOfBirds( FIndex, 5 );                                    // fill file config birds
                        Veit.Bat1.Dll.SetFileLowLimit(  FIndex, Veit.Bat1.Dll.EncodeWeight( 0.8) );              // fill file config limit
                        Veit.Bat1.Dll.SetFileHighLimit( FIndex, Veit.Bat1.Dll.EncodeWeight( 1.1 ) );             // fill file config limit
                        Veit.Bat1.Dll.SetFileWeightSortingMode( FIndex, (int)Veit.Bat1.WeightSorting.LIGHT_OK_HEAVY); // fill file weight sorting mode

                        Veit.Bat1.Dll.FileAllocSamples( FIndex, 2 );         // alloc samples list
                        int SIndex = 0;    // fill first sample
                        Veit.Bat1.Dll.SetSampleTimestamp( FIndex, SIndex, Veit.Bat1.Dll.EncodeTime( 1, 1, 2008, 10, 11, 12));
                        Veit.Bat1.Dll.SetSampleWeight(    FIndex, SIndex, Veit.Bat1.Dll.EncodeWeight( 1.5));
                        Veit.Bat1.Dll.SetSampleFlag(      FIndex, SIndex, (int)Veit.Bat1.SampleFlag.FEMALE );
                        SIndex++;          // fill second sample
                        Veit.Bat1.Dll.SetSampleTimestamp( FIndex, SIndex, Veit.Bat1.Dll.EncodeTime( 1, 1, 2008, 10, 11, 13 ) );
                        Veit.Bat1.Dll.SetSampleWeight(    FIndex, SIndex, Veit.Bat1.Dll.EncodeWeight( 1.6 ) );
                        Veit.Bat1.Dll.SetSampleFlag(      FIndex, SIndex, (int)Veit.Bat1.SampleFlag.MALE );

                        // create Group :
                        int GIndex = Veit.Bat1.Dll.GroupCreate();
                        Veit.Bat1.Dll.SetGroupName( GIndex, "MYGROUP" );     // directory name
                        Veit.Bat1.Dll.SetGroupNote( GIndex, "MYGROUP NOTE" );// directory note
                        Veit.Bat1.Dll.AddGroupFile( GIndex, 0 );             // add file index 0 to set
                        Veit.Bat1.Dll.AddGroupFile( GIndex, 1 );             // add file index 1 to set
                        Veit.Bat1.Dll.AddGroupFile( GIndex, 2 );             // add file index 2 to set
                        // save data to device :
                        if(!Veit.Bat1.Dll.SaveDevice())
                        {
                            Console.WriteLine( "ERROR : unable save" );
                            break;
                        }
                        Console.WriteLine( "Device saved..." );
                        break;

                    case ConsoleKey.F :
                        FillConfig();
                        break;

                    case ConsoleKey.R:
                        int Size = Veit.Bat1.Dll.GetEepromSize();
                        byte[] Buffer = new byte[Size];
                        if(!Veit.Bat1.Dll.ReadEeprom( 0, Buffer, Size ))
                        {
                            Console.WriteLine( "ERROR : unable read EEPROM" );
                            break;
                        }
                        FileStream ofs = File.Create( FileName );
                        ofs.Write( Buffer, 0, Size );
                        ofs.Close();
                        Console.WriteLine( "Read done..." );
                        break;

                    case ConsoleKey.U:
                        int USize = Veit.Bat1.Dll.GetEepromSize();
                        byte[] UBuffer = new byte[USize];
                        if(!File.Exists( FileName ))
                        {
                            Console.WriteLine( "ERROR : unable find file" );
                            break;
                        }
                        FileStream ifs = File.OpenRead( FileName );
                        ifs.Read( UBuffer, 0, USize );
                        ifs.Close();
                        if(!Veit.Bat1.Dll.DeviceByEeprom( UBuffer ))
                        {
                            Console.WriteLine( "ERROR : unable load device" );
                            break;
                        }
                        Console.WriteLine( "Device version : {0:X}", Veit.Bat1.Dll.GetDeviceVersion() );
                        StringBuilder ScaleName = new StringBuilder( 80);
                        Veit.Bat1.Dll.GetScaleName( ScaleName);
                        Console.WriteLine( "Device name    : {0}", ScaleName );
                        Console.WriteLine( "Device loaded..." );
                        break;

                    case ConsoleKey.D:
                        int LogoSize = Veit.Bat1.Dll.GetLogoSize();
                        byte[] Logo = new byte[LogoSize];
                        if(!Veit.Bat1.Dll.ReadEeprom( Veit.Bat1.Dll.GetLogoAddress(), Logo, LogoSize ))
                        {
                            Console.WriteLine( "ERROR : unable read EEPROM" );
                            break;
                        }
                        Console.WriteLine( "Logo loaded..." );
                        break;

                    case ConsoleKey.V:
                        Console.WriteLine( "Dll version : {0:X}", Veit.Bat1.Dll.GetDllVersion() );
                        Console.WriteLine( "USB support : {0}", Veit.Bat1.Dll.IsUsbLess() ? "NO" : "YES" );
                        break;

                    case ConsoleKey.T:
                        int Clock;
                        if(!Veit.Bat1.Dll.GetTime( out Clock )){
                            Console.WriteLine( "ERROR : unable get time" );
                            break;
                        }
                        Veit.Bat1.Dll.DecodeTime( Clock, out Day, out Month, out Year, out Hour, out Min, out Sec );
                        Console.WriteLine( "Time : {0}.{1}.{2} {3}:{4}:{5}", Day, Month, Year, Hour, Min, Sec );
                        break;

                    case ConsoleKey.Y:
                        FillSamples();
                        break;

                    case ConsoleKey.E:
                        int Promile;
                        if( !Veit.Bat1.Dll.LoadEstimation( out Promile )){
                            Console.WriteLine( "Unable get estimation" );
                            break;
                        }
                        Console.WriteLine( "Load size : {0}%%", Promile );
                        Veit.Bat1.Dll.SaveEstimation( out Promile );
                        Console.WriteLine( "Save size : {0}%%", Promile );
                        break;

                    case ConsoleKey.O:
                        if(!Veit.Bat1.Dll.LoadConfiguration())
                        {
                            Console.WriteLine( "Unable load" );
                            break;
                        }
                        Console.WriteLine( "Device version : {0:X}", Veit.Bat1.Dll.GetDeviceVersion() );
                        StringBuilder ScName = new StringBuilder( 80 );
                        Veit.Bat1.Dll.GetScaleName( ScName );
                        Console.WriteLine( "Device name    : {0}", ScName );
                        break;

                    case ConsoleKey.A:
                        Console.WriteLine( "Country  : {0}", Veit.Bat1.Locales.GetCountryName( (int)Veit.Bat1.Country.CZECH ) );
                        Console.WriteLine( "Language : {0}", Veit.Bat1.Locales.GetLanguage((int)Veit.Bat1.Country.CZECH));
                        Console.WriteLine( "CodePage CZ : {0}", Veit.Bat1.Locales.GetCodePage((int)Veit.Bat1.Language.CZECH));
                        Console.WriteLine( "CodePage JP : {0}", Veit.Bat1.Locales.GetCodePage((int)Veit.Bat1.Language.JAPANESE));
                        Console.WriteLine( "Date fmt : {0}", Veit.Bat1.Locales.GetDateFormat((int)Veit.Bat1.Country.CZECH));
                        Console.WriteLine( "Date s1  : {0}", Veit.Bat1.Locales.GetDateSeparator1( (int)Veit.Bat1.Country.CZECH ) );
                        Console.WriteLine( "Date s2  : {0}", Veit.Bat1.Locales.GetDateSeparator2( (int)Veit.Bat1.Country.CZECH ) );
                        Console.WriteLine( "Time fmt : {0}", Veit.Bat1.Locales.GetTimeFormat( (int)Veit.Bat1.Country.CZECH ) );
                        Console.WriteLine( "Time s   : {0}", Veit.Bat1.Locales.GetTimeSeparator( (int)Veit.Bat1.Country.CZECH ) );
                        Console.WriteLine( "DST      : {0}", Veit.Bat1.Locales.GetDaylightSavingType( (int)Veit.Bat1.Country.CZECH ) );
                        break;

                    case ConsoleKey.Z:
                        Veit.Bat1.Dll.NewDevice(); // default config, no Files, no Groups
                        Console.WriteLine( "Stab.range : {0}", Veit.Bat1.Dll.GetStabilisationRange() );
                        break;

                    case ConsoleKey.W:
                        if( !Veit.Bat1.Dll.LoadCrashInfo())
                        {
                            Console.WriteLine( "Unable load info" );
                            break;
                        }
                        Veit.Bat1.Dll.DecodeTime( Veit.Bat1.Dll.GetExceptionTimestamp(), out Day, out Month, out Year, out Hour, out Min, out Sec );
                        Console.WriteLine( "Exception Time : {0}.{1}.{2} {3}:{4}:{5}", Day, Month, Year, Hour, Min, Sec );
                        Console.WriteLine( "Exception Address : {0}", Veit.Bat1.Dll.GetExceptionAddress() );
                        Console.WriteLine( "Exception Type    : {0}", Veit.Bat1.Dll.GetExceptionType() );
                        Console.WriteLine( "Exception Status  : {0}", Veit.Bat1.Dll.GetExceptionStatus() );
                        Veit.Bat1.Dll.DecodeTime( Veit.Bat1.Dll.GetWatchDogTimestamp(), out Day, out Month, out Year, out Hour, out Min, out Sec );
                        Console.WriteLine( "WatchDog  Time : {0}.{1}.{2} {3}:{4}:{5}", Day, Month, Year, Hour, Min, Sec );
                        Console.WriteLine( "WatchDog  Status  : {0}", Veit.Bat1.Dll.GetWatchDogStatus() );
                        break;

                    case ConsoleKey.X :
                        return;

                    default:
                        Console.WriteLine( "C - Check" );
                        Console.WriteLine( "H - Cyclic check" );
                        Console.WriteLine( "L - Load device" );
                        Console.WriteLine( "K - Cyclic Load device" );
                        Console.WriteLine( "P - Cyclic Load + Write full config" );
                        Console.WriteLine( "S - Save device" );
                        Console.WriteLine( "R - Read EEPROM" );
                        Console.WriteLine( "U - Upload File to device" );
                        Console.WriteLine( "D - Download logo" );
                        Console.WriteLine( "F - Full files and groups" );
                        Console.WriteLine( "T - Time" );
                        Console.WriteLine( "Y - Fill one file" );
                        Console.WriteLine( "V - Version" );
                        Console.WriteLine( "E - Size estimation" );
                        Console.WriteLine( "O - Load configuration" );
                        Console.WriteLine( "A - Get locale" );
                        Console.WriteLine( "W - Crash status" );
                        Console.WriteLine( "Z - test" );
                        Console.WriteLine( "X - Exit" );
                        break;
                }
            }
        } // Main
//-----------------------------------------------------------------------------
//   Create file
//-----------------------------------------------------------------------------

        private static void CreateFile(string name) {
            int FIndex = Veit.Bat1.Dll.FileCreate();

            Veit.Bat1.Dll.SetFileName( FIndex, name );       // directory name
            Veit.Bat1.Dll.SetFileNote( FIndex, "MYFILE NOTE" );  // directory note
//            Veit.Bat1.Dll.SetCurrentFile( FIndex );              // set as working file

            Veit.Bat1.Dll.SetFileNumberOfBirds( FIndex, 5 );                                    // fill file config birds
            Veit.Bat1.Dll.SetFileLowLimit(  FIndex, Veit.Bat1.Dll.EncodeWeight( 0.8) );              // fill file config limit
            Veit.Bat1.Dll.SetFileHighLimit( FIndex, Veit.Bat1.Dll.EncodeWeight( 1.1 ) );             // fill file config limit
            Veit.Bat1.Dll.SetFileWeightSortingMode( FIndex, (int)Veit.Bat1.WeightSorting.LIGHT_OK_HEAVY); // fill file weight sorting mode

/*            Veit.Bat1.Dll.FileAllocSamples( FIndex, 2 );         // alloc samples list
            int SIndex = 0;    // fill first sample
            Veit.Bat1.Dll.SetSampleTimestamp( FIndex, SIndex, Veit.Bat1.Dll.EncodeTime( 1, 1, 2008, 10, 11, 12));
            Veit.Bat1.Dll.SetSampleWeight(    FIndex, SIndex, Veit.Bat1.Dll.EncodeWeight( 1.5));
            Veit.Bat1.Dll.SetSampleFlag(      FIndex, SIndex, (int)Veit.Bat1.SampleFlag.FEMALE );
            SIndex++;          // fill second sample
            Veit.Bat1.Dll.SetSampleTimestamp( FIndex, SIndex, Veit.Bat1.Dll.EncodeTime( 1, 1, 2008, 10, 11, 13 ) );
            Veit.Bat1.Dll.SetSampleWeight(    FIndex, SIndex, Veit.Bat1.Dll.EncodeWeight( 1.6 ) );
            Veit.Bat1.Dll.SetSampleFlag(      FIndex, SIndex, (int)Veit.Bat1.SampleFlag.MALE );*/
        } // CreateFile

//-----------------------------------------------------------------------------
//   Create groups
//-----------------------------------------------------------------------------

        private static void CreateGroups() {
            string name = "AAAAAAAAAAAAAAA";
            for (int i = 0; i < 1; i++) {
                // Vytvorim skupinu
                int index = Veit.Bat1.Dll.GroupCreate();
                Veit.Bat1.Dll.SetGroupName(index, name);
                Veit.Bat1.Dll.SetGroupNote(index, "MYGROUP NOTE" );

                // Oznacim nejake soubory
                for (int j = 0; j <= i; j++) {
                    Veit.Bat1.Dll.AddGroupFile(index, j );
                }
                
                // Zmenim jmeno AAAAA -> BBBBB atd.
                char[] array = name.ToCharArray();
                for (int j = 0; j < array.Length; j++) {
                    array[j]++;
                }
                name = new string(array);
            }
        } // CreateGroups

//-----------------------------------------------------------------------------
//   Create demo data
//-----------------------------------------------------------------------------

        private static bool FillConfig() {
            // if You will create device from scratch, call :
             Veit.Bat1.Dll.NewDevice(); // default config, no Files, no Groups

            // modify Config :
            Veit.Bat1.Dll.SetScaleName( "BAT#" );
            Veit.Bat1.Dll.SetSavingMode( (int)Veit.Bat1.SavingMode.MANUAL_BY_SEX );

            // create File :
            for(int i = 0; i < Veit.Bat1.Const.DIRECTORY_SIZE - 1; i++)
            {
                CreateFile( "F" + i.ToString() );
            }
            // create Group :
            CreateGroups();

            bool PowerOn;
            if(!Veit.Bat1.Dll.DeviceIsOn( out PowerOn ))
            {
                Console.WriteLine( "ERROR : unable get status" );
                return( false);
            }
            if(PowerOn)
            {
                if(!Veit.Bat1.Dll.DevicePowerOff())
                {
                    Console.WriteLine( "ERROR : unable switch power off" );
                    return( false);
                }
            }

            // save data to device :
            if(!Veit.Bat1.Dll.SaveDevice())
            {
                Console.WriteLine( "ERROR : unable save" );
                return( false);
            }
            Console.WriteLine( "Device saved..." );
            return ( true );
        } // FillConfig

//-----------------------------------------------------------------------------
//   Create file samples
//-----------------------------------------------------------------------------

        public const int FILE_SAMPLES = 11300;
//        public const int FILE_SAMPLES = 10;

        private static void FillSamples()
        {
            // Switch off power :
            bool PowerOn;
            if( !Veit.Bat1.Dll.DeviceIsOn( out PowerOn ))
            {
                Console.WriteLine( "ERROR : unable get status" );
                return;
            }
            if( PowerOn)
            {
                if( !Veit.Bat1.Dll.DevicePowerOff())
                {
                    Console.WriteLine( "ERROR : unable switch power off" );
                    return;
                }
            }
            // Load device :
            if(!Veit.Bat1.Dll.LoadDevice())
            {
                Console.WriteLine( "ERROR : unable load" );
                return;
            }
            Console.WriteLine( "Device loaded..." );
            // remove all files :
            Veit.Bat1.Dll.FilesDeleteAll();
            // create file :
            int FIndex = Veit.Bat1.Dll.FileCreate();
            Veit.Bat1.Dll.SetFileName( FIndex, "MYFILE" );       // directory name
            Veit.Bat1.Dll.SetFileNote( FIndex, "MYFILE NOTE" );  // directory note
            Veit.Bat1.Dll.SetCurrentFile( FIndex );              // set as working file
            // fill with samples :
            Veit.Bat1.Dll.FileAllocSamples( FIndex, FILE_SAMPLES);         // alloc samples list
            for( int i = 0; i < FILE_SAMPLES; i++)
            {
                Veit.Bat1.Dll.SetSampleTimestamp( FIndex, i, Veit.Bat1.Dll.EncodeTime( 1, 1, 2008, 10, i / 60, i % 60) );
                Veit.Bat1.Dll.SetSampleWeight( FIndex, i, Veit.Bat1.Dll.EncodeWeight( 1.5 ) );
                Veit.Bat1.Dll.SetSampleFlag( FIndex, i, (int)Veit.Bat1.SampleFlag.FEMALE );
            }
            // Save data to device :
            if(!Veit.Bat1.Dll.SaveDevice())
            {
                Console.WriteLine( "ERROR : unable save" );
                return;
            }
            Console.WriteLine( "Device saved..." );
        } // FillSamples

    } // class
} // namespace
