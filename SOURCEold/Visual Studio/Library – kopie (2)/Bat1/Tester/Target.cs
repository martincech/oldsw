﻿//******************************************************************************
//
//   Target.cs     Target
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Uart;
using System.Globalization;
using Device.Ftxx;
using System.Threading;

namespace Bat1.Tester {

   /// <summary>
   /// Target. Test firmware must be loaded in target in order to use this class.</summary>

   public class Target {
      private Config.Target Config;
      public TTest Test;
      public TUsb Usb;

      public Target(Config.Target _Config) {
         Config = _Config;

         Test = new TTest(Config.ComPortSettings);
         Usb = new TUsb(Config.ComUsbSettings, Config.Usb);
      }


      // DETECT - pokud bude prováděna detekce blank nebo programmed device bez seriového čísla tak ověřit, že není připojena další váha
      // pokud by se mělo provádět ruční testování během automatického tak váha ručně testovaná bude mít seriové číslo 0
      public class TUsb {
         private Config.Usb UsbConfig;
         private Device.Uart.Config.UsbSNUartSettings ComSettings;

         private Ft232 FtdiDevice = new Ft232();

         private UsbUart Com = new UsbUart();

         /// <summary>
         /// IsOpen</summary>

         public bool IsOpen {
            get {
               return FtdiDevice.IsOpen;
            }
         }

         /// <summary>
         /// Default constructor</summary>
         /// <param name="Configuration">Module configuration</param>

         public TUsb(Device.Uart.Config.UsbSNUartSettings _ComSettings, Config.Usb _UsbConfig) {
            ComSettings = _ComSettings;
            UsbConfig = _UsbConfig;
         }

         /// <summary>
         /// Uart object</summary>

         public UsbUart Uart {
            get {
               return Com;
            }
         }

         /// <summary>
         /// Open UART over USB</summary>

         public bool Open() {
            if(FtdiDevice.IsOpen) {
               return false; // ftdidevice open - user must close it
            }

            int Identifier = 0;

            if(ComSettings.SerialNumber != 0) {
               if(!Com.Locate(ComSettings.DeviceName, ComSettings.SerialNumber.ToString(), ref Identifier)) {
                  return false;
               }
            } else {
               if(!Com.Locate(ComSettings.DeviceName, ref Identifier)) {
                  return false;
               }
            }

            if(!Com.Open(Identifier)) {
               return false;
            }

            if(!Com.SetParameters(ComSettings.BaudRate, ComSettings.Parity, ComSettings.Bits, ComSettings.StopBit, ComSettings.Handshake)) {
               return false;
            }

            Com.SetRxWait(UsbConfig.RxTimeout);
            Com.Rts = false;
            Com.Dtr = false;

            return true;
         }

         /// <summary>
         /// Test UART over USB</summary>

         public bool TestUart() {
            Com.Flush();

            byte[] Data = new byte[1];

            // prepare data :
            for(int i = 0; i < UsbConfig.TestDataSize; i++) {
               Data[0] = (byte)i;

               // send data :
               if(Com.Write(Data, 1) != 1) {
                  return (false);
               }

               // receive reply :
               if(Com.Read(Data, 1) != 1) {
                  return (false);
               }

               // check
               if(Data[0] != (byte)i) {
                  return (false);
               }
            }

            return true;
         }

         /// <summary>
         /// Close UART over USB</summary>

         public void Close() {
            Com.Close();
         }

         private int DeviceDetectedIndex = -1;

         /// <summary>
         /// Detect programmed or blank device</summary>
         /// <returns>
         /// True if device detected, false if timeout</returns>

         public bool DetectDevice() {
            return (Wait(true, true, UsbConfig.ConnectDelay));
         }

         /// <summary>
         /// Detect programmed device</summary>
         /// <returns>
         /// True if device detected, false if timeout</returns>

         public bool DetectProgrammedDevice() {
            return (Wait(false, true, UsbConfig.ConnectDelay));
         }

         public bool SetSerialNumber(int SerialNumber) {
            if(Com.IsOpen) {
               return false; // Uart is open, user must close it
            }

            if(DeviceDetectedIndex == -1) {
               return false;
            }
            try {
               try {
                  if(FtdiDevice.OpenByIndex((uint)DeviceDetectedIndex) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

                  if(!FtdiDevice.IsOpen) {
                     throw new Exception();
                  }

                  Ft232.EEPROM_STRUCTURE PgmData = new Ft232.EEPROM_STRUCTURE();

                  if(FtdiDevice.ReadEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

                  PgmData.SerialNumber = SerialNumber.ToString();
                  PgmData.SerNumEnable = true;

                  if(FtdiDevice.WriteEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

               } finally {
                  FtdiDevice.Close();
               }
            } catch(Exception) {
               return false;
            }

            return true;
         }

         public bool ClearSerialNumber() {
            if(Com.IsOpen) {
               return false; // Uart is open, user must close it
            }

            if(DeviceDetectedIndex == -1) {
               return false;
            }
            try {
               try {
                  if(FtdiDevice.OpenByIndex((uint)DeviceDetectedIndex) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

                  if(!FtdiDevice.IsOpen) {
                     throw new Exception();
                  }

                  Ft232.EEPROM_STRUCTURE PgmData = new Ft232.EEPROM_STRUCTURE();

                  if(FtdiDevice.ReadEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

                  PgmData.SerialNumber = "00000000";
                  PgmData.SerNumEnable = false;

                  if(FtdiDevice.WriteEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

               } finally {
                  FtdiDevice.Close();
               }
            } catch(Exception) {
               return false;
            }

            return true;
         }

         /// <summary>
         /// Write FTDI EEPROM</summary>

         public bool WriteEeprom() {
            if(Com.IsOpen) {
               return false; // Uart is open, user must close it
            }

            if(DeviceDetectedIndex == -1) {
               return false;
            }

            if(UsbConfig.Ftdi.DeviceName.Length > 63) {
               return false;
            }
            if(UsbConfig.Ftdi.ManufacturerName.Length > 31) {
               return false;
            }

            try {
               try {
                  if(FtdiDevice.OpenByIndex((uint)DeviceDetectedIndex) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

                  if(!FtdiDevice.IsOpen) {
                     throw new Exception();
                  }

                  Ft232.EEPROM_STRUCTURE PgmData = new Ft232.EEPROM_STRUCTURE();

                  if(FtdiDevice.ReadEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

                  PgmData.Cbus0 = 3;
                  PgmData.Cbus1 = 2;
                  PgmData.Cbus2 = 0;
                  PgmData.Cbus3 = 1;
                  PgmData.Cbus4 = 5;
                  PgmData.Description = UsbConfig.Ftdi.DeviceName;
                  PgmData.EndpointSize = 64;
                  PgmData.HighDriveIOs = false;
                  PgmData.InvertCTS = false;
                  PgmData.InvertDCD = false;
                  PgmData.InvertDSR = false;
                  PgmData.InvertDTR = false;
                  PgmData.InvertRI = false;
                  PgmData.InvertRTS = false;
                  PgmData.InvertRXD = false;
                  PgmData.InvertTXD = false;
                  PgmData.Manufacturer = UsbConfig.Ftdi.ManufacturerName;
                  PgmData.ManufacturerID = "00";
                  PgmData.MaxPower = UsbConfig.Ftdi.MaxPower;
                  PgmData.ProductID = 0x6001;
                  PgmData.PullDownEnable = false;
                  PgmData.RemoteWakeup = false;
                  PgmData.RIsD2XX = true;
                  PgmData.SelfPowered = false;

                  if(ComSettings.SerialNumber == 0) {
                     PgmData.SerialNumber = "00000000";
                     PgmData.SerNumEnable = false;
                  }

                  PgmData.UseExtOsc = false;
                  PgmData.VendorID = 0x0403;




                  /*
                  PgmData.Signature1     = 0x00000000;
                  PgmData.Signature2     = 0xffffffff;
                  PgmData.Version        = 2;                  // FT232R extensions
                  PgmData.Manufacturer   = ManufacturerBuf;
                  PgmData.ManufacturerId = ManufacturerIdBuf;
                  PgmData.Description    = DescriptionBuf;
                  PgmData.SerialNumber   = SerialNumberBuf;
                  // read EEPROM :
                  if_fterr( FT_EE_Read( FHandle, &PgmData)){
                     FT_Close( FHandle);
                     FHandle     = INVALID_HANDLE_VALUE;
                     Status      = "FTDI read error";
                     AdapterName = "?";
                     return( false);
                  }
                  // new values :
                  PgmData.Signature1   = 0x00000000;
                  PgmData.Signature2   = 0xffffffff;
                  PgmData.Version      = 2;			// FT232R extensions
                  PgmData.VendorId     = 0x0403;
                  PgmData.ProductId    = 0x6001;
                  strcpy( PgmData.Manufacturer,   Manufacturer);
                  strcpy( PgmData.ManufacturerId, "00");
                  strcpy( PgmData.Description,    Device);
                  strcpy( PgmData.SerialNumber,   "00000001");
                  PgmData.MaxPower     = 0;
                  PgmData.PnP          = 0;
                  PgmData.SelfPowered  = 1;
                  PgmData.RemoteWakeup = 0;
                  // revision 4
                  PgmData.Rev4              = 0;
                  PgmData.IsoIn             = 0;
                  PgmData.IsoOut            = 0;
                  PgmData.PullDownEnable    = 0;
                  PgmData.SerNumEnable      = 0;
                  PgmData.USBVersionEnable  = 0;
                  PgmData.USBVersion        = 0x0110;
                  // FT2232C extensions
                  PgmData.Rev5              = 0;
                  PgmData.IsoInA            = 0;
                  PgmData.IsoInB            = 0;
                  PgmData.IsoOutA           = 0;
                  PgmData.IsoOutB           = 0;
                  PgmData.PullDownEnable5   = 0;
                  PgmData.SerNumEnable5     = 0;
                  PgmData.USBVersionEnable5 = 0;
                  PgmData.USBVersion5       = 0x0110;
                  PgmData.AIsHighCurrent    = 0;
                  PgmData.BIsHighCurrent    = 0;
                  PgmData.IFAIsFifo         = 0;
                  PgmData.IFAIsFifoTar      = 0;
                  PgmData.IFAIsFastSer      = 0;
                  PgmData.AIsVCP            = 0;
                  PgmData.IFBIsFifo         = 0;
                  PgmData.IFBIsFifoTar      = 0;
                  PgmData.IFBIsFastSer      = 0;
                  PgmData.BIsVCP            = 0;
                  // FT232R extensions
                  PgmData.UseExtOsc         = 0;
                  PgmData.HighDriveIOs      = 0;
                  PgmData.EndpointSize      = 64;
                  PgmData.PullDownEnableR   = 0;
                  PgmData.SerNumEnableR     = 1;
                  PgmData.InvertTXD         = 0;
                  PgmData.InvertRXD         = 0;
                  PgmData.InvertRTS         = 0;
                  PgmData.InvertCTS         = 0;
                  PgmData.InvertDTR         = 0;
                  PgmData.InvertDSR         = 0;
                  PgmData.InvertDCD         = 0;
                  PgmData.InvertRI          = 0;
                  PgmData.Cbus0             = 3;
                  PgmData.Cbus1             = 2;
                  PgmData.Cbus2             = 0;
                  PgmData.Cbus3             = 1;
                  PgmData.Cbus4             = 5;
                  PgmData.RIsD2XX           = 1;*/



                  if(FtdiDevice.WriteEEPROM(PgmData) != Ftdi.FT_STATUS.FT_OK) {
                     throw new Exception();
                  }

               } finally {
                  FtdiDevice.Close();
               }
            } catch(Exception) {
               return false;
            }

            return true;
         }

         /// <summary>
         /// Wait for device</summary>
         /// True - device connected, false - timeout expires</returns>

         private bool Wait(bool NewDevice, bool ProgrammedDevice, int Timeout) {
            for(int i = 0; i < Timeout; i++) {

               if(Detect(NewDevice, ProgrammedDevice)) {
                  return (true);
               }

               Thread.Sleep(1000);
            }

            return (false);
         }

         /// <summary>
         /// Wait untill device disconnects</summary>
         /// <returns>
         /// True - no device connected, false - timeout expires</returns>

         public bool WaitForDisconnect()
            // Wait for disconnected device (leave closed)
         {
            for(int i = 0; i < UsbConfig.ConnectDelay; i++) {
               if(!Detect(true, true)) {
                  return (true);
               }

               Thread.Sleep(1000);
            }

            return false;
         } // WaitForDisconnect

         /// <summary>
         /// Search for device</summary>

         Ftdi.FT_DEVICE_INFO_NODE[] FtdiDeviceListBefore;

         public bool DetectUnmarkedDeviceStart() {
            if(Com.IsOpen) {
               return false; // Uart is open, user must close it
            }

            FtdiDevice.Close();

            // Count of FTDI devices :
            uint NumDevices = 0;

            if(FtdiDevice.GetNumberOfDevices(ref NumDevices) != Ftdi.FT_STATUS.FT_OK) {
               return false;
            }

            if(NumDevices == 0) {
               return false;       // no devices
            }

            // Allocate storage for device info list
            FtdiDeviceListBefore = new Ftdi.FT_DEVICE_INFO_NODE[NumDevices];

            // Populate our device list
            if(FtdiDevice.GetDeviceList(FtdiDeviceListBefore) != Ftdi.FT_STATUS.FT_OK) {
               return false;
            }

            return true;
         }


         public bool DetectUnmarkedDevice() {
            Thread.Sleep(5000);

            if(Com.IsOpen) {
               return false; // Uart is open, user must close it
            }

            FtdiDevice.Close();

            // Count of FTDI devices :
            uint NumDevices = 0;

            if(FtdiDevice.GetNumberOfDevices(ref NumDevices) != Ftdi.FT_STATUS.FT_OK) {
               return false;
            }

            if(NumDevices == 0) {
               return false;       // no devices
            }

            // Allocate storage for device info list
            Ftdi.FT_DEVICE_INFO_NODE[] FtdiDeviceListAfter = new Ftdi.FT_DEVICE_INFO_NODE[NumDevices];

            // Populate our device list
            if(FtdiDevice.GetDeviceList(FtdiDeviceListAfter) != Ftdi.FT_STATUS.FT_OK) {
               return false;
            }

            if(FtdiDeviceListBefore.Length != FtdiDeviceListAfter.Length - 1) {
               return false;
            }

            // Search by device name
            DeviceDetectedIndex = FtdiDeviceListAfter.Length - 1;

            for(int i = 0; i < FtdiDeviceListBefore.Length; i++) {
               if(FtdiDeviceListAfter[i].SerialNumber != FtdiDeviceListBefore[i].SerialNumber) {
                  DeviceDetectedIndex = i;
                  break;
               }
            }

            return true;
         }

         /// <summary>
         /// Search for device</summary>

         private bool Detect(bool BlankDevice, bool ProgrammedDevice) {
            if(Com.IsOpen) {
               return false; // Uart is open, user must close it
            }

            FtdiDevice.Close();

            // Count of FTDI devices :
            uint NumDevices = 0;

            if(FtdiDevice.GetNumberOfDevices(ref NumDevices) != Ftdi.FT_STATUS.FT_OK) {
               return false;
            }

            if(NumDevices == 0) {
               return false;       // no devices
            }

            // Allocate storage for device info list
            Ftdi.FT_DEVICE_INFO_NODE[] ftdiDeviceList = new Ftdi.FT_DEVICE_INFO_NODE[NumDevices];

            // Populate our device list
            if(FtdiDevice.GetDeviceList(ftdiDeviceList) != Ftdi.FT_STATUS.FT_OK) {
               return false;
            }

            // Search by device name
            DeviceDetectedIndex = -1;
            int Index = -1;

            foreach(Ftdi.FT_DEVICE_INFO_NODE Device in ftdiDeviceList) {
               Index++;

               if(ComSettings.SerialNumber != 0) {
                  if(Device.SerialNumber != ComSettings.SerialNumber.ToString()) {
                  System.Diagnostics.Debug.WriteLine(Device.LocId.ToString());
                     continue;
                  }
               }

               if(BlankDevice) {
                  foreach(Config.Usb.TFtdi.Names.DeviceName Name in UsbConfig.Ftdi.BlankNames) {
                     if(Device.Description.ToString() == Name.Name) {
                        DeviceDetectedIndex = Index;
                        goto SearchEnd;
                     }
                  }
               }

               if(ProgrammedDevice) {
                  foreach(Config.Usb.TFtdi.Names.DeviceName Name in UsbConfig.Ftdi.ProgrammedNames) {
                     if(Device.Description.ToString() == Name.Name) {
                        DeviceDetectedIndex = Index;
                        goto SearchEnd;
                     }
                  }
               }
            }

         SearchEnd:
            if(DeviceDetectedIndex == -1) {
               return false;  // no device
            }

            return true;
         }
      }

























      public class TTest {
         private ComUart Port = new ComUart();

         private Device.Uart.Config.ComUartSettings Config;

         /// <summary>
         /// Target replies</summary>
         private const byte TEST_OK = (byte)'Y';
         private const byte TEST_ERROR = (byte)'N';

         private struct CmdStruct {
            public string Code;
            public int ReplyLength;
            public int ReplyTimeout;

            public CmdStruct(string code, int length, int timeout)
               : this() {
               Code = code;
               ReplyLength = length;
               ReplyTimeout = timeout;
            }
         }

         /// <summary>
         /// Commands definition</summary>

         private static class Commands {
            public static readonly CmdStruct TEST_ADC = new CmdStruct("A", 9, 3000);
            public static readonly CmdStruct TEST_CHARGER_ON = new CmdStruct("C", 1, 2000);
            public static readonly CmdStruct TEST_DISPLAY = new CmdStruct("D", 1, 3000);
            public static readonly CmdStruct TEST_OFF = new CmdStruct("F", 1, 1000);
            public static readonly CmdStruct TEST_IADC = new CmdStruct("I", 9, 3000);
            public static readonly CmdStruct TEST_MEMORY = new CmdStruct("M", 1, 24000);
            public static readonly CmdStruct TEST_CHARGER_OFF = new CmdStruct("O", 1, 2000);
            public static readonly CmdStruct TEST_POWER = new CmdStruct("P", 5, 500);
            public static readonly CmdStruct TEST_RTC = new CmdStruct("R", 1, 5000);
            public static readonly CmdStruct TEST_SOUND = new CmdStruct("S", 1, 2000);
            public static readonly CmdStruct TEST_USB = new CmdStruct("U", 1, 2000);
            public static readonly CmdStruct TEST_BACKLIGHT_ON = new CmdStruct("B", 1, 1000);
            public static readonly CmdStruct TEST_BACKLIGHT_OFF = new CmdStruct("G", 1, 1000);
            public static readonly CmdStruct TEST_KEYBOARD = new CmdStruct("K", 1 + Tester.Keyboard.KEYS_COUNT, 500);
         }

         private System.Text.ASCIIEncoding Enc = new System.Text.ASCIIEncoding();

         private byte[] Reply = new byte[10];

         /// <summary>
         /// IsOpen</summary>
      
         public bool IsOpen {
            get {
               return Port.IsOpen;
            }
         }

         /// <summary>
         /// Default constructor</summary>
         /// <param name="Configuration">Module configuration</param>

         public TTest(Device.Uart.Config.ComUartSettings _Config) {
            Config = _Config;
         }

         /// <summary>
         /// Open</summary>

         public bool Open() {
            if(Port.IsOpen) {
               Port.Close();
            }

            int Identifier = 0;

            if(!Port.Locate(Config.PortName, ref Identifier)) {
               return (false);
            }

            if(!Port.Open(Identifier)) {
               return (false);
            }

            Port.SetParameters(Config.BaudRate, Config.Parity, Config.Bits, Config.StopBit, Config.Handshake);

            Port.SetRxNowait();
            Port.Flush();

            return (true);
         }

         /// <summary>
         /// Close</summary>

         public bool Close() {
            Port.Close();

            return true;

         }

         /// <summary>
         /// Communication test</summary>

         public bool TestUart() {
            byte[] Buffer = new byte[256];//Config.TestDataSize];

            Port.SetRxWait(1000);//Config.RxTimeout);
            Port.Flush();

            byte[] DataRead = new byte[1];
            byte[] DataWrite = new byte[1];
            DataWrite[0] = (byte)'a';

            for(int i = 0; i < 256; i++) { ////////////////////////////////////////////////////////
               if(Port.Write(DataWrite, 1) != 1) {
                  return (false);
               }

               if(Port.Read(DataRead, 1) != 1) {
                  return (false);
               }

               if(DataRead[0] != DataWrite[0]) {
                  return false;
               }

               if(DataWrite[0] < (byte)'z') {
                  DataWrite[0]++;
               } else {
                  DataWrite[0] = (byte)'a';
               }
            }

            return (true);
         }

         /// <summary>
         /// Presence test</summary>
         /// <remarks>
         /// Target is present if responds when character is transmited over UART.</remarks>
      
         public bool TestPresence(ref bool Present) {
            Port.SetRxWait(100);//(Config.RxTimeout / 10);
            Port.Flush();

            byte[] DataRead = new byte[1];
            byte[] DataWrite = new byte[1];

            DataWrite[0] = (byte)'a';

            if(Port.Write(DataWrite, 1) != 1) {
               return (false);
            }

            if(Port.Read(DataRead, 1) != 1) {
               Present = false;
               return (true);
            }

            if(DataRead[0] != DataWrite[0]) {
               return false;
            }

            Present = true;
            return true;
         }

         /// <summary>
         /// Display test</summary>

         public bool TestDisplay() {
            return (Test(Commands.TEST_DISPLAY));
         }

         /// <summary>
         /// Memory test</summary>

         public bool TestMemory() {
            return (Test(Commands.TEST_MEMORY));
         }

         /// <summary>
         /// RTC test</summary>
      
         public bool TestRtc() {
            return (Test(Commands.TEST_RTC));
         }

         /// <summary>
         /// Sound test</summary>

         public bool TestSound() {
            return (Test(Commands.TEST_SOUND));
         }

         /// <summary>
         /// Read ADC value</summary>

         public bool ReadAdc(ref int Value) {
            if(!Test(Commands.TEST_ADC)) {
               return (false);
            }

            string HexString = (new CString(Reply, Commands.TEST_ADC.ReplyLength - 1)).ToString();

            if(!Int32.TryParse(HexString, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out Value)) {
               return false;
            }

            return (true);
         }

         /// <summary>
         /// Read internal ADC value</summary>

         public bool ReadIadc(ref int Value) {
            if(!Test(Commands.TEST_IADC)) {
               return (false);
            }

            string HexString = (new CString(Reply, Commands.TEST_IADC.ReplyLength - 1)).ToString();

            if(!Int32.TryParse(HexString, System.Globalization.NumberStyles.HexNumber, CultureInfo.InvariantCulture, out Value)) {
               return false;
            }

            return (true);

         }

         /// <summary>
         /// Start USB test</summary>
         /// <remarks>
         /// Next, USB UART should be tested.</remarks>
      
         public bool StartUsb() {
            return (StartTest(Commands.TEST_USB));
         }

         /// <summary>
         /// Get USB test result</summary>

         public bool UsbResult() {
            return (TestResult(Commands.TEST_USB));
         }

         /// <summary>
         /// Enable/disable charger</summary>
      
         public bool EnableCharger(bool Enable) {
            if(Enable) {
               return (Test(Commands.TEST_CHARGER_ON));
            } else {
               return (Test(Commands.TEST_CHARGER_OFF));
            }
         }

         /// <summary>
         /// Test power</summary>

         public bool TestPower(ref bool PPR, ref bool CHG, ref bool USBON, ref bool ENNAB)
            // Read power status
          {
            if(!Test(Commands.TEST_POWER)) {
               return (false);
            }

            PPR = (Reply[0] == 'H');
            CHG = (Reply[1] == 'H');
            USBON = (Reply[2] == 'H');
            ENNAB = (Reply[3] == 'H');
            return (true);
         }

         /// <summary>
         /// Switch off</summary>

         public bool SwitchOff() {
            return (Test(Commands.TEST_OFF));
         }

         /// <summary>
         /// Switch on/off backlight</summary>

         public bool SetBacklight(bool On) {
            if(On) {
               return (Test(Commands.TEST_BACKLIGHT_ON));
            } else {
               return (Test(Commands.TEST_BACKLIGHT_OFF));
            }
         }

         /// <summary>
         /// Read keyboard state</summary>

         public bool ReadKeyboard(ref Tester.Keyboard Keyboard) {
            if(!Test(Commands.TEST_KEYBOARD)) {
               return false;
            }

            for(int i = 0; i < Tester.Keyboard.KEYS_COUNT; i++) {
               Keyboard[i] = Reply[i] == 'H' ? Tester.Keyboard.Key.KeyState.RELEASED : Tester.Keyboard.Key.KeyState.PRESSED;
            }

            return true;
         }

         /// <summary>
         /// Test</summary>

         private bool Test(CmdStruct Cmd) {
            if(!StartTest(Cmd)) {
               return false;
            }

            if(!TestResult(Cmd)) {
               return false;
            }

            return true;
         }

         /// <summary>
         /// Start test</summary>

         private bool StartTest(CmdStruct Cmd) {
            if(!IsOpen) {
               return false;
            }

            Port.Flush();

            if(Port.Write(Enc.GetBytes(Cmd.Code), 1) == 0) {
               return false;
            }

            return true;
         }

         /// <summary>
         /// Test result</summary>

         private bool TestResult(CmdStruct Cmd) {
            Port.SetRxWait(Cmd.ReplyTimeout);

            int size = 0;

            if((size = Port.Read(Reply, Cmd.ReplyLength)) != Cmd.ReplyLength) {
               return false;
            }

            if(Reply[Cmd.ReplyLength - 1] != TEST_OK) {
               return false;
            }

            return true;
         }
      }
   }
}