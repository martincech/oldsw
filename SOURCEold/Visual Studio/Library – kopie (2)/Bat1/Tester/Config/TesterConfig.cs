﻿using System;
using Device.Uart;
using System.Configuration;
using Bat1;
using Veit.Bat1;



namespace Bat1.Tester {
   public enum FirmwareType {
      PRODUCTION = 0,
      TEST,
      Unknown
   }

   public class TesterConfig
   {

      // Firmware types


      // Firmware paths
      public string[] FirmwarePath = new string[Enum.GetValues(typeof(FirmwareType)).Length];

      public TesterConfig()
	   {
         FirmwarePath[(int)FirmwareType.PRODUCTION] = "D:\\Projekty\\Tester Bat1\\Data\\Bat1.hex";
         FirmwarePath[(int)FirmwareType.TEST] = "D:\\Projekty\\Tester Bat1\\Data\\Bat1test.hex";         
	   }
   }
}