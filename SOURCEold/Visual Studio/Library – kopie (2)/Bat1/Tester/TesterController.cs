﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Threading;
using Device.Ftxx;

namespace Bat1.Tester {
   public class TesterController
   {


      // batch spouštět přes kontroler - na začátku označí bat1 seriovým číslem, na konci jej smaže

      public Tester[] Testers;

      public Bat1Dll Dll;

      public Multimeter Multimeter;

      /*private bool[] Resources;

      public void GetResource(Tester.ESharedResources Resource) {
         while(Resources[(int) Resource] == false) {
            Thread.Sleep(100);
         }
         
         lock(Resources) {
           Resources[(int) Resource] = false;
         }
      }

      public void ReleaseResource(Tester.ESharedResources Resource) {
         lock(Resources) {
            Resources[(int)Resource] = true;
         }
      }*/

      public static object Locker2 = new object();
      
      int SerialNumber = 1;

      public int GetSerialNumber() {
         lock(Locker2) {
            return SerialNumber++;
         }
      }

      public static object Locker = new object();

      public void Release(SharedResource.ResourceType Type, Tester Tester) {
            SharedResource Res;
            
            switch(Type) {
               case SharedResource.ResourceType.Dll:
                  Dll.Close();
                  Res = Dll;
                  break;

               case SharedResource.ResourceType.Multimeter:
                  Res = Multimeter;
                  break;

               default:
                  throw new Exception("Unknown Resource");
            }

            Res.Locked = false;
      }

      public void Get(SharedResource.ResourceType Type, out SharedResource Res, Tester Tester) {
         lock(Locker) {
            switch(Type) {
               case SharedResource.ResourceType.Dll:
                  Res = Dll;
                  break;

               case SharedResource.ResourceType.Multimeter:
                  Res = Multimeter;
                  break;

               default:
                  throw new Exception("Unknown Resource");
            }


            while(Res.Locked == true) {
               Thread.Sleep(100);
            }

            Res.Locked = true;

            switch(Type) {
               case SharedResource.ResourceType.Dll:
                  Dll.FtdiSerialNumber(Tester.TargetConfig.ComUsbSettings.SerialNumber);
                  break;

               case SharedResource.ResourceType.Multimeter:
                  if(!Multimeter.IsOpen) {
                     if(!Multimeter.Open()) {
                        throw new Exception("Can't open multimeter");
                     }
                  }
                  break;

               default:
                  throw new Exception("Unknown Resource");
            }

         }
      }

      public TesterController()
	   {
         /*Resources = new bool[Enum.GetNames(typeof(Tester.ESharedResources)).Length];

         for(int i = 0; i < Enum.GetNames(typeof(Tester.ESharedResources)).Length; i++) {
            Resources[i] = true;
         }*/

         Config.Tester TesterConfig = ConfigurationManager.GetSection("Tester") as Config.Tester;

         Testers = new Tester[TesterConfig.DeviceSet.Count];

         Dll = new Bat1Dll();

         Multimeter = new Multimeter(TesterConfig.DeviceSet[0].Instruments.Multimeter);

         for(int i = 0 ; i < TesterConfig.DeviceSet.Count; i++) {
            Testers[i] = new Tester(this);
            TesterConfig.DeviceSet[i].Target.Usb = ConfigurationManager.GetSection("Usb") as Config.Usb;

            TesterConfig.DeviceSet[i].Target.ComUsbSettings.SerialNumber = (int) 0xFFFFFFF;

            Testers[i].Configure(TesterConfig.DeviceSet[i]);
         }

	   }

      /*public void Do(params Tester.Operations[] Operations, int TesterID) {
         bool Passed = true;

         if(TesterID < 0 || TesterID > Testers.Length) {
            return;
         }

         List<Tester.Operations> NotPassedOperations = new List<Tester.Operations>();

         try {

            //Use(TesterDevice.TesterBoard, TesterDevice.Usb, TesterDevice.Target, TesterDevice.PowerSupply, TesterDevice.Multimeter);

            TesterBoard.LedErrorOff();
            TesterBoard.LedReadyOff();

            foreach(Operations Oper in Operations) {
               try {
                  TestReport.H1(OperationDetail[(int)Oper].Name);

                  OperationDetail[(int)Oper].Function();

                  ReportPassed();

               } catch(TestFatalException e) {
                  if(e.Message.Length > 0) {
                     TestReport.Info(e.Message);
                  }

                  ReportNotPassed();
                  break;
               } catch(TestException e) {
                  if(e.Message.Length > 0) {
                     TestReport.Info(e.Message);
                  }

                  ReportNotPassed();
                  Passed = false;

                  NotPassedOperations.Add(Oper);
               }
            }
         } catch(TestInitException e) {
            if(e.Message.Length > 0) {
               TestReport.Info(e.Message);
            }
         } finally {
            Release();
         }

         if(Passed) {
            TesterBoard.LedReadyOn();
         } else {
            TesterBoard.LedErrorOn();
         }


         if(!Passed && Operations.Length > 1) {
            foreach(Operations Oper in NotPassedOperations) {
               TestReport.H2(OperationDetail[(int)Oper].Name + " NOT PASSED");
            }
         }

         TestReport.Save();
      }*/
   }
}