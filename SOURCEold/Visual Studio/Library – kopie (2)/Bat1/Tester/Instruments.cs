﻿//******************************************************************************
//
//   Instruments.cs     Instruments
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using AgilentInstruments;
using System.Threading;

namespace Bat1.Tester {
   /// <summary>
   /// Power supply</summary>

   public class PowerSupply {
      /// <summary>
      /// Power supply output</summary>

      public class Output {
         private AgE3631A.OutputList _AgOutput;

         private AgE3631A _PowerSupply;

         private double _Voltage;

         private double _Current;

         /// <summary>
         /// Default constructor</summary>
         /// <param name="PowerSupply">Power supply</param>
         /// <param name="Output">Power supply output</param>

         public Output(AgE3631A PowerSupply, AgE3631A.OutputList Output) {
            _AgOutput = Output;
            _PowerSupply = PowerSupply;
         }

         /// <summary>
         /// Set/get voltage</summary>
         /// <remarks>
         /// State is being internally held in memory.</remarks>

         public double Voltage {
            set {
               if(_Voltage != value) {
                  if(!_PowerSupply.SetVoltage(value, _AgOutput)) {
                     throw new Exception("Power supply can't set voltage");
                  }

                  _Voltage = value;
               }
            }

            get {
               return _Voltage;
            }
         }

         /// <summary>
         /// Set/get current</summary>
         /// <remarks>
         /// Set state is being internally held in memory. Getting the state means measuring real output current</remarks>

         public double Current {
            set {
               if(_Current != value) {
                  if(!_PowerSupply.SetCurrent(value, _AgOutput)) {
                     throw new Exception("Power supply can't set current");
                  }

                  _Current = value;
               }
            }

            get {
               double Curr = 0;

               if(!_PowerSupply.MeasureCurrent(ref Curr, _AgOutput)) {
                  throw new Exception("Power supply can't measure current");
               }

               return Curr;
            }
         }

      }

      public Output Usb;
      public Output Normal;
      public Output Inverted;

      private AgE3631A Device = new AgE3631A();

      private Config.Instruments.TPowerSupply DeviceConfig;

      private bool _On;

      /// <summary>
      /// Switch output on/off</summary>
      /// <remarks>
      /// State is being internally held in memory.</remarks>

      public bool On {
         set {
            if(_On != value) {
               if(!Device.EnableOutput(value)) {
                  throw new Exception("Power supply can't enable output");
               }

               _On = value;
            }
         }

         get {
            return _On;
         }
      }

      /// <summary>
      /// IsOpen</summary>

      public bool IsOpen {
         get {
            return Device.IsOpen;
         }
      }

      /// <summary>
      /// Output enum</summary>

      public enum Outputs {
         Usb,
         Normal,
         Inverted
      }

      /// <summary>
      /// Default constructor</summary>
      /// <param name="Configuration">Power supply configuration</param>

      public PowerSupply(Config.Instruments.TPowerSupply Configuration) {
         DeviceConfig = Configuration;
         CommonInit();
      }

      /// <summary>
      /// Open</summary>

      public bool Open() {
         if(Device.IsOpen) {
            return true;
         }

         Device.SetCom(DeviceConfig.ComSettings);

         if(!Device.CheckConnect()) {
            return false;
         }

         if(!Device.Control(true)) {
            return false;
         }

         if(!Device.EnableOutput(false)) {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Common initialization</summary>

      private void CommonInit() {
         Usb = new Output(Device, AgE3631A.OutputList.P6V);
         Normal = new Output(Device, AgE3631A.OutputList.P25V);
         Inverted = new Output(Device, AgE3631A.OutputList.N25V);
      }
   }

   /// <summary>
   /// Multimeter</summary>

   public class Multimeter : SharedResource {
      private Ag34410A Device = new Ag34410A();

      private Config.Instruments.TMultimeter DeviceConfig;

      /// <summary>
      /// IsOpen</summary>

      public bool IsOpen {
         get {
            return Device.IsOpen;
         }
      }

      /// <summary>
      /// Default constructor</summary>
      /// <param name="Configuration">Multimeter configuration</param>

      public Multimeter(Config.Instruments.TMultimeter _Config) {
         DeviceConfig = _Config;
      }

      /// <summary>
      /// Open</summary>

      public bool Open() {
         if(!Device.IsOpen) {
            if(!Device.Connect(DeviceConfig.Address)) {
               return false;
            }
         }

         // Set range and resolution
         Device.DCCurrentRange = Ag34410A.DCCurrentRangeList._100uA;
         Device.DCCurrentResolution = Ag34410A.Resolution.Default;

         return true;
      }

      /// <summary>
      /// Measure current</summary>

      public void MeasureCurrent(out double Current) {
         Current = 0;

         if(!Device.ReadDCCurrent(ref Current)) {
            if(Math.Abs(Current) > Device.MaxDCCurrent) {
               return;
            }

            throw new Exception("Multimeter can't measure current");
         }
      }
   }
}