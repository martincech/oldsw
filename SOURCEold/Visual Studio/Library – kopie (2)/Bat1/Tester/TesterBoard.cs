﻿//******************************************************************************
//
//   TesterBoard.cs     TesterBoard
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.UsbIO;

namespace Bat1.Tester {

   /// <summary>
   /// TesterBoard</summary>

   public class TesterBoard {
      private ElexolUsbIO UsbIO = new ElexolUsbIO();

      private Config.TesterBoard Config;

      private ElexolUsbIO.TPorts.TPort.TPin USB_DATA;
      private ElexolUsbIO.TPorts.TPort.TPin USB_POWER_SWITCH;
      private ElexolUsbIO.TPorts.TPort.TPin USB_POWER_SELECT;
      private ElexolUsbIO.TPorts.TPort.TPin SENSOR;
      private ElexolUsbIO.TPorts.TPort.TPin ON_OFF;
      private ElexolUsbIO.TPorts.TPort.TPin K0;
      private ElexolUsbIO.TPorts.TPort.TPin K1;
      private ElexolUsbIO.TPorts.TPort.TPin K2;
      private ElexolUsbIO.TPorts.TPort.TPin LED_READY;
      private ElexolUsbIO.TPorts.TPort.TPin LED_ERROR;
      private ElexolUsbIO.TPorts.TPort.TPin AMPERMETER;
      private ElexolUsbIO.TPorts.TPort.TPin POWER_SWITCH;
      private ElexolUsbIO.TPorts.TPort.TPin POWER_SELECT;
      private ElexolUsbIO.TPorts.TPort.TPin LOAD;
      private ElexolUsbIO.TPorts.TPort.TPin JUMPERS;

      /// <summary>
      /// IsOpen</summary>

      public bool IsOpen {
         get {
            return UsbIO.IsOpen;
         }
      }

      /// <summary>
      /// Default constructor</summary>
      /// <param name="Configuration">Module configuration</param>

      public TesterBoard(Config.TesterBoard Configuration) {
         Config = Configuration;

         InitCommon();
      }

      /// <summary>
      /// Open</summary>

      public bool Open() {
         if(!UsbIO.Open(Config.ComSettings)) {
            return false;
         }

         // není testována, jestli se operace zdařila
         // chtělo by to poslat nějak hromadně
         UsbDataDisconnect();
         UsbPowerSwitchOff();
         UsbPowerSelectUsb();
         OnOffRelease();
         K0Release();
         K1Release();
         K2Release();
         SensorMin();
         LedErrorOff();
         LedReadyOff();
         PowerSwitchOff();
         PowerSelectNormal();
         AmpermeterDisconnect();
         LoadDisconnect();
         JumpersDisconnect();

         USB_DATA.SetOutput();
         USB_POWER_SWITCH.SetOutput();
         USB_POWER_SELECT.SetOutput();
         SENSOR.SetOutput();
         ON_OFF.SetOutput();
         K0.SetOutput();
         K1.SetOutput();
         K2.SetOutput();
         LED_READY.SetOutput();
         LED_ERROR.SetOutput();
         AMPERMETER.SetOutput();
         POWER_SWITCH.SetOutput();
         POWER_SELECT.SetOutput();
         LOAD.SetOutput();
         JUMPERS.SetOutput();

         return true;
      }

      /// <summary>
      /// Close</summary>

      public bool Close() {
         USB_DATA.SetInput();
         USB_POWER_SWITCH.SetInput();
         USB_POWER_SELECT.SetInput();
         SENSOR.SetInput();
         ON_OFF.SetInput();
         K0.SetInput();
         K1.SetInput();
         K2.SetInput();
         LED_READY.SetInput();
         LED_ERROR.SetInput();
         AMPERMETER.SetInput();
         POWER_SWITCH.SetInput();
         POWER_SELECT.SetInput();
         LOAD.SetInput();
         JUMPERS.SetInput();

         return true;
      }

      /// <summary>
      /// Connect USB data and select and switch on USB supply</summary>

      public bool UsbConnect() {
         return UsbDataConnect() && UsbPowerSelectUsb() && UsbPowerSwitchOn();
      }

      /// <summary>
      /// Disconnect USB data and switch off USB supply</summary>
 
      public bool UsbDisconnect() {
         return UsbDataDisconnect() && UsbPowerSwitchOff();
      }

      /// <summary>
      /// Connect USB data</summary>
      
      public bool UsbDataConnect() {
         return USB_DATA.Set();
      }

      /// <summary>
      /// Disconnect USB data</summary>

      public bool UsbDataDisconnect() {
         return USB_DATA.Clear();
      }

      /// <summary>
      /// Switch on USB supply</summary>

      public bool UsbPowerSwitchOn() {
         return USB_POWER_SWITCH.Set();
      }

      /// <summary>
      /// Switch off USB supply</summary>

      public bool UsbPowerSwitchOff() {
         return USB_POWER_SWITCH.Clear();
      }

      /// <summary>
      /// Select USB as USB supply</summary>

      public bool UsbPowerSelectUsb() {
         return USB_POWER_SELECT.Set();
      }

      /// <summary>
      /// Select external supply as USB supply</summary>

      public bool UsbPowerSelectExternal() {
         return USB_POWER_SELECT.Clear();
      }

      /// <summary>
      /// Simulate sensor full load</summary>

      public bool SensorMax() {
         return SENSOR.Set();
      }

      /// <summary>
      /// Simulate sensor no load</summary>

      public bool SensorMin() {
         return SENSOR.Clear();
      }

      /// <summary>
      /// Push On/Off key</summary>

      public bool OnOffPush() {
         return ON_OFF.Set();
      }

      /// <summary>
      /// Release On/Off key</summary>

      public bool OnOffRelease() {
         return ON_OFF.Clear();
      }

      /// <summary>
      /// Push K0 key</summary>

      public bool K0Push() {
         return K0.Set();
      }

      /// <summary>
      /// Release K0 key</summary>

      public bool K0Release() {
         return K0.Clear();
      }

      /// <summary>
      /// Push K1 key</summary>

      public bool K1Push() {
         return K1.Set();
      }

      /// <summary>
      /// Release K1 key</summary>

      public bool K1Release() {
         return K1.Clear();
      }

      /// <summary>
      /// Push K2 key</summary>

      public bool K2Push() {
         return K2.Set();
      }

      /// <summary>
      /// Release K2 key</summary>

      public bool K2Release() {
         return K2.Clear();
      }

      /// <summary>
      /// Connect ampermeter</summary>

      public bool AmpermeterConnect() {
         return AMPERMETER.Set();
      }

      /// <summary>
      /// Disconnect ampermeter</summary>

      public bool AmpermeterDisconnect() {
         return AMPERMETER.Clear();
      }

      /// <summary>
      /// Switch on power supply</summary>

      public bool PowerSwitchOn() {
         return POWER_SWITCH.Set();
      }

      /// <summary>
      /// Switch off power supply</summary>
      /// 
      public bool PowerSwitchOff() {
         return POWER_SWITCH.Clear();
      }

      /// <summary>
      /// Select normal power supply</summary>

      public bool PowerSelectNormal() {
         return POWER_SELECT.Clear();
      }

      /// <summary>
      /// Select inverted power supply</summary>

      public bool PowerSelectInverted() {
         return POWER_SELECT.Set();
      }

      /// <summary>
      /// Connect load to normal power supply</summary>

      public bool LoadConnect() {
         return LOAD.Set();
      }

      /// <summary>
      /// Disconnect load to normal power supply</summary>

      public bool LoadDisconnect() {
         return LOAD.Clear();
      }

      /// <summary>
      /// Connect programming jumpers</summary>

      public bool JumpersConnect() {
         return JUMPERS.Set();
      }

      /// <summary>
      /// Disconnect programming jumpers</summary>

      public bool JumpersDisconnect() {
         return JUMPERS.Clear();
      }

      /// <summary>
      /// LED ready on</summary>

      public bool LedReadyOn() {
         return LED_READY.Set();
      }

      /// <summary>
      /// LED ready off</summary>

      public bool LedReadyOff() {
         return LED_READY.Clear();
      }

      /// <summary>
      /// LED error on</summary>

      public bool LedErrorOn() {
         return LED_ERROR.Set();
      }

      /// <summary>
      /// LED error off</summary>

      public bool LedErrorOff() {
         return LED_ERROR.Clear();
      }
      /// <summary>
      /// Pins definition</summary>

      private void InitCommon() {
         USB_DATA = UsbIO.Port["C"].Pin[2];
         USB_POWER_SWITCH = UsbIO.Port["C"].Pin[0];
         USB_POWER_SELECT = UsbIO.Port["C"].Pin[1];
         SENSOR = UsbIO.Port["B"].Pin[3];
         ON_OFF = UsbIO.Port["B"].Pin[1];
         K0 = UsbIO.Port["B"].Pin[0];
         K1 = UsbIO.Port["B"].Pin[4];
         K2 = UsbIO.Port["B"].Pin[2];
         LED_ERROR = UsbIO.Port["B"].Pin[6];
         LED_READY = UsbIO.Port["B"].Pin[5];
         AMPERMETER = UsbIO.Port["A"].Pin[4];
         POWER_SWITCH = UsbIO.Port["A"].Pin[3];
         POWER_SELECT = UsbIO.Port["A"].Pin[1];
         LOAD = UsbIO.Port["A"].Pin[2];
         JUMPERS = UsbIO.Port["A"].Pin[0];
      }

      // Detruktor pro zhasnutí LED - v době provádění destruktoru z nějakého důvodu už nefunguje komunikace s USB I/O deskou
   }
}