﻿//******************************************************************************
//
//   Tester.cs     Tester
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.Threading;
using System.Configuration;
using System.Collections.Generic;
using System.IO;

namespace Bat1.Tester {
   public class Tester : Worker {
      public class Keyboard {
         public class Key {
            public enum KeyState {
               PRESSED,
               RELEASED
            }

            public KeyState State = KeyState.RELEASED;
         }

         public static readonly int KEYS_COUNT = 4;

         public Key On = new Key();
         public Key K0 = new Key();
         public Key K1 = new Key();
         public Key K2 = new Key();

         private Key[] _Keys = new Key[KEYS_COUNT];

         public Keyboard() {
            _Keys[0] = On;
            _Keys[1] = K0;
            _Keys[2] = K1;
            _Keys[3] = K2;
         }

         public Key.KeyState this[int Index] {
            get {
               if(Index < 0 || Index >= KEYS_COUNT) {
                  // exception;
                  return Key.KeyState.RELEASED;
               }

               return _Keys[Index].State;
            }

            set {
               if(Index < 0 || Index >= KEYS_COUNT) {
                  // exception;
                  return;
               }

               _Keys[Index].State = value;
            }
         }
      }

      private class Report {
         private Crt Output;

         public Report(Crt Win) {
            Output = Win;
         }

         public void Clear() {
            Output.Clear();
         }

         public void H1(string String, params double[] NumValues) {
            H1(String, MakeUnitValues(NumValues));
         }

         public void H2(string String, params double[] NumValues) {
            H2(String, MakeUnitValues(NumValues));
         }

         public void H3(string String, params double[] NumValues) {
            H3(String, MakeUnitValues(NumValues));
         }

         public void Info(string String, params double[] NumValues) {
            Info(String, MakeUnitValues(NumValues));
         }

         public void H1(string String, params UnitValue[] Values) {
            H1(ParseValues(String, Values));
         }

         public void H2(string String, params UnitValue[] Values) {
            H2(ParseValues(String, Values));
         }

         public void H3(string String, params UnitValue[] Values) {
            H3(ParseValues(String, Values));
         }

         public void Info(string String, params UnitValue[] Values) {
            Info(ParseValues(String, Values));
         }

         public void H1(string String) {
            Output.WriteLine("--- " + String + " ---");
         }

         public void H2(string String) {
            Output.WriteLine("- " + String + " -");
         }

         public void H3(string String) {
            Output.WriteLine(String);
         }

         public void Info(string String) {
            Output.WriteLine(String);
         }

         public void Space() {
            Output.NewLine();
         }

         public void Value(UnitValue Value) {
            Output.WriteLine(FormatValue(Value));
         }

         public void Value(double Val) {
            UnitValue Value = new UnitValue();

            Value.Value = Val;
            Value.Unit = Unit;

            Output.WriteLine(FormatValue(Value));
         }

         public bool Save() {
            using(Stream file = File.OpenWrite("a.txt")) {
               //CopyStream("A", file);
            } 

            return true;
         }

         private UnitValue[] MakeUnitValues(params double[] NumValues) {
            UnitValue[] Values = new UnitValue[NumValues.Length];
            int i = 0;

            foreach(double NumValue in NumValues) {
               Values[i].Value = NumValue;
               Values[i].Unit = Unit;

               i++;
            }

            return Values;
         }

         private string ParseValues(string String, params UnitValue[] Values) {
            int LastPos = 0;
            int Pos = 0;
            int i = 0;

            string Output = "";

            while(true) {
               Pos = LastPos;

               LastPos = String.IndexOf('%', LastPos);

               if(LastPos < 0 || i >= Values.Length) {
                  Output += String.Substring(Pos, String.Length - Pos);
                  break;
               }

               if(LastPos != String.Length - 1) {
                  if(String[LastPos + 1] == '%') {
                     Output += String.Substring(Pos, LastPos);
                     LastPos += 2;
                     continue;
                  }
               }

               int a = LastPos - Pos - 1;

               if(Pos == 0 && LastPos == 0) {
                  a++;
               }

               Output += String.Substring(Pos, a);

               Output += FormatValue(Values[i]);

               i++;

               LastPos += 1;
            }

            return Output;
         }

         private string FormatValue(UnitValue Value) {
            String Prefix = "";
            if(Math.Abs(Value.Value) < 1e-12) {
               Value.Value = 0;
            } else if(Math.Abs(Value.Value) < 1e-4) {
               Value.Value *= 1000000;
               Prefix = "u";
            } else if(Math.Abs(Value.Value) < 1e-1) {
               Value.Value *= 1000;
               Prefix = "m";
            }

            return String.Format(" {0:0.000} " + Prefix + "{1}", Value.Value, TranslateUnits(Value.Unit));
         }

         private string TranslateUnits(Units Unit) {
            switch(Unit) {
               case Units.Volts:
                  return "V";

               case Units.Amperes:
                  return "A";

               default:
                  return "";
            }
         }

         public struct UnitValue {
            public double Value;
            public Units Unit;
         }

         public Units Unit;

         public enum Units {
            Volts,
            Amperes,
            Lsb
         }
      }

      public class TestException : System.Exception {
         public TestException() {
         }

         public TestException(string Message)
            : base(Message) {
         }
      }

      public class TestFatalException : TestException {
         public TestFatalException() {
         }

         public TestFatalException(string Message)
            : base(Message) {
         }
      }

      public class TestInitException : TestException {
         public TestInitException() {
         }

         public TestInitException(string Message)
            : base(Message) {
         }
      }

      public enum FirmwareType {
         PRODUCTION = 0,
         TEST,
         Unknown
      }

      public FirmwareType Firmware {
         get {
            return _Firmware;
         }

         private set {
            _Firmware = value;
         }
      }

      //public int FtdiSerialNumber = 0;

      // Configuration
      public Config.Tester TesterConfig; // general tester config
      public Config.Instruments InstrumentsConfig;
      public Config.Target TargetConfig;
      public Config.TesterBoard TesterBoardConfig;
      public Config.FactoryConfig FactoryConfiguration;
      public Config.Tests TestsConfig;
      public Config.TestBatch.TTestBatches TestBatches;

      // Devices
      //private Multimeter Multimeter;
      private PowerSupply PowerSupply;
      //private Usb Usb;
      private Target Target;
      private TesterBoard TesterBoard;

      public BackgroundOperation Operation = new BackgroundOperation();

      private FirmwareType _Firmware = FirmwareType.Unknown;

      private Report TestReport;

      private int Ticket = 0;
      private int TicketToRun = 0;

      // Operations
      public enum Operations {
         SWITCH_ON = 0,
         SWITCH_OFF,
         LOAD_PRODUCTION_FIRMWARE,
         LOAD_TEST_FIRMWARE,
         GET_FIRMWARE,
         FTDI_PROGRAMMING,
         TEST_KEYBOARD,
         TEST_DISPLAY,
         TEST_MEMORY,
         TEST_RTC,
         TEST_ADC,
         TEST_IADC,
         TEST_SOUND,
         TEST_PRINTER_COM,
         TEST_USB,
         TEST_CHARGER_CONNECTION,
         TEST_CHARGER_CURRENT,
         TEST_CHARGER_SHUTDOWN,
         TEST_POWER_SHUTDOWN,
         TEST_POWER_BACKLIGHT_ON,
         TEST_POWER_BACKLIGHT_OFF,
         TEST_POWER_IDLE,
         TEST_POWER_INVERTED,
         WRITE_CONFIG
      }

      // Devices
      private enum TesterDevice {
         Multimeter,
         PowerSupply,
         Target,
         TesterBoard,
         Usb
      }

      // Devices
      public enum ESharedResources {
         Target_StaticDll,
         Multimeter
      }

      private struct OperationStruct {
         public string Name;
         public OperationDelegate Function;
         public ESharedResources[] SharedResources;

         /*private struct SharedResources {
            ESharedResources Resource;
         }*/

         public OperationStruct(string _Name, OperationDelegate _Function, ESharedResources[] _SharedResources)
            : this() {
            Name = _Name;
            Function = _Function;
            SharedResources = _SharedResources;
         }
      }

      private OperationStruct[] OperationDetail = new OperationStruct[Enum.GetValues(typeof(Operations)).Length];

      private delegate void OperationDelegate();

      /// <summary>
      /// Constructor</summary>

      public void SetReportOutput(Crt Win) {
         TestReport = new Report(Win);
      }


      public int ID;

      public void MarkMyTarget(int SerialNumber) {
         // own DLL to be sure this is the only tester trying to mark its target
         // předělat na jiný typ resource, ať se zbytečně nezdržujeme
         SharedResource DllRes;
         Bat1Dll Dll;

         Controller.Get(SharedResource.ResourceType.Dll, out DllRes, this);

         Dll = (Bat1Dll)DllRes;
         
         try {
            TestReport.H1("Marking target, SN = " + SerialNumber.ToString());
            
            if(!Target.Usb.DetectUnmarkedDeviceStart()) {
               throw new TestException("");
            }

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectUnmarkedDevice()) {
               throw new TestException("");
            }

            if(!Target.Usb.SetSerialNumber(SerialNumber)) {
               throw new TestException("");
            }

            TargetConfig.ComUsbSettings.SerialNumber = SerialNumber;
         } finally {
            Target.Usb.Close();
            TesterBoard.UsbDisconnect();
            Controller.Release(SharedResource.ResourceType.Dll, this);
         }
      }



      public void UnmarkMyTarget() {
         try {
            TestReport.H1("Unmarking target");

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectDevice()) {
               throw new TestException("");
            }

            if(!Target.Usb.ClearSerialNumber()) {
               throw new TestException("");
            }
         } finally {
            Target.Usb.Close();
            TesterBoard.UsbDisconnect();
         }

         TargetConfig.ComUsbSettings.SerialNumber = 0;
      }



      public Operations OperationInProgress;
      public bool InProgress = false;




      public Tester(TesterController _Controller) {
         Controller = _Controller;

         OperationDetail[(int)Operations.SWITCH_ON] = new OperationStruct("Switch ON", new OperationDelegate(_SwitchOn), new ESharedResources[]{});
         OperationDetail[(int)Operations.SWITCH_OFF] = new OperationStruct("Switch OFF", new OperationDelegate(_SwitchOff), new ESharedResources[] { });
         OperationDetail[(int)Operations.LOAD_PRODUCTION_FIRMWARE] = new OperationStruct("Load Production Firmware", new OperationDelegate(_LoadProductionFirmware), new ESharedResources[] { });
         OperationDetail[(int)Operations.LOAD_TEST_FIRMWARE] = new OperationStruct("Load Test Firmware", new OperationDelegate(_LoadTestFirmware), new ESharedResources[] { });
         //OperationDetail[(int)Operations.GET_FIRMWARE] = new OperationStruct("Get Firmware", new OperationDelegate(_GetFirmware), new ESharedResources[] { });
         OperationDetail[(int)Operations.FTDI_PROGRAMMING] = new OperationStruct("FTDI Programming", new OperationDelegate(_FtdiProgramming), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_KEYBOARD] = new OperationStruct("Keyboard test", new OperationDelegate(_TestKeyboard), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_DISPLAY] = new OperationStruct("Display test", new OperationDelegate(_TestDisplay), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_MEMORY] = new OperationStruct("Memory test", new OperationDelegate(_TestMemory), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_RTC] = new OperationStruct("RTC test", new OperationDelegate(_TestRtc), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_ADC] = new OperationStruct("ADC test", new OperationDelegate(_TestAdc), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_IADC] = new OperationStruct("IADC test", new OperationDelegate(_TestIadc), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_SOUND] = new OperationStruct("Sound test", new OperationDelegate(_TestSound), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_PRINTER_COM] = new OperationStruct("Printer Com test", new OperationDelegate(_TestPrinterCom), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_USB] = new OperationStruct("USB test", new OperationDelegate(_TestUsb), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_CHARGER_CONNECTION] = new OperationStruct("Charger connection test", new OperationDelegate(_TestChargerConnection), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_CHARGER_CURRENT] = new OperationStruct("Charger current test", new OperationDelegate(_TestChargerCurrent), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_CHARGER_SHUTDOWN] = new OperationStruct("Charger shutdown test", new OperationDelegate(_TestChargerShutdown), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_POWER_SHUTDOWN] = new OperationStruct("Power shutdown", new OperationDelegate(_TestTargetShutdown), new ESharedResources[] { });
         OperationDetail[(int)Operations.TEST_POWER_BACKLIGHT_ON] = new OperationStruct("Power backlight on test", new OperationDelegate(_TestPowerBackLightOn), new ESharedResources[] { ESharedResources.Target_StaticDll });
         OperationDetail[(int)Operations.TEST_POWER_BACKLIGHT_OFF] = new OperationStruct("Power backlight off test", new OperationDelegate(_TestPowerBackLightOff), new ESharedResources[] { ESharedResources.Target_StaticDll });
         OperationDetail[(int)Operations.TEST_POWER_IDLE] = new OperationStruct("Power idle test", new OperationDelegate(_TestPowerIdle), new ESharedResources[] { ESharedResources.Multimeter });
         OperationDetail[(int)Operations.TEST_POWER_INVERTED] = new OperationStruct("Power inverted test", new OperationDelegate(_TestPowerInverted), new ESharedResources[] { ESharedResources.Multimeter });
         OperationDetail[(int)Operations.WRITE_CONFIG] = new OperationStruct("Write Configuration", new OperationDelegate(_WriteConfig), new ESharedResources[] { ESharedResources.Target_StaticDll });


      }

      public void Configure() {
         //try {
            /*
   Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

   config.Sections.Remove("Tests");
   config.Sections.Add("Tests", TestsConfig);

   config.Save();

   */
            // Tester config
            //ConfigurationManager.OpenExeConfiguration(CFiles.Tester);
            //ConfigurationManager.OpenExeConfiguration("D:\\Bat1Tester.config");
            /*
            ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
            configMap.ExeConfigFilename = @"D:\Bat1Tester.config";
            configMap.
            Configuration config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);

 */
            TesterConfig = ConfigurationManager.GetSection("Tester") as Config.Tester;

            InstrumentsConfig = TesterConfig.DeviceSet[0].Instruments;          //ConfigurationManager.GetSection("Instruments") as Config.Instruments;
            TargetConfig = TesterConfig.DeviceSet[0].Target;                  //ConfigurationManager.GetSection("Target") as Config.Target;
            TesterBoardConfig = TesterConfig.DeviceSet[0].TesterBoard;          //ConfigurationManager.GetSection("TesterBoard") as Config.TesterBoard;
            TargetConfig.Usb = ConfigurationManager.GetSection("Usb") as Config.Usb;
            Initialize();


         /*} catch(ConfigurationErrorsException) {
            throw new TestFatalException("Can't load configuration file/s");
         }*/


      }

      TesterController Controller;

      public void Configure(Config.Tester.TDeviceSetCollection.TDeviceSet Config) {
         TesterConfig = ConfigurationManager.GetSection("Tester") as Config.Tester;
         InstrumentsConfig = Config.Instruments;          //ConfigurationManager.GetSection("Instruments") as Config.Instruments;
         TargetConfig = Config.Target;                  //ConfigurationManager.GetSection("Target") as Config.Target;
         TesterBoardConfig = Config.TesterBoard;          //ConfigurationManager.GetSection("TesterBoard") as Config.TesterBoard;
         TargetConfig.Usb = ConfigurationManager.GetSection("Usb") as Config.Usb;

         

         Initialize();
      }

      private void Initialize() {
         TestBatches = (ConfigurationManager.GetSection("TestBatch") as Config.TestBatch).TestBatches;

         
         // Tests config
         //ConfigurationManager.OpenExeConfiguration(CFiles.Tests);
         //ConfigurationManager.OpenExeConfiguration("D:\\Bat1Tests.config");
         TestsConfig = ConfigurationManager.GetSection("Tests") as Config.Tests;

         // Factory config
         //ConfigurationManager.OpenExeConfiguration(CFiles.Factory);
         //ConfigurationManager.OpenExeConfiguration("D:\\Bat1Factory.config");
         FactoryConfiguration = ConfigurationManager.GetSection("FactoryConfig") as Config.FactoryConfig;

         if(TestBatches == null
            || InstrumentsConfig == null
            || TargetConfig == null
            || TesterBoardConfig == null
            || TesterConfig == null
            || TestsConfig == null
            || FactoryConfiguration == null
            || TargetConfig.Usb.Ftdi.ManufacturerName == null || TargetConfig.Usb.Ftdi.ManufacturerName.Length == 0) { // občas naskočila výjimka, že nějaký objekt z konfigurace není inicializovaný (=null)
            throw new ConfigurationErrorsException("Null sections");
         }

         //Multimeter = new Multimeter(InstrumentsConfig.Multimeter);
         PowerSupply = new PowerSupply(InstrumentsConfig.PowerSupply);
         Target = new Target(TargetConfig);
         TesterBoard = new TesterBoard(TesterBoardConfig);

         TesterBoard.Open();
         TesterBoard.LedReadyOn();
      }

      public void Close() {
         TesterBoard.Close();
      }

      /// <summary>
      /// Do</summary>

      private int _TesterSelected;

      public int TesterSelected {
         set {
            _TesterSelected = value;
         }

         get {
            return _TesterSelected;
         }
      }

      public void Do(params Operations[] Operations) {
         bool Passed = true;
        
         if(InProgress == true) {
            return;
         }

         InProgress = true;

         List<Operations> NotPassedOperations = new List<Operations>();

         try {
            if(Operations.Length > 1) {
               TestReport.Clear();
            }

            Operation.Worker = this;

            Use(TesterDevice.TesterBoard, TesterDevice.Usb, TesterDevice.Target, TesterDevice.PowerSupply, TesterDevice.Multimeter);

            TesterBoard.LedErrorOff();
            TesterBoard.LedReadyOff();

            foreach(Operations Oper in Operations) {
               try {
                  TestReport.H1(OperationDetail[(int)Oper].Name);

                  OperationInProgress = Oper;

                  OperationDetail[(int)Oper].Function();

                  ReportPassed();

               } catch(TestFatalException e) {
                  if(e.Message.Length > 0) {
                     TestReport.Info(e.Message);
                  }

                  ReportNotPassed();
                  break;
               } catch(TestException e) {
                  if(e.Message.Length > 0) {
                     TestReport.Info(e.Message);
                  }

                  ReportNotPassed();
                  Passed = false;

                  NotPassedOperations.Add(Oper);
               }
            }
         } catch(TestInitException e) {
            if(e.Message.Length > 0) {
               TestReport.Info(e.Message);
            }
         } finally {
            InProgress = false;
            Release();
         }

         if(Passed) {
            TesterBoard.LedReadyOn();
         } else {
            TesterBoard.LedErrorOn();
         }

         if(!Passed && Operations.Length > 1) {
            foreach(Operations Oper in NotPassedOperations) {
               TestReport.H2(OperationDetail[(int)Oper].Name + " NOT PASSED");
            }
         }

         TestReport.Save();
      }

      /// <summary>
      /// RunBatch</summary>

      public void RunBatch(string ID) {
         if(TestBatches[ID] == null) {
            return;
         }

         TestReport.Clear();

         Operations[] Batch = new Operations[TestBatches[ID].Tests.Count];

         int i = 0;

         foreach(Config.TestBatch.TTestBatches.TTestBatch.TTests.TTest Test in TestBatches[ID].Tests) {
            Batch[i++] = Test.Name;
         }

         MarkMyTarget(Controller.GetSerialNumber());

         Do(Batch);

         UnmarkMyTarget();
      }

      /*************************************************************************
      **************************** WITH INITIALIZATION *************************
      *************************************************************************/

      /// <summary>
      /// SwitchOn</summary>

      public void SwitchOn() {
         Do(Operations.SWITCH_ON);
      }

      /// <summary>
      /// SwitchOff</summary>

      public void SwitchOff() {
         Do(Operations.SWITCH_OFF);
      }

      /// <summary>
      /// LoadProductionFirmware</summary>

      public void LoadProductionFirmware() {
         Do(Operations.LOAD_PRODUCTION_FIRMWARE);
      }

      /// <summary>
      /// LoadTestFirmware</summary>

      public void LoadTestFirmware() {
         Do(Operations.LOAD_TEST_FIRMWARE);
      }

      /// <summary>
      /// GetFirmware</summary>

      /*public void GetFirmware() {
         Do(Operations.GET_FIRMWARE);
      }*/

      /// <summary>
      /// FtdiProgramming</summary>

      public void FtdiProgramming() {
         Do(Operations.FTDI_PROGRAMMING);
      }

      /// <summary>
      /// TestKeyboard</summary>

      public void TestKeyboard() {
         Do(Operations.TEST_KEYBOARD);
      }

      /// <summary>
      /// TestDisplay</summary>

      public void TestDisplay() {
         Do(Operations.TEST_DISPLAY);
      }

      /// <summary>
      /// TestMemory</summary>

      public void TestMemory() {
         Do(Operations.TEST_MEMORY);
      }

      /// <summary>
      /// TestRtc</summary>

      public void TestRtc() {
         Do(Operations.TEST_RTC);
      }

      /// <summary>
      /// TestAdc</summary>

      public void TestAdc() {
         Do(Operations.TEST_ADC);
      }

      /// <summary>
      /// TestIadc</summary>

      public void TestIadc() {
         Do(Operations.TEST_IADC);
      }

      /// <summary>
      /// TestSound</summary>

      public void TestSound() {
         Do(Operations.TEST_SOUND);
      }

      /// <summary>
      /// TestPrinterCom</summary>

      public void TestPrinterCom() {
         Do(Operations.TEST_PRINTER_COM);
      }

      /// <summary>
      /// TestUsb</summary>

      public void TestUsb() {
         Do(Operations.TEST_USB);
      }

      /// <summary>
      /// TestChargerConnection</summary>

      public void TestChargerConnection() {
         Do(Operations.TEST_CHARGER_CONNECTION);
      }

      /// <summary>
      /// TestChargerCurrent</summary>

      public void TestChargerCurrent() {
         Do(Operations.TEST_CHARGER_CURRENT);
      }

      /// <summary>
      /// TestChargerShutdown</summary>

      public void TestChargerShutdown() {
         Do(Operations.TEST_CHARGER_SHUTDOWN);
      }

      /// <summary>
      /// TestPowerShutdown</summary>

      public void TestPowerShutdown() {
         Do(Operations.TEST_POWER_SHUTDOWN);
      }

      /// <summary>
      /// TestPowerBacklightOn</summary>

      public void TestPowerBacklightOn() {
         Do(Operations.TEST_POWER_BACKLIGHT_ON);
      }

      /// <summary>
      /// TestPowerBacklightOff</summary>

      public void TestPowerBacklightOff() {
         Do(Operations.TEST_POWER_BACKLIGHT_OFF);
      }

      /// <summary>
      /// TestPowerIdle</summary>

      public void TestPowerIdle() {
         Do(Operations.TEST_POWER_IDLE);
      }

      /// <summary>
      /// TestPowerInverted</summary>

      public void TestPowerInverted() {
         Do(Operations.TEST_POWER_INVERTED);
      }

      /// <summary>
      /// WriteConfig</summary>

      public void WriteConfig() {
         Do(Operations.WRITE_CONFIG);
      }

      /// <summary>
      /// Switch on</summary>

      public void _SwitchOn() {
         _TargetSwitchOnPowerNormal();
      }

      /// <summary>
      /// Switch off</summary>

      public void _SwitchOff() {
         _TargetSwitchOff();
      }

      /*************************************************************************
      ************************** WITHOUT INITIALIZATION ************************
      *************************************************************************/

      /// <summary>
      /// Load production firmware to device</summary>

      public void _LoadProductionFirmware() {
         _LoadFirmware(FirmwareType.PRODUCTION);
      }

      /// <summary>
      /// Load test firmware to device</summary>

      public void _LoadTestFirmware() {
         _LoadFirmware(FirmwareType.TEST); ;
      }

      /// <summary>
      /// Load firmware</summary>
      /// <param name="FW">Type of the firmware</param>

      public void _LoadFirmware(FirmwareType FW) {
         LpcFlash Lpc = new LpcFlash();

         try {
            if(FW == FirmwareType.Unknown) {
               throw new TestException("Invalid firmware");
            }

            // Connect target USB to PC
            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectProgrammedDevice()) {
               throw new TestException("Device not present. Check USB/tester board. FTDI programmed???");
            }

            // Firmware
            Hex FirmwareFile = new Hex();

            TestReport.Info("Parsing file");

            Operation.Worker = FirmwareFile;

            Operation.Thread = new Thread(delegate(object unused) {
               FirmwareFile.Load(FactoryConfiguration.Firmware[FW]);
            });

            Operation.Thread.Start();

            while(Operation.Thread.IsAlive) {
               Progress = Operation.Worker.Progress;
               Thread.Sleep(100);
            }

            Device.Uart.Config.UsbSNUartSettings LpcComSettings = new Device.Uart.Config.UsbSNUartSettings();

            LpcComSettings.SerialNumber = TargetConfig.ComUsbSettings.SerialNumber;
            LpcComSettings.DeviceName = TargetConfig.ComUsbSettings.DeviceName;
            LpcComSettings.Bits = 8;
            LpcComSettings.Parity = Device.Uart.Uart.Parity.None;
            LpcComSettings.Handshake = Device.Uart.Uart.Handshake.None;
            LpcComSettings.StopBit = Device.Uart.Uart.StopBit.One;
            LpcComSettings.BaudRate = TesterConfig.Lpc.BaudRate;

            // configure Lpc
            Lpc.SetCom(Target.Usb.Uart, LpcComSettings);

            Lpc.CrystalFrequency = TesterConfig.Lpc.CrystalFrequency;

            Operation.Worker = Lpc;

            TestReport.Info("Contacting device");

            TesterBoard.JumpersConnect();

            if(!Lpc.GetDeviceInfo()) {
               throw new TestFatalException("Can't contact device. Jumpers?");
            }

            TestReport.Info("Writing code");

            // Testovat návratovou hodnotu programování !!!!!

            Operation.Thread = new Thread(delegate(object unused) {
               Lpc.WriteFlash(0, FirmwareFile.Code, (int)FirmwareFile.CodeSize);
            });

            Operation.Thread.Start();



            while(Operation.Thread.IsAlive) {
               Progress = Operation.Worker.Progress;
               Thread.Sleep(100);
            }

            TestReport.Info("Verifying code");

            Operation.Thread = new Thread(delegate(object unused) {
               Lpc.CompareFlash(0, FirmwareFile.Code, (int)FirmwareFile.CodeSize);
            });

            Operation.Thread.Start();

            while(Operation.Thread.IsAlive) {
               Progress = Operation.Worker.Progress;
               Thread.Sleep(100);
            }

            TestReport.Info("Resetting");

            Lpc.Reset();

            Firmware = FW;
         } finally {
            Lpc.Disconnect();
            TesterBoard.JumpersDisconnect();
         }
      }

      /// <summary>
      /// Get type of firmware</summary>

      /*public void _GetFirmware() {
         Firmware = FirmwareType.Unknown;

         _TargetSwitchOnPowerNormal();

         if(Firmware == FirmwareType.TEST) {
            TestReport.Info("Test firmware");
            return;
         }

         try {
            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            if(!Target.Usb.DetectProgrammedDevice()) {
               throw new TestException("No device");
            }

            Thread.Sleep(1500); // Delay for PC to close FTDI, otherwise Dll will not work

            if(Dll.CheckDevice()) {
               Firmware = FirmwareType.PRODUCTION;
               TestReport.Info("Production firmware");
            }
         } finally {
            Dll.Close();
            TesterBoard.UsbDisconnect();
         }

         if(Firmware == FirmwareType.Unknown) {
            TestReport.Info("Unknown firmware");
         }
      }*/

      /// <summary>
      /// FTDI EEPROM programming</summary>

      public void _FtdiProgramming() {
         try {
            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            Progress = 0.1;

            if(!Target.Usb.DetectDevice()) {
               throw new TestException("No device to be programmed");
            }

            Progress = 0.5;

            if(!Target.Usb.WriteEeprom()) {
               throw new TestException("Can't write EEPROM");
            }

            Progress = 0.55;

            if(!TesterBoard.UsbDisconnect()) {
               throw new TestException("Can't disconnect USB");
            }

            Progress = 0.60;

            if(!Target.Usb.WaitForDisconnect()) {
               throw new TestException("Can't disconnect USB");
            }

            Progress = 0.65;

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }

            Progress = 0.70;

            if(!Target.Usb.DetectProgrammedDevice()) {
               throw new TestException("No programmed device");
            }

            Progress = 1;
         } finally {
            Target.Usb.Close();
         }
      }

      /// <summary>
      /// Test keyboard</summary>

      public void _TestKeyboard() {
         Tester.Keyboard Kbd = new Tester.Keyboard();
         Tester.Keyboard ModelKbd = new Tester.Keyboard();

         _TargetSwitchOnPowerNormal();

         //---- release all keys : ---------------------------------------------------
         if(!TesterBoard.OnOffRelease()) {
            throw new TestException("Unable release ON key");
         }

         ModelKbd.On.State = Tester.Keyboard.Key.KeyState.RELEASED;

         if(!TesterBoard.K0Release()) {
            throw new TestException("Unable release K0 key");
         }

         ModelKbd.K0.State = Tester.Keyboard.Key.KeyState.RELEASED;

         if(!TesterBoard.K1Release()) {
            throw new TestException("Unable release K1 key");
         }

         ModelKbd.K1.State = Tester.Keyboard.Key.KeyState.RELEASED;

         if(!TesterBoard.K2Release()) {
            throw new TestException("Unable release K2 key");
         }

         ModelKbd.K2.State = Tester.Keyboard.Key.KeyState.RELEASED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 0.25;

         //---- press ON key : -------------------------------------------------------
         if(!TesterBoard.OnOffPush()) {
            throw new TestException("Unable press ON key");
         }

         ModelKbd.On.State = Tester.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);

         // release key :
         if(!TesterBoard.OnOffRelease()) {
            throw new TestException("Unable release ON key");
         }

         ModelKbd.On.State = Tester.Keyboard.Key.KeyState.RELEASED;

         //---- press K0 key : -------------------------------------------------------
         if(!TesterBoard.K0Push()) {
            throw new TestException("Unable push K0 key");
         }

         ModelKbd.K0.State = Tester.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 0.5;

         // release key :
         if(!TesterBoard.K0Release()) {
            throw new TestException("Unable release K0 key");
         }

         ModelKbd.K0.State = Tester.Keyboard.Key.KeyState.RELEASED;

         //---- press K1 key : -------------------------------------------------------
         if(!TesterBoard.K1Push()) {
            throw new TestException("Unable push K1 key");
         }

         ModelKbd.K1.State = Tester.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 0.75;

         // release key :
         if(!TesterBoard.K1Release()) {
            throw new TestException("Unable release K1 key");
         }

         ModelKbd.K1.State = Tester.Keyboard.Key.KeyState.RELEASED;

         //---- press K2 key : -------------------------------------------------------
         if(!TesterBoard.K2Push()) {
            throw new TestException("Unable push K2 key");
         }

         ModelKbd.K2.State = Tester.Keyboard.Key.KeyState.PRESSED;

         // read status :
         if(!Target.Test.ReadKeyboard(ref Kbd)) {
            throw new TestException("Can't read keyboard");
         }

         MatchKeyboard(Kbd, ModelKbd);
         Progress = 1;

         // release key :
         if(!TesterBoard.K2Release()) {
            throw new TestException("Unable release K2 key");
         }

         ModelKbd.K2.State = Tester.Keyboard.Key.KeyState.RELEASED;
      }

      /// <summary>
      /// Test display</summary>

      public void _TestDisplay() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestDisplay()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test memory</summary>

      public void _TestMemory() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestMemory()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test RTC</summary>

      public void _TestRtc() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestRtc()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test sound</summary>

      public void _TestSound() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestSound()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test printer COM</summary>

      public void _TestPrinterCom() {
         _TargetSwitchOnPowerNormal();

         if(!Target.Test.TestUart()) {
            throw new TestException("");
         }
      }

      /// <summary>
      /// Test ADC</summary>

      public void _TestAdc() {
         _TargetSwitchOnPowerNormal();

         Thread.Sleep(TestsConfig.Adc.InitDelay);

         TestReport.Unit = Report.Units.Lsb;

         TestReport.Info("Passed conditions: " + TestsConfig.Adc.SuccessfulTrials + "/" + TestsConfig.Adc.Trials + " readings in range");

         TestReport.H2("Min");

         TestReport.H3("Target range");

         ReportRange(TestsConfig.Adc.ZeroRange);

         if(!TesterBoard.SensorMin()) {
            throw new TestException("Unable control bridge (TB)");
         }

         TestReport.H3("Values:");

         Progress = 0.1;

         // read trials :
         int Count = 0;
         int Value = 0;
         for(int i = 0; i < TestsConfig.Adc.Trials; i++) {
            Thread.Sleep(TestsConfig.Adc.Delay);

            if(!Target.Test.ReadAdc(ref Value)) {
               throw new TestException("Unable read ADC (T)");
            }

            TestReport.Value(Value);

            if(InRange(Value, TestsConfig.Adc.ZeroRange)) {
               Count++;
            }

            Progress += 0.4 / TestsConfig.Adc.Trials;
         }

         if(Count < TestsConfig.Adc.SuccessfulTrials) {
            throw new TestException("ADC zero out of range");
         }

         TestReport.H2("Max");

         TestReport.H3("Target range");

         ReportRange(TestsConfig.Adc.MaxRange);

         //---------------------------------------------------------------------------
         // test max
         if(!TesterBoard.SensorMax()) {
            throw new TestException("Unable control bridge (TB)");
         }

         Thread.Sleep(TestsConfig.Adc.Delay);                  // wait for stable data

         TestReport.H3("Values:");

         Progress = 0.6;

         // read trials :
         Count = 0;
         for(int i = 0; i < TestsConfig.Adc.Trials; i++) {
            Thread.Sleep(TestsConfig.Adc.Delay);

            if(!Target.Test.ReadAdc(ref Value)) {
               throw new TestException("Unable read ADC (T)");
            }

            TestReport.Value(Value);

            if(InRange(Value, TestsConfig.Adc.MaxRange)) {
               Count++;
            }

            Progress += 0.4 / TestsConfig.Adc.Trials;
         }
         if(Count < TestsConfig.Adc.SuccessfulTrials) {
            throw new TestException("ADC max out of range");
         }
      }

      /// <summary>
      /// Test internal ADC</summary>

      public void _TestIadc() {
         _TargetSwitchOnPowerNormal();

         TesterBoard.UsbDisconnect();

         _SetVoltage(TestsConfig.Iadc.LowVoltage);

         // read trials :
         int Count = 0;
         int Value = 0;
         double Voltage = 0;

         TestReport.Unit = Report.Units.Volts;

         TestReport.Info("Passed conditions: " + TestsConfig.Iadc.SuccessfulTrials + "/" + TestsConfig.Iadc.Trials + " readings in range");

         TestReport.H2("Low voltage");

         TestReport.H3("Target range");

         ReportRange(TestsConfig.Iadc.LowRange);

         TestReport.H3("Values:");

         Progress = 0.1;

         for(int i = 0; i < TestsConfig.Iadc.Trials; i++) {
            Thread.Sleep(TestsConfig.Iadc.Delay);


            if(!Target.Test.ReadIadc(ref Value)) {
               throw new TestException("Can't read IADC");
            }

            Voltage = TestsConfig.Iadc.TransferFunction(Value);

            TestReport.Value(Voltage);

            if(InRange(Voltage, TestsConfig.Iadc.LowRange)) {
               Count++;
            }

            Progress += 0.4 / TestsConfig.Adc.Trials;
         }

         if(Count < TestsConfig.Iadc.SuccessfulTrials) {
            throw new TestException("IADC low out of range");
         }

         //---------------------------------------------------------------------------
         // test max
         _SetVoltage(TestsConfig.Iadc.HighVoltage);

         Thread.Sleep(TestsConfig.Iadc.Delay); // wait for stable data

         // read trials :
         Count = 0;

         TestReport.Space();

         TestReport.H2("High voltage");

         TestReport.H3("Target range");

         ReportRange(TestsConfig.Iadc.HighRange);

         TestReport.H3("Values:");

         Progress = 0.6;

         for(int i = 0; i < TestsConfig.Iadc.Trials; i++) {
            Thread.Sleep(TestsConfig.Iadc.Delay);

            if(!Target.Test.ReadIadc(ref Value)) {
               throw new TestException("Can't read IADC");
            }

            Voltage = TestsConfig.Iadc.TransferFunction(Value);

            TestReport.Value(Voltage);

            if(InRange(Voltage, TestsConfig.Iadc.HighRange)) {
               Count++;
            }

            Progress += 0.4 / TestsConfig.Adc.Trials;
         }
         if(Count < TestsConfig.Iadc.SuccessfulTrials) {
            throw new TestException("ADC max out of range");
         }

      }

      /// <summary>
      /// Test USB</summary>

      public void _TestUsb() {
         try {
            _TargetSwitchOnPowerNormal();

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Unable connect USB (TB)");
            }

            Progress = 0.1;

            if(!Target.Usb.DetectProgrammedDevice()) {
               throw new TestException("No programmed device present (FTDI programmed ???)");
            }

            Progress = 0.6;

            Thread.Sleep(TesterConfig.Target.InitTime);

            Progress = 0.7;

            if(!Target.Test.StartUsb()) {
               throw new TestException("Unable start USB (T)");
            }

            if(!Target.Usb.Open()) {
               throw new TestException("Unable open target communication");
            }

            Progress = 0.9;

            if(!Target.Usb.TestUart()) {
               throw new TestException("");
            }

            if(!Target.Test.UsbResult()) {
               throw new TestException("");
            }

            Progress = 1;
         } finally {
            Target.Usb.Close();
         }

      }

      /// <summary>
      /// Test charger signals</summary>

      public void _TestChargerConnection() {
         try {
            _TargetSwitchOnPowerNormal();

            TesterBoard.UsbDisconnect();

            _UsbChargerDisconnect();

            Thread.Sleep(TesterConfig.Target.UsbPowerSettleTime);

            Progress = 0.5;

            bool PPR = false;
            bool CHG = false;
            bool USBON = false;
            bool ENNAB = false;

            if(!Target.Test.TestPower(ref PPR, ref CHG, ref USBON, ref ENNAB)) {
               throw new TestException("Unable read data");
            }

            if(!PPR) {
               throw new TestException("PPR status without USB is LOW");
            }
            if(USBON) {
               throw new TestException("USBON status without USB is HIGH");
            }

            _UsbChargerConnect();

            Thread.Sleep(TesterConfig.Target.UsbPowerSettleTime);

            if(!Target.Test.TestPower(ref PPR, ref CHG, ref USBON, ref ENNAB)) {
               throw new TestException("Unable read data");
            }

            if(PPR) {
               throw new TestException("PPR status with USB is HIGH");
            }
            if(!USBON) {
               throw new TestException("USBON status with USB is LOW");
            }

            Progress = 1;
         } finally {
            _UsbChargerDisconnect();
         }

      }

      /// <summary>
      /// Test charger shutdown on full charge</summary>

      public void _TestChargerShutdown() {
         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current < %", TestsConfig.ChargerShutdown.Current);

            _VoltageSetAndConnectNormal();

            if(!TesterBoard.LoadConnect()) {
               throw new TestException("Can't connect load");
            }

            TesterBoard.UsbDisconnect();

            _UsbChargerConnect();

            bool Shutdown = false;

            Report.UnitValue CurrentU = new Report.UnitValue();
            CurrentU.Unit = Report.Units.Amperes;

            Report.UnitValue VoltageU = new Report.UnitValue();
            VoltageU.Unit = Report.Units.Volts;

            foreach(Config.Tests.Step Step in TestsConfig.ChargerShutdown.VoltageSteps) {
               _SetVoltage(Step.Voltage);

               Thread.Sleep(TesterConfig.Target.UsbPowerSettleTime);

               CurrentU.Value = PowerSupply.Usb.Current;
               VoltageU.Value = Step.Voltage;

               TestReport.Info("% @ %", CurrentU, VoltageU);

               if(CurrentU.Value < TestsConfig.ChargerShutdown.Current) {
                  Shutdown = true;
                  break;
               }

               Progress += 1.0 / TestsConfig.PowerShutdown.VoltageSteps.Count;
            }

            if(!Shutdown) {
               throw new TestException("");
            }
         } finally {
            TesterBoard.LoadDisconnect();

            _VoltageSetAndConnectNormal();
            _UsbChargerDisconnect();

            /* pokud je nahrán production firmware, váha po odpojení USB napájení
               za předpokladu, že odpojení USB napájení váhu nevypnulo, nějaký čas
               nereaguje na stisk ON/OFF tlačítka (nezapne se). Další testy, které
               mohou po tomto následovat, by tak nebyly schopné váhu zapnout.
            
               Lepší řešení - pokusit se váhu zapnout několikrát
             */

            Thread.Sleep(500);
         }
      }

      /// <summary>
      /// Test charger nominal current</summary>

      public void _TestChargerCurrent() {
         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current in range");

            ReportRange(TestsConfig.ChargerCurrent.Current);

            _SetVoltage(TestsConfig.ChargerCurrent.Voltage);

            TesterBoard.UsbDisconnect();

            _BatteryConnect();

            Progress = 0.2;

            if(!TesterBoard.LoadConnect()) {
               throw new TestException("Can't connect load");
            }

            _UsbChargerConnect();

            Thread.Sleep(TesterConfig.Target.UsbPowerSettleTime);

            Progress = 0.7;

            double Current = PowerSupply.Usb.Current;

            TestReport.Space();

            TestReport.Unit = Report.Units.Amperes;

            TestReport.Value(Current);

            if(!InRange(Current, TestsConfig.ChargerCurrent.Current)) {
               throw new TestException("");
            }

            Progress = 1;
         } finally {
            _VoltageSetAndConnectNormal();
            _UsbChargerDisconnect();
            TesterBoard.LoadDisconnect();

            /* pokud je nahrán production firmware, váha po odpojení USB napájení
               za předpokladu, že odpojení USB napájení váhu nevypnulo, nějaký čas
               nereaguje na stisk ON/OFF tlačítka (nezapne se). Další testy, které
               mohou po tomto následovat, by tak nebyly schopné váhu zapnout.
            
               Lepší řešení - pokusit se váhu zapnout několikrát
             */

            Thread.Sleep(500);
         }
      }

      /// <summary>
      /// Test target shutdown on low voltage</summary>

      public void _TestTargetShutdown() {
         //try {
         TestReport.Unit = Report.Units.Amperes;

         TestReport.Info("Passed conditions: current < %", TestsConfig.PowerShutdown.Current);

         _TargetSwitchOnPowerNormal();

         TesterBoard.UsbDisconnect();

         bool Shutdown = false;

         Report.UnitValue CurrentU = new Report.UnitValue();
         CurrentU.Unit = Report.Units.Amperes;

         Report.UnitValue VoltageU = new Report.UnitValue();
         VoltageU.Unit = Report.Units.Volts;

         foreach(Config.Tests.Step Step in TestsConfig.PowerShutdown.VoltageSteps) {
            _SetVoltage(Step.Voltage);

            Thread.Sleep(TesterConfig.Target.PowerSettleTime);

            CurrentU.Value = PowerSupply.Normal.Current;
            VoltageU.Value = Step.Voltage;

            TestReport.Info("% @ %", CurrentU, VoltageU);

            if(CurrentU.Value < TestsConfig.PowerShutdown.Current) {
               Shutdown = true;
               break;
            }

            Progress += 1.0 / TestsConfig.PowerShutdown.VoltageSteps.Count;
         }

         if(!Shutdown) {
            throw new TestException("");
         }

         //} finally {
         //VoltageSetAndConnectNormal();
         //}

      }

      /// <summary>
      /// Test backlight on current</summary>

      public void _TestPowerBackLightOn() {
         _TestPowerBackLight(true);
      }

      /// <summary>
      /// Test backlight of current</summary>

      public void _TestPowerBackLightOff() {
         _TestPowerBackLight(false);
      }

      public void _TestPowerBackLight(bool On) {
         SharedResource DllRes;
         Bat1Dll Dll;

         Controller.Get(SharedResource.ResourceType.Dll, out DllRes, this);

         Dll = (Bat1Dll)DllRes;
         
         try {
            int OldIntensity = 0;
         
            Config.Tests.Range BacklightRange;

            if(On) {
               BacklightRange = TestsConfig.PowerBacklightOn.Current;
            } else {
               BacklightRange = TestsConfig.PowerBacklightOff.Current;
            }

            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current in range");
            ReportRange(BacklightRange);

            _TargetSwitchOnPowerNormal();

            switch(Firmware) {
               case FirmwareType.TEST:
                  Target.Test.SetBacklight(On);
                  break;

               case FirmwareType.PRODUCTION:
               case FirmwareType.Unknown:
                  TesterBoard.UsbConnect();

                  Target.Usb.DetectProgrammedDevice();

                  Thread.Sleep(1500); // Delay for PC to close FTDI, otherwise Dll will not work

                  if(On) {
                     OldIntensity = Dll.GetBacklightIntensity();
                     Dll.SetBacklightIntensity(9);
                  } else {
                     Dll.SetBacklightIntensity(0);
                  }

                  Dll.SaveDevice();

                  _TargetDoSwitchOn();
                  break;
            }

            Thread.Sleep(100);

            double Current = PowerSupply.Normal.Current;

            switch(Firmware) {
               case FirmwareType.PRODUCTION: // restore
               case FirmwareType.Unknown:
                  Dll.SetBacklightIntensity(OldIntensity);
                  Dll.SaveDevice();
                  Dll.Close();
                  break;
            }

            TestReport.Space();

            TestReport.Unit = Report.Units.Amperes;

            TestReport.Value(Current);

            if(!InRange(Current, BacklightRange)) {
               throw new TestException("");
            }
         } finally {
            Controller.Release(SharedResource.ResourceType.Dll, this);
         }
      }

      /// <summary>
      /// Test idle current</summary>

      public void _TestPowerIdle() {
         SharedResource MultimeterRes;
         Multimeter Multimeter;

         Controller.Get(SharedResource.ResourceType.Multimeter, out MultimeterRes, this);

         Multimeter = (Multimeter)MultimeterRes;

         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current in range");

            ReportRange(TestsConfig.PowerIdle.Current);

            this.Progress = 0.2;

            _VoltageSetAndConnectNormal();

            TesterBoard.UsbDisconnect();

            _BatteryDisconnect(); // switch off

            Thread.Sleep(500);

            _BatteryConnect();

            TesterBoard.AmpermeterConnect();

            Thread.Sleep(500);

            this.Progress = 0.8;

            int SubSequentSamplesInRange = 0;
            double[] Samples = new double[10];
            int i = 0;
            double Sample;
            double Sum1 = 0;
            double Sum2 = 0;

            DateTime Start = DateTime.Now;

            while(true) {
               Multimeter.MeasureCurrent(out Sample);

               TestReport.Value(Sample);

               if(i >= Samples.Length) {
                  Array.Resize(ref Samples, Samples.Length + 10);
               }

               Samples[i] = Sample;

               /*
                Počítají se 2 plovoucí "průměry" z 50 vzorků.
                Jeden aktuální, druhý zpožděný o 30 vzorků
                Jestliže prvni > druhý - neprošel (proud neklesá)
                Jestli vypršel timeout - neprošel
                */

               Sum1 += Sample;

               if(i >= 10) {
                  Sum1 -= Samples[i - 10];
               }

               if(i >= 20) {
                  Sum2 += Samples[i - 20];

                  if(i >= 30) {
                     Sum2 -= Samples[i - 30];
                  }
               }

               if(!InRange(Sample, TestsConfig.PowerIdle.Current)) {
                  SubSequentSamplesInRange = 0;

                  if(Start.AddSeconds(120) < DateTime.Now) {
                     throw new TestException("Timeout");
                  }

                  if(i > 30) {
                     if((Sum1 > Sum2)) {
                        throw new TestException("");
                     }
                  }
               } else {
                  SubSequentSamplesInRange++;
               }

               if(SubSequentSamplesInRange >= 2) {
                  break;
               }

               i++;

               Thread.Sleep(1000);
            }

            this.Progress = 1;
         } finally {
            Controller.Release(SharedResource.ResourceType.Multimeter, this);
            TesterBoard.AmpermeterDisconnect();
         }
      }

      /// <summary>
      /// Test inverted voltage current</summary>

      public void _TestPowerInverted() {
         SharedResource MultimeterRes;
         Multimeter Multimeter;

         Controller.Get(SharedResource.ResourceType.Multimeter, out MultimeterRes, this);

         Multimeter = (Multimeter)MultimeterRes;

         try {
            TestReport.Unit = Report.Units.Amperes;

            TestReport.Info("Passed conditions: current in range");

            ReportRange(TestsConfig.PowerInverted.Current);

            TesterBoard.UsbDisconnect();

            TesterBoard.AmpermeterConnect();

            _BatteryConnectInverted();

            TesterBoard.OnOffPush();

            Thread.Sleep(TestsConfig.PowerInverted.SettleTime);

            TesterBoard.OnOffRelease();

            double Current = 0;

            Multimeter.MeasureCurrent(out Current);

            if(!InRange(Current, TestsConfig.PowerInverted.Current)) {
               throw new TestException("");
            }
         } finally {
            Controller.Release(SharedResource.ResourceType.Multimeter, this);
            _VoltageSetAndConnectNormal();
            Thread.Sleep(1000);
            TesterBoard.AmpermeterDisconnect();
         }
      }

      /// <summary>
      /// Write configuration to device</summary>

      public void _WriteConfig() {
         SharedResource DllRes;
         Bat1Dll Dll;

         Controller.Get(SharedResource.ResourceType.Dll, out DllRes, this);

         Dll = (Bat1Dll)DllRes;

         try {
            _BatteryDisconnect(); 
            //_VoltageSetAndConnectNormal();
            /*
             Pokud je před zapisováním konfigurace váha zapnutá, zápis loga se nezdaří.
             Stačí, když je váha zapnutá "z USB" (signalizuje nabíjení) a zápis loga funguje.
             */

            if(!TesterBoard.UsbConnect()) {
               throw new TestException("Can't connect USB");
            }
            
            if(!Target.Usb.DetectProgrammedDevice()) {
               throw new TestException("Device not present. Check USB/tester board. FTDI programmed???");
            }

            Thread.Sleep(1500); // Let PC close FTDI

            if(!Dll.CheckDevice()) {
               throw new TestException("Device not present. Check USB/tester board. FTDI programmed??? Production firmware programmed ???");
            }

            //Dll.Close()                                                                ?????

            // Verze nejde nastavovat ???
            /*Dll.SetDeviceVersion((uint)((FactoryConfiguration.Version.Major << 8) | FactoryConfiguration.Version.Minor));
            Dll.SetBuild((uint)FactoryConfiguration.Version.Build);
            Dll.SetHwVersion((uint)FactoryConfiguration.Version.Hw);*/

            // DefaultFile
            Dll.FilesDeleteAll();
            int FileIndex = Dll.FileCreate();
            Dll.SetCurrentFile(FileIndex);
            Dll.SetFileName(FileIndex, FactoryConfiguration.DefaultFile.Name);
            Dll.SetFileEnableMoreBirds(FileIndex, FactoryConfiguration.Weighing.Saving.EnableMoreBirds);
            Dll.SetFileNumberOfBirds(FileIndex, FactoryConfiguration.Weighing.Saving.NumberOfBirds);
            Dll.SetFileWeightSortingMode(FileIndex, (int)FactoryConfiguration.Weighing.WeightSorting.Mode);
            Dll.SetFileLowLimit(FileIndex, FactoryConfiguration.Weighing.WeightSorting.LowLimit);
            Dll.SetFileHighLimit(FileIndex, FactoryConfiguration.Weighing.WeightSorting.HighLimit);
            Dll.SetFileSavingMode(FileIndex, (int)FactoryConfiguration.Weighing.Saving.Mode);
            Dll.SetFileFilter(FileIndex, FactoryConfiguration.Weighing.Saving.Filter);
            Dll.SetFileStabilisationTime(FileIndex, FactoryConfiguration.Weighing.Saving.StabilisationTime);
            Dll.SetFileMinimumWeight(FileIndex, FactoryConfiguration.Weighing.Saving.MinimumWeight);
            Dll.SetFileStabilisationRange(FileIndex, FactoryConfiguration.Weighing.Saving.StabilisationRange);

            Dll.SetEnableFileParameters(FactoryConfiguration.EnableFileParameters);

            // User settings
            Dll.SetScaleName(FactoryConfiguration.ScaleName);
            Dll.SetCountry((int)FactoryConfiguration.Country);
            Dll.SetLanguage((int)FactoryConfiguration.Language);
            Dll.SetDeviceDateFormat((int)FactoryConfiguration.DateFormat);
            Dll.SetDateSeparator1(FactoryConfiguration.DateSeparator1);
            Dll.SetDateSeparator2(FactoryConfiguration.DateSeparator2);
            Dll.SetDeviceTimeFormat((int)FactoryConfiguration.TimeFormat);
            Dll.SetTimeSeparator(FactoryConfiguration.TimeSeparator);
            Dll.SetDaylightSavingType((int)FactoryConfiguration.DaylightSavingMode);
            Dll.SetPowerOffTimeout(FactoryConfiguration.PowerOffTimeout);

            // Units
            // zkontrolovat typy
            Dll.SetWeighingCapacity((int)FactoryConfiguration.Units.WeighingCapacity);
            Dll.SetWeighingDivision(FactoryConfiguration.Units.Division);
            Dll.SetWeighingUnits((int)FactoryConfiguration.Units.Units);

            // Sounds
            Dll.SetToneDefault((int)FactoryConfiguration.Sounds.ToneDefault);
            Dll.SetToneLight((int)FactoryConfiguration.Sounds.ToneLight);
            Dll.SetToneOk((int)FactoryConfiguration.Sounds.ToneOk);
            Dll.SetToneHeavy((int)FactoryConfiguration.Sounds.ToneHeavy);
            Dll.SetToneKeyboard((int)FactoryConfiguration.Sounds.ToneKeyboard);
            Dll.SetEnableSpecialSounds(FactoryConfiguration.Sounds.EnableSpecial);
            Dll.SetVolumeKeyboard(FactoryConfiguration.Sounds.VolumeKeyboard);
            Dll.SetVolumeSaving(FactoryConfiguration.Sounds.VolumeSaving);

            // Display
            Dll.SetDisplayMode((int)FactoryConfiguration.Display.Mode);
            Dll.SetDisplayContrast(FactoryConfiguration.Display.Contrast);
            Dll.SetBacklightMode((int)FactoryConfiguration.Display.Backlight.Mode);
            Dll.SetBacklightIntensity(FactoryConfiguration.Display.Backlight.Intensity);
            Dll.SetBacklightDuration(FactoryConfiguration.Display.Backlight.Duration);

            // Printer
            Dll.SetPrinterPaperWidth(FactoryConfiguration.Printer.PaperWidth);
            Dll.SetPrinterCommunicationFormat((int)FactoryConfiguration.Printer.CommunicationFormat);
            Dll.SetPrinterCommunicationSpeed(FactoryConfiguration.Printer.CommunicationSpeed);

            // Password
            if(FactoryConfiguration.Password.Enable) {
               // dodělat nastavení hesla
            } else {
               Dll.ClearPassword();
            }

            // Weighing
            // zkontrolovat jednotky
            Dll.SetWeightSortingMode((int)FactoryConfiguration.Weighing.WeightSorting.Mode);
            Dll.SetSavingMode((int)FactoryConfiguration.Weighing.Saving.Mode);
            Dll.SetEnableMoreBirds(FactoryConfiguration.Weighing.Saving.EnableMoreBirds);
            Dll.SetFilter(FactoryConfiguration.Weighing.Saving.Filter);
            Dll.SetStabilisationTime(FactoryConfiguration.Weighing.Saving.StabilisationTime);
            Dll.SetMinimumWeight(FactoryConfiguration.Weighing.Saving.MinimumWeight);
            Dll.SetStabilisationRange(FactoryConfiguration.Weighing.Saving.StabilisationRange);

            // Statistic
            Dll.SetUniformityRange(FactoryConfiguration.Statistic.UniformityRange);
            Dll.SetHistogramRange(FactoryConfiguration.Statistic.Histogram.Range);
            Dll.SetHistogramStep(FactoryConfiguration.Statistic.Histogram.Step);
            
            // Time
            DateTime now = DateTime.Now;
            if(!Dll.SetTime(Dll.EncodeTime(now.Day, now.Month, now.Year, now.Hour, now.Minute, now.Second))) {
               throw new TestException("Unable write time");
            }

            // Logo
            byte[] LogoData;
            if(!Veit.Bat1.Bat1Logo.ConvertImage(FactoryConfiguration.Logo, out LogoData)) {
               throw new TestException("Bad logo");
            }

            if(!Dll.WriteEeprom(Dll.GetLogoAddress(), LogoData, LogoData.Length)) {
               throw new TestException("Unable write logo");
            }

            Dll.SaveDevice();
         } finally {
            Dll.Close();
            Controller.Release(SharedResource.ResourceType.Dll, this);
            Target.Usb.Close();
            TesterBoard.UsbDisconnect();
         }
      }

      /*************************************************************************
      ************************** SUPPORTING METHODS ****************************
      *************************************************************************/

      private bool _UsbChargerConnect() {
         if(!TesterBoard.UsbPowerSelectExternal() || !TesterBoard.UsbPowerSwitchOn()) {
            throw new TestException("Can't connect usb power");
         }

         Thread.Sleep(TesterConfig.Target.UsbPowerSettleTime);

         return true;
      }

      private bool _UsbChargerDisconnect() {
         if(!TesterBoard.UsbPowerSwitchOff() || !TesterBoard.UsbPowerSelectUsb()) {
            throw new TestException("Can't disconnect usb power");
         }

         return true;
      }


      private bool _BatteryConnectInverted() {
         if(!TesterBoard.PowerSelectInverted() || !TesterBoard.PowerSwitchOn()) {
            throw new TestException("Tester board can't switch power");
         }

         return true;
      }

      private bool _BatteryConnect() {
         if(!TesterBoard.PowerSelectNormal() || !TesterBoard.PowerSwitchOn()) {
            throw new TestFatalException("Tester board can't switch power");
         }

         Thread.Sleep(TesterConfig.Target.PowerSettleTime);

         return true;
      }

      private bool _BatteryDisconnect() {
         if(!TesterBoard.PowerSwitchOff()) {
            throw new TestFatalException("Tester board can't switch power");
         }

         return true;
      }

      private bool _VoltageSetAndConnectNormal() {
         _VoltageSetAndConnect(TesterConfig.PowerSupply.Normal.Voltage);

         return true;
      }

      private bool _VoltageSetAndConnect(double Voltage) {
         _SetVoltage(Voltage);
         _BatteryConnect();

         return true;
      }

      private void _TargetSwitchOnPowerNormal() {
         if(!_VoltageSetAndConnectNormal()) {
            throw new TestFatalException("Unable switch on");
         }

         _TargetSwitchOn();
      }

      private void _TargetSwitchOn() {
         if(!_TargetSwitch(true)) {
            throw new TestFatalException("Unable switch on");
         }
      }

      private void _TargetSwitchOff() {
         if(!_TargetSwitch(false)) {
            throw new TestFatalException("Unable switch off");
         }
      }

      // dodělat korektní vypnutí pro production firmware
      private bool _TargetSwitch(bool On) {
         bool Present = false;

         if(Firmware == FirmwareType.TEST || Firmware == FirmwareType.Unknown) {
            if(Target.Test.TestPresence(ref Present)) {
               if(Present) {
                  Firmware = FirmwareType.TEST;
               }
            }
         }

         if(Firmware == FirmwareType.TEST) {
            if((Present && On) || (!Present && !On)) { // nothing to be done
               return true;
            }
         }

         if(On) { // switch on
            if(!_TargetDoSwitchOn()) {
               return false;
            }

            Thread.Sleep(TesterConfig.Target.InitTime);
         } else { // switch off
            if(Firmware == FirmwareType.TEST) {
               if(!Target.Test.SwitchOff()) {
                  return false;
               }
            } else {
               if(!_UsbChargerDisconnect()) {
                  return false;
               }

               if(!_BatteryDisconnect()) {
                  return false;
               }

               Thread.Sleep(500);

               if(!_BatteryConnect()) {
                  return false;
               }
            }
         }

         if(Firmware == FirmwareType.TEST || Firmware == FirmwareType.Unknown) {
            if(!Target.Test.TestPresence(ref Present)) {
               Firmware = FirmwareType.Unknown;
            } else {
               if(Present) {
                  Firmware = FirmwareType.TEST;
               }
            }
         }

         if(Firmware == FirmwareType.TEST) {
            if((!Present && On) || (Present && !On)) {
               return false;
            }
         }

         return true;
      }

      private bool _TargetDoSwitchOn() {
         return _TargetDoSwitch(true);

      }

      private bool _TargetDoSwitchOff() {
         return _TargetDoSwitch(false);
      }


      private bool _TargetDoSwitch(bool On) {
         if(!TesterBoard.OnOffPush()) {
            return false;
         }

         int PushTime;

         if(On) {
            PushTime = TesterConfig.Target.SwitchOnTime;
         } else {
            PushTime = TesterConfig.Target.SwitchOffTime;
         }

         Thread.Sleep(PushTime);

         if(!TesterBoard.OnOffRelease()) {
            return false;
         }

         return true;
      }

      private void _SetVoltage(double Voltage) {
         PowerSupply.Normal.Voltage = Voltage;

         Thread.Sleep(TesterConfig.PowerSupply.SettleTime);
      }

      private void Use(params TesterDevice[] Devices) {
         int MyTicket = Ticket;

         Ticket++;

         while(MyTicket != TicketToRun) {
            Thread.Sleep(100);
         }

         foreach(TesterDevice Dev in Devices) {
            switch(Dev) {
               case TesterDevice.Multimeter:
                  /*if(!Multimeter.IsOpen) {
                     if(!Multimeter.Open()) {
                        throw new TestInitException("Can't open multimeter");
                     }
                  }*/
                  break;

               case TesterDevice.PowerSupply:
                  if(!PowerSupply.IsOpen) {
                     if(!PowerSupply.Open()) {
                        throw new TestInitException("Can't open power supply");
                     }

                     PowerSupply.Inverted.Voltage = TesterConfig.PowerSupply.Inverted.Voltage;
                     PowerSupply.Inverted.Current = TesterConfig.PowerSupply.Inverted.CurrentLimit;

                     PowerSupply.Normal.Voltage = TesterConfig.PowerSupply.Normal.Voltage;
                     PowerSupply.Normal.Current = TesterConfig.PowerSupply.Normal.CurrentLimit;

                     PowerSupply.Usb.Voltage = TesterConfig.PowerSupply.Usb.Voltage;
                     PowerSupply.Usb.Current = TesterConfig.PowerSupply.Usb.CurrentLimit;

                     PowerSupply.On = true;
                  }
                  break;

               case TesterDevice.TesterBoard:
                  if(!TesterBoard.IsOpen) {
                     if(!TesterBoard.Open()) {
                        throw new TestInitException("Can't open tester board");
                     }
                  }
                  break;

               case TesterDevice.Target:
                  if(!Target.Test.IsOpen) {
                     if(!Target.Test.Open()) {
                        throw new TestInitException("Can't open target (Printer)");
                     }
                  }
                  break;

               case TesterDevice.Usb:
                  break;

               default:
                  throw new TestInitException("Unknown resource");
            }
         }
      }

      private void Release() {
         TicketToRun++;
      }

      private void ReportRange(Config.Tests.Range Range) {
         TestReport.Info("< % ; % >", Range.Min, Range.Max);
      }

      private void ReportPassed() {
         TestReport.H2("PASSED");
         TestReport.Space();
         TestReport.Space();
      }

      private void ReportNotPassed() {
         TestReport.H2("!!! NOT PASSED !!!");
         TestReport.Space();
         TestReport.Space();
      }

      private bool InRange(double value, Config.Tests.Range _Range) {
         if(value < _Range.Min || value > _Range.Max) {
            return false;
         }

         return true;
      }

      /// <summary>
      /// Match 2 keyboards</summary>

      private void MatchKeyboard(Tester.Keyboard Kbd, Tester.Keyboard ModelKbd) {
         if(Kbd.On.State != ModelKbd.On.State) {
            throw new TestException("Frozen ON key");
         }

         if(Kbd.K0.State != ModelKbd.K0.State) {
            throw new TestException("Frozen K0 key");
         }

         if(Kbd.K1.State != ModelKbd.K1.State) {
            throw new TestException("Frozen K1 key");
         }

         if(Kbd.K2.State != ModelKbd.K2.State) {
            throw new TestException("Frozen K2 key");
         }
      }
   }
}

