﻿using System;
using Device.Uart;

namespace Device.Bat1 {
   public class TesterBoardConfig : TesterConfig {
      public Uart.UsbUart.InterfaceUsbUartSettings ComSettings = new Uart.UsbUart.InterfaceUsbUartSettings();

      public TesterBoardConfig() {
         // USB I/O
         ComSettings.DeviceName = "USB I/O 24 R";
         ComSettings.BaudRate = 9600;
         ComSettings.Parity = Uart.Uart.Parity.None;
         ComSettings.Bits = 8;
         ComSettings.StopBits = Uart.Uart.StopBit.One;
         ComSettings.Handshake = Uart.Uart.Handshake.None;
      }
   }
}