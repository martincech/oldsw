﻿//******************************************************************************
//
//   PowerSupply.cs     General Agilent power supply
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.Globalization;
using Device.Uart;

namespace AgilentInstruments {

   public class PowerSupply {
      //***************************** Public ***********************************
      
      public bool IsOpen {
         get {
            if(!Uart.IsOpen) {
               return false;
            }

            if(!Control(true)) {
               return false;
            }

            SendCommand("OUTP?");

            string Reply = "";

            ReceiveReply(ref Reply);

            if(Reply.Length > 0) {
               return true;
            }

            return false;
         }
      }

      //*************************** Protected **********************************
      protected ComUart Uart;                     // COM interface

      protected const int RX_TIMEOUT = 300;      // total Rx timeout
      //protected const int RX_ICH_TIMEOUT = 0;    // Rx intercharacter timeout
      protected const int AG_MAX_BUFFER = 80;
      protected const int AG_COM_NAME = 8;
      protected const string AG_SUPPLY_OK = "+0,\"No error\"";

      //**************************** Private ***********************************

      private Device.Uart.Config.ComUartSettings ComSettings;

      private System.Text.ASCIIEncoding Enc = new System.Text.ASCIIEncoding();
      protected CultureInfo PowerSupplyCultureInfo; // decimal dot/comma

      //***************************** Public ***********************************

      //************************************************************************
      // Constructor
      //************************************************************************

      public PowerSupply() {
         Uart = new ComUart();

         PowerSupplyCultureInfo = new CultureInfo("en-US", false);
      }

      //************************************************************************
      // Set COM
      //************************************************************************

      public void SetCom(Device.Uart.Config.UartSettings Settings)
         // Set communinaction device name
      {
         Disconnect();

         if(Settings.GetType() != typeof(Device.Uart.Config.ComUartSettings)) {
            return;
         }

         ComSettings = (Device.Uart.Config.ComUartSettings)Settings;

         //ComSettings = Set;
         /*
         ComSettings.PortName = Set.PortName;
         ComSettings.BaudRate = Set.BaudRate;
         ComSettings.Parity = Set.Parity;
         ComSettings.Bits = Set.Bits;
         ComSettings.StopBits = Set.StopBit;
         ComSettings.Handshake = Set.Handshake;*/

      } // SetCom

      //************************************************************************
      // Remote
      //************************************************************************

      public bool Control(bool Remote)
         // Control by <Remote> / local
      {
         string Command;

         if(Remote) {
            Command = "SYST:REM";
         } else {
            Command = "SYST:LOC";
         }

         return (SendCommand(Command));
      } // Remote

      //************************************************************************
      // Display
      //************************************************************************

      public bool Display(string Text)
         // Display text
      {
         return (SendCommand("DISP:TEXT '" + Text + "'"));
      } // Display

      //************************************************************************
      // Clear display
      //************************************************************************

      public bool ClearDisplay()
         // Clear display
      {
         return (SendCommand("DISP:TEXT:CLE"));
      } // ClearDisplay

      //************************************************************************
      // Set voltage
      //************************************************************************

      public bool SetVoltage(double Voltage)
         // Set voltage
      {
         return (SendCommand(String.Format(PowerSupplyCultureInfo.NumberFormat, "VOLT {0:0.00000}", Voltage)));
      } // SetVoltage

      //************************************************************************
      // Set current
      //************************************************************************

      public bool SetCurrent(double Current)
         // Set current
      {
         return (SendCommand(String.Format(PowerSupplyCultureInfo.NumberFormat, "CURR {0:0.00000}", Current)));
      } // SetCurrent

      //************************************************************************
      // Enable output
      //************************************************************************

      public bool EnableOutput(bool Enable)
         // Enable output power
      {
         string Command;

         if(Enable) {
            Command = "OUTP ON";
         } else {
            Command = "OUTP OFF";
         }

         return (SendCommand(Command));
      } // EnableOutput

      //************************************************************************
      // Get voltage & current
      //************************************************************************

      public bool Get(ref double Voltage, ref double Current)
         // Get settings
      {
         if(!SendCommand("APPL?")) {
            return (false);
         }

         string Reply = "";

         if(!ReceiveReply(ref Reply)) {
            return (false);
         }

         if(Reply.Length < 2) {
            return (false);
         }

         Reply = Reply.Substring(1, Reply.Length - 2); // erase " from beginning and end of the string

         var ReplyParts = Reply.Split(',');

         if(ReplyParts.Length < 2) {
            return (false);
         }

         if(!Double.TryParse(ReplyParts[0], NumberStyles.Float, PowerSupplyCultureInfo, out Voltage)) {
            return (false);
         }

         if(!Double.TryParse(ReplyParts[1], NumberStyles.Float, PowerSupplyCultureInfo, out Current)) {
            return (false);
         }

         return (true);
      } // Get

      protected bool ReadNumericValue(ref double Value) {
         string Reply = "";

         if(!ReceiveReply(ref Reply)) {
            return (false);
         }

         if(Reply.Length < 2) {
            return (false);
         }

         if(!Double.TryParse(Reply, NumberStyles.Float, PowerSupplyCultureInfo, out Value)) {
            return (false);
         }

         return true;
      }

      //************************************************************************
      // MeasureCurrent
      //************************************************************************

      public bool MeasureCurrent(ref double Current)
         // MeasureCurrent
      {
         if(!SendCommand("MEAS:CURR?")) {
            return (false);
         }

         if(!ReadNumericValue(ref Current)) {
            return false;
         }

         return (true);
      } // MeasureCurrent

      //************************************************************************
      // MeasureVoltage
      //************************************************************************

      public bool MeasureVoltage(ref double Voltage)
         // MeasureVoltage
      {
         if(!SendCommand("MEAS?")) {
            return (false);
         }

         if(!ReadNumericValue(ref Voltage)) {
            return false;
         }

         return (true);
      } // MeasureVoltage

      //************************************************************************
      // Get error
      //************************************************************************

      public bool GetError(ref string Message)
         // Get error message
      {
         if(!SendCommand("SYST:ERR?")) {
            return (false);
         }

         if(!ReceiveReply(ref Message)) {
            return (false);
         }

         return (true);
      } // GetError

      //************************************************************************
      // Check connect
      //************************************************************************

      public bool CheckConnect()
         // Locate and open device
      {
         if(Uart.IsOpen) {
            return (true);
         }

         if(ComSettings.PortName == null) {
            return (false);
         }

         int Identifier = 0;

         if(!Uart.Locate(ComSettings.PortName, ref Identifier)) {
            Disconnect();
            return (false);
         }

         if(!Uart.Open(Identifier)) {
            Disconnect();
            return (false);
         }

         // default parameters :
         Uart.SetParameters(ComSettings.BaudRate, ComSettings.Parity, ComSettings.Bits, ComSettings.StopBit, ComSettings.Handshake);

         // common init :
         Uart.SetRxWait(RX_TIMEOUT);
         Uart.Flush();

         return (true);
      } // CheckConnect

      //************************************************************************
      // Disconnect
      //************************************************************************

      public void Disconnect()
         // Close device
      {
         if(Uart != null) {
            Uart.Close();
         }
      } // Disconnect

      //*************************** Protected **********************************

      //************************************************************************
      // Send command
      //************************************************************************

      protected bool SendCommand(string Command)
         // Send command to device
      {
         if(!CheckConnect()) {
            return (false);
         }

         int Size = Command.Length + 1;
         Command += "\n";

         Uart.Flush();

         if(Uart.Write(Enc.GetBytes(Command), Size) != Size) {
            Disconnect();  // something is wrong, try reopen
            return (false);
         }
         System.Threading.Thread.Sleep(300);
         return (true);
      } // SendCommand

      //************************************************************************
      // Read reply
      //************************************************************************

      protected bool ReceiveReply(ref string Reply)
         // Read reply from device
      {
         if(!CheckConnect()) {
            return (false);
         }

         int RetSize;
         byte[] Buffer = new byte[AG_MAX_BUFFER + 1];

         RetSize = Uart.Read(Buffer, AG_MAX_BUFFER);

         if(RetSize == 0) {
            return (false);
         }

         Reply = Enc.GetString(Buffer);
         int ReplyLength = Reply.IndexOf("\n");

         if(ReplyLength == -1) {
            return (false);            // no terminator
         }

         Reply = Reply.Substring(0, ReplyLength - 1);

         return (true);
      } // ReceiveReply

   } // AgPowerSupply

} // namespace