﻿//******************************************************************************
//
//   Uart.cs     Uart
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

namespace Device.Uart {

   //***************************************************************************
   //   Uart
   //***************************************************************************

   public abstract class Uart {

      //***************************** Private **********************************

      private bool _Dtr = false;
      private bool _Rts = false;
      private bool _Txd = false;

      //***************************** Public ***********************************

      public enum Parity {
         None,
         Odd,
         Even,
         Mark,
         Space
      }

      public enum StopBit {
         One,
         OneHalf,
         Two
      }

      public enum Handshake {
         None,
         Hardware,
         XonXoff
      }

      public bool Dtr {
         get {
            return _Dtr;
         }
         set {
            _Dtr = !value;
            SetDtr(!value);
         }
      }

      public bool Rts {
         get {
            return _Rts;
         }
         set {
            _Rts = !value;
            SetRts(!value);
         }
      }

      public bool Txd {
         get {
            return _Txd;
         }
         set {
            _Txd = !value;
            SetTxd(!value);
         }
      }

      public bool IsOpen {
         get {
            return _IsOpen();
         }
      }

      public abstract bool Open(int Identifier);
      public abstract void Close();
      public abstract int Write(byte[] Data, int Length);
      public abstract int Read(byte[] Data, int Length);
      public abstract void Flush();
      public abstract void SetRxNowait();
      public abstract void SetRxWait(int Timeout);
      public abstract bool SetParameters(int BaudRate, Parity _Parity, int _Bits, StopBit _StopBits, Handshake _Handshake);
      public abstract void SetParameters(int BaudRate, Parity _Parity);

      //**************************** Protected *********************************

      protected abstract bool _IsOpen();
      protected abstract void SetDtr(bool State);
      protected abstract void SetRts(bool State);
      protected abstract void SetTxd(bool State);

   } // Uart

} // namespace
