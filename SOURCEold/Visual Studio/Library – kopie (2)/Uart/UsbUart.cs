﻿//******************************************************************************
//
//   UsbUart.cs     UsbUart
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Ftxx;

namespace Device.Uart {
   ///***************************************************************************
   //   Uart
   ///***************************************************************************

   public class UsbUart : Uart {

      //*************************** Protected **********************************
      
      protected Ftdi FtdiDevice;

      protected string FUsbName;           // USB device name
      protected string FSerialNumber;      // USB device serial number
      protected byte FTxLatency;         // USB device output buffer latency

      //***************************** Private **********************************

      private const byte DEFAULT_LATENCY = 16;
      private const int DEFAULT_TX_TIMEOUT = 2000;

      private bool _ImmediateRead;

      //***************************** Public ***********************************

      public bool ImmediateRead {
         get {
            return _ImmediateRead;
         }
         private set {
            _ImmediateRead = value;
         }
      }

      //******************************************************************************
      // Constructor
      //******************************************************************************

      public UsbUart() {
         ImmediateRead = true;
         FTxLatency = DEFAULT_LATENCY;

         FtdiDevice = new Ftdi();
      }

      //******************************************************************************
      // Locate
      //******************************************************************************

      public bool Locate(string Name, ref int Identifier)
         // Find device by <Name>, returns <Identifier>
      {
         if(FtdiDevice.Locate(Name, ref Identifier) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         return true;
      } // Locate

      //******************************************************************************
      // Locate
      //******************************************************************************

      public bool Locate(string Name, string SerialNumber, ref int Identifier)
         // Find device by <Name>, returns <Identifier>
      {
         if(FtdiDevice.Locate(Name, SerialNumber, ref Identifier) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         return true;
      } // Locate

      //******************************************************************************
      // Access
      //******************************************************************************

      public bool Access(int Identifier)
         // Check device access by <Identifier>
      {
         if(FtdiDevice.Access(Identifier) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         return true;
      } // Access

      //******************************************************************************
      // Open
      //******************************************************************************

      public override bool Open(int Identifier)
         // Open device by <Identifier>
      {
         if(IsOpen) {
            Close();                         // close previous
         }

         if(FtdiDevice.Open(Identifier) != Ftdi.FT_STATUS.FT_OK) {
            return false;
         }

         if(FtdiDevice.GetDescription(out FUsbName) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         if(FtdiDevice.SetLatency(FTxLatency) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         if(FtdiDevice.GetSerialNumber(out FSerialNumber) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         if(FtdiDevice.GetDescription(out FUsbName) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         if(FtdiDevice.SetLatency(FTxLatency) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         return (true);
      } // Open

      //******************************************************************************
      // Close
      //******************************************************************************

      public override void Close()
         // Close device
      {
         if(!IsOpen) {
            return;
         }

         FtdiDevice.Close();
      } // Close

      //******************************************************************************
      // Write
      //******************************************************************************

      public override int Write(byte[] Data, int Length)
         // Write <Data> of size <Length>, returns written length (or 0 if error)
      {
         if(!IsOpen) {
            return 0;
         }

         uint w = 0;

         Ftdi.FT_STATUS Stat = FtdiDevice.Write(Data, Length, ref w);

         if(Stat != Ftdi.FT_STATUS.FT_OK) {
            return (0);   // timeout or error
         }

         //Array.Resize(ref Data, Length);
         //System.Diagnostics.Debug.WriteLine(BitConverter.ToString(Data));

         return ((int)w);
      } // Write

      //************************************************************************
      // Read
      //************************************************************************

      public override int Read(byte[] Data, int Length)
         // Read data <Data> of size <Length>, returns true length (or 0 if error)
      {
         if(!IsOpen) {
            return (0);
         }

         uint r = 0;
         if(ImmediateRead) {
            // RxNowait
            uint Available = 0;
            if(FtdiDevice.GetRxBytesAvailable(ref Available) != Ftdi.FT_STATUS.FT_OK) {
               return (0);
            }
            if(Available == 0) {
               return (0);                   // queue empty
            }
            if(Available < (uint)Length) {
               Length = (int)Available;           // short read
            }
            if(FtdiDevice.Read(Data, (uint)Length, ref r) != Ftdi.FT_STATUS.FT_OK) {
               return (0);
            }
            return ((int)r);
         }

         // RxWait, wait for first character :
         if(FtdiDevice.Read(Data, 1, ref r) != Ftdi.FT_STATUS.FT_OK) {
            return (0);
         }
         if(r != 1) {
            return (0);
         }
         if(Length == 1) {
            return (1);            // requested 1 byte only
         }

         // read remainder :
         Length--;

         byte[] DataChunk = new byte[Length];

         if(FtdiDevice.Read(DataChunk, (uint)Length, ref r) != Ftdi.FT_STATUS.FT_OK) {
            return (0);
         }

         Array.Copy(DataChunk, 0, Data, 1, Length);

         r++;

         return ((int)r);
      } // Read

      //************************************************************************
      // Flush
      //************************************************************************

      public override void Flush()
         // Make input/output queue empty
      {
         if(!IsOpen) {
            return;
         }

         FtdiDevice.Purge(Ftdi.FT_PURGE.FT_PURGE_RX | Ftdi.FT_PURGE.FT_PURGE_TX);
      } // Flush

      //************************************************************************
      // SetRxNowait
      //************************************************************************

      public override void SetRxNowait()
         // Set timing of receiver - returns collected data immediately
      {
         if(!IsOpen) {
            return;
         }

         ImmediateRead = true;

         FtdiDevice.SetTimeouts(0, DEFAULT_TX_TIMEOUT);
      } // SetRxNowait

      //************************************************************************
      // SetRxWait
      //************************************************************************

      public override void SetRxWait(int Timeout)
         // Set Rx <Timeout> [ms]
      {
         if(!IsOpen) {
            return;
         }

         if(Timeout == 0) {
            return;
         }

         ImmediateRead = false;

         FtdiDevice.SetTimeouts((uint)Timeout, DEFAULT_TX_TIMEOUT);

      } // SetRxWait

      //************************************************************************
      // SetParameters
      //************************************************************************

      public override bool SetParameters(int BaudRate, Parity Parity, int Bits, StopBit StopBits, Handshake Handshake) {
         if(!IsOpen) {
            return (false);
         }

         // Set baudrate
         if(FtdiDevice.SetBaudRate((uint)BaudRate) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         // Translate Byte size :
         byte WordBits;
         switch(Bits) {
            case 7:
               WordBits = Ftdi.FT_DATA_BITS.FT_BITS_7;
               break;
            case 8:
               WordBits = Ftdi.FT_DATA_BITS.FT_BITS_8;
               break;
            default:
               return (false); // invalid size
         }
         // Translate Stop Bits :
         byte FtdiStopBits;
         switch(StopBits) {
            case Uart.StopBit.One:
               FtdiStopBits = Ftdi.FT_STOP_BITS.FT_STOP_BITS_1;
               break;
            case Uart.StopBit.Two:
               FtdiStopBits = Ftdi.FT_STOP_BITS.FT_STOP_BITS_2;
               break;
            default:
               return (false);  // invalid stop bits count
         }

         // Translate Parity :
         byte FtdiParity;
         switch(Parity) {
            case Uart.Parity.None:
               FtdiParity = Ftdi.FT_PARITY.FT_PARITY_NONE;
               break;
            case Uart.Parity.Odd:
               FtdiParity = Ftdi.FT_PARITY.FT_PARITY_ODD;
               break;
            case Uart.Parity.Even:
               FtdiParity = Ftdi.FT_PARITY.FT_PARITY_EVEN;
               break;
            case Uart.Parity.Mark:
               FtdiParity = Ftdi.FT_PARITY.FT_PARITY_MARK;
               break;
            case Uart.Parity.Space:
               FtdiParity = Ftdi.FT_PARITY.FT_PARITY_SPACE;
               break;
            default:
               return (false);  // invalid parity
         }

         // Set parameters
         if(FtdiDevice.SetDataCharacteristics(WordBits, FtdiStopBits, FtdiParity) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         // Translate handshake :
         ushort FtdiHandshake;
         byte XonChar = 11;
         byte XoffChar = 13;
         switch(Handshake) {
            case Uart.Handshake.None:
               FtdiHandshake = Ftdi.FT_FLOW_CONTROL.FT_FLOW_NONE;
               break;
            case Uart.Handshake.Hardware:
               FtdiHandshake = Ftdi.FT_FLOW_CONTROL.FT_FLOW_RTS_CTS;
               break;
            case Uart.Handshake.XonXoff:
               FtdiHandshake = Ftdi.FT_FLOW_CONTROL.FT_FLOW_XON_XOFF;
               break;
            default:
               return (false);  // invalid handshake
         }

         // Set handshake
         if(FtdiDevice.SetFlowControl(FtdiHandshake, XonChar, XoffChar) != Ftdi.FT_STATUS.FT_OK) {
            return (false);
         }

         SetRxNowait();

         return (true);
      } // SetParameters

      //************************************************************************
      // SetParameters
      //************************************************************************

      public override void SetParameters(int BaudRate, Parity _Parity) {
         SetParameters(BaudRate, _Parity, 8, StopBit.One, Handshake.None);
      } // SetParameters

      //************************************************************************
      // _IsOpen
      //************************************************************************

      protected override bool _IsOpen() {
         return (FtdiDevice.IsOpen);
      }

      //************************************************************************
      // SetDtr
      //************************************************************************

      protected override void SetDtr(bool State) {
         if(!IsOpen) {
            return;
         }

         FtdiDevice.SetDTR(!State);
      }

      //************************************************************************
      // SetRts
      //************************************************************************

      protected override void SetRts(bool State) {
         if(!IsOpen) {
            return;
         }

         FtdiDevice.SetRTS(!State);
      }

      //************************************************************************
      // SetTxd
      //************************************************************************

      protected override void SetTxd(bool State) {
         if(!IsOpen) {
            return;
         }

         FtdiDevice.SetBreak(!State);
      }

   } // Uart

} // namespace
