﻿using System;
using Device.Uart;

namespace Device.Uart
{
   public class NativeUart {
      private ComUart Port = new ComUart();

      private class TFrame {
         public byte Address;
         public int Size;
         public byte HeaderCrc;
         public byte[] Data;
         public int DataCrc;
         public const byte Sof = 0x55;

         public byte GetValidHeaderCrc() {
            return (byte)(-(Address + Size));
         }

         public int GetValidDataCrc() {
            int DataCrc = 0;

            foreach(byte DataElement in this.Data) {
               DataCrc += DataElement;
            }

            DataCrc = -DataCrc;

            return DataCrc & 0xFFFF;
         }

         public byte[] GetBytes(byte Address, byte[] Data) {
            this.Address = Address;
            this.Size = Data.Length;
            this.HeaderCrc = GetValidHeaderCrc();
            this.Data = Data;
            this.DataCrc = GetValidDataCrc();


            int FramePos = 0;

            byte[] FrameBytes = new byte[1 + 1 + 2 + 1 + Data.Length + 2];

            FrameBytes[FramePos++] = 0x55;
            FrameBytes[FramePos++] = this.Address;
            FrameBytes[FramePos++] = this.HeaderCrc;
            FrameBytes[FramePos++] = (byte)(this.Data.Length & 0x00FF);
            FrameBytes[FramePos++] = (byte)((this.Data.Length & 0xFF00) >> 8);

            foreach(byte DataElement in this.Data) {
               FrameBytes[FramePos++] = DataElement;
            }

            FrameBytes[FramePos++] = (byte)(this.DataCrc & 0x00FF);
            FrameBytes[FramePos++] = (byte)((this.DataCrc & 0xFF00) >> 8);

            return FrameBytes;
         }
      }

      private TFrame Frame = new TFrame();

      public NativeUart() {
      }

      public bool IsOpen {
         get {
            return Port.IsOpen;
         }
      }

      public bool Open(string PortName) {
         int ID = 0;

         if(!Port.Locate(PortName, ref ID)) {
            return false;
         }

         if(!Port.Open(ID)) {
            return false;
         }

         Port.SetParameters(57600, Uart.Parity.None, 8, Uart.StopBit.One, Uart.Handshake.None);
         Port.SetRxWait(100);

         return false;
      }

      public bool JammerWrite(byte[] Data, int Length) {
         if(Port.Write(Data, Length) != Length) {
            return false;
         }

         return true;
      }

      public int JammerRead(byte[] Data, int Length) {
         return Port.Read(Data, Length);
      }

      public bool Write(byte Address, byte[] Data) {
         byte[] FrameBytes = Frame.GetBytes(Address, Data);

         if(Port.Write(FrameBytes, FrameBytes.Length) != FrameBytes.Length) {
            return false;
         }

         return true;
      }

      public bool Read(ref byte Address, ref byte[] Data) {
         byte[] Buffer = new byte[4];

         while(true) {
            if(Port.Read(Buffer, 1) != 1) {
               return false;
            }

            if(Buffer[0] != TFrame.Sof) {
               continue;
            }

            if(Port.Read(Buffer, 1) != 1) {
               return false;
            }

            Frame.Address = Buffer[0];

            if(Port.Read(Buffer, 1) != 1) {
               return false;
            }

            Frame.HeaderCrc = Buffer[0];

            if(Port.Read(Buffer, 1) != 1) {
               return false;
            }

            Frame.Size = Buffer[0];

            if(Port.Read(Buffer, 1) != 1) {
               return false;
            }

            Frame.Size |= Buffer[0] << 8;

            if(Frame.HeaderCrc != Frame.GetValidHeaderCrc()) {
               continue;
            }

            byte[] FrameData = new byte[Frame.Size];

            if(Port.Read(FrameData, Frame.Size) != Frame.Size) {
               return false;
            }

            Frame.Data = FrameData;

            if(Port.Read(Buffer, 1) != 1) {
               return false;
            }

            Frame.DataCrc = Buffer[0];

            if(Port.Read(Buffer, 1) != 1) {
               return false;
            }

            Frame.DataCrc |= Buffer[0] << 8;

            if((Frame.DataCrc & 0xFFFF) != Frame.GetValidDataCrc()) {
               System.Diagnostics.Debug.WriteLine(" " + (Frame.DataCrc & 0xFFFF).ToString() + " != " + Frame.GetValidDataCrc().ToString());
               continue;
            }

            Data = Frame.Data;
            Address = Frame.Address;

            return true;
         }
      }

      public void Close() {
         Port.Close();
      }
   }
}
