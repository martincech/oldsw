﻿//******************************************************************************
//
//   ComUart.cs     ComUart
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using System.IO.Ports;                 // SerialPort
using System.Runtime.InteropServices;  // DLL import

namespace Device.Uart {

   //***************************************************************************
   //   ComUart
   //***************************************************************************

   public class ComUart : Uart {

      //*************************** Protected **********************************

      protected int mHandle = INVALID_HANDLE_VALUE;

      protected const int MIN_COM_NUMBER = 1;
      protected const int MAX_COM_NUMBER = 99;

      //***************************** Public ***********************************

      //************************************************************************
      // Locator
      //************************************************************************

      public bool Locate(string Name, ref int Identifier)
         // Find device by <Name>, returns <Identifier>
      {
         if(Name.Length < 4) {
            return (false);
         }

         // check prefix
         if(Name.Substring(0, 3) != "COM") {
            return (false);
         }

         // get COM number
         int Number;

         if(!Int32.TryParse(Name.Substring(3, Name.Length - 3), out Number)) {
            return (false);
         }

         if(Number < MIN_COM_NUMBER ||
            Number > MAX_COM_NUMBER) {
            return (false);
         }

         Identifier = Number;

         return (true);
      } // Locate

      //************************************************************************
      // Access
      //************************************************************************

      public bool Access(int Identifier)
         // Check for device presention
      {
         string DeviceName = ComName("COM" + Identifier.ToString());
         int Handle;

         Handle = CreateFile(DeviceName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);

         if(Handle == INVALID_HANDLE_VALUE) {
            return (false);
         }

         CloseHandle(Handle);

         return (true);
      } // Access

      //************************************************************************
      // Open
      //************************************************************************

      public override bool Open(int Identifier)
         // Open device
      {
         Close();                       // close previous

         string DeviceName = ComName("COM" + Identifier.ToString());

         mHandle = CreateFile(DeviceName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);

         if(mHandle == INVALID_HANDLE_VALUE) {
            return (false);
         }

         return (true);
      } // Open

      //************************************************************************
      // Close
      //************************************************************************

      public override void Close()
         // Close device
      {
         if(mHandle == INVALID_HANDLE_VALUE) {
            return;
         }

         CloseHandle(mHandle);

         mHandle = INVALID_HANDLE_VALUE;
      } // Close

      //************************************************************************
      // Write
      //************************************************************************

      public override int Write(byte[] Data, int Length)
         // Write <Data> of size <Length>, returns written length (or 0 if error)
      {
         if(!IsOpen) {
            return 0;
         }

         int w;

         if(!WriteFile(mHandle, Data, Length, out w, 0)) {
            return (0);         // timeout or error
         }

         return (w);
      } // Write

      //************************************************************************
      // Read
      //************************************************************************

      public override int Read(byte[] Data, int Length)
         // Read data <Data> of size <Length>, returns true length (or 0 if error)
      {
         if(!IsOpen) {
            return 0;
         }

         int r = 0;

         if(!ReadFile(mHandle, Data, Length, out r, 0)) {
            return (0);
         }

         for(int i = 0 ; i < r ; i++) {
         System.Diagnostics.Debug.Write(String.Format("{0,2:X} ", Data[i]));
         }
         return (r);
      } // Read

      //************************************************************************
      // Flush
      //************************************************************************

      public override void Flush()
         // Make input/output queue empty
      {
         if(!IsOpen) {
            return;
         }

         PurgeComm(mHandle, PURGE_RXCLEAR | PURGE_TXCLEAR);
      } // Flush

      //************************************************************************
      // SetRxNowait
      //************************************************************************

      public override void SetRxNowait()
         // Set timing of receiver - returns collected data immediately
      {
         COMMTIMEOUTS to;

         if(!IsOpen) {
            return;                    // return silently
         }

         // Tx timeout :
         to.WriteTotalTimeoutMultiplier = 1;
         to.WriteTotalTimeoutConstant = 2000;

         // RxTimeout :
         to.ReadIntervalTimeout = 0xffffffff;
         to.ReadTotalTimeoutMultiplier = 0;
         to.ReadTotalTimeoutConstant = 0;
         SetCommTimeouts(mHandle, ref to);
      } // SetRxNowait

      //************************************************************************
      // SetRxWait
      //************************************************************************

      public override void SetRxWait(int Timeout)
         // Set Rx <Timeout> [ms]
      {
         COMMTIMEOUTS to;

         if(!IsOpen) {
            return;                    // return silently
         }

         // Tx timeout :
         to.WriteTotalTimeoutMultiplier = 1;
         to.WriteTotalTimeoutConstant = 2000;

         // RxTimeout :
         to.ReadIntervalTimeout = 0;
         to.ReadTotalTimeoutMultiplier = 0;
         to.ReadTotalTimeoutConstant = (uint)Timeout;
         SetCommTimeouts(mHandle, ref to);
      } // SetRxWait

      //************************************************************************
      // SetParameters
      //************************************************************************

      public override bool SetParameters(int BaudRate, Parity _Parity, int Bits, StopBit StopBits, Handshake _Handshake)
         // Set communication parameters
      {
         DCB dcb;

         if(!IsOpen) {
            return (false);                  // return silently
         }

         GetCommState(mHandle, out dcb);

         dcb.BaudRate = (uint)BaudRate;
         dcb.ByteSize = (byte)Bits;
         dcb.Parity = (byte)_Parity;
         dcb.StopBits = (byte)StopBits;

         switch(_Handshake) {
            case Handshake.None:
               dcb.Handshake = (DTR_CONTROL_ENABLE << fDtrControl) |
                               (RTS_CONTROL_ENABLE << fRtsControl);
               break;

            case Handshake.Hardware:
               dcb.Handshake = (DTR_CONTROL_HANDSHAKE << fDtrControl) |
                               (RTS_CONTROL_HANDSHAKE << fRtsControl) |
                               (1 << fOutxCtsFlow) |
                               (1 << fOutxDsrFlow) |
                               (1 << fDsrSensitivity);
               break;

            case Handshake.XonXoff:
               dcb.Handshake = (1 << fInX) |
                               (1 << fOutX) |
                               (DTR_CONTROL_ENABLE << fDtrControl) |
                               (RTS_CONTROL_ENABLE << fRtsControl);
               break;

            default:
               break;
         }

         SetCommState(mHandle, ref dcb);

         return (true);
      } // SetParameters

      //************************************************************************
      // SetParameters
      //************************************************************************

      public override void SetParameters(int BaudRate, Parity _Parity)
         // Set communication parameters - short version
      {
         SetParameters(BaudRate, _Parity, 8, StopBit.One, Handshake.None);
      } // SetParameters

      //*************************** Protected **********************************

      //************************************************************************
      // _IsOpen
      //************************************************************************

      protected override bool _IsOpen() {
         return (mHandle != INVALID_HANDLE_VALUE);
      }

      //************************************************************************
      // SetDtr - not implemented !
      //************************************************************************

      protected override void SetDtr(bool State) {
         if(!IsOpen) {
            return;
         }
      }

      //************************************************************************
      // SetRts - not implemented !
      //************************************************************************

      protected override void SetRts(bool State) {
         if(!IsOpen) {
            return;
         }
      }

      //************************************************************************
      // SetTxd - not implemented !
      //************************************************************************

      protected override void SetTxd(bool State) {
         if(!IsOpen) {
            return;
         }
      }

      //************************************************************************
      // ComName
      //************************************************************************

      protected static string ComName(string Name)
         // Converts <Name> to appropriate OS name
      {
         return ("\\\\.\\" + Name);
      } // ComName

      //************************************************************************
      // Win32 API
      //************************************************************************
      internal const uint GENERIC_READ = 0x80000000;
      internal const uint GENERIC_WRITE = 0x40000000;

      internal const uint PURGE_TXCLEAR = 0x0004;
      internal const uint PURGE_RXCLEAR = 0x0008;

      internal const int INVALID_HANDLE_VALUE = -1;

      internal const int OPEN_EXISTING = 3;

      [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
      internal static extern int CreateFile(string lpFileName, uint dwDesiredAccess, int dwShareMode, int lpSecurityAttributes, int dwCreationDisposition, int dwFlagsAndAttributes, int hTemplateFile);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern void CloseHandle(int hFile);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern Boolean ReadFile(int hFile, byte[] lpBuffer, int nNumberOfBytesToRead, out int lpNumberOfBytesRead, int lpOverlapped);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern Boolean WriteFile(int hFile, byte[] lpBuffer, int nNumberOfBytesToWrite, out int lpNumberOfBytesWritten, int lpOverlapped);

      [StructLayout(LayoutKind.Sequential, Pack = 1)]
      public struct DCB {
         public uint DCBlength;
         public uint BaudRate;
         public uint Handshake;
         public short wReserved;
         public short XonLim;
         public short XoffLim;
         public byte ByteSize;
         public byte Parity;
         public byte StopBits;
         public byte XonChar;
         public byte XoffChar;
         public byte ErrorChar;
         public byte EofChar;
         public byte EvtChar;
         public short wReserved2;
      };

      // Handshake bitfield offsets :
      public const int fBinary = 0;     /* Binary Mode (skip EOF check)    */
      public const int fParity = 1;     /* Enable parity checking          */
      public const int fOutxCtsFlow = 2;     /* CTS handshaking on output       */
      public const int fOutxDsrFlow = 3;     /* DSR handshaking on output       */
      public const int fDtrControl = 4;     /* DTR Flow control                */
      public const int fDsrSensitivity = 6;     /* DSR Sensitivity              */
      public const int fTXContinueOnXoff = 7;     /* Continue TX when Xoff sent */
      public const int fOutX = 8;     /* Enable output X-ON/X-OFF        */
      public const int fInX = 9;     /* Enable input X-ON/X-OFF         */
      public const int fErrorChar = 10;    /* Enable Err Replacement          */
      public const int fNull = 11;    /* Enable Null stripping           */
      public const int fRtsControl = 12;    /* Rts Flow control                */
      public const int fAbortOnError = 14;    /* Abort all reads and writes on Error */
      // DTR handshake :
      public const uint DTR_CONTROL_DISABLE = 0x00;
      public const uint DTR_CONTROL_ENABLE = 0x01;
      public const uint DTR_CONTROL_HANDSHAKE = 0x02;
      // RTS handshake
      public const uint RTS_CONTROL_DISABLE = 0x00;
      public const uint RTS_CONTROL_ENABLE = 0x01;
      public const uint RTS_CONTROL_HANDSHAKE = 0x02;
      public const uint RTS_CONTROL_TOGGLE = 0x03;

      [StructLayout(LayoutKind.Sequential, Pack = 1)]
      public struct COMMTIMEOUTS {
         public uint ReadIntervalTimeout;
         public uint ReadTotalTimeoutMultiplier;
         public uint ReadTotalTimeoutConstant;
         public uint WriteTotalTimeoutMultiplier;
         public uint WriteTotalTimeoutConstant;
      };

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int GetCommState(int hFile, out DCB dcb);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int SetCommState(int hFile, ref DCB dcb);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int SetCommTimeouts(int hFile, ref COMMTIMEOUTS to);

      [DllImport("kernel32.dll", SetLastError = true)]
      internal static extern int PurgeComm(int hFile, uint Mode);
   } // Uart

} // namespace
