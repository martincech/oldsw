﻿//******************************************************************************
//
//   LpcIsp.cs     LpcIsp
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

using System;
using Device.Uart;
using System.Threading; // Thread.Sleep()

public class LpcIsp : Worker {

   //******************************* Public ************************************

   // ISP return codes
   public enum ReturnCodes {
      CMD_SUCCESS,
      INVALID_COMMAND,
      SRC_ADDR_ERROR,
      DST_ADDR_ERROR,
      SRC_ADDR_NOT_MAPPED,
      DST_ADDR_NOT_MAPPED,
      COUNT_ERROR,
      INVALID_SECTOR,
      SECTOR_NOT_BLANK,
      SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION,
      COMPARE_ERROR,
      BUSY,
      PARAM_ERROR,
      ADDR_ERROR,
      ADDR_NOT_MAPPED,
      CMD_LOCKED,
      INVALID_CODE,
      INVALID_BAUD_RATE,
      INVALID_STOP_BIT,
      CODE_READ_PROTECTION_ENABLED
   };

   // start command mode
   public enum StartMode {
      START_MODE_ARM = 'A',
      START_MODE_THUMB = 'T'
   };

   // part identification number
   public enum PartIdentification {
      LPC2131 = 0x2FF01,
      LPC2132 = 0x2FF11,
      LPC2134 = 0x2FF12,
      LPC2136 = 0x2FF23,
      LPC2138 = 0x2FF25,
   };

   Device.Uart.Config.UartSettings ComSettings;


   public int CrystalFrequency;  // device crystal frequency [Hz]
   public ReturnCodes ErrorCode; // operation result

   //****************************** Protected **********************************

   protected Uart Port;                          // connection port

   protected bool FEnableEcho;                   // enable command echo

   protected CString FTxBuffer = new CString(LPC_BUFFER_SIZE);   // Tx buffer
   protected CString FRxBuffer = new CString(LPC_BUFFER_SIZE);   // Rx buffer

   protected const int LPC_MAX_COPY_SIZE = 4096;       // RAM to flash max. copy size
   protected const int LPC_RAM_ADDRESS = 0x40000200;   // free RAM start
   protected const int LPC_RAM_START = 0x40000000;     // installed RAM start
   protected const int LPC_VECTOR_SIZE = 64;           // interrupt vector page

   protected const int LPC_BUFFER_SIZE = 128;          // Rx/Tx buffer size

   // constants :
   protected const int RX_LINE_TIMEOUT = 100;
   protected const int RX_TIMEOUT = 10000;
   protected const int RX_INTERCHARACTER_TIMEOUT = 10;
   protected CString OK_REPLY = (CString)"OK\r\n";
   protected CString SYNCHRONIZED_CMD = (CString)"Synchronized\r\n";

   // UU encoded transfer
   protected const int UU_LINES = 20;        // lines for CRC
   protected const int UU_LINE_SIZE = 45;    // bytes per line
   protected const int UU_LINE_CHARS = 61;   // characters per line

   //******************************* Private ***********************************

   private const int FLUSH_DATA_SIZE = 1024;

   //******************************* Public ************************************

   public LpcIsp() {
      //DeviceName = "?";
      //BaudRate = 9600;
      //StopBits = 1;
      CrystalFrequency = 18432000;
      ErrorCode = ReturnCodes.CMD_SUCCESS;
      FEnableEcho = true;
   }

   //***************************************************************************
   // Close
   //***************************************************************************

   public void SetCom(Uart _Port, Device.Uart.Config.UartSettings _ComSettings)
      // Close communication port
   {
      Port = _Port;
      ComSettings = _ComSettings;
   } // Close

   //***************************************************************************
   // Close
   //***************************************************************************

   public void Close()
      // Close communication port
   {
      Disconnect();
   } // Close

   //******************************************************************************
   // Disconnect
   //******************************************************************************

   public void Disconnect()
      // Disconnect port
   {
      if(Port == null) {
         return;
      }

      Port.Close();
      Port = null;
   } // Disconnect

   //***************************************************************************
   // Check parameters
   //***************************************************************************

   public bool CheckParameters()
      // Check BaudRate & CrystalFrequency
   {
      switch(CrystalFrequency) {
         case 10000000:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 38400:
                  return (true);
               default:
                  return (false);
            }
         case 11059200:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 57600:
                  return (true);
               default:
                  return (false);
            }
         case 12288000:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 38400:
                  return (true);
               default:
                  return (false);
            }
         case 14745600:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 38400:
               case 57600:
               case 115200:
                  return (true);
               default:
                  return (false);
            }
         case 15360000:
            switch(ComSettings.BaudRate) {
               case 9600:
                  return (true);
               default:
                  return (false);
            }
         case 18432000:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 57600:
                  return (true);
               default:
                  return (false);
            }
         case 19660800:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 38400:
                  return (true);
               default:
                  return (false);
            }
         case 24576000:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 38400:
                  return (true);
               default:
                  return (false);
            }
         case 25000000:
            switch(ComSettings.BaudRate) {
               case 9600:
               case 19200:
               case 38400:
                  return (true);
               default:
                  return (false);
            }
         default:
            return (false);
      }
   } // CheckParameters

   //***************************************************************************
   // Enter programming
   //***************************************************************************

   public bool EnterProgramming()
      // Enter programming mode
   {
      if(!CheckConnect()) {
         return (false);
      }

      Port.Dtr = true;                   // reset
      Port.Rts = true;                   // P0.14

      Thread.Sleep(100);

      Port.Dtr = false;                  // release reset

      Thread.Sleep(100);

      Port.Rts = false;                  // release P0.14

      return (true);
   } // EnterProgramming

   //***************************************************************************
   // Reset target
   //***************************************************************************

   public bool ResetTarget()
      // Reset & run program
   {
      if(!CheckConnect()) {
         return (false);
      }

      Port.Dtr = true;                   // reset
      Port.Rts = false;                  // P0.14

      Thread.Sleep(100);

      Port.Dtr = false;                  // release reset

      return (true);
   } // ResetTarget

   //***************************************************************************
   // Synchronize
   //***************************************************************************

   public bool Synchronize()
      // Synchronize target
   {
      byte[] Question = new byte[] { (byte)'?' };

      if(!CheckConnect()) {
         return (false);
      }

      Port.Flush();
      FEnableEcho = true;                // default enabled

      // send question mark :
      if(Port.Write(Question, 1) != 1) {
         Disconnect();
         return (false);
      }

      if(!ReceiveFixed(SYNCHRONIZED_CMD)) {
         return (false);
      }

      // confirm synchonization :
      FTxBuffer = SYNCHRONIZED_CMD;
      if(!Send()) {
         return (false);
      }

      if(!ReceiveMatch(OK_REPLY)) {
         return (false);
      }

      // send crystal frequency :
      FTxBuffer = (CString)((CrystalFrequency / 1000).ToString() + "\r\n");

      if(!Send()) {
         return (false);
      }

      if(!ReceiveMatch(OK_REPLY)) {
         return (false);
      }
      return (true);
   } // Synchronize

   //***************************************************************************
   // Unlock
   //***************************************************************************

   public bool Unlock()
      // Unlock device
   {
      FTxBuffer = (CString)("U 23130\r\n");

      return (SimpleCommand());
   } // Unlock

   //***************************************************************************
   // Set Baud Rate
   //***************************************************************************

   /*public bool SetBaudRate(int NewBaudRate, int NewStopBits)
      // Set <BaudRate> with 1/2 <StopBits>
   {
      FTxBuffer = (CString)("B " + NewBaudRate.ToString() + " " + NewStopBits.ToString() + "\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      Thread.Sleep(100);

      BaudRate = NewBaudRate;
      StopBits = NewStopBits;
      Disconnect();                       // next time reopen port

      return (true);
   } // SetBaudRate*/


   //***************************************************************************
   // Echo
   //***************************************************************************

   public bool Echo(bool On)
      // Set command echo <On>
   {
      FTxBuffer = (CString)("A " + (On == true ? "1" : "0") + "\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      FEnableEcho = On;

      return (true);
   } // Echo

   //***************************************************************************
   // WriteToRam
   //***************************************************************************

   public bool WriteToRam(int Address, byte[] Data, int Size)
      // Write <Data> to RAM at <Address> with <Size>
   {
      if(Size % 4 != 0) {
         return (false);
      }

      if(Address % 4 != 0) {
         return (false);
      }

      if(Size == 0) {
         return (false);
      }

      if(Address < LPC_RAM_ADDRESS) {
         return (false);
      }

      // send command :
      FTxBuffer = (CString)("W " + Address.ToString() + " " + Size.ToString() + "\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      // UU encoded transfer :
      int BlockSize;
      int DataPos = 0;
      byte[] DataChunk = new byte[UU_LINES * UU_LINE_SIZE];

      do {
         // send CRC protected block :
         BlockSize = Size;               // remainder

         if(BlockSize > UU_LINES * UU_LINE_SIZE) {
            BlockSize = UU_LINES * UU_LINE_SIZE;
         }

         Array.Copy(Data, DataPos, DataChunk, 0, BlockSize);

         if(!SendBlock(DataChunk, BlockSize)) {
            return (false);
         }

         DataPos += BlockSize;
         Size -= BlockSize;          // already send
      } while(Size > 0);

      return (true);
   } // WriteToRam

   //***************************************************************************
   // ReadMemory
   //***************************************************************************

   public bool ReadMemory(int Address, ref byte[] Data, int Size)
      // Read <Data> from memory <Address> with <Size>
   {
      if(Size % 4 != 0) {
         return (false);
      }

      if(Address % 4 != 0) {
         return (false);
      }

      if(Size == 0) {
         return (false);
      }

      // send command :
      FTxBuffer = (CString)("R " + Address.ToString() + " " + Size.ToString() + "\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      // UU encoded transfer :
      int BlockSize;
      int DataPos = 0;
      byte[] DataChunk = new byte[UU_LINES * UU_LINE_SIZE];

      do {
         // receive CRC protected block :
         BlockSize = Size;               // remainder

         if(BlockSize > UU_LINES * UU_LINE_SIZE) {
            BlockSize = UU_LINES * UU_LINE_SIZE;
         }

         if(!ReceiveBlock(ref DataChunk, BlockSize)) {
            return (false);
         }

         Array.Copy(DataChunk, 0, Data, DataPos, BlockSize);

         DataPos += BlockSize;
         Size -= BlockSize;          // already send
      } while(Size > 0);

      return (true);
   } // ReadMemory

   //***************************************************************************
   // Preapare sector
   //***************************************************************************

   public bool PrepareSector(int StartSector, int EndSector)
      // Prepare sector <StartSector>..<EndSector> for write
   {
      FTxBuffer = (CString)("P " + StartSector.ToString() + " " + EndSector.ToString() + "\r\n");

      return (SimpleCommand());
   } // Prepare Sector

   //***************************************************************************
   // Copy Ram to Flash
   //***************************************************************************

   public bool CopyRamToFlash(int FlashAddress, int RamAddress, int Size)
      // Copy data from <RamAddress> to <FlashAddress> with <Size>
   {
      if(Size != 256 &&
          Size != 512 &&
          Size != 1024 &&
          Size != 4096) {
         return (false);
      }

      if(FlashAddress % 256 != 0) {
         return (false);
      }

      if(RamAddress % 4 != 0) {
         return (false);
      }

      FTxBuffer = (CString)("C " + FlashAddress.ToString() + " " + RamAddress.ToString() + " " + Size.ToString() + "\r\n");

      return (SimpleCommand());
   } // CopyRamToFlash

   //***************************************************************************
   // Go
   //***************************************************************************

   public bool Go(int StartAddress, StartMode Mode)
      // Run program at <StartAddress> with <Mode>
   {
      if(StartAddress % 4 != 0) {
         return (false);
      }

      FTxBuffer = (CString)("G " + StartAddress.ToString() + " " + ((char)Mode).ToString() + "\r\n");

      return (SimpleCommand());
   } // Go

   //***************************************************************************
   // Erase Sector
   //***************************************************************************

   public bool EraseSector(int StartSector, int EndSector)
      // Erase sectors <StartSector>..<EndSector>
   {
      FTxBuffer = (CString)("E " + StartSector.ToString() + " " + EndSector.ToString() + "\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      return (true);
   } // EraseSector

   //***************************************************************************
   // Blank check
   //***************************************************************************

   public bool BlankCheck(int StartSector, int EndSector)
      // Blank check sectors <StartSector>..<EndSector>
   {
      if(StartSector == 0) {
         StartSector = 1;          // always fails - vectors remapped to bootsector
      }

      FTxBuffer = (CString)("I " + StartSector.ToString() + " " + EndSector.ToString() + "\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      return (true);
   } // BlankCheck

   //***************************************************************************
   // Part Identification
   //***************************************************************************

   public bool ReadPartId(ref int PartId)
      // Read <PartId> number
   {
      FTxBuffer = (CString)("J\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      // read part number :
      if(!ReceiveRaw(0)) {
         return (false);
      }

      if(!IsDigit(FRxBuffer[0])) {
         return (false);                  // reply not numeric
      }

      PartId = ((CString)FRxBuffer).ToInt();

      return (true);
   } // ReadPartId


   //***************************************************************************
   // Read boot version
   //***************************************************************************

   public bool ReadBootVersion(ref int Version)
      // Read <Version> number
   {
      FTxBuffer = (CString)("K\r\n");

      if(!SimpleCommand()) {
         return (false);
      }

      // read part number :
      if(!ReceiveRaw(0)) {
         return (false);
      }

      if(!IsDigit(FRxBuffer[0])) {
         return (false);                  // reply not numeric
      }

      if(!IsDigit(FRxBuffer[1])) {
         return (false);                  // reply not numeric
      }

      Version = ((FRxBuffer[1] - '0') << 4) | (FRxBuffer[0] - '0');

      return (true);
   } // ReadBootVersion

   //***************************************************************************
   // Compare
   //***************************************************************************

   public bool Compare(int FlashAddress, int RamAddress, int Size)
      // Compare <FlashAddress> with <RamAddress> with <Size>
   {
      if(Size % 4 != 0) {
         return (false);
      }

      if(FlashAddress % 4 != 0) {
         return (false);
      }

      if(RamAddress % 4 != 0) {
         return (false);
      }

      if(FlashAddress < LPC_VECTOR_SIZE) {
         // skip vector area
         int Offset = LPC_VECTOR_SIZE - FlashAddress;
         FlashAddress = LPC_VECTOR_SIZE;
         RamAddress += Offset;
         Size -= Offset;
      }

      FTxBuffer = (CString)("M " + FlashAddress.ToString() + " " + RamAddress.ToString() + " " + Size.ToString() + "\r\n");

      return (SimpleCommand());
   } // Compare

   //****************************** Protected **********************************

   //***************************************************************************
   // Simple Command
   //***************************************************************************

   protected bool SimpleCommand()
      // Send command and check reply
   {
      // send command :
      if(!Send()) {
         return (false);
      }

      // receive status (code\r\n)
      if(!Receive(3)) {
         return (false);
      }

      if(!IsDigit(FRxBuffer[0])) {
         return (false);                  // reply not numeric
      }

      ErrorCode = (ReturnCodes)(int)(CString)FRxBuffer;

      return (ErrorCode == ReturnCodes.CMD_SUCCESS);
   } // SimpleCommand

   //***************************************************************************
   // Send block
   //***************************************************************************

   protected bool SendBlock(byte[] Data, int Size)
      // Send UU encoded block with CRC
   {

      int Crc = 0;
      int LineSize;
      int DataPos = 0;
      byte[] DataChunk = new byte[UU_LINE_SIZE];

      do {
         LineSize = Size;

         if(LineSize > UU_LINE_SIZE) {
            LineSize = UU_LINE_SIZE;
         }

         Array.Copy(Data, DataPos, DataChunk, 0, LineSize);

         if(!SendLine(DataChunk, LineSize)) {
            return (false);
         }

         Crc += LineCrc(DataChunk, LineSize);
         Size -= LineSize;            // already send
         DataPos += LineSize;
      } while(Size > 0);
      
      // send CRC :
      FTxBuffer = (CString)(Crc.ToString() + "\r\n");

      if(!Send()) {
         return (false);
      }

      // check confirmation :
      if(!ReceiveMatch(OK_REPLY)) {
         return (false);
      }

      return (true);
   } // SendBlock

   //******************************* Private ***********************************

   //***************************************************************************
   // Send line
   //***************************************************************************

   private bool SendLine(byte[] Data, int Size)
      // Send UU encoded line
   {
      int i = 0;
      int TriplesCount;
      int Remainder;

      TriplesCount = Size / 3;        // whole triples count
      Remainder = Size % 3;           // bytes of last partial triple

      FTxBuffer = new CString(LPC_BUFFER_SIZE);

      FTxBuffer[i++] = UuEncode((byte)Size);
      
      // send triples :
      int Triple;
      int Pos = 0;

      while(TriplesCount > 0) {
         Triple = Data[Pos++];
         Triple <<= 8;
         Triple |= Data[Pos++];
         Triple <<= 8;
         Triple |= Data[Pos++];
         FTxBuffer[i++] = UuEncode((byte)((Triple >> 18) & 63));
         FTxBuffer[i++] = UuEncode((byte)((Triple >> 12) & 63));
         FTxBuffer[i++] = UuEncode((byte)((Triple >> 6) & 63));
         FTxBuffer[i++] = UuEncode((byte)(Triple & 63));
         TriplesCount--;
      }
      // send remainder :
      if(Remainder > 0) {
         // Remainder is 1, 2
         Triple = Data[Pos++];
         Triple <<= 8;

         if(Remainder > 1) {
            Triple |= Data[Pos++];
         }

         Triple <<= 8;

         // skip third triple
         FTxBuffer[i++] = UuEncode((byte)((Triple >> 18) & 63));
         FTxBuffer[i++] = UuEncode((byte)((Triple >> 12) & 63));
         FTxBuffer[i++] = UuEncode((byte)((Triple >> 6) & 63));
         FTxBuffer[i++] = UuEncode((byte)(Triple & 63));
      }

      // end of line :
      FTxBuffer[i++] = (byte)'\r';
      FTxBuffer[i++] = (byte)'\n';
      FTxBuffer[i++] = (byte)'\0';

      if(!Send()) {
         return (false);
      }

      // line echo :
      if(FEnableEcho) {
         if(!ReceiveFixed(FTxBuffer)) {
            return (false);
         }
      } else {
         Thread.Sleep(50);                      // with special thanks to NXP
      }

      return (true);
   } // SendLine

   //***************************************************************************
   // Receive block
   //***************************************************************************

   private bool ReceiveBlock(ref byte[] Data, int Size)
      // Receive UU encoded block with CRC
   {
      int Crc = 0;
      int LineSize;
      int DataPos = 0;
      byte[] DataChunk = new byte[UU_LINE_SIZE];

      do {
         LineSize = Size;

         if(LineSize > UU_LINE_SIZE) {
            LineSize = UU_LINE_SIZE;
         }

         if(!ReceiveLine(ref DataChunk, LineSize)) {
            return (false);
         }

         Array.Copy(DataChunk, 0, Data, DataPos, LineSize);

         Crc += LineCrc(DataChunk, LineSize);
         Size -= LineSize;            // already received
         DataPos += LineSize;
      } while(Size > 0);

      // receive CRC :
      if(!ReceiveRaw(GetDigits(Crc) + 2)) {  // <number> /r/n
         return (false);
      }

      if(!IsDigit(FRxBuffer[0])) {
         return (false);
      }

      if(Crc != ((CString)FRxBuffer).ToInt()) {
         return (false);
      }
      // send confirmation :

      FTxBuffer = OK_REPLY;

      if(!Send()) {
         return (false);
      }

      // confirmation echo :
      if(FEnableEcho) {
         if(!ReceiveFixed(OK_REPLY)) {
            return (false);
         }
      }

      return (true);
   } // ReceiveBlock

   //***************************************************************************
   // Receive line
   //***************************************************************************

   private bool ReceiveLine(ref byte[] Data, int Size)
      // Receive UU encoded line
   {
      int Count;
      int TriplesCount;
      int Remainder;
      
      TriplesCount = Size / 3;         // whole triples count
      Remainder = Size % 3;            // bytes of last partial triple
      Count = TriplesCount * 4;        // characters count
      
      if(Remainder > 0) {
         Count += 4;                   // add characters for one triple
      }
     
      Count += 3;                      // <size> + /r/n characters
      
      if(!ReceiveRaw(Count)) {
         return (false);
      }

      if(Size != UuDecode(FRxBuffer[0])) {
         return (false);               // length mismatch
      }

      // read whole triples :
      int Triple;
      int i = 1;                       // start of data
      int Pos = 0;
      while(TriplesCount > 0) {
         Triple = UuDecode(FRxBuffer[i++]);
         Triple <<= 6;
         Triple |= UuDecode(FRxBuffer[i++]);
         Triple <<= 6;
         Triple |= UuDecode(FRxBuffer[i++]);
         Triple <<= 6;
         Triple |= UuDecode(FRxBuffer[i++]);

         Data[Pos++] = (byte)((Triple >> 16) & 0xFF);
         Data[Pos++] = (byte)((Triple >> 8) & 0xFF);
         Data[Pos++] = (byte)(Triple & 0xFF);

         TriplesCount--;
      }

      // read partial triple :
      if(Remainder > 0) {
         // Remainder is 1, 2
         Triple = UuDecode(FRxBuffer[i++]);
         Triple <<= 6;
         Triple |= UuDecode(FRxBuffer[i++]);
         Triple <<= 6;
         Triple |= UuDecode(FRxBuffer[i++]);
         Triple <<= 6;
         Triple |= UuDecode(FRxBuffer[i++]);

         Data[Pos++] = (byte)((Triple >> 16) & 0xFF);

         if(Remainder > 1) {
            Data[Pos++] = (byte)((Triple >> 8) & 0xFF);
         }
         // skip third triple
      }
      return (true);
   } // ReceiveLine


   //***************************************************************************
   // Line CRC
   //***************************************************************************

   private int LineCrc(byte[] Data, int Size)
      // Returns line CRC
   {
      int Crc = 0;
      int Pos = 0;

      for(int i = 0; i < Size; i++) {
         Crc += Data[Pos++];
      }

      return (Crc);
   } // LineCrc

   //***************************************************************************
   // UU Encode
   //***************************************************************************

   private byte UuEncode(byte Data)
      // Returns UU character encoded <Data>
   {
      return ((byte)((Data == 0) ? 0x60 : (Data + 0x20)));
   } // UuEncode

   //***************************************************************************
   // UU Decode
   //***************************************************************************

   private byte UuDecode(byte Data)
      // Returns binary UU decoded <Data>
   {
      return ((byte)((Data == 0x60) ? 0x00 : Data - 0x20));
   } // UuDecode

   //******************************************************************************
   // IsDigit
   //******************************************************************************

   private bool IsDigit(byte Ch) {
      if(Ch < (byte)'0' && Ch > (byte)'9') {
         return (false);
      }

      return (true);
   }

   //***************************************************************************
   // Get digits
   //***************************************************************************

   private int GetDigits(int Number)
      // Returns <Number> of digits
   {
      int Count = 1;                      // at least one digit
      
      for(int i = 0; i < 10; i++) {
         Number /= 10;
         
         if(Number == 0) {
            break;
         }

         Count++;
      }
      return (Count);
   } // GetDigits

   //***************************************************************************
   // Send
   //***************************************************************************

   private bool Send()
      // Send command
   {
      if(!CheckConnect()) {
         return (false);
      }

      Port.Flush();

      int Length = FTxBuffer.Length;

      if(Port.Write((byte[])FTxBuffer, Length) != Length) {
         Disconnect();                    // something is wrong, try reopen
         return (false);
      }

      return (true);
   } // Send

   //***************************************************************************
   // Receive
   //***************************************************************************

   private bool Receive(int ReqSize)
      // Receive reply
   {
      // read echo :
      if(FEnableEcho) {
         if(!ReceiveFixed((CString)FTxBuffer)) {
            return (false);
         }
      }
      // read reply :
      return (ReceiveRaw(ReqSize));
   } // Receive

   //***************************************************************************
   // Receive match
   //***************************************************************************

   private bool ReceiveMatch(CString Reply)
      // Receive with echo and compare with <Reply>
   {
      // read echo :
      if(FEnableEcho) {
         if(!ReceiveFixed((CString)FTxBuffer)) {
            return (false);
         }
      }
      return (ReceiveFixed(Reply));
   } // ReceiveMatch

   //***************************************************************************
   // Receive fixed
   //***************************************************************************

   private bool ReceiveFixed(CString Reply)
      // Receive and compare with <Reply>
   {
      int Length = Reply.Length;
      byte[] Data = new byte[LPC_BUFFER_SIZE];

      Port.SetRxWait(RX_TIMEOUT);

      int RxSize = Port.Read(Data, Length);

      if(RxSize != Length) {
         return (false);
      }

      Data[RxSize] = (byte)'\0';

      FRxBuffer = new CString(Data, RxSize);

      if(Reply != FRxBuffer) {
         FlushRxChars();
         return (false);
      }
      return (true);
   } // ReceiveFixed

   //***************************************************************************
   // Receive Raw
   //***************************************************************************

   private bool ReceiveRaw(int ReqSize)
      // Receive <ReqSize> characters. <ReqSize> == 0 up to timeout
   {
      // read requested size :
      int RxSize;    // actual read size
      byte[] Data = new byte[LPC_BUFFER_SIZE];

      if(ReqSize > 0) {
         Port.SetRxWait(RX_TIMEOUT);
         RxSize = Port.Read(Data, ReqSize);
         
         if(RxSize != ReqSize) {
            return (false);
         }

         FRxBuffer = new CString(Data, RxSize);

         return (true);
      }
      
      // read any size
      Port.SetRxWait(RX_LINE_TIMEOUT);
      RxSize = Port.Read(Data, LPC_BUFFER_SIZE - 1);       // space for zero terminator
      
      if(RxSize == 0) {
         return (false);
      }

      Data[RxSize] = (byte)'\0';
      FRxBuffer = (CString)Data;

      return (true);
   } // ReceiveRaw

   //***************************************************************************
   // Flush
   //***************************************************************************

   private void FlushRxChars()
      // Flush chars up to intercharacter timeout
   {
      byte[] DataChunk = new byte[1];
      byte[] Data = new byte[FLUSH_DATA_SIZE];

      Port.SetRxWait(RX_INTERCHARACTER_TIMEOUT);

      for(int i = 0; i < FLUSH_DATA_SIZE; i++) {
         if(Port.Read(DataChunk, 1) == 0) {
            Data[i] = DataChunk[0];

            return;
         }
      }
   } // FlushRxChars

   //******************************************************************************
   // Check Connection
   //******************************************************************************

   private bool CheckConnect()
      // Check if adapter is ready
   {
      if(Port != null && Port.IsOpen) {
         return (true);
      }

      if(Port == null) {
         return (false);   
      }

      int Identifier = 0;
      if(Port.GetType() == typeof(UsbUart) && ComSettings.GetType() == typeof(Device.Uart.Config.UsbSNUartSettings)) {
         UsbUart Usb = (UsbUart)Port;
         Device.Uart.Config.UsbSNUartSettings Settings = (Device.Uart.Config.UsbSNUartSettings)ComSettings;

         if(!Usb.Locate(Settings.DeviceName, Settings.SerialNumber.ToString(), ref Identifier)) {
            return (false);
         }
      } else if(Port.GetType() == typeof(UsbUart) && ComSettings.GetType() == typeof(Device.Uart.Config.UsbUartSettings)) {
         UsbUart Usb = (UsbUart)Port;
         Device.Uart.Config.UsbUartSettings Settings = (Device.Uart.Config.UsbUartSettings)ComSettings;

         if(!Usb.Locate(Settings.DeviceName, ref Identifier)) {
            return (false);
         }
      } else if(Port.GetType() == typeof(ComUart) && ComSettings.GetType() == typeof(Device.Uart.Config.ComUartSettings)) {
         ComUart Com = (ComUart)Port;
         Device.Uart.Config.ComUartSettings Settings = (Device.Uart.Config.ComUartSettings)ComSettings;


         if(!Com.Locate(Settings.PortName, ref Identifier)) {
            return (false);
         }
      } else {
         return false;
      }

      /*if(ViaUsb) {
         // USB setup
         UsbUart Usb = new UsbUart();

         if(!Usb.Locate(DeviceName, ref Identifier)) {
            return (false);
         }

         Port = Usb;
      } else {
         // COM setup
         ComUart Com = new ComUart();
         if(!Com.Locate(DeviceName, ref Identifier)) {
            return (false);
         }
         Port = Com;
      }*/

      if(!Port.Open(Identifier)) {
         //Port = null;
         return (false);
      }

      // default parameters :
      Port.SetParameters(ComSettings.BaudRate, Uart.Parity.None, 8, Uart.StopBit.One, Uart.Handshake.None);

      // common init :
      Port.Dtr = false;
      Port.Rts = false;
      Port.SetRxNowait();
      Port.Flush();

      return (true);
   } // CheckConnect

} // LpcIsp