﻿using System;
using System.Threading;

public class Worker {
   
   public DateTime StartTime;

   //public delegate void StatusCallbackDel(string s);
   //public StatusCallbackDel StatusCallback = null;


   private double _Progress = 0;

   public double Progress {
      get {
         return _Progress;
      }

      protected set {
         _Progress = value;


      }
   }

   /*
   private string _Status = "";

   public string Status {
      get {
         return _Status;
      }
      
      protected set {
         _Status = value;

         if(StatusCallback != null) {
            StatusCallback(_Status);
         }
      }
   }
   */



   public Worker() {
   }
}

public class BackgroundOperation {
   public Thread Thread;
   public Worker Worker;
   public bool InProgress;
};