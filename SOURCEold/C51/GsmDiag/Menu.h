//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// Globalni promenne :

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit
// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_1SEC             0x04           // Zobrazi se periodicke veci 1sec (napr. cas)
#define ZOBRAZIT_VSE              0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

// Co se zobrazuje na displeji behem vazeni
typedef enum {
  ZOBRAZENI_NEVAZISE,
  ZOBRAZENI_CEKANI,
  ZOBRAZENI_VAZENI,
  ZOBRAZENI_STATISTIKA,
  ZOBRAZENI_HISTOGRAM,
  ZOBRAZENI_LOGGER,
  ZOBRAZENI_HISTORIE_STATISTIKA,        // Za tuto polozku nic nedavat, provadi se testy >= ZOBRAZENI_HISTORIE_STATISTIKA
  ZOBRAZENI_HISTORIE_HISTOGRAM,
  ZOBRAZENI_HISTORIE_LOGGER,
  _ZOBRAZENI_COUNT
} TZobrazeni;
extern TZobrazeni __xdata__ Zobrazeni;          // Co se ma prave zobrazovat




void MenuInit();
// Inicializace zobrazeni

void MenuRedraw(byte Zobrazit);
  // Obnovi zobrazeni


#endif
