//*****************************************************************************
//
//    GsmDiag.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __GsmDiag_H__
   #define __GsmDiag_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifdef __C51__
// jen pro Keil
//   typedef int  int16;
//   typedef long long32;
   #include <stddef.h>        // makro offsetof
#endif


extern byte __xdata__ SmsDebugValue;
extern byte xdata ReadTimeout;         // Timeout pri cteni zpravy
extern byte xdata ReadDebugValue;      // Vysledek fce SmsRead
extern word xdata TotalReadError;      // Pocet chybnych nacitani
extern word xdata TotalReadEmpty;      // Pocet spravnych nacitani, prazdna SMS
extern word xdata TotalReadOk;         // Pocet spravnych nacitani, nejaky obsah

extern byte xdata SendDebugValue;      // Vysledek fce SmsSend
extern byte xdata SmsSent;             // YES/NO byla odeslana odpoved

// Parametry GSM
#define GSM_NUMBER_MAX_LENGTH 15        // Maximalni delka telefonniho cisla bez zakoncovaci nuly, pozor, musi byt mensi nez GSM_MAX_NUMBER v Hardware.h
#define GSM_NUMBER_MAX_COUNT  5         // Maximalni pocet definovanych telefonnich cisel

//-----------------------------------------------------------------------------
#endif
