//*****************************************************************************
//
//    GsmCtl.h         Bat2 GSM control
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __GsmCtl_H__
   #define __GsmCtl_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#include "GsmDiag.h"


#define GSM_TIMER           5     // interval cteni zprav [s]
#define GSM_REPLY_DELAY     500   // Cekani v ms mezi prijetim SMS a odeslanim odpovedi - u nekterych SIM karet to bez cekani blbne a neodesle odpoved.
                                  // O pulnoci to SMS posle, ale na request to neodpovi.
                                  // Testy: 100ms a 200ms nefunguje, 300ms uz funguje.

// Stav GSM modulu
typedef enum {
  GSM_OFF,      // Modul ma vypnute napajeni
  GSM_POWERED,  // Modul ma zapnute napajeni, ale jeste nekomunikuje (nezdaril se reset)
  GSM_LIVE,     // Modul ma zapnute napajeni a komunikuje, ale jeste neni prihlasen do site
  GSM_READY     // Modul je prihlasen do site, muze se odesilat/prijimat
} TGsmStatus;
extern TGsmStatus xdata GsmStatus;

typedef enum {
  READ_EMPTY,
  READ_ERROR,
  READ_OK
} TReadStatus;
extern TReadStatus xdata ReadStatus;   // Stav nacitani SMS
extern char xdata SmsReadText[ SMS_MAX_LENGTH];
extern char xdata SmsReadNumber[GSM_NUMBER_MAX_LENGTH + 1];



void GsmInit( void);
// Inicializace modulu

void GsmStart();
// Zapnuti modulu

void GsmStop();
// Vypnuti modulu

void GsmExecute( void);
// Vybirani a provadeni doslych povelu (SMS)

void GsmCreateStatisticMsg(int DayNumber);
// Z aktualnich dat vytvori SMS zpravu
// Pri <DayNumber>:
//   >= 0 hleda stary den v archivu
//   <  0 posle aktualni den (musi byt zapnuto vazeni)

void GsmSetMidnightMsg();
// Nastavi automat na vyslani SMS o pulnoci

#endif
