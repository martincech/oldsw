//*****************************************************************************
//
//    KbdCtl.h - Obsluha klavesnice
//    Version 1.0
//
//*****************************************************************************

#ifndef __KbdCtl_H__
   #define __KbdCtl_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void ZerKlavesu();
// Spotrebovani klavesy, volat pro vystupu z menu kvuli spotrebovani timeoutu

void BeepKey();
// Pipnuti pri stisku klavesy (vyuziva modul displeje)

byte CekejKlavesu();
// Ceka na stisk klavesy

byte WaitForEnterEsc();
// Ceka na stisk tlacitek Enter nebo Esc

void WaitForEnter();
// Ceka na stisk tlacitka Enter

#endif
