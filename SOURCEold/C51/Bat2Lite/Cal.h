//*****************************************************************************
//
//    Cal.h    - Calibration load/save
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Cal_H__
   #define __Cal_H__

#include "Hardware.h"     // pro podminenou kompilaci

extern float __xdata__ CalConst;          // Predpocitana konstanta

void CalLoad(void);
// Nacte kalibraci z EEPROM
// Nekontroluje zadne checksumy a nenastavuje default hodnoty

void CalCalculate(void);
// Vypocte konstantu z kalibrace. Volat pri startu vahy (po nacteni kalibrace) a vzdy po kalibraci.

TYesNo CalSave(void);
// Ulozi celou kalibraci do EEPROM
// Vraci NO, nepovedl-li se zapis

#endif
