//*****************************************************************************
//
//   NvRam.h        NVRAM application map
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __NvRam_H__
  #define __NvRam_H__

#include "..\inc\Ds17287.h"    // NVRAM
#include "Bat2.h"

//-----------------------------------------------------------------------------
// Struktura NVRAM
//-----------------------------------------------------------------------------

#define NVR_SPARE (RTC_NVRAM_SIZE - sizeof(TArchive) - sizeof(byte))

typedef struct {
  TArchive    Archive;                    // Statistika
  byte        ArchiveChecksum;            // Kontrolni soucet polozky Archive

  byte        Spare[ NVR_SPARE];
} TNvrData;  // 211 + 1 + 59 + 1 = 272 + NVR_SPARE bajtu




#endif  // __NvRam_H__
