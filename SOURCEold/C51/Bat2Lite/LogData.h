//*****************************************************************************
//
//   LogData.h
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __LogData_H__
  #define __LogData_H__

#include "..\inc\Uni.h"
//#include "..\inc\Stat.h"

// Popisovac statistiky - pomoci zakladnich datovych typu (bez TNumber)
// Pokud dam zde include hardware.h (kde je definovan TNumber), dojde k zacykleni Hardware.h - LogData.h - Stat.h
typedef struct {
   float XSuma;
   float X2Suma;
   word  Count;
} TStatistic2;  // 10 bajtu


// ---------------------------------------------------------------------------------------------
// Datum a cas
// ---------------------------------------------------------------------------------------------

// Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
typedef struct {
  byte Min;
  byte Hour;
  byte Day;
  byte Month;
  word Year;
} TLongDateTime;  // 6 bajtu

//-----------------------------------------------------------------------------
// Struktura archivu
//-----------------------------------------------------------------------------

// narazniky :

#define FL_ARCHIVE_EMPTY    0xFE
#define FL_ARCHIVE_FULL     0xFF

typedef struct SArchive {
   word          DayNumber;         // Cislo dne od pocatku vykrmu
   TLongDateTime DateTime;          // Datum dne
   TStatistic2   Stat;              // Statistika
   word          LastAverage;       // Prumerna vcerejsi hmotnost (pro vypocet daily gain)
   word          TargetWeight;      // Normovana hmotnost pro tento den
} TArchive;        // 16 bajtu - pozor, pri prekroceni 255 bajtu predefinovat promennou v SaveContext() a LoadContext()

//-----------------------------------------------------------------------------
// Konstanty pro ukladani do FIFO
//-----------------------------------------------------------------------------

#define BAT2LITE_FIFO_START         (sizeof(TArchive) + 1)   // Pocatecni adresa FIFO v RAM - musi byt vetsi nez sizeof(TArchive) + 1
#define BAT2LITE_FIFO_CAPACITY      56                       // Maximalni pocet ukladanych polozek
#define BAT2LITE_FIFO_MARKER_EMPTY  FL_ARCHIVE_EMPTY         // Znacka konce pri neuplnem zaplneni
#define BAT2LITE_FIFO_MARKER_FULL   FL_ARCHIVE_FULL          // znacka konce pri prepisovani

#endif  // __LogData_H__
