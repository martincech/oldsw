//*****************************************************************************
//
//    TestFifo.c - Testovaci podprogramy pro test Fifa archivu
//    Version 1.0, (c) P.Veit
//
//*****************************************************************************

#ifndef __TestFifo_H__
   #define __TestFifo_H__

void TestFifoExecute(void);
// Zaplni Logger i Archive
// Je treba zacinat s prazdnym archivem a nastartovanym vazenim, na datumu nezalezi.

#endif
