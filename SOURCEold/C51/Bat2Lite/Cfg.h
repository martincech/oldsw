//*****************************************************************************
//
//    Cfg.h - Configuration load/save
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Cfg_H__
   #define __Cfg_H__

#include "Hardware.h"     // pro podminenou kompilaci

TYesNo CfgLoad(void);
// Nacte konfiguraci z Flash
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)

TYesNo CfgSave( dword mask);
// Ulozi polozky definovane maskou do Flash
// Vraci NO, nepovedl-li se zapis

byte CfgCalcChecksum(byte __xdata__ *cfg);
// Spocita a vrati Kontrolni soucet konfigurace zadane v <cfg>

#endif
