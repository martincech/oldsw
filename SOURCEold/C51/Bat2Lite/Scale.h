//*****************************************************************************
//
//    Scale.h - Naslapne vahy
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Scale_H__
   #define __Scale_H__

#include "Hardware.h"           // zakladni datove typy

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Popisovac vahy :
typedef struct {
//   long       Sum;                      // Suma prevodu pro prumerovani
//   byte       Count;                    // Pocet vzroku v sume
   byte       DiscardNextStableWeight;  // Flag, ze se ma nasledujici stabilni hmotnost zahodit

   long       Weight;                   // Aktualni okamzita hmotnost na vaze
//   byte       NewWeight;                // Flag, ze se nacetla nova aktualni hmotnost
   long       LastSavedWeight;          // Posledni ulozena hmotnost

   // Detekce nastupu
//   TAverage   Average;                  // Klouzavy prumer
//   long       LastStableWeight;         // Hodnota posledni ustalene hmotnosti
//   byte       Stable;                   // Flag, ze je vaha ustalena
} TScale;

// Popisovac vah :

extern TScale __xdata__ Scale;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void ScaleInit( void);
// Inicializace

void ScaleInitFilter(void);
  // Inicializace filtru pro aktualni den vazeni podle aktualni konfigurace a kalibrace.
  // Volat:
  //   - Pri zapnuti vahy (zajistuje fce ScaleInit + FlockUpdateDay - vola se 2x!)
  //   - Pri zahajeni vazeni (zajistuje fce FlockUpdateDay)
  //   - Pri zmene dne behem vazeni (o pulnoci) (zajistuje fce FlockUpdateDay)
  //   - Pri zmene parametru ukladani (zajistuje fce FlockUpdateDay)
  //   - Po kalibraci (zajistuje fce CalibrateScale)

void ScaleInitAverage(void);
// Inicializace klouzaveho prumeru podle konfigurace

TYesNo ScaleExecute( void);
// Pokud vaha detekuje novy nastup, ulozi hmotnost do struktury a vrati YES.
// Automaticky nastavuje promennou Display

void ScaleEnable( void);
// Povoleni vazeni

void ScaleDisable( void);
// Zakaz vazeni

long ScaleReadConversionAverage(byte Count);
// Nacte <Count> prevodu a vrati prumer

void ScaleReadActualWeight(void);
// Nacte aktualni hmotnost a ulozi ji do <Scale.Weight>


#endif
