//*****************************************************************************
//
//    Bat2.h - common data definitions for Bat2 Lite
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Bat2_H__
   #define __Bat2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#include "LogData.h"                    // TLongDateTime

#ifdef __C51__
  // Jen pro Keil
  #include <stddef.h>        // makro offsetof
  #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#else
  // Jen Builder
  typedef int   int32;
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERSION                     0x0102      // verze FW
#define BUILD                       0           // Cislo buildu (pro zmene buildu se nemeni vlastnosti vahy a neprovadi se CfgReset)

#define SCALE_CAPACITY              99999       // Nosnost vah
#define SCALE_DECIMALS              3           // Pocet desetin
#define MAX_WEIGHT_CALIBRATION      200000      // Maximalni hmotnost zadavana pri kalibraci
#define MAX_WEIGHT_DISPLAY          200000      // Maximalni zobrazitelna hmotnost
#define MIN_WEIGHT_DISPLAY          -99999      // Minimalni zobrazitelna hmotnost

typedef enum {
  UNITS_KG,
  UNITS_LB,
  _UNITS_COUNT
} TUnits;

typedef enum {
  BACKLIGHT_OFF,
  BACKLIGHT_ON,
  BACKLIGHT_AUTO,
  _BACKLIGHT_COUNT
} TBacklight;

// Typ automatickeho zjistovani cilove hmotnosti
typedef enum {
  AUTOMODE_WITHOUT_GAIN,    // Cilova hmotnost se vypocte jako prumer ze vcerejska
  AUTOMODE_WITH_GAIN,       // Cilova hmotnost se vypocte jako prumer + denni prirustek ze vcerejska
  _AUTOMODE_COUNT
} TAutoMode;

// Typ naskoku/seskoku na vahu
typedef enum {
  JUMPMODE_ENTER,           // Vyhodnocuje se pouze naskok na vahu
  JUMPMODE_LEAVE,           // Vyhodnocuje se pouze seskok z vahy
  JUMPMODE_BOTH,            // Vyhodnocuje se naskok i seskok z vahy
  _JUMPMODE_COUNT
} TJumpMode;

// Hardwarova verze
typedef enum {
  HW_VERSION_BASIC,         // Bat2 Basic
  HW_VERSION_LITE,          // Bat2 Lite
  _HW_VERSION_COUNT
} THwVersion;

// ---------------------------------------------------------------------------------------------
// Default hodnoty nastaveni (cte i PC)
// ---------------------------------------------------------------------------------------------

#define DEFAULT_FEMALES_MARGIN_ABOVE    30
#define DEFAULT_FEMALES_MARGIN_BELOW    30
#define DEFAULT_MALES_MARGIN_ABOVE      30
#define DEFAULT_MALES_MARGIN_BELOW      30
#define DEFAULT_FILTER                  12     // 1.5sec (7.5Hz * 12)
#define DEFAULT_STABILIZATION_RANGE     30     // +-3.0%
#define DEFAULT_STABILIZATION_TIME      3      // 3 vzorky
#define DEFAULT_AUTO_MODE               AUTOMODE_WITH_GAIN
#define DEFAULT_JUMP_MODE               JUMPMODE_BOTH
#define DEFAULT_UNITS                   UNITS_KG
#define DEFAULT_HISTOGRAM_RANGE         40     // +-40%, izraelci maji +-45%
#define DEFAULT_UNIFORMITY_RANGE        10     // +-10%
#define DEFAULT_GSM_USE                 NO
#define DEFAULT_GSM_SEND_MIDNIGHT       NO
#define DEFAULT_GSM_PERIOD_MIDNIGHT     1
#define DEFAULT_GSM_SEND_REQUEST        NO
#define DEFAULT_GSM_EXECUTE_REQUEST     NO
#define DEFAULT_GSM_CHECK_NUMBERS       NO
#define DEFAULT_GSM_NUMBER_COUNT        0
#define DEFAULT_RS485_ADDRESS           2
#define DEFAULT_RS485_SPEED             9600
#define DEFAULT_RS485_PARITY            COM_PARITY_EVEN
#define DEFAULT_RS485_REPLY_DELAY       0
#define DEFAULT_RS485_SILENT_INTERVAL   5

// ---------------------------------------------------------------------------------------------
// Meze hodnot v nastaveni
// ---------------------------------------------------------------------------------------------

#define MIN_TARGET_WEIGHT           30      // Minimalni pripustna cilova hmotnost, pokud vyjde mensi, koriguju
#define MAX_TARGET_WEIGHT           0xFFFF  // Maximalni pripustna cilova hmotnost

#define MIN_IDENTIFICATION_NUMBER   1       // Minimalni hodnota identifikacniho cisla
#define MAX_IDENTIFICATION_NUMBER   999     // Maximalni hodnota identifikacniho cisla

#define MIN_HISTOGRAM_RANGE         10      // Minimalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu
#define MAX_HISTOGRAM_RANGE         100     // Maximalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu (+-100% dava rozsah 0 az 2xstred)
#define MIN_UNIFORMITY_RANGE        1       // Minimalni hodnota rozsahu uniformity v procentech
#define MAX_UNIFORMITY_RANGE        49      // Maximalni hodnota rozsahu uniformity v procentech (+-49% dava 98% celkem)

#define MIN_MARGIN                  0       // Minimalni hodnota okoli rustove krivky
#define MAX_MARGIN                  99      // Maximalni hodnota okoli rustove krivky
#define MIN_FILTER                  1       // Minimalni hodnota filtru
#define MAX_FILTER                  30      // Maximalni hodnota filtru (pouziva se zadany filtr * FILTER_MULTIPLIER)
#define MIN_STABILIZATION_RANGE     0       // Minimalni hodnota ustaleni
#define MAX_STABILIZATION_RANGE     250     // Maximalni hodnota ustaleni (+-25.0)
#define MIN_STABILIZATION_TIME      1       // Minimalni hodnota doby ustaleni
#define MAX_STABILIZATION_TIME      9       // Maximalni hodnota doby ustaleni (pouziva se zadany cas * STABILIZATION_TIME_MULTIPLIER)

#define MIN_STABILIZATION_RANGE_G   5       // Minimalni hodnota ustaleni v gramech

// Pri nastavovani filtrace pouzivam hodnoty zadane uzivatelem nasobene nasledujicimi koeficienty:
#define FILTER_MULTIPLIER           2       // Do filtru nastavuji hodnotu FILTER_MULTIPLIER * Config.Filter
#if (FILTER_MULTIPLIER * MAX_FILTER > FILTER_MAX_AVERAGING)
  error "Bat2.h: FILTER_MULTIPLIER * MAX_FILTER > FILTER_MAX_AVERAGING"
#endif
#define STABILIZATION_TIME_MULTIPLIER   4   // Do filtru nastavuji hodnotu STABILIZATION_TIME_MULTIPLIER * Config.StabilizationTime
#if (STABILIZATION_TIME_MULTIPLIER * MAX_STABILIZATION_TIME > 255)
  error "Bat2.h: STABILIZATION_TIME_MULTIPLIER * MAX_STABILIZATION_TIME > 255"
#endif

#define MIN_GSM_PERIOD_MIDNIGHT     1       // Minimalni hodnota periody vysilani pulnocnich SMS ve dnech
#define MAX_GSM_PERIOD_MIDNIGHT     99      // Maximalni hodnota periody vysilani pulnocnich SMS ve dnech

#define MIN_RS485_ADDRESS           1       // Minimalni hodnota adresy
#define MAX_RS485_ADDRESS           247     // Maximalni hodnota adresy
#define MIN_RS485_REPLY_DELAY       0       // Minimalni hodnota zpozdeni odpovedi
#define MAX_RS485_REPLY_DELAY       9999    // Maximalni hodnota zpozdeni odpovedi
#define MIN_RS485_SILENT_INTERVAL   1       // Minimalni hodnota prodlevy mezi dvema MODBUS pakety
#define MAX_RS485_SILENT_INTERVAL   255     // Maximalni hodnota prodlevy mezi dvema MODBUS pakety

// ---------------------------------------------------------------------------------------------
// Zobrazeni
// ---------------------------------------------------------------------------------------------

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Display
#define DISPLAY_CURRENT_WEIGHT      0x01           // Zobrazi se naslapna vaha po vypocteni nove aktualni hmotnosti
#define DISPLAY_SAVED_WEIGHT        0x02           // Zobrazi se naslapna vaha po ulozeni nove hmotnosti
#define DISPLAY_1SEC                0x04           // Zobrazi se periodicke veci 1sec (napr. cas)
#define DISPLAY_GRID                0x80           // Zobrazi se mrizka (kostra)
#define DISPLAY_ALL                 0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

extern byte __xdata__ Display;      // Co mam v nasledujicim kroku prekreslit


// Globalni datum a cas
extern TLongDateTime __xdata__ ActualDateTime;

// ---------------------------------------------------------------------------------------------
// Rustova krivka
// ---------------------------------------------------------------------------------------------

#define CURVE_MAX_POINTS        30          // Max pocet bodu s definici rustove krivky
#define CURVE_MAX_DAY           999         // Max cislo dne
#define CURVE_MAX_WEIGHT        0xFFFF      // Max hmotnost (aby se vlezla do 2 bajtu)
#define CURVE_END_WEIGHT        0           // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)
#define CURVE_FIRST_DAY_NUMBER  0           // Cislo prvniho dne, od ktereho se zacina krmit (= minimalni cislo dne, ktere lze pri zahajeni vazeni zadat)

#define TargetWeightOK(Weight) (Weight >= MIN_TARGET_WEIGHT && Weight <= CURVE_MAX_WEIGHT)

// Format bodu rustove krivky
typedef struct {
  word Day;         // 0..CURVE_MAX_DAY (999)
  word Weight;      // 0..CURVE_MAX_WEIGHT (0xFFFF)
} TCurvePoint;  // 4 bajty

// Pole 1 krivky
typedef TCurvePoint TCurvePoints[CURVE_MAX_POINTS];  // 120 bajtu

//-----------------------------------------------------------------------------
// Hejna
//-----------------------------------------------------------------------------

#define FLOCK_NAME_MAX_LENGTH       8           // Maximalni delka
#define FLOCK_EMPTY_NUMBER          0xFF        // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane
#define FLOCK_TIME_LIMIT_EMPTY      0xFF        // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale
#define FLOCK_TIME_LIMIT_MAX_HOUR   23          // Maximalni hodnota hodiny pro omezeni (0-23hod)

// Rozdeleni pohlavi
typedef enum {
  GENDER_FEMALE = 0,                            // Pohlavi samice (pokud se pouziva jen 1 pohlavi, jsou vzdy platne parametry pro samice)
  GENDER_MALE,                                  // Pohlavi samec
  _GENDER_COUNT
} TGender;

#define QUICK_WEIGHING_TITLE        "-       "  // Default nazev hejna pri rychlem vazeni - pozor, musi mit FLOCK_NAME_MAX_LENGTH znaku

// Zahlavi hejna
typedef struct {
  byte Number;                                  // Cislo hejna - pokud je hejno definovane, je rovno indexu hejna (0 - 9)
  byte Title[FLOCK_NAME_MAX_LENGTH];            // Nazev hejna (bez ukoncovaci nuly, prazdne znaky se doplni mezerami)
  byte UseCurves;                               // Zda se maji pouzivat rustove krivky nebo automaticke zjistovani cilove hmotnosti bez pouziti rustovych krivek
                                                // Pokud krivky nepouziva, jsou zadane pocatecni hmotnosti ulozene jako 1. bod ve krivce
  byte UseBothGenders;                          // Flag, zda se ma pouzivat rozliseni na samce a samice
  byte WeighFrom;                               // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se
  byte WeighTill;                               // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se
} TFlockHeader;  // 13 bajtu

// Struktura jednoho hejna
typedef struct {
  TFlockHeader  Header;
  TCurvePoints  GrowthCurve[_GENDER_COUNT];     // Rustova krivka pro samice (pripadne oboje) a pro samce
} TFlock; // 253 bajtu

#define FLOCKS_MAX_COUNT            10          // Pocet hejn, ktere muze zadat
typedef TFlock TFlocks[FLOCKS_MAX_COUNT];       // Cisla hejn odpovidaji pozici, tj. na pozici 0 je hejno cislo 0, na pozici 1 hejno cislo 1 atd.

// Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
// veci, ktere jsou stale potreba a v prubehu dne se nemeni, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake
// zmene v EEPROM, pri prechodu na dalsi den atd.
// Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu
typedef struct {
  word          DayNumber;                      // Cislo dne od pocatku vykrmu
  word          TargetWeight;    // Normovana hmotnost nactena z rustove krivky pro samice a samce
  TRawValue     MarginAbove;     // Horni mez hmotnosti samic a samcu
  TRawValue     MarginBelow;     // Dolni mez hmotnosti samic a samcu
} TWeighing;  // XXX bajtu

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

typedef struct {
  int32     ZeroCalibration;                    // Kalibrace nuly
  int32     RangeCalibration;                   // Kalibrace rozsahu
  int32     Range;                              // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
  byte      Division;                           // Dilek vah
} TCalibration; // 13 bajtu

//-----------------------------------------------------------------------------
// Korekcni krivka
//-----------------------------------------------------------------------------

typedef struct {
  word Day1;                        // Den prvniho zlomu, do tohoto dne je korekce nulova
  word Day2;                        // Den druheho zlomu, v tomto dni a dale se uplatni zadana korekce <Correction>
  byte Correction;                  // Korekce v procentech v den Day2. Pokud je hodnota 0, korekce se neuplatnuje
} TWeightCorrection; // 5 bajtu

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

// Parametry, ktere zadal pri zahajeni vykrmu. Tyto parametry se v celem prubehu vazeni nemeni.
// Po skonceni vykrmu se zde drzi vsechny parametry posledniho vykrmu az do doby odstartovani dalsiho vykrmu. Parametry odtut muze tedy vyuzivat
// historie i po ukonceni vykrmu.
typedef struct {
  byte           Running;            // Flag, ze prave probiha krmeni
  byte           UseCurve;           // Flag, zda zvolil krmeni podle rustove krivky. Pokud ne, zadal zakladni parametry primo a vazi se bez pouziti hejna, za pouziti parametru v TRychlyVykrm.
  word           CurveDayShift;      // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.
  TLongDateTime  DateTime;           // Datum zahajeni vykrmu
  word           InitialWeight;      // Zadana normovana hmotnost pro pocatecni den vykrmu pri UseFlock = NO
} TWeighingStart;  // 12 bajtu

#define CONFIG_RESERVED_SIZE    8                   // Pocet rezervovanych bajtu v TConfig

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word              Version;                        // VERSION
  byte              Build;                          // BUILD
  THwVersion        HwVersion;                      // Verze hardware (GSM, Dacs LW1, Modbus, ...)
  byte              Language;                       // Jazykova verze + jazyk

  byte              MarginAbove;                    // Okoli nad prumerem pro samice a samce
  byte              MarginBelow;                    // Okoli pod prumerem pro samice a samce
  byte              Filter;                         // Filtr prevodniku
  byte              StabilizationRange;             // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
  byte              StabilizationTime;              // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
  TAutoMode         AutoMode;                       // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)
  TJumpMode         JumpMode;                       // Typ naskoku/seskoku na vahu, ktery se vyhodnocuje

  TUnits            Units;                          // Zobrazovane jednotky

  TCurvePoints      GrowthCurve;                    // Rustova krivka

  TWeighingStart    WeighingStart;                  // Parametry vazeni

  TBacklight        Backlight;                      // Nastaveny rezim podsvitu

  TWeightCorrection WeightCorrection;               // Korekce hmotnosti

  byte              Reserved[CONFIG_RESERVED_SIZE]; // Pro budouci pouziti

  byte              Checksum;                       // Kontrolni soucet
} TConfig;  // 160 bajtu, pri provedeni zmen zkontrolovat fci CfgSaveConfigFromModule()

extern TConfig      __xdata__ Config;           // Buffer konfigurace v externi RAM
extern TCalibration __xdata__ Calibration;      // Buffer kalibrace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast

#define CFG_ALL                          0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERSION                      0x00000001L
#define CFG_BUILD                        0x00000002L
#define CFG_HW_VERSION                   0x00000004L
#define CFG_MARGIN_ABOVE                 0x00000008L
#define CFG_MARGIN_BELOW                 0x00000010L
#define CFG_FILTER                       0x00000020L
#define CFG_STABILIZATION_RANGE          0x00000040L
#define CFG_STABILIZATION_TIME           0x00000080L
#define CFG_AUTO_MODE                    0x00000100L
#define CFG_JUMP_MODE                    0x00000200L
#define CFG_GROWTH_CURVE                 0x00000400L
#define CFG_WEIGHING_START               0x00000800L
#define CFG_UNITS                        0x00001000L
#define CFG_LANGUAGE                     0x00002000L
#define CFG_BACKLIGHT                    0x00004000L
#define CFG_WEIGHT_CORRECTION            0x00008000L

//-----------------------------------------------------------------------------
// Struktura interni EEPROM
//-----------------------------------------------------------------------------

#define IEP_SPARE (IEP_SIZE - sizeof(TConfig) - sizeof(TCalibration))

typedef struct {
  TConfig       Config;             // Konfigurace
  TCalibration  Calibration;        // Kalibrace vah
  byte          Dummy[IEP_SPARE];
} TIep;

//-----------------------------------------------------------------------------
#endif
