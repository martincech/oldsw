//*****************************************************************************
//
//    Lang.h      - Definice jazykove verze
//    Version 1.0
//
//*****************************************************************************

#ifndef __Lang_H__
   #define __Lang_H__

#include "Hardware.h"     // zakladni datove typy
#include "Bat2.h"         // _JEDNOTKY_POCET

// Jazykova verze - definuje, o jakou jazykovou verzi vah se jedna
#define LANG_VERSION_FR     1       // En + Fr

// Logo zobrazene po startu
//#define LOGO_VEIT               1       // Standardni logo "poultry scale bat2"
//#define LOGO_BROILERTEKKNIIKA   1       // Logo Breilertekkniika Finsko
//#define LOGO_DACS               1       // Logo pro Dacs Dansko
//#define LOGO_SWEDEGG            1       // Logo pro Swedegg Svedsko
//#define LOGO_SODALEC            1       // Logo Sodalec Francie
//#define LOGO_SUBSAHARAHSCALES   1       // Logo pro Sub Saharah Scales v Jizni Africe
//#define LOGO_VERTA_TEKNO        1       // Logo pro Werta Tekno Turecko
//#define LOGO_YDE                1       // Logo pro Yde Belgie
//#define LOGO_CHOOKS-nepouzivat  1       // Logo pro Aussie Chooks Equipment v Australii
//#define LOGO_ACE                1       // Logo pro ACE Integrated Solutions v Australii
#define LOGO_FARMWEIGHSYSTEMS   1       // Logo pro Farm Weigh Systems v USA (Dwain)
//#define LOGO_BIGDUTCHMAN        1       // Logo Big Dutchman - POVOLIT SLIDING AVG


// Masky na dekodovani jazykove verze a jazyka
#define LANG_VERSION_MASK   0xE0        // Jazykova verze v hornich 3 bitech
#define LANG_MASK           0x1F        // Jazyk ve spodnich 5 bitech

// Nastaveni podle zvolene jazykove verze
#ifdef LANG_VERSION_FR
  #define LANG_VERSION      LANG_VERSION_FR     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_FRENCH,
    LANG_GERMAN,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

// ---------------------------------------------------------------------------------------------
// Definice jazyka
// ---------------------------------------------------------------------------------------------

extern byte code STR_LANGUAGE[];
extern byte code STR_MENU_LANGUAGE[];

// ---------------------------------------------------------------------------------------------
// HW verze
// ---------------------------------------------------------------------------------------------

extern byte code STR_HW_VERSION[];

// ---------------------------------------------------------------------------------------------
// Menu
// ---------------------------------------------------------------------------------------------

extern byte code STR_DAY[];
extern byte code STR_WEIGHT[];
extern byte code STR_TARGET_WEIGHT[];
extern byte code STR_LAST_SAVED_WEIGHT[];
extern byte code STR_OK[];
extern byte code STR_CANCEL[];
extern byte code STR_RETRY[];
extern byte code STR_YES[];
extern byte code STR_NO[];
extern byte code STR_WAIT[];

// Jednotky mam zvlast pro vyber v menu a pro zobrazeni (kvuli jednoduchosti)
extern byte code STR_UNITS_MENU[];    // Pro menu
extern byte code STR_UNITS[_UNITS_COUNT][3];

// Dilek v menu
extern byte code STR_DIVISION_MENU[];

extern byte code STR_INVALID_VALUE[];
extern byte code STR_SAVE_CHANGES[];
extern byte code STR_STOP_WEIGHING_FIRST[];

// Logger
extern byte code STR_NO_DATA[];

// Editace hejna
extern byte code STR_STOP_WEIGHING[];
extern byte code STR_STARTING_DAY[];

// Editace datumu a casu
extern byte code STR_DATE[];
extern byte code STR_TIME[];

// Parametry naslapne vahy
extern byte code STR_SCALE_MARGIN_ABOVE[];
extern byte code STR_SCALE_MARGIN_BELOW[];
extern byte code STR_SCALE_FILTER[];
extern byte code STR_SCALE_STABILIZATION_RANGE[];
extern byte code STR_SCALE_STABILIZATION_TIME[];
extern byte code STR_SCALE_AUTO_MODE_TYPE[];
extern byte code STR_JUMP_MODE[];
extern byte code STR_ENTER_LEAVE_BOTH[];

// Kalibrace
extern byte code STR_CALIBRATION_UNITS[];
extern byte code STR_CALIBRATION_DIVISION[];
extern byte code STR_CALIBRATION_ZERO[];
extern byte code STR_CALIBRATION_RANGE[];

// Podsvit
extern byte code STR_BACKLIGHT[];

extern byte code STR_ON_OFF_AUTO[];

//-----------------------------------------------------------------------------
// Hlavni Menu
//-----------------------------------------------------------------------------

extern byte code STR_MAIN_MENU_IDLE_BASIC[];
extern byte code STR_MAIN_MENU_IDLE_LITE[];

extern byte code STR_MAIN_MENU_WEIGHING_BASIC[];
extern byte code STR_MAIN_MENU_WEIGHING_LITE[];

// Struktura hlavniho menu
enum {
  ID_MAIN_MENU_START_STOP,
  ID_MAIN_MENU_ARCHIVE,
  ID_MAIN_MENU_SETUP,
  _ID_MAIN_MENU_COUNT
};

#define _ID_MAIN_MENU_COUNT_BASIC       (_ID_MAIN_MENU_COUNT - 1)     // Pocet polozek hlavniho menu ve verzi Basic
#define _ID_MAIN_MENU_COUNT_LITE         _ID_MAIN_MENU_COUNT          // Pocet polozek hlavniho menu ve verzi Lite

//-----------------------------------------------------------------------------
// Menu nastaveni
//-----------------------------------------------------------------------------

extern byte code STR_MENU_SETUP_TITLE[];

extern byte code STR_MENU_SETUP[];

// Struktura menu nastaveni
enum {
  ID_MENU_SETUP_DATE_TIME,
  ID_MENU_SETUP_GROWTH_CURVE,
  ID_MENU_SETUP_CORRECTION_CURVE,
  ID_MENU_SETUP_SAVING_PARAMETERS,
  ID_MENU_SETUP_BACKLIGHT,
  ID_MENU_SETUP_CALIBRATION,
  _ID_MENU_SETUP_COUNT
};

//-----------------------------------------------------------------------------
// Editace hejn
//-----------------------------------------------------------------------------

extern byte code STR_USE_CURVE[];

extern byte code STR_INITIAL_WEIGHT[];

// ---------------------------------------------------------------------------------------------
// Editace rustove krivky
// ---------------------------------------------------------------------------------------------

extern byte code STR_SAVE[];
extern byte code STR_DELETE[];
extern byte code STR_QUIT[];
extern byte code STR_NEXT[];
extern byte code STR_ADD[];
extern byte code STR_CHANGE[];
extern byte code STR_MOVE[];
extern byte code STR_DAY_COLON[];
extern byte code STR_WEIGHT_COLON[];
extern byte code STR_CURVE_FULL[];

extern byte code STR_CURVE_DAY_EXISTS[];
extern byte code STR_CURVE_DELETE[];

extern byte code STR_CURVE_EMPTY[];

// ---------------------------------------------------------------------------------------------
// Editace korekcni krivky
// ---------------------------------------------------------------------------------------------

extern byte code STR_CORRECTION_DAY1[];
extern byte code STR_CORRECTION_DAY2[];
extern byte code STR_CORRECTION_CORRECTION[];

#endif  // __Lang_H__
