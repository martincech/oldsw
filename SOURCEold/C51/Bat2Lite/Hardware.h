//*****************************************************************************
//
//    Hardware.h  - BAT2 Lite hardware descriptions
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************


#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof

#include "LogData.h"


//-----------------------------------------------------------------------------
// Lowbat
//-----------------------------------------------------------------------------

#define BatAccuOk()  BatLOWBAT
#define PowerFail()  !BatLOWBAT

//-----------------------------------------------------------------------------
// Verze desky - je treba zahrnout/zakomentovat moduly v makefile
//-----------------------------------------------------------------------------

//#define BOARD_VERSION_1     1       // Deska verze 1
#define BOARD_VERSION_2     1       // Deska verze 2

//-----------------------------------------------------------------------------
// Hardwarove verze - je treba zahrnout/zakomentovat moduly v makefile
//-----------------------------------------------------------------------------

//#define VERSION_BASIC       1       // Bat2 Basic
#define VERSION_LITE        1       // Bat2 Lite

//-----------------------------------------------------------------------------
// Prirazeni portu
//-----------------------------------------------------------------------------

#ifdef BOARD_VERSION_1
// Deska verze 1 --------------

// datova sbernice :
#define BatDATA P0
sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :
sbit BatCSRTC    = P1^0;
sbit BatRDRTC    = P1^1;
sbit BatWRRTC    = P1^2;
sbit BatALERTC   = P1^3;
sbit BatCSEE     = P1^4;               // CS flash
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;

// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// Port P2 :
sbit BatLOWBAT   = P2^0;
sbit BatZAPGSM   = P2^1;
sbit BatPISK     = P2^2;
sbit BatK0       = P2^3;
sbit BatK1       = P2^4;
sbit BatRES      = P2^5;               // RES display
sbit BatCS2      = P2^6;               // CS1 display
sbit BatCS1      = P2^7;               // CS2 display

// Port P3 :
sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatCSMOD    = P3^2;               // CS flash modulu
sbit BatDRAD     = P3^3;               // DRDY AD1241
sbit BatCLKAD    = P3^4;               // CLK  AD1241
sbit BatK2       = P3^5;
sbit BatSVIT     = P3^6;               // podsvit
sbit BatDATAAD   = P3^7;               // DOUT AD1241

// Kanal vahy :
#define WEIGHT_CHANNEL   0             // cislo kanalu A/D pro vahu

#elif BOARD_VERSION_2
// Deska verze 2 --------------

// datova sbernice :
#define BatDATA P0
sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :
sbit BatCLKAD    = P1^0;               // CLK  ADS1230
sbit BatPDWNAD   = P1^1;               // PDWN ADS1230
sbit BatLOWBAT   = P1^2;
sbit BatCSMOD    = P1^3;               // CS flash modulu
sbit BatCSEE     = P1^4;               // CS flash
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;

// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// Port P2 :
sbit BatK2       = P2^0;
sbit BatRES      = P2^1;               // RES display
sbit BatCS2      = P2^2;               // CS1 display
sbit BatCS1      = P2^3;               // CS2 display
sbit BatRDRTC    = P2^4;
sbit BatWRRTC    = P2^5;
sbit BatCSRTC    = P2^6;
sbit BatALERTC   = P2^7;

// Port P3 :
sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatDATAAD   = P3^2;               // DRDY/DOUT ADS1230
sbit BatSVIT     = P3^3;               // podsvit
sbit BatIGTGSM   = P3^4;
sbit BatZAPGSM   = P3^5;
sbit BatK0       = P3^6;
sbit BatK1       = P3^7;

// Kanal vahy :
#define WEIGHT_CHANNEL                // Nepouziva se

#endif // BOARD_VERSION_2

typedef long TRawValue;                // surova hodnota vazeni

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
// Pozor, pokud se pouziva dynamicke rizeni CPU_X2, vsechny ostatni zarizeni musi byt v X1 modu (T0_X2, T1_X2 atd. musi byt nastaveny na 1). Pokud
// jsou ostatni zarizeni nastavene v X2 modu, po prepnuti procesoru do X1 modu se prepnou soucasne i zarizeni. Napr. SPI je stale v X2 a prepina se do X1.
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2 (zatim neni implementovano, bezi stale v X2 na max. rychlost)

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE_T1  1          // zdvojena baudova rychlost pro timer 1

// Casovac 0 :
#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  BatAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

//-----------------------------------------------------------------------------
// A/D prevodnik ADS1241
//-----------------------------------------------------------------------------

#ifdef BOARD_VERSION_1                  // ADS1241 je jen na desce verze 1

#define AdcDIN         BatDATAAD       // vstup
#define AdcDOUT        BatDATAAD       // vystup
#define AdcSCLK        BatCLKAD        // hodiny
#define AdcDRDY        BatDRAD         // -ready

#define AdcClrCS()                     // chipselect vyber
#define AdcSetCS()                     // deselect

#define ADC_GAIN       ADC_GAIN_2      // zakladni zesileni
#define ADC_RATE       ADC_7_5HZ       // rychlost vzorkovani
#define ADC_POLARITY   ADC_BIPOLAR     // rezim polarity
#define ADC_RANGE      ADC_HALF_RANGE  // plny/polovicni rozsah

#define ADC_INTERRUPT  INT_EXTERNAL1   // rezim na pozadi, vstup preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

#define ADC_PREFILTER  3               // pocet vzorku pro prefiltr, pouze u stare desky s ADC ADS1241

#endif // BOARD_VERSION_1

//-----------------------------------------------------------------------------
// Prevodnik ADS123x
//-----------------------------------------------------------------------------

#ifdef BOARD_VERSION_2                  // ADS1230 je jen na desce verze 2

//#define __ADS1232__                    // typ prevodniku ADS1232
#define __ADS1230__                    // typ prevodniku ADS1230

// porty pripojeni signalu :
#define AdcSCLK        BatCLKAD       // hodiny
#define AdcDRDY        BatDATAAD       // -ready/data out
#define AdcPDWN        BatPDWNAD       // power down

// provoz v rezimu preruseni :
#define ADC_INTERRUPT  INT_EXTERNAL0   // rezim na pozadi, vstup preruseni
                                       // neni-li konstanta uvedena, nepreklada se rezim preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

// prumerovani vzorku v preruseni :
//#define ADC_AVERAGE_SHIFT  3           // pocet prumerovanych vzorku 2^SHIFT
                                       // neni-li konstanta uvedena, neprovadi se prumerovani
#endif // BOARD_VERSION_2

//-----------------------------------------------------------------------------
// Filtrace
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  60       // max. width of averaging window

// Basic data types :
typedef long   TRawWeight;             // weight data
typedef long   TLongWeight;            // weight sum
typedef byte   TSamplesCount;          // samples counter

//#define FILTER_VISIBLE    1          // show filter data

//-----------------------------------------------------------------------------
// Pripojeni RTC DS17287
//-----------------------------------------------------------------------------

// porty :

#define RtcALE         BatALERTC       // ALE address latch enable
#define RtcWR          BatWRRTC        // WR\ zapis
#define RtcRD          BatRDRTC        // RD\ cteni
#define RtcAD          BatDATA         // datova/adresni sbernice
#define RtcCS          BatCSRTC        // chipselect

// volne adresy v RAM :

#define RTC_RAM_FIRST  0x0E            // od teto adresy
#define RTC_RAM_LAST   0x3F            // do teto adresy je RAM k dispozici

// kapacita NVRAM :

#define RTC_NVRAM_SIZE 2048

// podmineny preklad :

#define RTC_USE_DATE    1              // cti/nastavuj datum
//#define RTC_USE_WDAY    1              // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1              // hlidej zadavane hodnoty na rozsah
//#define RTC_USE_ALARM   1              // pouzivej alarm/budik
#define RTC_USE_NVRAM   1              // pouzita NVRAM

//-----------------------------------------------------------------------------
// Radic displeje Samsung Ks0108b
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       BatDATA
#define DisplayD0         BatD0
#define DisplayD1         BatD1
#define DisplayD2         BatD2
#define DisplayD3         BatD3
#define DisplayD4         BatD4
#define DisplayD5         BatD5
#define DisplayD6         BatD6
#define DisplayD7         BatD7
#define DisplayRW         BatWRRTC
#define DisplayDI         BatALERTC
#define DisplayE          BatRDRTC
#define DisplayCS1        BatCS1
#define DisplayCS2        BatCS2
#define DisplayRESET      BatRES

// Podminena kompilace

// Datova sbernice DisplayDATA 1:1
#define KS_BUS     1
// Obecne funkce
#define KS_ON      1


//-----------------------------------------------------------------------------
// Graficky displej Powertip EL12864
//-----------------------------------------------------------------------------

// Rozmery displeje:
#define DISPLAY_WIDTH           128     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

// Podminena kompilace:
#define DISPLAY_SET_AREA_FAST   1
#define DISPLAY_SET_AREA        1
#define DISPLAY_HOR_LINE        1
#define DISPLAY_FAST_VERT_LINE  1
//#define DISPLAY_VERT_LINE       1
//#define DISPLAY_PIXEL           1
#define DISPLAY_CHAR            1
#define DISPLAY_SYMBOL          1

//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Podminena kompilace:

//#define DISPLAY_LINE                    1

// Vety
#define DISPLAY_STRING                  1
#define DISPLAY_STRING_RIGHT            1
#define DISPLAY_STRING_CENTER           1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER           1
#define DISPLAY_NUMBER                  1
#define DISPLAY_NUMBER_LEFT             1
//#define DISPLAY_CHAR_FORMATED           1

// Editace
#define DISPLAY_EDIT                    1
#define DISPLAY_EDIT_CHAR_WIDTH         5       // Sirka 1 cislice (podle pouziteho fontu)
#define DISPLAY_EDIT_DOT_WIDTH          1       // Sirka tecky a dvojtecky pri editaci cisla, data a casu (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_BEFORE_DOT      2       // Sirka mezery pred desetinnou carkou (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_AFTER_DOT       2       // Sirka mezery za desetinnou carkou (podle pouziteho fontu)
//#define DISPLAY_EDIT_TEXT               1

//#define DISPLAY_BARGRAF                 1

// Dialog
#define DISPLAY_DIALOG                  1
#define DIALOG_FLIP_KEYS                1        // Zda se ma otocit zobrazeni tlacitek Enter a Esc
#define DIALOG_BUTTON_Y                 (DISPLAY_HEIGHT - DIALOG_BUTTON_HEIGHT) // Y-ova souradnice tlacitek

// Menu
#define DISPLAY_MENU                    1
#define MENU_MAX_ITEMS                  9       // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
#define DISPLAY_MENU_LINE_HEIGHT        16      // Vyska radku v pixelech
#define DISPLAY_MENU_Y                  0       // Y-ova souradnice pocatku menu v pixelech
#define DISPLAY_MENU_LINES_COUNT        4       // Pocet radku v menu
#define DISPLAY_MENU_ARROW_DY           2       // Vertikalni posun sipky (podle pouziteho fontu)
#define DISPLAY_CHOOSE_ITEM             1

// Jazyky
#define DISPLAY_LANGUAGES               1       // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
#define DisplayEditExecute(Znaky)       KbdBeepKey()

//-----------------------------------------------------------------------------
// Fonty
//-----------------------------------------------------------------------------

// Podminena kompilace:
//#define DISPLAY_TAHOMA10          1
//#define DISPLAY_TAHOMA10_BOLD     1
#define DISPLAY_TAHOMA8           1
#define DISPLAY_LUCIDA6           1
//#define DISPLAY_SMALL8_COND_NUMERIC     1
//#define DISPLAY_SMALL8_NUMERIC          1
//#define DISPLAY_SMALL8_WIDE_NUMERIC     1
//#define DISPLAY_TAHOMA10_BOLD_NUMERIC   1
#define DISPLAY_ARIAL12_BOLD_NUMERIC    1
//#define DISPLAY_ARIAL14_BOLD_NUMERIC    1
#define DISPLAY_MYRIAD32          1
//#define DISPLAY_MYRIAD40          1
//#define DISPLAY_CHAR_WIDTH      1
#define DISPLAY_TEXT_WIDTH        1

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define BacklightOn()   BatSVIT = 1
#define BacklightOff()  BatSVIT = 0

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define K0         BatK0
#define K1         BatK1
#define K2         BatK2

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
//  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_LEFT,                              // sipka doleva
  K_RIGHT,                             // sipka doprava
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

//#define __DISPLAY_CURSOR__    1      // nastavovani kurzoru
#define __DISPLAY_CLEAR__     1        // mazani
#define __DISPLAY_CLR_EOL__   1        // mazani do konce radku

//-----------------------------------------------------------------------------
// Statistika
//-----------------------------------------------------------------------------

typedef float TNumber;                 // zakladni datovy typ

//#define STAT_UNIFORMITY      1         // povoleni vypoctu uniformity
//#define STAT_REAL_UNIFORMITY 1         // povoleni presneho vypoctu uniformity

//-----------------------------------------------------------------------------
// Parametry FIFO - archiv statistiky
//-----------------------------------------------------------------------------

struct SArchive;                       // dopredna deklarace

typedef struct SArchive TRfifoData;    // Typ ukladane struktury (max 255 bytu)
#define RfifoData       FifoArchive    // Globalni buffer ukladanych dat

#define RFIFO_START            BAT2LITE_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define RFIFO_CAPACITY         BAT2LITE_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define RFIFO_MARKER_EMPTY     BAT2LITE_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define RFIFO_MARKER_FULL      BAT2LITE_FIFO_MARKER_FULL    // znacka konce pri prepisovani

#define RFIFO_INVALID_ADDRESS  0xFFFF                    // neplatna adresa do FIFO - MUSI korespondovat s XFIFO_INVALID_ADDRESS ve fifo.tpl!!!!!

// Podminena kompilace :
#define RFIFO_FAST            1         // Zapamatovani pozice markeru
#define RFIFO_VERIFY          1         // Verifikace po zapisu dat
#define RFIFO_READ            1         // Cteni obsahu po polozkach
#define RFIFO_UPDATE          1         // Prepsani obsahu polozky
#define RFIFO_FIND_FIRST      1         // Nalezeni adresy prvniho zaznamu
#define RFIFO_FIND_LAST       1         // Nalezeni adresy posledniho zaznamu
#define RFIFO_FIND_NEXT       1         // Nalezeni adresy dalsiho zaznamu
#define RFIFO_FIND_PREVIOUS   1         // Nalezeni adresy predchoziho zaznamu


//-----------------------------------------------------------------------------

#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------


#endif
