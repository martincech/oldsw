//*****************************************************************************
//
//    Archive.c - Ukladani udaju
//    Version 1.0, (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __Archive_H__
   #define __Archive_H__

#include "Hardware.h"        // zakladni datove typy
#include "..\inc\RFifo.h"    // Fifo
#include "Bat2.h"

//extern TFifo code InitDayFifo;          // Vyuziva i GsmCtl

// Pracovni data :
//extern TFifo    __xdata__ DayFifo;
extern TRfifoData __xdata__ FifoArchive;            // Den v archivu
extern TArchive __xdata__   Archive;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void ArchiveInit( void);
// Inicializace archivu (po nabehu napajeni)

void ArchiveNewWeight( TRawValue Value);
// Prida nove zvazenou hodnotu do archivu

void ArchiveNewHour( void);
// Zapise zmenu hodiny do archivu

void ArchiveUpdate(void);
// Dosadim do archivu informace z TVykrmHejna

void ArchiveSaveDay(void);
// Ulozi aktualni statistiku do archivu ve Flash

void ArchiveFinishDay(void);
// Ukonci den

void ArchiveNewDay(void);
// Zalozi novy den

TRawValue ArchiveGetLastAverage(void);
// Prochazi archiv zezadu a vrati prumernou hmotnost posledniho dne v archivu

TRawValue ArchiveGetLastAverageAndGain(TRawValue data *Gain);
// Prochazi archiv zezadu a vrati posledni den starsi nez je aktualni den vazeni a nenulovym prumerem, nebo 0 pri chybe.
// Zaroven vrati i prirustek za nalezeny den. Pozor, VykrmHejna.CisloDneVykrmu uz musi byt definovany. Musim testovat i cislo dne, protoze pri
// kopirovani dat do modulu nebo vstupu do archivu se do flash ulozi i aktualni den a po resetu to pak zmrsi cilovou hmotnost.
// Pokud se vraci udaje za 1. den vazeni (kde je prirustek vzdy 0), vrati prirustek jako rozdil prumerne a cilove hmotnosti za tento den.
// <Gain> musi byt v xdata kvuli fci FifoReadFragment().

//-----------------------------------------------------------------------------
// Operace
//-----------------------------------------------------------------------------

void ArchiveReset( void);
// Mazani archivu

#define ArchiveReadDay( MyArchive, Index)      FifoReadFragment( &DayFifo, (Index), &(MyArchive), 0, sizeof( TArchive))
// Nacte polozku <MyArchive> z pozice <Index>

#endif
