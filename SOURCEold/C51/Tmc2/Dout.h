//*****************************************************************************
//
//    Dout.c - Digitalni vystupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Dout_H__
   #define __Dout_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

// Definice jednotlivych vystupu
extern byte __xdata__ DoutCooling;
extern byte __xdata__ DoutHeatingAndPump;
extern byte __xdata__ DoutVentilation;
extern byte __xdata__ DoutHeatingPump;


void DoutInit();
  // Inicializuje digitalnich vystupu

void DoutSetCooling(TYesNo Value);
  // Zapne / vypne chlazeni

void DoutSetHeatingAndPump(TYesNo Value);
  // Zapne / vypne topeni vcetne cerpadla

void DoutSetVentilation(TYesNo Value);
  // Zapne / vypne ventilatory

void DoutSetHeatingPump(TYesNo Value);
  // Zapne / vypne cerpadlo topeni (topeni samotne je vypnute)

#endif
