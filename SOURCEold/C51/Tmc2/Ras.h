//*****************************************************************************
//
//   Ras.h       Remote Terminal access
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Ras_H__
   #define __Ras_H__

#include "Hardware.h"          // zakladni datove typy
#include "Tmc.h"               // Rozsahy
#include "Time.h"              // Datum a cas


// Prikazy z terminalu:
#define RAS_CMD_SET_MODE                1           // Nastavi rezim
#define RAS_CMD_SET_TARGET_TEMPERATURE  2           // Nastavi cilovou teplotu
#define RAS_CMD_SET_LOCALIZATION        3           // Nastavi lozeni
#define RAS_CMD_OFFSET                  4           // Nastavi meze
#define RAS_CMD_SET_FRESH_AIR_MODE      5           // Nastavi rezim cerstveho vzduchu
#define RAS_CMD_FRESH_AIR_INCREASE      6           // Rucni pridani cerstveho vzduchu
#define RAS_CMD_FRESH_AIR_DECREASE      7           // Rucni ubrani cerstveho vzduchu
#define RAS_CMD_SET_FLOOR_FLAP_MODE     8           // Nastavi rezim klapek v podlaze
#define RAS_CMD_FLOOR_FLAP_INCREASE     9           // Rucni pridani klapek v podlaze
#define RAS_CMD_FLOOR_FLAP_DECREASE     10          // Rucni ubrani klapek v podlaze
#define RAS_CMD_SOUND                   11          // Zapne / vypne hlaseni alarmu
#define RAS_CMD_TIME                    12          // Nastavi novy datum a cas
#define RAS_CMD_LOGIN                   13          // Provede login ridice
#define RAS_GET_DATA                    14          // Posle zpet periodicka data TRasData

// Odpovedi ze serveru:
#define RAS_CMD_NAK                     0xF0        // Zaslany parametr je neplatny, nastaveni se neprovedlo (hodnota mimo rozsah, jednotka neni ve spravnem rezimu atd)


// Struktura pro periodicka data
typedef struct {
  byte AccuVoltage              : 5,    // Napeti ve voltech
       ConfigLocalization       : 3;    // TLocalizationStatus: lozeni prepravek ve skrini

  byte ControlMode              : 2,    // Rezim, ve kterem regulator pracuje TControlMode
       ControlAutoMode          : 1,    // Cinnost v automatickem rezimu - bud se topi, nebo chladi TAutoMode
       ControlEmergencyMode     : 1,    // YES/NO nouzove ovladani z rozvadece
       ControlOldEmergencyMode  : 1,    // Nouzove ovladani v predchozim kroku
       ControlFrontTempFault    : 1,    // Porucha predniho cidla (nedojeti do pasma cilove teploty)
       ControlMiddleTempFault   : 1,    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)
       ControlRearTempFault     : 1;    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)

  byte CoolingOn                : 1,    // YES/NO prave sepnute chlazeni
       CoolingFailure           : 1,    // YES/NO porucha chlazeni
       DieselUse                : 1,    // YES/NO zda se diesel motor pouziva
       DieselOn                 : 1,    // YES/NO bezi diesel
       DieselTemperature        : 4;    // Teplota motoru

  byte ServoFloorServoStatus    : 3,    // Stav serva v podlaze
       DieselChangeOil          : 1,    // YES/NO ma se vymenit olej s filtrem
       DieselOverheated         : 1,    // YES/NO prehraty diesel
       ElMotorOn                : 1,    // YES/NO zapnuty elektromotor
       HeatingFailure           : 1,    // YES/NO porucha samotneho topeni
       HeatingServoFailure      : 1;    // YES/NO porucha serva topeni

  byte FreshAirMode             : 1,    // Typ rizeni TFreshAirMode
       FreshAirPercent          : 7;    // Aktualni hodnota cersrtveho vzduchu v procentech

  byte FreshAirPosition          : 4,  // Poloha klapky pro zobrazeni a logovani
       FreshAirFloorFlapPosition : 4;  // Poloha klapky pro zobrazeni a logovani

  byte FreshAirFloorFlapMode    : 1,   // Typ rizeni
       FreshAirFloorFlapPercent : 7;   // Aktualni hodnota otevreni klapky v procentech

  byte HeatingPosition          : 4,   // Poloha serva topeni pro logovani
       RecirculationServoStatus : 3,    // Stav serva recirkulace
       HeatingFlame1            : 1;    // YES/NO topeni 1 prave hori

  byte HeatingPercent           : 7,    // Aktualni hodnota vykonu topeni v procentech
       FanOn                    : 1;    // YES/NO zapnute ventilatory

  byte ServoTopInductionServoStatus     : 3,    // Stav horniho naporoveho serva
       ServoBottomInductionServoStatus  : 3,    // Stav spodniho naporoveho serva
       FanLowPower                      : 1,    // YES/NO 50% vykon
       FanFailure                       : 1;    // YES/NO porucha ventilatoru

  int16 HeatingError;              // Rozdil mezi cilovou a prumernou teplotou
  int32 HeatingProportional;       // Proporcionalni slozka PI regulace
  int32 HeatingIntegral;           // Integracni slozna PI regulace

  byte LogDriver                : 7,    // Cislo nalogovaneho ridice
       AccuLowVoltage           : 1;    // YES/NO nizke napeti akumulatoru

  byte Co2                      : 7,     // Koncentrace CO2 s dilkem CO2_LSB ppm
       GsmRegistered            : 1;    // GSM modul je prihalsen do site

  byte Humidity                 : 7,                           // Relativni vlhkost v %
       GsmCalling               : 1;    // GSM modul prave komunikuje

  byte FiltersAirPressure       : 7,    // Tlakova ztrata na filtrech s dilkem AIR_PRESSURE_LSB Pa
       HeatingFlame2            : 1;    // YES/NO topeni 2 prave hori

  byte FansAirPressure          : 7,      // Tlakova ztrata na ventilatorech s dilkem AIR_PRESSURE_LSB Pa
       ChargingOn               : 1;    // YES/NO dobiji se

  char Temp1C[TEMP_COUNT];    // Teploty zaokrouhlene na 1 stupen
  int  Temp01C[TEMP_COUNT];   // Teploty zaokrouhlene na desetinu stupne

  TTransferTime Time;         // 4 bajty

  char ConfigTargetTemperature; // Cilova teplota, cele stupne

  byte ConfigMaxOffsetAbove     : 7,    // Maximalni odchylka teploty ve skrini nad cilovou teplotou
       AlarmShowAlarm           : 1;    // YES/NO zda se prave indikuje alarm

  byte ConfigMaxOffsetBelow     : 7,    // Maximalni odchylka teploty ve skrini pod cilovou teplotou
       AlarmQuiet               : 1;    // YES/NO potlaceny reproduktor pri alarmu

  byte ChargingFailure          : 1,    // YES/NO porucha dobijeni
       HeatingCircuitState      : 3,    // Stav ohrevu vody ve spodnim okruhu
       Dummy                    : 4;

} TRasData;   // 51 bajtu - upravit vzdy i PACKET_MAX_DATA v Hardware.h


void RasInit( void);
// Inicializace terminalu

void RasExecute( void);
// Periodicka obsluha - volat 1x za sekundu

#endif


