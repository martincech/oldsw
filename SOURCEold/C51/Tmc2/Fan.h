//*****************************************************************************
//
//    Fan.c - Kontrola behu a poruchy ventilatoru
//    Version 1.0
//
//*****************************************************************************

#ifndef __Fan_H__
   #define __Fan_H__

#include "Hardware.h"

typedef struct {
  byte On;          // YES/NO zapnute ventilatory
  byte LowPower;    // YES/NO 50% vykon
  byte Failure;     // YES/NO porucha ventilatoru
} TFan;
extern TFan __xdata__ Fan;

void FanInit();
  // Inicializace ventilatoru

void FanStart();
  // Start behu ventilatoru a nulovani chyby

void FanStop();
  // Stop behu ventilatoru a nulovani chyby

void FanCheck();
  // Zkontroluje beh a poruchu ventilatoru, volat 1x za sekundu.

#endif
