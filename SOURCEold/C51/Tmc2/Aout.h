//*****************************************************************************
//
//    Aout.c - Analogove vystupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Aout_H__
   #define __Aout_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#include "..\inc\Tda8444.h"     // analog output

extern byte __xdata__ AoutInductionServo;       // Ridici signal serva naporu a recirkulace
extern byte __xdata__ AoutFloorServo;           // Ridici signal serva v podlaze
extern byte __xdata__ AoutHeatingServo;         // Ridici signal serva topeni


void AoutInit();
  // Inicializuje analogovych vystupu

void AoutSetInductionServo(byte Value);
  // Nastavi obe naporova serva

void AoutSetHeatingServo(byte Value);
  // Nastavi servo topeni (je na 2 vystupech)

void AoutSetFloorServo(byte Value);
  // Nastavi servo v podlaze

#endif
