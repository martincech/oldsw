//*****************************************************************************
//
//    Heating.c - Ovladani topeni
//    Version 1.0
//
//*****************************************************************************


#ifndef __Heating_H__
   #define __Heating_H__

#include "Hardware.h"     // pro podminenou kompilaci
#include "DutyCyc.h"           // Vypocet stridy

// Stav ohrevu vody ve spodnim okruhu
typedef enum THeatingCircuitState {
  HEATING_CIRCUIT_INIT,             // Stav po zapnuti rezimu topeni
  HEATING_CIRCUIT_START_HEATING,    // Topeni je zapnute, cekam na zapaleni plamene
  HEATING_CIRCUIT_HEATING,          // Plamen hori a ceka se na zhasnuti plamene pri dosazeni 85C
  HEATING_CIRCUIT_CHECK_FAILURE,    // Plamen zhasnul, po nejakou dobu od zhasnuti kontroluju poruchu topeni (pri poruse plamen take zhasne)
  HEATING_CIRCUIT_PUMP,             // Plamen zhasnuty, jede jen cerpadlo a ceka se na pokles teploty vody v okruhu
  HEATING_CIRCUIT_OFF,              // Plamen zhasnuty, cerpadlo odstavene po dobe necinnosti a ceka se na pokles teploty vody v okruhu
  _HEATING_WATER_COUNT
}; // Pri zmene pozor na RasData, aby se to tam vlezlo

// Struktura topeni
typedef struct {
#ifdef USE_ON_OFF_HEATING
  byte On;                  // Zapnute topeni, pouze u bufiku
#endif // USE_ON_OFF_HEATING
  byte Percent;             // Aktualni hodnota vykonu topeni v procentech
  byte Position;            // Poloha serva topeni pro logovani
  byte Failure;             // YES/NO porucha samotneho topeni 1 nebo 2 (jsou sloucene)
  byte ServoFailure;        // YES/NO porucha serva topeni
  byte Flame1;              // YES/NO topeni 1 prave hori
  byte Flame2;              // YES/NO topeni 2 prave hori
  int16 Error;              // Rozdil mezi cilovou a prumernou teplotou
  int32 Proportional;       // Proporcionalni slozka PI regulace
  int32 Integral;           // Integracni slozna PI regulace
  int32 Derivative;         // Derivacni slozka PID regulace
  int16 LastTemperature;    // Teplota ve skrini z minuleho kroku
  TDutyCycle Flame1DutyCycle;   // Strida horeni plamene topeni 1
  TDutyCycle Flame2DutyCycle;   // Strida horeni plamene topeni 2
  byte WaterTemperature;    // Teplota vody na vstupu Webasta ve stupnich Celsia
  byte HeatingCircuitState; // Stav ohrevu vody ve spodnim okruhu
  byte AveragePercent;      // Prumerny vykon topeni
} THeating;
extern THeating __xdata__ Heating;

#define HEATING_MAX         100         // Maximalni hodnota vykonu topeni

// Konstanty PID regulatoru :
#define     REG_TS        10             // vzorkovaci perioda v sekundach
#define     REG_T_LSB     16             // velikost LSB : 1/REG_T_LSB C
#define     REG_Y_HI      100            // Maximum vystupu je 100%
#define     REG_Y_LO      0              // Minimum vystupu je 0%
#define     REG_MAX_TI    1000           // maximalni velikost Ti v sekundach
#define     REG_INT_SCALE (REG_MAX_TI / REG_TS)                // meritko zvetseni integracni slozky, pri max Ti LSB odchylky vyvola alespon integral += 1
#define     REG_INT_HI    ((long)REG_Y_HI * (long)REG_INT_SCALE * (long)REG_T_LSB)    // maximalni integracni hodnota
#define     REG_INT_LO    0              // minimalni integracni hodnota
#define     REG_PROP_SATURATION   (REG_T_LSB * REG_Y_HI)         // Hodnota saturace proporcionalni slozky (tj. 100%)
#define     REG_DER_HI    ((long) REG_Y_HI * (long)REG_T_LSB)    // maximalni integracni hodnota (+100%)
#define     REG_DER_LO    ((long)-REG_Y_HI * (long)REG_T_LSB)    // maximalni integracni hodnota (-100%)
#define     REG_DER_SCALE 16             // Derivacni slozka se pocita primo z teplot ve vnitrnim formatu (*256), kdezto proporcionalni i intergracni
                                         // slozka se pocita z odchylky, ktera je jen 16*T (na zacatku teto fce delam bitovy posuv err >>= 4).

void HeatingInit();
  // Inicializace

void HeatingStop();
  // Vypne topeni a vynuluje poruchu

TYesNo HeatingFullyClosed();
  // Vrati YES, pokud je ventil topeni plne uzavren nebo je servo odpojeno/v chybe

void HeatingAutoStart();
  // Zahaji topeni v automatickem rezimu

void HeatingAutoExecute(int Average);
  // Provede krok v rezimu auto topeni, volat 1x za sekundu. <Average> je prumerna teplota podle lozeni ve vnitrnim formatu

void HeatingResetDutyCycle();
  // Snulovani stridy


#endif
