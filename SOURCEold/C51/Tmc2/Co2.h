//*****************************************************************************
//
//    Co2.c - Mereni koncentrace CO2
//    Version 1.0
//
//*****************************************************************************


#ifndef __Co2_H__
   #define __Co2_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Struktura CO2
typedef struct {
  byte Value;                           // Koncentrace CO2 s dilkem CO2_LSB ppm
} TCo2;
extern TCo2 __xdata__ Co2;

#define CO2_LSB     100                 // Hodnotu ukladam s dilkem 100 ppm, rozsah 10.000ppm tak vyjde na 7 bitu

void Co2Init();
  // Inicializace

void Co2Calc();
  // Vypocte CO2

#endif
