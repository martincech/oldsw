//*****************************************************************************
//
//    Beep.c - Piskani reproduktoru
//    Version 1.0
//
//*****************************************************************************


#ifndef __Beep_H__
   #define __Beep_H__

#include "Hardware.h"
#include "..\inc\PcaTmc.h"     // PCA projektu TMC

void BeepKey();
  // Piskne po stisku klavesy (touch panelu)

#define BeepStartup() PcaBeep( NOTE_C4, VOL_10, 200)
  // Piskne po zapnuti jednotky

#define BeepAlarm() PcaBeep( NOTE_C4, VOL_10, 100)
  // Piskne pri alarmu

#endif
