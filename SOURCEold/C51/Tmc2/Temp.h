//*****************************************************************************
//
//    Temp.c - Teplomery
//    Version 1.0
//
//*****************************************************************************


#ifndef __Temp_H__
   #define __Temp_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#include "Tmc.h"                 // Indexy serv

#define TEMP_INVALID_1C  -100           // Neplatna teplota pri zaokrouhleni na 1 stupen
#define TEMP_INVALID_01C -1000          // Neplatna teplota pri zaokrouhleni na desetinu stupne (musi byt do 1024, v zaznamech ukaladam na 10 bitu + znamenko)

// Definice teplot
extern int __xdata__  Temp[TEMP_COUNT];      // Teploty ve vnitrnim formatu
extern char __xdata__ Temp1C[TEMP_COUNT];    // Teploty zaokrouhlene na 1 stupen
extern int __xdata__  Temp01C[TEMP_COUNT];   // Teploty zaokrouhlene na desetinu stupne

// Celkovy pocet chyb (pro diagnostiku)
extern word __xdata__ TempTotalErrorCounter[TEMP_COUNT];       // Celkovy pocet neplatnych nacteni u jednotlivych teplomeru od zapnuti jednotky

void TemperatureInit();
  // Inicializuje teplomery (fce TempInit() uz je definovana)

void TemperatureRead();
  // Nacte vsechny teploty a ulozi je do lokalnich promennych

void TempClearAverages();
  // Vynuluje prumerovani teplot

int TempCountAverage(byte address);
  // Vypocte a vrati prumer u zadane teploty. Teplota je na desetiny stupne (12.3C = 123)

int TempLocalizationAverage(int Front, int Middle, int Rear, TLocalizationStatus Localization, int Error);
  // Vypocte ze zadanych teplot prumer podle zadaneho lozeni. Teploty muzou byt ve vnitrnim formatu i zaokrouhlene, <Error> udava hodnotu pri chybe.

#endif
