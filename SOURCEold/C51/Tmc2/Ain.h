//*****************************************************************************
//
//    Ain.c - Analogove vstupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Ain_H__
   #define __Ain_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#include "Tmc.h"                 // Indexy serv
#include "..\inc\Adc0838.h"      // AD converter

// Definice jednotlivych vstupu
extern byte __xdata__ AinServo[_SERVO_COUNT]; // Vystupy jednotlivych serv
extern byte __xdata__ AinDieselTemperature;   // Teplota dieselu
extern byte __xdata__ AinFan;                 // Napeti ventilatoru
extern byte __xdata__ AinVoltage;             // Napeti akumulatoru
extern byte __xdata__ AinSuctionPressure;     // Tlak sani kompresoru
extern byte __xdata__ AinDischargePressure;   // Tlak vytlaku kompresoru
extern byte __xdata__ AinWaterTemperature;    // Teplota vody v topnem okruhu
extern byte __xdata__ AinCo2;                 // Koncentrace CO2
extern byte __xdata__ AinHumidity;            // Vlhkost vzduchu
extern byte __xdata__ AinFiltersPressure;     // Tlakova ztrata na vzduchovych filtrech
extern byte __xdata__ AinFansPressure;        // Tlakova ztrata na ventilatorech


void AinInit();
  // Inicializuje analogovych vstupu

void AinRead();
  // Nacte vsechny analogove vstupy a ulozi je do lokalnich promennych

byte AinCalcValue(byte ainValue, byte code *ainTable, byte code *valueTable, byte tableCount);
  // Prepocte hodnotu analogoveho vstupu <ainValue> na hodnotu podle prevodni tabulky

#endif
