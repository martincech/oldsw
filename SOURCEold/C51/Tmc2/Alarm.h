//*****************************************************************************
//
//    Alarm.c - Ovladani alarmu podle poruch
//    Version 1.0
//
//*****************************************************************************


#ifndef __Alarm_H__
   #define __Alarm_H__

#include "Hardware.h"     // pro podminenou kompilaci

typedef struct {
  byte ShowAlarm;         // YES/NO zda se prave indikuje alarm
  byte Quiet;             // YES/NO potlaceny reproduktor pri alarmu
} TAlarm;
extern TAlarm __xdata__ Alarm;



void AlarmInit();
  // Inicializuje alarmu

void AlarmChange();
  // Zapne / vypne hlaseni alarmu

void AlarmClear();
  // Vynuluje alarm (volat periodicky na zacatku kazdeho mericiho cyklu)

void AlarmExecute();
  // Obsluha piskani alarmu, volat kazdou sekundu na konci mericiho cyklu

void AlarmSet();
  // Nastavi alarm pri nejake poruse

#endif
