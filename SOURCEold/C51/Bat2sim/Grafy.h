//*****************************************************************************
//
//    Grafy.c      - Age grafy rustove krivky, planu krmeni
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Grafy_H__
   #define __Grafy_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#ifndef __Bat2_H__
   #include "Bat2.h"
#endif

word VypoctiAproximaciRustoveKrivky(TBodKrivky xdata *Krivka, word Den);
// Vypocte ze zadane hmotnost pro zadany den (den muze byt 0-999)

void UlozPrvniHodnotuRustoveKrivky(TBodKrivky xdata *Krivka, unsigned int Hmotnost);
// Vytvori jednoprvkovou rustovou krivku

unsigned int NactiPrvniHodnotuRustoveKrivky(TBodKrivky xdata *Krivka);
// Nacte z rustove krivky prvni hmotnost

bit TabulkaRustoveKrivky(TBodKrivky xdata *Krivka);
// Edituje tabulku rustove krivky

#endif
