//*****************************************************************************
//
//   NvRam.h        NVRAM application map
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __NvRam_H__
   #define __NvRam_H__

#ifndef __NvFlash_H__
   #include "..\inc\AtFlash.h"    // cache structure
#endif

#ifndef __Ds17287_H__
   #include "..\inc\Ds17287.h"    // NVRAM
#endif

#ifndef __Bat2_H__
   #include "Bat2.h"
#endif

//-----------------------------------------------------------------------------
// Struktura NVRAM
//-----------------------------------------------------------------------------

#ifndef FLASH_NVRAM // interni buffer flash

   #define NVR_SPARE (RTC_NVRAM_SIZE - sizeof( TArchiv))

   typedef struct {
      TArchiv     Archiv;                     // Statistika
      byte        Volne[ NVR_SPARE];
   } TNvrData;

//-----------------------------------------------------------------------------
#else // NVRAM cache flash

   #define NVR_SPARE (RTC_NVRAM_SIZE - sizeof( TArchiv) - sizeof( TFlashCache))

   typedef struct {
      TArchiv     Archiv;                     // Statistika
      byte        Volne[ NVR_SPARE];
      TFlashCache FlashCache;                 // cache pro flash
   } TNvrData;


   #define NvramPage() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Page)) // adresa cisla stranky
   #define NvramData() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Data)) // adresa bufferu dat
#endif // NVRAM cache
//-----------------------------------------------------------------------------
#endif
