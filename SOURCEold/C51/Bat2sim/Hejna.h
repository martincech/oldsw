//*****************************************************************************
//
//    Hejna.h      - Editace a parametry hejna
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hejna_H__
   #define __Hejna_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#ifndef __Bat2_H__
   #include "Bat2.h"
#endif

//---------------------------------------------------------------------------------------------
// Globalni promenne
//---------------------------------------------------------------------------------------------

extern TVykrmHejna xdata VykrmHejna;          // Data potrebna pro chod vykrmu aktualniho dne

//---------------------------------------------------------------------------------------------
// Funkce
//---------------------------------------------------------------------------------------------

#define ProbihaKrmeni() (Konfigurace.StartVykrmu.ProbihaKrmeni)
// Vyjadruje podminku, zda se prave krmi nebo ne - vlozi se napr. do if(ProbihaKrmeni()), if (!ProbihaKrmeni())

void NactiParametryVykrmu( void);
// Pokud se prave krmi, nacte aktualni hejno do pameti

void AktualizujDenVykrmu( void);
// Aktualizuje cislo dne vykrmu, dopocte parametry dne

//---------------------------------------------------------------------------------------------

TYesNo NoveHejno( byte Cislo);
// Edituje vsechny parametry hejna s cislem Cislo

TYesNo UpravaHejna( byte Cislo);
// Edituje vsechny parametry hejna s cislem Cislo

void SmazatHejno( byte Cislo);
// Smaze ze seznamu hejn hejno s cislem Cislo

void SmazatVsechnaHejna( void);
// Smaze seznam hejn

byte VyberHejno( void);
// Vybere 1 hejno z existujicich hejn a vrati jeho cislo.
// Pokud nevybere, vrati CISLO_PRAZDNEHO_HEJNA

#endif

