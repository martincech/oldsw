//*****************************************************************************
//
//    Exec.c - Vykonny modul
//    Version 1.0
//
//*****************************************************************************

#include "Bat2.h"
#include "Exec.h"
#include "..\inc\System.h"     // "operacni system" - kvuli SysDelay()
#include "Accu.h"              // Lowbat
#include "KbdCtl.h"            // Obsluha klavesnice
#include "Menu.h"              // Uzivatelske rozhrani
#include "Hejna.h"             // Editovani hejna
#include "NVahy.h"             // Naslapne vahy
#include "Archiv.h"            // Ukladani hodnot
#include "GsmCtl.h"            // Rizeni GSM
#include "Datum.h"             // Date/time computing
#include "Cfg.h"               // Konfigurace
#include "Cal.h"               // Kalibrace
#include "Datum.h"             // Date/time computing

#include "..\inc\Ds17287.h"    // RTC
#include "..\inc\bcd.h"        // BCD aritmetika
#include "..\inc\Pca.h"        // zvuky pomoci PCA
#include "..\inc\Ds17287.h"    // RTC
#include "..\inc\AtFlash.h"    // Flash memory
#include "..\inc\El12864.h"    // Displej, kvuli cyklickemu rozinani displeje
#include "..\inc\Kbd.h"        // klavesnice - nastaveni jazyka


//#define __DEBUG__

#ifdef __DEBUG__
  #include "..\inc\El12864.h"
#endif

static byte __xdata__ GsmTimer;            // Timer pro kontrolu GSM modulu
static byte __xdata__ CekatNaNovyDen;      // Flag, ze behem vypnuti vahy doslo ke zmene data a mam pozastavit vazeni,
                                           // dokud nezalozim novy den

extern TYesNo NastaveniJazyka;


// Lokalni funkce:
static void NactiDatumCas();
// Nactu aktualni datum a cas v binarni podobe do globalni promenne



//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void ExecInit() {
  // Inicializace vsech modulu
  Delay(300);
  if (PowerFail()) {
    AccuWaitForVoltage();       // Pockam, dokud nedojde k vzrustu napajeni
  }

  GsmTimer = GSM_TIMER;       // Inicializace xdata promenne, kvuli blbnuti Keilu

  EnableInts();               // Povoleni preruseni
  Timer0Run(TIMER0_PERIOD);   // Spust casovac 0

  PcaBeep( NOTE_C4, VOL_10, 200);  // uvodni pipnuti

  RtcInit();                  // inicializace RTC

  NactiDatumCas();            // Nactu aktualni datum a cas, aby byl okamzite k dispozici
  RtcSec();                   // Kvuli linkeru, aby negeneroval warning. Sekundy se normalne nepouzivaji

  FlashInit();
  CfgLoad();                  // Nacteni konfigurace z interni EEPROM
  CalLoad();                  // Kalibraci mam zvlast

  MenuInit();                 // inicializace zobrazeni (az za CfgLoad)

  GsmInit();                  // inicializace GSM komunikace
  GsmStart();                 // Nahozeni napajeni GSM modulu

  WatchDog();                 // eliminuj timeout

  ArchivInit();               // Inicializace archivu
  CekatNaNovyDen = !StejneDatum(&Archiv.Cas, &ActualDateTime);  // Pokud doslo behem vypnute vahy k pulnoci, pozastavim vazeni
  NactiParametryVykrmu();     // POZOR, musi byt az za ArchivInit, aby se v pripade automatickeho vazeni spravne nacetla prumerna hmotnost ze vcerejska
  NVahyInit();                // Inicializace A/D a spusteni prevodu
}

//-----------------------------------------------------------------------------
// Hlavni smycka
//-----------------------------------------------------------------------------

void ExecExecute() {
  // Hlavni smycka

  // Po resetu otestuju, zda nechce zmenit jazyk
  if (KbdPowerUpKey() == K_ENTER) {
    PipniKlavesnice();      // Indikace, ze muze klavesu pustit
    KbdPowerUpRelease();    // Cekam na pusteni
    NastaveniJazyka = YES;  // Nastavim flag, ze se nastavuje jazyk a vaha nema provadet zadnou cinnost
    MenuJazyk();
  }
  NastaveniJazyka = NO;     // Nenastavuje se jazyk, normalni cinnost

  // Cyklicka cinnost
  while(1) {
    switch(SysWaitEvent()) {
      case K_ENTER: {
        // Vratim default font i mod fontu
        PipniKlavesnice();
        MenuExecute();   // Rozjedu menu
        MenuZobrazPrubehVazeni(ZOBRAZIT_VSE);
        break;
      }
      case K_RIGHT: {
        // Prejdu na dalsi zobrazeni
        if (!ProbihaKrmeni()) {
          break;  // Zobrazeni menim jen kdyz se krmi
        }
        PipniKlavesnice();
        MenuDalsiZobrazeni();
        MenuZobrazPrubehVazeni(ZOBRAZIT_VSE);
        break;
      }
      case K_LEFT: {
        // Zmenim zobrazovane pohlavi
        if (!ProbihaKrmeni()) {
          break;  // Zobrazeni menim jen kdyz se krmi
        }
        if (!VykrmHejna.Zahlavi.RozdelovatPohlavi) {
          break;  // Behem krmeni se nerozpoznava pohlavi
        }
        if (Zobrazeni == ZOBRAZENI_STATISTIKA || Zobrazeni == ZOBRAZENI_HISTOGRAM) {
          // Pohlavi menim jen u statistiky a histogramu
          PipniKlavesnice();
          MenuZmenZobrazenePohlavi();
          MenuZobrazPrubehVazeni(ZOBRAZIT_VSE);
        }//if
        break;
      }
      case K_UP:
      case K_UP | K_REPEAT: {
        // Pokud je zobrazen histogram, posunu kurzor vpravo
        // Pokud je zobrazen logger, posunu se na dalsi stranu
        switch (Zobrazeni) {
          case ZOBRAZENI_HISTOGRAM: {
            PipniKlavesnice();
            MenuPosunHistogramDoprava();
            MenuZobrazPrubehVazeni(ZOBRAZIT_HISTOGRAM_KURZOR);
            break;
          }
          case ZOBRAZENI_LOGGER: {
            PipniKlavesnice();
            ZobrazLoggerDalsiStrana();
            MenuZobrazPrubehVazeni(ZOBRAZIT_LOGGER);
            break;
          }
        }//switch
        break;
      }
      case K_DOWN:
      case K_DOWN | K_REPEAT: {
        // Pokud je zobrazen histogram, posunu kurzor vlevo
        // Pokud je zobrazen logger, posunu se na predchozi stranu
        switch (Zobrazeni) {
          case ZOBRAZENI_HISTOGRAM: {
            PipniKlavesnice();
            MenuPosunHistogramDoleva();
            MenuZobrazPrubehVazeni(ZOBRAZIT_HISTOGRAM_KURZOR);
            break;
          }
          case ZOBRAZENI_LOGGER: {
            PipniKlavesnice();
            ZobrazLoggerPredchoziStrana();
            MenuZobrazPrubehVazeni(ZOBRAZIT_LOGGER);
            break;
          }
        }//switch
        break;
      }
      case K_ESC: {
        // Pokusy
#ifdef __DEBUG__
        PipniKlavesnice();
        MenuPokusFifo();
        Zobrazit = ZOBRAZIT_VSE;
        VymazDisplej();
#endif  // __DEBUG__
        break;
      }
      case K_REDRAW: {
        MenuZobrazPrubehVazeni(Zobrazit);
        break;
      }
    }//switch
  }//while
} // ExecExecute

//-----------------------------------------------------------------------------
// Obsluha naslapnych vah
//-----------------------------------------------------------------------------

void ExecNVahy() {
  // Obsluha naslapnych vah
  if (PowerFail() || CekatNaNovyDen) {
    return;
  }
  if (NVahyObsluha()) {
    // Zvazil se novy vzorek, ulozim do archivu
    ArchivNewWeight(NVaha.PosledniUlozenaHmotnost, NVaha.PosledniUlozenePohlavi);
    Zobrazit |= ZOBRAZIT_NV_ULOZENO;  // Zobrazim ulozenou hmotnost
  }
}

//-----------------------------------------------------------------------------
// Periodicka cinnost 1 sec
//-----------------------------------------------------------------------------

void ExecPeriodic() {
  // Obsluha periodicke cinnosti
  DisplejRoznout();
  NactiDatumCas();
  if (!StejneDatum(&Archiv.Cas, &ActualDateTime)) {
    // Zmena data - to musim testovat pred zmenou casu, na zacatku noveho dne to tedy zavola jen fci ArchivNewDay(), fci ArchivNeHour() uz ne.
    CekatNaNovyDen = NO;           // Kazdopadne shodim flag
    if (ProbihaKrmeni()) {
      ArchivFinishDay();           // ulozeni statistik a zakonceni dne
      GsmCreateStatisticMsg();     // Z aktualnich dat vytvori text SMS zpravy
      ArchivNewDay();              // nulovani statistik a zalozeni noveho dne
      GsmSetMidnightMsg();         // Samotnou zpravu poslu az po zalozeni noveho dne - kvuli lowbatu, posilani moc zere a dlouho trva
      AktualizujDenVykrmu();       // vypocet noveho dne vykrmu
      ArchivUpdate();              // Ulozim vypoctene hodnoty do archivu
    }
    // Pokud se nekrmi a dojde ke zmene data, Archiv.Cas se neobnovuje a proto to bude porad prekreslovat cely displej. Zatim to necham tak.
    Zobrazit = ZOBRAZIT_VSE;       // Kazdopadne prekreslim novy den, i kdyz se nekrmi
  } else if (Archiv.Cas.Hour != ActualDateTime.Hour) {
    // Zmena hodiny
    if (ProbihaKrmeni()) {
      ArchivNewHour();             // ulozeni zmeny casu
      Zobrazit = ZOBRAZIT_VSE;     // prekresli - prepocital se histogram
    }//if
  }//else
  if (--GsmTimer == 0) {
    GsmTimer = GSM_TIMER;
    GsmExecute();            // cteni a provadeni SMS povelu
  }
  Zobrazit |= ZOBRAZIT_1SEC; // Zobrazeni po 1sec
}

//-----------------------------------------------------------------------------
// Kontrola napajeni
//-----------------------------------------------------------------------------

void ExecCheckVoltage() {
  // Kontrola napajeciho napeti, v pripade vypadku ceka, dokud se neobnovi
  if (PowerFail()) {
    // Objevil se lowbat
    VypniPodsvit();     // Usetrim energii
    GsmStop();
    PipniKlavesnice();  // !!! Pro testy
    // Cekam, dokud se napajeni nespravi
    MenuZobrazLowBat();
    AccuWaitForVoltage();
    // Prave se obnovilo napajeni
    ZapniPodsvit();
    GsmStart();
    Zobrazit = ZOBRAZIT_VSE;   // Vse prekreslim
    SysSetTimeout();           // Pokud je v menu, vypadnu
  }//if
}

//-----------------------------------------------------------------------------
// Nacteni data a casu
//-----------------------------------------------------------------------------

static void NactiDatumCas() {
  // Nactu aktualni datum a cas v binarni podobe do globalni promenne
  ActualDateTime.Day   = bbcd2bin(RtcDay());
  ActualDateTime.Month = bbcd2bin(RtcMonth());
  ActualDateTime.Year  = wbcd2bin(RtcYear());
  ActualDateTime.Hour  = bbcd2bin(RtcHour());
  ActualDateTime.Min   = bbcd2bin(RtcMin());
}
