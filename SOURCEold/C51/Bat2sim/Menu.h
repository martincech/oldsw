//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// Globalni promenne :

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit

// Co se zobrazuje na displeji behem vazeni
typedef enum {
  ZOBRAZENI_NEVAZISE,
  ZOBRAZENI_VAZENI,
  ZOBRAZENI_STATISTIKA,
  ZOBRAZENI_HISTOGRAM,
  ZOBRAZENI_LOGGER,
  ZOBRAZENI_HISTORIE_STATISTIKA,
  ZOBRAZENI_HISTORIE_HISTOGRAM,
  ZOBRAZENI_HISTORIE_LOGGER,
  _ZOBRAZENI_COUNT
} TZobrazeni;
extern TZobrazeni __xdata__ Zobrazeni;          // Co se ma prave zobrazovat




void MenuInit();
// Inicializace zobrazeni

void MenuInitLogger();
// Inicializace zobrazeni loggeru na aktualni den vazeni, volat az po inicializaci archivu

void MenuLowBat();
// Zobrazeni nizkeho nepeti

//void MenuExecute( void);
// Hlavni smycka

void MenuExecute();
// Spusteni menu

void MenuZobrazLowBat();
// Zobrazi menu pro nizke napeti

void MenuZobrazPrubehVazeni(byte NoveZobrazit);
// Hlavni zobrazeni behem vazeni

void MenuZmenZobrazenePohlavi();
// Zmenim zobrazovane pohlavi

void MenuPosunHistogramDoprava();
// Posune kurzor v histogramu o 1 doprava

void MenuPosunHistogramDoleva();
// Posune kurzor v histogramu o 1 doleva

void ZobrazLoggerDalsiStrana();
// Posune na dalsi stranku, na konci prejde zpet na prvni stranku

void ZobrazLoggerPredchoziStrana();
// Posune na predchozi stranku, na zacatku prejde na posledni stranku

void MenuPokusFifo();
// Zaplni Logger i Archiv
// Je treba zacinat s prazdnym archivem a nastartovanym vazenim, na datumu nezalezi.

void MenuDalsiZobrazeni();
// Posune zobrazeni na dalsi

void MenuJazyk();
// Vyber jazyka

#endif
