//*****************************************************************************
//
//    NVahy.h - Naslapne vahy
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __NVahy_H__
   #define __NVahy_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#include "..\inc\Average.h"  // Klouzavy prumer

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Stavy naslapnych vah :

typedef enum {
  STAV_NV_INICIALIZACE,                 // Po zapnuti cekam, az se mne naplni historie nejakymi hmotnostmi
  STAV_NV_CEKAM_NA_NASTUP,              // Vaha ceka na vzrust hmotnosti tak, aby vzrust padnul do rustove krivky
  STAV_NV_CEKAM_NA_USTALENI,            // Hmotnost na vaze stoupla a padlo to do krivky, cekam na ustaleni vahy, abych ulozil ustalenou hmotnost
  STAV_NV_CEKAM_PO_ULOZENI              // Vaha se ustalila, ulozil jsem a nyni cekam, az povolim ulozeni dalsiho kusu
} TStavNaslapneVahy;

#define HISTORIE_POCET_HMOTNOSTI    2   // interni - kolik prvku zpetne se bude uchovavat
#define NVAHA_PRUMEROVANI           4   // Pocet vzroku do prumeru

// Popisovac vahy :
typedef struct {
   long       Suma;                     // Suma prevodu pro prumerovani
   byte       Pocet;                    // Pocet vzroku v sume
   byte       ZahoditVzorek;            // Flag, ze se ma nasledujici nacteny vzorek zahodit

   long       Hmotnost;                 // Aktualni okamzita hmotnost na vaze
   long       PosledniUlozenaHmotnost;  // Posledni ulozena hmotnost
   byte       PosledniUlozenePohlavi;   // Pohlavi u polsedne ulozene hmotnosti

   // Detekce nastupu
   TAverage   Average;                  // Klouzavy prumer
   long       PosledniUstalenaHmotnost;
   byte       Ustaleni;                 // Flag, ze je vaha ustalena

   // Pauza vazeni
   byte       CasovaPauza;              // Flag, zda je prave zapauzovane vazeni kvuli casovemu omezeni
   byte       Pauza;                    // Flag, zda je prave zapauzovane vazeni (casove nebo rucne)
} TNVaha;

// Popisovac vah :

extern TNVaha __xdata__ NVaha;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void NVahyInit( void);
// Inicializace

void NVahyInitAverage();
// Inicializace klouzaveho prumeru podle konfigurace

void NVahySetPause(TYesNo NewPause);
// Nastaveni pauzy vahy

TYesNo NVahyObsluha( void);
// Projede vsechny prevody naslapnych vah a pokud je nejaky novy prevod, vypocte jeho hmotnost, zkontroluje, zda se ma ulozit atd.
// Pokud se ma ulozit novy vzorek, vrati YES

void NVahyEnable( void);
// Povoleni vazeni

void NVahyDisable( void);
// Zakaz vazeni

#endif
