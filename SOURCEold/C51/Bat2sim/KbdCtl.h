//*****************************************************************************
//
//    KbdCtl.h - Obsluha klavesnice
//    Version 1.0
//
//*****************************************************************************

#ifndef __KbdCtl_H__
   #define __KbdCtl_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

//#define ZerKlavesu() Klavesa = K_NULL
// Spotrebovani klavesy

void PipniKlavesnice();
// Pipnuti pri stisku klavesy (vyuziva modul displeje)

byte CekejKlavesu();
// Ceka na stisk klavesy

byte CekejNaEnterEsc();
// Ceka na stisk tlacitek Enter nebo Esc

void CekejNaEnter();
// Ceka na stisk tlacitka Enter

#endif
