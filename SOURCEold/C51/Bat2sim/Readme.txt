                   Upravy


Poznamky, namety :

1. zjednoduseny rezim ("Automaticka Krivka") :
   Hejna.c51, funkce AktualizujDenVykrmu
   nenacita hodnoty krivky ale dosadi primo :
   - NormovaneHmotnostiSamice/Samce vezme vcerejsi prumernou hmotnost ze
     statistiky nebo pocatecni hodnotu zadanou uzivatelem v krivce
   - vypocte HorniMezSamice/Samci, DolniMezSamice/Samci, vypoctem z
     NormovaneHmotnostiSamice/Samce  a Samice/SamciOkoliNad,
     Samice/SamciOkoliPod
   - respektuje nastaveni RozdelovatPohlavi
   - toto chovani se vynuti nastavenim AutomatickaKrivka = YES;

  Tyto hodnoty postacuji k funkci Modulu NVahy.c51

  Neprimo se toho da docilit definovanim rustove krivky s jednim
  bodem (aproximace vrati tento bod), beze zmeny v modulu Hejna.c51

2. Step.h
  Parametry :

  byte SamiceOkoliNad;                 // Okoli nad prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamiceOkoliPod;                 // Okoli pod prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamciOkoliNad;                  // Okoli nad prumerem pro samce v procentech
  byte SamciOkoliPod;                  // Okoli pod prumerem pro samce v procentech
  byte UstaleniNaslapneVahy;           // Ustaleni hmotnosti po nastupu v procentech prvni zvazene hmotnosti

  by se asi mely presunout do hejna, aby slo experimentovat s ruznym okolim

3. Po vypadeni z menu, pokud bylo neco zmeneno, zavolat FlashFlush() (zatim to
   delaji vsechny funkce)

4. Funkce VyberHejna by mela mit option Existujici/Neexistujici.
   Ve funkci Nove by potom nabidla pro vyber jen prazdna hejna,
   ve funkci Uprav jen existujici

5. Centralizace prepoctu A/D hodnoty na interni reprezentaci (TRawValue),
   prevody reprezentace pro zobrazeni (na BCD ?) a kalibrace
   do jednoho modulu

6. Do hlavni smycky pridat test Low Bat a volat explicitne
   FlashFlush() (Logger vypaluje hodnoty do flash az
   po naplneni stranky)

7. GSM komunikace by mozna byla vhodna pres asynchronni
   bufferovany COM modul (nemam odzkouseno -
   stejne se ceka na odeslani/prijeti zpravy)

8. Asi bych zavedl centralizovany cas : kazdou sekundu
   se precte RTC a dosadi do globalni promenne. Ostatni
   moduly berou cas odtud

=============================

9. Logger.c - zrusit DayInit
              zverejnit inicializaci dne - z indexu vyplni popisovac
              SampleFifo (viz DayInit) : LoggerSamples( DayIndex, &SampleFifo)
              v LoggerNewDay volat jen makro + FifoReset
              v ostatnich pripadech makro + FifoInit
              zverejnit ReadFragment - naplneni &Day (viz LoggerInit) :
              LoggerDay( DayIndex, &Day);
10. Revidovat operace na TLoggerSample - pristup k hodnote, hodine a flagu
11. Statisticke operace nad Loggerem do samostatneho modulu (mimo Logger.c51)
