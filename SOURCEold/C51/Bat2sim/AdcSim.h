//*****************************************************************************
//
//    Ads1241s.h  -  A/D convertor ADS1241 SINGLE channel
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ads1241s_H__
   #define __Ads1241s_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void AdcInit( void);
// Inicializace prevodniku

void AdcStart( byte Channel);
// Zahajeni periodickeho mereni

void AdcStop( void);
// Zastaveni periodickeho mereni

TYesNo AdcDataReady();
// Test na nove zmerenou hodnotu

dword AdcRead( void);
// Cteni merene hodnoty


#endif
