//*****************************************************************************
//
//    GsmCtl.h         Bat2 GSM control
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __GsmCtl_H__
   #define __GsmCtl_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define GSM_TIMER           5     // interval cteni zprav [s]

// Stav GSM modulu
typedef enum {
  GSM_OFF,      // Modul ma vypnute napajeni
  GSM_POWERED,  // Modul ma zapnute napajeni, ale jeste nekomunikuje (nezdaril se reset)
  GSM_LIVE,     // Modul ma zapnute napajeni a komunikuje, ale jeste neni prihlasen do site
  GSM_READY     // Modul je prihlasen do site, muze se odesilat/prijimat
} TGsmStatus;
extern TGsmStatus xdata GsmStatus;


void GsmInit( void);
// Inicializace modulu

void GsmStart();
// Zapnuti modulu

void GsmStop();
// Vypnuti modulu

void GsmExecute( void);
// Vybirani a provadeni doslych povelu (SMS)

void GsmCreateStatisticMsg();
// Z aktualnich dat vytvori SMS zpravu

void GsmSetMidnightMsg();
// Nastavi automat na vyslani SMS o pulnoci

void GsmSendStatus( void);
// Odesle aktualni stav vahy na <GsmNumber>

void GsmSendStatistics( void);
// Odesle statistiky na <GsmNumber>

#endif
