//*****************************************************************************
//
//    Exec.h - Vykonny modul
//    Version 1.0
//
//*****************************************************************************

#ifndef __Exec_H__
   #define __Exec_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void ExecInit();
// Inicializace vsech modulu

void ExecExecute();
// Hlavni smycka

void ExecNVahy();
// Obsluha naslapnych vah

void ExecPeriodic();
// Obsluha periodicke cinnosti

void NactiDatumCas();
// Nactu aktualni datum a cas v binarni podobe do globalni promenne

void ExecCheckVoltage();
// Kontrola napajeciho napeti, v pripade vypadku ceka, dokud se neobnovi

#endif
