//*****************************************************************************
//
//    Archiv.h     - Ukladani udaju
//    Version 1.0, (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __Archiv_H__
   #define __Archiv_H__

#ifndef __Hardware_H__
   #include "Hardware.h"        // zakladni datove typy
#endif

#ifndef __FlFifo_H__
   #include "..\inc\FlFifo.h"   // Fifo
#endif

#ifndef __Bat2_H__
   #include "Bat2.h"
#endif

// Pracovni data :
extern TFifo   __xdata__ DayFifo;
extern TFifo   __xdata__ SamplesFifo;
extern TArchiv __xdata__ Archiv;

extern TFifo code InitSamplesFifo;      // Pro zobrazeni loggeru

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void ArchivInit( void);
// Inicializace archivu (po nabehu napajeni)

void ArchivNewWeight( TRawValue Value, byte Gender);
// Prida nove zvazenou hodnotu do archivu

void ArchivNewHour( void);
// Zapise zmenu hodiny do archivu

void ArchivUpdate();
// Dosadim do archivu informace z TVykrmHejna

void ArchivSaveDay();
// Ulozi aktualni statistiku do archivu ve Flash

void ArchivFinishDay();
// Ukonci den

void ArchivNewDay();
// Zalozi novy den

TRawValue ArchivGetYesterdayAverage( byte Gender);
// Vrati vcerejsi prumer, nebo 0 pri chybe

TRawValue ArchivGetLastAverage(byte Gender);
// Prochazi archiv zezadu a vrati posledni nenulovy prumer, nebo 0 pri chybe

TRawValue LoggerGetWeight(TLoggerSample __xdata__ *Sample);
// Ze zadaneho vzorku vrati hmotnost

//-----------------------------------------------------------------------------
// Operace
//-----------------------------------------------------------------------------

void ArchivReset( void);
// Mazani archivu

#define ArchivReadDay( MyArchiv, Index)      FifoReadFragment( &DayFifo, (Index), &(MyArchiv), offsetof( TArchivDailyInfo, Archiv), sizeof( TArchiv))
// Nacte polozku <MyArchiv> z pozice <Index>

#define ArchivDaySamples( MySamples, Index)  MySamples.Base = FifoItemAddress( &DayFifo, (Index), offsetof( TArchivDailyInfo, Samples))
// Inicializuje popisovac fifo <MySamples> ze dne na pozici <Index>
// POZOR, fifo MySamples uz musi byt inicializovane, zde se ulozi jen Base

#endif
