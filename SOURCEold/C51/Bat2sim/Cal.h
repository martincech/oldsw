//*****************************************************************************
//
//    Cal.h    - Calibration load/save
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Cal_H__
   #define __Cal_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

void CalLoad();
// Nacte kalibraci z EEPROM
// Nekontroluje zadne checksumy a nenastavuje default hodnoty

bit CalSave();
// Ulozi celou kalibraci do EEPROM
// Vraci NO, nepovedl-li se zapis

void CalSaveToFlash();
// Ulozi celou kalibraci do Flash

#endif
