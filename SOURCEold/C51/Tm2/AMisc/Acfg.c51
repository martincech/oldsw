//*****************************************************************************
//
//    Acfg.c - Auto configuration load/save
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

//#define __DEBUG__

#include "..\Tm2.h"
#include "Acfg.h"
#include "..\inc\iep.h"
#include <stddef.h>       // makro offsetof
#ifdef __DEBUG__
   #include "..\Tm2\Adisplay\Adisplay.h"   // zobrazovaci jednotka
   #include <stdio.h>
#endif

#define ACFG_IEP_START   offsetof( TAutoIep, Config)    // adresa zacatku parametru v EEPROM

// Makro pro zapis polozky struktury :

#define WriteItem( name)                                      \
   address = ACFG_IEP_START  + offsetof( TAutoConfig, name);  \
   cfg = (byte *)&AutoConfig + offsetof( TAutoConfig, name);  \
   if( sizeof( AutoConfig.name) == 1){                        \
      IepPageWriteData( address, *cfg);                       \
   } else if( sizeof( AutoConfig.name) == 2){                 \
      IepPageWriteData( address, *cfg);                       \
      IepPageWriteData( ++address, *(cfg + 1));               \
   } else {                                                   \
      for( i = 0; i < sizeof( AutoConfig.name); i++){         \
         IepPageWriteData( address, *cfg);                    \
         address++;                                           \
         cfg++;                                               \
      }                                                       \
   }                                                          \

// Lokalni funkce :

static void SetDefaults( void);
// Nastavi implicitni hodnoty parametru

static byte CalcChecksum( void);
// Spocita a vrati zabezpeceni

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

TYesNo AcfgLoad( void)
// Nacte konfiguraci z EEPROM
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)
{
word           address;
byte __xdata__ *cfg;
byte           i;

#ifdef __DEBUG__
   DisplayGotoRC( 29, 0);
#endif
   address = ACFG_IEP_START;      // startovni adresa v EEPROM
   cfg = (byte *)&AutoConfig;     // pretypovani na byte array
   // Pozor, prenos XDATA->XDATA nelze pouzit blokove cteni :
   for( i = 0; i < sizeof( TAutoConfig); i++){
      *cfg = IepRead( address++);
      cfg++;
   }
   // kontrola kouzelneho cisla :
   if( AutoConfig.Magic != TM2_MAGIC){
#ifdef __DEBUG__
      printf( "ACFG : MAGIC error");
      Delay(2000);
#endif
      SetDefaults();
      AcfgSave( ACFG_ALL);
      return( NO);
   }
   // kontrola verze :
   if( AutoConfig.Verze != TM2_VERZE){
#ifdef __DEBUG__
      printf( "ACFG : VERSION mismatch");
      Delay(2000);
#endif
      SetDefaults();
      AcfgSave( ACFG_ALL);
      return( NO);
   }
   // kontrola sumy :
   if( CalcChecksum() != AutoConfig.KontrolniSuma){
#ifdef __DEBUG__
      printf( "ACFG : CHECKSUM error");
      Delay(2000);
#endif
      SetDefaults();
      AcfgSave( ACFG_ALL);
      return( NO);
   }
   return( YES);
} // AcfgLoad

//-----------------------------------------------------------------------------
// Save
//-----------------------------------------------------------------------------

TYesNo AcfgSave( dword mask)
// Ulozi polozky definovane maskou do EEPROM
// Vraci NO, nepovedl-li se zapis
{

word           address;
byte __xdata__ *cfg;
byte            i;

   // sluzebni informace :
   AutoConfig.Magic         = TM2_MAGIC;
   AutoConfig.Verze         = TM2_VERZE;
   AutoConfig.KontrolniSuma = CalcChecksum();
   if( mask == ACFG_ALL){
      // zapis celou konfiguraci :
      cfg = (byte *)&AutoConfig;     // pretypovani na byte array
      address = ACFG_IEP_START;      // startovni adresa v EEPROM
      if( !IepPageWriteStart()){
         return( NO);
      }
      for( i = 0; i < sizeof( TAutoConfig); i++){
         IepPageWriteData( address, *cfg);
         address++;
         cfg++;
      }
      IepPageWritePerform();
      return( YES);
   }
   if( mask & ACFG_MAGIC){
      WriteItem( Magic);
   }
   if( mask & ACFG_VERZE){
      WriteItem( Verze);
   }
   if( mask & ACFG_ODCHYLKA_SKRIN){
      WriteItem( MaxOdchylkaSkrin);
   }
   if( mask & ACFG_TEPLOTA_CILOVA_PREDNI){
      WriteItem( TeplotaCilova[TEPLOTA_PREDNI]);
   }
   if( mask & ACFG_TEPLOTA_CILOVA_STREDNI){
      WriteItem( TeplotaCilova[TEPLOTA_STREDNI]);
   }
   if( mask & ACFG_TEPLOTA_CILOVA_ZADNI){
      WriteItem( TeplotaCilova[TEPLOTA_ZADNI]);
   }
   if( mask & ACFG_PERIODA_UKLADANI){
      WriteItem( PeriodaUkladani);
   }
   if( mask & ACFG_IDENTIFIKACNI_CISLO){
      WriteItem( IdentifikacniCislo);
   }
   if( mask & ACFG_LOZENI){
      WriteItem( Lozeni);
   }
   if( mask & ACFG_ROZDELENA_SKRIN){
      WriteItem( RozdelenaSkrin);
   }
   if( mask & ACFG_LOGIN){
      WriteItem( Login);
   }
   // Kontrolni soucet vzdy :
   WriteItem( KontrolniSuma);
   //-------------------------------------------------
   IepPageWritePerform();           // Zapis do EEPROM
   return( YES);
} // AcfgSave

//-----------------------------------------------------------------------------
// Default
//-----------------------------------------------------------------------------

static void SetDefaults( void)
// Nastavi implicitni hodnoty parametru
{
   // 5.5.2003: Zmeneny parametry regulatoru na 30/300 a periody vymeny oleje a filtru na 250/500
   // 15.6.2004: Zmeneny periody vymen oleje a filtru na 200hod (oboje - podle Kuboty).
   AutoConfig.Magic               = TM2_MAGIC;
   AutoConfig.Verze               = TM2_VERZE;
#ifdef __AREMOTE__
   AutoConfig.IdentifikacniCislo  = 0;  // U prijimace je default 0, tj. aby prijimal od vsech vysilacu
#else
   AutoConfig.IdentifikacniCislo  = 1;
#endif
   AutoConfig.MaxOdchylkaSkrin    = 5;
   AutoConfig.TeplotaCilova[TEPLOTA_PREDNI]    = 25;
   AutoConfig.TeplotaCilova[TEPLOTA_STREDNI]   = 25;
   AutoConfig.TeplotaCilova[TEPLOTA_ZADNI]     = 25;
   AutoConfig.PeriodaUkladani     = 10;
   AutoConfig.Lozeni              = ALOC_CELE;
   AutoConfig.RozdelenaSkrin      = NO;
   AutoConfig.Login               = NO;
   AutoConfig.KontrolniSuma       = CalcChecksum();
   AcfgSave( ACFG_ALL);
} // SetDefaults

//-----------------------------------------------------------------------------
// Checksum
//-----------------------------------------------------------------------------

static byte CalcChecksum( void)
// Spocita a vrati zabezpeceni
{
byte __xdata__ *cfg;
byte            i;
byte            sum;

   cfg = (byte *)&AutoConfig;     // pretypovani na byte array
   sum = 0;
   // bez koncove sumy :
   for( i = 0; i < sizeof( TAutoConfig) - 1; i++){
      sum += cfg[ i];
   }
   return( sum);
} // CalcChecksum

#ifdef __DEBUG__
//-----------------------------------------------------------------------------
// Checksum
//-----------------------------------------------------------------------------

/*TYesNo AcfgChecksumOk( void)
// Porovna kontrolni soucet v EEPROM a v AutoConfig
{
word address;
byte i;
byte sum;

   address = ACFG_IEP_START;      // startovni adresa v EEPROM
   sum = 0;
   // bez koncove sumy :
   for( i = 0; i < sizeof( TAutoConfig) - 1; i++){
      sum += IepRead( address++);
   }
   DisplayGotoRC( 29, 0);
   printf( "ACFG : CRC EEP %02X ACfg %02X New %02X", (unsigned)sum,
                                                     (unsigned)AutoConfig.KontrolniSuma,
                                                     (unsigned)CalcChecksum());
   return( sum == AutoConfig.KontrolniSuma);
} // AcfgCompareChecksum*/
#endif
