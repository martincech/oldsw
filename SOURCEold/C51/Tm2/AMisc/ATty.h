//*****************************************************************************
//
//    ATty.h       - Remote terminal transmitter
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __ATty_H__
   #define __ATty_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


void TtyInit( void);
// Inicializace vysilace

void TtyExecute( void);
// Periodicka cinnost

void TtyPause( void);
// Zastaveni vysilani

void TtyResume( void);
// Rozbehnuti vysilani

#endif
