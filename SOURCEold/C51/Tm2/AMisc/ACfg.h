//*****************************************************************************
//
//    Acfg.h - Auto configuration load/save
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Acfg_H__
   #define __Acfg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

TYesNo AcfgLoad( void);
// Nacte konfiguraci z EEPROM
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)

TYesNo AcfgSave( dword mask);
// Ulozi polozky definovane maskou do EEPROM
// Maska se ORuje z konstant ACFG_... viz Auto.h
// Vraci NO, nepovedl-li se zapis

// Ladici funkce :

TYesNo AcfgChecksumOk( void);
// Porovna kontrolni soucet v EEPROM a v AutoConfig

#endif
