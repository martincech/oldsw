//*****************************************************************************
//
//    APrint.c     - Auto Printer services
//    Version 1.0, (c) P.Veit, Vymos
//
//*****************************************************************************

#include "Aprint.h"
#include "..\inc\System.h"      // Operacni system
//#include "..\inc\Pcf8583.h"     // Real-time clock
#include "..\inc\Rx8025.h"       // RTC
//#include "..\inc\RS232.h"       // Seriova linka
#include "..\inc\Com.h"      // Interni UART
#include "..\inc\Fifo.h"        // obsluha FIFO - zde pouze pro tisk
#include "..\inc\X25645.h"      // zabudovana EEPROM - zde pouze pro tisk

//-----------------------------------------------------------------------------
// Konstanty
//-----------------------------------------------------------------------------

#define MAX_ZNAKU_NA_RADEK 24      // Maximalni pocet znaku, ktere se vejdou na radek
#define TISK_OFFSET_X      0       // Na kterem sloupci zacnu tisknout datum a cas
#define TISK_TEPLOTA_OFFSET_X 20   // Kolik sloupcu je posunuta teplota od kraje
#define TISK_CEKANI_MEZI_RADKY 900 // Kolik milisekund se ma cekat mezi jendotlivymi radky, aby to tiskarna stacila tisknout

#ifdef JAZYK_FRANCOUZSTINA

unsigned char code TISK_PROTOCOL[]="PROTOCOLE DE TEMPERATURE";
unsigned char code TISK_PRINTED[]="Imprime:";
unsigned char code TISK_TARGET[]="Consigne:";
unsigned char code TISK_DEGREES[]="\x0F8C";
unsigned char code TISK_DATE[]="DATE";
unsigned char code TISK_TIME[]="HEURE";
unsigned char code TISK_AVERAGE[]="MOYENNE:";
unsigned char code TISK_TOO_MANY[]="TROP DE DONNEES";
unsigned char code TISK_DRIVER[]="CONDUCTEUR:";
unsigned char code TISK_CUSTOMER[]="CLIENT:";
unsigned char code TISK_END[]="FIN DU PROTOCOLE";

#else

unsigned char code TISK_PROTOCOL[]="TEMPERATURE PROTOCOL";
unsigned char code TISK_PRINTED[]="Printed:";
unsigned char code TISK_TARGET[]="Target temp.:";
unsigned char code TISK_DEGREES[]="\x0F8C";
unsigned char code TISK_DATE[]="DATE";
unsigned char code TISK_TIME[]="TIME";
unsigned char code TISK_AVERAGE[]="AVERAGE:";
unsigned char code TISK_TOO_MANY[]="TOO MANY RECORDS";
unsigned char code TISK_DRIVER[]="DRIVER:";
unsigned char code TISK_CUSTOMER[]="CUSTOMER:";
unsigned char code TISK_END[]="END OF PROTOCOL";

#endif

//-----------------------------------------------------------------------------
// Pomocna makra
//-----------------------------------------------------------------------------

#define TiskniHodnotuSNulou(Pozice, Hodnota) \
  if (Hodnota<10) {                          \
    Radek[Pozice]=0x30;                      \
    Radek[Pozice+1]=0x30+Hodnota;            \
  } else {                                   \
    Radek[Pozice]=0x30+Hodnota/10;           \
    Radek[Pozice+1]=0x30+Hodnota%10;         \
  }                                          \

#define UlozDoRadku(Pozice, Pole, Delka) for (UN.ST.X1=Pozice; UN.ST.X1<Pozice+Delka; UN.ST.X1++) Radek[UN.ST.X1]=Pole[UN.ST.X1-Pozice]

//-----------------------------------------------------------------------------
// Globalni data
//-----------------------------------------------------------------------------

dword __xdata__ PocatecniDatum, KoncoveDatum;
word  __xdata__ PocatecniCas, KoncovyCas;
byte  __xdata__ PocetZaznamuNaRadek;

//-----------------------------------------------------------------------------
// Lokalni data
//-----------------------------------------------------------------------------

static word __xdata__ Start[2], Stop[2];         // Odkud kam (vcetne) se maji zaznamy cist
static byte __xdata__ PocetOpakovani;            // Kolikrat se cyklus projizdi (bud 1x pokud se neprepisuje, nebo 2x pokud se prepisuje)
static byte __xdata__ Radek[MAX_ZNAKU_NA_RADEK]; // Jednotlive znaky v radku, ktery postupne vytvarim

//-----------------------------------------------------------------------------
// Lokalni funkce
//-----------------------------------------------------------------------------

int VypoctiPrumernouTeplotu(TLocalizationStatus Lozeni);
// Vypocet prumerne teploty podle lozeni, vraci teplotu na desetiny (tj. napr. 215 pri 21.5 stupne).

TYesNo PrunikLozeni(TLocalizationStatus Lozeni);
  // Udela prunik AutoLog.Lozeni a zadaneho <Lozeni> a pokud se protinaji, vrati Yes.

static void DelaySysYield( word Ms);
// Ceka Ms milisekund, cekani rozouskuje na useku 10ms a v prubehu cekani tak vola SysYield()

static void VymazRadek( void);
// Smaze cely obsah radku, vyuziva UN.ST.X1

static void TiskniRadek( void);
// Posle radek na tiskarnu

static void TiskniLinku( void);
// Posle caru na tiskarnu

//-----------------------------------------------------------------------------
// Inicializace linky
//-----------------------------------------------------------------------------

void PrintStart( void)
// Inicializuje linku tiskarny
{
  // Inicializace linky - sdileny COM :
//  RS232Power = 1;             // zapnuti napajeni vystupniho budice
  AutoPRINT=0;
  ComInit();
/*  SetTimer2Baud(RS232_BAUD);
  ES=0;                       // zakaz preruseni od seriove linky*/
} // PrintStart

//-----------------------------------------------------------------------------
// Uvolneni linky
//-----------------------------------------------------------------------------

void PrintStop( void)
// Uvolni linku tiskarny
{
  while( !TI);                // cekej na vyslani
//  RS232Power = 0;             // vypnuti napajeni vystupniho budice
  AutoPRINT=1;
} // PrintStop

//-----------------------------------------------------------------------------
// Inicializace tiskarny
//-----------------------------------------------------------------------------

void PrintSetup( void)
// Nastaveni modu tiskarny
{
  ComTxChar(0x1B); ComTxChar('A');   // Smazani vstupniho bufferu
  ComTxChar(0x1B); ComTxChar('y'); ComTxChar(0x02);  // Vypnuti LEDky
  ComTxChar(0x1B); ComTxChar('['); ComTxChar(8); ComTxChar(24);  // Zpomaleni, aby to moc nezralo
} // PrintSetup

//-----------------------------------------------------------------------------
// Priprava tisku protokolu
//-----------------------------------------------------------------------------

TYesNo PrintPrepare( void)
// Prohleda FIFO, nastavi ukazatele, vraci NO je-li prazdne
{
word Adresa; // Adresa ve fifo - fyzicka adresa jednotlivych bajtu, ne poradove cislo zaznamu - ma to tedy krok sizeof(TFifoData)

  // Predem si pripravim odkud kam mam cist
  Adresa=FifoFindMarker();
  if (EepReadByte(Adresa)==AUTO_FIFO_MARKER_EMPTY) {
    // Fifo se jeste neprepisuje
    if (Adresa==FIFO_START) {
      return( NO);  // Fifo je prazdne, neni co tisknout
    }
    Start[0]=FIFO_START; Stop[0]=Adresa-sizeof(TFifoData);
    PocetOpakovani=1;
  } else {
    // Fifo uz se prepisuje
    // Jedu od pozice markeru+1 na konec a pak od zacatku po marker
    Start[0]=Adresa+sizeof(TFifoData); Stop[0]=FIFO_START + (FIFO_CAPACITY-1) * sizeof(TFifoData);
    Start[1]=FIFO_START; Stop[1]=Adresa-sizeof(TFifoData);
    PocetOpakovani=2;
  }//else
  return( YES);
} // PrintPrepare

//-----------------------------------------------------------------------------
// Tisk protokolu
//-----------------------------------------------------------------------------

/*void TestPrint() {
  PrintSetup();
  ComTxChar(0x0D);
  ComTxChar(0x0D);
  ComTxChar(0x0D);
  ComTxChar(0x0D);
  TiskniLinku();
    VymazRadek();
    UlozDoRadku(3, TISK_PROTOCOL, 18); TiskniRadek();
  TiskniLinku();
    VymazRadek();
    UlozDoRadku(3, TISK_PROTOCOL, 18); TiskniRadek();
  TiskniLinku();
    VymazRadek();
    UlozDoRadku(3, TISK_PROTOCOL, 18); TiskniRadek();
  TiskniLinku();
  ComTxChar(0x0D);
  ComTxChar(0x0D);
  ComTxChar(0x0D);
  ComTxChar(0x0D);
}*/

void PrintProtocol(TLocalizationStatus Lozeni)
// Vytiskne zaznamy za cele obdobi, ktere je v pameti. Bere jen teploty zadane v <Lozeni>
{
  unsigned long __xdata__ DatumZaznamu;
  unsigned int __xdata__  CasZaznamu;
  unsigned char __xdata__ PocetZaznamuVSume;   // Kolik mam napocitano vzorku v sume
  int __xdata__ Suma;                          // Suma teplot, staci int, musi byt se znamenkem
  long __xdata__ CelkovaSuma;  // Suma teplot z jednotlivych radku - pro celkovy prumer
  unsigned int __xdata__ PocetRadku;    // Celkovy pocet radku v tabulce
  unsigned char __xdata__ Opakovani;        // Pro cyklus for
  word Adresa; // Adresa ve fifo - fyzicka adresa jednotlivych bajtu, ne poradove cislo zaznamu - ma to tedy krok sizeof(TFifoData)
  TAutoUdalost __xdata__ *Udalost;          // Na pretypovani zaznamu na udalost - pro otestovani, zda jde o pomocny zaznam
  char CilovaTeplota;

  // Zeptam se ho na pocatecni a koncovy datum a cas
  // Pocatecni datum

  // Vynuluju pocitadla
  PocetZaznamuVSume=0;
  Suma=0;
  CelkovaSuma=0;
  PocetRadku=0;
  VymazRadek();

  // Pripravim HW pro vysilani po seriove lince
  PrintSetup();

    // Vytisknu hlavicku
    ComTxChar(0x0D); ComTxChar(0x0D);  // Povyjedu 1 radek, aby se tam srovnal papir - nekdy to vytisklo jen pulku prvniho radku

    VymazRadek();
#ifdef JAZYK_FRANCOUZSTINA
    UlozDoRadku(0, TISK_PROTOCOL, 24); TiskniRadek();
#else
    UlozDoRadku(2, TISK_PROTOCOL, 20); TiskniRadek();
#endif
    TiskniLinku();
    // Datum tisku
    VymazRadek();
    UlozDoRadku(0, TISK_PRINTED, 8);
    UN.ST.X4=RtcDay();
    Radek[14]=0x30+hnib(UN.ST.X4);
    Radek[15]=0x30+lnib(UN.ST.X4);
    Radek[16]='.';
    UN.ST.X4=RtcMonth();
    Radek[17]=0x30+hnib(UN.ST.X4);
    Radek[18]=0x30+lnib(UN.ST.X4);
    Radek[19]='.';
    UN.DT=RtcYear();
    Radek[20]=0x30+hnib(UN.ST.X3);
    Radek[21]=0x30+lnib(UN.ST.X3);
    Radek[22]=0x30+hnib(UN.ST.X4);
    Radek[23]=0x30+lnib(UN.ST.X4);
    TiskniRadek();

    DelaySysYield(TISK_CEKANI_MEZI_RADKY);

    // Cas tisku
    VymazRadek();
    UN.ST.X4=RtcHour();
    Radek[14]=0x30+hnib(UN.ST.X4);
    Radek[15]=0x30+lnib(UN.ST.X4);
    Radek[16]=':';
    UN.ST.X4=RtcMin();
    Radek[17]=0x30+hnib(UN.ST.X4);
    Radek[18]=0x30+lnib(UN.ST.X4);
    Radek[19]=':';
    UN.ST.X4=RtcSec();
    Radek[20]=0x30+hnib(UN.ST.X4);
    Radek[21]=0x30+lnib(UN.ST.X4);
    TiskniRadek();
    // Cilova teplota
    VymazRadek();
#ifdef JAZYK_FRANCOUZSTINA
    UlozDoRadku(0, TISK_TARGET, 9);
#else
    UlozDoRadku(0, TISK_TARGET, 13);
#endif
    switch (Lozeni) {   // Cilova teplota je urcena lozenim, ktere zadal pri tisku (ne lozenim v kazdem zaznamu)
      case ALOC_CELE:
      case ALOC_PREDNI_POLOVINA:
      case ALOC_KRAJNI_TRETINY:
      case ALOC_PREDNI_TRETINA: {
        CilovaTeplota = AutoConfig.TeplotaCilova[TEPLOTA_PREDNI];
        break;
      }
      case ALOC_ZADNI_POLOVINA:
      case ALOC_STREDNI_TRETINA: {
        CilovaTeplota = AutoConfig.TeplotaCilova[TEPLOTA_STREDNI];
        break;
      }
      case ALOC_ZADNI_TRETINA: {
        CilovaTeplota = AutoConfig.TeplotaCilova[TEPLOTA_ZADNI];
        break;
      }
    }//switch
    if (CilovaTeplota < 0) {
      Radek[13] = '-';
      CilovaTeplota = -CilovaTeplota;
    }
    if (CilovaTeplota > 9) Radek[14] = 0x30 + CilovaTeplota / 10;
    Radek[15] = 0x30 + CilovaTeplota % 10;
    UlozDoRadku(17, TISK_DEGREES, 2);
    TiskniRadek();
    TiskniLinku();
    // Popis sloupcu tabulky
    VymazRadek();
    UlozDoRadku(3, TISK_DATE, 4);
#ifdef JAZYK_FRANCOUZSTINA
    UlozDoRadku(12, TISK_TIME, 5);
#else
    UlozDoRadku(12, TISK_TIME, 4);
#endif
    UlozDoRadku(21, TISK_DEGREES, 2);
    TiskniRadek();
    TiskniLinku();

    DelaySysYield(TISK_CEKANI_MEZI_RADKY);

    for (Opakovani=0; Opakovani<PocetOpakovani; Opakovani++) {
      for (Adresa=Start[Opakovani]; Adresa<=Stop[Opakovani]; Adresa+=sizeof(TFifoData)) {
        SysYield();  // Behem tisku merim a reguluju, akorat neukladam
        FifoRead(Adresa);  // Nactu zaznam z EEPROM do AutoData

        // Pomocne zaznamy me nezajimaji
        Udalost=(TAutoUdalost*) &AutoLog;
        if (Udalost->TypZaznamu==AUTO_FIFO_PROTOCOL_TYPE) continue;  // Jde o pomocny zaznam, preskocim ho a jdu na dalsi

        // Vytvorim datum zaznamu
        UN.Word.Lsb=RtcYear();  // Aktualni rok v bcd
        UN.Word.Lsb=1000*(unsigned int)hnib(UN.ST.X3)+100*(unsigned int)lnib(UN.ST.X3)+10*(unsigned int)hnib(UN.ST.X4)+(unsigned int)lnib(UN.ST.X4);  // Prevedu na bin
        DatumZaznamu=(unsigned int)(UN.Word.Lsb & ~0x0007 | AutoLog.DateTime.Year);  // Aktualni datum
        if (DatumZaznamu > UN.Word.Lsb) {
          DatumZaznamu-=8;   // Nemuze byt vetsi nez soucasny
        }//if
        DatumZaznamu*=10000L;  // Posunu rok na prvni pozici
        DatumZaznamu+=100L*(unsigned long)AutoLog.DateTime.Month;
        DatumZaznamu+=(unsigned long)AutoLog.DateTime.Day;
        // Vytvorim cas zaznamu
        CasZaznamu=100*gethour(AutoLog.DateTime) + AutoLog.DateTime.Min;

        // Pokud je datum a cas zaznamu mensi nez pocatecni datum a cas, od ktereho chce tisknout, zahodim tento zaznam
        if (DatumZaznamu<PocatecniDatum) {
          continue;
        } else if (DatumZaznamu==PocatecniDatum) {
          // Jde o stejne dny, musim jeste porovnat casy
          if (CasZaznamu<PocatecniCas) continue;   // Pokud je cas rovny, zaznam beru
        }//if
        // Pokud je datum a cas zaznamu vetsi nez koncovy datum a cas, od ktereho chce tisknout, zahodim tento zaznam
        if (DatumZaznamu>KoncoveDatum) {
          continue;
        } else if (DatumZaznamu==KoncoveDatum) {
          // Jde o stejne dny, musim jeste porovnat casy
          if (CasZaznamu>KoncovyCas) continue;   // Pokud je cas rovny, zaznam beru
        }//if

        // Pokud dosel az sem, tento zaznam spada to zadaneho casoveho obdobi
        // Prictu tento zaznam do sumy
        if (PrunikLozeni(Lozeni)) {
          // Pokud je v zanznamu nejaka teplota, ktera odpovida zadanemu lozeni pri tisku, prictu ji do prumeru
          Suma+=VypoctiPrumernouTeplotu(Lozeni);
          PocetZaznamuVSume++;
        }
        // Pokud jsem nasumoval zadany pocet zaznamu, vypoctu prumer a vytisknu radek
        if (PocetZaznamuVSume>=PocetZaznamuNaRadek) {
          VymazRadek();
          // Vytisknu datum a cas tohoto, posledniho zaznamu v prumeru
          // Datum
          TiskniHodnotuSNulou(TISK_OFFSET_X, AutoLog.DateTime.Day);
          Radek[TISK_OFFSET_X+2]='/';
          TiskniHodnotuSNulou(TISK_OFFSET_X+3, AutoLog.DateTime.Month);
          Radek[TISK_OFFSET_X+5]='/';
          UN.DT=bindec(DatumZaznamu/10000L);
          Radek[TISK_OFFSET_X+6]=0x30+hnib(UN.ST.X3);
          Radek[TISK_OFFSET_X+7]=0x30+lnib(UN.ST.X3);
          Radek[TISK_OFFSET_X+8]=0x30+hnib(UN.ST.X4);
          Radek[TISK_OFFSET_X+9]=0x30+lnib(UN.ST.X4);
          // Cas
          TiskniHodnotuSNulou(TISK_OFFSET_X+12, gethour(AutoLog.DateTime));
          Radek[TISK_OFFSET_X+14]=':';
          TiskniHodnotuSNulou(TISK_OFFSET_X+15, AutoLog.DateTime.Min);
          // Teplota
          Suma/=(int)PocetZaznamuVSume;
          // Omezim
          if (Suma>999) Suma=999;
          if (Suma<-999) Suma=-999;
          // Prictu teplotu do celkove sumy
          CelkovaSuma+=Suma;
          PocetRadku++;
          if (Suma<0) {
            Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X-1]='-';  // Minusko
            Suma=-Suma;
          }
          // Vytisknu teplotu
          UN.DT=bindec(Suma);
          Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X]=0x30+lnib(UN.ST.X3);
          Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X+1]=0x30+hnib(UN.ST.X4);
          Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X+2]='.';
          Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X+3]=0x30+lnib(UN.ST.X4);
          // Poslu radek na tiskarnu
          TiskniRadek();
          DelaySysYield(TISK_CEKANI_MEZI_RADKY);        // Tiskarna to nestiha tisknout a preteka seriovy buffer
          // Vynuluju promenne
          Suma=0;
          PocetZaznamuVSume=0;
          // Osetrim pripadne zacykleni
          if (PocetRadku > AUTO_FIFO_CAPACITY/PocetZaznamuNaRadek || PocetRadku>100) {
            // Nemuze tam byt vic radku nez pocet zaznamu deleno poctem zaznamu na radek. Radsi to omezim i na pevny max. pocet radku, aby nedal periodu
            // 1 minuta na celou pamet - tiskarna by to nemusela vydrzet
            VymazRadek();
            UlozDoRadku(0, TISK_TOO_MANY, 16);
            TiskniRadek();
            break;  // Vylezu z for a ukoncim tisk
          }//if
        }//if
      }//for
    }//for

    // Pokud se tiskne treba jen 1 radek, buffer se nestaci vyprazdnit (behem tisku radku se ceka) a pak to nedotiskne => musim tady chvili pockat - behem cekani volam SysYield(), musim tedy cekat pomoci cyklu
    DelaySysYield(1000);        // Tiskarna to nestiha tisknout a preteka seriovy buffer

    TiskniLinku();
    // Na zaver vytisknu prumernou celkovou teplotu behem prepravy
    VymazRadek();
    UlozDoRadku(0, TISK_AVERAGE, 8);
    if (PocetRadku>0) CelkovaSuma/=(long)PocetRadku; else CelkovaSuma=0;
    if (CelkovaSuma<0) {
      Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X-1]='-';  // Minusko
      CelkovaSuma=-CelkovaSuma;
    }
    UN.DT=bindec(CelkovaSuma);
    Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X]=0x30+lnib(UN.ST.X3);
    Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X+1]=0x30+hnib(UN.ST.X4);
    Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X+2]='.';
    Radek[TISK_OFFSET_X+TISK_TEPLOTA_OFFSET_X+3]=0x30+lnib(UN.ST.X4);
    // Poslu radek na tiskarnu
    TiskniRadek();
    TiskniLinku();
    // Podpisy
    VymazRadek();
#ifdef JAZYK_FRANCOUZSTINA
    UlozDoRadku(0, TISK_DRIVER, 11);
#else
    UlozDoRadku(0, TISK_DRIVER, 7);
#endif
    TiskniRadek();
    for (UN.ST.X1=0; UN.ST.X1<6; UN.ST.X1++) ComTxChar(0x0D);   // Vynecham misto na podpis
    TiskniLinku();
    VymazRadek();
#ifdef JAZYK_FRANCOUZSTINA
    UlozDoRadku(0, TISK_CUSTOMER, 7);
#else
    UlozDoRadku(0, TISK_CUSTOMER, 9);
#endif
    TiskniRadek();
    for (UN.ST.X1=0; UN.ST.X1<6; UN.ST.X1++) ComTxChar(0x0D);   // Vynecham misto na podpis
    TiskniLinku();
    // Vytisknu, ze je konec protokolu
    VymazRadek();
    UlozDoRadku(4, TISK_END, 15); TiskniRadek();
    TiskniLinku();

    // Povysunu trochu papir
    ComTxChar(0x0D); ComTxChar(0x0D); ComTxChar(0x0D); ComTxChar(0x0D); ComTxChar(0x0D); ComTxChar(0x0D); ComTxChar(0x0D);

    // Povyjeti tiskarny (podpisy) dost trva => musim chvili pockat - behem cekani volam SysYield(), musim tedy cekat pomoci cyklu
    DelaySysYield(5000);
    SysFlushTimeout();        // Musim rozjet odpocitavani necinnosti znovu - tisk trva dlouho a po vytisteni mne to hazelo hned timeout.
} // PrintProtocol

//-----------------------------------------------------------------------------
// Prumerna teplota
//-----------------------------------------------------------------------------

int VypoctiPrumernouTeplotu(TLocalizationStatus Lozeni) {
// Vypocet prumerne teploty podle lozeni - celkove lozeni se urci jako prunik zadaneho <Lozeni> pri tisku a lozeni v kazdem zaznamu
// z globalni struktury AutoLog.
// Vraci teplotu na desetiny (tj. napr. 215 pri 21.5 stupne).
// POZOR: znici teploty v globalni strukture AutoLog!
int suma=0;
byte count=0;

   // Vynuluju teploty mimo zadane lozeni
   switch (Lozeni) {
      case ALOC_PREDNI_POLOVINA :
         AutoLog.TeplotaZadni = AUTO_TEMP_ERROR_LOG;
         break;
      case ALOC_ZADNI_POLOVINA :
         AutoLog.TeplotaPredni = AUTO_TEMP_ERROR_LOG;
         break;
      case ALOC_KRAJNI_TRETINY:
         AutoLog.TeplotaStredni = AUTO_TEMP_ERROR_LOG;
         break;
      case ALOC_PREDNI_TRETINA :
         AutoLog.TeplotaStredni = AUTO_TEMP_ERROR_LOG;
         AutoLog.TeplotaZadni = AUTO_TEMP_ERROR_LOG;
         break;
      case ALOC_STREDNI_TRETINA :
         AutoLog.TeplotaPredni = AUTO_TEMP_ERROR_LOG;
         AutoLog.TeplotaZadni = AUTO_TEMP_ERROR_LOG;
         break;
      case ALOC_ZADNI_TRETINA :
         AutoLog.TeplotaPredni = AUTO_TEMP_ERROR_LOG;
         AutoLog.TeplotaStredni = AUTO_TEMP_ERROR_LOG;
         break;
   }//switch

   // Vypoctu prumer ze zbyvajicich teplot podle lozeni ulozeneho v zaznamu
   switch (AutoLog.Lozeni) {
      case ALOC_CELE :
         // prumer ze vsech tri teplot
         if( AutoLog.TeplotaPredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaPredni;
            count++;
         }
         if( AutoLog.TeplotaStredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaStredni;
            count++;
         }
         if( AutoLog.TeplotaZadni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaZadni;
            count++;
         }
         break;

      case ALOC_PREDNI_POLOVINA :
         // prumer ze prednich dvou teplot
         if( AutoLog.TeplotaPredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaPredni;
            count++;
         }
         if( AutoLog.TeplotaStredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaStredni;
            count++;
         }
         break;

      case ALOC_ZADNI_POLOVINA :
         // prumer ze zadnich dvou teplot
         if( AutoLog.TeplotaStredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaStredni;
            count++;
         }
         if( AutoLog.TeplotaZadni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaZadni;
            count++;
         }
         break;

      case ALOC_KRAJNI_TRETINY :
         // prumer ze zadnich krajnich teplot
         if( AutoLog.TeplotaPredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaPredni;
            count++;
         }
         if( AutoLog.TeplotaZadni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaZadni;
            count++;
         }
         break;

      case ALOC_PREDNI_TRETINA :
         // prumer ze vsech tri teplot
         if( AutoLog.TeplotaPredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaPredni;
            count++;
         }
         break;

      case ALOC_STREDNI_TRETINA :
         // prumer ze vsech tri teplot
         if( AutoLog.TeplotaStredni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaStredni;
            count++;
         }
         break;

      case ALOC_ZADNI_TRETINA :
         // prumer ze vsech tri teplot
         if( AutoLog.TeplotaZadni != AUTO_TEMP_ERROR_LOG){
            // zapocitej do prumeru
            suma += AutoLog.TeplotaZadni;
            count++;
         }
         break;
   }
   if (count != 0){
      return( 10*suma / count);       // prumerna teplota z mericich cidel
   } else {
      return 0;  // zadne cidlo nemeri - zde nemuzu pouzit hodnotu AUTO_TEMP_ERROR_LOG, protoze zde je to na desetiny (-999 az 999). Pouziju radsi nulu.
   }
}

//-----------------------------------------------------------------------------
// Prunik lozeni
//-----------------------------------------------------------------------------

TYesNo PrunikLozeni(TLocalizationStatus Lozeni) {
  // Udela prunik AutoLog.Lozeni a zadaneho <Lozeni> a pokud se protinaji, vrati Yes.
  // Vyloucim pripady, kdy se lozeni neprotinaji
  switch (AutoLog.Lozeni) {
    case ALOC_PREDNI_POLOVINA :
      switch (Lozeni) {
        case ALOC_ZADNI_TRETINA :
          return NO;
      }
      break;
    case ALOC_ZADNI_POLOVINA :
      switch (Lozeni) {
        case ALOC_PREDNI_TRETINA :
          return NO;
      }
      break;
    case ALOC_KRAJNI_TRETINY:
      switch (Lozeni) {
        case ALOC_STREDNI_TRETINA :
          return NO;
      }
      break;
    case ALOC_PREDNI_TRETINA :
      switch (Lozeni) {
        case ALOC_ZADNI_POLOVINA :
        case ALOC_STREDNI_TRETINA :
        case ALOC_ZADNI_TRETINA :
          return NO;
      }
      break;
    case ALOC_STREDNI_TRETINA :
      switch (Lozeni) {
        case ALOC_KRAJNI_TRETINY:
        case ALOC_PREDNI_TRETINA :
        case ALOC_ZADNI_TRETINA :
          return NO;
      }
      break;
    case ALOC_ZADNI_TRETINA :
      switch (Lozeni) {
        case ALOC_PREDNI_POLOVINA :
        case ALOC_PREDNI_TRETINA :
        case ALOC_STREDNI_TRETINA :
          return NO;
      }
      break;
  }//switch
  // Pokud dosel az sem, lozeni se protinaji
  return YES;
}


//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

static void DelaySysYield(unsigned int Ms) {
  // Ceka Ms milisekund, cekani rozouskuje na useku 10ms a v prubehu cekani tak vola SysYield()
  unsigned int Pocet;
  for (Pocet=0; Pocet<Ms/10; Pocet++) {
    SysYield();
    SysDelay(10);
  }//for
}

//-----------------------------------------------------------------------------
// Prace s radky
//-----------------------------------------------------------------------------

static void VymazRadek( void)
// Smaze cely obsah radku
{
byte i;

   for( i=0; i<MAX_ZNAKU_NA_RADEK; i++){
      Radek[ i]=' ';
   }
} // VymazRadek

static void TiskniRadek( void)
// Posle radek na tiskarnu
{
byte i;

    for(i=0; i<MAX_ZNAKU_NA_RADEK; i++){
       ComTxChar(Radek[i]);
    }
    ComTxChar(0x0D);
} // TiskniRadek

static void TiskniLinku( void)
// Posle caru na tiskarnu
{
byte i;

    for(i=0; i<MAX_ZNAKU_NA_RADEK; i++){
       ComTxChar('-');
    }
    ComTxChar(0x0D);
} // TiskniLinku
