//*****************************************************************************
//
//    Adisplay.h - Auto display module
//    Version 1.0, (c) P.Veit
//
//*****************************************************************************

#ifndef __ADisplay_H__
   #define __ADisplay_H__

#ifndef __Tm2_H__
   #include "..\Tm2.h"  // vsechny potrebne definice
#endif

// Definice udalosti :
// definice klaves viz AutoHW.h

typedef enum {
// systemove klavesy :
//   K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
//  _K_FIRSTUSER,
// klavesy :
//   K_RIGHT = _K_FIRSTUSER,  // sipka doprava
//   K_UP,                    // sipka nahoru
//   K_ENTER,                 // Enter
//   K_ESC,                   // Esc
  _K_EVENTS    = 0x40,        // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,   // prekresleni namerenych hodnot
  K_BLINK_OFF,                // periodicke blikani - zhasni
  K_BLINK_ON,                 // periodicke blikani - rozsvit
  K_TIMEOUT                   // vyprsel cas necinnosti
// systemove klavesy :
//   K_REPEAT       = 0x80,   // opakovani klavesy OR ke klavese
//   K_RELEASED     = 0xFE,   //!!! nepouzivat, zatim nefunguje : pusteni klavesy (jednotlive i autorepeat)
//   K_IDLE         = 0xFF    // nepouzivat, vyhrazeno k internimu pouziti, prazdny cyklus cteni
} TEventType;

// Hlidani poruchy teploty uvnitr skrine (teplota ve skrini nedosahne cilove teploty)
#define TEPLOTA_MAX_DOBA_NABEHU   2400  // Kolik sekund ma teplota na dosahnuti pasma kolem cilove teploty od doby prepnuti rezimu nebo zmeny cilove teploty
#define TEPLOTA_MAX_DOBA_PREKMITU 300   // Kolik sekund ma teplota na "vylet" (prekmit) mimo pasmo kolem cilove teploty
#define PORUCHY_TEPLOT_POCET 3          // Celkovy pocet hlidanych teplot
typedef struct {
  unsigned int PocitadloMimoPasmo;      // Kolik sekund je teplota mimo zadane pasmo, tj. v chybe
  unsigned char DosahnutePasmo;         // Flag, ze se jiz jednou teplota dostala do pasma cilove teploty. Nuluju po zapnuti a prepnuti rezimu.
} TPoruchaTeploty;



//-----------------------------------------------------------------------------

void SmazVerziFirmware();

void AdispInit( void);
// Inicializace zobrazeni

void AdispExecute( void);
// Hlavni smycka programu

void NastaveniTiskarny();
// Nastavi tiskarnu tak, aby neblikala

void NulujPoruchyTeplotyVeSkrini();
// Vynuluje pocitadla poruch teploty (mimo pasmo cilove teploty) - volat po kazdem prepnuti rezimu

void MenuLogin();
  // Provede login ridice

//-----------------------------------------------------------------------------
// stdio

typedef enum {
   DISPLAY_PLANE1,
   DISPLAY_PLANE2
} display_plane_t;

void DisplayGotoRC( byte r, byte c);
// Presun kurzoru na radek <r> sloupec <c>

char putchar( char c);
// standardni zapis znaku

void DisplaySetPlane(bit plane);
void DisplayFillBox(unsigned int X, unsigned char Y, unsigned int Sirka, unsigned char Vyska, unsigned char Value);
void DisplayPlane(bit on);


void ZobrazTlacitkoSTextem(byte X, byte Y, byte Delka, byte code *Text);

#endif
