//*****************************************************************************
//
//    PktDef.h  - Packet data definitions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __PktDef_H__
   #define __PktDef_H__

#define DATA_SIZE 6          // pocet datovych bytu paketu
#define STX_CHAR  0x55       // naraznikovy znak
#define NULL_CHAR 0xAA       // vyplnovy znak

//-----------------------------------------------------------------------------
// Typy paketu
//-----------------------------------------------------------------------------

typedef enum {
   PKT_NULL = 0xA0,          // prazdny paket
   PKT_TIME,                 // cas
   PKT_TEMPERATURE,          // teploty
   PKT_CONFIG,               // konfiguracni data
   _PKT_LAST,                // posledni kod
   PKT_STOP                  // pomocny kod, stop vysilace
} TPacketType;

//-----------------------------------------------------------------------------
// Data paketu
//-----------------------------------------------------------------------------

// Casovy paket, hodnoty v BCD :
typedef struct {
   byte Hour;
   byte Min;
   byte Sec;
   byte Day;
   byte Month;
   byte Dummy;
   // Rok je v jinem paketu
} TTimeData;

// Paket teplot, hodnoty v 1C :
typedef struct {
   char TeplotaVenkovni;
   char TeplotaKanal;
   char TeplotaPredni;
   char TeplotaStredni;
   char TeplotaZadni;
   char TeplotaVyfukovana;
} TTemperatureData;

// Konfiguracni data
typedef struct {
   // Zbytek datumu
   word Year;
   // Konfigurace a stavove veliciny:
   byte PoruchaTeplotaPredni  : 1,
        PoruchaTeplotaStredni : 1,
        PoruchaTeplotaZadni   : 1,
        ZobrazitDiagnostiku   : 1,
        Lozeni                : 3,    // TLocalizationStatus
        RozdelenaSkrin        : 1;
   char TeplotaCilova[3];
} TConfigData;

// Souhrnny union :

typedef union {
   byte             Byte[DATA_SIZE];    // data jako byty
   TTimeData        Time;
   TTemperatureData Temp;
   TConfigData      Config;
} TPacketData;

//-----------------------------------------------------------------------------
// Definice paketu
//-----------------------------------------------------------------------------

typedef struct {
   byte        Stx;                 // naraznik - start
   byte        Id;                  // Identifikacni cislo pristroje (POZOR, nesmi byt vetsi nez STX_CHAR)
   byte        Address;             // cislo paketu (POZOR, nesmi byt STX_CHAR)
   TPacketData Data;                // data
   byte        Crc;                 // zabezpeceni
} TPacket;

#endif

