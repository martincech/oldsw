//*****************************************************************************
//
//    Auto.h - Auto common data definitions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Tm2_H__
   #define __Tm2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#ifdef __C51__
// jen pro Keil
//   typedef int  int16;
//   typedef long long32;
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
   #include <stddef.h>        // makro offsetof
#endif


//-----------------------------------------------------------------------------
// Jazyk
//-----------------------------------------------------------------------------

#define JAZYK_CESTINA 1
//#define JAZYK_ANGLICTINA 1
//#define JAZYK_FRANCOUZSTINA 1
//#define JAZYK_RUSTINA 1
//#define JAZYK_POLSTINA 1

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define TM2_VERZE    0x0101       // verze SW
#define TM2_MAGIC    0x55AA       // identifikace konfiguracniho souboru

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define MAX_PODSVIT 4
#define MIN_PODSVIT 1
extern byte Podsvit;                    // Definovano v main

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

// identifikace teplomeru :
// Pozor, musi pozicne odpovidat konstantam MWI_CH0..MWI_CH4
// 6.3.2003: Musi take zacinat od nuly, protoze je pouzivam jako indexy v poli pro prumerovani teplot
typedef enum {
   ATEMP_PREDNI,         // T1
   ATEMP_STREDNI,        // T2
   ATEMP_ZADNI,          // T3
   ATEMP_KANAL,          // T4
   ATEMP_VENKOVNI,       // T5
   ATEMP_VYFUKOVANA      // T6
} TThermoAddress;

#define ATEMP_COUNT 6    // Celkovy pocet merenych teplot - kvuli poli pro prumerovani

#define AUTO_TEMP_ERROR     (-100<<8)  // chyba mereni -100C misto merene hodnoty
#define AUTO_TEMP_ERROR_LOG -100       // chyba mereni -100C misto merene hodnoty pro loger na cele stupne

// Teplota : horni byte je v celych stupnich,
// dolni MSB 0.5C...0.0625C 0 0 0 0 LSB
// Na float se prevede jako temp/256

// Typ ulozeni :
typedef enum {
   ALOC_CELE,
   ALOC_PREDNI_POLOVINA,
   ALOC_ZADNI_POLOVINA,
   ALOC_KRAJNI_TRETINY,
   ALOC_PREDNI_TRETINA,
   ALOC_STREDNI_TRETINA,
   ALOC_ZADNI_TRETINA      // Toto musi zustat posledni, pouziva se to v testech
} TLocalizationStatus;

// Indexy teplot v polich
#define TEPLOTA_PREDNI  0
#define TEPLOTA_STREDNI 1
#define TEPLOTA_ZADNI   2





//-----------------------------------------------------------------------------
// Logger
//-----------------------------------------------------------------------------

// Perioda ukladani je v minutach
// Struktura v EEPROM viz komentare a konstanty AutoLog



//-----------------------------------------------------------------------------
// Include potrebnych modulu, povoluje se v Hardware.h
//-----------------------------------------------------------------------------

#ifdef AUTO_TEMPERATURE
#ifndef __Thermo_H__
   #include "..\inc\Thermo.h"
#endif
#endif // AUTO_TEMPERATURE
#ifdef AUTO_DIO
#ifndef __Adio_H__
   #include "Amisc\Adio.h"
#endif
#endif // AUTO_DIO
#ifdef AUTO_LOG
#ifndef __Alog_H__
   #include "Amisc\Alog.h"
#endif
#endif // AUTO_LOG
#ifdef AUTO_PRT
#ifndef __Aprt_H__
   #include "Amisc\Aprt.h"
#endif
#endif // AUTO_PRT
#ifdef AUTO_CONFIG
#ifndef __Acfg_H__
   #include "Amisc\Acfg.h"
#endif
#endif // AutoCONFIG

//-----------------------------------------------------------------------------
// Globalni komunikacni struktura
//-----------------------------------------------------------------------------


// Komunikacni struktura pro zobrazeni/logovani :
typedef struct {
   // merene teploty :
   int16  TeplotaVenkovni;
   int16  TeplotaKanal;
   int16  TeplotaPredni;
   int16  TeplotaStredni;
   int16  TeplotaZadni;
   int16  TeplotaVyfukovana;
   // Alarm (je nejaka porucha) :
   byte Alarm;                    // YES/NO
   // RTC :
   byte NastavenyHodiny;          // YES/NO zmena nastaveni hodin, nuluje se po zapisu do Logu
   // stavove informace :
   byte PoruchaNapajeni;          // YES/NO (Battery Low, jen pro EEPROM)

   byte PoruchaTeplotaPredni;     // Zda je teplota vepredu skrine mimo zadane pasmo
   byte PoruchaTeplotaStredni;    // Zda je teplota uprostred skrine mimo zadane pasmo
   byte PoruchaTeplotaZadni;      // Zda je teplota vzadu skrine mimo zadane pasmo

   byte Ticho;                    // Zda ma byt zapnuty repro nebo ne
   byte ZobrazitDiagnostiku;      // YES/NO zda zobrazovat diagnostiku
   byte CisloRidice;              // Cislo nalogovaneho ridice
#ifdef __AREMOTE__
   byte ZobrazitPakety;           // YES/NO zda prijimac bude vykreslovat pocet paketu
#endif //__AREMOTE__
 } TAutoData;

extern TAutoData __xdata__ AutoData;      // komunikacni struktura v externi RAM (pro modul Fifo)

//-----------------------------------------------------------------------------
// Struktura zaznamu Logu
//-----------------------------------------------------------------------------

// Komprimovany datum a cas :

typedef struct {
   byte Min        : 6;
   byte Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3;     // horni  3 bity
   byte Day        : 5;
   byte Month      : 4;
   byte Year       : 3;     // rok % 8
   byte RtcChanged : 1;     // priznak zmeny nastaveni RTC
} TShortDateTime;   // Velikost 3 bajty

// Ukladani roku: z roku se do struktury ukladaji pouze spodni 3 bity. Rekonstrukce pak probiha tak, ze hornich 13 bitu se vezme z aktualniho roku
// a doplni se temito 3 bity ze struktury a mam rok zrekonstruovany. Pokud je takto vznikly rok vetsi nez aktualni rok, odecte se od nej 8,
// protoze novejsi datum nez aktualni to byt nemohl. Funguje to pro az 8 let stare zaznamy.

// zapis hodin do struktury :
#define mkhour( dt, h)     dt.Hour_lo = (h) & 0x03; dt.Hour_hi = ((h) >> 2) & 0x07
// cteni hodin ze struktury :
#define gethour( dt)       ((dt.Hour_lo) | (dt.Hour_hi << 2))
// zapis roku do struktury :
#define mkyear( dt, y)     dt.Year = (y) & 0x07

// vlastni struktura logu :
typedef struct SAutoLog {
   // merene teploty :
   char  TeplotaVenkovni;
   char  TeplotaKanal;
   char  TeplotaPredni;
   char  TeplotaStredni;
   char  TeplotaZadni;
   char  TeplotaVyfukovana;
   // teploty pro regulator :
   char TeplotaCilova[3];          // pozadovana teplota jednotlivych cidel, cele stupne

   TShortDateTime DateTime;        // cas zaznamu

   byte Lozeni      : 3,           // TLocalizationStatus
        Dummy1      : 5;

   byte CisloRidice : 7,           // 0-99
        Dummy2      : 1;

} TAutoLog;  // Delka 14 bajtu
// (bylo puvodne 20 bajtu)

extern TAutoLog __xdata__ AutoLog;    // buffer logu v externi RAM

// Konstanty pro ukladani do EEPROM FIFO :

#define AUTO_FIFO_START            0  // Pocatecni adresa FIFO v EEPROM
#define AUTO_FIFO_CAPACITY       1600 // Maximalni pocet ukladanych polozek - klesla mne velikost zaznamu z 20 na 14 bajtu => mohl bych i vic, ale zatim to staci
#define AUTO_FIFO_MARKER_EMPTY  -127  // Znacka konce pri neuplnem zaplneni - teplota
#define AUTO_FIFO_MARKER_FULL   -128  // znacka konce pri prepisovani
#define AUTO_FIFO_PROTOCOL_TYPE -126  // Pokud je prvni bajt zaznamu roven tomuto, jde o zaznam s protokolem cinnosti ridice. Pokud ne, jde o normalni zaznam

// Datovy typ popisujici strukturu logu (v EEPROM) :

typedef TAutoLog TAutoLogArray[ AUTO_FIFO_CAPACITY];   // datovy logger

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

// Nechat kompatibilni s normalni jednotkou do auta
typedef struct {
   // interni data, pro ulozeni v EEPROM
   word Magic;                        // Identifikace souboru
   word Verze;                        // AUTO_VERSION
   byte IdentifikacniCislo;           // identifikace zarizeni
   byte MaxOdchylkaSkrin;             // Maximalni odchylka teploty ve skrini od cilove teploty
   char TeplotaCilova[3];             // Cilova teplota na jednotlivych cidlech, cele stupne
   byte PeriodaUkladani;              // perioda ukladani min
   byte Lozeni;                       // TLocalizationStatus: lozeni prepravek ve skrini
   byte RozdelenaSkrin;               // YES/NO, zda jsou vsechny 3 teploty v 1 skrini nebo oddelene
   byte Login;                        // YES/NO, zda se po zapnuti pouziva login ridice

   // interni data, pro ulozeni v EEPROM
   byte KontrolniSuma;                // kontrolni soucet
} TAutoConfig;

extern TAutoConfig __xdata__ AutoConfig; // buffer konfigurace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
#define ACFG_ALL                     0xFFFFFFFFL    // uloz vsechno
#define ACFG_MAGIC                   0x00000001L
#define ACFG_VERZE                   0x00000002L
#define ACFG_ODCHYLKA_SKRIN          0x00000004L
#define ACFG_TEPLOTA_CILOVA_PREDNI   0x00000080L
#define ACFG_TEPLOTA_CILOVA_STREDNI  0x00000100L
#define ACFG_TEPLOTA_CILOVA_ZADNI    0x00000200L
#define ACFG_PERIODA_UKLADANI        0x00000400L
#define ACFG_IDENTIFIKACNI_CISLO     0x00000800L
#define ACFG_LOZENI                  0x00001000L
#define ACFG_ROZDELENA_SKRIN         0x00002000L
#define ACFG_LOGIN                   0x00004000L

#define ACFG_KONTROLNI_SUMA          0x80000000L


//-----------------------------------------------------------------------------
// Protokolovani udalosti
//-----------------------------------------------------------------------------

// Typ udalosti :                       parametr udalosti

typedef enum {
   APRT_START,
   APRT_PRIHLASENI_RIDICE,              // cislo ridice
   APRT_ODHLASENI_RIDICE,               // cislo ridice
   APRT_NASTAVENI_HODIN,
   APRT_NASTAVENI_TEPLOTY,              // teplota
   APRT_NASTAVENI_TEPLOTY_PREDNI,       // teplota
   APRT_NASTAVENI_TEPLOTY_STREDNI,      // teplota
   APRT_NASTAVENI_TEPLOTY_ZADNI,        // teplota
   APRT_NASTAVENI_MEZI_SKRINE,          // teplota
   APRT_NASTAVENI_PERIODY_UKLADANI,     // perioda
   APRT_SMAZANI_LOGU,
   APRT_EXPORT_DAT,                     // YES uspesny, NO neuspesny
   APRT_TISK_ZAZNAMU,
   APRT_NASTAVENI_LOZENI,               // 15.6.2004: lozeni prepravek
   APRT_NASTAVENI_IDENTIFIKACNIHO_CISLA,
   APRT_NASTAVENI_ROZDELENA_SKRIN,
   APRT_NASTAVENI_LOGIN,
   _APRT_LAST,
   // znacky pro FIFO :
   APRT_FIFO_MARKER_FULL  = 0xFE,
   APRT_FIFO_MARKER_EMPTY = 0xFF
} TAprtTyp;

// Zaznam o udalosti :

typedef struct SAutoUdalost {
   byte           TypZaznamu;           // Odpovida 1. bajtu ve strukture AutoLog
   byte           Typ;                  // typ udalosti
   dword          Parametr;             // parametr udalosti
   byte           CisloRidice;          // Cislo nalogovaneho ridice
   TShortDateTime DateTime;             // cas vzniku udalosti
   byte           Spare[sizeof(TAutoLog) - sizeof(byte) - sizeof(byte) - sizeof(dword) - sizeof(byte) - sizeof(TShortDateTime)];  // Vyplneni, aby byla velikost stejna jako TAutoLog
} TAutoUdalost;

extern TAutoUdalost __xdata__ AutoUdalost;    // buffer udalosti v externi RAM


//-----------------------------------------------------------------------------
// Data v interni EEPROM
//-----------------------------------------------------------------------------

#define AUTO_IEP_SPARE1   (IEP_PAGE_SIZE - sizeof( TAutoConfig))       // vyhrazeni stranky na konfiguraci
#define AUTO_IEP_SPARE2   (IEP_SIZE                     \
                          - sizeof( TAutoConfig)        \
                          - AUTO_IEP_SPARE1)            \


typedef struct {
   TAutoConfig       Config;                      // konfigurace
   byte              Spare1[ AUTO_IEP_SPARE1];    // rezerva pro konfiguraci
   byte              Spare2[ AUTO_IEP_SPARE2];    // rezerva
} TAutoIep;

//-----------------------------------------------------------------------------
// Data v externim modulu
//-----------------------------------------------------------------------------

// Nekomprimovany datum a cas :

typedef struct {
   byte Min        : 6;
   byte Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3;     // horni  3 bity
   byte Day        : 5;
   byte Month      : 4;
   byte Dummy      : 4;     // vypln
   word Year;               // uplny rok
} TLongDateTime;

// makra shodna s TShortDateTime

// Razitko externiho modulu :

typedef struct {
   TLongDateTime DatumPorizeni;      // datum nahrani do modulu
   word MotoHodiny;                  // 17.10.2002: Celkovy pocet motohodin, ktery je v SW potreba pro stanoveni poctu motohodin do vymeny oleje/filtru.
                                     //             Navic bude treba do budoucna, protoze budou chtit vedet i celkovy pocet motohodin, kvuli spotrebe nafty.
                                     //             V podstate se sem zkopiruje promenna Motohodiny ze struktury TAutoData, jen pri kopirovani dat do modulu.
                                     //             Toto nemuze byt ve strukture Config, protoze to nema s nastavenim nic spolecneho a musela by se pokazde prepocitavat checksum.
   byte          KontrolniSuma;      // kontrolni suma modulu
} TAutoStamp;

extern TAutoStamp __xdata__ AutoStamp;        // buffer razitka v externi RAM

// Velikost rezervy v datovem modulu :

#define AUTO_XMEM_SPARE   XMEM_SIZE                     \
                          - sizeof( TAutoLogArray)      \
                          - sizeof( TAutoConfig)        \
                          - sizeof( TAutoStamp)         \

// Struktura datoveho externiho modulu :

typedef struct {
   TAutoLogArray Log;                            // zaznam dat
   TAutoConfig   Config;                         // aktualni konfigurace
   byte          Spare[ AUTO_XMEM_SPARE];        // rezerva
   TAutoStamp    Stamp;                          // zaznam o porizeni
} TAutoXmem;

//-----------------------------------------------------------------------------
#endif // __Auto_H__
