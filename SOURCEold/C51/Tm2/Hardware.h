//*****************************************************************************
//
//    Hardware.h  - Auto final hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE

// Typ jednotky: Pokud neni definovano nic, jde o TM2 bez vysilace (je treba rucne zakomentovat 2 moduly v Makefile).
#define __ATTY__       1        // Jde o vysilac k TM2/S - prekladat vysilac




//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define AutoAccuOk() AdioBatteryOk()

// spolecne vodice (podle schematu) :

#define AutoDATA      P0           // datova sbernice

// Teplomery na sbernici :
sbit AutoT1      = P1^0;     // teplomer T1
sbit AutoT2      = P1^1;     // teplomer T2
sbit AutoT3      = P1^2;     // teplomer T3
sbit AutoT4      = P1^3;     // teplomer T4
sbit AutoT5      = P1^4;     // teplomer T5
sbit AutoT6      = P1^5;     // teplomer T6
sbit AutoTOUT    = P2^0;     // teplomery spolecne buzeni

// Displej
sbit AutoDisplayCS  =  P2^7;         // /CS vyber cipu
sbit AutoDisplayA0  =  P2^6;         // prikaz/data nekdy oznaceny jako CD
sbit AutoDisplayRD  =  P3^7;         // cteni /RD
sbit AutoDisplayWR  =  P3^6;         // zapis /WR
sbit AutoDisplayRES =  P2^2;         // reset /RES
#define AutoDisplayData AutoDATA      // datova sbernice

// Touch Screen ADS 7846
sbit AutoTouchDCLK   =P0^4;           // hodiny DCLK
sbit AutoTouchDIN    =P0^5;           // vstup dat DIN
sbit AutoTouchDOUT   =P0^6;           // vystup dat DOUT
sbit AutoTouchPENIRQ =P3^2;           // preruseni od dotyku /PENIRQ
sbit AutoTouchCS     =P3^3;           // chipselect /CS - pokud se vyzuvia

// E2pot X9313
sbit E2PotCS    = P1^6;    // Chip select
sbit E2PotUD    = P0^0;    // Smer Up/Down
sbit E2PotINC   = P0^1;    // Provedeni 1 kroku

// I2C
sbit AutoSDA =  P2^6;
sbit AutoSCL =  P2^5;

// EEPROM
sbit AutoEepCS = P3^4;
sbit AutoEepSCK= P0^7;
sbit AutoEepSO = P0^3;
sbit AutoEepSI = P0^0;

// Pametovy modul
sbit AutoXmemCS = P3^3;
sbit AutoXmemSCK= P0^1;
sbit AutoXmemSO = P1^7;
sbit AutoXmemSI = P0^0;

// Tiskarna
sbit AutoPRINT   = P3^7;     // Odblokovani tiskarny

// Lowbat
sbit AutoLOWBAT  = P3^5;     // Hlidani napajeni

// Podsvit
sbit AutoPODSVIT = P2^3;        // Podsvit displeje

// Piskani
sbit AutoPISK =  P2^4;     // repro

// Majak
sbit AutoMAJAK =  P2^1;








// Adresy A/D prevodniku 1 :
#define AutoAV1   ADC_SINGLE_CH0   // A/D prevodnik 1/0
#define AutoAV2   ADC_SINGLE_CH1   // A/D prevodnik 1/1
#define AutoAV3   ADC_SINGLE_CH2   // A/D prevodnik 1/2
#define AutoAV4   ADC_SINGLE_CH3   // A/D prevodnik 1/3
#define AutoAV5   ADC_SINGLE_CH4   // A/D prevodnik 1/4
#define AutoAV6   ADC_SINGLE_CH5   // A/D prevodnik 1/5
#define AutoAV7   ADC_SINGLE_CH6   // A/D prevodnik 1/6
#define AutoAV8   ADC_SINGLE_CH7   // A/D prevodnik 1/7
// Adresy A/D prevodniku 2 :
//#define AutoAV9   ADC_SINGLE_CH0   // A/D prevodnik 2/0
//#define AutoAV10  ADC_SINGLE_CH1   // A/D prevodnik 2/1
#define AutoAV11  ADC_SINGLE_CH2   // A/D prevodnik 2/2
#define AutoAV12  ADC_SINGLE_CH3   // A/D prevodnik 2/3
#define AutoAV13  ADC_SINGLE_CH4   // A/D prevodnik 2/4
#define AutoAV14  ADC_SINGLE_CH5   // A/D prevodnik 2/5
#define AutoAV15  ADC_SINGLE_CH6   // A/D prevodnik 2/4
#define AutoAV16  ADC_SINGLE_CH7   // A/D prevodnik 2/5
//#define AutoB1B2  ADC_DIFF_CH3     // A/D prevodnik 2/6,7
#define AutoB1B2  ADC_DIFF_CH0     // A/D prevodnik 2/0,1
// Adresy D/A prevodniku :
#define AutoAVY1   0               // D/A prevodnik 0
#define AutoAVY2   1               // D/A prevodnik 1
#define AutoAVY3   2               // D/A prevodnik 2
#define AutoAVY4   3               // D/A prevodnik 3
#define AutoAVY5   4               // D/A prevodnik 4
#define AutoAVY6   5               // D/A prevodnik 5
// Digitalni vystupy :
#define AutoDVY1   AutoVYST1       // nazev za oddelovacem
#define AutoDVY2   AutoVYST2       // nazev za oddelovacem
#define AutoDVY3   AutoVYST3       // nazev za oddelovacem




//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz

#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
// Pozor, pokud se pouziva dynamicke rizeni CPU_X2, vsechny ostatni zarizeni musi byt v X1 modu (T0_X2, T1_X2 atd. musi byt nastaveny na 1). Pokud
// jsou ostatni zarizeni nastavene v X2 modu, po prepnuti procesoru do X1 modu se prepnou soucasne i zarizeni. Napr. SPI je stale v X2 a prepina se do X1.
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2 (zatim neni implementovano, bezi stale v X2 na max. rychlost)

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :

#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost (1=ne, 2=ano)

// Casovac 0 :

#define TIMER0_PERIOD 20        // perioda casovace 0 v ms
#define TIMER1_PERIOD 1        // perioda casovace 1 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
#include "Tm2HW.h"             // projektove nezavisle definice
//-----------------------------------------------------------------------------


// Casovani

void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) Delay( ms)             // zpozdeni 1ms z modulu Main

// Zpozdeni <us> * 1us :

#define uDelay( us)  \
                 {byte __count; __count = ((us) * 3) / 2; while( --__count);}

//-----------------------------------------------------------------------------

#endif // __Hardware_H__
