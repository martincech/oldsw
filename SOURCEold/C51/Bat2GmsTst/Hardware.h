//*****************************************************************************
//
//    Hardware.h  - BAT2 hardware descriptions
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof


//-----------------------------------------------------------------------------
// Lowbat
//-----------------------------------------------------------------------------

// Deska ver 1
//#define BatAccuOk()  BatLOWBAT
//#define PowerFail()  !BatLOWBAT
#define BatAccuOk()  1
#define PowerFail()  0

//-----------------------------------------------------------------------------
// Verze desky - je treba zahrnout/zakomentovat moduly v makefile
//-----------------------------------------------------------------------------

//#define BOARD_VERSION_1     1       // Deska verze 1
#define BOARD_VERSION_2     1       // Deska verze 2

// Od verze 1.11.0 se nepouziva kvuli nedostatku pameti (MODBUS se orezal, zustaly jen registry)
//#define VERSION_STRING_MODBUS   "V1.10.5"   // Verze produktu pro MODBUS, melo by odpovidat cislu verze v Bat2.h

//-----------------------------------------------------------------------------
// Hardwarove verze - je treba zahrnout/zakomentovat moduly v makefile
//-----------------------------------------------------------------------------

#define VERSION_GSM         1       // Je osazen GSM modul
//#define VERSION_DACS_LW1    1       // Emulace vahy Dacs LW1 pripojene k ACSLink
//#define VERSION_MODBUS      1       // MODBUS protokol pres RS-485

//-----------------------------------------------------------------------------
// Specialni algoritmy, je automaticky v makefile
//-----------------------------------------------------------------------------

//#define VERSION_BIGDUTCHMAN 1       // Sliding average pro Big Dutchman

// Verze pro Big Dutchman nemuze byt zaroven s MODBUSem
#if (defined(VERSION_BIGDUTCHMAN) && defined (VERSION_MODBUS))
  #error "VERSION_BIGDUTCHMAN nemuze byt soucasne s VERSION_MODBUS kvuli nedostatku XDATA"
#endif

//-----------------------------------------------------------------------------
// Prirazeni portu
//-----------------------------------------------------------------------------

#ifdef BOARD_VERSION_1
// Deska verze 1 --------------

// datova sbernice :
#define BatDATA P0
sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :
sbit BatCSRTC    = P1^0;
sbit BatRDRTC    = P1^1;
sbit BatWRRTC    = P1^2;
sbit BatALERTC   = P1^3;
sbit BatCSEE     = P1^4;               // CS flash
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;

// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// Port P2 :
sbit BatLOWBAT   = P2^0;
sbit BatZAPGSM   = P2^1;
sbit BatPISK     = P2^2;
sbit BatK0       = P2^3;
sbit BatK1       = P2^4;
sbit BatRES      = P2^5;               // RES display
sbit BatCS2      = P2^6;               // CS1 display
sbit BatCS1      = P2^7;               // CS2 display

// Port P3 :
sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatCSMOD    = P3^2;               // CS flash modulu
sbit BatDRAD     = P3^3;               // DRDY AD1241
sbit BatCLKAD    = P3^4;               // CLK  AD1241
sbit BatK2       = P3^5;
sbit BatSVIT     = P3^6;               // podsvit
sbit BatDATAAD   = P3^7;               // DOUT AD1241

// Kanal vahy :
#define WEIGHT_CHANNEL   0             // cislo kanalu A/D pro vahu

#elif BOARD_VERSION_2
// Deska verze 2 --------------

// datova sbernice :
#define BatDATA P0
sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :
sbit BatCLKAD    = P1^0;               // CLK  ADS1230
sbit BatPDWNAD   = P1^1;               // PDWN ADS1230
sbit BatLOWBAT   = P1^2;
sbit BatCSMOD    = P1^3;               // CS flash modulu
sbit BatCSEE     = P1^4;               // CS flash
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;

// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// Port P2 :
sbit BatK2       = P2^0;
sbit BatRES      = P2^1;               // RES display
sbit BatCS2      = P2^2;               // CS1 display
sbit BatCS1      = P2^3;               // CS2 display
sbit BatRDRTC    = P2^4;
sbit BatWRRTC    = P2^5;
sbit BatCSRTC    = P2^6;
sbit BatALERTC   = P2^7;

// Port P3 :
sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatDATAAD   = P3^2;               // DRDY/DOUT ADS1230
sbit BatSVIT     = P3^3;               // podsvit
sbit BatIGTGSM   = P3^4;
sbit BatZAPGSM   = P3^5;
sbit BatK0       = P3^6;
sbit BatK1       = P3^7;

// Kanal vahy :
#define WEIGHT_CHANNEL                // Nepouziva se

#endif // BOARD_VERSION_2


typedef long TRawValue;                // surova hodnota vazeni

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
// Pozor, pokud se pouziva dynamicke rizeni CPU_X2, vsechny ostatni zarizeni musi byt v X1 modu (T0_X2, T1_X2 atd. musi byt nastaveny na 1). Pokud
// jsou ostatni zarizeni nastavene v X2 modu, po prepnuti procesoru do X1 modu se prepnou soucasne i zarizeni. Napr. SPI je stale v X2 a prepina se do X1.
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2 (zatim neni implementovano, bezi stale v X2 na max. rychlost)

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE_T1  1          // zdvojena baudova rychlost pro timer 1

// Casovac 0 :
#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

// Casovac 1 :
#define TIMER1_PERIOD 1                // perioda casovace 1 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  BatAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

//-----------------------------------------------------------------------------
// A/D prevodnik ADS1241
//-----------------------------------------------------------------------------

#ifdef BOARD_VERSION_1                  // ADS1241 je jen na desce verze 1

#define AdcDIN         BatDATAAD       // vstup
#define AdcDOUT        BatDATAAD       // vystup
#define AdcSCLK        BatCLKAD        // hodiny
#define AdcDRDY        BatDRAD         // -ready

#define AdcClrCS()                     // chipselect vyber
#define AdcSetCS()                     // deselect

#define ADC_GAIN       ADC_GAIN_2      // zakladni zesileni
#define ADC_RATE       ADC_7_5HZ       // rychlost vzorkovani
#define ADC_POLARITY   ADC_BIPOLAR     // rezim polarity
#define ADC_RANGE      ADC_HALF_RANGE  // plny/polovicni rozsah

#define ADC_INTERRUPT  INT_EXTERNAL1   // rezim na pozadi, vstup preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

#endif // BOARD_VERSION_1

//-----------------------------------------------------------------------------
// Prevodnik ADS123x
//-----------------------------------------------------------------------------

#ifdef BOARD_VERSION_2                  // ADS1230 je jen na desce verze 2

//#define __ADS1232__                    // typ prevodniku ADS1232
#define __ADS1230__                    // typ prevodniku ADS1230

// porty pripojeni signalu :
#define AdcSCLK        BatCLKAD       // hodiny
#define AdcDRDY        BatDATAAD       // -ready/data out
#define AdcPDWN        BatPDWNAD       // power down

// provoz v rezimu preruseni :
#define ADC_INTERRUPT  INT_EXTERNAL0   // rezim na pozadi, vstup preruseni
                                       // neni-li konstanta uvedena, nepreklada se rezim preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

// prumerovani vzorku v preruseni :
//#define ADC_AVERAGE_SHIFT  3           // pocet prumerovanych vzorku 2^SHIFT
                                       // neni-li konstanta uvedena, neprovadi se prumerovani
#endif // BOARD_VERSION_2

//-----------------------------------------------------------------------------
// Pripojeni RTC DS17287
//-----------------------------------------------------------------------------

// porty :

#define RtcALE         BatALERTC       // ALE address latch enable
#define RtcWR          BatWRRTC        // WR\ zapis
#define RtcRD          BatRDRTC        // RD\ cteni
#define RtcAD          BatDATA         // datova/adresni sbernice
#define RtcCS          BatCSRTC        // chipselect

// volne adresy v RAM :

#define RTC_RAM_FIRST  0x0E            // od teto adresy
#define RTC_RAM_LAST   0x3F            // do teto adresy je RAM k dispozici

// kapacita NVRAM :

#define RTC_NVRAM_SIZE 2048

// podmineny preklad :

#define RTC_USE_DATE    1              // cti/nastavuj datum
//#define RTC_USE_WDAY    1              // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1              // hlidej zadavane hodnoty na rozsah
//#define RTC_USE_ALARM   1              // pouzivej alarm/budik
#define RTC_USE_NVRAM   1              // pouzita NVRAM

//-----------------------------------------------------------------------------
// ED2 interni SPI
//-----------------------------------------------------------------------------

#ifndef __SpiHw_H__
   #include "..\inc\SpiHw.h"
#endif

// SPI mode 3
#define SPI_BAUD   SPI_CLOCK_2         // rychlost
#define SPI_LEVEL  SPI_CLOCK_H         // klidova hladina SCK
#define SPI_EDGE   SPI_EDGE_RETURN     // strobovaci hrana SCK

//-----------------------------------------------------------------------------
// Flash Atmel AT45DB161
//-----------------------------------------------------------------------------

#define __AT45DB161__      1           // typ pameti
#define FLASH_TIMEOUT   5000           // pocet cyklu cekani na ready

#define __flash_data__  xdata          // kontextove promenne

// Podmineny preklad :
#define FLASH_READ_BYTE    1           // cteni bytu
#define FLASH_WRITE_BYTE   1           // zapis+flush bytu


// NVRAM buffer :
//#define FLASH_LOAD_BUFFER  1           // cteni pameti do bufferu
//#define FLASH_READ_BUFFER  1           // cteni bufferu
// Pro nonvolatile buffer :
#define FLASH_NVRAM        1           // pouzit modul NvFlash.c51
#define FLASH_VERIFY       1           // verifikace po zapisu stranky


/*
// FLASH buffer :
#define FLASH_LOAD_BUFFER  1           // cteni pameti do bufferu
#define FLASH_READ_BUFFER  1           // cteni bufferu
// Pro nonvolatile buffer :
//#define FLASH_NVRAM        1           // pouzit modul NvFlash.c51
//#define FLASH_VERIFY       1           // verifikace po zapisu stranky
*/

//-----------------------------------------------------------------------------
// Radic displeje Samsung Ks0108b
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       BatDATA
#define DisplayD0         BatD0
#define DisplayD1         BatD1
#define DisplayD2         BatD2
#define DisplayD3         BatD3
#define DisplayD4         BatD4
#define DisplayD5         BatD5
#define DisplayD6         BatD6
#define DisplayD7         BatD7
#define DisplayRW         BatWRRTC
#define DisplayDI         BatALERTC
#define DisplayE          BatRDRTC
#define DisplayCS1        BatCS1
#define DisplayCS2        BatCS2
#define DisplayRESET      BatRES

// Podminena kompilace

// Datova sbernice DisplayDATA 1:1
#define KS_BUS     1
// Obecne funkce
#define KS_ON      1

//-----------------------------------------------------------------------------
// Graficky displej Powertip EL12864
//-----------------------------------------------------------------------------

// Rozmery displeje:
#define DISPLAY_WIDTH           128     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

// Podminena kompilace:
//#define DISPLAY_SET_AREA_FAST   1
#define DISPLAY_SET_AREA        1
//#define DISPLAY_HOR_LINE        1
//#define DISPLAY_FAST_VERT_LINE  1
//#define DISPLAY_VERT_LINE       1
//#define DISPLAY_PIXEL           1
#define DISPLAY_CHAR            1
//#define DISPLAY_SYMBOL          1

//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Podminena kompilace:

//#define DISPLAY_LINE                    1

// Vety
#define DISPLAY_STRING                  1
#define DISPLAY_STRING_RIGHT            1
#define DISPLAY_STRING_CENTER           1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER           1
#define DISPLAY_NUMBER                  1
//#define DISPLAY_CHAR_FORMATED           1

// Editace
#define DISPLAY_EDIT                    1
#define DISPLAY_EDIT_CHAR_WIDTH         5       // Sirka 1 cislice (podle pouziteho fontu)
#define DISPLAY_EDIT_DOT_WIDTH          1       // Sirka tecky a dvojtecky pri editaci cisla, data a casu (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_BEFORE_DOT      2       // Sirka mezery pred desetinnou carkou (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_AFTER_DOT       2       // Sirka mezery za desetinnou carkou (podle pouziteho fontu)
#define DISPLAY_EDIT_TEXT               1

#define DISPLAY_BARGRAF                 1

// Dialog
#define DISPLAY_DIALOG                  1
#define DIALOG_FLIP_KEYS                1        // Zda se ma otocit zobrazeni tlacitek Enter a Esc
#define DIALOG_BUTTON_Y                 (DISPLAY_HEIGHT - DIALOG_BUTTON_HEIGHT) // Y-ova souradnice tlacitek

// Menu
#define DISPLAY_MENU                    1
#define MENU_MAX_ITEMS                  9       // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
#define DISPLAY_MENU_LINE_HEIGHT        16      // Vyska radku v pixelech
#define DISPLAY_MENU_Y                  0       // Y-ova souradnice pocatku menu v pixelech
#define DISPLAY_MENU_LINES_COUNT        4       // Pocet radku v menu
#define DISPLAY_MENU_ARROW_DY           2       // Vertikalni posun sipky (podle pouziteho fontu)
#define DISPLAY_CHOOSE_ITEM             1

// Jazyky
#define DISPLAY_LANGUAGES               1       // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
#define DisplayEditExecute(Znaky)       KbdBeepKey()

//-----------------------------------------------------------------------------
// Fonty
//-----------------------------------------------------------------------------

// Podminena kompilace:
//#define DISPLAY_TAHOMA10          1
//#define DISPLAY_TAHOMA10_BOLD     1
//#define DISPLAY_TAHOMA8           1
//#define DISPLAY_TAHOMA8_GR        1
#define DISPLAY_LUCIDA6           1
//#define DISPLAY_LUCIDA6_GR        1
//#define DISPLAY_MYRIAD32          1
//#define DISPLAY_MYRIAD40          1
//#define DISPLAY_CHAR_WIDTH        1
//#define DISPLAY_TEXT_WIDTH        1

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define BacklightOn()   BatSVIT = 1
#define BacklightOff()  BatSVIT = 0

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define K0         BatK0
#define K1         BatK1
#define K2         BatK2

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
//  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_LEFT,                              // sipka doleva
  K_RIGHT,                             // sipka doprava
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

#define MB_DEBUG
#define __SIMPLE_DISPLAY__    1        // jednoduche zobrazeni
//#define __DISPLAY_CURSOR__    1        // nastavovani kurzoru
#define __DISPLAY_CLEAR__     1        // mazani
#define __DISPLAY_CLR_EOL__   1        // mazani do konce radku

//-----------------------------------------------------------------------------
// Statistika
//-----------------------------------------------------------------------------

typedef float TNumber;                 // zakladni datovy typ

#define STAT_UNIFORMITY      1         // povoleni vypoctu uniformity
#define STAT_REAL_UNIFORMITY 1         // povoleni presneho vypoctu uniformity

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

typedef word      THistogramCount;     // pocet vzorku
typedef TRawValue THistogramValue;     // hodnota vzorku, musi byt signed
#define HISTOGRAM_MIN_STEP    1        // Minimalni hodnota kroku v THistogramValue

#define HISTOGRAM_SLOTS       39       // pocet sloupcu - sude i liche cislo (max.254)

#define HISTOGRAM_GETDENOMINATOR 1
#define HISTOGRAM_SET_STEP       1
#define HISTOGRAM_EMPTY          1
#define HISTOGRAM_GET_VALUE      1

//-----------------------------------------------------------------------------
// Klouzavy prumer
//-----------------------------------------------------------------------------

typedef byte   TAverageCount;     // Pocet vzorku
typedef long   TAverageValue;     // Hodnota vzorku

#define AVERAGE_CAPACITY 9        // Kapacita (maximalni pocet prvku) klouzaveho prumeru

// Podmineny preklad
#define AVERAGE_FULL    1         // Test zaplneni
#define AVERAGE_STEADY  1         // Test zaplneni

//-----------------------------------------------------------------------------
// Flash Fifo
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Komunikace RS232
//-----------------------------------------------------------------------------

#ifdef VERSION_DACS_LW1

// Seriovou linku obsluhuji zvlast, zde pouze nahozeni/zakaz vysilace RS485

#ifdef BOARD_VERSION_1
  // U desky verze 1 musim rucne otocit (starych desek s RS-485 se delalo malo, jde jen o zpetnou kompatibilitu)
  #define ComTxEnable()       BatZAPGSM = 1   // RS485 povoleni vysilace
  #define ComTxDisable()      BatZAPGSM = 0   // RS485 zakaz vysilace
#elif defined BOARD_VERSION_2
  // Nova deska
  #define ComTxEnable()       BatZAPGSM = 0   // RS485 povoleni vysilace
  #define ComTxDisable()      BatZAPGSM = 1   // RS485 zakaz vysilace
#endif  // BOARD_VERSION_2

#elif defined VERSION_GSM
// VERSION_GSM

#define COM_BAUD        9600          // Rychlost linky v baudech

//#define COM_PARITY_EVEN  1          // Suda parita
//#define COM_PARITY_ODD   1          // Licha parita
//#define COM_PARITY_MARK  1          // 2 stop bity (forced mark parity)

//#define COM_TIMER_1      1          // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2        1          // K casovani linky se vyuzije casovac 2

#define COM_RX_TIMEOUT     10000      // Meziznakovy timeout [us]

// Podmineny preklad :
#define COM_TX_CHAR        1           // Preklada se ComTxChar
#define COM_RX_CHAR        1           // Preklada se ComRxChar
#define COM_RX_WAIT        1           // Preklada se ComRxWait
#define COM_FLUSH_CHARS    1           // Preklada se ComFlushChars

#endif // VERSION_GSM

//-----------------------------------------------------------------------------
// Modbus RS232
//-----------------------------------------------------------------------------

#ifdef VERSION_MODBUS

#ifdef BOARD_VERSION_1
  // U desky verze 1 musim rucne otocit (starych desek s RS-485 se delalo malo, jde jen o zpetnou kompatibilitu)
  #define ComTxEnable()       BatZAPGSM = 1   // RS485 povoleni vysilace
  #define ComTxDisable()      BatZAPGSM = 0   // RS485 zakaz vysilace
#elif defined BOARD_VERSION_2
  // Nova deska
  #define ComTxEnable()       BatZAPGSM = 0   // RS485 povoleni vysilace
  #define ComTxDisable()      BatZAPGSM = 1   // RS485 zakaz vysilace
#endif  // BOARD_VERSION_2

//#define COM_TIMER_1      1          // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2        1          // K casovani linky se vyuzije casovac 2

#define MB_TIMEOUT_TIMER_PERIOD  TIMER1_PERIOD  // Perioda casovace pouzita pro pocitani timeoutu

//#define COM_DATA_SIZE        520      // velikost bufferu pro ASCII
#define COM_DATA_SIZE        256      // velikost bufferu pro RTU
typedef word com_pointer_t;           // datovy typ ukazatele do bufferu

#define COM_LEADER_CHAR    ':'
#define COM_TRAILER_CHAR   '\n'

#define COM_TX_SPACE       1          // podminena kompilace - prodleva Rx/Tx

#define __COM_DIAG__       1          // print diagnostics

#endif // VERSION_MODBUS

//-----------------------------------------------------------------------------
// Modbus
//-----------------------------------------------------------------------------

#ifdef VERSION_MODBUS

// identifikacni retezce, pozor neprehnat delku
#define MB_VENDOR_NAME             "Veit"                  // vyrobce
#define MB_PRODUCT_CODE            "Bat2"                  // kod produktu
#define MB_REVISION_STRING         VERSION_STRING_MODBUS   // verze produktu

// podminena kompilace :
//#define MB_FIXED_MODE              1   // mod komunikace volen komplilaci
//#define MB_ENABLE_DISCRETE         1   // povoleni operaci se skupinou DISCRETE
//#define MB_ENABLE_COILS            1   // povoleni operaci se skupinou COILS
//#define MB_ENABLE_INPUT_REGISTERS  1   // povoleni operaci se skupinou Holding REGISTERS
#define MB_ENABLE_REGISTERS        1   // povoleni operaci se skupinou Holding REGISTERS
//#define MB_ENABLE_FIFO             1   // povoleni operaci s FIFO registry
//#define MB_ENABLE_EXCEPTION_STATUS 1   // povoleni funkce exception status
//#define MB_ENABLE_DIAGNOSTIC       1   // povoleni diagnostickych operaci
//#define MB_ENABLE_COM_EVENT        1   // povoleni komunikacnich udalosti
//#define MB_ENABLE_SLAVE_ID         1   // povoleni identifikace
//#define MB_ENABLE_IDENTIFICATION   1   // povoleni textove identifikace

#endif // VERSION_MODBUS

//-----------------------------------------------------------------------------
// Modul SMS
//-----------------------------------------------------------------------------

#ifdef VERSION_GSM

#define SMS_MAX_LENGTH      161        // max. delka prijate zpravy, vc. koncove nuly
#define GSM_MAX_NUMBER      16         // max. delka telefonniho cisla

// Podminena kompilace :

//#define GSM_COM2            1          // komunikace pres COM2
//#define COM_RX_STRING       1          // funkce pro GSM_OPERATOR
#define GSM_OPERATOR        1          // nazev operatora
#define GSM_SIGNAL_STRENGTH 1          // sila signalu
//#define GSM_DATA            1          // datova komunikace
#define GSM_SMS               1          // vysilani, prijem SMS
//#define GSM_SMS_USE_GSMCTL  1          // pri obsluze SMS se bude vyuzivat modul GsmCtl
//#define GSM_SMS_MEMORY      1          // prekladat funkci SmsMemory
//#define GSM_SMS_PARAMETERS  1          // prekladat funkci SmsSendParameters
#define GSM_SMS_DEBUG       1          // Pouzije se debug mod
#define GSM_PIN             1          // Prace s PIN

// Expirace SMS zpravy :

#define SmsExpirationMins( m)   ((m) / 5)               // min 0  max 720 (0..143)
#define SmsExpirationHours( h)  (((h) - 12) * 2 + 143)  // min 13 max 24  (144..167)
#define SmsExpirationDays( d)   ((d) + 166)             // min 2  max 30  (168..196)
#define SmsExpirationWeeks( w)  ((w) + 192)             // min 5  max 58  (197..255)
#define SmsExpirationMax()      255                     // maximalni expirace

#define SMS_EXPIRATION    SmsExpirationDays(4)          // pozadovana expirace

// Timeouty radice GSM :

#define GSM_ERROR_MODE_TIMEOUT 10      // Prodleva po nastaveni modu chyb [ms] (min. 15ms vcetne COM_RX_TIMEOUT). Hodnota 5 jeste funguje, 10 je tutovka.
#define GSM_SMS_SEND_TIMEOUT  15       // SMS center reply timeout [s]
#define GSM_CONNECT_TIMEOUT   30       // od zvednuti do navazani spojeni  [s]
#define GSM_CHECK_PERIOD      30       // cas mezi dvema kontrolami registrace [s]
#define GSM_POWERED_PERIOD    20       // cas na nabeh napajeni modemu [s]
#define GSM_HANGUP_TIMEOUT    30       // zaveseni pri neaktivnim spojeni [s]

#endif // VERSION_GSM

//-----------------------------------------------------------------------------
// COM_util
//-----------------------------------------------------------------------------

#ifdef VERSION_GSM

// Podminena kompilace :

#define COM_TX_DIGIT        1
//#define COM_TX_HEX          1
#define COM_TX_DEC          1
//#define COM_RX_HEX          1
//#define COM_SKIP_CHARS      1
#define COM_RX_STRING         1
#define COM_TX_X_STRING       1
#define COM_RX_DELIMITER      1
//#define COM_TX_WORD           1

#endif // VERSION_GSM

//-----------------------------------------------------------------------------

#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------

#endif
