//*****************************************************************************
//
//    Hermes1.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hermes1_H__
   #define __Hermes1_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#include "Hardware.h"       // pro podminenou kompilaci

#ifdef __C51__
// jen pro Keil
//   typedef int  int16;
//   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERSION                     0x0202  // verze FW

typedef enum {
  BACKLIGHT_OFF,
  BACKLIGHT_ON,
  BACKLIGHT_AUTO,
  _BACKLIGHT_COUNT
} TBacklight;

typedef struct {
  byte  KeyTone;        // Vyska tonu odezvy klavesnice
  byte  KeyVolume;      // Hlasitost odezvy klavesnice
  byte  AlarmTone;      // Vyska tonu alarmu
  byte  AlarmVolume;    // Hlasitost alarmu
  byte  SignalTone;     // Vyska tonu alarmu pri vypadku signalu
  byte  SignalVolume;   // Hlasitost alarmu pri vypadku signalu
} TSound;

// Maxima a minima kvuli zobrazeni
#define TEMP_MIN_01C        -999            // Maximum teploty
#define TEMP_MAX_01C        999             // Minimum teploty
#define TEMP_DECIMALS       1               // Pocet desetinnych mist pro editaci a zobrazeni
#define TEMP_EDIT_LENGTH    3               // Delka teploty pro editaci

// Tony
#define SOUND_TONE_COUNT    10              // Pocet pouzitelnych tonu
extern word code SOUND_TONE[SOUND_TONE_COUNT];

// Hlasitost
#define SOUND_VOLUME_COUNT  10              // Pocet pouzitelnych hlasitosti
extern word code SOUND_VOLUME[SOUND_VOLUME_COUNT];

// Perioda vysilani
#define TRANSMIT_BASE_PERIOD    10              // Zakladni perioda vysilani v sekundach
#define GetTransmitPeriod()     (TRANSMIT_BASE_PERIOD + ((Config.IdentificationNumber - 1) % 10))  // Perioda vysilani podle Id. cisla v sekundach

// ---------------------------------------------------------------------------------------------
// Default hodnoty nastaveni (cte i PC)
// ---------------------------------------------------------------------------------------------

#define DEFAULT_IDENTIFICATION_NUMBER   1

#define DEFAULT_BACKLIGHT               BACKLIGHT_AUTO

#define DEFAULT_MAX_LIMIT               TEMP_MAX_01C
#define DEFAULT_MIN_LIMIT               TEMP_MIN_01C

#define DEFAULT_PACKET_COUNT            1

#define DEFAULT_SOUND_KEY_TONE          (SOUND_TONE_COUNT - 1)
#define DEFAULT_SOUND_KEY_VOLUME_TX     (SOUND_VOLUME_COUNT - 1)          // Rozdilne u vysilace a prijimace (prijimac vic rve)
#define DEFAULT_SOUND_KEY_VOLUME_RX     1
#define DEFAULT_SOUND_ALARM_TONE        6
#define DEFAULT_SOUND_ALARM_VOLUME      (SOUND_VOLUME_COUNT - 1)
#define DEFAULT_SOUND_SIGNAL_TONE       6
#define DEFAULT_SOUND_SIGNAL_VOLUME     3

// ---------------------------------------------------------------------------------------------
// Meze hodnot v nastaveni
// ---------------------------------------------------------------------------------------------

#define MIN_IDENTIFICATION_NUMBER   1       // Minimalni hodnota identifikacniho cisla
#define MAX_IDENTIFICATION_NUMBER   99      // Maximalni hodnota identifikacniho cisla
#define MIN_PACKET_COUNT            1       // Minimalni hodnota poctu paketu
#define MAX_PACKET_COUNT            20      // Maximalni hodnota poctu paketu

// ---------------------------------------------------------------------------------------------
// Zobrazeni
// ---------------------------------------------------------------------------------------------

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define DISPLAY_1SEC             0x01           // Zobrazi se periodicke veci 1sec (napr. cas)
#define DISPLAY_FRAME            0x02           // Zobrazi se mrizka (kostra)
#define DISPLAY_BLINK            0x04           // Zobrazi se blikani
#define DISPLAY_ALL              0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

extern byte __xdata__ Display;     // Co mam v nasledujicim kroku prekreslit

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

typedef struct {
   // interni data, pro ulozeni v EEPROM
   word         Version;                // AUTO_VERSION
   byte         IdentificationNumber;   // identifikace zarizeni
   byte         Language;               // Jazyk
   TBacklight   Backlight;              // Podsvit
   int          MaxLimit;               // Horni limit teploty
   int          MinLimit;               // Dolni limit teploty
   byte         PacketCount;            // Pocet paketu, ktere se vyslou hned po sobe
   TSound       Sound;                  // Nastaveni zvuku
   // Interni data, pro ulozeni v EEPROM
   byte  CheckSum;                    // kontrolni soucet
} TConfig;
extern TConfig __xdata__ Config;      // buffer konfigurace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
#define CFG_ALL                     0xFFFFFFFFL    // uloz vsechno
#define CFG_VERSION                 0x00000002L
#define CFG_IDENTIFICATION_NUMBER   0x00000004L
#define CFG_LANGUAGE                0x00000008L
#define CFG_BACKLIGHT               0x00000010L
#define CFG_MAX_LIMIT               0x00000020L
#define CFG_MIN_LIMIT               0x00000040L
#define CFG_PACKET_COUNT            0x00000080L
#define CFG_SOUND                   0x00000100L

#define CFG_CHECK_SUM               0x80000000L

//-----------------------------------------------------------------------------
// Kalibrace akumulatoru prijimace
//-----------------------------------------------------------------------------

typedef struct {
  byte  AccuMin;        // Prevod pri minimalnim napeti
  byte  AccuMax;        // Prevod pri minimalnim napeti
  byte  SignalMin;      // Prevod pri minimalnim signalu
  byte  SignalMax;      // Prevod pri maximalnim signalu
} TCalibration;
extern TCalibration __xdata__ Calibration;      // buffer kalibrace v externi RAM

//-----------------------------------------------------------------------------
// Struktura interni EEPROM
//-----------------------------------------------------------------------------

#define IEP_SPARE (IEP_SIZE - sizeof(TCalibration) - sizeof(TConfig))

typedef struct {
  TCalibration  Calibration;
  TConfig       Config;
  byte          Empty[IEP_SPARE];
} TIep;



//-----------------------------------------------------------------------------
#endif
