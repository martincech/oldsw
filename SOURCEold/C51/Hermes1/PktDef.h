//*****************************************************************************
//
//    PktDef.h  - Packet data definitions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __PktDef_H__
   #define __PktDef_H__

#define DATA_SIZE 2          // pocet datovych bytu paketu
#define STX_CHAR  0xFE       // naraznikovy znak
#define NULL_CHAR 0xAA       // vyplnovy znak

#define PACKET_ADDRESS      0xAA        // Standardni adresa paketu (vysilam jen 1 paket)

//-----------------------------------------------------------------------------
// Data paketu
//-----------------------------------------------------------------------------

// Struktura pro periodicka data
typedef struct {
   word Temperature             : 11,
        Dummy1                  : 5;
} TPeriodicData;   // 2 bajty - upravit vzdy i DATA_SIZE

#define PACKET_TEMP_NEGATIVE_MASK  0x0400  // Tento bit se nastavi, pokud je teplota zaporna

// Souhrnny union :

typedef union {
   byte             Byte[DATA_SIZE];    // data jako byty
   TPeriodicData    PeriodicData;
} TPacketData;

//-----------------------------------------------------------------------------
// Synchronizacni hlavicka
//-----------------------------------------------------------------------------

extern byte code PacketSync[3];

//-----------------------------------------------------------------------------
// Definice paketu
//-----------------------------------------------------------------------------

typedef struct {
   byte        Stx;                 // naraznik - start
   byte        Id;                  // Identifikacni cislo pristroje (POZOR, nesmi byt vetsi nez STX_CHAR)
   byte        Address;             // cislo paketu (POZOR, nesmi byt STX_CHAR)
   TPacketData Data;                // data
   byte        Crc;                 // zabezpeceni
} TPacket;   // 6 bajtu

#endif

