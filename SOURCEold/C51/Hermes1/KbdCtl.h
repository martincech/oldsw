//*****************************************************************************
//
//    KbdCtl.h - Obsluha klavesnice
//    Version 1.0
//
//*****************************************************************************

#ifndef __KbdCtl_H__
   #define __KbdCtl_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void ZerKlavesu();
// Spotrebovani klavesy, volat pro vystupu z menu kvuli spotrebovani timeoutu

byte KbdWaitForKey();
// Ceka na stisk klavesy

byte KbdWaitForEnterEsc();
// Ceka na stisk tlacitek Enter nebo Esc

void KbdWaitForEnter();
// Ceka na stisk tlacitka Enter

void KbdBeepKey(void);
// Piskne po stisku klavesy (touch panelu)

#endif
