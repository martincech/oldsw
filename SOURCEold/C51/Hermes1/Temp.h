//*****************************************************************************
//
//    Temp.h - Mereni teploty
//    Version 1.0
//
//*****************************************************************************

#ifndef __Temp_H__
   #define __Temp_H__

#include "Hardware.h"       // zakladni datove typy

typedef struct {
  int   Value;              // Teplota na desetiny stupne vcetne znamenka
  byte  MinAlarm;           // YES/NO prekrocena minimalni teplota
  byte  MaxAlarm;           // YES/NO prekrocena maximalni teplota
  word  OkCount;            // Pocet spravne nactenych teplot
  word  ErrorCount;         // Pocet chybne nactenych teplot
} TTemp;
extern TTemp __xdata__ Temp;

#define TEMP_ERROR_01C   -1000               // Chyba mereni -100C misto merene hodnoty

void TemperatureInit();
// Inicializace

void TemperatureRead();
// Cteni teploty

#endif
