//*****************************************************************************
//
//    Lang.h      - Definice jazykove verze
//    Version 1.0
//
//*****************************************************************************

#ifndef __Lang_H__
   #define __Lang_H__

#include "Hardware.h"     // zakladni datove typy

// Jazykova verze - definuje, o jakou jazykovou verzi vah se jedna
#define LANG_VERSION_FR     1       // En + Fr

// Masky na dekodovani jazykove verze a jazyka
#define LANG_VERSION_MASK   0xE0        // Jazykova verze v hornich 3 bitech
#define LANG_MASK           0x1F        // Jazyk ve spodnich 5 bitech

// Nastaveni podle zvolene jazykove verze
#ifdef LANG_VERSION_FR
  #define LANG_VERSION    LANG_VERSION_FR       // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_FRENCH,
    LANG_GERMAN,
    _LANG_COUNT
  } TJazyk;
  #define LANG_COUNT        3                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

// ---------------------------------------------------------------------------------------------
// Definice jazyka
// ---------------------------------------------------------------------------------------------

extern byte code STR_JAZYK[];
extern byte code STR_MENU_JAZYK[];

// ---------------------------------------------------------------------------------------------
// Menu
// ---------------------------------------------------------------------------------------------

extern byte code STR_DEN[];
extern byte code STR_HMOTNOST[];
extern byte code STR_START[];
extern byte code STR_HEJNO[];
extern byte code STR_CILOVAHMOTNOST[];
extern byte code STR_POSLEDNIULOZENAHMOTNOST[];
extern byte code STR_POCET[];
extern byte code STR_OK[];
extern byte code STR_CANCEL[];
extern byte code STR_RETRY[];
extern byte code STR_YES[];
extern byte code STR_NO[];
extern byte code STR_ERROR[];
extern byte code STR_WAIT[];
extern byte code STR_KS[];
//extern byte code STR_VERZE[];

extern byte code STR_INVALID_VALUE[];

// Editace identifikacniho cisla
extern byte code STR_IDENTIFICATION_NUMBER[];

// Podsvit
extern byte code STR_BACKLIGHT[];

extern byte code STR_ON_OFF_AUTO[];

// Heslo
extern byte code STR_PASSWORD[];

// Zvuky
extern byte code STR_SOUND_TONE[];
extern byte code STR_SOUND_VOLUME[];

// Meze
extern byte code STR_ENTER_MAX_LIMIT[];
extern byte code STR_ENTER_MIN_LIMIT[];

// Kvalita spojeni
extern byte code STR_CONNECTION_QUALITY[];

// Vypadek spojeni
extern byte code STR_CONNECTION_ERROR1[];
extern byte code STR_CONNECTION_ERROR2[];

// Kalibrace akumulatoru
extern byte code STR_CALIBRATE_ACCU[];
extern byte code STR_CALIBRATE_RANGE[];

// Kalibrace sily signalu
extern byte code STR_CALIBRATE_SIGNAL[];
extern byte code STR_CALIBRATE_SIGNAL_MIN[];
extern byte code STR_CALIBRATE_SIGNAL_MAX[];

// Parametry vysilace
extern byte code STR_PACKET_COUNT[];

//-----------------------------------------------------------------------------
// Hlavni Menu
//-----------------------------------------------------------------------------

extern byte code STR_MAIN_MENU[];

// Struktura hlavniho menu
enum {
  ID_MAIN_MENU_ID,
  ID_MAIN_MENU_LIMITS,
  ID_MAIN_MENU_BACKLIGHT,
  ID_MAIN_MENU_SOUND,

#ifndef __REMOTE__
  ID_MAIN_MENU_TRANSMITTER,
#else
  ID_MAIN_MENU_CONNECTION,
#endif// __REMOTE__

  _ID_MAIN_MENU_COUNT
};

//-----------------------------------------------------------------------------
// Menu Sound
//-----------------------------------------------------------------------------

extern byte code STR_MENU_SOUND_TITLE[];
extern byte code STR_MENU_SOUND[];

// Struktura menu
enum {
  ID_MENU_SOUND_KEY,
  ID_MENU_SOUND_ALARM,
#ifdef __REMOTE__
  ID_MENU_SOUND_SIGNAL,
#endif // __REMOTE__
  _ID_MENU_SOUND_COUNT
};


#endif  // __Lang_H__
