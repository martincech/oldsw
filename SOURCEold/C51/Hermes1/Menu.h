//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// Globalni promenne :

extern byte __xdata__ Display;              // Co mam v nasledujicim kroku prekreslit
extern byte __xdata__ ShowDiagnostic;       // YES/NO zobrazovat diagnostiku

// Typ zobrazeni
typedef enum {
  DISPLAY_MODE_NORMAL,              // Normalni zobrazeni
  DISPLAY_MODE_CONNECTION,          // Zobrazeni kvality spojeni
  DISPLAY_MODE_DIAGNOSTICS,         // Diagnosticke zobrazeni
  _DISPLAY_MODE_COUNT
} TDisplayMode;
extern TDisplayMode __xdata__ DisplayMode;     // Typ zobrazeni


void MenuInit();
// Inicializace zobrazeni

void MenuCalibrateAccu();
// Provede kalibraci akumulatoru

void MenuCalibrateSignal();
  // Provede kalibraci sily signalu

void MenuLanguage();
// Provede volbu jazyka

void MenuLogin();
  // Login ridice

void MenuExecute();
// Spusteni menu

void MenuZobrazLowBat();
// Zobrazi menu pro nizke napeti

void MenuRedraw(byte NewDisplay);
// Vykresleni behem mereni

#endif
