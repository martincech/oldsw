//*****************************************************************************
//
//    Alarm.c - Ovladani alarmu podle poruch
//    Version 1.0
//
//*****************************************************************************


#ifndef __Alarm_H__
   #define __Alarm_H__

#include "Hardware.h"     // pro podminenou kompilaci

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

typedef enum {
  ALARM_TYPE_TEMP,          // Teplota mimo meze
  ALARM_TYPE_SIGNAL,        // Ztrata signalu
  _ALARM_TYPE_COUNT
} TAlarmType;

typedef struct {
  byte       ShowAlarm;     // YES/NO zda se prave indikuje alarm
  TAlarmType Type;          // Typ alarmu
  byte       Quiet;         // YES/NO potlaceny reproduktor pri alarmu
} TAlarm;
extern TAlarm __xdata__ Alarm;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void AlarmInit(void);
// Inicializuje alarmu

void AlarmChange(void);
// Zapne / vypne hlaseni alarmu

void AlarmClear(void);
// Vynuluje alarm (volat periodicky na zacatku kazdeho mericiho cyklu)

void AlarmSet(TAlarmType AlarmType);
// Nastavi alarm pri nejake poruse

void AlarmTrigger(void);
// Casovac alarmu, volat kazdych TIMER0_PERIOD ms

#endif
