//*****************************************************************************
//
//    Tansmit.h       - Remote terminal transmitter
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Tansmit_H__
   #define __Tansmit_H__

#include "Hardware.h"

void TransmitInit( void);
// Inicializace vysilace

void TransmitExecute( void);
// Periodicka cinnost - volat vzdy az na konci sekundoveho tiku, aby se vysilalo v case, kdy ze nectou teplomery!

void TransmitCheckTransmitter();
  // Pokud je jiz paket odvysilan, vypne napajeni vysilace. Volat co nejcasteji.

/*void TransmitPause( void);
// Zastaveni vysilani

void TransmitResume( void);
// Rozbehnuti vysilani*/

#endif
