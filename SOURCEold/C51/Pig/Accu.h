//*****************************************************************************
//
//    Accu.h - Kontrola napajeciho napeti
//    Version 1.0
//
//*****************************************************************************

#ifndef __Accu_H__
   #define __Accu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void AccuWaitForVoltage();
// Pokud je nizke napajeci napeti, ceka na jeho obnoveni

#endif
