//*****************************************************************************
//
//    Bat2.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Bat2_H__
   #define __Bat2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Stat_H__
   #include "..\inc\Stat.h"
#endif

#ifndef __Histogr_H__
   #include "..\inc\Histogr.h"
#endif

#ifdef __C51__
// jen pro Keil
//   typedef int  int16;
//   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE                       0x0109  // verze FW

#define NOSNOST_NASLAPNYCH_VAH      99999
#define DILEK_NASLAPNYCH_VAH        1
#define POCETDESETIN_NASLAPNYCH_VAH 2
#define MAX_HMOTNOST_KALIBRACE       20000  // Maximalni hmotnost zadavana pri kalibraci
#define MAX_HMOTNOST_ZOBRAZENI       99999  // Maximalni zobrazitelna hmotnost
#define MIN_HMOTNOST_ZOBRAZENI       -9999  // Minimalni zobrazitelna hmotnost

typedef enum {
  UNITS_KG,
  UNITS_LB,
  _UNITS_COUNT
} TUnits;

typedef enum {
  BACKLIGHT_OFF,
  BACKLIGHT_ON,
  BACKLIGHT_AUTO,
  _BACKLIGHT_COUNT
} TBacklight;

// ---------------------------------------------------------------------------------------------
// Default hodnoty nastaveni (cte i PC)
// ---------------------------------------------------------------------------------------------

#define DEFAULT_IDENTIFICATION_NUMBER   1
#define DEFAULT_FILTER                  12     // 1.5sec (7.5Hz * 12)
#define DEFAULT_STABILIZATION_RANGE     30     // +-3.0%
#define DEFAULT_STABILIZATION_TIME      3      // 3 vzorky
#define DEFAULT_UNITS                   UNITS_KG
#define DEFAULT_HISTOGRAM_RANGE         40     // +-40%, izraelci maji +-45%
#define DEFAULT_UNIFORMITY_RANGE        10     // +-10%
#define DEFAULT_BACKLIGHT               BACKLIGHT_AUTO
#define DEFAULT_LIGHT_DURATION          5      // 5 sekund
#define DEFAULT_SOUND_DURATION          300    // 300 ms
#define DEFAULT_SOUND_VOLUME            5      // Nekde uprostred

// ---------------------------------------------------------------------------------------------
// Meze hodnot v nastaveni
// ---------------------------------------------------------------------------------------------

#define MIN_NORMOVANA_HMOTNOST      30      // Minimalni pripustna cilova hmotnost, pokud vyjde mensi, koriguju
#define MAX_NORMOVANA_HMOTNOST      0xFFFF  // Maximalni pripustna cilova hmotnost

#define MIN_IDENTIFICATION_NUMBER 1     // Minimalni hodnota identifikacniho cisla
#define MAX_IDENTIFICATION_NUMBER 99    // Maximalni hodnota identifikacniho cisla

#define MIN_HISTOGRAMU_RANGE 10      // Minimalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu
#define MAX_HISTOGRAMU_RANGE 100     // Maximalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu (+-100% dava rozsah 0 az 2xstred)
#define MIN_UNIFORMITY_RANGE 1       // Minimalni hodnota rozsahu uniformity v procentech
#define MAX_UNIFORMITY_RANGE 49      // Maximalni hodnota rozsahu uniformity v procentech (+-49% dava 98% celkem)

#define MIN_FILTER              1       // Minimalni hodnota filtru
#define MAX_FILTER              40      // Maximalni hodnota filtru (7.5Hz * 40 = cca 5sec)
#define MIN_STABILIZATION_RANGE 0       // Minimalni hodnota ustaleni
#define MAX_STABILIZATION_RANGE 250     // Maximalni hodnota ustaleni (+-25.0)
#define MIN_STABILIZATION_TIME  2       // Minimalni hodnota doby ustaleni
#define MAX_STABILIZATION_TIME  9       // Maximalni hodnota doby ustaleni - musi se rovnat AVERAGE_CAPACITY v hardware.h

#define MIN_WEIGHING_NUMBER   1       // Minimalni hodnota cisla vazeni
#define MAX_WEIGHING_NUMBER   999     // Maximalni hodnota cisla vazeni

#define MIN_WEIGHING_LIMIT    50      // Minimalni hodnota meze
#define MAX_WEIGHING_LIMIT    NOSNOST_NASLAPNYCH_VAH   // Maximalni hodnota meze

// ---------------------------------------------------------------------------------------------
// Zobrazeni
// ---------------------------------------------------------------------------------------------

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_NV_HMOTNOST      0x01           // Zobrazi se naslapna vaha po vypocteni nove aktualni hmotnosti
#define ZOBRAZIT_NV_ULOZENO       0x02           // Zobrazi se naslapna vaha po ulozeni nove hmotnosti
#define ZOBRAZIT_1SEC             0x04           // Zobrazi se periodicke veci 1sec (napr. cas)
#define ZOBRAZIT_HISTOGRAM_KURZOR 0x08           // Zobrazi se kurzor v histogramu spolecne s hmotnosti a poctem
#define ZOBRAZIT_LOGGER           0x10           // Zobrazi se stranka se vzorky loggeru
#define ZOBRAZIT_MRIZKU           0x80           // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_VSE              0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit


// ---------------------------------------------------------------------------------------------
// Datum a cas
// ---------------------------------------------------------------------------------------------

// Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
typedef struct {
  byte Min;
  byte Hour;
  byte Day;
  byte Month;
  word Year;
} TDateTime;  // 6 bajtu

// Globalni datum a cas
extern TDateTime __xdata__ ActualDateTime;

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

#define MAX_POCET_PLOSIN 10             // Maximalni pocet plosin

// Kalibrace 1 plosiny
typedef struct {
  int32 KalibraceNuly;                 // Kalibrace nuly
  int32 KalibraceRozsahu;              // Kalibrace rozsahu
  int32 Rozsah;                        // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
  byte Dilek;                           // Dilek vah
} TKalibracePlosiny; // 13 bajtu

// Kalibrace
typedef struct {
  byte PouzitaPlosina;                  // Cislo prave pouzite plosiny
  TKalibracePlosiny Plosiny[MAX_POCET_PLOSIN];
} TKalibrace;   // 131 bajtu

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

// Prubeh vazeni
typedef struct {
  byte      Running;                    // YES/NO prave probiha vazeni
  word      Number;                     // Cislo vazeni
  TDateTime StartDateTime;              // Datum a cas zahajeni vazeni
  TRawValue LowLimit;                   // Dolni mez hmotnosti
  TRawValue HighLimit;                  // Horni mez hmotnosti
} TWeighing;       // 17 bajtu

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Version;                        // VERZE
  byte IdentificationNumber;           // identifikace zarizeni
  byte Language;                       // Jazykova verze + jazyk

  byte Filter;                         // Filtr prevodniku
  byte StabilizationRange;             // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
  byte StabilizationTime;              // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)

  TUnits Units;                        // Zobrazovane jednotky

  byte HistogramRange;                 // Rozsah histogramu v +- % stredni hodnoty
  byte UniformityRange;                // Rozsah uniformity v +- %

  TWeighing Weighing;                  // Parametry vazeni

  TBacklight Backlight;                // Nastaveny rezim podsvitu

  byte LightDuration;                  // Doba svitu svetel po ulozeni v sekundach
  word SoundDuration;                  // Doba piskani po ulozeni v milisekundach
  byte SoundVolume;                    // Hlasitost piskani po ulozeni

  byte Battery;                        // YES/NO Vaha je napajena z baterie - zatim se nepouziva

  byte Checksum;                       // Kontrolni soucet
} TConfig;

extern TConfig __xdata__ Config;                // Buffer konfigurace v externi RAM
extern TKalibrace   __xdata__ Kalibrace;        // Buffer kalibrace v externi RAM

#define WeighingRunning()    Config.Weighing.Running  // Podminka, zda probiha vazeni

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast

#define CFG_ALL                          0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERSION                      0x00000001L
#define CFG_IDENTIFICATION_NUMBER        0x00000002L
#define CFG_FILTER                       0x00000004L
#define CFG_STABILIZATION_RANGE          0x00000008L
#define CFG_STABILIZATION_TIME           0x00000010L
#define CFG_WEIGHING                     0x00000020L
#define CFG_UNITS                        0x00000040L
#define CFG_HISTOGRAM_RANGE              0x00000080L
#define CFG_UNIFORMITY_RANGE             0x00000100L
#define CFG_LANGUAGE                     0x00000200L
#define CFG_BACKLIGHT                    0x00000400L
#define CFG_LIGHT_DURATION               0x00000800L
#define CFG_SOUND_DURATION               0x00001000L
#define CFG_SOUND_VOLUME                 0x00002000L

//-----------------------------------------------------------------------------
// Struktura archivu
//-----------------------------------------------------------------------------

// narazniky :

#define FL_ARCHIV_EMPTY        0xFE
#define FL_ARCHIV_FULL         0xFF

typedef struct {
   word         Number;                  // Cislo vazeni
   TDateTime    StartDateTime;           // Datum a cas zahajeni vazeni
   TStatistic   Stat;                    // statistika / statistika samic
   THistogram   Hist;                    // histogram  / histogram  samic
   TRawValue    LowLimit;                // Dolni mez hmotnosti
   TRawValue    HighLimit;               // Horni mez hmotnosti
   byte         RealUniformityUsed;      // YES/NO, zda se pouzil presny vypocet uniformity, ktery je v promenne RealUniformity
   byte         RealUniformity;          // Uniformita vypoctena presne z jednotlivych vzorku
} TArchive;        // 211 bajtu - pozor, pri prekroceni 255 bajtu predefinovat promennou v SaveContext() a LoadContext()

//-----------------------------------------------------------------------------
// Zaznam vzorku
//-----------------------------------------------------------------------------

// Naraznikove znaky :

#define LG_SAMPLE_EMPTY        0x7F     // nenaplnene FIFO
#define LG_SAMPLE_FULL         0xFF     // naplnene FIFO
#define LG_SAMPLE_VALUE        0x20     // zaznam hodnoty
#define LG_SAMPLE_GENDER       0x10     // bit pohlavi
#define LG_SAMPLE_WEIGHT_MSB   0x01     // 17. bit hmotnosti ve flagu

#define LG_SAMPLE_MASK_WEIGHT    0x01FFFF  // Hmotnost je ulozena ve spodnich 17bit, MSB byte je flag (hmotnost tedy zabira 1 spodni bit z flagu)
#define LG_SAMPLE_MASK_HI_WEIGHT 0x010000  // 17. bit hmotnosti
#define LG_SAMPLE_MASK_HOUR      0x001F    // Hodina je ulozena ve spodnich 5 bitech

typedef struct {
   byte  Flag;        // naraznik
   word  Value;       // vaha nebo hodina (vaha je na 17 bitu)
} TLoggerSample;      // 3 bajty

// zaznam jednotlivych vazeni :
#define FL_LOGGER_SAMPLES      1801L
#define FL_LOGGER_SAMPLE_SIZE  sizeof( TLoggerSample)                       // 3 bajty

// struktura dne (jen pro pro Builder a vypocet velikosti) :
typedef struct {
   TArchive       Archive;                                                   // 211  bytu
   TLoggerSample  Samples[ FL_LOGGER_SAMPLES];                               // 5403 bytu
} TArchiveDailyInfo;   // 5614 bytu

//-----------------------------------------------------------------------------
// Struktura FLASH
//-----------------------------------------------------------------------------

// rozdeleni pameti na sekce
// U AT45BD161B mam k dispozici 4096 x 512 = 2 097 152 bajtu

// konfiguracni sekce :
#define FL_CONFIG_BASE         0L
#define FL_CONFIG_SIZE         4096L

// archiv statistik & histogramu :
#define FL_ARCHIV_BASE         (FL_CONFIG_BASE + FL_CONFIG_SIZE)
#define FL_ARCHIV_DAY_SIZE     sizeof( TArchiveDailyInfo)                   // velikost polozky
#define FL_ARCHIV_DAYS         371L                                         // pocet polozek
#define FL_ARCHIV_SIZE         (FL_ARCHIV_DAY_SIZE * FL_ARCHIV_DAYS)        // celkem bytu (2082794), konec pameti 2086890

// struktura konfiguracni sekce :
typedef struct {
  TConfig      Config;                 // Konfigurace
  TKalibrace   Kalibrace;              // Kalibrace zkopirovana z interni EEPROM
} TConfigSection; // 2780 bajtu

//-----------------------------------------------------------------------------
// Struktura interni EEPROM
//-----------------------------------------------------------------------------


#define IEP_SPARE (IEP_SIZE - sizeof(TKalibrace))

typedef struct {
  TKalibrace    Kalibrace;             // Kalibrace vah
  byte          Volne[IEP_SPARE];
} TIntEEPROM;


//-----------------------------------------------------------------------------
#endif
