//*****************************************************************************
//
//    NVahy.h - Naslapne vahy
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __NVahy_H__
   #define __NVahy_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#include "..\inc\Average.h"  // Klouzavy prumer

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Popisovac vahy :
typedef struct {
   long       Suma;                     // Suma prevodu pro prumerovani
   byte       Pocet;                    // Pocet vzroku v sume
   byte       ZahoditVzorek;            // Flag, ze se ma nasledujici nacteny vzorek zahodit

   long       Hmotnost;                 // Aktualni okamzita hmotnost na vaze
   byte       NovaHmotnost;             // Flag, ze se nacetla nova aktualni hmotnost
   long       PosledniUlozenaHmotnost;  // Posledni ulozena hmotnost

   // Detekce nastupu
   TAverage   Average;                  // Klouzavy prumer
   long       PosledniUstalenaHmotnost;
   byte       Ustaleni;                 // Flag, ze je vaha ustalena

   // Pauza vazeni
   byte       Pauza;                    // Flag, zda je prave zapauzovane vazeni (casove nebo rucne)
} TNVaha;

// Popisovac vah :

extern TNVaha __xdata__ NVaha;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void NVahyInit( void);
// Inicializace

void NVahyInitAverage();
// Inicializace klouzaveho prumeru podle konfigurace

void NVahySetPause(TYesNo NewPause);
// Nastaveni pauzy vahy

TYesNo NVahyObsluha( void);
// Projede vsechny prevody naslapnych vah a pokud je nejaky novy prevod, vypocte jeho hmotnost, zkontroluje, zda se ma ulozit atd.
// Pokud se ma ulozit novy vzorek, vrati YES

void NVahyEnable( void);
// Povoleni vazeni

void NVahyDisable( void);
// Zakaz vazeni

#endif
