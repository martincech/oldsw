//*****************************************************************************
//
//    Light.c - LED indikace
//    Version 1.0
//
//*****************************************************************************

#ifndef __Light_H__
   #define __Light_H__

#include "Hardware.h"     // zakladni datove typy

void LightInit();

void LightRedOn();
  // Rozne cervene svetlo

void LightGreenOn();
  // Rozne zelene svetlo

void LightExecute();
  // Periodicka obsluha, volat 1x za sekundu


#endif
