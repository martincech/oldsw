//*****************************************************************************
//
//    Lang.h      - Definice jazykove verze
//    Version 1.0
//
//*****************************************************************************

#ifndef __Lang_H__
   #define __Lang_H__

#include "Hardware.h"     // zakladni datove typy
#include "Pig.h"         // _JEDNOTKY_POCET

// Jazykova verze - definuje, o jakou jazykovou verzi vah se jedna
//#define LANG_VERSION_FR     1       // En + Fr
#define LANG_VERSION_FI    2       // En + Fi
//#define LANG_VERSION_TR    3       // En + Tr

// Logo zobrazene po startu
#define LOGO_VEIT               1       // Standardni logo "poultry scale bat1"
//#define LOGO_DECS               1       // Logo pro Decs Dansko

// Masky na dekodovani jazykove verze a jazyka
#define LANG_VERSION_MASK   0xE0        // Jazykova verze v hornich 3 bitech
#define LANG_MASK           0x1F        // Jazyk ve spodnich 5 bitech

// Nastaveni podle zvolene jazykove verze
#ifdef LANG_VERSION_FR
  #define LANG_VERSION    LANG_VERSION_FR       // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_FRENCH,
    _LANG_COUNT
  } TJazyk;
  #define POCET_JAZYKU      2                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_FI
  #define LANG_VERSION    LANG_VERSION_FI       // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_FINNISH,
    _LANG_COUNT
  } TJazyk;
  #define POCET_JAZYKU      2                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_TR
  #define LANG_VERSION    LANG_VERSION_TR       // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_TURKISH,
    _LANG_COUNT
  } TJazyk;
  #define POCET_JAZYKU      2                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

// ---------------------------------------------------------------------------------------------
// Definice jazyka
// ---------------------------------------------------------------------------------------------

extern byte code STR_JAZYK[];
extern byte code STR_MENU_JAZYK[];

// ---------------------------------------------------------------------------------------------
// Menu
// ---------------------------------------------------------------------------------------------

extern byte code STR_DEN[];
extern byte code STR_HMOTNOST[];
extern byte code STR_START[];
extern byte code STR_HEJNO[];
extern byte code STR_CILOVAHMOTNOST[];
extern byte code STR_POSLEDNIULOZENAHMOTNOST[];
extern byte code STR_POCET[];
extern byte code STR_OK[];
extern byte code STR_ZRUSIT[];
extern byte code STR_ZNOVU[];
extern byte code STR_ANO[];
extern byte code STR_NE[];
extern byte code STR_CHYBA[];
extern byte code STR_CEKEJTE[];
extern byte code STR_KS[];
//extern byte code STR_VERZE[];

// Jednotky mam zvlast pro vyber v menu a pro zobrazeni (kvuli jednoduchosti)
extern byte code STR_JEDNOTKY_MENU[];    // Pro menu
extern byte code STR_JEDNOTKY[_UNITS_COUNT][3];

// Dilek v menu
extern byte code STR_DILEK_MENU[];

extern byte code STR_NEPLATNA_HODNOTA[];
extern byte code STR_ULOZIT_ZMENY[];
extern byte code STR_CHYBA_KRMI_SE[];

// Typy zobrazeni behem vazeni
extern byte code STR_VAZENI_NEVAZISE[];
extern byte code STR_VAZENI_CEKANI[];
extern byte code STR_VAZENI_VAZENI[];
extern byte code STR_VAZENI_PAUZA[];
extern byte code STR_VAZENI_ONLINE[];

// Historie
extern byte code STR_HISTORIE[];

// Statistika
extern byte code STR_CV[];
extern byte code STR_UNI[];
extern byte code STR_NUMBER[];

// Logger
extern byte code STR_STRANA[];
extern byte code STR_ZADNA_DATA[];

// Editace hejna
extern byte code STR_HEJNO_CHCETE_UKONCIT_VYKRM[];
extern byte code STR_HEJNO_CISLO_KRMI_SE[];
extern byte code STR_HEJNO_START_POUZIT_HEJNO[];
extern byte code STR_HEJNO_START_POCATECNI_DEN[];
extern byte code STR_ZADEJTE_CISLO_HEJNA[];
extern byte code STR_OPRAVDU_SMAZAT_VSECHNA_HEJNA[];
extern byte code STR_VSECHNA_HEJNA_SMAZANA[];
extern byte code STR_HEJNO_START_OKAMZITE[];  //!!!

// Editace datumu a casu
extern byte code STR_ZADEJTE_DATUM[];
extern byte code STR_ZADEJTE_CAS[];

// Editace identifikacniho cisla
extern byte code STR_ZADEJTE_IDENTIFIKACNICISLO[];

// Editace parametru statistiky
extern byte code STR_ZADEJTE_ROZSAHHISTOGRAMU[];
extern byte code STR_ZADEJTE_ROZSAHUNIFORMITY[];

// Parametry naslapne vahy
extern byte code STR_NV_SAMICE_NAD[];
extern byte code STR_NV_SAMICE_POD[];
extern byte code STR_NV_SAMCI_NAD[];
extern byte code STR_NV_SAMCI_POD[];
extern byte code STR_NV_FILTR[];
extern byte code STR_NV_USTALENI[];
extern byte code STR_NV_DELKA_USTALENI[];
extern byte code STR_NV_TYP_AUTO_REZIMU[];

// Kalibrace
extern byte code STR_KALIBRACE_JEDNOTKY[];
extern byte code STR_KALIBRACE_PLOSINA[];
extern byte code STR_KALIBRACE_DILEK[];
extern byte code STR_KALIBRACE_NULOVY_BOD[];
extern byte code STR_KALIBRACE_ROZSAH[];

// Podsvit
extern byte code STR_PODSVIT[];
extern byte code STR_ON_OFF_AUTO[];

// Zahajeni vazeni
extern byte code STR_LOW_LIMIT[];
extern byte code STR_HIGH_LIMIT[];
extern byte code STR_WEIGHING_NUMBER[];

// Ukonceni vazeni
extern byte code STR_STOP_WEIGHING[];

// Smazani archivu
extern byte code STR_DELETE_ARCHIVE[];

// Svetla
extern byte code STR_LIGHT_DURATION[];

// Zvuky
extern byte code STR_SOUND_DURATION[];
extern byte code STR_SOUND_VOLUME[];

// Sekundy
extern byte code STR_SEC[];


//-----------------------------------------------------------------------------
// Hlavni Menu
//-----------------------------------------------------------------------------

extern byte code STR_HLAVNI_MENU_PRI_NEKRMENI[];

extern byte code STR_HLAVNI_MENU_PRI_KRMENI[];

extern byte code STR_HLAVNI_MENU_PRI_KRMENI_PAUZA[];

// Struktura hlavniho menu
enum {
  ID_HLAVNI_MENU_PAUZA,
  ID_HLAVNI_MENU_STARTSTOP,
  ID_HLAVNI_MENU_ARCHIV,
  ID_HLAVNI_MENU_NASTAVENI,
  ID_HLAVNI_MENU_SMAZAT_PAMET,
  _ID_HLAVNI_MENU_POCET
};
#define ID_HLAVNI_MENU_POCET_NEKRMENI (_ID_HLAVNI_MENU_POCET - 1)   // Pocet polozek hlavniho menu kdyz se nekrmi (chybi tam pauza)
#define ID_HLAVNI_MENU_POCET_KRMENI   (_ID_HLAVNI_MENU_POCET - 1)   // Pocet polozek hlavniho menu behem krmeni (chybi tam smazani pameti)

//-----------------------------------------------------------------------------
// Menu nastaveni
//-----------------------------------------------------------------------------

extern byte code STR_MENU_NASTAVENI_NADPIS[];

extern byte code STR_MENU_NASTAVENI[];

// Struktura menu nastaveni
enum {
  ID_MENU_NASTAVENI_DATUMACAS,
  ID_MENU_NASTAVENI_IDENTIFIKACNICISLO,
  ID_MENU_NASTAVENI_STATISTIKA,
  ID_MENU_NASTAVENI_NASLAPNEVAHY,
  ID_MENU_NASTAVENI_PODSVIT,
  ID_MENU_NASTAVENI_SVETLA,
  ID_MENU_NASTAVENI_ZVUK,
  _ID_MENU_NASTAVENI_POCET
};

//-----------------------------------------------------------------------------
// Menu vahy
//-----------------------------------------------------------------------------

extern byte code STR_MENU_NASTAVENI_NASLAPNEVAHY_NADPIS[];

extern byte code STR_MENU_NASTAVENI_NASLAPNEVAHY[];

// Struktura menu nastaveni naslapne vahy
enum {
  ID_MENU_NASTAVENI_NASLVAHY_PARAMETRY,
  ID_MENU_NASTAVENI_NASLVAHY_JEDNOTKY,
  ID_MENU_NASTAVENI_NASLVAHY_PLOSINY,
  ID_MENU_NASTAVENI_NASLVAHY_KALIBRACE,
  _ID_MENU_NASTAVENI_NASLVAHY_POCET
};




#endif  // __Lang_H__
