//*****************************************************************************
//
//    Podsvit.h - Kontrola podsvitu
//    Version 1.0
//
//*****************************************************************************

#ifndef __Podsvit_H__
   #define __Podsvit_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void PodsvitInit();
  // Inicializace

void PodsvitAutoOn();
  // Rozne automaticky podsvit

void PodsvitExecute();
  // Ovladaniu podsvitu, volat kazdou sekundu

#endif
