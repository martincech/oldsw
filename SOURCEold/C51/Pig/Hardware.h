//*****************************************************************************
//
//    Hardware.h  - BAT2 hardware descriptions
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof


// Deska ver 1
#define BatAccuOk()  BatLOWBAT
#define PowerFail()  !BatLOWBAT

// Bastldeska
//#define BatAccuOk()  !BatLOWBAT
//#define PowerFail()  BatLOWBAT

//-----------------------------------------------------------------------------
// Prirazeni portu
//-----------------------------------------------------------------------------

// datova sbernice :

#define BatDATA P0

sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :

sbit BatCSRTC    = P1^0;
sbit BatRDRTC    = P1^1;
sbit BatWRRTC    = P1^2;
sbit BatALERTC   = P1^3;
sbit BatCSEE     = P1^4;               // CS flash
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;
// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// Port P2 :

sbit BatLOWBAT   = P2^0;
sbit BatZAPGSM   = P2^1;
sbit BatPISK     = P2^2;
sbit BatK0       = P2^3;
sbit BatK1       = P2^4;
sbit BatRES      = P2^5;               // RES display
sbit BatCS2      = P2^6;               // CS1 display
sbit BatCS1      = P2^7;               // CS2 display

// Port P3 :

sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatCSMOD    = P3^2;               // CS flash modulu
sbit BatDRAD     = P3^3;               // DRDY AD1241
sbit BatCLKAD    = P3^4;               // CLK  AD1241
sbit BatK2       = P3^5;
sbit BatSVIT     = P3^6;               // podsvit
sbit BatDATAAD   = P3^7;               // DOUT AD1241


// Kanal vahy :

#define WEIGHT_CHANNEL   0             // cislo kanalu A/D pro vahu
typedef long TRawValue;                // surova hodnota vazeni

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  2             // zdvojena baudova rychlost

// Casovac 0 :
#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  BatAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut BatPISK                 // vystup generatoru
#define PCA_IDLE_STATE 1               // Klidova uroven pinu PcaOut

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1               // asynchronni pipani

//-----------------------------------------------------------------------------
// A/D prevodnik ADS1241
//-----------------------------------------------------------------------------

#define AdcDIN         BatDATAAD       // vstup
#define AdcDOUT        BatDATAAD       // vystup
#define AdcSCLK        BatCLKAD        // hodiny
#define AdcDRDY        BatDRAD         // -ready

#define AdcClrCS()                     // chipselect vyber
#define AdcSetCS()                     // deselect

#define ADC_GAIN       ADC_GAIN_2      // zakladni zesileni
#define ADC_RATE       ADC_7_5HZ       // rychlost vzorkovani
#define ADC_POLARITY   ADC_BIPOLAR     // rezim polarity
#define ADC_RANGE      ADC_HALF_RANGE  // plny/polovicni rozsah

#define ADC_INTERRUPT  INT_EXTERNAL1   // rezim na pozadi, vstup preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

//-----------------------------------------------------------------------------
// Pripojeni RTC DS17287
//-----------------------------------------------------------------------------

// porty :

#define RtcALE         BatALERTC       // ALE address latch enable
#define RtcWR          BatWRRTC        // WR\ zapis
#define RtcRD          BatRDRTC        // RD\ cteni
#define RtcAD          BatDATA         // datova/adresni sbernice
#define RtcCS          BatCSRTC        // chipselect

// volne adresy v RAM :

#define RTC_RAM_FIRST  0x0E            // od teto adresy
#define RTC_RAM_LAST   0x3F            // do teto adresy je RAM k dispozici

// kapacita NVRAM :

#define RTC_NVRAM_SIZE 2048

// podmineny preklad :

#define RTC_USE_DATE    1              // cti/nastavuj datum
//#define RTC_USE_WDAY    1              // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1              // hlidej zadavane hodnoty na rozsah
//#define RTC_USE_ALARM   1              // pouzivej alarm/budik
#define RTC_USE_NVRAM   1              // pouzita NVRAM

//-----------------------------------------------------------------------------
// ED2 interni SPI
//-----------------------------------------------------------------------------

#ifndef __SpiHw_H__
   #include "..\inc\SpiHw.h"
#endif

// SPI mode 3
#define SPI_BAUD   SPI_CLOCK_2         // rychlost
#define SPI_LEVEL  SPI_CLOCK_H         // klidova hladina SCK
#define SPI_EDGE   SPI_EDGE_RETURN     // strobovaci hrana SCK

//-----------------------------------------------------------------------------
// Flash Atmel AT45DB161
//-----------------------------------------------------------------------------

#define __AT45DB161__      1           // typ pameti
#define FLASH_TIMEOUT   5000           // pocet cyklu cekani na ready

#define __flash_data__  xdata          // kontextove promenne

// Podmineny preklad :
#define FLASH_READ_BYTE    1           // cteni bytu
#define FLASH_WRITE_BYTE   1           // zapis+flush bytu


// NVRAM buffer :
//#define FLASH_LOAD_BUFFER  1           // cteni pameti do bufferu
//#define FLASH_READ_BUFFER  1           // cteni bufferu
// Pro nonvolatile buffer :
#define FLASH_NVRAM        1           // pouzit modul NvFlash.c51
#define FLASH_VERIFY       1           // verifikace po zapisu stranky


/*
// FLASH buffer :
#define FLASH_LOAD_BUFFER  1           // cteni pameti do bufferu
#define FLASH_READ_BUFFER  1           // cteni bufferu
// Pro nonvolatile buffer :
//#define FLASH_NVRAM        1           // pouzit modul NvFlash.c51
//#define FLASH_VERIFY       1           // verifikace po zapisu stranky
*/



//-----------------------------------------------------------------------------
// Radic displeje Samsung Ks0108b
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       BatDATA
#define DisplayD0         BatD0
#define DisplayD1         BatD1
#define DisplayD2         BatD2
#define DisplayD3         BatD3
#define DisplayD4         BatD4
#define DisplayD5         BatD5
#define DisplayD6         BatD6
#define DisplayD7         BatD7
#define DisplayRW         BatWRRTC
#define DisplayDI         BatALERTC
#define DisplayE          BatRDRTC
#define DisplayCS1        BatCS1
#define DisplayCS2        BatCS2
#define DisplayRESET      BatRES

// Podminena kompilace

// Datova sbernice DisplayDATA 1:1
#define KS_BUS     1
// Obecne funkce
#define KS_ON      1


//-----------------------------------------------------------------------------
// Displej - podminena kompilace
//-----------------------------------------------------------------------------

// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ xdata

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Cary, obdelniky atd.
#define DISPLAY_SET_AREA_FAST     1
#define DISPLAY_SET_AREA          1
#define DISPLAY_HOR_LINE          1
//#define DISPLAY_FAST_VERT_LINE    1
#define DISPLAY_VERT_LINE         1
//#define DISPLAY_PIXEL             1
//#define DISPLAY_LINE              1
//#define DISPLAY_FAST_BARGRAF      1

// Fonty - pouziti jednotlivych fontu
#define DISPLAY_TAHOMA8           1
#define DISPLAY_LUCIDA6           1
//#define DISPLAY_MYRIAD32          1
#define DISPLAY_MYRIAD40          1

// Vety - pouziti jednotlivych zarovnani u vet
#define DISPLAY_STRING            1
#define DISPLAY_STRING_RIGHT      1
#define DISPLAY_STRING_CENTER     1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER     1
#define DISPLAY_NUMBER            1
//#define DISPLAY_CHAR_FORMATED     1

// Symbol
#define DISPLAY_SYMBOL            1

// Editace
#define DISPLAY_EDIT              1
//#define DISPLAY_EDIT_TEXT         1

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
//#define ObsluzModEditace()
//void ObsluzModEditace();
void DisplayEditExecute(byte idata *Znaky);



//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// Podminena kompilace:

//#define DISPLAY_BARGRAF     1

// Dialog
#define DISPLAY_DIALOG 1
#define DIALOG_OTOCENE_KLAVESY 1        // Zda se ma otocit zobrazeni tlacitek Enter a Esc

// Menu
#define DISPLAY_MENU        1
#define MENU_MAX_ITEMS      9             // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
#define DISPLAY_CHOOSE_ITEM 1

// Jazyky
#define DISPLAY_LANGUAGES   1             // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka

//-----------------------------------------------------------------------------
// Displej - parametry
//-----------------------------------------------------------------------------

#define SIRKA_DISPLEJE 128
#define VYSKA_DISPLEJE 64

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define ZapniPodsvit() BatSVIT = 1
#define VypniPodsvit() BatSVIT = 0

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define K0         BatK0
#define K1         BatK1
#define K2         BatK2


// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
//  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_LEFT,                              // sipka doleva
  K_RIGHT,                             // sipka doprava
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

//#define __DISPLAY_CURSOR__    1      // nastavovani kurzoru
#define __DISPLAY_CLEAR__     1        // mazani
#define __DISPLAY_CLR_EOL__   1        // mazani do konce radku

//-----------------------------------------------------------------------------
// Statistika
//-----------------------------------------------------------------------------

typedef float TNumber;                 // zakladni datovy typ

#define STAT_UNIFORMITY      1         // povoleni vypoctu uniformity
#define STAT_REAL_UNIFORMITY 1         // povoleni presneho vypoctu uniformity

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

typedef word      THistogramCount;     // pocet vzorku
typedef TRawValue THistogramValue;     // hodnota vzorku
#define HISTOGRAM_MIN_STEP    1        // Minimalni hodnota kroku v THistogramValue

#define HISTOGRAM_SLOTS       39       // pocet sloupcu - sude i liche cislo (max.254)

#define HISTOGRAM_GETDENOMINATOR 1
#define HISTOGRAM_SET_STEP       1
//#define HISTOGRAM_EMPTY          1

//-----------------------------------------------------------------------------
// Klouzavy prumer
//-----------------------------------------------------------------------------

typedef byte   TAverageCount;     // Pocet vzorku
typedef long   TAverageValue;     // Hodnota vzorku

#define AVERAGE_CAPACITY 9        // Kapacita (maximalni pocet prvku) klouzaveho prumeru

// Podmineny preklad
#define AVERAGE_FULL    1         // Test zaplneni
#define AVERAGE_STEADY  1         // Test zaplneni

//-----------------------------------------------------------------------------
// Flash Fifo
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Komunikace RS232
//-----------------------------------------------------------------------------
#define COM_BAUD       19200          // Rychlost linky v baudech

//#define COM_PARITY_EVEN  1          // Suda parita
//#define COM_PARITY_ODD   1          // Licha parita

//#define COM_TIMER_1      1          // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2        1          // K casovani linky se vyuzije casovac 2

#define COM_RX_TIMEOUT     10000      // Meziznakovy timeout [us]

// Podmineny preklad :
#define COM_TX_CHAR        1           // Preklada se ComTxChar
#define COM_RX_CHAR        1           // Preklada se ComRxChar
#define COM_RX_WAIT        1           // Preklada se ComRxWait
#define COM_FLUSH_CHARS    1           // Preklada se ComFlushChars

//-----------------------------------------------------------------------------
// Modul GSM
//-----------------------------------------------------------------------------

#define SMS_MAX_LENGTH       161       // max. delka prijate zpravy, vc. koncove nuly

// buffer pro cislo :
extern char xdata GsmNumber[];
extern byte xdata GsmNumberLength;

// Definice ciloveho cisla :
#define SmsDstNumberLength()       GsmNumberLength              // pocet cislic ciloveho cisla
#define SmsDstNumberDigit( i)      (GsmNumber[i] - '0')         // cislice v poradi, jak je piseme

// Definice zdrojoveho cisla :
#define SmsSrcNumberLength( n)     GsmNumberLength = n;         // pocet cislic zdrojoveho cisla
#define SmsSrcNumberDigit( i, n)   GsmNumber[ i] = (n) + '0'    // cislice v poradi, jak je piseme

// Podminena kompilace :

//#define GSM_COM2            1          // komunikace pres COM2
//#define COM_RX_STRING       1          // funkce pro GSM_OPERATOR
//#define GSM_OPERATOR        1          // nazev operatora
//#define GSM_SIGNAL_STRENGTH 1          // sila signalu
//#define GSM_DATA            1          // datova komunikace
#define GSM_SMS             1          // vysilani, prijem SMS
//#define GSM_SMS_USE_GSMCTL  1          // pri obsluze SMS se bude vyuzivat modul GsmCtl

// Timeouty radice GSM :

#define GSM_SMS_SEND_TIMEOUT  15       // SMS center reply timeout [s]
#define GSM_CONNECT_TIMEOUT   30       // od zvednuti do navazani spojeni  [s]
#define GSM_CHECK_PERIOD      30       // cas mezi dvema kontrolami registrace [s]
#define GSM_POWERED_PERIOD    20       // cas na nabeh napajeni modemu [s]
#define GSM_HANGUP_TIMEOUT    30       // zaveseni pri neaktivnim spojeni [s]

//-----------------------------------------------------------------------------
// COM_util
//-----------------------------------------------------------------------------

// Podminena kompilace :

#define COM_TX_DIGIT        1
#define COM_TX_HEX          1
#define COM_TX_DEC          1
#define COM_RX_HEX          1
#define COM_SKIP_CHARS      1

//-----------------------------------------------------------------------------
// LED diody
//-----------------------------------------------------------------------------

// Pozor, nesmi se pouzit piny pripojene na vnitrni Flash - LED se rozinaji jeste pred ulozenim
#define SetRedLight(STATE)   BatCSMOD = !STATE
#define SetGreenLight(STATE) BatTXD   = !STATE


//-----------------------------------------------------------------------------

#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------

#endif
