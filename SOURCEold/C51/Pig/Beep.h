//*****************************************************************************
//
//    Beep.c - Piskani reproduktoru
//    Version 1.0
//
//*****************************************************************************


#ifndef __Beep_H__
   #define __Beep_H__

#include "Hardware.h"
#include "..\inc\Pca.h"        // zvuky pomoci PCA

void BeepKey();
  // Piskne po stisku klavesy (touch panelu)

void PipniKlavesnice();
  // Bohuzel je treba to definovat kvuli displeji

#define BeepStartup() PcaBeep( NOTE_C4, VOL_10, 200)
  // Piskne po zapnuti jednotky

void BeepSaving(byte Volume, word Duration);
  // Piskne pri ulozeni vzorku do pameti s hlasitosti <Volume> a delkou <Duration>

#endif
