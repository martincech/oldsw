//*****************************************************************************
//
//    Archiv.h     - Ukladani udaju
//    Version 1.0, (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __Archiv_H__
   #define __Archiv_H__

#ifndef __Hardware_H__
   #include "Hardware.h"        // zakladni datove typy
#endif

#ifndef __FlFifo_H__
   #include "..\inc\FlFifo.h"   // Fifo
#endif

#ifndef __Bat2_H__
   #include "Pig.h"
#endif

extern TFifo code InitDayFifo;          // Vyuziva i GsmCtl

// Pracovni data :
extern TFifo    __xdata__ DayFifo;
extern TFifo    __xdata__ SamplesFifo;
extern TArchive __xdata__ Archive;

extern TFifo code InitSamplesFifo;      // Pro zobrazeni loggeru

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void ArchivInit();
// Inicializace archivu (po nabehu napajeni)

void ArchivNewWeight( TRawValue Value);
// Prida nove zvazenou hodnotu do archivu

void ArchivNewHour( void);
// Zapise zmenu hodiny do archivu

void ArchivNewWeighing();
  // Zalozi nove vazeni podle parametru v Config.Weighing

void ArchivSaveDay();
// Ulozi aktualni statistiku do archivu ve Flash

void ArchivFinishWeighing();
// Ukonci den

void ArchivNewDay();
// Zalozi novy den

TRawValue LoggerGetWeight(TLoggerSample __xdata__ *Sample);
// Ze zadaneho vzorku vrati hmotnost

//-----------------------------------------------------------------------------
// Operace
//-----------------------------------------------------------------------------

void ArchivReset( void);
// Mazani archivu

#define ArchivReadDay( MyArchiv, Index)      FifoReadFragment( &DayFifo, (Index), &(MyArchiv), offsetof( TArchiveDailyInfo, Archive), sizeof( TArchive))
// Nacte polozku <MyArchiv> z pozice <Index>

#define ArchivDaySamples( MySamples, Index)  MySamples.Base = FifoItemAddress( &DayFifo, (Index), offsetof( TArchiveDailyInfo, Samples))
// Inicializuje popisovac fifo <MySamples> ze dne na pozici <Index>
// POZOR, fifo MySamples uz musi byt inicializovane, zde se ulozi jen Base

#endif
