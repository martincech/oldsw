//*****************************************************************************
//
//    Scale.h - Naslapne vahy
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Scale_H__
   #define __Scale_H__

#include "Hardware.h"           // zakladni datove typy
#include "..\inc\Average.h"     // Klouzavy prumer

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Popisovac vahy :
typedef struct {
   long       Sum;                      // Suma prevodu pro prumerovani
   byte       Count;                    // Pocet vzroku v sume
   byte       DiscardNextSample;        // Flag, ze se ma nasledujici nacteny vzorek zahodit

   long       Weight;                   // Aktualni okamzita hmotnost na vaze
   byte       NewWeight;                // Flag, ze se nacetla nova aktualni hmotnost
   long       LastSavedWeight;          // Posledni ulozena hmotnost
   byte       LastSavedGender;          // Pohlavi u posledne ulozene hmotnosti

   // Detekce nastupu
   TAverage   Average;                  // Klouzavy prumer
   long       LastStableWeight;         // Hodnota posledni ustalene hmotnosti
   byte       Stable;                   // Flag, ze je vaha ustalena

   // Pauza vazeni
   byte       WeighingLimitPause;       // Flag, zda je prave zapauzovane vazeni kvuli casovemu omezeni
   byte       Pause;                    // Flag, zda je prave zapauzovane vazeni (casove nebo rucne)
} TScale;

// Popisovac vah :

extern TScale __xdata__ Scale;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void ScaleInit( void);
// Inicializace

void ScaleInitAverage(void);
// Inicializace klouzaveho prumeru podle konfigurace

void ScaleSetPause(TYesNo NewPause);
// Nastaveni pauzy vahy

TYesNo ScaleExecute( void);
// Pokud vaha detekuje novy nastup, ulozi hmotnost do struktury a vrati YES.
// Automaticky nastavuje promennou Display

void ScaleEnable( void);
// Povoleni vazeni

void ScaleDisable( void);
// Zakaz vazeni

long ScaleReadConversionAverage(byte Count);
// Nacte <Count> prevodu a vrati prumer

#endif
