//*****************************************************************************
//
//    GsmMidnight.h         Bat2 midnight SMS sending
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __GsmMid_H__
   #define __GsmMid_H__

#include "Hardware.h"

#ifdef VERSION_GSM

// Stav pulnocni SMS
typedef struct {
  byte Created;                         // YES/NO v pameti je pripravena SMS pulnocni k odeslani
  word DayNumber;                       // Cislo dne statistiky v SMS
} TGsmMidnight;
extern TGsmMidnight __xdata__ GsmMidnight;

// Komplet, pouze pro NVRAM, jinak se nepouziva
typedef struct {
  TGsmMidnight Header;
  char Text[SMS_MAX_LENGTH];            // Text sestavene SMS
} TGsmMidnightNvRam;



void GsmMidnightInit(void);
  // Inicializace

void GsmMidnightClear(void);
  // Smaze obsah SMS
  // Volat po zahajeni vazeni, ukonceni vazeni a odeslani SMS

void GsmMidnightSaveSms(word DayNumber, char __xdata__ *Text);
  // Volat po sestaveni pulnocni SMS

void GsmMidnightLoadSms(char __xdata__ *Text);
  // Nacteni pulnocni SMS z NVRAM

TYesNo GsmMidnightCheckSendingTime(word DayNumber);
  // Volat periodicky kazdou minutu

#endif // VERSION_GSM

#endif // __GsmMid_H__
