//*****************************************************************************
//
//    BLight.h - Kontrola podsvitu
//    Version 1.0
//
//*****************************************************************************

#ifndef __BLight_H__
   #define __BLight_H__

#include "Hardware.h"     // zakladni datove typy

void BacklightInit(void);
  // Inicializace

void BacklightAutoOn(void);
  // Rozne automaticky podsvit

void BacklightExecute(void);
  // Ovladani podsvitu, volat kazdou sekundu

#endif
