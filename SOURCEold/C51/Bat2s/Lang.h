//*****************************************************************************
//
//    Lang.h      - Definice jazykove verze
//    Version 1.0
//
//*****************************************************************************

#ifndef __Lang_H__
   #define __Lang_H__

#include "Hardware.h"     // zakladni datove typy
#include "Bat2.h"         // _JEDNOTKY_POCET

// Jazykova verze - definuje, o jakou jazykovou verzi vah se jedna
#define LANG_VERSION_FR     1       // En + Fr
//#define LANG_VERSION_FI     1       // En + Fi
//#define LANG_VERSION_TR     1       // En + Tr
//#define LANG_VERSION_DK     1       // En + Dk
//#define LANG_VERSION_SP     1       // En + Sp
//#define LANG_VERSION_GE     1       // En + Ge
//#define LANG_VERSION_GE_FR  1       // Ge + FR
//#define LANG_VERSION_RU     1       // En + Ru
//#define LANG_VERSION_GR     1       // En + GR, pozor, je treba nastavit DISPLAY_TAHOMA8_GR v hardware.h

// Logo zobrazene po startu
//#define LOGO_VEIT               1       // Standardni logo "poultry scale bat2"
//#define LOGO_BROILERTEKKNIIKA   1       // Logo Breilertekkniika Finsko
//#define LOGO_DACS               1       // Logo pro Dacs Dansko
//#define LOGO_SWEDEGG            1       // Logo pro Swedegg Svedsko
//#define LOGO_SODALEC            1       // Logo Sodalec Francie
//#define LOGO_SUBSAHARAHSCALES   1       // Logo pro Sub Saharah Scales v Jizni Africe
//#define LOGO_VERTA_TEKNO        1       // Logo pro Werta Tekno Turecko
//#define LOGO_YDE                1       // Logo pro Yde Belgie
//#define LOGO_CHOOKS-nepouzivat  1       // Logo pro Aussie Chooks Equipment v Australii
//#define LOGO_ACE                1       // Logo pro ACE Integrated Solutions v Australii
#define LOGO_FARMWEIGHSYSTEMS   1       // Logo pro Farm Weigh Systems v USA (Dwain)
//#define LOGO_BIGDUTCHMAN        1       // Logo Big Dutchman - POVOLIT SLIDING AVG

// Masky na dekodovani jazykove verze a jazyka
#define LANG_VERSION_MASK   0xE0        // Jazykova verze v hornich 3 bitech
#define LANG_MASK           0x1F        // Jazyk ve spodnich 5 bitech

// Kontrola fontu (pouze rectina ma jiny font)
#ifdef LANG_VERSION_GR
  #ifndef DISPLAY_TAHOMA8_GR
    #error "DISPLAY_TAHOMA8_GR not defined"
  #endif
  #ifndef DISPLAY_LUCIDA6_GR
    #error "DISPLAY_LUCIDA6_GR not defined"
  #endif
#else
  #ifndef DISPLAY_TAHOMA8
    #error "DISPLAY_TAHOMA8 not defined"
  #endif
  #ifndef DISPLAY_LUCIDA6
    #error "DISPLAY_LUCIDA6 not defined"
  #endif
#endif


// Nastaveni podle zvolene jazykove verze
#ifdef LANG_VERSION_FR
  #define LANG_VERSION      LANG_VERSION_FR     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_FRENCH,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_FI
  #define LANG_VERSION      LANG_VERSION_FI     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_FINNISH,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_TR
  #define LANG_VERSION      LANG_VERSION_TR     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_TURKISH,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_DK
  #define LANG_VERSION      LANG_VERSION_DK     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_DANISH,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_SP
  #define LANG_VERSION      LANG_VERSION_SP     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_SPANISH,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_GE
  #define LANG_VERSION      LANG_VERSION_GE     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_GERMAN,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_GE_FR
  #define LANG_VERSION      LANG_VERSION_GE_FR  // Aktualni jazykova verze
  typedef enum {
    LANG_GERMAN,
    LANG_FRENCH,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_GERMAN         // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_RU
  #define LANG_VERSION      LANG_VERSION_RU     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_RUSSIAN,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_GR
  #define LANG_VERSION      LANG_VERSION_GR     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_GREEK,
    _LANG_COUNT
  } TLanguage;
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

// ---------------------------------------------------------------------------------------------
// Definice jazyka
// ---------------------------------------------------------------------------------------------

extern byte code STR_LANGUAGE[];
extern byte code STR_MENU_LANGUAGE[];

// ---------------------------------------------------------------------------------------------
// HW verze
// ---------------------------------------------------------------------------------------------

extern byte code STR_HW_VERSION[];

// ---------------------------------------------------------------------------------------------
// Menu
// ---------------------------------------------------------------------------------------------

extern byte code STR_DAY[];
extern byte code STR_WEIGHT[];
extern byte code STR_START[];
extern byte code STR_SCALE[];
extern byte code STR_TARGET_WEIGHT[];
extern byte code STR_LAST_SAVED_WEIGHT[];
extern byte code STR_COUNT[];
extern byte code STR_OK[];
extern byte code STR_CANCEL[];
extern byte code STR_RETRY[];
extern byte code STR_YES[];
extern byte code STR_NO[];
extern byte code STR_ERROR[];
extern byte code STR_WAIT[];
extern byte code STR_HEADS[];
//extern byte code STR_VERZE[];

// Jednotky mam zvlast pro vyber v menu a pro zobrazeni (kvuli jednoduchosti)
extern byte code STR_UNITS_MENU[];    // Pro menu
//extern byte code STR_UNITS[_UNITS_COUNT][3];
extern byte code STR_UNITS[_UNITS_COUNT][_LANG_COUNT][3];   // Pro zobrazeni

// Dilek v menu
extern byte code STR_DIVISION_MENU[];

extern byte code STR_INVALID_VALUE[];
extern byte code STR_SAVE_CHANGES[];
extern byte code STR_STOP_WEIGHING_FIRST[];

// Typy zobrazeni behem vazeni
extern byte code STR_WEIGHING_IDLE[];
extern byte code STR_WEIGHING_WAITING[];
extern byte code STR_WEIGHING_WEIGHING[];
extern byte code STR_WEIGHING_PAUSE[];
extern byte code STR_WEIGHING_ONLINE[];

// Historie
extern byte code STR_ARCHIVE[];

// Statistika
extern byte code STR_CV[];
extern byte code STR_UNI[];

// Logger
extern byte code STR_PAGE[];
extern byte code STR_NO_DATA[];

// Editace hejna
extern byte code STR_STOP_WEIGHING[];
extern byte code STR_SELECT_ANOTHER_FLOCK[];
extern byte code STR_USE_FLOCK[];
extern byte code STR_STARTING_DAY[];
extern byte code STR_PASSWORD[];
extern byte code STR_INVALID_PASSWORD[];
extern byte code STR_FLOCK_NUMBER[];
extern byte code STR_REALLY_DELETE_ALL_FLOCKS[];
extern byte code STR_ALL_FLOCKS_DELETED[];
extern byte code STR_START_WEIGHING_NOW[];
extern byte code STR_COMPARE_RESULTS[];
extern byte code STR_FLOCK_NOT_VALID[];

// Editace datumu a casu
extern byte code STR_DATE[];
extern byte code STR_TIME[];

// Editace identifikacniho cisla
extern byte code STR_IDENTIFICATION_NUMBER[];

// Editace parametru statistiky
extern byte code STR_HISTOGRAM_RANGE[];
extern byte code STR_UNIFORMITY_RANGE[];

// Parametry naslapne vahy
extern byte code STR_SCALE_MARGIN_ABOVE_FEMALES[];
extern byte code STR_SCALE_MARGIN_BELOW_FEMALES[];
extern byte code STR_SCALE_MARGIN_ABOVE_MALES[];
extern byte code STR_SCALE_MARGIN_BELOW_MALES[];
extern byte code STR_SCALE_FILTER[];
extern byte code STR_SCALE_STABILIZATION_RANGE[];
extern byte code STR_SCALE_STABILIZATION_TIME[];
extern byte code STR_SCALE_AUTO_MODE_TYPE[];
extern byte code STR_JUMP_MODE[];
extern byte code STR_ENTER_LEAVE_BOTH[];

// Kalibrace
extern byte code STR_CALIBRATION_UNITS[];
extern byte code STR_CALIBRATION_DIVISION[];
extern byte code STR_CALIBRATION_ZERO[];
extern byte code STR_CALIBRATION_RANGE[];

// Podsvit
extern byte code STR_BACKLIGHT[];

extern byte code STR_ON_OFF_AUTO[];

// Kopie do a z pametoveho modulu
extern byte code STR_INSERT_MODULE[];
extern byte code STR_NO_MODULE[];
extern byte code STR_COPY_OK[];
extern byte code STR_COPY_PROGRESS[];
extern byte code STR_CANT_COPY_CONFIG[];

// Gsm
#ifdef VERSION_GSM          // Pouze pokud pouziva GSM

extern byte code STR_GSM_USE[];
extern byte code STR_GSM_MIDNIGHT[];
extern byte code STR_GSM_PERIOD1[];
extern byte code STR_GSM_DAY1[];
extern byte code STR_GSM_PERIOD2[];
extern byte code STR_GSM_PERIOD_DAYS[];
extern byte code STR_GSM_SEND_TIME[];
extern byte code STR_GSM_REQUEST[];
extern byte code STR_GSM_CHECK_NUMBERS[];
extern byte code STR_GSM_SIGNAL_STENGTH[];
extern byte code STR_GSM_PIN_CODE[];
extern byte code STR_GSM_NUMBER[];
extern byte code STR_GSM_ACTIVE_NUMBERS[];

#endif // VERSION_GSM

// RS-485
#if defined(VERSION_DACS_LW1) || defined(VERSION_MODBUS)

extern byte code STR_RS485_ADDRESS[];
extern byte code STR_RS485_SPEED[];
extern byte code STR_RS485_SPEED_ITEMS[];
extern byte code STR_RS485_PARITY[];
extern byte code STR_RS485_PARITY_ITEMS[];
extern byte code STR_RS485_REPLY_DELAY[];
extern byte code STR_RS485_MS[];
extern byte code STR_RS485_PROTOCOL[];
extern byte code STR_RS485_PROTOCOL_ITEMS[];

#ifdef VERSION_MODBUS
extern byte code STR_RS485_SILENT_INTERVAL[];
#endif // VERSION_MODBUS

#endif // VERSION_DACS_LW1, VERSION_MODBUS

//-----------------------------------------------------------------------------
// Hlavni Menu
//-----------------------------------------------------------------------------

extern byte code STR_MAIN_MENU_IDLE[];

extern byte code STR_MAIN_MENU_WAITING[];

extern byte code STR_MAIN_MENU_WEIGHING[];

extern byte code STR_MAIN_MENU_PAUSE[];

// Struktura hlavniho menu
enum {
  ID_MAIN_MENU_PAUSE,
  ID_MAIN_MENU_START_STOP,
  ID_MAIN_MENU_ARCHIVE,
  ID_MAIN_MENU_COMPARE,
  ID_MAIN_MENU_COPY,
  ID_MAIN_MENU_SETUP,
  _ID_MAIN_MENU_COUNT
};
#define _ID_MAIN_MENU_COUNT_IDLE        (_ID_MAIN_MENU_COUNT - 1)     // Pocet polozek hlavniho menu kdyz se nekrmi (chybi tam pauza)
#define _ID_MAIN_MENU_COUNT_WAITING     (_ID_MAIN_MENU_COUNT - 1)     // Pocet polozek hlavniho menu kdyz se ceka na start krmeni (chybi tam pauza)
#define _ID_MAIN_MENU_COUNT_WEIGHING     _ID_MAIN_MENU_COUNT          // Pocet polozek hlavniho menu behem krmeni

//-----------------------------------------------------------------------------
// Menu nastaveni
//-----------------------------------------------------------------------------

extern byte code STR_MENU_SETUP_TITLE[];

extern byte code STR_MENU_SETUP[];

// Struktura menu nastaveni
enum {
  ID_MENU_SETUP_DATE_TIME,
  ID_MENU_SETUP_IDENTIFICATION_NUMBER,
  ID_MENU_SETUP_FLOCKS,
  ID_MENU_SETUP_STATISTICS,
  ID_MENU_SETUP_GSM_RS485,
  ID_MENU_SETUP_SCALE,
  ID_MENU_SETUP_BACKLIGHT,
  ID_MENU_SETUP_READ_FROM_MODULE,
  ID_MENU_SETUP_ONLINE,
  _ID_MENU_SETUP_COUNT
};

//-----------------------------------------------------------------------------
// Menu hejna
//-----------------------------------------------------------------------------

extern byte code STR_MENU_SETUP_FLOCKS_TITLE[];

extern byte code STR_MENU_SETUP_FLOCKS[];

// Struktura menu nastaveni hejna
enum {
  ID_MENU_SETUP_FLOCKS_NEW,
  ID_MENU_SETUP_FLOCKS_EDIT,
  ID_MENU_SETUP_FLOCKS_DELETE,
  ID_MENU_SETUP_FLOCKS_DELETE_ALL,
  _ID_MENU_SETUP_FLOCKS_COUNT
};

//-----------------------------------------------------------------------------
// Menu vahy
//-----------------------------------------------------------------------------

extern byte code STR_MENU_SETUP_SCALE_TITLE[];

extern byte code STR_MENU_SETUP_SCALE[];

// Struktura menu nastaveni naslapne vahy
enum {
  ID_MENU_SETUP_SCALE_PARAMETERS,
  ID_MENU_SETUP_SCALE_CORRECTION_CURVE,
  ID_MENU_SETUP_SCALE_UNITS,
  ID_MENU_SETUP_SCALE_CALIBRATION,
  _ID_MENU_SETUP_SCALE_COUNT
};

//-----------------------------------------------------------------------------
// Menu GSM
//-----------------------------------------------------------------------------

#ifdef VERSION_GSM          // Pouze pokud pouziva GSM

extern byte code STR_MENU_SETUP_GSM_TITLE[];

extern byte code STR_MENU_SETUP_GSM[];

// Struktura menu nastaveni GSM
enum {
  ID_MENU_SETUP_GSM_SETUP,
  ID_MENU_SETUP_GSM_NUMBERS,
  ID_MENU_SETUP_GSM_SIGNAL,
  ID_MENU_SETUP_GSM_PIN,
  _ID_MENU_SETUP_GSM_COUNT
};

#endif // VERSION_GSM

//-----------------------------------------------------------------------------
// Menu RS-485
//-----------------------------------------------------------------------------

#if defined(VERSION_DACS_LW1) || defined(VERSION_MODBUS)

extern byte code STR_MENU_SETUP_RS485_TITLE[];

extern byte code STR_MENU_SETUP_RS485[];

// Struktura menu nastaveni RS-485
enum {
  ID_MENU_SETUP_RS485_ADDRESS,
  ID_MENU_SETUP_RS485_SPEED,
  ID_MENU_SETUP_RS485_PARITY,
  ID_MENU_SETUP_RS485_PROTOCOL,
  ID_MENU_SETUP_RS485_DELAYS,
  _ID_MENU_SETUP_RS485_COUNT_ALL_VERSIONS  // Pozor, pocet polozek je pto kazdou verzi jiny
};

// Pocet polozek je pto kazdou verzi jiny
#ifdef VERSION_DACS_LW1
  #define _ID_MENU_SETUP_RS485_COUNT    (_ID_MENU_SETUP_RS485_COUNT_ALL_VERSIONS - 1)   // U Dacsu chybi protokol
#else
  #define _ID_MENU_SETUP_RS485_COUNT    (_ID_MENU_SETUP_RS485_COUNT_ALL_VERSIONS)       // U Modbusu je vse
#endif // VERSION_DACS_LW1

#endif // VERSION_DACS_LW1, VERSION_MODBUS

//-----------------------------------------------------------------------------
// Editace hejn
//-----------------------------------------------------------------------------

extern byte code STR_FLOCK_EXISTS[];
extern byte code STR_FLOCK_DOESNT_EXIST[];
extern byte code STR_ENTER_FLOCK_NAME[];
extern byte code STR_USE_BOTH_GENDERS[];            // Vyuziva i Menu.c
extern byte code STR_USE_CURVES[];
extern byte code STR_PREFACE_CURVE_FEMALES[];
extern byte code STR_PREFACE_CURVE_MALES[];
extern byte code STR_PREFACE_CURVE[];
extern byte code STR_USE_TIME_LIMIT[];
extern byte code STR_LIMIT_FROM[];
extern byte code STR_LIMIT_TILL[];
extern byte code STR_LIMIT_TIME[];

extern byte code STR_INITIAL_WEIGHT[];
extern byte code STR_INITIAL_WEIGHT_FEMALES[];
extern byte code STR_INITIAL_WEIGHT_MALES[];

extern byte code STR_NO_FLOCK_FOUND[];
extern byte code STR_CHOOSE_FLOCK[];
extern byte code STR_NONAME[];

// ---------------------------------------------------------------------------------------------
// Editace rustove krivky
// ---------------------------------------------------------------------------------------------

extern byte code STR_SAVE[];
extern byte code STR_DELETE[];
extern byte code STR_QUIT[];
extern byte code STR_NEXT[];
extern byte code STR_ADD[];
extern byte code STR_CHANGE[];
extern byte code STR_MOVE[];
extern byte code STR_DAY_COLON[];
extern byte code STR_WEIGHT_COLON[];
extern byte code STR_CURVE_FULL[];

extern byte code STR_CURVE_DAY_EXISTS[];
extern byte code STR_CURVE_DELETE[];

//-----------------------------------------------------------------------------
// SMS - Pozor, nevyuzivat zadne diakriticke znaky
//-----------------------------------------------------------------------------

extern byte code STR_SMS_SCALES[];
extern byte code STR_SMS_DAY[];
extern byte code STR_SMS_FEMALE[];
extern byte code STR_SMS_MALE[];
extern byte code STR_SMS_CNT[];
extern byte code STR_SMS_AVG[];
extern byte code STR_SMS_GAIN[];
extern byte code STR_SMS_SIG[];
extern byte code STR_SMS_CV[];
extern byte code STR_SMS_UNI[];

extern byte code STR_SMS_STOPPED[];
extern byte code STR_SMS_WAITING[];
extern byte code STR_SMS_NODATA[];

// ---------------------------------------------------------------------------------------------
// Editace korekcni krivky
// ---------------------------------------------------------------------------------------------

extern byte code STR_CORRECTION_USE[];
extern byte code STR_CORRECTION_DAY1[];
extern byte code STR_CORRECTION_DAY2[];
extern byte code STR_CORRECTION_CORRECTION[];


#endif  // __Lang_H__
