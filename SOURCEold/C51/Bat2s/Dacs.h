//*****************************************************************************
//
//    Dacs.c - emulace vahy Lohman LW1 pripojene k ACS
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#ifndef __Dacs_H__
   #define __Dacs_H__

#include "Hardware.h"               // zakladni datove typy

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Histogram pro ACS
#define DACS_HISTOGRAM_SLOTS    25              // Pocet sloupcu v histogramu posilanem do ACS

// Statistika aktualniho dne zverejnena pro ACS (neni shodna s aktualnim dnem v Bat2)
typedef struct {
  word  DayNumber;                              // Cislo dne
  word  TargetWeight;                           // Cilova hmotnost
  word  Average;                                // Prumerna hmotnost
  word  Count;                                  // Pocet vazeni
  byte  Uniformity;                             // Uniformita v %
  word  HistogramSlot[DACS_HISTOGRAM_SLOTS];    // Sloupce histogramu
} TDacsDay;     // 59 bajtu
extern TDacsDay __xdata__ DacsDay;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void DacsInit(void);
// Inicializace, volat az po inicializaci archivu

void DacsDisable(void);
// Pozastaveni cinnosti

void DacsEnable(void);
// Povoleni cinnosti

void DacsExecute(void);
// Provadeni prikazu, volat co nejcasteji

void DacsWaitTick(void);
// Obsluha cekani pred odeslanim odpovedi, volat s periodou TIMER0_PERIOD

void DacsNewWeight(void);
// Zkopiruje archiv aktualniho dne do DacsDay, volat po zvazeni noveho vzroku

#define DacsNewHour()   DacsNewWeight()
// Volat kazdou celou hodinu (prepocitava se histogram)

void DacsBat2Midnight(void);
// V Bat2 ubehla pulnoc, volat o pulnoci, az po FlockUpdateDay()

void DacsUpdateDay(void);
// Zkopiruje statistiku z aktualniho dne do struktury DacsDay


#endif // __Dacs_H__
