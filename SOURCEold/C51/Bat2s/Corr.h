//*****************************************************************************
//
//    Corr.c - Weight correction
//    Version 1.0, (c) P.Veit
//
//*****************************************************************************

#include "Bat2.h"

#define CorrectionIsActive(Day) (Config.WeightCorrection.Correction != 0 && Day > Config.WeightCorrection.Day1)
// Vrati YES, pokud se pro den <Day> provadi korekce

long CorrectionCalcWeight(word Day, long Weight);
// Vypocte korigovanou hmotnost ze dne <Day> s puvodni hmotnosti <Weight>

