//*****************************************************************************
//
//    Cfg.h - Configuration load/save
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Cfg_H__
   #define __Cfg_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Vyuziva se i pri cteni konfigurace z modulu
#define CFG_FL_START   (FL_CONFIG_BASE + offsetof(TConfigSection, Config))

void CfgCopyIdWithoutSpaces(byte __xdata__ *s);
// Kopie identifikacniho cisla bez uvodnich mezer

TYesNo CfgLoad(void);
// Nacte konfiguraci z Flash
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)

TYesNo CfgSave( dword mask);
// Ulozi polozky definovane maskou do Flash
// Vraci NO, nepovedl-li se zapis

byte CfgCalcChecksum(byte __xdata__ *cfg);
// Spocita a vrati Kontrolni soucet konfigurace zadane v <cfg>

void CfgSaveConfigFromModule(void);
// Zkopiruje vybrane polozky z <KonfiguraceModul> do <Konfigurace> a ulozi konfiguraci do flash

#endif
