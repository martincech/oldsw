//*****************************************************************************
//
//    Bat2.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Bat2_H__
   #define __Bat2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifdef __C51__
// jen pro Keil
//   typedef int  int16;
//   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE                       0x0108  // verze FW

#define NOSNOST_NASLAPNYCH_VAH      99999
#define DILEK_NASLAPNYCH_VAH        1
#define POCETDESETIN_NASLAPNYCH_VAH 3
#define MAX_HMOTNOST_KALIBRACE      200000  // Maximalni hmotnost zadavana pri kalibraci
#define MAX_HMOTNOST_ZOBRAZENI      200000  // Maximalni zobrazitelna hmotnost
#define MIN_HMOTNOST_ZOBRAZENI      -99999  // Minimalni zobrazitelna hmotnost

typedef enum {
  JEDNOTKY_KG,
  JEDNOTKY_LB,
  _JEDNOTKY_POCET
} TJednotky;

typedef enum {
  PODSVIT_OFF,
  PODSVIT_ON,
  PODSVIT_AUTO,
  _PODSVIT_POCET
} TPodsvit;

// Typ automatickeho zjistovani cilove hmotnosti
typedef enum {
  AUTOREZIM_BEZ_PRIRUSTKU,              // Cilova hmotnost se vypocte jako prumer ze vcerejska
  AUTOREZIM_S_PRIRUSTKEM,               // Cilova hmotnost se vypocte jako prumer + denni prirustek ze vcerejska
  _AUTOREZIM_POCET
} TTypAutoRezimu;

// ---------------------------------------------------------------------------------------------
// Default hodnoty nastaveni (cte i PC)
// ---------------------------------------------------------------------------------------------

#define DEFAULT_SAMICE_OKOLI_NAD     30
#define DEFAULT_SAMICE_OKOLI_POD     30
#define DEFAULT_SAMCI_OKOLI_NAD      30
#define DEFAULT_SAMCI_OKOLI_POD      30
#define DEFAULT_FILTR                12     // 1.5sec (7.5Hz * 12)
#define DEFAULT_USTALENI             30     // +-3.0%
#define DEFAULT_DELKA_USTALENI       3      // 3 vzorky
#define DEFAULT_TYP_AUTO_REZIMU      AUTOREZIM_S_PRIRUSTKEM
#define DEFAULT_JEDNOTKY             JEDNOTKY_KG
#define DEFAULT_ROZSAH_HISTOGRAMU    40     // +-40%, izraelci maji +-45%
#define DEFAULT_ROZSAH_UNIFORMITY    10     // +-10%
#define DEFAULT_GSM_USE              NO
#define DEFAULT_GSM_SEND_MIDNIGHT    NO
#define DEFAULT_GSM_PERIOD_MIDNIGHT  1
#define DEFAULT_GSM_SEND_REQUEST     NO
#define DEFAULT_GSM_EXECUTE_REQUEST  NO
#define DEFAULT_GSM_CHECK_NUMBERS    NO
#define DEFAULT_GSM_NUMBER_COUNT     0

// ---------------------------------------------------------------------------------------------
// Meze hodnot v nastaveni
// ---------------------------------------------------------------------------------------------

#define MIN_NORMOVANA_HMOTNOST      30      // Minimalni pripustna cilova hmotnost, pokud vyjde mensi, koriguju
#define MAX_NORMOVANA_HMOTNOST      0xFFFF  // Maximalni pripustna cilova hmotnost

#define MIN_IDENTIFIKACNI_CISLO 1     // Minimalni hodnota identifikacniho cisla
#define MAX_IDENTIFIKACNI_CISLO 99    // Maximalni hodnota identifikacniho cisla

#define MIN_ROZSAH_HISTOGRAMU 10      // Minimalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu
#define MAX_ROZSAH_HISTOGRAMU 100     // Maximalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu (+-100% dava rozsah 0 az 2xstred)
#define MIN_ROZSAH_UNIFORMITY 1       // Minimalni hodnota rozsahu uniformity v procentech
#define MAX_ROZSAH_UNIFORMITY 49      // Maximalni hodnota rozsahu uniformity v procentech (+-49% dava 98% celkem)

#define MIN_OKOLI_KRIVKY      0       // Minimalni hodnota okoli rustove krivky
#define MAX_OKOLI_KRIVKY      99      // Maximalni hodnota okoli rustove krivky
#define MIN_FILTR             1       // Minimalni hodnota filtru
#define MAX_FILTR             40      // Maximalni hodnota filtru (7.5Hz * 40 = cca 5sec)
#define MIN_USTALENI          0       // Minimalni hodnota ustaleni
#define MAX_USTALENI          250     // Maximalni hodnota ustaleni (+-25.0)
#define MIN_DOBA_USTALENI     2       // Minimalni hodnota doby ustaleni
#define MAX_DOBA_USTALENI     9       // Maximalni hodnota doby ustaleni - musi se rovnat AVERAGE_CAPACITY v hardware.h

#define MIN_GSM_PERIOD_MIDNIGHT 1     // Minimalni hodnota periody vysilani pulnocnich SMS ve dnech
#define MAX_GSM_PERIOD_MIDNIGHT 99    // Maximalni hodnota periody vysilani pulnocnich SMS ve dnech

// ---------------------------------------------------------------------------------------------
// Zobrazeni
// ---------------------------------------------------------------------------------------------

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_NV_HMOTNOST      0x01           // Zobrazi se naslapna vaha po vypocteni nove aktualni hmotnosti
#define ZOBRAZIT_NV_ULOZENO       0x02           // Zobrazi se naslapna vaha po ulozeni nove hmotnosti
#define ZOBRAZIT_1SEC             0x04           // Zobrazi se periodicke veci 1sec (napr. cas)
#define ZOBRAZIT_HISTOGRAM_KURZOR 0x08           // Zobrazi se kurzor v histogramu spolecne s hmotnosti a poctem
#define ZOBRAZIT_LOGGER           0x10           // Zobrazi se stranka se vzorky loggeru
#define ZOBRAZIT_MRIZKU           0x80           // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_VSE              0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit


// ---------------------------------------------------------------------------------------------
// Datum a cas
// ---------------------------------------------------------------------------------------------

// Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
typedef struct {
  byte Sec;
  byte Min;
  byte Hour;
  byte Day;
  byte Month;
  word Year;
} TCas;  // 6 bajtu

// Globalni datum a cas
extern TCas __xdata__ ActualDateTime;

// ---------------------------------------------------------------------------------------------
// Rustova krivka
// ---------------------------------------------------------------------------------------------

#define KRIVKA_MAX_POCET         30          // Max pocet pole s definici rustove krivky
#define KRIVKA_MAX_DEN           999         // Max cislo dne
#define KRIVKA_MAX_HMOTNOST      0xFFFF      // Max hmotnost (aby se vlezla do 2 bajtu)
#define KRIVKA_HMOTNOST_UKONCENI 0           // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)
#define KRIVKA_CISLO_PRVNIHO_DNE 0           // Cislo prvniho dne, od ktereho se zacina krmit (= minimalni cislo dne, ktere lze pri zahajeni vazeni zadat)

#define NormovanaHmotnostVPoradku(Hmotnost) (Hmotnost >= MIN_NORMOVANA_HMOTNOST && Hmotnost <= KRIVKA_MAX_HMOTNOST)

// Format bodu rustove krivky
typedef struct {
  word Den;                            // 0-KRIVKA_MAX_DEN (999)
  word Hmotnost;                       // 0-KRIVKA_MAX_HMOTNOST (0xFFFF)
} TBodKrivky;  // 4 bajty

// Pole krivky
typedef TBodKrivky TBodyKrivky[KRIVKA_MAX_POCET];  // 120 bajtu

//-----------------------------------------------------------------------------
// Hejna
//-----------------------------------------------------------------------------

#define MAX_DELKA_NAZVU_HEJNA     8    // Maximalni delka
#define CISLO_PRAZDNEHO_HEJNA  0xFF    // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane
#define NEVYPLNENY_CAS_OMEZENI 0xFF    // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale
#define MAX_HODINA               23    // Maximalni hodnota hodiny pro omezeni (0-23hod)

// Rozdeleni pohlavi
#define POHLAVI_SAMICE 0               // Pohlavi samice
#define POHLAVI_SAMEC  1               // Pohlavi samec

#define NAZEV_HEJNA_RYCHLE_VAZENI "-       "  // Default nazev hejna pri rychlem vazeni - pozor, musi mit MAX_DELKA_NAZVU_HEJNA znaku

// Zahlavi hejna
typedef struct {
  byte Cislo;                          // Cislo hejna - pokud je hejno definovane, je rovno indexu hejna (0 - 9)
  byte Nazev[MAX_DELKA_NAZVU_HEJNA];   // Nazev hejna (bez ukoncovaci nuly, prazdne znaky se doplni mezerami)
  byte PouzivatKrivky;                 // Zda se maji pouzivat rustove krivky nebo automaticke zjistovani cilove hmotnosti bez pouziti rustovych krivek
                                       // Pokud krivky nepouziva, jsou zadane pocatecni hmotnosti ulozene jako 1. bod ve krivce
  byte RozdelovatPohlavi;              // Flag, zda se ma pouzivat rozliseni na samce a samice
  byte VazitOdHodiny;                  // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno NEVYPLNENY_CAS_OMEZENI, neomezuje se
  byte VazitDoHodiny;                  // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno NEVYPLNENY_CAS_OMEZENI, neomezuje se
} TZahlaviHejna;  // 13 bajtu

// Struktura jednoho hejna
typedef struct {
  TZahlaviHejna Zahlavi;
  TBodyKrivky   RustovaKrivkaSamice;   // Rustova krivka pro samice, pripadne celkova rustova krivka, pokud se rozdeleni na samce a samice nepouziva
  TBodyKrivky   RustovaKrivkaSamci;    // Rustova rivka pro samce, pokud se pouziva
} THejno; // 253 bajtu

#define MAX_POCET_HEJN 10              // Pocet hejn, ktere muze zadat
typedef THejno THejna[MAX_POCET_HEJN]; // Cisla hejn odpovidaji pozici, tj. na pozici 0 je hejno cislo 0, na pozici 1 hejno cislo 1 atd.

// Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
// veci, ktere jsou stale potreba a v prubehu dne se nemeni, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake
// zmene v EEPROM, pri prechodu na dalsi den atd.
// Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu
typedef struct {
  TZahlaviHejna Zahlavi;
  word          CisloDneVykrmu;                                   // Cislo dne od pocatku vykrmu
  word          NormovanaHmotnostSamice, NormovanaHmotnostSamci;  // Normovana hmotnost nactena z rustove krivky pro samice a samce. Pokud se pohlavi nerozdeluje, bere se pro samice.
  TRawValue     HorniMezSamice, DolniMezSamice;                   // Dolni a horni mez hmotnosti z rustove krivky samic, pripadne pro obe pohlavi, pokud se rozdeleni na samce a samice nepouziva
  TRawValue     HorniMezSamci, DolniMezSamci;                     // Dolni a horni mez hmotnosti z rustove krivky samcu
} TVykrmHejna;

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

#define MAX_POCET_PLOSIN 10             // Maximalni pocet plosin

// Kalibrace 1 plosiny
typedef struct {
  int32 KalibraceNuly;                 // Kalibrace nuly
  int32 KalibraceRozsahu;              // Kalibrace rozsahu
  int32 Rozsah;                        // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
  byte Dilek;                           // Dilek vah
} TKalibracePlosiny; // 13 bajtu

// Kalibrace
typedef struct {
  byte PouzitaPlosina;                  // Cislo prave pouzite plosiny
  TKalibracePlosiny Plosiny[MAX_POCET_PLOSIN];
} TKalibrace;   // 131 bajtu

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

// Rychle zadani parametru vykrmu rovnou pri startu, bez pouziti nektereho preddefinovaneho hejna.
// Pri pouziti hejna se tyto parametry berou primo z hejna.
typedef struct {
  byte RozdelovatPohlavi;        // Flag, zda se ma pouzivat rozliseni na samce a samice
  word PocatecniHmotnostSamice;  // Zadana normovana hmotnost pro pocatecni den vykrmu pro samice. Pokud se pohlavi nerozdeluje, bere se pro samice.
  word PocatecniHmotnostSamci;   // Zadana normovana hmotnost pro pocatecni den vykrmu pro samce.
} TRychlyVykrm;  // 5 bajtu

// Parametry, ktere zadal pri zahajeni vykrmu. Tyto parametry se v celem prubehu vazeni nemeni.
// Po skonceni vykrmu se zde drzi vsechny parametry posledniho vykrmu az do doby odstartovani dalsiho vykrmu. Parametry odtut muze tedy vyuzivat
// historie i po ukonceni vykrmu.
typedef struct {
  byte      ProbihaKrmeni;      // Flag, ze prave probiha krmeni
  byte      CekaniNaStart;      // Flag, ze se prave ceka na opozdeny start krmeni. Ostatni polozky TStartVykrmu jsou vyplneny, staci inicializovat archiv atd.
  byte      PouzivatHejno;      // Flag, zda zvolil krmeni podle urciteho hejna. Pokud ne, zadal zakladni parametry primo a vazi se bez pouziti hejna, za pouziti parametru v TRychlyVykrm.
  byte      AktualniHejno;      // Pokud se krmi podle hejna, je zde cislo hejna, podle ktereho se prave krmi. Pri ukonceni vykrmu zde zustane cislo hejna, podle ktereho se krmilo.
  word      PosunutiKrivky;     // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.
  TCas      ZahajeniVykrmu;     // Datum zahajeni vykrmu
  TRychlyVykrm RychlyVykrm;     // Zde jsou parametry vykrmu pri PouzivatHejno=NO
  byte      RozsahUniformity;   // Rozsah uniformity v +- %
  byte      Online;             // Flag, ze probiha online mereni
} TStartVykrmu;  // 19 bajtu

// Parametry GSM
#define GSM_NUMBER_MAX_LENGTH 15        // Maximalni delka telefonniho cisla bez zakoncovaci nuly
#define GSM_NUMBER_MAX_COUNT  5         // Maximalni pocet definovanych telefonnich cisel
typedef struct {
  byte Use;                 // Flag, zda se GSM modul vyuziva (zda je pripojen)
  byte SendMidnight;        // Flag, zda se ma posilat o pulnoci statistika
  byte PeriodMidnight;      // Perioda ve dnech, kdy se ma posilat pulnocni statistika
  byte SendOnRequest;       // Flag, zda se ma odpovidat na zaslane SMS
  byte ExecuteOnRequest;    // Flag, zda se ma provadet cinnost podle zaslanych SMS (zatim nevyuzito)
  byte CheckNumbers;        // Flag, zda se ma pri prijeti prikazu kontrolovat cislo (odpovi se jen na prikazy odeslane z cisla, ktere je v seznamu)
  byte NumberCount;         // Pocet definovanych telefonnich cisel, na ktere se vysila o pulnoci
  byte Numbers[GSM_NUMBER_MAX_COUNT][GSM_NUMBER_MAX_LENGTH];  // Definovana telefonni cisla
} TGsm;  // 82 bajtu

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                          // VERZE
  byte IdentifikacniCislo;             // identifikace zarizeni
  byte Jazyk;                          // Jazykova verze + jazyk

  byte SamiceOkoliNad;                 // Okoli nad prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamiceOkoliPod;                 // Okoli pod prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamciOkoliNad;                  // Okoli nad prumerem pro samce v procentech
  byte SamciOkoliPod;                  // Okoli pod prumerem pro samce v procentech
  byte Filtr;                          // Filtr prevodniku
  byte UstaleniNaslapneVahy;           // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
  byte DelkaUstaleniNaslapneVahy;      // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
  TTypAutoRezimu TypAutoRezimu;        // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)

  TJednotky Jednotky;                  // Zobrazovane jednotky

  byte RozsahHistogramu;               // Rozsah histogramu v +- % stredni hodnoty
  byte RozsahUniformity;               // Rozsah uniformity v +- %

  TStartVykrmu StartVykrmu;

  TGsm Gsm;

  TPodsvit Podsvit;                    // Nastaveny rezim podsvitu
  byte Baterie;                        // Vaha je napajena z baterie - zatim se nepouziva

  byte KontrolniSuma;                  // Kontrolni soucet
} TKonfigurace;  // 119 bajtu, pri provedeni zmen zkontrolovat fci CfgSaveConfigFromModule()

extern TKonfigurace __xdata__ Konfigurace;      // Buffer konfigurace v externi RAM
extern TKonfigurace __xdata__ KonfiguraceModul; // Konfigurace nacena z pametoveho modulu
extern TKalibrace   __xdata__ Kalibrace;        // Buffer kalibrace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast

#define CFG_ALL                          0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERZE                        0x00000001L
#define CFG_IDENTIFIKACNI_CISLO          0x00000002L
#define CFG_SAMICE_OKOLI_NAD             0x00000004L
#define CFG_SAMICE_OKOLI_POD             0x00000008L
#define CFG_SAMCI_OKOLI_NAD              0x00000010L
#define CFG_SAMCI_OKOLI_POD              0x00000020L
#define CFG_FILTR                        0x00000040L
#define CFG_USTALENI_NASLAPNE_VAHY       0x00000080L
#define CFG_DELKA_USTALENI_NASLAPNE_VAHY 0x00000100L
#define CFG_TYP_AUTO_REZIMU              0x00000200L
#define CFG_START_VYKRMU                 0x00000400L
#define CFG_JEDNOTKY                     0x00000800L
#define CFG_ROZSAH_HISTOGRAMU            0x00001000L
#define CFG_ROZSAH_UNIFORMITY            0x00002000L
#define CFG_GSM                          0x00004000L
#define CFG_JAZYK                        0x00008000L
#define CFG_PODSVIT                      0x00010000L

//-----------------------------------------------------------------------------
// Struktura archivu
//-----------------------------------------------------------------------------

// narazniky :

#define FL_ARCHIV_EMPTY        0xFE
#define FL_ARCHIV_FULL         0xFF

typedef struct {
   word       CisloDneVykrmu;          // Cislo dne od pocatku vykrmu
   TCas       Cas;                     // datum a cas porizeni
   word       VcerejsiPrumerSamice;    // Prumerna vcerejsi hmotnost samic (pro vypocet daily gain)
   word       VcerejsiPrumerSamci;     // Prumerna vcerejsi hmotnost samcu
   word       NormovanaHmotnostSamice; // Normovana hmotnost samic pro tento den
   word       NormovanaHmotnostSamci;  // Normovana hmotnost samcu pro tento den
   byte       PouzitaRealnaUniformita; // YES/NO, zda se pouzil presny vypocet uniformity, ktery je v promennych RealnUniformitaSamice a RealnUniformitaSamci
   byte       RealnaUniformitaSamice;  // Uniformita samic vypoctena presne z jednotlivych vzorku
   byte       RealnaUniformitaSamci;   // Uniformita samcu vypoctena presne z jednotlivych vzorku
} TArchiv;        // 211 bajtu - pozor, pri prekroceni 255 bajtu predefinovat promennou v SaveContext() a LoadContext()

//-----------------------------------------------------------------------------
// Zaznam vzorku
//-----------------------------------------------------------------------------

// Naraznikove znaky :

#define LG_SAMPLE_EMPTY        0x7F     // nenaplnene FIFO
#define LG_SAMPLE_FULL         0xFF     // naplnene FIFO
#define LG_SAMPLE_VALUE        0x20     // zaznam hodnoty
#define LG_SAMPLE_GENDER       0x10     // bit pohlavi
#define LG_SAMPLE_WEIGHT_MSB   0x01     // 17. bit hmotnosti ve flagu

#define LG_SAMPLE_MASK_WEIGHT    0x01FFFF  // Hmotnost je ulozena ve spodnich 17bit, MSB byte je flag (hmotnost tedy zabira 1 spodni bit z flagu)
#define LG_SAMPLE_MASK_HI_WEIGHT 0x010000  // 17. bit hmotnosti
#define LG_SAMPLE_MASK_HOUR      0x001F    // Hodina je ulozena ve spodnich 5 bitech

typedef struct {
   byte  Flag;        // naraznik
   word  Value;       // vaha nebo hodina (vaha je na 17 bitu)
} TLoggerSample;      // 3 bajty

// zaznam jednotlivych vazeni :
#define FL_LOGGER_SAMPLES      1801L
#define FL_LOGGER_SAMPLE_SIZE  sizeof( TLoggerSample)                       // 3 bajty

// struktura dne (jen pro pro Builder a vypocet velikosti) :
typedef struct {
   TArchiv       Archiv;                                                    // 211  bytu
   TLoggerSample Samples[ FL_LOGGER_SAMPLES];                               // 5403 bytu
} TArchivDailyInfo;   // 5614 bytu

//-----------------------------------------------------------------------------
// Online ukladani
//-----------------------------------------------------------------------------

// Naraznikove znaky :
#define ON_SAMPLE_EMPTY        0x7F     // nenaplnene FIFO
#define ON_SAMPLE_FULL         0xFF     // naplnene FIFO
#define ON_SAMPLE_WEIGHT       0x08     // zaznam hmotnosti
#define ON_SAMPLE_SAVED        0x20     // Vaha by prave ulozila vzorek do pameti
#define ON_SAMPLE_STABLE       0x10     // Bit ustalene hmotnosti
#define ON_SAMPLE_SIGN         0x04     // Znamenko hmotnosti
#define ON_SAMPLE_WEIGHT_MSB   0x03     // 17. a 18. bit hmotnosti ve flagu

#define ON_SAMPLE_MASK_WEIGHT    0x03FFFF  // Hmotnost je ulozena ve spodnich 18bit, MSB byte je flag (hmotnost tedy zabira 2 spodni bity z flagu)
#define ON_SAMPLE_MASK_HOUR      0x001F    // Hodina je ulozena ve spodnich 5 bitech

typedef struct {
   byte  Flag;        // Naraznik
   word  Value;       // Hmotnost nebo hodina
} TOnlineSample;      // 3 bajty

// zaznam jednotlivych vazeni :
#define FL_ONLINE_SAMPLES      694265L
#define FL_ONLINE_SAMPLE_SIZE  sizeof(TOnlineSample)                       // 3 bajty

//-----------------------------------------------------------------------------
// Struktura FLASH
//-----------------------------------------------------------------------------

// rozdeleni pameti na sekce
// U AT45BD161B mam k dispozici 4096 x 512 = 2 097 152 bajtu

// konfiguracni sekce :
#define FL_CONFIG_BASE         0L
#define FL_CONFIG_SIZE         4096L

// archiv statistik & histogramu :
#define FL_ARCHIV_BASE         (FL_CONFIG_BASE + FL_CONFIG_SIZE)
#define FL_ARCHIV_DAY_SIZE     sizeof( TArchivDailyInfo)                    // velikost polozky
#define FL_ARCHIV_DAYS         371L                                         // pocet polozek
#define FL_ARCHIV_SIZE         (FL_ARCHIV_DAY_SIZE * FL_ARCHIV_DAYS)        // celkem bytu (2082794), konec pameti 2086890

// Online mereni :
#define FL_ONLINE_BASE         FL_ARCHIV_BASE                               // Stejne jako archiv
#define FL_ONLINE_SIZE         (FL_ONLINE_SAMPLE_SIZE * FL_ONLINE_SAMPLES)  // celkem bytu (2082795), konec pameti 2086891

// struktura konfiguracni sekce :
typedef struct {
  TKonfigurace Konfigurace;            // Konfigurace
  THejna       Hejna;                  // Definice jednotlivych hejn
  TKalibrace   Kalibrace;              // Kalibrace zkopirovana z interni EEPROM
} TConfigSection; // 2780 bajtu

//-----------------------------------------------------------------------------
// Struktura interni EEPROM
//-----------------------------------------------------------------------------


#define IEP_SPARE (IEP_SIZE - sizeof(TKalibrace))

typedef struct {
  TKalibrace    Kalibrace;             // Kalibrace vah
  byte          Volne[IEP_SPARE];
} TIntEEPROM;


//-----------------------------------------------------------------------------
#endif
