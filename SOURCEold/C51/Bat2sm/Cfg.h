//*****************************************************************************
//
//    Cfg.h - Configuration load/save
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Cfg_H__
   #define __Cfg_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Vyuziva se i pri cteni konfigurace z modulu
#define CFG_FL_START   (FL_CONFIG_BASE + offsetof(TConfigSection, Config))

void CfgCopyIdWithoutSpaces( byte __xdata__ *s);
// Kopie identifikacniho cisla bez uvodnich mezer

TYesNo CfgLoad(void);
// Nacte konfiguraci z Flash
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)

TYesNo CfgSave(void);
// Ulozi cely config do Flash
// Vraci NO, nepovedl-li se zapis

byte CfgCalcChecksum(void);
// Spocita a vrati Kontrolni soucet konfigurace

void CfgReadItem(byte offset, byte size);
// Nacte frament konfigurace z <offset> velikosti <size>

#endif
