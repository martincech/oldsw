//*****************************************************************************
//
//    GsmDiag.c - Diagnostika GSM
//    Version 1.0, (c) P.Veit
//
//*****************************************************************************

#ifndef __GsmDiag_H__
   #define __GsmDiag_H__


// Umisteni prvku:
#define GSM_DIAG_BASE_Y         10
#define READ_TEXT_Y             (GSM_DIAG_BASE_Y + 20)



void GsmDiagStatus(void);
// Vykresli stav GSM modulu

void GsmDiagSmsReadDebug(void);
// Prekresleni stavu cteni SMS

void GsmDiagSmsReadText(void);
// Prekresleni textu nactene SMS

void GsmDiagSmsSendDebug(void);
// Prekresleni stavu poslani SMS

void GsmDiagConfig(void);
// Vykresli config GSM

#endif
