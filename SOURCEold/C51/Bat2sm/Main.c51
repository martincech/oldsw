//*****************************************************************************
//
//    Main.c51 -  BAT2 for Sodalec with Megavi communication
//    Verze 1.00  P.Veit & VymOs
//
//*****************************************************************************


/*

Verze 1.50.0 SM - vychazi ze standardni verze pro Sodalec 1.50.0 S
  - 10.3.2011: Zcela odsstranen ConfigModule, usetreno hodne bajtu v XDATA. Kopirovani configu z pametoveho modulu se dela jinak.
  - 10.3.2011: V GsmCtl.c51 vynechana fce sprintf, nahrazeno vlastni fci.
  - 9.4.2011: Zjednodusena fce CfgSave(), nyni bez masky

Verze 1.50.1 SM
  - 10.11.2011: Request s textem "MEGAVI" a "MEGAVI X" se posila do Megavi ke zpracovani. MBridge zajistuje vse kolem.
  - 10.11.2011: Do MBridge se po resetu posila adresa RS-485 a vsechna pulnocni telefonni cisla. Tim se MBridge namoduje
                a zacne komunikovat. Zaroven se zajisti beh vice vah na jedne lince.
  - 10.11.2011: V menu je pristupne zadani adresy RS-485 i ve verzi GSM.
  - 10.11.2011: Default adresa RS-485 je 10.

Verze 1.50.2 SM
  - 16.11.2011: Default adresa RS-485 je nyni 1, rozsah 1-15.

Verze 1.50.3 SM
  - 22.12.2011: Pokud se vazi bez rozliseni pohlavi, posilam u statistiky samcu statistiku samic misto dosavadnich nul.
                Domluveno s Maximem.

Verze 1.50.4 SM
  - 9.5.2012: Ve statistice se posila take vcerejsi prumer.

Verze 1.50.5 SM
  - 27.6.2012: Podpora GSM modulu Sagem HiLoNC. Preneseno ze standardni verze BAT2, kde uz tento modul funguje.

 */

#include "Hardware.h"
#include "Bat2.h"
#include "..\inc\System.h"     // "operacni system" - kvuli SysDelay()
#include "Accu.h"              // Lowbat
#include "KbdCtl.h"            // Obsluha klavesnice
#include "Menu.h"              // Uzivatelske rozhrani
#include "Flock.h"             // Editovani hejna
#include "Scale.h"             // Naslapne vahy
#include "Archive.h"           // Ukladani hodnot
#include "GsmCtl.h"            // Rizeni GSM
#include "Date.h"              // Date/time computing
#include "Cfg.h"               // Konfigurace
#include "Cal.h"               // Kalibrace
#include "Date.h"              // Date/time computing
#include "BLight.h"            // Podsvit displeje
#include "TestFifo.h"          // Testovani archivu
#include "Dacs.h"              // Komunikace s Dacs ACS
#include "SpiBat.h"            // Rucni prepnuti na interni Falsh (je nutne po prechodu na xdata v SpiBat)
#include "SlideAvg.h"          // Sliding average pro Big Dutchman
#include "GsmMid.h"            // Posilani pulnocni SMS
#include "MegExe.h"            // Megavi control

#include "..\inc\System.h"     // "operacni system"
#include "..\inc\Kbd.h"        // klavesnice

#include "..\inc\Ds17287.h"    // RTC
#include "..\inc\bcd.h"        // BCD aritmetika
#include "..\inc\AtFlash.h"    // Flash memory
#include "..\inc\El12864.h"    // Displej, kvuli cyklickemu rozinani displeje

#ifdef VERSION_MODBUS
  #include "..\inc\MBCom.h"      // RS232
  #include "..\inc\MB.h"         // MB
  #include "MBReg.h"             // Register map
#endif // VERSION_MODBUS

//#define __DEBUG__
//#define __TEST_COMPONENTS__     // Behem inicializace se vypisuje, ktera soucastka se prave inicializuje
//#define __FAST_TIME__           // Zrychleni chodu casu pro testy: preskoceni z 0:00 -> 0:59:50

#if defined(__DEBUG__) || defined(__TEST_COMPONENTS__)
  #include "..\inc\Display.h"
  #include "..\inc\Font.h"
#endif

// Globalni datum a cas
TLongDateTime __xdata__ ActualDateTime;

// Odpocet necinnosti :
#define USER_EVENT_TIMEOUT      250             // Uzivatel neobsluhuje dele nez ... s - nastavil jsem to schvalne na 4 minuty, tj. co nejdelsi

#define SetTimer1Sec()          SecondCounter = 1000 / TIMER0_PERIOD    // Novy cyklus odpocitavani 1 sekundy

// Obsluha blikani :
volatile byte    BlinkCounter = BLINK_ON;       // Pocitadlo, ktere urcuje periodu blikani
volatile TYesNo  BlinkShow    = YES;            // Zda se ma prave zobrazit nebo skryt
volatile TYesNo  BlinkChange  = NO;             // Flag, ze se zmenil stav BlinkShow a ma se tedy roznout nebo zhasnout => ma se zmenit stav

// Promenne pro timer 1 sekundy
static volatile byte   SecondCounter    = 0;    // Odpocitavani 1s
static volatile TYesNo SecondTick       = NO;   // Priznak odpocitani 1s
//static volatile TYesNo UseIdleTimeout   = YES;  // Zda mam pocitat necinnost klavesnice nebo ne
static byte   __xdata__ UseIdleTimeout;           // Zda mam pocitat necinnost klavesnice nebo ne, inicializace XDATA musi byt v kodu
static byte   __xdata__ _Timeout;                 // Pocitani necinnosti, inicializace XDATA musi byt v kodu

// Promenne pro tick timeru
static volatile TYesNo Timer0Tick       = NO;   // Tick timeru 0, nastavuje preruseni, nuluje se softwarove

// Globalni promenne
TUn UN;

// Nastaveni jazyka
static byte __xdata__  ChangeLanguage;          // Flag, ze se prave nastavuje jazyk a cinnost vahy je tak omezena

#ifdef VERSION_GSM      // Pouze pokud pouziva GSM
  static byte __xdata__ GsmTimer;               // Timer pro kontrolu GSM modulu
#endif // VERSION_GSM

static byte __xdata__ MegaviTimer;              // Timer pro kontrolu Megavi
static byte __xdata__ WaitForNewDay;            // Flag, ze behem vypnuti vahy doslo ke zmene data a mam pozastavit vazeni,



//-----------------------------------------------------------------------------
// Lokalni funkce:
//-----------------------------------------------------------------------------

static void CheckScale(void);
// Obsluha naslapnych vah

static void CheckStartWeighing(void);
// Pokud se ceka na start vazeni, zkontroluje, zda uz se nema zahajit vazeni
// Znici UN

static void CheckVoltage(void);
// Kontrola napajeciho napeti, v pripade vypadku ceka, dokud se neobnovi

static void ReadDateTime(void);
// Nactu aktualni datum a cas v binarni podobe do globalni promenne

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

void SysDelay(word ms) {
  // Zpozdeni v milisekundach pro krystal 18.432MHz
  byte m;                                                          // m = R5, n = R6+R7
                                                                   // T = 1.085 mikrosec.
  while (ms > 0) {             // DELAY: SETB C/MOV A,R7/SUBB A,#0/MOV A,R6/SUBB A,#0/JC KONEC *7T*
    WDTRST=0x1E;
    WDTRST=0xE1;
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    // Pro X2 mod musim 2x tolik!!
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    --ms;                          // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*
  }                                                                             // SJMP DELAY *2T*
} // SysDelay

//-----------------------------------------------------------------------------
// Prepnuti kontextu na "operacni system" viz System.h
//-----------------------------------------------------------------------------

byte SysYield(void) {
  // Prepnuti kontextu na operacni system. Vraci udalost
  byte Key;

  WatchDog();                   // Zahybu watchdogem
  CheckVoltage();               // Kontrola napajeciho napeti

  // Tick timeru 0
  if (Timer0Tick) {
    Timer0Tick = NO;            // Shodim flag

#ifdef VERSION_DACS_LW1
    DacsWaitTick();             // Odpocet cekani
#endif // VERSION_DACS_LW1

  }//if

#ifdef VERSION_DACS_LW1
  DacsExecute();                // Provadeni prikazu, volat co nejcasteji
#endif // VERSION_DACS_LW1

#ifdef VERSION_MODBUS
  MBExecute();                  // Provadeni prikazu MODBUS, volat co nejcasteji
#endif // VERSION_MODBUS

  if (!ChangeLanguage) {
    // Pokud nastavuje jazyk, cinnost vahy je omezena
    // Casovac periodicke cinnosti :
    if (SecondTick) {
      // Ubehla 1 sekunda
      SecondTick = NO;          // Shodim flag
      SetTimer1Sec();           // Novy cyklus odpocitavani 1 sekundy
      if (UseIdleTimeout) {
        _Timeout++;             // Pocitam necinnost
      } else {
        _Timeout = 0;           // Nepocitam necinnost, stale nuluju
      }
      //--------- periodicka cinnost 1s :
      DisplayOn();
      ReadDateTime();
      if (!DateEqual(&Archive.DateTime, &ActualDateTime)) {
        // Zmena data - to musim testovat pred zmenou casu, na zacatku noveho dne to tedy zavola jen fci ArchiveNewDay(), fci ArchiveNewHour() uz ne.
        WaitForNewDay = NO;             // Kazdopadne shodim flag
        if (FlockWeighingRunning()) {
          ArchiveFinishDay();           // ulozeni statistik a zakonceni dne

#ifdef VERSION_GSM      // Pouze pokud pouziva GSM
          GsmPrepareMidnightSms();      // Z aktualnich dat pripravim pulnocni SMS k odeslani
#endif // VERSION_GSM

          ArchiveNewDay();              // nulovani statistik a zalozeni noveho dne

          FlockUpdateDay();             // vypocet noveho dne vykrmu
          ArchiveUpdate();              // Ulozim vypoctene hodnoty do archivu
          ArchiveNewHour();             // Ulozim i nultou hodinu na pocatku dne (kvuli online, ale i u loggeru to je dobre)

#ifdef VERSION_DACS_LW1
          DacsBat2Midnight();           // Aktualizace dat pro Dacs, volat az po FlockUpdateDay()
#endif // VERSION_DACS_LW1

#ifdef VERSION_BIGDUTCHMAN
          SlideAvgMidnight();
#endif // VERSION_BIGDUTCHMAN

        }
        // Pokud se nekrmi a dojde ke zmene data, Archive.DateTime se neobnovuje a proto to bude porad prekreslovat cely displej. Zatim to necham tak.
        Display = DISPLAY_ALL;          // Kazdopadne prekreslim novy den, i kdyz se nekrmi
      } else if (Archive.DateTime.Hour != ActualDateTime.Hour) {
        // Zmena hodiny
        if (FlockWeighingRunning()) {
          ArchiveNewHour();             // ulozeni zmeny casu

#ifdef VERSION_DACS_LW1
          DacsNewHour();                // Aktualizace dat pro Dacs - ArchiveNewHour() prepocital se histogram
#endif // VERSION_DACS_LW1

#ifdef VERSION_BIGDUTCHMAN
          SlideAvgNewHour();
#endif // VERSION_BIGDUTCHMAN

          Display = DISPLAY_ALL;        // prekresli - prepocital se histogram
        }//if
      }//else

      CheckStartWeighing();             // Hlidam opozdeny start vazeni

#ifdef __FAST_TIME__
      // Zrychleni chodu casu pro testy: preskoceni z 0:00 -> 0:59:50
      if (ActualDateTime.Min == 0) {
        CmdDateTime = ActualDateTime;
        CmdDateTime.Min = 59;
        DateSet();                    // Vyuziva globalni strukturu CmdDateTime
        RtcSetSec( 0x50);         // Zrychlim i sekundy
      }
#endif // __FAST_TIME__

      // Komunikace s Megavi
      if (--MegaviTimer == 0) {
        MegaviTimer = MEGAVI_TIMER;
        MegaviExecute();
      }

#ifdef VERSION_GSM      // Pouze pokud pouziva GSM
      if (--GsmTimer == 0) {
        GsmTimer = GSM_TIMER;
        if (FlockWeighingRunning()) {
          GsmCheckMidnightSending();      // Pokud je pripravena pulnocni SMS k odeslani, zkontroluju, zda uz ji nemam odeslat
        }
        GsmExecute();           // cteni a provadeni SMS povelu
      }
#endif // VERSION_GSM

      BacklightExecute();       // Ovladani podsvitu
      Display |= DISPLAY_1SEC;  // Zobrazeni po 1sec
    }//if

    // Obsluha naslapnych vah
    CheckScale();
  }//if

  // Casovac blikani
  if (BlinkChange) {
    // Uplynul cas blikani - bud mam skryt nebo zobrazit
    BlinkChange = NO;          // Shodim flag
    if (BlinkShow) {
      return K_BLINK_ON;
    } else {
      return K_BLINK_OFF;
    }
  }//if

  // Testovani necinnosti
  if (_Timeout > USER_EVENT_TIMEOUT) {
    _Timeout = 0;              // Zahajime dalsi cekani
    return K_TIMEOUT;
  }

  // Cteni klavesy
  Key = KbdGet();
  if (Key != K_IDLE) {
    _Timeout = 0;               // Konec necinnosti
    BacklightAutoOn();          // Rozne automaticky podsvit
    return Key;
  }

  // Kdyz mam co zobrazit, poslu prikaz k prekresleni
  if (Display) {
    return K_REDRAW;
  }

  // Pokud dosel az sem, neprobehla zadna udalost
  return K_IDLE;
} // SysYield

//-----------------------------------------------------------------------------
// Cekani na udalost viz System.h
//-----------------------------------------------------------------------------

byte SysWaitEvent(void) {
  // Cekani na udalost
  byte Key;

  while(1) {
    Key = SysYield();
    if (Key != K_IDLE) {
      return Key;               // Neprazdna udalost
    }
  }//while
} // SysWaitEvent

/*
//-----------------------------------------------------------------------------
// Nulovani timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysFlushTimeout(void) {
  // Nuluje timeout klavesnice
  _Timeout = 0;
  // nastavit kontext v modulu Kbd, aby nezustal vysilat treba K_REPEAT
} // SysFlushTimeout
*/

//-----------------------------------------------------------------------------
// Zakaz timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysDisableTimeout(void) {
  // Zakaze timeout klavesnice
  _Timeout       = 0;
  UseIdleTimeout = NO;
} // SysDisableTimeout

//-----------------------------------------------------------------------------
// Nulovani timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysEnableTimeout(void) {
  // Povoli timeout klavesnice
  _Timeout       = 0;
  UseIdleTimeout = YES;
} // SysEnableTimeout

//-----------------------------------------------------------------------------
// Nastaveni timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysSetTimeout(void) {
  // Nastavi timeout klavesnice
  _Timeout = USER_EVENT_TIMEOUT + 1;
} // SysSetTimeout

// ---------------------------------------------------------------------------------------------
// Preruseni casovace
// ---------------------------------------------------------------------------------------------

static void Timer0Handler() interrupt INT_TIMER0 {
  // Prerusovaci rutina casovace 0
  SetTimer0(TIMER0_PERIOD);             // S touto periodou
  Timer0Tick = YES;                     // Nastavim flag

  // casovac blikani :
  BlinkCounter--;
  if (BlinkCounter == 0) {
     // docitali jsme, nastav na novou hodnotu
     BlinkChange = YES;                 // nastav priznak
     if (BlinkShow) {
       BlinkCounter = BLINK_OFF;        // bylo roznuto,  perioda pro zhasni
     } else {
       BlinkCounter = BLINK_ON;         // bylo zhasnuto, perioda pro rozsvit
     }
     BlinkShow = !BlinkShow;            // novy smer citani
  }//if
  // vykonne funkce :
  CheckTrigger( SecondCounter, SecondTick = YES);    // casovac 1 s

  KbdTrigger();                         // casovac klavesnice
} // Timer0Handler

// ---------------------------------------------------------------------------------------------
// Preruseni casovace
// ---------------------------------------------------------------------------------------------

#ifdef VERSION_MODBUS

static void Timer1Handler() interrupt INT_TIMER1 {
  // Prerusovaci rutina casovace 1 - MODBUS potrebuje jemne krokovani po 1ms
  SetTimer1(TIMER1_PERIOD);             // S touto periodou
#ifdef COM_TX_SPACE
  if( ComStatus == COM_STATUS_TX_RUNNING){
    CheckTrigger( ComTimeoutCounter, ComStartTx());      // zpozdeny start Tx
  } else {
    CheckTrigger( ComTimeoutCounter, ComTimeoutDone());  // Rx timeout COMu
  }
#else
  CheckTrigger( ComTimeoutCounter, ComTimeoutDone());    // Rx timeout COMu
#endif
} // Timer1Handler

#endif // VERSION_MODBUS

// ---------------------------------------------------------------------------------------------
// Testovani pinu
// ---------------------------------------------------------------------------------------------

#define HybejPinem(Pin) \
  EA = 0;               \
  while (!EA) {         \
    Pin = 1;            \
    Delay(200);         \
    Pin = 0;            \
    Delay(100);         \
  }                     \

// ---------------------------------------------------------------------------------------------
// Main
// ---------------------------------------------------------------------------------------------

#define X_INIT_TEXT 0
#define Y_INIT_TEXT 56

#ifdef __TEST_COMPONENTS__
  #define InitText(Text) DisplaySetFont(FONT_LUCIDA6); DisplaySetArea(X_INIT_TEXT, Y_INIT_TEXT, X_INIT_TEXT + 30, Y_INIT_TEXT + 7, 0); DisplayString(X_INIT_TEXT, Y_INIT_TEXT, Text)
#else
  #define InitText(Text)
#endif

void main(void) {
  BatCS1 = 0;                   // deselect displeje
  BatCS2 = 0;
  BacklightOn();                // zapni podsvit

  TimingSetup();                // Nastaveni X2 modu
  EnableXRAM();                 // Povoleni pristupu k vnitrni XRAM
  WDTPRG = WDTPRG_2090;         // start Watch Dogu
  WatchDog();

#if defined(VERSION_DACS_LW1) || defined(VERSION_MODBUS)
  // 16.6.2009: U vah pro DACS se stavalo, ze po resetu byl sice port v H, ale week pullup v procesoru
  // nestacil pretahnout PUM do H. Zmenou z L do H se aktivuje strong pullup, ktery uz PUM pretahne.
  // Po zapnuti se bohuzel na okamzik aktivuje vysilac RS485, coz rusi linku, ale je to jedine vhodne
  // reseni. Pridavat na desku externi odpor je blbost.
  ComTxEnable();
  ComTxDisable();
#endif // VERSION_DACS_LW1 || VERSION_MODBUS

//  HybejPinem(MEGAVI_RE);

  // Inicializace globalnich promennych v XDATA
  UseIdleTimeout  = YES;
  _Timeout        = 0;

  SetTimer1Sec();               // Nastartuju odpocitavani sekundy

  // Inicializace vsech modulu
  Delay(300);
  if (PowerFail()) {
    AccuWaitForVoltage();       // Pockam, dokud nedojde k vzrustu napajeni
  }

#ifdef VERSION_GSM      // Pouze pokud pouziva GSM
  GsmTimer = GSM_TIMER;         // Inicializace xdata promenne, kvuli blbnuti Keilu
#endif // VERSION_GSM

  EnableInts();                 // Povoleni preruseni
  Timer0Run(TIMER0_PERIOD);     // Spust casovac 0

#ifdef VERSION_MODBUS
  Timer1Run(TIMER1_PERIOD);     // Spust casovac 1
#endif // VERSION_MODBUS

  InitText("KBD");
  KbdInit();

  InitText("RTC");
  RtcInit();                    // inicializace RTC

  ReadDateTime();               // Nactu aktualni datum a cas, aby byl okamzite k dispozici
  RtcSec();                     // Kvuli linkeru, aby negeneroval warning. Sekundy se normalne nepouzivaji

  InitText("FLASH");
  SpiInternalFlash();           // 25.9.2007: Musim rucne
  FlashInit();
  SpiRelease();                 // Deselect SPI & disable SPI controller
  InitText("CFG");
  CfgLoad();                    // Nacteni konfigurace z interni EEPROM
  InitText("CAL");
  CalLoad();                    // Kalibraci mam zvlast

  InitText("MENU");
  MenuInit();                   // inicializace zobrazeni (az za CfgLoad)
  BacklightInit();              // Inicializace podsvitu

  InitText("MEGAVI");
  MegaviExecuteInit();
  MegaviTimer = MEGAVI_TIMER;

#ifdef VERSION_GSM       // Pouze pokud pouziva GSM
  InitText("GSM");
  GsmMidnightInit();
  GsmInit();                    // inicializace GSM komunikace
  GsmStart();                   // Nahozeni napajeni GSM modulu
#endif // VERSION_GSM

  WatchDog();                   // eliminuj timeout

  InitText("ARCHIV");
  ArchiveInit();                // Inicializace archivu
  WaitForNewDay = !DateEqual(&Archive.DateTime, &ActualDateTime);  // Pokud doslo behem vypnute vahy k pulnoci, pozastavim vazeni
  InitText("PARAM");
  FlockReadWeighingParameters();     // POZOR, musi byt az za ArchiveInit, aby se v pripade automatickeho vazeni spravne nacetla prumerna hmotnost ze vcerejska
  InitText("ADC");
  ScaleInit();                  // Inicializace A/D a spusteni prevodu
  InitText("OK");

#ifdef VERSION_DACS_LW1
  InitText("DACS");
  DacsInit();                   // Volat az po ArchiveInit() a az jako posledni inicializovanou komponentu - rovnou zapina prijem
#endif // VERSION_DACS_LW1

#ifdef VERSION_MODBUS
  InitText("MODBUS");
  MBInitialize();
  MBRegInit();
#endif // VERSION_MODBUS

#ifdef VERSION_BIGDUTCHMAN
  InitText("SLIDEAVG");
  SlideAvgInit();
#endif // VERSION_BIGDUTCHMAN

  Delay(1000);

  // Po resetu otestuju, zda nechce zmenit jazyk
  if (KbdPowerUpKey() == K_ENTER) {
    KbdBeepKey();               // Indikace, ze muze klavesu pustit
    KbdPowerUpRelease();        // Cekam na pusteni
    ChangeLanguage = YES;       // Nastavim flag, ze se nastavuje jazyk a vaha nema provadet zadnou cinnost
    MenuLanguage();
  }
  ChangeLanguage = NO;          // Nenastavuje se jazyk, normalni cinnost

  // Cyklicka cinnost
  while(1) {
    switch(SysWaitEvent()) {

      case K_ENTER:
        // Vratim default font i mod fontu
        KbdBeepKey();
        MenuExecute();          // Rozjedu menu
        KbdDiscardKey();
        MenuDisplayWeighing(DISPLAY_ALL);       // Normalni cinnost vahy
        break;

      case K_RIGHT:
        // Prejdu na dalsi zobrazeni
        if (!FlockWeighingRunning()) {
          break;                // Zobrazeni menim jen kdyz se krmi
        }
        KbdBeepKey();
        MenuNextDisplay();
        MenuDisplayWeighing(DISPLAY_ALL);
        break;

      case K_LEFT:
        // Zmenim zobrazovane pohlavi
        if (!FlockWeighingRunning()) {
          break;                // Zobrazeni menim jen kdyz se krmi
        }
        if (!Weighing.Header.UseBothGenders) {
          break;                // Behem krmeni se nerozpoznava pohlavi
        }
        if (DisplayMode == DISPLAY_MODE_STATISTICS || DisplayMode == DISPLAY_MODE_HISTOGRAM) {
          // Pohlavi menim jen u statistiky a histogramu
          KbdBeepKey();
          MenuChangeGender();
          MenuDisplayWeighing(DISPLAY_ALL);
        }//if
        break;

      case K_UP:
      case K_UP | K_REPEAT:
        // Pokud je zobrazen histogram, posunu kurzor vpravo
        // Pokud je zobrazen logger, posunu se na dalsi stranu
        switch (DisplayMode) {

          case DISPLAY_MODE_HISTOGRAM:
            KbdBeepKey();
            MenuHistogramMoveRight();
            MenuDisplayWeighing(DISPLAY_HISTOGRAM_CURSOR);
            break;

          case DISPLAY_MODE_LOGGER:
            KbdBeepKey();
            MenuLoggerNextPage();
            MenuDisplayWeighing(DISPLAY_LOGGER);
            break;

        }//switch
        break;

      case K_DOWN:
      case K_DOWN | K_REPEAT:
        // Pokud je zobrazen histogram, posunu kurzor vlevo
        // Pokud je zobrazen logger, posunu se na predchozi stranu
        switch (DisplayMode) {

          case DISPLAY_MODE_HISTOGRAM:
            KbdBeepKey();
            MenuHistogramMoveLeft();
            MenuDisplayWeighing(DISPLAY_HISTOGRAM_CURSOR);
            break;

          case DISPLAY_MODE_LOGGER:
            KbdBeepKey();
            MenuLoggerPreviousPage();
            MenuDisplayWeighing(DISPLAY_LOGGER);
            break;

        }//switch
        break;

      case K_ESC:
        // Pokusy
#ifdef __DEBUG__
        KbdBeepKey();
        TestFifoExecute();
        Display = DISPLAY_ALL;
        DisplayClear();
#endif  // __DEBUG__
        break;

      case K_REDRAW:
        MenuDisplayWeighing(Display);       // Normalni cinnost vahy
        break;

    }//switch
  }//while
} // main

//-----------------------------------------------------------------------------
// Obsluha naslapnych vah
//-----------------------------------------------------------------------------

static void CheckScale(void) {
  // Obsluha naslapnych vah
  if (PowerFail() || WaitForNewDay) {
    return;
  }
  if (ScaleExecute()) {
    // Zvazil se novy vzorek, ulozim do archivu
    ArchiveNewWeight(Scale.LastSavedWeight, Scale.LastSavedGender);

#ifdef VERSION_DACS_LW1
    DacsNewWeight();                    // Zkopiruje archiv aktualniho dne pro komunikaci s Dacs ACS
#endif // VERSION_DACS_LW1

#ifdef VERSION_BIGDUTCHMAN
    SlideAvgNewWeight();
#endif // VERSION_BIGDUTCHMAN

    if (Config.WeighingStart.Online) {
      // Pokud se meri online, ulozim do online loggeru
      ArchiveNewActualWeight(Scale.Weight, YES, Scale.Stable);   // Jde o ulozenou hmotnost
    }
    Display |= DISPLAY_SAVED_WEIGHT;    // Zobrazim ulozenou hmotnost
    return;
  }//if
  if (FlockWeighingRunning() && Config.WeighingStart.Online && Scale.NewWeight) {
    ArchiveNewActualWeight(Scale.Weight, NO, Scale.Stable);
  }
}

//-----------------------------------------------------------------------------
// Cekani na start vazeni
//-----------------------------------------------------------------------------

static void CheckStartWeighing(void) {
  // Pokud se ceka na start vazeni, zkontroluje, zda uz se nema zahajit vazeni
  // Znici UN
  TUn StartUN;

  if (!FlockWeighingWaiting()) {
    return;                     // Neceka se
  }
  // Datum zahajeni
  StartUN.Word.Msb = Config.WeighingStart.DateTime.Year;
  StartUN.ST.X3    = Config.WeighingStart.DateTime.Month;
  StartUN.ST.X4    = Config.WeighingStart.DateTime.Day;
  // Aktualni datum
  UN.Word.Msb      = ActualDateTime.Year;
  UN.ST.X3         = ActualDateTime.Month;
  UN.ST.X4         = ActualDateTime.Day;
  // Porovnam datumy
  if (StartUN.DT > UN.DT) {
    return;                     // Jeste jsem nedosel na den startu
  }
  if (StartUN.DT == UN.DT) {
    // Jsem prave presne na dni startu => musim porovnat jeste cas
    StartUN.ST.X3 = Config.WeighingStart.DateTime.Hour;
    StartUN.ST.X4 = Config.WeighingStart.DateTime.Min;
    UN.ST.X3      = ActualDateTime.Hour;
    UN.ST.X4      = ActualDateTime.Min;
    if (StartUN.Word.Lsb > UN.Word.Lsb) {
      return;
    }// else cas je bud stejny nebo uz vetsi
  }// else aktualni datum je vetsi
  // Zahajim vazeni, vsechny parametry vazeni jsou uz ulozene v Config.WeighingStart
  Config.WeighingStart.WaitingForStart = NO;    // Zrusim cekani
  Config.WeighingStart.Running         = YES;
  CfgSave();                                    // Ulozim start krmeni
  FlockExecuteStartWeighing();                  // Provedu start vazeni
  DisplayMode = DISPLAY_MODE_WEIGHING;          // Zobrazuju vazeni
  Display = DISPLAY_ALL;                        // Prekreslim
}

//-----------------------------------------------------------------------------
// Kontrola napajeni
//-----------------------------------------------------------------------------

static void CheckVoltage(void) {
  // Kontrola napajeciho napeti, v pripade vypadku ceka, dokud se neobnovi
  if (BatAccuOk()) {
    return;                     // Napajeni je v poradku
  }

  // Objevil se lowbat
  BacklightOff();               // Usetrim energii

#ifdef VERSION_GSM      // Pouze pokud pouziva GSM
  GsmStop();
#endif // VERSION_GSM

//  BeepKey();  // !!! Pro testy
  // Cekam, dokud se napajeni nespravi
  MenuDisplayLowBat();
  AccuWaitForVoltage();
  // Prave se obnovilo napajeni
  // Podsvit nerozinam, zajisti se to automaticky podle nastaveneho rezimu

#ifdef VERSION_GSM      // Pouze pokud pouziva GSM
  GsmStart();
#endif // VERSION_GSM

  Display = DISPLAY_ALL;        // Vse prekreslim
  SysSetTimeout();              // Pokud je v menu, vypadnu
}

//-----------------------------------------------------------------------------
// Nacteni data a casu
//-----------------------------------------------------------------------------

static void ReadDateTime(void) {
  // Nactu aktualni datum a cas v binarni podobe do globalni promenne
  ActualDateTime.Day   = bbcd2bin(RtcDay());
  ActualDateTime.Month = bbcd2bin(RtcMonth());
  ActualDateTime.Year  = wbcd2bin(RtcYear());
  ActualDateTime.Hour  = bbcd2bin(RtcHour());
  ActualDateTime.Min   = bbcd2bin(RtcMin());
}

