//*****************************************************************************
//
//    Date.c - Prace s datem
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#include "Date.h"

#include "..\inc\bcd.h"        // BCD aritmetika
#include "..\inc\Ds17287.h"    // RTC

TLongDateTime __xdata__ CmdDateTime;            // Pro nastaveni casu

// Pocet dnu v mesici. Prechodny rok resim zvlast
byte code DAYS_IN_MONTH[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};   // Schvalne 13 prvku, abych to mohl volat rovnou s cislem mesice

// ---------------------------------------------------------------------------------------------
// Staticke funkce:
// ---------------------------------------------------------------------------------------------

static word DaysFromBeginningOfYear(TLongDateTime __xdata__ *Date);
// Vypocte pocet celych dnu od pocatku roku do zadaneho data. Nezapocitavam aktualni den, pouze cele, jiz ubehnute dny.

static word DaysTillEndOfYear(TLongDateTime __xdata__ *Date);
// Vypocte pocet celych dnu od zadaneho data do konce roku. Pokud je v puli dne, zapocitava se i aktualni den, na rozdil od fce DateDaysFromBeginningOfYear().

// ---------------------------------------------------------------------------------------------
// Test na prechodny rok
// ---------------------------------------------------------------------------------------------

#define LeapYear(Year)   (Year % 4 == 0)
// Prechodny rok nastava (viz. www.timeanddate.com/date/leapyear.html):
//   1. Every year divisible by 4 is a leap year.
//   2. But every year divisible by 100 is NOT a leap year
//   3. Unless the year is also divisible by 400, then it is still a leap year.
// This means that year 1800, 1900, 2100, 2200, 2300 and 2500 are NOT leap years, while year 2000 and 2400 are leap years.

// Jednoduche testovani bodu 1 je dostatecne, k prvni chybe dojde az v roce 2100.

// ---------------------------------------------------------------------------------------------
// Porovnani 2 datumu
// ---------------------------------------------------------------------------------------------

TYesNo DateEqual( TLongDateTime __xdata__ *Date1, TLongDateTime __xdata__ *Date2) {
  // vraci YES, jsou-li data shodna
  return( Date1->Day    == Date2->Day    &&
          Date1->Month  == Date2->Month  &&
          Date1->Year   == Date2->Year);
} // DateEqual

// ---------------------------------------------------------------------------------------------
// Pocet dnu mezi datumy
// ---------------------------------------------------------------------------------------------

word DateDaysBetweenTwoDates(TLongDateTime __xdata__ *StartDate, TLongDateTime __xdata__ *EndDate) {
  // Vypocte pocet celych dnu mezi dvema datumy. Bere se pocet celych dnu, tj. nebere se v uvahu cas.
  word Difference;
  word Year;

  // Nejprve zkontroluju, zda je pocatecni datum pred koncovym datem - aby mne to nejak neslo do minusu a nedavalo blbosti
  // Jdu postupne od roku az ke dni
  if (EndDate->Year < StartDate->Year) {
    return 0;
  }
  if (EndDate->Year == StartDate->Year) {
    // Pokud jsou oba datumy ve stejnem roce, musim testovat mesic
    if (EndDate->Month < StartDate->Month) {
      return 0;
    }
    if (EndDate->Month == StartDate->Month) {
      // Pokud jsou oba datumy ve stejnem roce i mesici, musim testovat jeste den
      if (EndDate->Day <= StartDate->Day) {
        // Sem dam i rovno (tj. datumy jsou stejne), i kdyz bych mohl jit dal a korektne by mne to vypocitalo nulu, ale je to zbytecne, stejne by vysla nula.
        return 0;
      }//if
    }//if
  } else {
    // Datumy jsou z formalniho hlediska v poradku, ale do word se mne nevleze rozdil vetsi jak 65535 dnu, tj. 179 let. Pokud je rozdil
    // roku vetsi jak 179, nemuzu pocitat dal, vratilo by mne to blbost
    if (EndDate->Year - StartDate->Year > 179) {
      return 0;
    }
  }//else
  // Pokud dosel az sem, datumy se daji odecitat, koncovy datum je az za pocatecnim datem
  if (EndDate->Year == StartDate->Year) {
    // Pokud se roky rovnaji, je rozdil mene nez 1 rok a pocitam to jinak
    Difference = LeapYear(StartDate->Year) ? 366 : 365;         // Prechodny nebo normalni rok
    Difference -= DaysFromBeginningOfYear(StartDate) + DaysTillEndOfYear(EndDate);
  } else {
    // Roky se nerovnaji
    // Prictu zbytek dnu do konce pocatecniho roku
    Difference = DaysTillEndOfYear(StartDate);
    // Prictu cele roky mezi pocatecnim a koncovym rokem
    for (Year = StartDate->Year + 1; Year < EndDate->Year; Year++) {
      Difference += LeapYear(Year) ? 366 : 365;                 // Prechodny nebo normalni rok
    }//for
    // Prictu pocet dnu od zacatku koncoveho roku do koncoveho data
    Difference += DaysFromBeginningOfYear(EndDate);
  }//else
  return Difference;
}

// ---------------------------------------------------------------------------------------------
// Nastaveni datumu a casu
// ---------------------------------------------------------------------------------------------

void DateSet(void) {
  // Nastavi datum a cas ulozeny ve strukture CmdDateTime
  RtcSetDay(  bbin2bcd(CmdDateTime.Day));
  RtcSetMonth(bbin2bcd(CmdDateTime.Month));
  RtcSetYear( wbin2bcd(CmdDateTime.Year));
  RtcSetHour( bbin2bcd(CmdDateTime.Hour));
  RtcSetMin(  bbin2bcd(CmdDateTime.Min));
  RtcSetSec(  0);         // Sekundy jen nuluju
}

// ---------------------------------------------------------------------------------------------
// Pocet dnu od pocatku roku
// ---------------------------------------------------------------------------------------------

static word DaysFromBeginningOfYear(TLongDateTime __xdata__ *Date) {
  // Vypocte pocet celych dnu od pocatku roku do zadaneho data. Nezapocitavam aktualni den, pouze cele, jiz ubehnute dny.
  byte Month;
  word Days;                    // Muze byt az 365

  // Prictu pocet ubehnutych dnu z aktualniho mesice
  Days = Date->Day - 1;         // Pouze cele dny, bez aktualniho dne, proto minus 1
  // Pokud ubehl vic jak 1 mesic, prictu pocty dnu za ubehnute cele mesice
  if (Date->Month > 1) {
    for (Month = 1; Month <= Date->Month - 1; Month++) {
      Days += DAYS_IN_MONTH[Month];
      if (Month == 2 && LeapYear(Date->Year)) {
        Days++;                 // V prechodnem roce ma unor 29 dni misto 28 ulozenych v poli DAYS_IN_MONTH[]
      }
    }//for
  }//if
  return Days;
}

// ---------------------------------------------------------------------------------------------
// Pocet dnu do konce roku
// ---------------------------------------------------------------------------------------------

static word DaysTillEndOfYear(TLongDateTime __xdata__ *Date) {
  // Vypocte pocet celych dnu od zadaneho data do konce roku. Pokud je v puli dne, zapocitava se i aktualni den, na rozdil od fce DateDaysFromBeginningOfYear().
  if (LeapYear(Date->Year)) {
    return 366 - DaysFromBeginningOfYear(Date);         // Prechodny rok
  } else {
    return 365 - DaysFromBeginningOfYear(Date);         // Normalni rok
  }//else
}
