//*****************************************************************************
//
//    MegExe.c - Megavi execution
//    Version 1.0
//
//*****************************************************************************

#ifndef __MegExe_H__
   #define __MegExe_H__

#include "Hardware.h"     // zakladni datove typy

#define MEGAVI_TIMER              5    // Zakladni interval exekutivy (kontrola SMS)
#define MEGAVI_DATA_UPDATE        30   // Interval aktualizace dat v Megavi

// Konfiguracni data pro MBridge :
#define MEGAVI_REPLY_TIMEOUT      5    // Timeout odpovedi na 'MEGAVI x' SMS [min]
#define MEGAVI_BROADCAST_TIMEOUT  30   // Timeout SMS broadcastu na vsechna cisla [min]
#define MEGAVI_DEVICE_TIMEOUT     60   // Timeout odpovedi MEGAVI zarizeni na 'MEGAVI x' vyzvu [s]

void MegaviExecuteInit(void);
// Inicializace modulu

void MegaviRestart( void);
// Restart komunikace, zaslani nove konfigurace

void MegaviExecute(void);
// Exekutiva MEGAVI, volat v intervalu MEGAVI_TIMER

TYesNo MegaviSmsReceived( void);
// Odesle SMS zpravu nactenou do GsmBuffer, GsmNumber do MEGAVI

TYesNo MegaviBridgeOk( void);
// Vrati YES, je-li komunikace s Bat2Bridge v poradku

#endif
