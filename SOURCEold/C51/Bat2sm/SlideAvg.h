//*****************************************************************************
//
//    SlideAvg.c - Sliding average pro Big Dutchman
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#ifndef __SlideAvg_H__
   #define __SlideAvg_H__

#include "Hardware.h"          // zakladni datove typy
#include "Bat2.h"              // Gender

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Pouzivam pouze jeden seznam hodinovych prumeru s tim, ze hodnoty pred aktualni hodinou jsou dnesni prumery
// a hodnoty za aktualni hodinou vcetne jsou vcerejsi prumery. Hodiny behem vazeni nelze zmenit a mohou plynout
// pouze dopredu, takze tento mechanismus funguje. Aktualni hodinu mam v extra promenne CurrentAverage.

// Pozor, algoritmus je kvuli nedostatku pameti velmi zjednoduseny. Pokud se vaha na par hodin vypne, v hodinovych
// statistikach zustanou prumery z pred dvema dny, ne ze vcerejska. Dalsi den bude prirustek v techto hodinach vypocten
// spatne. Algoritmus funguje pouze pokud se vaha behem vazeni nevypne (muze se vypnout pouze kratce, mene nez 1 hodinu).

typedef struct {
  byte CurrentHour;                         // Aktualni rozpracovana hodina
  word CurrentAverage[_GENDER_COUNT];       // Aktualni rozpracovany prumer
  word HourAverage[24][_GENDER_COUNT];      // Prumerne hmotnosti pro jednotlive hodiny, obsahuje pomichane hodnoty ze vcerejska i dneska
} TSlideAvg;  // 101 bajtu
extern TSlideAvg __xdata__ SlideAvg;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void SlideAvgInit(void);
  // Inicializace, volat az po inicializaci archivu

void SlideAvgNewWeight(void);
  // Zkopiruje archiv aktualniho dne do SlideAvg, volat po zvazeni noveho vzorku

void SlideAvgNewHour(void);
  // Ubehla hodina, volat kazdou celou hodinu

void SlideAvgMidnight(void);
  // Ubehla pulnoc, volat o pulnoci, az po FlockUpdateDay()

void SlideAvgStartWeighing(void);
  // Volat po zahajeni vazeni

word SlideAvgGetLastAverage(TArchive __xdata__ *DisplayedArchive, TGender DisplayedGender);
  // Z archivu <DisplayedArchive> zjisti denni prirustek pro pohlavi <DisplayedGender>


#endif // __SlideAvg_H__
