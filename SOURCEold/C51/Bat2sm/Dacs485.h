//*****************************************************************************
//
//    Dacs485.c - low-level komunikace s ACS pres RS-485
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#ifndef __Dacs485_H__
   #define __Dacs485_H__

#include "Hardware.h"               // zakladni datove typy
#include "Bat2.h"                   // Nastaveni rychlosti a parity

// ---------------------------------------------------------------------------------------------
// Definice
// ---------------------------------------------------------------------------------------------

#define START_OF_PACKET         0x01                            // Zacatek celeho paketu
#define END_OF_PACKET           0x05                            // Konec celeho paketu
#define START_OF_TEXT           0x02                            // Zacatek obsahu paketu
#define END_OF_TEXT             0x03                            // Konec obsahu paketu
#define END_OF_LRC              0x04                            // Znak za LRC

#define COM_BUFFER_SIZE         127                             // Velikost bufferu seriove linky (definovano p. Wagnerem)

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

extern volatile byte __xdata__  ComBuffer[COM_BUFFER_SIZE];     // Buffer seriove linky
extern volatile byte            ComBufferCount;                 // Pocet znaku v ComBuffer[]

//---------------------------------------------------------------------------------------------
// Funkce
//---------------------------------------------------------------------------------------------

void Dacs485Init(void);
// Inicializace seriove linky, vola se i pri zmene parametru seriove linky

void Dacs485InitReceiver(void);
// Inicializace prijmu paketu

void Dacs485Disable(void);
// Zakaz cinnosti

TYesNo Dacs485ReadPacket(void);
// Pokud je v ComBuffer[] nacteny cely paket, vrati YES.

void Dacs485SendPacket(void);
// Zahaji vysilani paketu v ComBuffer[], ktery obsahuje ComBufferCount znaku


#endif // __Dacs485_H__
