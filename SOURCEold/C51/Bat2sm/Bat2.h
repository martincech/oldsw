//*****************************************************************************
//
//    Bat2.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Bat2_H__
   #define __Bat2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#include "..\inc\Stat.h"
#include "..\inc\Histogr.h"

#ifdef __C51__
  // Jen pro Keil
  #include <stddef.h>        // makro offsetof
  #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#else
  // Jen Builder
  typedef int   int32;
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

// POZOR, upravovat i cislo verze v VERSION_STRING_MODBUS v hardware.h
#define VERSION                     0x0150      // verze FW
#define BUILD                       5           // Cislo buildu (pro zmene buildu se nemeni vlastnosti vahy a neprovadi se CfgReset)

#define SCALE_CAPACITY              99999       // Nosnost vah
#define SCALE_DECIMALS              3           // Pocet desetin
#define MAX_WEIGHT_CALIBRATION      200000      // Maximalni hmotnost zadavana pri kalibraci
#define MAX_WEIGHT_DISPLAY          200000      // Maximalni zobrazitelna hmotnost
#define MIN_WEIGHT_DISPLAY          -99999      // Minimalni zobrazitelna hmotnost

typedef enum {
  UNITS_KG,
  UNITS_LB,
  _UNITS_COUNT
} TUnits;

typedef enum {
  BACKLIGHT_OFF,
  BACKLIGHT_ON,
  BACKLIGHT_AUTO,
  _BACKLIGHT_COUNT
} TBacklight;

// Typ automatickeho zjistovani cilove hmotnosti
typedef enum {
  AUTOMODE_WITHOUT_GAIN,    // Cilova hmotnost se vypocte jako prumer ze vcerejska
  AUTOMODE_WITH_GAIN,       // Cilova hmotnost se vypocte jako prumer + denni prirustek ze vcerejska
  _AUTOMODE_COUNT
} TAutoMode;

// Typ naskoku/seskoku na vahu
typedef enum {
  JUMPMODE_ENTER,           // Vyhodnocuje se pouze naskok na vahu
  JUMPMODE_LEAVE,           // Vyhodnocuje se pouze seskok z vahy
  JUMPMODE_BOTH,            // Vyhodnocuje se naskok i seskok z vahy
  _JUMPMODE_COUNT
} TJumpMode;

// Rychlost komunikace RS-485 - pouziva se pouze pri editaci v menu
typedef enum {
  RS485_SPEED_1200,
  RS485_SPEED_2400,
  RS485_SPEED_4800,
  RS485_SPEED_9600,
  RS485_SPEED_19200,
  RS485_SPEED_38400,
  RS485_SPEED_57600,
  RS485_SPEED_115200,
  _RS485_SPEED_COUNT
} TSpeed;

// Parita pri komunikaci RS-485, POZOR, musi odpovidat parite definovane v MODBUS (alespon prvni mody)
typedef enum {
  RS485_PARITY_NONE,        // Zadna parita (paritni bit se nevysila)
  RS485_PARITY_EVEN,        // Suda parita
  RS485_PARITY_ODD,         // Licha parita
  RS485_PARITY_MARK,        // Vysilaji se 2 stop bity
  _RS485_PARITY_COUNT
} TParity;

// Komunikacni protokol
typedef enum {
  RS485_PROTOCOL_MODBUS_RTU,
  RS485_PROTOCOL_MODBUS_ASCII,
  _RS485_PROTOCOL_COUNT
} TProtocol;

// Hardwarova verze
typedef enum {
  HW_VERSION_GSM,           // GSM modul
  HW_VERSION_DACS_LW1,      // Emulace vahy LW1 pripojene k Dacs ACS pres RS-485
  HW_VERSION_MODBUS,        // Modbus pres RS-485
  _HW_VERSION_COUNT
} THwVersion;

// ---------------------------------------------------------------------------------------------
// Default hodnoty nastaveni (cte i PC)
// ---------------------------------------------------------------------------------------------

#define DEFAULT_FEMALES_MARGIN_ABOVE    30
#define DEFAULT_FEMALES_MARGIN_BELOW    20     // Sodalec chce default +30% a -20%
#define DEFAULT_MALES_MARGIN_ABOVE      30
#define DEFAULT_MALES_MARGIN_BELOW      20     // Sodalec chce default +30% a -20%
#define DEFAULT_FILTER                  12     // 1.5sec (7.5Hz * 12)
#define DEFAULT_STABILIZATION_RANGE     30     // +-3.0%
#define DEFAULT_STABILIZATION_TIME      3      // 3 vzorky
#define DEFAULT_AUTO_MODE               AUTOMODE_WITH_GAIN
#define DEFAULT_JUMP_MODE               JUMPMODE_BOTH
#define DEFAULT_UNITS                   UNITS_KG
#define DEFAULT_HISTOGRAM_RANGE         40     // +-40%, izraelci maji +-45%
#define DEFAULT_UNIFORMITY_RANGE        10     // +-10%
#define DEFAULT_GSM_USE                 NO
#define DEFAULT_GSM_SEND_MIDNIGHT       NO
#define DEFAULT_GSM_PERIOD_MIDNIGHT     1
#define DEFAULT_GSM_DAY_MIDNIGHT        999
#define DEFAULT_GSM_SEND_HOUR_MIDNIGHT  0       // Default posilam o pulnoci
#define DEFAULT_GSM_SEND_MIN_MIDNIGHT   0
#define DEFAULT_GSM_SEND_REQUEST        NO
#define DEFAULT_GSM_CHECK_NUMBERS       NO
#define DEFAULT_GSM_NUMBER_COUNT        0
#define DEFAULT_RS485_ADDRESS           1
#define DEFAULT_RS485_SPEED             9600
#define DEFAULT_RS485_PARITY            RS485_PARITY_EVEN
#define DEFAULT_RS485_REPLY_DELAY       0      // Pri pouziti prevodniku RS-485/USB ELO E211 neni treba, funguje i pri nule
#define DEFAULT_RS485_SILENT_INTERVAL   50     // Nejnizsi hodnota v PC je cca 50ms
#define DEFAULT_RS485_PROTOCOL          RS485_PROTOCOL_MODBUS_RTU

// ---------------------------------------------------------------------------------------------
// Meze hodnot v nastaveni
// ---------------------------------------------------------------------------------------------

#define MIN_TARGET_WEIGHT           30      // Minimalni pripustna cilova hmotnost, pokud vyjde mensi, koriguju
#define MAX_TARGET_WEIGHT           0xFFFF  // Maximalni pripustna cilova hmotnost

#define MIN_HISTOGRAM_RANGE         10      // Minimalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu
#define MAX_HISTOGRAM_RANGE         100     // Maximalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu (+-100% dava rozsah 0 az 2xstred)
#define MIN_UNIFORMITY_RANGE        1       // Minimalni hodnota rozsahu uniformity v procentech
#define MAX_UNIFORMITY_RANGE        49      // Maximalni hodnota rozsahu uniformity v procentech (+-49% dava 98% celkem)

#define MIN_MARGIN                  0       // Minimalni hodnota okoli rustove krivky
#define MAX_MARGIN                  99      // Maximalni hodnota okoli rustove krivky
#define MIN_FILTER                  1       // Minimalni hodnota filtru
#define MAX_FILTER                  40      // Maximalni hodnota filtru (7.5Hz * 40 = cca 5sec)
#define MIN_STABILIZATION_RANGE     0       // Minimalni hodnota ustaleni
#define MAX_STABILIZATION_RANGE     250     // Maximalni hodnota ustaleni (+-25.0)
#define MIN_STABILIZATION_TIME      2       // Minimalni hodnota doby ustaleni
#define MAX_STABILIZATION_TIME      9       // Maximalni hodnota doby ustaleni - musi se rovnat AVERAGE_CAPACITY v hardware.h

#define MIN_GSM_PERIOD_MIDNIGHT     1       // Minimalni hodnota periody vysilani pulnocnich SMS ve dnech
#define MAX_GSM_PERIOD_MIDNIGHT     99      // Maximalni hodnota periody vysilani pulnocnich SMS ve dnech

#define MIN_RS485_ADDRESS           1       // Minimalni hodnota adresy
#define MAX_RS485_ADDRESS           15      // Maximalni hodnota adresy
#define MIN_RS485_REPLY_DELAY       0       // Minimalni hodnota zpozdeni odpovedi
#define MAX_RS485_REPLY_DELAY       9999    // Maximalni hodnota zpozdeni odpovedi
#define MIN_RS485_SILENT_INTERVAL   1       // Minimalni hodnota prodlevy mezi dvema MODBUS pakety
#define MAX_RS485_SILENT_INTERVAL   255     // Maximalni hodnota prodlevy mezi dvema MODBUS pakety

// ---------------------------------------------------------------------------------------------
// Zobrazeni
// ---------------------------------------------------------------------------------------------

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Display
#define DISPLAY_CURRENT_WEIGHT      0x01           // Zobrazi se naslapna vaha po vypocteni nove aktualni hmotnosti
#define DISPLAY_SAVED_WEIGHT        0x02           // Zobrazi se naslapna vaha po ulozeni nove hmotnosti
#define DISPLAY_1SEC                0x04           // Zobrazi se periodicke veci 1sec (napr. cas)
#define DISPLAY_HISTOGRAM_CURSOR    0x08           // Zobrazi se kurzor v histogramu spolecne s hmotnosti a poctem
#define DISPLAY_LOGGER              0x10           // Zobrazi se stranka se vzorky loggeru
#define DISPLAY_GRID                0x80           // Zobrazi se mrizka (kostra)
#define DISPLAY_ALL                 0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

extern byte __xdata__ Display;      // Co mam v nasledujicim kroku prekreslit


// ---------------------------------------------------------------------------------------------
// Datum a cas
// ---------------------------------------------------------------------------------------------

// Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
typedef struct {
  byte Min;
  byte Hour;
  byte Day;
  byte Month;
  word Year;
} TLongDateTime;  // 6 bajtu

// Globalni datum a cas
extern TLongDateTime __xdata__ ActualDateTime;

// ---------------------------------------------------------------------------------------------
// Rustova krivka
// ---------------------------------------------------------------------------------------------

#define CURVE_MAX_POINTS        30          // Max pocet bodu s definici rustove krivky
#define CURVE_MAX_DAY           999         // Max cislo dne
#define CURVE_MAX_WEIGHT        0xFFFF      // Max hmotnost (aby se vlezla do 2 bajtu)
#define CURVE_END_WEIGHT        0           // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)
#define CURVE_FIRST_DAY_NUMBER  0           // Cislo prvniho dne, od ktereho se zacina krmit (= minimalni cislo dne, ktere lze pri zahajeni vazeni zadat)

#define TargetWeightOK(Weight) (Weight >= MIN_TARGET_WEIGHT && Weight <= CURVE_MAX_WEIGHT)

// Format bodu rustove krivky
typedef struct {
  word Day;         // 0..CURVE_MAX_DAY (999)
  word Weight;      // 0..CURVE_MAX_WEIGHT (0xFFFF)
} TCurvePoint;  // 4 bajty

// Pole 1 krivky
typedef TCurvePoint TCurvePoints[CURVE_MAX_POINTS];  // 120 bajtu

//-----------------------------------------------------------------------------
// Hejna
//-----------------------------------------------------------------------------

#define FLOCK_NAME_MAX_LENGTH       8           // Maximalni delka
#define FLOCK_EMPTY_NUMBER          0xFF        // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane
#define FLOCK_TIME_LIMIT_EMPTY      0xFF        // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale
#define FLOCK_TIME_LIMIT_MAX_HOUR   23          // Maximalni hodnota hodiny pro omezeni (0-23hod)

// Rozdeleni pohlavi
typedef enum {
  GENDER_FEMALE = 0,                            // Pohlavi samice (pokud se pouziva jen 1 pohlavi, jsou vzdy platne parametry pro samice)
  GENDER_MALE,                                  // Pohlavi samec
  _GENDER_COUNT
} TGender;

#define QUICK_WEIGHING_TITLE        "-       "  // Default nazev hejna pri rychlem vazeni - pozor, musi mit FLOCK_NAME_MAX_LENGTH znaku

// Zahlavi hejna
typedef struct {
  byte Number;                                  // Cislo hejna - pokud je hejno definovane, je rovno indexu hejna (0 - 9)
  byte Title[FLOCK_NAME_MAX_LENGTH];            // Nazev hejna (bez ukoncovaci nuly, prazdne znaky se doplni mezerami)
  byte UseCurves;                               // Zda se maji pouzivat rustove krivky nebo automaticke zjistovani cilove hmotnosti bez pouziti rustovych krivek
                                                // Pokud krivky nepouziva, jsou zadane pocatecni hmotnosti ulozene jako 1. bod ve krivce
  byte UseBothGenders;                          // Flag, zda se ma pouzivat rozliseni na samce a samice
  byte WeighFrom;                               // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se
  byte WeighTill;                               // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno FLOCK_TIME_LIMIT_EMPTY, neomezuje se
} TFlockHeader;  // 13 bajtu

// Struktura jednoho hejna
typedef struct {
  TFlockHeader  Header;
  TCurvePoints  GrowthCurve[_GENDER_COUNT];     // Rustova krivka pro samice (pripadne oboje) a pro samce
} TFlock; // 253 bajtu

#define FLOCKS_MAX_COUNT            10          // Pocet hejn, ktere muze zadat
typedef TFlock TFlocks[FLOCKS_MAX_COUNT];       // Cisla hejn odpovidaji pozici, tj. na pozici 0 je hejno cislo 0, na pozici 1 hejno cislo 1 atd.

// Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
// veci, ktere jsou stale potreba a v prubehu dne se nemeni, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake
// zmene v EEPROM, pri prechodu na dalsi den atd.
// Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu
typedef struct {
  TFlockHeader  Header;
  word          DayNumber;                      // Cislo dne od pocatku vykrmu
  word          TargetWeight[_GENDER_COUNT];    // Normovana hmotnost nactena z rustove krivky pro samice a samce
  TRawValue     MarginAbove[_GENDER_COUNT];     // Horni mez hmotnosti samic a samcu
  TRawValue     MarginBelow[_GENDER_COUNT];     // Dolni mez hmotnosti samic a samcu
  word          ComparisonWeight[_GENDER_COUNT];// Hmotnost pro porovnani nactena z rustove porovnavaci krivky pro samice a samce
} TWeighing;  // 273 bajtu

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

typedef struct {
  int32     ZeroCalibration;                    // Kalibrace nuly
  int32     RangeCalibration;                   // Kalibrace rozsahu
  int32     Range;                              // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
  byte      Division;                           // Dilek vah
} TCalibration; // 13 bajtu

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

// Rychle zadani parametru vykrmu rovnou pri startu, bez pouziti nektereho preddefinovaneho hejna.
// Pri pouziti hejna se tyto parametry berou primo z hejna.
typedef struct {
  byte UseBothGenders;                  // Flag, zda se ma pouzivat rozliseni na samce a samice
  word InitialWeight[_GENDER_COUNT];    // Zadana normovana hmotnost pro pocatecni den vykrmu pro samice a samce
} TQuickWeighing;  // 5 bajtu

// Parametry, ktere zadal pri zahajeni vykrmu. Tyto parametry se v celem prubehu vazeni nemeni.
// Po skonceni vykrmu se zde drzi vsechny parametry posledniho vykrmu az do doby odstartovani dalsiho vykrmu. Parametry odtut muze tedy vyuzivat
// historie i po ukonceni vykrmu.
typedef struct {
  byte           Running;            // Flag, ze prave probiha krmeni
  byte           WaitingForStart;    // Flag, ze se prave ceka na opozdeny start krmeni. Ostatni polozky TWeighingStart jsou vyplneny, staci inicializovat archiv atd.
  byte           UseFlock;           // Flag, zda zvolil krmeni podle urciteho hejna. Pokud ne, zadal zakladni parametry primo a vazi se bez pouziti hejna, za pouziti parametru v TRychlyVykrm.
  byte           CurrentFlock;       // Pokud se krmi podle hejna, je zde cislo hejna, podle ktereho se prave krmi. Pri ukonceni vykrmu zde zustane cislo hejna, podle ktereho se krmilo.
  word           CurveDayShift;      // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.
  TLongDateTime  DateTime;           // Datum zahajeni vykrmu
  TQuickWeighing QuickWeighing;      // Zde jsou parametry vykrmu pri PouzivatHejno=NO
  byte           UniformityRange;    // Rozsah uniformity v +- %
  byte           Online;             // Flag, ze probiha online mereni
} TWeighingStart;  // 19 bajtu

// Parametry GSM
#define GSM_NUMBER_MAX_LENGTH 15        // Maximalni delka telefonniho cisla bez zakoncovaci nuly, pozor, musi byt mensi nez GSM_MAX_NUMBER v Hardware.h
#define GSM_NUMBER_MAX_COUNT  5         // Maximalni pocet definovanych telefonnich cisel
typedef struct {
  byte Use;                 // Flag, zda se GSM modul vyuziva (zda je pripojen)
  byte SendMidnight;        // Flag, zda se ma posilat o pulnoci statistika
  byte PeriodMidnight1;     // Perioda ve dnech, kdy se ma posilat pulnocni statistika do dne DayMidnight1
  word DayMidnight1;        // Do tohoto dne vcetne se posila s periodou PeriodMidnight1
  byte PeriodMidnight2;     // Perioda posilani po dni DayMidnight1
  byte MidnightSendHour;    // Hodina, pri ktere se ma poslat pulnocni SMS
  byte MidnightSendMin;     // Minuta, pri ktere se ma poslat pulnocni SMS
  byte SendOnRequest;       // Flag, zda se ma odpovidat na zaslane SMS
  byte CheckNumbers;        // Flag, zda se ma pri prijeti prikazu kontrolovat cislo (odpovi se jen na prikazy odeslane z cisla, ktere je v seznamu)
  byte NumberCount;         // Pocet definovanych telefonnich cisel, na ktere se vysila o pulnoci
  byte Numbers[GSM_NUMBER_MAX_COUNT][GSM_NUMBER_MAX_LENGTH];  // Definovana telefonni cisla
} TGsm;  // 86 bajtu

// Parametry RS-485
typedef struct {
  byte      Address;        // Adresa vahy v siti RS-485
  dword     Speed;          // Rychlost komunikace v Baudech za sekundu
  TParity   Parity;         // Typ parity
  word      ReplyDelay;     // Zpozdeni mezi prijmem prikazu a odpovedi v milisekundach, kvantovani s TIMER0_PERIOD
  byte      SilentInterval; // Mezera mezi dvema pakety MODBUS v milisekundach
  TProtocol Protocol;       // Typ komunikacniho protokolu
} TRs485;  // 10 bajtu

// Korekcni krivka
typedef struct {
  word Day1;                // Den prvniho zlomu, do tohoto dne je korekce nulova
  word Day2;                // Den druheho zlomu, v tomto dni a dale se uplatni zadana korekce <Correction>
  byte Correction;          // Korekce v desetinach procenta (max. 25.5%) v den Day2. Pokud je hodnota 0, korekce se neuplatnuje.
} TWeightCorrection; // 5 bajtu

#define CONFIG_RESERVED_SIZE    5         // Pocet rezervovanych bajtu v TConfig
#define ID_MAX_LENGTH           10        // Maximalni delka identifikacniho cisla bez zakoncovaci nuly

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word              Version;                        // VERSION
  byte              Build;                          // BUILD
  THwVersion        HwVersion;                      // Verze hardware (GSM, Dacs LW1, Modbus, ...)
  byte              IdentificationNumber[ID_MAX_LENGTH];  // identifikace zarizeni v textovem formatu, stejne jako napr. GSM cisla. Pri zmene delsky zkontrolovat GsmStr[] v Gsmctl.c51.
  byte              Language;                       // Jazykova verze + jazyk

  byte              MarginAbove[_GENDER_COUNT];     // Okoli nad prumerem pro samice a samce
  byte              MarginBelow[_GENDER_COUNT];     // Okoli pod prumerem pro samice a samce
  byte              Filter;                         // Filtr prevodniku
  byte              StabilizationRange;             // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
  byte              StabilizationTime;              // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
  TAutoMode         AutoMode;                       // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)
  TJumpMode         JumpMode;                       // Typ naskoku/seskoku na vahu, ktery se vyhodnocuje

  TUnits            Units;                          // Zobrazovane jednotky

  byte              HistogramRange;                 // Rozsah histogramu v +- % stredni hodnoty
  byte              UniformityRange;                // Rozsah uniformity v +- %

  TWeighingStart    WeighingStart;                  // Parametry vazeni

  TGsm              Gsm;                            // Parametry GSM modulu

  TBacklight        Backlight;                      // Nastaveny rezim podsvitu

  TRs485            Rs485;                          // Parametry linky RS-485

  byte              ComparisonFlock;                // Cislo hejna, se kterym se porovnavaji vysledky, pripadne FLOCK_EMPTY_NUMBER pokud se neporovnava

  TWeightCorrection WeightCorrection;               // Korekce hmotnosti

  byte              Reserved[CONFIG_RESERVED_SIZE]; // Pro budouci pouziti

  byte              Checksum;                       // Kontrolni soucet
} TConfig;  // 155 bajtu, pri provedeni zmen zkontrolovat fci CfgSaveConfigFromModule()

extern TConfig      __xdata__ Config;           // Buffer konfigurace v externi RAM
extern TCalibration __xdata__ Calibration;      // Buffer kalibrace v externi RAM

//-----------------------------------------------------------------------------
// Struktura archivu
//-----------------------------------------------------------------------------

// narazniky :

#define FL_ARCHIVE_EMPTY    0xFE
#define FL_ARCHIVE_FULL     0xFF

typedef struct {
   word          DayNumber;                     // Cislo dne od pocatku vykrmu
   TLongDateTime DateTime;                      // datum a cas porizeni
   TStatistic    Stat[_GENDER_COUNT];           // statistika samic a samcu
   THistogram    Hist[_GENDER_COUNT];           // histogram samic a samcu
   word          LastAverage[_GENDER_COUNT];    // Prumerna vcerejsi hmotnost samic a samcu (pro vypocet daily gain)
   word          TargetWeight[_GENDER_COUNT];   // Normovana hmotnost samic a samcu pro tento den
   byte          RealUniformityUsed;            // YES/NO, zda se pouzil presny vypocet uniformity, ktery je v promennych RealnUniformitaSamice a RealnUniformitaSamci
   byte          RealUniformity[_GENDER_COUNT]; // Uniformita samic a samcu vypoctena presne z jednotlivych vzorku
} TArchive;        // 211 bajtu - pozor, pri prekroceni 255 bajtu predefinovat promennou v SaveContext() a LoadContext()

//-----------------------------------------------------------------------------
// Zaznam vzorku
//-----------------------------------------------------------------------------

// Naraznikove znaky :

#define LG_SAMPLE_EMPTY             0x7F        // nenaplnene FIFO
#define LG_SAMPLE_FULL              0xFF        // naplnene FIFO
#define LG_SAMPLE_VALUE             0x20        // zaznam hodnoty
#define LG_SAMPLE_GENDER            0x10        // bit pohlavi
#define LG_SAMPLE_WEIGHT_MSB        0x01        // 17. bit hmotnosti ve flagu

#define LG_SAMPLE_MASK_WEIGHT       0x01FFFF    // Hmotnost je ulozena ve spodnich 17bit, MSB byte je flag (hmotnost tedy zabira 1 spodni bit z flagu)
#define LG_SAMPLE_MASK_HI_WEIGHT    0x010000    // 17. bit hmotnosti
#define LG_SAMPLE_MASK_HOUR         0x001F      // Hodina je ulozena ve spodnich 5 bitech

typedef struct {
   byte  Flag;        // naraznik
   word  Value;       // vaha nebo hodina (vaha je na 17 bitu)
} TLoggerSample;      // 3 bajty

// zaznam jednotlivych vazeni :
#define FL_LOGGER_SAMPLES      1801L
#define FL_LOGGER_SAMPLE_SIZE  sizeof( TLoggerSample)                       // 3 bajty

// struktura dne (jen pro pro Builder a vypocet velikosti) :
typedef struct {
   TArchive      Archive;                                                   // 211  bytu
   TLoggerSample Samples[ FL_LOGGER_SAMPLES];                               // 5403 bytu
} TArchiveDailyInfo;   // 5614 bytu

//-----------------------------------------------------------------------------
// Online ukladani
//-----------------------------------------------------------------------------

// Naraznikove znaky :
#define ON_SAMPLE_EMPTY         0x7F        // nenaplnene FIFO
#define ON_SAMPLE_FULL          0xFF        // naplnene FIFO
#define ON_SAMPLE_WEIGHT        0x08        // zaznam hmotnosti
#define ON_SAMPLE_SAVED         0x20        // Vaha by prave ulozila vzorek do pameti
#define ON_SAMPLE_STABLE        0x10        // Bit ustalene hmotnosti
#define ON_SAMPLE_SIGN          0x04        // Znamenko hmotnosti
#define ON_SAMPLE_WEIGHT_MSB    0x03        // 17. a 18. bit hmotnosti ve flagu

#define ON_SAMPLE_MASK_WEIGHT   0x03FFFF    // Hmotnost je ulozena ve spodnich 18bit, MSB byte je flag (hmotnost tedy zabira 2 spodni bity z flagu)
#define ON_SAMPLE_MASK_HOUR     0x001F      // Hodina je ulozena ve spodnich 5 bitech

typedef struct {
   byte  Flag;        // Naraznik
   word  Value;       // Hmotnost nebo hodina
} TOnlineSample;      // 3 bajty

// zaznam jednotlivych vazeni :
#define FL_ONLINE_SAMPLES      694265L
#define FL_ONLINE_SAMPLE_SIZE  sizeof(TOnlineSample)    // 3 bajty

//-----------------------------------------------------------------------------
// Struktura FLASH
//-----------------------------------------------------------------------------

// rozdeleni pameti na sekce
// U AT45BD161B mam k dispozici 4096 x 512 = 2 097 152 bajtu

// konfiguracni sekce :
#define FL_CONFIG_BASE         0L
#define FL_CONFIG_SIZE         4096L

// archiv statistik & histogramu :
#define FL_ARCHIVE_BASE        (FL_CONFIG_BASE + FL_CONFIG_SIZE)
#define FL_ARCHIVE_DAY_SIZE    sizeof( TArchiveDailyInfo)                   // velikost polozky
#define FL_ARCHIVE_DAYS        371L                                         // pocet polozek
#define FL_ARCHIVE_SIZE        (FL_ARCHIVE_DAY_SIZE * FL_ARCHIVE_DAYS)      // celkem bytu (2082794), konec pameti 2086890

// Online mereni :
#define FL_ONLINE_BASE         FL_ARCHIVE_BASE                              // Stejne jako archiv
#define FL_ONLINE_SIZE         (FL_ONLINE_SAMPLE_SIZE * FL_ONLINE_SAMPLES)  // celkem bytu (2082795), konec pameti 2086891

// struktura konfiguracni sekce :
typedef struct {
  TConfig       Config;         // Konfigurace
  TFlocks       Flocks;         // Definice jednotlivych hejn
  TCalibration  Calibration;    // Kalibrace zkopirovana z interni EEPROM
} TConfigSection; // 2706 bajtu

//-----------------------------------------------------------------------------
// Struktura interni EEPROM
//-----------------------------------------------------------------------------

#define IEP_SPARE (IEP_SIZE - sizeof(TCalibration))

typedef struct {
  TCalibration  Calibration;             // Kalibrace vah
  byte          Dummy[IEP_SPARE];
} TIntEEPROM;


//-----------------------------------------------------------------------------
#endif
