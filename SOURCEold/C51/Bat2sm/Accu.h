//*****************************************************************************
//
//    Accu.h - Kontrola napajeciho napeti
//    Version 1.0
//
//*****************************************************************************

#ifndef __Accu_H__
   #define __Accu_H__

#include "Hardware.h"               // zakladni datove typy

void AccuWaitForVoltage(void);
// Pokud je nizke napajeci napeti, ceka na jeho obnoveni

#endif
