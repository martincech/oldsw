//*****************************************************************************
//
//    DacsPkt.c - kodovani a dekodovani paketu ACS
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#ifndef __DacsPkt_H__
   #define __DacsPkt_H__

#include "Hardware.h"               // zakladni datove typy
#include "Bat2.h"                   // Nastaveni rychlosti a parity

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

#define DATA_MAX_COUNT          15      // Maximalni pocet parametru, ktere posila z a do ACS v prikazech RD, RL a WD

// Typ prijateho prikazu
typedef enum {
  COMMAND_RD,                           // Nacteni bloku dat
  COMMAND_RL,                           // Nacteni jednotlivych parametru
  COMMAND_WD,                           // Zapis bloku dat
  COMMAND_ER,                           // Chybna syntaxe prijmuteho paketu
  _COMMAND_COUNT
} TCommand;

// Struktura paketu
typedef struct {
  TCommand  Command;                    // Prikaz
  int       Data[DATA_MAX_COUNT];       // Data (jednotlive parametry poslane v paketu)
  byte      DataCount;                  // Pocet dat
} TDacsPacket;
extern TDacsPacket __xdata__ DacsPacket;

//---------------------------------------------------------------------------------------------
// Funkce
//---------------------------------------------------------------------------------------------

void DacsPacketInit(void);
// Inicializace

void DacsPacketDisable(void);
// Pozastaveni cinnosti

TYesNo DacsPacketRead(void);
// Nacteni a dekodovani paketu, volat co nejcasteji. Pokud je nacten paket, vrati YES.

void DacsPacketSend(void);
// Zakoduje a posle paket, ktery je definovan ve strukture DacsPacket


#endif // __DacsPkt_H__
