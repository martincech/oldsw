//*****************************************************************************
//
//    GsmCtl.h         Bat2 GSM control
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __GsmCtl_H__
   #define __GsmCtl_H__

#include "Hardware.h"

#ifdef GSM_SMS_DEBUG
  #include "Bat2.h"               // GSM_NUMBER_MAX_LENGTH pro GSM diagnostiku
#endif // GSM_SMS_DEBUG

#define GSM_TIMER           5     // interval cteni zprav [s]
#define GSM_REPLY_DELAY     500   // Cekani v ms mezi prijetim SMS a odeslanim odpovedi - u nekterych SIM karet to bez cekani blbne a neodesle odpoved.
                                  // O pulnoci to SMS posle, ale na request to neodpovi.
                                  // Testy: 100ms a 200ms nefunguje, 300ms uz funguje.

// Stav GSM modulu
typedef enum {
  GSM_OFF,                      // Modul ma vypnute napajeni
  GSM_POWERED,                  // Modul ma zapnute napajeni, ale jeste nekomunikuje (nezdaril se reset)
  GSM_LIVE,                     // Modul ma zapnute napajeni a komunikuje, ale jeste neni prihlasen do site
  GSM_CHECK_PIN,                // Modul ma zapnute napajeni a komunikuje, ale jeste jsem nezkontroloval PIN
  GSM_LOCKED_PIN,               // Karta vyzaduje zadani PIN, pripadne je zablokovana po vicenasobnem chybnem zadani PIN (to nerozlisuju)
  GSM_DELAY_BEFORE_READY,       // Modul se jiz prihlasil do site, ale cekam par cyklu, nez umoznim dalsi komunikaci - o pulnoci na baterky to nechtelo posilat
  GSM_READY                     // Modul je prihlasen do site, muze se odesilat/prijimat
} TGsmStatus;
extern TGsmStatus __xdata__ GsmStatus;

extern char __xdata__ GsmBuffer[];      // Buffer na SMS vcetne zakoncovaci nuly, sdili i Megavi
extern char __xdata__ GsmNumber[];      // Vcetne zakoncovaci nuly, sdili i Megavi

extern byte __xdata__ GsmSignal;                               // Sila signalu v procentech

#ifdef GSM_SMS_DEBUG

extern byte __xdata__ SmsDebugValue;
typedef enum {
  READ_EMPTY,
  READ_ERROR,
  READ_OK
} TReadStatus;
extern TReadStatus __xdata__ ReadStatus;   // Stav nacitani SMS
extern char __xdata__ SmsReadText[ GSM_DIAG_SMS_MAX_LENGTH + 1];       // Omezim kvuli nedostatku XRAM, stejne zobrazuju jen 15 znaku ze prijate zpravy
extern char __xdata__ SmsReadNumber[GSM_NUMBER_MAX_LENGTH + 1];

extern byte __xdata__ ReadTimeout;         // Timeout pri cteni zpravy
extern byte __xdata__ ReadDebugValue;      // Vysledek fce SmsRead
extern word __xdata__ TotalReadError;      // Pocet chybnych nacitani
extern word __xdata__ TotalReadEmpty;      // Pocet spravnych nacitani, prazdna SMS
extern word __xdata__ TotalReadOk;         // Pocet spravnych nacitani, nejaky obsah

extern byte __xdata__ SendDebugValue;      // Vysledek fce SmsSend
extern byte __xdata__ SmsSent;             // YES/NO byla odeslana odpoved
extern byte __xdata__ DecodeDebug;         // Hodnota pri kontrole textu SMS

#endif // GSM_SMS_DEBUG


void GsmInit(void);
// Inicializace modulu

void GsmStart(void);
// Zapnuti modulu

void GsmStop(void);
// Vypnuti modulu

TYesNo GsmPin(char xdata *Pin);
// Zadani PIN

void GsmClear(void);
// Uvedeni do vychoziho stavu po zapnuti nebo vypnuti vazeni

void GsmExecute(void);
// Vybirani a provadeni doslych povelu (SMS)

void GsmPrepareMidnightSms(void);
// Pokud mam dnes posilat pulnicni SMS, ulozim SMS do NVRAM a pripravim ji tak k odeslani

void GsmCheckMidnightSending(void);
// Nastavi automat na vyslani SMS o pulnoci

#endif
