//*****************************************************************************
//
//    Str.c - Prace se stringy
//    Version 1.0, (c) P.Veit
//
//*****************************************************************************

#ifndef __Str_H__
   #define __Str_H__

byte __xdata__ *StrAddColon(byte code *Original, byte __xdata__ *s);
// Prida na konec retezce <Original> dvojtecku, zkopiruje vysledek do <s> a vrati ukazatel na <s>

byte __xdata__ *StrMakeDate(byte Day, byte Month, word Year, byte __xdata__ *s);
// Vytvori string s datumem ve formatu DD.MM.YYYY zakonceny nulou
// Vyuziva UN.DT

byte __xdata__ *StrMakeTime(byte Hour, byte Min, byte __xdata__ *s);
// Vytvori string s datumem ve formatu HH:MM zakonceny nulou

#endif
