//*****************************************************************************
//
//    StatCalc.c - Calculation of statistics for display purposes
//    Version 1.0
//
//*****************************************************************************

#ifndef __StatCalc_H__
   #define __StatCalc_H__

#include "Hardware.h"     // zakladni datove typy
#include "Bat2.h"

typedef struct {
  word TargetWeight;
  word Count;
  word Average;
  word LastAverage;
  long Gain;            // Muze byt az -65.535, tj. long
  word Sigma;
  word Cv;
  byte Uniformity;
} TStatCalcResult;
extern TStatCalcResult xdata StatCalcResult;


void StatCalculate(TArchive xdata *Archive, TGender Gender);
// Vypocte statistiku a ulozi ji do globalni struktury <StatCalcResult>


#endif
