//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#include "Hardware.h"       // zakladni datove typy

// Globalni promenne :

// Co se zobrazuje na displeji behem vazeni
typedef enum {
  DISPLAY_MODE_IDLE,
  DISPLAY_MODE_WAITING,
  DISPLAY_MODE_WEIGHING,
  DISPLAY_MODE_STATISTICS,
  DISPLAY_MODE_HISTOGRAM,
  DISPLAY_MODE_LOGGER,
  DISPLAY_MODE_ARCHIVE_STATISTICS,              // Za tuto polozku nic nedavat, provadi se testy >= DISPLAY_MODE_ARCHIVE_STATISTICS
  DISPLAY_MODE_ARCHIVE_HISTOGRAM,
  DISPLAY_MODE_ARCHIVE_LOGGER,
  _DISPLAY_MODE_COUNT
} TDisplayMode;
extern TDisplayMode __xdata__ DisplayMode;      // Co se ma prave zobrazovat



void MenuInit(void);
// Inicializace zobrazeni, volat az po nahrani konfigurace

void MenuLanguage(void);
// Provede volbu jazyka

void MenuExecute(void);
// Spusteni menu

void MenuLoggerNextPage(void);
// Posune logger na dalsi stranku, na konci prejde zpet na prvni stranku

void MenuLoggerPreviousPage(void);
// Posune na predchozi stranku, na zacatku prejde na posledni stranku

void MenuDisplayLowBat(void);
// Zobrazi nizke napeti

void MenuDisplayWeighing(byte NewDisplay);
// Hlavni zobrazeni behem vazeni

void MenuChangeGender(void);
// Zmeni zobrazovane pohlavi

void MenuHistogramMoveRight(void);
// Posune kurzor v histogramu o 1 doprava

void MenuHistogramMoveLeft(void);
// Posune kurzor v histogramu o 1 doleva

void MenuNextDisplay(void);
// Posune zobrazeni aktualniho vazeni na dalsi


#endif
