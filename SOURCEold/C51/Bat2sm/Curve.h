//*****************************************************************************
//
//    Curve.c - Prace s rustovymi krivkami
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Curve_H__
   #define __Curve_H__

#include "Hardware.h"     // zakladni datove typy
#include "Bat2.h"

word CurveFindLastDay(TCurvePoint __xdata__ *CurvePoint);
// Najde v krivce nejvyssi den a vrati jej.
// Pokud je krivka prazdna, vrati KRIVKA_MAX_DEN + 1

word CurveCalculateApproximation(TCurvePoint __xdata__ *CurvePoint, word Day);
// Vypocte ze zadane hmotnost pro zadany den (den muze byt 0-999)

void CurveSaveFirstWeight(TCurvePoint __xdata__ *CurvePoint, word Weight);
// Vytvori jednoprvkovou rustovou krivku se zadanou hmotnosti

word CurveLoadFirstWeight(TCurvePoint __xdata__ *CurvePoint);
// Nacte z rustove krivky prvni hmotnost

TYesNo CurveTable(TCurvePoint __xdata__ *CurvePoint);
// Edituje tabulku rustove krivky

#endif
