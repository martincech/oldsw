//*****************************************************************************
//
//    Lang.h      - Definice jazykove verze
//    Version 1.0
//
//*****************************************************************************

#ifndef __Lang_H__
   #define __Lang_H__

// Logo zobrazene po startu
#define LOGO_VEIT               1       // Standardni logo "poultry scale bat1"
//#define LOGO_AUTOMATION_MASTERS 1       // Logo pro Pakistan " AUTOMATION MASTERS bat1"
//#define LOGO_VERTA_TEKNO        1       // Logo pro Turecko
//#define LOGO_BROILERTEKKNIIKA   1       // Logo pro Seppa do Finska
//#define LOGO_SWEDEGG            1       // Logo pro Swedegg Svedsko
//#define LOGO_SODALEC            1       // Logo Sodalec Francie
//#define LOGO_SUBSAHARAHSCALES   1       // Logo pro Sub Saharah Scales v Jizni Africe
//#define LOGO_ISHII                1       // Logo pro Ishii v Japonsku
//#define LOGO_CHOOKS             1       // Logo pro Aussie Chooks Equipment v Australii
//#define LOGO_YDE                1       // Logo pro Yde Belgie
//#define LOGO_FARMWEIGHSYSTEMS   1       // Logo pro Farm Weigh Systems v USA (Dwain)
//#define LOGO_ACE                1       // Logo ACE Phil Australie

// Jazykova verze - definuje, o jakou jazykovou verzi vah se jedna - u Bat1 se vyuziva i pri testovani na jazyk v Main.c51
//#define LANG_VERSION_OLD      1        // Puvodni jazyky - pozor, nebude korektne fungovat testovani na jazyky!
//#define LANG_VERSION_WEST     1        // Zapadni jazyky
#define LANG_VERSION_EAST     2        // Vychodni jazyky
//#define LANG_VERSION_NORTH    3        // Severske jazyky
//#define LANG_VERSION_JAPANESE 4        // Japonstina

// Masky na dekodovani jazykove verze a jazyka
#define LANG_VERSION_MASK   0xE0        // Jazykova verze v hornich 3 bitech
#define LANG_MASK           0x1F        // Jazyk ve spodnich 5 bitech

// Nastaveni podle zvolene jazykove verze
#ifdef LANG_VERSION_OLD
  #define LANG_VERSION    LANG_VERSION_OLD     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_GERMAN,
    LANG_SPANISH,
    LANG_FRENCH,
    LANG_ITALIAN,
    LANG_PORTUGAL,
    LANG_RUSSIAN,
    LANG_DUTCH,
    LANG_POLISH,
    LANG_CZECH,
    LANG_FINNISH,
    _LANG_COUNT
  } TJazyk;
  #define POCET_JAZYKU      11                  // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_WEST
  #define LANG_VERSION    LANG_VERSION_WEST     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_GERMAN,
    LANG_SPANISH,
    LANG_FRENCH,
    LANG_ITALIAN,
    LANG_PORTUGAL,
    LANG_DUTCH,
    _LANG_COUNT
  } TJazyk;
  #define POCET_JAZYKU      7                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_EAST
  #define LANG_VERSION    LANG_VERSION_EAST     // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_GERMAN,
    LANG_SPANISH,
    LANG_FRENCH,
    LANG_RUSSIAN,
    LANG_TURKISH,
    LANG_POLISH,
    LANG_CZECH,
    _LANG_COUNT
  } TJazyk;
  #define POCET_JAZYKU      8                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_NORTH
  #define LANG_VERSION    LANG_VERSION_NORTH    // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_GERMAN,
    LANG_SPANISH,
    LANG_FRENCH,
    LANG_NORWEGIAN,
    LANG_FINNISH,
    _LANG_COUNT
  } TJazyk;
  #define POCET_JAZYKU      6                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif

#ifdef LANG_VERSION_JAPANESE
  #define LANG_VERSION    LANG_VERSION_JAPANESE // Aktualni jazykova verze
  typedef enum {
    LANG_ENGLISH,
    LANG_JAPANESE,
    _LANG_COUNT,
    LANG_GERMAN,                        // Dalsi jazyky uz definovany v JP nejsou, ale je treba je definovat aspon zde
    LANG_SPANISH,
    LANG_FRENCH
  } TJazyk;
  #define POCET_JAZYKU      2                   // Celkovy pocet jazyku
  #define LANG_DEFAULT      LANG_ENGLISH        // Default jazyk pro tuto jazykovou verzi
#endif


// ---------------------------------------------------------------------------------------------
// Definice jazyka
// ---------------------------------------------------------------------------------------------

extern unsigned char code LANGUAGE[];
extern unsigned char code POLOZKYMENUJAZYK[];

// ---------------------------------------------------------------------------------------------
// Menu
// ---------------------------------------------------------------------------------------------

#define POCETPOLOZEKHLAVNIHOMENU 9
extern unsigned char code POLOZKYMENU[];  // Max 22 + zakoncovaci nula

#define POCETPOLOZEKMENUNASTAVENI 8
extern unsigned char code POLOZKYMENUNASTAVENI[];

extern unsigned char code POLOZKYMENUPODSVIT[];

extern unsigned char code POLOZKYMENUZVUKY[];

extern unsigned char code POLOZKYMENUMEZ[];

extern unsigned char code POLOZKYMENUVYBERSTATISTIKY[];

// ---------------------------------------------------------------------------------------------
// Jednotky
// ---------------------------------------------------------------------------------------------

extern unsigned char code UNITS[];
extern unsigned char code POLOZKYMENUJEDNOTKY[];

// ---------------------------------------------------------------------------------------------
// Dalsi
// ---------------------------------------------------------------------------------------------

extern unsigned char code HESLO[];  // Kvuli nekterym jazykum musi byt na 2 radky

extern unsigned char code STR_OK[POCET_JAZYKU][6];

extern unsigned char code ZRUSIT[];

extern unsigned char code ANO[POCET_JAZYKU][6];

extern unsigned char code NE[POCET_JAZYKU][6];

extern unsigned char code NEPLATNEHESLO[];

extern unsigned char code OPAKOVAT[];

extern unsigned char code PRIDAT[];

extern unsigned char code HOTOVO[];

extern unsigned char code STR_JEDNOTKY[3][POCET_JAZYKU][3];

extern unsigned char code AUTOMANUALVYBER[];

extern unsigned char code UKLADAM[];

extern unsigned char code ULOZENO[];

//extern unsigned char code VERZE[POCET_JAZYKU][9];

extern unsigned char code MENUKALIBRACENULY[];

extern unsigned char code MENUKALIBRACEROZSAHU[];

extern unsigned char code MENUKALIBRACEULOZIT[];

extern unsigned char code MENUFILTR[];

extern unsigned char code MENUUSTALENI[];

extern unsigned char code MENUODLEHCENI[];

extern unsigned char code MENUPOCETSOUBORU[];

extern unsigned char code MENUSMAZATVSECHNYSOUBORY[];

extern unsigned char code MENUCISLOSOUBORU[];

extern unsigned char code MENUOKOLIPRUMERU[];

extern unsigned char code MENUMEZ50[];

extern unsigned char code MENUUKLADANI[];

extern unsigned char code AUTO[POCET_JAZYKU][5];

extern unsigned char code MANUAL[];

extern unsigned char code VYBER[];

extern unsigned char code ZAKLADNI[];

extern unsigned char code ROZSIRENE[];

extern unsigned char code MENUPOCET[];

extern unsigned char code MENUPREPNOUTSOUBOR[];

extern unsigned char code MENUSOUBORYOBSAZENY[];

extern unsigned char code MENUSOUBORNAHRADIT[];

extern unsigned char code MENUSOUBOROPRAVDU[];

extern unsigned char code MENUSOUBORVYMAZAT[];

extern unsigned char code MENUSOUBOROPRAVDUVYMAZAT[];

extern unsigned char code MENUPODSVITTYP[];

extern unsigned char code MENUPODSVITINTENZITA[];

extern unsigned char code VYPNUTYZAPNUTYAUTOMATICKY[];

extern unsigned char code MENUPODSTVITVYPNUTY[];

extern unsigned char code MENUPODSTVITZAPNUTY[];

extern unsigned char code MENUPODSTVITAUTOMATICKY[];

extern unsigned char code MENUZVUKYTON[];

extern unsigned char code MENUZVUKYHLASITOST[];

extern unsigned char code MENUSMAZATVZOREK[];

extern unsigned char code MENUVLOZITAKTUALNI[];

extern unsigned char code MENUSOUBORPRAZDNY[];

extern unsigned char code MENUSOUBORPROBIHAMAZANI[];

extern unsigned char code POVOLITZAKAZATHESLO[];

extern unsigned char code MENUULOZIT[];

extern unsigned char code MENUMEZHMOTNOST[];

extern unsigned char code MENUMEZPIPAT[];

extern unsigned char code MENUMEZTONNAD[];

extern unsigned char code MENUMEZTONPOD[];

extern unsigned char code MENUMEZZOBRAZENI[];

extern unsigned char code MENUZOBRAZENI[];

extern unsigned char code ZAKLADNIROZSIRENE[];

extern unsigned char code STATISTIKAPOCET[];

extern unsigned char code STATISTIKACV[POCET_JAZYKU][3];

extern unsigned char code STATISTIKAUNIFORMITA[POCET_JAZYKU][5];  // Musi byt delka 5 kvuli JP

extern unsigned char code STATISTIKAHISTOGRAMSOUBOR[];

extern unsigned char code STATISTIKAHISTOGRAMMEZ[];

extern unsigned char code STATISTIKAHISTOGRAMHMOTNOST[];

extern unsigned char code STATISTIKAHISTOGRAMPOCET[];

extern unsigned char code MENUKOMUNIKACESPC[];

extern unsigned char code MENUTISK[];

extern unsigned char code MENUPRETIZENI[];

extern unsigned char code MENUPODTECENI[];

extern unsigned char code CHYBANEPLATNAHODNOTA[];

extern unsigned char code CHYBASPATNAPAMET[];

extern unsigned char code CHYBASOUBORPLNY[];

extern unsigned char code CHYBASPATNAPAMETMAZANI[];

extern unsigned char code CHYBATISKU[];


#endif  // __Lang_H__
