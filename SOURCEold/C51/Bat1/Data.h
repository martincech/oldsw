//*****************************************************************************
//
//    Data.c - zdroj dat z jednoho nebo vice souboru pro statisticke vypocty
//    Version 1.0
//
//*****************************************************************************

#ifndef __Data_H__
   #define __Data_H__

#include "Hardware.h"     // zakladni datove typy

#define DATA_MAX_FILES          99      // Maximalni pocet souboru, ktere muze naraz vybrat
//#define DATA_FILES_TERMINATOR   0       // Zakonceni posloupnosti cisel souboru

typedef struct {
  byte  Files[DATA_MAX_FILES];          // Cisla vybranych fyzickych souboru
  byte  FilesCount;                     // Pocet souboru v poli Files[]
  byte  CurrentFile;                    // Aktualni fyzicky zpracovavany soubor
  byte  CurrentFileIndex;               // Index aktualniho zpracovavaneho souboru v poli Files[]
  word  CurrentAddress;                 // Aktualni adresa v EEPROM v ramci aktualniho souboru

  word  Weight;                         // Hmotnost vzorku nacteneho z pameti
  byte  Flag;                           // YES/NO flag vzorku nacteneho z pameti
} TData;
extern TData __xdata__ Data;

// Vypocte delku jednoho souboru podle nastaveneho poctu souboru
#define DelkaSouboruVBajtech() (8000 / PocetSouboru + 2)


void DataClear();
  // Vymaze vsechny soubory ze seznamu

TYesNo DataAddFile(byte FileNumber);
  // Prida do seznamu soubor zadany cislem <FileNumber>. Pokud je uz seznam plny, vrati NO.

void DataSetOneFile(byte FileNumber);
  // Nastavi na 1 soubor zadany cislem <FileNumber>

TYesNo DataFirst();
  // Nastavi cteni na 1. vzorek prvniho souboru

TYesNo DataNext();
  // Posune se na dalsi platny vzorek (bud na dalsi vzorek v aktualnim souboru, nebo na zacatek dalsiho souboru). Pokud je uz na konci, vrati NO.

TYesNo DataRead();
  // Nacte vzorek na aktualni pozici a ulozi udaje do Weight a Flag

TYesNo DataFileExists(byte FileNumber);
  // Pokud je uz soubor <FileNumber> v seznamu, vrati YES.

#endif
