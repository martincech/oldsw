//*****************************************************************************
//
//    Lifo.h - LIFO EEPROM module
//    Version 1.0, Petr
//
//*****************************************************************************

#ifndef __Lifo_H__
   #define __Lifo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Komunikacni struktura pro uzivatele modulu :

extern TLifoData __xdata__ LifoData;     // zde jen pro kontrolu, musi byt soucasti projektu !


// Inicializace
void LifoInit();

// Atualni pozice v LIFO
unsigned int LifoGetIndex();

// Pocet prvku v LIFO
unsigned int LifoCount();

// Pridani zaznamu do LIFO
bit LifoAdd();

// Cteni zaznamu z aktualni pozice
bit LifoRead();

// Presun na prvni zaznam
bit LifoFirst();

// Presun na dalsi zaznam
bit LifoNext();

// Presun na predchozi zaznam
bit LifoPrev();

// Presun na posledni zaznam
bit LifoLast();

// Smaze cele Lifo
bit LifoReset();

// Smazani posledniho zaznamu
bit LifoDeleteLast();




#endif
