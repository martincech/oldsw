//*****************************************************************************
//
//   ComPkt.h    COM packet interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __ComPkt_H__
   #define __ComPkt_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

//-----------------------------------------------------------------------------
// Kratky prikaz
//-----------------------------------------------------------------------------

TYesNo ComRxPacket( byte data *cmd, dword data *arg);
// Prijme paket, vrati typ prikazu <cmd> a parametr prikazu <arg>.
// Vrati NO, pokud paket neprisel

void ComTxPacket( byte cmd, dword arg);
// Vysle paket s kodem prikazu <cmd> a parametrem <arg>

//-----------------------------------------------------------------------------
// Datovy blok
//-----------------------------------------------------------------------------

void ComTxBlockStart( word size);
// Vysle zahlavi paketu pro data o velikosti <size>

void ComTxBlockByte( byte b);
// Vysle byte <b> bloku dat

void ComTxBlockEnd( void);
// Uzavre datovy blok

TYesNo ComRxBlock( void xdata *buffer, word data *size);
// Prijem datoveho bloku

#endif
