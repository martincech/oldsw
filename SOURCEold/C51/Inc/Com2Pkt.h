//*****************************************************************************
//
//   Com2Pkt.h   COM packet interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Com2Pkt_H__
   #define __Com2Pkt_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

//-----------------------------------------------------------------------------
// Kratky prikaz
//-----------------------------------------------------------------------------

TYesNo Com2RxPacket( byte data *cmd, dword data *arg);
// Prijme paket, vrati typ prikazu <cmd> a parametr prikazu <arg>.
// Vrati NO, pokud paket neprisel

void Com2TxPacket( byte cmd, dword arg);
// Vysle paket s kodem prikazu <cmd> a parametrem <arg>

//-----------------------------------------------------------------------------
// Datovy blok
//-----------------------------------------------------------------------------

void Com2TxBlockStart( word size);
// Vysle zahlavi paketu pro data o velikosti <size>

void Com2TxBlockByte( byte b);
// Vysle byte <b> bloku dat

void Com2TxBlockEnd( void);
// Uzavre datovy blok

TYesNo Com2RxBlock( void xdata *buffer, word data *size);
// Prijem datoveho bloku

#endif
