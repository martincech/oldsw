//*****************************************************************************
//
//    Fmt.h        Format specifications
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __Fmt_H__
   #define __Fmt_H__

// priznaky vlozene do parametru <width> :

#define FMT_UNSIGNED       0x80             // cislo bez znamenka
#define FMT_LEADING_0      0x08             // priznak vedoucich 0
#define FMT_WIDTH          0x07             // maska - pocet znaku cisla
#define FMT_DECIMALS       0x70             // maska - pocet cislic za carkou
#define FMT_DECIMALS_SHIFT 4                // posun cislic za carkou
#define FMT_MINUS          0x80000000L      // zaporne znamenko pro float

// standardni formatovani printf <Total> je celkovy pocet cislic + 2 (znamenko a tecka),
// <Decimal> je pocet mist za teckou

#define FmtPrecision( Total, Decimal) ((((Total) - 2) & FMT_WIDTH)  | \
                                      (((Decimal) << FMT_DECIMALS_SHIFT) & FMT_DECIMALS))

#define FmtSetWidth(    Width)    ((Width) & FMT_WIDTH)
#define FmtSetDecimals( Decimal)  (((Decimal) << FMT_DECIMALS_SHIFT) & FMT_DECIMALS)

#endif
