//*****************************************************************************
//
//    Owi.h  -  1-Wire bus services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Owi_H__
   #define __Owi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Identifikacni retezec :

#define OWI_SERIAL_LENGTH 6              // delka serioveho cisla

typedef struct {
   byte family;                        // kod typu cipu
   byte serial[ OWI_SERIAL_LENGTH];    // seriove cislo
   byte crc;                           // zabezpeceni
} TOwiIdentification;

#define OWI_IDENTIFICATION_LENGTH sizeof( TOwiIdentification)


// Kody prikazu pro operace s ROM :

#define OWI_CMD_SEARCH_ROM       0xF0    // vyhledani a identifikace zarizeni
#define OWI_CMD_READ_ROM         0x33    // cteni identifikace (pro 1 zarizeni)
#define OWI_CMD_MATCH_ROM        0x55    // vyber zarizeni podle identifikace
#define OWI_CMD_SKIP_ROM         0xCC    // vyrazeni identifikace (pro 1 zarizeni)
#define OWI_CMD_ALARM_SEARCH     0xEC    // vyhledani a identifikace zarizeni s alarmem

//-----------------------------------------------------------------------------

TYesNo OwiInit( void);
// Zkontroluje sbernici, vraci 0 pri chybe (zkrat)

bit OwiReset( void);
// Resetuje sbernici, vraci signal pritomnosti.
// 0 - zarizeni pritomno, 1 - nenalezeno zadne

bit OwiReadBit( void);
// Vytvori Read slot a vrati hodnotu bitu

void OwiWriteBit( bit value);
// Vytvori Write slot s hodnotou <value>

byte OwiReadByte( void);
// Precte byte ze sbernice

void OwiWriteByte( byte value);
// Zapise byte na sbernici

TYesNo OwiReadROM( TOwiIdentification *ident);
// Precte identifikaci cipu

//--- interni funkce, nepouzivat :

void OwiMicroDelay( word count);
// Zpozdeni v mikrosekundach

void OwiResetCrc( void);
// Nuluje zabezpeceni, volat vzdy pred vypoctem

byte OwiCalcCrc( byte value);
// Vypocte z bytu zabezpeceni, vrati vysledek

byte OwiGetCrc( void);
// Vrati vysledek vypoctu zabezpeceni

#endif

