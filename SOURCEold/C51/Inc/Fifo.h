//*****************************************************************************
//
//    Fifo.h - FIFO EEPROM module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Fifo_H__
   #define __Fifo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Komunikacni struktura pro uzivatele modulu :

extern TFifoData __xdata__ FifoData;     // zde jen pro kontrolu, musi byt soucasti projektu !


void FifoInit( void);
// Inicializuje modul

TYesNo FifoWrite( void);
// Zapise globalni komunikacni strukturu do FIFO

void FifoRead( word address);
// Naplni komunikacni strukturu daty z <address>

word FifoFindMarker( void);
// Vrati adresu markeru

TYesNo FifoReset( void);
// Zalozi prazdne FIFO

word FifoFindFirst();
// Vrati adresu prvniho zaznamu

word FifoFindLast();
// Vrati adresu posledniho zaznamu

#endif
