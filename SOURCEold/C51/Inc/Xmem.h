//*****************************************************************************
//
//    Xmem.h  -  External EEPROM module using X25645 / AT25640 services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Xmem_H__
   #define __Xmem_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// POZOR : u blokoveho cteni/zapisu nelze vkladat obsluhu
// zarizeni, ktera pouzivaji sdilene vodice EEPROM


#ifdef XMEM_USE_INS    // HW signal INSERT je pritomen
   #define XmemIsPresent()     XmemInsert
   // Vraci YES, je-li pritomen pametovy modul
#else                  // SW testovani pritomnosti
   TYesNo XmemIsPresent( void);
   // Vraci YES, je-li pritomen pametovy modul
#endif

void XmemInit( void);
// Nastavi klidove hodnoty na sbernici, inicializuje pamet

byte XmemReadByte( word address);
// Precte byte z EEPROM <address>

TYesNo XmemWriteByte( word address, byte value);
// Zapise byte <value> na <address> v EEPROM
// Vraci NO neni-li zapis mozny

//------ Blokove cteni -----------------------------

void XmemBlockReadStart( word address);
// Zahaji blokove cteni z EEPROM <address>

#define XmemBlockReadData() XmemRawReadByte()
// Cteni bytu bloku

#define XmemBlockReadStop() XmemStop()
// Ukonceni cteni bloku

//------ Strankovy zapis -----------------------------
// Pozor, hranice stranek hlida uzivatel API
// XEP_PAGE_SIZE je v Hardware.h

TYesNo XmemPageWriteStart( word address);
// Zahaji zapis stranky od <address> v EEPROM.
// Vraci NO neni-li zapis mozny

#define XmemPageWriteData( value) XmemRawWriteByte( value)
// Zapis bytu do stranky

#define XmemPageWritePerform() XmemStop()
// Odstartuje fyzicky zapis stranky do EEPROM

//------ Interni funkce, nepouzivat : ----------------

void XmemRawWriteByte( byte value);
// Zapise byte, po zapise zustane SCK = 1

byte XmemRawReadByte( void);
// Precte a vrati byte, po cteni zustane SCK = 1

void XmemStop( void);
// Ukonci operaci

#endif
