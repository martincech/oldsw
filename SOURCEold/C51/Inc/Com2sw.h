//*****************************************************************************
//
//    Com2sw.h - RS232 software emulation services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Com2sw_H__
   #define __Com2sw_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef COM2_TX_BUFFER_SIZE         // asynchronni vysilac
   // interni promenna, nepouzivat :
   extern volatile byte  _tx2_running;
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void Com2TxChar( byte c);
// Vyslani znaku. Po timeoutu vraci NO

#ifdef COM2_TX_BUFFER_SIZE         // asynchronni vysilac
   #define Com2TxBusy()  _tx2_running
   // Probiha vysilani
#else
   #define Com2TxBusy() NO         // synchronni vysilani, vzdy pripraven
#endif

TYesNo Com2RxChar( byte data *Char);
// Vraci prijaty znak. Po timeoutu vraci NO

TYesNo Com2RxWait( byte Timeout);
// Ceka na znak az <Timeout> * 100 milisekund

void Com2FlushChars( void);
// Vynecha vsechny znaky az do meziznakoveho timeoutu


#endif

