//*****************************************************************************
//
//    Display.h  -  Zobrazeni
//    Verze 1.0
//
//*****************************************************************************

#ifndef __Display_H__
   #define __Display_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

extern byte __xdata__ Language;

// Specialni nastaveni editace
#define DISPLAY_EDIT_DATE   9       // Delka, ktera ve fci DisplayEdit() znaci editaci datumu
#define DISPLAY_EDIT_TIME   10      // Delka, ktera ve fci DisplayEdit() znaci editaci casu
#define DISPLAY_EDIT_SIGN   0x40    // Maska pro delku, pri ktere se edituje cislo vcetne znamenka

// Konstanty pro editaci textu
#define TEXT_SPACE          0x20     // Kde je ve fontu mezera
#define TEXT_UNDERSCORE     0x5F     // Kde je ve fontu podtrzitko - pri editaci se zobrazuje misto mezery, aby tam neco blikalo

// Typ pro predani tlacitek do fce Dialog()
#define DIALOG_NO_WAIT      0x80     // Pokud je nastavene MSB v TKlavesyDialogu, jen vykreslim a necekam na stisk tlacitka - vhodne napr. pro editaci atd.
typedef enum {
  // Zadne klavesy
  DIALOG_KEYS_EMPTY,            // Zadne klavesy a neceka na stisk tlacitka, hned odejde
  // Jedna klavesa
  _DIALOG_KEYS_ONE_KEY = 0x10,  // Odtud zacina 1 klavesa
  DIALOG_KEYS_OK = _DIALOG_KEYS_ONE_KEY,
  DIALOG_KEYS_CANCEL,
  DIALOG_KEYS_QUIT,
  DIALOG_KEYS_NEXT,
  // Dve klavesy
  _DIALOG_KEYS_TWO_KEYS = 0x30, // Odtud zacinaji 2 klavesy
  DIALOG_KEYS_OKCANCEL = _DIALOG_KEYS_TWO_KEYS,
  DIALOG_KEYS_YESNO,
  DIALOG_KEYS_RETRYCANCEL
} TDialogKeys;

// Konstanty pro vyber z menu
#define MENU_ESC_PRESSED    0xFF   // Tuto hodnotu vrati fce VyberMenu(), pokud stiskne Esc nebo dojde k timeoutu, tj. nevybere zadnou polozku

// Pozice kurzoru v menu
typedef struct {
  byte CurrentItem;         // Aktualne vybrana polozka v menu
  byte FirstDisplayedItem;  // Menu, ktere je zobrazeno na 1. radku
} TMenuPosition;

// Menu
typedef struct {
  byte *Items;              // Ukazatel na polozky oddelene nulou
  byte ItemsCount;          // Celkovy pocet polozek
  byte *Title;              // Nadpis podmenu
  word Show;                // Ktere polozky zobrazit a ktere ne (LSB odpovida 1. polozce)
  TMenuPosition Position;   // Pozice kurzoru v menu
//  byte CurrentItem;         // Aktualne vybrana polozka v menu
//  byte FirstDisplayedItem;  // Menu, ktere je zobrazeno na 1. radku
} TMenuItems;




void DisplayLine(byte x0, byte y0, byte x1, byte y1);
  // Zobrazi usecku mezi souradnicemi x0,y0 a x1,y1

void DisplayString(byte x, byte y, byte *s);
  // Vykresli vetu zvolenym fontem zarovnanou nalevo

void DisplayStringRight(byte x, byte y, byte *s);
  // Vykresli vetu zvolenym fontem zarovnanou napravo

void DisplayStringCenter(byte x, byte y, byte *s);
  // Vykresli vetu zvolenym fontem zarovnanou na stred

void DisplayExpandNumber(byte *Znaky);
  // Rozlozim cislo v UN.DT na jednotlive znaky v poli Znaky[8]

void DisplayNumber(byte X, byte Y, byte Delka, byte Desetiny);
  // Zobrazi cislo UN.DT zvolenym fontem zarovnane zprava

void DisplayNumberLeft(byte X, byte Y, byte Length, byte Decimals);
  // Zobrazi cislo UN.DT zvolenym fontem zarovnane zleva

void DisplayCharFormated(byte X, byte Y, byte Delka, byte Desetiny);
  // Zobrazi znak UN.ST.X1 zvolenym fontem zarovnane zprava a zformatovany jako cislo s delkou Delka a poctem desetin Desetiny.

TYesNo DisplayEdit(byte X, byte Y, byte Delka, byte Desetiny);
  // Edituje cislo UN.DT, Y je cislo radku. Vyuziva font Tahoma8 s neproporcionalni sirkou 6 bodu.

TYesNo DisplayEditText(byte X, byte Y, byte xdata *Text, byte Delka, TYesNo EditujiText);
  // Edituje text v Text o delce Delka, Y je cislo radku. Vyuziva font Tahoma8 s neproporcionalni sirkou 6 bodu.

void DisplayBarGraf(byte X, byte Y, byte Delka, byte Vyska, int Hodnota, int Min, int Max);
  // Zobrazi bargraf, Min, Max i Hodnota mohou byt i zaporne

byte DisplayDialog(byte *Text, TDialogKeys Keys);
  // Vykresli na displeji dialog s textem Text a klavesami Klavesy. V promenne Text znamena \xFF oddelovac radku, \x0 je konec stringu.

byte code *DecodeString(byte code *s, byte Index);
byte code *LangSimpleDecode(byte code *s);
#define LangDecode(s, Pocet) DecodeString(s, Pocet * Language)

byte DisplayMenu(TMenuItems *MenuItems);
  // Provede vyber z menu
byte DisplayChooseItem(byte X, byte Y, byte code *Polozka, byte Pocet, byte Vybrane);
  // Vybere 1 polozku z urciteho poctu moznych a vrati cislo vybrane polozky. V <Vybrane> je default hodnota zacinajici od nuly.

#endif
