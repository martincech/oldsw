//*****************************************************************************
//
//    Ads1241s.h  -  A/D convertor ADS1241 SINGLE channel
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ads1241s_H__
   #define __Ads1241s_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef ADC_INTERRUPT                    // prevodnik v prerusovacim modu
// interni data :
extern bit _AdcStatus;                  // stav mereni
#endif // ADC_INTERRUPT

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void AdcInit( void);
// Inicializace prevodniku

#define AdcIsReady()         (!AdcDRDY)
// Prevodnik ma pripravena data

void AdcSetup( void);
// Nastaveni prevodu, autokalibrace

void AdcSelectInput( byte Channel);
// Prepnuti multiplexoru
// parametr <Channel> je cislo kanalu 0.. ADC_COUNT-1

dword AdcReadValue( void);
// Precteni namerene hodnoty

//-----------------------------------------------------------------------------
#ifdef ADC_INTERRUPT       // prevodnik v prerusovacim modu


void AdcStart( byte Channel);
// Zahajeni periodickeho mereni

void AdcStop( void);
// Zastaveni periodickeho mereni

#define AdcDataReady()       _AdcStatus
// Test na nove zmerenou hodnotu

dword AdcRead( void);
// Cteni merene hodnoty

#endif // ADC_INTERRUPT
//-----------------------------------------------------------------------------

#endif
