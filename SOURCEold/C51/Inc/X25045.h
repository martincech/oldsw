//*****************************************************************************
//
//    X25045.h  -  X25045 services
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __X25045_H__
   #define __X25045_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif




// Status register format X25045 : abych mohl nastavit watchdog pri inicializaci
#define EEP_MASK_WD14  0x00        // Watchdog 1.4s
#define EEP_MASK_WD06  0x10        // Watchdog 600ms
#define EEP_MASK_WD02  0x20        // Watchdog 200ms
#define EEP_MASK_WD0   0x30        // Watchdog disabled




void EepWatchDog();

void EepInit(byte WatchDog);
// Nastavi klidove hodnoty na sbernici, inicializuje pamet a nastavi watchdog

byte EepReadByte( word address);
// Precte byte z EEPROM <address>

TYesNo EepWriteByte( word address, byte value);
// Zapise byte <value> na <address> v EEPROM
// Vraci NO neni-li zapis mozny

//------ Blokove cteni -----------------------------

void EepBlockReadStart( word address);
// Zahaji blokove cteni z EEPROM <address>

#define EepBlockReadData() EepRawReadByte()
// Cteni bytu bloku

#define EepBlockReadStop() EepStop()
// Ukonceni cteni bloku

//------ Strankovy zapis -----------------------------
// Pozor, hranice stranek hlida uzivatel API
// EEP_PAGE_SIZE je v Hardware.h

TYesNo EepPageWriteStart( word address);
// Zahaji zapis stranky od <address> v EEPROM.
// Vraci NO neni-li zapis mozny

#define EepPageWriteData( value) EepRawWriteByte( value)
// Zapis bytu do stranky

#define EepPageWritePerform() EepStop()
// Odstartuje fyzicky zapis stranky do EEPROM

//------ Interni funkce, nepouzivat : ----------------

void EepRawWriteByte( byte value);
// Zapise byte, po zapise zustane SCK = 1

byte EepRawReadByte( void);
// Precte a vrati byte, po cteni zustane SCK = 1

void EepStop( void);
// Ukonci operaci

#endif
