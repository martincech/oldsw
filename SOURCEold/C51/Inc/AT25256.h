//*****************************************************************************
//
//    AT25256.h    AT25256 EEPROM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __X25645_H__
   #define __X25645_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// POZOR : u blokoveho cteni/zapisu nelze vkladat obsluhu
// zarizeni, ktera pouzivaji sdilene vodice EEPROM

void EepInit( void);
// Nastavi klidove hodnoty na sbernici, inicializuje pamet

TYesNo EepCheckConnection();
// Otestuje pritomnost pametoveho modulu i bez HW pripojeni signalu INS

byte EepReadByte( word address);
// Precte byte z EEPROM <address>

TYesNo EepWriteByte( word address, byte value);
// Zapise byte <value> na <address> v EEPROM
// Vraci NO neni-li zapis mozny

//------ Blokove cteni -----------------------------

void EepBlockReadStart( word address);
// Zahaji blokove cteni z EEPROM <address>

#define EepBlockReadData() EepRawReadByte()
// Cteni bytu bloku

#define EepBlockReadStop() EepStop()
// Ukonceni cteni bloku

//------ Strankovy zapis -----------------------------
// Pozor, hranice stranek hlida uzivatel API
// EEP_PAGE_SIZE je v Hardware.h

TYesNo EepPageWriteStart( word address);
// Zahaji zapis stranky od <address> v EEPROM.
// Vraci NO neni-li zapis mozny

#define EepPageWriteData( value) EepRawWriteByte( value)
// Zapis bytu do stranky

#define EepPageWritePerform() EepStop()
// Odstartuje fyzicky zapis stranky do EEPROM

//------ Interni funkce, nepouzivat : ----------------

void EepRawWriteByte( byte value);
// Zapise byte

byte EepRawReadByte( void);
// Precte a vrati byte

void EepStop( void);
// Ukonci operaci

#endif
