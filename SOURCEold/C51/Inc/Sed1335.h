//*****************************************************************************
//
//    Sed1335.h - SED 1335F LCD display services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Sed1335_H__
   #define __Sed1335_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Font_H__
   #include "FontTmc.h"
#endif

// Souradnice beze zmeny :

#define DISPLAY_CURRENT   0xFF

// Zobrazovaci roviny :

typedef enum {
   DISPLAY_PLANE1,
   DISPLAY_PLANE2
} display_plane_t;

// ---------- Prikazy
#define DISPLAY_CMD_MWRITE         0x42       // zapis do videopameti
// P1..PN zapisovane byty, inkrement/dekrement adresy probiha podle CSRDIR

//-----------------------------------------------------------------------------
// CSRDIR
//-----------------------------------------------------------------------------

// nastaveni autoinkrementu adresy a smeru pohybu kurzoru
#define DISPLAY_CMD_CSR_RIGHT      0x4C        // doprava, inkrement
#define DISPLAY_CMD_CSR_LEFT       0x4D        // doleva, dekrement
#define DISPLAY_CMD_CSR_UP         0x4E        // nahoru, dekrement o AP (sirka virtualniho radku)
#define DISPLAY_CMD_CSR_DOWN       0x4F        // dolu, inkrement o AP

// Konstanty pro displej POWERTIP PG320240 :
#define DISPLAY_WIDTH   320                   // graficke rozliseni v pixelech vodorovne
#define DISPLAY_HEIGHT  240                   // graficke rozliseni v pixelech svisle


// Zapis bajtu <Data> do radice
#define WriteByte(Data)         \
  DisplayData = Data;           \
  DisplayCS = 0;                \
  DisplayWR = 0;                \
  DisplayWR = 1;                \
  DisplayCS = 1;                \

// Zapis dat <Data> do radice
#define DisplayWriteData(Data) WriteByte(Data)

// Zapis prikazu <Command> do radice
#define DisplayWriteCommand(Command)    \
  DisplayAddressCommand();              \
  WriteByte(Command);                   \
  DisplayAddressData();


void DisplayInit( void);
// Inicializuje display

//---------- Funkce na nastaveni kontextu :

word DisplayOffset(byte X, byte Y);
// Offset adresy podle souradnic

void SetCursor( word address);
// Nastavi adresu do video RAM

void DisplaySetPlane( bit plane);
// Nastavi pracovni zobrazovaci rovinu

void DisplaySetFont( byte font);
// Nastavi pracovni font

void DisplayMoveTo( byte x, byte y);
// Presun pozice kurzoru <x> je pocet osmic pixelu, <y> je pocet pixelu

//---------- Kreslici funkce :

void DisplayClear( void);
// Smaze nastavenou rovinu

void DisplayPlane( bit on);
// Podle promenne <on> zapne/vypne nastavenou rovinu

void DisplayFillBox( byte w, byte h, byte value);
// Nakresli obdelnik do nastavene roviny od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech, <value>
// je hodnota osmice

#define DisplayBox( w, h) DisplayFillBox( w, h, 0xFF)
// Nakresli obdelnik do nastavene roviny od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech

#define DisplayClearBox( w, h) DisplayFillBox( w, h, 0x00)
// Smaze obdelnik v nastavene rovine od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech

void DisplayPattern( byte code *pattern, byte w, byte h);
// Nakresli obdelnik vyplneny vzorem z adresy <pattern>
// do nastavene roviny od nastavene pozice.
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech

void DisplayGotoRC( byte r, byte c);
// presune kurzor na zadanou textovou pozici <r> radek <c> sloupec
// Hodnota DISPLAY_CURRENT znamena beze zmeny

char putchar( char c);
// standardni zapis znaku

void DisplayCursor( bool on);
// je-li <on>=NO zhasne kurzor, jinak rozsviti

void DisplayClrEol( void);
// smaze az do konce radku, nemeni pozici kurzoru

#endif

