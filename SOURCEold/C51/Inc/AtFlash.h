//*****************************************************************************
//
//   AtFlash.h      Flash memory linear access
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __AtFlash_H__
   #define __AtFlash_H__

#ifndef __At45dbxx_H__
   #include "..\inc\At45dbxx.h"
#endif

// hranice stranky pri zapisu i cteni neni treba respektovat


#define FLASH_FLUSH_ADDRESS  0xFFFFFFFEL   // neplatna adresa Flash

#if   FLASH_PAGE_SIZE == 264
   #define FLASH_VIRTUAL_PAGE        256    // velikost stranky
   // stranka/buffer z linearni adresy :
   #define MkBufferAddress( a)     ((a) & 0xFF)
   #define MkPageAddress( a)       ((word)((a) >> 8))
#elif FLASH_PAGE_SIZE == 528
   #define FLASH_VIRTUAL_PAGE        512    // velikost stranky
   // stranka/buffer z linearni adresy :
   #define MkBufferAddress( a)     ((a) & 0x1FF)
   #define MkPageAddress( a)       ((word)((a) >> 9))
#elif FLASH_PAGE_SIZE == 1056
   #define FLASH_VIRTUAL_PAGE       1024    // velikost stranky
   // stranka/buffer z linearni adresy :
   #define MkBufferAddress( a)     ((a) & 0x3FF)
   #define MkPageAddress( a)       ((word)((a) >> 10))
#else
   #error "Unknown FLASH DATA device"
#endif

// Virtualni kapacita pameti :
#define FLASH_SIZE ((dword)FLASH_PAGES * FLASH_VIRTUAL_PAGE)


//-----------------------------------------------------------------------------
#ifdef FLASH_NVRAM
   // nonvolatile buffer

   // struktura dat v NVRAM :
   typedef struct {
      word Page;                         // cislo stranky
      byte Data[ FLASH_VIRTUAL_PAGE];    // data stranky
   } TFlashCache;   // 514 bajtu (pro AT45DB161)
#endif
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void FlashInit( void);
// Inicializace

void FlashWriteStart( dword Address);
// Start zapisu od <Address>

void FlashWriteData( byte Data);
// Zapis na aktualni adresu a posun adresy

#ifndef FLASH_NVRAM
   // internal buffer
   #define FlashWriteDone() FlashWriteBufferDone()
   // Ukonceni zapisu
#else
   // nonvolatile buffer
   #define FlashWriteDone()
   // Ukonceni zapisu
#endif

#define FlashFlush()     FlashWriteStart( FLASH_FLUSH_ADDRESS)
// Vynuceny zapis stranky

byte FlashReadByte( dword Address);
// Jednorazove cteni bytu

void FlashWriteByte( dword Address, byte Value);
// Zapise byte vcetne Flush stranky

void FlashReadStart( dword Address);
// Start cteni od <Address>

byte FlashReadData( void);
// Cteni z aktualni adresy a posun adresy

#define FlashReadDone()  FlashCommonReadDone()
// Ukonceni cteni

#endif
