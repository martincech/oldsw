//*****************************************************************************
//
//    Iep.h  -  89C51 RD2 internal EEPROM services
//    Version 1.2, (c) Vymos
//
//*****************************************************************************

#ifndef __Iep_H__
   #define __Iep_H__

// Hardwarove konstanty :

#define IEP_PAGE_SIZE   64                // velikost stranky EEPROM
#define IEP_SIZE        2048              // kapacita EEPROM

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Rd2_H__
   #include "Rd2.h"
#endif


//-----------------------------------------------------------------------------
// Jednoduche cteni / zapis
//-----------------------------------------------------------------------------

byte IepRead( word address);
// Precte a vrati byte z interni EEPROM na adrese <address>

TYesNo IepWriteByte( word address, byte value);
// Zapise byte <value> do interni EEPROM na adresu <address>.
// Vraci NO neni-li zapis mozny


//-----------------------------------------------------------------------------
// Zapis stranky
//-----------------------------------------------------------------------------

// Pozor, hranice stranek hlida uzivatel API
// podle konstanty IEP_PAGE_SIZE

#ifdef __T89C51RD2__

   TYesNo IepPageWriteStart( void);
   // Zahajuje zapis do stranky, vraci NO nelze-li zapisovat

   void IepPageWriteData( word address, byte value);
   // Zapis bytu do stranky

   void IepPageWritePerform();
   // Zahaji fyzicky zapis stranky

#else
   // AT89C51ED2
   TYesNo IepPageWriteStart( void);
   // Zahajuje zapis do stranky, vraci NO nelze-li zapisovat

   #define IepPageWriteData( address, value) IepRawWriteByte( address, value);
   // Zapis bytu do stranky, jako zapis bytu

   #define IepPageWritePerform()

   void IepRawWriteByte( word address, byte value);
   // Zapise byte <value> do interni EEPROM na adresu <address>.
#endif // AT89C51ED2

#endif
