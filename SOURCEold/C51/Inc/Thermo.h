//*****************************************************************************
//
//    Thermo.h - Thermometer services module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Thermo_H__
   #define __Thermo_H__

#ifndef __Ds18x20_H__
   #include "Ds18x20.h"
#endif


#ifdef MwiDATA
   // starsi verze HW ma na tomto portu je ponekud nestastne
   // digitalni vystup DV8. Funkce zajisti definovany stav
   // na sbernici pred operacemi s DV8

   #define ThermoPrepareInputs()    MwiAttach()
   // funkce pro pripravu na cteni DV8
#endif

void ThermoInit( void);
// Inicializuje skupinu teplomeru

void ThermoMeasure( void);
// Zmeri skupinu teplot. Volat po 1s

char TempConvert1C(int temp);
// Prevod a zaokrouhleni teploty z interniho formatu na cele stupne

int TempConvert01C(int temp);
// Prevod a zaokrouhleni teploty z interniho formatu na desetiny stupne


#endif
