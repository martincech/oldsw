//*****************************************************************************
//
//    Ads1241.h  -  A/D convertor ADS1241 services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Ads1241_H__
   #define __Ads1241_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define ADC_COUNT 4                     // pocet vstupu prevodniku


#ifdef ADC_INTERRUPT                    // prevodnik v prerusovacim modu
// interni data :
extern byte   __adc_status__ _AdcStatus;  // stav mereni
#endif // ADC_INTERRUPT

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void AdcInit( void);
// Inicializace prevodniku

#define AdcIsReady()         (!AdcDRDY)
// Prevodnik ma pripravena data

void AdcSetup( void);
// Nastaveni prevodu, autokalibrace

void AdcSelectInput( byte Channel);
// Prepnuti multiplexoru
// parametr <Channel> je cislo kanalu 0.. ADC_COUNT-1

dword AdcReadValue( void);
// Precteni namerene hodnoty

//-----------------------------------------------------------------------------
#ifdef ADC_INTERRUPT       // prevodnik v prerusovacim modu


void AdcStart( void);
// Zahajeni periodickeho mereni

void AdcStop( void);
// Zastaveni periodickeho mereni

#define AdcDataReady( Channel)   (_AdcStatus & (1 << (Channel)))
// Test na nove zmerenou hodnotu
// parametr <Channel> je cislo kanalu 0.. ADC_COUNT-1

#define AdcAnyDataReady()        (_AdcStatus & 0x0F)
// Vraci YES, je-li zmereny libovolny kanal

dword AdcRead( byte Channel);
// Cteni merene hodnoty
// parametr <Channel> je cislo kanalu 0.. ADC_COUNT-1

#endif // ADC_INTERRUPT
//-----------------------------------------------------------------------------

#endif
