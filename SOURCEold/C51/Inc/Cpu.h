//*****************************************************************************
//
//    Cpu.h  -  89C51 basic macros
//    Version 1.1, (c) Vymos
//
//*****************************************************************************

// 17.8.2007: CKCON_SPIX2 byl spatne nadefinovan + patri do CKCON1 (ne do CKCON0)

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//** 03/2004 Rozsireno o AT89C51ED2

// interni makro na vypocet deliciho pomeru :
#define TimerDividier( ms, x2) (-((FXTAL * x2 / 12) * (ms) / 1000L))

// interni makro na vypocet baudu u casovace 2:
#define Baud2Dividier( bd)  (-(FXTAL * T2_X2 / 32 / (bd)))

//-----------------------------------------------------------------------------

#ifndef __AT89C2051__
// aktivace/refresh WatchDogu :
#define WatchDog()      { WDTRST = WDTRST_1; WDTRST = WDTRST_2; }
#endif

// Globalni preruseni :

#define DisableInts()   EA = 0
#define EnableInts()    EA = 1

// Nastaveni modu X2

#ifdef __T89C51RD2__
   #define TimingSetup()   CKCON =                           \
                           (CPU_X2 == 2 ? CKCON_X2    : 0) | \
                           (T0_X2  == 1 ? CKCON_T0X2  : 0) | \
                           (T1_X2  == 1 ? CKCON_T1X2  : 0) | \
                           (T2_X2  == 1 ? CKCON_T2X2  : 0) | \
                           (SI_X2  == 1 ? CKCON_SIX2  : 0) | \
                           (PCA_X2 == 1 ? CKCON_PCAX2 : 0) | \
                           (WD_X2  == 1 ? CKCON_WDX2  : 0)
#else
   // AT89C51ED2
   #define TimingSetup()   AUXR1 = AUXR1_NORMAL;             \
                           CKCON0 =                          \
                           (CPU_X2 == 2 ? CKCON_X2    : 0) | \
                           (T0_X2  == 1 ? CKCON_T0X2  : 0) | \
                           (T1_X2  == 1 ? CKCON_T1X2  : 0) | \
                           (T2_X2  == 1 ? CKCON_T2X2  : 0) | \
                           (SI_X2  == 1 ? CKCON_SIX2  : 0) | \
                           (PCA_X2 == 1 ? CKCON_PCAX2 : 0) | \
                           (WD_X2  == 1 ? CKCON_WDX2  : 0)
   // SPI_X2 do CKCON0 nepatri. Zatim nechavam SPI default v X2 modu (kvuli zpetne kompatibilite s Bat2)

   // Dynamicka zmena X2 modu CPU (jen pokud nedefinoval CPU_X2 natvrdo)
   #ifndef CPU_X2

     // Nastaveni X2 modu:
     #define StartX2()  CPU_X2 = 2;         \
                        CKCON0 |= CKCON_X2

     // Zastaveni X2 modu:
     #define StopX2()   CPU_X2 = 1;         \
                        CKCON0 &= ~CKCON_X2

   #endif // CPU_X2


#endif

// Nastaveni modu externi RAM :

#ifdef __T89C51RD2__
   #define EnableXRAM()    AUXR  = AUXR_AO | AUXR_XRS1024 | AUXR_M0         // 1kB RAM
#else
   // AT89C51ED2
   #define EnableXRAM()    AUXR  = AUXR_AO | AUXR_XRS2048 | AUXR_M0         // 2kB RAM
#endif

// Nastaveni periody casovace 0 :

#define SetTimer0( ms)       TL0 = TimerDividier( T0_X2, ms) & 0xFF; TH0 = TimerDividier( T0_X2, ms) >> 8

// Nastaveni periody casovace 1 :

#define SetTimer1( ms)       TL1 = TimerDividier( T1_X2, ms) & 0xFF; TH1 = TimerDividier( T1_X2, ms) >> 8

// Nastaveni casovace 1 jako generatoru baudove rychlosti :

#define SetTimer1Baud( bd)   TH1 = -(FXTAL / 12 * DOUBLE_BAUDRATE_T1 / 32 / (bd));

// Nastaveni periody casovace 2 :

#define SetTimer2( ms)       TL2 = TimerDividier( T2_X2, ms) & 0xFF; TH2 = TimerDividier( T2_X2, ms) >> 8

// Nastaveni casovace 2 jako generatoru baudove rychlosti :

#define SetTimer2Baud( bd)   RCAP2L = Baud2Dividier( bd) & 0xFF; RCAP2H = Baud2Dividier( bd) >> 8

// Spusteni casovace 0 (predtim povolit interrupt !) :

#define Timer0Run( ms)   \
   TMOD &= 0xF0; TMOD |= TMOD_NORMAL0; SetTimer0( ms); ET0  = 1; TR0  = 1;

// Spusteni casovace 1 (predtim povolit interrupt !) :

#define Timer1Run( ms)   \
   TMOD &= 0x0F; TMOD |= TMOD_NORMAL1; SetTimer1( ms); ET1  = 1; TR1  = 1;

// Spousteni funkce uvnitr interruptu casovace :

#define CheckTrigger( counter, expression)  \
                     if( counter){          \
                        if( !--(counter)){  \
                            expression;     \
                        }                   \
                     }                      \

#endif
