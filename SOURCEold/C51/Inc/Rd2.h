//*****************************************************************************
//
//    Rd2.h  -  89C51 RD2 register definitions
//    Version 1.1, (c) Vymos
//
//*****************************************************************************

#ifndef __Rd2_H__
   #define __Rd2_H__

//** 03/2004 Rozsireno o AT89C51ED2

// 17.8.2007: CKCON_SPIX2 byl spatne nadefinovan + patri do CKCON1 (ne do CKCON0)

//-----------------------------------------------------------------------------
// Masky registru procesoru RD2 :
//-----------------------------------------------------------------------------

// Registr CKCON (0x8F) :

#define CKCON_X2        0x01              // povoleni X2 rezimu
#define CKCON_T0X2      0x02              // timer 0 12 period
#define CKCON_T1X2      0x04              // timer 1 12 period
#define CKCON_T2X2      0x08              // timer 2 12 period
#define CKCON_SIX2      0x10              // serial  12 period
#define CKCON_PCAX2     0x20              // PCA 12 period
#define CKCON_WDX2      0x40              // WatchDog 12 period
#ifdef __AT89C51ED2__
   #define CKCON1_SPIX2 0x01              // SPI 12 period

   #define CKCON_X2_NORMAL (CKCON_X2 | CKCON_T0X2 | CKCON_T1X2 | CKCON_T2X2 | \
                            CKCON_SIX2 | CKCON_PCAX2 | CKCON_WDX2)
#else
   #define CKCON_X2_NORMAL (CKCON_X2 | CKCON_T0X2 | CKCON_T1X2 | CKCON_T2X2 | \
                            CKCON_SIX2 | CKCON_PCAX2 | CKCON_WDX2)
#endif

// Registr AUXR1 (0xA2) :

#define AUXR1_DPS       0x01              // second data pointer DPTR1
#define AUXR1_GF3       0x08              // general purpose flag
#ifdef __AT89C51ED2__
   #define AUXR1_ENBOOT 0x20              // enable BOOT ROM map
#endif

#define AUXR1_NORMAL       0              // disable BOOT ROM

// Registr AUXR  (0x8E) :

#define AUXR_AO         0x01              // ALE active only during MOVX, MOVC
#define AUXR_EXTRAM     0x02              // External data memory access (0-internal XRAM)
#define AUXR_XRS256     0x00              // 256 bytu XRAM
#define AUXR_XRS512     0x04              // 512 bytu XRAM
#define AUXR_XRS768     0x08              // 768 bytu XRAM
#define AUXR_XRS1024    0x0C              // 1024 bytu XRAM
#define AUXR_M0         0x20              // /RD and /WR pulse length 30 clock period (0-6 period)
#ifdef __AT89C51ED2__
   #define AUXR_XRS2048 0x1C              // 2048 bytu XRAM
   #define AUXR_DPU     0x80              // disable Weak pullup
#endif

// Registr T2MOD (0xC9) :

#define T2MOD_DCEN      0x01              // enable Timer 2 as up/down counter
#define T2MOD_T2OE      0x02              // P1.0/T2 as clock output (0-clock input or port)

// Registr CMOD ( 0xD9) :

#define CMOD_ECF        0x01              // PCA enable counter overflow interrupt
#define CMOD_CPS12      0x00              // PCA Internal clock fosc/12 ( fosc/6 in X2)
#define CMOD_CPS4       0x02              // PCA Internal clock fosc/4 ( fosc/2 in X2)
#define CMOD_CPST0      0x04              // PCA clock - timer 0 overflow
#define CMOD_CPSEXT     0x06              // PCA clock external P1.2/ECI (max fosc/8)
#define CMOD_WDTE       0x40              // PCA module 4 watchdog
#define CMOD_CIDL       0x80              // PCA counter off during IDLE Mode

// Registr CCON (0xD8) :

#define CCON_CCF0       0x01              // PCA module 0 interrupt flag
#define CCON_CCF1       0x02              // PCA module 1 interrupt flag
#define CCON_CCF2       0x04              // PCA module 2 interrupt flag
#define CCON_CCF3       0x08              // PCA module 3 interrupt flag
#define CCON_CCF4       0x10              // PCA module 4 interrupt flag
#define CCON_CR         0x40              // PCA counter run
#define CCON_CF         0x80              // PCA counter overflow flag

// Registry CCAPMn (0xDA, 0xDB, 0xDC, 0xDD, 0xDE) :

#define CCAPM_ECCF      0x01              // enable CCFn interrupt (in CCON)
#define CCAPM_PWM       0x02              // CEXn pin used as PWM output
#define CCAPM_TOG       0x04              // CEXn pin toggles on compare/capture
#define CCAPM_MAT       0x08              // match of the PCA and compare register sets CCFn bit
#define CCAPM_CAPN      0x10              // Negative edge capture
#define CCAPM_CAPP      0x20              // Positive edge capture
#define CCAPM_ECOM      0x40              // Enable comparator function

// Mody PCA (vcetne povoleni preruseni od modulu) :

#define CCAPM_STOP             0x00
#define CCAPM_POSITIVE_EDGE    (CCAPM_CAPP | CCAPM_ECCF)
#define CCAPM_NEGATIVE_EDGE    (CCAPM_CAPN | CCAPM_ECCF)
#define CCAPM_BOTH_EDGES       (CCAPM_CAPN | CCAPM_CAPP | CCAPM_ECCF)
#define CCAPM_SW_TIMER         (CCAPM_ECOM | CCAPM_MAT  | CCAPM_ECCF)

#define CCAPM_HIGH_SPEED       (CCAPM_ECOM | CCAPM_MAT  | CCAPM_TOG)   // bez preruseni
#define CCAPM_RUN_PWM          (CCAPM_ECOM | CCAPM_PWM)                // bez preruseni
#define CCAPM_WDOG             (CCAPM_ECOM | CCAPM_MAT)                // module 4  only



// Registr EECON (0xD2) :
#define EECON_EEBUSY    0x01              // probiha zapis do EEPROM
#define EECON_EEE       0x02              // chipselect EEPROM
#ifdef __T89C51RD2__
   // jen pro T89C51RD2
   #define EECON_EEPL1  0x50              // prvni kodove slovo zapisu
   #define EECON_EEPL2  0xA0              // druhe kodove slovo zapisu
#endif
// Registr EETIM (0xD3) :

// Normalni mod 5 * FXTAL [MHz] mod 2x 10 * FXTAL [Mhz]

// Registr IPH (0xB7) IPL (0xB8) MSB of interrupt priority (IP) :

#define IPH_PX0         0x01              // external int 0
#define IPH_PT0         0x02              // timer 0 overflow
#define IPH_PX1         0x04              // external int 1
#define IPH_PT1         0x08              // timer 1 overflow
#define IPH_PS          0x10              // serial port
#define IPH_PT2         0x20              // Timer 2 overflow
#define IPH_PPC         0x40              // PCA

// Registr WDTPRG (0xA7) :

#define WDTPRG_16       0x00              // 2^14 machine cycles, 16.3ms @ 12MHz
#define WDTPRG_32       0x01              // 2^15 machine cycles, 32.7ms @ 12MHz
#define WDTPRG_65       0x02              // 2^16 machine cycles, 65.5ms @ 12MHz
#define WDTPRG_131      0x03              // 2^17 machine cycles, 131ms @ 12MHz
#define WDTPRG_262      0x04              // 2^18 machine cycles, 262ms @ 12MHz
#define WDTPRG_542      0x05              // 2^19 machine cycles, 542ms @ 12MHz
#define WDTPRG_1050     0x06              // 2^20 machine cycles, 1.05s @ 12MHz
#define WDTPRG_2090     0x07              // 2^21 machine cycles, 2.09s @ 12MHz

// Registr WDTRST (0xA6) :

#define WDTRST_1        0x1E              // prvni byte watchdog
#define WDTRST_2        0xE1              // druhy byte watchdog

// Registr PCON ( 0x87) :

#define PCON_IDL        0x01              // Enter IDLE Mode
#define PCON_PD         0x02              // Enter Power Down Mode
#define PCON_GF0        0x04              // General purpose flag
#define PCON_GF1        0x08              // General purpose flag
#define PCON_POF        0x10              // Power off flag (VCC rises form 0 to nominal)
#define PCON_SMOD0      0x40              // Serial port mode bit 0 - select FE in SCON (0-SM0 in SCON)
#define PCON_SMOD1      0x80              // Serial port mode bit 1 - double baudrate in mode 1,2,3

//-----------------------------------------------------------------------------
// Prerusovaci system
//-----------------------------------------------------------------------------

// Cisla preruseni :

#define INT_EXTERNAL0      0              // externi preruseni 0, vektor 0x03
#define INT_TIMER0         1              // preruseni od casovace 0, vektor 0x0B
#define INT_EXTERNAL1      2              // externi preruseni 1, vektor 0x13
#define INT_TIMER1         3              // preruseni od casovace 1, vektor 0x1B
#define INT_SERIAL         4              // preruseni od COMu, vektor 0x23
// 89C52
#define INT_TIMER2         5              // preruseni od casovace 2, vektor 0x2B
// RD2
#define INT_PCA            6              // preruseni od PCA, vektor 0x33
// ED2
#define INT_KBD            7              // preruseni od klavesnice, vektor 0x3B
#define INT_SPI            9              // preruseni od SPI, vektor 0x4B

// Registr TCON - externi preruseni :

#define TCON_IT0           0x01           // Preruseni hranou/0-hladinou od /INT0
#define TCON_IE0           0x02           // aktivovan /INT0
#define TCON_IT1           0x04           // Preruseni hranou/0-hladinou od /INT1
#define TCON_IE1           0x08           // aktivovan /INT1

// Registr IP (0xB8) interrupt priority :

#define IP_PX0             0x01           // external int 0
#define IP_PT0             0x02           // timer 0 overflow
#define IP_PX1             0x04           // external int 1
#define IP_PT1             0x08           // timer 1 overflow
#define IP_PS              0x10           // serial port
#define IP_PT2             0x20           // Timer 2 overflow
#define IP_PPCH            0x40           // PCA
// ED2 IPx1 (0xB2, 0xB3) interrupt priority :
#define IP_KBD             0x01           // KBD
#define IP_SPI             0x04           // SPI

// Registr IE (0xA8) interrupt enable :

#define IE_EX0             0x01           // povoleni /INT0
#define IE_ET0             0x02           // povoleni casovace 0
#define IE_EX1             0x04           // povoleni /INT1
#define IE_ET1             0x08           // povoleni casovace 1
#define IE_ES              0x10           // povoleni COMu
#define IE_EA              0x80           // enable all
// 89C52
#define IE_ET2             0x20           // povoleni casovace 2
// RD2
#define IE_EC              0x40           // povoleni PCA

// ED2 Registr IEN1 (0xB1) interrupt enable :
#define IEN1_KBD           0x01           // povoleni KBD
#define IEN1_SPI           0x04           // povoleni SPI

//-----------------------------------------------------------------------------
// Casovace 0,1
//-----------------------------------------------------------------------------

// Registr TMOD :

// casovac 0
#define TMOD_G0            0x08           // Hradlovani ze vstupu /INT0, 0-programove TR0
#define TMOD_CT0           0x04           // Citani ze vstupu T0, 0-z internich hodin/12
#define TMOD_M130          0x00           // 13 bitovy citac
#define TMOD_M160          0x01           // 16 bitovy citac
#define TMOD_M80           0x02           // 8 bitovy citac RELOAD mode
#define TMOD_MMIXED0       0x03           // TL0 je Timer 0, TH0 je Timer 1, TL1+TH1 nepouzity
// casovac 1
#define TMOD_G1            0x80           // Hradlovani ze vstupu /INT0, 0-programove TR0
#define TMOD_CT1           0x40           // Citani ze vstupu T0, 0-z internich hodin/12
#define TMOD_M131          0x00           // 13 bitovy citac
#define TMOD_M161          0x10           // 16 bitovy citac
#define TMOD_M81           0x20           // 8 bitovy citac RELOAD mode
#define TMOD_MMIXED1       0x30           // TL0 je Timer 0, TH0 je Timer 1, TL1+TH1 nepouzity

#define TMOD_NORMAL0  TMOD_M160          // casovac 0 16 bitu z vnitrnich hodin
#define TMOD_NORMAL1  TMOD_M161          // casovac 1 16 bitu z vnitrnich hodin

// Registr TCON - casovace :

#define TCON_TR0           0x10           // spusteni casovace 0
#define TCON_TF0           0x20           // pretok casovace 0
#define TCON_TR1           0x40           // spusteni casovace 1
#define TCON_TF1           0x80           // pretok casovace 1

//-----------------------------------------------------------------------------
// Casovac 2
//-----------------------------------------------------------------------------

// Registr T2CON (0xC8) :

#define T2CON_CP_RL2       0x01           // zachyceni negativni hranou T2EX, 0-auto reload
#define T2CON_C_T2         0x02           // Citani ze vstupu T2, 0-z internich hodin
#define T2CON_TR2          0x04           // spusteni casovace 2
#define T2CON_EXEN2        0x08           // povoleni T2EX pinu pro capture/reload
#define T2CON_TCLK         0x10           // Tx pouzij casovac 2 pro COM, 0-casovac 1
#define T2CON_RCLK         0x20           // Rx pouzij casovac 2 pro COM, 0-casovac 1
#define T2CON_EXF2         0x40           // preruseni od capture/reload
#define T2CON_TF2          0x80           // pretok casovace 2

#define T2CON_COM          (T2CON_TCLK | T2CON_RCLK | T2CON_TR2)  // pouziti casovace 2 pro COM

// Registr T2MOD (0xC9) :

#define T2MOD_DCEN          0x01          // povoleni casovace 2 up/down
#define T2MOD_T2OE          0x02          // P1.0/T2 as clock output, 0-port/input

//-----------------------------------------------------------------------------
// COM
//-----------------------------------------------------------------------------

// Registr SCON (0x98) :

#define SCON_RI            0x01           // Rx nacten znak
#define SCON_TI            0x02           // Tx je mozne vyslat znak
#define SCON_RB8           0x04           // Rx prijaty 9. bit
#define SCON_TB8           0x08           // Tx vysilany 9. bit
#define SCON_REN           0x10           // povoleni prijmu
#define SCON_SM2           0x20           // serial mode bit 2 - multiprocessor COM
#define SCON_SM1           0x40           // serial mode bit 1
#define SCON_SM0           0x80           // serial mode bit 0 (PCON_SMOD0 musi byt v 0)
#define SCON_SMFIXED8      0x00           // serial mode 0 - shift+clock/12
#define SCON_SMTIMER8      0x40           // serial mode 1 - 8 bit+timer (viz T2CON)
#define SCON_SMFIXED9      0x80           // serial mode 2 - 9 bit+clock/64 nebo 32
#define SCON_SMTIMER9      0xC0           // serial mode 3 - 9 bit+timer (viz T2CON)
// RD2
#define SCON_FE            SCON_SM0       // cteni framing error FE (povoluje se PCON_SMOD0=1)

#define SCON_NORMAL        (SCON_SMTIMER8 | SCON_REN) // normalni COM 8 bitu, rizeni z casovace
#define SCON_PARITY        (SCON_SMTIMER9 | SCON_REN) // normalni COM 9 bitu, rizeni z casovace

#ifdef __AT89C51ED2__
//-----------------------------------------------------------------------------
// SPI
//-----------------------------------------------------------------------------

// Registr SPCON (0xC3) :

#define SPCON_SPR0         0x01           // baud 0
#define SPCON_SPR1         0x02           // baud 1
#define SPCON_CPHA         0x04           // clock phase 0-leave, 1-return
#define SPCON_CPOL         0x08           // clock polarity 0-L, 1-H
#define SPCON_MSTR         0x10           // master
#define SPCON_SSDIS        0x20           // disable SS
#define SPCON_SPEN         0x40           // enable SPI
#define SPCON_SPR2         0x80           // baud 2

// Registr SPSTA (0xC4) :

#define SPSTA_MODF         0x10           // mode fault (SS level)
#define SPSTA_SSERR        0x20           // slave SS fail
#define SPSTA_WCOL         0x40           // write collision
#define SPSTA_SPIF         0x80           // transfer completed

#endif // ED2

#endif
