//*****************************************************************************
//
//    Fifo.h - FIFO EEPROM module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Fifo2_H__
   #define __Fifo2_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Komunikacni struktura pro uzivatele modulu :

extern TFifo2Data __xdata__ Fifo2Data;     // zde jen pro kontrolu, musi byt soucasti projektu !


void Fifo2Init( void);
// Inicializuje modul

TYesNo Fifo2Write( void);
// Zapise globalni komunikacni strukturu do FIFO

void Fifo2Read( word address);
// Naplni komunikacni strukturu daty z <address>

word Fifo2FindMarker( void);
// Vrati adresu markeru

TYesNo Fifo2Reset( void);
// Zalozi prazdne FIFO

#endif
