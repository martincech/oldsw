//*****************************************************************************
//
//   Spi.h       SPI interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Spi_H__
   #define __Spi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SpiInit( void);
// Inicializace sbernice

#ifndef SPI_SIMPLE_CS
   // chipselect pomoci funkce

   void SpiAttach( void);
   // Nastaveni sbernice a select zarizeni

   void SpiRelease( void);
   // Uvolneni sbernice, deselect
#endif // else chipselect makrem

byte SpiReadByte();
// Precte byte

void SpiWriteByte(byte Value);
// Zapise byte

#endif
