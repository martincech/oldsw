//*****************************************************************************
//
//    Ad7714.h  -  AD7714 services
//    Verze 1.0
//
//*****************************************************************************

#ifndef __Ad7714_H__
   #define __Ad7714_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Konstanty pro volbu zesileni - ORuje se do setup registru, jde o bity G2, G1 a G0
#define ADC_GAIN_1         0x00
#define ADC_GAIN_2         0x04
#define ADC_GAIN_4         0x08
#define ADC_GAIN_8         0x0C
#define ADC_GAIN_16        0x10
#define ADC_GAIN_32        0x14
#define ADC_GAIN_64        0x18
#define ADC_GAIN_128       0x1C
// Vyber unipolarniho/bipolarniho modu - ORuje se do filter high registru, jde o bit B/U
#define ADC_UNIPOLAR_MODE  0x80
#define ADC_BIPOLAR_MODE   0x00
// Maska pro Current Boost bit ve Filter High registru
#define ADC_CURRENT_BOOST  0x20



bit AdcReady(unsigned char Vstup);
// Pokud je novy prevod u daneho vstupu, vrati 1, jinak 0. Nahrazuje primy test pinu DRDY a nacita jeho stav z registru. Akorat je treba zadavat i vstup.

void AdcInit();
// Inicializuje sbernice a kalibruje prevodnik podle nastavenych parametru

// Nyni jako makro
//void AdcSetInput(unsigned char Vstup);
// Nastavi prevodnik na zadany vstup

void AdcSetInputNoWait(unsigned char Vstup);
// Nastavi vstup prevodniku, ale neceka, az se vstup prepne - to si musi osetrit sam uzivatel pomoci fce AdcReady().

unsigned long AdcRead(unsigned char Vstup);
// Adresuje kanal <Vstup>, vrati prectenou hodnotu

// Ceka, az je prevodnik pripraven
// Musim rozlisovat cislo vstupu, jinak to bude prepinat a bude to pomale
#define AdcWait(Vstup) while (!AdcReady(Vstup));

#define AdcSetInput(Vstup) AdcWait(Vstup)


#endif

