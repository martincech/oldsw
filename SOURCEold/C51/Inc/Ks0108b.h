//*****************************************************************************
//
//    Ks0108b.hw  - Radic displeje Samsung Ks0108b
//    Verze 1.0
//
//*****************************************************************************

#ifndef __Ks0108b_H__
   #define __Ks0108b_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

extern TYesNo Driver;       // Cislo radice (0 = CS1, 1 = CS2)


void KsWriteCommand(byte Command);
  // Zapis prikazu

void KsWriteData(byte Data);
  // Zapis dat

byte KsReadData();
  // Nacteni dat

void KsInit();
  // Inicializace radice

void KsOn();
  // Posle do displeje prikaz Display ON, aby zacal zobrazovat


#endif
