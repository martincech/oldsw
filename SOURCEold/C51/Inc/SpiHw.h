//*****************************************************************************
//
//   SpiHw.h     ED2 SPI interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __SpiHw_H__
   #define __SpiHw_H__

// Baudova rychlost :

#define SPI_CLOCK_2         0x00           // peripheral clock / 2
#define SPI_CLOCK_4         0x01           // peripheral clock / 4
#define SPI_CLOCK_8         0x02           // peripheral clock / 8
#define SPI_CLOCK_16        0x03           // peripheral clock / 16
#define SPI_CLOCK_32        0x80           // peripheral clock / 32
#define SPI_CLOCK_64        0x81           // peripheral clock / 64
#define SPI_CLOCK_128       0x82           // peripheral clock / 128

// Klidova uroven SCK :

#define SPI_CLOCK_L         0x00           // klidova uroven SCK = L
#define SPI_CLOCK_H         0x08           // klidova uroven SCK = H

// Strobovani hranou SCK :

#define SPI_EDGE_LEAVE      0x00           // strobovani pri opusteni klidove urovne
#define SPI_EDGE_RETURN     0x04           // strobovani pri navratu do klidove urovne

#define SpiEnable()         SPCON |=  SPCON_SPEN          // enable SPI
#define SpiDisable()        SPCON &= ~SPCON_SPEN          // disable SPI

// ostatni viz spi.h

#endif
