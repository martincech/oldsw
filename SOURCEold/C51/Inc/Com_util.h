//******************************************************************************
//
//   COM_util.h   Serial communication utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __COM_util_H__
   #define __COM_util_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void ComTxDigit( byte n);
// vyslani cislice 0..9

void ComTxString( char code *Text);
// Vysle retezec

void ComTxXString( char xdata *Text);
// Vysle retezec

void ComTxHex( byte Number);
// Vysle hexadecimalni reprezentaci bytu

void ComTxDec( byte Number);
// Vysle dekadickou reprezentaci bytu

void ComTxWord( word Number);
// Vysle dekadickou reprezentaci word 0..9999

byte ComRxDec( void);
// Cte cislo po prvni nenumericky znak

byte ComRxHex( void);
// Cte dva znaky, jako hexa reprezentaci

TYesNo ComWaitChar( char ch);
// Ceka na znak <ch>

void ComSkipChars( byte Count);
// Preskoci pocet znaku <Count>

TYesNo ComRxMatch( char code *Text, byte Timeout);
// Cte ocekavany retezec, vraci YES souhlasi-li

TYesNo ComRxString( char xdata *Text, byte Length);
// Cte retezec v uvozovkach

byte ComRxDelimiter( char xdata *Text, byte Length, char Delimiter);
// Cte retezec az po znak <Delimiter>, vraci delku

#endif




