//*****************************************************************************
//
//    Ififo.h - FIFO in internal EEPROM module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Ififo_H__
   #define __Ififo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Komunikacni struktura pro uzivatele modulu :

extern TIfifoData __xdata__ IfifoData;     // zde jen pro kontrolu, musi byt soucasti projektu !


void IfifoInit( void);
// Inicializuje modul

TYesNo IfifoWrite( void);
// Zapise globalni komunikacni strukturu do FIFO

void IfifoRead( word address);
// Naplni komunikacni strukturu daty z <address>

word IfifoFindMarker( void);
// Vrati adresu markeru

TYesNo IfifoReset( void);
// Zalozi prazdne FIFO

#endif
