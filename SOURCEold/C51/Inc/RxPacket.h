//*****************************************************************************
//
//    RxPacket.h  - Packet receiver
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __RxPacket_H__
   #define __RxPacket_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Globalni data :

extern TPacket __xdata__ Packet;
extern bit _PacketReady;


TYesNo RxPacketInit( void);
// Inicializace komunikace

TYesNo RxPacketCrc( void);
// Kontroluje zabezpeceni paketu

//TYesNo RxPacketReady( void);
// Je pripraven prijaty paket

#define RxPacketReady()   (_PacketReady)

//void RxPacketDone( void);
// Paket zpracovan

#define RxPacketDone()    (_PacketReady = NO)

#endif
