//*****************************************************************************
//
//    Rfifo.c - FIFO with external RAM in Ds17287
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Rfifo_H__
   #define __Rfifo_H__

#include "Hardware.h"

// Komunikacni struktura pro uzivatele modulu :

extern TRfifoData __xdata__ RfifoData;     // zde jen pro kontrolu, musi byt soucasti projektu !


void RfifoInit( void);
// Inicializuje modul

TYesNo RfifoWrite( void);
// Zapise globalni komunikacni strukturu do FIFO

void RfifoRead( word address);
// Naplni komunikacni strukturu daty z <address>

word RfifoFindMarker( void);
// Vrati adresu markeru

TYesNo RfifoReset( void);
// Zalozi prazdne FIFO

TYesNo RfifoUpdate( word Address);
// Naplni komunikacni strukturu daty z <address>

word RfifoFindFirst(void);
// Vrati adresu prvniho zaznamu

word RfifoFindLast(void);
// Vrati adresu posledniho zaznamu

word RfifoFindNext(word Address);
// Vrati adresu nasledujici polozky za polozkou <Address>. Pokud ve fifo zadny zaznam neni (tj. fifo je prazdne), vrati adresu XFIFO_INVALID_ADDRESS.

word RfifoFindPrevious(word Address);
// Vrati adresu predchozi polozky pred polozkou <Address>. Pokud ve fifo zadny zaznam neni (tj. fifo je prazdne), vrati adresu XFIFO_INVALID_ADDRESS.

#endif
