//*****************************************************************************
//
//   Bat2Dat.h   Bat2 bridge data definitions
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bat2Dat_H__
   #define __Bat2Dat_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif


//-----------------------------------------------------------------------------
// Statistics
//-----------------------------------------------------------------------------

typedef struct {
   word  Target;
   word  Count;
   word  Average;
   int32 Gain;
   word  Sigma;
   word  Uniformity;
   word  LastAverage;
} TBat2Statistics;

//-----------------------------------------------------------------------------
// Weight
//-----------------------------------------------------------------------------

typedef word TBat2Weight;

//-----------------------------------------------------------------------------
// Male/Female data response
//-----------------------------------------------------------------------------

#define BAT2_WEIGHT_COUNT  50                    // last weighings count

typedef struct {
   TBat2Statistics Statistics;
   TBat2Weight     Weight[ BAT2_WEIGHT_COUNT];
} TBat2Data;

#endif
