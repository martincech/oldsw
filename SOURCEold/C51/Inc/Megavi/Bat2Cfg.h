//*****************************************************************************
//
//   Bat2Cfg.h    Bat2 bridge configuration definitions
//   Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bat2Cfg_H__
   #define __Bat2Cfg_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __Bat2Sms_H__
   #include "Bat2Sms.h"
#endif

#define BAT2_PHONEBOOK_SIZE 5          // Bat2 phonebook numbers count

//-----------------------------------------------------------------------------
// Commands
//-----------------------------------------------------------------------------

typedef struct {
   byte BridgeAddress;                 // RS485 bridge address
   byte ReplyTimeout;                  // single SMS request timeout [min]
   byte BroadcastTimeout;              // broadcast SMS timeout [min]
   byte MegaviTimeout;                 // SMS request MEGAVI timeout[ s]
   byte PhoneBookSize;                 // valid phonebook items count
   char Phone[ BAT2_PHONEBOOK_SIZE][ BAT2_SMS_PHONE_NUMBER_SIZE];      // phonebook numbers
} TBat2Configuration;

// WARNING : check for endian swap on data item size > byte/char

#endif
