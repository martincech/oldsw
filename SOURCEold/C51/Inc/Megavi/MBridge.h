//*****************************************************************************
//
//    MBridge.h     Bat2Bridge protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __MBridge_H__
   #define __MBridge_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __BridgeDef_H__
   #include "BridgeDf.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void MBridgeInit( void);
// Communication initialisation

TYesNo MBridgeConfigurationSet( TBat2Configuration __xdata__ *Configuration);
// Send <Configuration> data

//-----------------------------------------------------------------------------

TYesNo MBridgeMaleDataSet( TBat2Data __xdata__ *MaleData);
// Send <MaleData>

TYesNo MBridgeFemaleDataSet( TBat2Data __xdata__ *FemaleData);
// Send <FemaleData>

//-----------------------------------------------------------------------------

TYesNo MBridgeSmsCount( byte data *SmsCount);
// Get SMS count

TYesNo MBridgeSmsGet( byte __xdata__ *Phone, byte __xdata__ *Text);
// Get current SMS for send

TYesNo MBridgeSmsStatusSet( byte SendOk);
// Set sended SMS status to <SendOk> (moves current SMS to next position)

//-----------------------------------------------------------------------------

TYesNo MBridgeSmsReceived( char __xdata__ *Phone, char __xdata__ *Text);
// Send received SMS <Phone> and <Text>

#endif
