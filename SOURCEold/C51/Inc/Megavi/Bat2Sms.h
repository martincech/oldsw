//*****************************************************************************
//
//   Bat2Sms.h    Bat2 SMS protocol definitions
//   Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bat2Sms_H__
   #define __Bat2Sms_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif


#define BAT2_SMS_PHONE_NUMBER_SIZE  15           // phone digits count
#define BAT2_SMS_SIZE               160          // characters count per SMS

#endif
