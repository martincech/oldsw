//*****************************************************************************
//
//   BridgeDf.h    Bat2 Bridge protocol
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __BridgeDf_H__
   #define __BridgeDf_H__

#ifndef __Bat2Cfg_H__
   #include "Bat2Cfg.h"
#endif

#ifndef __Bat2Dat_H__
   #include "Bat2Dat.h"
#endif

#ifndef __Bat2Sms_H__
   #include "Bat2Sms.h"
#endif

// Rem : shorter phone number or SMS message filled with '\0' characters
//       up to total size

//-----------------------------------------------------------------------------
// Protocol
//-----------------------------------------------------------------------------

// <Leader><Size1><Size2><DATA><CRC>
//
// CRC is bit inversion of Sum <Size1>,<Size2>,<DATA>

#define BRIDGE_LEADER_CHAR    0x55

#define BRIDGE_LEADER_OFFSET  0
#define BRIDGE_SIZE1_OFFSET   1
#define BRIDGE_SIZE2_OFFSET   2
#define BRIDGE_DATA_OFFSET    3
#define BRIDGE_CRC_OFFSET     3

#define BRIDGE_FRAME_SIZE     4

// SPI request for reply data :
#define BRIDGE_QUERY_CHAR     0xAA

//-----------------------------------------------------------------------------
// Commands
//-----------------------------------------------------------------------------

typedef enum {
   BRIDGE_COMMAND_UNDEFINED,

   BRIDGE_COMMAND_CONFIGURATION_SET,        // set bridge configuration

   BRIDGE_COMMAND_MALE_DATA_SET,            // update Male data
   BRIDGE_COMMAND_FEMALE_DATA_SET,          // update Female data

   BRIDGE_COMMAND_SMS_COUNT_GET,            // get pending SMS count
   BRIDGE_COMMAND_SMS_PHONE_GET,            // get first pending SMS Phone number
   BRIDGE_COMMAND_SMS_TEXT_GET,             // get first pending SMS Text
   BRIDGE_COMMAND_SMS_STATUS_SET,           // set first pending SMS status

   BRIDGE_COMMAND_SMS_PHONE_SET,            // received SMS Phone number
   BRIDGE_COMMAND_SMS_TEXT_SET,             // received SMS Text
   _BRIDGE_COMMAND_LAST
} EBridgeCommand;

//-----------------------------------------------------------------------------
// Simple command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;
} TBridgeCommandSimple;

typedef TBridgeCommandSimple TBridgeCommandSmsCountGet;
typedef TBridgeCommandSimple TBridgeCommandSmsPhoneGet;
typedef TBridgeCommandSimple TBridgeCommandSmsTextGet;

//-----------------------------------------------------------------------------
// Send configuration
//-----------------------------------------------------------------------------

typedef struct {
   byte               Command;
   TBat2Configuration Configuration;
} TBridgeCommandConfigurationSet;

//-----------------------------------------------------------------------------
// Send data
//-----------------------------------------------------------------------------

typedef struct {
   byte      Command;
   TBat2Data Data;
} TBridgeCommandDataSet;

//-----------------------------------------------------------------------------
// Send SMS status
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;
   byte Slot;
   byte Status;              // YES - delivered, NO - unable deliver
} TBridgeCommandSmsStatusSet;

//-----------------------------------------------------------------------------
// Send SMS phone
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;
   char Phone[ BAT2_SMS_PHONE_NUMBER_SIZE];      // destination phone number
} TBridgeCommandSmsPhoneSet;

//-----------------------------------------------------------------------------
// Send SMS text
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;
   char Text[ BAT2_SMS_SIZE];                    // SMS text
} TBridgeCommandSmsTextSet;

//-----------------------------------------------------------------------------
// Command union
//-----------------------------------------------------------------------------

typedef union {
   byte                           Command;
   TBridgeCommandConfigurationSet ConfigurationSet;
   TBridgeCommandDataSet          DataSet;
   TBridgeCommandSmsCountGet      SmsCountGet;
   TBridgeCommandSmsPhoneGet      SmsPhoneGet;
   TBridgeCommandSmsTextGet       SmsTextGet;
   TBridgeCommandSmsStatusSet     SmsStatusSet;
   TBridgeCommandSmsPhoneSet      SmsPhoneSet;
   TBridgeCommandSmsTextSet       SmsTextSet;
} TBridgeCommand;

//-----------------------------------------------------------------------------
// Replies
//-----------------------------------------------------------------------------

#define BRIDGE_REPLY_BASE  0x80
#define BRIDGE_REPLY_ERROR 0xFF

typedef enum {
   BRIDGE_REPLY_UNDEFINED = BRIDGE_REPLY_BASE,

   BRIDGE_REPLY_CONFIGURATION_SET,

   BRIDGE_REPLY_MALE_DATA_SET,
   BRIDGE_REPLY_FEMALE_DATA_SET,

   BRIDGE_REPLY_SMS_COUNT_GET,
   BRIDGE_REPLY_SMS_PHONE_GET,
   BRIDGE_REPLY_SMS_TEXT_GET,
   BRIDGE_REPLY_SMS_STATUS_SET,

   BRIDGE_REPLY_SMS_PHONE_SET,
   BRIDGE_REPLY_SMS_TEXT_SET,

   _BRIDGE_REPLY_LAST
} EBridgeReply;

//-----------------------------------------------------------------------------
// Simple reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;
} TBridgeReplySimple;

typedef TBridgeReplySimple TBridgeReplyConfigurationSet;
typedef TBridgeReplySimple TBridgeReplyDataSet;
typedef TBridgeReplySimple TBridgeReplySmsStatusSet;
typedef TBridgeReplySimple TBridgeReplySmsPhoneSet;
typedef TBridgeReplySimple TBridgeReplySmsTextSet;

//-----------------------------------------------------------------------------
// Reply SMS count
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;
   byte Count;
} TBridgeReplySmsCountGet;

//-----------------------------------------------------------------------------
// Reply SMS phone get
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;
   byte Slot;
   char Phone[ BAT2_SMS_PHONE_NUMBER_SIZE];      // destination phone number
} TBridgeReplySmsPhoneGet;

#define BRIDGE_SLOT_INVALID 0xFF

//-----------------------------------------------------------------------------
// Reply SMS text get
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;
   byte Slot;
   char Text[ BAT2_SMS_SIZE];                    // SMS text
} TBridgeReplySmsTextGet;

//-----------------------------------------------------------------------------
// Reply union
//-----------------------------------------------------------------------------

typedef union {
   byte                         Reply;

   TBridgeReplyConfigurationSet ConfigurationSet;

   TBridgeReplyDataSet          DataSet;

   TBridgeReplySmsCountGet      SmsCountGet;
   TBridgeReplySmsPhoneGet      SmsPhoneGet;
   TBridgeReplySmsTextGet       SmsTextGet;
   TBridgeReplySmsStatusSet     SmsStatusSet;

   TBridgeReplySmsPhoneSet      SmsPhoneSet;
   TBridgeReplySmsTextSet       SmsTextSet;
} TBridgeReply;

#endif
