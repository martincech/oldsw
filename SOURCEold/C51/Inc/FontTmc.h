//*****************************************************************************
//
//    Font.h - Font services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Font_H__
   #define __Font_H__

#ifndef __Uni_H__
   #include "uni.h"
#endif

// Specialni znaky :

#define CH_NULL       0x00     // znak, ktery je v podstate mezera, ale neprepise podklad

// Znaku pro kresleni ramecku :

typedef enum {
   CH_UL,                      // horni levy roh
   CH_UR,                      // horni pravy roh
   CH_LL,                      // dolni levy roh
   CH_LR,                      // pravy dolni roh

   CH_TOP,                     // horni vodorovna hrana
   CH_LEFT,                    // leva svisla hrana

   _CH_FRAME_SHORT,            // ! nepouzivat, delka pro symetricke ramecky

   CH_BOTTOM = CH_FRAME_SHORT, // dolni vodorovna hrana
   CH_RIGHT,                   // prava svisla hrana

   _CH_FRAME_LAST              // ! nepouzivat, delka cele sekce
};

// Ofsety pro ruzne typy ramecku :

typedef enum {
   FRAME_THIN          = 0x20,                          // tenky ramecek, zaciname 0x20 abychom se vyhnuli ridicim znakum
   FRAME_THICK         = FRAME_THIN + _CH_FRAME_SHORT   // tlusty ramecek
};

// Popisovac fontu :

typedef struct {
   byte width;               // sirka v pixelech
   byte height;              // vyska v pixelech
   byte offset;              // zacatek fontu
   byte minus;               // Pozice znaku '-' v poli znaku
   byte space;               // Pozice znaku mezera v poli znaku
   byte dot;                 // Pozice tecky
   byte colon;               // Pozice dvojtecky
   // Pro akceleraci a zkraceni kodu by se hodilo :
   // byte_t bwidth;           // sirka fontu v bytech
   // byte_t blength;          // delka jednoho znaku v bytech
   byte code *array;         // pole bytu fontu
} TFontDescriptor;


// Jednotlive fonty :

typedef enum {
//   FONT_VGA_8x16,
   FONT_SYSTEM,
   FONT_FRAME_8x8,
   FONT_NUM_MYRIAD40,   // Numericky neproporcionalni font Myriad 40
   FONT_NUM_MYRIAD32,   // Numericky neproporcionalni font Myriad 23
   FONT_NUM_TAHOMA18,   // Numericky neproporcionalni font Tahoma 18
   _FONT_LAST
};

// Popisovace jednotlivych fontu :
extern TFontDescriptor code FontFrame;
extern TFontDescriptor code FontSystem;
extern TFontDescriptor code FontNumericMyriad40;
extern TFontDescriptor code FontNumericMyriad32;
extern TFontDescriptor code FontNumericTahoma18;

#define TAHOMA18_HEIGHT 28              // Vyska proporcionalniho fontu Tahoma18 v pixelech
extern byte code FONT_TAHOMA18[192][75];


// Vyctovy typ typu fontu
typedef enum {
  FONT_TAHOMA8,
  FONT_LUCIDA6,
  FONT_MYRIAD32,
  FONT_MYRIAD40
} TCisloFontu;

// Vyctovy typ pro mod zobrazeni fontu
typedef enum {
  FONT_MOD_NORMALNI,
  FONT_MOD_RYCHLY,
  FONT_MOD_INVERZNI,
  FONT_MOD_RYCHLY_A_INVERZNI,
  FONT_MOD_XOR
} TTypZobrazeniFontu;

// Globalni struktura fontu
typedef struct {
  unsigned char Id;                // Cislo fontu, podle ktereho se dostanu na definici
  unsigned char code *Pole;        // Ukazatel na definici
  unsigned char Vyska;             // Vyska znaku v pixelech
  unsigned char PocetBajtuNaZnak;  // Pocet bajtu na 1 znak, abych byl schopen odkrokovat na zadany znak
  TTypZobrazeniFontu Mod;          // Flag, zda se ma font vykreslovat rychle (prekresli pozadi) nebo standardne (ORuje se s pozadim)
  unsigned char Mezera;            // Sirka mezery mezi jednotlivymi znaky ve vete
  unsigned char MezeraZaCarkou;    // Sirka mezery za desetinnou carkou, pouziva se ve fci ZobrazCislo()
} TFont;
extern TFont xdata Font;



#endif
