//*****************************************************************************
//
//    QStat.h  -   Fixed point statistics utility
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Stat_H__
   #define __Stat_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Popisovac statistiky :

typedef struct {
   qword XSuma;
   qword X2Suma;
   word  Count;
} TStatistic;  // 10 bajtu

//-----------------------------------------------------------------------------

void StatClear( TStatistic __xdata__ *statistic);
// smaze statistiku

void StatAdd( TStatistic __xdata__ *statistic, dword x);
// prida do statistiky

dword StatAverage( TStatistic __xdata__ *statistic);
// vrati stredni hodnotu

float StatSigma( TStatistic __xdata__ *statistic);
// vrati smerodatnou odchylku

float StatVariation( dword average, float sigma);
// vrati variaci (procentni smerodatna odchylka)

float StatUniformity( dword average, float sigma, byte UniformityWidth);
// vrati procentualni pocet vzorku padajicich do rozsahu stredni
// hodnoty +-UniformityWidth*100%

float Gauss( float x);
// gaussova krivka

float GaussIntegral( float x, float gaussx);
// gaussova krivka integrovana od <x> do +oo, <gaussx> je hodnota gaussovy
// krivky pro <x>

void StatRealUniformityInit(dword Min, dword Max, dword Average);
// Inicializuje globalni promenne

void StatRealUniformityAdd(dword Number);
// Prida novy vzorek do uniformity

byte StatRealUniformityGet();
// Vypocte uniformitu v celych procentech

#endif
