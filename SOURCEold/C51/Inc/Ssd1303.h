//*****************************************************************************
//
//    Ssd1303.c  -  Display driver SSD1303
//    Version 1.0
//
//*****************************************************************************

#ifndef __SSD1303_H__
   #define __SSD1303_H__

#include "Hardware.h"     // zakladni datove typy

//-----------------------------------------------------------------------------
// Prikazy
//-----------------------------------------------------------------------------

#define SSD1303_SET_LOWER_COLUMN_ADDRESS                0x00
#define SSD1303_SET_HIGHER_COLUMN_ADDRESS               0x10
#define SSD1303_SET_DISPLAY_START_LINE                  0x40
#define SSD1303_SET_CONTRAST_CONTROL_REGISTER           0x81
#define SSD1303_SET_SEGMENT_REMAP_0                     0xA0
#define SSD1303_SET_SEGMENT_REMAP_1                     0xA1
#define SSD1303_SET_NORMAL_DISPLAY                      0xA6
#define SSD1303_SET_INVERSE_DISPLAY                     0xA7
#define SSD1303_SET_MULTIPLEX_RATIO                     0xA8
#define SSD1303_SET_DISPLAY_OFF                         0xAE
#define SSD1303_SET_DISPLAY_ON                          0xAF
#define SSD1303_SET_PAGE_ADDRESS                        0xB0
#define SSD1303_SET_COM_OUTPUT_SCAN_DIRECTION_NORMAL    0xC0
#define SSD1303_SET_COM_OUTPUT_SCAN_DIRECTION_REMAPPED  0xC8
#define SSD1303_SET_DISPLAY_OFFSET                      0xD3
#define SSD1303_SET_DC_CONVERTER_ON                     0xAD
#define SSD1303_SET_DC_CONVERTER_OFF                    0xAA

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void Ssd1303WriteCommand(byte Command);
  // Zapise do displeje prikaz <Command>

byte Ssd1303ReadStatus(void);
  // Nacte status displeje

void Ssd1303WriteData(byte Data);
  // Zapise do displeje data <Data>

byte Ssd1303ReadData(void);
  // Nacte z displeje data

void Ssd1303Init(void);
  // Inicializace radice


#endif

