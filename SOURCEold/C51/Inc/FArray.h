//*****************************************************************************
//
//    FArray.h - FIFO array module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __FArray_H__
   #define __FArray_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Promenna <address> udava zacatek pole hodnot. POZOR, cela oblast musi lezet ve strance !
// promenna <count> udava pocet prvku pole
// promenna <size> je velikost prvku v bytech
// cela oblast ma velikost <count> * <size> bytu

// datovy typ pro ulozeni hodnoty
// maximalni ukladana hodnota je dword

typedef union {
   dword dw;
   word  w;
   byte  b;
   byte  array[ 4];
} TFarData;


TYesNo FarWrite( dword value, word address, byte count, byte size);
// Zapise hodnotu do interni EEPROM

dword FarRead( word address, byte count, byte size);
// Precte hodnotu z interni EEPROM

TYesNo FarReset( word address, byte count, byte size);
// Vymaze celou oblast, zapise marker na prvni pozici

#endif
