//*****************************************************************************
//
//   MB.h        Modbus serial communication
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MB_H__
   #define __MB_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MBInitialize( void);
// Inicializace - volat i po zmene parametru

void MBExecute( void);
// Automat protokolu, volat co nejcasteji

//- Pomocne funkce -------------------------------------------------------------

void MBException( byte Code);
// Zapise do bufferu chybovy paket

byte MBGetFunctionCode( void);
// Vrati funkcni kod posledni operace

#endif
