//*****************************************************************************
//
//    Com.h - RS232 communication services
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Com_H__
   #define __Com_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void ComInit( void);
// Inicializace komunikace

TYesNo ComTxReady( void);
// Vraci 1, je-li mozne zapsat do fronty

byte ComTxCount( void);
// Vraci pocet volnych mist ve fronte

void  ComTx( byte c);
// Vyslani znaku

TYesNo ComRxReady( void);
// Vraci 1, je-li k dispozici znak

byte ComRxCount( void);
// Vraci pocet prijatych znaku

byte ComRx( void);
// Vraci prijaty znak

void ComFlush( void);
// Vyprazdni fronty Rx a Tx


#endif

