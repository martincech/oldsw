//*****************************************************************************
//
//   Modul.h     Flash Modul
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __Modul_H__
   #define __Modul_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

TYesNo ModulIsPresent( void);
// Vrati YES, je-li modul pritomen

void ModulCopy( void);
// Kopiruje obsah flash na modul

TYesNo ModulVerify( void);
// Kontroluje obsah flash proti modulu

void ModulReadConfig();
// Nacte z modulu konfiguraci do struktury KonfiguraceModul

void ModulCopyFlocksFromModule();
// Zkopiruje vsechna hejna z modulu do interni flash

#endif
