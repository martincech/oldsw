//*****************************************************************************
//
//    Percents.c - Vykresleni prubehu
//    Version 1.0
//
//*****************************************************************************

#ifndef __Percents_H__
   #define __Percents_H__

#include "Hardware.h"     // zakladni datove typy

void PercentsInit(byte Index, dword Max);
// Inicializace hodnot

void PercentsExecute(byte Index, dword Value);
// Vypocte a zobrazi novou hodnotu procent

void PercentsClear(byte Index);
// Vymaze procenta z displeje, vcetne symbolu %


#endif
