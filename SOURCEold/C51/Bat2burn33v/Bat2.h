//*****************************************************************************
//
//    Bat2.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Bat2_H__
   #define __Bat2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifdef __C51__
// jen pro Keil
//   typedef int  int16;
//   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif


// ---------------------------------------------------------------------------------------------
// Zobrazeni
// ---------------------------------------------------------------------------------------------

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_NV_HMOTNOST      0x01           // Zobrazi se naslapna vaha po vypocteni nove aktualni hmotnosti
#define ZOBRAZIT_NV_ULOZENO       0x02           // Zobrazi se naslapna vaha po ulozeni nove hmotnosti
#define ZOBRAZIT_1SEC             0x04           // Zobrazi se periodicke veci 1sec (napr. cas)
#define ZOBRAZIT_HISTOGRAM_KURZOR 0x08           // Zobrazi se kurzor v histogramu spolecne s hmotnosti a poctem
#define ZOBRAZIT_LOGGER           0x10           // Zobrazi se stranka se vzorky loggeru
#define ZOBRAZIT_MRIZKU           0x80           // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_VSE              0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit


//-----------------------------------------------------------------------------
#endif
