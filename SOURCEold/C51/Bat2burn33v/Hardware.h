//*****************************************************************************
//
//    Hardware.h  - BAT2 hardware descriptions
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof


// Deska ver 1
#define BatAccuOk()  YES
#define PowerFail()  NO

// Bastldeska
//#define BatAccuOk()  !BatLOWBAT
//#define PowerFail()  BatLOWBAT

//-----------------------------------------------------------------------------
// Prirazeni portu
//-----------------------------------------------------------------------------

// port P0 :

sbit HermesDC       = P0^0;
sbit HermesCS       = P0^1;
sbit HermesE        = P0^2;
sbit HermesRW       = P0^3;
sbit HermesRES      = P0^4;
sbit HermesDAD      = P0^5;
sbit HermesCLKAD    = P0^6;
sbit HermesPISK     = P0^7;

// port P1 :

sbit BatCSEE     = P1^3;               // CS flash
sbit BatCSMOD    = P1^4;               // CS modul
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;
// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// datova sbernice :

#define HermesDATA P5

sbit HermesD0   = HermesDATA^0;
sbit HermesD1   = HermesDATA^1;
sbit HermesD2   = HermesDATA^2;
sbit HermesD3   = HermesDATA^3;
sbit HermesD4   = HermesDATA^4;
sbit HermesD5   = HermesDATA^5;
sbit HermesD6   = HermesDATA^6;
sbit HermesD7   = HermesDATA^7;

// Port P3 :

sbit HermesRXD      = P3^0;
sbit HermesLED1     = P3^1;
sbit HermesLED2     = P3^2;
sbit HermesK0       = P3^3;
sbit HermesK2       = P3^4;
sbit HermesK3       = P3^5;
sbit HermesK1       = P3^6;
sbit HermesCSAD     = P3^7;

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  2             // zdvojena baudova rychlost

// Casovac 0 :
#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  BatAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut HermesPISK                 // vystup generatoru
#define PCA_IDLE_STATE 1               // Klidova uroven pinu PcaOut

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1               // asynchronni pipani


//-----------------------------------------------------------------------------
// Pripojeni RTC DS17287
//-----------------------------------------------------------------------------

// porty :

#define RtcALE         BatALERTC       // ALE address latch enable
#define RtcWR          BatWRRTC        // WR\ zapis
#define RtcRD          BatRDRTC        // RD\ cteni
#define RtcAD          BatDATA         // datova/adresni sbernice
#define RtcCS          BatCSRTC        // chipselect

// volne adresy v RAM :

#define RTC_RAM_FIRST  0x0E            // od teto adresy
#define RTC_RAM_LAST   0x3F            // do teto adresy je RAM k dispozici

// kapacita NVRAM :

#define RTC_NVRAM_SIZE 2048

// podmineny preklad :

//#define RTC_USE_DATE    1              // cti/nastavuj datum
//#define RTC_USE_WDAY    1              // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1              // hlidej zadavane hodnoty na rozsah
//#define RTC_USE_ALARM   1              // pouzivej alarm/budik
#define RTC_USE_NVRAM   1              // pouzita NVRAM

//-----------------------------------------------------------------------------
// SW pripojeni SPI sbernice
//-----------------------------------------------------------------------------

/*#define SpiSCK   BatSCKEE          // hodiny SCK
#define SpiSO    BatSOEE           // vystup dat SO
#define SpiSI    BatSIEE           // vstup dat SI (muze byt shodny s SO)
//#define SpiCS    CS           // chipselect CS\

// Inverze signalu:
#define SPI_SI_H  1
#define SPI_SCK_H 1

// jednoduchy chipselect :
//#define SPI_SIMPLE_CS  1               // jednoduchy chipselect
//#define SpiAttach()    SpiCS = 0       // select
//#define SpiRelease()   SpiCS = 1       // deselect*/

//-----------------------------------------------------------------------------
// ED2 interni SPI
//-----------------------------------------------------------------------------

#ifndef __SpiHw_H__
   #include "..\inc\SpiHw.h"
#endif

// SPI mode 3
#define SPI_BAUD   SPI_CLOCK_2         // rychlost
//#define SPI_BAUD   SPI_CLOCK_128         // rychlost
#define SPI_LEVEL  SPI_CLOCK_H         // klidova hladina SCK
#define SPI_EDGE   SPI_EDGE_RETURN     // strobovaci hrana SCK

//-----------------------------------------------------------------------------
// Flash Atmel AT45DB161
//-----------------------------------------------------------------------------

#define __AT45DB161__      1           // typ pameti
#define FLASH_TIMEOUT   5000           // pocet cyklu cekani na ready

#define __flash_data__  xdata          // kontextove promenne

// Podmineny preklad :
//#define FLASH_READ_BYTE    1           // cteni bytu
//#define FLASH_WRITE_BYTE   1           // zapis+flush bytu

/*
// NVRAM buffer :
//#define FLASH_LOAD_BUFFER  1           // cteni pameti do bufferu
//#define FLASH_READ_BUFFER  1           // cteni bufferu
// Pro nonvolatile buffer :
#define FLASH_NVRAM        1           // pouzit modul NvFlash.c51
#define FLASH_VERIFY       1           // verifikace po zapisu stranky
*/


// FLASH buffer :
#define FLASH_LOAD_BUFFER  1           // cteni pameti do bufferu
#define FLASH_READ_BUFFER  1           // cteni bufferu
// Pro nonvolatile buffer :
//#define FLASH_NVRAM        1           // pouzit modul NvFlash.c51
//#define FLASH_VERIFY       1           // verifikace po zapisu stranky




//-----------------------------------------------------------------------------
// Radic displeje SSD1303
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       HermesDATA
#define DisplayD0         HermesD0
#define DisplayD1         HermesD1
#define DisplayD2         HermesD2
#define DisplayD3         HermesD3
#define DisplayD4         HermesD4
#define DisplayD5         HermesD5
#define DisplayD6         HermesD6
#define DisplayD7         HermesD7
#define DisplayRW         HermesRW
#define DisplayDC         HermesDC
#define DisplayE          HermesE
#define DisplayCS         HermesCS
#define DisplayRES        HermesRES

// Cekaci rutiny:
#define DisplayDelay60ns()      _nop_()
#define DisplayDelay140ns()     _nop_()
#define DisplayDelay100us()     SysDelay(1)

// Podminena kompilace:
//#define SSD1303_DC_CONVERTER    1       // Vyuziti interniho menice napeti 9V -> 3.3V
#define SSD1303_BUS             1       // Datova sbernice 1:1 na portu
//#define SSD1303_READ_STATUS     1       // Preklada se cteni statusu

//-----------------------------------------------------------------------------
// Graficky displej Univision UG-3264GMCAT01 (132x64)
//-----------------------------------------------------------------------------

// Rozmery displeje:
#define DISPLAY_WIDTH           132     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

// Podminena kompilace:
//#define DISPLAY_ROTATED         1       // Orientace rotace displeje (rotovany = ksanda nad displejem)
#define DISPLAY_CONTRAST        1       // Nastaveni kontrastu displeje
//#define DISPLAY_NORMAL_DISPLAY  1       // Prepinani na normalni (pozitivni) zobrazeni
//#define DISPLAY_INVERSE_DISPLAY 1       // Prepinani na inverzni zobrazeni

// Cary, obdelniky atd.
#define DISPLAY_SET_AREA_FAST   1
#define DISPLAY_SET_AREA        1
//#define DISPLAY_HOR_LINE        1
//#define DISPLAY_FAST_VERT_LINE  1
//#define DISPLAY_VERT_LINE       1
//#define DISPLAY_PIXEL           1
#define DISPLAY_CHAR            1
#define DISPLAY_SYMBOL          1

//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Podminena kompilace:

//#define DISPLAY_LINE                    1

// Vety
#define DISPLAY_STRING                  1
//#define DISPLAY_STRING_RIGHT            1
#define DISPLAY_STRING_CENTER           1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER           1
#define DISPLAY_NUMBER                  1
//#define DISPLAY_CHAR_FORMATED           1

// Editace
//#define DISPLAY_EDIT                    1
#define DISPLAY_EDIT_CHAR_WIDTH         5       // Sirka 1 cislice (podle pouziteho fontu)
#define DISPLAY_EDIT_DOT_WIDTH          1       // Sirka tecky a dvojtecky pri editaci cisla, data a casu (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_BEFORE_DOT      2       // Sirka mezery pred desetinnou carkou (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_AFTER_DOT       2       // Sirka mezery za desetinnou carkou (podle pouziteho fontu)
//#define DISPLAY_EDIT_TEXT               1

//#define DISPLAY_BARGRAF                 1

// Dialog
#define DISPLAY_DIALOG                  1
#define DIALOG_FLIP_KEYS                1        // Zda se ma otocit zobrazeni tlacitek Enter a Esc
#define DIALOG_BUTTON_Y                 (DISPLAY_HEIGHT - DIALOG_BUTTON_HEIGHT) // Y-ova souradnice tlacitek

// Menu
//#define DISPLAY_MENU                    1
#define MENU_MAX_ITEMS                  9       // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
#define DISPLAY_MENU_LINE_HEIGHT        16      // Vyska radku v pixelech
#define DISPLAY_MENU_Y                  0       // Y-ova souradnice pocatku menu v pixelech
#define DISPLAY_MENU_LINES_COUNT        4       // Pocet radku v menu
#define DISPLAY_MENU_ARROW_DY           2       // Vertikalni posun sipky (podle pouziteho fontu)
//#define DISPLAY_CHOOSE_ITEM             1

// Jazyky
//#define DISPLAY_LANGUAGES               1       // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
#define DisplayEditExecute(Znaky) KbdBeepKey()

//-----------------------------------------------------------------------------
// Fonty
//-----------------------------------------------------------------------------

// Podminena kompilace:
//#define DISPLAY_TAHOMA10          1
//#define DISPLAY_TAHOMA10_BOLD     1
#define DISPLAY_TAHOMA8           1
#define DISPLAY_LUCIDA6           1
//#define DISPLAY_MYRIAD32          1
//#define DISPLAY_MYRIAD40          1
#define DISPLAY_TEXT_WIDTH        1

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define ZapniPodsvit()
#define VypniPodsvit()

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define K0         HermesK0
#define K1         HermesK1
#define K2         HermesK2
#define K3         HermesK3

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
//  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_LEFT,                              // sipka doleva
  K_RIGHT,                             // sipka doprava
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

//#define __DISPLAY_CURSOR__    1      // nastavovani kurzoru
#define __DISPLAY_CLEAR__     1        // mazani
#define __DISPLAY_CLR_EOL__   1        // mazani do konce radku


//-----------------------------------------------------------------------------
// Komunikace RS232
//-----------------------------------------------------------------------------
#define COM_BAUD       19200          // Rychlost linky v baudech

//#define COM_PARITY_EVEN  1          // Suda parita
//#define COM_PARITY_ODD   1          // Licha parita

//#define COM_TIMER_1      1          // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2        1          // K casovani linky se vyuzije casovac 2

#define COM_RX_TIMEOUT     10000      // Meziznakovy timeout [us]

// Podmineny preklad :
#define COM_TX_CHAR        1           // Preklada se ComTxChar
#define COM_RX_CHAR        1           // Preklada se ComRxChar
#define COM_RX_WAIT        1           // Preklada se ComRxWait
#define COM_FLUSH_CHARS    1           // Preklada se ComFlushChars

//-----------------------------------------------------------------------------
// Modul GSM
//-----------------------------------------------------------------------------

#define SMS_MAX_LENGTH       161       // max. delka prijate zpravy, vc. koncove nuly
#define GSM_MAX_NUMBER      16         // max. delka telefonniho cisla

// buffer pro cislo :
extern char xdata GsmNumber[];
extern byte xdata GsmNumberLength;

// Definice ciloveho cisla :
#define SmsDstNumberLength()       GsmNumberLength              // pocet cislic ciloveho cisla
#define SmsDstNumberDigit( i)      (GsmNumber[i] - '0')         // cislice v poradi, jak je piseme

// Definice zdrojoveho cisla :
#define SmsSrcNumberLength( n)     GsmNumberLength = n;         // pocet cislic zdrojoveho cisla
#define SmsSrcNumberDigit( i, n)   GsmNumber[ i] = (n) + '0'    // cislice v poradi, jak je piseme

// Podminena kompilace :

//#define GSM_COM2            1          // komunikace pres COM2
//#define COM_RX_STRING       1          // funkce pro GSM_OPERATOR
//#define GSM_OPERATOR        1          // nazev operatora
//#define GSM_SIGNAL_STRENGTH 1          // sila signalu
//#define GSM_DATA            1          // datova komunikace
//#define GSM_SMS             1          // vysilani, prijem SMS
//#define GSM_SMS_USE_GSMCTL  1          // pri obsluze SMS se bude vyuzivat modul GsmCtl

// Timeouty radice GSM :

#define GSM_ERROR_MODE_TIMEOUT 10      // Prodleva po nastaveni modu chyb [ms] (min. 15ms vcetne COM_RX_TIMEOUT). Hodnota 5 jeste funguje, 10 je tutovka.
#define GSM_SMS_SEND_TIMEOUT  15       // SMS center reply timeout [s]
#define GSM_CONNECT_TIMEOUT   30       // od zvednuti do navazani spojeni  [s]
#define GSM_CHECK_PERIOD      30       // cas mezi dvema kontrolami registrace [s]
#define GSM_POWERED_PERIOD    20       // cas na nabeh napajeni modemu [s]
#define GSM_HANGUP_TIMEOUT    30       // zaveseni pri neaktivnim spojeni [s]

//-----------------------------------------------------------------------------
// COM_util
//-----------------------------------------------------------------------------

// Podminena kompilace :

// Podminena kompilace :

#define COM_TX_DIGIT        1
//#define COM_TX_HEX          1
#define COM_TX_DEC          1
//#define COM_RX_HEX          1
//#define COM_SKIP_CHARS      1
#define COM_RX_STRING         1
#define COM_TX_X_STRING       1
#define COM_RX_DELIMITER      1


//-----------------------------------------------------------------------------

#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------

#endif
