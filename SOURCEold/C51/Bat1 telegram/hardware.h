// Volba typu procesoru :

//#define __T89C51RD2__
#define __AT89C51ED2__

#ifdef __T89C51RD2__
   #include <reg52.h>

   // SFR procesoru  T89C51
   // Hardwarovy WatchDog T89C51
   sfr WDTRST = 0xA6;
   sfr WDTPRG = 0xA7;
   // EEPROM na cipu
   sfr EECON = 0xD2;
   sfr EETIM = 0xD3;
   // AUXR registr pro urceni externi pameti
   sfr AUXR  = 0x8E;
   sfr AUXR1 = 0xA2;
   // registr pro volbu rychlosti procesoru
   sfr CKCON=0x8F;
#endif

#ifdef __AT89C51ED2__
   #include "ed2.h"

   // prejmenovani registru :
   #define CKCON CKCON0
   #define IE    IEN0
   #define IP    IPL0
   #define IPH   IPH0

// Registr IP (0xB8), nyni IPL0 interrupt priority :

   #define PX0   PX0L           // external int 0
   #define PT0   PT0L           // timer 0 overflow
   #define PX1   PX1L           // external int 1
   #define PT1   PT1L           // timer 1 overflow
   #define PS    PLS            // serial port
   #define PT2   PT2L           // Timer 2 overflow
   #define PPC   PPCL           // PCA
#endif

unsigned char code CISLOVERZE[]="XXXXX";   // Aktualni cislo verze - musi mit vzdy format XX.XX, posila se to do PC

// Porty na HW06 (24.11.2003)
sbit DI = P2^0;
sbit RW = P2^1;
sbit E = P2^2;
sbit CS1 = P0^4;
sbit CS2 = P0^3;
sbit RESET = P0^2;
sbit D0 = P2^3;
sbit D1 = P2^4;
sbit D2 = P2^5;
sbit D3 = P2^6;
sbit D4 = P2^7;
sbit D5 = P0^7;
sbit D6 = P0^6;
sbit D7 = P0^5;

sbit K0 = P3^6;
sbit K1 = P3^5;
sbit K2 = P3^4;

// AD7705
sbit SCLK = P1^5;
sbit SDATA = P3^2;
sbit DRDY = P3^3;

// EEPROM Xicor X25645
sbit CS=P1^3;
sbit SO=P1^4;
sbit SI=P1^7;
sbit SCK=P1^6;

// Tiskarna
sbit STROBE=P2^3;   // STROBE = D0
sbit BUSY=P0^1;
sbit DT0=P2^4;   // Datove piny jsou taky zprehazene, DT0=D1, DT1=D2 atd.
sbit DT1=P2^5;
sbit DT2=P2^6;
sbit DT3=P2^7;
sbit DT4=P0^7;
sbit DT5=P0^6;
sbit DT6=P0^5;  // Datovych pinu je jen 7, musim tisknout jen hodnoty do 127 (na 8. pin nebylo misto, je to obsazene signalem STROBE)

// Nizke napajeni
sbit BAT = P0^0;

// Podsvit displeje
sbit PODSVIT = P3^7;

// Sirena
sbit SIRENA = P1^2;
