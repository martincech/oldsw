//*****************************************************************************
//
//    Fifo.tpl - FIFO template module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

// Data jsou ukladana po zaznamech TFifoData
// Za poslednim zaznamem je zaznam - marker,
// ktery ma na prvnim bytu smluveny znak
// FIFO_MARKER_EMPTY je-li FIFO nenaplnene, resp.
// FIFO_MARKER_FULL  je-li FIFO plne a prepisuje se
// FIFO zacina na adrese FIFO_START_ADDRESS, prvni volna je FIFO_NEXT_ADDRESS

#define XFIFO_INVALID_ADDRESS 0xFFFF                                                 // neplatna adresa do FIFO
#define XFIFO_NEXT_ADDRESS    (XFIFO_START + (XFIFO_CAPACITY * sizeof( TXFifoData))) // prvni volna adresa za FIFO

// Komunikacni struktura pretypovana na pole :

#define XFifoArray     ((byte __xdata__ *)&XFifoData)    // umozni indexovat po bytu

#ifdef XFIFO_FAST
   static word __xdata__ marker_address;                      // adresa markeru
#endif

// Lokalni funkce :

static TYesNo WriteData( word addr);
// Zapise komunikacni strukturu krome prvniho bytu

#ifdef XFIFO_VERIFY
   static TYesNo VerifyData( word addr);
   // Porovna data zapsana v EEPROM s komunikacni strukturou (vynecha prvni byte)
#endif

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void XFifoInit( void)
// Inicializuje modul
{
   XInit();
#ifdef XFIFO_FAST
   marker_address = XFifoFindMarker();    // nalezeni markeru
#endif
} // XFifoInit

//-----------------------------------------------------------------------------
// Zapis
//-----------------------------------------------------------------------------

TYesNo XFifoWrite( void)
// Zapise globalni komunikacni strukturu do FIFO
{
byte x;
word addr;

#ifdef XFIFO_FAST
   x = XReadByte( marker_address);
   if( x != XFIFO_MARKER_EMPTY && x != XFIFO_MARKER_FULL){
      // marker ukazuje spatne, najdi novou pozici markeru
      marker_address = XFifoFindMarker();
   }
   addr = marker_address;
#else
   addr = XFifoFindMarker();     // vzdy hledej marker
#endif
   if( addr == XFIFO_INVALID_ADDRESS){
      return( NO);          // znacka nenalezena, resp. nepodarilo se nove FIFO
   }
   // zapis dat, krome prvniho bytu
   if( !WriteData( addr)){
      return( NO);          // zapis dat se nepovedl
   }
#ifdef XFIFO_VERIFY
   // verifikace dat, krome prvniho bytu
   if( !VerifyData( addr)){
      return( NO);          // zapis dat se nepovedl
   }
#endif
   // novy marker :
   if( addr + sizeof( TXFifoData) >= XFIFO_NEXT_ADDRESS){
      // plne FIFO, zahajime prepis (nebo pokracujeme v prepisu)
      if( !XWriteByte( XFIFO_START, XFIFO_MARKER_FULL)){
         return( NO);
      }
#ifdef XFIFO_VERIFY
      // verifikace markeru :
      if( XReadByte( XFIFO_START) != XFIFO_MARKER_FULL){
         return( NO);       // nezapsal se novy marker
      }
#endif
#ifdef XFIFO_FAST
      marker_address = XFIFO_START;  // nova pozice markeru
#endif
   } else {
      // zapis markeru na nasledujici pozici FIFO
      x = XReadByte( addr);  // stary marker
      if( !XWriteByte( addr + sizeof( TXFifoData), x)){
         return( NO);
      }
#ifdef XFIFO_VERIFY
      // verifikace markeru :
      if( XReadByte( addr  + sizeof( TXFifoData)) != x){
         return( NO);         // nezapsal se novy marker
      }
#endif
#ifdef XFIFO_FAST
      marker_address = addr + sizeof( TXFifoData);
#endif
   }
   // prepsani stareho markeru novymi daty :
   x = XFifoArray[ 0];         // prvni byte komunikacni struktury
   if( !XWriteByte( addr, x)){
      return( NO);            // zustal stary marker
   }
#ifdef XFIFO_VERIFY
   if( XReadByte( addr) != x){
      return( NO);            // zustal stary marker
   }
#endif
   return( YES);
} // XFifoWrite

#ifdef XFIFO_READ
//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

void XFifoRead( word address)
// Naplni komunikacni strukturu daty z <address>
{
byte i;

   XBlockReadStart( address);
   for( i = 0; i < sizeof( TXFifoData); i++){
      XFifoArray[ i] = XBlockReadData( address++);
   }
   XBlockReadStop();
} // XFifoRead
#endif // XFIFO_READ

//-----------------------------------------------------------------------------
// Hledani Markeru
//-----------------------------------------------------------------------------

word XFifoFindMarker( void)
// Vrati adresu markeru
{
word i;
byte x;
byte found;

    found = NO;
    for( i = XFIFO_START; i < XFIFO_NEXT_ADDRESS; i += sizeof( TXFifoData)){
       x = XReadByte( i);
       if( x == XFIFO_MARKER_EMPTY || x == XFIFO_MARKER_FULL){
           found = YES;
           break;
       }
    }
    if( found){
       return( i);
    }
    // znacka nenalezena, zaloz prazdne FIFO :
    if( !XFifoReset()){
       return( XFIFO_INVALID_ADDRESS);
    }
    return( XFIFO_START);
} // XFifoFindMarker

//-----------------------------------------------------------------------------
// Prazdne Fifo
//-----------------------------------------------------------------------------

TYesNo XFifoReset( void)
// Zalozi prazdne FIFO
{
    // Zaloz prazdne FIFO (marker na prvni adrese) :
    if( !XWriteByte( XFIFO_START, XFIFO_MARKER_EMPTY)){
       return( NO);
    }
    if( XReadByte( XFIFO_START) != XFIFO_MARKER_EMPTY){
       return( NO);    // neproslo kontrolni cteni
    }
#ifdef XFIFO_FAST
   marker_address = XFIFO_START;
#endif
    return( YES);
} // XFifoReset

//-----------------------------------------------------------------------------
// Zapis dat
//-----------------------------------------------------------------------------

static TYesNo WriteData( word addr)
// Zapise komunikacni strukturu krome prvniho bytu
{
byte i;
byte new_page;

   addr++;               // vynechavame prvni byte
   new_page = YES;       // start nove stranky
   for( i = 1; i < sizeof( TXFifoData); i++){
      if( new_page){
         // zacatek nove stranky
         new_page = NO;
         if( !XPageWriteStart( addr)){
            return( NO);
         }
      }
      XPageWriteData( addr, XFifoArray[ i]);       // zapis dat do stranky
      addr++;                                      // nova adresa
      if( (addr & (X_PAGE_SIZE - 1)) == 0){
         // nova adresa je v nove strance
         XPageWritePerform();                      // Zapis predchozi stranku
         new_page = YES;
      }
   }
   if( !new_page){
      XPageWritePerform();                         // zapis zbytek posledni stranky
   }
   return( YES);
} // WriteData

#ifdef XFIFO_VERIFY
//-----------------------------------------------------------------------------
// Verifikace dat
//-----------------------------------------------------------------------------

static TYesNo VerifyData( word addr)
// Porovna data zapsana v EEPROM s komunikacni strukturou (vynecha prvni byte)
{
byte i;

   if( sizeof( TXFifoData) == 1){
      return( YES);           // prvni byte se vynechava, hotovo
   }
   // vynechavame prvni byte :
   addr++;
   XBlockReadStart( addr);
   for( i = 1; i < sizeof( TXFifoData); i++){
      if( XBlockReadData( addr++) != XFifoArray[ i]){
         XBlockReadStop();
         return( NO);         // spatne zapsany byte
      }
   }
   XBlockReadStop();
   return( YES);
} // VerifyData

#endif // XFIFO_VERIFY


#ifdef XFIFO_FIND_FIRST
//-----------------------------------------------------------------------------
// Hledani prvniho (tj. nejstarsiho) zaznamu
//-----------------------------------------------------------------------------
word XFifoFindFirst() {
  // Vrati adresu prvniho zaznamu. Pokud ve fifo zadny zaznam neni (tj. fifo je prazdne), vrati adresu XFIFO_INVALID_ADDRESS.
  word Adresa;
  Adresa=XFifoFindMarker();  // Najdu adresu markeru
  // Musim rozlisit, zda se uz prepisuje nebo jeste ne
  if (XReadByte(Adresa)==XFIFO_MARKER_EMPTY) {
    // Neprepisuje se
    if (Adresa==XFIFO_START) {
      // Fifo je prazdne, zadny prvni zaznam v nem neni
      return XFIFO_INVALID_ADDRESS;
    } else {
      // Nejake zaznamy tam jsou, ale zatim se neprepisuje => prvni zaznam je na prvni pozici fifo
      return XFIFO_START;
    }//else
  } else {
    // Prepisuje se - prvni zaznam je vzdy na pozici za markerem
    // Pokud je marker na posledni adrese fifo (na konci), je prvni zaznam na miste prvniho zaznamu fifo. Pokud je marker nekde uprostred,
    // je prvni zaznam normalne za nim.
    if (Adresa==XFIFO_NEXT_ADDRESS-sizeof(TXFifoData)) {
      // Marker je na poslednim zaznamu fifo
      return XFIFO_START;
    } else {
      // Marker je nekde uprostred
      return Adresa+sizeof(TXFifoData);
    }//else
  }//else
}
#endif //XFIFO_FIND_FIRST


#ifdef XFIFO_FIND_LAST
//-----------------------------------------------------------------------------
// Hledani posledniho (tj. nejmladsiho) zaznamu
//-----------------------------------------------------------------------------
word XFifoFindLast() {
  // Vrati adresu posledniho zaznamu. Pokud ve fifo zadny zaznam neni (tj. fifo je prazdne), vrati adresu XFIFO_INVALID_ADDRESS.
  word Adresa;
  Adresa=XFifoFindMarker();  // Najdu adresu markeru
  // Musim rozlisit, zda se uz prepisuje nebo jeste ne
  if (XReadByte(Adresa)==XFIFO_MARKER_EMPTY) {
    // Neprepisuje se
    if (Adresa==XFIFO_START) {
      // Fifo je prazdne, zadny zaznam v nem neni
      return XFIFO_INVALID_ADDRESS;
    } else {
      // Nejake zaznamy tam jsou, ale zatim se neprepisuje => posledni zaznam je na pozici pred markerem
      return Adresa-sizeof(TXFifoData);
    }//else
  } else {
    // Prepisuje se
    // Pokud je marker na prvni adrese fifo (na zacatku), je posledni zaznam na miste posledniho zaznamu fifo. Pokud je marker nekde uprostred,
    // je posledni zaznam normalne pred nim, jako v predchozim pripade.
    if (Adresa==XFIFO_START) {
      // Marker je na zacatku fifo
      return XFIFO_NEXT_ADDRESS-sizeof(TXFifoData);
    } else {
      // Marker je nekde uprostred
      return Adresa-sizeof(TXFifoData);
    }//else
  }//else
}
#endif //XFIFO_FIND_LAST

//-----------------------------------------------------------------------------
// Update
//-----------------------------------------------------------------------------

#ifdef XFIFO_UPDATE

TYesNo XFifoUpdate(word Address) {
  // Ulozi globalni komunikacni strukturu do FIFO na adresu <Address>
  byte x;
  word MarkerAddress;

   if (Address == XFIFO_INVALID_ADDRESS) {
      return NO;                // Adresa polozky je neplatna
   }

#ifdef XFIFO_FAST
   x = XReadByte( marker_address);
   if( x != XFIFO_MARKER_EMPTY && x != XFIFO_MARKER_FULL){
      // marker ukazuje spatne, najdi novou pozici markeru
      marker_address = XFifoFindMarker();
   }
   MarkerAddress = marker_address;
#else
   MarkerAddress = XFifoFindMarker();     // vzdy hledej marker
#endif
   if( MarkerAddress == XFIFO_INVALID_ADDRESS){
      return( NO);          // znacka nenalezena, resp. nepodarilo se nove FIFO
   }
   if (Address == MarkerAddress) {
      return NO;                // Adresa pro update nemuze ukazovat na marker
   }
   // zapis dat, krome prvniho bytu
   if( !WriteData( Address)){
      return( NO);          // zapis dat se nepovedl
   }
#ifdef XFIFO_VERIFY
   // verifikace dat, krome prvniho bytu
   if( !VerifyData( Address)){
      return( NO);          // zapis dat se nepovedl
   }
#endif
   // Zapis prvniho bajtu:
   x = XFifoArray[ 0];        // prvni byte komunikacni struktury
   if( !XWriteByte( Address, x)){
      return( NO);            // zapis dat se nepovedl
   }
#ifdef XFIFO_VERIFY
   if( XReadByte( Address) != x){
      return( NO);            // zapis dat se nepovedl
   }
#endif
   return( YES);
} // XFifoUpdate

#endif // XFIFO_UPDATE

//-----------------------------------------------------------------------------
// Nasledujici polozka
//-----------------------------------------------------------------------------

#ifdef XFIFO_FIND_NEXT

word XFifoFindNext(word Address) {
  // Vrati adresu nasledujici polozky za polozkou <Address>. Pokud ve fifo zadny zaznam neni (tj. fifo je prazdne), vrati adresu XFIFO_INVALID_ADDRESS.
  word MarkerAddress;
  byte x;

   if (Address == XFIFO_INVALID_ADDRESS) {
      return XFIFO_INVALID_ADDRESS;     // Adresa puvodni polozky je neplatna
   }

   // Nactu adresu markeru
#ifdef XFIFO_FAST
   x = XReadByte( marker_address);
   if( x != XFIFO_MARKER_EMPTY && x != XFIFO_MARKER_FULL){
      // marker ukazuje spatne, najdi novou pozici markeru
      marker_address = XFifoFindMarker();
   }
   MarkerAddress = marker_address;
#else
   MarkerAddress = XFifoFindMarker();     // vzdy hledej marker
#endif

   // Kontrola markeru
   if( MarkerAddress == XFIFO_INVALID_ADDRESS){
      return XFIFO_INVALID_ADDRESS;     // znacka nenalezena, resp. nepodarilo se nove FIFO
   }
   if (Address == MarkerAddress) {
      return XFIFO_INVALID_ADDRESS;     // Adresa puvodni polozky je neplatna, ukazuje na marker
   }

   // Zjisteni adresy nasledujici polozky
  Address += sizeof(TXFifoData);
  if (Address >= XFIFO_NEXT_ADDRESS) {
    // Prekrocil jsem adresovy rozsah, vratim se zpet na zacatek
    Address = XFIFO_START;
  }
  if (Address == MarkerAddress) {
    return XFIFO_INVALID_ADDRESS;       // Adresa puvodni polozky uz byla na konci fifa, na dalsi polozku nelze prejit
  }
  return Address;
} // XFifoFindNext

#endif // XFIFO_FIND_NEXT

//-----------------------------------------------------------------------------
// Predchozi polozka
//-----------------------------------------------------------------------------

#ifdef XFIFO_FIND_PREVIOUS

word XFifoFindPrevious(word Address) {
  // Vrati adresu predchozi polozky pred polozkou <Address>. Pokud ve fifo zadny zaznam neni (tj. fifo je prazdne), vrati adresu XFIFO_INVALID_ADDRESS.
  word MarkerAddress;
  TYesNo Overwrite;             // YES/NO Fifo se prepisuje
  byte x;

   if (Address == XFIFO_INVALID_ADDRESS) {
      return XFIFO_INVALID_ADDRESS;     // Adresa puvodni polozky je neplatna
   }

   // Nactu adresu markeru
#ifdef XFIFO_FAST
   x = XReadByte( marker_address);
   if( x != XFIFO_MARKER_EMPTY && x != XFIFO_MARKER_FULL){
      // marker ukazuje spatne, najdi novou pozici markeru
      marker_address = XFifoFindMarker();
   }
   MarkerAddress = marker_address;
#else
   MarkerAddress = XFifoFindMarker();     // vzdy hledej marker
#endif

   // Kontrola markeru
   if( MarkerAddress == XFIFO_INVALID_ADDRESS){
      return XFIFO_INVALID_ADDRESS;     // znacka nenalezena, resp. nepodarilo se nove FIFO
   }
   if (Address == MarkerAddress) {
      return XFIFO_INVALID_ADDRESS;     // Adresa puvodni polozky je neplatna, ukazuje na marker
   }

   // Prepis
   Overwrite = (TYesNo)(XReadByte(MarkerAddress) == XFIFO_MARKER_FULL);

   // Zjisteni adresy predchozi polozky
   if (Address == XFIFO_START) {
     // Jsme na prvni polozce
     if (!Overwrite) {
       return XFIFO_INVALID_ADDRESS;    // Prazdne FIFO
     }
     Address = XFIFO_NEXT_ADDRESS;      // Polozka je na konci - jdu az za adresni prostor, dalsim radkem prejdu na posledni polozku
   }
   Address -= sizeof(TXFifoData);

   if (Address == MarkerAddress) {
     return XFIFO_INVALID_ADDRESS;      // Adresa puvodni polozky uz byla na zacatku fifa, na predchozi polozku nelze prejit
   }

   return Address;
} // XFifoFindNext

#endif // XFIFO_FIND_PREVIOUS


