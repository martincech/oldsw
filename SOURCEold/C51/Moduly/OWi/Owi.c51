//*****************************************************************************
//
//    Owi.c  -  1-Wire bus services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "..\inc\Owi.h"

#define MARKER()   OwiDQ = 0; OwiDQ = 1   // pro ladeni, impuls na sbernici

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

TYesNo OwiInit( void)
// Zkontroluje sbernici, vraci 0 pri chybe (zkrat)
{
   OwiDQ = 1;          // klidovy stav je 1
   OwiMicroDelay( 1);  // chvilka cekani
   return( OwiDQ);
} // OwiInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

bit OwiReset( void)
// Resetuje sbernici, vraci signal pritomnosti.
// 0 - zarizeni pritomno, 1 - nenalezeno zadne
{
bit presence;

   if( !OwiDQ){
      return( 1);      // trvaly zkrat
   }
   OwiDQ = 0;
   OWI_DELAY_RESET;

   OwiDisableInts();         // 70us se ceka na impuls pritomnosti, kriticka sekce
   OwiDQ = 1;
   OWI_DELAY_PRESENCE;
   MARKER();
   presence = OwiDQ;
   OwiEnableInts();          // konec kritickeho useku
                             // pri rozumne programovanych obsluhach interruptu nebude nutne
   OWI_DELAY_RESET_TIMESLOT;
   return( presence);
} // OwiReset

//-----------------------------------------------------------------------------
// Cteni bitu
//-----------------------------------------------------------------------------

bit OwiReadBit( void)
// Vytvori Read slot a vrati hodnotu bitu. Pozor, za touto funkci
// nasleduje OWI_DELAY_READ_SLOT
{
byte i;
bit  value;

   OwiDisableInts();                       // kriticka sekce casovani
   OwiDQ = 0;                              // start slotu
   OWI_DELAY_START;                        // sirka startbitu
   OwiDQ = 1;
   for( i = 0; i < OWI_DELAY_READ; i++);   // zpozdeni max. 15us od zacatku startbitu
   MARKER();
   value = OwiDQ;                          // precteni hodnoty
   OwiEnableInts();                        // konec kriticke sekce

   OWI_DELAY_READ_SLOT;                    // konec slotu
   return( value);
} // OwiReadBit

//-----------------------------------------------------------------------------
// Zapis bitu
//-----------------------------------------------------------------------------

void OwiWriteBit( bit value)
// Vytvori Write slot s hodnotou <value>
{
   OwiDisableInts();  // zacatek kriticke sekce casovani
   OwiDQ = 0;         // start slotu
   if( value){
      OwiDQ = 1;      // Write Slot 1
   } // else Write Slot 0
   OwiEnableInts();   // konec kriticke sekce casovani

   OWI_DELAY_WRITE;
   OwiDQ = 1;         // konec slotu
} // OwiWriteBit

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte OwiReadByte( void)
// Precte byte ze sbernice
{
byte i;
byte value;

   value = 0;
   for( i = 0; i < 8; i++){
      value >>= 1;
      if( OwiReadBit()){
         value |= 0x80;
      }
   }
   return( value);
} // OwiReadByte

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

void OwiWriteByte( byte value)
// Zapise byte na sbernici
{
byte i;

   for( i = 0; i < 8; i++){
      OwiWriteBit( value & 1);
      value >>= 1;
   }
} // OwiWriteByte

#ifdef OWI_READ_ROM

//-----------------------------------------------------------------------------
// Cteni identifikace
//-----------------------------------------------------------------------------

TYesNo OwiReadROM( TOwiIdentification *ident)
// Precte identifikaci cipu
{
byte i;

   if( OwiReset()){
      return( NO);      // chybi presence impuls
   }
   OwiWriteByte( OWI_CMD_READ_ROM);
   ident->family = OwiReadByte();
   for( i = 0; i < OWI_SERIAL_LENGTH; i++){
      ident->serial[ i] = OwiReadByte();
   }
   ident->crc = OwiReadByte();
#ifdef OWI_CRC8
   // Kontrola zabezpeceni
   OwiResetCrc();
   // pres vsechny byty :
   for( i = 0; i < OWI_IDENTIFICATION_LENGTH; i++){
      OwiCalcCrc( ((byte *)ident)[ i]);
   }
   // vysledek by mel byt 0 :
   if( OwiGetCrc() != 0){
      return( NO);
   }
#endif
   return( YES);
} // OwiReadROM
#endif

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

void OwiMicroDelay( word count)
// Zpozdeni v mikrosekundach
{
word i;

   for( i = 0; i < count; i++);
} // MicroDelay


#ifdef OWI_CRC8

//-----------------------------------------------------------------------------
// Vypocet CRC
//-----------------------------------------------------------------------------

static byte _old_crc;        // kontext CRC, pred vypoctem nulovat !

static byte code crc_table[] = {
0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
157,195, 33,127,252,162, 64, 30, 95, 1,227,189, 62, 96,130,220,
35,125,159,193, 66, 28,254,160,225,191, 93, 3,128,222, 60, 98,
190,224, 2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89, 7,
219,133,103, 57,186,228, 6, 88, 25, 71,165,251,120, 38,196,154,
101, 59,217,135, 4, 90,184,230,167,249, 27, 69,198,152,122, 36,
248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91, 5,231,185,
140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
202,148,118, 40,171,245, 23, 73, 8, 86,180,234,105, 55,213,139,
87, 9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
};

void OwiResetCrc( void)
// Nuluje zabezpeceni, volat vzdy pred vypoctem
{
   _old_crc = 0;
} // OwiResetCrc


byte OwiCalcCrc( byte value)
// Vypocte z bytu zabezpeceni, vrati vysledek
{
   _old_crc = crc_table[ value ^ _old_crc];
   return( _old_crc);
} // CalcCrc


byte OwiGetCrc( void)
// Vrati vysledek vypoctu zabezpeceni
{
   return( _old_crc);
} // OwiGetCrc

#endif

