//*****************************************************************************
//
//    Mwi.c  -  1-Wire bus services
//    Version 1.0, (c) Vymos
//
//  16.5.2006: moznost inverze vstupu (dosud bylo pouze u vystupu), nove DQI_LO a DQI_HI v hardware.h
//
//*****************************************************************************

#include "..\inc\Mwi.h"

// Makra pro generovani funkci, pro prehlednost je lepsi zdrojovy
// text Owi.c :

// zakaz preruseni po dobu cekani na presence :
#define _MwiReset( DQI)              \
         if( DQI == DQI_LO) {        \
            presence = 1;            \
         } else {                    \
           MwiDQO = DQO_LO;          \
           MWI_DELAY_RESET;          \
           DisableInts();            \
           MwiDQO = DQO_HI;          \
           MWI_DELAY_PRESENCE;       \
           presence = (TYesNo)(DQI == DQI_HI);    \
           EnableInts();             \
         }                           \

#define _MwiReadBit( DQI)         \
         DisableInts();           \
         MwiDQO = DQO_LO;         \
         MWI_DELAY_START;         \
         MwiDQO = DQO_HI;         \
         for( i = 0; i < MWI_DELAY_READ; i++); \
         value = (TYesNo)(DQI == DQI_HI);      \
         EnableInts();            \




// Lokalni funkce :

static void MwiMicroDelay( word count);
// Zpozdeni v mikrosekundach

//-----------------------------------------------------------------------------
// Atakovani sbernice
//-----------------------------------------------------------------------------

void MwiAttach( void)
// Pripravi sbernici pro provoz
{
   #ifdef MwiDATA
      // sdilena sbernice
      MwiDATA = 0xFF;                  // vstupy do jednicek
      MwiDQO  = DQO_HI;                // vystup do jednicky
      MwiCS   = 0;                     // povoleni latche 1-wire sbernic
   #else
      // individualni vstupy
      #ifdef MWI_CH0
         MwiDQI0 = 1;                  // vstupy do jednicek - primo na procesoru
      #endif
      #ifdef MWI_CH1
         MwiDQI1 = 1;
      #endif
      #ifdef MWI_CH2
         MwiDQI2 = 1;
      #endif
      #ifdef MWI_CH3
         MwiDQI3 = 1;
      #endif
      #ifdef MWI_CH4
         MwiDQI4 = 1;
      #endif
      #ifdef MWI_CH5
         MwiDQI5 = 1;
      #endif
      #ifdef MWI_CH6
         MwiDQI6 = 1;
      #endif
      #ifdef MWI_CH7
         MwiDQI7 = 1;
      #endif
      MwiDQO = DQO_HI;                 // vystup do jednicky - az za pripadnym invertorem
   #endif // sdilena sbernice
} // MwiAttach

#ifdef MwiDATA
//-----------------------------------------------------------------------------
// Uvolneni sbernice
//-----------------------------------------------------------------------------

void MwiRelease( void)
// Uvolneni sbernice - jen pro sdilenou sbernici
{
   MwiDATA = 0xFF;                     // vstupy do jednicek (primo na procesoru)
   MwiDQO  = DQO_HI;                   // vystup do jednicky
   MwiCS   = 1;                        // zakaz latche 1-wire sbernic
} // MwiRelease
#endif

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

void MwiResetAll( void)
// Resetuje sbernici
{
   MwiDQO = DQO_LO;
   MWI_DELAY_RESET;
   MwiDQO = DQO_HI;
   MWI_DELAY_RESET;
} // MwiResetAll

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

TYesNo MwiReset( byte channel)
// Resetuje sbernici, vraci signal pritomnosti.
// YES - zarizeni pritomno
{
bit presence;

   switch( channel){
      #ifdef MWI_CH0
      case MWI_CH0 :
         _MwiReset( MwiDQI0);
         break;
      #endif
      #ifdef MWI_CH1
      case MWI_CH1 :
         _MwiReset( MwiDQI1);
         break;
      #endif
      #ifdef MWI_CH2
      case MWI_CH2 :
         _MwiReset( MwiDQI2);
         break;
      #endif
      #ifdef MWI_CH3
      case MWI_CH3 :
         _MwiReset( MwiDQI3);
         break;
      #endif
      #ifdef MWI_CH4
      case MWI_CH4 :
         _MwiReset( MwiDQI4);
         break;
      #endif
      #ifdef MWI_CH5
      case MWI_CH5 :
         _MwiReset( MwiDQI5);
         break;
      #endif
      #ifdef MWI_CH6
      case MWI_CH6 :
         _MwiReset( MwiDQI6);
         break;
      #endif
      #ifdef MWI_CH7
      case MWI_CH7 :
         _MwiReset( MwiDQI7);
         break;
      #endif
   }
   MWI_DELAY_RESET_TIMESLOT;
   return( !presence);
} // MwiReset

//-----------------------------------------------------------------------------
// Cteni bitu
//-----------------------------------------------------------------------------

bit MwiReadBit( byte channel)
// Vytvori Read slot a vrati hodnotu bitu
{
byte i;
bit  value;

   switch( channel){
      #ifdef MWI_CH0
      case MWI_CH0 :
         _MwiReadBit( MwiDQI0);
         break;
      #endif
      #ifdef MWI_CH1
      case MWI_CH1 :
         _MwiReadBit( MwiDQI1);
         break;
      #endif
      #ifdef MWI_CH2
      case MWI_CH2 :
         _MwiReadBit( MwiDQI2);
         break;
      #endif
      #ifdef MWI_CH3
      case MWI_CH3 :
         _MwiReadBit( MwiDQI3);
         break;
      #endif
      #ifdef MWI_CH4
      case MWI_CH4 :
         _MwiReadBit( MwiDQI4);
         break;
      #endif
      #ifdef MWI_CH5
      case MWI_CH5 :
         _MwiReadBit( MwiDQI5);
         break;
      #endif
      #ifdef MWI_CH6
      case MWI_CH6 :
         _MwiReadBit( MwiDQI6);
         break;
      #endif
      #ifdef MWI_CH7
      case MWI_CH7 :
         _MwiReadBit( MwiDQI7);
         break;
      #endif
   }
   MWI_DELAY_READ_SLOT;
   return( value);
} // MwiReadBit

//-----------------------------------------------------------------------------
// Zapis bitu
//-----------------------------------------------------------------------------

void MwiWriteBit( bit value)
// Vytvori Write slot s hodnotou <value>
{
   DisableInts();
   MwiDQO = DQO_LO;
   MWI_DELAY_START;
   if( value){
      MwiDQO = DQO_HI;
   }
   MWI_DELAY_WRITE;
   MwiDQO = DQO_HI;
   EnableInts();
} // MwiWriteBit

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte MwiReadByte( byte channel)
// Precte byte ze sbernice
{
byte i;
byte value;

   value = 0;
   for( i = 0; i < 8; i++){
      value >>= 1;
      if( MwiReadBit( channel)){
         value |= 0x80;
      }
   }
   return( value);
} // MwiReadByte

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

void MwiWriteByte( byte value)
// Zapise byte na sbernici
{
byte i;

   for( i = 0; i < 8; i++){
      MwiWriteBit( value & 1);
      value >>= 1;
   }
} // MwiWriteByte

#ifdef MWI_READ_ROM

//-----------------------------------------------------------------------------
// Cteni identifikace
//-----------------------------------------------------------------------------

TYesNo MwiReadROM( byte channel, TMwiIdentification *ident)
// Precte identifikaci cipu
{
byte i;

   if( !MwiReset( channel)){
      return( NO);      // chybi presence impuls
   }
   MwiWriteByte( MWI_CMD_READ_ROM);
   ident->family = MwiReadByte( channel);
   for( i = 0; i < MWI_SERIAL_LENGTH; i++){
      ident->serial[ i] = MwiReadByte( channel);
   }
   ident->crc = MwiReadByte( channel);
#ifdef MWI_CRC8
   // Kontrola zabezpeceni
   MwiResetCrc();
   // pres vsechny byty :
   for( i = 0; i < MWI_IDENTIFICATION_LENGTH; i++){
      MwiCalcCrc( ((byte *)ident)[ i]);
   }
   // vysledek by mel byt 0 :
   if( MwiGetCrc() != 0){
      return( NO);
   }
#endif
   return( YES);
} // MwiReadROM
#endif

#ifdef MWI_CRC8

//-----------------------------------------------------------------------------
// Vypocet CRC
//-----------------------------------------------------------------------------

byte _mwi_old_crc;        // kontext CRC, pred vypoctem nulovat !

#ifdef MWI_FAST_CRC8         // vypocet CRC tabulkou

static byte code crc_table[] = {
0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
157,195, 33,127,252,162, 64, 30, 95, 1,227,189, 62, 96,130,220,
35,125,159,193, 66, 28,254,160,225,191, 93, 3,128,222, 60, 98,
190,224, 2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89, 7,
219,133,103, 57,186,228, 6, 88, 25, 71,165,251,120, 38,196,154,
101, 59,217,135, 4, 90,184,230,167,249, 27, 69,198,152,122, 36,
248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91, 5,231,185,
140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
202,148,118, 40,171,245, 23, 73, 8, 86,180,234,105, 55,213,139,
87, 9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53
};

byte MwiCalcCrc( byte value)
// Vypocte z bytu zabezpeceni, vrati vysledek
{
   _mwi_old_crc = crc_table[ value ^ _mwi_old_crc];
   return( _mwi_old_crc);
} // CalcCrc

#endif

#else   //-- Algoritmicky vypocet CRC -----------------------------------------

byte MwiCalcCrc( byte value)
// Vypocte z bytu zabezpeceni, vrati vysledek
{
byte i;
byte feedback;

   for( i = 8; i > 0; i--){
      feedback = 0;                                    // vystup prvniho hradla XOR, zatim 0
      if( (value ^ _mwi_old_crc) & 0x01){              // XOR obou LSB
         _old_crc ^= 0x18;                             // uplatni zpetnou vazbu pres vnitrni hradla
         feedback  = 0x80;                             // vystup prvniho hradla je 1
      }
      _old_crc  = (_mwi_old_crc >> 1) | feedback;      // posun registr, do MSB pridej XOR
      value >>= 1;                                     // dalsi datovy bit
   }
   return( _mwi_old_crc);
} // MwiCalcCrc

#endif // MWI_FAST_CRC8

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

static void MwiMicroDelay( word count)
// Zpozdeni v mikrosekundach
{
word i;

   for( i = 0; i < count; i++);
} // MicroDelay


