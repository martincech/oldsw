//*****************************************************************************
//
//    Lifo.h - LIFO EEPROM module
//    Version 1.0, Petr
//
//*****************************************************************************

#ifndef __Lifo_H__
   #define __Lifo_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Komunikacni struktura pro uzivatele modulu :

extern TLifoData __xdata__ LifoData;     // zde jen pro kontrolu, musi byt soucasti projektu !


void LifoInit();
// Inicializuje modul

TYesNo LifoWrite();
// Zapise globalni komunikacni strukturu do LIFO

void LifoRead(word address);
// Naplni komunikacni strukturu daty z <address>

word LifoFindMarker();
// Vrati adresu markeru

TYesNo LifoReset();
// Zalozi prazdne LIFO

#endif
