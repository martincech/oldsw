//*****************************************************************************
//
//    Hardware.h  - TMC hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof


//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

// spolecne vodice (podle schematu) :

// Port 0 :

#define DATA    P0                  // datova sbernice
// datove bity :
sbit BatD0     = DATA^0;           // D0
sbit BatD1     = DATA^1;           // D1
sbit BatD2     = DATA^2;           // D2
sbit BatD3     = DATA^3;           // D3
sbit BatD4     = DATA^4;           // D4
sbit BatD5     = DATA^5;           // D5
sbit BatD6     = DATA^6;           // D6
sbit BatD7     = DATA^7;           // D7

// Port 1:

sbit DispA0     = P1^0;
sbit DispRD     = P1^1;
sbit DispWR     = P1^2;
sbit DispRES    = P1^3;
sbit DispCS     = P1^4;


// Port 2:

sbit DispOff    = P2^0;
sbit DispOn     = P2^7;

// Port 3 :

sbit BatK0       = P3^7;
sbit BatK1       = P3^6;
sbit BatK2       = P3^6;



// BAT2:
/*
// datova sbernice :

#define BatDATA P0

sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :

sbit BatCSRTC    = P1^0;
sbit BatRDRTC    = P1^1;
sbit BatWRRTC    = P1^2;
sbit BatALERTC   = P1^3;
sbit BatCSEE     = P1^4;               // CS flash
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;
// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// Port P2 :

sbit BatLOWBAT   = P2^0;
sbit BatZAPGSM   = P2^1;
sbit BatPISK     = P2^2;
sbit BatK0       = P2^3;
sbit BatK1       = P2^4;
sbit BatRES      = P2^5;               // RES display
sbit BatCS2      = P2^6;               // CS1 display
sbit BatCS1      = P2^7;               // CS2 display

// Port P3 :

sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatCSMOD    = P3^2;               // CS flash modulu
sbit BatDRAD     = P3^3;               // DRDY AD1241
sbit BatCLKAD    = P3^4;               // CLK  AD1241
sbit BatK2       = P3^5;
sbit BatSVIT     = P3^6;               // podsvit
sbit BatDATAAD   = P3^7;               // DOUT AD1241*/



//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz

#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :

#define DOUBLE_BAUDRATE  1             // zdvojena baudova rychlost (1=ne, 2=ano)

// Casovac 0 :

#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

//#define K0         Key0
//#define K1         Key1

#define K0         BatK0
#define K1         BatK1
#define K2         BatK2

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
//  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_LEFT,                              // sipka doleva
  K_RIGHT,                             // sipka doprava
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Radic displeje SSD1303
//-----------------------------------------------------------------------------

/*// Hardwarove pripojeni displeje
#define DisplayDATA       DATA
#define DisplayD0         DispD0
#define DisplayD1         DispD1
#define DisplayD2         DispD2
#define DisplayD3         DispD3
#define DisplayD4         DispD4
#define DisplayD5         DispD5
#define DisplayD6         DispD6
#define DisplayD7         DispD7
#define DisplayRW         DispRW
#define DisplayDC         DispDC
#define DisplayE          DispE
#define DisplayCS         DispCS
#define DisplayRES        DispRES

// Cekaci rutiny:
#define DisplayDelay60ns()      _nop_()
#define DisplayDelay140ns()     _nop_()
#define DisplayDelay100us()     SysDelay(1)

// Podminena kompilace:
//#define SSD1303_DC_CONVERTER    1       // Vyuziti interniho menice napeti 9V -> 3.3V
#define SSD1303_BUS             1       // Datova sbernice 1:1 na portu
//#define SSD1303_READ_STATUS     1       // Preklada se cteni statusu*/

//-----------------------------------------------------------------------------
// Radic displeje Samsung Ks0108b
//-----------------------------------------------------------------------------

/*// Hardwarove pripojeni displeje
#define DisplayDATA       BatDATA
#define DisplayD0         BatD0
#define DisplayD1         BatD1
#define DisplayD2         BatD2
#define DisplayD3         BatD3
#define DisplayD4         BatD4
#define DisplayD5         BatD5
#define DisplayD6         BatD6
#define DisplayD7         BatD7
#define DisplayRW         BatWRRTC
#define DisplayDI         BatALERTC
#define DisplayE          BatRDRTC
#define DisplayCS1        BatCS1
#define DisplayCS2        BatCS2
#define DisplayRESET      BatRES

// Podminena kompilace

// Datova sbernice DisplayDATA 1:1
//#define KS_BUS     1
// Obecne funkce
#define KS_ON      1*/

//-----------------------------------------------------------------------------
// Radic displeje Samsung S6B1713
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       DATA
#define DisplayD0         BatD0
#define DisplayD1         BatD1
#define DisplayD2         BatD2
#define DisplayD3         BatD3
#define DisplayD4         BatD4
#define DisplayD5         BatD5
#define DisplayD6         BatD6
#define DisplayD7         BatD7
#define DisplayA0         DispA0
#define DisplayRD         DispRD
#define DisplayWR         DispWR
#define DisplayRES        DispRES
#define DisplayCS         DispCS

// Cekaci rutiny:
#define DisplayDelay100us()     SysDelay(1)

// Podminena kompilace:
#define S6B1713_BUS             1       // Datova sbernice 1:1 na portu
//#define S6B1713_READ_STATUS     1       // Preklada se cteni statusu

//-----------------------------------------------------------------------------
// Graficky displej Univision UG-3264GMCAT01 (132x64)
//-----------------------------------------------------------------------------

/*// Rozmery displeje:
#define DISPLAY_WIDTH           132     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

// Podminena kompilace:
//#define DISPLAY_ROTATED         1       // Orientace rotace displeje (rotovany = ksanda nad displejem)
#define DISPLAY_CONTRAST        1       // Nastaveni kontrastu displeje
//#define DISPLAY_NORMAL_DISPLAY  1       // Prepinani na normalni (pozitivni) zobrazeni
#define DISPLAY_INVERSE_DISPLAY 1       // Prepinani na inverzni zobrazeni

// Cary, obdelniky atd.
#define DISPLAY_SET_AREA_FAST   1
#define DISPLAY_SET_AREA        1
#define DISPLAY_HOR_LINE        1
#define DISPLAY_FAST_VERT_LINE  1
#define DISPLAY_VERT_LINE       1
#define DISPLAY_PIXEL           1
#define DISPLAY_CHAR            1
#define DISPLAY_SYMBOL          1*/

//-----------------------------------------------------------------------------
// Graficky displej Powertip EL12864
//-----------------------------------------------------------------------------

/*// Rozmery displeje:
#define DISPLAY_WIDTH           128     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

// Podminena kompilace:
#define DISPLAY_SET_AREA_FAST   1
#define DISPLAY_SET_AREA        1
#define DISPLAY_HOR_LINE        1
#define DISPLAY_FAST_VERT_LINE  1
#define DISPLAY_VERT_LINE       1
#define DISPLAY_PIXEL           1
#define DISPLAY_CHAR            1
#define DISPLAY_SYMBOL          1*/

//-----------------------------------------------------------------------------
// Graficky displej Truly MST-G12864DGSY-23W (128x64)
//-----------------------------------------------------------------------------

// Rozmery displeje:
#define DISPLAY_WIDTH           128     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

// Podminena kompilace:
//#define DISPLAY_ROTATED         1       // Orientace rotace displeje (rotovany = ksanda nad displejem)
//#define DISPLAY_CONTRAST        1       // Nastaveni kontrastu displeje
//#define DISPLAY_NORMAL_DISPLAY  1       // Prepinani na normalni (pozitivni) zobrazeni
//#define DISPLAY_INVERSE_DISPLAY 1       // Prepinani na inverzni zobrazeni
#define DISPLAY_OFF             1       // Vypinani displeje (zapinani je automaticky pristupne)

// Cary, obdelniky atd.
#define DISPLAY_SET_AREA_FAST   1
#define DISPLAY_SET_AREA        1
#define DISPLAY_HOR_LINE        1
#define DISPLAY_FAST_VERT_LINE  1
#define DISPLAY_VERT_LINE       1
#define DISPLAY_PIXEL           1
#define DISPLAY_CHAR            1
#define DISPLAY_SYMBOL          1

//-----------------------------------------------------------------------------
// Fonty
//-----------------------------------------------------------------------------

// Podminena kompilace:
//#define DISPLAY_TAHOMA10                1
//#define DISPLAY_TAHOMA10_BOLD           1
#define DISPLAY_TAHOMA8                 1
//#define DISPLAY_LUCIDA6                 1
//#define DISPLAY_SMALL8_COND_NUMERIC     1
//#define DISPLAY_SMALL8_NUMERIC          1
//#define DISPLAY_SMALL8_WIDE_NUMERIC     1
#define DISPLAY_TAHOMA10_BOLD_NUMERIC	1
#define DISPLAY_ARIAL12_BOLD_NUMERIC    1
//#define DISPLAY_ARIAL14_BOLD_NUMERIC    1
#define DISPLAY_MYRIAD32                1
//#define DISPLAY_MYRIAD40                1
//#define DISPLAY_CHAR_WIDTH      1
#define DISPLAY_TEXT_WIDTH              1


//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Podminena kompilace:

#define DISPLAY_LINE                    1

// Vety
#define DISPLAY_STRING                  1
#define DISPLAY_STRING_RIGHT            1
#define DISPLAY_STRING_CENTER           1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER           1
#define DISPLAY_NUMBER                  1
#define DISPLAY_CHAR_FORMATED           1

// Editace
#define DISPLAY_EDIT                    1
#define DISPLAY_EDIT_CHAR_WIDTH         5       // Sirka 1 cislice (podle pouziteho fontu)
#define DISPLAY_EDIT_DOT_WIDTH          1       // Sirka tecky a dvojtecky pri editaci cisla, data a casu (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_BEFORE_DOT      2       // Sirka mezery pred desetinnou carkou (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_AFTER_DOT       2       // Sirka mezery za desetinnou carkou (podle pouziteho fontu)
#define DISPLAY_EDIT_TEXT               1

//#define DISPLAY_BARGRAF                 1

// Dialog
#define DISPLAY_DIALOG                  1
//#define DIALOG_OTOCENE_KLAVESY          1        // Zda se ma otocit zobrazeni tlacitek Enter a Esc
#define DIALOG_BUTTON_Y                 (DISPLAY_HEIGHT - DIALOG_BUTTON_HEIGHT) // Y-ova souradnice tlacitek

// Menu
#define DISPLAY_MENU                    1
#define MENU_MAX_ITEMS                  5       // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
#define DISPLAY_MENU_LINE_HEIGHT        16      // Vyska radku v pixelech
#define DISPLAY_MENU_Y                  8       // Y-ova souradnice pocatku menu v pixelech
#define DISPLAY_MENU_LINES_COUNT        3       // Pocet radku v menu
#define DISPLAY_MENU_ARROW_DY           4       // Vertikalni posun sipky (podle pouziteho fontu)
#define DISPLAY_CHOOSE_ITEM             1

// Jazyky
//#define DISPLAY_LANGUAGES               1       // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
#define DisplayEditExecute(Znaky)

//-----------------------------------------------------------------------------

#endif // __Hardware_H__
