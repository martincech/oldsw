//*****************************************************************************
//
//    Servo.c - Servo control module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "..\inc\Servo.h"
#include "..\Auto\Amisc\Alog.h"

static byte timer[ _SERVO_LAST];


// Lokalni funkce :

static byte NormalizeValue( byte value);
// Zakoduje merene napeti do dohodnuteho rozsahu

static byte ReadControl( byte address);
// Cteni ridiciho napeti

static byte ReadMeasure( byte address);
// Cteni zpetneho napeti

//-----------------------------------------------------------------------------
// Inicializace serv
//-----------------------------------------------------------------------------

void ServoInit( void)
// Inicializuje modul serv
{
byte i;

   for( i = 0; i < _SERVO_LAST; i++){
      timer[ i] = 0;
   }
   AdcCS0 = 1;    // prvni prevodnik
   AdcCS1 = 1;    // druhy prevodnik
   AdcInit();     // sbernice
} // ServoInit

void ResetServoTimer(byte Cislo) {
  // Vynuluje timer u serva cislo <Cislo> - pouziva se pri prepnuti rezimu
  timer[Cislo]=0;
}

//-----------------------------------------------------------------------------
// Hlidani serva
//-----------------------------------------------------------------------------

void ServoCheck( void)
// Kontroluje cinnost serv. Volat 1x za sekundu
{
byte i;
byte control_value;
byte measure_value;
byte difference;        // absolutni rozdil

   // Pruchod pres vsechna serva :
   for( i = 0; i < _SERVO_LAST; i++){
      control_value = ReadControl( i);
      AdcWait();                         // Radeji prodleva po CS
      measure_value = ReadMeasure( i);
      AlogServoPosition( i, NormalizeValue( control_value),
                            NormalizeValue( measure_value));  // hlaseni o poloze
      if( control_value < SERVO_OFF_LIMIT){
         // utrzeny potenciometr, ridici napeti pod urovni
         AlogServoStatus( i, ASER_CONTROL);
         continue;
      }
      if( measure_value < SERVO_OFF_LIMIT){
         // zpetne napeti, mensi nez limit, servo vypnuto
         AlogServoStatus( i, ASER_OFF);
         continue;
      }
      // servo v provozu, kontroluj
      difference = control_value > measure_value ?
                   control_value - measure_value :
                   measure_value - control_value;
      if( difference > SERVO_ACCURACY){
         // nesouhlasi polohy, citej cas
         timer[ i]++;
         if( timer[ i] == 0){
            timer[ i] = 0xFF;                                  // limitace pri pretoku
         }
         if( timer[ i] > SERVO_RESPONSE){
            AlogServoStatus( i, ASER_TIMEOUT);  // prekrocen cas
         } else {
            AlogServoStatus( i, ASER_RUNNING);  // zatim v casovem limitu
         }
      } else {
         // souhlasi polohy, nuluj cas
         timer[ i] = 0;
         AlogServoStatus( i, ASER_OK);
      }
   }
} // ServoCheck

//-----------------------------------------------------------------------------
// Normalizace mereni
//-----------------------------------------------------------------------------

static byte NormalizeValue( byte value)
// Zakoduje merene napeti do dohodnuteho rozsahu
{
   // 21.3.2003: Mereni se provadi od 2V do 10V, ne od 1.5V.
   if (value <= SERVO_MIN) return(0);   // Je vypnuto koduj jako 0
   if (value >= SERVO_MAX) return(15);  // Je na max
   value -= SERVO_MIN;                  // odecti offset
   value /= SERVO_STEP;                 // vydel krokem, 0. krok koduj opet jako 0
   // 18.4.2003: Krok jsem zmenil ze 13 na 12, presne to bylo 12,125. PRoblem nyni muze byt ten, ze i kdyz je maximalni poloha 15, muze po vydeleni
   // krokem 12 vyjit i poloha 16 (pri plne otevrenem servu, tj. kdyz dava SERVO_MAX, (SERVO_MAX-SERVO_MIN/SERVO_STEP=245/12=16.166) tak se servo
   // indikuje jako vypnute. Omezim to tedy na max. hodnotu 15.
   if (value>15) value=15;
   return value;
} // NormalizeValue

//-----------------------------------------------------------------------------
// Cteni rizeni
//-----------------------------------------------------------------------------

static byte ReadControl( byte address)
// Cteni ridiciho napeti
{
byte value;

   switch( address){
      case SERVO_0 :
         SERVO_0_CONTROL_CS = 0;
         value = AdcRead( SERVO_0_CONTROL);
         SERVO_0_CONTROL_CS = 1;
         return( value);

      case SERVO_1 :
         SERVO_1_CONTROL_CS = 0;
         value = AdcRead( SERVO_1_CONTROL);
         SERVO_1_CONTROL_CS = 1;
         return( value);

      case SERVO_2 :
         SERVO_2_CONTROL_CS = 0;
         value = AdcRead( SERVO_2_CONTROL);
         SERVO_2_CONTROL_CS = 1;
         return( value);

      case SERVO_3 :
         SERVO_3_CONTROL_CS = 0;
         value = AdcRead( SERVO_3_CONTROL);
         SERVO_3_CONTROL_CS = 1;
         return( value);

      case SERVO_4 :
         SERVO_4_CONTROL_CS = 0;
         value = AdcRead( SERVO_4_CONTROL);
         SERVO_4_CONTROL_CS = 1;
         return( value);

      case SERVO_5 :
         SERVO_5_CONTROL_CS = 0;
         value = AdcRead( SERVO_5_CONTROL);
         SERVO_5_CONTROL_CS = 1;
         return( value);
   }
} // ReadControl

//-----------------------------------------------------------------------------
// Cteni zpetneho hlaseni
//-----------------------------------------------------------------------------

static byte ReadMeasure( byte address)
// Cteni zpetneho napeti
{
byte value;

   switch( address){
      case SERVO_0 :
         SERVO_0_MEASURE_CS = 0;
         value = AdcRead( SERVO_0_MEASURE);
         SERVO_0_MEASURE_CS = 1;
         return( value);

      case SERVO_1 :
         SERVO_1_MEASURE_CS = 0;
         value = AdcRead( SERVO_1_MEASURE);
         SERVO_1_MEASURE_CS = 1;
         return( value);

      case SERVO_2 :
         SERVO_2_MEASURE_CS = 0;
         value = AdcRead( SERVO_2_MEASURE);
         SERVO_2_MEASURE_CS = 1;
         return( value);

      case SERVO_3 :
         SERVO_3_MEASURE_CS = 0;
         value = AdcRead( SERVO_3_MEASURE);
         SERVO_3_MEASURE_CS = 1;
         return( value);

      case SERVO_4 :
         SERVO_4_MEASURE_CS = 0;
         value = AdcRead( SERVO_4_MEASURE);
         SERVO_4_MEASURE_CS = 1;
         return( value);

      case SERVO_5 :
         SERVO_5_MEASURE_CS = 0;
         value = AdcRead( SERVO_5_MEASURE);
         SERVO_5_MEASURE_CS = 1;
         return( value);
   }
} // ReadMeasure

