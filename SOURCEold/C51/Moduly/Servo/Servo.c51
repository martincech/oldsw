//*****************************************************************************
//
//    Servo.c - Servo control module
//    Version 2.0, (c) Vymos
//
//*****************************************************************************

#include "..\inc\Servo.h"
#include "..\Auto\Amisc\Alog.h"
#include "..\Auto\Amisc\AFAir.h" // Prevod cerstveho vzduchu na vystup prevodniku

static xdata word timer[ _SERVO_LAST];


// Lokalni funkce :

static byte NormalizeValue( byte value);
// Zakoduje merene napeti do dohodnuteho rozsahu

static byte ReadControl( byte address);
// Cteni ridiciho napeti

static byte ReadMeasure( byte address);
// Cteni zpetneho napeti

//-----------------------------------------------------------------------------
// Inicializace serv
//-----------------------------------------------------------------------------

void ServoInit( void)
// Inicializuje modul serv
{
byte i;

   for( i = 0; i < _SERVO_LAST; i++){
      timer[ i] = 0;
   }
   AdcCS0 = 1;    // prvni prevodnik
   AdcCS1 = 1;    // druhy prevodnik
   AdcInit();     // sbernice
} // ServoInit

void ResetServoTimer(byte Cislo) {
  // Vynuluje timer u serva cislo <Cislo> - pouziva se pri prepnuti rezimu
  timer[Cislo]=0;
}

//-----------------------------------------------------------------------------
// Hlidani serva
//-----------------------------------------------------------------------------

void ServoCheck( void)
// Kontroluje cinnost serv. Volat 1x za sekundu
{
byte i;
byte control_value;
byte measure_value;
byte difference;        // absolutni rozdil
bit ServoMimoPolohu;    // Flag, ze je dane servo mimo nastavenou polohu a ma se citat doba timeoutu

  // Serva v naporu a podlaze jsou mechanicky nastavena tak, ze jdou od 2V do 8V, pri ridicim napeti >8V uz zpetne hlaseni zustane na 8V a nejde dal na 10V.
  // Pri ridicim napeti vetsim nez SERVO_CONTROL_LIMIT prestavam kontrolovat pozici serva a prohlasim jeho polohu za OK.

   // Pruchod pres vsechna serva :
   for (i = 0; i < _SERVO_LAST; i++) {
      control_value = ReadControl( i);
      AdcWait();                         // Radeji prodleva po CS
      measure_value = ReadMeasure( i);
      AlogServoPosition( i, NormalizeValue( control_value),
                            NormalizeValue( measure_value));  // hlaseni o poloze
      if( control_value < SERVO_OFF_LIMIT){
         // utrzeny potenciometr, ridici napeti pod urovni
         // Pozor, u serva topeni, kdyz je prepinac rezimu v jine poloze nez v topeni (auto nebo rucne), tak ridici napeti je hardwarove nastaveno
         // na nulu a hlasilo by to tak utrzene servo.
         if (i==SERVO_TOPENI && !(AutoData.PrepinacRezimu==ASW_TOPENI_RUCNE || (AutoData.PrepinacRezimu==ASW_AUTOMAT && AutoData.AutomatickyRezim==AUTOREZIM_TOPENI))) {
           // Prepinac rezimu neni v poloze topeni (nebo auto a topi se) a kontroluju prave servo topeni => tuto chybu ignoruju
           AlogServoStatus( i, ASER_OK);
         } else {
           AlogServoStatus( i, ASER_CONTROL);
         }//else
         continue;
      }
      if( measure_value < SERVO_OFF_LIMIT){
         // zpetne napeti, mensi nez limit, servo vypnuto
         AlogServoStatus( i, ASER_OFF);
         continue;
      }

      // Servo v provozu, kontroluj
      // 24.11.2003: Poloha serva musi splnovat nasledujici:
      // 1. Normalni servo (topeni a recirkulace): zmerene napeti musi odpovidat ridicimu s presnosti SERVO_ACCURACY za vsech okolnosti
      // 2. Servo, u ktereho se mechanicky nastavuje doraz (podlaha, napor): do ridiciho napeti SERVO_CONTROL_LIMIT kontoroluju servo stejne jako normalni
      // servo, tj., s presnosti SERVO_ACCURACY. Pri ridicim napeti vyssim nez SERVO_CONTROL_LIMIT uz se namerene napetio nemusi zvysovat (servo uz je na
      // mechanickem dorazu), tj. kontroluji jen, zda je namerene napeti vetsi nez SERVO_CONTROL_LIMIT.

      ServoMimoPolohu=0;    // Default je servo ve spravne poloze

      // Pokud jde o servo v naporu nebo podlaze, nekontroluju servo v uplne otevrenem stavu, kvuli mechanickemu zablokovani
      if ((i==SERVO_NAPOR1 || i==SERVO_NAPOR2 || i==SERVO_PODLAHA1 || i==SERVO_PODLAHA2 || i==SERVO_PODLAHA3 || i==SERVO_PODLAHA4) && control_value > SERVO_CONTROL_LIMIT) {
        // Jde o servo naporu/podlahy a ridici napeti je uz moc velke, takze servo musi davat jen vetsi napeti nez SERVO_CONTROL_LIMIT, vic uz mu
        // mechanicke zablokovani nedovoli.
        if (measure_value<SERVO_CONTROL_LIMIT) {
          // Nesouhlasi polohy, citej cas
          ServoMimoPolohu=1;
        }//if
      } else {
        // Jde bud o servo recirkulace/topeni nebo o serva naporu/podlahy, u nichz je ridici napeti pod mezi. Namerene napeti tak musi odpovidat ridicimu.
        difference = control_value > measure_value ?
                    control_value - measure_value :
                    measure_value - control_value;
        if (difference > SERVO_ACCURACY) {
          // Nesouhlasi polohy, citej cas
          ServoMimoPolohu=1;
        }//if
      }//else

      if (ServoMimoPolohu) {
        // Servo je mimo nastavenou polohu, citam cas, po ktery je mimo polohu
        timer[ i]++;
        if (timer[ i] == 0) {
          timer[ i] = 0xFFFF;                                  // limitace pri pretoku
        }
        if (timer[ i] > SERVO_RESPONSE) {
          AlogServoStatus(i, ASER_TIMEOUT);  // prekrocen cas
        } else {
          AlogServoStatus(i, ASER_RUNNING);  // zatim v casovem limitu
        }
      } else {
        // Poloha serva je v poradku
        timer[i] = 0;
        AlogServoStatus(i, ASER_OK);
      }//else
   }
} // ServoCheck

//-----------------------------------------------------------------------------
// Normalizace mereni
//-----------------------------------------------------------------------------

static byte NormalizeValue( byte value)
// Zakoduje merene napeti do dohodnuteho rozsahu
{
   // 21.3.2003: Mereni se provadi od 2V do 10V, ne od 1.5V.
   if (value <= SERVO_MIN) return(0);   // Je vypnuto koduj jako 0
   if (value >= SERVO_MAX) return(15);  // Je na max
   value -= SERVO_MIN;                  // odecti offset
   value /= SERVO_STEP;                 // vydel krokem, 0. krok koduj opet jako 0
   // 18.4.2003: Krok jsem zmenil ze 13 na 12, presne to bylo 12,125. PRoblem nyni muze byt ten, ze i kdyz je maximalni poloha 15, muze po vydeleni
   // krokem 12 vyjit i poloha 16 (pri plne otevrenem servu, tj. kdyz dava SERVO_MAX, (SERVO_MAX-SERVO_MIN/SERVO_STEP=245/12=16.166) tak se servo
   // indikuje jako vypnute. Omezim to tedy na max. hodnotu 15.
   if (value>15) value=15;
   return value;
} // NormalizeValue

//-----------------------------------------------------------------------------
// Cteni rizeni
//-----------------------------------------------------------------------------

static byte ReadControl( byte address)
// Cteni ridiciho napeti
{
byte value;

   switch (address) {
      case SERVO_NAPOR1:
      case SERVO_NAPOR2:
      case SERVO_RECIRKULACE:
      case SERVO_PODLAHA1:
      case SERVO_PODLAHA2:
      case SERVO_PODLAHA3:
      case SERVO_PODLAHA4:
        // Serva ventilace nemaji ridici signal privedeny do jednotky - pocitam ho z cerstveho vzduchu
        return (PrepoctiCerstvyVzduchNaADC(AutoData.CerstvyVzduch));

/*      case SERVO_NAPOR1 :
         SERVO_NAPOR1_CONTROL_CS = 0;
         value = AdcRead( SERVO_NAPOR1_CONTROL);
         SERVO_NAPOR1_CONTROL_CS = 1;
         return( value);

      case SERVO_NAPOR2 :
         SERVO_NAPOR2_CONTROL_CS = 0;
         value = AdcRead( SERVO_NAPOR2_CONTROL);
         SERVO_NAPOR2_CONTROL_CS = 1;
         return( value);

      case SERVO_RECIRKULACE :
         SERVO_RECIRKULACE_CONTROL_CS = 0;
         value = AdcRead( SERVO_RECIRKULACE_CONTROL);
         SERVO_RECIRKULACE_CONTROL_CS = 1;
         return( value);

      case SERVO_PODLAHA1 :
         SERVO_PODLAHA1_CONTROL_CS = 0;
         value = AdcRead( SERVO_PODLAHA1_CONTROL);
         SERVO_PODLAHA1_CONTROL_CS = 1;
         return( value);

      case SERVO_PODLAHA2 :
         SERVO_PODLAHA2_CONTROL_CS = 0;
         value = AdcRead( SERVO_PODLAHA2_CONTROL);
         SERVO_PODLAHA2_CONTROL_CS = 1;
         return( value);

      case SERVO_PODLAHA3 :
         SERVO_PODLAHA3_CONTROL_CS = 0;
         value = AdcRead( SERVO_PODLAHA3_CONTROL);
         SERVO_PODLAHA3_CONTROL_CS = 1;
         return( value);*/

      case SERVO_TOPENI :
         SERVO_TOPENI_CONTROL_CS = 0;
         value = AdcRead(SERVO_TOPENI_CONTROL);
         SERVO_TOPENI_CONTROL_CS = 1;
         return (value);
   }
} // ReadControl

//-----------------------------------------------------------------------------
// Cteni zpetneho hlaseni
//-----------------------------------------------------------------------------

static byte ReadMeasure( byte address)
// Cteni zpetneho napeti
{
byte value;

   switch( address){
      case SERVO_NAPOR1:
         SERVO_NAPOR1_MEASURE_CS = 0;
         value = AdcRead( SERVO_NAPOR1_MEASURE);
         SERVO_NAPOR1_MEASURE_CS = 1;
         return( value);

      case SERVO_NAPOR2:
         SERVO_NAPOR2_MEASURE_CS = 0;
         value = AdcRead( SERVO_NAPOR2_MEASURE);
         SERVO_NAPOR2_MEASURE_CS = 1;
         return( value);

      case SERVO_RECIRKULACE :
         SERVO_RECIRKULACE_MEASURE_CS = 0;
         value = AdcRead( SERVO_RECIRKULACE_MEASURE);
         SERVO_RECIRKULACE_MEASURE_CS = 1;
         return( value);

      case SERVO_PODLAHA1 :
         SERVO_PODLAHA1_MEASURE_CS = 0;
         value = AdcRead( SERVO_PODLAHA1_MEASURE);
         SERVO_PODLAHA1_MEASURE_CS = 1;
         return( value);

      case SERVO_PODLAHA2 :
         SERVO_PODLAHA2_MEASURE_CS = 0;
         value = AdcRead( SERVO_PODLAHA2_MEASURE);
         SERVO_PODLAHA2_MEASURE_CS = 1;
         return( value);

      case SERVO_PODLAHA3 :
         SERVO_PODLAHA3_MEASURE_CS = 0;
         value = AdcRead( SERVO_PODLAHA3_MEASURE);
         SERVO_PODLAHA3_MEASURE_CS = 1;
         return( value);

      case SERVO_PODLAHA4 :
         SERVO_PODLAHA4_MEASURE_CS = 0;
         value = AdcRead( SERVO_PODLAHA4_MEASURE);
         SERVO_PODLAHA4_MEASURE_CS = 1;
         return( value);

      case SERVO_TOPENI :
         SERVO_TOPENI_MEASURE_CS = 0;
         value = AdcRead( SERVO_TOPENI_MEASURE);
         SERVO_TOPENI_MEASURE_CS = 1;
         return( value);
   }
} // ReadMeasure

