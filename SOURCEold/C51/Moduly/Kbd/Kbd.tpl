//*****************************************************************************
//
//    Kbd.tpl -  Keyboard services code template
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "..\inc\System.h"     // "operacni system" - kvuli SysDelay()

byte __xdata__ _kbd_counter;              // citac delky & kontext, inicializace XDATA musi byt v kodu

// horni 2 bity _kbd_counter jsou kontextova informace :
#define KBD_IDLE         0x00       // cekame na udalost
#define KBD_FIRST_PRESS  0x40       // prvni impuls od klavesy
#define KBD_WAIT_AUTO    0x80       // stisknuto, ceka se na autorepeat
#define KBD_WAIT_REPEAT  0xC0       // stisknuto, ceka se na dalsi repeat

#define KBD_TEST         0xC0       // maska pro testovani kontextu
#define KBD_COUNTER      0x3F       // maska citace

// Lokalni funkce :

static byte ReadKey( void);
// Vrati kod prave stisknute klavesy


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void KbdInit(void) {
  // Inicializace
  _kbd_counter = 0;                     // Inicializaci XDATA musim delat v kodu zvlast
}

#ifdef KBD_POWER_UP

//-----------------------------------------------------------------------------
// Klavesa po zapnuti
//-----------------------------------------------------------------------------

byte KbdPowerUpKey( void)
// Vrati klavesu, ktera je drzena po zapnuti nebo K_RELEASED
{
byte key;

   key = ReadKey();
   SysDelay( 10);
   return( ReadKey());
} // KbdPowerUpKey

//-----------------------------------------------------------------------------
// Pusteni klavesy po zapnuti
//-----------------------------------------------------------------------------

void KbdPowerUpRelease( void)
// Ceka na pusteni klavesy, drzene po zapnuti
{
   while( ReadKey() != K_RELEASED){
      WatchDog();            // cekame na pusteni
   }
} // KbdPowerUpRelease
#endif

#ifdef KBD_GETCH
//-----------------------------------------------------------------------------
// Cekani na klavesu
//-----------------------------------------------------------------------------

byte getch( void)
// Ceka na stisknuti klavesy, vrati ji
{
byte key;

   while((key = KbdGet()) == K_IDLE){
       WatchDog();
   }
   while( KbdGet() != K_RELEASED){
       WatchDog();
   }
   return( key);
} // getch
#endif

//-----------------------------------------------------------------------------
// Test
//-----------------------------------------------------------------------------

byte KbdGet( void)
// Testuje stisknuti klavesy, vraci klavesu nebo
// K_RELEASED neni-li stisknuto nic, volat periodicky
{
byte key;

   if( (_kbd_counter & KBD_TEST) == KBD_FIRST_PRESS){
      // prodleva po prvnim doteku
      if( (_kbd_counter & KBD_COUNTER) != 0){
         return( K_IDLE);            // stale cekame
      }
      // konec prodlevy, precti klavesu
      key = ReadKey();
      if( key == K_RELEASED){
         // pusteni po prodleve
         _kbd_counter = 0;           // neaktivni casovac, KBD_IDLE
         return( K_IDLE);            // neni stisknuto, jen kratky impuls
      }
   } else {
      // ve vsech ostatnich kontextech
      key = ReadKey();
   }
   if( key == K_RELEASED){
      // nic neni zmacknuto (pro vsechny kontexty krome KBD_FIRST_PRESS)
      if( (_kbd_counter & KBD_TEST) == KBD_IDLE){
         return( K_IDLE);         // cekame na prvni dotek
      }
      // pusteni klavesy
      _kbd_counter = 0;           // neaktivni casovac, KBD_IDLE
      return( K_RELEASED);        // pusteni klavesy
   }
   switch( _kbd_counter & KBD_TEST){
      case KBD_IDLE :
         _kbd_counter = KBD_FIRST_PRESS | KBD_DEBOUNCE; // zaciname cekat na ustaleni
         return( K_IDLE);

      case KBD_FIRST_PRESS :
         // konec prodlevy po prvnim doteku
         _kbd_counter = KBD_WAIT_AUTO | KBD_AUTOREPEAT_START; // zaciname cekat na autorepeat
         return( key);     // platna klavesa

      case KBD_WAIT_AUTO :
      case KBD_WAIT_REPEAT :
         if( (_kbd_counter & KBD_COUNTER) != 0){
            return( K_IDLE); // stale cekame
         }
         // zahajeni autorepeat
         _kbd_counter = KBD_WAIT_REPEAT | KBD_AUTOREPEAT_SPEED; // cekame na dalsi repeat
         return( key | K_REPEAT);   // platna klavesa
      }
} // KbdGet
