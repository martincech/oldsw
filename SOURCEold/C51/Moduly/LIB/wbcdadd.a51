;------------------------------------------------------------------------------
;
; wbcdadd.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    WBCDADD

?PR?_wbcdadd?WBCDADD     SEGMENT CODE

        PUBLIC  _wbcdadd

; //-----------------------------------------------------------------------------
; // wbcdadd
; //-----------------------------------------------------------------------------
;
; dword wbcdadd( word x /*R6:R7*/, word y /*R4:R5*/)

        RSEG  ?PR?_wbcdadd?WBCDADD
	USING	0
_wbcdadd:
			; SOURCE LINE # 266
; // vrati x+y
; {
			; SOURCE LINE # 268
	  mov     a, r7                   ; LSB parametr X
	  add     a, r5                   ; secti s LSB Y
	  da      a                       ; BCD korekce
	  mov     r7, a                   ; vysledek
	  mov     a, r6                   ; MSB parametr X
	  addc    a, r4                   ; pricti vsechny prenosy
	  da      a                       ; BCD korekce
	  mov     r6, a                   ; vysledek MSB
	  clr     a
	  mov     r4, a                   ; navrat vypln
	  addc    a, acc                  ; jen prenos
	  mov     r5, a                   ; navrat prenosu
; } // wbcdadd
			; SOURCE LINE # 283
	RET

        END
