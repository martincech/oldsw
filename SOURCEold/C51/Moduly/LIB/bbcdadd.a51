;------------------------------------------------------------------------------
;
; bbcdadd.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    BBCDADD

?PR?_bbcdadd?BBCDADD     SEGMENT CODE

        PUBLIC  _bbcdadd

; //-----------------------------------------------------------------------------
; // bbcdadd
; //-----------------------------------------------------------------------------

; word bbcdadd( byte x /*R7*/, byte y /*R5*/)

        RSEG  ?PR?_bbcdadd?BBCDADD
	USING	0
_bbcdadd:
			; SOURCE LINE # 248
; // vrati x+y
; {
			; SOURCE LINE # 250
	  mov     a, r7                   ; parametr X
	  add     a, r5                   ; secti s Y
	  da      a                       ; BCD korekce
	  mov     r7, a                   ; navratova hodnota
	  clr     a                       ; nuluj prenos
	  addc    a, acc                  ; jen prenos
	  mov     r6, a                   ; navratova hodnota
; } // bbcdadd
			; SOURCE LINE # 260
	RET

        END
