;------------------------------------------------------------------------------
;
; wbcdinc.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    WBCDINC

?PR?_wbcdinc?WBCDINC     SEGMENT CODE

        PUBLIC  _wbcdinc

; //-----------------------------------------------------------------------------
; // wbcdinc
; //-----------------------------------------------------------------------------
;
; dword wbcdinc( word x) // R6:R7

        RSEG  ?PR?_wbcdinc?WBCDINC
	USING	0
_wbcdinc:
			; SOURCE LINE # 189
; // vrati x+y
; {
			; SOURCE LINE # 191
	  mov     a, r7                   ; LSB parametr X
	  add     a, #1                   ; pricti 1
	  da      a                       ; BCD korekce
	  mov     r7, a                   ; vysledek
	  mov     a, r6                   ; MSB parametr X
	  addc    a, #0                   ; pricti vsechny prenosy
	  da      a                       ; BCD korekce
	  mov     r6, a                   ; vysledek MSB
	  clr     a
	  mov     r4, a                   ; navrat vypln
	  addc    a, acc                  ; jen prenos
	  mov     r5, a                   ; navrat prenosu
; } // wbcdinc
			; SOURCE LINE # 206
	RET

        END

