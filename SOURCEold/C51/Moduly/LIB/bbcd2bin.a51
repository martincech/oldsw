;------------------------------------------------------------------------------
;
; ABCD.A51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    BBCD2BIN

?PR?_bbcd2bin?BBCD2BIN    SEGMENT CODE

	PUBLIC	_bbcd2bin



; //-----------------------------------------------------------------------------
; // bbcd2bin
; //-----------------------------------------------------------------------------

; byte bbcd2bin( byte x)

        RSEG  ?PR?_bbcd2bin?BBCD2BIN
	USING	0
_bbcd2bin:
; // prevede 0..0x99 do bin
; {
			; SOURCE LINE # 143
;    return( (x >> 4) * 10 + (x & 0x0F));
			; SOURCE LINE # 144
	MOV  	A,R7
	SWAP 	A
	ANL  	A,#0FH
	MOV  	B,#0AH
	MUL  	AB
	MOV  	R6,A
	MOV  	A,R7
	ANL  	A,#0FH
	ADD  	A,R6
	MOV  	R7,A
; } // bbcd2bin
			; SOURCE LINE # 145
	RET

        END
