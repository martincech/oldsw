//*****************************************************************************
//
//    Sfont.c       Scalable Font services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Sfont.h"

#include "Tahoma.fnt"

#define MAX_HEIGHT  VYSKATAHOMA18

byte xdata LastColumn[ MAX_HEIGHT];

typedef union {
   word w;
   byte b[ 2];
} TWord;

//-----------------------------------------------------------------------------
// Zapis obdelnikoveho vzoru
//-----------------------------------------------------------------------------

void DisplayPatternX( byte code *pattern, byte w, byte h)
// Nakresli obdelnik vyplneny vzorem z adresy <pattern>
// <w> je sirka v osmicich pixelu, <h> je vyska v pixelech
// postupuje po radcich
{
byte i, j;
word Address;        // bitova adresa
byte BWidth;
byte BMask;
byte Shift;
word Tmp;
byte code *p;

   DisplayEnable();
   Address = 0;
   BWidth  = w / 8;                    // cele osmice
   BMask   = 0xFF << (8 - (w % 8));    // nedokoncena osmice, jednicky od LSB v poctu (w % 8)

   for( j = 0; j < h; j++){
      // jeden radek
      SetCursor( _offset);
      WriteCommand( DISPLAY_CMD_MWRITE);
      p     = &pattern[ Address / 8];                // bytova adresa
      Shift = Address % 8;                           // ofset LSB
      for( i = 0; i < BWidth; i++){
         Tmp      = *(word code *)p++;               // bytova adresa, nacti dva byty
         Tmp    <<= Shift;                           // eliminuj ofset LSB
         WriteData(  Tmp >> 8);                      // zapis MSB
      }
      // neuplny byte :
      if( BMask){
         Tmp      = *(word code *)p;                 // bytova adresa, nacti dva byty
         Tmp    <<= Shift;                           // eliminuj ofset LSB
         WriteData( (Tmp >> 8) & BMask);             // zapis neuplny byte
      }
      Address += w;                                  // dalsi 'bitovy' radek
      _offset += DISPLAY_X_BYTE;
   }
   DisplayDisable();
} // DisplayPattern

//---------------------------------------------------------------------------------------------
// putchar
//---------------------------------------------------------------------------------------------

void ZnakTahoma18( word x, byte y, char ch)
{
word Accu;
byte code *Char;
byte OffsetX;    // bitovy ofset X
byte ByteWidth;  // bytova sirka fontu
byte Width;      // pixelova sirka fontu
byte i, j;

   DisplayEnable();
   Char      = &TAHOMA18[ ch - 32][0];                   // prvni byte znaku
   Width     = *Char;                                    // prvni byte znaku je sirka v pixelech
   Char++;                                               // prvni byte bitmapy znaku
   ByteWidth = (*Char + 7) / 8;                          // bytova sirka
   OffsetX   = x % 8;                                    // bitovy ofset souradnice
   x        /= 8;                                        // bytova souradnice x
//V
   DisplayMoveTo( x, y);
   DisplayPatternX( Char, Width, VYSKATAHOMA18);
   return;
//V
   WriteCommand( DISPLAY_CMD_CSR_DOWN);                  // pohyb kurzoru dolu
   // svisle pruhy - pruchod pres X :
   for( i = 0; i < ByteWidth; i++){
      DisplayMoveTo( x, y);
      SetCursor( _offset);
      // zapis ve svislem smeru :
      WriteCommand( DISPLAY_CMD_MWRITE);
      for( j = 0; j < VYSKATAHOMA18; j++){
         Accu   = *Char;                                 // byte znaku
         Accu <<= OffsetX;                               // posun na pozadovany ofset
         WriteData( LastColumn[ j] | (Accu & 0xFF));     // stary obsah s novym znakem do displeje
         LastColumn[ j] = Accu >> 8;                     // horni cast znaku priste
         Char++;                                         // dalsi byte v ose Y
      } // souradnice Y
      x++;                                               // nasledujici bytova souradnice X
   } // souradnice X
   WriteCommand( DISPLAY_CMD_CSR_RIGHT);                 // standardni smer pohybu
   DisplayDisable();
} // ZnakTahoma18

//---------------------------------------------------------------------------------------------
// cputs
//---------------------------------------------------------------------------------------------

void VetaTahoma18( word x, byte y, char code *s)
{
char ch;

   if( !s){
      return;
   }
   VymazPosledniSloupec();
   ch = *s;
   while( ch){
      ZnakTahoma18( x, y, ch);
      x += (word)TAHOMA18[ ch - 32][0] + TAHOMA18_MEZERA;
      s++;
      ch = *s;
   }
} // VetaTahoma18

//---------------------------------------------------------------------------------------------
// width
//---------------------------------------------------------------------------------------------

word SirkaTahoma18( char code *s)
{
word Width;
char ch;

   if( !s){
      return( 0);
   }
   ch = *s;
   if( !ch){
      return( 0);
   }
   Width = 0;
   while( ch){
      Width += (word)TAHOMA18[ ch - 32][0] + TAHOMA18_MEZERA;
      s++;
      ch = *s;
   }
   return( Width - TAHOMA18_MEZERA);  // mezera za poslednim se nepocita
} // SirkaTahoma18

//---------------------------------------------------------------------------------------------
// mazani
//---------------------------------------------------------------------------------------------

void VymazPosledniSloupec()
{
byte xdata *p;
byte i;

   p = LastColumn;
   for( i = 0; i < MAX_HEIGHT; i++){
      *p = 0;
      p++;
   }
} // VymazPosledniSloupec


