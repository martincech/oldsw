//*****************************************************************************
//
//   MBRDiscr.h      Modbus process reply - discrete commands
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBRDiscr_H__
   #define __MBRDiscr_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MBRReadDiscrete( word Address, word Count);
// Precte pole binarnich hodnot o delce <Count> bitu

#endif
