//*****************************************************************************
//
//   MBCom.c     Modbus serial communication
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "..\inc\MBCom.h"

// globalni data :
byte xdata    ComBuffer[ COM_DATA_SIZE];        // datovy buffer
com_pointer_t ComDataSize;                      // delka prijatych/vysilanych dat

byte volatile ComStatus;               // stav zarizeni
word volatile ComTimeoutCounter;       // timeout counter

// lokalni data :
static byte _ComParity;
static byte _ComMode;
static com_pointer_t _ComPointer;      // Rx/Tx buffer pointer
static word _ComTimeout;               // meziznakovy timeout
#ifdef COM_TX_SPACE
static word __xdata__ _ComTxSpace;     // mezera mezi Rx-Tx
#endif // COM_TX_SPACE

//-----------------------------------------------------------------------------
// Inicializace komunikace
//-----------------------------------------------------------------------------

TYesNo ComInit( word Baud, byte Parity, byte Mode)
// Inicializace komunikace
{
   ComDisableInt();
   #ifdef ComTxDisable
      ComTxDisable();                  // RS485 disable Tx
   #endif
   #if DOUBLE_BAUDRATE_T1 == 2
      PCON |=  PCON_SMOD1;
   #else
      PCON &= ~PCON_SMOD1;
   #endif
   #ifdef COM_TIMER_1
      // Timer 1 jako baud generator :
      SetTimer1Baud( Baud);
      TMOD &= 0x0F;
      TMOD |= TMOD_M81;                // timer, mode 2, TR1 control
      TR1   = 1;                       // run timer 1
   #elif COM_TIMER_2
      // Timer 2 jako baud generator :
      if( Baud == COM_115200Bd){
         SetTimer2Baud( 115200);
      } else {
         SetTimer2Baud( Baud);
      }
      T2CON  = T2CON_COM;              // TR2=1, RCLK, TCLK
   #else
      // ED2 ma baudrate timer, mozna casem doplnit
      #error "Neni definovan casovac pro RS232"
   #endif
   switch( Parity){
      case COM_PARITY_EVEN :
      case COM_PARITY_ODD  :
      case COM_PARITY_MARK :
         SCON = SCON_SMTIMER9;
         break;
      case COM_PARITY_NONE :
      case COM_PARITY_7BITS_EVEN :
      case COM_PARITY_7BITS_ODD :
      case COM_PARITY_7BITS_MARK :
         SCON = SCON_SMTIMER8;
         break;
      default :
         return( NO);
   }
   // kontextove promenne :
   ComStatus         = COM_STATUS_STOPPED;
   ComDataSize       = 0;
   ComTimeoutCounter = 0;

   _ComParity  = Parity;
   _ComMode    = Mode;
   _ComTimeout = 20;
#ifdef COM_TX_SPACE
   _ComTxSpace = 0;
#endif // COM_TX_SPACE

   return( YES);
} // ComInit

//-----------------------------------------------------------------------------
// Timeout;
//-----------------------------------------------------------------------------

void ComSetTimeout( word ms)
// Nastaveni meziznakoveho timeoutu
{
   _ComTimeout = ms / MB_TIMEOUT_TIMER_PERIOD;
   if( _ComTimeout < 2){
      _ComTimeout = 2;                 // minimalni pocet tiku je 2 (rozsah TIMER_PERIOD0..2*TIMER_PERIOD0)
   }
} // ComSetTimeout

#ifdef COM_TX_SPACE
//-----------------------------------------------------------------------------
// Timeout;
//-----------------------------------------------------------------------------

void ComSetTxSpace( word ms)
// Nastaveni mezery mezi Rx a Tx
{
   _ComTxSpace = ms / MB_TIMEOUT_TIMER_PERIOD;
   if( _ComTxSpace < 2){
      _ComTxSpace = 0;                 // mene nez 2 tiky nema smysl
   }
} // ComSetTxSpace

#endif // COM_TX_SPACE

//-----------------------------------------------------------------------------
// Start Tx
//-----------------------------------------------------------------------------

void ComTxStart( void)
// Zahaji vysilani bufferu
{
   #ifdef ComTxEnable
      ComTxEnable();                   // RS485, povoleni vysilani
   #endif
   ComStatus   = COM_STATUS_TX_RUNNING;
   _ComPointer = 0;                    // ukazatel do bufferu
#ifdef COM_TX_SPACE
   if( _ComTxSpace != 0){
      TI = 0;                          // zpozdeny start
      ComTimeoutCounter = _ComTxSpace; // pocet tiku do startu
   } else {
      TI = 1;                          // okamzite vyvolat Tx interrupt
      ComTimeoutCounter = 0;           // zakazat start v preruseni casovace
   }
   ComEnableInt();
#else
   TI = 1;                             // vyvolat Tx interrupt
   ComEnableInt();
#endif // COM_TX_SPACE
} // ComTxStart

//-----------------------------------------------------------------------------
// Start Rx
//-----------------------------------------------------------------------------

void ComRxStart( void)
// Zahaji prijem do bufferu
{
   ComStatus         = COM_STATUS_RX_RUNNING;   // start prijmu
   DisableInts();
   ComTimeoutCounter = 0;                       // zatim zadny znak
   EnableInts();
   ComDataSize       = 0;                       // ukazatel do bufferu
   ComEnableRx();
   RI = 0;                                      // flush predchozich znaku
   ComEnableInt();
} // ComRxStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void ComStopAll( void)
// Ukonci praci v preruseni (Rx i Tx)
{
   ComDisableInt();                    // zakaz preruseni COM
   // zakaz casovace :
   DisableInts();
   ComTimeoutCounter = 0;
   EnableInts();
   // stav COMu :
   ComStatus         = COM_STATUS_STOPPED;
   ComDataSize       = 0;
} // ComStopAll

//-----------------------------------------------------------------------------
// Prerusovaci rutina
//-----------------------------------------------------------------------------

static void ComIsr( void) interrupt INT_SERIAL
{
byte c;

   if( RI){
      // preruseni od prijimace
      RI = 0;                          // zrus priznak preruseni
      c  = SBUF;                       // prevezmi znak
      // kontrola parity : ----------------------------------------------------
      switch( _ComParity){
         case COM_PARITY_EVEN :
            ACC = c;
            if( RB8 != P){
               ComStatus = COM_STATUS_RX_PARITY;
            }
            break;
         case COM_PARITY_ODD :
            ACC = c;
            if( RB8 == P){
               ComStatus = COM_STATUS_RX_PARITY;
            }
            break;
         case COM_PARITY_MARK :
            if( RB8 != 1){
               ComStatus = COM_STATUS_RX_PARITY;
            }
         case COM_PARITY_7BITS_EVEN :
            ACC = c;
            if( P == 1){
               ComStatus = COM_STATUS_RX_PARITY;
            }
            c &= 0x7F;
            break;
         case COM_PARITY_7BITS_ODD :
            ACC = c;
            if( P == 0){
               ComStatus = COM_STATUS_RX_PARITY;
            }
            c &= 0x7F;
            break;
         case COM_PARITY_7BITS_MARK :
            if( !(c & 0x80)){
               ComStatus = COM_STATUS_RX_PARITY;
            }
            c &= 0x7F;
            break;
      }
      //-----------------------------------------------------------------------
      if( ComDataSize == COM_DATA_SIZE){
         // plny buffer
         ComStatus = COM_STATUS_RX_OVERRUN;
      } else {
         if( (_ComMode & COM_MODE_TRAILER) && (c == COM_TRAILER_CHAR)){
            // cekame na tento ukoncovaci znak
            if( !(_ComMode & COM_MODE_LEADER) || (ComDataSize != 0)){
               ComStatus = COM_STATUS_RX_DONE;      // stop
               ComBuffer[ ComDataSize++] = c;       // uloz znak
            } // else cekame na vedouci znak, pokracuj
         }
      }
      if( ComStatus != COM_STATUS_RX_RUNNING){
         // zastaveni prijmu
         ComStopRx();                            // zastavit prijem, zakazat preruseni
         ComTimeoutCounter = 0;                  // zastavit citani
      } else {
         // pripravit dalsi prijem
         if( _ComMode & COM_MODE_TIMEOUT){
            ComTimeoutCounter = _ComTimeout;     // citani meziznakoveho timeoutu
         }
         if( _ComMode & COM_MODE_LEADER){
            // vyhodnocujeme vedouci znak
            if( c == COM_LEADER_CHAR){
               ComDataSize = 0;                  // ukladat vzdy od zacatku
            }
            if( (c == COM_LEADER_CHAR) || (ComDataSize != 0)){
               ComBuffer[ ComDataSize++] = c;    // uloz znak
            } // else cekame na vedouci znak
         } else {
            // ukladame vsechny znaky
            ComBuffer[ ComDataSize++] = c;       // uloz znak
         }
      }
   }
   //---------------------------------------------------------------------------
   //---------------------------------------------------------------------------
   if( TI){
      // preruseni od vysilace
      TI = 0;
      if( _ComPointer == ComDataSize){
         // vysilani ukonceno
         #ifdef ComTxDisable
            ComTxDisable();            // RS485, zakaz vysilani
         #endif
         // start prijmu :
         ComStatus  = COM_STATUS_RX_RUNNING;   // start prijmu
         ComTimeoutCounter = 0;                // zatim zadny znak
         ComDataSize       = 0;                // ukazatel do bufferu
         ComEnableRx();
         RI = 0;                               // flush predchozich znaku
         return;
      }
      c = ComBuffer[ _ComPointer++];
      // nastaveni parity : ----------------------------------------------------
      switch( _ComParity){
         case COM_PARITY_EVEN :
            ACC = c;
            TB8 = P;
            break;
         case COM_PARITY_ODD :
            ACC = c;
            TB8 = !P;
            break;
         case COM_PARITY_MARK :
            TB8 = 1;
            break;
         case COM_PARITY_7BITS_EVEN :
            c &= 0x7F;                 // shodit paritni bit
            ACC = c;
            if( P){
               c |= 0x80;
            }
            break;
         case COM_PARITY_7BITS_ODD :
            c &= 0x7F;                 // shodit paritni bit
            ACC = c;
            if( !P){
               c |= 0x80;
            }
            break;
         case COM_PARITY_7BITS_MARK :
            c |= 0x80;                 // vzdy nahodit paritni bit
            break;
      }
      //-----------------------------------------------------------------------
      SBUF = c;                        // vyslani
   }
} // ComIsr

#ifdef __COM_DIAG__

//-----------------------------------------------------------------------------
// Diagnostika
//-----------------------------------------------------------------------------

#include "..\inc\conio.h"

void ComDiag( void)
// Print diagnostics
{
   DisplayGotoRC( 0, 0);
/*
   cputs( "Stat : ");
   switch( ComStatus){
      case COM_STATUS_STOPPED :
         // zastaveno - stav po inicializaci
         putchar( '-');
         break;
      case COM_STATUS_RX_RUNNING :
         // ceka se na data
         putchar( 'R');
         break;                        // ceka na Rx data
      case COM_STATUS_RX_PARITY :
         // paket s chybou parity
         putchar( 'P');
         break;
      case COM_STATUS_RX_OVERRUN :
         // paket s chybou prebehu
         putchar( 'O');
         break;
      case COM_STATUS_RX_DONE :
         putchar( 'D');
         break;
      case COM_STATUS_RX_TIMEOUT :
         // RTU dokoncen paket
         putchar( '+');
         break;
      case COM_STATUS_TX_RUNNING :
         putchar( 'T');
         break;                        // ceka na dokonceni Tx
      default :
         putchar( '?');
         break;
   }
   putchar( '\n');
*/
   cputs( "Stat : "); cbyte( ComStatus); putchar( '\n');
   cputs( "Size : "); cword( ComDataSize); putchar( '\n');
   cputs( "Cntr : "); cword( ComTimeoutCounter); putchar( '\n');
   cputs( "Ptr  : "); cword( _ComPointer); putchar( '\n');

   cputs( "ES   : "); ES  ? putchar( '1') : putchar( '0'); putchar( '\n');
   cputs( "REN  : "); REN ? putchar( '1') : putchar( '0'); putchar( '\n');
   cputs( "TI   : "); TI  ? putchar( '1') : putchar( '0'); putchar( '\n');
   cputs( "RI   : "); RI  ? putchar( '1') : putchar( '0'); putchar( '\n');

} // ComDiag


#endif // __COM_DIAG__
