; C:\SOURCE\C51\VEIT\MODULY\XMEM\XMEMAT.A51 generated from: C:\SOURCE\C51\VEIT\MODULY\XMEM\XMEMAT.C51

$NOMOD51

NAME	XMEMAT

TmcSDA	BIT	0E8H.1
TmcTP4	BIT	090H.2
SSADR	DATA	096H
CCF2	BIT	0D8H.2
TmcTP5	BIT	090H.3
CCF3	BIT	0D8H.3
P0	DATA	080H
TmcTP6	BIT	0C0H.1
TmcCSD	BIT	0A0H.7
SSDAT	DATA	095H
CCF4	BIT	0D8H.4
P1	DATA	090H
TmcTP7	BIT	0C0H.2
P2	DATA	0A0H
TmcRX2	BIT	090H.4
P3	DATA	0B0H
AC	BIT	0D0H.6
P4	DATA	0C0H
T0	BIT	0B0H.4
TmcTX2	BIT	0B0H.3
SPCON	DATA	0C3H
P5	DATA	0E8H
T1	BIT	0B0H.5
TmcTX3	BIT	0C0H.4
EA	BIT	0A8H.7
T2	BIT	090H.0
TmcCLKAD	BIT	0A0H.1
TmcSCK	BIT	090H.6
SSCON	DATA	093H
EC	BIT	0A8H.6
TmcSCL	BIT	0A0H.5
CF	BIT	0D8H.7
TmcCSM	BIT	0B0H.7
CH	DATA	0F9H
FE	BIT	098H.7
TmcMAJAK	BIT	0A0H.0
SPSTA	DATA	0C4H
IEN0	DATA	0A8H
IEN1	DATA	0B1H
TmcCLKDI	BIT	0E8H.0
CL	DATA	0E9H
P0_0	BIT	080H.0
CCAP0H	DATA	0FAH
CEX0	BIT	090H.3
P1_0	BIT	090H.0
P0_1	BIT	080H.1
CCAP1H	DATA	0FBH
IPH0	DATA	0B7H
P2_0	BIT	0A0H.0
CEX1	BIT	090H.4
P1_1	BIT	090H.1
P0_2	BIT	080H.2
CCAP2H	DATA	0FCH
IPH1	DATA	0B3H
P3_0	BIT	0B0H.0
P2_1	BIT	0A0H.1
CEX2	BIT	090H.5
P1_2	BIT	090H.2
P0_3	BIT	080H.3
TmcWRD	BIT	0E8H.5
CCAP3H	DATA	0FDH
P4_0	BIT	0C0H.0
P3_1	BIT	0B0H.1
P2_2	BIT	0A0H.2
CEX3	BIT	090H.6
P1_3	BIT	090H.3
P0_4	BIT	080H.4
TmcRXD	BIT	0B0H.0
CCAP4H	DATA	0FEH
CCAP0L	DATA	0EAH
CR	BIT	0D8H.6
P5_0	BIT	0E8H.0
P4_1	BIT	0C0H.1
P3_2	BIT	0B0H.2
P2_3	BIT	0A0H.3
CEX4	BIT	090H.7
P1_4	BIT	090H.4
P0_5	BIT	080H.5
TmcSOM	BIT	0C0H.5
CCAP1L	DATA	0EBH
CCAPM0	DATA	0DAH
IPL0	DATA	0B8H
EXF2	BIT	0C8H.6
P5_1	BIT	0E8H.1
P4_2	BIT	0C0H.2
RD	BIT	0B0H.7
P3_3	BIT	0B0H.3
P2_4	BIT	0A0H.4
P1_5	BIT	090H.5
P0_6	BIT	080H.6
TmcTXD	BIT	0B0H.1
CCAP2L	DATA	0ECH
CCAPM1	DATA	0DBH
IPL1	DATA	0B2H
P5_2	BIT	0E8H.2
P4_3	BIT	0C0H.3
P3_4	BIT	0B0H.4
P2_5	BIT	0A0H.5
P1_6	BIT	090H.6
P0_7	BIT	080H.7
CCAP3L	DATA	0EDH
CCAPM2	DATA	0DCH
ES	BIT	0A8H.4
P5_3	BIT	0E8H.3
P4_4	BIT	0C0H.4
P3_5	BIT	0B0H.5
P2_6	BIT	0A0H.6
P1_7	BIT	090H.7
CCAP4L	DATA	0EEH
CCAPM3	DATA	0DDH
UD	BIT	0D0H.1
P5_4	BIT	0E8H.4
P4_5	BIT	0C0H.5
P3_6	BIT	0B0H.6
P2_7	BIT	0A0H.7
TmcD0	BIT	080H.0
CCAPM4	DATA	0DEH
P5_5	BIT	0E8H.5
P4_6	BIT	0C0H.6
P3_7	BIT	0B0H.7
TmcD1	BIT	080H.1
RI	BIT	098H.0
P5_6	BIT	0E8H.6
P4_7	BIT	0C0H.7
TmcD2	BIT	080H.2
CY	BIT	0D0H.7
P5_7	BIT	0E8H.7
INT0	BIT	0B0H.2
TmcCSPAN	BIT	0C0H.3
TmcD3	BIT	080H.3
TI	BIT	098H.1
INT1	BIT	0B0H.3
TmcD4	BIT	080H.4
TmcD5	BIT	080H.5
TmcD6	BIT	080H.6
CKCON0	DATA	08FH
TmcD7	BIT	080H.7
PT0L	BIT	0B8H.1
CKCON1	DATA	0AFH
TmcPENIN	BIT	0B0H.2
PT1L	BIT	0B8H.3
RCAP2H	DATA	0CBH
PT2L	BIT	0B8H.5
SP	DATA	081H
CMOD	DATA	0D9H
CCON	DATA	0D8H
T2EX	BIT	090H.1
PX0L	BIT	0B8H.0
OV	BIT	0D0H.2
PX1L	BIT	0B8H.2
RCAP2L	DATA	0CAH
FCON	DATA	0D1H
C_T2	BIT	0C8H.1
WR	BIT	0B0H.6
TmcOUTDI	BIT	0A0H.3
KBLS	DATA	09CH
CKRL	DATA	097H
RCLK	BIT	0C8H.5
TCLK	BIT	0C8H.4
PPCL	BIT	0B8H.6
SBUF	DATA	099H
PCON	DATA	087H
SCON	DATA	098H
TMOD	DATA	089H
TCON	DATA	088H
BDRCON	DATA	09BH
TmcBUZTP	BIT	0E8H.7
SSCS	DATA	094H
IE0	BIT	088H.1
IE1	BIT	088H.3
AUXR	DATA	08EH
B	DATA	0F0H
TmcSI	BIT	090H.7
CP_RL2	BIT	0C8H.0
TmcCSEE	BIT	0B0H.4
OSCCON	DATA	086H
ACC	DATA	0E0H
TmcSO	BIT	090H.5
ET0	BIT	0A8H.1
ET1	BIT	0A8H.3
TF0	BIT	088H.5
PI2	DATA	0F8H
ET2	BIT	0A8H.5
TF1	BIT	088H.7
RB8	BIT	098H.2
TH0	DATA	08CH
TF2	BIT	0C8H.7
EX0	BIT	0A8H.0
TH1	DATA	08DH
IT0	BIT	088H.0
EX1	BIT	0A8H.2
TB8	BIT	098H.3
TH2	DATA	0CDH
IT1	BIT	088H.2
P	BIT	0D0H.0
TmcCSDO	BIT	0B0H.5
SM0	BIT	098H.7
TL0	DATA	08AH
SM1	BIT	098H.6
TL1	DATA	08BH
ECI	BIT	090H.2
KBE	DATA	09DH
SM2	BIT	098H.5
TL2	DATA	0CCH
KBF	DATA	09EH
TmcRESD	BIT	0C0H.6
TmcSCKM	BIT	0A0H.6
RS0	BIT	0D0H.3
TR0	BIT	088H.4
RS1	BIT	0D0H.4
TR1	BIT	088H.6
TR2	BIT	0C8H.2
WDTPRG	DATA	0A7H
TmcA0D	BIT	0E8H.6
PI2_0	BIT	0F8H.0
PI2_1	BIT	0F8H.1
DPH	DATA	083H
TmcPISK	BIT	0A0H.4
BRL	DATA	09AH
DPL	DATA	082H
EXEN2	BIT	0C8H.3
REN	BIT	098H.4
T2MOD	DATA	0C9H
T2CON	DATA	0C8H
WDTRST	DATA	0A6H
EECON	DATA	0D2H
SADEN	DATA	0B9H
TmcSVIT	BIT	0E8H.3
RXD	BIT	0B0H.0
TmcDAD	BIT	0A0H.2
PLS	BIT	0B8H.4
SADDR	DATA	0A9H
TXD	BIT	0B0H.1
TmcLOWBAT	BIT	0E8H.2
AUXR1	DATA	0A2H
TmcCS1AD	BIT	0B0H.6
CKSEL	DATA	085H
TmcCS2AD	BIT	0C0H.7
F0	BIT	0D0H.5
TmcCSEPOT	BIT	0E8H.4
PSW	DATA	0D0H
TmcTP1	BIT	090H.0
TmcTP2	BIT	0C0H.0
CCF0	BIT	0D8H.0
TmcTP3	BIT	090H.1
SPDAT	DATA	0C5H
CCF1	BIT	0D8H.1
?PR?XmemInit?XMEMAT  SEGMENT CODE
?PR?_XmemReadByte?XMEMAT                 SEGMENT CODE
?DT?_XmemReadByte?XMEMAT                 SEGMENT DATA OVERLAYABLE
?PR?_XmemBlockReadStart?XMEMAT           SEGMENT CODE
?DT?_XmemBlockReadStart?XMEMAT           SEGMENT DATA OVERLAYABLE
?PR?XmemStop?XMEMAT  SEGMENT CODE
?PR?_XmemPageWriteStart?XMEMAT           SEGMENT CODE
?DT?_XmemPageWriteStart?XMEMAT           SEGMENT DATA OVERLAYABLE
?PR?WrenCmd?XMEMAT   SEGMENT CODE
?PR?RdsrCmd?XMEMAT   SEGMENT CODE
?PR?_WrsrCmd?XMEMAT  SEGMENT CODE
?DT?_WrsrCmd?XMEMAT  SEGMENT DATA OVERLAYABLE
?PR?WaitForReady?XMEMAT                  SEGMENT CODE
?PR?_XmemRawWriteByte?XMEMAT             SEGMENT CODE
?PR?XmemRawReadByte?XMEMAT               SEGMENT CODE
?PR?WrdiCmd?XMEMAT   SEGMENT CODE
?PR?XmemIsPresent?XMEMAT                 SEGMENT CODE
	PUBLIC	XmemIsPresent
	PUBLIC	XmemRawReadByte
	PUBLIC	_XmemRawWriteByte
	PUBLIC	_XmemPageWriteStart
	PUBLIC	XmemStop
	PUBLIC	_XmemBlockReadStart
	PUBLIC	_XmemReadByte
	PUBLIC	XmemInit

	RSEG  ?DT?_XmemReadByte?XMEMAT
?_XmemReadByte?BYTE:
      value?141:   DS   1

	RSEG  ?DT?_XmemBlockReadStart?XMEMAT
?_XmemBlockReadStart?BYTE:
    address?242:   DS   2

	RSEG  ?DT?_XmemPageWriteStart?XMEMAT
?_XmemPageWriteStart?BYTE:
    address?443:   DS   2

	RSEG  ?DT?_WrsrCmd?XMEMAT
?_WrsrCmd?BYTE:
      value?745:   DS   1
; //*****************************************************************************
; //
; //    XmemAT.c     External EEPROM module using AT25256 template
; //    Version 1.0  (c) VymOs
; //
; //*****************************************************************************
; 
; #include "..\inc\Xmem.h"
; 
; //-----------------------------------------------------------------------------
; // Makra pro generovani zdrojoveho textu
; //-----------------------------------------------------------------------------
; 
; // Porty :
; 
; #define XCS    XmemCS
; #define XSCK   XmemSCK
; #define XSO    XmemSO
; #define XSI    XmemSI
; 
; // Inverze signalu:
; #define XSI_H  XMEM_XSI_H
; #define XSCK_H XMEM_XSCK_H
; 
; // Funkce :
; 
; #define XInit           XmemInit
; #define XReadByte       XmemReadByte
; #define XBlockReadStart XmemBlockReadStart
; #define XRawReadByte    XmemRawReadByte
; #define XStop           XmemStop
; #define XWriteByte      XmemWriteByte
; #define XPageWriteStart XmemPageWriteStart
; #define XRawWriteByte   XmemRawWriteByte
; #define XAccuOk         XmemAccuOk
; 
; // Podminena kompilace :
; 
; #ifdef XMEM_READ_BYTE
;    #define X_READ_BYTE     1
; #endif
; #ifdef XMEM_WRITE_BYTE
;    #define X_WRITE_BYTE    1
; #endif
; 
; #include "..\AT25256\AT25256.tpl"      // zdrojovy text
; 
; #ifndef XMEM_USE_INS                   // SW testovani pritomnosti
; 
; //-----------------------------------------------------------------------------
; // Instrukce WRDI
; //-----------------------------------------------------------------------------
; 
; static void WrdiCmd( void)
; // Posle instrukci WRDI
; {
;   Select();                           // select CS
;   XRawWriteByte( EEP_WRDI);
;   Deselect();                         // deselect CS
; } // WrdiCmd
; 
; //-----------------------------------------------------------------------------
; // Zjisteni pripojeni modulu

	RSEG  ?PR?XmemInit?XMEMAT
	USING	0
XmemInit:
			; SOURCE LINE # 63
; //-----------------------------------------------------------------------------
; 
			; SOURCE LINE # 65
; TYesNo XmemIsPresent( void)
			; SOURCE LINE # 66
	SETB 	TmcCSM
; // Vraci YES, je-li pritomen pametovy modul
			; SOURCE LINE # 67
	SETB 	TmcSCKM
	SETB 	TmcSOM
; {
;    SetupCS()                          // vychozi urovne pred CS
			; SOURCE LINE # 69
	LCALL	WaitForReady
;    WrenCmd();                         // Povoleni zapisu - nastavi se WEN ve status registru
			; SOURCE LINE # 70
	LCALL	WrenCmd
;    if( !(RdsrCmd() & EEP_MASK_WEN)){
			; SOURCE LINE # 71
	CLR  	A
	MOV  	R7,A
	LCALL	_WrsrCmd
;       CleanupCS();
			; SOURCE LINE # 72
	SETB 	TmcSCKM
	SETB 	TmcD3
	SETB 	TmcSOM
;       return( NO);                    // modul neni vlozen
			; SOURCE LINE # 73
	RET  	
; END OF XmemInit

;    }
;    WrdiCmd();                         // Zakaz zapisu - shodi se bit WEN
;    if(   RdsrCmd() & EEP_MASK_WEN){
;       CleanupCS();
;       return( NO);                    // modul neni vlozen
;    }
;    CleanupCS();

	RSEG  ?PR?_XmemReadByte?XMEMAT
	USING	0
_XmemReadByte:
;---- Variable 'address?140' assigned to Register 'R6/R7' ----
			; SOURCE LINE # 80
;    return( YES);                      // modul vlozen
; } // XmemIsPresent
			; SOURCE LINE # 82
; 
; #endif // XMEM_USE_INS
; *** sync lost ***
			; SOURCE LINE # 85
	LCALL	_XmemBlockReadStart
; *** sync lost ***
			; SOURCE LINE # 86
	LCALL	XmemRawReadByte
	MOV  	value?141,R7
; *** sync lost ***
			; SOURCE LINE # 87
	LCALL	XmemStop
; *** sync lost ***
			; SOURCE LINE # 88
	MOV  	R7,value?141
; *** sync lost ***
			; SOURCE LINE # 89
?C0002:
	RET  	
; END OF _XmemReadByte

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?_XmemBlockReadStart?XMEMAT
	USING	0
_XmemBlockReadStart:
	MOV  	address?242,R6
	MOV  	address?242+01H,R7
			; SOURCE LINE # 97
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 99
; *** sync lost ***
			; SOURCE LINE # 100
	SETB 	TmcSCKM
	SETB 	TmcSOM
; *** sync lost ***
			; SOURCE LINE # 101
	LCALL	WaitForReady
; *** sync lost ***
			; SOURCE LINE # 102
	CLR  	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 103
	MOV  	R7,#03H
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 104
	MOV  	A,address?242
	MOV  	R7,A
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 105
	MOV  	R7,address?242+01H
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 106
	RET  	
; END OF _XmemBlockReadStart

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?XmemStop?XMEMAT
	USING	0
XmemStop:
			; SOURCE LINE # 112
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 114
; *** sync lost ***
			; SOURCE LINE # 115
	SETB 	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 116
	SETB 	TmcSCKM
	SETB 	TmcD3
	SETB 	TmcSOM
; *** sync lost ***
			; SOURCE LINE # 117
	RET  	
; END OF XmemStop

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?_XmemPageWriteStart?XMEMAT
	USING	0
_XmemPageWriteStart:
	MOV  	address?443,R6
	MOV  	address?443+01H,R7
			; SOURCE LINE # 141
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 143
; *** sync lost ***
			; SOURCE LINE # 144
	JB   	TmcLOWBAT,?C0005
; *** sync lost ***
			; SOURCE LINE # 145
	CLR  	C
	RET  	
; *** sync lost ***
			; SOURCE LINE # 146
?C0005:
; *** sync lost ***
			; SOURCE LINE # 147
	SETB 	TmcSCKM
	SETB 	TmcSOM
; *** sync lost ***
			; SOURCE LINE # 148
	LCALL	WaitForReady
; *** sync lost ***
			; SOURCE LINE # 149
	LCALL	WrenCmd
; *** sync lost ***
			; SOURCE LINE # 150
	CLR  	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 151
	MOV  	R7,#02H
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 152
	MOV  	A,address?443
	MOV  	R7,A
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 153
	MOV  	R7,address?443+01H
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 154
	SETB 	C
; *** sync lost ***
			; SOURCE LINE # 155
?C0006:
	RET  	
; END OF _XmemPageWriteStart

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?WrenCmd?XMEMAT
	USING	0
WrenCmd:
			; SOURCE LINE # 161
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 163
; *** sync lost ***
			; SOURCE LINE # 164
	CLR  	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 165
	MOV  	R7,#06H
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 166
	SETB 	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 167
	RET  	
; END OF WrenCmd

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?RdsrCmd?XMEMAT
	USING	0
RdsrCmd:
			; SOURCE LINE # 174
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 176
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 179
	CLR  	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 180
	MOV  	R7,#05H
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 181
	LCALL	XmemRawReadByte
;---- Variable 'value?644' assigned to Register 'R7' ----
; *** sync lost ***
			; SOURCE LINE # 182
	SETB 	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 183
; *** sync lost ***
			; SOURCE LINE # 184
?C0008:
	RET  	
; END OF RdsrCmd

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?_WrsrCmd?XMEMAT
	USING	0
_WrsrCmd:
	MOV  	value?745,R7
			; SOURCE LINE # 190
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 192
; *** sync lost ***
			; SOURCE LINE # 193
	CLR  	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 194
	MOV  	R7,#01H
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 195
	MOV  	R7,value?745
	LCALL	_XmemRawWriteByte
; *** sync lost ***
			; SOURCE LINE # 196
	SETB 	TmcCSM
; *** sync lost ***
			; SOURCE LINE # 197
	RET  	
; END OF _WrsrCmd

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?WaitForReady?XMEMAT
	USING	0
WaitForReady:
			; SOURCE LINE # 203
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 205
?C0010:
; *** sync lost ***
			; SOURCE LINE # 206
	LCALL	RdsrCmd
	MOV  	A,R7
	JB   	ACC.0,?C0010
; *** sync lost ***
			; SOURCE LINE # 207
?C0012:
	RET  	
; END OF WaitForReady

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?_XmemRawWriteByte?XMEMAT
	USING	0
_XmemRawWriteByte:
;---- Variable 'Value?946' assigned to Register 'R7' ----
			; SOURCE LINE # 213
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 215
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 218
;---- Variable 'i?947' assigned to Register 'R6' ----
	MOV  	R6,#08H
?C0015:
; *** sync lost ***
			; SOURCE LINE # 219
; *** sync lost ***
			; SOURCE LINE # 220
	MOV  	A,R7
	JNB  	ACC.7,?C0016
; *** sync lost ***
			; SOURCE LINE # 221
	CLR  	TmcD3
; *** sync lost ***
			; SOURCE LINE # 222
	SJMP 	?C0017
?C0016:
; *** sync lost ***
			; SOURCE LINE # 223
	SETB 	TmcD3
; *** sync lost ***
			; SOURCE LINE # 224
?C0017:
; *** sync lost ***
			; SOURCE LINE # 225
	CLR  	TmcSCKM
; *** sync lost ***
			; SOURCE LINE # 226
	MOV  	A,R7
	ADD  	A,ACC
	MOV  	R7,A
; *** sync lost ***
			; SOURCE LINE # 227
	SETB 	TmcSCKM
; *** sync lost ***
			; SOURCE LINE # 228
	DJNZ 	R6,?C0015
; *** sync lost ***
			; SOURCE LINE # 229
	RET  	
; END OF _XmemRawWriteByte

; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***
; *** sync lost ***

	RSEG  ?PR?XmemRawReadByte?XMEMAT
	USING	0
XmemRawReadByte:
			; SOURCE LINE # 235
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 237
; *** sync lost ***
			; SOURCE LINE # 238
;---- Variable 'Value?1048' assigned to Register 'R7' ----
	CLR  	A
	MOV  	R7,A
; *** sync lost ***
			; SOURCE LINE # 239
;---- Variable 'i?1049' assigned to Register 'R6' ----
	MOV  	R6,#08H
; *** sync lost ***
; *** sync lost ***
			; SOURCE LINE # 241
	SETB 	TmcSOM
?C0021:
; *** sync lost ***
			; SOURCE LINE # 242
; *** sync lost ***
			; SOURCE LINE # 243
	CLR  	TmcSCKM
; *** sync lost ***
			; SOURCE LINE # 244
	MOV  	A,R7
	ADD  	A,ACC
	MOV  	R7,A
; *** sync lost ***
			; SOURCE LINE # 245
	JNB  	TmcSOM,?C0022
; *** sync lost ***
			; SOURCE LINE # 246
	ORL  	AR7,#01H
; *** sync lost ***
			; SOURCE LINE # 247
?C0022:
; *** sync lost ***
			; SOURCE LINE # 248
	SETB 	TmcSCKM
; *** sync lost ***
			; SOURCE LINE # 249
	DJNZ 	R6,?C0021
; *** sync lost ***
			; SOURCE LINE # 250
; *** sync lost ***
			; SOURCE LINE # 251
?C0023:
	RET  	
; END OF XmemRawReadByte


	RSEG  ?PR?WrdiCmd?XMEMAT
	USING	0
WrdiCmd:
			; SOURCE LINE # 54
			; SOURCE LINE # 56
			; SOURCE LINE # 57
	CLR  	TmcCSM
			; SOURCE LINE # 58
	MOV  	R7,#04H
	LCALL	_XmemRawWriteByte
			; SOURCE LINE # 59
	SETB 	TmcCSM
			; SOURCE LINE # 60
	RET  	
; END OF WrdiCmd


	RSEG  ?PR?XmemIsPresent?XMEMAT
	USING	0
XmemIsPresent:
			; SOURCE LINE # 66
			; SOURCE LINE # 68
			; SOURCE LINE # 69
	SETB 	TmcSCKM
	SETB 	TmcSOM
			; SOURCE LINE # 70
	LCALL	WrenCmd
			; SOURCE LINE # 71
	LCALL	RdsrCmd
	MOV  	A,R7
	JB   	ACC.1,?C0025
			; SOURCE LINE # 72
	SETB 	TmcSCKM
	SETB 	TmcD3
	SETB 	TmcSOM
			; SOURCE LINE # 73
	CLR  	C
	RET  	
			; SOURCE LINE # 74
?C0025:
			; SOURCE LINE # 75
	LCALL	WrdiCmd
			; SOURCE LINE # 76
	LCALL	RdsrCmd
	MOV  	A,R7
	JNB  	ACC.1,?C0027
			; SOURCE LINE # 77
	SETB 	TmcSCKM
	SETB 	TmcD3
	SETB 	TmcSOM
			; SOURCE LINE # 78
	CLR  	C
	RET  	
			; SOURCE LINE # 79
?C0027:
			; SOURCE LINE # 80
	SETB 	TmcSCKM
	SETB 	TmcD3
	SETB 	TmcSOM
			; SOURCE LINE # 81
	SETB 	C
			; SOURCE LINE # 82
?C0026:
	RET  	
; END OF XmemIsPresent

	END
