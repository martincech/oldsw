//*****************************************************************************
//
//    Histogr.c  -  Histogram calculations
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#include "..\inc\Histogr.h"

//-----------------------------------------------------------------------------
// Mazani
//-----------------------------------------------------------------------------

void HistogramClear( THistogram __xdata__ *Histogram)
// Mazani histogramu
{
byte i;

   for( i = 0; i < HISTOGRAM_SLOTS; i++){
      Histogram->Slot[ i] = 0;
   }
} // HistogramClear

//-----------------------------------------------------------------------------
// Pridani hodnoty
//-----------------------------------------------------------------------------

void HistogramAdd( THistogram __xdata__ *Histogram, THistogramValue Value)
// Prida hodnotu do histogramu
{
byte Index;

   Value -= Histogram->Center;

#if (HISTOGRAM_SLOTS % 2 == 0)
   // Sudy pocet
   if( Value >= 0){
      if( Value >= (HISTOGRAM_SLOTS / 2 * Histogram->Step)){
         Histogram->Slot[ HISTOGRAM_SLOTS - 1] += 1;
         return;                       // nad rozsahem
      }
      Index  = Value / Histogram->Step; // vzdalenost od stredu
      Index += HISTOGRAM_SLOTS / 2;
   } else {
      // Value < 0
      Value = -Value;
      if( Value >= (HISTOGRAM_SLOTS / 2 * Histogram->Step)){
         Histogram->Slot[ 0] += 1;
         return;                          // pod rozsahem
      }
      Index  = Value / Histogram->Step;    // vzdalenost od stredu
      Index  = HISTOGRAM_SLOTS / 2 - 1 - Index;
   }
#else
  // Lichy pocet
   if( Value >= 0){
      if (Value >= (HISTOGRAM_SLOTS * Histogram->Step / 2)) {
         Histogram->Slot[HISTOGRAM_SLOTS - 1] += 1;
         return;                       // nad rozsahem
      }
      Index  = (Value + Histogram->Step / 2) / Histogram->Step; // vzdalenost od stredu
      Index += HISTOGRAM_SLOTS / 2;
   } else {
      Value = -Value;
      if (Value >= (HISTOGRAM_SLOTS * Histogram->Step / 2)) {
         Histogram->Slot[0] += 1;
         return;                          // pod rozsahem
      }
      Index  = (Value + Histogram->Step / 2) / Histogram->Step; // vzdalenost od stredu
      Index  = HISTOGRAM_SLOTS / 2 - Index;
   }
#endif

   Histogram->Slot[ Index] += 1;
} // HistogramAdd

//-----------------------------------------------------------------------------
// Normovani histogramu
//-----------------------------------------------------------------------------

#ifdef HISTOGRAM_GETDENOMINATOR

THistogramValue HistogramGetDenominator(THistogram __xdata__ *Histogram, THistogramValue MaxValue) {
  // Vrati cislo, kterym je treba podelit vsechny sloupce, aby se histogram normoval na maximalni hodnotu <MaxValue>
  THistogramValue Max = 0;
  byte j;

  // Zjistim maximalni hodnotu (vysku sloupce) v histogramu
  for (j = 0; j < HISTOGRAM_SLOTS; j++) {
    if (Histogram->Slot[j] > Max) {
      Max = Histogram->Slot[j];
    }
  }//for j

  if (Max % MaxValue > 0) {
    Max /= MaxValue;
    Max++;
  } else {
    Max /= MaxValue;
  }//else
  if (Max == 0) {
    return 1;   // Nulu nevracim (bude se tim delit)
  }
  return Max;
}//HistogramGetDenominator

#endif

//-----------------------------------------------------------------------------
// Vypocet kroku
//-----------------------------------------------------------------------------

#ifdef HISTOGRAM_SET_STEP

void HistogramSetStep(THistogram __xdata__ *Histogram, byte Range) {
  // Vypocte a ulozi do zadaneho histogramu krok podle nastaveneho stredu a rozsahu. Rozsah se zadava v +-%, tj. Range = 10 znamena +-10% okolo stredu.
  // Pokud krok vyjde mensi nez HISTOGRAM_MIN_STEP, nastavi krok na HISTOGRAM_MIN_STEP
  // Pokud minimum histogramu vyjde zaporne, posune stred histogramu tak, aby minimum vychazelo na 0
  Histogram->Step = Histogram->Center * (THistogramValue)(2 * Range) / (THistogramValue)(100) / (THistogramValue)HISTOGRAM_SLOTS;
  if (Histogram->Step < HISTOGRAM_MIN_STEP) {
    // Krok vysel nulovy, nastavim ho na minimalni hodnotu
    Histogram->Step = HISTOGRAM_MIN_STEP;
  }
  if (Histogram->Center < Histogram->Step * (THistogramValue)HISTOGRAM_SLOTS / (THistogramValue)2) {
    // Minimum histogramu by vyslo zaporne - posunu stred tak, aby minimum vyslo na 0
    Histogram->Center = Histogram->Step * (THistogramValue)HISTOGRAM_SLOTS / (THistogramValue)2;
  }
}

#endif  // HISTOGRAM_SET_STEP

//-----------------------------------------------------------------------------
// Test, zda je histogram prazdny
//-----------------------------------------------------------------------------

#ifdef HISTOGRAM_EMPTY

TYesNo HistogramEmpty(THistogram __xdata__ *Histogram) {
  // Pokud je histogram prazdny, vrati YES
  byte j;

  for (j = 0; j < HISTOGRAM_SLOTS; j++) {
    if (Histogram->Slot[j] > 0) {
      return NO;   // Nasel jsem nejaky prvek
    }
  }//for
  return YES;   // Pokud dosel az sem, nenasel jsem zadny prvek
}

#endif  // HISTOGRAM_EMPTY

//-----------------------------------------------------------------------------
// Vypocet hodnoty sloupce histogramu
//-----------------------------------------------------------------------------

#ifdef HISTOGRAM_GET_VALUE

THistogramValue HistogramGetValue(THistogram __xdata__ *Histogram, byte SlotIndex) {
  // Vrati hodnotu sloupce s indexem <SlotIndex>
  return (THistogramValue)(Histogram->Center + ((THistogramValue)SlotIndex - (THistogramValue)(HISTOGRAM_SLOTS / 2)) * Histogram->Step);
}

#endif // HISTOGRAM_GET_VALUE
