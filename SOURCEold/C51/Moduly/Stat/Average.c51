//*****************************************************************************
//
//    Average.c - Klouzavy prumer
//    Version 1.0
//
//*****************************************************************************

#include "..\inc\Average.h"
#include <math.h>

//-----------------------------------------------------------------------------
// Nastaveni delky
//-----------------------------------------------------------------------------

void AverageSetLength(TAverage __xdata__ *Average, TAverageCount NewLength) {
  // Nastavi novou delku klouzaveho prumeru. Melo by nasledovat vynulovani prumeru.
  if (NewLength <= 0) {
    NewLength = 1;
  } else if (NewLength > AVERAGE_CAPACITY) {
    NewLength = AVERAGE_CAPACITY;
  }
  Average->Length = NewLength;
}

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AverageClear(TAverage __xdata__ *Average) {
  // Smaze vsechny prvky
  TAverageCount j;

  for (j = 0; j < AVERAGE_CAPACITY; j++) {
    Average->Values[j] = 0;
  }
  Average->Count = 0;
}

//-----------------------------------------------------------------------------
// Pridani prvku
//-----------------------------------------------------------------------------

void AverageAdd(TAverage __xdata__ *Average, TAverageValue Value) {
  // Prida do prumeru prvek
  TAverageCount j;

  if (Average->Length == 0) {
    return;
  }
  // Posunu vsechny vzorky
  for (j = Average->Length - 1; j > 0; j--) {
    Average->Values[j] = Average->Values[j - 1];
  }
  if (Average->Count < Average->Length) {
    // Pole jeste neni zaplnene, prictu
    Average->Count++;
  }
  // Pridam do pole novy vzorek
  Average->Values[0] = Value;
}

//-----------------------------------------------------------------------------
// Vypocet prumeru
//-----------------------------------------------------------------------------

TAverageValue AverageGetAverage(TAverage __xdata__ *Average) {
  // Vrati prumer klouzaveho prumeru
  TAverageCount j;
  TAverageValueSum Sum;

  if (Average->Count == 0) {
    return 0;
  }
  Sum = 0;
  for (j = 0; j < Average->Length; j++) {
    Sum += (TAverageValueSum)Average->Values[j];
  }
  return Sum / (TAverageValueSum)Average->Count;
}

//-----------------------------------------------------------------------------
// Test zaplneni
//-----------------------------------------------------------------------------

#ifdef AVERAGE_FULL

TYesNo AverageFull(TAverage __xdata__ *Average) {
  // Pokud je prumer zaplnen, vrati YES
  return (Average->Count >= Average->Length);
}

#endif //AVERAGE_FULL

//-----------------------------------------------------------------------------
// Test ustaleni
//-----------------------------------------------------------------------------

#ifdef AVERAGE_STEADY

TYesNo AverageSteady(TAverage __xdata__ *Average, TAverageValue MaxOffset) {
  // Pokud jsou vsechny vzorky vzdalene od prumeru max. +- MaxOffset, vrati YES
  TAverageCount j;
  TAverageValue AverageValue;

  AverageValue = AverageGetAverage(Average);   // Absolutni hodnota prumeru

  // Testuju jednotlive vzorky
  for (j = 0; j < Average->Length; j++) {
    if (labs(Average->Values[j] - AverageValue) > MaxOffset) {
      return NO;  // Tento vzorek je prilis vzdalen
    }
  }//for
  return YES;
}

#endif //AVERAGE_STEADY
