//*****************************************************************************
//
//    Hardware.h  - BAT3 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof


//-----------------------------------------------------------------------------
// Prirazeni portu
//-----------------------------------------------------------------------------

// datova sbernice :

#define BatDATA P0

sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :

sbit BatCSRTC    = P1^0;
sbit BatRDRTC    = P1^1;
sbit BatWRRTC    = P1^2;
sbit BatALERTC   = P1^3;
sbit BatCSEE     = P1^4;               // CS flash
sbit BatSOEE     = P1^5;
sbit BatSCKEE    = P1^6;
sbit BatSIEE     = P1^7;
// SPI procesoru :
#define BatMISO  BatSOEE
#define BatSCK   BatSCKEE
#define BatMOSI  BatSIEE

// Port P2 :

sbit BatLOWBAT   = P2^0;
sbit BatZAPGSM   = P2^1;
sbit BatPISK     = P2^2;
sbit BatK0       = P2^3;
sbit BatK1       = P2^4;
sbit BatRES      = P2^5;               // RES display
sbit BatCS2      = P2^6;               // CS1 display
sbit BatCS1      = P2^7;               // CS2 display

// Port P3 :

sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatCSMOD    = P3^2;               // CS flash modulu
sbit BatDRAD     = P3^3;               // DRDY AD1241
sbit BatCLKAD    = P3^4;               // CLK  AD1241
sbit BatK2       = P3^5;
sbit BatSVIT     = P3^6;               // podsvit
sbit BatDATAAD   = P3^7;               // DOUT AD1241


// Kanal vahy :

#define WEIGHT_CHANNEL   0             // cislo kanalu A/D pro vahu
typedef long TRawValue;                // surova hodnota vazeni

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE_T1  1          // zdvojena baudova rychlost pro timer 1

// Casovac 0 :
#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  BatAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut BatPISK                 // vystup generatoru
#define PCA_IDLE_STATE 1               // Klidova uroven pinu PcaOut

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1               // asynchronni pipani

//-----------------------------------------------------------------------------
// A/D prevodnik ADS1241
//-----------------------------------------------------------------------------

#define AdcDIN         BatDATAAD       // vstup
#define AdcDOUT        BatDATAAD       // vystup
#define AdcSCLK        BatCLKAD        // hodiny
#define AdcDRDY        BatDRAD         // -ready

#define AdcClrCS()                     // chipselect vyber
#define AdcSetCS()                     // deselect

#define ADC_GAIN       ADC_GAIN_2      // zakladni zesileni
#define ADC_RATE       ADC_7_5HZ       // rychlost vzorkovani
#define ADC_POLARITY   ADC_BIPOLAR     // rezim polarity
#define ADC_RANGE      ADC_HALF_RANGE  // plny/polovicni rozsah

#define ADC_INTERRUPT  INT_EXTERNAL1   // rezim na pozadi, vstup preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

// podminena kompilace prefiltru (zakomentovat pri zruseni prefiltru)
#define ADC_PREFILTER  3               // pocet vzorku pro prefiltr

//-----------------------------------------------------------------------------
// Filtrace
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window

// Basic data types :
typedef long   TRawWeight;             // weight data
typedef long   TLongWeight;            // weight sum
typedef byte   TSamplesCount;          // samples counter

//#define FILTER_VISIBLE    1          // show filter data

//-----------------------------------------------------------------------------
// Radic displeje Samsung Ks0108b
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       BatDATA
#define DisplayD0         BatD0
#define DisplayD1         BatD1
#define DisplayD2         BatD2
#define DisplayD3         BatD3
#define DisplayD4         BatD4
#define DisplayD5         BatD5
#define DisplayD6         BatD6
#define DisplayD7         BatD7
#define DisplayRW         BatWRRTC
#define DisplayDI         BatALERTC
#define DisplayE          BatRDRTC
#define DisplayCS1        BatCS1
#define DisplayCS2        BatCS2
#define DisplayRESET      BatRES

// Podminena kompilace

#define DISPLAY_SET_AREA         1
#define DISPLAY_CHAR             1

// Datova sbernice DisplayDATA 1:1
#define KS_BUS     1
// Obecne funkce
#define KS_ON      1

#define __SIMPLE_DISPLAY__

// Fonty - pouziti jednotlivych fontu
#define DISPLAY_LUCIDA6   1

//-----------------------------------------------------------------------------
// Graficky displej Powertip EL12864
//-----------------------------------------------------------------------------

// Rozmery displeje:
#define DISPLAY_WIDTH           128     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define BacklightOn()   BatSVIT = 1
#define BacklightOff()  BatSVIT = 0

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define K0         BatK0
#define K1         BatK1
#define K2         BatK2


// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
//  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_LEFT,                              // sipka doleva
  K_RIGHT,                             // sipka doprava
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_STABLE,                            // prekresleni stabilni hodnoty
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

//#define __DISPLAY_CURSOR__    1      // nastavovani kurzoru
#define __DISPLAY_CLEAR__     1        // mazani
#define __DISPLAY_CLR_EOL__   1        // mazani do konce radku


//-----------------------------------------------------------------------------

//#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------

#endif
