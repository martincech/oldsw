//*****************************************************************************
//
//    X25045.tpl  -  Template for X25045 services
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

// Vychazi z kodu pro X25645, pouze jsou vypusteny nektere konstanty (Status register je trosku jiny) a prenasi se pouze 1 bajt adresy - prenasi se celkem
// 9 bajtu, MSB bit se prenasi primo v instrukci read nebo write. Navic velikost stranky je zde pouze 16 bajtu, oproti 32 bajtum u X25645.

// Pozor, XSO je vystup z pameti, budeme jej cist
// XSI je vstup do pameti, do nej zapisujeme

// Instrukce X25045:
#define EEP_WREN    0x06           // Set Write Enable Latch (Enable Write)
#define EEP_WRDI    0x04           // Reset Write Enable/Reset Flag Bit
#define EEP_RSDR    0x05           // Read Status Register
#define EEP_WRSR    0x01           // Write Status Register
#define EEP_READ    0x03           // Read data - 4. bit se doplni nevyssim bitem adresy
#define EEP_WRITE   0x02           // Write data - 4. bit se doplni nevyssim bitem adresy

// Maska pro instrukce read a write:
#define EEP_MASK_9BIT   0x08       // 4. bit intrukce write a read musim doplnit nejvyssim bitem adresy. Pokud je adresa >0xFF, nasatvim tento bit


// Status register format X25045:
#define EEP_MASK_WIP  0x01         // WIP je v 1 probiha-li zapis
#define EEP_MASK_WEL  0x02         // je v 1 je-li zarizeni Write Enabled
#define EEP_MASK_BL0  0x04         // ochranny bit 0 (1/4 kapacity)
#define EEP_MASK_BL1  0x08         // ochranny bit 1 (1/2 kapacity)


// Lokalni funkce :

static void WrenCmd(void);
// Posle instrukci WREN

static byte RsdrCmd(void);
// Precte a vrati status registr

static void WrsrCmd(byte value);
// Zapise do status registru

static void WipPoll(void);
// Cekani na pripravenost

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void XInit(byte WatchDog) {
  // Nastavi klidove hodnoty na sbernici, inicializuje pamet
  XCS    = 1;                    // deselect
  XSCK   = 0;                    // vychozi postaveni hodin
  XSI    = 1;                    // data out
  XSO    = 1;                    // data in
  // Zapis do stavoveho registru:
  WipPoll();
  WrenCmd();                     // Set write enable latch
//  WrsrCmd(EEP_MASK_WD0);         // Zrusit watchdog a ochranu pameti
  WrsrCmd(WatchDog & 0xF0);         // Zrusit watchdog a ochranu pameti (ochranu pameti udelam vynulovanim dolnich bitu)
} // XInit

#ifdef X_READ_BYTE

//-----------------------------------------------------------------------------
// Cteni z pameti
//-----------------------------------------------------------------------------

byte XReadByte(word address) {
  // Precte byte z EEPROM <address>
  byte value;

  XBlockReadStart(address);      // zacatek bloku
  value = XRawReadByte();        // jeden datovy byte
  XStop();                       // konec cteni
  return (value);
} // XReadByte

#endif

//-----------------------------------------------------------------------------
// Strankove cteni z pameti
//-----------------------------------------------------------------------------

void XBlockReadStart(word address) {
  // Zahaji blokove cteni z EEPROM <address>
  WipPoll();                     // pripravenost
  XCS     = 0;                   // select
  // Nejvyssi bit adresy (je jich celkem 9) se prenasi v instrukci
  if (address<=0xFF) {
     // Adresa vyjde do 1 bajtu, neni treba prenaset 9. bit
     XRawWriteByte(EEP_READ);   // READ instruction bez 9. bitu adresy (nechavam jej nulovy)
     XRawWriteByte(address);    // Adresa
   } else {
     // Adresa je vetsi nez 1 bajt => 0. bit horniho bajtu davam do adresy
     XRawWriteByte(EEP_READ | EEP_MASK_9BIT);  // READ instruction vcetne 9. bitu adresy
     XRawWriteByte(address & 0x0FF);   // Zbytek adresy
   }//else
} // XBlockReadStart

//-----------------------------------------------------------------------------
// Ukonceni
//-----------------------------------------------------------------------------

void XStop( void)
// Ukonci operaci
{
   XSCK  = 0;                     // Predchozi operace nechava v 1
   _nop_();                       // tcsh > 250ns
   XCS   = 1;                     // deselect
 } // XStop

#ifdef X_WRITE_BYTE

//-----------------------------------------------------------------------------
// Zapis do pameti
//-----------------------------------------------------------------------------

TYesNo XWriteByte( word address, byte value)
// Zapise byte <value> na <address> v EEPROM
{
   if( !XPageWriteStart( address)){
      return( NO);
   }
   XRawWriteByte( value);         // data
   XStop();                       // provedeni zapisu
   return( YES);                  // necekame na dokonceni zapisu
} // XWriteByte

#endif

//-----------------------------------------------------------------------------
// Zapis Stranky do pameti
//-----------------------------------------------------------------------------

TYesNo XPageWriteStart(word address) {
  // Zahaji zapis stranky od <address> v EEPROM
  if (!XAccuOk()) return (NO);
  WipPoll();
  WrenCmd();                     // Set write enable latch
  XCS=0;                         // select
  // Nejvyssi bit adresy (je jich celkem 9) se prenasi v instrukci
  if (address<=0xFF) {
    // Adresa vyjde do 1 bajtu, neni treba prenaset 9. bit
    XRawWriteByte(EEP_WRITE);   // WRITE instruction bez 9. bitu adresy (nechavam jej nulovy)
    XRawWriteByte(address);     // Adresa
  } else {
    // Adresa je vetsi nez 1 bajt => 0. bit horniho bajtu davam do adresy
    XRawWriteByte(EEP_WRITE | EEP_MASK_9BIT);  // WRITE instruction vcetne 9. bitu adresy
    XRawWriteByte(address & 0x0FF);   // Zbytek adresy
  }//else
  return (YES);
} // XPageWriteStart

//-----------------------------------------------------------------------------
// Instrukce WREN
//-----------------------------------------------------------------------------

static void WrenCmd( void)
// Posle instrukci WREN
{
   XCS   = 0;                     // select
   XRawWriteByte( EEP_WREN);
   XStop();
} // WrenCmd

//-----------------------------------------------------------------------------
// Instrukce RDSR
//-----------------------------------------------------------------------------

static byte RsdrCmd( void)
// Precte a vrati status registr
{
byte value;

   XCS  = 0;                      // select
   XRawWriteByte( EEP_RSDR);
   value  = XRawReadByte();
   XStop();
   return( value);
}  // RsdrCmd

//-----------------------------------------------------------------------------
// Instrukce WRSR
//-----------------------------------------------------------------------------

static void WrsrCmd( byte value)
// Zapise do status registru
{
   XCS  = 0;                      // select
   XRawWriteByte( EEP_WRSR);
   XRawWriteByte( value);
   XStop();
}  // WrsrCmd

//-----------------------------------------------------------------------------
// Cekani na dokonceni zapisu
//-----------------------------------------------------------------------------

static void WipPoll( void)
// Cekani na pripravenost
{
  while( RsdrCmd() & EEP_MASK_WIP);
} // WipPoll

//-----------------------------------------------------------------------------
// Zapis bytu
//-----------------------------------------------------------------------------

void XRawWriteByte( byte value)
// Zapise byte, po zapise zustane SCK = 1
{
byte i;

   for( i = 8; i > 0; i--){
      XSCK  = 0;
      XSI   = value & 0x80;
      XSCK  = 1;                  // strobovani nabeznou hranou
      value <<= 1;
   }
} // XRawWriteByte

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte XRawReadByte( void)
// Precte a vrati byte, po cteni zustane SCK = 1
{
byte value;
byte i;

   XSO  = 1;                      // vyrazeni vystupniho budice
   value  = 0;
   for( i = 8; i > 0; i--){
      XSCK  = 0;                  // strobovaci hrana
      value <<= 1;                // prodleva
      XSCK  = 1;                  // neaktivni hrana
      if( XSO){                   // teprve ted cteme
         value++;
      }
   }
   return( value);
} // XRawReadByte


//-----------------------------------------------------------------------------
// WatchDog
//-----------------------------------------------------------------------------
void EepWatchDog() {
  XCS=0; XCS=1;
}

