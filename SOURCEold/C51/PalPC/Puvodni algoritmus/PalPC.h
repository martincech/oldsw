//*****************************************************************************
//
//    PalPC.h - common data definitions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __PalPC_H__
   #define __PalPC_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif



//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE    0x0100       // verze SW - POZOR, musi odpovidat promenne STR_CISLO_VERZE v main.c51



// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Pro promennou ModEditace - co se edituje
enum {
  EDITACE_NORMAL,
  EDITACE_PODSVIT
};

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_VSE 0xFF               // Zobrazi se vse (jsou nastaveny vsechny bity)
#define ZOBRAZIT_MRIZKA 0x01            // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_G_SVISLE 0x02          // Zobrazi se zrychleni Gmetru svisle
#define ZOBRAZIT_G_VODOROVNE 0x04       // Zobrazi se zrychleni Gmetru vodorovne
#define ZOBRAZIT_OTACKY 0x08            // Zobrazi se otacky

// Mod cinnosti
typedef enum {
  REZIM_ZOBRAZENI_G,                    // Jednoduche zobrazeni hodnot G+RPM
  REZIM_2D_G_METR,                      // 2D Gmetr (tecka na displeji)
  REZIM_PRUBEH_G,                       // Mereni prubehu zrychleni
  REZIM_PRUBEH_G_KONEC                  // Mereni prubehu zrychleni
} TRezim;
#define ZAKLADNI_REZIM REZIM_2D_G_METR  // Vychozi rezim

// Co mam vysilat po seriove lince
// Vsechny prikazy musi byt mezi POSLAT_NIC a POSLAT_POSLEDNI
enum {
  POSLAT_NIC,              // Seriova linka je v klidu
  POSLAT_PRUBEHY,
  POSLAT_VERZI,
  POSLAT_POSLEDNI          // Index posledniho, uz neplatneho
};

//-----------------------------------------------------------------------------
// Konfigurace
//-----------------------------------------------------------------------------

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                           // VERZE

  unsigned int MaxOtackyPrubehu;        // Maximalni otacky u mereni prubehu akcelerace

  byte KontrolniSuma;                   // Kontrolni soucet
} TKonfigurace;

extern TKonfigurace __xdata__ Konfigurace; // Buffer konfigurace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast
#define CFG_ALL                       0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERZE                     0x00000001L
#define CFG_MAXOTACKYPRUBEHU          0x00000002L





//-----------------------------------------------------------------------------
// g-metr
//-----------------------------------------------------------------------------

#define POCET_G_METRU 2

// Indexy jednotlivych g-metru/RPM v poli
#define SVISLE 0             // Vodorovne zrychleni (zatacky)
#define VODOROVNE 1          // Svisle zrychleni (plyn/brzda)
#define OTACKY 2             // Otacky motoru

typedef unsigned int TStrida;           // Strida v setinach procenta, maximalne tedy 10000 odpovida 100.00

// Kalibrace 1 Gmetru
typedef struct {
  TStrida KalibraceNuly;                // Kalibrace 0 g
  TStrida KalibraceRozsahu;             // Kalibrace 1 g
} TKalGMetru;

typedef TKalGMetru TKalibraceGMetru[POCET_G_METRU];
extern TKalibraceGMetru __xdata__ KalibraceGMetru;

// Mereni 1 casoveho useku v preruseni
typedef struct {
  unsigned int Capture;                   // Aktualni hodnota Capture registru
  unsigned int PosledniCapture;  // Predchozi hodnota Capture registru
  unsigned char PocetPreteceni;  // Pocet preteceni Capture registru
} TDoba;

// 1 mereni 1 periody v preruseni
typedef struct {
  TDoba DobaL;                   // Doba trvani signalu L
  TDoba DobaH;                   // Doba trvani signalu H
  unsigned char NovaPerioda;     // Flag, ze se nacetla nejaka nova perioda
} TPeriodaPreruseni;

// 1 mereni 1 periody mimo preruseni
typedef struct {
  TDoba DobaL;                   // Doba trvani signalu L
  TDoba DobaH;                   // Doba trvani signalu H
} TPerioda;

// 1 mereni 1 Gmetru
#define MAX_POCET_PERIOD 20000   // Osetreni sumu (v klidu je to cca 7000, 1g=cca 9500)
typedef struct {
  // Promene pro vypocet jedne periody (v L nebo H)
  TPeriodaPreruseni PeriodaPreruseni;              // Aktualni namerena perioda - na konci mereni v preruseni sem prekopiruju i DobaH, abych mel cas na vypocet
  TPerioda Perioda;              // Aktualni namerena perioda - na konci mereni v preruseni sem prekopiruju i DobaH, abych mel cas na vypocet

  // Promenne pro vypocet stridy ze dvou period obdelnika
  unsigned long PocetPeriodH;    // Pocet period ve stavu H pro mereni stridy
  unsigned long PocetPeriodL;    // Pocet period ve stavu L pro mereni stridy

  // Promenne pro vypocet hodnoty G z periody
  int Hodnota;                   // Aktualni zrychleni (zatacky)
  unsigned char NovaHodnota;     // Flag, ze se vypocetla nova hodnota
} TG;

// Jedno mereni pro oba Gmetry
typedef TG TGs[POCET_G_METRU];

// Struktura s hodnotami G v minulem kroku
typedef struct {
  int Vodorovne;
  int Svisle;
} THistorieG;

// 1 mereni otacek
typedef struct {
  // Promene pro vypocet jedne periody
  unsigned int Capture;          // Aktualni hodnota Capture registru
  unsigned int PosledniCapture;  // Predchozi hodnota Capture registru
  unsigned char PocetPreteceni;  // Pocet preteceni Capture registru
  unsigned char NovaPerioda;     // Flag, ze se nacetla nejaka nova perioda

  // Promenne pro vypocet hodnoty otacek z periody
  int Hodnota;                   // Aktualni zrychleni (zatacky)
  unsigned char NovaHodnota;     // Flag, ze se vypocetla nova hodnota
} TOtacky;

#define PRUMEROVANI_ZOBRAZENI_G 100     // Pocet vzorku do prumeru G pri zobrazovani
//#define PRUMEROVANI_2D_G_GMETR 40       // Pocet vzorku do prumeru G pri zobrazeni Gmetru
#define PRUMEROVANI_2D_G_GMETR 1       // Pocet vzorku do prumeru G pri zobrazeni Gmetru
#define PRUMEROVANI_OTACKY 10           // Pocet vzorku do prumeru otacek

// Prumerovani namerenych velicin
typedef struct {
  long Suma;                 // Nacitana suma pro prumerovani
  int Pocet;                 // Pocet vzorku v sume
  int Hodnota;               // Aktualni zrychleni (zatacky)
  unsigned char Prumerovani; // Kolik prvku do prmeru se ma nasumovat
  unsigned char NovaHodnota; // Flag, ze se vypocetla nova hodnota
} TPrum;

// Do teto struktury prumeruju podle potreby - prumeruju 2 G metry + 1 mereni otacek
typedef TPrum TPrumer[POCET_G_METRU+1];

// Minimum a maximum namerenych velicin
typedef struct {
  int Min;                  // Minimum veliciny
  int Max;                  // Maximum veliciny
} TMM;

typedef TMM TMinMax[POCET_G_METRU];




//-----------------------------------------------------------------------------
// Mereni prubehu zrychleni
//-----------------------------------------------------------------------------

#define MIN_OTACKY 500                  // Minimalni otacky v 1/min
#define MAX_OTACKY 10000                // Maximalni otacky v 1/min
#define ROZSAH_OTACEK (MAX_OTACKY-MIN_OTACKY)  // Rozsah otacek, ve kterem se meri
#define OTACKY_KROK 250                  // Po jakem kroku se meri otacky, musi tim byt delitelne MIN_OTACKY a MAX_OTACKY
#define OTACKY_POCET_KROKU (ROZSAH_OTACEK/OTACKY_KROK)  // 9500/50=190
#define OTACKY_MAX_POCET_V_SUME 1000    // Maximalni pocet prvku v sume, aby neco nepreteklo (pribude 100 prvku za sekundu)
#define MAX_POCET_PRUBEHU 5             // Maximalni pocet prubehu, ktere lze ulozit do EEPROM

// Struktura pro sumovani behem mereni
typedef struct {
  long Suma;                            // Sem sumuju behem mereni
  unsigned int Pocet;                   // Pocet prvku v sume
  unsigned int Otacky;                  // Otacky, kterych se suma tyka. Otacky mohou dale jen rust.
} TPrubehSuma;

// Jeden prubeh zrychleni, otacky odpovidaji poloze v poli
typedef int16 TPrubeh[OTACKY_POCET_KROKU];      // 190*2=380 bajtu
typedef TPrubeh TPrubehy[MAX_POCET_PRUBEHU];  // Prubehy v EEPROM





//-----------------------------------------------------------------------------
// Struktura cele interni EEPROM
//-----------------------------------------------------------------------------
#define IEP_SPARE (IEP_SIZE - sizeof(TKonfigurace) - sizeof(TKalibraceGMetru) - sizeof(TPrubehy))  // Velikost mezery na konci interni EEPROM

typedef struct {
  TKalibraceGMetru Kalibrace;           // Kalibrace vseho - tu drzim zvlast. Schvalne ji mam hned na zacatku, aby se pri zmene delky cehokoliv neporusila
  TKonfigurace Konfigurace;             // Konfigurace
  TPrubehy Prubehy;                     // Prubehy zrychleni

  unsigned char Volne[IEP_SPARE];
} TIntEEPROM;



//-----------------------------------------------------------------------------
#endif
