//*****************************************************************************
//
//    Hardware.h  - hardware descriptions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#ifdef __C51__
// jen pro Keil
#include "..\inc\89C51RD2.h"
#endif
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE


//-----------------------------------------------------------------------------
// Hardwarove pripojeni displeje
//-----------------------------------------------------------------------------

sbit DisplayDI =    P3^2;
sbit DisplayRW =    P3^3;
sbit DisplayE =     P3^4;
sbit DisplayCS1 =   P3^5;
sbit DisplayCS2 =   P3^6;
sbit DisplayRESET = P3^7;


sbit DisplayD0 =    P1^0;
sbit DisplayD1 =    P1^1;
sbit DisplayD2 =    P1^2;
sbit DisplayD3 =    P2^4;
sbit DisplayD4 =    P2^5;
sbit DisplayD5 =    P2^6;
sbit DisplayD6 =    P1^6;
sbit DisplayD7 =    P1^7;



// Pripojeni vstupu pro mereni kmitoctu/stridy
sbit VSTUP_G1=P1^3;
sbit VSTUP_G2=P1^4;

// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ idata




//-----------------------------------------------------------------------------
// Hardwarove pripojeni klavesnice
//-----------------------------------------------------------------------------
sbit KbdUp = P2^1;
sbit KbdRight = P2^0;
sbit KbdEnter = P0^1;
sbit KbdEsc = P0^0;



//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 11.0592 MHz
//#define FXTAL 11059200L
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define T2_X2      1            // timer 2 v rezimu X2
#define SI_X2      1            // COM v rezimu X2
#define PCA_X2     2            // PCA v rezimu X2
#define WD_X2      1            // WatchDog v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost - nechci

// Casovac 0 :
#define TIMER0_PERIOD 20        // perioda casovace 0 v ms
// Casovac 1 - pro podsvit
#define TIMER1_PERIOD 1         // perioda casovace 1 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM


//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       1      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

#define IEP_READ_BYTE     1        // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1        // zapis jednoho bytu











// Casovani - musi byt kvuli klavesnici
void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) Delay( ms)             // zpozdeni 1ms z modulu Main





//-----------------------------------------------------------------------------
#include "PalPCHW.h"         // projektove nezavisle definice - dal jsem si to do adresare projektu, namisto do spol. adresare inc, kde je to neprehledne
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
#endif
