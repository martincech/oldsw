//*****************************************************************************
//
//    PalPCCfg.h - Logger configuration load/save
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __PalPCCfg_H__
   #define __PalPCCfg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

bit PalPCCfgLoad();
// Nacte konfiguraci z EEPROM
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)

void PalPCCalLoad();
// Nacte kalibraci z EEPROM
// Nekontroluje zadne checksumy a nenastavuje default hodnoty

bit PalPCCfgSave( dword mask);
// Ulozi polozky definovane maskou do EEPROM
// Maska se ORuje z konstant ACFG_... viz Auto.h
// Vraci NO, nepovedl-li se zapis

bit PalPCCalSave();
// Ulozi celou kalibraci do EEPROM
// Vraci NO, nepovedl-li se zapis

#endif
