//*****************************************************************************
//
//    PalPCHW.h  - Logger independent hardware descriptions
//    Verze 1.0
//
//*****************************************************************************

#ifndef __PalPCHW_H__
   #define __PalPCHW_H__





//-----------------------------------------------------------------------------
// Displej - podminena kompilace
//-----------------------------------------------------------------------------

// Obecne funkce
#define USE_DISPLEJ_ROZNOUT      1

// Cary atd.
#define USE_VYMAZ_OBLAST_RYCHLE  1
//#define USE_VYMAZ_OBLAST         1
#define USE_OBDELNIK_RYCHLE      1
//#define USE_VODOROVNA_CARA       1
#define USE_VODOROVNA_TECKOVANA_CARA 1
#define USE_SVISLA_CARA_RYCHLE   1
#define USE_SVISLA_TECKOVANA_CARA_RYCHLE 1
//#define USE_SVISLA_CARA          1
//#define USE_KRESLI_BOD           1
//#define USE_CARA                 1

// Fonty - pouziti jednotlivych fontu
#define USE_TAHOMA8    1
#define USE_LUCIDA6    1
//#define USE_MYRIAD32   1
//#define USE_MYRIAD40   1

// Vety - pouziti jednotlivych zarovnani u vet
#define USE_VETA         1
#define USE_VETA_NASTRED 1
//#define USE_VETA_NAPRAVO 1
//#define USE_VETA_NALEVO  1

// Zobrazeni cisla
#define USE_ROZLOZENI_CISLA 1
#define USE_ZOBRAZENI_CISLA 1
//#define USE_ZOBRAZENI_ZNAKU_VE_FORMATU 1

// Symbol
#define USE_SYMBOL 1

// Editace
//#define ALFANUMERICKA_KLAVESNICE 1   // Pokud je definovano, pouziva se numericka klavesnice. Jinak pouziva k editaci sipky
#define USE_EDITACE 1
//#define USE_EDITACE_TEXTU 1

// Dialog
#define USE_DIALOG 1

// Menu
#define USE_MENU 1
#define MENU_MAX_POCET_POLOZEK 5        // maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
//#define ObsluzModEditace()                                                                                                                              \
#define DisplayEditExecute(Znaky) PipniKlavesnice()


//-----------------------------------------------------------------------------
// Displej - parametry
//-----------------------------------------------------------------------------

#define SIRKA_DISPLEJE 128
#define VYSKA_DISPLEJE 64




//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

// definice klaves :
enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // Klavesy, v zavislosti na konstrukci klavesnice :
  K_ESC = _K_FIRSTUSER,
  K_UP,
  K_ENTER,
  K_RIGHT,
  K_DOWN=K_RIGHT,
  K_NULA,                     // Nepouziva se, jen kvuli fci Editace()

  // Udalosti
  _K_EVENTS    = 0x60,        // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,   // prekresleni namerenych hodnot
  K_BLINK_OFF,                // periodicke blikani - zhasni
  K_BLINK_ON,                 // periodicke blikani - rozsvit
  K_TIMEOUT,                  // vyprsel cas necinnosti
  K_SERIAL,                   // Mam obslouzit seriovy port

  // systemove klavesy :
  K_REPEAT       = 0x80,   // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,   // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF    // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (400/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch




//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

#define RS232_BAUD      9600          // Rychlost linky v baudech
#define RS232_USE_PARITY    1         // Provoz s paritou nebo bez

#define RS232_PARITY_EVEN   1         // Suda parita
//#define RS232_PARITY_ODD   1        // Licha parita

//#define RS232_CASOVAC_1    1        // K casovani linky se vyuzije casovac 1
#define RS232_CASOVAC_2    1          // K casovani linky se vyuzije casovac 2 (pouzitelne jen u C51RD2, ne u maleho Atmela)

//#define RS232_GET           1         // Zda se pouziva cteni ze seriove linky
#define RS232_SEND          1         // Zda se pouziva zapis na seriovou linku






//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __PalPC_H__
  #include "PalPC.h"                   // Dal jsem si to do adresace projektu, ne do spolecneho inc, kde je to neprehledne
#endif
//-----------------------------------------------------------------------------

#endif
