//*****************************************************************************
//
//    PalAin.c - Analogove vstupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __PalAin_H__
   #define __PalAin_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#define AIN_COUNT 16                    // Celkovy pocet analogovych vstupu


void PalAinInit();
  // Inicializuje AD prevodniky

void PalAinReadInputs();
  // Precte analogove vstupy a ulozi je do PalData

void PalAinConvertValues();
  // Zkonvertuje prevody vsech analogovych vstupu na hodnoty podle kalibrace a ulozi je do PalData

void PalAinCheckLimits();
  // Zkontroluje hodnoty vsech vstupu na meze a nastavi alarmy.

#endif
