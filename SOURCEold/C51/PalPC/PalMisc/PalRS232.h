//*****************************************************************************
//
//    PalRS232.c - prace se seriovym portem
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __PalRS232_H__
   #define __PalRS232_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

void PalRS232CheckCommand();
  // Provedu prikaz, ktery mne poslalo PC po seriovem portu. Prikaz je ulozen v TypSerioveKomunikace.

void PalRS232SendOnline();
  // Pokud jsou nejaka nova data, poslu je online

#endif
