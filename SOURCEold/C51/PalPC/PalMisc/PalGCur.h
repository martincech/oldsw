//*****************************************************************************
//
//    PalGCur.c - Mereni prubehu zrychleni
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __PalGCur_H__
   #define __PalGCur_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

void PalGCurPrubehInit();
  // Inicializace mereni prubehu zrychleni

void PalGCurPrubehZprumerujBody();
  // Vypocte prumer z aktualne merenych otacek

void PalGCurKontrolaMereniPrubehu();
  // Ulozi novou hodnotu zrychleni do prubehu

#endif
