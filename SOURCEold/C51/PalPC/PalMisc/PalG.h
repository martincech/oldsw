//*****************************************************************************
//
//    PalG.c - Auto digital input/output module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __PalG_H__
   #define __PalG_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


void PalGInitGMetr(unsigned char Cislo);
  // Vynuluju stav gmetru

void PalGInitPrumer(unsigned char Cislo);
  // Vynuluju sumu u daneho gmetru

void PalGInitMinMax(unsigned char Cislo);
  // Vynuluju minimum a maximum u daneho gmetru

void PalGKontrolaGMetru();
  // Zkontroluje oba Gmetry a pokud je k dispozici nova hodnota u nektereho z nich (nebo obou), nastavi promennou Zobrazit

void PalGKontrolaRychlosti();
  // Zkontroluje rychlost a pokud je k dispozici nova hodnota, nastavi promennou Zobrazit

void PalGOtackyInit();
  // Inicializuju mereni otacek

void PalGFiltraceOtacekInit();
  // Inicializuju filtraci spicek otacek

void PalGKontrolaOtacek();
  // Zkontroluje otackomer a pokud je k dispozici nova hodnota, nastavi promennou Zobrazit

void PalGNastavPrumerovani();
  // Nastavi prumerovani vsech velicin podle zvoleneho rezimu

void PalGNulujGmetr(unsigned char Cislo);
  // Vynuluje dany gmetr

#endif
