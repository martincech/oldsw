//*****************************************************************************
//
//    PalDisp.c - Mereni prubehu zrychleni
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __PalDisp_H__
   #define __PalDisp_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


void PalDispPrubehInit();
  // Inicializace mereni prubehu zrychleni

void PalDispPrubehZprumerujBody();
  // Vypocte prumer z aktualne merenych otacek

void PalDispKontrolaMereniPrubehu();
  // Ulozi novou hodnotu zrychleni do prubehu

void PalDispNastavRezimMereniPrubehu();
  // Nastavi rezim mereni prubehu

void PalDispInit2DGmetr();
  // Inicializuje historii pro vykresleni 2D gmetru

void PalDispNastavRezim(TRezim NovyRezim);
  // Nastavi zadany rezim a provede vsechny potrebne inicializace atd.

void PalDispZobrazitHodnotyG();
  // Zobrazi rezim hodnot G

void PalDispZobrazitGMetr();
  // Zobrazi rezim 2D gmetr

void PalDispZobrazitPrubehG();
  // Zobrazi rezim mereni prubehu G

void PalDispZobrazitPrubehGKonec();
  // Ukoncim mereni prubehu zrychleni - zeptam se na ulozeni atd.

void PalDispZobrazitDiagnostiku();
  // Zobrazi rezim diagnostiky

void PalDispMenu();
  // Zobrazi menu


#endif
