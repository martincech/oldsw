//*****************************************************************************
//
//    PalDio.c - Auto digital input/output module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __PalDio_H__
   #define __PalDio_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


void PalDioInit();
// Inicializuje Din/Dout

void PalDioSetBusIdle();
// Nastavi klid na sbernici

void PalDioReadInputs();
// Precte digitalni vstupy, volat podle potreby

TYesNo PalDioBatteryOk();
// Precte stav baterie. Vraci YES je-li napeti v norme.
// Lze volat kdykoliv

void PalDioSetOutput(byte Cislo, TYesNo Hodnota);
  // Nastavi zadany digitalni vystup na zadanou hodnotu



#endif
