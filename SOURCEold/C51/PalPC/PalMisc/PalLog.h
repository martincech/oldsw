//*****************************************************************************
//
//    PalLog.c - Ukladani
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __PalLog_H__
   #define __PalLog_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


bit PalLogUlozPrubeh(unsigned char CisloPrubehu);
  // Ulozi do EEPROM namereny prubeh pod cislem CisloPrubehu


#endif
