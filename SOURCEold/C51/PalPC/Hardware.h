//*****************************************************************************
//
//    Hardware.h  - hardware descriptions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#ifdef __C51__
// jen pro Keil
#include "..\inc\89C51ED2.h"
#endif
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE






//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define PalAccuOk() PalDioBatteryOk()

// Sbernice
sbit PalD0 = P2^5;
sbit PalD1 = P2^6;
sbit PalD2 = P2^7;
sbit PalD3 = P0^7;
sbit PalD4 = P0^6;
sbit PalD5 = P0^5;
sbit PalD6 = P0^4;
sbit PalD7 = P0^3;

// Sdilene porty
#define PalDI P2^2
#define PalRW P2^3
#define PalE  P2^4

// Pripojeni displeje
sbit DisplayDI =    PalDI;
sbit DisplayRW =    PalRW;
sbit DisplayE =     PalE;
sbit DisplayCS1 =   P0^2;
sbit DisplayCS2 =   P0^1;
sbit DisplayRESET = P0^0;
#define DisplayD0   PalD0
#define DisplayD1   PalD1
#define DisplayD2   PalD2
#define DisplayD3   PalD3
#define DisplayD4   PalD4
#define DisplayD5   PalD5
#define DisplayD6   PalD6
#define DisplayD7   PalD7

// Pripojeni Gmetru
sbit VSTUP_G1=P1^3;
sbit VSTUP_G2=P1^4;

// Pripojeni klavesnice
sbit KbdK0 = P1^0;
sbit KbdK1 = P1^1;
sbit KbdK2 = P1^2;

// Digitalni vstupy
#define DIN_COUNT 7                     // Pocet digitalnich vstupu
sbit DinCS = P2^0;
#define Din0 PalD7
#define Din1 PalD6
#define Din2 PalD5
#define Din3 PalD4
#define Din4 PalD3
#define Din5 PalD2
#define Din6 PalD1

#define DinBatteryLow PalD0             // Lowbat

// Frekvencni vstupy
sbit Fin0=P1^5;
sbit Fin1=P1^6;
sbit Fin2=P1^7;

// Digitalni vystupy
#define DOUT_COUNT 7                     // Pocet digitalnich vystupu
sbit DoutCS = P3^4;
#define Dout0 PalD0
#define Dout1 PalD1
#define Dout2 PalD2
#define Dout3 PalD3
#define Dout4 PalD4
#define Dout5 PalD5
#define Dout6 PalD6

// Analogove vstupy
#define AIN_COUNT 16                    // Pocet analogovych vstupu
#define AIN_REFERENCE 500               // Referencni napeti AD prevodniku v setinach voltu

// I2C
sbit PalSCL=P3^7;

// Reproduktor
#define REPRO   PalD7

// SPI
#define PalSO  DisplayDI
#define PalSCK DisplayRW
#define PalSI  DisplayE

// Vnitrni Flash
sbit PalFlashCS = P2^1;

/*// Vnitrni EEPROM
sbit PalEepCS = P2^1;
#define PalEepSO DisplayDI
#define PalEepSCK DisplayRW
#define PalEepSI DisplayE

// Vnejsi EEPROM (modul)
sbit PalXmemCS = P3^3;
sbit PalXmemSO = P3^2;
#define PalXmemSCK DisplayE
#define PalXmemSI DisplayRW*/

// AD prevodniky
sbit AinCS1 = P3^6;
sbit AinCS2 = P3^5;


// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ idata







//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 11.0592 MHz
//#define FXTAL 11059200L
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define T2_X2      2            // timer 2 v rezimu X2 - v X2 modu zvlada RS232 rychlost 38400 baudu/s
#define SI_X2      1            // COM v rezimu X2
#define PCA_X2     2            // PCA v rezimu X2
#define WD_X2      1            // WatchDog v rezimu X2
#define SPI_X2     1            // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost - nechci

// Casovac 0 :
#define TIMER0_PERIOD 20        // perioda casovace 0 v ms
// Casovac 1 - pro podsvit
#define TIMER1_PERIOD 1         // perioda casovace 1 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM


//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       PalAccuOk()      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

#define IEP_READ_BYTE     1        // cteni jednoho bytu
#define IEP_WRITE_BYTE    1        // zapis jednoho bytu











// Casovani - musi byt kvuli klavesnici
void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) SysDelay( ms)             // zpozdeni 1ms z modulu Main





//-----------------------------------------------------------------------------
#include "PalPCHW.h"         // projektove nezavisle definice - dal jsem si to do adresare projektu, namisto do spol. adresare inc, kde je to neprehledne
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
#endif
