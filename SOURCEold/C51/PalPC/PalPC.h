//*****************************************************************************
//
//    PalPC.h - common data definitions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __PalPC_H__
   #define __PalPC_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif




//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE    0x0100       // verze SW - POZOR, musi odpovidat promenne STR_CISLO_VERZE v main.c51

#define MAX_SIGNED_INT 32767  // Maximalni hodnota ulozitelna do typu int
#define MIN_SIGNED_INT -32768 // Minimalni hodnota ulozitelna do typu int

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Pro promennou ModEditace - co se edituje
enum {
  EDITACE_NORMAL,
  EDITACE_PODSVIT
};

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_VSE 0xFF               // Zobrazi se vse (jsou nastaveny vsechny bity)
#define ZOBRAZIT_MRIZKA 0x01            // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_G_SVISLE 0x02          // Zobrazi se zrychleni Gmetru svisle
#define ZOBRAZIT_G_VODOROVNE 0x04       // Zobrazi se zrychleni Gmetru vodorovne
#define ZOBRAZIT_OTACKY 0x08            // Zobrazi se otacky
#define ZOBRAZIT_PERIODICKY 0x20        // Zobrazi se periodicke veci (bargrafy v 2D rezimu atd)

// Mod cinnosti
typedef enum {
  REZIM_ZOBRAZENI_G,                    // Jednoduche zobrazeni hodnot G+RPM
  REZIM_2D_G_METR_BARGRAF,              // 2D Gmetr (tecka na displeji)
  REZIM_2D_G_METR_DIGIT,                // 2D Gmetr s 1 velkou hodnotou
  REZIM_PRUBEH_G,                       // Mereni prubehu zrychleni
  REZIM_DIAGNOSTIKA,                    // Diagnosticky rezim - zobrazeni vstupu/vystupu
  // Specialni rezimy dam az na konec
  REZIM_PRUBEH_G_KONEC=100,             // Mereni prubehu zrychleni
  REZIM_ONLINE                          // Start/stop online posilani dat
} TRezim;
#define ZAKLADNI_REZIM REZIM_2D_G_METR_DIGIT  // Vychozi rezim

// Co mam vysilat po seriove lince
// Vsechny prikazy musi byt mezi POSLAT_NIC a POSLAT_POSLEDNI
enum {
  POSLAT_NIC,              // Seriova linka je v klidu
  POSLAT_PRUBEHY,
  POSLAT_VERZI,
  POSLAT_POSLEDNI          // Index posledniho, uz neplatneho
};

//-----------------------------------------------------------------------------
// Konfigurace
//-----------------------------------------------------------------------------

// Meze jednoho analogoveho vstupu
typedef struct {
  int Min;
  int Max;
} TMezAin;
typedef TMezAin TMezeAin[AIN_COUNT];

// Pojmenovani vstupu
#define MAX_DELKA_JMENA_VSTUPU 6        // Maximalni pocet znaku u jmena vstupu
typedef byte TJmenoVstupu[MAX_DELKA_JMENA_VSTUPU];
typedef TJmenoVstupu TJmenaAin[AIN_COUNT];

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                           // VERZE

  unsigned int MaxOtackyPrubehu;        // Maximalni otacky u mereni prubehu akcelerace
  TMezeAin MezeAin;                     // Meze analogovych vstupu
  TJmenaAin JmenaAin;                   // Jmena jednotlivych analogovych vstupu

  byte KontrolniSuma;                   // Kontrolni soucet
} TKonfigurace;

extern TKonfigurace __xdata__ Konfigurace; // Buffer konfigurace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast
#define CFG_ALL                       0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERZE                     0x00000001L
#define CFG_MAXOTACKYPRUBEHU          0x00000002L
#define CFG_MEZEAIN                   0x00000004L
#define CFG_POJMENOVANIAIN            0x00000008L





//-----------------------------------------------------------------------------
// g-metr
//-----------------------------------------------------------------------------

#define POCET_G_METRU 2

// Indexy jednotlivych g-metru/RPM v poli
#define SVISLE 0             // Vodorovne zrychleni (zatacky)
#define VODOROVNE 1          // Svisle zrychleni (plyn/brzda)
#define OTACKY 2             // Otacky motoru
#define RYCHLOST 3             // Rychlost vozidla

// Kalibrace 1 Gmetru
typedef struct {
  unsigned int Zcal;          // Kalibrace 0 g
  unsigned int T2cal;         // Perioda signalu
  unsigned int K;             // Konstanta
} TKalGMetru;

typedef TKalGMetru TKalibraceGMetru[POCET_G_METRU];

typedef enum {
  CEKAM_NA_PRVNI_VZRUST,
  CEKAM_NA_POKLES,
  CEKAM_NA_VZRUST
} TStavMereniStridy;

// Mereni stridy v preruseni pro oba Gmetry
typedef struct {
  TStavMereniStridy Stav[POCET_G_METRU];              // Aktualni stav mereni
  unsigned int PocatecniCapture[POCET_G_METRU];       // Cas v okamziku pozitivni hrany
  unsigned int SirkaPulzu[POCET_G_METRU];             // Sirka pulzu v H
  unsigned int PeriodaSignalu;                        // Perioda signalu, stejna pro oba Gmetry
  unsigned int PreruseniSirkaImpulsu[POCET_G_METRU];  // Sirka signalu v H
  unsigned char NovaHodnota[POCET_G_METRU];           // Flag, ze byla zmerena nova strida
} TMereniStridy;

// Ulozeni stridy pro 1 Gmetr (do extra promenne, kterou nebude menit prerusovaci rutina)
typedef struct {
  unsigned int SirkaPulzu;             // Sirka pulzu v H
  unsigned int PeriodaSignalu;         // Perioda signalu, stejna pro oba Gmetry, ale tady to pro jednoduchost rozdelim
} TStrida;

// 1 mereni 1 Gmetru
typedef struct {
  // Promene pro vypocet jedne stridy
  TStrida Strida;

  // Promenne pro vypocet hodnoty G z periody
  int Hodnota;                   // Aktualni zrychleni (zatacky)
  unsigned char NovaHodnota;     // Flag, ze se vypocetla nova hodnota
} TG;

// Jedno mereni pro oba Gmetry
typedef TG TGs[POCET_G_METRU];

// Tara pro oba Gmetry
typedef int TTaraG [POCET_G_METRU];

// Struktura s hodnotami G v minulem kroku
typedef struct {
  int Vodorovne;
  int Svisle;
} THistorieG;

// 1 mereni otacek
#define OTACKY_POCET_PERIOD 4           // Kolik period se ma v preruseni sumovat, musi zustat 4 (kvuli 4valci aspon delitelne 4), jinak je treba zmena ve vypoctu otacek
#define RYCHLOST_POCET_PERIOD 4         // Kolik period se ma v preruseni sumovat
//#define OTACKY_MAX_ZMENA 4000           // Maximalni zmena otacek v 1 kroku mereni - objevovaly se tam spicky
typedef struct {
  // Promene pro vypocet jedne periody
  unsigned int Capture;          // Aktualni hodnota Capture registru
  unsigned int PosledniCapture;  // Predchozi hodnota Capture registru
  unsigned char PocetPreteceni;  // Pocet preteceni Capture registru
  unsigned char PocetNamerenychPeriod;  // Pocet period signalu, ktere jsem v preruseni nascital
  unsigned char NovaPerioda;     // Flag, ze se nacetla nejaka nova perioda

  // Promenne pro vypocet hodnoty otacek z periody
  int Hodnota;                   // Aktualni otacky
  unsigned char NovaHodnota;     // Flag, ze se vypocetla nova hodnota
} TOtacky;

#define PRUMEROVANI_ZOBRAZENI_G 100     // Pocet vzorku do prumeru G pri zobrazovani
#define PRUMEROVANI_2D_G_GMETR 15       // Pocet vzorku do prumeru G pri zobrazeni Gmetru
#define PRUMEROVANI_OTACKY_2D_G_GMETR 1 // Pocet vzorku do prumeru otacek
#define PRUMEROVANI_OTACKY 5            // Pocet vzorku do prumeru otacek
#define PRUMEROVANI_OTACKY_PRUBEH 1     // Pocet vzorku do prumeru otacek pri mereni prubehu G
#define PRUMEROVANI_RYCHLOST 1          // Pocet vzorku do prumeru rychlosti

// Prumerovani namerenych velicin
typedef struct {
  long Suma;                 // Nacitana suma pro prumerovani
  int Pocet;                 // Pocet vzorku v sume
  int Hodnota;               // Aktualni zrychleni (zatacky)
  unsigned char Prumerovani; // Kolik prvku do prmeru se ma nasumovat
  unsigned char NovaHodnota; // Flag, ze se vypocetla nova hodnota
} TPrum;

// Do teto struktury prumeruju podle potreby - prumeruju 2 G metry + 1 mereni otacek
typedef TPrum TPrumer[POCET_G_METRU+2];

// Klouzavy prumer (integrace) otacek
#define OTACKY_POCET_KLOUZAVY_PRUMER 3  // Pocet prvku klouzaveho prumeru
typedef int TOtackyKlouzavyPrumer[OTACKY_POCET_KLOUZAVY_PRUMER];


// Minimum a maximum namerenych velicin
typedef struct {
  int Min;                  // Minimum veliciny
  int Max;                  // Maximum veliciny
} TMM;

typedef TMM TMinMax[POCET_G_METRU];




//-----------------------------------------------------------------------------
// Mereni prubehu zrychleni
//-----------------------------------------------------------------------------

#define MIN_OTACKY 1000                 // Minimalni otacky v 1/min
#define MAX_OTACKY 9000                 // Maximalni otacky v 1/min
#define ROZSAH_OTACEK (MAX_OTACKY-MIN_OTACKY)  // Rozsah otacek, ve kterem se meri
#define OTACKY_KROK 100                 // Po jakem kroku se meri otacky, musi tim byt delitelne MIN_OTACKY a MAX_OTACKY
#define OTACKY_POCET_KROKU (ROZSAH_OTACEK/OTACKY_KROK)  // 9000/200=45
#define OTACKY_MAX_POCET_V_SUME 1000    // Maximalni pocet prvku v sume, aby neco nepreteklo (pribude 100 prvku za sekundu)
#define MAX_POCET_PRUBEHU 8            // Maximalni pocet prubehu, ktere lze ulozit do EEPROM

// Struktura pro sumovani behem mereni
typedef struct {
  long Suma;                            // Sem sumuju behem mereni
  unsigned int Pocet;                   // Pocet prvku v sume
  unsigned int Otacky;                  // Otacky, kterych se suma tyka. Otacky mohou dale jen rust.
  unsigned int CelkovyPocet;            // Celkovy pocet mereni behem celeho mereni prubehu
  unsigned int PocetMimoRozsah;         // Pocet mereni mimo rozsah pri mereni prubehu G
} TPrubehSuma;

// Jeden prubeh zrychleni, otacky odpovidaji poloze v poli
typedef int16 TPrubeh[OTACKY_POCET_KROKU];      // 45*2=90 bajtu
typedef TPrubeh TPrubehy[MAX_POCET_PRUBEHU];  // Prubehy v EEPROM




//-----------------------------------------------------------------------------
// Kalibrace analogovych vstupu
//-----------------------------------------------------------------------------

#define KALIBRACE_AIN_MAX_POCET_BODU 4     // Maximalni pocet bodu v kalibracni krivce
#define KALIBRACE_AIN_MAX_POCET_DESETIN 4  // Maximalni pocet desetinnych mist

// Kalibrace jednoho analogoveho vstupu
typedef struct {
  byte Prevod[KALIBRACE_AIN_MAX_POCET_BODU];
  int16 Hodnota[KALIBRACE_AIN_MAX_POCET_BODU];
  byte PocetDesetin;                    // Na kolik desetinnych mist se hodnota zobrazuje
  byte PocetBodu;                       // Pocet zadanych bodu v kalibracni krivce (nemusi byt zadane vsechny)
  int Min;                              // Minimalni hodnota pro zobrazeni
  int Max;                              // Maximalni hodnota pro zobrazeni
} TKalibrace1Ain;  // Velikost 18 bajtu

typedef TKalibrace1Ain TKalibraceAin[AIN_COUNT];



//-----------------------------------------------------------------------------
// Kalibrace vseho - globalni struktura pro vsechny kalibrace
//-----------------------------------------------------------------------------

typedef struct {
  TKalibraceGMetru Gmetr;               // Gmetr
  TKalibraceAin Ain;                    // Analogove vstupy
} TKalibrace;

// Kalibrace
extern TKalibrace __xdata__ Kalibrace;  // Buffer kalibrace v externi RAM



//-----------------------------------------------------------------------------
// Include potrebnych modulu, povoluje se v Hardware.h
//-----------------------------------------------------------------------------

#ifndef __PalDio_H__
   #include "PalMisc\PalDio.h"
#endif


//-----------------------------------------------------------------------------
// Globalni komunikacni struktura
//-----------------------------------------------------------------------------

typedef struct {
  // Prace s Gmetrem a otackami
  TMereniStridy MereniStridyPreruseni;    // Mereni stridy obou Gmetru, jehoz parametry se meni v preruseni
  TGs GMetr;
  THistorieG HistorieG;
  TOtacky Otacky;
  TOtacky Rychlost;
  TPrumer Prumer;
  TMinMax MinMax;

  // Stavove informace
  byte JsemVKalibraci;   // Flag, ze jsem v kalibraci a nema se tak pocitat G, ale maji se nechat jednotlive periody
  byte Zobrazovat;       // Flag, zda se maji veliciny zobrazovat nebo ne (v menu ne)
  byte Zobrazit;         // Co se ma zobrazit na displeji
  TRezim Rezim;          // Rezim cinnosti
  byte OnlineRezim;      // Flag, zda se maji posilat online data

  // Mereni prubehu zrychleni
  TPrubeh Prubeh;

  // Digitalni vstupy
  byte Din[DIN_COUNT];

  // Digitalni vystupy
  byte Dout[DOUT_COUNT];

  // Analogove vstupy
  byte Ain[AIN_COUNT];                  // Prevody
  int AinValue[AIN_COUNT];              // Hodnoty prevedene z prevodu podle kalibrace
  byte AinAlarm[AIN_COUNT];             // Zda je hodnota mimo meze

  // Seriova linka
  byte TypSerioveKomunikace;  // Co mam nacist nebo poslat

  // Alarm
  byte Alarm;

  byte PoslatOnline;                    // Flag, ze se ma poslat online prenos

} TPalData;

extern TPalData __xdata__ PalData;      // Komunikacni struktura v externi RAM






//-----------------------------------------------------------------------------
// Struktura cele interni EEPROM
//-----------------------------------------------------------------------------
#define IEP_SPARE (IEP_SIZE - sizeof(TKonfigurace) - sizeof(TKalibrace) - sizeof(TPrubehy))  // Velikost mezery na konci interni EEPROM

typedef struct {
  TKalibrace Kalibrace;                 // Kalibrace vseho - tu drzim zvlast. Schvalne ji mam hned na zacatku, aby se pri zmene delky cehokoliv neporusila
  TKonfigurace Konfigurace;             // Konfigurace
  TPrubehy Prubehy;                     // Prubehy zrychleni

  unsigned char Volne[IEP_SPARE];
} TIntEEPROM;



//-----------------------------------------------------------------------------
#endif
