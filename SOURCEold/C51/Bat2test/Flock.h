//*****************************************************************************
//
//    Flock.h      - Editace a parametry hejna
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Flock_H__
   #define __Flock_H__

#include "Hardware.h"                   // zakladni datove typy
#include "Bat2.h"

// Parametry pro start vazeni
typedef struct {
  byte           Online;                // Flag, zda se jedna o online vazeni
  word           InitialDayNumber;      // Cislo prvniho dne vazeni
  byte           FlockNumber;           // Cislo hejna, podle ktereho se ma vazit, nebo FLOCK_EMPTY_NUMBER pri rychlem vazeni
  TQuickWeighing QuickWeighing;         // Parametry zadane pro rychle vazeni (bez pouziti hejna)
  byte           WaitingForStart;       // Flag, zda se bude cekat na opozdeny start vazeni
  byte           ComparisonFlock;       // Cislo hejna, se kterym se porovnavaji vysledky, pripadne FLOCK_EMPTY_NUMBER pokud se neporovnava
  TLongDateTime  DateTime;              // Datum zahajeni vykrmu
} TCmdStartWeighing;
extern TCmdStartWeighing __xdata__ CmdStartWeighing;

//---------------------------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------------------------

#define FLOCK_COMPARISON_NOT_USED   0   // Provnani se nepouziva

//---------------------------------------------------------------------------------------------
// Globalni promenne
//---------------------------------------------------------------------------------------------

extern TWeighing __xdata__ Weighing;    // Data potrebna pro chod vykrmu aktualniho dne

//---------------------------------------------------------------------------------------------
// Podminky
//---------------------------------------------------------------------------------------------

#define FlockWeighingRunning()          (Config.WeighingStart.Running)
// Vyjadruje podminku, zda se prave vazi nebo ne - vlozi se napr. do if(FlockWeighingRunning()), if (!FlockWeighingRunning())

#define FlockWeighingWaiting()          (Config.WeighingStart.WaitingForStart)
// Vyjadruje podminku, zda se prave ceka na opozdeny start vazeni

#define FlockWeighingRunningOrWaiting() (Config.WeighingStart.WaitingForStart || Config.WeighingStart.Running)
// Vyjadruje podminku, zda se prave ceka na opozdeny start vazeni nebo uz se vazi

#define FlockWeighingFlock(FlockNumber) (FlockWeighingRunningOrWaiting() && Config.WeighingStart.UseFlock && FlockNumber == Config.WeighingStart.CurrentFlock)
// Vyjadruje podminku, zda se prave podle hejna <FlockNumber> vazi

#define FlockWeighingAnyFlock()         (FlockWeighingRunningOrWaiting() && Config.WeighingStart.UseFlock)
// Vyjadruje podminku, zda se prave podle nejakeho hejna vazi

//---------------------------------------------------------------------------------------------
// Funkce
//---------------------------------------------------------------------------------------------

void FlockStartWeighing(void);
// Zahaji vazeni podle parametru zadanych v globalni strukture CmdStartWeighing

void FlockExecuteStartWeighing(void);
// Odstartuje vazeni, v Config.WeighingStart uz musi byt vyplnene parametry

void FlockStopWeighing(void);
// Zastavi vazeni

//---------------------------------------------------------------------------------------------

void FlockReadHeader(byte FlockNumber);
// Nacte do globalniho <FlockHeader> hlavicku hejna <FlockNumber>

void FlockSaveHeader(byte FlockNumber);
// Ulozi z globalniho <FlockHeader> hlavicku hejna <FlockNumber>

void FlockReadCurve(byte FlockNumber, byte Gender);
// Nacte do globalniho <CurvePoints> rustovou krivku hejna <FlockNumber> a pohlavi <Gender>

void FlockSaveCurve(byte FlockNumber, byte Gender);
// Ulozi z globalniho <CurvePoints> rustovou krivku hejna <FlockNumber> a pohlavi <Gender>

void FlockReadItem( byte FlockNumber, byte __xdata__ *Buffer, word Offset, word Length);
// Nacte z Flash polozku z hejna s cislem <FlockNumber> a ulozi ji do <Buffer>.
// Polozka se zadava ofsetem od zacatku struktury.

void FlockSaveItem( byte FlockNumber, byte __xdata__ *Buffer, word Offset, word Length);
// Zapise do Flash polozku hejna s cislem <FlockNumber>.
// Polozka se zadava ofsetem od zacatku struktury. Obsah polozky je v <Buffer>, delka v <Length>

byte FlockReadFlockNumber(byte FlockNumber);
// Nacte z Flash a vrati cislo hejna na pozici <FlockNumber>

//---------------------------------------------------------------------------------------------

void FlockReadWeighingParameters( void);
// Pokud se prave krmi, nacte aktualni hejno do pameti

void FlockUpdateDay( void);
// Aktualizuje cislo dne vykrmu, dopocte parametry dne.
// Pouziva globalni <CurvePoints>

//---------------------------------------------------------------------------------------------

TYesNo FlockNewFlock( byte FlockNumber);
// Edituje vsechny parametry hejna s cislem <FlockNumber>

TYesNo FlockEditFlock( byte FlockNumber);
// Edituje vsechny parametry hejna s cislem <FlockNumber>

void FlockDeleteFlock( byte FlockNumber);
// Smaze ze seznamu hejn hejno s cislem <FlockNumber>

void FlockDeleteAllFlocks( void);
// Smaze vsechna hejna

byte FlockChooseFlock( void);
// Vybere 1 hejno z existujicich hejn a vrati jeho cislo. Pokud nevybere, vrati FLOCK_EMPTY_NUMBER
// Vyuziva globalni <FlockHeader>

TYesNo FlockContainsCurve(byte FlockNumber);
// Pokud hejno s cislem <FlockNumber> obsahuje krivku, vrati YES.
// Pouziva globalni <FlockHeader>

word FlockGetComparisonWeight(word DayNumber, byte Gender);
// Vrati hmotnost pro porovnani pro den <DayNumber> a pohlavi <Gender>. Pokud se neporovnava, vrati FLOCK_COMPARISON_NOT_USED.
// Vyuziva globalni <FlockHeader> i <CurvePoints>

#endif

