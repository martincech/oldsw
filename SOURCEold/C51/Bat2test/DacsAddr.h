//*****************************************************************************
//
//    DacsAddr.h - adresy pro ACS, prevzato od p. Wagnera
//    Version 1.0, Petr Veit
//
//*****************************************************************************


// vaha vsech zvbirat co se prave nachazi na platforme
#define   s_cweig      11        /* current weight */

// prumerna vaha
#define   s_aweig      12        /* average weight */

// pocet vazeni od zac.dne
#define   s_noch       13        /* no of weighing */

#define   s_unif       14        /* uniformity */

// popsano v manualu k ACS5
#define   s_gm12       15        /* w.gr. -12  */
#define   s_gm11       16        /* w.gr. -11 */
#define   s_gm10       17        /* w.gr. -10 */
#define   s_gm9        18        /* w.gr. - 9 */
#define   s_gm8        19        /* w.gr. - 8 */

#define   s_gm7        20       /* w.gr.  - 7 */
#define   s_gm6        21       /* w.gr.  - 6 */
#define   s_gm5        22       /* w.gr.  - 5 */
#define   s_gm4        23       /* w.gr.  - 4 */
#define   s_gm3        24       /* w.gr.  - 3 */
#define   s_gm2        25       /* w.gr.  - 2 */
#define   s_gm1        26       /* w.gr.  - 1 */
#define   s_gmid       27       /* w.gr. mid */
#define   s_gp1        28       /* w.gr.  1 */
#define   s_gp2        29       /* w.gr.  2 */

#define   s_gp3        30       /* w.gr.  3 */
#define   s_gp4        31       /* w.gr.  4 */
#define   s_gp5        32       /* w.gr.  5 */
#define   s_gp6        33       /* w.gr.  6 */
#define   s_gp7        34       /* w.gr.  7 */
#define   s_gp8        35       /* w.gr.  8 */
#define   s_gp9        36       /* w.gr.  9 */
#define   s_gp10       37       /* w.gr. 10 */
#define   s_gp11       38       /* w.gr. 11 */
#define   s_gp12       39       /* w.gr. 12 */

#define   s_wlevov     40       /* range over x% */
#define   s_wlevun     41       /* range below x% */


// vyber krivek
// krivka c 0 - zadna krivka nepouzita
// krivka c 1-5 nepouzita ve vasi vaze
// krivka c 6 je krivka definovana pomoci par. s_cup1...s_cuw5

#define   s_wcurno     42       /* curve no.  */


#define   s_wcalib     43       /* kalibracni zavazi */

#define   s_date       44       /* datum velice spec format !!  ddmmy */
#define   s_time       45       /* time  specielni format   hhmm*/
#define   s_proday     46       /* vyrovni den */


#define   s_setweig    47       /* set weight - zadana vaha */

// info o kalibraci vahy
#define   s_tara       48       /* tara */
#define   s_gain       49       /* gain x.xxx  (1.000 )  */



// krivka c.6
#define   s_cup1       50       /* 1. progr.prod.day */
#define   s_cuw1       51       /* 1. prog. set weight */
#define   s_cup2       52       /* 2. progr.prod.day */
#define   s_cuw2       53       /* 2. prog. set weight */
#define   s_cup3       54       /* 3. progr.prod.day */
#define   s_cuw3       55       /* 3. prog. set weight */
#define   s_cup4       56       /* 4. progr.prod.day */
#define   s_cuw4       57       /* 4. prog. set weight */
#define   s_cup5       58       /* 5. progr.prod.day */
#define   s_cuw5       59       /* 5. prog. set weight */


// nas terminal pise hodnoty do s_testpd od 0 do 32000 (max int)
// a vaha uklada do s_testw odpovidajici zadanou vahu pro vyrobni den
// z s_testpd....
#define   s_testpd     60       /* test program.prod.day*/
#define   s_testw      61       /* test program.set weight*/


#define   s_wrange     62       /* swinging range */

// vaha posledne zvazeneho a akceptovaneho kurete
#define   s_laweig     63       /* last weighing */
