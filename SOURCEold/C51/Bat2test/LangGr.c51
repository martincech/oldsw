//*****************************************************************************
//
// Definice stringu:
//   - En
//   - Gr
//
//*****************************************************************************

#include "Lang.h"                   // Stringy

#ifdef LANG_VERSION_GR  // Prekladam, jen pokud je tato jazykova verze aktivni

// ---------------------------------------------------------------------------------------------
// Definice jazyka
// ---------------------------------------------------------------------------------------------

byte code STR_LANGUAGE[] = "Language";   // Toto menu se nepreklada
byte code STR_MENU_LANGUAGE[] =
  "English\x0"
  "E\x09A\x09A\x096\x098\x099\x08B";

// ---------------------------------------------------------------------------------------------
// HW verze
// ---------------------------------------------------------------------------------------------

byte code STR_HW_VERSION[] =
  "GSM\x0"
  "LW1\x0"
  "MODBUS";

// ---------------------------------------------------------------------------------------------
// Menu
// ---------------------------------------------------------------------------------------------

byte code STR_DAY[] =
  "Day\x0"
  "Jour";
byte code STR_WEIGHT[] =
  "Weight\x0"
  "Poids";
byte code STR_START[] =
  "Start\x0"
  "D\x082but";
byte code STR_FLOCK[] =
  "Flock\x0"
  "Lot";
byte code STR_TARGET_WEIGHT[] =
  "Target\x0"
  "Cible";
byte code STR_LAST_SAVED_WEIGHT[] =
  "Last\x0"
  "Precedent";
byte code STR_COUNT[] =
  "Count\x0"
  "Nombre";
byte code STR_OK[] =
  "OK\x0"
  "OK";
byte code STR_CANCEL[] =
  "Cancel\x0"
  "Annuler";
byte code STR_RETRY[] =
  "Retry\x0"
  "Re-essay";
byte code STR_YES[] =
  "Yes\x0"
  "Oui";
byte code STR_NO[] =
  "No\x0"
  "Non";
byte code STR_ERROR[] =
  "Error\x0"
  "Erreur";
byte code STR_WAIT[] =
  "Wait\x0"
  "Attendre";
byte code STR_HEADS[] =
  "hds\x0"
  "anx";
/*byte code STR_VERZE[] =
  "version\x0"
  "version";*/

// Jednotky mam zvlast pro vyber v menu a pro zobrazeni (kvuli jednoduchosti)
byte code STR_UNITS_MENU[]=    // Pro menu
   "kg\x0" "lb\x0"
   "kg\x0" "lb";
byte code STR_UNITS[_UNITS_COUNT][_LANG_COUNT][3]= {   // Pro zobrazeni
  {
    "kg",
    "kg"
  },
  {
    "lb",
    "lb"
  }
};

// Dilek v menu
byte code STR_DIVISION_MENU[] =
  "1\x0"
  "2\x0"
  "5\x0"
  "10";

byte code STR_INVALID_VALUE[] =
  "The value\x0FFis not valid\x0"
  "La valeur n'est\x0FFpas valable";
byte code STR_SAVE_CHANGES[] =
  "Save changes?\x0"
  "Enregistrer\x0FFmodifications?";
byte code STR_STOP_WEIGHING_FIRST[] =
  "Weighing is started.\x0FFStop the weighing\x0FFfirst.\x0"
  "La pesee est commencee.\x0FFArreter la pesee d'abord.";

// Typy zobrazeni behem vazeni
byte code STR_WEIGHING_IDLE[] =
  "STOPPED\x0"
  "ARRET";
byte code STR_WEIGHING_WAITING[] =
  "WAITING\x0"
  "EN ATTENTE";
byte code STR_WEIGHING_WEIGHING[] =
  "WEIGHING\x0"
  "PESEE";
byte code STR_WEIGHING_PAUSE[] =
  "PAUSED\x0"
  "PAUSE";
byte code STR_WEIGHING_ONLINE[] =
  "ONLINE\x0"
  "EN LIGNE";

// Historie
byte code STR_ARCHIVE[] =
  "ARCHIVE\x0"
  "ARCHIVE";

// Statistika
byte code STR_CV[] =
  "Cv\x0"
  "Cv";
byte code STR_UNI[] =
  "UNI\x0"
  "Hom.";

// Logger
byte code STR_PAGE[] =
  "Page\x0"
  "Page";
byte code STR_NO_DATA[] =
  "No data\x0"
  "Pas de donnees";

// Editace hejna
byte code STR_STOP_WEIGHING[] =
  "Really stop\x0FFweighing of the flock?\x0"
  "Arreter vraiment\x0FFla pesee?";
byte code STR_SELECT_ANOTHER_FLOCK[] =
  "The selected flock is\x0FFnow weighed.\x0FFSelect another flock.\x0"
  "Le lot choisi est\x0FFen pesee. Choisir\x0FFun autre lot.";
byte code STR_USE_FLOCK[] =
  "Use a predefined flock?\x0"
  "Utiliser un lot\x0FFpredefini?";
byte code STR_STARTING_DAY[] =
  "Starting day:\x0FF\x0FF\x0FF\x0"
  "Debut de pesee:\x0FF\x0FF\x0FF";
byte code STR_FLOCK_NUMBER[] =
  "Flock number:\x0FF\x0FF\x0FF\x0"
  "Numero de lot:\x0FF\x0FF\x0FF";
byte code STR_REALLY_DELETE_ALL_FLOCKS[] =
  "Really delete\x0FFall the flocks?\x0"
  "Supprimer vraiment\x0FFtous les lots?";
byte code STR_ALL_FLOCKS_DELETED[] =
  "All the flocks\x0FFwere deleted.\x0"
  "Tous les lots\x0FFsont supprimes.";
byte code STR_START_WEIGHING_NOW[] =
  "Start weighing now?\x0"
  "Commencer la pes\x082e\x0FFmaintenant?";
byte code STR_COMPARE_RESULTS[] =
  "Compare with\x0FFanother flock?\x0"
  "Comparaison avec\x0FFun autre lot?";
byte code STR_FLOCK_NOT_VALID[] =
  "The selected flock is\x0FFnot valid.\x0FFSelect another flock.\x0"
  "Le lot choisi n'est\x0FFpas valable.\x0FFS\x082lectionner un autre lot.";

// Editace datumu a casu
byte code STR_DATE[] =
  "Date:\x0FF\x0FF\x0FF\x0"
  "Date:\x0FF\x0FF\x0FF";
byte code STR_TIME[] =
  "Time:\x0FF\x0FF\x0FF\x0"
  "Heure:\x0FF\x0FF\x0FF";

// Editace identifikacniho cisla
byte code STR_IDENTIFICATION_NUMBER[] =
  "Identification number:\x0FF\x0FF\x0FF\x0"
  "Numero d'identification:\x0FF\x0FF\x0FF";

// Editace parametru statistiky
byte code STR_HISTOGRAM_RANGE[] =
  "Histogram range:\x0FF\x0FF\x0FF\x0"
  "Echelle de l'histogramme:\x0FF\x0FF\x0FF";
byte code STR_UNIFORMITY_RANGE[] =
  "Uniformity range:\x0FF\x0FF\x0FF\x0"
  "Echelle d'uniformite:\x0FF\x0FF\x0FF";

// Parametry naslapne vahy
byte code STR_SCALE_MARGIN_ABOVE_FEMALES[] =
  "Margin above target\x0FFfor females:\x0FF\x0FF\x0"
  "Ecart au-dessus de la\x0FFcourbe pour les femelles:\x0FF\x0FF";
byte code STR_SCALE_MARGIN_BELOW_FEMALES[] =
  "Margin under target\x0FFfor females:\x0FF\x0FF\x0"
  "Ecart en-dessous de la\x0FFcourbe pour les femelles:\x0FF\x0FF";
byte code STR_SCALE_MARGIN_ABOVE_MALES[] =
  "Margin above target\x0FFfor males:\x0FF\x0FF\x0"
  "Ecart au-dessus de la\x0FFcourbe pour les m\x011les:\x0FF\x0FF";
byte code STR_SCALE_MARGIN_BELOW_MALES[] =
  "Margin under target\x0FFfor males:\x0FF\x0FF\x0"
  "Ecart en-dessous de la\x0FFcourbe pour les m\x011les:\x0FF\x0FF";
byte code STR_SCALE_FILTER[] =
  "Filter:\x0FF\x0FF\x0FF\x0"
  "Filtre:\x0FF\x0FF\x0FF";
byte code STR_SCALE_STABILIZATION_RANGE[] =
  "Stabilization:\x0FF\x0FF\x0FF\x0"
  "Stabilisation:\x0FF\x0FF\x0FF";
byte code STR_SCALE_STABILIZATION_TIME[] =
  "Stabilization\x0FFtime:\x0FF\x0FF\x0"
  "Temps\x0FFde stabilisation:\x0FF\x0FF";
byte code STR_SCALE_AUTO_MODE_TYPE[] =
  "Use gain in\x0FFautomatic mode?\x0"
  "Utiliser le calcul\x0FFautomatique du GMQ?";
byte code STR_JUMP_MODE[] =
   "Save sample upon:\x0FF\x0FF\x0FF\x0"
   "Sauveg. la donn\x082e\x0FFlors de:\x0FF\x0FF";
byte code STR_ENTER_LEAVE_BOTH[] =
   "Entering the scale\x0" "Leaving the scale\x0" "Both\x0"
   "Arriv\x082e sur balance\x0" "Sortie de la balance\x0" "Des deux";

// Kalibrace
byte code STR_CALIBRATION_UNITS[] =
  "Units:\x0FF\x0FF\x0FF\x0"
  "Unit\x082:\x0FF\x0FF\x0FF";
byte code STR_CALIBRATION_DIVISION[] =
  "Division:\x0FF\x0FF\x0FF\x0"
  "Graduation:\x0FF\x0FF\x0FF";
byte code STR_CALIBRATION_ZERO[] =
  "Empty the scale\x0"
  "Liberer les balances";
byte code STR_CALIBRATION_RANGE[] =
  "Put on the scale:\x0FF\x0FF\x0FF\x0"
  "Calibrer les balances:\x0FF\x0FF\x0FF";

// Podsvit
byte code STR_BACKLIGHT[]=
   "Backlight:\x0FF\x0FF\x0FF\x0"
   "Eclairage:\x0FF\x0FF\x0FF";

byte code STR_ON_OFF_AUTO[]=
   "Off\x0" "On\x0" "Automatic\x0"
   "Arr\x015t\x082\x0" "En marche\x0" "Automatique";

// Kopie do a z pametoveho modulu
byte code STR_INSERT_MODULE[] =
  "Insert a memory\x0FFmodule\x0"
  "Inserer un module\x0FFde memoire";
byte code STR_NO_MODULE[] =
  "Memory module\x0FFis not present\x0"
  "Le module de memoire\x0FFn'est pas present";
byte code STR_COPY_OK[] =
  "Copy successful\x0"
  "Copie reussie";
byte code STR_COPY_PROGRESS[] =
  "Progress:\x0FF\x0FF\x0FF\x0"
  "Progression:\x0FF\x0FF\x0FF";
byte code STR_CANT_COPY_CONFIG[] =
  "Cannot read\x0FFconfiguration\x0"
  "La configuration ne\x0FFpeut pas etre lue";

// Gsm

#ifdef VERSION_GSM          // Pouze pokud pouziva GSM

byte code STR_GSM_USE[] =
  "Use GSM\x0FFcommunication?\x0"
  "Utiliser appel GSM?";
byte code STR_GSM_MIDNIGHT[] =
  "Send statistics\x0FFat midnight?\x0"
  "Envoyer les donnees\x0FFa minuit?";
byte code STR_GSM_PERIOD[] =
  "Period:\x0FF\x0FF\x0FF\x0"
  "P\x082riode:\x0FF\x0FF\x0FF";
byte code STR_GSM_PERIOD_DAYS[] =
  "days\x0"
  "Jours";
byte code STR_GSM_REQUEST[] =
  "Send data\x0FFupon request?\x0"
  "Envoyer les donnees\x0FFa la demande?";
byte code STR_GSM_CHECK_NUMBERS[] =
  "Check numbers?\x0"
  "Verifier les numeros?";
byte code STR_GSM_NUMBER[] =
  "Phone number:\x0FF\x0FF\x0FF\x0"
  "Numero de telephone:\x0FF\x0FF\x0FF";
byte code STR_GSM_ACTIVE_NUMBERS[] =
  "Active numbers:\x0FF\x0FF\x0FF\x0"
  "Numeros utilisables:\x0FF\x0FF\x0FF";

#endif // VERSION_GSM

// RS-485

#if defined(VERSION_DACS_LW1) || defined(VERSION_MODBUS)

byte code STR_RS485_ADDRESS[] =
  "Address:\x0FF\x0FF\x0FF\x0"
  "R\x082f\x082rence:\x0FF\x0FF\x0FF";
byte code STR_RS485_SPEED[] =
  "Speed:\x0FF\x0FF\x0FF\x0"
  "Vitesse:\x0FF\x0FF\x0FF";
byte code STR_RS485_SPEED_ITEMS[] =
   "1200 Bd\x0" "2400 Bd\x0" "4800 Bd\x0" "9600 Bd\x0" "19200 Bd\x0" "38400 Bd\x0" "57600 Bd\x0" "115200 Bd";
byte code STR_RS485_PARITY[] =
  "Parity:\x0FF\x0FF\x0FF\x0"
  "Parit\x082:\x0FF\x0FF\x0FF";
byte code STR_RS485_PARITY_ITEMS[] =
   "8-n-1\x0" "8-e-1\x0" "8-o-1\x0" "8-n-2";
byte code STR_RS485_REPLY_DELAY[] =
  "Reply delay:\x0FF\x0FF\x0FF\x0"
  "Temps de r\x082ponse:\x0FF\x0FF\x0FF";
byte code STR_RS485_MS[] =
  "ms\x0"
  "ms";

#ifdef VERSION_MODBUS
byte code STR_RS485_SILENT_INTERVAL[] =
  "Silent interval:\x0FF\x0FF\x0FF\x0"
  "Temporisation:\x0FF\x0FF\x0FF";
byte code STR_RS485_PROTOCOL[] =
  "Protocol:\x0FF\x0FF\x0FF\x0"
  "Protocole:\x0FF\x0FF\x0FF";
byte code STR_RS485_PROTOCOL_ITEMS[] =
   "MODBUS RTU\x0" "MODBUS ASCII";
#endif // VERSION_MODBUS

#endif // VERSION_DACS_LW1, VERSION_MODBUS



//-----------------------------------------------------------------------------
// Hlavni Menu
//-----------------------------------------------------------------------------

byte code STR_MAIN_MENU_IDLE[] =
  // Eng
  "Start weighing\x0"
  "Archive\x0"
  "Compare\x0"
  "Copy data\x0"
  "Setup\x0"
  // Sp
  "Debut de la pesee\x0"
  "Archive\x0"
  "Comparer\x0"
  "Copier les donnees\x0"
  "Mise au point";

byte code STR_MAIN_MENU_WAITING[] =
  // Eng
  "Stop weighing\x0"
  "Archive\x0"
  "Compare\x0"
  "Copy data\x0"
  "Setup\x0"
  // Sp
  "Arr\x015ter la pesee\x0"
  "Archive\x0"
  "Comparer\x0"
  "Copier les donnees\x0"
  "Mise au point";

byte code STR_MAIN_MENU_WEIGHING[] =
  // Eng
  "Pause weighing\x0"
  "Stop weighing\x0"
  "Archive\x0"
  "Compare\x0"
  "Copy data\x0"
  "Setup\x0"
  // Sp
  "Pause\x0"
  "Arr\x015ter la pesee\x0"
  "Archive\x0"
  "Comparer\x0"
  "Copier les donnees\x0"
  "Mise au point";

byte code STR_MAIN_MENU_PAUSE[] =
  // Eng
  "Continue weighing\x0"
  "Stop weighing\x0"
  "Archive\x0"
  "Compare\x0"
  "Copy data\x0"
  "Setup\x0"
  // Sp
  "Continuer a peser\x0"
  "Arr\x015ter la pesee\x0"
  "Archive\x0"
  "Comparer\x0"
  "Copier les donnees\x0"
  "Mise au point";

//-----------------------------------------------------------------------------
// Menu nastaveni
//-----------------------------------------------------------------------------

byte code STR_MENU_SETUP_TITLE[] =
  "Setup\x0"
  "Mise au point";

byte code STR_MENU_SETUP[] =
  // Eng
  "Date and time\x0"
  "Identification number\x0"
  "Flocks\x0"
  "Statistics\x0"
#ifdef VERSION_GSM
  "GSM\x0"
#else
  "RS-485\x0"
#endif // VERSION_GSM
  "Scale\x0"
  "Backlight\x0"
  "Read from module\x0"
  "Start online\x0"
  // Sp
  "Date et heure\x0"
  "Numero d'identification\x0"
  "Lots\x0"
  "Statistiques\x0"
#ifdef VERSION_GSM
  "GSM\x0"
#else
  "RS-485\x0"
#endif // VERSION_GSM
  "Balances\x0"
  "Eclairage\x0"
  "Lecture du module\x0"
  "Debut mode en ligne";

//-----------------------------------------------------------------------------
// Menu hejna
//-----------------------------------------------------------------------------

byte code STR_MENU_SETUP_FLOCKS_TITLE[] =
  "Flocks\x0"
  "Lots";

byte code STR_MENU_SETUP_FLOCKS[] =
  // Eng
  "New flock\x0"
  "Edit flock\x0"
  "Delete flock\x0"
  "Delete all flocks\x0"
  // Sp
  "Nouveau lot\x0"
  "Editer le lot\x0"
  "Supprimer le lot\x0"
  "Supprimer tous les lots";

//-----------------------------------------------------------------------------
// Menu vahy
//-----------------------------------------------------------------------------

byte code STR_MENU_SETUP_SCALE_TITLE[] =
  "Scale\x0"
  "Balances";

byte code STR_MENU_SETUP_SCALE[] =
  // Eng
  "Saving parameters\x0"
  "Correction curve\x0"
  "Units\x0"
  "Calibration\x0"
  // Sp
  "Sauvegarde\x0"
  "Courbe de correction\x0"
  "Unit\x082\x0"
  "Calibrage";

//-----------------------------------------------------------------------------
// Menu GSM
//-----------------------------------------------------------------------------

#ifdef VERSION_GSM          // Pouze pokud pouziva GSM

byte code STR_MENU_SETUP_GSM_TITLE[] =
  "GSM\x0"
  "GSM";

byte code STR_MENU_SETUP_GSM[] =
  // Eng
  "Settings\x0"
  "Numbers\x0"
  // Sp
  "Mises au point\x0"
  "Numeros";

#endif // VERSION_GSM

//-----------------------------------------------------------------------------
// Menu RS-485
//-----------------------------------------------------------------------------

#if defined(VERSION_DACS_LW1) || defined(VERSION_MODBUS)

byte code STR_MENU_SETUP_RS485_TITLE[] =
  "RS-485\x0"
  "RS-485";

byte code STR_MENU_SETUP_RS485[] =
  // Eng
  "Address\x0"
  "Speed\x0"
  "Parity\x0"
#ifdef VERSION_MODBUS
  "Protocol\x0"
#endif // VERSION_MODBUS
  "Delays\x0"
  // Sp
  "R\x082f\x082rence\x0"
  "Vitesse\x0"
  "Parit\x082\x0"
#ifdef VERSION_MODBUS
  "Protocole\x0"
#endif // VERSION_MODBUS
  "Attente";

#endif // VERSION_DACS_LW1, VERSION_MODBUS

//-----------------------------------------------------------------------------
// Editace hejn
//-----------------------------------------------------------------------------

byte code STR_FLOCK_EXISTS[] =
  "The flock already exists.\x0FFEnter different number.\x0"
  "Le lot existe deja:\x0FFentrer un nombre\x0FFdifferent.";
byte code STR_FLOCK_DOESNT_EXIST[] =
  "The selected flock\x0FFdoesn't exist.\x0FFSelect another number.\x0"
  "Le lot choisi n'existe\x0FFpas: selectionner\x0FFun autre nombre.";
byte code STR_ENTER_FLOCK_NAME[] =
  "Flock name:\x0FF\x0FF\x0FF\x0"
  "Nom du lot:\x0FF\x0FF\x0FF";
byte code STR_USE_BOTH_GENDERS[] =   // Vyuziva i Menu.c
  "Use both genders?\x0"
  "Separer les sexes?";
byte code STR_USE_CURVES[] =
  "Use growth curves?\x0"
  "Utiliser des courbes\x0FFde poids specifiques?";
byte code STR_PREFACE_CURVE_FEMALES[] =
  "Now you can enter\x0FFa growth curve\x0FFfor females.\x0"
  "Maintenant programmer\x0FFune courbe de croissance\x0FFfemelles";
byte code STR_PREFACE_CURVE_MALES[] =
  "Now you can enter\x0FFa growth curve\x0FFfor males.\x0"
  "Maintenant programmer\x0FFune courbe de croissance\x0FFm\x011les";
byte code STR_PREFACE_CURVE[] =
  "Now you can enter\x0FFa growth curve.\x0"
  "Maintenant programmer\x0FFune courbe\x0FFde croissance.";
byte code STR_USE_TIME_LIMIT[] =
  "Limit the weighing\x0FFby time?\x0"
  "Limiter le temps\x0FFde pesee?";
byte code STR_LIMIT_FROM[] =
  "Weigh from:\x0FF\x0FF\x0FF\x0"
  "Pesee \x010 partir de:\x0FF\x0FF\x0FF";
byte code STR_LIMIT_TILL[] =
  "Weigh till:\x0FF\x0FF\x0FF\x0"
  "Pesee jusqu'a:\x0FF\x0FF\x0FF";
byte code STR_LIMIT_TIME[] =
  "Hours\x0"
  "Heures";

byte code STR_INITIAL_WEIGHT[] =
  "Initial weight:\x0FF\x0FF\x0FF\x0"
  "Poids initial:\x0FF\x0FF\x0FF";
byte code STR_INITIAL_WEIGHT_FEMALES[] =
  "Initial weight\x0FFfor females:\x0FF\x0FF\x0"
  "Poids initial\x0FFpour femelles:\x0FF\x0FF";
byte code STR_INITIAL_WEIGHT_MALES[] =
  "Initial weight\x0FFfor males:\x0FF\x0FF\x0"
  "Poids initial\x0FFpour males:\x0FF\x0FF";

byte code STR_NO_FLOCK_FOUND[] =
  "No flock found\x0"
  "Pas de lot\x0FFen memoire";
byte code STR_CHOOSE_FLOCK[] =
  "Choose a flock:\x0FF\x0FF\x0FF\x0"
  "Choisir un lot:\x0FF\x0FF\x0FF";
byte code STR_NONAME[] =
  "no name\x0"
  "Pas de nom";

// ---------------------------------------------------------------------------------------------
// Editace rustove krivky
// ---------------------------------------------------------------------------------------------

byte code STR_SAVE[] =
  "Save\x0"
  "Sauveg.";
byte code STR_DELETE[] =
  "Delete\x0"
  "Effacer";
byte code STR_QUIT[] =
  "Quit\x0"
  "Quitter";
byte code STR_NEXT[] =
  "Next\x0"
  "Suivant";
byte code STR_ADD[] =
  "Add\x0"
  "Ajouter";
byte code STR_CHANGE[] =
  "Change\x0"
  "Modifier";
byte code STR_MOVE[] =
  "Move\x0"
  "Deplac.";
byte code STR_DAY_COLON[] =
  "Day:\x0"
  "Jour:";
byte code STR_WEIGHT_COLON[] =
  "Weight:\x0"
  "Poids:";
byte code STR_CURVE_FULL[] =
  "The curve is full\x0"
  "La courbe est satur\x082e";

byte code STR_CURVE_DAY_EXISTS[] =
  "The day already exists.\x0FFOverwrite?\x0"
  "Ce jour existe deja.\x0FFLe remplacer?";
byte code STR_CURVE_DELETE[] =
  "Delete the selected\x0FFday from the table?\x0"
  "Supprimer le jour\x0FFchoisi du tableau?";

//-----------------------------------------------------------------------------
// SMS - Pozor, nevyuzivat zadne diakriticke znaky
//-----------------------------------------------------------------------------

byte code STR_SMS_SCALES[] =
  "SCALE \x0"
  "BALANCE ";
byte code STR_SMS_DAY[] =
  "DAY \x0"
  "JOUR ";
byte code STR_SMS_FEMALE[] =
  "FEMALES: \x0"
  "FEMELLE: ";
byte code STR_SMS_MALE[] =
  "MALES: \x0"
  "MALE: ";
byte code STR_SMS_CNT[] =
  "Cnt \x0"
  "Nb ";
byte code STR_SMS_AVG[] =
  "Avg \x0"
  "Moy ";
byte code STR_SMS_GAIN[] =
  "Gain \x0"
  "GMQ ";
byte code STR_SMS_SIG[] =
  "Sig \x0"
  "Ety ";
byte code STR_SMS_CV[] =
  "Cv \x0"
  "Cv ";
byte code STR_SMS_UNI[] =
  "Uni \x0"
  "Hom ";

byte code STR_SMS_STOPPED[] =
  "STOPPED\x0"
  "ARRET";
byte code STR_SMS_WAITING[] =
  "WAITING\x0"
  "EN ATTENTE";
byte code STR_SMS_NODATA[] =
  "NO DATA\x0"
  "PAS DE DONNEES";

// ---------------------------------------------------------------------------------------------
// Editace korekcni krivky
// ---------------------------------------------------------------------------------------------

byte code STR_CORRECTION_USE[] =
  "Use correction curve?\x0"
  "Utiliser la courbe\x0FFde correction?";

byte code STR_CORRECTION_DAY1[] =
  "Day 1:\x0FF\x0FF\x0FF\x0"
  "Jour 1:\x0FF\x0FF\x0FF";

byte code STR_CORRECTION_DAY2[] =
  "Day 2:\x0FF\x0FF\x0FF\x0"
  "Jour 2:\x0FF\x0FF\x0FF";

byte code STR_CORRECTION_CORRECTION[] =
  "Correction:\x0FF\x0FF\x0FF\x0"
  "Correction:\x0FF\x0FF\x0FF";



#endif
