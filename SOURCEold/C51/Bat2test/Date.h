//*****************************************************************************
//
//    Date.h - Prace s datem
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Date_H__
   #define __Date_H__

#include "Hardware.h"     // zakladni datove typy
#include "Bat2.h"

extern TLongDateTime __xdata__ CmdDateTime;         // Pro nastaveni casu

TYesNo DateEqual( TLongDateTime __xdata__ *Date1, TLongDateTime __xdata__ *Date2);
// vraci YES, jsou-li data shodna

word DateDaysBetweenTwoDates(TLongDateTime __xdata__ *StartDate, TLongDateTime __xdata__ *EndDate);
// Vypocte pocet celych dnu mezi dvema datumy. Bere se pocet celych dnu, tj. nebere se v uvahu cas.

void DateSet(void);
// Nastavi datum a cas ulozeny ve strukture CmdDateTime

#endif
