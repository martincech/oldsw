//*****************************************************************************
//
//   Module.h    Flash Modul
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __Module_H__
   #define __Module_H__

#include "Hardware.h"     // zakladni datove typy

TYesNo ModuleIsPresent( void);
// Vrati YES, je-li modul pritomen

void ModuleCopy( void);
// Kopiruje obsah flash na modul

TYesNo ModuleVerify( void);
// Kontroluje obsah flash proti modulu

void ModuleReadConfig();
// Nacte z modulu konfiguraci do struktury KonfiguraceModul

void ModuleCopyFlocksFromModule();
// Zkopiruje vsechna hejna z modulu do interni flash

#endif
