//*****************************************************************************
//
//   NvRam.h        NVRAM application map
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __NvRam_H__
  #define __NvRam_H__

#include "..\inc\AtFlash.h"    // cache structure
#include "..\inc\Ds17287.h"    // NVRAM
#include "Bat2.h"
#include "Dacs.h"
#include "SlideAvg.h"
#include "GsmMid.h"

//-----------------------------------------------------------------------------
// Struktura NVRAM
//-----------------------------------------------------------------------------

// Velikost oblasti pro ukladani Dacs
#ifdef VERSION_DACS_LW1
  #define DACS_SIZE (sizeof(TDacsDay) + sizeof(byte))
#else
  #define DACS_SIZE 0
#endif // VERSION_DACS_LW1

// Velikost oblasti pro ukladani Sliding average
#ifdef VERSION_BIGDUTCHMAN
  #define SLIDEAVG_SIZE (sizeof(TSlideAvg) + sizeof(byte))
#else
  #define SLIDEAVG_SIZE 0
#endif // VERSION_BIGDUTCHMAN

// Velikost oblasti pro ukladani pulnocni SMS
#ifdef VERSION_GSM
  #define GSMMIDNIGHT_SIZE (sizeof(TGsmMidnightNvRam) + sizeof(byte))
#else
  #define GSMMIDNIGHT_SIZE 0
#endif // VERSION_GSM


#ifndef FLASH_NVRAM // interni buffer flash

  #define NVR_SPARE (RTC_NVRAM_SIZE - sizeof(TArchive) - sizeof(byte) - DACS_SIZE - SLIDEAVG_SIZE - GSMMIDNIGHT_SIZE)

  typedef struct {
    TArchive    Archive;                    // Statistika
    byte        ArchiveChecksum;            // Kontrolni soucet polozky Archive

#ifdef VERSION_DACS_LW1
    TDacsDay    DacsDay;                    // Statistika dne pro Dacs ACS
    byte        DacsDayChecksum;            // Kontrolni soucet polozky DacsDay
#endif // VERSION_DACS_LW1

#ifdef VERSION_BIGDUTCHMAN
    TSlideAvg   SlideAvg;                   // Data pro Sliding average
    byte        SlideAvgChecksum;           // Kontrolni soucet polozky SlideAvg
#endif // VERSION_BIGDUTCHMAN

#ifdef VERSION_GSM
    TGsmMidnightNvRam GsmMidnightNvRam;
    byte              GsmMidnightNvRamChecksum;     // Kontrolni soucet polozky GsmMidnightNvRam
#endif // VERSION_GSM

    byte        Spare[ NVR_SPARE];
  } TNvrData;  // 211 + 1 + 59 + 1 = 272 + NVR_SPARE bajtu

//-----------------------------------------------------------------------------
#else // NVRAM cache flash

  #define NVR_SPARE (RTC_NVRAM_SIZE - sizeof(TArchive) - sizeof(byte) - DACS_SIZE - SLIDEAVG_SIZE - GSMMIDNIGHT_SIZE - sizeof(TFlashCache))

  typedef struct {
    TArchive    Archive;                    // Statistika
    byte        ArchiveChecksum;            // Kontrolni soucet polozky Archive

#ifdef VERSION_DACS_LW1
    TDacsDay    DacsDay;                    // Statistika dne pro Dacs ACS
    byte        DacsDayChecksum;            // Kontrolni soucet polozky DacsDay
#endif // VERSION_DACS_LW1

#ifdef VERSION_BIGDUTCHMAN
    TSlideAvg   SlideAvg;                   // Data pro Sliding average
    byte        SlideAvgChecksum;           // Kontrolni soucet polozky SlideAvg
#endif // VERSION_BIGDUTCHMAN

#ifdef VERSION_GSM
    TGsmMidnightNvRam GsmMidnightNvRam;
    byte              GsmMidnightNvRamChecksum;     // Kontrolni soucet polozky GsmMidnightNvRam
#endif // VERSION_GSM

    byte        Spare[ NVR_SPARE];

    TFlashCache FlashCache;                 // cache pro flash
  } TNvrData;   // 211 + 1 + 59 + 1 = 272 + NVR_SPARE + 514 bajtu

  #define NvramPage() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Page)) // adresa cisla stranky
  #define NvramData() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Data)) // adresa bufferu dat
#endif // NVRAM cache
//-----------------------------------------------------------------------------

#endif
