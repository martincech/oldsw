//*****************************************************************************
//
//    Hardware.h  - Casomira hardware descriptions
//    Version 1.0
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof

//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define DATA P1
sbit D0 = DATA^0;
sbit D1 = DATA^1;
sbit D2 = DATA^2;
sbit D3 = DATA^3;
sbit D4 = DATA^4;
sbit D5 = DATA^5;
sbit D6 = DATA^6;
sbit D7 = DATA^7;

sbit DI = P0^2;
sbit RW = P3^3;
sbit E = P2^6;
sbit CS1 = P3^5;
sbit CS2 = P3^7;
sbit RESET = P3^6;

sbit START = P2^2;

sbit CERVENA = P3^2;
sbit ORANZOVA = P3^4;
sbit ZELENA = P2^3;
sbit VOLBASEMAFORU = P2^4;

// Brany
sbit START_LEFT   = P2^0;
sbit START_RIGHT  = P2^1;
sbit FINISH_LEFT  = P2^5;
sbit FINISH_RIGHT = P2^7;
sbit USE_SENSORS  = P0^4;

//-----------------------------------------------------------------------------
// Brany
//-----------------------------------------------------------------------------

#define UseStartSensors()        USE_SENSORS
#define StartLeftInterrupted()   START_LEFT
#define StartRightInterrupted()  START_RIGHT
#define FinishLeftInterrupted()  (!FINISH_LEFT)
#define FinishRightInterrupted() (!FINISH_RIGHT)

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  2             // zdvojena baudova rychlost

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

#define TIMER0_PERIOD 1     // Perioda casovace 1ms pro stopky

//-----------------------------------------------------------------------------
// Radic displeje Samsung Ks0108b
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       DATA
#define DisplayD0         D0
#define DisplayD1         D1
#define DisplayD2         D2
#define DisplayD3         D3
#define DisplayD4         D4
#define DisplayD5         D5
#define DisplayD6         D6
#define DisplayD7         D7
#define DisplayRW         RW
#define DisplayDI         DI
#define DisplayE          E
#define DisplayCS1        CS1
#define DisplayCS2        CS2
#define DisplayRESET      RESET

// Podminena kompilace

// Datova sbernice DisplayDATA 1:1
#define KS_BUS     1
// Obecne funkce
#define KS_ON      1


//-----------------------------------------------------------------------------
// Displej - podminena kompilace
//-----------------------------------------------------------------------------

// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ xdata

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Cary, obdelniky atd.
#define DISPLAY_SET_AREA_FAST     1
//#define DISPLAY_SET_AREA          1
//#define DISPLAY_HOR_LINE          1
#define DISPLAY_FAST_VERT_LINE    1
//#define DISPLAY_VERT_LINE         1
//#define DISPLAY_PIXEL             1
//#define DISPLAY_LINE              1
//#define DISPLAY_FAST_BARGRAF      1

// Fonty - pouziti jednotlivych fontu
#define DISPLAY_TAHOMA8           1
//#define DISPLAY_LUCIDA6           1
//#define DISPLAY_MYRIAD32          1
//#define DISPLAY_MYRIAD40          1

// Vety - pouziti jednotlivych zarovnani u vet
#define DISPLAY_STRING            1
#define DISPLAY_STRING_RIGHT      1
//#define DISPLAY_STRING_CENTER     1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER     1
#define DISPLAY_NUMBER            1
//#define DISPLAY_CHAR_FORMATED     1

// Symbol
//#define DISPLAY_SYMBOL            1

// Editace
//#define DISPLAY_EDIT              1
//#define DISPLAY_EDIT_TEXT         1

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
//#define ObsluzModEditace()
#define DisplayEditExecute(Znaky) PipniKlavesnice()



//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// Podminena kompilace:

//#define DISPLAY_BARGRAF     1

// Dialog
//#define DISPLAY_DIALOG 1
//#define DIALOG_OTOCENE_KLAVESY 1        // Zda se ma otocit zobrazeni tlacitek Enter a Esc

// Menu
//#define DISPLAY_MENU        1
//#define MENU_MAX_ITEMS      9             // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
//#define DISPLAY_CHOOSE_ITEM 1

// Jazyky
//#define DISPLAY_LANGUAGES   1             // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka

//-----------------------------------------------------------------------------
// Displej - parametry
//-----------------------------------------------------------------------------

#define DISPLAY_WIDTH 128
#define DISPLAY_HEIGHT 64


//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define K0         START

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
  K_START  = _K_FIRSTUSER,             // Start
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (1000/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch


#endif // __Hardware_H__
