//*****************************************************************************
//
//    Timer.c - Stopky
//    Version 1.0
//
//*****************************************************************************

#ifndef __Timer_H__
   #define __Timer_H__

#include "Hardware.h"     // zakladni datove typy

// Stav stopek
typedef enum {
  TIMER_OFF,                            // Stopky vypnute
  TIMER_ON                              // Stopky bezi
} TTimerStatus;

// Data stopek pro 1 vuz
typedef struct {
  TTimerStatus Status;                  // Stav stopek
  word Time;                            // Cas v milisekundach, max. 65sec
} TTimer;


void TimerInit(TTimer __xdata__ *Timer);
  // Inicializace stopek

void TimerStart(TTimer __xdata__ *Timer);
  // Start stopek

void TimerPeriod(TTimer __xdata__ *Timer);
  // Obsluha periody stopek

void TimerStop(TTimer __xdata__ *Timer);
  // Stop stopek

dword TimerCalculateTime(word Time);
  // Prepocte interni cas stopek na milisekundy

#endif
