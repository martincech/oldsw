//*****************************************************************************
//
//    Light.c - Ovladani semaforu
//    Version 1.0
//
//*****************************************************************************

#ifndef __Light_H__
   #define __Light_H__

#include "Hardware.h"     // zakladni datove typy

void LightOff();
  // Vypne vsechna svetla semaforu

void LightStart();
  // Provede startovaci sekvenci, na konci zustane roznuta zelena

void LightPeriodic();
  // Pocita dobu svitu zeleneho svetla, pri prekroceni doby zhasne. Volat po 100ms.

#define LightUse() !VOLBASEMAFORU
  // Pokud se pouziva semafor, vrati YES

#endif
