//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif


void MenuInit();
// Inicializace zobrazeni

void MenuRedrawLight();
  // Prekresleni displeje pri startu sekvence semaforu

void MenuRedrawStart();
// Prekresleni displeje pri startu mereni

void MenuRedrawStop();
  // Vykresleni predcasneho ukonceni sprintu pomoci tlacitka

void MenuRedrawFinish();
  // Vykresleni vysledku sprintu

void MenuRedrawPeriodic();
  // Periodicke prekresleni displeje

#endif
