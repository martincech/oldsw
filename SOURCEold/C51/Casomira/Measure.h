//*****************************************************************************
//
//    Measure.c - Mereni obou drah
//    Version 1.0
//
//*****************************************************************************

#ifndef __Measure_H__
   #define __Measure_H__

#include "Hardware.h"     // zakladni datove typy
#include "Timer.h"        // Stopky

// Stav mereni
typedef enum {
  MEASURE_IDLE,
  MEASURE_STARTING,
  MEASURE_RUNNING
} TMeasureStatus;

// Stav mereni 1 drahy
typedef enum {
  LINE_STARTING,                        // Auto jeste neodstartovalo
  LINE_RUNNING,                         // Auto odstartovalo
  LINE_FINISHED                         // auto projelo cilem
} TLineStatus;

// Start v 1 draze
typedef struct {
  byte Valid;                           // YES/NO Hlidani korektniho startu
  word Time;                            // Cas reakce v TIMER0_PERIOD milisekund, max. 65000 * TIMER0_PERIOD miklisekund
} TStart;

// Stav mereni sumu na cilovem cidle 1 drahy
typedef enum {
  NOISE_SIGNAL_OK,                      // Brana je volna, prijimam IR signal
  NOISE_SIGNAL_INTERRUPTED              // Signal je prerusen
} TNoiseStatus;

// Sum na cilovem cidle v 1 draze
typedef struct {
  TNoiseStatus Status;                  // Stav hlidani sumu
  word Time;                            // Cas pocatku sumu v TIMER0_PERIOD milisekundach
} TNoise;

// Jedna draha
typedef struct {
  byte StartGateInterrupted;            // Aktualni stav startovaci brany
  byte FinishGateInterrupted;           // Aktualni stav cilove brany
  TLineStatus Status;                   // Stav drahy
  byte NewStatus;                       // YES/NO prave doslo ke zmene stavu
  TTimer Timer;                         // Stopky
  TStart Start;                         // Start sprintu
  TNoise Noise;                         // Sum signalu
  word Time;                            // Cas samotneho sprintu (bez doby reakce)
} TLine;

typedef struct {
  TMeasureStatus Status;                // Stav mereni
  TLine LeftLine;                       // Leva draha
  TLine RightLine;                      // Prava draha
} TMeasure;
extern TMeasure __xdata__ Measure;

void MeasureInit();
  // Inicializace mereni

void MeasureReadSensors();
  // Nacte stavy bran

void MeasureStart();
  // Start mereni

void MeasurePeriod();
  // Obsluha periody mereni

void MeasureExecute();
  // Obsluha mereni

void MeasureStop();
// Predcasne rucni ukonceni mereni

#endif
