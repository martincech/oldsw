//*****************************************************************************
//
//    Hardware.h  - Transmonitoring hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE


//-----------------------------------------------------------------------------
// Zapojeni portu
//-----------------------------------------------------------------------------

#define AutoAccuOk() AdioBatteryOk()

// spolecne vodice (podle schematu) :

#define AutoDATA      P0           // datova sbernice

// teplomery na sbernici :
sbit AutoT1      = AutoDATA^0;     // teplomer T1
sbit AutoT2      = AutoDATA^1;     // teplomer T2
sbit AutoT3      = AutoDATA^2;     // teplomer T3
sbit AutoT4      = AutoDATA^3;     // teplomer T4
sbit AutoT5      = AutoDATA^4;     // teplomer T5
sbit AutoT6      = AutoDATA^5;     // teplomer T6
sbit AutoTOUT    = AutoDATA^6;     // teplomery spolecne buzeni
sbit AutoLOWBAT  = AutoDATA^7;     // Hlidani napajeni

sbit AutoSVIT    = P1^0;           // Podsvit displeje
sbit AutoCSCIDLA = P1^1;           // CS 1-wire
sbit AutoCS      = P1^2;           // CS potenciometr
sbit AutoINC     = P1^3;           // INC potenciometr
sbit AutoSO      = P1^4;           // SO  EEPROM, DisplayA0, AdcData
sbit AutoCSEE    = P1^5;           // CS  EEPROM
sbit AutoSI      = P1^6;           // SI  EEPROM, DisplayRD, I2C SDA
sbit AutoCLK     = P1^7;           // SCK EEPROM, DisplayWR, AdcCLK


sbit AutoDCLK    = P2^0;           // DCLK touchpad
sbit AutoBUSY    = P2^1;           // BUSY touchpad
sbit AutoDOUT    = P2^2;           // DOUT touchpad
sbit AutoPEN     = P2^3;           // PEN  touchpad
sbit AutoRES     = P2^4;           // Reset displeje
sbit AutoPISK    = P2^5;           // sirena
sbit AutoVYST1   = P2^6;           // Digitalni vystup 1
sbit AutoCSD     = P2^7;           // DisplayCS

sbit AutoRXD     = P3^0;           // COM Rx
sbit AutoTXD     = P3^1;           // COM Tx
sbit AutoSCL     = P3^2;           // I2C SCL
sbit AutoSIMOD   = P3^3;           // SI modulu EEPROM
sbit AutoSOMOD   = P3^4;           // CS  modulu EEPROM
sbit AutoCLKMOD  = P3^5;           // SCK modulu EEPROM
sbit AutoCSMOD   = P3^6;           // SO  modulu EEPROM
sbit AutoRD      = P3^7;           // Napajeni RS232

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz

#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :

#define CPU_X2     2            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define T2_X2      1            // timer 2 v rezimu X2
#define SI_X2      1            // COM v rezimu X2
#define PCA_X2     1            // PCA v rezimu X2
#define WD_X2      1            // WatchDog v rezimu X2
#define SPI_X2     1            // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :

#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost (1=ne, 2=ano)

// Casovac 0 :

#define TIMER0_PERIOD 20        // perioda casovace 0 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut AutoPISK         // vystup generatoru

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1        // asynchronni pipani

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       AutoAccuOk()      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

#define IEP_READ_BYTE     1        // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1        // zapis jednoho bytu

//-----------------------------------------------------------------------------
// Pripojeni AT25640 / X25645 na desce
//-----------------------------------------------------------------------------
// Tady je zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

// zapojeni pinu volne k uzivani :
#define EepCS    AutoCSEE           // chipselect /CS
#define EepSCK   AutoCLK            // hodiny SCK
#define EepSO    AutoSO             // vystup dat SO
#define EepSI    AutoSI             // vstup dat SI (muze byt shodny s SO)
// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// ochrana proti vybiti aku :

#define EepAccuOk()       AutoAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    64      // velikost stranky
#define EEP_SIZE      32768      // celkova kapacita

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// Podminena kompilace :

#define EEP_READ_BYTE    1       // cteni jednoho bytu
#define EEP_WRITE_BYTE   1       // zapis jednoho bytu
#define EEP_USE_INS      1       // Zda se vyuziva signal INS



//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu s AT25640 / X25645
//-----------------------------------------------------------------------------
// Tady je taky zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

#define XmemCS     AutoCSMOD     // chipselect /CS
#define XmemSCK    AutoCLKMOD    // hodiny SCK
#define XmemSO     AutoSOMOD     // vystup dat SO
#define XmemSI     AutoSIMOD     // vstup dat SI (muze byt shodny s SO)
//#define XmemInsert !AutoINSMOD   // signal pritomnosti modulu aktivni v 0
#define XmemInsert 1             // Modul stale vlozen - INS uz neobsluhuju, jen na zacatku kopirovani otestuju pritomnost

// ochrana proti vybiti aku :

#define XmemAccuOk()       AutoAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :
#define XMEM_PAGE_SIZE    64      // velikost stranky
#define XMEM_SIZE      32768      // kapacita externiho pametoveho modulu

// Inverze signalu:
#define XMEM_XSI_H  1
#define XMEM_XSCK_H 1

// Podminena kompilace :

//#define XMEM_READ_BYTE    1       // cteni jednoho bytu
//#define XMEM_WRITE_BYTE   1       // zapis jednoho bytu
//#define XMEM_USE_INS      1       // Zda se vyuziva signal INS

//-----------------------------------------------------------------------------
// Pripojeni 1-Wire sbernic
//-----------------------------------------------------------------------------


#define MwiCS    AutoCSCIDLA      // chipselect Latche
#define MwiDATA  AutoDATA         // datova sbernice

#define MwiDQI0  AutoT1           // T1 Data Input Channel 0
#define MwiDQI1  AutoT2           // T2 Data Input Channel 1
#define MwiDQI2  AutoT3           // T3 Data Input Channel 2
#define MwiDQI3  AutoT4           // T4 Data Input Channel 3
#define MwiDQI4  AutoT5           // T5 Data Input Channel 4
#define MwiDQI5  AutoT6           // T6 Data Input Channel 5 - rezerva

#define MwiDQO0  AutoTOUT         // Data Output Channel 0
#define MwiDQO1  AutoTOUT         // Data Output Channel 1
#define MwiDQO2  AutoTOUT         // Data Output Channel 2
#define MwiDQO3  AutoTOUT         // Data Output Channel 3
#define MwiDQO4  AutoTOUT         // Data Output Channel 4
#define MwiDQO5  AutoTOUT         // Data Output Channel 5 - rezerva

//#define MwiD7    AutoTGND         // Data Input/Output - rezerva

// logicke urovne na vystupu :

#define DQO_LO    1               // je pres tranzistor, inverzni logika
#define DQO_HI    0

// casove prodlevy :

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

// Celkove zpozdeni cyklu for 2 + 4 * n

#define MWI_DELAY_RESET          MwiMicroDelay( 122)  // Zpozdeni 480 us pro reset
#define MWI_DELAY_PRESENCE       MwiMicroDelay( 17)   // Zpozdeni 70us mezi reset a presence
#define MWI_DELAY_RESET_TIMESLOT MwiMicroDelay( 104)  // Zpozdeni 410us mezi presence a dalsim
#define MWI_DELAY_START          _nop_();_nop_();_nop_()  // minimalni delka startbitu > 1us
#define MWI_DELAY_READ           11                   // Zpozdeni 15us v cyklu for
#define MWI_DELAY_READ_SLOT      MwiMicroDelay( 24)   // Zpozdeni 100us
#define MWI_DELAY_WRITE          MwiMicroDelay( 27)   // Zpozdeni 110us



// zakaz a povoleni interruptu, pouzito pro kriticky casovane sekce :

#define MwiDisableInts()         EA = 0
#define MwiEnableInts()          EA = 1

// identifikace kanalu a zaroven podmineny preklad :

#define MWI_CH0                  0                    // preklada se kanal 0
#define MWI_CH1                  1                    // preklada se kanal 1
#define MWI_CH2                  2                    // preklada se kanal 2
#define MWI_CH3                  3                    // preklada se kanal 3
#define MWI_CH4                  4                    // preklada se kanal 4
#define MWI_CH5                  5                    // preklada se kanal 5 - pridano, vyfukovana teplota

// podmineny preklad narocnych funkci :

#define MWI_CRC8                 1                    // preklada se kod pro vypocet CRC

//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------

#define IicSCL  AutoSCL         // I2C hodiny
#define IicSDA  AutoSI          // I2C data

// casove prodlevy

#define IIC_WAIT   {byte i; for( i = 6; i > 0; i--);}  // 4takty + doplnit NOPy na 5us

// podmineny preklad :

#define IIC_READ    1           // cteni dat se sbernice


//-----------------------------------------------------------------------------
// Pripojeni RTC RX-8025 pres I2C sbernici
//-----------------------------------------------------------------------------

// podmineny preklad :

#define RTC_USE_DATE    1       // cti/nastavuj datum
#define RTC_USE_WDAY    1       // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1       // hlidej zadavane hodnoty na rozsah
#define RTC_USE_ALARM   1       // pouzivej alarm/budik

//-----------------------------------------------------------------------------
// Teplomery DS18B20
//-----------------------------------------------------------------------------

#define TEMP_DS18B20       1      // definice typu DS18B20 12 bitovy

#define TEMP_SECURE_READ   1      // bezpecne cteni (vcetne CRC) (musi byt definovan MWI_CRC8)

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

#define _THERMO_LAST       (MWI_CH5 + 1)  // celkovy pocet teplomeru
#define THERMO_TRIALS      3              // pocet pokusu na cteni z jednoho teplomeru


//-----------------------------------------------------------------------------
// Digitalni vstupy/vystupy
//-----------------------------------------------------------------------------

// digitalni vstupy :

#define AdioCS            AutoCSVST                   // chipselect vstupniho latche
#define AdioBUS           AutoDATA                    // datova sbernice

#define AdioBatteryLow    AutoLOWBAT                  // nizke napajeni

// digitalni vystupy :
#define AdioAlarmSwitch        AutoVYST1              // Vystup spinani majaku u navesu pri alarmu

//-----------------------------------------------------------------------------
// Povoleni prekladu Auto.h
//-----------------------------------------------------------------------------

#define AUTO_TEMPERATURE    1             // preklada se teplomer
#define AUTO_SERVO          1             // preklada se servo
#define AUTO_ACCU           1             // preklada se aku
#define AUTO_FAN            1             // preklada se ventilator
#define AUTO_DIO            1             // prekladaji se digitalni vstupy/vystupy
#define AUTO_LOG            1             // preklada se logger
//#define AUTO_PRT            1             // preklada se protokolovani
#define AUTO_REG            1             // preklada se regulator
#define AUTO_DIESEL         1             // preklada se diesel
#define AUTO_CONFIG         1             // preklada se konfigurace
#define AUTO_FRESH_AIR      1             // preklada se fresh air


//-----------------------------------------------------------------------------
// Parametry FIFO - datovy logger
//-----------------------------------------------------------------------------

struct SAutoLog;                       // dopredna deklarace

typedef struct SAutoLog TFifoData;     // Typ ukladane struktury (max 255 bytu)
#define FifoData        AutoLog        // Globalni buffer ukladanych dat

#define FIFO_START            AUTO_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define FIFO_CAPACITY         AUTO_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define FIFO_MARKER_EMPTY     AUTO_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define FIFO_MARKER_FULL      AUTO_FIFO_MARKER_FULL    // znacka konce pri prepisovani

#define FIFO_INVALID_ADDRESS 0xFFFF                    // neplatna adresa do FIFO - MUSI korespondovat s XFIFO_INVALID_ADDRESS ve fifo.tpl!!!!!

// Podminena kompilace :
#define FIFO_FAST            1   // Zapamatovani pozice markeru
#define FIFO_VERIFY          1   // Verifikace po zapisu dat
#define FIFO_READ            1   // Cteni obsahu po polozkach
//#define FIFO_FIND_FIRST      1   // Nalezeni adresy prvniho zaznamu
//#define FIFO_FIND_LAST       1   // Nalezeni adresy posledniho zaznamu

/*//-----------------------------------------------------------------------------
// Parametry FIFO2 - protokolovani
//-----------------------------------------------------------------------------

struct SAutoUdalost;                   // dopredna deklarace

typedef struct SAutoUdalost TFifo2Data;        // Typ ukladane struktury (max 255 bytu)
#define Fifo2Data           AutoUdalost        // Globalni buffer ukladanych dat

#define FIFO2_START            APRT_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define FIFO2_CAPACITY         APRT_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define FIFO2_MARKER_EMPTY     APRT_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define FIFO2_MARKER_FULL      APRT_FIFO_MARKER_FULL    // znacka konce pri prepisovani

// Podminena kompilace :
#define FIFO2_FAST            1   // Zapamatovani pozice markeru
#define FIFO2_VERIFY          1   // Verifikace po zapisu dat
//#define FIFO_READ            1   // Cteni obsahu po polozkach


//-----------------------------------------------------------------------------
// Parametry IFIFO - protokolovani
//-----------------------------------------------------------------------------

struct SAutoUdalost;                   // dopredna deklarace

typedef struct SAutoUdalost TIfifoData;        // Typ ukladane struktury (max 255 bytu)
#define IfifoData           AutoUdalost        // Globalni buffer ukladanych dat

#define IFIFO_START            APRT_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define IFIFO_CAPACITY         APRT_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define IFIFO_MARKER_EMPTY     APRT_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define IFIFO_MARKER_FULL      APRT_FIFO_MARKER_FULL    // znacka konce pri prepisovani

// Podminena kompilace :
#define IFIFO_FAST            1   // Zapamatovani pozice markeru
#define IFIFO_VERIFY          1   // Verifikace po zapisu dat
//#define IFIFO_READ            1   // Cteni obsahu po polozkach*/


//-----------------------------------------------------------------------------
// Pripojeni displeje
//-----------------------------------------------------------------------------

#define DisplayCS   AutoCSD         // /CS vyber cipu
#define DisplayA0   AutoSO          // prikaz/data nekdy oznaceny jako CD
#define DisplayRD   AutoSI          // cteni /RD
#define DisplayWR   AutoCLK         // zapis /WR
#define DisplayRES  AutoRES         // reset /RES

#define DisplayData AutoDATA          // datova sbernice

// makra nezavisla na zapojeni pinu (volne k pouziti) :

#define DisplayEnable()  DisplayA0 = 0; DisplayWR = 1; DisplayRD = 1; DisplayCS = 0
#define DisplayDisable() DisplayCS = 1

#define DisplayAddressCommand() (DisplayA0 = 1)
#define DisplayAddressData()    (DisplayA0 = 0)

#define DisplayReset() (DisplayRES = 1)    // /RES je pres invertor
#define DisplayRun()   (DisplayRES = 0)

// void   DisplayWriteData( byte_t d);
// zapis dat do displeje

#define DisplayWriteData( d) {DisplayWR = 0; DisplayData = d; DisplayWR = 1;}

// byte DisplayReadData( void);
// cteni dat z displeje (v modulu Hardware.c)

// Pracovni mod displeje :

#define DISPLAY_MODE_MIXED 1            // graficky mod displeje - smiseny

// Preklad funkci :

// #define DISPLAY_MOVE_TO  1
// #define DISPLAY_CLEAR    1
// #define DISPLAY_PLANE    1
// #define DISPLAY_FILL_BOX 1
#define DISPLAY_PATTERN  1
#define DISPLAY_GOTO_RC  1
#define DISPLAY_PUTCHAR  1

// zatim jen MIXED :
// #define DISPLAY_CURSOR   1
// #define DISPLAY_CLR_EOL  1

//-----------------------------------------------------------------------------
// Pripojeni pomocne klavesnice
//-----------------------------------------------------------------------------

#define KitKbdUp    AutoSOMOD     // Klavesa Up
#define KitKbdEsc   AutoCLKMOD    // Klavesa Esc
#define KitKbdEnter AutoCSMOD     // Klavesa Enter
#define KitKbdDown1 KitKbdEnter   // Sipka dolu zapojena mezi
#define KitKbdDown2 KitKbdEsc     // Sipka dolu zapojena mezi

// definice klaves :
typedef enum {
   // systemove klavesy :
   K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
   _K_FIRSTUSER,

   // klavesy, v zavislosti na konstrukci klavesnice :
   K_UP = _K_FIRSTUSER,       // sipka nahoru
   K_DOWN,                    // sipka dolu
   K_ENTER,                   // Enter
   K_ESC,                     // Esc

  _K_EVENTS    = 0x40,        // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,   // prekresleni namerenych hodnot
  K_BLINK_OFF,                // periodicke blikani - zhasni
  K_BLINK_ON,                 // periodicke blikani - rozsvit
  K_TIMEOUT,                  // vyprsel cas necinnosti

   // systemove klavesy :
   K_REPEAT       = 0x80,     // opakovani klavesy OR ke klavese
   K_RELEASED     = 0xFE,     // pusteni klavesy (jednotlive i autorepeat)
   K_IDLE         = 0xFF      // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (300/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

#define RS232Power      AutoRD        // Port napajeni budice
#define RS232_BAUD      9600          // Rychlost linky v baudech
//#define RS232_USE_PARITY    1         // Provoz s paritou nebo bez

//#define RS232_PARITY_EVEN   1         // Suda parita
//#define RS232_PARITY_ODD   1        // Licha parita

//#define RS232_CASOVAC_1    1        // K casovani linky se vyuzije casovac 1
#define RS232_CASOVAC_2    1          // K casovani linky se vyuzije casovac 2 (pouzitelne jen u C51RD2, ne u maleho Atmela)

//#define RS232_GET           1         // Zda se pouziva cteni ze seriove linky
#define RS232_SEND          1         // Zda se pouziva zapis na seriovou linku



//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

//-----------------------------------------------------------------------------

// Casovani

void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) Delay( ms)             // zpozdeni 1ms z modulu Main

// Zpozdeni <us> * 1us :

#define uDelay( us)  \
                 {byte __count; __count = ((us) * 3) / 2; while( --__count);}

//-----------------------------------------------------------------------------

#endif // __Hardware_H__
