//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;


#define CHAR_VALUES_COUNT       48

unsigned char CharValues[CHAR_VALUES_COUNT] = {
225,
226,
227,
228,
230,
231,
232,
233,
234,
235,
236,
237,
239,
242,
243,
244,
246,
247,
248,
249,
250,
251,
252,
253,
254,
255,
193,
195,
196,
198,
199,
200,
201,
202,
203,
207,
211,
212,
214,
215,
216,
217,
218,
219,
220,
221,
222,
223
};

static int FindCharInArray(unsigned char Char) {
  // Vrati index znaku <Char> v poli <CharValues> nebo -1 pokud nenajde.
  for (int i = 0; i < CHAR_VALUES_COUNT; i++) {
    if (CharValues[i] == Char) {
      return i;
    }
  }
  return -1;
}

#define RUSSIAN_BASE    0xAD            // Prvni rusky znak ve fontu Bat2

static AnsiString ConvertChar(unsigned char Char) {
  int Index;
  char s[2];

  Index = FindCharInArray(Char);
  if (Index < 0) {
    s[0] = Char;
    s[1] = 0;
    return AnsiString(s);               // Jde o normalni ASCII znak
  }

  AnsiString Str = "\\x0";
  Str += IntToHex(RUSSIAN_BASE + Index, 2);
  return Str;
}

static AnsiString ParseString(AnsiString Text) {
  AnsiString Str = "";

  for (int i = 1; i <= Text.Length(); i++) {
    Str += ConvertChar(Text[i]);
  }

  return Str;
}



//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{


}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
  Edit2->Text = ParseString(Edit1->Text);
}
//---------------------------------------------------------------------------
