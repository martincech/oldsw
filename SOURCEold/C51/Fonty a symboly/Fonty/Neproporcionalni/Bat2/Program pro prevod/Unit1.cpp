//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
  if (OpenDialog1->Execute()) {
    Image1->Picture->LoadFromFile(OpenDialog1->FileName);
  }//if
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
  int VyskaZnaku=VyskaEdit->Text.ToInt();
  int SirkaZnaku;     // Sirka aktualniho znaku v pixelech
  bool NalezenCernyBod;  // Pro hledani sirky znaku
  int X,Y;
  int PocetBajtuNaZnak;   // Vypoctu z PocetBituVPoli - pro vsechny znaky stejne
  int PocetZnakuVObrazku;
  unsigned char Maska;
  unsigned char Pole[1000]; // Pole bajtu, ktere definuji znak
  int IndexVPoli;    // Poradovy index - abych vedel, kam ukladat
  int CisloZnaku=0;    // Poradove cislo znaku
  int ZapisovanySoubor;
  String Str;
  char s[10];

  ProgressBar1->Max=Image1->Picture->Bitmap->Height;

  PocetBajtuNaZnak=Image1->Picture->Bitmap->Width*VyskaZnaku/8;  // 8 bitu na bajt
  if ((Image1->Picture->Bitmap->Width*VyskaZnaku)%8>0) PocetBajtuNaZnak++;  // Zbytek po deleni
  PocetLabel->Caption=PocetBajtuNaZnak;

  PocetZnakuVObrazku=Image1->Picture->Bitmap->Height/VyskaZnaku;

  ZapisovanySoubor = FileCreate(SouborEdit->Text);

  // Ulozim hlavicku
  Str="unsigned char code FONT[" + String(PocetZnakuVObrazku) + "][" + String(PocetBajtuNaZnak) + "] = {";  // +1 kvuli sirce znaku
  FileWrite(ZapisovanySoubor, Str.c_str(), Str.Length());
  // CRLF
  s[0]=10; s[2]=0;
  FileWrite(ZapisovanySoubor, s, 1);

  SirkaZnaku=Image1->Picture->Bitmap->Width;

  for (int y=0; y<Image1->Picture->Bitmap->Height; y+=VyskaZnaku) {  // Jednotlive znaky

    // Zjistim sirku znaku - jedu zprava a hledam prvni cerny bod
/*    SirkaZnaku=Image1->Picture->Bitmap->Width;
    NalezenCernyBod=false;
    for (X=Image1->Picture->Bitmap->Width-1; X>=0 && !NalezenCernyBod; X--) {
      for (int Y=y; Y<y+VyskaZnaku && !NalezenCernyBod; Y++) {
        if (Image1->Picture->Bitmap->Canvas->Pixels[X][Y]==clBlack) NalezenCernyBod=true;
      }// for Y
      if (!NalezenCernyBod) SirkaZnaku--;   // V teto rade jsem nenasel cerny bod
    }// for X
    if (!NalezenCernyBod) SirkaZnaku=Image1->Picture->Bitmap->Width/3+1;   // Jde o mezeru
    // Ted mam nalezenou sirku aktualniho znaku v promenne SirkaZnaku*/

    IndexVPoli=0;
    for (int i=0; i<1000; i++) Pole[i]=0; // Vynuluju pole
    for (int Stranka=y; Stranka<y+VyskaZnaku; Stranka+=8) {  // 8 bitu na stranku
      for (X=0; X<SirkaZnaku; X++) {  // Jednotlive pixely v radku
        Maska=1;
        for (int Y=Stranka; Y<Stranka+8; Y++) {   // Jednotlive radky znaku
          if (Image1->Picture->Bitmap->Canvas->Pixels[X][Y]==clBlack) Pole[IndexVPoli]|=Maska;
          if (Maska<128) {
            // Posunu masku
            Maska*=2;
          } else {
            // Posunu se na dalsi bajt a jedu s maskou od zacatku
            Maska=1;
            IndexVPoli++;
          }//else
        }//for Y
      }//for X
    }//for Stranka  
    // Ted mam v Pole[] definici znaku, v IndexVPoli pocet bajtu v Pole[] a v SirkaZnaku sirku znaku

    // Muzu ulozit definici znaku - sirka znaku + bajty oddelene carkama
    // Sirka znaku
    // Sirka znaku je konstantni
//    Str=String(SirkaZnaku)+",";
//    FileWrite(ZapisovanySoubor, Str.c_str(), Str.Length());
    for (int i=0; i<PocetBajtuNaZnak; i++) {
      Str=String((int)Pole[i])+",";
      FileWrite(ZapisovanySoubor, Str.c_str(), Str.Length());
    }//for i


    // CRLF
    s[0]=10; s[2]=0;
    FileWrite(ZapisovanySoubor, s, 1);

    // Jdu na dalsi znak
    CisloZnaku++;
    ProgressBar1->Position=y;
  }//for y

  // Umazu posledni carku - ta je tam prebytecna
  FileSeek(ZapisovanySoubor, -2, 1);  // 2 bajty zpet (CRLF a carka)

  // Zavorku uzavrit
  Str="};";
  FileWrite(ZapisovanySoubor, Str.c_str(), Str.Length());

  FileClose(ZapisovanySoubor);
}
//---------------------------------------------------------------------------
