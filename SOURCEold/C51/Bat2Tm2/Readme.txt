         Testovani GSM Teltonika

1. Byl upraven modul COM (chybejici nastaveni priznaku TI)
   Je mozne zrusit funkci GsmInit a nahradit zpet ComInit

2. Adresar Bat2tst obsahuje testovani modemu. Je treba
   upravit telefonni cislo, kam bude posilat SMS a znovu
   prelozit

3. Obsluha programu Bat2tst :
   - sipka nahoru : inicializace
   - sipka dolu : cekani na registraci do site
   - Enter : cteni prijate SMS
   - sipka vlevo : smazani prectene zpravy
   - Esc : vyslani SMS

4. Program Bat2tm2 je programovani modemu
   pred pouzitim v Bat2


Pozn. Po vyslani SMS ceka cca 10s a potom ukaze
vsechny prijate znaky. Odpoved by mela obsahovat
jen text (priblizne) 

\r\n+CMGS : <neajke cislo>\r\n
\r\nOK\r\n

Netisknutelne znaky se zobrazuji jako otazniky.
Spravne by melo fungovat tak, ze po vyslani SMS
by za cca 2-3s mela prijit potvrzovaci odpoved.
Ve skutecnosti okamzite po odeslani prijde smeti,
potom potvrzeni a za nim dalsi smeti. Nejspis to
nejak souvisi s aktivitou vysilace modulu.


