//*****************************************************************************
//
//    Tm2.h          Teltonika Tm2 utility
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#ifndef __Tm2_H__
   #define __Tm2_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

TYesNo TmBaudDetection( void);
// Search & set baudrate

TYesNo TmEchoOff( void);
// Switch echo off

TYesNo TmFactoryDefault( void);
// Switch echo off

TYesNo TmSetBaud( word Baud);
// Set modem baud rate

TYesNo TmSimMemory( void);
// Set SMS memory to SIM card

TYesNo TmHandshakeOff( void);
// Set no handshake

TYesNo TmNoPowerSave( void);
// Disable power save mode

TYesNo TmSaveProfile( void);
// Save as profile 0

TYesNo TmSetProfile( void);
// Set active profile 0

TYesNo TmPowerOff( void);
// Switch power off

#endif
