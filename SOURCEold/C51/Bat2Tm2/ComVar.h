//*****************************************************************************
//
//    ComVar.h       RS232 variable baudrate
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#ifndef __ComVar_H__
   #define __ComVar_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define COM_115200Bd  0                // highest baudrate code

void ComVarInit( word Baud);
// Communication initialisation


#endif
