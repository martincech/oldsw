//*****************************************************************************
//
//    Logger.h - common data definitions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Mackadlo_H__
   #define __Mackadlo_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif



//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE    0x0100       // verze SW - POZOR, musi odpovidat promenne STR_CISLO_VERZE v main.c51



// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Pro promennou ModEditace - co se edituje
enum {
  EDITACE_NORMAL,
  EDITACE_PODSVIT
};

// Stavy
typedef enum {
  STAV_VAZENI,                    // Normalne se vazi
  STAV_PREPINAM_NA_AKU,           // Dosel mi impuls, ze mam zmerit aku, prepnul jsem vstup prevodniku na mereni aku a cekam, az se prepne
  STAV_CEKAM_NA_DALSI_PREVOD,     // Precetl jsem 1 prevod po prepnuti na aku a ted cekam na dalsi
  STAV_CEKAM_NA_PREVOD_AKU        // Vstup se prepnul na aku, poslal jsem prikaz ke cteni prevodu a cekam na platny prevod
} TStavMereni;

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_VSE 0xFF               // Zobrazi se vse (jsou nastaveny vsechny bity)
#define ZOBRAZIT_MRIZKA 0x01            // Zobrazi se mrizka (kostra) vazeni
#define ZOBRAZIT_AKU 0x02               // Zobrazi se stav aku
#define ZOBRAZIT_HMOTNOST 0x04          // Zobrazi se aktualni hmotnost
#define ZOBRAZIT_CAS 0x08               // Zobrazi se cas
#define ZOBRAZIT_VZOREK 0x10            // Zobrazi se posledni ulozeny vzorek a procenta zaplneni pameti

// Sipky ve fontu Myriad
#define SIPKA_NAHORU 12
#define SIPKA_DOLU   13

// Co mam vysilat po seriove lince
// Vsechny prikazy musi byt mezi POSLAT_NIC a POSLAT_POSLEDNI
enum {
  POSLAT_NIC=0,            // Seriova linka je v klidu
  POSLAT_DATA=1,
  POSLAT_NASTAVENI=2,
  POSLAT_JMENOVAH=3,
  POSLAT_VERZI=4,
  POSLAT_STAV_PAMETI=5,
  NACIST_NASTAVENI=6,
  NACIST_JMENOVAH=7,
  SMAZAT_DATA=8,
  POSLAT_POSLEDNI=9          // Index posledniho, uz neplatneho
};

// Rezim mackadla
typedef enum {
  REZIM_MACKANI,             // Standardni mackadlo, tj. hledani maxima po stisku tlacitka
  REZIM_MERENI               // Standardni vaha, tj. kontinualni mereniu sily
} TRezim;

// Stav mereni sily
typedef enum {
  SILA_MERENI_MAXIMA,      // Probiha mereni maxima sily, dokud kost nepraskne
  SILA_ZMERENO             // Maximum sily jsem zmeril, cekam na ulozeni
} TStavMereniSily;


#define STX 0x02           // Start of text


//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

#define MAX_DELKA_KOMENTARE 10          // Maximalni delka komentare k hmotnosti
#define MAX_DELKA_PREDPONY MAX_DELKA_KOMENTARE          // Maximalni delka predpony ke komentari
#define MAX_DELKA_IDENTIFIKACE 10       // Maximalni delka identifikace pristroje
#define MAX_POCET_CISLIC 5              // Maximalni pocet cislic v zobrazovane hmotnosti

// Konstanty typu podsvitu
enum {
  PODSVIT_VYPNUT,
  PODSVIT_ZAPNUT,
  PODSVIT_AUTO
};

// Konstanty jednotek
enum {
  JEDNOTKY_G,
  JEDNOTKY_KG,
  JEDNOTKY_T,
  JEDNOTKY_N
};

// Kalibrace vahy
typedef struct {
  // Kalibrace vahy
  long KalibraceNuly;          // Prevodnik je nebo bude 24 bitovy => musim long
  long KalibraceRozsahu;
  long Rozsah;                 // Rozsah taky long, buhvi co tam bude
  // Kalibrace aku
  long KalibraceNulyAku;
  long KalibraceRozsahuAku;
} TKalibrace;

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                        // VERZE

  unsigned char Identifikace[MAX_DELKA_IDENTIFIKACE];  // Identifikace pristroje

  unsigned char Predpona[MAX_DELKA_KOMENTARE];         // Predpona komentare

  unsigned char Podsvit;                // Typ podsvitu displeje (vypnut, zapnut, auto)
  unsigned char IntenzitaPodsvitu;    // Intenzita pro podsvit (0 az 9)

  // Zakladni parametry vahy
  unsigned char Jednotky;               // Viz enum
  unsigned long Nosnost;
  unsigned char Dilek;
  unsigned char PocetDesetin;
  unsigned char Filtr;

  unsigned char Rezim;                  // Rezim mackadla - viz enum TRezim

  byte KontrolniSuma;                // Kontrolni soucet
} TKonfigurace;

extern TKonfigurace __xdata__ Konfigurace; // Buffer konfigurace v externi RAM
extern TKalibrace __xdata__ Kalibrace;     // Buffer kalibrace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast
#define CFG_ALL                       0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERZE                     0x00000001L
#define CFG_IDENTIFIKACE              0x00000002L
#define CFG_PREDPONA                  0x00000004L
#define CFG_PODSVIT                   0x00000008L
#define CFG_INTENZITA_PODSVITU        0x00000010L
#define CFG_JEDNOTKY                  0x00000020L
#define CFG_NOSNOST                   0x00000040L
#define CFG_DILEK                     0x00000080L
#define CFG_POCET_DESETIN             0x00000100L
#define CFG_FILTR                     0x00000200L
#define CFG_REZIM                     0x00000400L





//-----------------------------------------------------------------------------
// Zaznamy
//-----------------------------------------------------------------------------

#define YEAR_OFFSET 2000                // Jako zaklad roku beru 2000

// Jeden zaznam, ktery ukladam do EEPROM
typedef struct SZaznam {
  // Komentar - ten musi byt jako prvni, protoze musim zaistit, aby se 1. bajt struktury nikdy nerovnal hodnote LIFO_MARKER,
  // tj. 0xFF. V textu byt 0xFF nemuze.
  unsigned char Komentar[MAX_DELKA_KOMENTARE];
  // Datum a cas
  unsigned char Hodina : 5;
  unsigned char Rok_lo : 3;       // Spodni 3 bity Staci 6 bitu, offset je 2000, rok tedy vypoctu jako 2000+Rok
  unsigned char Rok_hi : 3;       // Horni 3 bity
  unsigned char Den : 5;
  unsigned char Minuta : 6;
  unsigned char Mesic_lo : 2;     // Spodni 2 bity
  unsigned char Mesic_hi : 2;     // Horni 2 bity
  unsigned char Sekunda : 6;
  // Hmotnost
  long Hmotnost;
} TZaznam;     // Velikost 18 bajtu

// zapis roku do struktury :
#define mkyear(dt, h)     dt.Rok_lo = (h-YEAR_OFFSET) & 0x07; dt.Rok_hi = ((h-YEAR_OFFSET) >> 3) & 0x07
// cteni roku ze struktury :
#define getyear(dt)       YEAR_OFFSET + ((dt.Rok_lo) | (dt.Rok_hi << 3))
// zapis mesice do struktury :
#define mkmonth(dt, m)     dt.Mesic_lo = (m) & 0x03; dt.Mesic_hi = ((m) >> 2) & 0x03
// cteni mesice ze struktury :
#define getmonth(dt)       ((dt.Mesic_lo) | (dt.Mesic_hi << 2))

typedef TZaznam TZaznamy[LIFO_CAPACITY];

extern TZaznam __xdata__ Zaznam;    // Buffer zaznamu v externi RAM




//-----------------------------------------------------------------------------
// Struktura cele externi EEPROM
//-----------------------------------------------------------------------------
#define EEP_SPARE (EEP_SIZE - sizeof(TZaznamy))  // Velikost mezery na konci externi EEPROM

typedef struct {
  TZaznamy Zaznamy;                // Zaznamy
  unsigned char Volne[EEP_SPARE];
} TExtEEPROM;


//-----------------------------------------------------------------------------
// Struktura cele interni EEPROM
//-----------------------------------------------------------------------------
#define IEP_SPARE (IEP_SIZE - sizeof(TKonfigurace) - sizeof(TKalibrace))  // Velikost mezery na konci interni EEPROM
#define IEP_CFG_START   offsetof(TIntEEPROM, Konfigurace)    // Adresa zacatku konfigurace v EEPROM

typedef struct {
  TKalibrace Kalibrace;               // Kalibrace vseho - tu drzim zvlast. Schvalne ji mam hned na zacatku, aby se pri zmene delky cehokoliv neporusila
  TKonfigurace Konfigurace;           // Konfigurace
  unsigned char Volne[IEP_SPARE];
} TIntEEPROM;





//-----------------------------------------------------------------------------
// Struktura pro online editaci komentare pri vazeni
//-----------------------------------------------------------------------------
typedef struct {
  unsigned char Index;      // Index znaku, ktery se prave edituje
  unsigned char Text[MAX_DELKA_KOMENTARE];  // Editovany text
} TKomentar;




//-----------------------------------------------------------------------------
#endif
