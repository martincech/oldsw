//*****************************************************************************
//
//    Hardware.h  - Auto final hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE

//#define __ATTY__       1        // prekladat vysilac terminalu




//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define AutoAccuOk() AdioBatteryOk()

// spolecne vodice (podle schematu) :

#define AutoDATA      P0           // datova sbernice
// teplomery na sbernici :
sbit AutoT1      = P1^0;     // teplomer T1
sbit AutoT2      = P1^1;     // teplomer T2
sbit AutoT3      = P1^2;     // teplomer T3
sbit AutoT4      = P1^3;     // teplomer T4
sbit AutoT5      = P1^4;     // teplomer T5
sbit AutoT6      = P1^5;     // teplomer T6
sbit AutoTOUT    = P2^0;     // teplomery spolecne buzeni

sbit AutoPRINT   = P0^2;     // Odblokovani tiskarny
sbit AutoLOWBAT  = P3^5;     // Hlidani napajeni

sbit    AutoPODSVIT = P2^3;        // Podsvit displeje


// Displej
sbit AutoDisplayCS  =  P2^7;         // /CS vyber cipu
sbit AutoDisplayA0  =  P2^6;         // prikaz/data nekdy oznaceny jako CD
//sbit AutoDisplayRD  =  P3^7;         // cteni /RD
sbit AutoDisplayWR  =  P3^6;         // zapis /WR
sbit AutoDisplayRES =  P2^2;         // reset /RES
#define AutoDisplayData AutoDATA      // datova sbernice

// Touch Screen ADS 7846
sbit AutoTouchDCLK   =P0^4;           // hodiny DCLK
sbit AutoTouchDIN    =P0^5;           // vstup dat DIN
sbit AutoTouchDOUT   =P0^6;           // vystup dat DOUT
sbit AutoTouchPENIRQ =P3^2;           // preruseni od dotyku /PENIRQ
sbit AutoTouchCS     =P3^3;           // chipselect /CS - pokud se vyzuvia

// E2pot X9313
sbit E2PotCS    = P1^6;    // Chip select
sbit E2PotUD    = P0^0;    // Smer Up/Down
sbit E2PotINC   = P0^1;    // Provedeni 1 kroku

// Piskani
sbit AutoPISK =  P2^4;     // repro




//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz

#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
// Pozor, pokud se pouziva dynamicke rizeni CPU_X2, vsechny ostatni zarizeni musi byt v X1 modu (T0_X2, T1_X2 atd. musi byt nastaveny na 1). Pokud
// jsou ostatni zarizeni nastavene v X2 modu, po prepnuti procesoru do X1 modu se prepnou soucasne i zarizeni. Napr. SPI je stale v X2 a prepina se do X1.
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2 (zatim neni implementovano, bezi stale v X2 na max. rychlost)

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :

#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost (1=ne, 2=ano)

// Casovac 0 :

#define TIMER0_PERIOD 20        // perioda casovace 0 v ms
#define TIMER1_PERIOD 1        // perioda casovace 1 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
#include "Tm1HW.h"             // projektove nezavisle definice
//-----------------------------------------------------------------------------


// Casovani

void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) Delay( ms)             // zpozdeni 1ms z modulu Main

//-----------------------------------------------------------------------------

#endif // __Hardware_H__
