//*****************************************************************************
//
//    AutoHW.h  - Auto independent hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Tm1HW_H__
   #define __Tm1HW_H__

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut AutoPISK         // vystup generatoru

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1        // asynchronni pipani


//-----------------------------------------------------------------------------
// Pripojeni 1-Wire sbernic
//-----------------------------------------------------------------------------


//#define MwiCS    AutoCSCIDLA      // chipselect Latche
#define MwiDATA  P1         // datova sbernice

#define MwiDQI0  AutoT1           // T1 Data Input Channel 0
#define MwiDQI1  AutoT2           // T2 Data Input Channel 1
#define MwiDQI2  AutoT3           // T3 Data Input Channel 2
#define MwiDQI3  AutoT4           // T4 Data Input Channel 3
#define MwiDQI4  AutoT5           // T5 Data Input Channel 4
#define MwiDQI5  AutoT6           // T6 Data Input Channel 5 - rezerva

#define MwiDQO0  AutoTOUT         // Data Output Channel 0
#define MwiDQO1  AutoTOUT         // Data Output Channel 1
#define MwiDQO2  AutoTOUT         // Data Output Channel 2
#define MwiDQO3  AutoTOUT         // Data Output Channel 3
#define MwiDQO4  AutoTOUT         // Data Output Channel 4
#define MwiDQO5  AutoTOUT         // Data Output Channel 5 - rezerva

//#define MwiD7    AutoTGND         // Data Input/Output - rezerva

// logicke urovne na vystupu :

//#define DQO_LO    1               // je pres tranzistor, inverzni logika
//#define DQO_HI    0
#define DQO_LO    0               // je pres hradlo a jeste tranzistor, normalni logika
#define DQO_HI    1

// casove prodlevy :

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

// Celkove zpozdeni cyklu for 2 + 4 * n

#define MWI_DELAY_RESET          MwiMicroDelay( 122)  // Zpozdeni 480 us pro reset
#define MWI_DELAY_PRESENCE       MwiMicroDelay( 17)   // Zpozdeni 70us mezi reset a presence
#define MWI_DELAY_RESET_TIMESLOT MwiMicroDelay( 104)  // Zpozdeni 410us mezi presence a dalsim
#define MWI_DELAY_START          _nop_();_nop_();_nop_()  // minimalni delka startbitu > 1us
#define MWI_DELAY_READ           11                   // Zpozdeni 15us v cyklu for
#define MWI_DELAY_READ_SLOT      MwiMicroDelay( 24)   // Zpozdeni 100us
#define MWI_DELAY_WRITE          MwiMicroDelay( 27)   // Zpozdeni 110us



// zakaz a povoleni interruptu, pouzito pro kriticky casovane sekce :

#define MwiDisableInts()         EA = 0
#define MwiEnableInts()          EA = 1

// identifikace kanalu a zaroven podmineny preklad :

#define MWI_CH0                  0                    // preklada se kanal 0
#define MWI_CH1                  1                    // preklada se kanal 1
#define MWI_CH2                  2                    // preklada se kanal 2
#define MWI_CH3                  3                    // preklada se kanal 3
#define MWI_CH4                  4                    // preklada se kanal 4
#define MWI_CH5                  5                    // preklada se kanal 5 - pridano, vyfukovana teplota

// podmineny preklad narocnych funkci :

#define MWI_CRC8                 1                    // preklada se kod pro vypocet CRC



//-----------------------------------------------------------------------------
// Teplomery DS18B20
//-----------------------------------------------------------------------------

#define TEMP_DS18B20       1      // definice typu DS18B20 12 bitovy
//#define TEMP_DS18S20         1      // definice typu DS18S20 9 bitovy

#define TEMP_SECURE_READ   1      // bezpecne cteni (vcetne CRC) (musi byt definovan MWI_CRC8)

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

#define _THERMO_LAST       (MWI_CH5 + 1)  // celkovy pocet teplomeru
#define THERMO_TRIALS      3              // pocet pokusu na cteni z jednoho teplomeru


//-----------------------------------------------------------------------------
// Digitalni vstupy/vystupy
//-----------------------------------------------------------------------------

// digitalni vstupy :
#define AdioBatteryLow    AutoLOWBAT                  // nizke napajeni

//-----------------------------------------------------------------------------
// Povoleni prekladu Auto.h
//-----------------------------------------------------------------------------

#define AUTO_TEMPERATURE    1             // preklada se teplomer
#define AUTO_SERVO          1             // preklada se servo
#define AUTO_ACCU           1             // preklada se aku
#define AUTO_FAN            1             // preklada se ventilator
#define AUTO_DIO            1             // prekladaji se digitalni vstupy/vystupy
#define AUTO_LOG            1             // preklada se logger
//#define AUTO_PRT            1             // preklada se protokolovani
#define AUTO_REG            1             // preklada se regulator
#define AUTO_DIESEL         1             // preklada se diesel
#define AUTO_CONFIG         1             // preklada se konfigurace
#define AUTO_FRESH_AIR      1             // preklada se fresh air






//-----------------------------------------------------------------------------
// Pripojeni displeje
//-----------------------------------------------------------------------------

#define DisplayCS   AutoDisplayCS         // /CS vyber cipu
#define DisplayA0   AutoDisplayA0          // prikaz/data nekdy oznaceny jako CD
#define DisplayRD   AutoDisplayRD          // cteni /RD
#define DisplayWR   AutoDisplayWR         // zapis /WR
#define DisplayRES  AutoDisplayRES         // reset /RES

#define DisplayData AutoDisplayData          // datova sbernice

// makra nezavisla na zapojeni pinu (volne k pouziti) :

#define DisplayEnable()  DisplayWR = 1; DisplayRD = 1; DisplayCS = 0
#define DisplayDisable() DisplayCS = 1

#define DisplayAddressCommand() (DisplayA0 = 1)
#define DisplayAddressData()    (DisplayA0 = 0)

#define DisplayReset() (DisplayRES = 1)  // Je to pres hradlo
#define DisplayRun()   (DisplayRES = 0)

// void   DisplayWriteData( byte_t d);
// zapis dat do displeje

#define DisplayWriteData( d) {DisplayWR = 0; DisplayData = d; DisplayWR = 1;}

// byte DisplayReadData( void);
// cteni dat z displeje (v modulu Hardware.c)

// Pracovni mod displeje :

#define DISPLAY_MODE_GRAPHICS2 1            // graficky mod displeje - 2 roviny

//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------



// definice klaves :
typedef enum {
   // systemove klavesy :
   K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
   _K_FIRSTUSER,

   K_TOUCH,                   // aktivni touchpad

   K_IDLE         = 0xFF    // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};








//-----------------------------------------------------------------------------
// Pripojeni Touch Screen ADS 7846
//-----------------------------------------------------------------------------

#define TouchDCLK   AutoTouchDCLK            // hodiny DCLK
#define TouchDIN    AutoTouchDIN               // vstup dat DIN
#define TouchDOUT   AutoTouchDOUT             // vystup dat DOUT
#define TouchPENIRQ AutoTouchPENIRQ              // preruseni od dotyku /PENIRQ

// chipselect :
#define TouchCS           AutoTouchCS          // chipselect /CS
#define TouchSelect()     TouchCS = 0
#define TouchDeselect()   TouchCS = 1

// trvale zadratovany chipselect :
//#define TouchSelect()
//#define TouchDeselect()

// technicke konstanty :

#define TOUCH_DELAY       4        // doba ustaleni udaje  [ms]
#define TOUCH_PEN_RISE    5        // doba po nabehu PENIRQ [ms]
#define TOUCH_PEN_FALL   15        // doba odpadeni PENIRQ [ms]
#define TOUCH_DATA_SIZE  12        // 12 bitu A/D
//#define TOUCH_DATA_SIZE   8        //  8 bitu A/D

#define TOUCH_REPEAT_COUNT 2       // Pocet opakovani cteni souradnic
#define TOUCH_REPEAT_DELAY 5       // Cekani v ms mezi jednotlivymi ctenimi souradnic
#define TOUCH_REPEAT_RANGE 30      // Maximalni odchylka souradnic v absolutnich jednotkach pri opakovanem cteni

#define TOUCH_AUTOREPEAT_START (700/TIMER0_PERIOD)  // prodleva prvniho autorepeat
#define TOUCH_AUTOREPEAT_SPEED (300/TIMER0_PERIOD)  // kadence autorepeat

// Rozsahy souradnic :

#define TOUCH_X_RANGE     320      // rozsah souradnice X : 0..<X-1>
#define TOUCH_Y_RANGE     240      // rozsah souradnice Y : 0..<Y-1>
#define TOUCH_XL        0x100      // surovy levy okraj
#define TOUCH_XR        0xEF0      // surovy pravy okraj
#define TOUCH_YU        0xED0      // surovy horni okraj
#define TOUCH_YD        0x180      // surovy dolni okraj
/* pro 8 bitu
#define TOUCH_XL         0x10      // surovy levy okraj
#define TOUCH_XR         0xEF      // surovy pravy okraj
#define TOUCH_YU         0xED      // surovy horni okraj
#define TOUCH_YD         0x18      // surovy dolni okraj
*/

// Pomineny preklad :

//#define TOUCH_READ_VBAT   1        // cteni vstupu Vbat
//#define TOUCH_READ_AUX    1        // cteni vstupu AUX



//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __Auto_H__
  #include "Tm1.h"
#endif
//-----------------------------------------------------------------------------

#endif
