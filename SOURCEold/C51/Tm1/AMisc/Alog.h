//*****************************************************************************
//
//    Alog.h - Auto data & message logger
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Alog_H__
   #define __Alog_H__

#ifndef __Tm1_H__
   #include "Tm1.h"     // konstanty a datove typy
#endif


//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

void AlogTemperature( byte address, int value);
// Mereni teploty cislo <address>, hodnota <value>
// ma horni byte v celych stupnich,
// dolni MSB 0.5C...0.0625C 0 0 0 0 LSB
// nebo ALOG_TEMPERATURE_ERROR je-li cidlo v poruse

void AlogBatteryOk( bit status);
// Napeti akumulatoru. V poradku <status> = YES.


#endif

