//*****************************************************************************
//
//    Adio.c - Auto digital input/output module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __Adio_H__
   #define __Adio_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif




void AdioInit();


TYesNo AdioBatteryOk(void);
// Precte stav baterie. Vraci YES je-li napeti v norme.
// Lze volat kdykoliv


#endif
