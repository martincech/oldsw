//*****************************************************************************
//
//    Auto.h - Auto common data definitions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Tm1_H__
   #define __Tm1_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
//   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
   #include <stddef.h>        // makro offsetof
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define AMONITOR_VERZE    0x0100       // verze SW

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define MAX_PODSVIT 4
#define MIN_PODSVIT 1
extern byte Podsvit;                    // Definovano v main

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

// identifikace teplomeru :
// Pozor, musi pozicne odpovidat konstantam MWI_CH0..MWI_CH4
// 6.3.2003: Musi take zacinat od nuly, protoze je pouzivam jako indexy v poli pro prumerovani teplot
typedef enum {
   ATEMP_PREDNI,         // T1
   ATEMP_STREDNI,        // T2
   ATEMP_ZADNI,          // T3
   ATEMP_KANAL,          // T4
   ATEMP_VENKOVNI,       // T5
   ATEMP_VYFUKOVANA      // T6
} TThermoAddress;

#define ATEMP_COUNT 6    // Celkovy pocet merenych teplot - kvuli poli pro prumerovani

#define AUTO_TEMP_ERROR   (-100<<8)  // chyba mereni -100C misto merene hodnoty

// Teplota : horni byte je v celych stupnich,
// dolni MSB 0.5C...0.0625C 0 0 0 0 LSB
// Na float se prevede jako temp/256


//-----------------------------------------------------------------------------
// Include potrebnych modulu, povoluje se v Hardware.h
//-----------------------------------------------------------------------------

#ifdef AUTO_TEMPERATURE
#ifndef __Thermo_H__
   #include "..\inc\Thermo.h"
#endif
#endif // AUTO_TEMPERATURE
#ifdef AUTO_DIO
#ifndef __Adio_H__
   #include "Amisc\Adio.h"
#endif
#endif // AUTO_DIO
#ifdef AUTO_LOG
#ifndef __Alog_H__
   #include "Amisc\Alog.h"
#endif
#endif // AUTO_LOG

//-----------------------------------------------------------------------------
// Globalni komunikacni struktura
//-----------------------------------------------------------------------------

// Komunikacni struktura pro zobrazeni/logovani :
typedef struct {
   // merene teploty :
   int16  TeplotaVenkovni;
   int16  TeplotaKanal;
   int16  TeplotaPredni;
   int16  TeplotaStredni;
   int16  TeplotaZadni;
   int16  TeplotaVyfukovana;
   // stavove informace :
   byte PoruchaNapajeni;          // YES/NO (Battery Low, jen pro EEPROM)
} TAutoData;

extern TAutoData __xdata__ AutoData;      // komunikacni struktura v externi RAM (pro modul Fifo)





//-----------------------------------------------------------------------------
#endif // __Auto_H__
