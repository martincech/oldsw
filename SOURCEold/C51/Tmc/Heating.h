//*****************************************************************************
//
//    Heating.c - Ovladani topeni
//    Version 1.0
//
//*****************************************************************************


#ifndef __Heating_H__
   #define __Heating_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Struktura topeni
typedef struct {
#ifdef USE_ON_OFF_HEATING
  byte On;                  // Zapnute topeni, pouze u bufiku
#endif // USE_ON_OFF_HEATING
  byte Percent;             // Aktualni hodnota vykonu topeni v procentech
  byte Position;            // Poloha serva topeni pro logovani
  byte Failure;             // YES/NO porucha samotneho topeni
  byte ServoFailure;        // YES/NO porucha serva topeni
  byte Economy;             // YES/NO nizky vykon topeni
  byte Flame;               // YES/NO topeni prave hori
  int16 Error;              // Rozdil mezi cilovou a prumernou teplotou
  int32 Proportional;       // Proporcionalni slozka PI regulace
  int32 Integral;           // Integracni slozna PI regulace
  int16 LastTemperature;    // Teplota ve skrini z minuleho kroku
  int32 Derivative;         // Derivacni slozka PID regulace
} THeating;
extern THeating __xdata__ Heating;

#define HEATING_MAX         100         // Maximalni hodnota vykonu topeni
#define HEATING_MANUAL_STEP 10          // Prirustek v % v manualnim rezimu

// Konstanty PID regulatoru :
#define     REG_TS        10             // vzorkovaci perioda v sekundach
#define     REG_T_LSB     16             // velikost LSB : 1/REG_T_LSB C
#define     REG_Y_HI      100            // Maximum vystupu je 100%
#define     REG_Y_LO      0              // Minimum vystupu je 0%
#define     REG_MAX_TI    1000           // maximalni velikost Ti v sekundach
#define     REG_MAX_KP    1000           // maximalni velikost Kp
#define     REG_INT_SCALE (REG_MAX_TI / REG_TS)                // meritko zvetseni integracni slozky, pri max Ti LSB odchylky vyvola alespon integral += 1
#define     REG_INT_HI    ((long)REG_Y_HI * (long)REG_INT_SCALE * (long)REG_T_LSB)    // maximalni integracni hodnota
#define     REG_INT_LO    0              // minimalni integracni hodnota
#define     REG_PROP_SATURATION   (REG_T_LSB * REG_Y_HI)         // Hodnota saturace proporcionalni slozky (tj. 100%)
#define     REG_DER_HI    ((long) REG_Y_HI * (long)REG_T_LSB)    // maximalni integracni hodnota (+100%)
#define     REG_DER_LO    ((long)-REG_Y_HI * (long)REG_T_LSB)    // maximalni integracni hodnota (-100%)
#define     REG_DER_SCALE 16             // Derivacni slozka se pocita primo z teplot ve vnitrnim formatu (*256), kdezto proporcionalni i intergracni
                                         // slozka se pocita z odchylky, ktera je jen 16*T (na zacatku teto fce delam bitovy posuv err >>= 4).

void HeatingInit();
  // Inicializace

void HeatingStop();
  // Vypne topeni a vynuluje poruchu

void HeatingManualIncrease();
  // Pridani vykonu topeni v rucnim rezimu

void HeatingManualDecrease();
  // Ubrani vykonu topeni v rucnim rezimu

TYesNo HeatingFullyClosed();
  // Vrati YES, pokud je ventil topeni plne uzavren nebo je servo odpojeno/v chybe

void HeatingManualStart();
  // Zahaji topeni v rucnim rezimu

void HeatingManualExecute();
  // Provede krok v rezimu manualniho topeni, volat 1x za sekundu

void HeatingManualExecute();
  // Provede krok v rezimu auto topeni, volat 1x za sekundu

void HeatingAutoStart();
  // Zahaji topeni v automatickem rezimu

void HeatingAutoExecute(int Average);
  // Provede krok v rezimu auto topeni, volat 1x za sekundu. <Average> je prumerna teplota podle lozeni ve vnitrnim formatu

#endif
