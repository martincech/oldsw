//*****************************************************************************
//
//    Menu.c - TMC display module
//    Version 1.0
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#include "Hardware.h"
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Time.h"              // Datum a cas

// Co se ma zobrazit
#define REDRAW_GRID     0x01            // Smaze displej a vykresli mrizku
#define REDRAW_PERIODIC 0x02            // Prekresli periodicka data po 1sec (teploty, klapky, cas atd)
#define REDRAW_ALL      0xFF            // Prekresli cely displej
extern byte __xdata__ Redraw;

// Obsluha zapnuti diagnostickeho rezimu
#define MAX_DIAGNOSTIC_COUNTER 1        // Pocet sekund, po ktere je aktivni volba diagnostickeho rezimu
extern byte __xdata__ DiagnosticCounter;// Pocitadlo aktivity volby diagnostickeho rezimu
extern byte __xdata__ DiagnosticMode;   // YES/NO zobrazeni diagnostiky

// Podsvit
#define BRIGHTNESS_MAX 4
#define BRIGHTNESS_MIN 1

void MenuInit();
  // Inicializace zobrazeni, pred volanim uz musi byt nactena konfigurace

void MenuBlink(TYesNo On);
  // Zobrazi / schova blikajici rovinu

void MenuRedraw();
  // Prekresleni displeje

void MenuTouch();
  // Zpracuje stisk touch panelu v hlavnim zobrazeni

void MenuLogin();
  // Provede login ridice

void MenuNewTargetTemperature(char Temp);
  // Nastavi novou cilovou teplotu <Temp> v celych stupnich se znamenkem

void MenuNewLocalization(byte Localization);
  // Nastavi nove lozeni <Localization>

void MenuNewFreshAirMode(TFreshAirMode Mode);
  // Nastavi novy rezim cerstveho vzduchu

void MenuNewFloorFlapsMode(TFloorFlap Flap, TFlapMode Mode);
  // Nastvavi novy rezim prednich / zadnich klapek v podlaze

void MenuNewSound();
  // Zapne / vypne hlaseni alarmu

void MenuNewTime(TTransferTime data *NewTime);
  // Nastavi novy cas <NewTime>

void MenuNewLogin(byte Number);
  // Provede samotny login ridice <Number>

void MenuNewOffset(byte Above, byte Below);
  // Nastavi nove meze nad a pod cilovou teplotou
  // Znici UN.DT

#endif
