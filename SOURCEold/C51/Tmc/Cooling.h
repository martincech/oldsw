//*****************************************************************************
//
//    Cooling.c - Ovladani chlazeni
//    Version 1.0
//
//*****************************************************************************


#ifndef __Cooling_H__
   #define __Cooling_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Struktura chlazeni
typedef struct {
  byte Percent;                         // Nastavena hodnota vykonu chlazeni v procentech (jen v rucnim rezimu)
  byte On;                              // YES/NO prave sepnute chlazeni
  byte ManualOnTimer;                   // Pocitadlo casu sepnuti/rozepnuti v manualnim rezimu
  byte Failure;                         // YES/NO porucha chlazeni
} TCooling;
extern TCooling __xdata__ Cooling;

#define COOLING_MAX         100         // Maximalni hodnota vykonu topeni

void CoolingInit();
  // Inicializace

void CoolingManualIncrease();
  // Pridani vykonu chlazeni v rucnim rezimu

void CoolingManualDecrease();
  // Ubrani vykonu chlazeni v rucnim rezimu

void CoolingManualStart();
  // Start rucniho rezimu

void CoolingManualStart();
  // Start rucniho rezimu

void CoolingManualExecute();
  // Provede krok v rezimu manualniho chlazeni, volat 1x za sekundu

void CoolingStop();
  // Vypne chlazeni

void CoolingAutoExecute(int Average);
  // Krok automatickeho regulatoru chlazeni, volat 1x za sekundu. <Average> je prumerna teplota podle lozeni ve vnitrnim formatu


#endif
