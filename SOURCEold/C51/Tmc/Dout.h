//*****************************************************************************
//
//    Dout.c - Digitalni vystupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Dout_H__
   #define __Dout_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

// Definice jednotlivych vystupu
extern byte __xdata__ DoutCooling;
extern byte __xdata__ DoutHeating;
extern byte __xdata__ DoutVentilation;
extern byte __xdata__ DoutHeatingEconomy;
extern byte __xdata__ DoutBeacon;


void DoutInit();
  // Inicializuje digitalnich vystupu

void DoutSetCooling(TYesNo Value);
  // Zapne / vypne chlazeni

void DoutSetHeating(TYesNo Value);
  // Zapne / vypne topeni

void DoutSetVentilation(TYesNo Value);
  // Zapne / vypne ventilatory

void DoutSetHetingEconomy(TYesNo Value);
  // Zapne / vypne ekonomicky rezim topeni

void DoutSetBeacon(TYesNo Value);
  // Zapne / vypne majak

#endif
