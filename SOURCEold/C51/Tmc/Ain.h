//*****************************************************************************
//
//    Ain.c - Analogove vstupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Ain_H__
   #define __Ain_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#include "Tmc.h"                 // Indexy serv
#include "..\inc\Adc0838.h"      // AD converter

// Definice jednotlivych vstupu
extern byte __xdata__ AinServo[_SERVO_COUNT]; // Vystupy jednotlivych serv
extern byte __xdata__ AinDieselTemperature;   // Teplota dieselu
extern byte __xdata__ AinFan;                 // Napeti ventilatoru
extern byte __xdata__ AinCurrent;             // Bocnik (diferencialni)
extern byte __xdata__ AinVoltage;             // Napeti akumulatoru
//extern byte __xdata__ AinCurrentFans;         // Bocnik pro ventilatory naporu (diferencialni)


void AinInit();
  // Inicializuje analogovych vstupu

void AinRead();
  // Nacte vsechny analogove vstupy a ulozi je do lokalnich promennych

#endif
