//*****************************************************************************
//
//    Servo.c - Kontrola stavu serv
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Servo_H__
   #define __Servo_H__

#include "Tmc.h"                        // Definice serv
#include "Ain.h"               // Analogove vstupy

extern TServoStatus __xdata__ ServoStatus[_SERVO_COUNT];    // Stavy jednotlivych serv


// technologicke parametry serva :
// 21.3.2003: Serva se meri od 2V do 10V, ne od 1.5V
#define SERVO_ACCURACY  (ADC_RANGE * 30 / 100)     // presnost serva 30%, tj. 3V z 10V rozsahu
#define SERVO_RESPONSE   300                       // maximalni doba odezvy serva v sekundach
#define SERVO_OFF_LIMIT (ADC_RANGE * 15 / 100)     // vypnute servo, zpetne hlaseni < 1.5V
#define SERVO_MIN       (ADC_RANGE * 20 / 100)     // Zcela zavrene servo je pri 2V, 51 = 0x33
#define SERVO_MAX       (ADC_RANGE * 96 / 100)     // Zcela otevrene servo je pri 10V, realne zmereno 9.6V, 245 = 0xF5
// 18.4.2003: krok zmenen ze 13 na 12

void ServoInit( void);
// Inicializuje modul serv

void ServoResetTimer(byte Cislo);
// Vynuluje timer u serva cislo <Cislo> - pouziva se pri prepnuti rezimu

TYesNo ServoFullyClosed(byte Cislo);
  // Pokud je servo <Cislo> plne zavrene, vrati YES.

void ServoCheck( void);
// Kontroluje cinnost serva. Volat 1x za sekundu

#endif
