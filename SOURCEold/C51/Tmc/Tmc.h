//*****************************************************************************
//
//    Tmc.h - TMC definice
//    Version 1.0
//
//*****************************************************************************

#ifndef __Tmc_H__
   #define __Tmc_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#include "..\inc\uni.h"
#include "Hardware.h"     // pro podminenou kompilaci
#include "LogData.h"           // Struktura pro ukladani

#ifdef __C51__
// jen pro Keil
//   typedef int  int16;
//   typedef long long32;
//   typedef unsigned short int word;
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
   #include <stddef.h>        // makro offsetof
#endif

//-----------------------------------------------------------------------------
// Jazyk
//-----------------------------------------------------------------------------

// POZOR, po nastaveni jazyka zkontrolovat fonty v hardware.h, aby odpovidaly zvolenemu jazyku

#define LANGUAGE_CZECH 1
//#define LANGUAGE_POLISH 1
//#define LANGUAGE_ENGLISH 1
//#define LANGUAGE_RUSSIAN 1
//#define LANGUAGE_ROMANIAN 1
//#define LANGUAGE_GERMAN 1
//#define LANGUAGE_SWEDISH 1
//#define LANGUAGE_HUNGARIAN 1
//-----------------------------------------------------------------------------
// Rozsahy
//-----------------------------------------------------------------------------

#define MAX_TARGET_TEMPERATURE  35      // Maximalni cilova teplota ve skrini ve stupnich celsia.
#define MIN_TARGET_TEMPERATURE  5       // Minimalni cilova teplota ve skrini ve stupnich celsia.

#define MIN_LOGIN               1       // Minimalni cislo ridice
#define MAX_LOGIN               99      // Maximalni cislo ridice

#define MAX_OFFSET              99      // Maximalni odchylka nad/pod cilovou teplotou

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define TMC_VERSION  0x0126       // verze FW
//#define TMC_VERSION  0x0170       // verze FW - specialni pro Iveco BEST Opava, ktere nema pvbec podlahove klapky, nastavuje se pomoci USE_FLOOR_FLAPS
#define TMC_MAGIC    0x55AA       // identifikace konfiguracniho souboru

//-----------------------------------------------------------------------------
// Serva
//-----------------------------------------------------------------------------

// identifikace serv :
// Pozor, musi pozicne odpovidat konstantam v Ain.c a Servo.c
typedef enum {
   SERVO_FLOOR_FL,                      // Predni leve v podlaze
   SERVO_FLOOR_FM,                      // Predni uprostred v podlaze
   SERVO_FLOOR_FR,                      // Predni prave v podlaze
   SERVO_FLOOR_RL,                      // Zadni leve v podlaze
   SERVO_FLOOR_RM,                      // Zadni uprostred v podlaze
   SERVO_FLOOR_RR,                      // Zadni prave v podlaze
   SERVO_HEATING,                       // Topeni
   SERVO_INDUCTION1,                    // Napor 1
   SERVO_INDUCTION2,                    // Napor 2
   SERVO_RECIRCULATION,                 // Recirkulace
   _SERVO_COUNT
} TServoAddress;

// hlaseni stavu serv :
typedef enum {
   SERVO_STATUS_OK,              // v poradku, souhlasi pozadovana poloha a zpetne hlaseni
   SERVO_STATUS_RUNNING,         // nesouhlasi pozadovana poloha a zpetne hlaseni, zatim v casovem limitu
   SERVO_STATUS_TIMEOUT,         // nesouhlasi pozadovana poloha a zpetne hlaseni, prekrocen cas
   SERVO_STATUS_CONTROL,         // ridici napeti (pozadovana poloha) pod limitem (utrzeny potenciometr)
   SERVO_STATUS_OFF              // vypnuto napajeni serva (manualni provoz)
} TServoStatus;

// Stav serv pro logovani (beh serva v limitu me tam nezajima, polohy 0-3 vyhodne zakoduju do 2 bitu)
typedef enum {
   SERVO_LOG_OK,             // v poradku, souhlasi pozadovana poloha a zpetne hlaseni, pripadne servo jede, ale jeste je v casovem limitu
   SERVO_LOG_TIMEOUT,        // nesouhlasi pozadovana poloha a zpetne hlaseni, prekrocen cas, zaseknute servo
   SERVO_LOG_CONTROL,        // ridici napeti (pozadovana poloha) pod limitem (utrzeny potenciometr)
   SERVO_LOG_MANUAL          // vypnuto napajeni serva (manualni provoz)
} TServoStatusLog;



//-----------------------------------------------------------------------------
// Typ ulozeni
//-----------------------------------------------------------------------------

typedef enum {
   _LOC_FIRST,
   LOC_CELE = _LOC_FIRST,
   LOC_PREDNI_POLOVINA,
   LOC_ZADNI_POLOVINA,
   LOC_KRAJNI_TRETINY,
   LOC_PREDNI_TRETINA,
   LOC_STREDNI_TRETINA,
   LOC_ZADNI_TRETINA,      // Toto musi zustat posledni, pouziva se to v testech
   _LOC_LAST = LOC_ZADNI_TRETINA,
   _LOC_COUNT
} TLocalizationStatus;

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

// identifikace teplomeru :
// Pozor, musi pozicne odpovidat konstantam MWI_CH0..MWI_CH4
// 6.3.2003: Musi take zacinat od nuly, protoze je pouzivam jako indexy v poli pro prumerovani teplot
typedef enum {
   TEMP_FRONT,         // T1 - pozor, teploty ve skrini musi byt s indexem 0 az 2, vyuziva se to v polich
   TEMP_MIDDLE,        // T2
   TEMP_REAR,          // T3
   TEMP_CHANNEL,       // T4
   TEMP_OUTSIDE,       // T5
   TEMP_RECIRCULATION, // T6
   TEMP_COUNT          // Celkovy pocet merenych teplot - kvuli poli pro prumerovani
} TThermoAddress;

// Teplota : horni byte je v celych stupnich,
// dolni MSB 0.5C...0.0625C 0 0 0 0 LSB
// Na float se prevede jako temp/256

//-----------------------------------------------------------------------------
// Diesel
//-----------------------------------------------------------------------------

typedef dword TDieselMotoHour;      // datovy typ pro motominuty - motohodiny jsou word
                                    // pri zmene typu POZOR na prirazovani do TFarData

#define DSL_COUNT        50         // pocet polozek pole hodnot - POZOR, pocet * velikost polozky se musi vejit do 1 bajtu

// Datovy typ pro zapis do iEEPROM :
typedef TDieselMotoHour TDieselArray[DSL_COUNT];
// Motohodiny jsou v hodinach

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

#define CONFIG_RESERVED_SIZE    31       // Pocet rezervovanych bajtu v TConfig

typedef struct {
   // interni data, pro ulozeni v EEPROM
   word  Magic;                       // Identifikace souboru
   word  Version;                     // AUTO_VERSION
   byte  IdentificationNumber;        // identifikace zarizeni
   char  TargetTemperature;           // Cilova teplota, cele stupne
   byte  Localization;                // TLocalizationStatus: lozeni prepravek ve skrini
   int16 AutoHysteresis;              // Hystereze pro prechod mezi topenim a chlazenim C * 256 (ve vnitrnim formatu teploty)
   int32 RegKp;                       // zesileni 1000 == 1.000
   int32 RegTi;                       // integracni cas s
   byte  MaxOffsetAbove;              // Maximalni odchylka teploty ve skrini nad cilovou teplotou
   byte  MaxOffsetBelow;              // Maximalni odchylka teploty ve skrini pod cilovou teplotou
   word  ChangeOilPeriod;             // Perioda motohodin do vymeny oleje
   word  ChangeOilAtHours;            // Motohodiny, pri kterych se ma vymenit olej (interni udaj, neni v menu)
   byte  SavingPeriod;                // Perioda ukladani v minutach
   byte  Login;                       // YES/NO pouziti loginu ridice
   word  RegTd;                       // Derivacni cas v celych sekundach
   byte  MaxOvershootSum;             // Maximalni integral prekmitu teploty pres AutoHysteresis v C*s

   byte  Reserved[CONFIG_RESERVED_SIZE];    // Pro budouci pouziti

   byte  CheckSum;                    // kontrolni soucet
} TConfig;    // 60 bajtu - vic ne, aby se vlezlo do 1 stranky EEPROM (mam to tak koncipovane)

extern TConfig __xdata__ Config;      // buffer konfigurace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
#define CFG_ALL                     0xFFFFFFFFL    // uloz vsechno
#define CFG_MAGIC                   0x00000001L
#define CFG_VERSION                 0x00000002L
#define CFG_IDENTIFICATION_NUMBER   0x00000004L
#define CFG_TARGET_TEMPERATURE      0x00000008L
#define CFG_LOCALIZATION            0x00000010L
#define CFG_AUTO_HYSTERESIS         0x00000020L
#define CFG_REG_KP                  0x00000040L
#define CFG_REG_TI                  0x00000080L
#define CFG_CHANGE_OIL_PERIOD       0x00000100L
#define CFG_CHANGE_OIL_AT_HOURS     0x00000200L
#define CFG_MAX_OFFSET_ABOVE        0x00000400L
#define CFG_MAX_OFFSET_BELOW        0x00000800L
#define CFG_SAVING_PERIOD           0x00001000L
#define CFG_LOGIN                   0x00002000L
#define CFG_REG_TD                  0x00004000L
#define CFG_MAX_OVERSHOOT_SUM       0x00008000L

#define CFG_CHECK_SUM               0x80000000L

//-----------------------------------------------------------------------------
// Protokolovani udalosti
//-----------------------------------------------------------------------------

// Typ udalosti :                       parametr udalosti
typedef enum {
   PRT_START,
   PRT_PRIHLASENI_RIDICE,              // cislo ridice
   PRT_NASTAVENI_HODIN,
   PRT_NASTAVENI_TEPLOTY,              // teplota
   PRT_NASTAVENI_MEZI_SKRINE,          // teplota nad + pod
   PRT_NASTAVENI_PERIODY_UKLADANI,     // perioda
   PRT_NASTAVENI_REG_KP,               // Kp
   PRT_NASTAVENI_REG_TI,               // Ti
   PRT_NASTAVENI_HYSTEREZE,            // Hystereze * 10
   PRT_NASTAVENI_PERIODY_OLEJE,        // perioda
   PRT_SMAZANI_MOTOHODIN,
   PRT_SMAZANI_LOGU,
   PRT_VYMENA_OLEJE,
   PRT_EXPORT_DAT,                     // YES uspesny, NO neuspesny
   PRT_TISK_ZAZNAMU,
   PRT_NASTAVENI_LOZENI,               // lozeni prepravek
   PRT_NASTAVENI_IDENTIFIKACNIHO_CISLA,// Identifikacni cislo
   PRT_NASTAVENI_LOGIN,                // YES/NO
   PRT_DATOVE_VOLANI,
   PRT_NASTAVENI_REG_TD,               // Td
   PRT_NASTAVENI_MAX_PREKMIT,          // Maximalni suma prekmitu
   _PRT_LAST,
   // znacky pro FIFO :
   PRT_FIFO_MARKER_FULL  = 0xFE,
   PRT_FIFO_MARKER_EMPTY = 0xFF
} TPrtType;

// Zaznam o udalosti :

typedef struct SEvent {
   byte           RecordType;           // Odpovida 1. bajtu ve strukture TLog
   byte           Type;                 // typ udalosti
   dword          Parameter;            // parametr udalosti
   byte           Driver;               // Cislo nalogovaneho ridice
   // Datum a cas se uklada normalne do TLog
   byte           Spare[sizeof(TLog) - sizeof(byte) - sizeof(byte) - sizeof(dword) - sizeof(byte)];  // Vyplneni, aby byla velikost stejna jako TLog
} TEvent;

extern TEvent __xdata__ Event;    // buffer udalosti v externi RAM

//-----------------------------------------------------------------------------
// Data v interni EEPROM
//-----------------------------------------------------------------------------

#define IEP_SPARE1   (IEP_PAGE_SIZE - sizeof( TConfig))       // vyhrazeni stranky na konfiguraci
#define IEP_SPARE2   (IEP_SIZE                 \
                      - sizeof( TConfig)       \
                      - IEP_SPARE1             \
                      - sizeof( TDieselArray)) \

typedef struct {
   TConfig           Config;                      // konfigurace
   byte              Spare1[ IEP_SPARE1];         // rezerva pro konfiguraci
   byte              Spare2[ IEP_SPARE2];         // rezerva
   TDieselArray      DieselArray;                 // pole motohodin
} TIep;

//-----------------------------------------------------------------------------
// Data v externim modulu
//-----------------------------------------------------------------------------

// Nekomprimovany datum a cas:
typedef struct {
   byte Min        : 6,
        Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3,     // horni  3 bity
        Day        : 5;
   byte Month      : 4,
        Dummy      : 4;     // vypln
   word Year;               // uplny rok
} TLongDateTime;
// makra shodna s TShortDateTime

// Razitko externiho modulu:
typedef struct {
   TLongDateTime    DatumPorizeni;      // datum nahrani do modulu
   word             MotoHodiny;         // Celkovy pocet motohodin dieselu
   byte             UseBothFloorFlaps;  // YES/NO pouzivaji se oddelene podlahove klapky
   byte             KontrolniSuma;      // kontrolni suma modulu
} TStamp;
extern TStamp __xdata__ Stamp;        // buffer razitka v externi RAM

// Velikost rezervy v datovem modulu :
#define XMEM_SPARE   XMEM_SIZE                 \
                     - sizeof( TLogArray)      \
                     - sizeof( TConfig)        \
                     - sizeof( TStamp)         \

// Struktura datoveho externiho modulu :
typedef struct {
   TLogArray Log;                            // zaznam dat
   TConfig   Config;                         // aktualni konfigurace
   byte      Spare[XMEM_SPARE];              // rezerva
   TStamp    Stamp;                          // zaznam o porizeni
} TXmem;


//-----------------------------------------------------------------------------
#endif // __Tmc_H__
