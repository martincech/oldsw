//*****************************************************************************
//
//    LogData.h - TMC log data structure
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __LogData_H__
   #define __LogData_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

//#error "LogData included"

//-----------------------------------------------------------------------------
// Struktura zaznamu Logu
//-----------------------------------------------------------------------------

// Komprimovany datum a cas
typedef struct {
   byte Min        : 6;
   byte Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3;     // horni  3 bity
   byte Day        : 5;
   byte Month      : 4;
   byte Year       : 3;     // rok % 8
   byte Changed    : 1;     // priznak zmeny nastaveni RTC
} TShortDateTime;   // Velikost 3 bajty

// Ukladani roku: z roku se do struktury ukladaji pouze spodni 3 bity. Rekonstrukce pak probiha tak, ze hornich 13 bitu se vezme z aktualniho roku
// a doplni se temito 3 bity ze struktury a mam rok zrekonstruovany. Pokud je takto vznikly rok vetsi nez aktualni rok, odecte se od nej 8,
// protoze novejsi datum nez aktualni to byt nemohl. Funguje to pro az 8 let stare zaznamy.

// zapis hodin do struktury :
#define mkhour( dt, h)     dt.Hour_lo = (h) & 0x03; dt.Hour_hi = ((h) >> 2) & 0x07
// cteni hodin ze struktury :
#define gethour( dt)       ((dt.Hour_lo) | (dt.Hour_hi << 2))
// zapis roku do struktury :
#define mkyear( dt, y)     dt.Year = (y) & 0x07


// Struktura pro zaznam
typedef struct SLog {
  // Cilovou teplotu necham na prvnim miste, kde je marker -127
  char TargetTemperature;                   // Cilova teplota na cele stupne vcetne znamenka

  // Teploty ve skrini a poloha vzduchovych serv
  word FrontTemp                    : 10,   // Absolutni hodnota na desetiny C
       FrontTempSign                : 1,    // Znamenko
       FrontTempOffset              : 1,    // YES/No teplota mimo meze
       FreshAirPosition             : 4;    // Poloha serva naporu a recirkulace (cerstveho vzduchu)

  word MiddleTemp                   : 10,   // Absolutni hodnota na desetiny C
       MiddleTempSign               : 1,    // Znamenko
       MiddleTempOffset             : 1,    // YES/No teplota mimo meze
       FrontFloorPosition           : 4;    // Poloha prednich serv v podlaze

  word RearTemp                     : 10,   // Absolutni hodnota na desetiny C
       RearTempSign                 : 1,    // Znamenko
       RearTempOffset               : 1,    // YES/No teplota mimo meze
       RearFloorPosition            : 4;    // Poloha zadnich serv v podlaze

  // Teploty okoli, v kanalu a recirkulaci
  word ChannelTemp                  : 10,   // Absolutni hodnota na desetiny C
       ChannelTempSign              : 1,    // Znamenko
       Voltage                      : 5;    // Napeti ve voltech

  word OutsideTemp                  : 10,   // Absolutni hodnota na desetiny C
       OutsideTempSign              : 1,    // Znamenko
       LowVoltage                   : 1,    // YES/NO nizke napeti akumulatoru
       ChargingOn                   : 1,    // YES/NO bezi dobijeni
       ChargingFailure              : 1,    // YES/NO porucha bobijeni
       DieselOn                     : 1,    // YES/NO diesel bezi
       ChangeOil                    : 1;    // YES/NO vymena oleje

  word RecirculationTemp            : 10,   // Absolutni hodnota na desetiny C
       RecirculationTempSign        : 1,    // Znamenko
       DieselTemperature            : 4,    // Teplota dieselu
       DieselUse                    : 1;    // YES/NO pouziti diesel motoru v prepravniku

  // Servo topeni a poruchy serv
  byte HeatingPosition              : 4,    // Poloha serva topeni
       HeatingServoStatus           : 2,    // Stav serva topeni
       RecirculationServoStatus     : 2;    // Stav serva recirkulace

  byte FrontLeftFloorServoStatus    : 2,    // Stav leveho predniho serva v podlaze
       FrontMiddleFloorServoStatus  : 2,    // Stav stredniho predniho serva v podlaze
       FrontRightFloorServoStatus   : 2,    // Stav praveho predniho serva v podlaze
       RearLeftFloorServoStatus     : 2;    // Stav leveho zadniho serva v podlaze

  byte RearMiddleFloorServoStatus   : 2,    // Stav stredniho zadniho serva v podlaze
       RearRightFloorServoStatus    : 2,    // Stav praveho zadniho serva v podlaze
       TopInductionServoStatus      : 2,    // Stav horniho naporoveho serva
       BottomInductionServoStatus   : 2;    // Stav spodniho naporoveho serva

  // Rezim
  byte Mode                         : 3,    // Zvoleny rezim (TLogControlMode)
       ManualCoolingPower           : 4,    // Aktualni vykon pri rucnim chlazeni
       EmergencyMode                : 1;    // YES/NO nouzove ovladani z rozvadece

  byte Current;                             // Proud v amperech

  // Ovladani klapek, lozeni
  byte ManualFreshAir               : 1,    // YES/NO rucne/automaticky cerstvy vzduch
       ManualFrontFloor             : 1,    // YES/NO rucne/automaticky predni klapky v podlaze
       ManualRearFloor              : 1,    // YES/NO rucne/automaticky zadni klapky v podlaze
       Localization                 : 3,    // Lozeni prepravek (TLocalizationStatus)
       FanLowPower                  : 1,    // YES/NO nizky vykon ventilatoru
       Alarm                        : 1;    // YES/NO je hlasen alarm

  // Agregaty a poruchy agregatu
  byte CoolingOn                    : 1,    // YES/NO sepnuta spojka chlazeni
       HeatingFlame                 : 1,    // YES/NO plamen topeni
       EconomicHeating              : 1,    // YES/NO ekonomicky beh topeni
       HeatingFailure               : 1,    // YES/NO porucha topeni
       CoolingFailure               : 1,    // YES/NO porucha chlazeni
       FanFailure                   : 1,    // YES/NO porucha ventilatoru
       ElMotorOn                    : 1,    // YES/NO zapnuty elektromotor
       GsmRegistered                : 1;    // YES/NO GSM modul je prihlaseny do site (symbol antenky)

  // Login
  byte Driver                       : 7,    // Cislo logovaneho ridice, 0-99
       Dummy                        : 1;

  // Datum a cas zaznamu - az na konci, na toto misto se vyplnuje i datum a cas pri protokolovani
  TShortDateTime DateTime;

} TLog;  // 24 bajtu

// Konstanty pro ukladani do EEPROM FIFO :
#define TMC_FIFO_START            0  // Pocatecni adresa FIFO v EEPROM
#define TMC_FIFO_CAPACITY       1330 // Maximalni pocet ukladanych polozek
#define TMC_FIFO_MARKER_EMPTY  -127  // Znacka konce pri neuplnem zaplneni - teplota
#define TMC_FIFO_MARKER_FULL   -128  // znacka konce pri prepisovani
#define TMC_FIFO_PROTOCOL_TYPE -126  // Pokud je prvni bajt zaznamu roven tomuto, jde o zaznam s protokolem cinnosti ridice. Pokud ne, jde o normalni zaznam

// Datovy typ popisujici strukturu logu (v EEPROM) :
typedef TLog TLogArray[TMC_FIFO_CAPACITY];   // datovy logger 31920 bajtu


#endif
