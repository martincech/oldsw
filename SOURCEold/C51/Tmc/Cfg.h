//*****************************************************************************
//
//    Cfg.h - Configuration load/save
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Cfg_H__
   #define __Cfg_H__

#include "Hardware.h"     // pro podminenou kompilaci

TYesNo CfgLoad( void);
// Nacte konfiguraci z EEPROM
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)

TYesNo CfgSave( dword mask);
// Ulozi polozky definovane maskou do EEPROM
// Maska se ORuje z konstant ACFG_... viz Auto.h
// Vraci NO, nepovedl-li se zapis

// Ladici funkce :

TYesNo CfgChecksumOk( void);
// Porovna kontrolni soucet v EEPROM a v AutoConfig

#endif
