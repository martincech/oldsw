//*****************************************************************************
//
//   TmcCmd.h    TMC communication protocol
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __TmcCmd_H__
   #define __TmcCmd_H__

// Prikazy :

typedef enum {
   TMCCMD_NONE,                // zadny prikaz
   TMCCMD_VERSION,             // cislo verze
   TMCCMD_READ,                // cteni EEPROM
   TMCCMD_READ_CONFIG,         // cteni konfigurace + razitka
   TMCCMD_READ_ALL,            // cteni cele EEPROM
   TMCCMD_REPLY = 0x80,        // priznakovy bit odpovedi
   TMCCMD_ERROR = 0xFF         // odpoved na chybny prikaz
} TmcCmdCommand;

// VERSION :
// Odpoved : V poli Data je hexadecimalni cislo verze

// READ :
// V poli Data prikazu je adresa EEPROM
// Odpoved : dlouhy paket s daty

#endif
