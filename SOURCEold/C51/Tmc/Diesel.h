//*****************************************************************************
//
//    Diesel.c - Obsluha diesel motoru
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Diesel_H__
   #define __Diesel_H__

#include "Hardware.h"



typedef struct {
  byte  Use;            // YES/NO zda se diesel motor pouziva
  byte  On;             // YES/NO bezi diesel
  byte  JustStarted;    // YES/NO diesel byl prave nastartovan (jde z vypnuteho stavu do zapnuteho)
  word  MotoHours;      // Pocet nabehanych motohodin v hodinach
  byte  ChangeOil;      // YES/NO ma se vymenit olej s filtrem
  byte  Temperature;    // Teplota motoru
  byte  Overheated;     // YES/NO prehraty diesel
} TDiesel;
extern TDiesel __xdata__ Diesel;

#define DSL_TEMP_STEPS        11       // Pocet kroku


void DieselInit( void);
// Nastaveni inicialniho stavu pocitadla motohodin

void DieselExecute( void);
// Aktualizace motohodin, volat min 1x za sekundu

void DieselSet(word Motohours, word Oil);
// Nastaveni poctu motohodin a vymeny oleje

void DieselQuitOil( void);
// Odkvituje vymenu oleje

//----- Interni funkce, nepouzivat :

word DieselRead( void);
// Cteni motohodin - vrati pocet motohodin

TYesNo DieselWrite( word MotoHour);
// Zapis motohodin

TYesNo DieselReset( void);
// Vymaze celou oblast

void DieselCheckTemp();
// Nacte teplotu dieselu a ulozi ji. Pokud se cidlo nevyuziva, ulozi nulu, pokud je cidlo odpojene (porucha), ulozi plnou hodnotu.
// Zajistuje i prumerovani.

#endif
