//*****************************************************************************
//
//   Ras.h       Remote Terminal access
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Ras_H__
   #define __Ras_H__

#include "Hardware.h"          // zakladni datove typy
#include "Tmc.h"               // Rozsahy
#include "Time.h"              // Datum a cas


// Prikazy z terminalu:
#define RAS_CMD_SET_MODE                1           // Nastavi rezim
#define RAS_CMD_HEATING_INCREASE        2           // Rucni pridani topeni
#define RAS_CMD_HEATING_DECREASE        3           // Rucni ubrani topeni
#define RAS_CMD_COOLING_INCREASE        4           // Rucni pridani chlazeni
#define RAS_CMD_COOLING_DECREASE        5           // Rucni ubrani chlazeni
#define RAS_CMD_SET_TARGET_TEMPERATURE  6           // Nastavi cilovou teplotu
#define RAS_CMD_SET_LOCALIZATION        7           // Nastavi lozeni
#define RAS_CMD_SET_FRESH_AIR_MODE      8           // Nastavi rezim cerstveho vzduchu
#define RAS_CMD_FRESH_AIR_INCREASE      9           // Rucni pridani cerstveho vzduchu
#define RAS_CMD_FRESH_AIR_DECREASE      10          // Rucni ubrani cerstveho vzduchu
#define RAS_CMD_SET_FRONT_FLAP_MODE     11          // Nastavi rezim prednich klapek v podlaze
#define RAS_CMD_FRONT_FLAP_INCREASE     12          // Rucni pridani prednich klapek v podlaze
#define RAS_CMD_FRONT_FLAP_DECREASE     13          // Rucni ubrani prednich klapek v podlaze
#define RAS_CMD_SET_REAR_FLAP_MODE      14          // Nastavi rezim zadnich klapek v podlaze
#define RAS_CMD_REAR_FLAP_INCREASE      15          // Rucni pridani zadnich klapek v podlaze
#define RAS_CMD_REAR_FLAP_DECREASE      16          // Rucni ubrani zadnich klapek v podlaze
#define RAS_CMD_SOUND                   17          // Zapne / vypne hlaseni alarmu
#define RAS_CMD_TIME                    18          // Nastavi novy datum a cas
#define RAS_CMD_LOGIN                   19          // Provede login ridice
#define RAS_CMD_OFFSET                  20          // Nastavi novych mezi nad a pod cilovou teplotou
#define RAS_GET_DATA                    21          // Posle zpet periodicka data TRasData

// Odpovedi ze serveru:
#define RAS_CMD_NAK                     0xF0        // Zaslany parametr je neplatny, nastaveni se neprovedlo (hodnota mimo rozsah, jednotka neni ve spravnem rezimu atd)


// Struktura pro periodicka data
typedef struct {
  byte AccuVoltage              : 5,    // Napeti ve voltech
       ConfigLocalization       : 3;    // TLocalizationStatus: lozeni prepravek ve skrini

  byte ControlMode              : 3,    // Rezim, ve kterem regulator pracuje TControlMode
       ControlAutoMode          : 1,    // Cinnost v automatickem rezimu - bud se topi, nebo chladi TAutoMode
       ControlEmergencyMode     : 1,    // YES/NO nouzove ovladani z rozvadece
       ControlOldEmergencyMode  : 1,    // Nouzove ovladani v predchozim kroku
       ControlFrontTempFault    : 1,    // Porucha predniho cidla (nedojeti do pasma cilove teploty)
       ControlMiddleTempFault   : 1;    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)

  byte ControlRearTempFault     : 1,    // Porucha zadniho cidla (nedojeti do pasma cilove teploty)
       CoolingPercent           : 4,    // Nastavena hodnota vykonu chlazeni v procentech (jen v rucnim rezimu)
       CoolingOn                : 1,    // YES/NO prave sepnute chlazeni
       CoolingFailure           : 1,    // YES/NO porucha chlazeni
       DieselUse                : 1;    // YES/NO zda se diesel motor pouziva

  byte DieselOn                 : 1,    // YES/NO bezi diesel
       DieselChangeOil          : 1,    // YES/NO ma se vymenit olej s filtrem
       DieselTemperature        : 4,    // Teplota motoru
       DieselOverheated         : 1,    // YES/NO prehraty diesel
       ElMotorOn                : 1;    // YES/NO zapnuty elektromotor

  byte FreshAirMode             : 1,    // Typ rizeni TFreshAirMode
       FreshAirPercent          : 7;    // Aktualni hodnota cersrtveho vzduchu v procentech

  byte FreshAirPosition               : 4,  // Poloha klapky pro zobrazeni a logovani
       FreshAirFrontFloorFlapPosition : 4;  // Poloha klapky pro zobrazeni a logovani

  byte FreshAirFrontFloorFlapMode    : 1,   // Typ rizeni
       FreshAirFrontFloorFlapPercent : 7;   // Aktualni hodnota otevreni klapky v procentech

  byte FreshAirRearFloorFlapMode    : 1,    // Typ rizeni
       FreshAirRearFloorFlapPercent : 7;    // Aktualni hodnota otevreni klapky v procentech

  byte FreshAirRearFloorFlapPosition : 4,   // Poloha klapky pro zobrazeni a logovani
       HeatingPosition               : 4;   // Poloha serva topeni pro logovani

  byte HeatingPercent           : 7,    // Aktualni hodnota vykonu topeni v procentech
       FanOn                    : 1;    // YES/NO zapnute ventilatory

  byte ServoFrontLeftFloorServoStatus   : 3,    // Stav leveho predniho serva v podlaze
       ServoFrontMiddleFloorServoStatus : 3,    // Stav stredniho predniho serva v podlaze
       HeatingFailure                   : 1,    // YES/NO porucha samotneho topeni
       HeatingServoFailure              : 1;    // YES/NO porucha serva topeni

  byte ServoFrontRightFloorServoStatus  : 3,    // Stav praveho predniho serva v podlaze
       ServoRearLeftFloorServoStatus    : 3,    // Stav leveho zadniho serva v podlaze
       HeatingEconomy                   : 1,    // YES/NO nizky vykon topeni
       HeatingFlame                     : 1;    // YES/NO topeni prave hori

  byte ServoRearMiddleFloorServoStatus  : 3,    // Stav stredniho zadniho serva v podlaze
       ServoRearRightFloorServoStatus   : 3,    // Stav praveho zadniho serva v podlaze
       ChargingOn                       : 1,    // YES/NO dobiji se
       ChargingFailure                  : 1;    // YES/NO porucha dobijeni

  byte ServoTopInductionServoStatus     : 3,    // Stav horniho naporoveho serva
       ServoBottomInductionServoStatus  : 3,    // Stav spodniho naporoveho serva
       FanLowPower                      : 1,    // YES/NO 50% vykon
       FanFailure                       : 1;    // YES/NO porucha ventilatoru

  int16 HeatingError;              // Rozdil mezi cilovou a prumernou teplotou
  int32 HeatingProportional;       // Proporcionalni slozka PI regulace
  int32 HeatingIntegral;           // Integracni slozna PI regulace

  byte LogDriver                : 7,    // Cislo nalogovaneho ridice
       AccuLowVoltage           : 1;    // YES/NO nizke napeti akumulatoru

  char Temp1C[TEMP_COUNT];    // Teploty zaokrouhlene na 1 stupen
  int  Temp01C[TEMP_COUNT];   // Teploty zaokrouhlene na desetinu stupne

  TTransferTime Time;         // 4 bajty

  char ConfigTargetTemperature; // Cilova teplota, cele stupne

  byte ConfigMaxOffsetAbove     : 7,    // Maximalni odchylka teploty ve skrini nad cilovou teplotou
       AlarmShowAlarm           : 1;    // YES/NO zda se prave indikuje alarm

  byte ConfigMaxOffsetBelow     : 7,    // Maximalni odchylka teploty ve skrini pod cilovou teplotou
       AlarmQuiet               : 1;    // YES/NO potlaceny reproduktor pri alarmu

  byte RecirculationServoStatus : 3,    // Stav serva recirkulace
       GsmRegistered            : 1,    // GSM modul je prihalsen do site
       GsmCalling               : 1,    // GSM modul prave komunikuje
       Dummy                    : 3;

} TRasData;   // 52 bajtu - upravit vzdy i PACKET_MAX_DATA v Hardware.h


void RasInit( void);
// Inicializace terminalu

void RasExecute( void);
// Periodicka obsluha - volat 1x za sekundu

#endif


