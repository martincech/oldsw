//*****************************************************************************
//
//    Servo.c - Kontrola stavu serv
//    Version 2.0, (c) Vymos
//
//*****************************************************************************

#include "Servo.h"
#include "Aout.h"              // Analogove vystupy
#include "Alarm.h"             // Indikace poruch
#include "Control.h"           // Automaticke rizeni

TServoStatus __xdata__ ServoStatus[_SERVO_COUNT]; // Stavy jednotlivych serv

#ifndef __TMCREMOTE__

static __xdata__ word timer[ _SERVO_COUNT];

// Lokalni funkce :

static byte ReadControl( byte address);
// Cteni ridiciho napeti

//-----------------------------------------------------------------------------
// Inicializace serv
//-----------------------------------------------------------------------------

void ServoInit( void)
// Inicializuje modul serv
{
  byte i;

  for( i = 0; i < _SERVO_COUNT; i++){
    timer[ i] = 0;
    ServoStatus[i] = SERVO_STATUS_OK;
  }
} // ServoInit

//-----------------------------------------------------------------------------
// Nulovani timeru
//-----------------------------------------------------------------------------

void ServoResetTimer(byte Cislo) {
  // Vynuluje timer u serva cislo <Cislo> - pouziva se pri prepnuti rezimu
  timer[Cislo] = 0;
}

//-----------------------------------------------------------------------------
// Test zavreni serva
//-----------------------------------------------------------------------------

#ifdef USE_COOLING

TYesNo ServoFullyClosed(byte Cislo) {
  // Pokud je servo <Cislo> plne zavrene, vrati YES.
  // Toleranci beru 10% rozsahu
  return (TYesNo)(AinServo[Cislo] < SERVO_MIN + (SERVO_MAX - SERVO_MIN) / 10);
}

#endif // USE_COOLING

//-----------------------------------------------------------------------------
// Hlidani serva
//-----------------------------------------------------------------------------

void ServoCheck( void)
// Kontroluje cinnost serv. Volat 1x za sekundu
{
byte i;
byte control_value;
byte measure_value;
byte difference;        // absolutni rozdil
TYesNo ServoMimoPolohu;    // Flag, ze je dane servo mimo nastavenou polohu a ma se citat doba timeoutu

   // Pruchod pres vsechna serva :
   for (i = 0; i < _SERVO_COUNT; i++) {
      control_value = ReadControl( i);
      measure_value = AinServo[i];
      if( control_value < SERVO_OFF_LIMIT){
         // utrzeny potenciometr, ridici napeti pod urovni
         ServoStatus[i] = SERVO_STATUS_CONTROL;
         if (i != SERVO_HEATING) {
           // Alarm nastavim jen pro vzduchove serva, poruchu serva topeni resim zvlast v Heating.c51
           AlarmSet();             // Nastavim alarm
         }
         continue;
      }
      if( measure_value < SERVO_OFF_LIMIT){
         // zpetne napeti, mensi nez limit, servo vypnuto. POrucvhu pro vzduchova serva nenastavuju, zobrazi se jen krizek. Servo topeni se resi zvlast.
         ServoStatus[i] = SERVO_STATUS_OFF;
         continue;
      }

      // Servo v provozu, kontroluj: zmerene napeti musi odpovidat ridicimu s presnosti SERVO_ACCURACY
      ServoMimoPolohu = NO;    // Default je servo ve spravne poloze
      difference = control_value > measure_value ?
                   control_value - measure_value :
                   measure_value - control_value;
      if (difference > SERVO_ACCURACY) {
        // Nesouhlasi polohy, citej cas
        ServoMimoPolohu = YES;
      }//if

      if (ServoMimoPolohu) {
        // Servo je mimo nastavenou polohu, citam cas, po ktery je mimo polohu
        timer[ i]++;
        if (timer[ i] == 0) {
          timer[ i] = 0xFFFF;                                  // limitace pri pretoku
        }
        if (timer[ i] > SERVO_RESPONSE) {
          ServoStatus[i] = SERVO_STATUS_TIMEOUT;  // prekrocen cas
          if (i != SERVO_HEATING) {
            // Alarm nastavim jen pro vzduchove serva, poruchu serva topeni resim zvlast
            AlarmSet();             // Nastavim alarm
          }
        } else {
          ServoStatus[i] = SERVO_STATUS_RUNNING;  // zatim v casovem limitu
        }
      } else {
        // Poloha serva je v poradku
        timer[i] = 0;
        ServoStatus[i] = SERVO_STATUS_OK;
      }//else
   }
} // ServoCheck

//-----------------------------------------------------------------------------
// Cteni rizeni
//-----------------------------------------------------------------------------

static byte CalculateInput(byte Output) {
  // Podle zadane hodnoty DA prevodniku 0..DAC_VALUE_MAX vypocte odpovidajici AD prevod 0..ADC_RANGE
  return (word)((word)Output * (word)ADC_RANGE / (word)DAC_VALUE_MAX);
}

static byte ReadControl( byte address)
// Cteni ridiciho napeti (analogoveho vystupu)
{
   switch (address) {
     case SERVO_INDUCTION1:
     case SERVO_INDUCTION2:
     case SERVO_RECIRCULATION:
        return (CalculateInput(AoutInductionServo));

     case SERVO_FLOOR_FL:
     case SERVO_FLOOR_FM:
     case SERVO_FLOOR_FR:
        return (CalculateInput(AoutFrontFloorServo));

     case SERVO_FLOOR_RL:
     case SERVO_FLOOR_RM:
     case SERVO_FLOOR_RR:
        return (CalculateInput(AoutRearFloorServo));

     case SERVO_HEATING:
        return (CalculateInput(AoutHeatingServo));
   }
} // ReadControl

#endif // __TMCREMOTE__

