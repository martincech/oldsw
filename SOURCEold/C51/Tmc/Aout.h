//*****************************************************************************
//
//    Aout.c - Analogove vystupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Aout_H__
   #define __Aout_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#include "..\inc\Tda8444.h"     // analog output

extern byte __xdata__ AoutInductionServo;       // Ridici signal serva naporu a recirkulace
extern byte __xdata__ AoutFrontFloorServo;      // Ridici signal predniho serva v podlaze
extern byte __xdata__ AoutRearFloorServo;       // Ridici signal zadniho serva v podlaze
extern byte __xdata__ AoutHeatingServo;         // Ridici signal serva topeni


void AoutInit();
  // Inicializuje analogovych vystupu

void AoutSetInductionServo(byte Value);
  // Nastavi obe naporova serva

void AoutSetHeatingServo(byte Value);
  // Nastavi servo topeni (je na 2 vystupech)

void AoutSetFrontFloorServo(byte Value);
  // Nastavi predni serva v podlaze

void AoutSetRearFloorServo(byte Value);
  // Nastavi zadni serva v podlaze

#endif
