//*****************************************************************************
//
//    Accu.c - Mereni napeti a proudu akumulatoru
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Accu_H__
   #define __Accu_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Globalni promenne
typedef struct {
  byte Voltage;         // Napeti ve voltech
  byte LowVoltage;      // YES/NO nizke napeti akumulatoru
  byte Current;         // Celkovy proud v amperech
//  byte CurrentFans;     // Proud ventilatoru v amperech
} TAccu;
extern TAccu __xdata__ Accu;

void AccuInit();
  // Inicializace mereni akumulatoru

void AccuCheck(TYesNo IsDieselRunning);
// Zkontroluje napeti a proud akumulatoru. Volat podle potreby

#endif
