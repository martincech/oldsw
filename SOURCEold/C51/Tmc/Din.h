//*****************************************************************************
//
//    Din.c - Digitalni vstupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Din_H__
   #define __Din_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

extern byte __xdata__ DinCharging;             // YES/NO Dobiji se
extern byte __xdata__ DinDieselRun;            // YES/NO Bezi diesel
extern byte __xdata__ DinEmergency;            // YES/NO Nouzove ovladani z rozvadece
extern byte __xdata__ DinHeatingCoolingError;  // YES/NO Porucha topeni nebo chlazeni
extern byte __xdata__ DinHeatingFlame;         // YES/NO Detekce plamene topeni (topeni hori)
extern byte __xdata__ DinFan;                  // YES/NO Beh ventilatoru (vyhodnocovat spolecne s Ain)

#define DIN_DIESEL_RUNNING      1              // Pri jake hodnote portu DinDieselRun je diesel nastartovany
#define DIN_CHARGING_ON         1              // Pri jake hodnote portu DinCharging bezi dobijeni
#define DIN_FAN_ERROR           0              // Pri jake hodnote portu DinFan jsou ventilatory v poruse
#define DIN_COOLING_FAILURE     0              // Pri jake hodnote portu DinHeatingCoolingError je chlazeni v poruse
#define DIN_EMERGENCY_ON        1              // Pri jake hodnote portu DinEmergency se nouzove ovlada

void DinInit();
  // Inicializuje digitalnich vstupu

void DinRead();
  // Nacte vsechny digitalni vstupy a ulozi je do lokalnich promennych

#endif
