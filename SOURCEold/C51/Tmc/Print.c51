//*****************************************************************************
//
//    Print.c     - TMC Printer services
//    Version 1.1  (c) P.Veit, Vymos
//
//*****************************************************************************

#include "Print.h"
#include "Time.h"              // Aktualni datum a cas
#include "Temp.h"              // Teploty
#include "..\inc\System.h"     // Operacni system
#include "..\inc\Fifo.h"       // obsluha FIFO - zde pouze pro tisk
#include "..\inc\X25645.h"     // zabudovana EEPROM - zde pouze pro tisk
#include "..\inc\Printer.h"    // Tiskarna
#include "..\inc\bcd.h"

//-----------------------------------------------------------------------------
// Konstanty
//-----------------------------------------------------------------------------

byte code TISK_PROTOCOL[] = "TEMPERATURE PROTOCOL";
byte code TISK_PRINTED[]  = "Printed:";
byte code TISK_TARGET[]   = "Target temp.:";
byte code TISK_DEGREES[]  = "\x0F8C";
byte code TISK_DATE[]     = "DATE";
byte code TISK_TIME[]     = "TIME";
byte code TISK_AVERAGE[]  = "AVERAGE:";
byte code TISK_TOO_MANY[] = "TOO MANY RECORDS";
byte code TISK_DRIVER[]   = "DRIVER:";
byte code TISK_CUSTOMER[] = "CUSTOMER:";
byte code TISK_END[]      = "END OF PROTOCOL";

//-----------------------------------------------------------------------------
// Globalni data
//-----------------------------------------------------------------------------

dword __xdata__ PocatecniDatum, KoncoveDatum;
word  __xdata__ PocatecniCas, KoncovyCas;
byte  __xdata__ PocetZaznamuNaRadek;

//-----------------------------------------------------------------------------
// Lokalni data
//-----------------------------------------------------------------------------

static word __xdata__ Start[2], Stop[2];         // Odkud kam (vcetne) se maji zaznamy cist
static byte __xdata__ PocetOpakovani;            // Kolikrat se cyklus projizdi (bud 1x pokud se neprepisuje, nebo 2x pokud se prepisuje)

//-----------------------------------------------------------------------------
// Inicializace tiskarny
//-----------------------------------------------------------------------------

void PrintInit( void)
// Inicializuje tiskarnu
{
  PrtInit();
} // PrintInit

//-----------------------------------------------------------------------------
// Star tisku
//-----------------------------------------------------------------------------

void PrintStart( void)
// Inicializuje linku tiskarny pred startem tisku
{
  PrtStart();
} // PrintStart

//-----------------------------------------------------------------------------
// Uvolneni linky
//-----------------------------------------------------------------------------

void PrintStop( void)
// Uvolni linku tiskarny
{
  PrtStop();
} // PrintStop

//-----------------------------------------------------------------------------
// Priprava tisku protokolu
//-----------------------------------------------------------------------------

TYesNo PrintPrepare() {
  // Prohleda FIFO, nastavi ukazatele, vraci NO je-li prazdne
  word Adresa; // Adresa ve fifo - fyzicka adresa jednotlivych bajtu, ne poradove cislo zaznamu - ma to tedy krok sizeof(TFifoData)

  // Predem si pripravim odkud kam mam cist
  Adresa = FifoFindMarker();
  if (EepReadByte(Adresa) == TMC_FIFO_MARKER_EMPTY) {
    // Fifo se jeste neprepisuje
    if (Adresa == FIFO_START) {
      return( NO);  // Fifo je prazdne, neni co tisknout
    }
    Start[0] =FIFO_START;
    Stop[0]  = Adresa - sizeof(TFifoData);
    PocetOpakovani = 1;
  } else {
    // Fifo uz se prepisuje
    // Jedu od pozice markeru+1 na konec a pak od zacatku po marker
    Start[0] = Adresa + sizeof(TFifoData);
    Stop[0]  = FIFO_START + (FIFO_CAPACITY-1) * sizeof(TFifoData);
    Start[1] = FIFO_START;
    Stop[1]  = Adresa - sizeof(TFifoData);
    PocetOpakovani = 2;
  }//else
  return YES;
} // PrintPrepare

//-----------------------------------------------------------------------------
// Tisk protokolu
//-----------------------------------------------------------------------------

static void PrintTemp01(int Temp) {
  // Vytiskne teplotu na desetiny stupne vcetne znamenka
  PrtDec(Temp / 10, 2);  // Cele stupne vcetne znamenka
  if (Temp < 0) {
    Temp =- Temp;   // U desetin me uz znamenko nezajima
  }
  PrtChar('.');
  PrtChar(0x30 + Temp % 10);      // Desetiny
}

void PrintProtocol() {
  // Vytiskne zaznamy za cele obdobi, ktere je v pameti
  word  RokZaznamu;      // Rok vyseparovany ze zaznamu
  dword DatumZaznamu;
  word  CasZaznamu;
  byte  PocetZaznamuVSume;     // Kolik mam napocitano vzorku v sume
  int   Suma;                   // Suma teplot, staci int, musi byt se znamenkem
  long  CelkovaSuma;           // Suma teplot z jednotlivych radku - pro celkovy prumer
  word  PocetRadku;            // Celkovy pocet radku v tabulce
  byte  Opakovani;             // Pro cyklus for
  word  Adresa;                // Adresa ve fifo - fyzicka adresa jednotlivych bajtu, ne poradove cislo zaznamu - ma to tedy krok sizeof(TFifoData)
  int   FrontTemp, MiddleTemp, RearTemp;  // Teploty z logu vcetne znamenka
  TEvent __xdata__ *PrintEvent;// Na pretypovani zaznamu na udalost - pro otestovani, zda jde o pomocny zaznam

  // Vynuluju pocitadla
  PocetZaznamuVSume = 0;
  Suma              = 0;
  CelkovaSuma       = 0;
  PocetRadku        = 0;

  // Pripravim HW pro vysilani po seriove lince
  PrtStart();

  // Vytisknu hlavicku
  PrtNewline();     // Povyjedu 1 radek, aby se tam srovnal papir - nekdy to vytisklo jen pulku prvniho radku
  PrtSpace(2); PrtString(TISK_PROTOCOL); PrtNewline();
  PrtSeparator();
  // Datum tisku
  PrtString(TISK_PRINTED);
  PrtSpace(6);
  PrtByte(bbin2bcd(Time.Day)); PrtChar('.'); PrtByte(bbin2bcd(Time.Month)); PrtChar('.'); PrtWord(wbin2bcd(Time.Year));
  PrtNewline();
  // Cas tisku
  PrtSpace(14);
  PrtByte(bbin2bcd(Time.Hour)); PrtChar(':'); PrtByte(bbin2bcd(Time.Min));
  PrtNewline();
  // Cilova teplota
  PrtString(TISK_TARGET);
  PrtDec(Config.TargetTemperature, 2);  // Vcetne znamenka
  PrtSpace(1);
  PrtString(TISK_DEGREES);
  PrtNewline();
  PrtSeparator();
  // Popis sloupcu tabulky
  PrtSpace(3);
  PrtString(TISK_DATE);
  PrtSpace(5);
  PrtString(TISK_TIME);
  PrtSpace(5);
  PrtString(TISK_DEGREES);
  PrtNewline();
  PrtSeparator();

  for (Opakovani = 0; Opakovani < PocetOpakovani; Opakovani++) {
    for (Adresa = Start[Opakovani]; Adresa <= Stop[Opakovani]; Adresa += sizeof(TFifoData)) {
      SysYield();  // Behem tisku merim a reguluju, akorat neukladam
      FifoRead(Adresa);  // Nactu zaznam z EEPROM

      // Pomocne zaznamy me nezajimaji
      PrintEvent = (TEvent __xdata__ *) &Log;
      if (PrintEvent->RecordType == TMC_FIFO_PROTOCOL_TYPE) {
        continue;  // Jde o pomocny zaznam, preskocim ho a jdu na dalsi
      }

      // Vytvorim datum zaznamu
      RokZaznamu = (word)(Time.Year & ~0x0007 | Log.DateTime.Year);  // Aktualni datum
      if (RokZaznamu > Time.Year) {
        RokZaznamu -= 8;   // Nemuze byt vetsi nez soucasny
      }//if
      DatumZaznamu = (dword)RokZaznamu * 10000L;  // Posunu rok na prvni pozici
      DatumZaznamu += 100L * (dword)Log.DateTime.Month;
      DatumZaznamu += (dword)Log.DateTime.Day;
      // Vytvorim cas zaznamu
      CasZaznamu = 100 * gethour(Log.DateTime) + Log.DateTime.Min;

      // Pokud je datum a cas zaznamu mensi nez pocatecni datum a cas, od ktereho chce tisknout, zahodim tento zaznam
      if (DatumZaznamu < PocatecniDatum) {
        continue;
      } else if (DatumZaznamu == PocatecniDatum) {
        // Jde o stejne dny, musim jeste porovnat casy
        if (CasZaznamu < PocatecniCas) {
          continue;   // Pokud je cas rovny, zaznam beru
        }
      }//if
      // Pokud je datum a cas zaznamu vetsi nez koncovy datum a cas, od ktereho chce tisknout, zahodim tento zaznam
      if (DatumZaznamu > KoncoveDatum) {
        continue;
      } else if (DatumZaznamu == KoncoveDatum) {
        // Jde o stejne dny, musim jeste porovnat casy
        if (CasZaznamu > KoncovyCas) {
          continue;   // Pokud je cas rovny, zaznam beru
        }
      }//if

      // Pokud dosel az sem, tento zaznam spada to zadaneho casoveho obdobi
      // Prictu tento zaznam do sumy
      // Obslouzim znamenko vsech teplot (to je ulozene zvlast)
      FrontTemp = Log.FrontTemp;
      if (Log.FrontTempSign) {
        FrontTemp = -FrontTemp;
      }
      MiddleTemp = Log.MiddleTemp;
      if (Log.MiddleTempSign) {
        MiddleTemp = -MiddleTemp;
      }
      RearTemp = Log.RearTemp;
      if (Log.RearTempSign) {
        RearTemp = -RearTemp;
      }
      FrontTemp = TempLocalizationAverage(FrontTemp, MiddleTemp, RearTemp, Log.Localization, TEMP_INVALID_01C);   // Vyuziju promennou FrontTemp
      if (FrontTemp != TEMP_INVALID_01C) {
        // Teplota nebyla v poruse, prictu
        Suma += FrontTemp;
        PocetZaznamuVSume++;
      }
      // Pokud jsem nasumoval zadany pocet zaznamu, vypoctu prumer a vytisknu radek
      if (PocetZaznamuVSume >= PocetZaznamuNaRadek) {
        // Vytisknu datum a cas tohoto, posledniho zaznamu v prumeru
        // Datum
        PrtByte(bbin2bcd(Log.DateTime.Day)); PrtChar('.'); PrtByte(bbin2bcd(Log.DateTime.Month)); PrtChar('.'); PrtWord(wbin2bcd(RokZaznamu));
        // Cas
        PrtSpace(2);
        PrtByte(bbin2bcd(gethour(Log.DateTime))); PrtChar(':'); PrtByte(bbin2bcd(Log.DateTime.Min));
        // Teplota
        Suma /= (int)PocetZaznamuVSume;
        // Omezim
        if (Suma > 999) Suma = 999;
        if (Suma <- 999) Suma = -999;
        // Prictu teplotu do celkove sumy
        CelkovaSuma += Suma;
        PocetRadku++;
        // Vytisknu teplotu
        PrtSpace(2);
        PrintTemp01(Suma);
        // Poslu radek na tiskarnu
        PrtNewline();
        // Vynuluju promenne
        Suma = 0;
        PocetZaznamuVSume = 0;
        // Osetrim pripadne zacykleni
        if (PocetRadku > TMC_FIFO_CAPACITY / PocetZaznamuNaRadek || PocetRadku > 100) {
          // Nemuze tam byt vic radku nez pocet zaznamu deleno poctem zaznamu na radek. Radsi to omezim i na pevny max. pocet radku, aby nedal periodu
          // 1 minuta na celou pamet - tiskarna by to nemusela vydrzet
          PrtString(TISK_TOO_MANY);
          PrtNewline();
          break;  // Vylezu z for a ukoncim tisk
        }//if
      }//if
    }//for
  }//for

  PrtSeparator();
  // Na zaver vytisknu prumernou celkovou teplotu behem prepravy
  PrtString(TISK_AVERAGE);
  if (PocetRadku > 0) {
    CelkovaSuma /= (long)PocetRadku;
  } else {
    CelkovaSuma = 0;
  }
  PrtSpace(11);
  PrintTemp01(CelkovaSuma);
  // Poslu radek na tiskarnu
  PrtNewline();
  PrtSeparator();

  // Podpisy
  PrtString(TISK_DRIVER);
  PrtNewlines(6);    // Vynecham misto na podpis
  PrtSeparator();
  PrtString(TISK_CUSTOMER);
  PrtNewlines(6);    // Vynecham misto na podpis
  PrtSeparator();
  // Vytisknu, ze je konec protokolu
  PrtSpace(5);
  PrtString(TISK_END);
  PrtNewline();
  PrtSeparator();
  // Povysunu trochu papir
  PrtNewlines(7);

  SysFlushTimeout();        // Musim rozjet odpocitavani necinnosti znovu - tisk trva dlouho a po vytisteni mne to hazelo hned timeout.
} // PrintProtocol
