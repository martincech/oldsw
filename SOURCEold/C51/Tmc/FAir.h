//*****************************************************************************
//
//    FAir.c - Ovladani cerstveho vzduchu
//    Version 1.0
//
//*****************************************************************************


#ifndef __FAir_H__
   #define __FAir_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Ovladani cerstveho vzduchu
typedef enum {
  FAIR_AUTO,                            // Cerstvy vzduch se reguluje automaticky
  FAIR_MANUAL,                           // Cerstvy vzduch se reguluje rucne
  _FAIR_LAST = FAIR_MANUAL,
  _FAIR_COUNT
} TFreshAirMode;

// Ovladani podlahovych klapek
typedef enum {
  FLOOR_FLAP_FRONT = 0,
  FLOOR_FLAP_REAR,
  _FLOOR_FLAP_LAST = FLOOR_FLAP_REAR,
  _FLOOR_FLAP_COUNT
} TFloorFlap;

typedef enum {
  FLAP_AUTO,                            // Klapka se reguluje automaticky
  FLAP_MANUAL,                          // Klapka se reguluje rucne
  _FLAP_LAST = FLAP_MANUAL,
  _FLAP_COUNT
} TFlapMode;

typedef struct {
  TFlapMode Mode;                       // Typ rizeni
  byte      Percent;                    // Aktualni hodnota otevreni klapky v procentech
  byte      Position;                   // Poloha klapky pro zobrazeni a logovani
} TFlap;

// Struktura cerstveho vzduchu
typedef struct {
  TFreshAirMode Mode;                   // Typ rizeni
  byte          Percent;                // Aktualni hodnota cersrtveho vzduchu v procentech
  byte          Position;               // Poloha klapky pro zobrazeni a logovani
  TFlap         FloorFlaps[_FLOOR_FLAP_COUNT];  // Podlahove klapky
} TFreshAir;
extern TFreshAir __xdata__ FreshAir;

#define FAIR_MIN         0              // Minimalni hodnota cerstveho vzduchu
#define FAIR_MAX         100            // Maximalni hodnota cerstveho vzduchu
#define FAIR_FLAP_STEPS  16             // Pocet kroku pro zobrazeni klapky

void FAirInit();
  // Inicializace

void FAirNewMode(TFreshAirMode Mode);
  // Nastavi novy rezim cerstveho vzduchu

void FAirManualIncrease();
  // Pridani cerstveho vzduchu v rucnim rezimu

void FAirManualDecrease();
  // Ubrani cerstveho vzduchu v rucnim rezimu

void FAirNewFloorFlapsMode(TFloorFlap Flap, TFlapMode Mode);
  // Nastavi novy rezim prednich / zadnich klapek v podlaze

void FAirFloorFlapManualIncrease(TFloorFlap Index);
  // Pridani (otevreni) podlahove klapky s indexem <Index> v rucnim rezimu

void FAirFloorFlapManualDecrease(TFloorFlap Index);
  // Ubrani (zavreni) podlahove klapky s indexem <Index> v rucnim rezimu

byte FAirCalculateServoPosition(byte Percent);
  // Prepocte hodnotu v procentech na polohu serva, procenta jsou v mezich FAIR_MIN az FAIR_MAX, pocet poloh serva je FAIR_FLAP_STEPS

void FAirExecute();
  // Nastavi serva naporu, recirkulace a podlahy do pozadovane polohy

#endif
