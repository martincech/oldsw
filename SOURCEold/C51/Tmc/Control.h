//*****************************************************************************
//
//    Control.c - ovladani ventilatoru, topeni a chlazeni
//    Version 1.0
//
//*****************************************************************************


#ifndef __Control_H__
   #define __Control_H__

#include "Hardware.h"

// Rezim
typedef enum {
  MODE_OFF,
  MODE_AUTO,
  MODE_VENTILATION,
  MODE_HEATING,
  MODE_COOLING,
  _MODE_LAST = MODE_COOLING,
  _MODE_COUNT
} TControlMode;

// Automaticke topeni nebo chlazeni
typedef enum {
  AUTO_MODE_HEATING,
  AUTO_MODE_COOLING
} TAutoMode;

// Rezim pro logovani
typedef enum {
  MODE_LOG_OFF,
  MODE_LOG_AUTO_HEATING,
  MODE_LOG_AUTO_COOLING,
  MODE_LOG_VENTILATION,
  MODE_LOG_HEATING,
  MODE_LOG_COOLING
} TLogControlMode;

// Hlidani poruchy teploty uvnitr skrine (teplota ve skrini nedosahne cilove teploty)
#define TEPLOTA_MAX_DOBA_NABEHU   2400  // Kolik sekund ma teplota na dosahnuti pasma kolem cilove teploty od doby prepnuti rezimu nebo zmeny cilove teploty
#define TEPLOTA_MAX_DOBA_PREKMITU 300   // Kolik sekund ma teplota na "vylet" (prekmit) mimo pasmo kolem cilove teploty
#define PORUCHY_TEPLOT_POCET 3          // Celkovy pocet hlidanych teplot
typedef struct {
  byte Fault;                           // YES/NO teplota je prave v poruse
  word OutsideRangeCounter;             // Kolik sekund je teplota mimo zadane pasmo, tj. v chybe
  byte RangeReached;                    // YES/NO, ze se jiz jednou teplota dostala do pasma cilove teploty. Nuluju po zapnuti a prepnuti rezimu.
} TTempFault;

// Struktura rizeni
typedef struct {
  TControlMode Mode;                 // Rezim, ve kterem regulator pracuje
  byte         NewMode;              // YES/NO prave se zmenil rezim
  TAutoMode    AutoMode;             // Cinnost v automatickem rezimu - bud se topi, nebo chladi
  byte         EmergencyMode;        // YES/NO nouzove ovladani z rozvadece
  byte         OldEmergencyMode;     // Nouzove ovladani v predchozim kroku
  int          AverageTemperature;   // Prumerna teplota ve skrini podle lozeni (ve vnitrnim formatu)
  TTempFault   TempFaults[PORUCHY_TEPLOT_POCET];   // Poruchy jednotlivych teplot ve skrini (nedojeti do pasma cilove teploty)
  // Integrace prekmitu C * 256 (ve vnitrnim formatu)
  long         OvershootSum;         // Integrace prekmitu nad hysterezi C * 256, muzu nasumovat az 255C => musi byt long (ani word nestaci)
} TControl;
#ifdef __C51__
// jen pro Keil, v builderu to koliduje s jinym TControl
extern TControl __xdata__ Control;
#endif


void ControlInit();
  // Inicializace

void ClearFaultsInBody();
  // Vynuluje pocitadla poruch teploty (mimo pasmo cilove teploty) - volat po kazdem prepnuti rezimu

void ControlExecute();
  // Krok rizeni, volat 1x za sekundu

TLogControlMode ControlModeLog();
  // Vrati aktualni rezim pro logovani

void ControlSetNewMode(TControlMode Mode);
  // Nastavi novy rezim

#endif
