//*****************************************************************************
//
//    Otacky.c - Mereni otacek
//    Version 1.0
//
//*****************************************************************************


#ifndef __Otacky_H__
   #define __Otacky_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


// 1 mereni otacek
#define OTACKY_POCET_PERIOD   4         // Kolik period se ma v preruseni sumovat, musi zustat 4 (kvuli 4valci aspon delitelne 4), jinak je treba zmena ve vypoctu otacek
#define RYCHLOST_POCET_PERIOD 4         // Kolik period se ma v preruseni sumovat
typedef struct {
  // Promene pro vypocet jedne periody
  word Capture;               // Aktualni hodnota Capture registru
  word PosledniCapture;       // Predchozi hodnota Capture registru
  byte PocetPreteceni;        // Pocet preteceni Capture registru
  byte PocetNamerenychPeriod; // Pocet period signalu, ktere jsem v preruseni nascital
  byte NovaPerioda;           // Flag, ze se nacetla nejaka nova perioda
  // Promenne pro vypocet hodnoty otacek z periody
  int Hodnota;                // Aktualni otacky
  byte NovaHodnota;           // Flag, ze se vypocetla nova hodnota
} TOtacky;
extern TOtacky __xdata__ Otacky;

// Prumerovani
#define OTACKY_PRUMEROVANI 5
typedef struct {
  long Suma;                  // Nacitana suma pro prumerovani
  int Pocet;                  // Pocet vzorku v sume
  int Hodnota;                // Aktualni hodnota
  byte Prumerovani;           // Kolik prvku do prumeru se ma nasumovat
  byte NovaHodnota;           // Flag, ze se vypocetla nova hodnota
} TPrumer;
extern TPrumer __xdata__ OtackyPrumer;

// Klouzavy prumer (integrace) otacek
#define OTACKY_POCET_KLOUZAVY_PRUMER 3  // Pocet prvku klouzaveho prumeru
typedef int TOtackyKlouzavyPrumer[OTACKY_POCET_KLOUZAVY_PRUMER];


void OtackyInit();
  // Inicializuje mereni otacek

void OtackyCheck();
  // Zkontroluje otackomer

#endif
