//*****************************************************************************
//
//    Pwm.c - Pulse width modulation
//    Version 1.0
//
//*****************************************************************************


#ifndef __Pwm_H__
   #define __Pwm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


typedef struct {
  word Pocitadlo;
  word PeriodaH;
  word PeriodaL;
  byte UbehlaPerioda;                 // Flag, ze ubehla perioda PWM
} TPwm;
extern TPwm Pwm;       // Nedelam xdata, je to v preruseni

#define PWM_PERIODA_MS 200                             // Perioda v ms
#define PWM_PERIODA    (PWM_PERIODA_MS/TIMER1_PERIOD)  // Perioda v ms / perioda timeru - pocet ticku
#define PWM_OUTPUT     PWM_OUT                         // Vystup, kterym se vyhrev ovlada
#define PWM_ON         1                               // Hodnota zapnuteho vystupu
#define PWM_OFF        0                               // Hodnota vypnuteho vystupu

void PwmInit();

void PwmSet(byte Strida);
  // Nastavi PWM na zadanou stridu v %

#define PwmTrigger()                                                                \
  if (Pwm.Pocitadlo < PWM_PERIODA - 1) {                                            \
    Pwm.Pocitadlo++;                                                                \
  } else {                                                                          \
    Pwm.Pocitadlo = 0;                                                              \
    Pwm.UbehlaPerioda = YES;                                                        \
  }                                                                                 \
  if (Pwm.Pocitadlo < Pwm.PeriodaH) PWM_OUTPUT = PWM_ON; else PWM_OUTPUT = PWM_OFF; \

#endif
