//*****************************************************************************
//
//    Lambda.c - Vypocet hodnoty Lambda z Ip
//    Version 1.0
//
//*****************************************************************************


#ifndef __Lambda_H__
   #define __Lambda_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

typedef struct {
  byte Lambda;                           // Hodnota lambda v setinach
  word Ratio;                            // Air to Fuel Ratio - pomer palivo : vzduch v setinach
} TLambda;
extern TLambda __xdata__ Lambda;

#define TABLE_COUNT              9     // Pocet prvku v poli kalibrace
#define LAMBDA_LEAN_INFINITY     255   // Hodnota Lambda = chude nekonecno (vzduch)
#define LAMBDA_RICH_INFINITY     0     // Hodnota Lambda = bohate nekonecno

void LambdaCalculate(int Ip);

#endif
