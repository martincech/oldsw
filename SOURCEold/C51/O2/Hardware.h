//*****************************************************************************
//
//    Hardware.h  - hardware descriptions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#ifdef __C51__
// jen pro Keil
#include "..\inc\89C51ED2.h"
#endif
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE






//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define O2AccuOk() 1

// Sbernice
sbit PalD0 = P2^5;
sbit PalD1 = P2^6;
sbit PalD2 = P2^7;
sbit PalD3 = P0^7;
sbit PalD4 = P0^6;
sbit PalD5 = P0^5;
sbit PalD6 = P0^4;
sbit PalD7 = P0^3;

// Sdilene porty
#define PalDI P2^2
#define PalRW P2^3
#define PalE  P2^4

// Pripojeni displeje

// Pripojeni Gmetru
sbit VSTUP_G1=P1^3;
sbit VSTUP_G2=P1^4;

// Pripojeni klavesnice
sbit KbdK0 = P1^0;
sbit KbdK1 = P1^1;
sbit KbdK2 = P1^2;

// Digitalni vstupy
#define DIN_COUNT 7                     // Pocet digitalnich vstupu
sbit DinCS = P2^0;
#define Din0 PalD7
#define Din1 PalD6
#define Din2 PalD5
#define Din3 PalD4
#define Din4 PalD3
#define Din5 PalD2
#define Din6 PalD1

#define DinBatteryLow PalD0             // Lowbat

// Frekvencni vstupy
sbit Fin0=P1^5;
sbit Fin1=P1^6;
sbit Fin2=P1^7;

// Vystup pro obdelnik pro mereni odporu
sbit RI_OUT=P1^5;

// Vystup pro vyhrev lambdy
sbit PWM_OUT=P1^6;

// Digitalni vystupy
#define DOUT_COUNT 7                     // Pocet digitalnich vystupu
sbit DoutCS = P3^4;
#define Dout0 PalD0
#define Dout1 PalD1
#define Dout2 PalD2
#define Dout3 PalD3
#define Dout4 PalD4
#define Dout5 PalD5
#define Dout6 PalD6

// Analogove vstupy
#define AIN_COUNT 8                    // Pocet analogovych vstupu
#define AIN_REFERENCE 500               // Referencni napeti AD prevodniku v setinach voltu

// I2C
sbit PalSCL=P3^7;

// Reproduktor
#define REPRO   PalD7

// SPI
#define PalSO  DisplayDI
#define PalSCK DisplayRW
#define PalSI  DisplayE

// Vnitrni Flash
sbit PalFlashCS = P2^1;

/*// Vnitrni EEPROM
sbit PalEepCS = P2^1;
#define PalEepSO DisplayDI
#define PalEepSCK DisplayRW
#define PalEepSI DisplayE

// Vnejsi EEPROM (modul)
sbit PalXmemCS = P3^3;
sbit PalXmemSO = P3^2;
#define PalXmemSCK DisplayE
#define PalXmemSI DisplayRW*/

// AD prevodniky
sbit AinCS1 = P3^6;
sbit AinCS2 = P3^5;



//-----------------------------------------------------------------------------
// Radic displeje Samsung Ks0108b
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
sbit DisplayDI =    PalDI;
sbit DisplayRW =    PalRW;
sbit DisplayE =     PalE;
sbit DisplayCS1 =   P0^2;
sbit DisplayCS2 =   P0^1;
sbit DisplayRESET = P0^0;
#define DisplayD0   PalD0
#define DisplayD1   PalD1
#define DisplayD2   PalD2
#define DisplayD3   PalD3
#define DisplayD4   PalD4
#define DisplayD5   PalD5
#define DisplayD6   PalD6
#define DisplayD7   PalD7


// Podminena kompilace

// Datova sbernice DisplayDATA 1:1
//#define KS_BUS     1
// Obecne funkce
#define KS_ON      1


//-----------------------------------------------------------------------------
// Displej - podminena kompilace
//-----------------------------------------------------------------------------

// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ xdata

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Cary, obdelniky atd.
#define DISPLAY_SET_AREA_FAST     1
//#define DISPLAY_SET_AREA          1
//#define DISPLAY_HOR_LINE          1
//#define DISPLAY_FAST_VERT_LINE    1
//#define DISPLAY_VERT_LINE         1
//#define DISPLAY_PIXEL             1
//#define DISPLAY_LINE              1
//#define DISPLAY_FAST_BARGRAF      1

// Fonty - pouziti jednotlivych fontu
#define DISPLAY_TAHOMA8           1
#define DISPLAY_LUCIDA6           1
//#define DISPLAY_MYRIAD32          1
//#define DISPLAY_MYRIAD40          1

// Vety - pouziti jednotlivych zarovnani u vet
#define DISPLAY_STRING            1
//#define DISPLAY_STRING_RIGHT      1
#define DISPLAY_STRING_CENTER     1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER     1
#define DISPLAY_NUMBER            1
//#define DISPLAY_CHAR_FORMATED     1

// Symbol
#define DISPLAY_SYMBOL            1

// Editace
#define DISPLAY_EDIT              1
//#define DISPLAY_EDIT_TEXT         1

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
//#define ObsluzModEditace()
#define DisplayEditExecute(Znaky) PipniKlavesnice()



//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// Podminena kompilace:

//#define DISPLAY_BARGRAF     1

// Dialog
#define DISPLAY_DIALOG 1
//#define DIALOG_OTOCENE_KLAVESY 1        // Zda se ma otocit zobrazeni tlacitek Enter a Esc

// Menu
#define DISPLAY_MENU        1
#define MENU_MAX_ITEMS      8             // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
//#define DISPLAY_CHOOSE_ITEM 1

// Jazyky
//#define DISPLAY_LANGUAGES   1             // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka



//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 11.0592 MHz
//#define FXTAL 11059200L
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define T2_X2      2            // timer 2 v rezimu X2 - v X2 modu zvlada RS232 rychlost 38400 baudu/s
#define SI_X2      1            // COM v rezimu X2
#define PCA_X2     2            // PCA v rezimu X2
#define WD_X2      1            // WatchDog v rezimu X2
#define SPI_X2     1            // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost - nechci

// Casovac 0 :
#define TIMER0_PERIOD 10        // perioda casovace 0 v ms
// Casovac 1 - pro mereni odporu 1-4kHz
#define TIMER1_PERIOD 1         // perioda casovace 1 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM


//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       O2AccuOk()      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

//#define IEP_READ_BYTE     1        // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1        // zapis jednoho bytu





//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------

#define IicSCL  PalSCL        // I2C hodiny
#define IicSDA  DisplayE      // I2C data

// casove prodlevy

#define IIC_WAIT   {byte i; for( i = 6; i > 0; i--);}  // 4takty + doplnit NOPy na 5us

// podmineny preklad :

//#define IIC_READ    1           // cteni dat se sbernice



//-----------------------------------------------------------------------------
// Pripojeni DA prevodniku TDA8444 pres I2C sbernici
//-----------------------------------------------------------------------------

#define DAC_ADDRESS 0           // adresa zadratovana piny A0..A2

// Pro DIL pouzdro:
//#define DAC_BASE_ADDRESS 0x40          // zaklad adresy prevodniku
//#define DAC_MASK_ADDRESS 0x0E          // poloha adresy pinu A0..A2
// Pro SMD pouzdro:
#define DAC_BASE_ADDRESS 0x48          // zaklad adresy prevodniku - pro SMD se lisi, A2 je vzdy 1
#define DAC_MASK_ADDRESS 0x06          // poloha adresy pinu A0..A1

// podmineny preklad :

#define DAC_SINGLE_WRITE 1      // zapis do jednoho prevodniku
//#define DAC_BURST_WRITE  1      // zapis do vsech prevodniku naraz



//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

//#define RS232_BAUD      9600          // Rychlost linky v baudech
#define RS232_BAUD      38400          // Rychlost linky v baudech
#define RS232_USE_PARITY    1         // Provoz s paritou nebo bez

#define RS232_PARITY_EVEN   1         // Suda parita
//#define RS232_PARITY_ODD   1        // Licha parita

//#define RS232_CASOVAC_1    1        // K casovani linky se vyuzije casovac 1
#define RS232_CASOVAC_2    1          // K casovani linky se vyuzije casovac 2 (pouzitelne jen u C51RD2, ne u maleho Atmela)
//#define RS232_BRL          1          // K casovani linky se vyuzije registr BRL (pouzitelne jen u ATC51ED2, ne u maleho Atmela ani TC51RD2)

//#define RS232_GET           1         // Zda se pouziva cteni ze seriove linky
#define RS232_SEND          1         // Zda se pouziva zapis na seriovou linku




// Casovani - musi byt kvuli klavesnici
void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) SysDelay( ms)             // zpozdeni 1ms z modulu Main





//-----------------------------------------------------------------------------
#include "O2HW.h"         // projektove nezavisle definice - dal jsem si to do adresare projektu, namisto do spol. adresare inc, kde je to neprehledne
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
#endif
