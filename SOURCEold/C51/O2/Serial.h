//*****************************************************************************
//
//    Serial.c - Seriova linka
//    Version 1.0
//
//*****************************************************************************


#ifndef __Serial_H__
   #define __Serial_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#define SERIAL_PERIODA_MS 100           // Perioda vysilani v ms

void SerialInit();
  // Inicializuje seriovou komunikaci

void SerialOnline();
  // Posle data

#endif
