//*****************************************************************************
//
//    PalPCCfg.h - Logger configuration load/save
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Cfg_H__
   #define __Cfg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

bit CfgLoad();
// Nacte konfiguraci z EEPROM
// Vraci NO, pro neplatnou konfiguraci (dosazeni default hodnot)

bit CfgSave( dword mask);
// Ulozi polozky definovane maskou do EEPROM
// Maska se ORuje z konstant ACFG_... viz Auto.h
// Vraci NO, nepovedl-li se zapis

#endif
