//*****************************************************************************
//
//    Ohrev.c - Ohrev lambda sondy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Ohrev_H__
   #define __Ohrev_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#define OHREV_NAPETI          10        // Napeti ve voltech, ktere se pouziva pro ohrev
#define OHREV_START_NAPETI    5         // Napeti ve voltech, od ktereho zacinam s ohrevem

#define OHREV_VYKON_MIN       0         // Minimalni vykon v %
#define OHREV_VYKON_MAX       100       // Minimalni vykon v %

// Regulator
#define OHREV_REG_PERODA_MS   400       // Perioda regulatoru v ms
#define OHREV_REG_CILOVY_RI   80        // Cilovy Ri v ohmech, na ktery reguluju
#define OHREV_REG_KP          Konfigurace.OhrevKp   // Zesileni regulatoru * 1000
#define OHREV_REG_TI_MS       Konfigurace.OhrevTi   // Integracni cas regulatoru v milisekundach
#define OHREV_REG_PROP_MAX    99        // Maximalni hodnota proporcialni slozky
#define OHREV_REG_PROP_MIN    -99       // Minimalni hodnota proporcialni slozky
#define OHREV_REG_INT_MAX     100 * 100 // Maximalni hodnota integracni slozky
#define OHREV_REG_INT_MIN     100 * 0   // Minimalni hodnota integracni slozky

// Rampa - max 0.4V/s
#define PrepoctiNapetiNaStridu(Napeti) (Napeti * 100.0 / OHREV_NAPETI)
#define RAMPA_KROK_VYKONU PrepoctiNapetiNaStridu(0.4 * OHREV_REG_PERODA_MS / 1000.0)  // Krok vykonu na 1 periodu tak, aby se dodrzela rampa 0.4V/s
#define RAMPA_KONECNY_RI 90                    // Pri dosazeni tohoto Ri ukoncim rampu


typedef enum {
  OHREV_VYPNUT,                         // Ohrev je vypnuty
  OHREV_RAMPA,                          // Rampa po startu
  OHREV_AUTO,                           // Automaticky rezim (regulator)
  _OHREV_POCET
} TRezimOhrevu;

typedef struct {
  int Proporcional;                     // proporcionalni slozka
  long Integral;                        // integralni slozka * 100
} TRegulator;

typedef struct {
  TRezimOhrevu Rezim;                   // V jakem rezimu ohrev pracuje
  TRegulator Regulator;                 // Aktualni parametry regulace
  byte Vykon;                           // Aktualni vykon ohrevu v % (0-100)
} TOhrev;
extern TOhrev __xdata__ Ohrev;

void OhrevInit();

void OhrevStart();
  // Odstartuje vyhrivani lambdy

void OhrevObsluha();
  // Obsluha ohrevu podle aktualniho rezimu

void OhrevStop();
  // Zastavi vyhrivani lambdy

#endif
