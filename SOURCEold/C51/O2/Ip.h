//*****************************************************************************
//
//    Ip.c - Mereni Ip
//    Version 1.0
//
//*****************************************************************************


#ifndef __Ip_H__
   #define __Ip_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

typedef struct {
  // Prumerovani
  word Sum;                             // Suma
  byte Count;                           // Pocet prvku v sume

  // Zobrazeni
  int  Value;                           // Vypoctena hodnota Ip v mA na desetiny
  byte Conversion;                      // Nacteny prevod
} TIp;
extern TIp __xdata__ Ip;

// Kalibrace
#define IP_KALIBRACE_MIN        -443
#define IP_KALIBRACE_MIN_PREVOD 31
#define IP_KALIBRACE_MAX        420
#define IP_KALIBRACE_MAX_PREVOD 221

#define IP_PERIODA_MS           10       // Perioda mereni Ip v ms

void IpInit();

void IpRead();

#endif
