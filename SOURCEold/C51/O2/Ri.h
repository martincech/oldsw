//*****************************************************************************
//
//    Ri.c - Mereni Ri
//    Version 1.0
//
//*****************************************************************************


#ifndef __Ri_H__
   #define __Ri_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

typedef struct Mereni {
  // Prumerovani
  word SumaPrevod;
  byte PocetPrevod;
  byte Nacteno;                        // Flag, ze uz se nacetl potrebny pocet vzorku a v promenne Prevod je nova platna hodnota
  // Vypocteny prumer mereni
  byte Prevod;
} TMereni;

typedef struct {
  TMereni H;
  TMereni L;

  word Hodnota;                         // Namerena hodnota Ri v ohmech
} TRi;
extern TRi __xdata__ Ri;

// Pri kalibraci jsem nameril tyto hodnoty: 50ohm = rozdil 49, 200ohm = rozdil 190 (zesileni zesilovace 40)
#define RI_KALIBRACE_MIN        50
#define RI_KALIBRACE_MIN_PREVOD 49
#define RI_KALIBRACE_MAX        200
#define RI_KALIBRACE_MAX_PREVOD 190

#define RI_PRUMEROVANI 50               // Jak moc se prumeruje (pri 1kHz mam casu dost)


void RiInit();

void RiRead();

#endif
