//*****************************************************************************
//
//    Nernst.c - Mereni napeti Nernstova clanku
//    Version 1.0
//
//*****************************************************************************


#ifndef __Nernst_H__
   #define __Nernst_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

typedef struct {
  // Prumerovani
  word Sum;                             // Suma
  byte Count;                           // Pocet prvku v sume

  // Zobrazeni
  int  Value;                           // Vypoctena hodnota napeti v mV
  byte Conversion;                      // Nacteny prevod
} TNernstVoltage;
extern TNernstVoltage __xdata__ NernstVoltage;

// Kalibrace
#define NERNST_KALIBRACE_MIN        0
#define NERNST_KALIBRACE_MIN_PREVOD 0
#define NERNST_KALIBRACE_MAX        5000  // Reference je 5V
#define NERNST_KALIBRACE_MAX_PREVOD 0xFF

#define NERNST_PRUMEROVANI          1

void NernstInit();

void NernstRead();

#endif
