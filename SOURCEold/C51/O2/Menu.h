//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_VSE        0xFF        // Zobrazi se vse (jsou nastaveny vsechny bity)
#define ZOBRAZIT_MRIZKA     0x01        // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_PERIODICKY 0x02        // Zobrazi se periodicke veci (bargrafy v 2D rezimu atd)

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit



/*// Co se zobrazuje na displeji behem vazeni
typedef enum {
  ZOBRAZENI_NEVAZISE,
  ZOBRAZENI_VAZENI,
  ZOBRAZENI_STATISTIKA,
  ZOBRAZENI_HISTOGRAM,
  ZOBRAZENI_HISTORIE_STATISTIKA,
  ZOBRAZENI_HISTORIE_HISTOGRAM,
  _ZOBRAZENI_COUNT
} TZobrazeni;
extern TZobrazeni __xdata__ Zobrazeni;          // Co se ma prave zobrazovat*/




void MenuInit();
// Inicializace zobrazeni

void MenuZobraz();

void Menu();

#endif
