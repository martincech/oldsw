//*****************************************************************************
//
//    Ain.c - Analogove vstupy
//    Version 1.0
//
//*****************************************************************************


#ifndef __Ain_H__
   #define __Ain_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


void AinInit();
  // Inicializuje AD prevodniky

byte AinRead(byte Kanal);
  // Precte analogovy vstup, vstup muze byt 0 az AIN_COUNT-1

void AinZmerPrevodRi();

#endif
