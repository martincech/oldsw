//*****************************************************************************
//
//    O2HW.h  - Logger independent hardware descriptions
//    Verze 1.0
//
//*****************************************************************************

#ifndef __O2HW_H__
   #define __O2HW_H__




//-----------------------------------------------------------------------------
// Displej - parametry
//-----------------------------------------------------------------------------

#define SIRKA_DISPLEJE 128
#define VYSKA_DISPLEJE 64


//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

// definice klaves :
enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // Klavesy, v zavislosti na konstrukci klavesnice :
  K_ESC = _K_FIRSTUSER,
  K_UP,
  K_DOWN,
  K_LEFT,
  K_ENTER,
  K_RIGHT,
  K_NULA,                     // Nepouziva se, jen kvuli fci Editace()

  // Udalosti
  _K_EVENTS    = 0x60,        // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,   // prekresleni namerenych hodnot
  K_BLINK_OFF,                // periodicke blikani - zhasni
  K_BLINK_ON,                 // periodicke blikani - rozsvit
  K_TIMEOUT,                  // vyprsel cas necinnosti
  K_SERIAL,                   // Mam obslouzit seriovy port

  // systemove klavesy :
  K_REPEAT       = 0x80,   // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,   // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF    // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (400/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch




//-----------------------------------------------------------------------------
// Pripojeni ADC 0838
//-----------------------------------------------------------------------------

#define AdcDIO  DisplayRW        // DI + DO spolecna data
#define AdcCLK  DisplayDI        // CLK hodiny

// casove prodlevy
#define AdcWait() _nop_();_nop_();_nop_();_nop_();   // max. hodinovy kmitocet 400kHz - doplnit NOPy na 1.25us

// ovladani chipselectu - pro vice prevodniku nahradit prazdnym makrem

#define AdcEnable()         // externi chipselect
#define AdcDisable()




//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __O2_H__
  #include "O2.h"                   // Dal jsem si to do adresace projektu, ne do spolecneho inc, kde je to neprehledne
#endif
//-----------------------------------------------------------------------------

#endif
