//*****************************************************************************
//
//    O2.h - common data definitions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __O2_H__
   #define __O2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif



//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE    0x0100       // verze SW - POZOR, musi odpovidat promenne STR_CISLO_VERZE v main.c51

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Pro promennou ModEditace - co se edituje
enum {
  EDITACE_NORMAL,
  EDITACE_PODSVIT
};


typedef struct {
  byte blank;
} TData;
extern TData xdata Data;



//-----------------------------------------------------------------------------
// Konfigurace
//-----------------------------------------------------------------------------

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                           // VERZE

  // Regulace ohrevu
  long OhrevKp;                         // Zesileni regulatoru * 1000
  long OhrevTi;                         // Integracni cas regulatoru v milisekundach

  // Regulace napeti Nernstova clanku
  long RegKp;                          // Zesileni regulatoru * 1000
  long RegTi;                          // Integracni cas regulatoru v milisekundach

  byte PrumerovaniIp;                   // Prumerovani mereni Ip

  byte KontrolniSuma;                   // Kontrolni soucet
} TKonfigurace;

extern TKonfigurace __xdata__ Konfigurace; // Buffer konfigurace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast
#define CFG_ALL                       0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERZE                     0x00000001L
#define CFG_OHREVKP                   0x00000002L
#define CFG_OHREVTI                   0x00000004L
#define CFG_REGKP                     0x00000008L
#define CFG_REGTI                     0x00000010L
#define CFG_PRUMEROVANI_IP            0x00000020L




//-----------------------------------------------------------------------------
// Struktura cele interni EEPROM
//-----------------------------------------------------------------------------
#define IEP_SPARE (IEP_SIZE - sizeof(TKonfigurace))  // Velikost mezery na konci interni EEPROM

typedef struct {
  TKonfigurace Konfigurace;             // Konfigurace

  unsigned char Volne[IEP_SPARE];
} TIntEEPROM;




//-----------------------------------------------------------------------------
// Include potrebnych modulu, povoluje se v Hardware.h
//-----------------------------------------------------------------------------









#endif
