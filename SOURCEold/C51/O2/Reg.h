//*****************************************************************************
//
//    Reg.c - Regulace Nernstova napeti
//    Version 1.0
//
//*****************************************************************************


#ifndef __Reg_H__
   #define __Reg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

// Regulator
#define REG_PERIODA_MS  10        // Perioda regulatoru v ms
#define REG_CILOVE_U    2950      // Cilove napeti Nernstova clanku v mV (450mV + stred 2.5V)
#define REG_KP          Konfigurace.RegKp   // Zesileni regulatoru * 1000
#define REG_TI_MS       Konfigurace.RegTi   // Integracni cas regulatoru v milisekundach
#define REG_PROP_MAX    63        // Maximalni hodnota proporcialni slozky
#define REG_PROP_MIN    -63       // Minimalni hodnota proporcialni slozky
#define REG_INT_MAX     100 * 63  // Maximalni hodnota integracni slozky
#define REG_INT_MIN     100 * 0   // Minimalni hodnota integracni slozky

#define VYSTUP_MIN      0
#define VYSTUP_MAX      0x3F

#define VYSTUP_DEFAULT  (VYSTUP_MAX / 2)  // Default nastavim a midpoint 2.5V

#define VYSTUP_CISLO    0         // Cislo vystupu DAC prevodniku


typedef struct {
  int Proporcional;                     // proporcionalni slozka
  long Integral;                        // integralni slozka * 100
} TRegulatorN;

typedef struct {
  TRegulatorN Regulator;                // Aktualni parametry regulace
  byte Vystup;                          // Aktualni vystup
} TRegNernst;
extern TRegNernst __xdata__ RegNernst;

void RegInit();

void RegReset();

void RegObsluha();


#endif
