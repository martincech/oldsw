//*****************************************************************************
//
//    Auto.h - Auto common data definitions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Auto_H__
   #define __Auto_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
   #include <stddef.h>        // makro offsetof
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define AMONITOR_VERZE    0x0100       // verze SW
#define AUTO_MAGIC    0x55AA       // identifikace konfiguracniho souboru

//-----------------------------------------------------------------------------
// Podsvit
//-----------------------------------------------------------------------------

#define MAX_PODSVIT 4
#define MIN_PODSVIT 1
extern byte Podsvit;                    // Definovano v main

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

// identifikace teplomeru :
// Pozor, musi pozicne odpovidat konstantam MWI_CH0..MWI_CH4
// 6.3.2003: Musi take zacinat od nuly, protoze je pouzivam jako indexy v poli pro prumerovani teplot
typedef enum {
   ATEMP_PREDNI,         // T1
   ATEMP_STREDNI,        // T2
   ATEMP_ZADNI,          // T3
   ATEMP_KANAL,          // T4
   ATEMP_VENKOVNI,       // T5
   ATEMP_VYFUKOVANA      // T6
} TThermoAddress;

#define ATEMP_COUNT 6    // Celkovy pocet merenych teplot - kvuli poli pro prumerovani

#define AUTO_TEMP_ERROR   (-100<<8)  // chyba mereni -100C misto merene hodnoty

// Teplota : horni byte je v celych stupnich,
// dolni MSB 0.5C...0.0625C 0 0 0 0 LSB
// Na float se prevede jako temp/256

//-----------------------------------------------------------------------------
// Serva
//-----------------------------------------------------------------------------

// identifikace serv :
// Pozor, musi pozicne odpovidat konstantam SERVO_0..SERVO_7

typedef enum {
   ASER_NAPOR1,          // Napor 1
   ASER_NAPOR2,          // Napor 2
   ASER_RECIRKULACE,     // Recitkulace
   ASER_PODLAHA1,        // Podlaha 1
   ASER_PODLAHA2,        // Podlaha 2
   ASER_PODLAHA3,        // Podlaha 3
   ASER_PODLAHA4,        // Podlaha 4
   ASER_TOPENI           // Topeni
} TServoAddress;

// hlaseni stavu serv :

typedef enum {
   ASER_OK,              // v poradku, souhlasi pozadovana poloha a zpetne hlaseni
   ASER_RUNNING,         // nesouhlasi pozadovana poloha a zpetne hlaseni, zatim v casovem limitu
   ASER_TIMEOUT,         // nesouhlasi pozadovana poloha a zpetne hlaseni, prekrocen cas
   ASER_CONTROL,         // ridici napeti (pozadovana poloha) pod limitem (utrzeny potenciometr)
   ASER_OFF              // vypnuto napajeni serva (manualni provoz)
} TServoStatus;

// Stav serv pro logovani (beh serva v limitu me tam nezajima, polohy 0-3 vyhodne zakoduju do 2 bitu)
typedef enum {
   SERVO_OK,             // v poradku, souhlasi pozadovana poloha a zpetne hlaseni, pripadne servo jede, ale jeste je v casovem limitu
   SERVO_TIMEOUT,        // nesouhlasi pozadovana poloha a zpetne hlaseni, prekrocen cas, zaseknute servo
   SERVO_CONTROL,        // ridici napeti (pozadovana poloha) pod limitem (utrzeny potenciometr)
   SERVO_MANUAL          // vypnuto napajeni serva (manualni provoz)
} TServoStatusLog;


// Hodnoty mereni potenciometru a zpetneho hlaseni 0..15

//-----------------------------------------------------------------------------
// Akumulator
//-----------------------------------------------------------------------------

// Napeti na akumulatoru na cele volty

// Proud z akumulatoru, cele ampery se znamenkem

//-----------------------------------------------------------------------------
// Ventilator
//-----------------------------------------------------------------------------

// Napeti na ridicim potenciometru ventilatoru je 0..15


//-----------------------------------------------------------------------------
// Prepinac Fresh air
//-----------------------------------------------------------------------------

typedef enum {
  FRESH_AIR_AUTO,
  FRESH_AIR_20,
  FRESH_AIR_40,
  FRESH_AIR_60,
  FRESH_AIR_80,
  FRESH_AIR_100
} TFreshAirSwitchStatus;



//-----------------------------------------------------------------------------
// Digitalni vstupy
//-----------------------------------------------------------------------------

// Prepinac rezimu :

typedef enum {
   ASW_VYPNUTO,         // neni zapnuna zadna z voleb, prepinac je v poloze VYP
   ASW_VENTILACE,
   ASW_AUTOMAT,
   ASW_CHLAZENI_RUCNE,
   ASW_TOPENI_RUCNE
} TSwitchStatus;

//-----------------------------------------------------------------------------
// Logger
//-----------------------------------------------------------------------------

// Perioda ukladani je v minutach
// Struktura v EEPROM viz komentare a konstanty AutoLog


//-----------------------------------------------------------------------------
// Regulator
//-----------------------------------------------------------------------------

// Komentar k velicinam viz AutoData, AutoConfig

//-----------------------------------------------------------------------------
// Diesel
//-----------------------------------------------------------------------------

typedef word TAdslMotoHour;      // datovy typ pro motohodiny
                                 // pri zmene typu POZOR na prirazovani do TFarData

#define ADSL_COUNT        10                      // pocet polozek pole hodnot

// Datovy typ pro zapis do iEEPROM :

typedef TAdslMotoHour TAdslArray[ ADSL_COUNT];

// Motohodiny jsou v hodinach

#define CEKANI_PO_NASTARTOVANI_DIESELU 40    // Kolik sekund cekat po nastartovani dieselu na vyhodnocovani chyby dieselu (musi zahrnovat dobu zhaveni)

//-----------------------------------------------------------------------------
// Include potrebnych modulu, povoluje se v Hardware.h
//-----------------------------------------------------------------------------

#ifdef AUTO_TEMPERATURE
#ifndef __Thermo_H__
   #include "..\inc\Thermo.h"
#endif
#endif // AUTO_TEMPERATURE
#ifdef AUTO_DIO
#ifndef __Adio_H__
   #include "Amisc\Adio.h"
#endif
#endif // AUTO_DIO
#ifdef AUTO_LOG
#ifndef __Alog_H__
   #include "Amisc\Alog.h"
#endif
#endif // AUTO_LOG
#ifdef AUTO_PRT
#ifndef __Aprt_H__
   #include "Amisc\Aprt.h"
#endif
#endif // AUTO_PRT
#ifdef AUTO_CONFIG
#ifndef __Acfg_H__
   #include "Amisc\Acfg.h"
#endif
#endif // AutoCONFIG

//-----------------------------------------------------------------------------
// Globalni komunikacni struktura
//-----------------------------------------------------------------------------

// 27.5.2003: Typ pro rezim chlazeni aut  zda se prave topi nebo chladi
typedef enum {
  AUTOREZIM_TOPENI,
  AUTOREZIM_CHLAZENI
} TAutomatickyRezim;

// Komunikacni struktura pro zobrazeni/logovani :

typedef struct {
   // merene teploty :
   int16  TeplotaVenkovni;
   int16  TeplotaKanal;
   int16  TeplotaPredni;
   int16  TeplotaStredni;
   int16  TeplotaZadni;
   int16  TeplotaVyfukovana;
   // zpetna hlaseni serv:
   byte ServoTopeni;                    // 0 az 15
   // potenciometry:
   byte PotTopeni;
   // stav serv TServoStatus:
   byte StavNapor1;
   byte StavNapor2;
   byte StavRecirkulace;
   byte StavPodlaha1;
   byte StavPodlaha2;
   byte StavPodlaha3;
   byte StavPodlaha4;
   byte StavTopeni;
   // mereni akumulatoru:
   byte AkuNapeti;
   byte AkuProud;
   // stavove informace :
   byte PrepinacRezimu;           // TSwitchStatus
   byte PrepinacFreshAir;         // TFreshAirSwitchStatus
   byte NastartovanyDiesel;       // YES/NO
   byte NastartovanyElektromotor; // YES/NO
   byte DobijeniAku;              // YES/NO
   byte SepnuteChlazeni;          // YES/NO
   byte PoruchaNapajeni;          // YES/NO (Battery Low, jen pro EEPROM)
   byte VymenaOleje;              // YES/NO
   byte VymenaFiltru;             // YES/NO
   byte StavNafty;                // Stav nafty v pridavne nadrzi (0 az ADSL_FUEL_STEPS-1)
   byte PolovicniVykonVentilatoru;// YES/NO polovicni vykon ventilatoru (NO=plny vykon)
   // Poruchy
   byte PoruchaVentilatoru;       // YES/NO
   byte PoruchaTopeni;            // YES/NO
   byte PoruchaChlazeni;          // YES/NO  10.2.2003
   byte PoruchaDobijeni;          // YES/NO 18.9.2003
   byte PoruchaDieselu;           // YES/NO
   // stav regulatoru :
   byte RegSensorError;           // YES/NO chyba mereni teploty
   byte Topeni;                   // 0..DAC_VALUE_MAX akcni velicina na topeni
   byte ZapnutoChlazeni;          // YES/NO indikace zapnuti chlazeni
   // motohodiny :
   word MotoHodiny;               // muze se zobrazovat jako tachometr
   // Alarm (je nejaka porucha) :
   byte Alarm;                    // YES/NO
   // RTC :
   byte NastavenyHodiny;          // YES/NO zmena nastaveni hodin, nuluje se po zapisu do Logu
   // 27.5.2003: Automaticky rezim topeni nebo chlazeni - v poloze chlazeni aut to topi i chladi
   TAutomatickyRezim AutomatickyRezim;
   // 18.6.2003: Zobrazeni diagnostiky
   byte ZobrazitDiagnostiku;      // YES/NO zda behem regulace zobrazovat diagnostiku

   // Nove pridane veci ve verzi 2.00:
   byte CerstvyVzduch;            // Mnozstvi cerstveho vzduchu v procentech (20-100%)
   byte PoruchaTeplotaPredni;     // Zda je teplota vepredu skrine mimo zadane pasmo
   byte PoruchaTeplotaStredni;    // Zda je teplota uprostred skrine mimo zadane pasmo
   byte PoruchaTeplotaZadni;      // Zda je teplota vzadu skrine mimo zadane pasmo

   // Nove pridane veci ve verzi 2.02:
   byte VysokyVykonTopeni;        // Zda je vykon topeni vysoky (75C) nebo nizky (65C)
} TAutoData;

extern TAutoData __xdata__ AutoData;      // komunikacni struktura v externi RAM (pro modul Fifo)

//-----------------------------------------------------------------------------
// Struktura zaznamu Logu
//-----------------------------------------------------------------------------

// Komprimovany datum a cas :

typedef struct {
   byte Min        : 6;
   byte Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3;     // horni  3 bity
   byte Day        : 5;
   byte Month      : 4;
   byte Year       : 3;     // rok % 8
   byte RtcChanged : 1;     // priznak zmeny nastaveni RTC
} TShortDateTime;   // Velikost 3 bajty

// Ukladani roku: z roku se do struktury ukladaji pouze spodni 3 bity. Rekonstrukce pak probiha tak, ze hornich 13 bitu se vezme z aktualniho roku
// a doplni se temito 3 bity ze struktury a mam rok zrekonstruovany. Pokud je takto vznikly rok vetsi nez aktualni rok, odecte se od nej 8,
// protoze novejsi datum nez aktualni to byt nemohl. Funguje to pro az 8 let stare zaznamy.

// zapis hodin do struktury :
#define mkhour( dt, h)     dt.Hour_lo = (h) & 0x03; dt.Hour_hi = ((h) >> 2) & 0x07
// cteni hodin ze struktury :
#define gethour( dt)       ((dt.Hour_lo) | (dt.Hour_hi << 2))
// zapis roku do struktury :
#define mkyear( dt, y)     dt.Year = (y) & 0x07

// vlastni struktura logu :

typedef struct SAutoLog {
   // merene teploty :
   char  TeplotaVenkovni;
   char  TeplotaKanal;
   char  TeplotaPredni;
   char  TeplotaStredni;
   char  TeplotaZadni;
   char  TeplotaVyfukovana;
   // teploty pro regulator :
   char TeplotaCilova;            // pozadovana teplota regulatoru, cele stupne


   // Stavy jednotlivych serv - TServoStatusLog, na 2 bity
   byte StavNapor1       : 2,
        StavNapor2       : 2,
        StavRecirkulace  : 2,
        StavTopeni       : 2;
   byte StavPodlaha1     : 2,
        StavPodlaha2     : 2,
        StavPodlaha3     : 2,
        StavPodlaha4     : 2;

   // Zpetne hlaseni serva topeni a potenciometr topeni:
   byte ServoTopeni      : 4,
        PotTopeni        : 4;

   // mereni akumulatoru :
   byte AkuNapeti;
   byte AkuProud;

   // Stavove informace
   byte PrepinacRezimu           : 3,
        PrepinacFreshAir         : 3,
        NastartovanyDiesel       : 1,
        NastartovanyElektromotor : 1;

   byte DobijeniAku              : 1,
        SepnuteChlazeni          : 1,
        VymenaOleje              : 1,
        VymenaFiltru             : 1,
        StavNafty                : 4;

   byte PolovicniVykonVentilatoru : 1,
        PoruchaVentilatoru        : 1,
        PoruchaTopeni             : 1,
        PoruchaChlazeni           : 1,
        PoruchaDobijeni           : 1,
        PoruchaDieselu            : 1,
        AutomatickyRezim          : 1,
        Dummy2                    : 1;

   TShortDateTime DateTime;           // cas zaznamu
   byte CerstvyVzduch;
} TAutoLog;  // Delka 19 bajtu

extern TAutoLog __xdata__ AutoLog;    // buffer logu v externi RAM

// Konstanty pro ukladani do EEPROM FIFO :

#define AUTO_FIFO_START            0  // Pocatecni adresa FIFO v EEPROM
#define AUTO_FIFO_CAPACITY       1600 // Maximalni pocet ukladanych polozek - klesla mne velikost zaznamu z 20 na 19 bajtu => mohl bych i vic, ale zatim to staci
#define AUTO_FIFO_MARKER_EMPTY  -127  // Znacka konce pri neuplnem zaplneni - teplota
#define AUTO_FIFO_MARKER_FULL   -128  // znacka konce pri prepisovani
#define AUTO_FIFO_PROTOCOL_TYPE -126  // Pokud je prvni bajt zaznamu roven tomuto, jde o zaznam s protokolem cinnosti ridice. Pokud ne, jde o normalni zaznam

// Datovy typ popisujici strukturu logu (v EEPROM) :

typedef TAutoLog TAutoLogArray[ AUTO_FIFO_CAPACITY];   // datovy logger

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

typedef struct {
   // interni data, pro ulozeni v EEPROM
   word Magic;                        // Identifikace souboru
   word Verze;                        // AUTO_VERSION
   byte IdentifikacniCislo;           // identifikace zarizeni
   // kontrolovane meze teplot :
   byte MaxOdchylkaSkrin;               // Maximalni odchylka teploty ve skrini od cilove teploty
   // mothodiny :
   word PeriodaVymenyOleje;           // perioda motohodin do vymeny oleje
   word PeriodaVymenyFiltru;          // perioda motohodin do vymeny filtru
   word VymenaOleje;                  // pocet motohodin do vymeny oleje  (interni udaj, neni v menu)
   word VymenaFiltru;                 // pocet motohodin do vymeny filtru (interni udaj, neni v menu)
   // regulator :
   char   TeplotaCilova;              // pozadovana teplota regulatoru, cele stupne
   long32 RegKp;                      // zesileni 1000 == 1*
   long32 RegTi;                      // integracni cas s
   int16  Hystereze;                  // hystereze chlazeni C * 256
   // Logger :
   byte PeriodaUkladani;              // perioda ukladani min
   // 30.5.2003: Hystereze pro prechod mezi automatickym topenim/chlazenim v rezimu chlazeni aut.
   int16 HysterezePrechodu;           // Hystereze pro prechod C * 256
   byte Naves;                        // Zda jde o naves nebo nakladni vozidlo (u tahace se zobrazuje navic stav nafty v nadrzi)

   // interni data, pro ulozeni v EEPROM
   byte KontrolniSuma;                // kontrolni soucet
} TAutoConfig;

extern TAutoConfig __xdata__ AutoConfig; // buffer konfigurace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
#define ACFG_ALL                     0xFFFFFFFFL    // uloz vsechno
#define ACFG_MAGIC                   0x00000001L
#define ACFG_VERZE                   0x00000002L
#define ACFG_ODCHYLKA_SKRIN          0x00000004L
#define ACFG_PERIODA_VYMENY_OLEJE    0x00000080L
#define ACFG_PERIODA_VYMENY_FILTRU   0x00000100L
#define ACFG_VYMENA_OLEJE            0x00000200L
#define ACFG_VYMENA_FILTRU           0x00000400L
#define ACFG_TEPLOTA_CILOVA          0x00000800L
#define ACFG_REG_KP                  0x00002000L
#define ACFG_REG_TI                  0x00004000L
#define ACFG_HYSTEREZE               0x00008000L
#define ACFG_PERIODA_UKLADANI        0x00010000L
#define ACFG_IDENTIFIKACNI_CISLO     0x00020000L
#define ACFG_HYSTEREZE_PRECHODU      0x00040000L
#define ACFG_NAVES                   0x00080000L

#define ACFG_KONTROLNI_SUMA          0x80000000L

//-----------------------------------------------------------------------------
// Protokolovani udalosti
//-----------------------------------------------------------------------------

// Typ udalosti :                       parametr udalosti

typedef enum {
   APRT_START,
   APRT_PRIHLASENI_RIDICE,              // cislo ridice
   APRT_ODHLASENI_RIDICE,               // cislo ridice
   APRT_NASTAVENI_HODIN,
   APRT_NASTAVENI_TEPLOTY,              // teplota
   APRT_NASTAVENI_MEZI_SKRINE,          // teplota
   APRT_NASTAVENI_PERIODY_UKLADANI,     // perioda
   APRT_NASTAVENI_REG_KP,               // Kp
   APRT_NASTAVENI_REG_TI,               // Ti
   APRT_NASTAVENI_HYSTEREZE,            // Hystereze * 10
   APRT_NASTAVENI_PERIODY_OLEJE,        // perioda
   APRT_NASTAVENI_PERIODY_FILTRU,       // perioda
   APRT_SMAZANI_MOTOHODIN,
   APRT_SMAZANI_LOGU,
   APRT_VYMENA_OLEJE,
   APRT_VYMENA_FILTRU,
   APRT_EXPORT_DAT,                     // YES uspesny, NO neuspesny
   APRT_TISK_ZAZNAMU,
   APRT_NASTAVENI_HYSTEREZE_PRECHODU,   // 30.5.2003: Hystereze prechodu mezi chlazenim a tpenim v rezimu chlazeni aut * 10
   APRT_NASTAVENI_NAVES,                // Provedeni naves/nakladak
   _APRT_LAST,
   // znacky pro FIFO :
   APRT_FIFO_MARKER_FULL  = 0xFE,
   APRT_FIFO_MARKER_EMPTY = 0xFF
} TAprtTyp;

// Zaznam o udalosti :

typedef struct SAutoUdalost {
   byte           TypZaznamu;           // Odpovida 1. bajtu ve strukture AutoLog
   byte           Typ;                  // typ udalosti
   dword          Parametr;             // parametr udalosti
   TShortDateTime DateTime;             // cas vzniku udalosti
   byte           Spare[sizeof(TAutoLog)-sizeof(byte)-sizeof(byte)-sizeof(dword)-sizeof(TShortDateTime)];  // Vyplneni, aby byla velikost stejna jako TAutoLog
} TAutoUdalost;

extern TAutoUdalost __xdata__ AutoUdalost;    // buffer udalosti v externi RAM

//-----------------------------------------------------------------------------
// Data v interni EEPROM
//-----------------------------------------------------------------------------

#define AUTO_IEP_SPARE1   (IEP_PAGE_SIZE - sizeof( TAutoConfig))       // vyhrazeni stranky na konfiguraci
#define AUTO_IEP_SPARE2   (IEP_SIZE                     \
                          - sizeof( TAutoConfig)        \
                          - AUTO_IEP_SPARE1             \
                          - sizeof( TAdslArray))        \


typedef struct {
   TAutoConfig       Config;                      // konfigurace
   byte              Spare1[ AUTO_IEP_SPARE1];    // rezerva pro konfiguraci
   byte              Spare2[ AUTO_IEP_SPARE2];    // rezerva
   TAdslArray        Adsl;                        // pole motohodin
} TAutoIep;

//-----------------------------------------------------------------------------
// Data v externim modulu
//-----------------------------------------------------------------------------

// Nekomprimovany datum a cas :

typedef struct {
   byte Min        : 6;
   byte Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3;     // horni  3 bity
   byte Day        : 5;
   byte Month      : 4;
   byte Dummy      : 4;     // vypln
   word Year;               // uplny rok
} TLongDateTime;

// makra shodna s TShortDateTime

// Razitko externiho modulu :

typedef struct {
   TLongDateTime DatumPorizeni;      // datum nahrani do modulu
   word MotoHodiny;                  // 17.10.2002: Celkovy pocet motohodin, ktery je v SW potreba pro stanoveni poctu motohodin do vymeny oleje/filtru.
                                     //             Navic bude treba do budoucna, protoze budou chtit vedet i celkovy pocet motohodin, kvuli spotrebe nafty.
                                     //             V podstate se sem zkopiruje promenna Motohodiny ze struktury TAutoData, jen pri kopirovani dat do modulu.
                                     //             Toto nemuze byt ve strukture Config, protoze to nema s nastavenim nic spolecneho a musela by se pokazde prepocitavat checksum.
   byte          KontrolniSuma;      // kontrolni suma modulu
} TAutoStamp;

extern TAutoStamp __xdata__ AutoStamp;        // buffer razitka v externi RAM

// Velikost rezervy v datovem modulu :

#define AUTO_XMEM_SPARE   XMEM_SIZE                     \
                          - sizeof( TAutoLogArray)      \
                          - sizeof( TAutoConfig)        \
                          - sizeof( TAutoStamp)         \

// Struktura datoveho externiho modulu :

typedef struct {
   TAutoLogArray Log;                            // zaznam dat
   TAutoConfig   Config;                         // aktualni konfigurace
   byte          Spare[ AUTO_XMEM_SPARE];        // rezerva
   TAutoStamp    Stamp;                          // zaznam o porizeni
} TAutoXmem;

//-----------------------------------------------------------------------------
#endif // __Auto_H__
