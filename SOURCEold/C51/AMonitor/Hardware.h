//*****************************************************************************
//
//    Hardware.h  - Auto final hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE

#define __TM2__             // preklad jednotky TM2
//#define __ATTY__       1        // prekladat vysilac terminalu



#define RtcMin() 0
#define RtcHour() 0
#define RtcSec() 0
#define RtcDay() 1
#define RtcMonth() 1
#define RtcYear() 2004
#define RtcSetHour(a) ;
#define RtcSetMin(a) ;
#define RtcSetSec(a) ;
#define RtcSetDay(a) ;
#define RtcSetMonth(a) ;
#define RtcSetYear(a) ;
#define RtcInit(a) ;



//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define AutoAccuOk() AdioBatteryOk()

// spolecne vodice (podle schematu) :

#define AutoDATA      P0           // datova sbernice
// teplomery na sbernici :
sbit AutoT1      = AutoDATA^0;     // teplomer T1
sbit AutoT2      = AutoDATA^1;     // teplomer T2
sbit AutoT3      = AutoDATA^2;     // teplomer T3
sbit AutoT4      = AutoDATA^3;     // teplomer T4
sbit AutoT5      = AutoDATA^4;     // teplomer T5
sbit AutoT6      = AutoDATA^5;     // teplomer T6
sbit AutoTOUT    = AutoDATA^6;     // teplomery spolecne buzeni
//sbit AutoTDV8    = AutoDATA^7;     // rezerva - digitalni vstup DV8
// digitalni vstupy na sbernici :
/*sbit AutoDV1     = AutoDATA^0;     // digitalni vstup 1
sbit AutoDV2     = AutoDATA^1;     // digitalni vstup 2
sbit AutoDV3     = AutoDATA^2;     // digitalni vstup 3
sbit AutoDV4     = AutoDATA^3;     // digitalni vstup 4
sbit AutoDV5     = AutoDATA^4;     // digitalni vstup 5
sbit AutoDV6     = AutoDATA^5;     // digitalni vstup 6
sbit AutoDV7     = AutoDATA^6;     // digitalni vstup 7*/
sbit AutoPRINT   = AutoDATA^2;     // Odblokovani tiskarny
sbit AutoLOWBAT  = AutoDATA^7;     // Hlidani napajeni

sbit    AutoPODSVIT = P1^0;        // Podsvit displeje
sbit    AutoCSCIDLA = P1^1;        // CS 1-wire
//sbit    AutoCS1     = P1^2;        // CS A/D 1
//sbit    AutoCS2     = P1^3;        // CS A/D 2
sbit    AutoSO      = P1^4;        // SO  EEPROM, DisplayA0, AdcData
sbit    AutoCSEE    = P1^5;        // CS  EEPROM
sbit    AutoSI      = P1^6;        // SI  EEPROM, DisplayRD, I2C SDA
sbit    AutoCLK     = P1^7;        // SCK EEPROM, DisplayWR, AdcCLK


/*sbit    AutoK2      = P2^0;        // K2 klavesnice
sbit    AutoK1      = P2^1;        // K1 klavesnice
sbit    AutoK0      = P2^2;        // K0 klavesnice*/
sbit    AutoRES     = P2^4;        // Reset displeje
sbit    AutoPISK    = P2^5;        // sirena
//sbit    AutoVYST1   = P2^6;        // Digitalni vystup 1
//sbit    AutoVYST2   = P2^5;        // Digitalni vystup 2
sbit    AutoVYST3   = P2^6;        // Digitalni vystup 3
sbit    AutoCSD     = P2^7;        // DisplayCS

sbit    AutoRXD     = P3^0;        // COM Rx
sbit    AutoTXD     = P3^1;        // COM Tx
//sbit    AutoSCL     = P3^2;        // I2C SCL
sbit    AutoSIMOD   = P3^3;        // SI modulu EEPROM
sbit    AutoSOMOD   = P3^4;        // CS  modulu EEPROM
sbit    AutoCLKMOD  = P3^5;        // SCK modulu EEPROM
sbit    AutoCSMOD   = P3^6;        // SO  modulu EEPROM
//sbit    AutoINSMOD  = P3^7;        // INS  modulu EEPROM
sbit    AutoRSPOWER = P3^7;        // Napajeni RS232

// Adresy A/D prevodniku 1 :
#define AutoAV1   ADC_SINGLE_CH0   // A/D prevodnik 1/0
#define AutoAV2   ADC_SINGLE_CH1   // A/D prevodnik 1/1
#define AutoAV3   ADC_SINGLE_CH2   // A/D prevodnik 1/2
#define AutoAV4   ADC_SINGLE_CH3   // A/D prevodnik 1/3
#define AutoAV5   ADC_SINGLE_CH4   // A/D prevodnik 1/4
#define AutoAV6   ADC_SINGLE_CH5   // A/D prevodnik 1/5
#define AutoAV7   ADC_SINGLE_CH6   // A/D prevodnik 1/6
#define AutoAV8   ADC_SINGLE_CH7   // A/D prevodnik 1/7
// Adresy A/D prevodniku 2 :
//#define AutoAV9   ADC_SINGLE_CH0   // A/D prevodnik 2/0
//#define AutoAV10  ADC_SINGLE_CH1   // A/D prevodnik 2/1
#define AutoAV11  ADC_SINGLE_CH2   // A/D prevodnik 2/2
#define AutoAV12  ADC_SINGLE_CH3   // A/D prevodnik 2/3
#define AutoAV13  ADC_SINGLE_CH4   // A/D prevodnik 2/4
#define AutoAV14  ADC_SINGLE_CH5   // A/D prevodnik 2/5
#define AutoAV15  ADC_SINGLE_CH6   // A/D prevodnik 2/4
#define AutoAV16  ADC_SINGLE_CH7   // A/D prevodnik 2/5
//#define AutoB1B2  ADC_DIFF_CH3     // A/D prevodnik 2/6,7
#define AutoB1B2  ADC_DIFF_CH0     // A/D prevodnik 2/0,1
// Adresy D/A prevodniku :
#define AutoAVY1   0               // D/A prevodnik 0
#define AutoAVY2   1               // D/A prevodnik 1
#define AutoAVY3   2               // D/A prevodnik 2
#define AutoAVY4   3               // D/A prevodnik 3
#define AutoAVY5   4               // D/A prevodnik 4
#define AutoAVY6   5               // D/A prevodnik 5
// Digitalni vystupy :
#define AutoDVY1   AutoVYST1       // nazev za oddelovacem
#define AutoDVY2   AutoVYST2       // nazev za oddelovacem
#define AutoDVY3   AutoVYST3       // nazev za oddelovacem


//-----------------------------------------------------------------------------
// Pripojeni Touch Screen ADS 7846
//-----------------------------------------------------------------------------

sbit TouchDCLK   =P2^0;             // hodiny DCLK
sbit TouchDIN    =P1^6;               // vstup dat DIN
sbit TouchBUSY   =P2^1;             // signalizace BUSY
sbit TouchDOUT   =P2^2;             // vystup dat DOUT
sbit TouchPENIRQ =P2^3;              // preruseni od dotyku /PENIRQ

// chipselect :
//#define TouchCS           P1^0          // chipselect /CS
//#define TouchSelect()     TouchCS = 0
//#define TouchDeselect()   TouchCS = 1

// trvale zadratovany chipselect :
#define TouchSelect()
#define TouchDeselect()

// technicke konstanty :

#define TOUCH_DELAY       4        // doba ustaleni udaje  [ms]
#define TOUCH_PEN_RISE    5        // doba po nabehu PENIRQ [ms]
#define TOUCH_PEN_FALL   15        // doba odpadeni PENIRQ [ms]
#define TOUCH_DATA_SIZE  12        // 12 bitu A/D
//#define TOUCH_DATA_SIZE   8        //  8 bitu A/D

// Rozsahy souradnic :

#define TOUCH_X_RANGE     320      // rozsah souradnice X : 0..<X-1>
#define TOUCH_Y_RANGE     240      // rozsah souradnice Y : 0..<Y-1>
#define TOUCH_XL        0x100      // surovy levy okraj
#define TOUCH_XR        0xEF0      // surovy pravy okraj
#define TOUCH_YU        0x180      // surovy horni okraj
#define TOUCH_YD        0xED0      // surovy dolni okraj
/* pro 8 bitu
#define TOUCH_XL         0x10      // surovy levy okraj
#define TOUCH_XR         0xEF      // surovy pravy okraj
#define TOUCH_YU         0x18      // surovy horni okraj
#define TOUCH_YD         0xED      // surovy dolni okraj
*/

// Pomineny preklad :

//#define TOUCH_READ_VBAT   1        // cteni vstupu Vbat
//#define TOUCH_READ_AUX    1        // cteni vstupu AUX


//-----------------------------------------------------------------------------
// Pripojeni X9313
//-----------------------------------------------------------------------------

// zapojeni pinu volne k uzivani :
sbit E2PotCS    = P1^2;    // Chip select
sbit E2PotUD    = P1^6;    // Smer Up/Down
sbit E2PotINC   = P1^3;    // Provedeni 1 kroku


//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz

#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :

#define CPU_X2     2            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define T2_X2      1            // timer 2 v rezimu X2
#define SI_X2      1            // COM v rezimu X2
#define PCA_X2     1            // PCA v rezimu X2
#define WD_X2      1            // WatchDog v rezimu X2
#define SPI_X2     1            // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :

#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost (1=ne, 2=ano)

// Casovac 0 :

#define TIMER0_PERIOD 20        // perioda casovace 0 v ms
#define TIMER1_PERIOD 1        // perioda casovace 1 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
#include "AutoHW.h"             // projektove nezavisle definice
//-----------------------------------------------------------------------------


// Casovani

void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) Delay( ms)             // zpozdeni 1ms z modulu Main

//-----------------------------------------------------------------------------

#endif // __Hardware_H__
