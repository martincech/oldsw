//*****************************************************************************
//
//    APrint.h     - Auto Printer services
//    Version 1.0, (c) P.Veit, Vymos
//
//*****************************************************************************

#ifndef __APrint_H__
   #define __APrint_H__

#ifndef __Tm2_H__
   #include "Tm2buf.h"     // konstanty a datove typy
#endif

// Globalni parametry pro PrintProtocol :

extern dword __xdata__ PocatecniDatum, KoncoveDatum;
extern word  __xdata__ PocatecniCas, KoncovyCas;
extern byte  __xdata__ PocetZaznamuNaRadek;


void PrintStart( void);
// Inicializuje linku tiskarny

void PrintStop( void);
// Uvolni linku tiskarny

void PrintSetup( void);
// Nastaveni modu tiskarny

TYesNo PrintPrepare( void);
// Prohleda FIFO, nastavi ukazatele, vraci NO je-li prazdne

//void TestPrint();

void PrintProtocol( void);
// Vytiskne zaznamy za cele obdobi, ktere je v pameti

#endif
