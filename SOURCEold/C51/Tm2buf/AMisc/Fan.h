//*****************************************************************************
//
//    Fan.c - Ovladani ventilatoru
//    Version 1.0
//
//*****************************************************************************


#ifndef __Fan_H__
   #define __Fan_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

void FanInit();
// Inicializace ventilatoru

void FanSwitch(TYesNo On);
// Ovladani ventilatoru


#endif
