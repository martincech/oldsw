//*****************************************************************************
//
//    Alog.h - Auto data & message logger
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Alog_H__
   #define __Alog_H__

#ifndef __Tm2_H__
   #include "Tm2buf.h"     // konstanty a datove typy
#endif

extern bit _UkladatZaznamy;


//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

void AlogTemperature( byte address, int value);
// Mereni teploty cislo <address>, hodnota <value>
// ma horni byte v celych stupnich,
// dolni MSB 0.5C...0.0625C 0 0 0 0 LSB
// nebo ALOG_TEMPERATURE_ERROR je-li cidlo v poruse

void AlogClearAverages();
// Vynuluje sumy a pocty vzorku u vsech teplot

//-----------------------------------------------------------------------------
// Serva
//-----------------------------------------------------------------------------

void AlogServoStatus( byte address, byte status);
// Hlaseni o stavu serva cislo <address>, typ chyby <status>

void AlogServoPosition( byte address, byte control, byte measure);
// Hlaseni o pozici serva cislo <address>. <control> je ridici napeti
// z potenciometru, <measure> je zpetne hlaseni od serva.
// Hodnoty jsou na 4 bity horni 4 bity jsou 0


//-----------------------------------------------------------------------------
// Akumulator
//-----------------------------------------------------------------------------

void AlogAccuVoltage( byte value);
// Napeti na akumulatoru na cele volty

void AlogAccuCurrent( char value);
// Proud z akumulatoru, cele ampery se znamenkem

//-----------------------------------------------------------------------------
// Ventilator
//-----------------------------------------------------------------------------

void AlogFanLowPower(bit value);
// <value>=1 znamena 50% vykon, 0 znamena 100% vykon

void AlogFanFailure( bit failure);
// Porucha ventilatoru


//-----------------------------------------------------------------------------
// Stav nafty v nadrzi
//-----------------------------------------------------------------------------

void AlogDieselFuel( byte value);
// Stav nafty v nadrzi u navesu


//-----------------------------------------------------------------------------
// Prepinac rezimu
//-----------------------------------------------------------------------------

void AlogSwitchStatus( byte status);
// Nastaveni prepinace rezimu <status> je TSwitchStatus

void AlogFreshAirSwitchStatus(byte status);
// Nastaveni prepinace Fresh air, <status> je TFreshAirSwitchStatus

void AlogDieselStart( bit status);
// Nastartovany diesel <status> = YES.

void AlogDieselFailure( bit status);
// Porucha dieselu <status> = YES

void AlogHeatingFailure( bit status);
// Porucha topeni <status> = YES

void AlogSwitchCooler( bit status);
// Vystup spinani chlazeni. Sepnuto <status> = YES.

void AlogTempFailure(unsigned char Cislo, bit failure);
// Porucha teploty ve skrini cislo Cislo <failure> = YES

void AlogCoolerOn(bit status);
// Zapnuto chlazeni <status> = YES

void AlogClearCoolingFailureCounter();
  // Vynuluje pocitadlo chyby chlazeni

void AlogCoolingFailure( bit failure);
// Porucha chlazeni <status> = YES

void AlogDobijeniAku( bit status);
// Dobijeni akumulatoru <status> = YES.

void AlogElektromotorStart();
// Z chodu dieselu a nabijeni aku odvodi chod elektromotoru. Volat az po zalogovani chodu dieselu a dobijeni.

void AlogClearChargingFailureCounter();
  // Vynuluje pocitadlo chyby chlazeni

void AlogChargingFailure( bit failure);
// Porucha dobijeni <failure> = YES

void AlogBatteryOk( bit status);
// Napeti akumulatoru. V poradku <status> = YES.

//-----------------------------------------------------------------------------
// Zapisy do FIFO
//-----------------------------------------------------------------------------

void AlogInit( void);
// Inicializuje modul

//void AlogPause( void);
// Pozastaveni zapisu
#define AlogPause()      _UkladatZaznamy = NO

//void AlogResume( void);
// Uvolneni zapisu
#define AlogResume()     _UkladatZaznamy = YES

void AlogExecute( void);
// Prekopiruje aktualni stav do FifoData a v urceny cas zapise
// Volat po mereni

TYesNo ALogWriteProtocol(byte type, dword parameter);
// Ulozi do logu udalost

TYesNo AlogReset( void);
// Smaze logger

void AlogClear( void);
// Nulovani kumulovanych velicin loggeru

TYesNo AlogCopy( void);
// Prekopiruje interni EEPROM na externi modul

#endif

