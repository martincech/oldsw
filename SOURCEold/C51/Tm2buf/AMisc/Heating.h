//*****************************************************************************
//
//    Heating.c - Ovladani topeni
//    Version 1.0
//
//*****************************************************************************


#ifndef __Heating_H__
   #define __Heating_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


// Regulace topeni
typedef struct {
  byte On;                  // Zapnute topeni
} THeating;
extern THeating __xdata__ Heating;


void HeatingInit();
// Inicializace topeni

void HeatingSwitch(TYesNo On);
// Ovladani topeni

void HeatingExecute(int Target, int Real);
// Regulace topeni. Teplota <Target> je cilova teplota, teplota <Real> je realna teplota.
// Teplota ma horni byte v celych stupnich, dolni MSB 0.5C...0.0625C 0 0 0 0 LSB nebo ALOG_TEMPERATURE_ERROR je-li cidlo v poruse


#endif
