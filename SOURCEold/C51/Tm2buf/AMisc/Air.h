//*****************************************************************************
//
//    Air.c - Ovladani cerstveho vzduchu (recirkulace)
//    Version 1.0
//
//*****************************************************************************


#ifndef __Air_H__
   #define __Air_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

void AirInit();
// Inicializace

void AirSet(byte Percent);
// Nastavi cersrvy vzduch na <Percent> %


#endif
