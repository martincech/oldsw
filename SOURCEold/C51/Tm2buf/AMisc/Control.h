//*****************************************************************************
//
//    Control.c - Ovladani ventilace
//    Version 1.0
//
//*****************************************************************************


#ifndef __Control_H__
   #define __Control_H__

#ifndef __Hardware_H__
   #include "Hardware.h"    // pro podminenou kompilaci
#endif

// Stav rizeni vytapeni
typedef enum {
  MODE_OFF,
  MODE_VENTILATION,
  MODE_HEATING_AUTO,
  MODE_HEATING_MANUAL
} TControlMode;

// Stav ventilace
typedef struct {
  TControlMode  Mode;
  byte          FreshAir;               // Cerstvy vzduch v %
} TControl;
extern TControl __xdata__ Control;



void ControlInit();
// Inicializace

void ControlExecute();
// Krok rizeni

void ControlSwitchFan();
// Zapne/vypne ventilatory

void ControlSwitchHeatingMode();
// Zmena stavu topeni

void ControlSwitchHeating();
// Zapne/vypne topeni

void ControlIncreaseFreshAir();
// Prida cerstveho vzduchu

void ControlDecreaseFreshAir();
// Ubere cerstveho vzduchu

#endif
