//*****************************************************************************
//
//    AutoHW.h  - Auto independent hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Tm2HW_H__
   #define __Tm2HW_H__

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut AutoPISK         // vystup generatoru

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1        // asynchronni pipani

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       AutoAccuOk()      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

#define IEP_READ_BYTE     1        // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1        // zapis jednoho bytu

//-----------------------------------------------------------------------------
// Pripojeni AT25640 / X25645 na desce
//-----------------------------------------------------------------------------
// Tady je zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

// zapojeni pinu volne k uzivani :
#define EepCS    AutoEepCS           // chipselect /CS
#define EepSCK   AutoEepSCK            // hodiny SCK
#define EepSO    AutoEepSO             // vystup dat SO
#define EepSI    AutoEepSI             // vstup dat SI (muze byt shodny s SO)

// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// ochrana proti vybiti aku :

#define EepAccuOk()       AutoAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    64      // velikost stranky
#define EEP_SIZE      32768      // celkova kapacita

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// Podminena kompilace :

#define EEP_READ_BYTE    1       // cteni jednoho bytu
#define EEP_WRITE_BYTE   1       // zapis jednoho bytu
#define EEP_USE_INS      1       // Zda se vyuziva signal INS



//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu s AT25640 / X25645
//-----------------------------------------------------------------------------
// Tady je taky zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

#define XmemCS     AutoXmemCS     // chipselect /CS
#define XmemSCK    AutoXmemSCK    // hodiny SCK
#define XmemSO     AutoXmemSO     // vystup dat SO
#define XmemSI     AutoXmemSI     // vstup dat SI (muze byt shodny s SO)
//#define XmemInsert !AutoINSMOD   // signal pritomnosti modulu aktivni v 0
// ochrana proti vybiti aku :

#define XmemAccuOk()       AutoAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :
#define XMEM_PAGE_SIZE    64      // velikost stranky
#define XMEM_SIZE      32768      // kapacita externiho pametoveho modulu

// Inverze signalu:
#define XMEM_XSI_H  0
#define XMEM_XSCK_H 0

// Podminena kompilace :

//#define XMEM_READ_BYTE    1       // cteni jednoho bytu
//#define XMEM_WRITE_BYTE   1       // zapis jednoho bytu
//#define XMEM_USE_INS      1       // Zda se vyuziva signal INS

//-----------------------------------------------------------------------------
// Pripojeni 1-Wire sbernic
//-----------------------------------------------------------------------------


//#define MwiCS    AutoCSCIDLA      // chipselect Latche
#define MwiDATA  P1         // datova sbernice

#define MwiDQI0  AutoT1           // T1 Data Input Channel 0
#define MwiDQI1  AutoT2           // T2 Data Input Channel 1
#define MwiDQI2  AutoT3           // T3 Data Input Channel 2
#define MwiDQI3  AutoT4           // T4 Data Input Channel 3
#define MwiDQI4  AutoT5           // T5 Data Input Channel 4
#define MwiDQI5  AutoT6           // T6 Data Input Channel 5 - rezerva

#define MwiDQO0  AutoTOUT         // Data Output Channel 0
#define MwiDQO1  AutoTOUT         // Data Output Channel 1
#define MwiDQO2  AutoTOUT         // Data Output Channel 2
#define MwiDQO3  AutoTOUT         // Data Output Channel 3
#define MwiDQO4  AutoTOUT         // Data Output Channel 4
#define MwiDQO5  AutoTOUT         // Data Output Channel 5 - rezerva

//#define MwiD7    AutoTGND         // Data Input/Output - rezerva

// logicke urovne na vystupu :

//#define DQO_LO    1               // je pres tranzistor, inverzni logika
//#define DQO_HI    0
#define DQO_LO    0               // je pres hradlo a jeste tranzistor, normalni logika
#define DQO_HI    1

// casove prodlevy :

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

// Celkove zpozdeni cyklu for 2 + 4 * n

#define MWI_DELAY_RESET          MwiMicroDelay( 122)  // Zpozdeni 480 us pro reset
#define MWI_DELAY_PRESENCE       MwiMicroDelay( 17)   // Zpozdeni 70us mezi reset a presence
#define MWI_DELAY_RESET_TIMESLOT MwiMicroDelay( 104)  // Zpozdeni 410us mezi presence a dalsim
#define MWI_DELAY_START          _nop_();_nop_();_nop_()  // minimalni delka startbitu > 1us
#define MWI_DELAY_READ           11                   // Zpozdeni 15us v cyklu for
#define MWI_DELAY_READ_SLOT      MwiMicroDelay( 24)   // Zpozdeni 100us
#define MWI_DELAY_WRITE          MwiMicroDelay( 27)   // Zpozdeni 110us



// zakaz a povoleni interruptu, pouzito pro kriticky casovane sekce :

#define MwiDisableInts()         EA = 0
#define MwiEnableInts()          EA = 1

// identifikace kanalu a zaroven podmineny preklad :

#define MWI_CH0                  0                    // preklada se kanal 0
#define MWI_CH1                  1                    // preklada se kanal 1
#define MWI_CH2                  2                    // preklada se kanal 2
#define MWI_CH3                  3                    // preklada se kanal 3
#define MWI_CH4                  4                    // preklada se kanal 4
#define MWI_CH5                  5                    // preklada se kanal 5 - pridano, vyfukovana teplota

// podmineny preklad narocnych funkci :

#define MWI_CRC8                 1                    // preklada se kod pro vypocet CRC

//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------

#define IicSCL  AutoSCL         // I2C hodiny
#define IicSDA  AutoSDA         // I2C data

// casove prodlevy

#define IIC_WAIT   {byte i; for( i = 6; i > 0; i--);}  // 4takty + doplnit NOPy na 5us

// podmineny preklad :

#define IIC_READ    1           // cteni dat se sbernice


//-----------------------------------------------------------------------------
// Pripojeni DA prevodniku TDA8444 pres I2C sbernici
//-----------------------------------------------------------------------------

#define DAC_ADDRESS 0           // adresa zadratovana piny A0..A2

// Pro DIL pouzdro:
#define DAC_BASE_ADDRESS 0x40          // zaklad adresy prevodniku
#define DAC_MASK_ADDRESS 0x0E          // poloha adresy pinu A0..A2
// Pro SMD pouzdro:
//#define DAC_BASE_ADDRESS 0x48          // zaklad adresy prevodniku - pro SMD se lisi, A2 je vzdy 1
//#define DAC_MASK_ADDRESS 0x06          // poloha adresy pinu A0..A1

// podmineny preklad :
#define DAC_SINGLE_WRITE 1      // zapis do jednoho prevodniku


//-----------------------------------------------------------------------------
// Pripojeni RTC RX-8025 pres I2C sbernici
//-----------------------------------------------------------------------------

// podmineny preklad :

#define RTC_USE_DATE    1       // cti/nastavuj datum
//#define RTC_USE_WDAY    0       // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1       // hlidej zadavane hodnoty na rozsah
//#define RTC_USE_ALARM   0       // pouzivej alarm/budik

//-----------------------------------------------------------------------------
// Teplomery DS18B20
//-----------------------------------------------------------------------------

#define TEMP_DS18B20       1      // definice typu DS18B20 12 bitovy
//#define TEMP_DS18S20         1      // definice typu DS18S20 9 bitovy

#define TEMP_SECURE_READ   1      // bezpecne cteni (vcetne CRC) (musi byt definovan MWI_CRC8)

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

#define _THERMO_LAST       (MWI_CH5 + 1)  // celkovy pocet teplomeru
#define THERMO_TRIALS      3              // pocet pokusu na cteni z jednoho teplomeru


//-----------------------------------------------------------------------------
// Digitalni vstupy/vystupy
//-----------------------------------------------------------------------------

// digitalni vstupy :

#define AdioCS            AutoCSVST                   // chipselect vstupniho latche
#define AdioBUS           AutoDATA                    // datova sbernice

#define AdioBatteryLow    AutoLOWBAT                  // nizke napajeni

// digitalni vystupy :
#define AdioFanSwitch     AutoMAJAK                   // Zapinani ventilatoru



//-----------------------------------------------------------------------------
// Povoleni prekladu Auto.h
//-----------------------------------------------------------------------------

#define AUTO_TEMPERATURE    1             // preklada se teplomer
#define AUTO_SERVO          1             // preklada se servo
#define AUTO_ACCU           1             // preklada se aku
#define AUTO_FAN            1             // preklada se ventilator
#define AUTO_DIO            1             // prekladaji se digitalni vstupy/vystupy
#define AUTO_LOG            1             // preklada se logger
//#define AUTO_PRT            1             // preklada se protokolovani
#define AUTO_REG            1             // preklada se regulator
#define AUTO_DIESEL         1             // preklada se diesel
#define AUTO_CONFIG         1             // preklada se konfigurace
#define AUTO_FRESH_AIR      1             // preklada se fresh air


//-----------------------------------------------------------------------------
// Parametry FIFO - datovy logger
//-----------------------------------------------------------------------------

struct SAutoLog;                       // dopredna deklarace

typedef struct SAutoLog TFifoData;     // Typ ukladane struktury (max 255 bytu)
#define FifoData        AutoLog        // Globalni buffer ukladanych dat

#define FIFO_START            AUTO_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define FIFO_CAPACITY         AUTO_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define FIFO_MARKER_EMPTY     AUTO_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define FIFO_MARKER_FULL      AUTO_FIFO_MARKER_FULL    // znacka konce pri prepisovani

#define FIFO_INVALID_ADDRESS 0xFFFF                    // neplatna adresa do FIFO - MUSI korespondovat s XFIFO_INVALID_ADDRESS ve fifo.tpl!!!!!

// Podminena kompilace :
#define FIFO_FAST            1   // Zapamatovani pozice markeru
#define FIFO_VERIFY          1   // Verifikace po zapisu dat
#define FIFO_READ            1   // Cteni obsahu po polozkach
//#define FIFO_FIND_FIRST      1   // Nalezeni adresy prvniho zaznamu
//#define FIFO_FIND_LAST       1   // Nalezeni adresy posledniho zaznamu

//-----------------------------------------------------------------------------
// Pripojeni displeje
//-----------------------------------------------------------------------------

#define DisplayCS   AutoDisplayCS         // /CS vyber cipu
#define DisplayA0   AutoDisplayA0          // prikaz/data nekdy oznaceny jako CD
//#define DisplayRD   AutoDisplayRD          // cteni /RD
#define DisplayWR   AutoDisplayWR         // zapis /WR
#define DisplayRES  AutoDisplayRES         // reset /RES

#define DisplayData AutoDisplayData          // datova sbernice

// makra nezavisla na zapojeni pinu (volne k pouziti) :

#define DisplayEnable()  DisplayWR = 1; DisplayRD = 1; DisplayCS = 0
#define DisplayDisable() DisplayCS = 1

#define DisplayAddressCommand() (DisplayA0 = 1)
#define DisplayAddressData()    (DisplayA0 = 0)

#define DisplayReset() (DisplayRES = 1)  // Je to pres hradlo
#define DisplayRun()   (DisplayRES = 0)

// void   DisplayWriteData( byte_t d);
// zapis dat do displeje

#define DisplayWriteData( d) {DisplayWR = 0; DisplayData = d; DisplayWR = 1;}
//#define DisplayWriteData( d) (XBYTE[0] = d)

// byte DisplayReadData( void);
// cteni dat z displeje (v modulu Hardware.c)

// Pracovni mod displeje :

#define DISPLAY_MODE_GRAPHICS2 1            // graficky mod displeje - 2 roviny

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------


// definice klaves :
typedef enum {
   // systemove klavesy :
   K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
   _K_FIRSTUSER,

   // klavesy, v zavislosti na konstrukci klavesnice :
   K_RIGHT = _K_FIRSTUSER,         // sipka doprava
   K_UP,                           // sipka nahoru
   K_ENTER,                        // Enter
   K_ESC,                          // Esc
   K_TOUCH,                   // aktivni touchpad

   // systemove klavesy :
   K_REPEAT       = 0x80,   // opakovani klavesy OR ke klavese
   K_RELEASED     = 0xFE,   // pusteni klavesy (jednotlive i autorepeat)
   K_IDLE         = 0xFF    // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};




//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

#define RS232Power      AutoRSPOWER   // Port napajeni budice
#define RS232_BAUD      9600          // Rychlost linky v baudech
//#define RS232_USE_PARITY    1         // Provoz s paritou nebo bez

//#define RS232_PARITY_EVEN   1         // Suda parita
//#define RS232_PARITY_ODD   1        // Licha parita

//#define RS232_CASOVAC_1    1        // K casovani linky se vyuzije casovac 1
#define RS232_CASOVAC_2    1          // K casovani linky se vyuzije casovac 2 (pouzitelne jen u C51RD2, ne u maleho Atmela)

//#define RS232_GET           1         // Zda se pouziva cteni ze seriove linky
#define RS232_SEND          1         // Zda se pouziva zapis na seriovou linku





//-----------------------------------------------------------------------------
// Pripojeni Touch Screen ADS 7846
//-----------------------------------------------------------------------------

#define TouchDCLK   AutoTouchDCLK            // hodiny DCLK
#define TouchDIN    AutoTouchDIN               // vstup dat DIN
#define TouchDOUT   AutoTouchDOUT             // vystup dat DOUT
#define TouchPENIRQ AutoTouchPENIRQ              // preruseni od dotyku /PENIRQ

// chipselect :
#define TouchCS           AutoTouchCS          // chipselect /CS
#define TouchSelect()     TouchCS = 0
#define TouchDeselect()   TouchCS = 1

// trvale zadratovany chipselect :
//#define TouchSelect()
//#define TouchDeselect()

// technicke konstanty :

#define TOUCH_DELAY       4        // doba ustaleni udaje  [ms]
#define TOUCH_PEN_RISE    5        // doba po nabehu PENIRQ [ms]
#define TOUCH_PEN_FALL   15        // doba odpadeni PENIRQ [ms]
#define TOUCH_DATA_SIZE  12        // 12 bitu A/D
//#define TOUCH_DATA_SIZE   8        //  8 bitu A/D

#define TOUCH_REPEAT_COUNT 2       // Pocet opakovani cteni souradnic
#define TOUCH_REPEAT_DELAY 5       // Cekani v ms mezi jednotlivymi ctenimi souradnic
#define TOUCH_REPEAT_RANGE 30      // Maximalni odchylka souradnic v absolutnich jednotkach pri opakovanem cteni

#define TOUCH_AUTOREPEAT_START (700/TIMER0_PERIOD)  // prodleva prvniho autorepeat
#define TOUCH_AUTOREPEAT_SPEED (300/TIMER0_PERIOD)  // kadence autorepeat

// Rozsahy souradnic :

#define TOUCH_X_RANGE     320      // rozsah souradnice X : 0..<X-1>
#define TOUCH_Y_RANGE     240      // rozsah souradnice Y : 0..<Y-1>
#define TOUCH_XL        0x100      // surovy levy okraj
#define TOUCH_XR        0xEF0      // surovy pravy okraj
#define TOUCH_YU        0xED0      // surovy horni okraj
#define TOUCH_YD        0x180      // surovy dolni okraj
/* pro 8 bitu
#define TOUCH_XL         0x10      // surovy levy okraj
#define TOUCH_XR         0xEF      // surovy pravy okraj
#define TOUCH_YU         0xED      // surovy horni okraj
#define TOUCH_YD         0x18      // surovy dolni okraj
*/

// Pomineny preklad :

//#define TOUCH_READ_VBAT   1        // cteni vstupu Vbat
//#define TOUCH_READ_AUX    1        // cteni vstupu AUX



//-----------------------------------------------------------------------------
// Parametry radiove komunikace
//-----------------------------------------------------------------------------

#include "PktDef.h"           // popis protokolu

#define PACKET_BAUD 2400
//#define PACKET_PARITY
#define TXPACKET_TIMER_2              // vysilac  - rizeni casovacem 2
#define RXPACKET_TIMER_2              // terminal - rizeni casovacem 2



//-----------------------------------------------------------------------------
// Topeni
//-----------------------------------------------------------------------------

#define HEATING_DAC 1                   // Kanal
#define HEATING_ON  0                   // Hodnota pro zapnuti topeni
#define HEATING_OFF DAC_VALUE_MAX       // Hodnota pro vypnuti topeni

//-----------------------------------------------------------------------------
// Cerstvy vzduch
//-----------------------------------------------------------------------------

#define AIR_DAC 0                       // Kanal

#define FRESH_AIR_MIN_DAC 0x0B          // Vystup DA prevodniku, aby dal 2V (zcela zavrene servo)
#define FRESH_AIR_MAX_DAC 0x3E          // Vystup DA prevodniku, aby dal 10V (zcela otevrene servo)

#define MIN_FRESH_AIR 0                 // Minimalni hodnota cerstveho vzduchu v procentech
#define MAX_FRESH_AIR 100               // Miximalni hodnota cerstveho vzduchu v procentech
#define FRESH_AIR_RANGE (MAX_FRESH_AIR - MIN_FRESH_AIR)   // Rozsah cerstveho vzduchu v procentech

//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __Tm2_H__
  #include "Tm2buf.h"
#endif
//-----------------------------------------------------------------------------

#endif
