//*****************************************************************************
//
//    Hardware.h  - hardware descriptions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C2051.h"
#include "..\inc\rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include "..\inc\X25045.h"     // Watch Dog

#define CteckaAccuOk()  1                // V pripade, ze by se merilo napeti, udelat tady test. Standardne je napeti stale ok.

// Piny modulu :
sbit CteckaCSMOD   = P1^2;        // CS
sbit CteckaSCKMOD  = P1^3;        // SCK
sbit CteckaSOMOD   = P1^4;        // SO
sbit CteckaSIMOD   = P1^5;        // SI

// Piny supervisory u watchdogu X25045
sbit CteckaCSEE    = P3^4;        // chipselect /CS
sbit CteckaSCKEE   = P3^2;        // hodiny SCK
sbit CteckaSOEE    = P3^5;        // vystup dat SO
sbit CteckaSIEE    = P3^3;        // vstup dat SI (muze byt shodny s SO)


//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 11.0592 MHz
#define FXTAL 11059200L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     1            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define SI_X2      1            // COM v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  2      // zdvojena baudova rychlost

//-----------------------------------------------------------------------------
// Pripojeni X25045 na desce
//-----------------------------------------------------------------------------

#define EepCS    CteckaCSEE           // chipselect /CS
#define EepSCK   CteckaSCKEE          // hodiny SCK
#define EepSO    CteckaSOEE           // vystup dat SO
#define EepSI    CteckaSIEE           // vstup dat SI (muze byt shodny s SO)

// ochrana proti vybiti aku :

#define EepAccuOk()       CteckaAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    16      // velikost stranky
#define EEP_SIZE        512      // celkova kapacita

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// Podminena kompilace :

//#define EEP_READ_BYTE    1       // cteni jednoho bytu
//#define EEP_WRITE_BYTE   1       // zapis jednoho bytu

#define WatchDog()      EepWatchDog()

//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu AT45DB161
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Pripojeni SPI sbernice
//-----------------------------------------------------------------------------

#define SpiSCK   CteckaSCKMOD          // hodiny SCK
#define SpiSO    CteckaSOMOD           // vystup dat SO
#define SpiSI    CteckaSIMOD           // vstup dat SI (muze byt shodny s SO)
#define SpiCS    CteckaCSMOD           // chipselect CS\

// Inverze signalu:
#define SPI_SI_H  1
#define SPI_SCK_H 1

// jednoduchy chipselect :
#define SPI_SIMPLE_CS  1               // jednoduchy chipselect
#define SpiAttach()    SpiCS = 0       // select
#define SpiRelease()   SpiCS = 1       // deselect

//-----------------------------------------------------------------------------
// Flash Atmel AT45DB161
//-----------------------------------------------------------------------------

#define __AT45DB161__  1            // typ pameti
#define FLASH_TIMEOUT   5000        // pocet cyklu cekani na ready

#define __flash_data__              // kontextove promenne

// Podmineny preklad :
#define FLASH_READ_BYTE    1        // cteni bytu
#define FLASH_WRITE_BYTE   1        // zapis+flush bytu

// FLASH buffer :
#define FLASH_LOAD_BUFFER  1           // cteni pameti do bufferu
#define FLASH_READ_BUFFER  1           // cteni bufferu
// Pro nonvolatile buffer :
//#define FLASH_NVRAM        1           // pouzit modul NvFlash.c51
//#define FLASH_VERIFY       1           // verifikace po zapisu stranky



//-----------------------------------------------------------------------------
// Parametry seriove komunikace
//-----------------------------------------------------------------------------

#define COM_BAUD            57600      // Rychlost linky v baudech
#define COM_PARITY_EVEN     1          // Suda parita
#define COM_TIMER_1         1          // K casovani linky se vyuzije casovac 1

#define COM_RX_TIMEOUT      20000      // Meziznakovy timeout [us]

//-----------------------------------------------------------------------------
#endif
