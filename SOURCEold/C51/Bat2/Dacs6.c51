//*****************************************************************************
//
//    Dacs6.c - Vaha pripojena k nove ACS6
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#include "Dacs6.h"
#include "Bat2.h"              // Archiv, Config
#include "DacsPkt.h"           // Definice a parametry prikazu
#include "DacsPkt6.h"          // Definice a parametry prikazu
#include "DacsAddr.h"          // Adresy
#include "Scale.h"             // Naslapne vahy
#include "Archive.h"           // Archiv udaju
#include "Curve.h"             // Rustove krivky, editace planu krmeni
#include "Flock.h"             // Editovani hejna
#include "Cfg.h"               // Konfigurace
#include "Date.h"              // Date/time computing
#include "NvRam.h"             // NVRAM application map
#include "..\inc\Ds17287.h"    // NVRAM
#include "..\inc\bcd.h"        // BCD aritmetika

//#define __DEBUG__

#ifdef __DEBUG__
  #include "..\inc\System.h"   // "operacni system"
  #include "..\inc\El12864.h"    // Displej
  #include "..\inc\Display.h"    // Displej
  #include "..\inc\Font.h"       // Fonty
#endif


#ifdef VERSION_DACS_ACS6

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

TDacsDay __xdata__ DacsDay;                             // Statistika aktualniho dne zverejnena pro ACS (neni shodna s aktualnim dnem v Bat2)

// ---------------------------------------------------------------------------------------------
// Lokalni promenne
// ---------------------------------------------------------------------------------------------

// Automat
typedef enum {
  MODE_READING,         // Probiha prijem paketu
  MODE_WAITING,         // Ceka se pred odeslanim odpovedi
  MODE_SENDING,         // Probiha odesilani odpovedi
  _MODE_COUNT
} TMode;
static TMode        __xdata__   Mode;                   // Rezim automatu
static word         __xdata__   DelayCounter;           // Pocitadlo cekani pred odpovedi

static byte DataCount;  // Pocet znaku v datech pri sestavovani odpovedi

#ifdef __DEBUG__
static word __xdata__ DebugCount;
#endif

// ---------------------------------------------------------------------------------------------
// Lokalni funkce
// ---------------------------------------------------------------------------------------------

static TYesNo ExecuteCommand(void);
// Provedeni prikazu, obsah paketu je ve strukture DacsPacket

static void CreateEr(void);
// Vytvori odpoved ER. Hodnoty vrati ve strukture DacsPacket.

static TYesNo CommandVersion(void);
// Provede prikaz Version. Data vrati ve strukture DacsPacket.

static TYesNo CommandStatistics(void);
// Provede prikaz Statistics. Data vrati ve strukture DacsPacket.

static int ReadData(byte Address);
// Vrati hodnotu na adrese <Address>

static void SetDateTime(TLongDateTime Value);
// Nastavi datum a cas podle poslane hodnoty <Value>

static TYesNo WriteData(byte Address, int Value);
// Zapise hodnotu <Value> na adresu <Address> a vykona prikaz.

static void ClearStats(TGender Gender);
// Vynuluje statistiku pohlavi <Gender> v DacsDay

static void ClearDay(void);
// Vynuluje DacsDay

static void CreateDayFromToday(void);
// Zkopiruje statistiku z aktualniho dne do struktury DacsDay

static TYesNo LoadContext(void);
// Nacte kontextova data z NVRAM

static void SaveContext(TYesNo Force);
// Ulozi kontextova data do NVRAM


#ifdef __DEBUG__
  #define ShowDebug(text)               \
    DebugCount++;                       \
    DisplaySetFont(FONT_LUCIDA6);       \
    Font.Mode = FONT_MODE_NORMAL;       \
    DisplayClear();                     \
    UN.DT = DebugCount;                 \
    DisplayNumber(20, 0, 0, 0);         \
    DisplayString(0, 10, text);         \
    SysDelay(1000);
#else
  #define ShowDebug(text)
#endif

// ---------------------------------------------------------------------------------------------
// Inicializace
// ---------------------------------------------------------------------------------------------

void DacsInit(void) {
  // Inicializace, volat az po inicializaci archivu
  DacsEnable();                         // Povolim prijem
#ifdef __DEBUG__
  DebugCount = 0;
#endif
}

// ---------------------------------------------------------------------------------------------
// Pozastaveni cinnosti
// ---------------------------------------------------------------------------------------------

void DacsDisable(void) {
  // Pozastaveni cinnosti
  DacsPacketDisable();                  // Zakazu prijem / vysilani paketu
}

// ---------------------------------------------------------------------------------------------
// Povoleni cinnosti
// ---------------------------------------------------------------------------------------------

void DacsEnable(void) {
  // Povoleni cinnosti
  Mode = MODE_READING;                  // Zacinam nacitanim paketu
  DacsPacketInit();                     // Inicializace posilani a cteni paketu
}

// ---------------------------------------------------------------------------------------------
// Komunikace
// ---------------------------------------------------------------------------------------------

void DacsExecute(void) {
  // Provadeni prikazu, volat co nejcasteji
  switch (Mode) {

    case MODE_READING:
      // Nacitam pakety
      if (!DacsPacketRead()) {
        return;                         // Zadny paket neprisel
      }
      // Provedeni prikazu
      if (!ExecuteCommand()) {
        // Zpracovani se nazdarilo, odpovim chybu
        CreateEr();
      }
      // Pokud chce cekat pred odeslanim odpovedi, prepnu na cekani
      if (Config.Rs485.ReplyDelay == 0) {
        Mode = MODE_SENDING;            // Nechce cekat
      } else {
        Mode = MODE_WAITING;            // Chce cekat
        DelayCounter = Config.Rs485.ReplyDelay / TIMER0_PERIOD;         // Nastavim pocitadlo cekani
        if (DelayCounter == 0) {
          DelayCounter = 1;             // Aspon 1 perioda, i kdyby zadal jen 1ms (kvantuje se na TIMER0_PERIOD, tj. 20ms)
        }
      }
      return;

    case MODE_WAITING:
      // Cekam pred odeslanim odpovedi, obsluha cekani probiha na pozadi
      return;

    default: // MODE_SENDING
      // Poslu odoved (po odeslani se povoli prijem automaticky)
      DacsPacketSend();
      Mode = MODE_READING;

  }//switch
}

//-----------------------------------------------------------------------------
// Cekani pred odeslanim odpovedi
//-----------------------------------------------------------------------------

void DacsWaitTick(void) {
  // Obsluha cekani pred odeslanim odpovedi, volat s periodou TIMER0_PERIOD
  if (Mode != MODE_WAITING) {
    return;                     // Neceka se
  }
  if (--DelayCounter) {
    return;                     // Jeste cekam dal
  }
  // Perioda uz ubehla, muzu odeslat odpoved
  Mode = MODE_SENDING;
}

//-----------------------------------------------------------------------------
// Start vazeni
//-----------------------------------------------------------------------------

void DacsStartWeighing(void) {
  // Prave se zahajilo nove vazeni, vynuluju historii v NVRAM

  ShowDebug("StartWeighing");

  ClearDay();                           // Snuluju DacsDay
  SaveContext(YES);                     // Zapis do NVRAM za kazdou cenu
}

//-----------------------------------------------------------------------------
// Pulnoc v Bat2
//-----------------------------------------------------------------------------

void DacsBat2BeforeMidnight(void) {
  // V Bat2 je tesne pred pulnoci, ulozim si ubehnuty den do NVRAM

  ShowDebug("BeforeMidnight");

  CreateDayFromToday();                 // Vytvorim DacsDay z aktualniho dne vazeni
  SaveContext(YES);                     // Zapis do NVRAM za kazdou cenu
}

//-----------------------------------------------------------------------------
// Kopirovani
//-----------------------------------------------------------------------------

static void UpdateStats(TGender Gender) {
  TStatistic __xdata__ *Stat;

  Stat = &Archive.Stat[Gender];

  DacsDay.Stats[Gender].TargetWeight = Weighing.TargetWeight[Gender];
  DacsDay.Stats[Gender].Average      = StatAverage(Stat);
  DacsDay.Stats[Gender].Count        = Stat->Count;
  if (Archive.DayNumber == CURVE_FIRST_DAY_NUMBER + Config.WeighingStart.CurveDayShift) {
    // Jde o 1. den vazeni, prirustek je 0. Pokud bych ho spocital, vysel by prirustek roven prumerne hmotnosti, coz je blbost.
    DacsDay.Stats[Gender].Gain = 0;
  } else {
    // Jde o nektery dalsi den vazeni, prirustek muzu spocitat
    DacsDay.Stats[Gender].Gain = DacsDay.Stats[Gender].Average - Archive.LastAverage[Gender];
  }
  if (Archive.RealUniformityUsed) {
    DacsDay.Stats[Gender].Uniformity = Archive.RealUniformity[Gender];
  } else {
    DacsDay.Stats[Gender].Uniformity = StatUniformity(StatAverage(Stat), StatSigma(Stat), Config.WeighingStart.UniformityRange);
  }
  DacsDay.Stats[Gender].Sigma        = StatSigma(Stat);
  DacsDay.Stats[Gender].Cv           = (float)(10.0 * StatVariation(DacsDay.Stats[Gender].Average, DacsDay.Stats[Gender].Sigma));
}

static void CreateDayFromToday(void) {
  // Zkopiruje statistiku z aktualniho dne do struktury DacsDay
  DacsDay.DayNumber = Archive.DayNumber;
  UpdateStats(GENDER_FEMALE);
  if (Weighing.Header.UseBothGenders) {
    UpdateStats(GENDER_MALE);
  } else {
    ClearStats(GENDER_MALE);
  }
} // CreateDayFromToday

// ---------------------------------------------------------------------------------------------
// Provedeni prikazu
// ---------------------------------------------------------------------------------------------

static TYesNo ExecuteCommand(void) {
  // Provedeni prikazu, obsah paketu je ve strukture DacsPacket

  switch (DacsPacket.Command) {
    case COMMAND_STATISTICS:
      return CommandStatistics();

    case COMMAND_VERSION:
      return CommandVersion();

    default:    // COMMAND_ER
      return NO;

  }//switch
} // ExecuteCommand

// ---------------------------------------------------------------------------------------------
// Vytvoreni prikazu ER
// ---------------------------------------------------------------------------------------------

static void CreateEr(void) {
  // Vytvori odpoved ER. Hodnoty vrati ve strukture DacsPacket.
//  DacsPacket.Command   = COMMAND_ER;
//  DacsPacket.DataCount = 0;
} // CreateEr

// ---------------------------------------------------------------------------------------------
// Provedeni prikazu Version
// ---------------------------------------------------------------------------------------------

static TYesNo CommandVersion(void) {
  // Provede prikaz Version. Data vrati ve strukture DacsPacket.
  DacsPacket.Data[0] = 0xD1;    // Kod odpovedi
  DacsPacket.Data[1] = 0;       // Rezerva
  DacsPacket.Data[2] = bbcd2bin(VERSION & 0x00FF);       // Minor
  DacsPacket.Data[3] = bbcd2bin(VERSION >> 8);           // Major
  DacsPacket.DataCount = 4;
  return YES;
} // CommandVersion

// ---------------------------------------------------------------------------------------------
// Pridani znaku do bufferu
// ---------------------------------------------------------------------------------------------

static void AddByte(byte ch) {
  // Prida do DacsPacket.Data[] znak <ch>
  DacsPacket.Data[DataCount++] = ch;
} // AddByte

// ---------------------------------------------------------------------------------------------
// Pridani 16bit cisla do bufferu
// ---------------------------------------------------------------------------------------------

static void AddWord(word w) {
  // Prida do DacsPacket.Data[] word <w>
  DacsPacket.Data[DataCount++] = w & 0x00FF;     // LSB prvni
  DacsPacket.Data[DataCount++] = w >> 8;         // MSB druhe
} // AddWord

// ---------------------------------------------------------------------------------------------
// Pridani 32bit cisla do bufferu
// ---------------------------------------------------------------------------------------------

static void AddDword(dword d) {
  // Prida do DacsPacket.Data[] dword <d>
  TUn UN;

  UN.DT = d;
  DacsPacket.Data[DataCount++] = UN.ST.X4;       // LSB prvni
  DacsPacket.Data[DataCount++] = UN.ST.X3;
  DacsPacket.Data[DataCount++] = UN.ST.X2;
  DacsPacket.Data[DataCount++]   = UN.ST.X1;
} // AddDword

// ---------------------------------------------------------------------------------------------
// Pridani statistiky jednoho pohlavi do bufferu
// ---------------------------------------------------------------------------------------------

static void AddStatistics(TDacsStats __xdata__ *Stats) {
  AddDword(Stats->TargetWeight);
  AddDword(Stats->Average);
  AddWord(Stats->Count);
  AddDword(Scale.Weight);
  AddDword(Stats->Gain);
  AddByte(Stats->Uniformity);
  AddWord(Stats->Sigma);
  AddWord(Stats->Cv);
}

// ---------------------------------------------------------------------------------------------
// Provedeni prikazu Statistics
// ---------------------------------------------------------------------------------------------

static void CreateDay(word DayNumber) {
  // Podle vyzadaneho cisla dne <DayNumber> vytvori data v DacsDay
  if (!FlockWeighingRunningOrWaiting()) {
    // Pokud se prave nevazi, snuluju statistiku
    ShowDebug("CreateDay: nevazi se");
    ClearDay();
    return;
  }

  // Vazi se, zachovam se podle vyzadaneho cisla dne

  if (DayNumber > Archive.DayNumber) {
    // ACS je napred, vaha tento den jeste nevazila, statistiky jsou nulove
    ShowDebug("CreateDay: ACS je napred");
    ClearDay();
    return;
  }

  if (DayNumber == Archive.DayNumber) {
    // V ACS je stejny den jako prave ve vaze, vytvorim statistiku z aktualniho dne vazeni
    ShowDebug("CreateDay: CreateDayFromToday");
    CreateDayFromToday();
    return;
  }

  // ACS je pozadu, chce starsi den. Nactu vcerejsi DacsDay ulozeny v NVRAM a porovnam cisla dnu. Pokud budou dny sedet, poslu tuto statistiku.
  if (!LoadContext()) {
    // Narusena pamet, poslu same nuly
    ShowDebug("CreateDay: LoadContext error");
    ClearDay();
    return;
  }

  // Podarilo se den nacist, zkontroluju cislo dne
  if (DayNumber != DacsDay.DayNumber) {
    // Pozadovane cislo dne nemam v NVRAM, poslu nuly
    ShowDebug("CreateDay: nemam den v NVRAM");
    ClearDay();
    return;
  }

  // Den je nacteny z NVRAM a v poradku, poslu ho
  ShowDebug("CreateDay: nacteno z NVRAM");
}

static TYesNo CommandStatistics(void) {
  // Provede prikaz Statistics. Data vrati ve strukture DacsPacket.
  // Pokusim se nastavit datum a cas podle ACS
  SetDateTime(DacsPacket.DateTime);

  // Podle vyzadaneho cisla dne DacsPacket.ProductionDay vytvorim DacsDay
  CreateDay(DacsPacket.ProductionDay);

  // Sestavim odpoved
  DataCount = 0;
  DacsPacket.Data[DataCount++] = 0xA2;    // Kod odpovedi
  DacsPacket.Data[DataCount++] = 0x80;    // Typ
  DacsPacket.Data[DataCount++] = 0;       // Rezerva

/*  // Test rozmisteni polozek:
  DacsDay.Stats[GENDER_FEMALE].TargetWeight = 1;
  DacsDay.Stats[GENDER_FEMALE].Average      = 2;
  DacsDay.Stats[GENDER_FEMALE].Count        = 3;
  Scale.Weight                              = 4;
  DacsDay.Stats[GENDER_FEMALE].Gain         = 5;
  DacsDay.Stats[GENDER_FEMALE].Uniformity   = 6;
  DacsDay.Stats[GENDER_FEMALE].Sigma        = 7;
  DacsDay.Stats[GENDER_FEMALE].Cv           = 8;
  AddStatistics(&DacsDay.Stats[GENDER_FEMALE]);
  DacsDay.Stats[GENDER_MALE].TargetWeight   = 9;
  DacsDay.Stats[GENDER_MALE].Average        = 10;
  DacsDay.Stats[GENDER_MALE].Count          = 11;
  Scale.Weight                              = 12;
  DacsDay.Stats[GENDER_MALE].Gain           = 13;
  DacsDay.Stats[GENDER_MALE].Uniformity     = 14;
  DacsDay.Stats[GENDER_MALE].Sigma          = 15;
  DacsDay.Stats[GENDER_MALE].Cv             = 16;
  AddStatistics(&DacsDay.Stats[GENDER_MALE]);*/

/*  // Test zapornych hodnot:
  DacsDay.Stats[GENDER_FEMALE].TargetWeight = 1;
  DacsDay.Stats[GENDER_FEMALE].Average      = 2;
  DacsDay.Stats[GENDER_FEMALE].Count        = 3;
  Scale.Weight                              = -20000;
  DacsDay.Stats[GENDER_FEMALE].Gain         = -21000;
  DacsDay.Stats[GENDER_FEMALE].Uniformity   = 6;
  DacsDay.Stats[GENDER_FEMALE].Sigma        = 7;
  DacsDay.Stats[GENDER_FEMALE].Cv           = 8;
  AddStatistics(&DacsDay.Stats[GENDER_FEMALE]);
  DacsDay.Stats[GENDER_MALE].TargetWeight   = 9;
  DacsDay.Stats[GENDER_MALE].Average        = 10;
  DacsDay.Stats[GENDER_MALE].Count          = 11;
  Scale.Weight                              = -22000;
  DacsDay.Stats[GENDER_MALE].Gain           = -23000;
  DacsDay.Stats[GENDER_MALE].Uniformity     = 14;
  DacsDay.Stats[GENDER_MALE].Sigma          = 15;
  DacsDay.Stats[GENDER_MALE].Cv             = 16;
  AddStatistics(&DacsDay.Stats[GENDER_MALE]);*/

  // Realne statistiky z vazeni
  AddStatistics(&DacsDay.Stats[GENDER_FEMALE]);
  AddStatistics(&DacsDay.Stats[GENDER_MALE]);

  DacsPacket.DataCount = DataCount;
  return YES;
} // CommandStatistics

// ---------------------------------------------------------------------------------------------
// Nastaveni datumu a casu
// ---------------------------------------------------------------------------------------------

static void SetDateTime(TLongDateTime Value) {
  // Nastavi datum a cas podle poslane hodnoty <Value>
  if (FlockWeighingRunningOrWaiting()) {
    // Vazi se => cas muzu menit pouze v ramci aktualni hodiny, datum musi byt shodny
    if (ActualDateTime.Day   != Value.Day
     || ActualDateTime.Month != Value.Month
     || ActualDateTime.Year  != Value.Year
     || ActualDateTime.Hour  != Value.Hour) {
      return;           // Jinou hodinu nebo den nastavit nelze
    }
    if (ActualDateTime.Min == 59) {
      return;           // Pokud je napr. 10:59, radeji zmenu casu neumoznim (V ActualDateTime by mohlo byt jeste 10:59, ale v RTC uz 11:00)
    }
  }

  // ACS mi posila i sekundy, ale v BAT2 nastavuju cas pouze na minuty, sekundy nastavuju vzdy nulove. Cas synchronizuju jen pokud jsou
  // minuty odlisne. Pokud jsou i minuty stejne, necham sekundy v BAT2 plynout.
  if (ActualDateTime.Day   == Value.Day
   && ActualDateTime.Month == Value.Month
   && ActualDateTime.Year  == Value.Year
   && ActualDateTime.Hour  == Value.Hour
   && ActualDateTime.Min   == Value.Min) {
    return;
  }

  // Nevazi se => datum muzu menit
  CmdDateTime = Value;
  // Ulozim zadany cas do PCF
  DateSet();                                                    // Vyuziva globalni strukturu CmdDateTime
} // SetDateTime

//-----------------------------------------------------------------------------
// Mazani
//-----------------------------------------------------------------------------

static void ClearStats(TGender Gender) {
  // Vynuluje statistiku pohlavi <Gender> v DacsDay
  DacsDay.Stats[Gender].TargetWeight = 0;
  DacsDay.Stats[Gender].Average      = 0;
  DacsDay.Stats[Gender].Count        = 0;
  DacsDay.Stats[Gender].Gain         = 0;
  DacsDay.Stats[Gender].Uniformity   = 100;
  DacsDay.Stats[Gender].Sigma        = 0;
  DacsDay.Stats[Gender].Cv           = 0;
} // ClearStats

static void ClearDay(void) {
  // Vynuluje DacsDay
  DacsDay.DayNumber = 0;
  ClearStats(GENDER_FEMALE);
  ClearStats(GENDER_MALE);
} // ClearDay

//-----------------------------------------------------------------------------
// Nacteni kontextu
//-----------------------------------------------------------------------------

static TYesNo LoadContext(void) {
  // Nacte kontextova data z NVRAM
  word            Address;      // Musi byt word, DacsDay je umisten v TNvrData daleko
  byte __xdata__ *Dst;
  byte            i;
  byte            CheckSum;

  ShowDebug("LoadContext");

  // vychozi adresa :
  Address   = offsetof(TNvrData, DacsDay);
  CheckSum  = 0;
  // cteni polozek :
  Dst = (byte __xdata__ *)&DacsDay;
  for (i = 0; i < sizeof( TDacsDay); i++) {
    *Dst = RtcRamRead( Address++);
    CheckSum += *Dst;
    Dst++;
  }
  // kontrolni soucet :
  if (~CheckSum != RtcRamRead(Address)) {
    return NO;          // narusena data
  }
  return YES;
} // LoadContext

//-----------------------------------------------------------------------------
// Ulozeni kontextu
//-----------------------------------------------------------------------------

static void SaveContext(TYesNo Force) {
  // Ulozi kontextova data do NVRAM
  word            Address;      // Musi byt word, DacsDay je umisten v TNvrData daleko
  byte __xdata__ *Src;
  byte            i;
  byte            CheckSum;

  if (PowerFail() && !Force) {
    return;                     // neukladat, vypadek napajeni
  } // else ignoruj vypadek nebo napajeni OK

  ShowDebug("SaveContext");

  // vychozi adresa :
  Address  = offsetof(TNvrData, DacsDay);
  CheckSum = 0;
  // zapis polozek :
  Src = (byte __xdata__ *)&DacsDay;
  for (i = 0; i < sizeof(TDacsDay); i++) {
    RtcRamWrite( Address++, *Src);
    CheckSum += *Src;
    Src++;
  }
  // kontrolni soucet :
  RtcRamWrite(Address, ~CheckSum);
} // SaveContext

#endif // VERSION_DACS_ACS6
