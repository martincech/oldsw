//*****************************************************************************
//
//    Online.h     - Ukladani udaju online
//    Version 1.0
//
//*****************************************************************************

#ifndef __Online_H__
   #define __Online_H__

#include "Hardware.h"        // zakladni datove typy
#include "..\inc\FlFifo.h"   // Fifo
#include "Bat2.h"

// Pracovni data :
extern TFifo __xdata__ OnlineFifo;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void OnlineInit(void);
// Inicializace online mereni (po nabehu napajeni)

void OnlineReset(void);
// Mazani online dat

void OnlineNewWeight(TRawValue Weight, TYesNo Saved, TYesNo Stable);
// Zaznam noveho online vzorku hmotnosti

void OnlineNewHour(byte Hour);
// Zaznam nove hodiny <Hour> (binarne)

#endif

