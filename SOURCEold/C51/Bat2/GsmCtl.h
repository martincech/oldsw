//*****************************************************************************
//
//    GsmCtl.h         Bat2 GSM control
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __GsmCtl_H__
   #define __GsmCtl_H__

#include "Hardware.h"

#ifdef GSM_SMS_DEBUG
  #include "Bat2.h"               // GSM_NUMBER_MAX_LENGTH pro GSM diagnostiku
#endif // GSM_SMS_DEBUG

#define GSM_TIMER           5     // interval cteni zprav [s]
#define GSM_REPLY_DELAY     500   // Cekani v ms mezi prijetim SMS a odeslanim odpovedi - u nekterych SIM karet to bez cekani blbne a neodesle odpoved.
                                  // O pulnoci to SMS posle, ale na request to neodpovi.
                                  // Testy: 100ms a 200ms nefunguje, 300ms uz funguje.

// Stav GSM modulu
typedef enum {
  GSM_OFF,                      // Modul ma vypnute napajeni
  GSM_POWERED,                  // Modul ma zapnute napajeni, ale jeste nekomunikuje (nezdaril se reset)
  GSM_LIVE,                     // Modul ma zapnute napajeni a komunikuje, ale jeste neni prihlasen do site
  GSM_DELAY_BEFORE_READY,       // Modul se jiz prihlasil do site, ale cekam par cyklu, nez umoznim dalsi komunikaci - o pulnoci na baterky to nechtelo posilat
  GSM_READY                     // Modul je prihlasen do site, muze se odesilat/prijimat
} TGsmStatus;
extern TGsmStatus __xdata__ GsmStatus;


#ifdef GSM_SMS_DEBUG

extern byte __xdata__ SmsDebugValue;
typedef enum {
  READ_EMPTY,
  READ_ERROR,
  READ_OK
} TReadStatus;
extern TReadStatus __xdata__ ReadStatus;   // Stav nacitani SMS
extern char __xdata__ SmsReadText[ SMS_MAX_LENGTH];
extern char __xdata__ SmsReadNumber[GSM_NUMBER_MAX_LENGTH + 1];

extern byte __xdata__ ReadTimeout;         // Timeout pri cteni zpravy
extern byte __xdata__ ReadDebugValue;      // Vysledek fce SmsRead
extern word __xdata__ TotalReadError;      // Pocet chybnych nacitani
extern word __xdata__ TotalReadEmpty;      // Pocet spravnych nacitani, prazdna SMS
extern word __xdata__ TotalReadOk;         // Pocet spravnych nacitani, nejaky obsah

extern byte __xdata__ SendDebugValue;      // Vysledek fce SmsSend
extern byte __xdata__ SmsSent;             // YES/NO byla odeslana odpoved
extern byte __xdata__ DecodeDebug;         // Hodnota pri kontrole textu SMS

#endif // GSM_SMS_DEBUG


void GsmInit(void);
// Inicializace modulu

void GsmPowerOnMidnight(void);
// Pokud neni GSM modul nahozeny stale, zapnu jej pred vyslanim SMS o pulnoci

void GsmStart(void);
// Zapnuti modulu

void GsmStop(void);
// Vypnuti modulu

void GsmExecute(void);
// Vybirani a provadeni doslych povelu (SMS)

void GsmCreateStatisticMsg(int DayNumber);
// Z aktualnich dat vytvori SMS zpravu
// Pri <DayNumber>:
//   >= 0 hleda stary den v archivu
//   <  0 posle aktualni den (musi byt zapnuto vazeni)

void GsmSetMidnightMsg(void);
// Nastavi automat na vyslani SMS o pulnoci

#endif
