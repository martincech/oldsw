//*****************************************************************************
//
//    Dacs6.c - Vaha pripojena k nove ACS6
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#ifndef __Dacs6_H__
   #define __Dacs6_H__

#include "Hardware.h"               // zakladni datove typy
#include "Bat2.h"

#ifdef VERSION_DACS_ACS6

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Statistika pro jedno pohlavi
typedef struct {
  word  TargetWeight;                           // Cilova hmotnost
  word  Average;                                // Prumerna hmotnost
  word  Count;                                  // Pocet vazeni
  int   Gain;                                   // Denni prirustek
  byte  Uniformity;                             // Uniformita v %
  word  Sigma;
  word  Cv;
} TDacsStats;     // 13 bajtu

// Statistika predchoziho dne ulozena v NVRAM
typedef struct {
  word       DayNumber;                         // Cislo dne ve vaze
  TDacsStats Stats[_GENDER_COUNT];              // Statistika pro jednotlive pohlavi
} TDacsDay;     // 28 bajtu

extern TDacsDay __xdata__ DacsDay;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void DacsInit(void);
// Inicializace, volat az po inicializaci archivu

void DacsDisable(void);
// Pozastaveni cinnosti

void DacsEnable(void);
// Povoleni cinnosti

void DacsExecute(void);
// Provadeni prikazu, volat co nejcasteji

void DacsWaitTick(void);
// Obsluha cekani pred odeslanim odpovedi, volat s periodou TIMER0_PERIOD

void DacsStartWeighing(void);
// Prave se zahajilo nove vazeni, volat po zahajeni vazeni

void DacsBat2BeforeMidnight(void);
// V Bat2 je tesne pred pulnoci, volat pred pulnoci

#endif // VERSION_DACS_ACS6

#endif // __Dacs6_H__
