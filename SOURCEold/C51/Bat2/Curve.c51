//*****************************************************************************
//
//    Curve.c - Prace s rustovymi krivkami
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#include "Curve.h"
#include "Lang.h"               // Stringy
#include "KbdCtl.h"             // Klavesy
#include "..\inc\El12864.h"     // Displej
#include "..\inc\Font.h"        // Fonty
#include "..\inc\Display.h"     // Displej

// Symboly :
extern byte code SYMBOL_DOWN[];
extern byte code SYMBOL_ENTER[];
extern byte code SYMBOL_ESC[];
extern byte code SYMBOL_LEFT[];
extern byte code SYMBOL_RIGHT[];
extern byte code SYMBOL_UP[];
extern byte code SYMBOL_LEFTRIGHT[];
extern byte code SYMBOL_UPDOWN[];

extern byte code SYMBOL_ARROW[];


// Souradnice tabulky pro zadavani rustove krivky
#define TAB_X1                  60
#define TAB_X2                  127
#define TAB_WIDTH_COL1          26      // Sirka prvniho sloupce
#define TAB_Y1                  0
#define TAB_Y2                  63
#define TAB_HEIGHT_ROW1         8
#define TAB_HEIGHT_ROW          11
#define TAB_ROW_COUNT           5
#define TAB_SPACE_X             3       // Kolik pixelu od svisle cary se ma zobrazit cislo
#define TAB_SPACE_Y             0       // Kolik pixelu od vodorovne cary se ma zobrazit cislo
#define TAB_CURSOR_SPACE_X      1       // Kolik bodu vynechat mezi tabulkou a kurzorem
#define TAB_CURSOR_SPACE_Y      2       // O kolik bodu niz od radku zobrazit kurzor
#define TAB_CURSOR_WIDTH        4       // Sirka pruhu pro kurzor (sipku), kvuli mazani

// Stavy editace tabulky
typedef enum {
  TAB_MODE_VIEW,
  TAB_MODE_ADD_DAY,
  TAB_MODE_CHANGE_DAY,
  TAB_MODE_ADD_WEIGHT,
  TAB_MODE_CHANGE_WEIGHT
} TTableMode;

// Co se bude prekreslovat
typedef enum {
  TAB_DISPLAY_NOTHING,
  TAB_DISPLAY_ALL,
  TAB_DISPLAY_LEFT_SIDE,
  TAB_DISPLAY_CONTENT,
  TAB_DISPLAY_CURSOR
} TTableDisplay;

// ---------------------------------------------------------------------------------------------
// Staticke funkce:
// ---------------------------------------------------------------------------------------------

static void DrawFrame(void);
// Vykresli prazdnou tabulku
// Font musi byt nastaven na Lucida6

static void DrawLines(void);
// Vykresli linky v tabulce

static void DrawLeftSide(TTableMode Stav);
// Vykresli levou stranu displeje (klavesy atd.) v zavislosti na stavu

static void SortCurve(TCurvePoint __xdata__ *Curve, byte PointsCount);
// Setridi pole podle dne vzestupne, je treba zadat i pocet prvku <ItemsCount>, takze neni treba hledat konec krivky

static word FindMaxWeight(TCurvePoint __xdata__ *Curve, byte PointsCount);
// Najde v krivce nejvyssi hmotnost a vrati ji, je treba zadat i pocet prvku <ItemsCount>, takze neni treba hledat konec krivky

static void ClearCursor(void);
// Smaze sloupec s kurzorem


// ---------------------------------------------------------------------------------------------
// Nalezeni posledniho dne ve krivce
// ---------------------------------------------------------------------------------------------

word CurveFindLastDay(TCurvePoint __xdata__ *CurvePoint) {
  // Najde v krivce nejvyssi den a vrati jej (krivka je uz setridena)
  // Pokud je krivka prazdna, vrati CURVE_MAX_DAY + 1
  byte PointsCount = 0;         // Pokud je krivka zcela plna, neobsahuje zakonceni => musim hlidat i cislo pozice
  word MaxDay      = 0;

  // Pokud je krivka prazdna, na prvni pozici je hmotnost CURVE_END_WEIGHT
  if (CurvePoint->Weight == CURVE_END_WEIGHT) {
    return CURVE_MAX_DAY + 1;           // Krivka je prazdna
  }
  // Krivka neni prazdna, projizdim jednotlive body a hledam maximalni den, dokud nenarazim na konec krivky
  while (CurvePoint->Weight != CURVE_END_WEIGHT && PointsCount++ < CURVE_MAX_POINTS) {
    if (CurvePoint->Day > MaxDay) {
      MaxDay = CurvePoint->Day;         // Nasel jsem nove maximum
    }
    CurvePoint++;                       // Jdu na dalsi prvek
  }
  return MaxDay;
}

// ---------------------------------------------------------------------------------------------
// Aproximace rustove krivky
// ---------------------------------------------------------------------------------------------

word CurveCalculateApproximation(TCurvePoint __xdata__ *CurvePoint, word Day) {
  // Vypocte ze zadane krivky hmotnost pro zadany den (den muze byt 0-999)
  byte PointsCount = 0;                 // Pokud je krivka zcela plna, neobsahuje zakonceni => musim hlidat i cislo pozice
  TCurvePoint __xdata__ *LastPoint;     // Bod krivky z predchoziho kroku (pro vypocet aproximace ze 2 bodu)

  // Pokud ve krivce nic neni, vratim nulu
  if (CurvePoint->Weight == CURVE_END_WEIGHT) {
    return 0;                           // Krivka je prazdna
  }
  // Pokud je den mensi nez minimalni den ve krivce, vratim hmotnost prvniho dne
  if (Day < CurvePoint->Day) {
    return CurvePoint->Weight;          // Pod limitem vracime minimalni hmotnost
  }
  // Projizdim jednotlive body krivky
  while (CurvePoint->Weight != CURVE_END_WEIGHT && PointsCount++ < CURVE_MAX_POINTS) {
    if (CurvePoint->Day == Day) {
      return CurvePoint->Weight;        // Nasel jsem primo zadany den => vratim primo hmotnost ve krivce (kvuli 1. bodu zde tento test musi byt)
    }
    if (CurvePoint->Day > Day) {
      // Nasel jsem prvni den, ktery lezi za zadanym dnem => aproximuju z predchoziho a tohoto dne. LastPoint je uz v tomto miste urcite inicializovan.
      // Vypoctu hmotnost podle vzorce primky:
      //     Y2-Y1
      // Y = ----- * (X-X1) + Y1
      //     X2-X1
      // Musim to pocitat long, protoze se tam muze nasobit 999 * 64000
      // Rozdil hmotnosti (CurvePoint->Weight - LastPoint->Weight) muze pri klesajici krivce vyjit zaporny => obe hodnoty musim prevest zvlast na long
      // Rozdil dnu (CurvePoint->Day - LastPoint->Day) i (Day - LastPoint->Day) vyjde vzdy kladny, protoze krivka je setridena podle dnu vzestupne
      // => rozdil dnu muzu pocitat ve word
      return (long)(((long)CurvePoint->Weight - (long)LastPoint->Weight) * (long)(Day - LastPoint->Day)
                    / (long)(CurvePoint->Day - LastPoint->Day))
                    + LastPoint->Weight;
    }
    LastPoint = CurvePoint;
    CurvePoint++;                            // Jdu na dalsi prvek
  }
  // Nenasel jsem zadny bod krivky, ktery by byl roven nebo vetsi nez zadany den => den vypadl z krivky, vracim hmotnost posledniho dne v krivce
  return LastPoint->Weight;                  // CurvePoint uz je neplatny (mimo definovanou krivku) - musim vzit predchozi bod
} // CurveCalculateApproximation

// ---------------------------------------------------------------------------------------------
// Ulozeni prvni hodnoty do rustove krivky - pro pouziti pri automatickem rezimu
// ---------------------------------------------------------------------------------------------

#if defined(VERSION_GSM) || defined(VERSION_DACS_LW1)  || defined(VERSION_DACS_ACS6) || defined(VERSION_MODBUS)
// Neni u Lite a Basic

void CurveSaveFirstWeight(TCurvePoint __xdata__ *CurvePoint, word Weight) {
  // Vytvori jednoprvkovou rustovou krivku se zadanou hmotnosti
  CurvePoint[0].Day    = CURVE_FIRST_DAY_NUMBER;
  CurvePoint[0].Weight = Weight;
  // Zakoncim
  CurvePoint[1].Weight = CURVE_END_WEIGHT;
}

#endif // VERSION_GSM || VERSION_DACS_LW1 || VERSION_DACS_ACS6 || VERSION_MODBUS

// ---------------------------------------------------------------------------------------------
// Nacteni prvni hodnoty z rustove krivky - pro pouziti pri automatickem rezimu
// ---------------------------------------------------------------------------------------------

#if defined(VERSION_GSM) || defined(VERSION_DACS_LW1)  || defined(VERSION_DACS_ACS6) || defined(VERSION_MODBUS)
// Neni u Lite a Basic

word CurveLoadFirstWeight(TCurvePoint __xdata__ *CurvePoint) {
  // Nacte z rustove krivky prvni hmotnost
  return CurvePoint->Weight;
}

#endif // VERSION_GSM || VERSION_DACS_LW1 || VERSION_DACS_ACS6 || VERSION_MODBUS

// ---------------------------------------------------------------------------------------------
// Tabulka rustove krivky
// ---------------------------------------------------------------------------------------------

// Pokud zadal mene radku nez je maximalni pocet radku, zakoncim krivku hmotnosti rovnou CURVE_END_WEIGHT
// Pokud by zadal vsechny radky, zadnou nulu nikam nepisu (nemam uz kam)
#define FinishCurve()   if (RowCount < CURVE_MAX_POINTS) CurvePoint[RowCount].Weight = CURVE_END_WEIGHT

// Zjistim pocet prvku krivky - konec krivky znaci prvni hmotnost rovna hodnote KRIVKA_HMOTNOST_UKONCENI
// Pokud neni nalezena zadna ukoncovaci hmotnost, je krivka plna
// 14.9.2005: Cislo dne uz muze byt i nula, testuji jen podle hmotnosti. Hmotnost je dostacujici podminka, cislo dne jsem testoval pro sichr.
#define FindCurveEnd()                                            \
  for (RowCount = 0; RowCount < CURVE_MAX_POINTS; RowCount++) {   \
    if (CurvePoint[RowCount].Weight == CURVE_END_WEIGHT) break;   \
  }

TYesNo CurveTable(TCurvePoint __xdata__ *CurvePoint) {
  TTableMode idata Mode       = TAB_MODE_VIEW;          // Stav, ve kterem se tabulka nachazi
  byte idata Index            = 0;                      // Index radku v tabulce, na kterem je kurzor
  byte idata FirstRowIndex    = 0;                      // Index prvniho radku v tabulce
  byte idata RowCount         = 0;                      // Celkovy pocet prvku (radku) v tabulce
  TTableDisplay idata Display = TAB_DISPLAY_ALL;        // Co se ma prekreslit
  TYesNo Overwrite;                                     // Flag, ze zadany den jiz v krivce existuje a chce jej prepsat nove zadanym dnem
  word idata Day;                                       // Den, ktery mam do krivky pridat
  byte j;

  // Zjistim pocet radku v tabulce - konec krivky znaci prvni hmotnost rovna hodnote CURVE_END_WEIGHT
  FindCurveEnd();
  while (1) {
    // Pokud je treba neco prekreslit, prekreslim
    switch (Display) {
      case TAB_DISPLAY_ALL:
        // Prekreslim cely displej
        DisplayClear();
        DrawLeftSide(Mode);
        DrawFrame();
        // Schvalne nyni nedavam break a prejdu na vykresleni obsahu tabulky na dalsi case

      case TAB_DISPLAY_CONTENT:
        // Prekreslim obsah tabulky, napr. po pridani radku, rolovani tabulky atd.
        DisplayClearAreaFast(TAB_X1 + 1, (TAB_Y1 + TAB_HEIGHT_ROW1) / 8, TAB_X1 + TAB_WIDTH_COL1 - TAB_SPACE_X, TAB_Y2 / 8 + 1);  // Levy sloupec
        DisplayClearAreaFast(TAB_X1 + TAB_WIDTH_COL1 + 1, (TAB_Y1 + TAB_HEIGHT_ROW1) / 8, TAB_X2 - TAB_SPACE_X, TAB_Y2 / 8 + 1);  // Pravy sloupec
        // I kdyz se bude kurzor mazat a prekreslovat jeste dal, v prekresleni kurzoru, smazu ho uz ted, protoze vykresleni dat trva dele, tak aby
        // kurzor netrcel na minule pozici behem vykreslovani.
        ClearCursor();          // Sloupec s kurzorem
        DrawLines();            // Obnovim smazane linky v tabulce
        for (j = 0; j < TAB_ROW_COUNT; j++) {
          if (j + FirstRowIndex >= RowCount) {
            break;              // Uz neni co vykreslovat, ukoncim to
          }
          UN.DT = CurvePoint[j + FirstRowIndex].Day;
          DisplayNumber(TAB_X1 + TAB_WIDTH_COL1 - TAB_SPACE_X, TAB_HEIGHT_ROW1 + j * TAB_HEIGHT_ROW + TAB_SPACE_Y, 0, 0);
          UN.DT = CurvePoint[j + FirstRowIndex].Weight;
          DisplayNumber(TAB_X2 - TAB_SPACE_X, TAB_HEIGHT_ROW1 + j * TAB_HEIGHT_ROW + TAB_SPACE_Y, 0, SCALE_DECIMALS);
        }//for
        // Schvalne ted nedavam break, aby to slo jeste na vykresleni kurzoru

      case TAB_DISPLAY_CURSOR:
        ClearCursor();          // Sloupec s kurzorem
        DisplaySymbol(TAB_X1 - TAB_CURSOR_SPACE_X - TAB_CURSOR_WIDTH,
                      TAB_Y1 + TAB_HEIGHT_ROW1 + TAB_CURSOR_SPACE_Y + (Index - FirstRowIndex) * TAB_HEIGHT_ROW,
                      SYMBOL_ARROW);
        break;

      case TAB_DISPLAY_LEFT_SIDE:
        // Prekreslim levou stranu s buttony
        DisplayClearAreaFast(0, 0, TAB_X1 - TAB_CURSOR_SPACE_X - TAB_CURSOR_WIDTH - 1, 8);      // Cela leva strana az po kurzor
        DrawLeftSide(Mode);
        break;

    }//switch

    // Po prekresleni kazdopadne nastavim, aby dale nic neprekresloval
    Display = TAB_DISPLAY_NOTHING;

    // Vykonne funkce a obsluha tlacitka
    switch (Mode) {
      case TAB_MODE_VIEW:
        // ============ PROHLIZENI ==============
        switch (KbdWaitForKey()) {
          case K_UP:
          case K_UP | K_REPEAT:
            // Posunu se v tabulce o 1 radek nahoru
            if (Index == 0) {
              break;                            // Kurzor uz je na zacatku tabulky
            }
            KbdBeepKey();
            Index--;
            // Pokud je treba rolovat tabulkou, udelam to
            if (Index < FirstRowIndex) {
              // Prekreslim celou tabulku, kurzor neni treba prekreslovat, protoze uz je na prvnim radku
              FirstRowIndex = Index;            // Aktualizuju index prvniho radku
              Display = TAB_DISPLAY_CONTENT;
            } else {
              // Prekreslim pouze kurzor
              Display = TAB_DISPLAY_CURSOR;
            }//else
            break;

          case K_DOWN:
          case K_DOWN | K_REPEAT:
            // Posunu se v tabulce o 1 radek dolu
            if (RowCount == 0 || Index == RowCount - 1) {
              break;                            // Tabulka je bud prazdna, nebo uz je kurzor na poslednim prvku krivky
            }
            KbdBeepKey();
            Index++;
            // Pokud je treba rolovat tabulkou, udelam to
            if (Index >= FirstRowIndex + TAB_ROW_COUNT) {
              // Prekreslim celou tabulku, kurzor neni treba prekreslovat, protoze uz je na poslednim radku
              FirstRowIndex++;
              Display = TAB_DISPLAY_CONTENT;
            } else {
              // Prekreslim pouze kurzor
              Display = TAB_DISPLAY_CURSOR;
            }//else
            break;

          case K_ENTER:
            // Chce pridat novy radek do tabulky
            KbdBeepKey();
            if (RowCount == CURVE_MAX_POINTS) {
              // Tabulka uz je plna, nelze pridat dalsi radek
              DisplayDialog(LangSimpleDecode(STR_CURVE_FULL), DIALOG_KEYS_OK);
              Display = TAB_DISPLAY_ALL;
              break;
            }
            Mode    = TAB_MODE_ADD_DAY;
            Display = TAB_DISPLAY_LEFT_SIDE;
            break;

          case K_RIGHT:
            // Chce zmenit radek v tabulce
            if (RowCount == 0) {
              break;    // Tabulka je prazdna, neni co menit
            }
            KbdBeepKey();
            Mode    = TAB_MODE_CHANGE_WEIGHT;
            Display = TAB_DISPLAY_LEFT_SIDE;
            break;

          case K_LEFT:
            // Chce smazat radek v tabulce
            if (RowCount == 0) {
              break;    // Tabulka je prazdna, neni co mazat
            }
            KbdBeepKey();
            if (DisplayDialog(LangSimpleDecode(STR_CURVE_DELETE), DIALOG_KEYS_YESNO) != K_ENTER) {
              // Nechce smazat
              Display = TAB_DISPLAY_ALL;
              break;
            }
            // Chce smazat
            for (j = Index; j < RowCount - 1; j++) {
              CurvePoint[j] = CurvePoint[j + 1];
            }
            RowCount--;
            // Ubral jsem z krivky bod, musim krivku zakoncit ukoncovaci hmotnosti
            FinishCurve();
            // Tridit neni treba, protoze data byla setridena a umazanim 1 radku se to nerozdrbalo
            // Pokud jsem nesmazal zrovna posledni radek z tabulky, ponecham kurzor tam kde byl
            if (Index > 0 && Index >= RowCount) {
              // Smazal jsem posledni radek, ktery zmizi a na jeho misto se zadny dalsi nepresune => musim jit s kurzorem o radek vys
              Index--;
              if (Index < TAB_ROW_COUNT) {
                FirstRowIndex = 0;
              } else {
                FirstRowIndex = Index - TAB_ROW_COUNT + 1;
              }
            }//if
            // Jdu zpet do prohlizeni
            Mode    = TAB_MODE_VIEW;
            Display = TAB_DISPLAY_ALL;          // Byl tam dialog pres cely displej
            break;

          case K_ESC:
            // Kazdopadne zakoncim krivku - pokud zadal mene radku nez je maximalni pocet radku, zakoncim krivku hmotnosti rovnou CURVE_END_WEIGHT
            // Pokud by zadal vsechny radky, zadnou nulu nikam nepisu (nemam uz kam)
            KbdBeepKey();
            FinishCurve();
            // Zeptam se, zda chce ulozit zmeny
            if (DisplayDialog(LangSimpleDecode(STR_SAVE_CHANGES), DIALOG_KEYS_YESNO) == K_ENTER) {
              return YES;       // Chce ulozit krivku
            }//if
            // Schvalne ted nedavam break, aby to doslo jeste na K_TIMEOUT

          case K_TIMEOUT:
            // Chce odejit z editace rustove krivky
            KbdBeepKey();
            return NO;
            break;

        }//switch
        break;

      case TAB_MODE_ADD_DAY:
        // ============ PRIDANI DNE ==============
        Day = CurveFindLastDay(CurvePoint);
        if (Day < CURVE_MAX_DAY) {
          Day++;                                // V krivce uz je ulozen nejaky den, nabidnu mu dalsi den
        } else {
          Day = CURVE_FIRST_DAY_NUMBER;         // Krivka je dosud prazdna, nabidnu mu prvni den
        }
        UN.DT = Day;
        DisplaySetFont(FONT_TAHOMA8);
        if (!DisplayEdit(0, 10, 3, 0)) {
          Mode    = TAB_MODE_VIEW;
          Display = TAB_DISPLAY_LEFT_SIDE;
          break;
        }
        // Kontrolu na rozsah cisla dne neni treba provadet, cislo dne se zadava od 0 do 999
        // Zkontroluju, zda uz zadany den v krivce neexistuje
        Overwrite = NO;         // Vynuluju flag, mohl byt z predchoziho pridavani nastaveny
        Day = UN.Word.Lsb;      // Aby to bylo rychlejsi (editovany den je word => staci ulozit Lsb z UN)
        for (j = 0; j < RowCount; j++) {
          if (CurvePoint[j].Day == Day) {
            break;              // Zadany den uz v tabulce je
          }
        }//for j
        if (j < RowCount) {
          // Pokud jsem den v predchozim for nasel, j je mensi nez RowCount. Pokud jsem nenasel, j = RowCount, protoze cyklus dobehl az do konce.
          // Test musim delat az mimo cyklus for, protoze pripadny break v nasledujicim kodu by vyskocil z for a ne z case jak potrebuju
          if (DisplayDialog(LangSimpleDecode(STR_CURVE_DAY_EXISTS), DIALOG_KEYS_YESNO) != K_ENTER) {
            // Nechce prepsat, ukoncim pridavani noveho dne a jdu zpet do prohlizeni
            Mode    = TAB_MODE_VIEW;
            Display = TAB_DISPLAY_ALL;
            break;
          }//if
          Overwrite = YES;      // Nastavim flag, abych vedel, ze mam prepisovat a ne pridat na konec krivky
        }//if
        // Jdu na editaci hmotnosti
        Mode = TAB_MODE_ADD_WEIGHT;
        if (Overwrite) {
          // Je treba prekreslit cele, byl tam dialog pres cely displej
          Display = TAB_DISPLAY_ALL;
        } else {
          Display = TAB_DISPLAY_LEFT_SIDE;
        }//else
        break;

      case TAB_MODE_ADD_WEIGHT:
      case TAB_MODE_CHANGE_WEIGHT:
        // ============ PRIDANI NEBO ZMENA HMOTNOSTI ==============
        UN.Word.Msb = 0;             // Hmotnost je jen na 2 bajty
        if (Mode == TAB_MODE_ADD_WEIGHT) {
          UN.Word.Lsb = FindMaxWeight(CurvePoint, RowCount);    // Pri pridani noveho dne nabidnu hmotnost v poslednim radku
        } else {
          UN.Word.Lsb = CurvePoint[Index].Weight;               // Edituju hmotnost na aktualnim radku
        }
        // Edituju hmotnost
        DisplaySetFont(FONT_TAHOMA8);
        if (!DisplayEdit(0, 10, 5, SCALE_DECIMALS)) {
          Mode    = TAB_MODE_VIEW;
          Display = TAB_DISPLAY_LEFT_SIDE;
          break;
        }//if
        // Zkontroluju, zda zadal platnou cilovou hmotnost - zde musim pouzit cele UN.DT, mohl zadat napr. 99999
        if (!TargetWeightOK(UN.DT)) {
          if (DisplayDialog(LangSimpleDecode(STR_INVALID_VALUE), DIALOG_KEYS_RETRYCANCEL) == K_ENTER) {
            // Chce se opravit => prekreslim a udelam novy cyklus
            Display = TAB_DISPLAY_ALL;                  // Je treba prekreslit cele, byl tam dialog pres cely displej
            break;
          } else {
            // Nechce se opravit, ukoncim pridavani noveho dne a jdu zpet do prohlizeni
            Mode    = TAB_MODE_VIEW;
            Display = TAB_DISPLAY_ALL;
            break;
          }//else
        }//if
        // Zadal korektne hmotnost => ulozim ji do krivky
        if (Mode == TAB_MODE_ADD_WEIGHT) {
          // Pri pridani nove hmotnosti ulozim novy bod do krivky (den i hmotnost)
          if (Overwrite) {
            // Zadany den uz v krivce existuje a chtel ho prepsat => nezakladam novy den, ale najdu ho a prepisu
            for (j = 0; j < RowCount; j++) {
              if (CurvePoint[j].Day == Day) {
                break;        // Nasel jsem, v j mam index
              }
            }//for j
            if (j == RowCount) {
              j = 0;          // Toto by nemelo nastat, ale co kdyby, nez zmrsit nejakou neznamou promennou, radsi prepisu nulty den
            }
          } else {
            // Novy den pridam do krivky
            j = RowCount;     // Index noveho dne
            RowCount++;       // Zvysim
          }//else
          // Ted mam v j index, kam to mam ulozit
          CurvePoint[j].Day    = Day;
          CurvePoint[j].Weight = UN.Word.Lsb;           // Zadana hmotnost prosla makrem TargetWeightOK(), je urcite jen na 2 bajty
          // Pokud pridal do krivky novy bod, musim krivku zakoncit ukoncovaci hmotnosti a znovu setridit - az tady, po ulozeni noveho bodu
          // Pokud prepisoval uz existujici bod, nemusim delat nic, protoze den zustal stejny
          if (!Overwrite) {
            FinishCurve();                              // Zakoncim
            SortCurve(CurvePoint, RowCount);            // Setridim
            // Nastavim kurzor v tabulce na prave pridany radek - kvuli setrideni ho ted musim vyhledat znovu, jeho index se zmenil
            for (j = 0; j < RowCount; j++) {
              if (CurvePoint[j].Day == Day) {
                // Nasel jsem, v j mam index
                Index = j;
                if (Index < TAB_ROW_COUNT) {
                  FirstRowIndex = 0;
                } else {
                  FirstRowIndex = Index - TAB_ROW_COUNT + 1;
                }
                break;
              }//if
            }//for
          }//if
        } else {
          // Pri editaci jen zmenim aktualni hmotnost
          CurvePoint[Index].Weight = UN.Word.Lsb;       // Zadana hmotnost prosla makrem TargetWeightOK(), je urcite jen na 2 bajty
          // Tridit neni treba, protoze data byla setridena a den jsem tady nemenil
          // Kurzor taky nemenim, zobrazeni zustane
        }
        // Jdu zpet do prohlizeni
        Mode    = TAB_MODE_VIEW;
        Display = TAB_DISPLAY_ALL;        // Musim prekreslit i tabulklu s daty
        break;

    }//switch
    WatchDog();
  }//while
} // CurveTable

// ---------------------------------------------------------------------------------------------
// Vykresleni prazdne tabulky
// ---------------------------------------------------------------------------------------------

static void DrawFrame(void) {
  // Vykresli prazdnou tabulku
  // Font musi byt nastaven na Lucida6
  // Tabulka
  DisplayHorLine(TAB_X1, TAB_Y1, TAB_X2-TAB_X1, 1);
  DisplayHorLine(TAB_X1, TAB_Y2, TAB_X2-TAB_X1, 1);
  DisplayHorLine(TAB_X1, TAB_Y1 + TAB_HEIGHT_ROW1, TAB_X2 - TAB_X1, 1);
  DisplayFastVertLine(TAB_X1, TAB_Y1 / 8, (TAB_Y2 - TAB_Y1) / 8 + 1, 0xFF);
  DisplayFastVertLine(TAB_X2, TAB_Y1 / 8, (TAB_Y2 - TAB_Y1) / 8 + 1, 0xFF);
  DisplayFastVertLine(TAB_X1 + TAB_WIDTH_COL1, TAB_Y1 / 8, (TAB_Y2 - TAB_Y1) / 8 + 1, 0xFF);  // Stredova delici cara
  // Vyplneni hlavicky barvou
  DisplaySetAreaFast(TAB_X1, TAB_Y1 / 8, TAB_X2, (TAB_Y1 + TAB_HEIGHT_ROW1) / 8 - 1, 0xFF);
  // Vyplnim Hlavicku tabulky
  DisplaySetFont(FONT_LUCIDA6);
  Font.Mode = FONT_MODE_INVERSED;
  DisplayStringCenter(TAB_X1 + TAB_WIDTH_COL1 / 2, 0, LangSimpleDecode(STR_DAY));
  DisplayStringCenter(TAB_X1 + TAB_WIDTH_COL1 + (TAB_X2 - TAB_X1 - TAB_WIDTH_COL1) / 2, 0, LangSimpleDecode(STR_WEIGHT));
  DisplaySetFont(FONT_TAHOMA8);  // Zpet
}

// ---------------------------------------------------------------------------------------------
// Vykresleni linek v tabulce
// ---------------------------------------------------------------------------------------------

static void DrawLines(void) {
  // Vykresli linky v tabulce
  byte j;

  // Obnovim linky, ktere se znici smazanim obsahu tabulky
  DisplayHorLine(TAB_X1, TAB_Y2, TAB_X2 - TAB_X1, 1);
  DisplayHorLine(TAB_X1, TAB_Y1 + TAB_HEIGHT_ROW1, TAB_X2 - TAB_X1, 1);
  // Jednotlive radky tabulky
  for (j = TAB_HEIGHT_ROW1 + TAB_HEIGHT_ROW; j < TAB_Y2; j += TAB_HEIGHT_ROW) {
    DisplayHorLine(TAB_X1 + 2, j, TAB_X2 - TAB_X1 - 3, 2);      // Teckovana
  }
}

// ---------------------------------------------------------------------------------------------
// Vykresleni leve strany tabulky
// ---------------------------------------------------------------------------------------------

#define X_LEFT  13      // Zobrazeni textu vedle tlacitek vlevo

static void DrawLeftSide(TTableMode Mode) {
  // Vykresli levou stranu displeje (klavesy atd.) v zavislosti na stavu
  DisplaySetFont(FONT_LUCIDA6);
  switch (Mode) {
    case TAB_MODE_VIEW: {
      DisplaySymbol(0, 1, SYMBOL_UPDOWN);
      DisplayString(X_LEFT, 2, LangSimpleDecode(STR_MOVE));
      DisplaySymbol(0, 14, SYMBOL_ENTER);
      DisplayString(X_LEFT, 15, LangSimpleDecode(STR_ADD));
      DisplaySymbol(0, 27, SYMBOL_RIGHT);
      DisplayString(X_LEFT, 28, LangSimpleDecode(STR_CHANGE));
      DisplaySymbol(0, 40, SYMBOL_LEFT);
      DisplayString(X_LEFT, 41, LangSimpleDecode(STR_DELETE));
      DisplaySymbol(0, 53, SYMBOL_ESC);
      DisplayString(X_LEFT, 54, LangSimpleDecode(STR_QUIT));
      break;
    }
    case TAB_MODE_ADD_DAY: {
      DisplayString(0, 0, LangSimpleDecode(STR_DAY_COLON));
      DisplaySymbol(0, 40, SYMBOL_ENTER);
      DisplayString(X_LEFT, 41, LangSimpleDecode(STR_NEXT));
      DisplaySymbol(0, 53, SYMBOL_ESC);
      DisplayString(X_LEFT, 54, LangSimpleDecode(STR_CANCEL));
      break;
    }
    case TAB_MODE_ADD_WEIGHT: {
      DisplayString(0, 0, LangSimpleDecode(STR_WEIGHT_COLON));
      DisplaySymbol(0, 40, SYMBOL_ENTER);
      DisplayString(X_LEFT, 41, LangSimpleDecode(STR_SAVE));
      DisplaySymbol(0, 53, SYMBOL_ESC);
      DisplayString(X_LEFT, 54, LangSimpleDecode(STR_CANCEL));
      break;
    }
    case TAB_MODE_CHANGE_DAY:
    case TAB_MODE_CHANGE_WEIGHT: {
      switch (Mode) {
        case TAB_MODE_CHANGE_DAY: DisplayString(0, 0, LangSimpleDecode(STR_DAY_COLON)); break;
        case TAB_MODE_CHANGE_WEIGHT: DisplayString(0, 0, LangSimpleDecode(STR_WEIGHT_COLON)); break;
      }//switch
      DisplaySymbol(0, 40, SYMBOL_ENTER);
      DisplayString(X_LEFT, 41, LangSimpleDecode(STR_SAVE));
      DisplaySymbol(0, 53, SYMBOL_ESC);
      DisplayString(X_LEFT, 54, LangSimpleDecode(STR_CANCEL));
      break;
    }
  }//switch
  DisplaySetFont(FONT_TAHOMA8);  // Zpet
}

// ---------------------------------------------------------------------------------------------
// Trideni krivky
// ---------------------------------------------------------------------------------------------

static void SortCurve(TCurvePoint __xdata__ *StartPoint, byte PointsCount) {
  // Setridi pole podle dne vzestupne, je treba zadat i pocet prvku <ItemsCount>, takze neni treba hledat konec krivky
  TCurvePoint PointBackup;              // Zaloha pri prohazovani bodu
  TCurvePoint __xdata__ *Point;         // Pracovni ukazatel
  byte i, j;

  if (PointsCount < 2) {
    return;                             // Krivka je prazdna nebo obsahuje jen 1 bod => neni co tridit
  }

  for (i = 0; i < PointsCount; i++) {
    // Najdu minimum ve zbytku pole
    Point = StartPoint;
    for (j = i; j < PointsCount; j++) {
      if (Point->Day < StartPoint->Day) {
        // Tento prvek je mensi => prohodim je
        PointBackup = *StartPoint;      // Zazalohuju
        *StartPoint = *Point;           // A prohodim
        *Point      = PointBackup;
      }
      Point++;                          // Jdu na dalsi prvek
    }
    StartPoint++;                       // Posunu zacatek zbytku pole
  }
}

// ---------------------------------------------------------------------------------------------
// Nalezeni nejvyssi hmotnosti ve krivce
// ---------------------------------------------------------------------------------------------

static word FindMaxWeight(TCurvePoint __xdata__ *CurvePoint, byte PointsCount) {
  // Najde v krivce nejvyssi hmotnost a vrati ji, je treba zadat i pocet prvku <ItemsCount>, takze neni treba hledat konec krivky
  word MaxWeight = 0;
  byte j;

  for (j = 0; j < PointsCount; j++) {
    if (CurvePoint[j].Weight > MaxWeight) {
      MaxWeight = CurvePoint[j].Weight;
    }
  }
  return MaxWeight;
}

// ---------------------------------------------------------------------------------------------
// Smazani kurzoru
// ---------------------------------------------------------------------------------------------

static void ClearCursor(void) {
  // Smaze sloupec s kurzorem
  DisplayClearAreaFast(TAB_X1 - TAB_CURSOR_SPACE_X - TAB_CURSOR_WIDTH, (TAB_Y1 + TAB_HEIGHT_ROW1) / 8, TAB_X1 - TAB_CURSOR_SPACE_X - 1, TAB_Y2 / 8 + 1);
}

