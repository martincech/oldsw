//*****************************************************************************
//
//   NvRam.h        NVRAM application map
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __NvRam_H__
  #define __NvRam_H__

#include "..\inc\AtFlash.h"    // cache structure
#include "..\inc\Ds17287.h"    // NVRAM
#include "Bat2.h"
#include "Dacs.h"
#include "Dacs6.h"
#include "SlideAvg.h"

//-----------------------------------------------------------------------------
// Struktura NVRAM
//-----------------------------------------------------------------------------

// Velikost oblasti pro ukladani Dacs
#if defined(VERSION_DACS_LW1) || defined(VERSION_DACS_ACS6)
  #define DACS_SIZE (sizeof(TDacsDay) + sizeof(byte))
#else
  #define DACS_SIZE 0
#endif // VERSION_DACS_LW1 || VERSION_DACS_ACS6

// Velikost oblasti pro ukladani Sliding average
#ifdef VERSION_BIGDUTCHMAN
  #define SLIDEAVG_SIZE (sizeof(TSlideAvg) + sizeof(byte))
#else
  #define SLIDEAVG_SIZE 0
#endif // VERSION_BIGDUTCHMAN

#ifndef FLASH_NVRAM // interni buffer flash

  #define NVR_SPARE (RTC_NVRAM_SIZE - sizeof(TArchive) - sizeof(byte) - DACS_SIZE - SLIDEAVG_SIZE)

  typedef struct {
    TArchive    Archive;                    // Statistika
    byte        ArchiveChecksum;            // Kontrolni soucet polozky Archive

#if defined(VERSION_DACS_LW1) || defined(VERSION_DACS_ACS6)
    TDacsDay    DacsDay;                    // Statistika dne pro Dacs ACS
    byte        DacsDayChecksum;            // Kontrolni soucet polozky DacsDay
#endif // VERSION_DACS_LW1 || VERSION_DACS_ACS6

#ifdef VERSION_BIGDUTCHMAN
    TSlideAvg   SlideAvg;                   // Data pro Sliding average
    byte        SlideAvgChecksum;           // Kontrolni soucet polozky SlideAvg
#endif // VERSION_BIGDUTCHMAN

    byte        Spare[ NVR_SPARE];
  } TNvrData;  // 211 + 1 + 59 + 1 = 272 + NVR_SPARE bajtu

//-----------------------------------------------------------------------------
#else // NVRAM cache flash

  #define NVR_SPARE (RTC_NVRAM_SIZE - sizeof(TArchive) - sizeof(byte) - DACS_SIZE - SLIDEAVG_SIZE - sizeof(TFlashCache))

  typedef struct {
    TArchive    Archive;                    // Statistika
    byte        ArchiveChecksum;            // Kontrolni soucet polozky Archive

#if defined(VERSION_DACS_LW1) || defined(VERSION_DACS_ACS6)
    TDacsDay    DacsDay;                    // Statistika dne pro Dacs ACS
    byte        DacsDayChecksum;            // Kontrolni soucet polozky DacsDay
#endif // VERSION_DACS_LW1 || VERSION_DACS_ACS6

#ifdef VERSION_BIGDUTCHMAN
    TSlideAvg   SlideAvg;                   // Data pro Sliding average
    byte        SlideAvgChecksum;           // Kontrolni soucet polozky SlideAvg
#endif // VERSION_BIGDUTCHMAN

    byte        Spare[ NVR_SPARE];

    TFlashCache FlashCache;                 // cache pro flash
  } TNvrData;   // 211 + 1 + 59 + 1 = 272 + NVR_SPARE + 514 bajtu

  #define NvramPage() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Page)) // adresa cisla stranky
  #define NvramData() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Data)) // adresa bufferu dat
#endif // NVRAM cache
//-----------------------------------------------------------------------------

#endif
