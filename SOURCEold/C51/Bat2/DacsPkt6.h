//*****************************************************************************
//
//    DacsPkt.c - kodovani a dekodovani paketu ACS6
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#ifndef __DacsPkt6_H__
   #define __DacsPkt6_H__

#include "Hardware.h"               // zakladni datove typy
#include "Bat2.h"                   // Nastaveni rychlosti a parity

#ifdef VERSION_DACS_ACS6

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Typ prijateho prikazu
typedef enum {
  COMMAND_STATISTICS,                   // Nacteni statistik
  COMMAND_VERSION,                      // Nacteni verze vahy
  _COMMAND_COUNT
} TCommand;

// Struktura paketu
typedef struct {
  TCommand  Command;                    // Prikaz
  word      ProductionDay;              // Cislo dne v ACS
  TLongDateTime DateTime;               // Datum a cas v ACS
  byte      Data[70];                   // Odpoved vahy do ACS
  byte      DataCount;                  // Pocet bajtu v odpovedi
} TDacsPacket;
extern TDacsPacket __xdata__ DacsPacket;

//---------------------------------------------------------------------------------------------
// Funkce
//---------------------------------------------------------------------------------------------

void DacsPacketInit(void);
// Inicializace

void DacsPacketDisable(void);
// Pozastaveni cinnosti

TYesNo DacsPacketRead(void);
// Nacteni a dekodovani paketu, volat co nejcasteji. Pokud je nacten paket, vrati YES.

void DacsPacketSend(void);
// Zakoduje a posle paket, ktery je definovan ve strukture DacsPacket

#endif // VERSION_DACS_ACS6

#endif // __DacsPkt6_H__
