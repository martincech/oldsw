//*****************************************************************************
//
//   NvRam.h        NVRAM application map
//   Version 1.0    (c) VymOs
//
//*****************************************************************************

#ifndef __NvRam_H__
   #define __NvRam_H__

#ifndef __NvFlash_H__
   #include "..\inc\NvFlash.h"    // cache structure
#endif

#ifndef __Ds17287_H__
   #include "..\inc\Ds17287.h"    // NVRAM
#endif

//-----------------------------------------------------------------------------
// Struktura NVRAM
//-----------------------------------------------------------------------------

typedef struct {
   TFlashCache FlashCache;
} TNvrData;

//-----------------------------------------------------------------------------
// Makra pro NvFlash
//-----------------------------------------------------------------------------

#define NvramPage() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Page)) // adresa cisla stranky
#define NvramData() (offsetof( TNvrData, FlashCache) + offsetof( TFlashCache, Data)) // adresa bufferu dat

#endif
