//*****************************************************************************
//
//    Logger.h      - Zaznam udalosti
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Logger_H__
   #define __Logger_H__

#ifndef __Hardware_H__
   #include "Hardware.h"        // zakladni datove typy
#endif

#ifndef __FlFifo_H__
   #include "..\inc\FlFifo.h"   // Fifo
#endif

// Pracovni data :
extern TFifo   __xdata__ SamplesFifo;


//-----------------------------------------------------------------------------
// Denni zaznam
//-----------------------------------------------------------------------------

// Naraznikove znaky :

#define LG_DAY_EMPTY          0xFC      // nenaplnene FIFO
#define LG_DAY_FULL           0xFD      // naplnene FIFO

typedef struct {
   byte Day;                            // BCD den
   byte Month;                          // BCD mesic
   word Year;                           // BCD rok
} TLoggerDay;   // 4 bajty

//-----------------------------------------------------------------------------
// Zaznam vzorku
//-----------------------------------------------------------------------------

// Naraznikove znaky :

#define LG_SAMPLE_EMPTY        0x81     // nenaplnene FIFO
#define LG_SAMPLE_FULL         0x82     // naplenene FIFO
#define LG_SAMPLE_VALUE        0x80     // zaznam hodnoty
#define LG_SAMPLE_GENDER       0x40     // bit pohlavi
#define LG_SAMPLE_LOW_HOUR     0x00     // min. zaznam hodiny
#define LG_SAMPLE_HIGH_HOUR    0x23     // max. zaznam hodiny

#define LG_SAMPLE_MASK_WEIGHT  0x00FFFFFF  // Hmotnost je ulozena ve spodnich 24bit, MSB byte je flag

typedef union {
   byte      Flag;        // naraznik
   byte      Hour;        // hodina    0..23
   TRawValue Value;       // vaha 24 bitu, MSB je flag
} TLoggerSample;  // 6 bajtu

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void LoggerInit( void);
// Inicializace loggeru

void LoggerReset( void);
// Mazani vsech udaju

void LoggerRecordSample( TRawValue Value, byte Gender);
// Zaznam noveho vzorku

void LoggerNewHour( byte Hour);
// Zaznam nove hodiny <Hour> (BCD)

void LoggerNewDay( byte Day, byte Month, word Year);
// Zalozeni noveho dne

byte LoggerGetUniformity(TFifo __xdata__ *Fifo, byte Pohlavi, TNumber Prumer, byte Okoli);
// Vypocet uniformity ze vzorku v loggeru

#endif
