//*****************************************************************************
//
//    Archiv.h     - Ukladani udaju
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Archiv_H__
   #define __Archiv_H__

#ifndef __Hardware_H__
   #include "Hardware.h"        // zakladni datove typy
#endif

#ifndef __Stat_H__
   #include "..\inc\Stat.h"     // statistika
#endif

#ifndef __Histogr_H__
   #include "..\inc\Histogr.h"  // histogram
#endif

#ifndef __FlFifo_H__
   #include "..\inc\FlFifo.h"   // Fifo
#endif

#ifndef __Step_H__
   #include "Step.h"
#endif

// Pracovni data :
extern TArchiv __xdata__ Archiv;
extern TArchiv __xdata__ HistorieArchiv;
extern TFifo   __xdata__ AFifo;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void ArchivInit( void);
// Inicializace archivu (po nabehu napajeni)

void ArchivNewWeight( TRawValue Value, byte Gender);
// Prida nove zvazenou hodnotu do archivu

void ArchivNewHour( void);
// Zapise zmenu hodiny do archivu

void ArchivNewDay( void);
// Ukonci den

TRawValue ArchivGetYesterdayAverage( byte Gender);
// Vrati vcerejsi prumer, nebo 0 pri chybe

TRawValue ArchivGetLastAverage(byte Gender);
// Prochazi archiv zezadu a vrati posledni nenulovy prumer, nebo 0 pri chybe

//-----------------------------------------------------------------------------
// Operace
//-----------------------------------------------------------------------------

void ArchivReset( void);
// Mazani archivu

#endif
