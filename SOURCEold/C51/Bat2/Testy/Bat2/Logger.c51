//*****************************************************************************
//
//    Logger.c      - Zaznam udalosti
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#include "Logger.h"
#include "Archiv.h"            // sizeof
#include "..\inc\AtFlash.h"    // Flash memory
#include "..\inc\FlFifo.h"     // FIFO
#include "..\inc\Stat.h"       // Statistika - uniformita


TFifo   __xdata__ DayFifo = { FL_LOGGER_BASE,
                              FL_LOGGER_DAYS,
                              FL_LOGGER_DAY_SIZE,
                              LG_DAY_EMPTY,
                              LG_DAY_FULL,
                              0, 0};

TFifo   __xdata__ SamplesFifo = { 0L,           // dynamicky
                                  FL_LOGGER_SAMPLES,
                                  FL_LOGGER_SAMPLE_SIZE,
                                  LG_SAMPLE_EMPTY,
                                  LG_SAMPLE_FULL,
                                  0, 0};

// pracovni buffer (zapis dne neprobiha soucasne se zapisem vzorku) :

static union {
   TLoggerDay    Day;
   TLoggerSample Sample;
} __xdata__ Tmp;

// Lokalni funkce :

static void DayInit( TFifoIndex Index);
// Inicializuje popisovac dennich zaznamu na pozici <Index>

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void LoggerInit( void)
// Inicializace loggeru
{
TFifoIndex Index;
bit        NewDay;

   WatchDog();       // U Flash pameti to muze trvat dlouho (uz mne to blblo u Bat2)
   FifoInit( &DayFifo);
   Index  = FifoLast( &DayFifo);
   WatchDog();       // U Flash pameti to muze trvat dlouho (uz mne to blblo u Bat2)
   NewDay = YES;
   if( Index != FIFO_INVALID_INDEX){
      // posledni existuje, zkontroluj datum
      FifoReadFragment( &DayFifo, Index, &Tmp.Day, 0, sizeof( TLoggerDay));
      if( Tmp.Day.Day   == ActualDateTime.Day   &&
          Tmp.Day.Month == ActualDateTime.Month &&
          Tmp.Day.Year  == ActualDateTime.Year){
         // dnesek jiz existuje
         DayInit( Index);           // inicializuj jako pracovni
         return;
      }
   }
   // zaloz novy den
   LoggerNewDay( ActualDateTime.Day, ActualDateTime.Month, ActualDateTime.Year);
} // LoggerInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

void LoggerReset( void)
// Mazani vsech udaju
{
   FifoReset( &DayFifo);
   LoggerNewDay( ActualDateTime.Day, ActualDateTime.Month, ActualDateTime.Year);
} // LoggerReset

//-----------------------------------------------------------------------------
// Zaznam vzorku
//-----------------------------------------------------------------------------

void LoggerRecordSample( TRawValue Value, byte Gender)
// Zaznam noveho vzorku
{
   Tmp.Sample.Value = Value;
   if( Gender){
      Tmp.Sample.Flag  = LG_SAMPLE_VALUE | LG_SAMPLE_GENDER;
   } else {
      Tmp.Sample.Flag  = LG_SAMPLE_VALUE;
   }
   FifoWrite( &SamplesFifo, &Tmp.Sample);
} // LoggerRecordSample

//-----------------------------------------------------------------------------
// Zmena hodiny
//-----------------------------------------------------------------------------

void LoggerNewHour( byte Hour)
// Zaznam nove hodiny <Hour> (binarne)
{
   Tmp.Sample.Value = 0;
   Tmp.Sample.Hour  = Hour;
   FifoWrite( &SamplesFifo, &Tmp.Sample);
} // LoggerNewHour

//-----------------------------------------------------------------------------
// Zmena dne
//-----------------------------------------------------------------------------

void LoggerNewDay( byte Day, byte Month, word Year)
// Zalozeni noveho dne
{
TFifoIndex Index;

   Tmp.Day.Day   = Day;
   Tmp.Day.Month = Month;
   Tmp.Day.Year  = Year;
   FifoWriteFragment( &DayFifo, &Tmp.Day, 0, sizeof( TLoggerDay));
   Index = FifoLast( &DayFifo);
   DayInit( Index);
   FlashFlush();
   // Smazu i vsechny zaznamy hmotnosti za novy den
   FifoReset(&SamplesFifo);
} // LoggerNewDay

//-----------------------------------------------------------------------------
// Inicializace dne
//-----------------------------------------------------------------------------

static void DayInit( TFifoIndex Index)
// Inicializuje popisovac dennich zaznamu na pozici <Index>
{
   // vzorky zacinaji za zahlavim (TLoggerDay) :
   SamplesFifo.Base = FifoItemAddress( &DayFifo, Index, sizeof( TLoggerDay));
   FifoInit( &SamplesFifo);
} // DayInit

//-----------------------------------------------------------------------------
// Vypocet uniformity ze vzorku v loggeru
//-----------------------------------------------------------------------------

/*byte LoggerGetUniformity(TFifo __xdata__ *Fifo, byte Pohlavi, TNumber Prumer, byte Okoli) {
  // Projede vsechny vzorky a presne vypocte uniformitu v okoli +-<Okoli>% od prumeru <Prumer>
  TFifoIndex Index;
  TLoggerSample __xdata__ Sample;

  if (Pohlavi == POHLAVI_SAMEC) Pohlavi = LG_SAMPLE_GENDER; else Pohlavi = 0;  // Pripravim si to tak, abych mohl rovnou porovnavat spravny bit
  StatRealUniformityInit(Prumer * (TNumber)(100 - Okoli) / 100.0, Prumer * (TNumber)(100 + Okoli) / 100.0, Prumer);
  Index = FifoFirst( &SamplesFifo);
  while (Index != FIFO_INVALID_INDEX) {
    FifoRead(Fifo, Index, &Sample);
    if ((Sample.Flag & LG_SAMPLE_VALUE) && ((Sample.Flag & LG_SAMPLE_GENDER) == Pohlavi)) {
      // Jde o zaznam hmotnosti se zadanym pohlavim
      StatRealUniformityAdd(Sample.Value & LG_SAMPLE_MASK_WEIGHT);
    }//if
    Index = FifoNext(Fifo, Index);
    WatchDog();
  }//while
  return StatRealUniformityGet();
}*/
