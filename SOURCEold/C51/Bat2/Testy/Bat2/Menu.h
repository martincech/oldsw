//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// Globalni promenne :

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit

// Co se zobrazuje na displeji behem vazeni
typedef enum {
  ZOBRAZENI_NEVAZISE,
  ZOBRAZENI_VAZENI,
  ZOBRAZENI_STATISTIKA,
  ZOBRAZENI_HISTOGRAM,
  ZOBRAZENI_HISTORIE_STATISTIKA,
  ZOBRAZENI_HISTORIE_HISTOGRAM,
  _ZOBRAZENI_COUNT
} TZobrazeni;
extern TZobrazeni __xdata__ Zobrazeni;          // Co se ma prave zobrazovat




void MenuInit();
// Inicializace zobrazeni

void MenuExecute( void);
// Hlavni smycka

#endif
