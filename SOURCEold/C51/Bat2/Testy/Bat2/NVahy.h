//*****************************************************************************
//
//    NVahy.h - Naslapne vahy
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __NVahy_H__
   #define __NVahy_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Stavy naslapnych vah :

typedef enum {
  STAV_NV_INICIALIZACE,                 // Po zapnuti cekam, az se mne naplni historie nejakymi hmotnostmi
  STAV_NV_CEKAM_NA_NASTUP,              // Vaha ceka na vzrust hmotnosti tak, aby vzrust padnul do rustove krivky
  STAV_NV_CEKAM_NA_USTALENI,            // Hmotnost na vaze stoupla a padlo to do krivky, cekam na ustaleni vahy, abych ulozil ustalenou hmotnost
  STAV_NV_CEKAM_PO_ULOZENI              // Vaha se ustalila, ulozil jsem a nyni cekam, az povolim ulozeni dalsiho kusu
} TStavNaslapneVahy;

#define HISTORIE_POCET_HMOTNOSTI    2   // interni - kolik prvku zpetne se bude uchovavat

// Popisovac vahy :
typedef struct {
   long       Hmotnost;                      // aktualni mereni
   long       PosledniUlozenaHmotnost;
   byte       PosledniUlozenePohlavi;
   // interni kontext :
   byte       Stav;
   byte       DobaCekani;
   long       HistorieHmotnosti[ HISTORIE_POCET_HMOTNOSTI];
   long       HmotnostPredNastupem;
   byte       Pauza;        // Flag, zda je prave zapauzovane vazeni kvuli casovemu omezeni nebo rucne
} TNVaha;

// Popisovac vah :

extern TNVaha __xdata__ NVaha;

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void NVahyInit( void);
// Inicializace

void NVahyObsluha( void);
// Projede vsechny prevody naslapnych vah a pokud je nejaky novy prevod, vypocte jeho hmotnost, zkontroluje, zda se ma ulozit atd.
// Vrati hodnotu, ktera je vytvorena ORovanim konstant ZOBRAZIT_NV1 az ZOBRAZIT_NV4 a vyjadruje tak primo, ktera vaha se ma prekreslit => muze se potom
// primo ulozit do promenne Zobrazit

void NVahyEnable( void);
// Povoleni vazeni

void NVahyDisable( void);
// Zakaz vazeni

#endif
