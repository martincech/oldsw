//*****************************************************************************
//
//   SpiBat.h    SPI interface BAT
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __SpiBat_H__
   #define __SpiBat_H__


// Interni promenna :
extern bit _ExternalFlash;


#define SpiExternalFlash()   _ExternalFlash = YES
// Prepne na externi Flash

#define SpiInternalFlash()   _ExternalFlash = NO
// Prepne na interni Flash


#endif
