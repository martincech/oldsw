//*****************************************************************************
//
//    Main.c51       - BAT2 testovani
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "..\inc\System.h"     // "operacni system"
#include "..\inc\Pca.h"        // zvuky pomoci PCA
#include "..\inc\Ads7846.h"    // touch screen
#include "..\inc\AtFlash.h"    // Flash
#include "SpiBat.h"            // Prepinani flash
#include "Modul.h"             // Prace s modulem

#include "..\inc\Display.h"    // obecny zobrazovac
#include "..\inc\conio.h"      // jednoduchy display
#include "..\inc\bcd.h"

// odpocet necinnosti :
#define USER_EVENT_TIMEOUT 20    // uzivatel neobsluhuje dele nez ... s

// start odpocitavani 1 s :
#define SetTimer1s()  counter1s = 1000 / TIMER0_PERIOD

// Lokalni promenne :

volatile byte counter1s   = 0;         // odpocitavani 1 s
volatile bit  timer1s     = 0;         // priznak odpocitani 1 s

//static void ReadLine( word address);

#define MAX_BUFFER 10
static byte Buffer[ MAX_BUFFER];

// ---------------------------------------------------------------------------------------------
// Main
// ---------------------------------------------------------------------------------------------

void main()
{
word Value = 0;
word address;

// Systemova inicializace : ---------------------------------------------------
   BatCS1  = 0;                // deselect displeje
   BatCS2  = 0;
   BatSVIT   = 1;              // zapni podsvit
   BatZAPGSM = 0;              // zapni GSM

   TimingSetup();              // Nastaveni X2 modu
   EnableXRAM();               // Povoleni pristupu k vnitrni XRAM
   WDTPRG = WDTPRG_2090;       // start Watch Dogu
   WatchDog();

   EnableInts();               // Povoleni preruseni
   Timer0Run( TIMER0_PERIOD);  // spust casovac 0

   if( Value){
      TouchIsAutoRepeat();
      FlashWriteData(0);
      FlashWriteStart(0);
      FlashReadData();
      FlashReadStart(0);
   }
// Inicializace modulu : ------------------------------------------------------

   PcaBeep( NOTE_C4, VOL_10, 200);  // uvodni pipnuti
   SysDelay( 300);

   DisplayInit();
   TouchInit();                // inicializace touch screen

   cputs( "No nazdar");
   SysDelay( 1000);
   DisplayClear();
   cputs( "Zaciname...");
   FlashInit();
   SpiInternalFlash();
//   SpiExternalFlash();

   SetTimer1s();               // zahajeni odpoctu 1 s

   while (1) {
      DisplayGotoRC( 0, 0);
      switch(SysWaitEvent()){
         case K_ENTER :
            PcaBeep( NOTE_G3, VOL_10, 100);   // pipnuti
            DisplayClrEol();
            if( ModulIsPresent()){
               cputs( "Modul present");
            } else {
               cputs( "Modul missing");
            }
            break;

         case K_ESC :
            PcaBeep( NOTE_G3, VOL_10, 100);   // pipnuti
            DisplayClear();
            break;

         case K_UP :
            PcaBeep( NOTE_G3, VOL_10, 100);   // pipnuti
            address += FLASH_VIRTUAL_PAGE;
/*
            DisplayClrEol();
            cputs( "Address = ");
            cprinthex( address, 2);
*/
            cputs( "Verify start...");
            DisplayGotoRC( 1, 0);
            DisplayClrEol();
            if( !ModulVerify()){
               PcaBeep( NOTE_G2, VOL_10, 500);   // pipnuti
               cputs( "Verify ERR...");
               break;
            }
            PcaBeep( NOTE_G2, VOL_10, 500);   // pipnuti
            cputs( "Verify OK...");
            break;

         case K_DOWN :
            PcaBeep( NOTE_G3, VOL_10, 100);   // pipnuti
            address -= FLASH_VIRTUAL_PAGE;
/*
            DisplayClrEol();
            cputs( "Address = ");
            cprinthex( address, 2);
*/
            cputs( "Copy start...");
            DisplayGotoRC( 1, 0);
            DisplayClrEol();
            ModulCopy();
            PcaBeep( NOTE_G2, VOL_10, 500);   // pipnuti
            cputs( "Copy done...");
            break;

         case K_REDRAW :
            break;

         case K_TIMEOUT :
            break;

         default :
            break;
      }//switch
   }//while
} // main

//-----------------------------------------------------------------------------
// Prepnuti kontextu na "operacni system" viz System.h
//-----------------------------------------------------------------------------

byte SysYield()
// Prepnuti kontextu na operacni system. Vraci udalost
{
static byte timeout = 0;
word x, y;

   WatchDog();
   // nejprve casovac periodicke cinnosti :
   if( timer1s){
      // uplynula 1s
      timer1s = 0;              // zrus priznak
      SetTimer1s();             // novy cyklus
      timeout++;                // pocitame necinnost
      return( K_REDRAW);
   }
   // testuj necinnost :
   if( timeout > USER_EVENT_TIMEOUT){
      timeout = 0;             // zahajime dalsi cekani
      return( K_TIMEOUT);
   }
   if( TouchIsPressed()){
      timeout = 0;
      TouchReadCoordinates( &x, &y);
      if( y < TOUCH_Y_RANGE / 2){
         if( x < TOUCH_X_RANGE / 2){
            return( K_ENTER);
         } else {
            return( K_ESC);
         }
      } else {
         if( x < TOUCH_X_RANGE / 2){
            return( K_UP);
         } else {
            return( K_DOWN);
         }
      }
   }
   return( K_IDLE);             // zadna udalost
} // SysYield

//-----------------------------------------------------------------------------
// Cekani na udalost viz System.h
//-----------------------------------------------------------------------------

byte SysWaitEvent( void)
// Cekani na udalost
{
byte key;

   while( 1){
      key = SysYield();
      if( key != K_IDLE){
         return( key);      // neprazdna udalost
      }
   }
} // SysWaitEvent

//-----------------------------------------------------------------------------
// Prerusovaci rutina casovace 0
//-----------------------------------------------------------------------------

static void Timer0Handler( void) interrupt INT_TIMER0
// Prerusovaci rutina casovace 0
{
   SetTimer0( TIMER0_PERIOD);                // s touto periodou
   // vykonne funkce :
   CheckTrigger( counter1s, timer1s = 1);    // casovac 1 s
//   KbdTrigger();                             // casovac klavesnice
   PcaTrigger();                             // handler asynchronniho pipani
} // Timer0Handler

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

void SysDelay(unsigned int n)
{
// Uprava pro krystal 18.432MHz
  unsigned char m;                                                                     // m = R5, n = R6+R7
                                                                            // T = 1.085 mikrosec.
  while(n > 0) {              // DELAY: SETB C/MOV A,R7/SUBB A,#0/MOV A,R6/SUBB A,#0/JC KONEC *7T*
    WDTRST=0x1E;
    WDTRST=0xE1;
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    // Pro X2 mod musim 2x tolik!!
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    --n;                          // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*
  }                                                                             // SJMP DELAY *2T*
} // SysDelay

/*
//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

static void ReadLine( word address)
{
byte i;

   FlashReadStart( address);
   for( i = 0; i < MAX_BUFFER; i++){
      Buffer[ i] = FlashReadData();
   }
   FlashReadDone();
   for( i = 0; i < 10; i++){
      cprinthex( Buffer[ i], 1);
   }
   putchar( '\n');
} // ReadLine
*/
