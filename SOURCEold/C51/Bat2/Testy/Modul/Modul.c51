//*****************************************************************************
//
//   Modul.c     Flash Modul
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "Modul.h"
#include "SpiBat.h"
#include "..\inc\AtFlash.h"
#include "..\inc\At45dbxx.h"  // Flash page access

#define CHECK_TRIALS  3                  // pocet pokusu na zjisteni pritomnosti
#define MAX_BUFFER   16                  // velikost bufferu pro kopirovani

static byte __xdata__ Buffer[ MAX_BUFFER];    // pracovni buffer


//-----------------------------------------------------------------------------
// Pritomnost
//-----------------------------------------------------------------------------

TYesNo ModulIsPresent( void)
// Vrati YES, je-li modul pritomen
{
byte i;

   SpiExternalFlash();
   for( i = 0; i < CHECK_TRIALS; i++){
      if( FlashCheckSignature( FlashStatus())){
         SpiInternalFlash();
         return( YES);
      }
   }
   SpiInternalFlash();
   return( NO);
} // ModulIsPresent

//-----------------------------------------------------------------------------
// Kopirovani
//-----------------------------------------------------------------------------

void ModulCopy( void)
// Kopiruje obsah flash na modul
{
byte  i;
word  Page, Offset;

   Page    = 0;
   Offset  = 0;
   do {
      WatchDog();
      // nacti fragment :
      SpiInternalFlash();
      FlashBlockReadStart( Page, Offset);
      for( i = 0; i < MAX_BUFFER; i++){
         Buffer[ i] = FlashBlockReadData();
      }
      FlashBlockReadDone();
      // zapis fragment :
      SpiExternalFlash();
      FlashWriteBufferStart( Offset);
      for( i = 0; i < MAX_BUFFER; i++){
         FlashWriteBufferData( Buffer[ i]);
      }
      FlashWriteBufferDone();
      // posun adresy :
      Offset  += MAX_BUFFER;
      if( Offset >= FLASH_VIRTUAL_PAGE){
         // stranka naplnena
         Offset = 0;
         FlashWaitForReady();             // cekej na pripravenost
         FlashSaveBuffer( Page);          // zapis buffer
         FlashWaitForReady();             // cekej na pripravenost
         Page++;                          // nova stranka
      }
   } while( Page < FLASH_PAGES);
   SpiInternalFlash();
} // ModulCopy

//-----------------------------------------------------------------------------
// Verifikace
//-----------------------------------------------------------------------------

TYesNo ModulVerify( void)
// Kontroluje obsah flash proti modulu
{
byte  i;
word  Page, Offset;

   Page    = 0;
   Offset  = 0;
   do {
      WatchDog();
      // nacti fragment :
      SpiInternalFlash();
      FlashBlockReadStart( Page, Offset);
      for( i = 0; i < MAX_BUFFER; i++){
         Buffer[ i] = FlashBlockReadData();
      }
      FlashBlockReadDone();
      // komparuj fragment :
      SpiExternalFlash();
      FlashBlockReadStart( Page, Offset);
      for( i = 0; i < MAX_BUFFER; i++){
         if( FlashBlockReadData() != Buffer[ i]){
            FlashBlockReadDone();
            SpiInternalFlash();
            return( NO);
         }
      }
      FlashBlockReadDone();
      // posun adresy :
      Offset  += MAX_BUFFER;
      if( Offset >= FLASH_VIRTUAL_PAGE){
         // stranka prectena
         Offset = 0;
         Page++;                          // nova stranka
      }
   } while( Page < FLASH_PAGES);
   return( YES);
} // ModulVerify
