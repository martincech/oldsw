//*****************************************************************************
//
//    Hardware.h  - BAT2 hardware descriptions
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE

/*!!!
#define BatAccuOk()  !BatLOWBAT
#define PowerFail()  BatLOWBAT
*/

#define BatAccuOk()  1
#define PowerFail()  0

//-----------------------------------------------------------------------------
// Prirazeni portu
//-----------------------------------------------------------------------------

// datova sbernice :

#define BatDATA P0

sbit BatD0       = BatDATA^0;
sbit BatD1       = BatDATA^1;
sbit BatD2       = BatDATA^2;
sbit BatD3       = BatDATA^3;
sbit BatD4       = BatDATA^4;
sbit BatD5       = BatDATA^5;
sbit BatD6       = BatDATA^6;
sbit BatD7       = BatDATA^7;

// port P1 :

sbit BatCSRTC    = P1^0;
sbit BatRDRTC    = P1^1;
sbit BatWRRTC    = P1^2;
sbit BatALERTC   = P1^3;
sbit BatCSEE     = P1^4;               // CS flash
sbit BatP15      = P1^5;
sbit BatP16      = P1^6;
sbit BatP17      = P1^7;
// SPI procesoru :
#define BatMISO  BatP15
#define BatSCK   BatP16
#define BatMOSI  BatP17

// Port P2 :

sbit BatLOWBAT   = P2^0;
sbit BatZAPGSM   = P2^1;
sbit BatPISK     = P2^2;
sbit BatPENIN    = P2^3;               // PENIRQ touchpad
sbit BatCSPAN    = P2^4;               // CS touchpad
sbit BatRES      = P2^5;               // RES display
sbit BatCS2      = P2^6;               // CS1 display
sbit BatCS1      = P2^7;               // CS2 display

// Port P3 :

sbit BatRXD      = P3^0;
sbit BatTXD      = P3^1;
sbit BatCSMOD    = P3^2;               // CS flash modulu
sbit BatDRAD     = P3^3;               // DRDY AD1241
sbit BatCLKAD    = P3^4;               // CLK  AD1241
sbit BatDINAD    = P3^5;               // DIN  AD1241
sbit BatSVIT     = P3^6;               // podsvit
sbit BatDOUTAD   = P3^7;               // DOUT AD1241


// Kanal vahy :

#define WEIGHT_CHANNEL   0             // cislo kanalu A/D pro vahu
typedef long TRawValue;                // surova hodnota vazeni

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1             // zdvojena baudova rychlost - nechci

// Casovac 0 :
#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  BatAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut BatPISK                 // vystup generatoru

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1               // asynchronni pipani

//-----------------------------------------------------------------------------
// A/D prevodnik ADS1241
//-----------------------------------------------------------------------------

#define AdcDIN         BatDINAD        // vstup
#define AdcDOUT        BatDOUTAD       // vystup
#define AdcSCLK        BatCLKAD        // hodiny
#define AdcDRDY        BatDRAD         // -ready

#define AdcClrCS()                     // chipselect vyber
#define AdcSetCS()                     // deselect

#define ADC_GAIN       ADC_GAIN_1      // zakladni zesileni
#define ADC_RATE       ADC_7_5HZ       // rychlost vzorkovani
#define ADC_POLARITY   ADC_BIPOLAR     // rezim polarity
#define ADC_RANGE      ADC_FULL_RANGE  // plny/polovicni rozsah

#define ADC_INTERRUPT  INT_EXTERNAL1   // rezim na pozadi, vstup preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

//-----------------------------------------------------------------------------
// Pripojeni RTC DS17287
//-----------------------------------------------------------------------------

// porty :

#define RtcALE         BatALERTC       // ALE address latch enable
#define RtcWR          BatWRRTC        // WR\ zapis
#define RtcRD          BatRDRTC        // RD\ cteni
#define RtcAD          BatDATA         // datova/adresni sbernice
#define RtcCS          BatCSRTC        // chipselect

// podmineny preklad :

#define RTC_USE_DATE    1              // cti/nastavuj datum
#define RTC_USE_WDAY    1              // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1              // hlidej zadavane hodnoty na rozsah
#define RTC_USE_ALARM   1              // pouzivej alarm/budik
#define RTC_USE_NVRAM   1              // pouzita NVRAM

//-----------------------------------------------------------------------------
// Pripojeni SPI sbernice
//-----------------------------------------------------------------------------

#define SpiSCK         BatSCK          // hodiny SCK
#define SpiSO          BatMISO         // vystup dat SO
#define SpiSI          BatMOSI         // vstup dat SI (muze byt shodny s SO)
#define SpiCS          BatCSEE         // chipselect

// Inverze signalu:
#define SPI_SI_H  1
#define SPI_SCK_H 1

//-----------------------------------------------------------------------------
// Flash Atmel AT45DB041
//-----------------------------------------------------------------------------

#define __AT45DB161__      1           // typ pameti
#define FLASH_TIMEOUT   5000           // pocet cyklu cekani na ready

// Podmineny preklad :
#define FLASH_READ_BYTE    1           // cteni bytu
#define FLASH_WRITE_BYTE   1           // zapis+flush bytu

//-----------------------------------------------------------------------------
// Displej - podminena kompilace
//-----------------------------------------------------------------------------

// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ idata

// Porty :

#define DisplayD0         BatD0
#define DisplayD1         BatD1
#define DisplayD2         BatD2
#define DisplayD3         BatD3
#define DisplayD4         BatD4
#define DisplayD5         BatD5
#define DisplayD6         BatD6
#define DisplayD7         BatD7
#define DisplayRW         BatWRRTC
#define DisplayDI         BatALERTC
#define DisplayE          BatRDRTC
#define DisplayCS1        BatCS1
#define DisplayCS2        BatCS2
#define DisplayRESET      BatRES


#define USE_NASTAV_OBLAST          1
// Fonty - pouziti jednotlivych fontu
#define USE_TAHOMA8    1
#define USE_LUCIDA6    1

/*
// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Obecne funkce
#define USE_DISPLEJ_ROZNOUT      1

// Cary atd.
#define USE_VYMAZ_OBLAST_RYCHLE  1
#define USE_NASTAV_OBLAST        1
#define USE_OBDELNIK_RYCHLE      1
#define USE_VODOROVNA_CARA       1
#define USE_VODOROVNA_TECKOVANA_CARA 1
#define USE_SVISLA_CARA_RYCHLE   1
//#define USE_SVISLA_TECKOVANA_CARA_RYCHLE 1
#define USE_SVISLA_CARA          1
//#define USE_KRESLI_BOD           1
//#define USE_CARA                 1

// Fonty - pouziti jednotlivych fontu
#define USE_TAHOMA8    1
#define USE_LUCIDA6    1
//#define USE_MYRIAD32   1
//#define USE_MYRIAD40   1

// Vety - pouziti jednotlivych zarovnani u vet
#define USE_VETA         1
#define USE_VETA_NASTRED 1
#define USE_VETA_NAPRAVO 1
//#define USE_VETA_NALEVO  1

// Zobrazeni cisla
#define USE_ROZLOZENI_CISLA 1
#define USE_ZOBRAZENI_CISLA 1
//#define USE_ZOBRAZENI_ZNAKU_VE_FORMATU 1

// Symbol
#define USE_SYMBOL 1

// Editace
//#define ALFANUMERICKA_KLAVESNICE 1   // Pokud je definovano, pouziva se numericka klavesnice. Jinak pouziva k editaci sipky
#define KONEC_EDITACE_KLAVESA_NULA 1  // Zda se ma pouzivat ukonceni editace pomoci klavesy Nula (krome Esc)
#define USE_EDITACE 1
#define USE_EDITACE_TEXTU 1

// Dialog
#define USE_DIALOG 1

// Menu
#define USE_MENU 1
#define MENU_MAX_POCET_POLOZEK 6        // maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
#define USE_VYBERPOLOZKU 1

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
#define ObsluzModEditace()                                                                                                                              \
/*  if (ModEditace==EDITACE_PODSVIT) {                                                                                                                    \
    UN.DT=(unsigned long)(10000000*(unsigned long)Znaky[7]+1000000*(unsigned long)Znaky[6]+100000*(unsigned long)Znaky[5]+10000*(unsigned long)Znaky[4] \
          +1000*(unsigned long)Znaky[3]+100*(unsigned long)Znaky[2]+10*(unsigned long)Znaky[1]+(unsigned long)Znaky[0]);                                \
    Konfigurace.IntenzitaPodsvitu=UN.DT;                                                                                                                \
    SpustRizeniPodsvitu();                                                                                                                              \
  }                                                                                                                                                     \*/

//-----------------------------------------------------------------------------
// Displej - parametry
//-----------------------------------------------------------------------------

#define SIRKA_DISPLEJE 128
#define VYSKA_DISPLEJE 64

//-----------------------------------------------------------------------------
// Pripojeni Touch Screen ADS 7846
//-----------------------------------------------------------------------------

// porty :

#define TouchDCLK      BatD7           // hodiny DCLK
#define TouchDIN       BatD6           // vstup dat DIN
#define TouchDOUT      BatD5           // vystup dat DOUT
#define TouchPENIRQ    BatPENIN        // preruseni od dotyku /PENIRQ

// chipselect :

#define TouchCS           BatCSPAN     // chipselect /CS
#define TouchSelect()     BatCSPAN = 0
#define TouchDeselect()   BatCSPAN = 1

// technicke konstanty :

#define TOUCH_DELAY         4          // doba ustaleni udaje  [ms]
#define TOUCH_PEN_RISE      5          // doba po nabehu PENIRQ [ms]
#define TOUCH_PEN_FALL     15          // doba odpadeni PENIRQ [ms]
#define TOUCH_DATA_SIZE    12          // 12 bitu A/D
//#define TOUCH_DATA_SIZE   8          //  8 bitu A/D

#define TOUCH_REPEAT_COUNT  2          // Pocet opakovani cteni souradnic
#define TOUCH_REPEAT_DELAY  5          // Cekani v ms mezi jednotlivymi ctenimi souradnic
#define TOUCH_REPEAT_RANGE 10          // Maximalni odchylka souradnic v absolutnich jednotkach pri opakovanem cteni

#define TOUCH_AUTOREPEAT_START (700/TIMER0_PERIOD)  // prodleva prvniho autorepeat
#define TOUCH_AUTOREPEAT_SPEED (300/TIMER0_PERIOD)  // kadence autorepeat

// Rozsahy souradnic :

#define TOUCH_X_RANGE     128          // rozsah souradnice X : 0..<X-1>
#define TOUCH_Y_RANGE      64          // rozsah souradnice Y : 0..<Y-1>
#define TOUCH_XL        0xE00          // surovy levy okraj
#define TOUCH_XR        0x230          // surovy pravy okraj
#define TOUCH_YU        0x410          // surovy horni okraj
#define TOUCH_YD        0xBA0          // surovy dolni okraj

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_TOUCH,                             // Touchpanel
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (400/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

//#define __DISPLAY_CURSOR__    1      // nastavovani kurzoru
#define __DISPLAY_CLEAR__     1        // mazani
//#define __DISPLAY_CLR_EOL__   1        // mazani do konce radku

//-----------------------------------------------------------------------------
// Statistika
//-----------------------------------------------------------------------------

typedef float TNumber;                 // zakladni datovy typ

#define STAT_UNIFORMITY      1         // povoleni vypoctu uniformity
//#define STAT_REAL_UNIFORMITY 1       // povoleni presneho vypoctu uniformity

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

typedef word      THistogramCount;     // pocet vzorku
typedef TRawValue THistogramValue;     // hodnota vzorku

#define HISTOGRAM_SLOTS       40       // pocet sloupcu - sude cislo (max.254)

#define HISTOGRAM_GETDENOMINATOR 1

//-----------------------------------------------------------------------------
// Flash Fifo
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Komunikace RS232
//-----------------------------------------------------------------------------
#define COM_BAUD        9600          // Rychlost linky v baudech

//#define COM_PARITY_EVEN  1          // Suda parita
//#define COM_PARITY_ODD   1          // Licha parita

//#define COM_TIMER_1      1          // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2        1          // K casovani linky se vyuzije casovac 2

#define COM_RX_TIMEOUT    10000       // Meziznakovy timeout
#define COM_TX_TIMEOUT    10000       // Prodleva na vyslani znaku

//-----------------------------------------------------------------------------
// Modul SMS
//-----------------------------------------------------------------------------

#define SMS_MAX_LENGTH  161           // max. delka prijate zpravy, vc. koncove nuly

// buffery pro cisla :
extern char code DstNumber[];         // cilove cislo
extern byte code DstLength;           // pocet cislic ciloveho cisla
extern char xdata SrcNumber[];        // zdrojove cislo
extern byte xdata SrcLength;          // pocet cislic zdrojoveho cisla

// Definice ciloveho cisla :
#define SmsDstNumberLength()       DstLength                    // pocet cislic ciloveho cisla
#define SmsDstNumberDigit( i)      (DstNumber[i] - '0')         // cislice v poradi, jak je piseme

// Definice zdrojoveho cisla :
#define SmsSrcNumberLength( n)     SrcLength = n;               // pocet cislic zdrojoveho cisla
#define SmsSrcNumberDigit( i, n)   SrcNumber[ i] = (n) + '0'    // cislice v poradi, jak je piseme


//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __Step_H__
//   #include "Step.h"
#endif
#ifndef __Hejna_H__
//   #include "Hejna.h"
#endif

//-----------------------------------------------------------------------------

#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------

#endif
