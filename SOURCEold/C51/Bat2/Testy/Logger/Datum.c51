//*****************************************************************************
//
//    Datum.c - Prace s datem
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#include "Datum.h"

// Pocet dnu v mesici. Prechodny rok resim zvlast
unsigned char code POCET_DNU_V_MESICI[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};   // Schvalne 13 prvku, abych to mohl volat rovnou s cislem mesice

unsigned int PocetDnuOdPocatkuRoku(TDateTime *Datum) {
  // Vypocte pocet celych dnu od pocatku roku do zadaneho data. Nezapocitavam aktualni den, pouze cele, jiz ubehnute dny.
  unsigned char j;
  unsigned int PocetDnu;   // Muze byt az 365
  bit PrechodnyRok=0;      // Flag, zda jde o prechodny rok

  if (Datum->Year%4==0) PrechodnyRok=1;
  // Prictu pocet ubehnutych dnu z aktualniho mesice
  PocetDnu=Datum->Day-1;     // Pouze cele dny, bez aktualniho dne, proto minus 1
  // Pokud ubehl vic jak 1 mesic, prictu pocty dnu za ubehnute cele mesice
  if (Datum->Month>1) {
    for (j=1; j<=Datum->Month-1; j++) {
      PocetDnu+=POCET_DNU_V_MESICI[j];
      if (PrechodnyRok && j==2) PocetDnu++;    // V prechodnem roce ma unor 29 dni misto 28 ulozenych v poli POCET_DNU_V_MESICI[]
    }//for
  }//if
  return PocetDnu;
}

unsigned int PocetDnuDoKonceRoku(TDateTime *Datum) {
  // Vypocte pocet celych dnu od zadaneho data do konce roku. Pokud je v puli dne, zapocitava se i aktualni den, na rozdil od fce PocetDnuOdPocatkuRoku().
  if (Datum->Year%4==0) {
    return 366-PocetDnuOdPocatkuRoku(Datum);   // Prechodny rok
  } else {
    return 365-PocetDnuOdPocatkuRoku(Datum);   // Normalni rok
  }//else
}

unsigned int PocetDnuMeziDatumy(TDateTime *PocatecniDatum, TDateTime *KoncoveDatum) {
  // Vypocte pocet celych dnu mezi dvema datumy. Bere se pocet celych dnu, tj. nebere se v uvahu cas.
  unsigned int Rozdil;
  unsigned int Rok;
  // Nejprve zkontroluju, zda je pocatecni datum pred koncovym datem - aby mne to nejak neslo do minusu a nedavalo blbosti
  // Jdu postupne od roku az ke dni
  if (KoncoveDatum->Year<PocatecniDatum->Year) {
    return 0;
  } else if (KoncoveDatum->Year==PocatecniDatum->Year) {
    // Pokud jsou oba datumy ve stejnem roce, musim testovat mesic
    if (KoncoveDatum->Month<PocatecniDatum->Month) {
      return 0;
    } else if (KoncoveDatum->Month==PocatecniDatum->Month) {
      // Pokud jsou oba datumy ve stejnem roce i mesici, musim testovat jeste den
      if (KoncoveDatum->Day<=PocatecniDatum->Day) {
        // Sem dam i rovno (tj. datumy jsou stejne), i kdyz bych mohl jit dal a korektne by mne to vypocitalo nulu, ale je to zbytecne, stejne by vysla nula.
        return 0;
      }//if
    }//else
  } else {
    // Datumy jsou z formalniho hlediska v poradku, ale do unsigned int se mne nevleze rozdil vetsi jak 65535 dnu, tj. 179 let. Pokud je rozdil
    // roku vetsi jak 179, nemuzu pocitat dal, vratilo by mne to blbost
    if (KoncoveDatum->Year-PocatecniDatum->Year > 179) return 0;
  }//else
  // Pokud dosel az sem, datumy se daji odecitat, koncovy datum je az za pocatecnim datem
  if (KoncoveDatum->Year==PocatecniDatum->Year) {
    // Pokud se roky rovnaji, je rozdil mene nez 1 rok a pocitam to jinak
    if (PocatecniDatum->Year%4==0) Rozdil=366; else Rozdil=365;   // Normalni nebo prechodny rok
    Rozdil-=PocetDnuOdPocatkuRoku(PocatecniDatum)+PocetDnuDoKonceRoku(KoncoveDatum);
  } else {
    // Roky se nerovnaji
    // Prictu zbytek dnu do konce pocatecniho roku
    Rozdil=PocetDnuDoKonceRoku(PocatecniDatum);
    // Prictu cele roky mezi pocatecnim a koncovym rokem
    for (Rok=PocatecniDatum->Year+1; Rok<KoncoveDatum->Year; Rok++) {
      if (Rok%4==0) Rozdil+=366; else Rozdil+=365;   // Normalni nebo prechodny rok
    }//for
    // Prictu pocet dnu od zacatku koncoveho roku do koncoveho data
    Rozdil+=PocetDnuOdPocatkuRoku(KoncoveDatum);
  }//else
  return Rozdil;
}
