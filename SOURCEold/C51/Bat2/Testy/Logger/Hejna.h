//*****************************************************************************
//
//    Hejna.h      - Editace a parametry hejna
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hejna_H__
   #define __Hejna_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

extern word        xdata CisloDneVykrmu;      // Cislo dne od pocatku vykrmu
extern TVykrmHejna xdata VykrmHejna;          // Data potrebna pro chod vykrmu aktualniho dne

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

#define ProbihaKrmeni() (Konfigurace.AktualniHejno < MAX_POCET_HEJN)
// Vyjadruje podminku, zda se prave krmi nebo ne - vlozi se napr. do if(ProbihaKrmeni()), if (!ProbihaKrmeni())

void NactiParametryVykrmu( void);
// Pokud se prave krmi, nacte aktualni hejno do pameti

void AktualizujDenVykrmu( void);
// Aktualizuje cislo dne vykrmu, dopocte parametry dne

bit EditaceHejna( byte Cislo);
// Edituje vsechny parametry hejna s cislem Cislo
// Pokud ma parametr Cislo nastaven MSB bit, chce zalozit nove hejno.

bit SmazatHejno( byte Cislo);
// Smaze ze seznamu hejn hejno s cislem Cislo

byte VyberHejno( void);
// Vybere 1 hejno z existujicich hejn a vrati jeho cislo.
// Pokud nevybere, vrati CISLO_PRAZDNEHO_HEJNA

#endif

