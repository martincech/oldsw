//*****************************************************************************
//
//    NVahy.h - Naslapne vahy
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __NVahy_H__
   #define __NVahy_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#ifndef __Stat_H__
   #include "..\inc\Stat.h"  // statistika
#endif

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

// Stavy naslapnych vah :

typedef enum {
  STAV_NV_INICIALIZACE,                 // Po zapnuti cekam, az se mne naplni historie nejakymi hmotnostmi
  STAV_NV_CEKAM_NA_NASTUP,              // Vaha ceka na vzrust hmotnosti tak, aby vzrust padnul do rustove krivky
  STAV_NV_CEKAM_NA_USTALENI,            // Hmotnost na vaze stoupla a padlo to do krivky, cekam na ustaleni vahy, abych ulozil ustalenou hmotnost
  STAV_NV_CEKAM_PO_ULOZENI              // Vaha se ustalila, ulozil jsem a nyni cekam, az povolim ulozeni dalsiho kusu
} TStavNaslapneVahy;

#define HISTORIE_POCET_HMOTNOSTI    2  // interni - kolik prvku zpetne se bude uchovavat

// Popisovac vahy :
typedef struct {
   byte       Cislo;                         // cislo vahy 1..MAX_POCET_NASLAPNYCH_VAH
   long       Hmotnost;                      // aktualni mereni
   long       PosledniUlozenaHmotnost;       // jen pro testovani
   byte       PosledniUlozenePohlavi;        // jen pro testovani
   TStatistic Statistika;
   // interni kontext :
   byte       Stav;
   byte       DobaCekani;
   long       HistorieHmotnosti[ HISTORIE_POCET_HMOTNOSTI];
   long       HmotnostPredNastupem;
} TNVaha;

// Pole vah :
extern TNVaha __xdata__ NVahy[ MAX_POCET_NASLAPNYCH_VAH];

// ---------------------------------------------------------------------------------------------
// Funkce
// ---------------------------------------------------------------------------------------------

void NVahyInit( void);
// Inicializace

byte NVahyObsluha( void);
// Projede vsechny prevody naslapnych vah a pokud je nejaky novy prevod, vypocte jeho hmotnost, zkontroluje, zda se ma ulozit atd.
// Vrati hodnotu, ktera je vytvorena ORovanim konstant ZOBRAZIT_NV1 az ZOBRAZIT_NV4 a vyjadruje tak primo, ktera vaha se ma prekreslit => muze se potom
// primo ulozit do promenne Zobrazit

void NVahyEnable( void);
// Povoleni vazeni

void NVahyDisable( void);
// Zakaz vazeni

void NVahyNovyDen( void);
// Inicializace noveho dne - ulozeni/mazani statistik

#endif
