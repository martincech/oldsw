//*****************************************************************************
//
//    Hardware.h  - Step hardware descriptions
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE

#include "Misc.h"               // OtestujAccuOK

#define AgeAccuOk() OtestujAccuOK()

sbit AgeCLK    = P3^3;
sbit AgeDI     = P2^6;
sbit AgeRW     = P2^7;
sbit AgeAD1    = P2^1;
sbit AgeAD2    = P2^0;
sbit AgeLATCH  = P3^4;
sbit AgeCSEE1  = P3^6;
sbit AgeCSEE2  = P3^7;
sbit AgeE      = P3^5;

//-----------------------------------------------------------------------------
// Hardwarove pripojeni displeje
//-----------------------------------------------------------------------------
sbit DisplayDI =    P2^6;
sbit DisplayRW =    P2^7;
sbit DisplayE =     P3^5;
sbit DisplayCS1 =   P1^0;
sbit DisplayCS2 =   P1^1;
sbit DisplayRESET = P1^2;
sbit DisplayD0 =    P0^7;
sbit DisplayD1 =    P0^6;
sbit DisplayD2 =    P0^5;
sbit DisplayD3 =    P0^4;
sbit DisplayD4 =    P0^3;
sbit DisplayD5 =    P0^2;
sbit DisplayD6 =    P0^1;
sbit DisplayD7 =    P0^0;

//sbit KbdK0 = P2^5;
sbit KbdK0 = P2^2;
sbit KbdK1 = P2^3;
sbit KbdK2 = P2^4;

#define DatovaSbernice P0               // Datova sbernice D0-D7, ktera je sdilena

// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ idata


//-----------------------------------------------------------------------------
// LowBat
//-----------------------------------------------------------------------------

sbit EN573   = P1^6;
sbit LowBat  = P0^1;
sbit ENVYST1 = P1^4;         // chipselect vstupu 0-7
sbit ENVYST2 = P1^5;         // chipselect vstupu 8-15

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 18.432 MHz
#define FXTAL 18432000L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define T2_X2      1            // timer 2 v rezimu X2
#define SI_X2      1            // COM v rezimu X2
#define PCA_X2     1            // PCA v rezimu X2
#define WD_X2      1            // WatchDog v rezimu X2
#define SPI_X2     1            // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost - nechci

// Casovac 0 :
#define TIMER0_PERIOD 20        // perioda casovace 0 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM


//-----------------------------------------------------------------------------
// Pripojeni AT25640 / X25645
//-----------------------------------------------------------------------------
// Tady je zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

// zapojeni pinu volne k uzivani :
#define EepCS   AgeCSEE1           // chipselect /CS
#define EepSCK  AgeRW              // hodiny SCK
#define EepSO   AgeDI              // vystup dat SO
#define EepSI   AgeE               // vstup dat SI (muze byt shodny s SO)
// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// ochrana proti vybiti aku :
#define EepAccuOk()       AgeAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :

// Atmel :
//#define EEP_PAGE_SIZE    64      // velikost stranky
//#define EEP_SIZE      32768      // celkova kapacita
// X25645 :
#define EEP_PAGE_SIZE    32      // velikost stranky
#define EEP_SIZE       8192      // celkova kapacita


// Podminena kompilace :

#define EEP_READ_BYTE    1       // cteni jednoho bytu
#define EEP_WRITE_BYTE   1       // zapis jednoho bytu

//-----------------------------------------------------------------------------
// A/D prevodnik ADS1241
//-----------------------------------------------------------------------------

#define AdcDIN        AgeLATCH     // vstup
#define AdcDOUT       AgeAD2       // vystup
#define AdcSCLK       AgeAD1       // hodiny
#define AdcDRDY       AgeCLK       // -ready

#define AdcClrCS()                 // chipselect vyber
#define AdcSetCS()                 // deselect

#define ADC_GAIN       ADC_GAIN_1      // zakladni zesileni
#define ADC_RATE       ADC_7_5HZ       // rychlost vzorkovani
#define ADC_POLARITY   ADC_BIPOLAR     // rezim polarity
#define ADC_RANGE      ADC_FULL_RANGE  // plny/polovicni rozsah

#define ADC_INTERRUPT  INT_EXTERNAL1   // rezim na pozadi, vstup preruseni
#define __adc_data__   xdata           // umisteni namerenych hodnot
#define __adc_status__                 // umisteni stavoveho bytu

//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------
// zapojeni pinu volne k uzivani :

//sbit IicSCL     = P2^2;         // I2C hodiny
sbit IicSCL     = P2^5;         // I2C hodiny
sbit IicSDA     = P3^5;         // I2C data

// casove prodlevy

#define IIC_WAIT   { _nop_(); _nop_(); }      // 4takty + doplnit NOPy na 5us

// podmineny preklad :

#define IIC_READ    1           // cteni dat se sbernice

//-----------------------------------------------------------------------------
// Pripojeni RTC PCF8583 pres I2C sbernici
//-----------------------------------------------------------------------------

#define RTC_ADDRESS 0           // adresa zadratovana pinem A0

// podmineny preklad :
//#define RTC_USE_HSEC    1       // cti/nastavuj setiny sekund
#define RTC_USE_DATE    1       // cti/nastavuj datum
//#define RTC_USE_WDAY    1       // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1       // hlidej zadavane hodnoty na rozsah
//#define RTC_USE_ALARM   1       // Pouzivani alarmu

//-----------------------------------------------------------------------------





//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu s AT25640 / X25645
//-----------------------------------------------------------------------------
// Tady je taky zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

sbit XmemCS=P1^7;     // chipselect /CS
sbit XmemSCK=P2^7;    // hodiny SCK
sbit XmemSO=P0^2;     // vystup dat SO
sbit XmemSI=P3^5;     // vstup dat SI (muze byt shodny s SO)
sbit XmemINS=P0^1;    // signal pritomnosti modulu aktivni v 0

//#define XmemInsert !XmemINS       // Flag, zda je modul pripojen nebo ne
//#define XMEM_USE_INS   1          // Je pritomen HW signal INS

// Inverze signalu:
#define XMEM_XSI_H  0
#define XMEM_XSCK_H 0

// ochrana proti vybiti aku :
#define XmemAccuOk()       AgeAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :
#define XMEM_PAGE_SIZE    64      // velikost stranky
#define XMEM_SIZE      32768      // kapacita externiho pametoveho modulu

// Podminena kompilace :

//#define XMEM_READ_BYTE    1       // cteni jednoho bytu
//#define XMEM_WRITE_BYTE   1       // zapis jednoho bytu


//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

#define RS232_BAUD      9600          // Rychlost linky v baudech
#define RS232_USE_PARITY    1         // Provoz s paritou nebo bez

#define RS232_PARITY_EVEN   1         // Suda parita
//#define RS232_PARITY_ODD   1        // Licha parita

//#define RS232_CASOVAC_1    1        // K casovani linky se vyuzije casovac 1
#define RS232_CASOVAC_2    1          // K casovani linky se vyuzije casovac 2 (pouzitelne jen u C51RD2, ne u maleho Atmela)

//#define RS232_GET           1         // Zda se pouziva cteni ze seriove linky
#define RS232_SEND          1         // Zda se pouziva zapis na seriovou linku

//-----------------------------------------------------------------------------
#include "StepHW.h"         // projektove nezavisle definice
//-----------------------------------------------------------------------------

#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------
#endif
