//*****************************************************************************
//
//    Step.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Step_H__
   #define __Step_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif


#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif



//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE    0x0100       // verze SW

#define NOSNOST_NASLAPNYCH_VAH      60000
#define DILEK_NASLAPNYCH_VAH        1
#define POCETDESETIN_NASLAPNYCH_VAH 3
#define MAX_POCET_NASLAPNYCH_VAH    4  // Maximalni pocet naslapnych vah

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
// POZOR, nemenit poradi

#define ZOBRAZIT_NV1    0x01               // Zobrazi se 1. naslapna vaha
#define ZOBRAZIT_NV2    0x02               // Zobrazi se 2. naslapna vaha
#define ZOBRAZIT_NV3    0x04               // Zobrazi se 3. naslapna vaha
#define ZOBRAZIT_NV4    0x08               // Zobrazi se 4. naslapna vaha
#define ZOBRAZIT_MRIZKU 0x10               // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_VSE    0xFF               // Zobrazi se vse (jsou nastaveny vsechny bity)

#define ZobrazitVahuCislo( c)     (1 << (c))               // <c> cislo vahy 0..MAX_POCET_NASLAPNYCH_VAH-1

bit OtestujAccuOK();


// ---------------------------------------------------------------------------------------------
// Rustova krivka
// ---------------------------------------------------------------------------------------------

#define KRIVKA_MAX_POCET         30          // Max pocet pole s definici rustove krivky
#define KRIVKA_MAX_DEN           999         // Max cislo dne
#define KRIVKA_MAX_HMOTNOST      0xFFFF      // Max hmotnost (aby se vlezla do 2 bajtu)
#define KRIVKA_HMOTNOST_UKONCENI 0           // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)
#define KRIVKA_CISLO_PRVNIHO_DNE 1           // Cislo prvniho dne, od ktereho se zacina krmit

// Format bodu rustove krivky
typedef struct {
  word Den;       // 0-KRIVKA_MAX_DEN (999)
  word Hmotnost;  // 0-KRIVKA_MAX_HMOTNOST (0xFFFF)
} TBodKrivky;     // 4 bajty

// Pole krivky
typedef TBodKrivky TBodyKrivky[KRIVKA_MAX_POCET];  // 120 bajtu

//-----------------------------------------------------------------------------
// Hejna
//-----------------------------------------------------------------------------

#define MAX_DELKA_NAZVU_HEJNA 8          // Maximalni delka
#define CISLO_PRAZDNEHO_HEJNA 0xFF       // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane
#define NEVYPLNENY_CAS_OMEZENI 0xFF      // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale
#define MAX_HODINA 23                    // Maximalni hodnota hodiny pro omezeni (0-23hod)

// Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
typedef struct {
  byte Min;
  byte Hour;
  byte Day;
  byte Month;
  word Year;
} TDateTime;

// Zahlavi hejna
typedef struct {
  byte Cislo;                        // Cislo hejna
  byte Nazev[MAX_DELKA_NAZVU_HEJNA]; // Nazev hejna (bez ukoncovaci nuly, prazdne znaky se doplni mezerami)
  byte RozdelovatPohlavi;            // Flag, zda se ma pouzivat rozliseni na samce a samice
  byte VazitOdHodiny;                // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno NEVYPLNENY_CAS_OMEZENI, neomezuje se
  byte VazitDoHodiny;                // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno NEVYPLNENY_CAS_OMEZENI, neomezuje se
} TZahlaviHejna;

// Struktura jednoho hejna
typedef struct {
  TZahlaviHejna Zahlavi;
  TBodyKrivky   RustovaKrivkaSamice;          // Rustova krivka pro samice, pripadne celkova rustova krivka, pokud se rozdeleni na samce a samice nepouziva
  TBodyKrivky   RustovaKrivkaSamci;           // Rustova rivka pro samce, pokud se pouziva
} THejno;

#define MAX_POCET_HEJN 5                      // Pocet hejn, ktere muze zadat
typedef THejno THejna[MAX_POCET_HEJN];        // Cisla hejn odpovidaji pozici, tj. na pozici 0 je hejno cislo 0, na pozici 1 hejno cislo 1 atd.

// Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
// veci, ktere jsou stale potreba, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake zmene v EEPROM, pri prechodu
// na dalsi den atd.
// Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu

typedef struct {
  TZahlaviHejna Zahlavi;
  unsigned int  NormovanaHmotnostSamice, NormovanaHmotnostSamci;  // Normovana hmotnost nactena z rustove krivky pro samice a samce. Pokud se pohlavi nerozdeluje, bere se pro samice.
  long          HorniMezSamice, DolniMezSamice;                   // Dolni a horni mez hmotnosti z rustove krivky samic, pripadne pro obe pohlavi, pokud se rozdeleni na samce a samice nepouziva
  long          HorniMezSamci, DolniMezSamci;                     // Dolni a horni mez hmotnosti z rustove krivky samcu
} TVykrmHejna;

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

// Kalibrace 1 vahy
typedef struct {
  long KalibraceNuly;          // Prevodnik je nebo bude 24 bitovy => musim long
  long KalibraceRozsahu;
  word Rozsah;
} TJednaKalibrace;

// Kalibraci vseho drzim zvlast, aby se pri zmene verze firmware neprepsala dafaultnimi hodnotami
typedef struct {
  TJednaKalibrace KalibraceNaslapnychVah[MAX_POCET_NASLAPNYCH_VAH];  // Kalibrace jednotlivych zavesnych vah
} TKalibrace;

#define MAX_IDENTIFIKACNI_CISLO 31      // Maximalni hodnota identifikacniho cisla

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                        // VERZE
  byte IdentifikacniCislo;           // identifikace zarizeni
  byte SamiceOkoliNad;               // Okoli nad prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamiceOkoliPod;               // Okoli pod prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamciOkoliNad;                // Okoli nad prumerem pro samce v procentech
  byte SamciOkoliPod;                // Okoli pod prumerem pro samce v procentech
  byte UstaleniNaslapneVahy;         // Ustaleni hmotnosti po nastupu v procentech prvni zvazene hmotnosti

  byte      AktualniHejno;           // Cislo hejna, podle ktereho se prave krmi. Pokud se krmi zadne hejno, je zde ulozena hodnota CISLO_PRAZDNEHO_HEJNA
  word      PosunutiKrivky;          // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.
  TDateTime ZahajeniVykrmu;          // Datum zahajeni vykrmu

  byte KontrolniSuma;                // Kontrolni soucet
} TKonfigurace;

extern TKonfigurace __xdata__ Konfigurace;   // Buffer konfigurace v externi RAM
extern TKalibrace   __xdata__ Kalibrace;     // Buffer kalibrace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast

#define CFG_ALL                       0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERZE                     0x00000001L
#define CFG_IDENTIFIKACNI_CISLO       0x00000002L
#define CFG_SAMICE_OKOLI_NAD          0x00000004L
#define CFG_SAMICE_OKOLI_POD          0x00000008L
#define CFG_SAMCI_OKOLI_NAD           0x00000010L
#define CFG_SAMCI_OKOLI_POD           0x00000020L
#define CFG_USTALENI_NASLAPNE_VAHY    0x00000040L
#define CFG_AKTUALNI_HEJNO            0x00000080L
#define CFG_POSUNUTI_KRIVKY           0x00000100L
#define CFG_ZAHAJENI_VYKRMU           0x00000200L

//-----------------------------------------------------------------------------
// Struktura cele externi EEPROM
//-----------------------------------------------------------------------------

#define EEP_SPARE (EEP_SIZE - sizeof(THejna))  // Velikost mezery na konci externi EEPROM

typedef struct {
  byte   Volne[EEP_SPARE];  // !!!
  THejna Hejna;             // Definice jednotlivych hejn
} TExtEEPROM;

//-----------------------------------------------------------------------------
// Struktura cele interni EEPROM
//-----------------------------------------------------------------------------


#define IEP_SPARE (IEP_SIZE - sizeof(TKalibrace) - sizeof(TKonfigurace))

typedef struct {
  TKalibrace    Kalibrace;             // Kalibrace vseho - tu drzim zvlast. Schvalne ji mam hned na zacatku, aby se pri zmene delky cehokoliv neporusila
  TKonfigurace  Konfigurace;           // Konfigurace
  byte          Volne[IEP_SPARE];
} TIntEEPROM;

//-----------------------------------------------------------------------------
#endif
