//*****************************************************************************
//
//    Misc.c - Age miscelaneous input
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Misc_H__
   #define __Misc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

bit OtestujAccuOK();
// Otestuje napajeci napeti a kdyz je v poradku, vrati 1.


#endif
