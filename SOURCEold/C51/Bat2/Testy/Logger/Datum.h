//*****************************************************************************
//
//    Datum.h - Prace s datem
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Datum_H__
   #define __Datum_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

unsigned int PocetDnuOdPocatkuRoku(TDateTime *Datum);
// Vypocte pocet celych dnu od pocatku roku do zadaneho data. Nezapocitavam aktualni den, pouze cele, jiz ubehnute dny.

unsigned int PocetDnuDoKonceRoku(TDateTime *Datum);
// Vypocte pocet celych dnu od zadaneho data do konce roku. Pokud je v puli dne, zapocitava se i aktualni den, na rozdil od fce PocetDnuOdPocatkuRoku().

unsigned int PocetDnuMeziDatumy(TDateTime *PocatecniDatum, TDateTime *KoncoveDatum);
  // Vypocte pocet celych dnu mezi dvema datumy. Bere se pocet celych dnu, tj. nebere se v uvahu cas.

#endif
