//*****************************************************************************
//
//    Serial.h - Komunikace seriovym portem
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Serial_H__
   #define __Serial_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


void SerialInit( void);
// Inicializace komunikace

void SerialSendWeight( byte Number, long Weight);
// Vysle aktualni hmotnost

void SerialSendSavedWeight( byte Number, long Weight);
// Vysle posledni ulozenou hmotnost

#endif
