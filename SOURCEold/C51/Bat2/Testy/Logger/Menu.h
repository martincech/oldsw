//*****************************************************************************
//
//    Menu.c - Age user interface
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Menu_H__
   #define __Menu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

// Globalni promenne :

extern byte idata Zobrazit;     // Hmotnost ani aku hned nezobrazuju, dokud nezmerim

void MenuInit();
// Inicializace zobrazeni

void MenuExecute( void);
// Hlavni smycka

#endif
