//*****************************************************************************
//
//    StepHW.h  - Step independent hardware descriptions
//    Verze 1.0
//
//*****************************************************************************

#ifndef __StepHW_H__
   #define __StepHW_H__

//-----------------------------------------------------------------------------
// Displej - podminena kompilace
//-----------------------------------------------------------------------------

// Obecne funkce
#define USE_DISPLEJ_ROZNOUT      1

// Cary atd.
#define USE_VYMAZ_OBLAST_RYCHLE  1
//#define USE_VYMAZ_OBLAST         1
#define USE_OBDELNIK_RYCHLE      1
#define USE_VODOROVNA_CARA       1
#define USE_VODOROVNA_TECKOVANA_CARA 1
#define USE_SVISLA_CARA_RYCHLE   1
//#define USE_SVISLA_TECKOVANA_CARA_RYCHLE 1
//#define USE_SVISLA_CARA          1
//#define USE_KRESLI_BOD           1
//#define USE_CARA                 1

// Fonty - pouziti jednotlivych fontu
#define USE_TAHOMA8    1
#define USE_LUCIDA6    1
//#define USE_MYRIAD32   1
//#define USE_MYRIAD40   1

// Vety - pouziti jednotlivych zarovnani u vet
#define USE_VETA         1
#define USE_VETA_NASTRED 1
//#define USE_VETA_NAPRAVO 1
//#define USE_VETA_NALEVO  1

// Zobrazeni cisla
#define USE_ROZLOZENI_CISLA 1
#define USE_ZOBRAZENI_CISLA 1
//#define USE_ZOBRAZENI_ZNAKU_VE_FORMATU 1

// Symbol
#define USE_SYMBOL 1

// Editace
//#define ALFANUMERICKA_KLAVESNICE 1   // Pokud je definovano, pouziva se numericka klavesnice. Jinak pouziva k editaci sipky
#define KONEC_EDITACE_KLAVESA_NULA 1  // Zda se ma pouzivat ukonceni editace pomoci klavesy Nula (krome Esc)
#define USE_EDITACE 1
#define USE_EDITACE_TEXTU 1

// Dialog
#define USE_DIALOG 1

// Menu
#define USE_MENU 1
#define MENU_MAX_POCET_POLOZEK 5        // maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
#define ObsluzModEditace()                                                                                                                              \
/*  if (ModEditace==EDITACE_PODSVIT) {                                                                                                                    \
    UN.DT=(unsigned long)(10000000*(unsigned long)Znaky[7]+1000000*(unsigned long)Znaky[6]+100000*(unsigned long)Znaky[5]+10000*(unsigned long)Znaky[4] \
          +1000*(unsigned long)Znaky[3]+100*(unsigned long)Znaky[2]+10*(unsigned long)Znaky[1]+(unsigned long)Znaky[0]);                                \
    Konfigurace.IntenzitaPodsvitu=UN.DT;                                                                                                                \
    SpustRizeniPodsvitu();                                                                                                                              \
  }                                                                                                                                                     \*/


//-----------------------------------------------------------------------------
// Displej - parametry
//-----------------------------------------------------------------------------

#define SIRKA_DISPLEJE 128
#define VYSKA_DISPLEJE 64

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       AgeAccuOk()      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

//#define IEP_READ_BYTE     1        // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1        // zapis jednoho bytu


//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
  K_RIGHT = _K_FIRSTUSER,         // sipka doprava
  K_DOWN  = _K_FIRSTUSER,         // sipka dolu, musi byt pro editaci
  K_UP,                           // sipka nahoru
  K_ENTER,                        // Enter
  K_ESC,                          // Esc
  K_NULA,                         // Nulovani
  K_BLANK,                        // Dalsi, prazdna klavesa, zatim bez urceni
  K_MENU = K_ENTER,               // Menu

  // Udalosti
  _K_EVENTS    = 0x40,        // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,   // prekresleni namerenych hodnot
  K_BLINK_OFF,                // periodicke blikani - zhasni
  K_BLINK_ON,                 // periodicke blikani - rozsvit
  K_TIMEOUT,                   // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,   // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,   // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF    // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (400/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

//#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Statistika
//-----------------------------------------------------------------------------

typedef float TNumber;                 // zakladni datovy typ
#define __statistic__  __xdata__       // pouziva se interni XRAM

#define STAT_UNIFORMITY  1             // povoleni vypoctu uniformity

//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __Step_H__
  #include "Step.h"                   // Dal jsem si to do adresace projektu, ne do spolecneho inc, kde je to neprehledne
#endif
//-----------------------------------------------------------------------------

#endif
