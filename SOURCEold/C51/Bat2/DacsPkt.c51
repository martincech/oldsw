//*****************************************************************************
//
//    DacsPkt.c - kodovani a dekodovani paketu ACS
//    Version 1.0, Petr Veit
//
//*****************************************************************************

#include "DacsPkt.h"
#include "Dacs485.h"                // Komunikace pres RS-485

#include <stdlib.h>                 // atoi

#ifdef VERSION_DACS_LW1

// ---------------------------------------------------------------------------------------------
// Globalni promenne
// ---------------------------------------------------------------------------------------------

TDacsPacket __xdata__ DacsPacket;

// ---------------------------------------------------------------------------------------------
// Definice
// ---------------------------------------------------------------------------------------------

#define PACKET_MIN_LENGTH       15                              // Minimalni delka platneho paketu tak, aby obsahoval aslespon 1 parametr

// ---------------------------------------------------------------------------------------------
// Lokalni promenne
// ---------------------------------------------------------------------------------------------

static byte __xdata__           PacketNumber;                   // Cislo prijateho paketu v ASCII, ktere je treba odeslat v odpovedi

// ---------------------------------------------------------------------------------------------
// Lokalni funkce
// ---------------------------------------------------------------------------------------------

static TYesNo CheckLrc(void);
// Zkontroluje LRC paketu v Buffer, pocet znaku v bufferu je ComBufferCount. Buffer zacina adresou.

static TYesNo CheckHeader(byte *DataIndex);
// Zkotroluje hlavicku a LRC paketu v ComBuffer, delka paketu je ComBufferCount. Paket zacina adresou a konci znakem END_OF_PACKET.
// Pokud je hlavicka v poradku, vrati v <DataIndex> index zacateku dat v paketu a fce vrati YES.
// Vyplni pozadovany prikaz do DacsPacket.Command

static TYesNo DecodeNumbers(byte __xdata__ *Data);
// Dekoduje cisla v <Data> a ulozi je do DacsPacket.Data[]. Cisla jsou oddelena carkami, string je ukoncen znakem END_OF_TEXT.
// Text muze byt "1,2ETX" nebo "1,2,ETX", tj. carka muze byt i za poslednim cislem

static void AddNumber(int Number);
// Prida <Number> a carku do ComBuffer

static void AddLrc(void);
// Vypocte LRC a prida ho na konec paketu (paket musi koncit znakem END_OF_TEXT)

static void AddChar(byte ch);
// Prida do ComBuffer[] znak <ch>

static void CreatePacket(void);
// Vytvori paket ze struktury DacsPacket

// ---------------------------------------------------------------------------------------------
// Inicializace
// ---------------------------------------------------------------------------------------------

void DacsPacketInit(void) {
  // Inicializace
  Dacs485Init();                        // Inicializace seriove linky
} // DacsPacketInit

// ---------------------------------------------------------------------------------------------
// Zakaz cinnosti
// ---------------------------------------------------------------------------------------------

void DacsPacketDisable(void) {
  // Pozastaveni cinnosti
  Dacs485Disable();
} // DacsPacketDisable

// ---------------------------------------------------------------------------------------------
// Nacteni paketu
// ---------------------------------------------------------------------------------------------

TYesNo DacsPacketRead(void) {
  // Nacteni a dekodovani paketu, volat co nejcasteji. Pokud je nacten paket, vrati YES.
  byte DataIndex;                       // Zacatek dat v paketu

  if (!Dacs485ReadPacket()) {
    return NO;                          // Zadny paket neceka na zpracovani
  }

  // Nacteny paket je v ComBuffer, delka paketu je ComBufferCount. Paket zacina adresou a konci znakem END_OF_PACKET.
  if (!CheckHeader(&DataIndex)) {
    // Chybny paket
    Dacs485InitReceiver();              // Start prijmu dalsiho paketu
    return NO;
  }

  // Dekodovani dat do pole Data[]
  if (!DecodeNumbers(&ComBuffer[DataIndex])) {
    DacsPacket.Command = COMMAND_ER;    // Syntakticka chyba v paketu, odpovim chybu
  }

  return YES;                           // Paket jsem uspesne dekodoval, cislo paketu zustane v PacketNumber.
} // DacsPacketRead

// ---------------------------------------------------------------------------------------------
// Poslani paketu
// ---------------------------------------------------------------------------------------------

void DacsPacketSend(void) {
  // Posle paket, ktery je definovan ve strukture DacsPacket

  // Vytvorim string paketu
  CreatePacket();
  // Poslu paket
  Dacs485SendPacket();
} // DacsPacketSend

// ---------------------------------------------------------------------------------------------
// Kontrola LRC
// ---------------------------------------------------------------------------------------------

static TYesNo CheckLrc(void) {
  // Zkontroluje LRC paketu v Buffer, pocet znaku v bufferu je ComBufferCount. Buffer zacina adresou.
  byte j;
  byte Checksum = START_OF_PACKET;      // Vcetne prvniho znaku START_OF_PACKET

  for (j = 0; j < ComBufferCount; j++) {
    Checksum ^= ComBuffer[j];
    if (ComBuffer[j] == END_OF_TEXT) {
      break;                            // Konec paketu
    }
  }
  j++;                                  // LRC je za znakem END_OF_TEXT
  Checksum ^= 0xFF;
  return (TYesNo)(Checksum == ComBuffer[j]);
} // CheckLrc

// ---------------------------------------------------------------------------------------------
// Kontrola hlavicky paketu a LRC
// ---------------------------------------------------------------------------------------------

static TYesNo CheckHeader(byte *DataIndex) {
  // Zkotroluje hlavicku a LRC paketu v ComBuffer, delka paketu je ComBufferCount. Paket zacina adresou a konci znakem END_OF_PACKET.
  // Pokud je hlavicka v poradku, vrati v <DataIndex> index zacateku dat v paketu a fce vrati YES.
  // Vyplni pozadovany prikaz do DacsPacket.Command
  byte Address;                         // Cilova adresa paketu
  byte Index = 0;                       // Index pro prohledavani pole

  // Overim delku paketu - hlavicku paketu tak budu moci projet bez testovani koncoveho znaku a prekroceni delky paketu
  if (ComBufferCount < PACKET_MIN_LENGTH) {
    return NO;
  }
  // Adresa
  Address = ComBuffer[Index++];         // Zatim ponecham v ASCII
  // START_OF_TEXT
  if (ComBuffer[Index++] != START_OF_TEXT) {
    return NO;
  }
  // Opakovani adresy
  if (ComBuffer[Index++] != Address) {  // Porovnat muzu v ASCII
    return NO;
  }
  // Kontrola adresy (prevedu z ASCII na binarni)
  if (Address - 0x30 != Config.Rs485.Address) {
    // Paket nepatri mne
    return NO;
  }
  // Kontrola checksum
  if (!CheckLrc()) {
    return NO;
  }
  // Prikaz (2 znaky)
  if (ComBuffer[Index] == 'R') {
    if (ComBuffer[Index + 1] == 'D') {
      // Prikaz RD
      DacsPacket.Command = COMMAND_RD;
    } else if (ComBuffer[Index + 1] == 'L') {
      // Prikaz RL
      DacsPacket.Command = COMMAND_RL;
    }
  } else if (ComBuffer[Index] == 'W' && ComBuffer[Index + 1] == 'D') {
    // Prikaz WD
    DacsPacket.Command = COMMAND_WD;
  } else {
    // Neznamy prikaz
    DacsPacket.Command = COMMAND_ER;
  }
  Index += 2;
  // Cislo paketu
  PacketNumber = ComBuffer[Index++];       // Ponecham v ASCII
  // Vynecham carku
  Index++;
  // Vynecham "BLOCK,"
  while (ComBuffer[Index++] != ',');
  // Vynecham "MENU," (2 znaky) - jen u prikazu RL a WD
  if (DacsPacket.Command == COMMAND_RL || DacsPacket.Command == COMMAND_WD) {
    while (ComBuffer[Index++] != ',');
  }
  // Zde zacinaji data
  *DataIndex = Index;
  return YES;
} // CheckHeader

// ---------------------------------------------------------------------------------------------
// Dekodovani cisel ze stringu
// ---------------------------------------------------------------------------------------------

#define NUMBER_MAX_LENGTH       6       // Maximalni delka strigu s cislem int, vcetne znamenka (bez zakoncovaci nuly)

static TYesNo DecodeNumbers(byte __xdata__ *Data) {
  // Dekoduje cisla v <Data> a ulozi je do DacsPacket.Data[]. Cisla jsou oddelena carkami, string je ukoncen znakem END_OF_TEXT.
  // Text muze byt "1,2ETX" nebo "1,2,ETX", tj. carka muze byt i za poslednim cislem
  byte Index;                           // Index v s[]
  byte s[NUMBER_MAX_LENGTH + 1];        // Vcetne zakoncovaci nuly

  DacsPacket.DataCount = 0;
  Index = 0;

  while (1) {
    s[Index] = *Data;
    if (*Data == ',' || *Data == END_OF_TEXT) {
      // Konec cisla
      if (Index == 0) {
        if (*Data == ',') {
          // Prazdne cislo (dve carky vedle sebe - to nemuze byt)
          return NO;
        }
        // Text konci carkou ("1,2,ETX") - to je v poradku
        return YES;
      }
      if (DacsPacket.DataCount == DATA_MAX_COUNT) {
        return NO;                      // Pocet cisel je vetsi nez mam pole
      }
      s[Index] = 0;                     // Zakoncim string (doted je na pozici Index carka nebo END_OF_TEXT, prepisu ji nulou)
      DacsPacket.Data[DacsPacket.DataCount++] = atoi(s);
      Index = 0xFF;                     // Zacinam dalsi cislo (Index++ da nulu)
      if (*Data == END_OF_TEXT) {
        return YES;                     // Konec textu
      }
    }
    Index++;
    if (Index > NUMBER_MAX_LENGTH) {
      return NO;                        // Cislo je delsi nez "-12345", to by nemelo
    }
    Data++;
  }
} // DecodeNumbers

// ---------------------------------------------------------------------------------------------
// Pridani cisla do bufferu
// ---------------------------------------------------------------------------------------------

static void AddNumber(int Number) {
  // Prida <Number> a carku do ComBuffer
  if (Number < 0) {
    AddChar('-');
    Number = -Number;
  }
  // Rozlozim cislo na jednotlive znaky a pridam je do ComBuffer[]
  UN.DT = bindec(Number);
  if (Number > 9999) {
    AddChar(0x30 + lnib(UN.ST.X2));
  }
  if (Number > 999) {
    AddChar(0x30 + hnib(UN.ST.X3));
  }
  if (Number > 99) {
    AddChar(0x30 + lnib(UN.ST.X3));
  }
  if (Number > 9) {
    AddChar(0x30 + hnib(UN.ST.X4));
  }
  AddChar(0x30 + lnib(UN.ST.X4));       // Jednotky pridam vzdy, i kdyz by byly nulove
  // Oddelovaci carka
  AddChar(',');
} // AddNumber

// ---------------------------------------------------------------------------------------------
// Vypocet LRC
// ---------------------------------------------------------------------------------------------

static void AddLrc(void) {
  // Vypocte LRC a prida ho na konec paketu (paket musi koncit znakem END_OF_TEXT)
  byte j;
  byte Checksum = START_OF_PACKET;      // Vcetne prvniho znaku START_OF_PACKET

  for (j = 3; j < ComBufferCount; j++) {
    Checksum ^= ComBuffer[j];
  }
  Checksum ^= 0xFF;
  AddChar(Checksum);
} // AddLrc

// ---------------------------------------------------------------------------------------------
// Pridani znaku do bufferu
// ---------------------------------------------------------------------------------------------

static void AddChar(byte ch) {
  // Prida do ComBuffer[] znak <ch>
  ComBuffer[ComBufferCount++] = ch;
} // AddChar

// ---------------------------------------------------------------------------------------------
// Vytvoreni hlavicky paketu
// ---------------------------------------------------------------------------------------------

static void CreatePacket(void) {
  // Vytvori paket ze struktury DacsPacket
  byte j;

  ComBufferCount = 0;

  // Uvodni pevne znaky
  AddChar(START_OF_PACKET);
  AddChar(START_OF_PACKET);
  AddChar(START_OF_PACKET);
  AddChar(0x30);
  AddChar(START_OF_TEXT);
  // Adresa
  AddChar(0x30 + Config.Rs485.Address);
  // Prikaz
  switch (DacsPacket.Command) {
    case COMMAND_RD:
      AddChar('D');
      AddChar('B');
      break;

    case COMMAND_RL:
      AddChar('L');
      AddChar('B');
      break;

    case COMMAND_WD:
      AddChar('O');
      AddChar('K');
      break;

    default:    // COMMAND_ER
      AddChar('E');
      AddChar('R');
  }
  // Cislo paketu
  AddChar(PacketNumber);                // Cislo paketu kopiruju z prijateho paketu
  AddChar(',');
  // Status
  AddNumber(0x4000);
  // Data
  for (j = 0; j < DacsPacket.DataCount; j++) {
    AddNumber(DacsPacket.Data[j]);
  }
  // End of text
  AddChar(END_OF_TEXT);
  // LRC
  AddLrc();
  // Konec paketu
  AddChar(END_OF_LRC);
  AddChar(END_OF_PACKET);
} // CreatePacket


#endif // VERSION_DACS_LW1

