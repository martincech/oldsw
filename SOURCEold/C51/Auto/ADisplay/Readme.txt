Poznamky k modulu Adisplay
--------------------------


1. Prepracovani Menu
- Umoznik poslani klavesy sam sobe :
  Upravit cekani na klavesu, je-li _Klavesa != K_NULL return( _Klavesa).
  Dusledne doplnit zrani klavesy ve vsech menu
- zavest klavesu K_MENU_START, ktera prekresli cele menu
- zavest stary/novy kurzor a prekreslit kurzorovy blok jen
  pri nerovnosti
- pred spustenim menu poslat klavesu K_MENU_START
- po ukonceni menu (krome K_TIMEOUT) poslat K_MENU_START
  na prekresleni rodice

  MenuXXX()
  {
  byte cursor, old_cursor;

     old_cursor = -1;  // prekresli kurzor
     cursor     = 0;   // prvni polozka
     while( 1){
        switch( CekejKlavesu()){
           case K_MENU_REDRAW :
              // smaz obrazkovku a prekresli cele menu
              break;

           case K_UP :
              ...
              break;
              ...

           case K_ENTER :
              switch( cursor){
              ..
              }
              PosliKlavesu( K_MENU_START);  // prekresli rodice
              return;

           case K_ESC :
              PosliKlavesu( K_MENU_START);  // prekresli rodice
              return;

           case K_TIMEOUT :
              // nezer klavesu, neprekresluj, rodic taky vypadne
              return;
        }
        ZerKlavesu();                      // spotrebuj klavesu
        if( cursor != old_cursor){
           // smaz blok
           // nakresli blok
        }
     }  // while
  } // MenuXXX


2. Rychle prekreslovani interaktivnich prvku
  reakcni doba pri prekreslovani je az 1s, proto
  by mozna bylo dobre interaktivni prvky (prepinac,
  potenciometry) prekreslovat co nejcasteji.
- do AutoData doplnit masku Prekresli a definovat bitove masky
  pro jednotlive prvky a masku prekresli vsechno
- Do WaitEvent, neni-li co delat, precti prvky a je-li
  zmena hodnoty nastav masku prekresleni a vrat K_REDRAW
- do ALog doplnit ORovani masky prekresleni

3. Rozsekat Adisplay na moduly Adisplay
   (prekreslovani zakladni obrazovky)
   AMenu, a grafiku (presunout do Sed1335)

