//*****************************************************************************
//
//    Aaccu.c - Auto Fresh air module
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __AFAir_H__
   #define __AFAir_H__

#ifndef __Adc0838_H__
   #include "..\inc\Adc0838.h"
#endif


void AFreshAirCheckSwitch();
// Nacte polohu prepinace Fresh air

unsigned char PrepoctiCerstvyVzduchNaDAC(unsigned char CerstvyVzduch);
  // Prepocte cerstvy vzduch z procent na analogovy vystup (DAC)

unsigned char PrepoctiCerstvyVzduchNaADC(unsigned char CerstvyVzduch);
  // Prepocte cerstvy vzduch z procent na analogovy vstup (ADC)

#endif
