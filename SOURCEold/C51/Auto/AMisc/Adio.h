//*****************************************************************************
//
//    Adio.c - Auto digital input/output module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __Adio_H__
   #define __Adio_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

// Struktura pro ovladani majaku (majak neblika pri chybe porad, ale prerusovane po cca 1 minute)
#define MAJAK_DOBA_ROZNUTI  5           // Doba roznuti v sekundach
#define MAJAK_DOBA_ZHASNUTI 55          // Doba zhasnuti v sekundach
typedef enum {
  MAJAK_ZHASNUTY,                       // Majak je prave zhasnuty, neni zadna porucha
  MAJAK_ROZNUTY_ON,                     // Majak prave blika a sviti
  MAJAK_ROZNUTY_OFF                     // Majak prave blika a nesviti
} TStavMajaku;
typedef struct {
  TStavMajaku Stav;                     // Aktualni stav majaku
  unsigned char Pocitadlo;              // Pocitadlo pro dobu roznuti/zhasnuti
} TMajak;

void AdioNulujPocitadloLokalniPoruchyChlazeni();

//void AdioCheckLocalFailures();
// Otestovani lokalni chyby chlazeni

void AdioReadInputs( void);
// Precte digitalni vstupy, volat podle potreby

void AdioSwitchCooler( bit status);
// Nastaveni spinace chlazeni, <status> = YES je zapnuto, NO vypnuto

void AdioSwitchPump(bit status);
// Nastaveni cerpadla topeni, <status> = YES je zapnuto, NO vypnuto

void AdioInit();
// Inicializuju alarm

void AdioSwitchAlarm( bit status);
// Nastaveni spinace majaku, <status> = YES je zapnuto, NO vypnuto

void AdioSwitchHeatingPower(bit Vysoky);
// Pri Vysoky=1 nastavi vysoky vykon topeni, pri Vysoky=0 nastavi nizky vykon topeni

TYesNo AdioBatteryOk(void);
// Precte stav baterie. Vraci YES je-li napeti v norme.
// Lze volat kdykoliv


#endif
