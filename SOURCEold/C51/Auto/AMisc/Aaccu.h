//*****************************************************************************
//
//    Aaccu.c - Auto Accumulator check module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Aaccu_H__
   #define __Aaccu_H__

#ifndef __Adc0838_H__
   #include "..\inc\Adc0838.h"
#endif


void AaccuCheck( void);
// Zkontroluje napeti a proud akumulatoru. Volat podle potreby

#endif
