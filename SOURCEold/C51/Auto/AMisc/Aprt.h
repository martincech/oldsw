//*****************************************************************************
//
//    Aprt.h - Auto protocol module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Aprt_H__
   #define __Aprt_H__

#ifndef __Auto_H__
   #include "Auto.h"     // konstanty a datove typy
#endif


void AprtInit( void);
// Inicializuje modul

TYesNo AprtWrite( byte type, dword parameter);
// Zapis do protokolu <type> je typ operace s parametrem <parameter>

TYesNo AprtReset( void);
// Smaze protokol

TYesNo AprtCopy( void);
// Prekopiruje interni EEPROM na externi modul


#endif
