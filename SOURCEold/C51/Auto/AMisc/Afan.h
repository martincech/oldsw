//*****************************************************************************
//
//    Afan.h - Auto Fan check module
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Afan_H__
   #define __Afan_H__

#ifndef __Adc0838_H__
   #include "..\inc\Adc0838.h"
#endif


void AfanCheck( void);
// Zkontroluje napeti potenciometru ventilatoru
// Volat podle potreby

#endif
