//*****************************************************************************
//
//    Areg.h - Auto temperature regulators
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Areg_H__
   #define __Areg_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void AregInit( void);
// Inicializace modulu regulatoru

void AregExecute( void);
// Provede cinnosti souvisejici s regulatory.
// Volat 1x za sekundu

#endif
