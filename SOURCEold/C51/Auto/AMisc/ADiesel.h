//*****************************************************************************
//
//    ADiesel.h - Auto check diesel motohours
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Adiesel_H__
   #define __Adiesel_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void AdslInit( void);
// Nastaveni inicialniho stavu pocitadla motohodin

void AdslExecute( void);
// Aktualizace motohodin, volat min 1x za minutu

void AdslClear( void);
// Vynulovani poctu motohodin

void AdslQuitOil( void);
// Odkvituje vymenu oleje

void AdslQuitFilter( void);
// Odkvituje vymenu filtru

//----- Interni funkce, nepouzivat :

word AdslRead( void);
// Cteni motohodin - vrati pocet motohodin

TYesNo AdslWrite( word MotoHour);
// Zapis motohodin

TYesNo AdslReset( void);
// Vymaze celou oblast

//void AdslCheckFuel();
// Nacte stav paliva v nadrzi a ulozi jej. Pokud nejde o naves, ulozim plny stav nadrze.

void AdslCheckTemp();
// Nacte teplotu dieselu a ulozi ji. Pokud se cidlo nevyuziva, ulozi nulu, pokud je cidlo odpojene (porucha), ulozi plnou hodnotu.
// Zajistuje i prumerovani.

#endif
