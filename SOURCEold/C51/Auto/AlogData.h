//*****************************************************************************
//
//    ALogData.h - Auto log data structure
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __AlogData_H__
   #define __AlogData_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

//#error "AlogData included"

//-----------------------------------------------------------------------------
// Struktura zaznamu Logu
//-----------------------------------------------------------------------------

// Komprimovany datum a cas :

typedef struct {
   byte Min        : 6;
   byte Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3;     // horni  3 bity
   byte Day        : 5;
   byte Month      : 4;
   byte Year       : 3;     // rok % 8
   byte RtcChanged : 1;     // priznak zmeny nastaveni RTC
} TShortDateTime;   // Velikost 3 bajty

// Ukladani roku: z roku se do struktury ukladaji pouze spodni 3 bity. Rekonstrukce pak probiha tak, ze hornich 13 bitu se vezme z aktualniho roku
// a doplni se temito 3 bity ze struktury a mam rok zrekonstruovany. Pokud je takto vznikly rok vetsi nez aktualni rok, odecte se od nej 8,
// protoze novejsi datum nez aktualni to byt nemohl. Funguje to pro az 8 let stare zaznamy.

// zapis hodin do struktury :
#define mkhour( dt, h)     dt.Hour_lo = (h) & 0x03; dt.Hour_hi = ((h) >> 2) & 0x07
// cteni hodin ze struktury :
#define gethour( dt)       ((dt.Hour_lo) | (dt.Hour_hi << 2))
// zapis roku do struktury :
#define mkyear( dt, y)     dt.Year = (y) & 0x07

// vlastni struktura logu :

typedef struct SAutoLog {
   // merene teploty :
   char  TeplotaVenkovni;
   char  TeplotaKanal;
   char  TeplotaPredni;
   char  TeplotaStredni;
   char  TeplotaZadni;
   char  TeplotaVyfukovana;
   // teploty pro regulator :
   char TeplotaCilova;            // pozadovana teplota regulatoru, cele stupne


   // Stavy jednotlivych serv - TServoStatusLog, na 2 bity
   byte StavNapor1       : 2,
        StavNapor2       : 2,
        StavRecirkulace  : 2,
        StavTopeni       : 2;
   byte StavPodlaha1     : 2,
        StavPodlaha2     : 2,
        StavPodlaha3     : 2,
        StavPodlaha4     : 2;

   // Zpetne hlaseni serva topeni a potenciometr topeni:
   byte ServoTopeni      : 4,
        PotTopeni        : 4;

   // mereni akumulatoru :
   byte AkuNapeti;
   byte AkuProud;

   // Stavove informace
   byte PrepinacRezimu           : 3,
        PrepinacFreshAir         : 3,
        NastartovanyDiesel       : 1,
        NastartovanyElektromotor : 1;

   byte DobijeniAku              : 1,
        SepnuteChlazeni          : 1,
        VymenaOleje              : 1,
        VymenaFiltru             : 1,
        StavNafty                : 4;

   byte PolovicniVykonVentilatoru : 1,
        PoruchaVentilatoru        : 1,
        PoruchaTopeni             : 1,
        PoruchaChlazeni           : 1,
        PoruchaDobijeni           : 1,
        PoruchaDieselu            : 1,
        AutomatickyRezim          : 1,
        Dummy2                    : 1;

   TShortDateTime DateTime;           // cas zaznamu
   byte CerstvyVzduch;

   // 15.6.2004: Lozeni
   byte Lozeni    : 3,                // TLocalizationStatus
        Dummy3    : 5;

} TAutoLog;  // Delka 20 bajtu

extern TAutoLog __xdata__ AutoLog;    // buffer logu v externi RAM

// Konstanty pro ukladani do EEPROM FIFO :

#define AUTO_FIFO_START            0  // Pocatecni adresa FIFO v EEPROM
#define AUTO_FIFO_CAPACITY       1600 // Maximalni pocet ukladanych polozek - klesla mne velikost zaznamu z 20 na 19 bajtu => mohl bych i vic, ale zatim to staci
#define AUTO_FIFO_MARKER_EMPTY  -127  // Znacka konce pri neuplnem zaplneni - teplota
#define AUTO_FIFO_MARKER_FULL   -128  // znacka konce pri prepisovani
#define AUTO_FIFO_PROTOCOL_TYPE -126  // Pokud je prvni bajt zaznamu roven tomuto, jde o zaznam s protokolem cinnosti ridice. Pokud ne, jde o normalni zaznam

// Datovy typ popisujici strukturu logu (v EEPROM) :

typedef TAutoLog TAutoLogArray[ AUTO_FIFO_CAPACITY];   // datovy logger

#endif
