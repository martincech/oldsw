//*****************************************************************************
//
//    AutoHW.h  - Auto independent hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __AutoHW_H__
   #define __AutoHW_H__

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut AutoPISK         // vystup generatoru

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1        // asynchronni pipani

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       AutoAccuOk()      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

#define IEP_READ_BYTE     1        // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1        // zapis jednoho bytu

//-----------------------------------------------------------------------------
// Pripojeni AT25640 / X25645 na desce
//-----------------------------------------------------------------------------
// Tady je zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

// zapojeni pinu volne k uzivani :
#define EepCS    AutoCSEE           // chipselect /CS
#define EepSCK   AutoCLK            // hodiny SCK
#define EepSO    AutoSO             // vystup dat SO
#define EepSI    AutoSI             // vstup dat SI (muze byt shodny s SO)
// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// ochrana proti vybiti aku :

#define EepAccuOk()       AutoAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    64      // velikost stranky
#define EEP_SIZE      32768      // celkova kapacita

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// Podminena kompilace :

#define EEP_READ_BYTE    1       // cteni jednoho bytu
#define EEP_WRITE_BYTE   1       // zapis jednoho bytu



//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu s AT25640 / X25645
//-----------------------------------------------------------------------------
// Tady je taky zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

#define XmemCS     AutoCSMOD     // chipselect /CS
#define XmemSCK    AutoCLKMOD    // hodiny SCK
#define XmemSO     AutoSOMOD     // vystup dat SO
#define XmemSI     AutoSIMOD     // vstup dat SI (muze byt shodny s SO)
//#define XmemInsert !AutoINSMOD   // signal pritomnosti modulu aktivni v 0
//#define XmemInsert 1             // Modul stale vlozen - INS uz neobsluhuju, jen na zacatku kopirovani otestuju pritomnost

// ochrana proti vybiti aku :

#define XmemAccuOk()       AutoAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :
#define XMEM_PAGE_SIZE    64      // velikost stranky
#define XMEM_SIZE      32768      // kapacita externiho pametoveho modulu

// Inverze signalu:
#define XMEM_XSI_H  1
#define XMEM_XSCK_H 1

// Podminena kompilace :

//#define XMEM_READ_BYTE    1       // cteni jednoho bytu
//#define XMEM_WRITE_BYTE   1       // zapis jednoho bytu
//#define XMEM_USE_INS      1       // Zda se vyuziva signal INS

//-----------------------------------------------------------------------------
// Pripojeni 1-Wire sbernic
//-----------------------------------------------------------------------------


#define MwiCS    AutoCSCIDLA      // chipselect Latche
#define MwiDATA  AutoDATA         // datova sbernice

#define MwiDQI0  AutoT1           // T1 Data Input Channel 0
#define MwiDQI1  AutoT2           // T2 Data Input Channel 1
#define MwiDQI2  AutoT3           // T3 Data Input Channel 2
#define MwiDQI3  AutoT4           // T4 Data Input Channel 3
#define MwiDQI4  AutoT5           // T5 Data Input Channel 4
#define MwiDQI5  AutoT6           // T6 Data Input Channel 5 - rezerva

#define MwiDQO   AutoTOUT         // Data Output Channel 0

//#define MwiD7    AutoTGND         // Data Input/Output - rezerva

// logicke urovne na vystupu :

#define DQO_LO    1               // je pres tranzistor, inverzni logika
#define DQO_HI    0

// casove prodlevy :

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

// Celkove zpozdeni cyklu for 2 + 4 * n

#define MWI_DELAY_RESET          MwiMicroDelay( 122)  // Zpozdeni 480 us pro reset
#define MWI_DELAY_PRESENCE       MwiMicroDelay( 17)   // Zpozdeni 70us mezi reset a presence
#define MWI_DELAY_RESET_TIMESLOT MwiMicroDelay( 104)  // Zpozdeni 410us mezi presence a dalsim
#define MWI_DELAY_START          _nop_();_nop_();_nop_()  // minimalni delka startbitu > 1us
#define MWI_DELAY_READ           11                   // Zpozdeni 15us v cyklu for
#define MWI_DELAY_READ_SLOT      MwiMicroDelay( 24)   // Zpozdeni 100us
#define MWI_DELAY_WRITE          MwiMicroDelay( 27)   // Zpozdeni 110us



// zakaz a povoleni interruptu, pouzito pro kriticky casovane sekce :

#define MwiDisableInts()         EA = 0
#define MwiEnableInts()          EA = 1

// identifikace kanalu a zaroven podmineny preklad :

#define MWI_CH0                  0                    // preklada se kanal 0
#define MWI_CH1                  1                    // preklada se kanal 1
#define MWI_CH2                  2                    // preklada se kanal 2
#define MWI_CH3                  3                    // preklada se kanal 3
#define MWI_CH4                  4                    // preklada se kanal 4
#define MWI_CH5                  5                    // preklada se kanal 5 - pridano, vyfukovana teplota

// podmineny preklad narocnych funkci :

#define MWI_CRC8                 1                    // preklada se kod pro vypocet CRC

//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------

#define IicSCL  AutoSCL         // I2C hodiny
#define IicSDA  AutoSI          // I2C data

// casove prodlevy

#define IIC_WAIT   {byte i; for( i = 6; i > 0; i--);}  // 4takty + doplnit NOPy na 5us

// podmineny preklad :

#define IIC_READ    1           // cteni dat se sbernice

//-----------------------------------------------------------------------------
// Pripojeni ADC 0838
//-----------------------------------------------------------------------------

#define AdcDIO  AutoSO         // DI + DO spolecna data
#define AdcCLK  AutoCLK        // CLK hodiny

// casove prodlevy

#define AdcWait() _nop_();_nop_();_nop_();_nop_();   // max. hodinovy kmitocet 400kHz - doplnit NOPy na 1.25us

// ovladani chipselectu - pro vice prevodniku nahradit prazdnym makrem

#define AdcEnable()         // externi chipselect
#define AdcDisable()

//-----------------------------------------------------------------------------
// Pripojeni DA prevodniku TDA8444 pres I2C sbernici
//-----------------------------------------------------------------------------

#define DAC_ADDRESS 0           // adresa zadratovana piny A0..A2

// podmineny preklad :

#define DAC_SINGLE_WRITE 1      // zapis do jednoho prevodniku

//-----------------------------------------------------------------------------
// Pripojeni RTC PCF8583 pres I2C sbernici
//-----------------------------------------------------------------------------

#define RTC_ADDRESS 0           // adresa zadratovana pinem A0

// podmineny preklad :

//#define RTC_USE_HSEC    1       // cti/nastavuj setiny sekund
#define RTC_USE_DATE    1       // cti/nastavuj datum
//#define RTC_USE_WDAY    1       // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1       // hlidej zadavane hodnoty na rozsah

//-----------------------------------------------------------------------------
// Teplomery DS18B20
//-----------------------------------------------------------------------------

#define TEMP_DS18B20       1      // definice typu DS18B20 12 bitovy
//#define TEMP_DS18S20         1      // definice typu DS18S20 9 bitovy

#define TEMP_SECURE_READ   1      // bezpecne cteni (vcetne CRC) (musi byt definovan MWI_CRC8)

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

#define _THERMO_LAST       (MWI_CH5 + 1)  // celkovy pocet teplomeru
#define THERMO_TRIALS      3              // pocet pokusu na cteni z jednoho teplomeru

//-----------------------------------------------------------------------------
// Pripojeni serv
//-----------------------------------------------------------------------------

// chipselecty prevodniku ADC 0838 :

#define AdcCS0   AutoCS1        // prevodnik 0
#define AdcCS1   AutoCS2        // prevodnik 1

// technologicke parametry serva :

// 21.3.2003: Serva se meri od 2V do 10V, ne od 1.5V
#define SERVO_ACCURACY  (ADC_RANGE * 10 / 100)     // presnost serva 10 %
#define SERVO_RESPONSE   600                       // maximalni doba odezvy serva v sekundach
#define SERVO_OFF_LIMIT (ADC_RANGE * 15 / 100)     // vypnute servo, zpetne hlaseni < 1.5V
#define SERVO_MIN       (ADC_RANGE * 20 / 100)     // Zcela zavrene servo je pri 2V, 51 = 0x33
#define SERVO_MAX       (ADC_RANGE * 96 / 100)     // Zcela otevrene servo je pri 10V, realne zmereno 9.6V, 245 = 0xF5
// 18.4.2003: krok zmenen ze 13 na 12
#define SERVO_STEP      (((SERVO_MAX-SERVO_MIN) / 16)) // zaokrouhlena kvantizace na 16 kroku (hodnota 12, presne 12,125, takze je to celkem presne a neni treba delat nejake desetinasobky)
#define SERVO_CONTROL_LIMIT (ADC_RANGE * 60 / 100) // Kdyz stoupne u nekterych serv ridici napeti nad toto, neprovadim kontrolu pozice serva. Toto napeti
                                                   // nastavit o neco nizsi nez je mechanicky doraz, pri testovani se tam neresi zadna SERVO_ACCURACY.
                                                   // Na ruskem navesu to namerilo max 7V, nastavim radsi 6.

// adresy na prevodnicich :

// Serva ventilace jsou bez ridiciho signalu, ten vypocitavam z cerstveho vzduchu
#define SERVO_NAPOR1             0                      // Napor 1
#define SERVO_NAPOR1_MEASURE     AutoAV3                // Adresa zpetneho hlaseni na prevodniku
#define SERVO_NAPOR1_MEASURE_CS  AdcCS0                 // Cipselect prevodniku hlaseni

#define SERVO_NAPOR2             1                      // Napor 2
#define SERVO_NAPOR2_MEASURE     AutoAV2                // Adresa zpetneho hlaseni na prevodniku
#define SERVO_NAPOR2_MEASURE_CS  AdcCS0                 // Cipselect prevodniku hlaseni

#define SERVO_RECIRKULACE             2                 // Recirkulace
#define SERVO_RECIRKULACE_MEASURE     AutoAV1           // Adresa zpetneho hlaseni na prevodniku
#define SERVO_RECIRKULACE_MEASURE_CS  AdcCS0            // Cipselect prevodniku hlaseni

#define SERVO_PODLAHA1             3                    // Podlaha 1
#define SERVO_PODLAHA1_MEASURE     AutoAV16             // Adresa zpetneho hlaseni na prevodniku
#define SERVO_PODLAHA1_MEASURE_CS  AdcCS1               // Cipselect prevodniku hlaseni

#define SERVO_PODLAHA2             4                    // Podlaha 2
#define SERVO_PODLAHA2_MEASURE     AutoAV15             // Adresa zpetneho hlaseni na prevodniku
#define SERVO_PODLAHA2_MEASURE_CS  AdcCS1               // Cipselect prevodniku hlaseni

#define SERVO_PODLAHA3             5                    // Podlaha 3
#define SERVO_PODLAHA3_MEASURE     AutoAV14             // Adresa zpetneho hlaseni na prevodniku
#define SERVO_PODLAHA3_MEASURE_CS  AdcCS1               // Cipselect prevodniku hlaseni

#define SERVO_PODLAHA4             6                    // Podlaha 4
#define SERVO_PODLAHA4_MEASURE     AutoAV13             // Adresa zpetneho hlaseni na prevodniku
#define SERVO_PODLAHA4_MEASURE_CS  AdcCS1               // Cipselect prevodniku hlaseni

#define SERVO_TOPENI             7                      // Topeni - to uz ma ridici i zpetny signal
#define SERVO_TOPENI_CONTROL     AutoAV7                // PotTOP/ST3 adresa rizeni na prevodniku
#define SERVO_TOPENI_CONTROL_CS  AdcCS0                 // cipselect prevodniku rizeni
#define SERVO_TOPENI_MEASURE     AutoAV6                // ST5 adresa zpetneho hlaseni na prevodniku
#define SERVO_TOPENI_MEASURE_CS  AdcCS0                 // cipselect prevodniku hlaseni

#define _SERVO_LAST         8                      // celkovy pocet serv

//-----------------------------------------------------------------------------
// Pripojeni mereni akumulatoru
//-----------------------------------------------------------------------------

// chipselecty prevodniku jsou definovany

// Kalibracni konstanty mereni :

#define AACCU_VOLTAGE_MIN        0                 // merime od 0
#define AACCU_VOLTAGE_MAX     3833                 // maximalni rozsah * 0.01V = 255 LSB

//#define AACCU_CURRENT_MAX    37606                 // maximalni proud * 0.01A = 255 LSB - pro bocnik 60mV @ 100A
#define AACCU_CURRENT_MAX    56409                 // maximalni proud * 0.01A = 255 LSB - pro bocnik 60mV @ 150A
#define AACCU_CURRENT_MIN        0                 // minimalni proud * 0.01A =   0 LSB

#define MAX_PROUD 255                              // Maximalni prud v amperech - aby mne nepretekla promenna


// adresy na prevodnicich :

//#define AACCU_VOLTAGE        AutoAV14               // adresa napeti aku na prevodniku
#define AACCU_VOLTAGE        AutoAV11               // adresa napeti aku na prevodniku
#define AACCU_VOLTAGE_CS     AdcCS1                 // cipselect prevodniku napeti aku

#define AACCU_CURRENT        AutoB1B2               // adresa proudu aku na prevodniku
#define AACCU_CURRENT_CS     AdcCS1                 // cipselect prevodniku proudu aku

//-----------------------------------------------------------------------------
// Pripojeni mereni ventilatoru
//-----------------------------------------------------------------------------

// chipselecty jsou uz definovany

// adresy na prevodnicich :

#define AFAN1_VOLTAGE_CS     AdcCS0                 // cipselect prevodniku ventilatoru 1
#define AFAN1_VOLTAGE        AutoAV5                // adresa napeti ventilatoru 1 na prevodniku
#define AFAN2_VOLTAGE_CS     AdcCS0                 // cipselect prevodniku ventilatoru 2
#define AFAN2_VOLTAGE        AutoAV4                // adresa napeti ventilatoru 2 na prevodniku

// Kalibracni konstanty:
// 24V (plny vykon) dava na vstupu 0xA4=164, 12V (polovicni vykon) dava 0x52=82
// Mez pro plny vykon beru 0x72=114, pro polovicni vykon 0x39=57 (70% namerenych spravnych hodnot)

#define AFAN_MAX       0x72
#define AFAN_MIN       0x39




//-----------------------------------------------------------------------------
// Diesel
//-----------------------------------------------------------------------------

/*// Mereni stavu nafty:
#define ADSL_FUEL_VOLTAGE_CS     AdcCS1                 // cipselect prevodniku ventilatoru 1
#define ADSL_FUEL_VOLTAGE        AutoAV12               // adresa napeti ventilatoru 1 na prevodniku
// Kalibracni konstanty (meri se 0-12V):
// 11V (plny stav) dava na vstupu 0x49, nulovy stav je 1V=0x07
// Realne palivomer meri 0-6V. Uz pri 0.1V chci 1 dilek, pri 5V a vice uz chci plny stav
#define ADSL_FUEL_AV_MAX       0x23     // 0x23 odpovida preklopeni na plny stav pri 5.5V
#define ADSL_FUEL_AV_MIN       0x07     // 0x07 Odpovida preklopeni na 1 dilek pri 1.5V
#define ADSL_FUEL_MIN_AV_RANGE (ADSL_FUEL_AV_MAX-ADSL_FUEL_AV_MIN)
#define ADSL_FUEL_STEPS 11               // Pocet kroku*/

// Mereni teploty dieselu:
#define ADSL_TEMP_VOLTAGE_CS     AdcCS1                 // cipselect prevodniku
#define ADSL_TEMP_VOLTAGE        AutoAV12               // adresa napeti na prevodniku
// Kalibracni konstanty (meri se 0-12V):
// Zobrazuju 75C az 105C
// Pri zapojenem seriovem odporu 500R a napajeni 12V: na 75C dava cidlo 4.55V, pri 105C dava 2.3V
#define ADSL_TEMP_AV_MAX       0x20     // Odpovida preklopeni na min. teplotu (75C) pri 4.55V
#define ADSL_TEMP_AV_MIN       0x0E     // Odpovida preklopeni na max. teplotu (105C) pri 2.35V
#define ADSL_TEMP_MIN_AV_RANGE (ADSL_TEMP_AV_MAX-ADSL_TEMP_AV_MIN)
#define ADSL_TEMP_STEPS 11               // Pocet kroku
#define ADSL_TEMP_AV_NOT_USED  0x02      // Pokud se cidlo nevyuziva, vstup se uzemni a dava 0V
#define ADSL_TEMP_AV_ERROR     0x50      // Pri poruse (odpojene cidlo) je na vstupu plne napeti (0x50 odpovida preklopeni na 11.90V)





//-----------------------------------------------------------------------------
// Pripojeni mereni prepinace Fresh air
//-----------------------------------------------------------------------------

// Kalibracni konstanty mereni :

// Namerene napeti prepinace v jednotlivych polohach:
// AUTO: 0x00
// 20%:  0x32
// 40%:  0x63
// 60%:  0x97
// 80%:  0xC8
// 100%: 0xFA
// Mezery mezi jednotlivymi polohohami jsou 0x32
// Meze jednotlivych poloh:
#define AV_FRESH_AIR_20   0x19
#define AV_FRESH_AIR_40   0x4B
#define AV_FRESH_AIR_60   0x7D
#define AV_FRESH_AIR_80   0xAF
#define AV_FRESH_AIR_100  0xE1

// adresy na prevodnicich :
#define AV_FRESH_AIR      AutoAV8       // adresa napeti aku na prevodniku
#define AV_FRESH_AIR_CS   AdcCS0        // cipselect prevodniku napeti aku
#define AREG_FRESH_AIR_DAC  AutoAVY6      // adresa D/A prevodniku cerstveho vzduchu

#define FRESH_AIR_TEMPERATURE_RANGE (TempMk1C(2)) // +-1 stupen = 2 stupne

#define FRESH_AIR_MIN_DAC 0x0B          // Vystup DA prevodniku, aby dal 2V (zcela zavrene servo)
#define FRESH_AIR_MAX_DAC 0x3E          // Vystup DA prevodniku, aby dal 10V (zcela otevrene servo)

#define MIN_FRESH_AIR 20                // Minimalni hodnota cerstveho vzduchu v procentech
#define MAX_FRESH_AIR 100               // Miximalni hodnota cerstveho vzduchu v procentech
#define FRESH_AIR_RANGE (MAX_FRESH_AIR-MIN_FRESH_AIR)   // Rozsah cerstveho vzduchu v procentech

//-----------------------------------------------------------------------------
// Digitalni vstupy/vystupy
//-----------------------------------------------------------------------------

// digitalni vstupy :

#define AdioCS            AutoCSVST                   // chipselect vstupniho latche
#define AdioCS_DV8        AutoCSCIDLA                 // chipselect vstupniho latche - DV8 je pripojen spolu s cidly
#define AdioBUS           AutoDATA                    // datova sbernice

#define AdioHeatingManual !AutoDV4                    // topeni rucne
#define AdioAuto          !AutoDV2                    // automat
#define AdioCoolerManual  !AutoDV3                    // chlazeni rucne spina proti zemi
#define AdioVentilation   !AutoDV1                    // ventilace
#define AdioFanFailure    !AutoDV5                    // porucha ventilatoru
#define AdioDieselStart   AutoDV6                     // nastartovany diesel
#define AdioDieselFailure !AutoDV7                    // Porucha dieselu (prehrati nebo neni olej)
#define AdioBatteryLow    AutoLOWBAT                  // nizke napajeni
#define AdioAlternatorOn  AutoDV5                     // Dobiji se akumulator => neco se toci (diesel nebo elektromotor)

// DV8 s cidly
#define AdioCoolerOn      AutoTDV8                    // Beh chlazeni - je na stejnem portu jako porucha topeni
#define AdioHeatingFailure !AutoTDV8                  // porucha topeni - je na stejnem portu jako chod chlazeni



// digitalni vystupy :

#define AdioCoolerSwitch       AutoDVY1               // vystup spinani chlazeni
#define AdioPumpSwitch         AutoDVY2               // 29.5.2003: Vystup spinani cerpadla topeni
#define AdioAlarmSwitch        AutoDVY3               // Vystup spinani majaku u navesu pri alarmu
#define AdioHeatingPowerSwitch AutoDVY3               // Vystup pro ovladani vykonu topeni u nakladaku - je stejny jako majak

//-----------------------------------------------------------------------------
// Povoleni prekladu Auto.h
//-----------------------------------------------------------------------------

#define AUTO_TEMPERATURE    1             // preklada se teplomer
#define AUTO_SERVO          1             // preklada se servo
#define AUTO_ACCU           1             // preklada se aku
#define AUTO_FAN            1             // preklada se ventilator
#define AUTO_DIO            1             // prekladaji se digitalni vstupy/vystupy
#define AUTO_LOG            1             // preklada se logger
//#define AUTO_PRT            1             // preklada se protokolovani
#define AUTO_REG            1             // preklada se regulator
#define AUTO_DIESEL         1             // preklada se diesel
#define AUTO_CONFIG         1             // preklada se konfigurace
#define AUTO_FRESH_AIR      1             // preklada se fresh air


//-----------------------------------------------------------------------------
// Parametry FIFO - datovy logger
//-----------------------------------------------------------------------------

struct SAutoLog;                       // dopredna deklarace

typedef struct SAutoLog TFifoData;     // Typ ukladane struktury (max 255 bytu)
#define FifoData        AutoLog        // Globalni buffer ukladanych dat

#define FIFO_START            AUTO_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define FIFO_CAPACITY         AUTO_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define FIFO_MARKER_EMPTY     AUTO_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define FIFO_MARKER_FULL      AUTO_FIFO_MARKER_FULL    // znacka konce pri prepisovani

#define FIFO_INVALID_ADDRESS 0xFFFF                    // neplatna adresa do FIFO - MUSI korespondovat s XFIFO_INVALID_ADDRESS ve fifo.tpl!!!!!

// Podminena kompilace :
#define FIFO_FAST            1   // Zapamatovani pozice markeru
#define FIFO_VERIFY          1   // Verifikace po zapisu dat
#define FIFO_READ            1   // Cteni obsahu po polozkach
//#define FIFO_FIND_FIRST      1   // Nalezeni adresy prvniho zaznamu
//#define FIFO_FIND_LAST       1   // Nalezeni adresy posledniho zaznamu

/*//-----------------------------------------------------------------------------
// Parametry FIFO2 - protokolovani
//-----------------------------------------------------------------------------

struct SAutoUdalost;                   // dopredna deklarace

typedef struct SAutoUdalost TFifo2Data;        // Typ ukladane struktury (max 255 bytu)
#define Fifo2Data           AutoUdalost        // Globalni buffer ukladanych dat

#define FIFO2_START            APRT_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define FIFO2_CAPACITY         APRT_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define FIFO2_MARKER_EMPTY     APRT_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define FIFO2_MARKER_FULL      APRT_FIFO_MARKER_FULL    // znacka konce pri prepisovani

// Podminena kompilace :
#define FIFO2_FAST            1   // Zapamatovani pozice markeru
#define FIFO2_VERIFY          1   // Verifikace po zapisu dat
//#define FIFO_READ            1   // Cteni obsahu po polozkach


//-----------------------------------------------------------------------------
// Parametry IFIFO - protokolovani
//-----------------------------------------------------------------------------

struct SAutoUdalost;                   // dopredna deklarace

typedef struct SAutoUdalost TIfifoData;        // Typ ukladane struktury (max 255 bytu)
#define IfifoData           AutoUdalost        // Globalni buffer ukladanych dat

#define IFIFO_START            APRT_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define IFIFO_CAPACITY         APRT_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define IFIFO_MARKER_EMPTY     APRT_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define IFIFO_MARKER_FULL      APRT_FIFO_MARKER_FULL    // znacka konce pri prepisovani

// Podminena kompilace :
#define IFIFO_FAST            1   // Zapamatovani pozice markeru
#define IFIFO_VERIFY          1   // Verifikace po zapisu dat
//#define IFIFO_READ            1   // Cteni obsahu po polozkach*/

//-----------------------------------------------------------------------------
// Parametry kruhoveho pole
//-----------------------------------------------------------------------------

// Podminena komplilace :
#define FAR_RESET            1         // Preklada se funkce pro reset

// znacka ve FIFO :
// ukladana hodnota se ji nesmi rovnat !
// pro byte  0xMM
// pro word  0xMM00
// pro dword 0xMM000000

#define FAR_MARKER     0x80           // hodnota MM

//-----------------------------------------------------------------------------
// Parametry regulatoru
//-----------------------------------------------------------------------------

#define AREG_HEATING_DAC AutoAVY1   // adresa D/A prevodniku topeni




//-----------------------------------------------------------------------------
// Pripojeni displeje
//-----------------------------------------------------------------------------

#define DisplayCS   AutoCSD         // /CS vyber cipu
#define DisplayA0   AutoSO          // prikaz/data nekdy oznaceny jako CD
#define DisplayRD   AutoSI          // cteni /RD
#define DisplayWR   AutoCLK         // zapis /WR
#define DisplayRES  AutoRES         // reset /RES

#define DisplayData AutoDATA          // datova sbernice

// makra nezavisla na zapojeni pinu (volne k pouziti) :

#define DisplayEnable()  DisplayWR = 1; DisplayRD = 1; DisplayCS = 0
#define DisplayDisable() DisplayCS = 1

#define DisplayAddressCommand() (DisplayA0 = 1)
#define DisplayAddressData()    (DisplayA0 = 0)

#define DisplayReset() (DisplayRES = 0)
#define DisplayRun()   (DisplayRES = 1)

// void   DisplayWriteData( byte_t d);
// zapis dat do displeje

#define DisplayWriteData( d) {DisplayWR = 0; DisplayData = d; DisplayWR = 1;}

// byte DisplayReadData( void);
// cteni dat z displeje (v modulu Hardware.c)

// Pracovni mod displeje :

#define DISPLAY_MODE_GRAPHICS2 1            // graficky mod displeje - 2 roviny

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define KbdK0    AutoK0         // Minus
#define KbdK1    AutoK1         // Enter
#define KbdK2    AutoK2         // Plus

#define KbdKESC1 AutoK1        // Esc zapojen mezi
#define KbdKESC2 AutoK2

// definice klaves :
typedef enum {
   // systemove klavesy :
   K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
   _K_FIRSTUSER,

   // klavesy, v zavislosti na konstrukci klavesnice :
   K_RIGHT = _K_FIRSTUSER,         // sipka doprava
   K_UP,                           // sipka nahoru
   K_ENTER,                        // Enter
   K_ESC,                          // Esc

   // systemove klavesy :
   K_REPEAT       = 0x80,   // opakovani klavesy OR ke klavese
   K_RELEASED     = 0xFE,   // pusteni klavesy (jednotlive i autorepeat)
   K_IDLE         = 0xFF    // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (300/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch



//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

#define RS232Power      AutoRSPOWER   // Port napajeni budice

#define COM_BAUD        9600          // Rychlost linky v baudech

#define COM_PARITY_EVEN    1          // Suda parita
//#define COM_PARITY_ODD   1          // Licha parita

//#define COM_TIMER_1      1          // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2        1          // K casovani linky se vyuzije casovac 2

#define COM_RX_TIMEOUT   10000        // Meziznakovy timeout

//-----------------------------------------------------------------------------
// Parametry radiove komunikace
//-----------------------------------------------------------------------------

#include "..\Auto\PktDef.h"           // popis protokolu

#define PACKET_BAUD 2400
//#define PACKET_PARITY
#define TXPACKET_TIMER_2              // vysilac  - rizeni casovacem 2
#define RXPACKET_TIMER_2              // terminal - rizeni casovacem 2

//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __Auto_H__
//  #include "Auto.h"
#endif
//-----------------------------------------------------------------------------

#endif
