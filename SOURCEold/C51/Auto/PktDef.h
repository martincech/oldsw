//*****************************************************************************
//
//    PktDef.h  - Packet data definitions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __PktDef_H__
   #define __PktDef_H__

#define DATA_SIZE 7          // pocet datovych bytu paketu
#define STX_CHAR  0x55       // naraznikovy znak
#define NULL_CHAR 0xAA       // vyplnovy znak

//-----------------------------------------------------------------------------
// Typy paketu
//-----------------------------------------------------------------------------

typedef enum {
   PKT_NULL = 0xA0,          // prazdny paket
   PKT_TIME,                 // cas
   PKT_TEMPERATURE,          // teploty
   PKT_SERVOPOT,                // zpetna hlaseni serv, potenciometru a aku
//   PKT_POTENTIOMETER,        // potenciometry
   PKT_STATUS,               // stavy digitalnich velicin
   PKT_CONFIG,               // konfiguracni data
   _PKT_LAST,                // posledni kod
   PKT_STOP                  // pomocny kod, stop vysilace
} TPacketType;

//-----------------------------------------------------------------------------
// Data paketu
//-----------------------------------------------------------------------------

// Casovy paket, hodnoty v BCD :
typedef struct {
   byte Hour;
   byte Min;
   byte Sec;
   byte Day;
   byte Month;
   word Year;
} TTimeData;

// Paket teplot, hodnoty v 1C :
typedef struct {
   char  TeplotaVenkovni;
   char  TeplotaKanal;
   char  TeplotaPredni;
   char  TeplotaStredni;
   char  TeplotaZadni;
   char  TeplotaVyfukovana;
} TTemperatureData;

// Paket zpetych hlaseni serv, potenciometru a aku:
typedef struct {
   byte ServoTopeni;
   byte Topeni;         // akcni velicina na topeni
   byte PotTopeni;
   byte AkuNapeti;      // napeti akumulatoru
   byte AkuProud;       // proud akumulatoru
} TServoPotData;

// Paket signalu 1 :
typedef struct {
   // stav serv :
   byte StavNapor1   : 3,
        StavNapor2   : 3,
        Dummy1  : 2;

   byte StavRecirkulace : 3,
        StavPodlaha1    : 3,
        Dummy2  : 2;

   byte StavPodlaha2   : 3,
        StavPodlaha3   : 3,
        Dummy3  : 2;

   byte StavPodlaha4   : 3,
        StavTopeni   : 3,
        Dummy4  : 2;

   byte PrepinacRezimu           : 3,
        PrepinacFreshAir         : 3,
        NastartovanyDiesel       : 1,
        NastartovanyElektromotor : 1;

   byte DobijeniAku              : 1,
        SepnuteChlazeni          : 1,
        VymenaOleje              : 1,
        VymenaFiltru             : 1,
        StavNafty                : 4;

   byte PolovicniVykonVentilatoru : 1,
        PoruchaVentilatoru        : 1,
        PoruchaTopeni             : 1,
        PoruchaChlazeni           : 1,
        PoruchaDobijeni           : 1,
        PoruchaDieselu            : 1,
        RegSensorError            : 1,
        ZapnutoChlazeni           : 1;
} TStatusData;

// Konfiguracni data
typedef struct {
   // Zbytek stavovych dat:
   byte AutomatickyRezim    : 2,
        Naves               : 1,
        ZobrazitDiagnostiku : 1,
        PoruchaTeplotaPredni : 1,
        PoruchaTeplotaStredni : 1,
        PoruchaTeplotaZadni : 1,
        Dummy               : 1;               // Volny 1 bit

   byte CerstvyVzduch;

   // Konfigurace: (volba Naves je jiz drive u stavovych dat)
   byte MaxOdchylkaSkrin;
   char TeplotaCilova;

   // 15.6.2004: Lozeni
   byte Lozeni                   : 3,   // TLocalizationStatus
        ZobrazovatTeplotuDieselu : 1,   // 12.10.2004
        Dummy3                   : 4;

} TConfigData;

// Souhrnny union :

typedef union {
   byte             Byte[DATA_SIZE];    // data jako byty
   TTimeData        Time;
   TTemperatureData Temp;
   TServoPotData    ServoPot;
   TStatusData      Status;
   TConfigData      Config;
} TPacketData;

//-----------------------------------------------------------------------------
// Definice paketu
//-----------------------------------------------------------------------------

typedef struct {
   byte        Stx;                 // naraznik - start
   byte        Id;                  // Identifikacni cislo pristroje (POZOR, nesmi byt vetsi nez STX_CHAR)
   byte        Address;             // cislo paketu (POZOR, nesmi byt STX_CHAR)
   TPacketData Data;                // data
   byte        Crc;                 // zabezpeceni
} TPacket;

#endif

