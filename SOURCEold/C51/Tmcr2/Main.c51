//*****************************************************************************
//
//    Main.c51       - TMC testovani
//    Version 2.00   (c) VymOs
//
//*****************************************************************************

/*

Verze 2.00.0:
   - 28.2.2012: Synchronizace s TMC 2.00.0 pro naves Mach


*/



#include "Hardware.h"
#include "Tmcr.h"              // Prikazy do TMC
#include "..\inc\System.h"     // "operacni system"
#include "..\inc\PcaTmc.h"     // PCA projektu TMC
#include "..\inc\Ads7846.h"    // touch screen
#include "..\Tmc2\Alarm.h"      // Indikace poruch
#include "..\Tmc2\Menu.h"       // Zobrazeni
#include "..\Tmc2\Beep.h"       // Repro
#include "..\Tmc2\Ras.h"        // Terminalovy server


// odpocet necinnosti :
#define USER_EVENT_TIMEOUT 250    // uzivatel neobsluhuje dele nez ... s

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// start odpocitavani 1 s :
#define SetTimer1s()  counter1s  = 1000 / TIMER0_PERIOD
#define SetTimer1s3() counter1s3 = 1300 / TIMER0_PERIOD

// Lokalni promenne :
volatile byte counter1s   = 0;        // odpocitavani 1 s
volatile byte counter1s3  = 0;        // odpocitavani 1.3 s
volatile bit  timer1s     = 0;        // priznak odpocitani 1 s
volatile bit  timer1s3    = 0;        // priznak odpocitani 1.3 s
char counter_blink       = BLINK_ON;  // pocitadlo blikani znamenko urcuje smer citani
volatile bit timer_blink = 0;         // priznak blikani
volatile bit blink_on    = 1;         // blikani - roznout, zhasnout

static byte BrightnessCounter = 0;    // Pocitadlo v preruseni
extern byte Brightness;               // Nastaveny podsvit MIN_PODSVIT az MAX_PODSVIT

byte _Timeout = 0;                    // pocitani necinnosti - kvuli tisku protokolu jsem to musel vytahnout ven z fce SysYield()

TConfig __xdata__ Config;             // buffer konfigurace v externi RAM, Tmc.h

// ---------------------------------------------------------------------------------------------
// Main
// ---------------------------------------------------------------------------------------------

#define TestujPinRychle(Pin)    \
while (EA) {                    \
  Pin = 0;                      \
  WatchDog();                   \
  Pin = 1;                      \
}

#define TestujPinPomalu(Pin)    \
while (EA) {                    \
  Pin = 0;                      \
  SysDelay(1);                  \
  Pin = 1;                      \
  SysDelay(1);                  \
}

void main() {
// Systemova inicializace : ---------------------------------------------------
   TimingSetup();              // Nastaveni X2 modu
   EnableXRAM();               // Povoleni pristupu k vnitrni XRAM
   WDTPRG = WDTPRG_2090;       // start Watch Dogu
   WatchDog();

   DisplayPodsvit( 1);

   EnableInts();               // Povoleni preruseni
   Timer0Run( TIMER0_PERIOD);  // spust casovac 0
   Timer1Run( TIMER1_PERIOD);  // spust casovac 1

// Inicializace modulu : ------------------------------------------------------

   TmcrInit();  // Nastavi vychozi data a komunikaci s TMC
   MenuInit();
   TouchInit();
   PcaInit();
   AlarmInit();
//   RasInit();

   BeepStartup();   // Musi byt az po PcaInit()
   SysDelay(500);

   SetTimer1s();               // zahajeni odpoctu 1 s
   SetTimer1s3();              // zahajeni odpoctu 1.3 s

//   TestujPinRychle(XmemCS);
//   TestujPinPomalu(XmemCS);

   // Login ridice
#ifndef __TMCREMOTE__
   if (Config.Login) {
     MenuLogin();               // Nalogovani ridice
     DiagnosticCounter = 0;     // Aby bylo mozne prejit do diagnostickeho rezimu i pokud pouziva logovani
   }
#endif // __TMCREMOTE__

   while (1) {
      switch(SysWaitEvent()){
         case K_TOUCH :
            MenuTouch();
            break;

         case K_BLINK_ON :
            // Blikani 2. rovinou
            MenuBlink(YES);
            break;

         case K_BLINK_OFF :
            // Blikani 2. rovinou
            MenuBlink(NO);
            break;

         case K_REDRAW :
            MenuRedraw();
            break;

         case K_TIMEOUT :
            break;

         default :
            break;
      }//switch
   }//while
} // main

//-----------------------------------------------------------------------------
// Prepnuti kontextu na "operacni system" viz System.h
//-----------------------------------------------------------------------------

// Kontrola touchpanelu, vola se casteji v SysYield()
#define CheckTouch()    \
  if (TouchGet()) {     \
    _Timeout = 0;       \
    return( K_TOUCH);   \
  }

byte SysYield()
// Prepnuti kontextu na operacni system. Vraci udalost
{
   WatchDog();
   CheckTouch();
   // osetreni uzivatelskeho rezimu GSM :
   // Terminal
//   RasExecute();                // Volat co nejcasteji, aby nebylo zpozdeni ve zpracovani
   // casovac periodicke cinnosti :
   if( timer1s){
      // uplynula 1s
      timer1s = 0;              // zrus priznak
      SetTimer1s();             // novy cyklus
      _Timeout++;               // pocitame necinnost
      if (DiagnosticCounter < MAX_DIAGNOSTIC_COUNTER) {
        DiagnosticCounter++;    // Volba diagnostiky je jeste platna
      }
      // Cinnost:
      AlarmExecute();           // Obslouzim piskani alarmu na konci kazdeho mericiho cyklu
      CheckTouch();

      Redraw |= REDRAW_PERIODIC;  // Prekreslim nove nactene hodnoty
      return( K_REDRAW);
   }
   if( timer1s3){
      // uplynula 1.3 s
      timer1s3 = 0;             // zrus priznak
      SetTimer1s3();            // novy cyklus
      // Cinnost:
      TmcrReadPeriodic();       // Nactu data z TMC
   }
   // casovac blikani :
   if( timer_blink){
      // uplynul cas blikani
      timer_blink = 0;         // zrus priznak
      if( blink_on){
         return( K_BLINK_ON);
      } else {
         return( K_BLINK_OFF);
      }
   }
   // testuj necinnost :
   if( _Timeout > USER_EVENT_TIMEOUT){
      _Timeout = 0;             // zahajime dalsi cekani
      return( K_TIMEOUT);
   }
   CheckTouch();
   return( K_IDLE);             // zadna udalost
} // SysYield

//-----------------------------------------------------------------------------
// Cekani na udalost viz System.h
//-----------------------------------------------------------------------------

byte SysWaitEvent( void)
// Cekani na udalost
{
byte key;

   while( 1){
      key = SysYield();
      if( key != K_IDLE){
         return( key);      // neprazdna udalost
      }
   }
} // SysWaitEvent

//-----------------------------------------------------------------------------
// Nastaveni timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysSetTimeout( void)
// Nastavi timeout klavesnice
{
   _Timeout = USER_EVENT_TIMEOUT + 1;
} // SysSetTimeout

//-----------------------------------------------------------------------------
// Prerusovaci rutina casovace 0
//-----------------------------------------------------------------------------

static void Timer0Handler( void) interrupt INT_TIMER0
// Prerusovaci rutina casovace 0
{
   SetTimer0( TIMER0_PERIOD);                // s touto periodou
   // vykonne funkce :
   CheckTrigger( counter1s,  timer1s  = 1);  // casovac 1 s
   CheckTrigger( counter1s3, timer1s3 = 1);  // casovac 1.3 s
   PcaTrigger();                             // handler asynchronniho pipani
   TouchTrigger();                           // handler touch panelu
   // casovac blikani :
   counter_blink--;
   if( counter_blink == 0){
      // docitali jsme, nastav na novou hodnotu
      timer_blink = 1;                       // nastav priznak
      if( blink_on){
         counter_blink = -256 + BLINK_OFF;   // bylo roznuto,  perioda pro zhasni
      } else {
         counter_blink = BLINK_ON;           // bylo zhasnuto, perioda pro rozsvit
      }
      blink_on = !blink_on;                  // novy smer citani
   }
} // Timer0Handler

//-----------------------------------------------------------------------------
// Prerusovaci rutina casovace 1
//-----------------------------------------------------------------------------

static void Timer1Handler( void) interrupt INT_TIMER1
// Prerusovaci rutina casovace 0
{
   SetTimer1(TIMER1_PERIOD);                // s touto periodou
   // vykonne funkce :
   // Podsvit
   BrightnessCounter++;
   if (BrightnessCounter <= Brightness) {
     DisplayPodsvit(YES);
   } else {
     DisplayPodsvit(NO);
   }
   if (BrightnessCounter > BRIGHTNESS_MAX) {
     BrightnessCounter = 0;
   }
} // Timer1Handler

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

void SysDelay(unsigned int n)
{
// Uprava pro krystal 18.432MHz
  unsigned char m;                                                                     // m = R5, n = R6+R7
                                                                            // T = 1.085 mikrosec.
  while(n > 0) {              // DELAY: SETB C/MOV A,R7/SUBB A,#0/MOV A,R6/SUBB A,#0/JC KONEC *7T*
    WDTRST=0x1E;
    WDTRST=0xE1;
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    // Pro X2 mod musim 2x tolik!!
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    --n;                          // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*
  }                                                                             // SJMP DELAY *2T*
} // SysDelay
