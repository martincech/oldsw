//*****************************************************************************
//
//    Signal.h - Mereni sily signalu
//    Version 1.0
//
//*****************************************************************************

#include "Signal.h"
#include "Hermes1r.h"           // ConnectionOk
#include "Ain.h"                // Analogove vstupy
#include "Cal.h"                // Kalibrace
#include "Accu.h"               // Stav akumulatoru

#include "..\Hermes1\Hermes1.h" // Kalibrace

TSignal __xdata__ Signal;

//-----------------------------------------------------------------------------
// Lokalni promenne
//-----------------------------------------------------------------------------

static byte __xdata__ ValidId;  // YES/NO prave prijimany paket ma platne identifikacni cislo

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void SignalInit(void) {
  Signal.Indication          = 0;
  Signal.ZeroSignalIgnored   = NO;
  Signal.IndicatedConversion = 0;
}

//-----------------------------------------------------------------------------
// Nacteni sily signalu
//-----------------------------------------------------------------------------

void SignalExecute(void) {
  // Obsluha mereni sily signalu. Volat 1x za sekundu.
  if (!ConnectionOk) {
    Signal.Indication          = 0;     // Signal neni treba merit, je nulovy
    Signal.ZeroSignalIgnored   = NO;
    Signal.IndicatedConversion = 0;
    return;
  }
}

//-----------------------------------------------------------------------------
// Kalibrace sily signalu
//-----------------------------------------------------------------------------

byte SignalCalibrate(void) {
  // Nacte kalibraci sily signalu
  return AinCalibrate(AIN_SIGNAL);
}

//-----------------------------------------------------------------------------
// Oznameni prijeti platneho paketu
//-----------------------------------------------------------------------------

void SignalPacketValid(void) {
  // Oznameni prijeti platneho paketu
  byte NewIndication;

  NewIndication = CalCalculate(Signal.Conversion, Calibration.SignalMin, Calibration.SignalMax, 0, SIGNAL_INDICATION_MAX);
  if (NewIndication == 0 && Signal.Indication > 0 && !Signal.ZeroSignalIgnored) {
    // Jde o prvni nulovy signal => ten vyfiltruju, casto to na 1 krok vypadava, nevim proc
    // Pokud je nulovy signal podruhe, uz ho zverejnim
    Signal.ZeroSignalIgnored = YES;                     // Nastavim flag, ze jsem nulovy prevod ignoroval
    return;
  }
  Signal.ZeroSignalIgnored   = NO;                      // Shodim flag
  Signal.Indication          = NewIndication;           // Zverejnim
  Signal.IndicatedConversion = Signal.Conversion;       // Zverejnim i platny prevod (pouze pro diagnostiku)
}

//-----------------------------------------------------------------------------
// Callback pro mereni signalu - pozor, vola se z preruseni seriove linky
//-----------------------------------------------------------------------------

void PacketIdReceived(byte Id) {
  // Prave se po seriove lince nacetlo identifikacni cislo <Id> - jsem stale v preruseni
  if (Id != Config.IdentificationNumber) {
    // Identifikacni cislo neodpovida
    ValidId = NO;
    return;
  }
  // Identifikacni cislo je v poradku
  ValidId = YES;
}

void PacketAddressReceived(byte Address) {
  // Prave se po seriove lince nacetla adresa <Address> - jsem stale v preruseni
  if (!ValidId) {
    // Paket ma neplatne identifikacni cislo, nepatri tomuto prijimaci nebo jde o sum
    return;
  }
  if (Address != PACKET_ADDRESS) {
    // Adresa paketu neodpovida - jde o sum
    return;
  }
  if (Accu.Busy) {
    // Prave probiha nacitani Ain akumulatoru a toto nacitani jsem prerusil - nemuzu ted zacit nove nacitani Ain signalu. Nactu az priste.
    return;
  }

  // Paket se zatim zda byt platny - zmerim prevod signalu a uschovam. Zverejnim ho az po prijmu kompletniho paketu a po kontrole CRC
  Signal.Conversion = AinRead(AIN_SIGNAL);      // Zmerim a ulozim si prevod
}
