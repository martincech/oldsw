//*****************************************************************************
//
//    Cal.h    - Calibration load/save
//    Version 1.0  (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Cal_H__
   #define __Cal_H__

#include "Hardware.h"     // pro podminenou kompilaci

void CalLoad();
// Nacte kalibraci z EEPROM
// Nekontroluje zadne checksumy a nenastavuje default hodnoty

TYesNo CalSave();
// Ulozi celou kalibraci do EEPROM
// Vraci NO, nepovedl-li se zapis

byte CalCalculate(byte Conversion, byte CalMin, byte CalMax, byte Min, byte Max);
  // Z <Conversion> vypocte hodnotu podle kalibrace, prevodni funkce musi byt stoupajici.
  // <CalMin> a <CalMax> jsou prevody maxima a minima, <Min> a <Max> je minimalni a maximalni hodnota.

//byte CalCalculateInt(byte Conversion, byte CalMin, byte CalMax, byte Min, byte Max);
  // Z <Conversion> vypocte hodnotu podle kalibrace, prevodni funkce musi byt stoupajici.
  // <CalMin> a <CalMax> jsou prevody maxima a minima, <Min> a <Max> je minimalni a maximalni hodnota.

#endif
