//*****************************************************************************
//
//    Hermes1r.c - prijem dat z vysilace
//    Version 1.0
//
//*****************************************************************************

#ifndef __Hermes1r_H__
   #define __Hermes1r_H__

#include "Hardware.h"     // pro podminenou kompilaci


extern word __xdata__ PacketCounterOk;         // pocet dobrych paketu
extern word __xdata__ PacketCounterErr;        // pocet vadnych paketu

extern byte __xdata__ ConnectionOk;     // YES/NO - spojeni bezi
extern byte __xdata__ ConnectionUp;     // YES/NO - nabeh spojeni po vypadku
extern byte __xdata__ ConnectionDown;   // YES/NO - prave doslo ke ztrate spojeni

extern byte __xdata__ WaitingForFirstPacket; // YES/NO stale se ceka na prijem prvniho paketu (tj. zadny jeste prijat nebyl)

void Hermes1rInit();
  // Nastavi vychozi data

void Hermes1rExecute();
  // Periodicka cinnost

void Hermes1rTimer1s();
// Tik casovace, volat po 1s

#endif
