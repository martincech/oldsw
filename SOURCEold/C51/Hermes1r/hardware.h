//*****************************************************************************
//
//    Hardware.h  - Tr1r hardware descriptions
//    Version 1.0  (c) P.Veit
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof


#define __REMOTE__    1         // Jde o prijimac - prekladat prijimac

#define AccuOk()      YES
#define PowerFail()   NO

//-----------------------------------------------------------------------------
// Prirazeni portu
//-----------------------------------------------------------------------------

// port P0 :
sbit HermesLED2     = P0^0;
sbit HermesLED1     = P0^1;
sbit HermesRXSEL    = P0^2;

#define EnableRx()  HermesRXSEL = 0
#define DisableRx() HermesRXSEL = 1

// port P1 :
sbit HermesK0       = P1^0;
sbit HermesK1       = P1^1;
sbit HermesK2       = P1^2;
sbit HermesK3       = P1^3;
sbit HermesDAD      = P1^4;
sbit HermesCLKAD    = P1^5;
sbit HermesCSAD     = P1^6;


// datova sbernice :
#define HermesDATA  P2

sbit HermesD0   = HermesDATA^0;
sbit HermesD1   = HermesDATA^1;
sbit HermesD2   = HermesDATA^2;
sbit HermesD3   = HermesDATA^3;
sbit HermesD4   = HermesDATA^4;
sbit HermesD5   = HermesDATA^5;
sbit HermesD6   = HermesDATA^6;
sbit HermesD7   = HermesDATA^7;

// Port P3 :
sbit HermesRXD      = P3^0;
sbit HermesTXD      = P3^1;
sbit HermesPISK     = P3^2;
sbit HermesCS       = P3^3;
sbit HermesRES      = P3^4;
sbit HermesDC       = P3^5;
sbit HermesRW       = P3^6;
sbit HermesE        = P3^7;

#define Led1On()    HermesLED1 = 0
#define Led1Off()   HermesLED1 = 1
#define Led2On()    HermesLED2 = 0
#define Led2Off()   HermesLED2 = 1


//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 4.9125 MHz
#define FXTAL 4912500L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
// Pozor, pokud se pouziva dynamicke rizeni CPU_X2, vsechny ostatni zarizeni musi byt v X1 modu (T0_X2, T1_X2 atd. musi byt nastaveny na 1). Pokud
// jsou ostatni zarizeni nastavene v X2 modu, po prepnuti procesoru do X1 modu se prepnou soucasne i zarizeni. Napr. SPI je stale v X2 a prepina se do X1.
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2 (zatim neni implementovano, bezi stale v X2 na max. rychlost)

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1             // zdvojena baudova rychlost

// Casovac 0 :
#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()     1           // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaOut HermesPISK              // vystup generatoru
#define PCA_IDLE_STATE 1               // Klidova uroven pinu PcaOut

// Podminena kompilace :

#define PCA_ASYNC_BEEP 1               // asynchronni pipani

//-----------------------------------------------------------------------------
// Radic displeje SSD1303
//-----------------------------------------------------------------------------

// Hardwarove pripojeni displeje
#define DisplayDATA       HermesDATA
#define DisplayD0         HermesD0
#define DisplayD1         HermesD1
#define DisplayD2         HermesD2
#define DisplayD3         HermesD3
#define DisplayD4         HermesD4
#define DisplayD5         HermesD5
#define DisplayD6         HermesD6
#define DisplayD7         HermesD7
#define DisplayRW         HermesRW
#define DisplayDC         HermesDC
#define DisplayE          HermesE
#define DisplayCS         HermesCS
#define DisplayRES        HermesRES

// Cekaci rutiny:
#define DisplayDelay60ns()      _nop_()
#define DisplayDelay140ns()     _nop_()
#define DisplayDelay100us()     SysDelay(1)

// Podminena kompilace:
//#define SSD1303_DC_CONVERTER    1       // Vyuziti interniho menice napeti 9V -> 3.3V
#define SSD1303_BUS             1       // Datova sbernice 1:1 na portu
//#define SSD1303_READ_STATUS     1       // Preklada se cteni statusu

//-----------------------------------------------------------------------------
// Graficky displej Univision UG-3264GMCAT01 (132x64)
//-----------------------------------------------------------------------------

// Rozmery displeje:
#define DISPLAY_WIDTH           132     // Sirka v pixelech
#define DISPLAY_HEIGHT          64      // Vyska v pixelech

// Podminena kompilace:
//#define DISPLAY_ROTATED         1       // Orientace rotace displeje (rotovany = ksanda nad displejem)
#define DISPLAY_CONTRAST        1       // Nastaveni kontrastu displeje
//#define DISPLAY_NORMAL_DISPLAY  1       // Prepinani na normalni (pozitivni) zobrazeni
//#define DISPLAY_INVERSE_DISPLAY 1       // Prepinani na inverzni zobrazeni

// Cary, obdelniky atd.
#define DISPLAY_SET_AREA_FAST   1
#define DISPLAY_SET_AREA        1
//#define DISPLAY_HOR_LINE        1
//#define DISPLAY_FAST_VERT_LINE  1
//#define DISPLAY_VERT_LINE       1
//#define DISPLAY_PIXEL           1
#define DISPLAY_CHAR            1
#define DISPLAY_SYMBOL          1

//-----------------------------------------------------------------------------
// Fonty
//-----------------------------------------------------------------------------

// Podminena kompilace:
//#define DISPLAY_TAHOMA10                1
#define DISPLAY_TAHOMA10_BOLD           1
//#define DISPLAY_TAHOMA8                 1
#define DISPLAY_LUCIDA6                 1
//#define DISPLAY_SMALL8_COND_NUMERIC     1
#define DISPLAY_SMALL8_NUMERIC          1
//#define DISPLAY_SMALL8_WIDE_NUMERIC     1
#define DISPLAY_TAHOMA10_BOLD_NUMERIC   1
//#define DISPLAY_MYRIAD32                1
#define DISPLAY_MYRIAD40                1
//#define DISPLAY_CHAR_WIDTH              1
#define DISPLAY_TEXT_WIDTH              1

//-----------------------------------------------------------------------------
// Zobrazeni
//-----------------------------------------------------------------------------

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// Podminena kompilace:

//#define DISPLAY_LINE                    1

// Vety
#define DISPLAY_STRING                  1
//#define DISPLAY_STRING_RIGHT            1
#define DISPLAY_STRING_CENTER           1

// Zobrazeni cisla
#define DISPLAY_EXPAND_NUMBER           1
#define DISPLAY_NUMBER                  1
//#define DISPLAY_CHAR_FORMATED           1

// Editace
#define DISPLAY_EDIT                    1
#define DISPLAY_EDIT_CHAR_WIDTH         8       // Sirka 1 cislice (podle pouziteho fontu)
#define DISPLAY_EDIT_DOT_WIDTH          2       // Sirka tecky a dvojtecky pri editaci cisla, data a casu (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_BEFORE_DOT      1       // Sirka mezery pred desetinnou carkou (podle pouziteho fontu)
#define DISPLAY_EDIT_CHAR_SPACE_AFTER_DOT       2       // Sirka mezery za desetinnou carkou (podle pouziteho fontu)
//#define DISPLAY_EDIT_TEXT               1

//#define DISPLAY_BARGRAF                 1

// Dialog
#define DISPLAY_DIALOG                  1
//#define DIALOG_OTOCENE_KLAVESY          1       // Zda se ma otocit zobrazeni tlacitek Enter a Esc
#define DIALOG_BUTTON_Y                 51      // Y-ova souradnice tlacitek

// Menu
#define DISPLAY_MENU                    1
#define MENU_MAX_ITEMS                  9       // Maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti
#define DISPLAY_MENU_LINE_HEIGHT        16      // Vyska radku v pixelech
#define DISPLAY_MENU_Y                  8       // Y-ova souradnice pocatku menu v pixelech
#define DISPLAY_MENU_LINES_COUNT        3       // Pocet radku v menu
#define DISPLAY_MENU_ARROW_DY           4       // Vertikalni posun sipky (podle pouziteho fontu)
#define DISPLAY_CHOOSE_ITEM             1

// Jazyky
#define DISPLAY_LANGUAGES               1       // Pokud je definovano, popisky u tlacitek se automaticky dekoduji do spravneho jazyka

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
void DisplayEditExecute(byte idata *Chars);

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

#define K0         HermesK0
#define K1         HermesK1
#define K2         HermesK2
#define K3         HermesK3

// definice klaves :
typedef enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // klavesy, v zavislosti na konstrukci klavesnice :
//  K_RIGHT = _K_FIRSTUSER,              // sipka doprava
  K_DOWN  = _K_FIRSTUSER,              // sipka dolu, musi byt pro editaci
  K_UP,                                // sipka nahoru
  K_LEFT,                              // sipka doleva
  K_RIGHT,                             // sipka doprava
  K_ENTER,                             // Enter
  K_ESC,                               // Esc
  K_NULA,                              // !!! El12864
  K_BLANK,                             // !!! Menu
  K_MENU = K_ENTER,                    // Menu

  // Udalosti
  _K_EVENTS    = 0x40,                 // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,            // prekresleni namerenych hodnot
  K_BLINK_OFF,                         // periodicke blikani - zhasni
  K_BLINK_ON,                          // periodicke blikani - rozsvit
  K_TIMEOUT,                           // vyprsel cas necinnosti

  // systemove klavesy :
  K_REPEAT       = 0x80,               // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,               // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF                // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (500/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch

//-----------------------------------------------------------------------------
// Parametry radiove komunikace
//-----------------------------------------------------------------------------

#include "..\Hermes1\PktDef.h"           // popis protokolu

#define PACKET_BAUD     2400
//#define PACKET_PARITY
#define TXPACKET_TIMER_2              // vysilac  - rizeni casovacem 2
#define RXPACKET_TIMER_2              // terminal - rizeni casovacem 2

// Podminena kompilace :
#define TXPACKET_SYNC   1         // Vysila synchronizacni posloupnost

// Callback pro mereni signalu - pozor, vola se z preruseni seriove linky
void PacketIdReceived(byte Id);
void PacketAddressReceived(byte Address);
#define PacketCompleteReceived();

//-----------------------------------------------------------------------------
// Pripojeni ADC 0832
//-----------------------------------------------------------------------------

#define AdcCS   HermesCSAD   // DI + DO spolecna data
#define AdcDIO  HermesDAD   // DI + DO spolecna data
#define AdcCLK  HermesCLKAD // CLK hodiny

// casove prodlevy
#define AdcWait() _nop_();_nop_();   // max. hodinovy kmitocet 400kHz - doplnit NOPy na 1.25us

// ovladani chipselectu - pro vice prevodniku nahradit prazdnym makrem
// a CS ovladat pred a po volani API explicitne :

#define AdcEnable()  AdcCS = 0
#define AdcDisable() AdcCS = 1

//-----------------------------------------------------------------------------
// Analogove vstupy
//-----------------------------------------------------------------------------

#define AIN_ACCU    1       // Cislo analogoveho vstupu pro mereni akumulatoru
#define AIN_SIGNAL  0       // Cislo analogoveho vstupu pro mereni sily signalu

//-----------------------------------------------------------------------------

#define Delay( ms)  SysDelay( ms)

//-----------------------------------------------------------------------------


#endif
