//*****************************************************************************
//
//    Accu.h - Kontrola napajeciho napeti
//    Version 1.0
//
//*****************************************************************************

#ifndef __Accu_H__
   #define __Accu_H__

#include "Hardware.h"     // zakladni datove typy

#define ACCU_INDICATION_MAX   4     // Maximalni hodnota poctu dilku indikace

typedef struct {
  byte  Conversion;                 // Prevod
  byte  Indication;                 // Pocet dilku
  byte  Busy;                       // YES/NO prave se cte Ain
} TAccu;
extern TAccu __xdata__ Accu;

void AccuInit();

void AccuRead();
// Nacte stav akumulatoru

byte AccuCalibrate();
// Nacte kalibraci akumulatoru

#endif
