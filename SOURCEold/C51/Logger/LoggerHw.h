//*****************************************************************************
//
//    LoggerHW.h  - Logger independent hardware descriptions
//    Verze 1.0
//
//*****************************************************************************

#ifndef __LoggerHW_H__
   #define __LoggerHW_H__





//-----------------------------------------------------------------------------
// Displej - podminena kompilace
//-----------------------------------------------------------------------------

// Obecne funkce
#define USE_DISPLEJ_ROZNOUT      1

// Cary atd.
#define USE_VYMAZ_OBLAST_RYCHLE  1
#define USE_VYMAZ_OBLAST         1
#define USE_OBDELNIK_RYCHLE      1
//#define USE_VODOROVNA_CARA       1
#define USE_VODOROVNA_TECKOVANA_CARA 1
//#define USE_SVISLA_CARA_RYCHLE   1
#define USE_SVISLA_TECKOVANA_CARA_RYCHLE 1
//#define USE_SVISLA_CARA          1
#define USE_KRESLI_BOD           1
//#define USE_CARA                 1

// Fonty - pouziti jednotlivych fontu
#define USE_TAHOMA8    1
#define USE_LUCIDA6    1
#define USE_MYRIAD32   1
//#define USE_MYRIAD40   1

// Vety - pouziti jednotlivych zarovnani u vet
#define USE_VETA         1
#define USE_VETA_NASTRED 1
#define USE_VETA_NAPRAVO 1
//#define USE_VETA_NALEVO  1

// Zobrazeni cisla
#define USE_ROZLOZENI_CISLA 1
#define USE_ZOBRAZENI_CISLA 1
#define USE_ZOBRAZENI_ZNAKU_VE_FORMATU 1

// Symbol
#define USE_SYMBOL 1

// Editace
#define ALFANUMERICKA_KLAVESNICE 1   // Pokud je definovano, pouziva se numericka klavesnice. Jinak pouziva k editaci sipky
#define USE_EDITACE 1
#define USE_EDITACE_TEXTU 1

// Dialog
#define USE_DIALOG 1

// Menu
#define USE_MENU 1
#define MENU_MAX_POCET_POLOZEK 5        // maximalni pocet polozek v menu - nastavit na co nejmensi, kvuli alokaci pameti

// Makro pro obsluhu editace velicin, ktere se meni primo pri editaci (podsvit, zvuky atd.)
// Tady nemuzu pocitat stejne jako pri stisku Enter - tam to delam s mensimi naroky na pamet postupne jako byte, word atd. Jenze u toho
// znehodnodtim Znaky[], coz tady nemuzu, protoze jeste bude editovat.
//#define ObsluzModEditace()                                                                                                                              \
#define DisplayEditExecute(Znaky) 															\
  PipniKlavesnice()																	\
  if (ModEditace==EDITACE_PODSVIT) {                                                                                                                    \
    UN.DT=(unsigned long)(10000000*(unsigned long)Znaky[7]+1000000*(unsigned long)Znaky[6]+100000*(unsigned long)Znaky[5]+10000*(unsigned long)Znaky[4] \
          +1000*(unsigned long)Znaky[3]+100*(unsigned long)Znaky[2]+10*(unsigned long)Znaky[1]+(unsigned long)Znaky[0]);                                \
    Konfigurace.IntenzitaPodsvitu=UN.DT;                                                                                                                \
    SpustRizeniPodsvitu();                                                                                                                              \
  }


//-----------------------------------------------------------------------------
// Displej - parametry
//-----------------------------------------------------------------------------

#define SIRKA_DISPLEJE 128
#define VYSKA_DISPLEJE 64


//-----------------------------------------------------------------------------
// Parametry pripojeni signalu k prevodniku
//-----------------------------------------------------------------------------
#define KANAL_VAHA     3    // Kanal, ke kteremu je pripojen snimac
#define KANAL_AKU      1    // Kanal, ke kteremu je pripojen akumulator


//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

#define RS232_BAUD      9600          // Rychlost linky v baudech
#define RS232_USE_PARITY    1         // Provoz s paritou nebo bez

#define RS232_PARITY_EVEN   1         // Suda parita
//#define RS232_PARITY_ODD   1        // Licha parita

//#define RS232_CASOVAC_1    1        // K casovani linky se vyuzije casovac 1
#define RS232_CASOVAC_2    1          // K casovani linky se vyuzije casovac 2 (pouzitelne jen u C51RD2, ne u maleho Atmela)

#define RS232_GET           1         // Zda se pouziva cteni ze seriove linky
#define RS232_SEND          1         // Zda se pouziva zapis na seriovou linku



//-----------------------------------------------------------------------------
// Parametry LIFO
//-----------------------------------------------------------------------------

//#define __xdata__                // komunikacni buffer neni v XDATA
//#define __xdata__ xdata          // komunikacni buffer je v XDATA

struct SZaznam;                       // dopredna deklarace

typedef struct SZaznam TLifoData;      // Typ ukladane struktury (max 255 bytu)
#define LifoData        Zaznam  // Globalni buffer ukladanych dat

#define LIFO_START           0   // Pocatecni adresa LIFO v EEPROM
#define LIFO_CAPACITY     1800   // Maximalni pocet ukladanych polozek (32768/18=1820.44), tj. 368 bajtu rezerva
#define LIFO_MARKER       0xFF   // Znacka konce

// Podminena kompilace :
#define LIFO_FAST            1   // Zapamatovani pozice markeru (tj. rychlejsi provoz)
#define LIFO_VERIFY          1   // Verifikace po zapisu dat

//#define LIFO_GET_INDEX       1   // Cislo aktualniho zaznamu
#define LIFO_COUNT           1   // Celkovy pocet zaznamu
#define LIFO_READ            1   // Cteni zaznamu
//#define LIFO_FIRST           1   // Presun na prvni zaznam
//#define LIFO_NEXT            1   // Presun na dalsi zaznam
//#define LIFO_PREV            1   // Presun na predchozi zaznam
#define LIFO_LAST            1   // Presun na posledni zaznam
#define LIFO_DELETE_LAST     1   // Smazani posledniho zaznamu



//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------

// definice klaves :
enum {
  // systemove klavesy :
  K_NULL  = 0,             // vyhrazeno k pouziti pro menu & okna
  _K_FIRSTUSER,

  // Klavesy, v zavislosti na konstrukci klavesnice :
  K_ESC = _K_FIRSTUSER,
  K_DEL,
  K_ENTER,
  K_SHIFT,

  // Znaky a cislice dam tak, aby odpovidaly ASCII tabulce
  K_SPACE = ' ',

  K_0 = '0',
  K_1,
  K_2,
  K_DOWN = K_2,
  K_3,
  K_4,
  K_LEFT = K_4,
  K_5,
  K_6,
  K_RIGHT = K_6,
  K_7,
  K_8,
  K_UP = K_8,
  K_9,

  K_HVEZDICKA = '*',
  K_PLUS,
  K_TECKA = '.',

  K_A = 'A',
  K_B,
  K_C,
  K_D,
  K_E,
  K_F,
  K_G,
  K_H,
  K_I,
  K_J,
  K_K,
  K_L,
  K_M,
  K_N,
  K_O,
  K_P,
  K_Q,
  K_R,
  K_S,
  K_T,
  K_U,
  K_V,
  K_W,
  K_X,
  K_Y,
  K_Z,        // 'Z' = 0x5A

  // Udalosti
  _K_EVENTS    = 0x60,        // zde zacinaji udalosti
  K_REDRAW     = _K_EVENTS,   // prekresleni namerenych hodnot
  K_BLINK_OFF,                // periodicke blikani - zhasni
  K_BLINK_ON,                 // periodicke blikani - rozsvit
  K_TIMEOUT,                  // vyprsel cas necinnosti
  K_SERIAL,                   // Mam obslouzit seriovy port

  // systemove klavesy :
  K_REPEAT       = 0x80,   // opakovani klavesy OR ke klavese
  K_RELEASED     = 0xFE,   // pusteni klavesy (jednotlive i autorepeat)
  K_IDLE         = 0xFF    // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

// definice casovych konstant v ticich casovace :

#define KBD_DEBOUNCE            1                 // ustaleni po stisknuti
#define KBD_AUTOREPEAT_START (400/TIMER0_PERIOD)  // prodleva autorepeat
#define KBD_AUTOREPEAT_SPEED (200/TIMER0_PERIOD)  // kadence autorepeat

// Podmineny preklad :

#define KBD_POWER_UP       1                      // klavesa po zapnuti
//#define KBD_GETCH          1                      // funkce getch




//-----------------------------------------------------------------------------
// Pripojeni RTC PCF8583 pres I2C sbernici
//-----------------------------------------------------------------------------

#define RTC_ADDRESS 0           // adresa zadratovana pinem A0

// podmineny preklad :

//#define RTC_USE_HSEC    1       // cti/nastavuj setiny sekund
#define RTC_USE_DATE    1       // cti/nastavuj datum
//#define RTC_USE_WDAY    1       // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1       // hlidej zadavane hodnoty na rozsah




//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __Logger_H__
  #include "Logger.h"                   // Dal jsem si to do adresace projektu, ne do spolecneho inc, kde je to neprehledne
#endif
//-----------------------------------------------------------------------------

#endif
