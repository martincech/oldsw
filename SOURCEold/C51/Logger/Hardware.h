//*****************************************************************************
//
//    Hardware.h  - hardware descriptions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#ifdef __C51__
// jen pro Keil
#include "..\inc\89C51RD2.h"
#endif
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include "..\inc\cpu.h"
#include <datconv.h>
#include <intrins.h>
#include <absacc.h>             // makro XBYTE


//-----------------------------------------------------------------------------
// Hardwarove pripojeni displeje
//-----------------------------------------------------------------------------
sbit DisplayDI =    P1^6;
sbit DisplayRW =    P1^4;
sbit DisplayE =     P1^2;
sbit DisplayCS1 =   P2^7;
sbit DisplayCS2 =   P2^6;
sbit DisplayRESET = P2^5;
sbit DisplayD0 =    P1^0;
sbit DisplayD1 =    P0^0;
sbit DisplayD2 =    P0^1;
sbit DisplayD3 =    P0^2;
sbit DisplayD4 =    P0^4;
sbit DisplayD5 =    P0^5;
sbit DisplayD6 =    P0^6;
sbit DisplayD7 =    P0^7;

sbit PODSVIT   =    P2^4;


//-----------------------------------------------------------------------------
// Lowbat
//-----------------------------------------------------------------------------
sbit LowBat =    P0^3;

//-----------------------------------------------------------------------------
// Hardwarove pripojeni klavesnice
//-----------------------------------------------------------------------------
// Ka a K7 zde chybi, to je klavesa START
// Kb az Kh jsou sdilene s displejem, ty tedy budu budit
sbit KbdKb = P0^7;
sbit KbdKc = P0^6;
sbit KbdKd = P0^5;
sbit KbdKe = P0^4;
sbit KbdKf = P0^2;
sbit KbdKg = P0^1;
sbit KbdKh = P0^0;
sbit KbdK1 = P1^1;
sbit KbdK2 = P1^3;
sbit KbdK3 = P1^5;
sbit KbdK4 = P1^7;
sbit KbdK5 = P3^2;
sbit KbdK6 = P3^3;
sbit KbdK8 = P3^4;


//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------
// zapojeni pinu volne k uzivani :

sbit IicSCL     = P2^2;         // I2C hodiny
sbit IicSDA     = P2^3;         // I2C data

// casove prodlevy

#define IIC_WAIT   _nop_()      // 4takty + doplnit NOPy na 5us

// podmineny preklad :

#define IIC_READ    1           // cteni dat se sbernice

//-----------------------------------------------------------------------------
// Pripojeni RTC PCF8583 pres I2C sbernici
//-----------------------------------------------------------------------------

#define RTC_ADDRESS 0           // adresa zadratovana pinem A0

// podmineny preklad :

//#define RTC_USE_HSEC    1       // cti/nastavuj setiny sekund
#define RTC_USE_DATE    1       // cti/nastavuj datum
//#define RTC_USE_WDAY    1       // cti/nastavuj den v tydnu
#define RTC_RANGE_CHECK 1       // hlidej zadavane hodnoty na rozsah

//-----------------------------------------------------------------------------





// Zda je struktura fontu ulozena v idata nebo ne
#define __Font_idata__ idata

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 11.0592 MHz
#define FXTAL 11059200L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
#define T2_X2      1            // timer 2 v rezimu X2
#define SI_X2      1            // COM v rezimu X2
#define PCA_X2     1            // PCA v rezimu X2
#define WD_X2      1            // WatchDog v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost - nechci

// Casovac 0 :
#define TIMER0_PERIOD 20        // perioda casovace 0 v ms
// Casovac 1 - pro podsvit
#define TIMER1_PERIOD 1         // perioda casovace 1 v ms

#define __xdata__ xdata         // pouziva se XDATA - interni XRAM


//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// casovani zapisu :

#define IEP_EETIM         ((FXTAL / 1000000L + 1) * CPU_X2 * 5)   // casovani zapisu

// ochrana proti vybiti aku :

#define IepAccuOk()       LowBat      // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

//#define IEP_READ_BYTE     1        // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1        // zapis jednoho bytu




//-----------------------------------------------------------------------------
// Pripojeni AT25640 / X25645
//-----------------------------------------------------------------------------
// Tady je zapojena SGS-Thomson M95265, protokol je ale stejny. Navic se pamet nedeli na stranky, ale max. se muze naraz zapsat 64 bajtu, pri libovolne
// startovaci adrese. Nastavenim EEP_PAGE_SIZE to mam zajisteno, akorat to bude umele delit na stranky, coz mi nevadi.

// zapojeni pinu volne k uzivani :
sbit EepCS    = P3^5;            // chipselect /CS
sbit EepSCK   = P3^6;            // hodiny SCK
sbit EepSO    = P1^4;            // vystup dat SO
sbit EepSI    = P1^6;            // vstup dat SI (muze byt shodny s SO)
// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// ochrana proti vybiti aku :

#define EepAccuOk()       LowBat    // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    64      // velikost stranky
#define EEP_SIZE      32768      // celkova kapacita

// Podminena kompilace :

#define EEP_READ_BYTE    1       // cteni jednoho bytu
#define EEP_WRITE_BYTE   1       // zapis jednoho bytu



//-----------------------------------------------------------------------------
// Pripojeni AD7714
//-----------------------------------------------------------------------------

// zapojeni pinu volne k uzivani :
//sbit AdcDIO     = P1^2;     // DI + DO spolecna data
sbit AdcDIO     = P2^1;     // DI + DO spolecna data
sbit AdcCLK     = P3^7;     // CLK hodiny
//sbit AdcDRDY    = P3^2;     // DRDY vystup, nemusi se pouzivat

// Obsluha DRDY
//#define ADC_DRDY_CONNECTED 1    // Pokud je definovano, cte se zaneprazdneni z pinu DRDY. Pokud neni nadefinovano, cte se to z registru pomoci komunikace

// Podmineny preklad
#define ADC_USE_SET_INPUT_NO_WAIT 1    // Zda se pouziva fce  AdcSetInputNoWait(), tj. prepinani kanalu bez cekani

// Nastaveni parametru prevodniku
// Zesileni a bipolarni/unipolarni rezim
#define ADC_GAIN    ADC_GAIN_2          // Volba zesileni
#define ADC_MODE    ADC_BIPOLAR_MODE    // Volba bipolarniho nebo unipolarniho rezimu
// Filtr
// Nastaveni pro 10Hz je high=0x07 a low=0x80
#define ADC_FILTER_HIGH     0x07        // Horni bajt filtru (zadavat 4 LSB bity)
#define ADC_FILTER_LOW      0x80        // Dolni bajt filtru (zadavat celych 8 bitu)









// Casovani - musi byt kvuli klavesnici
void Delay( word n);
// Zpozdeni n x 1ms

#define delay( ms) Delay( ms)             // zpozdeni 1ms z modulu Main





//-----------------------------------------------------------------------------
#include "LoggerHW.h"         // projektove nezavisle definice - dal jsem si to do adresare projektu, namisto do spol. adresare inc, kde je to neprehledne
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
#endif
