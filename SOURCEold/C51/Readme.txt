Poznamky k modularni konstrukci
===============================

1. Predpokladame pracovni adresar D:\SRC
2. Do podadresare D:\SRC\INC ukladame soubory *.h jednotlivych
   modulu
3. Do podadresare D:\SRC\<modul> ulozime zdrojove soubory modulu,
   testovaci program, poznamky apod. Modulem rozumime napr.
   obsluhu RS232
4. Do specialniho podadresare D:\SRC\<projekt> ulozime zdrojovy
   soubor projektu (zpravidla jen Main.c), projektovy soubor
   a session pro MEW32 (viz dale). Dulezitou soucasti je soubor
   Hardware.h, do ktereho umistime vsechny definice z pouzitych
   modulu, tj <modul>.hw. Definice podle potreby v soubory Hardware.h
   upravime.
5. V adresari D:\SRC\<projekt> vytvorime tzv. Makefile podle dodaneho
   vzoru. Obsahuje seznam pouzitych modulu, hlavickovych souboru, apod.

Finalni navrh
-------------

- mame k dispozici soubor Hardware.h, ktery vznikl
  experimentovanim s bastlem

1. Podle schematu zapojeni se na zacatek Hardware.h
   umisti symbolicke definice prirazeni portu a sbernic
   ve tvaru <Projekt><NAZEV_SIGNALU_VE_SCHEMATU>, 
   napr. AutoCLK

2. Ve zbytku souboru Hardware.h se definice sbit u jednotlivych
   modulu nahradi #define SignalCLK AutoCLK. Timto se definuje
   schema zapojeni.

3. Ze souboru Hardware.h se vyriznou definice tykajici
   se schematu zapojeni a umisti do novehou souboru
   <Projekt>HW.h do adresare INC. Do souboru Hardware.h
   se umisti #include tohoto souboru

4. Prechod mezi bastlem a finalni verzi, resp. zmena
   prirazeni portu se potom deje v souboru Hardware.h
   projektu zmenou prirazeji <Projekt><NAZEV_SIGNALU>.
   Zmeny parametru uvedenych v <Projekt>HW.h se provadeji
   v Hardware.h za includem souboru <Projekt>HW.h,
   postupem :
   #undef PARAMETR 
   #define PARAMETR <nova_hodnota>
 


Prace s projekty v prostredi Multi Edit
---------------------------------------

Do adresare, kde je C51.exe (C:\...KEIL\BIN)
nakopirovat kmake.exe a builtins.mak

Pro praci budou stacit dva prikazy :
- preklad souboru v aktivnim okne
- sestaveni celeho projektu
Do prostredi MEW32 tyto prikazy dostaneme nasledujicim postupem :

Zvolime Tools/Customize, zalozku Customize a v ni tlacitko
File Extensions. Vybereme C51 a zvolime Edit.
Stiskneme tlacitko Compiler/Program setup.
V okne s programy zvolime Keil C51 Compiler V5.5 a stiskneme Copy
Polozku prejmenujeme na
   Description : "C Keil compiler"
   Command :  'c51 <FILE>.<EXT>
   Working dir : Current
   takto bude kompilator prekladat soubor prave aktivni
   v editoru a ne aaa.c51

Nyni vybereme Insert a definujeme novou polozku :
   Description 'Build all'
   Command     'kmake -B
   Working dir : Current
   ostatni volby default
Stiskneme tlacitko ... u Program type a otevreme okno Compiler/Program Type
Stiskneme Insert a vyplnime dialog Compiler/Program type setup :
 Type : Borland Keil make
 Exefile : C:\KEIL\BIN\KMAKE.EXE
 a do Regular expressions zkopirujeme retezce
 z Keil C51 Compiler (vybrat v okne Compiler/Program Type tlacitkem Edit)
Takto bude po prekladu mozne trasovat chyby na radcich zdrojoveho textu.
Ukoncime volbou Select, takze u Build All bude Program type 'Borland Keil make'

Zvolime menu Tools/Customize, zalozku Sessions
zde zvolime 'Encoded status file for each dir'
Timto zajistime ukladani kontextu projektu (otevrena
okna a dalsi nastaveni do adresare projektu)


Vytvoreni noveho projektu (nazev napr. AUTO) :
-----------------------------------------------

Vytvorime novou Session editoru (je nezbytne nutna, aby
byl definovan pracovni adresar). Zvolime File\Session Manager.
Vyplnime polozky
Name      : AUTO
Directory : D:\SRC\AUTO
Stiskneme Accept a Select.
Tim jsme zvolili Session s pracovnim adresarem D:\SRC\AUTO

Zadame Project/Set/Add a vybereme D:\SRC\AUTO\AUTO.PRJ.
Volby adresaru nastavime na adresar D:\SRC\AUTO.
Zvolime Set, tim je projekt nastaven jako implicitni.
Do okna projektu Project/View muzeme
vlozit vsechny zajimave soubory (budou dostupne kliknutim).
Zvolime Project/Options a nastavime na zalozce Directories
D:\SRC\AUTO
V zalozce Tools muzeme definovat nastroje pro sestaveni
projektu, doporucujeme nastavit analogicky k 'Build All'
viz vyse. Takto je definovano sestavovani bez ohledu
na to, jaky typ souboru je zrovna otevren (Menu Tools/Execute compiler
je zavisle na pripone otevreneho souboru).



V prazdnem adresari D:\SRC\AUTO umistime soubor Makefile,
ktery bude vypadat napr. takto :

#-----------------------------------------------------------------------------
#   AUTO Makefile
#-----------------------------------------------------------------------------

CFLAGS= CD DB              # volby pro kompilator C51
AFLAGS= DB                 # volby pro kompilator A51
                           # volby pro linker :
LFLAGS= RS(256) PL(68) PW(78)


# Zavislost na hlavickovych souborech :

HEADERS= ..\inc\Uni.h       \
         Hardware.h         \
         ..\inc\Display.h   \
         ..\inc\Font.h      \
         ..\inc\Mwi.h       \
         ..\inc\Temp.h

# Seznam prekladanych modulu :

OBJS=      ..\Display\Display.obj  \
           ..\Display\Font.obj     \
           ..\Temp\Mwi.obj         \
           ..\Temp\Temp.obj        \
           Main.obj

# Seznam linkovanych modulu (stejny jako OBJS, az na ty carky, kokoti z Keilu !) :

LINK_OBJS= ..\Display\Display.obj,  \
           ..\Display\Font.obj,     \
           ..\Temp\Mwi.obj,         \
           ..\Temp\Temp.obj,        \
           Main.obj

# Seznam uzivatelskych knihoven (musi zacinat carkou) :

LIBS =

#-------------------------------------------------------------
# Vlastni akce :

# Prevod objectu do HEX :

Auto.hex : Auto.exe

# Linkovani projektu

Auto.exe : $(OBJS)
        @echo $(LINK_OBJS) $(LIBS) TO $&.exe $(LFLAGS) > link.arg
	$(LINK) @link.arg

# Preklad modulu :

..\Display\Display.obj : ..\Display\Display.c  $(HEADERS)
..\Display\Font.obj    : ..\Display\Font.c     $(HEADERS)
..\Temp\Mwi.obj        : ..\Temp\Mwi.c         $(HEADERS)
..\Temp\Temp.obj       : ..\Temp\Temp.c        $(HEADERS)
Main.obj :               Main.c                $(HEADERS)





