//*****************************************************************************
//
//    Packet.h  - Packet receiver
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Packet_H__
   #define __Packet_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#include "..\Auto\PktDef.h"

// Globalni data :

extern TPacket __xdata__ Packet;
extern bit _PacketReady;

TYesNo PacketInit( void);
// Inicializace komunikace

TYesNo PacketCrc( void);
// Kontroluje zabezpeceni paketu

//TYesNo PacketReady( void);
// Je pripraven prijaty paket

#define PacketReady()   (_PacketReady)

//void PacketDone( void);
// Paket zpracovan

#define PacketDone()    (_PacketReady = NO)

#endif
