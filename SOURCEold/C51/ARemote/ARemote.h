//*****************************************************************************
//
//    ARemote.h  - Remote display utility
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __ARemote_H__
   #define __ARemote_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Komunikacni struktura :

typedef struct {
   // prenaseny cas :
   byte Hour;
   byte Min;
   byte Sec;
   byte Day;
   byte Month;
   word Year;
   // stavova hlaseni :
   byte ConnectionOk;    // YES/NO - spojeni bezi
   byte ConnectionUp;    // YES/NO - nabeh spojeni po vypadku
} TRemoteData;

// Deklarace dat terminalu :

extern TRemoteData __xdata__ RemoteData;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

TYesNo RemoteInit( void);
// Inicializace komunikace

void RemoteExecute( void);
// Cyklicka cinnost, volat po 20ms

void RemoteTimer1s( void);
// Tik casovace, volat po 1s

#endif
