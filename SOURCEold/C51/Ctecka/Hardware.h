//*****************************************************************************
//
//    Hardware.h  - hardware descriptions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C2051.h"
#include "..\inc\Rd2.h"
#include "..\inc\uni.h"
#include <intrins.h>

#define CteckaAccuOk()  1                // V pripade, ze by se merilo napeti, udelat tady test. Standardne je napeti stale ok.

// Piny pametoveho modulu:
/*sbit    CteckaINSMOD  = P1^6;        // INS modulu EEPROM
sbit    CteckaCSMOD   = P1^5;        // CS  modulu EEPROM
sbit    CteckaCLKMOD  = P1^2;        // SCK modulu EEPROM
sbit    CteckaSOMOD   = P1^4;        // SO  modulu EEPROM
sbit    CteckaSIMOD   = P1^3;        // SI  modulu EEPROM*/
// 18.3.2004: Zmeneny piny
sbit    CteckaINSMOD  = P1^6;        // INS modulu EEPROM
sbit    CteckaCSMOD   = P1^2;        // CS  modulu EEPROM
sbit    CteckaCLKMOD  = P1^3;        // SCK modulu EEPROM
sbit    CteckaSOMOD   = P1^4;        // SO  modulu EEPROM
sbit    CteckaSIMOD   = P1^5;        // SI  modulu EEPROM

// Piny supervisory u watchdogu X25045
sbit CteckaCSEE=P3^4;           // chipselect /CS
sbit CteckaCLK=P3^2;            // hodiny SCK
sbit CteckaSO=P3^5;             // vystup dat SO
sbit CteckaSI=P3^3;             // vstup dat SI (muze byt shodny s SO)


//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 11.0592 MHz
#define FXTAL 11059200L

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     1            // procesor v rezimu X2
#define T0_X2      1            // timer 0 v rezimu X2
#define T1_X2      1            // timer 1 v rezimu X2
//#define T2_X2      1            // timer 2 v rezimu X2
#define SI_X2      1            // COM v rezimu X2
//#define PCA_X2     1            // PCA v rezimu X2
//#define WD_X2      1            // WatchDog v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :
#define DOUBLE_BAUDRATE  1      // zdvojena baudova rychlost - nechci





//-----------------------------------------------------------------------------
#include "CteckaHW.h"         // projektove nezavisle definice - dal jsem si to do adresare projektu, namisto do spol. adresare inc, kde je to neprehledne
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
#endif
