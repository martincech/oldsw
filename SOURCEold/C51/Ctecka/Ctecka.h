//*****************************************************************************
//
//    Ctecka.h - Auto common data definitions
//    Version 1.0, (c) Petr Veit
//
//*****************************************************************************

#ifndef __Ctecka_H__
   #define __Ctecka_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include <stddef.h>        // makro offsetof
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define CTECKA_VERZE    0x0101       // verze SW

// Prikazy, ktere posila PC do ctecky
#define CMD_VERSION 0x10           // Poslu cislo verze
#define CMD_READ 0x20              // Poslu bajt ze zadane adresy
#define CMD_WRITE 0x30             // Zapisu poslany bajt na zadanou adresu
#define CMD_INSERT 0x40            // Poslu, zda je modul vlozeny ve ctecce.
#define CMD_INIT 0x50              // Poslu, zda je modul vlozeny ve ctecce.
#define CMD_BLOCK_READ 0x60        // Nacte pozadovany pocet bajtu od zadane adresy pomoci blokoveho cteni (tj. rychleji)

// Odpovedi
#define ACK 0x02
#define NAK 0x03


//-----------------------------------------------------------------------------
// Seriova linky
//-----------------------------------------------------------------------------

static byte Buffer[4];    // Prijimam naraz max. 4 bajty (pri blokovem cteni)
static byte Prikaz;       // Prikaz, ktery mne poslalo PC a ktery mam provest. Vyznam prikazu je napr. CMD_READ atd. Data jsou v poli Buffer.
static word Pocitadlo;    // Pocitadlo pro blokove cteni

static bit RS232Timeout;  // Zda doslo behem prijmu znaku k timeoutu


//-----------------------------------------------------------------------------
// Include potrebnych modulu, povoluje se v Hardware.h
//-----------------------------------------------------------------------------



//-----------------------------------------------------------------------------
#endif // __Ctecka_H__
