//*****************************************************************************
//
//    CteckaHW.h  - Auto independent hardware descriptions
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __CteckaHW_H__
   #define __CteckaHW_H__


//-----------------------------------------------------------------------------
// Pripojeni X25045 na desce
//-----------------------------------------------------------------------------

#define EepCS    CteckaCSEE           // chipselect /CS
#define EepSCK   CteckaCLK            // hodiny SCK
#define EepSO    CteckaSO             // vystup dat SO
#define EepSI    CteckaSI             // vstup dat SI (muze byt shodny s SO)
// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// ochrana proti vybiti aku :

#define EepAccuOk()       CteckaAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    16      // velikost stranky
#define EEP_SIZE        512      // celkova kapacita

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// Podminena kompilace :

//#define EEP_READ_BYTE    1       // cteni jednoho bytu
//#define EEP_WRITE_BYTE   1       // zapis jednoho bytu
#define EEP_USE_INS      1       // Zda se vyuziva signal INS






//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu s AT25640 / X25645
//-----------------------------------------------------------------------------

#define XmemCS     CteckaCSMOD     // chipselect /CS
#define XmemSCK    CteckaCLKMOD    // hodiny SCK
#define XmemSO     CteckaSOMOD     // vystup dat SO
#define XmemSI     CteckaSIMOD     // vstup dat SI (muze byt shodny s SO)
#define XmemInsert !CteckaINSMOD   // signal pritomnosti modulu aktivni v 0

// ochrana proti vybiti aku :

#define XmemAccuOk()       CteckaAccuOk()    // nastavi se na funkci hlidani AKU

// parametry pameti :

#define XMEM_PAGE_SIZE    32      // velikost stranky
#define XMEM_SIZE       8192      // celkova kapacita

// Inverze signalu:
#define XMEM_XSI_H  1
#define XMEM_XSCK_H 1

// Podminena kompilace :
#define XMEM_READ_BYTE    1       // cteni jednoho bytu
#define XMEM_WRITE_BYTE   1       // zapis jednoho bytu
#define XMEM_USE_INS      1       // Zda se vyuziva signal INS


//-----------------------------------------------------------------------------
// Parametry linky RS232
//-----------------------------------------------------------------------------

#define RS232_BAUD      9600          // Rychlost linky v baudech
#define RS232_USE_PARITY    1         // Provoz s paritou nebo bez

#define RS232_PARITY_EVEN   1         // Suda parita
//#define RS232_PARITY_ODD   1        // Licha parita

#define RS232_CASOVAC_1    1        // K casovani linky se vyuzije casovac 1
//#define RS232_CASOVAC_2    1          // K casovani linky se vyuzije casovac 2 (pouzitelne jen u C51RD2, ne u maleho Atmela)

#define RS232_GET           1         // Zda se pouziva cteni ze seriove linky
#define RS232_SEND          1         // Zda se pouziva zapis na seriovou linku


//-----------------------------------------------------------------------------
// hlavni include projektu, pro naplneni doprednych deklaraci,
// aby sly prelozit parametricke moduly

#ifndef __Ctecka_H__
  #include "Ctecka.h"                   // Dal jsem si to do adresace projektu, ne do spolecneho ic, kde je to neprehledne
#endif
//-----------------------------------------------------------------------------

#endif
