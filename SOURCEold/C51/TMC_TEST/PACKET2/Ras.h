//*****************************************************************************
//
//   Ras.h       Remote Terminal access
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Ras_H__
   #define __Ras_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void RasInit( void);
// Inicializace terminalu

void RasExecute( void);
// Periodicka obsluha - volat 1x za sekundu

#endif


