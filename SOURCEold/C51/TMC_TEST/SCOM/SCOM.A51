; C:\SOURCE\C51\VEIT\AUTO2\SCOM.A51 generated from: C:\SOURCE\C51\VEIT\AUTO2\SCOM.C51

$NOMOD51

NAME	SCOM

SSADR	DATA	096H
CCF2	BIT	0D8H.2
CCF3	BIT	0D8H.3
P0	DATA	080H
SSDAT	DATA	095H
CCF4	BIT	0D8H.4
P1	DATA	090H
P2	DATA	0A0H
P3	DATA	0B0H
AC	BIT	0D0H.6
P4	DATA	0C0H
T0	BIT	0B0H.4
SPCON	DATA	0C3H
P5	DATA	0E8H
T1	BIT	0B0H.5
EA	BIT	0A8H.7
T2	BIT	090H.0
SSCON	DATA	093H
EC	BIT	0A8H.6
CF	BIT	0D8H.7
CH	DATA	0F9H
FE	BIT	098H.7
SPSTA	DATA	0C4H
IEN0	DATA	0A8H
IEN1	DATA	0B1H
CL	DATA	0E9H
P0_0	BIT	080H.0
CCAP0H	DATA	0FAH
CEX0	BIT	090H.3
P1_0	BIT	090H.0
P0_1	BIT	080H.1
CCAP1H	DATA	0FBH
IPH0	DATA	0B7H
P2_0	BIT	0A0H.0
CEX1	BIT	090H.4
P1_1	BIT	090H.1
P0_2	BIT	080H.2
CCAP2H	DATA	0FCH
IPH1	DATA	0B3H
P3_0	BIT	0B0H.0
P2_1	BIT	0A0H.1
CEX2	BIT	090H.5
P1_2	BIT	090H.2
P0_3	BIT	080H.3
CCAP3H	DATA	0FDH
P4_0	BIT	0C0H.0
P3_1	BIT	0B0H.1
P2_2	BIT	0A0H.2
CEX3	BIT	090H.6
P1_3	BIT	090H.3
P0_4	BIT	080H.4
CCAP4H	DATA	0FEH
CCAP0L	DATA	0EAH
CR	BIT	0D8H.6
P5_0	BIT	0E8H.0
P4_1	BIT	0C0H.1
P3_2	BIT	0B0H.2
P2_3	BIT	0A0H.3
CEX4	BIT	090H.7
P1_4	BIT	090H.4
P0_5	BIT	080H.5
CCAP1L	DATA	0EBH
CCAPM0	DATA	0DAH
IPL0	DATA	0B8H
EXF2	BIT	0C8H.6
P5_1	BIT	0E8H.1
P4_2	BIT	0C0H.2
RD	BIT	0B0H.7
P3_3	BIT	0B0H.3
P2_4	BIT	0A0H.4
P1_5	BIT	090H.5
P0_6	BIT	080H.6
CCAP2L	DATA	0ECH
CCAPM1	DATA	0DBH
IPL1	DATA	0B2H
P5_2	BIT	0E8H.2
P4_3	BIT	0C0H.3
P3_4	BIT	0B0H.4
P2_5	BIT	0A0H.5
P1_6	BIT	090H.6
P0_7	BIT	080H.7
CCAP3L	DATA	0EDH
CCAPM2	DATA	0DCH
ES	BIT	0A8H.4
P5_3	BIT	0E8H.3
P4_4	BIT	0C0H.4
P3_5	BIT	0B0H.5
P2_6	BIT	0A0H.6
P1_7	BIT	090H.7
CCAP4L	DATA	0EEH
CCAPM3	DATA	0DDH
UD	BIT	0D0H.1
P5_4	BIT	0E8H.4
P4_5	BIT	0C0H.5
P3_6	BIT	0B0H.6
P2_7	BIT	0A0H.7
CCAPM4	DATA	0DEH
P5_5	BIT	0E8H.5
P4_6	BIT	0C0H.6
P3_7	BIT	0B0H.7
RI	BIT	098H.0
P5_6	BIT	0E8H.6
P4_7	BIT	0C0H.7
CY	BIT	0D0H.7
P5_7	BIT	0E8H.7
INT0	BIT	0B0H.2
TI	BIT	098H.1
INT1	BIT	0B0H.3
CKCON0	DATA	08FH
PT0L	BIT	0B8H.1
CKCON1	DATA	0AFH
PT1L	BIT	0B8H.3
RCAP2H	DATA	0CBH
PT2L	BIT	0B8H.5
SP	DATA	081H
CMOD	DATA	0D9H
CCON	DATA	0D8H
T2EX	BIT	090H.1
PX0L	BIT	0B8H.0
OV	BIT	0D0H.2
PX1L	BIT	0B8H.2
RCAP2L	DATA	0CAH
FCON	DATA	0D1H
C_T2	BIT	0C8H.1
WR	BIT	0B0H.6
KBLS	DATA	09CH
CKRL	DATA	097H
RCLK	BIT	0C8H.5
TCLK	BIT	0C8H.4
PPCL	BIT	0B8H.6
SBUF	DATA	099H
PCON	DATA	087H
SCON	DATA	098H
TMOD	DATA	089H
TCON	DATA	088H
BDRCON	DATA	09BH
SSCS	DATA	094H
IE0	BIT	088H.1
IE1	BIT	088H.3
AUXR	DATA	08EH
B	DATA	0F0H
CP_RL2	BIT	0C8H.0
AutoRX	BIT	090H.3
OSCCON	DATA	086H
ACC	DATA	0E0H
AutoTX	BIT	090H.4
ET0	BIT	0A8H.1
ET1	BIT	0A8H.3
TF0	BIT	088H.5
PI2	DATA	0F8H
ET2	BIT	0A8H.5
TF1	BIT	088H.7
RB8	BIT	098H.2
TH0	DATA	08CH
TF2	BIT	0C8H.7
EX0	BIT	0A8H.0
TH1	DATA	08DH
IT0	BIT	088H.0
EX1	BIT	0A8H.2
TB8	BIT	098H.3
TH2	DATA	0CDH
IT1	BIT	088H.2
P	BIT	0D0H.0
SM0	BIT	098H.7
TL0	DATA	08AH
SM1	BIT	098H.6
TL1	DATA	08BH
ECI	BIT	090H.2
KBE	DATA	09DH
SM2	BIT	098H.5
TL2	DATA	0CCH
KBF	DATA	09EH
RS0	BIT	0D0H.3
TR0	BIT	088H.4
RS1	BIT	0D0H.4
TR1	BIT	088H.6
TR2	BIT	0C8H.2
WDTPRG	DATA	0A7H
PI2_0	BIT	0F8H.0
PI2_1	BIT	0F8H.1
DPH	DATA	083H
BRL	DATA	09AH
DPL	DATA	082H
EXEN2	BIT	0C8H.3
REN	BIT	098H.4
T2MOD	DATA	0C9H
T2CON	DATA	0C8H
WDTRST	DATA	0A6H
EECON	DATA	0D2H
SADEN	DATA	0B9H
RXD	BIT	0B0H.0
PLS	BIT	0B8H.4
SADDR	DATA	0A9H
TXD	BIT	0B0H.1
AUXR1	DATA	0A2H
CKSEL	DATA	085H
F0	BIT	0D0H.5
PSW	DATA	0D0H
CCF0	BIT	0D8H.0
SPDAT	DATA	0C5H
CCF1	BIT	0D8H.1
?PR?SComInit?SCOM    SEGMENT CODE
?PR?_SComTx?SCOM     SEGMENT CODE
?PR?_SComRx?SCOM     SEGMENT CODE
?PR?PcaHandler?SCOM  SEGMENT CODE
?DT?SCOM             SEGMENT DATA
	PUBLIC	_SComRx
	PUBLIC	_SComTx
	PUBLIC	SComInit

	RSEG  ?DT?SCOM
          Count:   DS   1
    Counter?342:   DS   2
          Data?:   DS   1
; //*****************************************************************************
; //
; //    SCom.c - SW RS232 simple communication services
; //    Version 1.0, (c) VymOs
; //
; //*****************************************************************************
; 
; #include "SCom.h"
; 
; // Bastl : Module 0 = Rx,
; //         Module 1 = Tx
; 
; #define RX_CCAPM     CCAPM0
; #define RX_COUNTER_L CCAP0L
; #define RX_COUNTER_H CCAP0H
; #define RX_BITS      9              // vcetne startbitu
; #define RX_FLAG      CCF0
; 
; #define TX_CCAPM     CCAPM1
; #define TX_COUNTER_L CCAP1L
; #define TX_COUNTER_H CCAP1H
; #define TX_BITS      9              // vcetne stopbitu
; #define TX_FLAG      CCF1
; 
; // Casovani bitu :
; 
; #define ONE_BIT_COUNT      (FXTAL / 12 / SCOM_BAUD)
; #define HALF_BIT_COUNT     (ONE_BIT_COUNT / 2)
; 
; // Kontextova data :
; 
; static volatile byte Data;         // Rx/Tx byte
; static volatile byte Count;        // Rx/Tx bit count
; 
; // Pomocna struktura :
; 
; typedef union {
;    word Counter;
;    byte Byte[ 2];
; } TCounter;
; 
; //-----------------------------------------------------------------------------
; // Inicializace komunikace
; //-----------------------------------------------------------------------------
; 
; void SComInit( void)

	RSEG  ?PR?SComInit?SCOM
	USING	0
SComInit:
			; SOURCE LINE # 46
; // Inicializace komunikace
; {
			; SOURCE LINE # 48
;    // klidovy stav PCA :
;    EC         = 0;                     // zakaz preruseni od PCA
			; SOURCE LINE # 50
	CLR  	EC
;    CCON       = 0;                     // stop PCA, smazani vsech priznaku preruseni
			; SOURCE LINE # 51
	CLR  	A
	MOV  	CCON,A
;    CMOD       = 0;                     // hodiny fosc/12
			; SOURCE LINE # 52
	MOV  	CMOD,A
;    CL         = 0;                     // nulovani citace
			; SOURCE LINE # 53
	MOV  	CL,A
;    CH         = 0;
			; SOURCE LINE # 54
	MOV  	CH,A
;    CCAPM0     = CCAPM_STOP;            // zakaz vsech komparatoru
			; SOURCE LINE # 55
	MOV  	CCAPM0,A
;    CCAPM1     = CCAPM_STOP;
			; SOURCE LINE # 56
	MOV  	CCAPM1,A
;    CCAPM2     = CCAPM_STOP;
			; SOURCE LINE # 57
	MOV  	CCAPM2,A
;    CCAPM3     = CCAPM_STOP;
			; SOURCE LINE # 58
	MOV  	CCAPM3,A
;    CCAPM4     = CCAPM_STOP;
			; SOURCE LINE # 59
	MOV  	CCAPM4,A
;    // klidovy stav na portech :
;    SComTX     = 1;
			; SOURCE LINE # 61
	SETB 	AutoTX
;    SComRX     = 1;
			; SOURCE LINE # 62
	SETB 	AutoRX
;    // povoleni preruseni :
;    CR         = 1;                     // povol citani
			; SOURCE LINE # 64
	SETB 	CR
;    EC         = 1;                     // povol preruseni od PCA
			; SOURCE LINE # 65
	SETB 	EC
; } // SComInit
			; SOURCE LINE # 66
	RET  	
; END OF SComInit

; 
; //-----------------------------------------------------------------------------
; // Vysilani
; //-----------------------------------------------------------------------------
; 
; void SComTx( byte c)

	RSEG  ?PR?_SComTx?SCOM
	USING	0
_SComTx:
;---- Variable 'c?140' assigned to Register 'R7' ----
			; SOURCE LINE # 72
; // Vyslani znaku
; {
			; SOURCE LINE # 74
;    Data     = c;                       // vysilana data
			; SOURCE LINE # 75
	MOV  	Data?,R7
;    Count    = TX_BITS;                 // delka vysilanych dat
			; SOURCE LINE # 76
	MOV  	Count,#09H
;    TX_CCAPM = CCAPM_NEGATIVE_EDGE;     // preruseni od startbitu
			; SOURCE LINE # 77
	MOV  	CCAPM1,#011H
;    SComTX   = 0;                       // zahaj startbit
			; SOURCE LINE # 78
	CLR  	AutoTX
?C0002:
;    while( TX_CCAPM & CCAPM_ECCF);      // cekani na konec (zakaz preruseni = STOP)
			; SOURCE LINE # 79
	MOV  	A,CCAPM1
	JB   	ACC.0,?C0002
; } // SComTx
			; SOURCE LINE # 80
?C0004:
	RET  	
; END OF _SComTx

; 
; //-----------------------------------------------------------------------------
; // Prijem
; //-----------------------------------------------------------------------------
; 
; TYesNo SComRx( byte data *c)

	RSEG  ?PR?_SComRx?SCOM
	USING	0
_SComRx:
;---- Variable 'c?241' assigned to Register 'R7' ----
			; SOURCE LINE # 86
; // Vraci prijaty znak
; {
			; SOURCE LINE # 88
;    Data     = 0;                       // prijimana data
			; SOURCE LINE # 89
	CLR  	A
	MOV  	Data?,A
;    Count    = RX_BITS;                 // delka prijimanych dat
			; SOURCE LINE # 90
	MOV  	Count,#09H
;    RX_CCAPM = CCAPM_NEGATIVE_EDGE;     // cekani na startbit
			; SOURCE LINE # 91
	MOV  	CCAPM0,#011H
?C0005:
;    while( RX_CCAPM & CCAPM_ECCF);      // cekani na konec (zakaz preruseni = STOP)
			; SOURCE LINE # 92
	MOV  	A,CCAPM0
	JB   	ACC.0,?C0005
?C0006:
;    *c = Data;
			; SOURCE LINE # 93
	MOV  	R0,AR7
	MOV  	@R0,Data?
;    return( YES);
			; SOURCE LINE # 94
	SETB 	C
; } // SComRx
			; SOURCE LINE # 95
?C0007:
	RET  	
; END OF _SComRx

CSEG	AT	00033H
	LJMP	PcaHandler

; 
; //-----------------------------------------------------------------------------
; // Prerusovaci rutina
; //-----------------------------------------------------------------------------
; 
; static void PcaHandler( void) interrupt INT_PCA

	RSEG  ?PR?PcaHandler?SCOM
	USING	0
PcaHandler:
	PUSH 	ACC
	PUSH 	PSW
			; SOURCE LINE # 101
; // Prerusovaci rutina PCA
; {
; static TCounter Counter;
; 
;    // Rx ----------------------------------------------------------------------
;    if( RX_FLAG){
			; SOURCE LINE # 107
	JNB  	CCF0,?PcaHandler?rx_done
;       RX_FLAG = 0;                             // zrus priznak preruseni
			; SOURCE LINE # 108
	CLR  	CCF0
;       if( RX_CCAPM & CCAPM_CAPN){
			; SOURCE LINE # 109
	MOV  	A,CCAPM0
	JNB  	ACC.4,?C0009
;          // capture negative - vzorkovani startbitu
;          Counter.Byte[ 1] = RX_COUNTER_L;      // nejprve LSB
			; SOURCE LINE # 111
	MOV  	Counter?342+01H,CCAP0L
;          Counter.Byte[ 0] = RX_COUNTER_H;      // potom MSB
			; SOURCE LINE # 112
	MOV  	Counter?342,CCAP0H
;          Counter.Counter += HALF_BIT_COUNT;
			; SOURCE LINE # 113
	MOV  	A,#050H
	ADD  	A,Counter?342+01H
	MOV  	Counter?342+01H,A
	CLR  	A
	ADDC 	A,Counter?342
	MOV  	Counter?342,A
;          RX_COUNTER_L     = Counter.Byte[ 1];  // nejpve LSB
			; SOURCE LINE # 114
	MOV  	CCAP0L,Counter?342+01H
;          RX_COUNTER_H     = Counter.Byte[ 0];  // potom MSB
			; SOURCE LINE # 115
	MOV  	CCAP0H,Counter?342
;          RX_CCAPM         = CCAPM_SW_TIMER;    // compare mode - casovani bitu
			; SOURCE LINE # 116
	MOV  	CCAPM0,#049H
;          goto rx_done;
			; SOURCE LINE # 117
	SJMP 	?PcaHandler?rx_done
;       }
			; SOURCE LINE # 118
?C0009:
;       Counter.Counter += ONE_BIT_COUNT;
			; SOURCE LINE # 119
	MOV  	A,#0A0H
	ADD  	A,Counter?342+01H
	MOV  	Counter?342+01H,A
	CLR  	A
	ADDC 	A,Counter?342
	MOV  	Counter?342,A
;       RX_COUNTER_L     = Counter.Byte[ 1];     // nejpve LSB
			; SOURCE LINE # 120
	MOV  	CCAP0L,Counter?342+01H
;       RX_COUNTER_H     = Counter.Byte[ 0];     // potom MSB
			; SOURCE LINE # 121
	MOV  	CCAP0H,Counter?342
;       if( Count == RX_BITS){
			; SOURCE LINE # 122
	MOV  	A,Count
	CJNE 	A,#09H,?C0011
;          if( SComRX){
			; SOURCE LINE # 123
	JNB  	AutoRX,?C0012
;             // falesny startbit
;             RX_CCAPM = CCAPM_NEGATIVE_EDGE;     // nove cekani na startbit
			; SOURCE LINE # 125
	MOV  	CCAPM0,#011H
;             goto rx_done;
			; SOURCE LINE # 126
	SJMP 	?PcaHandler?rx_done
;          }
			; SOURCE LINE # 127
?C0012:
;          Count--;                               // startbit ok
			; SOURCE LINE # 128
	DEC  	Count
;          goto rx_done;
			; SOURCE LINE # 129
	SJMP 	?PcaHandler?rx_done
;       }
			; SOURCE LINE # 130
?C0011:
;       Data >>= 1;
			; SOURCE LINE # 131
	MOV  	A,Data?
	CLR  	C
	RRC  	A
	MOV  	Data?,A
;       if( SComRX){
			; SOURCE LINE # 132
	JNB  	AutoRX,?C0013
;          Data |= 0x80;
			; SOURCE LINE # 133
	ORL  	Data?,#080H
;       }
			; SOURCE LINE # 134
?C0013:
;       if( --Count == 0){
			; SOURCE LINE # 135
	DJNZ 	Count,?PcaHandler?rx_done
;          RX_CCAPM = CCAPM_STOP;                // konec dat
			; SOURCE LINE # 136
	MOV  	CCAPM0,#00H
;       }
			; SOURCE LINE # 137
;    }
			; SOURCE LINE # 138
; rx_done :;
			; SOURCE LINE # 139
?PcaHandler?rx_done:
;    // Tx ----------------------------------------------------------------------
;    if( TX_FLAG){
			; SOURCE LINE # 141
	JNB  	CCF1,?C0020
;       TX_FLAG = 0;                             // zrus priznak preruseni
			; SOURCE LINE # 142
	CLR  	CCF1
;       if( TX_CCAPM & CCAPM_CAPN){
			; SOURCE LINE # 143
	MOV  	A,CCAPM1
	JNB  	ACC.4,?C0016
;          // capture negative - zahajeni startbitu
;          Counter.Byte[ 1] = TX_COUNTER_L;      // nejprve LSB
			; SOURCE LINE # 145
	MOV  	Counter?342+01H,CCAP1L
;          Counter.Byte[ 0] = TX_COUNTER_H;      // potom MSB
			; SOURCE LINE # 146
	MOV  	Counter?342,CCAP1H
;          Counter.Counter += ONE_BIT_COUNT;
			; SOURCE LINE # 147
	MOV  	A,#0A0H
	ADD  	A,Counter?342+01H
	MOV  	Counter?342+01H,A
	CLR  	A
	ADDC 	A,Counter?342
	MOV  	Counter?342,A
;          TX_COUNTER_L     = Counter.Byte[ 1];  // nejpve LSB
			; SOURCE LINE # 148
	MOV  	CCAP1L,Counter?342+01H
;          TX_COUNTER_H     = Counter.Byte[ 0];  // potom MSB
			; SOURCE LINE # 149
	MOV  	CCAP1H,Counter?342
;          TX_CCAPM         = CCAPM_SW_TIMER;    // compare mode - casovani bitu
			; SOURCE LINE # 150
	MOV  	CCAPM1,#049H
;          goto tx_done;
			; SOURCE LINE # 151
	SJMP 	?C0020
;       }
			; SOURCE LINE # 152
?C0016:
;       Counter.Counter += ONE_BIT_COUNT;        // nasledujici bit
			; SOURCE LINE # 153
	MOV  	A,#0A0H
	ADD  	A,Counter?342+01H
	MOV  	Counter?342+01H,A
	CLR  	A
	ADDC 	A,Counter?342
	MOV  	Counter?342,A
;       TX_COUNTER_L     = Counter.Byte[ 1];     // nejpve LSB
			; SOURCE LINE # 154
	MOV  	CCAP1L,Counter?342+01H
;       TX_COUNTER_H     = Counter.Byte[ 0];     // potom MSB
			; SOURCE LINE # 155
	MOV  	CCAP1H,Counter?342
;       // compare mode - datove bity/stopbit
;       if( Count == 0){
			; SOURCE LINE # 157
	MOV  	A,Count
	JNZ  	?C0018
;          TX_CCAPM = CCAPM_STOP;                // konec stopbitu
			; SOURCE LINE # 158
	MOV  	CCAPM1,A
;          goto tx_done;
			; SOURCE LINE # 159
	SJMP 	?C0020
;       }
			; SOURCE LINE # 160
?C0018:
;       if( --Count == 0){
			; SOURCE LINE # 161
	DJNZ 	Count,?C0019
;          SComTX   = 1;                         // zahaj stopbit
			; SOURCE LINE # 162
	SETB 	AutoTX
;          goto tx_done;
			; SOURCE LINE # 163
	SJMP 	?C0020
;       }
			; SOURCE LINE # 164
?C0019:
;       SComTX = Data & 0x01;                    // datovy bit
			; SOURCE LINE # 165
	MOV  	A,Data?
	RRC  	A
	MOV  	AutoTX,C
;       Data >>= 1;
			; SOURCE LINE # 166
	MOV  	A,Data?
	CLR  	C
	RRC  	A
	MOV  	Data?,A
;    }
			; SOURCE LINE # 167
; tx_done :;
			; SOURCE LINE # 168
?PcaHandler?tx_done:
; } // PcaHandler
			; SOURCE LINE # 169
?C0020:
	POP  	PSW
	POP  	ACC
	RETI 	
; END OF PcaHandler

	END
