//*****************************************************************************
//
//    SCom.h - SW RS232 simple communication services
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __SCom_H__
   #define __SCom_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SComInit( void);
// Inicializace komunikace

void  SComTx( byte c);
// Vyslani znaku

TYesNo SComRx( byte data *c);
// Vraci prijaty znak

#endif
