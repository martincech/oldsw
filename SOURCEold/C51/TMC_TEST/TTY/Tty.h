//*****************************************************************************
//
//   Tty.h       Terminal interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Tty_H__
   #define __Tty_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

void TtyInit( void);
// Inicializace terminalu

TYesNo TtyCommand( byte cmd, dword arg);
// Vyslani prikazu

void TtyExecute( void);
// Periodicka data - volat 1x za sekundu

#endif


