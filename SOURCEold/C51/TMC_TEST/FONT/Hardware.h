//*****************************************************************************
//
//    Hardware.h  - TMC hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\inc\89C51ED2.h"
#include "..\inc\Rd2.h"
#include "..\inc\cpu.h"
#include "..\inc\uni.h"
#include <intrins.h>
#include <datconv.h>
#include <absacc.h>             // makro XBYTE
#include <stddef.h>             // makro offsetof

#define __ATTY__       1        // prekladat vysilac terminalu

//-----------------------------------------------------------------------------
// Globalni definice
//-----------------------------------------------------------------------------

#define TmcAccuOk()   TmcLOWBAT

// spolecne vodice (podle schematu) :

// Port 0 :

#define TmcDATA      P0                // datova sbernice
// datove bity :
sbit TmcD0      = TmcDATA^0;           // D0
sbit TmcD1      = TmcDATA^1;           // D1
sbit TmcD2      = TmcDATA^2;           // D2
sbit TmcD3      = TmcDATA^3;           // D3
sbit TmcD4      = TmcDATA^4;           // D4
sbit TmcD5      = TmcDATA^5;           // D5
sbit TmcD6      = TmcDATA^6;           // D6
sbit TmcD7      = TmcDATA^7;           // D7

// Port 1 :
sbit TmcTP1     = P1^0;                // TP1 - teplomer Rx
sbit TmcTP3     = P1^1;                // TP3 - teplomer Rx
sbit TmcTP4     = P1^2;                // TP4 - teplomer Rx
sbit TmcTP5     = P1^3;                // TP5 - teplomer Rx
sbit TmcRX2     = P1^4;                // RX2 - prijimac GSM
sbit TmcSO      = P1^5;                // SO  - EEPROM, MC33298, ADS7846
sbit TmcSCK     = P1^6;                // SCK - EEPROM, MC33298, ADS7846
sbit TmcSI      = P1^7;                // SI  - EEPROM, MC33298, ADS7846

// Port 2 :
sbit TmcMAJAK   = P2^0;                // MAJAK
sbit TmcCLKAD   = P2^1;                // CLKAD - hodiny TLC0838
sbit TmcDAD     = P2^2;                // DAD   - data TLC0838
sbit TmcOUTDI   = P2^3;                // OUTDI - vystup 74HC165
sbit TmcPISK    = P2^4;                // PISK  - sirena
sbit TmcSCL     = P2^5;                // SCK   - I2C clock (RX8025, TDA8444)
sbit TmcSCKM    = P2^6;                // SCKM  - hodiny modulu
sbit TmcCSD     = P2^7;                // CSD   - chipselect display

// Port 3 :
sbit TmcRXD     = P3^0;                // RxD   - prijimac ADM202/Flip
sbit TmcTXD     = P3^1;                // TxD   - vysilac  ADM202/Flip
sbit TmcPENIN   = P3^2;                // PENIN - INT0, preruseni ADS7846
sbit TmcTX2     = P3^3;                // TX2   - vysilac GSM
sbit TmcCSEE    = P3^4;                // CSEE  - chipselect EEPROM
sbit TmcCSDO    = P3^5;                // CSDO  - chipselect MC33298
sbit TmcCS1AD   = P3^6;                // CS1AD - chipselect TLC0838
sbit TmcCSM     = P3^7;                // CSM   - chipselect modulu

// Port 4 :
sbit TmcTP2     = P4^0;                // TP2   - teplomer Rx
sbit TmcTP6     = P4^1;                // TP6   - teplomer Rx
sbit TmcTP7     = P4^2;                // TP7   - teplomer Rx
sbit TmcCSPAN   = P4^3;                // CSPAN - chipselect ADS7846
sbit TmcTX3     = P4^4;                // TX3   - vysilac tiskarna
sbit TmcSOM     = P4^5;                // SOM   - data modulu
sbit TmcRESD    = P4^6;                // RESD  - reset display
sbit TmcCS2AD   = P4^7;                // CS2AD - chipselect TLC0838

// Port 5 :
sbit TmcCLKDI   = P5^0;                // CLKDI  - hodiny 74HC165
sbit TmcSDA     = P5^1;                // SDA    - I2C clock (RX8025, TDA8444)
sbit TmcLOWBAT  = P5^2;                // LOWBAT - kontrola napajeni
sbit TmcSVIT    = P5^3;                // SVIT   - podsvit display
sbit TmcCSEPOT  = P5^4;                // CSEPOT - chipselect potenciometru
sbit TmcWRD     = P5^5;                // WRD    - WR display
sbit TmcA0D     = P5^6;                // A0D    - A0 display
sbit TmcBUZTP   = P5^7;                // BUZTP  - teplomery Tx


//-----------------------------------------------------------------------------
// Prirazeni vstupu periferii
//-----------------------------------------------------------------------------

// Vstupy citace PCA :
#define TmcCEX0  TmcTP5                // Modul 0
#define TmcCEX1  TmcRX2                // Modul 1
#define TmcCEX2  TmcSO                 // Modul 2
#define TmcCEX3  TmcSCK                // Modul 3

// Adresy A/D prevodniku 1 :
#define TmcAV1   ADC_SINGLE_CH0        // A/D prevodnik 1/0
#define TmcAV2   ADC_SINGLE_CH1        // A/D prevodnik 1/1
#define TmcAV3   ADC_SINGLE_CH2        // A/D prevodnik 1/2
#define TmcAV4   ADC_SINGLE_CH3        // A/D prevodnik 1/3
#define TmcAV5   ADC_SINGLE_CH4        // A/D prevodnik 1/4
#define TmcAV6   ADC_SINGLE_CH5        // A/D prevodnik 1/5
#define TmcAV7   ADC_SINGLE_CH6        // A/D prevodnik 1/6
#define TmcAV8   ADC_SINGLE_CH7        // A/D prevodnik 1/7

// Adresy A/D prevodniku 2 :
#define TmcAV9   ADC_SINGLE_CH0        // A/D prevodnik 2/0
#define TmcAV10  ADC_SINGLE_CH1        // A/D prevodnik 2/1
#define TmcAV11  ADC_SINGLE_CH2        // A/D prevodnik 2/2
#define TmcAV12  ADC_SINGLE_CH3        // A/D prevodnik 2/3
#define TmcAV13  ADC_SINGLE_CH4        // A/D prevodnik 2/4
#define TmcAV14  ADC_SINGLE_CH5        // A/D prevodnik 2/5
#define TmcAV15  ADC_SINGLE_CH6        // A/D prevodnik 2/4
#define TmcAV16  ADC_SINGLE_CH7        // A/D prevodnik 2/5

#define TmcAVDIF ADC_DIFF_CH0          // A/D prevodnik 2/0,1 diferencialni vstup
#define TmcAVAKU TmcAV11               // A/D prevodnik mereni aku 24V

// Adresy D/A prevodniku :
#define TmcAVY1   0                    // D/A prevodnik 0
#define TmcAVY2   1                    // D/A prevodnik 1
#define TmcAVY3   2                    // D/A prevodnik 2
#define TmcAVY4   3                    // D/A prevodnik 3
#define TmcAVY5   4                    // D/A prevodnik 4
#define TmcAVY6   5                    // D/A prevodnik 5

#define TmcAVY12  0                    // D/A prevodnik 1+2
#define TmcAVY34  2                    // D/A prevodnik 3+4

// Digitalni vstupy :
#define TmcDI1    0x01                 // vstup DI1 (D0)
#define TmcDI2    0x02                 // vstup DI1 (D1)
#define TmcDI3    0x04                 // vstup DI1 (D2)
#define TmcDI4    0x08                 // vstup DI1 (D3)
#define TmcDI5    0x10                 // vstup DI1 (D4)
#define TmcDI6    0x20                 // vstup DI1 (D5)
#define TmcDI7    0x40                 // vstup DI1 (D6)
#define TmcDI8    0x80                 // vstup DI1 (D7)

// Digitalni vystupy :
#define TmcDVY1   0x01                 // vystup DVY1 (OP0)
#define TmcDVY2   0x02                 // vystup DVY2 (OP1)
#define TmcDVY3   0x04                 // vystup DVY3 (OP2)
#define TmcDVY4   0x08                 // vystup DVY4 (OP3)
#define TmcDVY5   0x10                 // vystup DVY5 (OP4)
#define TmcDVY6   0x20                 // vystup DVY6 (OP5)
#define TmcDVY7   0x40                 // vystup DVY7 (OP6)
#define TmcDVY8   0x80                 // vystup DVY8 (OP7)

//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

#define FXTAL 18432000L                // Krystal 18.432 MHz

// Konstanty pro vypocty casu v rezimu X2 (1=normal, 2=X2) :
#define CPU_X2     2                   // procesor v rezimu X2
#define T0_X2      1                   // timer 0 v rezimu X2
#define T1_X2      1                   // timer 1 v rezimu X2
#define T2_X2      1                   // timer 2 v rezimu X2
#define SI_X2      1                   // COM v rezimu X2
#define PCA_X2     1                   // PCA v rezimu X2
#define WD_X2      1                   // WatchDog v rezimu X2
#define SPI_X2     1                   // SPI v rezimu X2

// Konstanta pro dvojnasobnou baudovou rychlost SMOD1 (1=normal, 2=double) :

#define DOUBLE_BAUDRATE  1             // zdvojena baudova rychlost (1=ne, 2=ano)

// Casovac 0 :

#define TIMER0_PERIOD 20               // perioda casovace 0 v ms

#define __xdata__ xdata                // pouziva se XDATA - interni XRAM

//-----------------------------------------------------------------------------
// Tonovy generator PCA
//-----------------------------------------------------------------------------

#define PcaSoundOut      TmcPISK       // vystup generatoru

// PCA Modul 3 :
#define SOUND_CCAPM      CCAPM3
#define SOUND_COUNTER_L  CCAP3L
#define SOUND_COUNTER_H  CCAP3H
#define SOUND_FLAG       CCF3


// Podminena kompilace :

#define PCA_SOUND        1             // povoleni zvukoveho vystupu
#define PCA_ASYNC_BEEP   1             // asynchronni pipani

//-----------------------------------------------------------------------------
// COM parametry interniho UART
//-----------------------------------------------------------------------------

#define COM_BAUD         9600          // Rychlost linky v baudech

#define COM_PARITY_EVEN  1             // Suda parita
//#define COM_PARITY_ODD 1             // Licha parita

//#define COM_TIMER_1    1             // K casovani linky se vyuzije casovac 1
#define COM_TIMER_2      1             // K casovani linky se vyuzije casovac 2

#define COM_RX_TIMEOUT   10000         // Meziznakovy timeout [us]

#define COM_RX_WAIT      1             // Preklada se funkce ComRxWait
#define COM_FLUSH_CHARS  1             // preklada se funkce ComFlushChars

//-----------------------------------------------------------------------------
// COM2 pres PCA
//-----------------------------------------------------------------------------

#define PcaRX2           TmcRX2        // prijimaci port, POZOR musi souhlasit s CEX1
#define PcaTX2           TmcTX2        // vysilaci port

// PCA Modul 1 :
#define RX2_CCAPM        CCAPM1
#define RX2_COUNTER_L    CCAP1L
#define RX2_COUNTER_H    CCAP1H
#define RX2_FLAG         CCF1

// PCA Modul 0 :
#define TX2_CCAPM        CCAPM0
#define TX2_COUNTER_L    CCAP0L
#define TX2_COUNTER_H    CCAP0H
#define TX2_FLAG         CCF0

#define COM2_BAUD        38400
#define COM2_RX_TIMEOUT   3000         // meziznakovy timeout [us]

#define PCA_COM2         1             // povoleni prekladu COM2
#define COM2_HALFDUPLEX  1             // poloduplexni provoz
//#define COM2_RX_WAIT     1             // preklada se funkce Com2RxWait
//#define COM2_FLUSH_CHARS 1             // preklada se funkce Com2FlushChars

//-----------------------------------------------------------------------------
// COM3 pres PCA
//-----------------------------------------------------------------------------

#define PcaTX3           TmcTX3        // vysilaci port

// PCA Modul 2 :
#define TX3_CCAPM        CCAPM2
#define TX3_COUNTER_L    CCAP2L
#define TX3_COUNTER_H    CCAP2H
#define TX3_FLAG         CCF2

#define COM3_BAUD        9600

#define PCA_COM3         1             // povoleni prekladu COM3

//-----------------------------------------------------------------------------
// Interni EEPROM
//-----------------------------------------------------------------------------

// ochrana proti vybiti aku :

#define IepAccuOk()  TmcAccuOk()       // nastavi se na funkci hlidani AKU

// prace s prerusenim :

#define IepDisableInts()  EA = 0
#define IepEnableInts()   EA = 1

// Podminena kompilace :

//#define IEP_READ_BYTE     1          // cteni jednoho bytu
//#define IEP_WRITE_BYTE    1          // zapis jednoho bytu

//-----------------------------------------------------------------------------
// Ovladani majaku
//-----------------------------------------------------------------------------

#define TmcMajak( On)    TmcMAJAK = !(On)

//-----------------------------------------------------------------------------
// Pripojeni X9313
//-----------------------------------------------------------------------------

#define E2PotCS    TmcCSEPOT           // Chip select
#define E2PotUD    TmcD0               // Smer Up/Down
#define E2PotINC   TmcD1               // Provedeni 1 kroku

//-----------------------------------------------------------------------------
// Pripojeni displeje
//-----------------------------------------------------------------------------

#define DisplayData TmcDATA            // datova sbernice

#define DisplayCS   TmcCSD             // /CS vyber cipu
#define DisplayA0   TmcA0D             // prikaz/data nekdy oznaceny jako CD
#define DisplayWR   TmcWRD             // zapis /WR
#define DisplayRES  TmcRESD            // reset /RES

// makra nezavisla na zapojeni pinu (volne k pouziti) :

#define DisplayEnable()  DisplayWR = 1; DisplayCS = 0
#define DisplayDisable() DisplayCS = 1

#define DisplayAddressCommand() (DisplayA0 = 1)
#define DisplayAddressData()    (DisplayA0 = 0)

#define DisplayReset() (DisplayRES = 1)
#define DisplayRun()   (DisplayRES = 0)

#define DisplayWriteData( d) {DisplayWR = 0; DisplayData = d; DisplayWR = 1;}

// Pracovni mod displeje :

//#define DISPLAY_MODE_GRAPHICS2 1       // graficky mod displeje - 2 roviny
#define DISPLAY_MODE_MIXED 1             // textovy mod

#define DisplayPodsvit( On)   TmcSVIT = (On)  // rizeni podsvitu

#define DISPLAY_MOVE_TO  1            // funkce move to

//-----------------------------------------------------------------------------
// Jednoduche zobrazeni
//-----------------------------------------------------------------------------

#define DISPLAY_PUTCHAR    1
#define DISPLAY_PATTERN    1
//#define DISPLAY_CURSOR     1        // nastavovani kurzoru
#define DISPLAY_CLEAR      1        // mazani
//#define DISPLAY_CLR_EOL    1        // mazani do konce radku
#define DISPLAY_GOTO_RC    1
//#define DISPLAY_MOVE_TO    1

//-----------------------------------------------------------------------------
// Pripojeni klavesnice
//-----------------------------------------------------------------------------


// definice klaves :
typedef enum {
   // systemove klavesy :
   K_NULL  = 0,                        // vyhrazeno k pouziti pro menu & okna
   _K_FIRSTUSER,

   // klavesy, v zavislosti na konstrukci klavesnice :
   K_RIGHT = _K_FIRSTUSER,             // sipka doprava
   K_LEFT,                             // sipka doleva
   K_UP,                               // sipka nahoru
   K_DOWN,                             // sipka dolu
   K_ENTER,                            // Enter
   K_ESC,                              // Esc
   K_TOUCH,                            // aktivni touchpad
   K_TIMEOUT,                          // timeout
   K_REDRAW,                           // prekresleni

   // systemove klavesy :
   K_REPEAT       = 0x80,              // opakovani klavesy OR ke klavese
   K_RELEASED     = 0xFE,              // pusteni klavesy (jednotlive i autorepeat)
   K_IDLE         = 0xFF               // vyhrazeno k internimu pouziti, prazdny cyklus cteni
};

//-----------------------------------------------------------------------------
// Pripojeni Touch Screen ADS 7846
//-----------------------------------------------------------------------------

#define TouchDCLK    TmcSCK            // hodiny DCLK
#define TouchDIN     TmcSI             // vstup dat DIN
#define TouchDOUT    TmcSO             // vystup dat DOUT
#define TouchPENIRQ  TmcPENIN          // preruseni od dotyku /PENIRQ

// chipselect :
#define TouchCS           TmcCSPAN     // chipselect /CS
#define TouchSelect()     TouchCS = 0
#define TouchDeselect()   TouchCS = 1

// technicke konstanty :

#define TOUCH_DELAY       4            // doba ustaleni udaje  [ms]
#define TOUCH_PEN_RISE    5            // doba po nabehu PENIRQ [ms]
#define TOUCH_PEN_FALL   15            // doba odpadeni PENIRQ [ms]
#define TOUCH_DATA_SIZE  12            // 12 bitu A/D

#define TOUCH_REPEAT_COUNT 2           // Pocet opakovani cteni souradnic
#define TOUCH_REPEAT_DELAY 5           // Cekani v ms mezi jednotlivymi ctenimi souradnic
#define TOUCH_REPEAT_RANGE 30          // Maximalni odchylka souradnic v absolutnich jednotkach pri opakovanem cteni

#define TOUCH_AUTOREPEAT_START (700/TIMER0_PERIOD)  // prodleva prvniho autorepeat
#define TOUCH_AUTOREPEAT_SPEED (300/TIMER0_PERIOD)  // kadence autorepeat

// Rozsahy souradnic :
//#define TOUCH_X_RANGE     0            // kalibrace
//#define TOUCH_Y_RANGE     0            // kalibrace
#define TOUCH_X_RANGE     320          // rozsah souradnice X : 0..<X-1>
#define TOUCH_Y_RANGE     240          // rozsah souradnice Y : 0..<Y-1>
#define TOUCH_XL        0x0F0          // surovy levy okraj
#define TOUCH_XR        0xF10          // surovy pravy okraj
#define TOUCH_YU        0xEA0          // surovy horni okraj
#define TOUCH_YD        0x120          // surovy dolni okraj

//-----------------------------------------------------------------------------
// Pripojeni I2C sbernice
//-----------------------------------------------------------------------------

#define IicSCL  TmcSCL                 // I2C hodiny
#define IicSDA  TmcSDA                 // I2C data

// casove prodlevy

#define IIC_WAIT   {byte i; for( i = 6; i > 0; i--);}  // 4takty + doplnit NOPy na 5us

// podmineny preklad :

#define IIC_READ    1                  // cteni dat se sbernice


//-----------------------------------------------------------------------------
// Pripojeni RTC RX-8025 pres I2C sbernici
//-----------------------------------------------------------------------------

// podmineny preklad :

#define RTC_USE_DATE    1              // cti/nastavuj datum
#define RTC_RANGE_CHECK 1              // hlidej zadavane hodnoty na rozsah

//-----------------------------------------------------------------------------
// Pripojeni DA prevodniku TDA8444 pres I2C sbernici
//-----------------------------------------------------------------------------

#define DAC_ADDRESS      0x04          // adresa zadratovana piny A0..A2

// podmineny preklad :

#define DAC_SINGLE_WRITE 1             // zapis do jednoho prevodniku

//-----------------------------------------------------------------------------
// Pripojeni ADC 0838
//-----------------------------------------------------------------------------

#define AdcDIO  TmcDAD                 // DI + DO spolecna data
#define AdcCLK  TmcCLKAD               // CLK hodiny

#define Adc1Enable()   TmcCS1AD = 0    // chipselect prevodniku 1
#define Adc1Disable()  TmcCS1AD = 1    // deselect prevodniku 1

#define Adc2Enable()   TmcCS2AD = 0    // chipselect prevodniku 2
#define Adc2Disable()  TmcCS2AD = 1    // deselect prevodniku 2

// casove prodlevy

#define AdcWait() _nop_();_nop_();_nop_();_nop_();   // max. hodinovy kmitocet 400kHz - doplnit NOPy na 1.25us

// pro vice prevodniku externi chipselect :
#define AdcEnable()
#define AdcDisable()

//-----------------------------------------------------------------------------
// Pripojeni AT25256
//-----------------------------------------------------------------------------

#define EepCS    TmcCSEE               // chipselect /CS
#define EepSCK   TmcSCK                // hodiny SCK
#define EepSO    TmcSO                 // vystup dat SO
#define EepSI    TmcSI                 // vstup dat SI (muze byt shodny s SO)

// ovladani chipselectu je soucasti protokolu, nelze modifikovat

// ochrana proti vybiti aku :

#define EepAccuOk()       TmcAccuOk()  // nastavi se na funkci hlidani AKU

// parametry pameti :

#define EEP_PAGE_SIZE    64            // velikost stranky
#define EEP_SIZE      32768            // celkova kapacita

// Inverze signalu:
#define EEP_XSI_H  1
#define EEP_XSCK_H 1

// Podminena kompilace :

//#define EEP_READ_BYTE    1             // cteni jednoho bytu
//#define EEP_WRITE_BYTE   1             // zapis jednoho bytu

//-----------------------------------------------------------------------------
// Pripojeni externiho pametoveho modulu AT25256
//-----------------------------------------------------------------------------

#define XmemCS     TmcCSM              // chipselect /CS
#define XmemSCK    TmcSCKM             // hodiny SCK
#define XmemSO     TmcSOM              // vystup dat SO
#define XmemSI     TmcD3               // vstup dat SI (muze byt shodny s SO)

// ochrana proti vybiti aku :

#define XmemAccuOk()       TmcAccuOk() // nastavi se na funkci hlidani AKU

// parametry pameti :
#define XMEM_PAGE_SIZE    64           // velikost stranky
#define XMEM_SIZE      32768           // kapacita externiho pametoveho modulu

// Inverze signalu:
#define XMEM_XSI_H  0
#define XMEM_XSCK_H 0

// Podminena kompilace :

#define XMEM_READ_BYTE    1          // cteni jednoho bytu
//#define XMEM_WRITE_BYTE   1          // zapis jednoho bytu
//#define XMEM_USE_INS      1          // Zda se vyuziva signal INS

//-----------------------------------------------------------------------------
// Pripojeni 1-Wire sbernic
//-----------------------------------------------------------------------------

#define MwiDQI0  TmcTP1                // T1 Data Input Channel 0
#define MwiDQI1  TmcTP2                // T2 Data Input Channel 1
#define MwiDQI2  TmcTP3                // T3 Data Input Channel 2
#define MwiDQI3  TmcTP4                // T4 Data Input Channel 3
#define MwiDQI4  TmcTP5                // T5 Data Input Channel 4
#define MwiDQI5  TmcTP6                // T6 Data Input Channel 5
#define MwiDQI6  TmcTP7                // T7 Data Input Channel 6

#define MwiDQO   TmcBUZTP              // Data Output all channels

// logicke urovne na vystupu :

#define DQO_LO    0                    // je pres hradlo a jeste tranzistor, normalni logika
#define DQO_HI    1

// casove prodlevy :

// Volani funkce s konstantou 4 takty
// cyklus je <count + 1> * 12 taktu

// Celkove zpozdeni cyklu for 2 + 4 * n

#define MWI_DELAY_RESET          MwiMicroDelay( 122)  // Zpozdeni 480 us pro reset
#define MWI_DELAY_PRESENCE       MwiMicroDelay( 17)   // Zpozdeni 70us mezi reset a presence
#define MWI_DELAY_RESET_TIMESLOT MwiMicroDelay( 104)  // Zpozdeni 410us mezi presence a dalsim
#define MWI_DELAY_START          _nop_();_nop_();_nop_();_nop_()  // minimalni delka startbitu > 1us
#define MWI_DELAY_READ           10                   // Zpozdeni 15us v cyklu for
#define MWI_DELAY_READ_SLOT      MwiMicroDelay( 24)   // Zpozdeni 100us
#define MWI_DELAY_WRITE          MwiMicroDelay( 27)   // Zpozdeni 110us

// identifikace kanalu a zaroven podmineny preklad :

#define MWI_CH0                  0     // preklada se kanal 0
#define MWI_CH1                  1     // preklada se kanal 1
#define MWI_CH2                  2     // preklada se kanal 2
#define MWI_CH3                  3     // preklada se kanal 3
#define MWI_CH4                  4     // preklada se kanal 4
#define MWI_CH5                  5     // preklada se kanal 5
#define MWI_CH6                  6     // preklada se kanal 6

// podmineny preklad narocnych funkci :

#define MWI_CRC8                 1     // preklada se kod pro vypocet CRC
#define MWI_FAST_CRC8            1     // vypocet CRC tabulkou

//-----------------------------------------------------------------------------
// Teplomery DS18B20
//-----------------------------------------------------------------------------

#define TEMP_DS18B20       1           // definice typu DS18B20 12 bitovy
#define TEMP_SECURE_READ   1           // bezpecne cteni (vcetne CRC) (musi byt definovan MWI_CRC8)

//-----------------------------------------------------------------------------
// Teplomery
//-----------------------------------------------------------------------------

#define _THERMO_LAST    (MWI_CH6 + 1)  // celkovy pocet teplomeru
#define THERMO_TRIALS   3              // pocet pokusu na cteni z jednoho teplomeru

//-----------------------------------------------------------------------------
// Digitalni vstup 74HC165
//-----------------------------------------------------------------------------

#define DinCP       TmcCLKDI           // hodiny CP
#define DinPL       TmcD2              // parallel load /PL
#define DinQ7       TmcOUTDI           // vystup registru Q7

//-----------------------------------------------------------------------------
// Digitalni vystup MC33298
//-----------------------------------------------------------------------------

#define DoutSI      TmcSI              // serial input SI
#define DoutSO      TmcSO              // serial output SO
#define DoutSCLK    TmcSCK             // serial clock SCLK
#define DoutCS      TmcCSDO            // chipselect /CS

// Je-li definovan nasledujici symbol, funkce DoutWrite
// vraci aktualni stav vystupu

#define DOUT_READBACK    1

// Maska kontroly pullup a spravne hodnoty
// na uvedenych vystupech
// 0x01 je DO0 .. 0x80 je DO7

#define DOUT_MASK   0xFF               // maska aktivnich vystupu

//-----------------------------------------------------------------------------
// Povoleni prekladu Tmc.h
//-----------------------------------------------------------------------------

#define TMC_TEMPERATURE    1           // preklada se teplomer
#define TMC_SERVO          1           // preklada se servo
#define TMC_ACCU           1           // preklada se aku
#define TMC_FAN            1           // preklada se ventilator
#define TMC_DIO            1           // prekladaji se digitalni vstupy/vystupy
#define TMC_LOG            1           // preklada se logger
//#define TMC_PRT            1         // preklada se protokolovani
#define TMC_REG            1           // preklada se regulator
#define TMC_DIESEL         1           // preklada se diesel
#define TMC_CONFIG         1           // preklada se konfigurace
#define TMC_FRESH_AIR      1           // preklada se fresh air


//-----------------------------------------------------------------------------
// Parametry FIFO - datovy logger
//-----------------------------------------------------------------------------

struct STmcLog;                        // dopredna deklarace

typedef struct STmcLog  TFifoData;     // Typ ukladane struktury (max 255 bytu)
#define FifoData        TmcLog         // Globalni buffer ukladanych dat

#define FIFO_START            TMC_FIFO_START          // Pocatecni adresa FIFO v EEPROM
#define FIFO_CAPACITY         TMC_FIFO_CAPACITY       // Maximalni pocet ukladanych polozek
#define FIFO_MARKER_EMPTY     TMC_FIFO_MARKER_EMPTY   // Znacka konce pri neuplnem zaplneni
#define FIFO_MARKER_FULL      TMC_FIFO_MARKER_FULL    // znacka konce pri prepisovani

#define FIFO_INVALID_ADDRESS 0xFFFF                    // neplatna adresa do FIFO - MUSI korespondovat s XFIFO_INVALID_ADDRESS ve fifo.tpl!!!!!

// Podminena kompilace :
#define FIFO_FAST            1         // Zapamatovani pozice markeru
#define FIFO_VERIFY          1         // Verifikace po zapisu dat
#define FIFO_READ            1         // Cteni obsahu po polozkach
//#define FIFO_FIND_FIRST      1       // Nalezeni adresy prvniho zaznamu
//#define FIFO_FIND_LAST       1       // Nalezeni adresy posledniho zaznamu

//-----------------------------------------------------------------------------
// Modul SMS
//-----------------------------------------------------------------------------

#define SMS_MAX_LENGTH  161            // max. delka prijate zpravy, vc. koncove nuly

// buffery pro cisla :
extern char code DstNumber[];          // cilove cislo
extern byte code DstLength;            // pocet cislic ciloveho cisla
extern char xdata SrcNumber[];         // zdrojove cislo
extern byte xdata SrcLength;           // pocet cislic zdrojoveho cisla

// Definice ciloveho cisla :
#define SmsDstNumberLength()       DstLength                    // pocet cislic ciloveho cisla
#define SmsDstNumberDigit( i)      (DstNumber[i] - '0')         // cislice v poradi, jak je piseme

// Definice zdrojoveho cisla :
#define SmsSrcNumberLength( n)     SrcLength = n;               // pocet cislic zdrojoveho cisla
#define SmsSrcNumberDigit( i, n)   SrcNumber[ i] = (n) + '0'    // cislice v poradi, jak je piseme

// Podminena kompilace :

#define COM_RX_STRING       1          // funkce pro SMS_OPERATOR
#define SMS_OPERATOR        1          // nazev operatora
#define SMS_SIGNAL_STRENGTH 1          // sila signalu
#define SMS_COM2            1          // komunikace pres COM2

//-----------------------------------------------------------------------------
// Modul tiskarny
//-----------------------------------------------------------------------------

#define PRINT_COM3          1          // presmeruj na COM3

#define PrintPowerOn()                 // zapnuti napajeni budice tiskarny
#define PrintPowerOff()                // zapnuti napajeni budice tiskarny

// Podmineny preklad funkci :
#define PRT_HEX             1          // PrtHex
#define PRT_DEC             1          // PrtDec
#define PRT_FLOAT           1          // PrtFloat
#define PRT_CHARS           1          // PrtChars, PrtPage, PrtSeparator...
//#define PRT_DEBUG           1          // tiskni vsechny platne cislice bez ohledu na sirku

//-----------------------------------------------------------------------------

#define uDelay( us)  \
                 {byte __count; __count = ((us) * 3) / 2; while( --__count);}

//-----------------------------------------------------------------------------

#endif // __Hardware_H__
