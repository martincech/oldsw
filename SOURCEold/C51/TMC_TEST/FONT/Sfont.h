//*****************************************************************************
//
//    Sfont.h       Scalable Font services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __SFont_H__
   #define __SFont_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

void ZnakTahoma18( word x, byte y, char ch);

void VetaTahoma18( word x, byte y, char code *s);

word SirkaTahoma18( char code *s);

void VymazPosledniSloupec();

#endif
