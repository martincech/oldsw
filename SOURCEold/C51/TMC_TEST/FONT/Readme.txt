   Poznamky k realizaci TMC


1. na zapinani a vypinani majaku je urceno makro TmcMajak( On)
2. na zapinani a vypinani podsvitu je urceno makro DisplayPodsvit( On)
3. v modulu Adio vyhodit funkci ThermoPrepareInputs a zpracovani DV8
4. ve funkci DinRead pouzivat konstanty TmcDIx
5. ve funkci DoutWrite pouzivat konstanty TmcDVYx
6. obsluha tiskarny je v modulu Print/Printer

--------------------------------------------------------------------------
nova struktura projektu :

moduly obsluhujici HW (BIOS) maji include Hardware.h
tak, jak to bylo

parametrizovane SW moduly (napr. Fifo) maji include Project.h
(puvodne Hardware.h). V souboru Project.h (je v lokalnim
adresari stejne jako Hardware.h), bude include Hardware.h
a include vsech headeru, ktere jsou potrebne na parametrizaci
modulu.

moduly lokalni v projektu maji svuj zdrojovy text a header
s datovymi strukturami. Neexistuje zadny spolecny header
(Auto.h, Bat2.h), kde je definovano skoro vsechno.

Moduly, ktere budou prekladany v Borlandu
(resp. jejich header), musi pouzivat datove typy

byte, word, dword, qword, int8, int16, int32, int64

(viz uni.h) aby se nemusely kopirovat do adresaru Builderu
a opravovat (!)





