//*****************************************************************************
//
//    Text.c - graphic text services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Graphic.h"
#include "GIntern.h"   // Graphic internals
#include "Font.h"
#include "Bcd.h"       // char2dec
#include "Fonts.h"     // project fonts

#ifdef GRAPHIC_CONIO
   #include "conio.h"  // console
#endif

typedef enum {
   PITCH_NORMAL,
   PITCH_FIXED,
   PITCH_NUMERIC
} TPitchMode;

static TFontDescriptor const *Font;    // pracovni font
static int _x, _y;                     // souradnice textu
static byte _PitchMode;                // override pitch (TPitchMode)
#ifdef GRAPHIC_TEXT_INDENTATION
   static int _xParagraph;             // souradnice odstavce
#endif

// Lokalni funkce :

static int GetIndex( int c);
// Najde index zacatku znaku ve fontu

//-----------------------------------------------------------------------------
// Font
//-----------------------------------------------------------------------------

void GSetFont( int FontIndex)
// Nastavi pracovni font
{
   Font = Fonts[ FontIndex];
   _PitchMode = PITCH_NORMAL;
} // GSetFont

//-----------------------------------------------------------------------------
// Pitch
//-----------------------------------------------------------------------------

void GSetFixedPitch( void)
// Nastavi neproporcionalni pismo
{
   _PitchMode = PITCH_FIXED;
} // GSetFixedPitch

void GSetNumericPitch( void)
// Set nonproportional font with numeric spacing
{
   _PitchMode = PITCH_NUMERIC;
} // GSetNumericPitch

void GSetNormalPitch( void)
// Povoli proporcionalni pismo
{
   _PitchMode = PITCH_NORMAL;
} // GSetNormalPitch

//-----------------------------------------------------------------------------
// Pozice
//-----------------------------------------------------------------------------

void GTextAt( int x, int y)
// Levy horni roh textu
{
   _x = x;
   _y = y;
#ifdef GRAPHIC_TEXT_INDENTATION
   _xParagraph = x;             // souradnice odstavce
#endif
} // GTextAt

//-----------------------------------------------------------------------------
// Max. Sirka
//-----------------------------------------------------------------------------

int GCharWidth( void)
// Vrati maximalni sirku znaku
{
   return( Font->Width + Font->HSpace);
} // GCharWidth

//-----------------------------------------------------------------------------
// Max. Sirka cislic
//-----------------------------------------------------------------------------

int GNumberWidth( void)
// Returns maximal number width
{
   return( Font->NumWidth + Font->HSpace);
} // GNumberWidth

//-----------------------------------------------------------------------------
// Max. Vyska
//-----------------------------------------------------------------------------

int GCharHeight( void)
// Vrati maximalni vysku znaku
{
   return( Font->Height + Font->VSpace);
} // GCharHeight

//-----------------------------------------------------------------------------
// Sirka pismene
//-----------------------------------------------------------------------------

int GLetterWidth( int c)
// Returns letter width
{
int Index;
int Width;

   Index  = GetIndex( c);
   if( Font->Type == FONT_FIXED){
      Width = Font->Width;
   } else {
      Width = Font->Array[ Index++];
   }
   return( Width + Font->HSpace);
} // GLetterWidth

#ifndef GRAPHIC_TEXT_INDENTATION
//-----------------------------------------------------------------------------
// Sirka
//-----------------------------------------------------------------------------

int GTextWidth( const char *Text)
// Vrati sirku textu v pixelech
{
int Width;

   if( !*Text){
      return( 0);                      // empty string
   }
   Width = 0;
   while( *Text && *Text != '\n'){     // az dokonce nebo po prvni zlom radku
      if( Font->Type == FONT_FIXED){
         Width += Font->Width;
      } else {
         Width += Font->Array[ GetIndex( *Text)];
      }
      Width += Font->HSpace;           // horizontalni posun
      Text++;
   }
   return( Width);
} // GTextWidth

//-----------------------------------------------------------------------------
// Vyska
//-----------------------------------------------------------------------------

int GTextHeight( const char *Text)
// Vrati vysku textu v pixelech
{
   return( Font->Height + Font->VSpace);
} // GTextHeight

#else // GRAPHIC_TEXT_INDENTATION

//-----------------------------------------------------------------------------
// Sirka
//-----------------------------------------------------------------------------

int GTextWidth( const char *Text)
// Vrati sirku textu v pixelech
{
int Width;
int MaxWidth;

   if( !*Text){
      return( 0);                      // empty string
   }
   MaxWidth = 0;
   do {
      Width = 0;
      while( *Text && *Text != '\n'){  // az dokonce nebo po prvni zlom radku
         if( Font->Type == FONT_FIXED){
            Width += Font->Width;
         } else {
            Width += Font->Array[ GetIndex( *Text)];
         }
         Width += Font->HSpace;        // horizontalni posun
         Text++;
      }
      if( Width > MaxWidth){
         MaxWidth = Width;
      }
   } while( *Text++);
   return( MaxWidth);
} // GTextWidth

//-----------------------------------------------------------------------------
// Vyska
//-----------------------------------------------------------------------------

int GTextHeight( const char *Text)
// Vrati vysku textu v pixelech
{
int Rows;

   if( !*Text){
      return( 0);                      // empty string
   }
   Rows = 1;
   while( *Text){
      if( *Text == '\n'){
         Rows++;
      }
      Text++;
   }
   return( Font->Height * Rows + Font->VSpace * (Rows - 1));
} // GTextHeight

#endif // GRAPHIC_TEXT_INDENTATION

//-----------------------------------------------------------------------------
// Znak
//-----------------------------------------------------------------------------

int putchar( int c)
// standardni zapis znaku
{
int Index;
int Width;

   if( c == '\n'){
      // newline character
      _y += Font->Height + Font->VSpace;
#ifdef GRAPHIC_TEXT_INDENTATION
      _x = _xParagraph;             // souradnice odstavce
#else
      _x = 0;                       // zacatek noveho radku
#endif
      return( c);
   }
   Index  = GetIndex( c);
   if( Font->Type == FONT_FIXED){
      Width = Font->Width;
   } else {
      Width = Font->Array[ Index++];
   }
   // kopie bitmapy :
#ifdef GRAPHIC_LETTER_CENTERING
   if( _PitchMode == PITCH_FIXED){
      // move character at center of the field
      GBitBlt( _x + (Font->Width - Width) / 2, _y, Width, Font->Height, &Font->Array[ Index]);
   } else {
      GBitBlt( _x, _y, Width, Font->Height, &Font->Array[ Index]);
   }
#else
   GBitBlt( _x, _y, Width, Font->Height, &Font->Array[ Index]);
#endif
   if( _PitchMode == PITCH_NORMAL){
      _x += Width + Font->HSpace;                // proportional width
   } else if( _PitchMode == PITCH_FIXED){
      _x += Font->Width + Font->HSpace;          // fixed width
   } else {
      // numeric width
      if( c == '.' || c == ':'){
         _x += Width + Font->HSpace;             // separators with proportional width
      } else {
         _x += Font->NumWidth + Font->HSpace;    // numbers, signum, space : numeric width
      }
   }
   return( c);
} // putchar

//-----------------------------------------------------------------------------
// Index
//-----------------------------------------------------------------------------

static int GetIndex( int c)
// Najde index zacatku znaku ve fontu
{
int Index;

   Index  = c - Font->Offset;          // baze znakoveho fontu
   if( Font->Type == FONT_NUMERIC){
      // numericke fonty prekodovat :
      if( c >= '0' && c <= '9'){
         Index = char2dec( c);         // numericky znak
      } else {
         switch( c){
            case ' ' :
               Index = NCHAR_SPACE;
               break;
            case '-' :
               Index = NCHAR_MINUS;
               break;
            case '.' :
               Index = NCHAR_DOT;
               break;
            case ':' :
               Index = NCHAR_COLON;
               break;
            case '+' :
               Index = NCHAR_PLUS;
               break;
            default :
               Index = NCHAR_SPACE;
               break;
         }
      }
   }
   Index *= Font->Size;                // zacatek znaku
   return( Index);
} // GetIndex

#ifdef GRAPHIC_CONIO
//-----------------------------------------------------------------------------
// Kurzor
//-----------------------------------------------------------------------------

void ccursor( bool on)
// Je-li <on>=NO zhasne kurzor, jinak rozsviti
{
} // ccursor

//-----------------------------------------------------------------------------
// Mazani
//-----------------------------------------------------------------------------

void cclear( void)
// Smaze display
{
   GClear();
   GRedraw();
   cgoto( 0, 0);
} // cclear

//-----------------------------------------------------------------------------
// Pozice
//-----------------------------------------------------------------------------

void cgoto( int r, int c)
// Presune kurzor na zadanou textovou pozici <r> radek <c> sloupec
{
   GTextAt( c * Font->AvgWidth, r * (Font->Height + Font->VSpace));
} // cgoto

//-----------------------------------------------------------------------------
// Mazani radku
//-----------------------------------------------------------------------------

void cclreol( void)
// smaze az do konce radku, nemeni pozici kurzoru
{
int Color;
int Mode;

   // X max - s vyuzitim clippingu
   Color = _Color;
   Mode  = _Mode;
   GSetColor( COLOR_WHITE);
   GSetMode(  GMODE_REPLACE);
   GBox( _x, _y, G_WIDTH, Font->Height + Font->VSpace);
   GSetColor( Color);
   GSetMode( Mode);
#ifdef CONIO_FLUSH
   GFlush();
#endif
} // cclreol

#endif // GRAPHIC_CONIO
