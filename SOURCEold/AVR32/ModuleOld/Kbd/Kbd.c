//*****************************************************************************
//
//    Kbd.c -  Keyboard services code template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../inc/System.h"             // Operating system
#include "../inc/Cpu.h"                // WatchDog

word _KbdCounter = 0;                 // timer counter
word _KbdStatus  = 0;                 // context
word _KbdKey     = K_RELEASED;        // context key (getch)

// kbd counter context info :
#define KBD_IDLE         0x0000        // wait for first press
#define KBD_FIRST_PRESS  0x0001        // wait for stabilisation after first press
#define KBD_WAIT_REPEAT  0x0002        // wait for autorepeat
#define KBD_WAIT_RELEASE 0x0003        // stop autorepeat, wait for release

#define GetStatus()               _KbdStatus
#define GetCounter()              _KbdCounter
#define MkStatus( Code, Timeout)  _KbdStatus  = (Code);\
                                  _KbdCounter = ((Timeout) / TIMER_PERIOD)
#define MkIdle()                  _KbdStatus  = KBD_IDLE

// Local functions :

static int ReadKey( void);
// Returns key pressed

//-----------------------------------------------------------------------------
// Klavesa po zapnuti
//-----------------------------------------------------------------------------

int KbdPowerUpKey( void)
// Returns key holded after power up, or K_RELEASED
{
int Key;

   Key = ReadKey();
   SysDelay( 10);
   return( ReadKey());
} // KbdPowerUpKey

//-----------------------------------------------------------------------------
// Pusteni klavesy po zapnuti
//-----------------------------------------------------------------------------

void KbdPowerUpRelease( void)
// Wait for power up key release
{
   while( ReadKey() != K_RELEASED){
      WatchDog();            // wait for release
   }
} // KbdPowerUpRelease

//-----------------------------------------------------------------------------
// Test
//-----------------------------------------------------------------------------

TYesNo kbhit( void)
// Returns YES on key hit
{
   _KbdKey = KbdGet();
   if( _KbdKey == K_IDLE){
      return( NO);            // nothing pressed
   }
   return( YES);
} // kbhit

//-----------------------------------------------------------------------------
// Cekani na klavesu
//-----------------------------------------------------------------------------

int getch( void)
// Waits for key, returns it
{
int Key;

   if( _KbdKey == K_IDLE){
      // wait for press
      while( !kbhit()){
         WatchDog();
      }
   }
   Key = _KbdKey;                     // remember
   // wait for release :
   while( KbdGet() != K_RELEASED){
       WatchDog();
   }
   _KbdKey = K_IDLE;                  // release
   return( Key);
} // getch

//-----------------------------------------------------------------------------
// Wait release
//-----------------------------------------------------------------------------

void KbdDisable( void)
// Disable key, wait for release
{
   MkStatus( KBD_WAIT_RELEASE, 0);     // new context
} // KbdDisable

//-----------------------------------------------------------------------------
// Test
//-----------------------------------------------------------------------------

int KbdGet( void)
// Test for key, returns K_IDLE for no key pressed
{
int Key;

   Key = ReadKey();
   switch( GetStatus()){
      case KBD_IDLE :
         if( Key == K_RELEASED){
            return( K_IDLE);           // nothing pressed
         }
         MkStatus( KBD_FIRST_PRESS, KBD_DEBOUNCE);         // wait for key stabilisation
         return( K_IDLE);

      case KBD_FIRST_PRESS :
         if( GetCounter() != 0){
            return( K_IDLE);           // wait after first touch
         }
         if( Key == K_RELEASED){
            MkIdle();                  // wait for first touch
            return( K_IDLE);           // stabilisation not reached
         }
         MkStatus( KBD_WAIT_REPEAT, KBD_AUTOREPEAT_START); // wait for autorepeat
         return( Key);                 // valid key

      case KBD_WAIT_REPEAT :
         if( Key == K_RELEASED){
            MkIdle();                  // wait for first touch
            return( K_RELEASED);       // key release
         }
         if( GetCounter() != 0){
            return( K_IDLE);           // wait for autorepeat
         }
         // autorepeat start :
         MkStatus( KBD_WAIT_REPEAT, KBD_AUTOREPEAT_SPEED); // wait for next autorepeat
         return( Key | K_REPEAT);      // valid key

      case KBD_WAIT_RELEASE :
         if( Key == K_RELEASED){
            MkIdle();                  // wait for first touch
            return( K_RELEASED);       // key release
         }
         return( K_IDLE);
   }
   return( K_IDLE);                    // let be compiler happy
} // KbdGet
