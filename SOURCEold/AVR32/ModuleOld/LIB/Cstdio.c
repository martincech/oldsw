//*****************************************************************************
//
//    CStdio.c  - Standard input/output via UART
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Cstdio.h"
#include "Hardware.h"

#ifndef PUTCHAR_COM1
   #define __COM0__
#else
   #define __COM1__
#endif
#include "Com.h"

int ComPutchar( char c, FILE *stream);

FILE StdioStream = FDEV_SETUP_STREAM( ComPutchar, NULL, _FDEV_SETUP_WRITE);

//-----------------------------------------------------------------------------
// COM putchar
//-----------------------------------------------------------------------------

int ComPutchar( char c, FILE *stream)
{
   if( c == '\n'){
      ComTxChar( '\r');                // translate LF to CR+LF
   }
   ComTxChar( c);
   return( 0);
} // ComPutchar
