//*****************************************************************************
//
//    xprint.c     Simple formatting services template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"                  // conditional compilation
#include "xprint.h"
#include "Bcd.h"
#ifdef PRINTF_STRING
   #include "wgt/StrDef.h"
#endif

//-----------------------------------------------------------------------------
// String
//-----------------------------------------------------------------------------

void xputs( TPutchar *xputchar, const char *String)
// Print <String>
{
#ifdef PRINTF_STRING
   String = StrGet( String);
#endif
   while( *String){
      xputchar( *String);
      String++;
   }
} // xputs

//-----------------------------------------------------------------------------
// String with newline
//-----------------------------------------------------------------------------

void xputsn(  TPutchar *xputchar, const char *String)
// Print <String> up to LF
{
#ifdef PRINTF_STRING
   String = StrGet( String);
#endif
   while( *String && *String != '\n'){
      xputchar( *String);
      String++;
   }
} // xputsn

//-----------------------------------------------------------------------------
// Decimal number
//-----------------------------------------------------------------------------

void xprintdec( TPutchar *xputchar, int32 x, dword Flags)
// Print decimal number <x> with <Flags>
{
   if( !(Flags & FMT_UNSIGNED)){
      // with signum
      if( x < 0){
         Flags |= FMT_NEGATIVE;                  // add negative signum
         x = -x;
      }
   }
   x = dbin2bcd( x);
   xprinthex( xputchar, x, Flags);
} // xprintdec

//-----------------------------------------------------------------------------
// Hexadecimal number
//-----------------------------------------------------------------------------

void xprinthex( TPutchar *xputchar, dword x, dword Flags)
// Print hexadecimal number <x>, <Flags> is width
{
byte Nibble;
int  i;
int  Decimals;      // count after dot
int  Width;         // array width
int  Digits;        // valid numbers
int  Spaces;        // leading spaces

   Decimals = FmtGetDecimals( Flags);            // count after dot
   Width    = FmtGetWidth( Flags);               // total width
   if( Flags & FMT_LEADING_0){
      // valid numbers by total width
      Digits = Width;
      if( (Flags & FMT_NEGATIVE) || (Flags & FMT_PLUS)){
         Digits--;                               // position for signum
      }
      if( Decimals){
         Digits--;                               // position for dot
      }
   } else {
      Digits = BcdGetXWidth( x);                 // valid numbers
   }
   // correction 0. for float :
   if( Digits <= Decimals){
      Digits = Decimals + 1;                     // zero before dot + numbers after dot
   }
   // leading spaces :
   if((Flags & FMT_NEGATIVE) || (Flags & FMT_PLUS)){
      Spaces = Width - Digits - 1;               // position for dot
   } else {
      Spaces = Width - Digits;
   }
   if( Decimals){
      Spaces--;                                  // mandatory dot
   }
   if( Spaces < 0){
      Spaces = 0;                                // wrong total width
   }
   for( i = 0; i < Spaces; i++){
      xputchar( ' ');
   }
   if( Flags & FMT_NEGATIVE){
      xputchar( '-');
   } else if( Flags & FMT_PLUS){
      xputchar( '+');
   }
   x <<= (8 - Digits) * 4;                        // cut leading zeroes
   // nibble from MSB..LSB with Digits width :
   for( i = Digits; i > 0; i--){
      Nibble = (byte)(x >> 28);
      x <<= 4;
      if( Decimals == i){
         xputchar( '.');
      }
      xputchar( nibble2hex( Nibble));
   }
} // xprinthex

//-----------------------------------------------------------------------------
// Decimal with decimal dot
//-----------------------------------------------------------------------------

void xfloat( TPutchar *xputchar, dword x, int Width, int Decimals, dword Flags)
// Print <x> as float with total <Width>, <Decimals> and additional <Flags>
{
   xprintdec( xputchar, x, FmtPrecision( Width, Decimals) | Flags);
} // xfloat
