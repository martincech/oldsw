//*****************************************************************************
//
//    uconio.c     UART simple formatted output
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"           // podminena kompilace
#include "uconio.h"
#include "xprint.h"

#include <stdarg.h>

#ifdef UCONIO_COM0
   #define __COM0__
#endif
#ifdef UCONIO_COM1
   #define __COM1__
#endif
#ifdef UCONIO_COM2
   #define __COM2__
#endif
#ifdef UCONIO_COM3
   #define __COM3__
#endif
#include "../inc/Com.h"

// Local functions :

static void SendChar( int ch);
// sends character

//-----------------------------------------------------------------------------
// Character
//-----------------------------------------------------------------------------

void uputch( char ch)
// character output
{
   uputchar( ch);
} // uputch

//-----------------------------------------------------------------------------
// String
//-----------------------------------------------------------------------------

void uputs( const char *string)
// string output
{
   xputs( uputchar, string);
} // uputs

//-----------------------------------------------------------------------------
// Hexadecimal
//-----------------------------------------------------------------------------

void uprinthex( dword x, dword flags)
// hexadecimal output
{
   xprinthex( uputchar, x, flags);
} // uprinthex

//-----------------------------------------------------------------------------
// Decadic
//-----------------------------------------------------------------------------

void uprintdec( int32 x, dword flags)
// decadic number output
{
   xprintdec( uputchar, x, flags);
} // uprintdec

//-----------------------------------------------------------------------------
// Float
//-----------------------------------------------------------------------------

void ufloat( int x, int w, int d, dword flags)
// floating number output
{
   xfloat( uputchar, x, w, d, flags);
} // ufloat

//-----------------------------------------------------------------------------
// Formatted
//-----------------------------------------------------------------------------

void uprintf( const char *Format, ...)
// simple formatted output
{
va_list arg;

   va_start( arg, Format);
   xvprintf( uputchar, Format, arg);
} // uprintf

//------------------------------------------------------------------------------
// uputchar
//------------------------------------------------------------------------------

int uputchar( int ch)
// output character
{
   if( ch == '\n'){
      SendChar( '\r');                 // add carriage return
   }
   SendChar( ch);
   return( ch);   
} // uputchar

//------------------------------------------------------------------------------
// send
//------------------------------------------------------------------------------

static void SendChar( int ch)
// sends character
{
   while( ComTxBusy());
   ComTxChar( ch);
} // SendChar
