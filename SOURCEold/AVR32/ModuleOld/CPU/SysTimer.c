//******************************************************************************
//
//  SysTimer.c     System timer services
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "System.h"
#include "Cpu.h"

#define TIMER_COUNT ((TIMER_PERIOD * F_BUS_A / 8) / 1000)

static volatile TTimer _TimerTick;

static void __irq SysTimerHandler( void);
// Zpracovani preruseni od casovace

//------------------------------------------------------------------------------
//   Start timer
//------------------------------------------------------------------------------

void SysStartTimer( void)
// Start systemoveho casovace
{
   CpuIrqAttach( AVR32_TC_IRQ0, SYS_TIMER_PRIORITY, SysTimerHandler);
   AVR32_TC.channel[0].cmr = AVR32_TC_WAVSEL_UP_AUTO          << AVR32_TC_WAVSEL_OFFSET  | // Up mode with automatic reset
                             AVR32_TC_EEVTEDG_NO_EDGE         << AVR32_TC_EEVTEDG_OFFSET | // No external event
                             AVR32_TC_TCCLKS_TIMER_DIV3_CLOCK << AVR32_TC_TCCLKS_OFFSET;   // PBA/8
   AVR32_TC.channel[0].ier = 1 << AVR32_TC_CPCS_OFFSET;                  // enable compare interrupt
   AVR32_TC.channel[0].rc  = TIMER_COUNT;                                // set period
   AVR32_TC.channel[0].ccr = AVR32_TC_SWTRG_MASK | AVR32_TC_CLKEN_MASK;  // start
} // SysStartTimer

//------------------------------------------------------------------------------
//   Timer handler
//------------------------------------------------------------------------------

static void __irq SysTimerHandler( void)
// Zpracovani preruseni od casovace
{
   AVR32_TC.channel[0].sr;             // clear flag
   _TimerTick += TIMER_PERIOD;         // system timer
   SysTimerExecute();                  // user actions
} // SysTimerHandler

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

TTimer SysTime( void)
// Cte milisekundovy citac
{
   return( _TimerTick);
} // SysTime
