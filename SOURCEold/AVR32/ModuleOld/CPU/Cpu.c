//*****************************************************************************
//
//    Cpu.c        AVR32 CPU functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Cpu.h"
#include "System.h"
// framework
#include "cycle_counter.h"
#include "pm.h"
#include "intc.h"
#include "wdt.h"

//#include <Hardware.h>

//------------------------------------------------------------------------------
//   CPU initialisation
//------------------------------------------------------------------------------

void CpuInit( void)
// Cpu startup initialisation
{
   pm_switch_to_osc0( &AVR32_PM, F_XTAL, XTAL_STARTUP);
   INTC_init_interrupts();
} // CpuInit

//------------------------------------------------------------------------------
//   IRQ
//------------------------------------------------------------------------------

void CpuIrqAttach( int Irq, int Priority, TIrqHandler *Handler)
// Install IRQ <Handler> at <Irq> with <Priority> level
{
   INTC_register_interrupt( Handler, (dword)Irq, (dword)Priority);
} // CpuIrqAttach

//-----------------------------------------------------------------------------
// Watch dog
//-----------------------------------------------------------------------------

void WatchDogInit( void)
// Initialize watchdog
{
#ifdef WATCHDOG_ENABLE
   wdt_enable( WATCHDOG_INTERVAL * 1000);
#endif
} // WatchDogInit

void WatchDog( void)
// Refresh watchdog
{
#ifdef WATCHDOG_ENABLE
   wdt_clear();
#endif
} // WatchDog

//------------------------------------------------------------------------------
//   Delay ms
//------------------------------------------------------------------------------

void SysDelay( dword ms)
// Delay <ms>
{
   cpu_delay_ms( ms, F_CPU);
} // SysDelay

//------------------------------------------------------------------------------
//   Delay us
//------------------------------------------------------------------------------

void SysUDelay( dword us)
// Delay <us>
{
   cpu_delay_us( us, F_CPU);
} // SysUDelay
