//*****************************************************************************
//
//    Twi.c        TWI services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Twi.h"
#include "System.h"


#ifndef TWI_FREQUENCY
   #define TWI_FREQUENCY         100000   // TWI frequency [Hz]
#endif

#define TWI_ADDRESS_NONE  0               // without internal address

// Local functions :

static void TwiSetClock( void);
// Set clock dividiers

static TYesNo TwiWaitFor( unsigned Mask);
// Wait for any <Mask> bit set

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void TwiInit( void)
// Initialisation
{
unsigned Status;

   GpioFunction( AVR32_TWI_SDA_0_0_PIN, AVR32_TWI_SDA_0_0_FUNCTION);
   GpioFunction( AVR32_TWI_SCL_0_0_PIN, AVR32_TWI_SCL_0_0_FUNCTION);
   // external pullups 2 x 10k are mandatory,  internal pullup doesn't work
   // reset :
   AVR32_TWI.cr = AVR32_TWI_CR_SWRST_MASK;
   Status = AVR32_TWI.sr;                       // clear flags
   Status = AVR32_TWI.rhr;                      // clear RXRDY flag (see errata)
   TwiSetClock();
} // TwiInit

//-----------------------------------------------------------------------------
// Send byte
//-----------------------------------------------------------------------------

TYesNo TwiWriteByte( byte Chip, byte Data)
// Send <Data> to <Chip>
{
   return( TwiWriteTo( Chip, 0, TWI_ADDRESS_NONE, &Data, 1));
} // TwiWriteByte

//-----------------------------------------------------------------------------
// Receive byte
//-----------------------------------------------------------------------------

TYesNo TwiReadByte( byte Chip, byte *Data)
// Read <Data> from <Chip>
{
   return( TwiReadFrom( Chip, 0, TWI_ADDRESS_NONE, Data, 1));
} // TwiReadByte

//-----------------------------------------------------------------------------
// Write buffer
//-----------------------------------------------------------------------------

TYesNo TwiWrite( byte Chip, void *Data, int Size)
// Write <Data> with <Size> to <Chip>
{
   return( TwiWriteTo( Chip, 0, TWI_ADDRESS_NONE, Data, Size));
} // TwiWrite

//-----------------------------------------------------------------------------
// Read buffer
//-----------------------------------------------------------------------------

TYesNo TwiRead( byte Chip, void *Data, int Size)
// Read <Data> with <Size> from <Chip>
{
   return( TwiReadFrom( Chip, 0, TWI_ADDRESS_NONE, Data, Size));
} // TwiRead

//-----------------------------------------------------------------------------
// Write buffer to address
//-----------------------------------------------------------------------------

TYesNo TwiWriteTo( byte Chip, unsigned Address, int AddressSize, void *Data, int Size)
// Write <Data> with <Size> to <Chip> to internal <Address>
{
byte *p;

   Chip >>= 1;               // TWI DADR starts at LSB+1
   // set master mode :
   AVR32_TWI.cr   = AVR32_TWI_CR_MSEN_MASK | AVR32_TWI_CR_SVDIS_MASK;
   // direction = write, set chip address and internal address size :
   AVR32_TWI.mmr  = (0           << AVR32_TWI_MMR_MREAD_OFFSET) |
                    (Chip        << AVR32_TWI_MMR_DADR_OFFSET)  |
                    (AddressSize << AVR32_TWI_MMR_IADRSZ_OFFSET);
   // set internal address :
   AVR32_TWI.iadr = Address;
   // send data :
   p = (byte *)Data;
   while( Size--){
      AVR32_TWI.thr = *p;
      p++;
      // wait for THR empty :
      if( !TwiWaitFor( AVR32_TWI_SR_TXRDY_MASK)){
         return( NO);                  // NACK reply
      }
   }
   // wait for stop :
   TwiWaitFor( AVR32_TWI_SR_TXCOMP_MASK);
   // disable master :
   AVR32_TWI.cr = AVR32_TWI_CR_MSDIS_MASK;
   return( YES);
} // TwiWriteTo

//-----------------------------------------------------------------------------
// Read buffer from address
//-----------------------------------------------------------------------------

TYesNo TwiReadFrom( byte Chip, unsigned Address, int AddressSize, void *Data, int Size)
// Write <Data> with <Size> to <Chip> from internal <Address>
{
byte *p;
int  Count;

   Chip >>= 1;               // TWI DADR starts at LSB+1
   // set master mode :
   AVR32_TWI.cr   = AVR32_TWI_CR_MSEN_MASK | AVR32_TWI_CR_SVDIS_MASK;
   // direction = read, set chip address and internal address size :
   AVR32_TWI.mmr  = (1           << AVR32_TWI_MMR_MREAD_OFFSET) |
                    (Chip        << AVR32_TWI_MMR_DADR_OFFSET)  |
                    (AddressSize << AVR32_TWI_MMR_IADRSZ_OFFSET);
   // set internal address :
   AVR32_TWI.iadr = Address;
   // start receiving :
   if( Size == 1){
      // single byte only
      AVR32_TWI.cr   = AVR32_TWI_START_MASK | AVR32_TWI_STOP_MASK;  // with NACK reply
   } else {
      // multiple bytes
      AVR32_TWI.cr   = AVR32_TWI_START_MASK;
   }
   // receive data (but least) :
   p     = (byte *)Data;
   Count = Size - 1;
   while( Count--){
      // wait for RHR ready :
      if( !TwiWaitFor( AVR32_TWI_SR_RXRDY_MASK)){
         return( NO);                  // NACK reply
      }
     *p = AVR32_TWI.rhr;
      p++;
   }
   // receive last byte :
   if( Size != 1){
      AVR32_TWI.cr   = AVR32_TWI_STOP_MASK;      // with NACK reply
   }
   if( !TwiWaitFor( AVR32_TWI_SR_RXRDY_MASK)){
      return( NO);                               // NACK reply
   }
   *p = AVR32_TWI.rhr;
   // wait for stop :
   TwiWaitFor( AVR32_TWI_SR_TXCOMP_MASK);
   // disable master :
   AVR32_TWI.cr = AVR32_TWI_CR_MSDIS_MASK;
   return( YES);
} // TwiReadFrom

//*****************************************************************************

//-----------------------------------------------------------------------------
// Set clock
//-----------------------------------------------------------------------------

static void TwiSetClock( void)
// Set clock dividiers
{
unsigned Dividier;
unsigned Increment;

   Dividier  = 0;                      // no prescaler
   Increment = F_BUS_A / (TWI_FREQUENCY * 2) - 4;     // counter increment without prescaler
   // search for appropriate prescaler :
   while( Increment > 0xFF){
      if( Dividier >= 7){
         return;                      // error - unable set timing
      }
      Dividier++;                     // increase prescaler
      Increment /= 2;                 // reduce counter increment by prescaler
   }
   // set waveform generator data :
   AVR32_TWI.cwgr = ((Increment << AVR32_TWI_CWGR_CLDIV_OFFSET) |
                     (Increment << AVR32_TWI_CWGR_CHDIV_OFFSET) |
                     (Dividier  << AVR32_TWI_CWGR_CKDIV_OFFSET));
} // TwiSetClock

//-----------------------------------------------------------------------------
// Wait
//-----------------------------------------------------------------------------

static TYesNo TwiWaitFor( unsigned Mask)
// Wait for any <Mask> bit set
{
unsigned Status;

   forever {
      Status = AVR32_TWI.sr;
      if( Status & (AVR32_TWI_SR_NACK_MASK | AVR32_TWI_SR_OVRE_MASK)){
         return( NO);                  // NACK reply or RX overrun
      }
      if( Status & Mask){
         return( YES);                 // any mask bit set
      }
   }
   return( NO);                        // compiler only
} // TwiWaitFor
