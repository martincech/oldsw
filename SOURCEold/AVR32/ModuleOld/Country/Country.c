//******************************************************************************
//
//   Country.c     Country & locales utility
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include "Country.h"

#include "CountryTable.c"         // project specific country tables

#if LANGUAGE_COUNT > 1

static int SearchCodePage( int Language);
// Search code page by <Language>
#endif

//------------------------------------------------------------------------------
//  Load
//------------------------------------------------------------------------------

void CountryLoad( int Country)
// Fill country data by <Country> code
{
   CountryGet().Country  = Country;
   CountryGet().Language = _LocaleIndex[ Country].Language;
   CountryGet().Locale   = _Locales[ _LocaleIndex[ Country].Locale];
} // CountryLoad

//------------------------------------------------------------------------------
//  Language
//------------------------------------------------------------------------------

int CountryGetLanguage( void)
// Returns language code
{
#if LANGUAGE_COUNT > 1
   return( CountryGet().Language);
#else
   return( 0);
#endif
} // CountryGetLanguage

void CountrySetLanguage( int Language)
// Set <Language> and code page
{
#if LANGUAGE_COUNT > 1
   CountryGet().Language        = Language;
   CountryGet().Locale.CodePage = SearchCodePage( Language);
#endif
} // CountrySetLanguage

//------------------------------------------------------------------------------
//  Code Page
//------------------------------------------------------------------------------

int CountryGetCodePage( void)
// Returns code page
{
   return( CountryGet().Locale.CodePage);
} // CountryGetCodePage

//------------------------------------------------------------------------------
//  Date format
//------------------------------------------------------------------------------

int CountryGetDateFormat( void)
// Returns date format enum
{
   return( CountryGet().Locale.DateFormat);
} // CountryGetDateFormat

//------------------------------------------------------------------------------
//  Date Separator 1
//------------------------------------------------------------------------------

char CountryGetDateSeparator1( void)
// Returns date separator character
{
   return( CountryGet().Locale.DateSeparator1);
} // CountryGetDateSeparator1

//------------------------------------------------------------------------------
//  Date Separator 2
//------------------------------------------------------------------------------

char CountryGetDateSeparator2( void)
// Returns date separator character
{
   return( CountryGet().Locale.DateSeparator2);
} // CountryGetDateSeparator2

//------------------------------------------------------------------------------
//  Time format
//------------------------------------------------------------------------------

int CountryGetTimeFormat( void)
// Returns time format enum
{
   return( CountryGet().Locale.TimeFormat);
} // CountryGetTimeFormat

//------------------------------------------------------------------------------
//  Time separator
//------------------------------------------------------------------------------

char CountryGetTimeSeparator( void)
// Returns time separator character
{
   return( CountryGet().Locale.TimeSeparator);
} // CountryGetTimeSeparator

//------------------------------------------------------------------------------
//  DST
//------------------------------------------------------------------------------

int CountryGetDstType( void)
// Returns daylight saving type
{
   return( CountryGet().Locale.DstType);
} // CountryGetDstType

//******************************************************************************

#if LANGUAGE_COUNT > 1
//------------------------------------------------------------------------------
//  Search Code Page
//------------------------------------------------------------------------------

static int SearchCodePage( int Language)
// Search code page by <Language>
{
int i;

   for( i = 0; i < _COUNTRY_COUNT; i++){
      if( _LocaleIndex[ i].Language == Language){
         return( _Locales[ _LocaleIndex[ i].Locale].CodePage);
      }
   }
   return( 0);
} // SearchCodePage
#endif
