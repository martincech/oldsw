//*****************************************************************************
//
//    Rx8025Sys.c  -  Real time clock RX-8025 SA/NB system services
//    Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "Rtc.h"
#include "Iic.h"
#include "System.h"   // SysUDelay
#include "Bcd.h"      // BCD arihmetic
#include "Dt.h"       // date & time

// I2C Address :
#define RTC_BASE_ADDRESS  0x64                        // RTC base address
#define RTC_READ         (RTC_BASE_ADDRESS | 0x01)    // RTC read
#define RTC_WRITE         RTC_BASE_ADDRESS            // RTC write

//-----------------------------------------------------------------------------
// Register addresses
//-----------------------------------------------------------------------------

// upper nibble :
#define RTC_SEC_ADDRESS           0x00       // second BCD
#define RTC_MIN_ADDRESS           0x01       // minutes BCD
#define RTC_HOUR_ADDRESS          0x02       // hours BCD
#define RTC_WDAY_ADDRESS          0x03       // day of week BCD
#define RTC_DAY_ADDRESS           0x04       // day of month BCD
#define RTC_MONTH_ADDRESS         0x05       // month BCD
#define RTC_YEAR_ADDRESS          0x06       // year BCD

#define RTC_CLOCK_ADDRESS         RTC_SEC_ADDRESS     // first address of clock information
#define RTC_CLOCK_SIZE            7                   // size of clock information

#define RTC_OFFSET_ADDRESS        0x07       // digital offset - XTAL correction
#define RTC_ALARMW_MIN_ADDRESS    0x08       // Alarm W - minute
#define RTC_ALARMW_HOUR_ADDRESS   0x09       // Alarm W - hour
#define RTC_ALARMW_WDAY_ADDRESS   0x0A       // Alarm W - weekday
#define RTC_ALARMD_MIN_ADDRESS    0x0B       // Alarm D - minute
#define RTC_ALARMD_HOUR_ADDRESS   0x0C       // Alarm D - hour

#define RTC_CONTROL1_ADDRESS      0x0E       // Control 1
#define RTC_CONTROL2_ADDRESS      0x0F       // Control 2

#define RTC_SIMPLE_READ           0x04       // simple read (OR with address)

//-----------------------------------------------------------------------------
// Register masks
//-----------------------------------------------------------------------------

// Register CONTROL 1 :
#define RTC_INTA_OFF              0x00       // CT0-2 /INTA = HiZ
#define RTC_INTA_LOW              0x01       // CT0-2 /INTA = Fixed Low
#define RTC_INTA_PULSE_2HZ        0x02       // CT0-2 /INTA = Pulse mode 2 Hz
#define RTC_INTA_PULSE_1HZ        0x03       // CT0-2 /INTA = Pulse mode 1 Hz
#define RTC_INTA_LEVEL_SEC        0x04       // CT0-2 /INTA = Level mode 1 sec
#define RTC_INTA_LEVEL_MIN        0x05       // CT0-2 /INTA = Level mode 1 min
#define RTC_INTA_LEVEL_HOUR       0x06       // CT0-2 /INTA = Level mode 1 hour
#define RTC_INTA_LEVEL_MONTH      0x07       // CT0-2 /INTA = Level mode 1 month
#define RTC_CLEN2                 0x10       // /CLEN2 FOUT control
#define RTC_24                    0x20       // /12,24 24-hour clock
#define RTC_DALE                  0x40       // DALE alarm D enable
#define RTC_WALE                  0x80       // WALE alarm W enable

// Register CONTROL 2 :
#define RTC_DAFG                  0x01       // DAFG alarm D flag
#define RTC_WAFG                  0x02       // WAFG alarm W flag
#define RTC_CTFG                  0x04       // CTFG periodic interrupt flag
#define RTC_CLEN1                 0x08       // /CLEN1 FOUT control
#define RTC_PON                   0x10       // PON  1 - power on reset detected
#define RTC_XST                   0x20       // /XST 0 - oscillator stop detected
#define RTC_VDET                  0x40       // VDET 1 - power drop detected
#define RTC_VDSL_13               0x80       // VDSL set power drop treshold 1.3V (0 - 2.1V)

// Default values :
#define RTC_CONTROL1_DEFAULT     (RTC_INTA_OFF | RTC_24)
#define RTC_CONTROL2_DEFAULT     (RTC_XST)

// Local functions :

#ifdef RTC_USE_ALARM
static void RtcWriteRegister( byte Address, byte Value);
// Write <Value> at register <Address>

static byte RtcReadRegister( byte Address);
// Read register by <Address>
#endif // RTC_USE_ALARM

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

TYesNo RtcInit( void)
// Initialize bus and RTC
{
TYesNo Ack;

   IicInit();
   IicStart();
   Ack = IicSend( RTC_WRITE);
/*
   if( !Ack){
      IicStop();
      return( NO);                     // RTC doesn't respond
   }
*/
   // write control registers :
   Ack = IicSend( RTC_CONTROL1_ADDRESS << 4);
   Ack = IicSend( RTC_CONTROL1_DEFAULT);     // address CONTROL1
   Ack = IicSend( RTC_CONTROL2_DEFAULT);     // address CONTROL2
   IicStop();
   SysUDelay( 65);                     // stop delay
   return( YES);
} // RtcInit

//-----------------------------------------------------------------------------
// Load
//-----------------------------------------------------------------------------

TTimestamp RtcLoad( void)
// Returns RTC time
{
byte       Clock[ RTC_CLOCK_SIZE];
TLocalTime TmpTime;
TYesNo     Ack;

   IicStart();
   Ack = IicSend( RTC_WRITE);
   Ack = IicSend( RTC_CLOCK_ADDRESS << 4);
   // Read data :
   IicStart();
   Ack = IicSend( RTC_READ);
   IicRead( Clock, RTC_CLOCK_SIZE);
   IicStop();
   SysUDelay( 65);                     // stop delay
   // Decode data :
   TmpTime.Sec   = dbcd2bin( Clock[ RTC_SEC_ADDRESS]);
   TmpTime.Min   = dbcd2bin( Clock[ RTC_MIN_ADDRESS]);
   TmpTime.Hour  = dbcd2bin( Clock[ RTC_HOUR_ADDRESS]);
   TmpTime.Day   = dbcd2bin( Clock[ RTC_DAY_ADDRESS]);
   TmpTime.Month = dbcd2bin( Clock[ RTC_MONTH_ADDRESS]);
   TmpTime.Year  = dbcd2bin( Clock[ RTC_YEAR_ADDRESS]);
   return( DtCompose( &TmpTime));
} // RtcLoad

//-----------------------------------------------------------------------------
// Save
//-----------------------------------------------------------------------------

void RtcSave( TTimestamp Time)
// Set RTC time by <Time>
{
byte       Clock[ RTC_CLOCK_SIZE];
TLocalTime TmpTime;

   // Encode data :
   DtSplit( Time, &TmpTime);
   Clock[ RTC_SEC_ADDRESS]   = dbin2bcd( TmpTime.Sec);
   Clock[ RTC_MIN_ADDRESS]   = dbin2bcd( TmpTime.Min);
   Clock[ RTC_HOUR_ADDRESS]  = dbin2bcd( TmpTime.Hour);
   Clock[ RTC_WDAY_ADDRESS]  = dbin2bcd( (DtDow( Time) + 1) % 7);
   Clock[ RTC_DAY_ADDRESS]   = dbin2bcd( TmpTime.Day);
   Clock[ RTC_MONTH_ADDRESS] = dbin2bcd( TmpTime.Month);
   Clock[ RTC_YEAR_ADDRESS]  = dbin2bcd( TmpTime.Year);
   // Write data :
   IicStart();
   IicSend( RTC_WRITE);
   IicSend( RTC_CLOCK_ADDRESS << 4);
   IicWrite( Clock, RTC_CLOCK_SIZE);
   IicStop();
   SysUDelay( 65);                     // stop delay
} // RtcSave

#ifdef RTC_USE_ALARM
//-----------------------------------------------------------------------------
// Set alarm
//-----------------------------------------------------------------------------

void RtcSetAlarm( byte HourBcd, byte MinuteBcd)
// Set alarm at <HourBcd>:<MinuteBcd>
{
#ifdef RTC_RANGE_CHECK
   if( HourBcd > 0x23){
      HourBcd = 0x23;
   }
   if( MinuteBcd > 0x59){
      MinuteBcd = 0x59;
   }
#endif
   RtcWriteRegister( RTC_ALARMD_MIN_ADDRESS,  MinuteBcd);
   RtcWriteRegister( RTC_ALARMD_HOUR_ADDRESS, HourBcd);
   RtcWriteRegister( RTC_CONTROL1_ADDRESS, RTC_CONTROL1_DEFAULT | RTC_DALE); // povoleni
   RtcWriteRegister( RTC_CONTROL2_ADDRESS, RTC_CONTROL2_DEFAULT);            // mazani flagu
} // RtcSetAlarm

//-----------------------------------------------------------------------------
// Check alarm
//-----------------------------------------------------------------------------

TYesNo RtcCheckAlarm( void)
// Returns YES, if alarm active (and clear alarm)
{
   if( RtcReadRegister( RTC_CONTROL2_ADDRESS) & RTC_DAFG){
      RtcWriteRegister( RTC_CONTROL2_ADDRESS, RTC_CONTROL2_DEFAULT);         // mazani flagu
      return( YES);
   }
   return( NO);
} // RtcCheckAlarm

//-----------------------------------------------------------------------------
// Alarm off
//-----------------------------------------------------------------------------

void RtcAlarmOff( void)
// Alarm off
{
   RtcWriteRegister( RTC_CONTROL1_ADDRESS, RTC_CONTROL1_DEFAULT);            // zakaz
   RtcWriteRegister( RTC_CONTROL2_ADDRESS, RTC_CONTROL2_DEFAULT);            // mazani flagu
} // RtcAlarmOff

//-----------------------------------------------------------------------------
// Write register
//-----------------------------------------------------------------------------

static void RtcWriteRegister( byte Address, byte Value)
// Write <Value> at register <Address>
{
   IicStart();
   IicSend( RTC_WRITE);
   IicSend( Address << 4);
   IicSend( Value);
   IicStop();
   SysUDelay( 65);                     // stop delay
} // RtcWriteRegister

//-----------------------------------------------------------------------------
// Read register
//-----------------------------------------------------------------------------

static byte RtcReadRegister( byte Address)
// Read register by <Address>
{
byte Value;

   IicStart();
   IicSend( RTC_WRITE);
   IicSend( Address << 4);
   // Read data :
   IicStart();
   IicSend( RTC_READ);
   Value = IicReceive( NO);            // read single byte with NAK
   IicStop();
   SysUDelay( 65);                     // stop delay
   return( Value);
} // RtcReadRegister
#endif  // RTC_USE_ALARM
