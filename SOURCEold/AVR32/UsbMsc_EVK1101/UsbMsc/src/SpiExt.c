#include "spi.h"
#include "SpiExt.h"  // Extended SPI library
#include "System.h"
#include "uconio.h"
byte *SpiTxPtr = NULL;
word SpiTxLength = 0;
word SpiTxCount = 0;

byte *SpiRxPtr = NULL;
word SpiRxLength = 0;
word SpiRxCount = 0;

void (* SpiRxCallback)(void) = NULL;
void (* SpiTxCallback)(void) = NULL;


void SpiRegisterRxCallback(void (* Callback)(void)) {
   SpiRxCallback = Callback;
}

void SpiRegisterTxCallback(void (* Callback)(void)) {
   SpiTxCallback = Callback;
}

__attribute__((__interrupt__)) void SpiISRTx(void)
{
   volatile dword SpiStatus = AVR32_SPI.sr;

   if((SpiStatus & (1 << AVR32_SPI_TDRE_OFFSET))) {
	  if(SpiTxCount < SpiTxLength) {
		 AVR32_SPI.tdr = *SpiTxPtr;
		 SpiTxCount++;
		 SpiTxPtr++;
	  } else {
		 SpiDisableTxInt(&AVR32_SPI);
	  }
   }
   
   if((SpiStatus & (1 << AVR32_SPI_NSSR_OFFSET))) {
	  SpiDisableTxInt(&AVR32_SPI);
	  SpiTxCallback();
   }
   
   //co kdy� master data v�bec nedo�te, zasekne se a nech� CS = 0? Budeme po��d busy?
}

__attribute__((__interrupt__)) void SpiISRRx(void)
{
   volatile dword SpiStatus = AVR32_SPI.sr;
   
   if((SpiStatus & (1 << AVR32_SPI_RDRF_OFFSET))) {
	  if(SpiRxCount < SpiRxLength) {
	     *SpiRxPtr = AVR32_SPI.rdr;
	     SpiRxCount++;
	     SpiRxPtr++;
	  } else {
		 SpiDisableRxInt(&AVR32_SPI);
	  }
   }
   
   if((SpiStatus & (1 << AVR32_SPI_NSSR_OFFSET))) {
	  SpiDisableRxInt(&AVR32_SPI);
	  SpiRxCallback();
   }
}

word SpiGetRxCount(void) {
	return SpiRxCount;
}

void SpiTx(volatile avr32_spi_t *spi, byte *Buffer, word Length) {
   SpiDisableRxInt(spi); 
   
   SpiTxLength = Length;
   SpiTxPtr = Buffer;
   SpiTxCount = 0;
   
   INTC_register_interrupt((__int_handler) (&SpiISRTx), AVR32_SPI_IRQ, AVR32_INTC_INT1);
   
   SpiEnableTxInt(spi); 
}

void SpiRx(volatile avr32_spi_t *spi, byte *Buffer, word Length) {
   SpiDisableTxInt(spi); 
   
   SpiRxLength = Length;
   SpiRxPtr = Buffer;
   SpiRxCount = 0;
   
   // read pending char
   (void) spi->sr;
   (void) spi->rdr;
   
   INTC_register_interrupt((__int_handler) (&SpiISRRx), AVR32_SPI_IRQ, AVR32_INTC_INT1);

   SpiEnableRxInt(spi); 
}



// vylep�it - zat�mco nad�azen� vrstva zpracov�v� p�ijat� byte, SPI m��e p�ij�mat dal��
TYesNo SpiReadChar(volatile avr32_spi_t *spi, unsigned short *data) {
   unsigned int timeout = SPI_TIMEOUT;

   while (!(spi->sr & AVR32_SPI_SR_TDRE_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   spi->tdr = 0xFF << AVR32_SPI_TDR_TD_OFFSET;

   while((spi->sr & AVR32_SPI_SR_TXEMPTY_MASK) != 0);
   
   timeout = SPI_TIMEOUT;

   while((spi->sr & (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) !=
         (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   *data = spi->rdr >> AVR32_SPI_RDR_RD_OFFSET;

   return YES;
}