#ifndef _EFS_TRANS_SLAVE_H_
#define _EFS_TRANS_SLAVE_H_

#include "System.h"
#include "EfsTransDef.h"

void EfsTransInit(void);
void EfsTransReceive(void);
void EfsTransTransmit(word size);
TYesNo EfsTransDataIsReady(void);
word EfsTransGetDataSize(void);
byte* EfsTransGetData(void);
byte* EfsTransGetTxBuffer(void);
TYesNo EfsTransTransmitIsComplete(void);
TYesNo EfsWaitForTransmitComplete(void);
#endif