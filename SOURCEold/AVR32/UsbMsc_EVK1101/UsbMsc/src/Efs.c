#include "System.h"

#include "Efs.h"
#include "EfsTransSlave.h" // transportn� vrstva

#include <string.h> // memcpy

#include "uconio.h"

#include "navigation.h"
#include "file.h"
#include "fat.h"


#define FILL_FILE_NB_WRITE 855L
#define FILL_FILE_BUF_SIZE 120L
TYesNo fill_file_fast( void )
{
const UNICODE _MEM_TYPE_SLOW_ name[50]={'l','o','g','_','f','a','s','t','.','t','x','t',0};
_MEM_TYPE_SLOW_ Fs_file_segment g_recorder_seg;
byte g_trans_buffer[FILL_FILE_BUF_SIZE];
U16 u16_nb_write;
memset( g_trans_buffer , 0x55 , FILL_FILE_BUF_SIZE );

if( !nav_file_create( (const FS_STRING) name )) // Create file
return NO;
if( !file_open(FOPEN_MODE_W)) // Open file in write mode and forces the file size to 0
return NO;
// Define the size of segment to prealloc (unit 512B)
// Note: you can alloc more in case of you don't know total size
g_recorder_seg.u16_size = (FILL_FILE_NB_WRITE*FILL_FILE_BUF_SIZE + 512L)/512L;
// ****PREALLLOC****** the segment to fill
if( !file_write( &g_recorder_seg ))
{
file_close();
return NO;
}
// Check the size of segment allocated
if( g_recorder_seg.u16_size < ((FILL_FILE_NB_WRITE*FILL_FILE_BUF_SIZE + 512L)/512L) )
{
file_close();
return NO;
}
// Close/open file to reset size
file_close(); // Closes file. This routine don't remove the previous allocation.
if( !file_open(FOPEN_MODE_W)) // Opens file in write mode and forces the file size to 0
return NO;
for( u16_nb_write=0; u16_nb_write<FILL_FILE_NB_WRITE; u16_nb_write++ )
{
// HERE, the file cluster list is already allocated and the write routine is faster.
if( !file_write_buf( g_trans_buffer , FILL_FILE_BUF_SIZE ))
{
file_close();
return NO;
}
}
file_close();
return YES;
}
/*
byte EfsFileOpen(byte *FileName, byte Mode) {
   fill_file_fast();

   return EFS_STAT_OK;
}*/



byte EfsFileOpen(byte *FileName, byte Mode) {
   // not valid path
   if(FileName[0] == '\0') {
	   return EFS_STAT_ERR;
   }
   
   // select file
   if(!nav_setcwd((FS_STRING) FileName, true, false)) {
      return EFS_STAT_ERR;
   }

   // open file
   if(!file_open(Mode)) {
	   return EFS_STAT_ERR;
   }

   return EFS_STAT_OK;
}

byte EfsFileClose(void)
{ 
   file_close();

   return EFS_STAT_OK;
}

byte EfsFileWrite(byte *Data, word Count, word *Written) {
   Written = 0;

   if(0) { // kdy� nebyl otev�en soubor
      return EFS_STAT_ERR;
   }

   if(!file_write_buf(Data, Count)) {
      return EFS_STAT_ERR;
   }
   
   Written = Count;
   
   return EFS_STAT_OK;
}

void EfsInit(void) {
	EfsTransInit();
}

byte EfsFileSeek(dword Pos, byte Whence) {
   if(file_seek(Pos, Whence) == false) {
      return EFS_STAT_ERR;
   }
   
   return EFS_STAT_OK;
}

#define FILL_FILE_NB_WRITE 855L
#define FILL_FILE_BUF_SIZE 120L

byte EfsFileCreate(byte *FileName, dword Size) {
   _MEM_TYPE_SLOW_ Fs_file_segment g_recorder_seg;

   if( !nav_file_create( (const FS_STRING) FileName )) // Create file
      return EFS_STAT_ERR;
   
   if(Size == 0) {
	   return EFS_STAT_OK;
   }
   
   if( !file_open(FOPEN_MODE_W)) // Open file in write mode and forces the file size to 0
      return EFS_STAT_ERR;
   
   // Define the size of segment to prealloc (unit 512B)
   // Note: you can alloc more in case of you don't know total size
   g_recorder_seg.u16_size = (Size + 512L)/512L;
   
   // ****PREALLLOC****** the segment to fill
   if( !file_write( &g_recorder_seg )) {
      file_close();
      return EFS_STAT_ERR;
   }
   
   // Check the size of segment allocated
   if( g_recorder_seg.u16_size < ((Size + 512L)/512L) ) {
      file_close();
      return EFS_STAT_ERR;
   }

   file_close();
   
   return EFS_STAT_OK;
}



byte EfsFileDelete(byte *FileName) {
   Fs_index sav_index;
   
   if(FileName[0] == '\0') {
	   return EFS_STAT_ERR;
   }
   
   // Save the position
   sav_index = nav_getindex();
   
   while( 1 )
   {
      // Restore the position
      nav_gotoindex(&sav_index);
	  
      // Select file or directory
      if(!nav_setcwd((FS_STRING) FileName, true, false)) {
         break;
	  }
		 
      // Delete file or directory
      if(!nav_file_del(false)) {
         return EFS_STAT_ERR;
      }
   }

   return EFS_STAT_OK;
}