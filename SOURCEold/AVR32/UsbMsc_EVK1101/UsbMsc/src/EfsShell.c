#include "System.h"

#include "Efs.h"
#include "EfsTransSlave.h" // transportní vrstva

#include "uconio.h"
#include "Hardware.h"


void EfsShellInit(void) {
	EfsTransInit();
	EfsTransReceive();
}

byte buf[100];


void EfsShellTask(void) {
   word CmdSize;
   EfsCmdUnion *Cmd;
   word ReplySize;
   EfsReplyUnion *Reply;

   if(EfsTransDataIsReady() == NO) {
	   return;
   }

   CmdSize = EfsTransGetDataSize();
   Cmd = (EfsCmdUnion *) EfsTransGetData();
   
   Reply = EfsTransGetTxBuffer();

   switch(Cmd->Cmd) {
      case EFS_CMD_FILEOPEN:
         Reply->Reply = EfsFileOpen(Cmd->FileOpen.FileName, Cmd->FileOpen.Mode);
         ReplySize = sizeof(Reply->FileOpen);
		 break;
		 
      case EFS_CMD_FILECLOSE:
         Reply->Reply = EfsFileClose();
         ReplySize = sizeof(Reply->FileClose);
		 break;
		 		 
      case EFS_CMD_FILEWRITE:
	     StatusLed0Tog();
         Reply->Reply = EfsFileWrite(Cmd->FileWrite.Data, Cmd->FileWrite.Count, &(Reply->FileWrite.Written));
         StatusLed0Tog();
		 ReplySize = sizeof(Reply->FileWrite);
		 StatusLed0Tog();
		 break;

      case EFS_CMD_FILESEEK:
         Reply->Reply = EfsFileSeek(Cmd->FileSeek.Pos, Cmd->FileSeek.Whence);
         ReplySize = sizeof(Reply->FileSeek);
		 break;

      case EFS_CMD_FILECREATE:
         Reply->Reply = EfsFileCreate(Cmd->FileCreate.FileName, Cmd->FileCreate.Size);
         ReplySize = sizeof(Reply->FileSeek);
		 break;

      case EFS_CMD_FILEDELETE:
         Reply->Reply = EfsFileDelete(Cmd->FileDelete.FileName);
         ReplySize = sizeof(Reply->FileSeek);
		 break;
		 	 
      default:
	     Reply->Reply = EFS_STAT_ERR;
		 ReplySize = 1;
		 break;
   }

   EfsTransTransmit(ReplySize);

   if(EfsWaitForTransmitComplete() == YES) {
      
   }
   
   EfsTransReceive();
}