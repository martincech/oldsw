//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
//#include "Cpu.h"       // CPU startup
//#include "System.h"    // Operating system
#include "sleepmgr.h";
#include "sysclk.h";
#include "udc.h";

int main (void)
{
   //CpuInit();
   //WatchDogInit();
   
   // Authorize interrupts
irq_initialize_vectors();
cpu_irq_enable();
// Initialize the sleep manager service
sleepmgr_init();
// Initialize the clock service
sysclk_init();
// Enable USB stack device
udc_start();




   while(1) {

   }  
}



byte HidEnable() {
   return 1;
}

void HidDisable() {

}


void HidPlugged(byte b_vbus_high) {
   if (b_vbus_high) {
      // Connect USB device
      udc_attach();
   } else {
      // Disconnect USB device
      udc_detach();
   }
}