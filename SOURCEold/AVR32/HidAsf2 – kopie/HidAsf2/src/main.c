/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * AVR Software Framework (ASF).
 */
#include <asf.h>
#include "sleepmgr.h";
#include "sysclk.h";
#include "udc.h";

int main (void)
{
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();

	sysclk_init();

	// Start USB stack to authorize VBus monitoring
	udc_start();

	/*if (!udc_include_vbus_monitoring()) {
		// VBUS monitoring is not available on this product
		// thereby VBUS has to be considered as present
		main_vbus_action(true);
	}*/

	// The main loop manages only the power mode
	// because the USB management is done by interrupt
	while (true) {}
}



U8 HidEnable() {
   return 1;
}

void HidDisable() {

}


void HidPlugged(U8 b_vbus_high) {
   if (b_vbus_high) {
      // Connect USB device
      udc_attach();
   } else {
      // Disconnect USB device
      udc_detach();
   }
}