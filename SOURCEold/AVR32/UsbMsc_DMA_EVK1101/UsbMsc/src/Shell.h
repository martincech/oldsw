//*****************************************************************************
//
//    Shell.c   File Access Shell over SPI
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef _SHELL_H_
#define _SHELL_H_

void ShellScanCmd(void);
void ShellInit(void);


byte ShellReadBufferRx(word pos);
byte ShellReadBufferTx(word pos);
#endif