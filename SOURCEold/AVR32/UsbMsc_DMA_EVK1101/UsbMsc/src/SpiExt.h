//*****************************************************************************
//
//    SpiExt.h   SPI extended
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef _SPI_EXT_H_
#define _SPI_EXT_H_

#include "spi.h"
#include "System.h"

volatile byte SpiTxComplete;

volatile byte SpiRxIntEnabled;

//-----------------------------------------------------------------------------
// SpiEnableRxInt
//-----------------------------------------------------------------------------

void SpiEnableRxInt(volatile avr32_spi_t *spi);

//-----------------------------------------------------------------------------
// SpiEnableTxInt
//-----------------------------------------------------------------------------

#define SpiEnableTxInt(spi) ((spi)->ier = (1 << AVR32_SPI_TDRE_OFFSET))

//-----------------------------------------------------------------------------
// SpiDisableRxInt
//-----------------------------------------------------------------------------

#define SpiDisableRxInt(spi) ((spi)->idr = (1 << AVR32_SPI_RDRF_OFFSET));\
                              SpiRxIntEnabled = NO;

//-----------------------------------------------------------------------------
// SpiDisableTxInt
//-----------------------------------------------------------------------------

#define SpiDisableTxInt(spi) ((spi)->idr = (1 << AVR32_SPI_TDRE_OFFSET))


void SpiStartTransmit(volatile avr32_spi_t *spi, byte *buffer, word length);
#define SpiStopTransmit(x)    SpiDisableTxInt(x);\
                              SpiTxComplete = YES;
#define SpiStartReceive(x)    SpiEnableRxInt(x);
#define SpiStopReceive(x)     SpiDisableRxInt(x);

__attribute__((__interrupt__)) void SpiISR(void);

TYesNo SpiBufferIsEmpty(void);
TYesNo SpiReadBufferChar(byte *ch);
word SpiReadBuffer(byte *buffer, word length);
void SpiFlushBuffer(void);
TYesNo SpiWaitForTransmitComplete(volatile avr32_spi_t *spi);

volatile void (*SPICallbackRx)(void);
volatile void (*SPICallbackTx)(void);

#define SPIRegisterCallbackRx(x)   (SPICallbackRx = x) 
#define SPIRegisterCallbackTx(x)   (SPICallbackTx = x) 

#define SPIUnregisterCallbackRx()   (SPICallbackRx = NULL)
#define SPIUnregisterCallbackTx()   (SPICallbackTx = NULL)






byte SpiRB(byte pos);

#endif