//*****************************************************************************
//
//    Shell.c   File Access Shell over SPI
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include <string.h>  
#include "Hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"    // UART printf
#include "ushell_task.h"    // UART printf !!!!! smazat
#include "navigation.h" // File interface
#include "file.h" // File interface

#include "spi.h"     // Spi

#include "Shell.h"
#include "pdca.h"


#define SHELL_DMA_CHANNEL_RX   0
#define SHELL_DMA_CHANNEL_TX   1

#define SHELL_BUFFER_LENGTH   257

byte ShellBufferTx1[SHELL_BUFFER_LENGTH];
byte ShellBufferTx2[SHELL_BUFFER_LENGTH];
byte ShellBufferRx[2*SHELL_BUFFER_LENGTH];

volatile TYesNo ShellBufferTx1Full = NO;
volatile TYesNo ShellBufferTx2Full = NO;

volatile byte *ShellBufferTxWrite = ShellBufferTx1;
volatile byte *ShellBufferTxOut = NULL;
volatile word ShellBufferTxOutLength = 0;


   pdca_channel_options_t DMARxChannel = {
	   .addr = ShellBufferRx,
	   .size = SHELL_BUFFER_LENGTH*2,
	   .r_addr = ShellBufferRx,
	   .r_size = SHELL_BUFFER_LENGTH*2,
	   .pid = AVR32_PDCA_PID_SPI_RX,
	   .transfer_size = AVR32_PDCA_BYTE
   };

   pdca_channel_options_t DMATxChannel = {
	   .addr = NULL,
	   .size = 0,
	   .r_addr = NULL,
	   .r_size = 0,
	   .pid = AVR32_PDCA_PID_SPI_TX,
	   .transfer_size = AVR32_PDCA_BYTE
   };
   


//------------------------------------------------------------------------------
// CMD static functions
//------------------------------------------------------------------------------

static byte ShellCmdDisk(void);
static byte ShellCmdDF(void);
static byte ShellCmdDir(void);
static byte ShellCmdCd(void);
static byte ShellCmdFileOpen(void);
static byte ShellCmdFileClose(void);
static byte ShellCmdFileRead(void);
static byte ShellCmdFileWrite(void);
static byte ShellCmdMkdir(void);
static byte ShellCmdRmdir(void);
static byte ShellCmdRename(void);
static byte ShellCmdFileSeek(void);
static byte ShellCmdTouch(void);

//------------------------------------------------------------------------------
// Codes
//------------------------------------------------------------------------------

#define SHELL_HOST_ERR_OK               0x00
#define SHELL_HOST_ERR_BAD_CMD          0x77
#define SHELL_HOST_ERR_EXEC_ERROR       0x78
#define SHELL_HOST_ERR_NO_DRIVE         0x79
#define SHELL_HOST_ERR_DRIVE_ERROR      0x80
#define SHELL_HOST_ERR_DIR_ERROR        0x81
#define SHELL_HOST_ERR_NODIR_ERROR      0x82
#define SHELL_HOST_ERR_FILE_ERR         0x83
#define SHELL_HOST_ERR_WRITE_ERROR      0x84

//------------------------------------------------------------------------------
// Commands
//------------------------------------------------------------------------------

#define SHELL_CMD_DISK        0x01
#define SHELL_CMD_DF          0x02
#define SHELL_CMD_DIR         0x03
#define SHELL_CMD_CD          0x04
#define SHELL_CMD_FILE_OPEN   0x05
#define SHELL_CMD_FILE_CLOSE  0x06
#define SHELL_CMD_FILE_READ   0x07
#define SHELL_CMD_FILE_WRITE  0x08
#define SHELL_CMD_MKDIR       0x09
#define SHELL_CMD_RMDIR       0x0A
#define SHELL_CMD_RENAME      0x0B
#define SHELL_CMD_FILE_SEEK   0x0C
#define SHELL_CMD_TOUCH       0x0D

typedef const struct {
   byte cmd;               // CMD
   byte StaticLength;      // Static part of parameter length
   TYesNo VariableLength;  // Will be parameter of variable length present? 
   byte (*Function)(void)  // Command specific function
} TShellCmd;

// CMD structure	
TShellCmd ShellCommands[] = {
   { SHELL_CMD_DISK,        0, NO,  &ShellCmdDisk      },
   { SHELL_CMD_DF,          1, NO,  &ShellCmdDF        },
   { SHELL_CMD_DIR,         4, NO,  &ShellCmdDir       },
   { SHELL_CMD_CD,          0, YES, &ShellCmdCd        },
   { SHELL_CMD_FILE_OPEN,   1, YES, &ShellCmdFileOpen  },
   { SHELL_CMD_FILE_CLOSE,  0, NO,  &ShellCmdFileClose },
   { SHELL_CMD_FILE_READ,   4, NO,  &ShellCmdFileRead  },
   { SHELL_CMD_FILE_WRITE,  4, NO,  &ShellCmdFileWrite },
   { SHELL_CMD_MKDIR,       0, YES, &ShellCmdMkdir     },
   { SHELL_CMD_RMDIR,       0, YES, &ShellCmdRmdir     },
   { SHELL_CMD_RENAME,      0, YES, &ShellCmdRename    },
   { SHELL_CMD_FILE_SEEK,   5, NO,  &ShellCmdFileSeek  },
   { SHELL_CMD_TOUCH,       0, YES, &ShellCmdTouch     }
}; 

#define SHELL_COMMANDS_NB      (sizeof(ShellCommands)/sizeof(ShellCommands[0]))

//------------------------------------------------------------------------------
// Misc
//------------------------------------------------------------------------------

#define SPI_DEVICE          &AVR32_SPI

#define SHELL_CHAR_TIMEOUT           20000
#define SHELL_TRANSACTION_TIMEOUT    500000
#define SHELL_PARAMS_MAX_LENGTH      50
#define SHELL_CMD_DELIMITER          '\n'
#define SHELL_BLOCK_LENGTH           255 // nem��e b�t v�c ne� 255, proto�e informace o d�lce bloku je p�en�ena v jednom bytu

#if SHELL_BLOCK_LENGTH > 255
   #error "Data block length can't exceed 255 bytes"
#endif

byte ShellParamBuffer[SHELL_PARAMS_MAX_LENGTH + 1]; // last byte for null termination

//------------------------------------------------------------------------------
// Static functions
//------------------------------------------------------------------------------

static TYesNo ShellTransmitData(byte *buffer, byte length, TYesNo TransmitLength, TYesNo WillContinue);
static void ShellCallbackTx(void);
static void ShellCallbackRx(void);

//! @brief Appends the '\' char at the end of path
//!
// N�kter� funkce ji vy�aduj� - upravit
static void ushell_path_valid_synta( char *path )
{
   uint8_t u8_tmp;
   
   // Compute size of substitute
   for( u8_tmp=0; u8_tmp<MAX_FILE_PATH_LENGTH; u8_tmp++ )
   {
      if( path[u8_tmp]==0)
         break;
   }
   // Append the '\' char for the nav_setcwd to enter the found directory
   if ( path[u8_tmp-1] != '\\')
   {
      path[u8_tmp]='\\';
      path[u8_tmp+1]=0;
   }
}

//-----------------------------------------------------------------------------
// PDCAISR
//-----------------------------------------------------------------------------
__attribute__((__interrupt__)) void PDCAISR_RX(void)
{
   avr32_pdca_channel_t *channel = pdca_get_handler(SHELL_DMA_CHANNEL_RX);
   
   if((channel->isr & (1 << AVR32_PDCA_TRC_OFFSET))) {
	   uprintf("!Rx Reload!");
	   pdca_reload_channel(SHELL_DMA_CHANNEL_RX, &ShellBufferRx, 2*SHELL_BUFFER_LENGTH);
   } 
}

__attribute__((__interrupt__)) void PDCAISR_TX(void)
{
   word i;
   avr32_pdca_channel_t *channel = pdca_get_handler(SHELL_DMA_CHANNEL_TX);
   
   if((channel->isr & (1 << AVR32_PDCA_TRC_OFFSET))) {
	  ShellStatusBusy();
	   
	  if(ShellBufferTxOut == NULL) {
		 ShellBufferTx1Full = NO;
		 ShellBufferTx2Full = NO;
	     pdca_disable_interrupt_transfer_complete(SHELL_DMA_CHANNEL_TX);
		 
		 /*
		    bylo by dobr� zde �ekat na �pln� konec p�enosu, tj. testovat SPI, jestli
			p�enesla posledn� byte
		 */
		 
		 return;
	  }
	   
	  if(ShellBufferTxOut == ShellBufferTx1) {
		 ShellBufferTx2Full = NO;
		 
		 /*uprintf("Posilam %d bytu", ShellBufferTxOutLength);
		 
		 for(i = 0 ; i < ShellBufferTxOutLength ; i++) {
			 uprintf("%d ", ShellBufferTxOut[i]);
		 }
		 
		 uprintf("\n\n");*/
		 
		 pdca_reload_channel(SHELL_DMA_CHANNEL_TX, ShellBufferTxOut, ShellBufferTxOutLength); 
		 ShellStatusReady();
		 SysUDelay(10);
		 /*
		    P�i p�en�en� jen jednoho bytu ho DMA hned zkop�ruje do SPI a vyvol� nov� interrupt,
		    kter� okam�it� vr�t� status do Busy => kr�tk� doba trv�n� ready, kterou nemus� master zaznamenat
		 */

		   
	  } else {
		 ShellBufferTx1Full = NO;
		 
		 /*uprintf("Posilam %d bytu", ShellBufferTxOutLength);
		 
		 for(i = 0 ; i < ShellBufferTxOutLength ; i++) {
			 uprintf("%d ", ShellBufferTxOut[i]);
		 }
		 
		 uprintf("\n\n");*/
		 
		 pdca_reload_channel(SHELL_DMA_CHANNEL_TX, ShellBufferTxOut, ShellBufferTxOutLength); 
		 ShellStatusReady();
		 SysUDelay(10);
	   }
	   
	   ShellBufferTxOut = NULL;
	   
	   StatusLed1Tog();
   } 
}


/*
   P��jem znak� bude po��d zapnut�
   Vyskytl se probl�m - p�i op�tovn�m zapnut� p��jmu DMA na�etlo jeden znak, kter� se
   po posledn� transakci nach�zel v RX registru SPI
   Jeho p�e�ten� p��mo z SPI nepomohlo
*/


void ShellStartReceive(void) {
	/*(void) (SPI_DEVICE)->sr;
	(void) (SPI_DEVICE)->rdr;
	pdca_enable(SHELL_DMA_CHANNEL_RX);*/
}

void ShellStopReceive(void) {
	//pdca_disable(SHELL_DMA_CHANNEL_RX);
}


word ShellWriteBufferPtr = 0;
//-----------------------------------------------------------------------------
// ShellWriteBufferFreeSpace
//-----------------------------------------------------------------------------

word ShellWriteBufferFreeSpace(void) {
	return SHELL_BUFFER_LENGTH - ShellWriteBufferPtr;
}

//-----------------------------------------------------------------------------
// ShellWriteChar
//-----------------------------------------------------------------------------



static TYesNo ShellWriteChar(byte ch)
// reads char with timeout
{
   dword timeout = SHELL_TRANSACTION_TIMEOUT;
   
   if(ShellWriteBufferFreeSpace() == 0) {
      return NO;
   }
   
	ShellBufferTxWrite[ShellWriteBufferPtr++] = ch;

	if(ShellWriteBufferPtr >= SHELL_BUFFER_LENGTH) {
		ShellWriteBufferPtr = 0;
	}
	
	return YES;
}


static TYesNo ShellWrite(byte *buffer, word length)
// reads char with timeout
{
   word i;

   if(ShellWriteBufferFreeSpace() < length) {
	  return NO;
   }

   for(i = 0 ; i < length ; i++) {
	  ShellBufferTxWrite[ShellWriteBufferPtr++] = buffer[i];

	  if(ShellWriteBufferPtr >= SHELL_BUFFER_LENGTH) {
		 ShellWriteBufferPtr = 0;
	  }
   }

   return YES;
}

TYesNo ShellWaitForTxBufferEmpty() {
   dword timeout = SHELL_TRANSACTION_TIMEOUT;
	
   while(ShellBufferTx1Full == YES && ShellBufferTx2Full == YES) {
	  if(!timeout--) {
		 ShellBufferTxOut = NULL;
		 ShellBufferTx1Full == NO;
		 ShellBufferTx2Full == NO;
		 pdca_disable_interrupt_transfer_complete(SHELL_DMA_CHANNEL_TX);
		 return NO;
	  }
		
	  SysUDelay(1);
   }
	
   return YES;
}



TYesNo ShellWaitForTransmitComplete() {
   dword timeout = SHELL_TRANSACTION_TIMEOUT;
	
   while((ShellBufferTx1Full == YES || ShellBufferTx2Full == YES)) {
	  if(!timeout--) {
		 ShellBufferTxOut = NULL;
		 ShellBufferTx1Full == NO;
		 ShellBufferTx2Full == NO;
		 pdca_disable_interrupt_transfer_complete(SHELL_DMA_CHANNEL_TX);
		 return NO;
	  }
		
	  SysUDelay(1);
   }
	
   return YES;
}

//-----------------------------------------------------------------------------
// ShellTransmitData
//-----------------------------------------------------------------------------

// m� m�t shell vlastn� buffer, ze kter�ho jsou vys�l�na data, nebo jej p�esunout a� do SPI modulu?

static TYesNo ShellTransmitData(byte *buffer, byte length, TYesNo TransmitLength, TYesNo WillContinue)
{
   word timeout = SHELL_TRANSACTION_TIMEOUT;
   
   if(ShellWaitForTxBufferEmpty() == NO) {
      return NO;
   }

   ShellWriteChar(SHELL_HOST_ERR_OK);
   
   if(TransmitLength == YES) {
	   ShellWriteChar(length);
   }
   
   ShellWrite(buffer, length);
   
   ShellBufferTxOut = ShellBufferTxWrite;
   ShellBufferTxOutLength = length + 1 + TransmitLength;
   
   if(ShellBufferTxWrite == ShellBufferTx1) {
	   ShellBufferTx1Full = YES;
	   ShellBufferTxWrite = ShellBufferTx2;
   } else {
	   ShellBufferTx2Full = YES;
	   ShellBufferTxWrite = ShellBufferTx1;
   }

   ShellWriteBufferPtr = 0;

   pdca_enable_interrupt_transfer_complete(SHELL_DMA_CHANNEL_TX);

   if(WillContinue == NO) {
	   if(ShellWaitForTransmitComplete() == NO) {
	     return NO;
	   }
   }

   return YES;
}

//-----------------------------------------------------------------------------
// ShellTransmitError
//-----------------------------------------------------------------------------

static TYesNo ShellTransmitError(byte error)
{
   // wait for previous transaction
   return YES;

}

//-----------------------------------------------------------------------------
// ShellReadChar
//-----------------------------------------------------------------------------

word ShellReadBufferPtr = 0;

static TYesNo ShellReadChar(byte *ch)
// reads char with timeout
{
   word timeout = SHELL_CHAR_TIMEOUT;
   
   while(ShellReadBufferPtr == (2 * SHELL_BUFFER_LENGTH - pdca_get_load_size(SHELL_DMA_CHANNEL_RX))) { // wait for next char till timeout expires
	  if(!timeout--) { // timeout, error
	     return NO;
	  }
				   
	  SysUDelay(1);

	  continue;
	}
	
	*ch = ShellBufferRx[ShellReadBufferPtr++];
	
	if(ShellReadBufferPtr >= 2*SHELL_BUFFER_LENGTH) {
		ShellReadBufferPtr = 0;
	}
	
	return YES;
}

//-----------------------------------------------------------------------------
// ShellReadBufferEmpty
//-----------------------------------------------------------------------------

static TYesNo ShellReadBufferEmpty() {
	if(ShellReadBufferPtr == (2*SHELL_BUFFER_LENGTH - pdca_get_load_size(SHELL_DMA_CHANNEL_RX))) {
		return YES;
	}
	
	return NO;
}

//-----------------------------------------------------------------------------
// ShellFlushBuffer
//-----------------------------------------------------------------------------

static TYesNo ShellFlushBuffer() {
	pdca_disable(SHELL_DMA_CHANNEL_RX);
	pdca_load_channel(SHELL_DMA_CHANNEL_RX, &ShellBufferRx, 2*SHELL_BUFFER_LENGTH);
	pdca_enable(SHELL_DMA_CHANNEL_RX);
	
	ShellReadBufferPtr = 0;
}
//-----------------------------------------------------------------------------
// ShellCmdDisk
//-----------------------------------------------------------------------------

byte ShellCmdDisk(void)
{
   byte i;
   byte buffer[4];
   
   // drive count
   buffer[0] = nav_drive_nb();

   // drive names
   for(i = 0; i < buffer[0]; i++) {
	  buffer[i + 1] = 'a' + i;
   }

   // transmit data
   if(ShellTransmitData((byte *) buffer, i + 1, YES, NO) == NO) {
	  return SHELL_HOST_ERR_EXEC_ERROR;
   }

   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdDF
//-----------------------------------------------------------------------------

byte ShellCmdDF()
{
   qword buffer[2];
   Fs_index sav_index = nav_getindex(); // Save current position
   byte drive = ShellParamBuffer[0] - 'a';
   
   if(drive >= nav_drive_nb()) {
	  return SHELL_HOST_ERR_DRIVE_ERROR; 
   }

   nav_drive_set(drive); // Select drive
   
   if(!nav_partition_mount()) { // Mount drive
      return SHELL_HOST_ERR_DRIVE_ERROR;
   }

   // !!! prvn� vol�n� nav_partition_freespace() trv� velmi dlouho !!!!
   buffer[0] = (qword) (nav_partition_space() << FS_SHIFT_B_TO_SECTOR);
   buffer[1] = (qword) (nav_partition_freespace() << FS_SHIFT_B_TO_SECTOR);

   nav_gotoindex(&sav_index); // Restore position
   
   /* Mo�n� by bylo dobr� d�t sem
   
      if(CS == high) {
	     return HOST_ERR_OK
	  }
	  
      master data nevy�etl, proto�e v�, �e jejich p��prava m��e trvat dlouho, zept� se na n� n�sledn�, nov�m p��kazem
      pokud tady tento if nebude, funkce skon�� chybou, proto�e master bude t�ko tak dlouho �ekat na v�po�et voln�ho
	  m�sta a transakci ukon��.
   */
   
   if(ShellTransmitData((byte *) buffer, 16, YES, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdDir
//-----------------------------------------------------------------------------

byte ShellCmdDir()
{
   /* v bufferu je pot�eba m�sto na: 
      1) na datov� blok, jeho� velikost je SHELL_BLOCK_LENGTH
	  2) a� na jeden dal�� cel� z�znam (dir + name + size), kter� m��e p�et�ct do dal��ho datov�ho bloku, 
	     kter� bude vys�l�n a� v dal��m kroku
		 
	  ov��it
   */
   byte buffer[SHELL_BLOCK_LENGTH + 1 + MAX_FILE_PATH_LENGTH + 4 + 1];
   dword temp;
   word temp2, i, bufferPtr = 0;
   word offset = *((word *) (ShellParamBuffer + 0));
   word count = *((word *) (ShellParamBuffer + 2));

   if(!nav_filelist_reset()) {
      return SHELL_HOST_ERR_DRIVE_ERROR;
   }

   if(offset != 0) {
      if(!nav_filelist_set(offset - 1, FS_FIND_NEXT)) {
		 // offset too large
	     if(ShellTransmitData(NULL, 0, YES, NO) == NO) {
            return SHELL_HOST_ERR_EXEC_ERROR;
         }
 
         return SHELL_HOST_ERR_OK;
	  }
   }
   
   // For each file in list
   while(count-- && nav_filelist_set(0, FS_FIND_NEXT) )
   {
	  // obtain dir/file
      if(nav_file_isdir()) {
         buffer[bufferPtr] = 1;
      } else {
         buffer[bufferPtr] = 0;
      }

      bufferPtr++;

      // obtain file name
      nav_file_name((FS_STRING) buffer + bufferPtr, MAX_FILE_PATH_LENGTH, FS_NAME_GET, true);

	  while(buffer[bufferPtr++] != '\0');

      // obtain file size
	  temp = nav_file_lgt();
	  
	  buffer[bufferPtr] = temp >> 24;
	  buffer[bufferPtr+1] = temp >> 16;
	  buffer[bufferPtr+2] = temp >> 8;
	  buffer[bufferPtr+3] = temp >> 0;
	  //*((dword *) (buffer + bufferPtr)) = nav_file_lgt();
	  
	  bufferPtr += 4;

      
	  if(bufferPtr >= SHELL_BLOCK_LENGTH) {
		 // send block
         if(ShellTransmitData(buffer, SHELL_BLOCK_LENGTH, YES, YES) == NO) {
	         return SHELL_HOST_ERR_EXEC_ERROR;
         }
          
		 // some data overflown, copy them at the beginning of the buffer
		 temp2 = bufferPtr;
		 bufferPtr = 0;
		 for(i = SHELL_BLOCK_LENGTH; i < temp2; i++, bufferPtr++) {
			 buffer[bufferPtr] = buffer[i];
		 }
	  }
   }

   // send last block
   if(ShellTransmitData(buffer, bufferPtr, YES, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }
 
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdCd
//-----------------------------------------------------------------------------

byte ShellCmdCd(void)
{ 
   byte *path = ShellParamBuffer;					 

   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   if(!strcmp(path, "..")) { // parent directory
	   if(!nav_dir_gotoparent()) {
	     return SHELL_HOST_ERR_DIR_ERROR;
	   }
	   
   } else { // path
      ushell_path_valid_synta(path);
   
      // set path
      if(!nav_setcwd((FS_STRING) path, true, false)) {
         return SHELL_HOST_ERR_DIR_ERROR;
      }
   }
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileOpen
//-----------------------------------------------------------------------------

byte ShellCmdFileOpen(void)
{ 
   byte mode = ShellParamBuffer[0];	
   byte *path = ShellParamBuffer + 1;					 

   // not valid path
   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // select file
   if(!nav_setcwd((FS_STRING) path, true, false)) {
      return SHELL_HOST_ERR_FILE_ERR;
   }

   // open file
   if(!file_open(mode)) {
	   return SHELL_HOST_ERR_FILE_ERR;
   }

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileOpen
//-----------------------------------------------------------------------------

byte ShellCmdFileClose(void)
{ 
   file_close();
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileRead
//-----------------------------------------------------------------------------

byte ShellCmdFileRead(void)
{ 
   dword count =  *((dword *) ShellParamBuffer);
   byte buffer[SHELL_BLOCK_LENGTH];
   word bufferPtr = 0;
   uprintf("Budu cist %d znaku\n\n", count);
   
   if(0) { // kdy� nebyl otev�en soubor
      return SHELL_HOST_ERR_FILE_ERR;
   }

   while(count-- && (file_eof() == false))
   {
	  buffer[bufferPtr++] = file_getc(); // �ten� by asi �lo zrychlit pou�it�m fce file_read_buf

	  // send data
	  if(bufferPtr >= SHELL_BLOCK_LENGTH) {
         if(ShellTransmitData(buffer, SHELL_BLOCK_LENGTH, YES, YES) == NO) {
	         return SHELL_HOST_ERR_EXEC_ERROR;
         }
		 
		 bufferPtr = 0;
	  }
   }

   // send rest of data
   if(ShellTransmitData(buffer, bufferPtr, YES, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }
 
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileWrite
//-----------------------------------------------------------------------------

byte ShellCmdFileWrite(void)
{
   dword count = *((dword *) ShellParamBuffer);
   byte blockLength;
   byte ch;
   byte buffer[SHELL_BLOCK_LENGTH];
   byte i;

   if(0) { // kdy� nebyl otev�en soubor
      return SHELL_HOST_ERR_FILE_ERR;
   }

   // register SPI callback
/*   SPIRegisterCallbackRx(&ShellCallbackRx);
   
   // start receive
   SpiStartReceive(SPI_DEVICE);*/

   // tell master we are ready
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }

   // Read blocks
   while(count) {
	  if(count >= SHELL_BLOCK_LENGTH) {
	     blockLength = SHELL_BLOCK_LENGTH;
	  } else {
		  blockLength = count;
	  }
	  
	  count -= blockLength;     
	  
	  ShellReadChar(&buffer[0]); // first char is invalid
	  ShellReadChar(&buffer[0]); // read block size
	  
	  // read data chars
      for(i = 0 ; i < blockLength ; i++) {
         if(ShellReadChar(&buffer[i]) == NO) {
			 //SPIUnregisterCallbackRx();
		     //SpiStopReceive(SPI_DEVICE);
	         return SHELL_HOST_ERR_EXEC_ERROR;
         } 
      }

      if(count) {
		 // prepare for next block 

		 if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
			//SPIUnregisterCallbackRx();
		    //SpiStopReceive(SPI_DEVICE);
            return SHELL_HOST_ERR_EXEC_ERROR;
         }

	  }	else { // all data received
         //SPIUnregisterCallbackRx();
	  }

      // write data
      if(!file_write_buf(buffer, blockLength)) {
		  //SpiStopReceive(SPI_DEVICE);
		  return SHELL_HOST_ERR_WRITE_ERROR;
	  }
   }

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }

   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellMkdir
//-----------------------------------------------------------------------------

byte ShellCmdMkdir(void)
{
   byte *path = ShellParamBuffer;	
	
   // valid path
   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // make dir
   if(!nav_dir_make((FS_STRING) path)) {
      return SHELL_HOST_ERR_DIR_ERROR;
   }
	  
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellRmdir
//-----------------------------------------------------------------------------

// hl�s� ok i kdy� soubor/adres�� neexistuje

byte ShellCmdRmdir(void) // m��e slou�it i k odstran�n� soubor�
{
   byte *path = ShellParamBuffer;	
   Fs_index sav_index;

   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // Save the position
   sav_index = nav_getindex();
   
   while( 1 )
   {
      // Restore the position
      nav_gotoindex(&sav_index);
	  
      // Select file or directory
      if(!nav_setcwd((FS_STRING) path, true, false)) {
         break;
	  }
		 
      // Delete file or directory
      if(!nav_file_del(false)) {
         return SHELL_HOST_ERR_DIR_ERROR;
      }
   }

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellInit
//-----------------------------------------------------------------------------

byte ShellCmdRename(void)
{
   byte *name0 = ShellParamBuffer; // change this name
   byte *name1 = ShellParamBuffer; // to this name
	
   // valid name;
   if(name0[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // find second name
   while(*(++name1) != '\0');
   
   name1++;
   
   // valid
   if(name1[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // Select source file
   if(!nav_setcwd((FS_STRING) name0, true, false)) {
      return SHELL_HOST_ERR_FILE_ERR;
   }
   
   // Rename file or directory
   if(!nav_file_rename((FS_STRING) name1)) {
      return SHELL_HOST_ERR_FILE_ERR;
   }
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }   
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileSeek
//-----------------------------------------------------------------------------

byte ShellCmdFileSeek(void) {
	dword pos = *((dword *) ShellParamBuffer);
	byte origin = ShellParamBuffer[4];
	
	// Seek
	if(file_seek(pos, origin) == false) {
		return SHELL_HOST_ERR_FILE_ERR;
	}

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }  
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdTouch
//-----------------------------------------------------------------------------

byte ShellCmdTouch(void) 
{
   byte *name = ShellParamBuffer;	
   
   // valid name
   if(name[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // create file
   if(!nav_file_create((FS_STRING)name)) {
	   return SHELL_HOST_ERR_FILE_ERR;
   }
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }  
   
   return SHELL_HOST_ERR_OK;
}


//-----------------------------------------------------------------------------
// ShellInit
//-----------------------------------------------------------------------------

void ShellInit(void)
{   
   SpiPortInit();
   spi_initSlave( SPI_DEVICE, 8, 0);
   spi_enable( SPI_DEVICE);
  
   ShellStatusInit();
   ShellStatusReady();

   
   pdca_init_channel(SHELL_DMA_CHANNEL_RX, &DMARxChannel);
   pdca_init_channel(SHELL_DMA_CHANNEL_TX, &DMATxChannel);
   
   INTC_register_interrupt((__int_handler) (&PDCAISR_RX), AVR32_PDCA_IRQ_0, AVR32_INTC_INT1); // level mus� b�t vy��� ne� level usb�ka
   INTC_register_interrupt((__int_handler) (&PDCAISR_TX), AVR32_PDCA_IRQ_1, AVR32_INTC_INT1); // level mus� b�t vy��� ne� level usb�ka

   pdca_enable(SHELL_DMA_CHANNEL_RX);
   pdca_enable(SHELL_DMA_CHANNEL_TX);
   
   pdca_enable_interrupt_transfer_complete(SHELL_DMA_CHANNEL_RX);
   
   InterruptEnable();
   
}

byte ShellReadBufferRx(word pos) {
	return ShellBufferRx[pos];
}

byte ShellReadBufferTx(word pos) {
	return ShellBufferTx1[pos];
}
//-----------------------------------------------------------------------------
// ShellScanCmd
//-----------------------------------------------------------------------------

void ShellScanCmd(void)
{
   byte ptr = 0;
   TShellCmd *cmd;
   byte ch;
   byte i;
   byte error = SHELL_HOST_ERR_OK;
   char buffer[100];
   dword timeout = SHELL_CHAR_TIMEOUT;

   // nothing to do
   if(ShellReadBufferEmpty()) {
      return;
   }

   if(ShellReadChar(&ch)) { // read cmd
      // find command
	  for(i = 0 ; i < SHELL_COMMANDS_NB ; i++) {
			if(ShellCommands[i].cmd == (byte) ch) {
			   cmd = &ShellCommands[i];
			   break;
			}
	  }
	
	  // invalid command
	  if(i == SHELL_COMMANDS_NB) {
         error = SHELL_HOST_ERR_BAD_CMD;
	  }
			
	  // read param
	  ptr = 0;
	  ShellParamBuffer[0] = '\0';

	  while(error == SHELL_HOST_ERR_OK) {
		 if(cmd->VariableLength == NO) {
			   if(cmd->StaticLength == ptr) {
				  break;
			   }
		 }
			   
		 if(!ShellReadChar(&ch)) { // read next char
		    error = SHELL_HOST_ERR_BAD_CMD;
			continue;
		 }

		 if(ch == SHELL_CMD_DELIMITER) { // cmd delimiter received
            if(cmd->VariableLength == YES && ptr >= cmd->StaticLength) { // check if it is valid delimiter
			   if(ptr > 0 && ShellParamBuffer[ptr - 1] != '\0') { // null-terminate the string, for sure
	              ShellParamBuffer[ptr - 1] = '\0';
               }
   
			   break;
			}
		 }
			   
		 if(ptr >= SHELL_PARAMS_MAX_LENGTH) { // param exceeds its allowed length, error
			error = SHELL_HOST_ERR_BAD_CMD;
			break;
		 }
  
		 // store char
		 ShellParamBuffer[ptr] = ch;
				   
		 ptr++;
	  }	

      ShellStopReceive();

	  if(error == SHELL_HOST_ERR_OK) {
		 // tell master we are busy
		 ShellStatusBusy();
		 SysDelay(1); // pokud v obsluze p��kazu bude p��li� brzy n�sledovat ready, master nestihne zaregistrovat busy = p��kaz se nezda��
		 error = cmd->Function();
	  } 

	  if(error != SHELL_HOST_ERR_OK) {
	     ShellTransmitError(error);
				
         uprintf("\n\nError: %d", error);			
         // ??? p�i jak�mkoliv erroru by bylo dobr� po�kat na CS = 1, teprve pak pokra�ovat ???
	  }

      /*
	     Nen� zaru�eno, �e v tomto okam�iku je posledn� SPI transakce �pln� ukon�ena
	  */

      /*for(i = 0 ; i < 15 ; i++) {
	     uprintf("%d", SpiRB(i));
	  }*/
/*
      uputs("\n\n");
	  
	  for(i = 0 ; i < 15 ; i++) {
	     uprintf("%d ", ShellBufferRx[i]);
	  }
	  
	  uputs("\n\n");
	  
	  for(i = 0 ; i < 15 ; i++) {
	     uprintf("%d ", ShellBufferTx1[i]);
	  }
	  
	  uputs("\n\n");
	  
	  for(i = 0 ; i < 15 ; i++) {
	     uprintf("%d ", ShellBufferTx2[i]);
	  }
	  	  
	  uputs("\n\n");
		
	  uprintf("Tx Pos: %d ", pdca_get_load_size(SHELL_DMA_CHANNEL_TX));
	  
	  uputs("\n\n");
		
	  uprintf("Rx Pos: %d ", pdca_get_load_size(SHELL_DMA_CHANNEL_RX));	
	  
	  uputs("\n\n");  
	    */
	  SysDelay(1);

      ShellFlushBuffer();
	  ShellStatusReady();
	  StatusLed0Tog();
	  ShellStartReceive();
	  /*ShellReadChar(&ch); // jeden znak je v RX registru SPI, po enablu DMA bude precten
	  ShellFlushBuffer();*/

   }
}