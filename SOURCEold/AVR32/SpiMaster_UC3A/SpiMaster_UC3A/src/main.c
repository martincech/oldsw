//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"    // UART printf
#include "Cstdio.h"
#include "Efs.h"

#include "At25xx.h"

#define __COM3__
#include "Com.h"

#include "Uart.h"
#include <string.h>
#include <stdlib.h>


#define CMD_DI    "di"
#define CMD_DF    "df"
#define CMD_LS    "ls"
#define CMD_CD    "cd"
#define CMD_FO    "fo"
#define CMD_FC    "fc"
#define CMD_FR    "fr"
#define CMD_FW    "fw"
#define CMD_MK    "mk"
#define CMD_RM    "rm"
#define CMD_RN    "rn"
#define CMD_FS    "fs"
#define CMD_FN    "fn"

#define CMD_FL    "fl"

#define PARAM_BUFFER_LENGTH 50

const byte *HelpText = "\nFSPI command syntax\n" \
   "di                Show available drives\n" \
   "df [drive]        Show drive's total/free space\n" \
   "ls                Show current directory's list\n" \
   "cd [path]         Set path\n" \
   "fo [name] [mode]  Open file (mode = r, r+, w, w+, a)\n" \
   "fc                Close currently opened file\n" \
   "fr [num]          Read num of bytes from file\n" \
   "fw                Write bytes to file\n" \
   "mk [name]         Create a new directory\n" \
   "rm [name]         Delete a file or directory\n" \
   "rn [old] [new]    Rename file or directory\n" \
   "fs [num] [mode]   Set a position in file (mode = B, E, CF, CR)\n" \
   "fn [name]         Create a new file\n" \
   "\n";

static void Help(void);

static void Help(void) {
	uputs(HelpText);
}

static void FlashTest() {
   word PagesNb = 5;
   word FlashPage = 0;
   word i;
   dword fwBytesWritten;
   byte FlashBuffer[ 2*FLASH_PAGE_SIZE];


               uprintf( "Write\n");
               for( i = 0; i < FLASH_PAGE_SIZE; i++){
                  FlashBuffer[ i] = i;
               }
			   FlashBlock4Erase(0);
			   FlashWrite(0, FlashBuffer, FLASH_PAGE_SIZE);
			   
               for( i = 0; i < FLASH_PAGE_SIZE; i++){
                  FlashBuffer[ i] = 0;
               }
			   
               FlashRead(0, FlashBuffer, FLASH_PAGE_SIZE);
              
			   uprintf( "Read\n");

               for( i = 0; i < FLASH_PAGE_SIZE; i++){
                  uprintf( "%d ", FlashBuffer[i]);

               }
/*
   EfsFileDelete("n.txt");

   if(EfsFileCreate("n.txt", PagesNb * FLASH_PAGE_SIZE) != EFS_STAT_OK) {
      return;
   }

   if(EfsFileOpen("n.txt", FOPEN_MODE_W) != EFS_STAT_OK) {
      return;
   }
   
   for(FlashPage = 0; FlashPage < PagesNb ; FlashPage++) {
      FlashBlockReadStart( FlashPage, 0);
      
	  for( i = 0; i < FLASH_PAGE_SIZE; i++){
         FlashBuffer[i] = FlashBlockReadData();
      }
	  
      FlashBlockReadDone();

      FlashPage++;

      FlashBlockReadStart( FlashPage, 0);
      
	  for( i = FLASH_PAGE_SIZE; i < 2 * FLASH_PAGE_SIZE; i++){
         FlashBuffer[i] = FlashBlockReadData();
      }
	  
      FlashBlockReadDone();
	   
      EfsFileWrite(FlashBuffer, 2 * FLASH_PAGE_SIZE, &fwBytesWritten);
	}	
	
   EfsFileClose();*/
}


int main (void)
{
   byte cmd[3];
   byte cmdPtr = 0;
   byte paramBuffer[PARAM_BUFFER_LENGTH + 1];
   byte paramBufferPtr = 0;
   
   byte *foFileName;
   byte foMode;

   dword frBytesTotal;
   dword frBytesReadTotal;
   dword frBytesReadBlock;
   word frBytesBlock;
   
   dword fwBytesWritten;
   byte fwEnd;
   
   byte ch;
   word i = 0, k = 0;
   word bufferPtr;
   
   byte buffer[700];
   byte drivesNb = 0;
   byte drives[5];
   qword total;
   qword free;
   dword *temp;


   
   StatusLedInit();

   CpuInit();

   //UartInit( &AVR32_USART1);
   //ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   
   
   UartInit( &AVR32_USART3);
   ComSetup( UART3_BAUD, UART3_FORMAT, UART3_TIMEOUT);
   
   UartInit( &AVR32_USART2);
   UartSetup( &AVR32_USART2, UART2_BAUD, UART2_FORMAT, UART2_TIMEOUT);
   
   UartInit( &AVR32_USART1);
   UartSetup( &AVR32_USART1, UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   
   //uputs( "Start...\n");
   
   //EfsInit();
   
   //Help();

   
   //FlashInit();

			   
   while(1) {
	  SysDelay(1000);
	  uputch('A');
      UartTxChar(&AVR32_USART2, 'B');
	  
	  if(ComRxWait(0)) {
			ch = 0;
			
            if(ComRxChar(&ch)) {
               UartTxChar(&AVR32_USART1, ch);
		    }
	     }	
		 
	  if(UartRxWait(&AVR32_USART2, 0)) {
			ch = 0;
			
            if(UartRxChar(&AVR32_USART2, &ch)) {
               UartTxChar(&AVR32_USART1, ch);
		    }
	     }	
		 
	  continue;
	  // Wait for CMD
      cmdPtr = 0;
	  
	  while(cmdPtr < 2) {
	     if(ComRxWait(0)) {
			ch = 0;
			
            if(ComRxChar(&ch)) {
			   if(ch == 8) {
			      if(cmdPtr > 0) {
			         cmdPtr--;
				  }
			   } else {	
				  cmd[cmdPtr++] = ch; 			
			   }
			   
			   uputch(ch);
		    }
	     }			
	  }	  

	  cmd[2] = '\0';

	  // Wait for param	
	  paramBufferPtr = 0;
	   
	  do {
         if(ComRxWait(0)) {
			ch = 0;
			
            if(ComRxChar(&ch)) {
			   if(ch == 8) {
			      if(paramBufferPtr > 0) {
			         paramBufferPtr--;
				  }
			   } else {
			      paramBuffer[paramBufferPtr++] = ch;
			   }
			   
			   uputch(ch);
			}
		 }
	  } while(ch != '\n' && ch != '\r' && paramBufferPtr < PARAM_BUFFER_LENGTH);

      ComRxChar(&ch); 
	  /* 
	     P�e�ten� druh�ho znaku z dvojice \r\n
	     Co kdy� nep�ijde...?
	  */

	  uprintf("\n\n");

//-----------------------------------------------------------------------------
// fo
//-----------------------------------------------------------------------------

	  if(!strcmp(cmd, CMD_FO)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		 
		 if(paramBuffer < 3 || paramBuffer[0] != ' ') {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 while(paramBuffer[--paramBufferPtr] != ' ');

		 if(paramBufferPtr == 0) {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 paramBuffer[paramBufferPtr++] = '\0';
		 
		 foFileName = paramBuffer + 1;

		 if(!strcmp("r", paramBuffer + paramBufferPtr)) {
			 foMode = FOPEN_MODE_R;
		 } else if(!strcmp("r+", paramBuffer + paramBufferPtr)) {
			 foMode = FOPEN_MODE_R_PLUS;
		 } else if(!strcmp("w", paramBuffer + paramBufferPtr)) {
			 foMode = FOPEN_MODE_W;
		 } else if(!strcmp("w+", paramBuffer + paramBufferPtr)) {
			 foMode = FOPEN_MODE_W_PLUS;
		 } else if(!strcmp("a", paramBuffer + paramBufferPtr)) {
			 foMode = FOPEN_MODE_APPEND;
		 } else {
            uprintf("Error\n\n");
			continue;
		 }

	     if(EfsFileOpen(foFileName, foMode) != EFS_STAT_OK) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uprintf("File %s opened\n", paramBuffer + 1);
	     } 
		 
		 uputch('\n');
		 
//-----------------------------------------------------------------------------
// fc
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FC)) {
	     if(EfsFileClose() != EFS_STAT_OK) {
			uprintf("Error\n");
		 } else {
	        uputs("File closed\n");
	     }
		 
		 uputch('\n');	 
		 
//-----------------------------------------------------------------------------
// fw
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FW)) {
		 bufferPtr = 0;
		 fwEnd = 0;
		 
	     while(1) {
            if(ComRxWait(0)) {
			   ch = 0;
			
               if(ComRxChar(&ch)) {
				  switch(ch) {
			         case 17:
					    fwEnd = 1;
					    break;
					
					 case 8:
			            if(bufferPtr > 0) {
			               bufferPtr--;
				        }
						break;
						
					 default:
					    buffer[bufferPtr++] = ch;
						
						if(bufferPtr > 500) {
							fwEnd = 1;
						}
						
					    break;
					 
				  }
				  
				  if(fwEnd) {
				     break;
				  }
				  			   
			      uputch(ch);
			   }
		    }
	     }
		  
	     if(EfsFileWrite(buffer, bufferPtr, &fwBytesWritten) != EFS_STAT_OK) {
			uprintf("Error\n");
		 } else {
	        uprintf("Bytes written: %d\n", fwBytesWritten);
	     }
		 
		 uputch('\n');	   

//-----------------------------------------------------------------------------
// fl
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FL)) {
	     FlashTest();
		 
		 uputch('\n');	 

//-----------------------------------------------------------------------------
// fs
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FS)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		 
		 if(paramBuffer < 3 || paramBuffer[0] != ' ') {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 while(paramBuffer[--paramBufferPtr] != ' ');

		 if(paramBufferPtr == 0) {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 paramBuffer[paramBufferPtr] = '\0';
		 paramBufferPtr++;

		 if(!strcmp("B", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FS_SEEK_SET;
		 } else if(!strcmp("E", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FS_SEEK_END;
		 } else if(!strcmp("CF", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FS_SEEK_CUR_FW;
		 } else if(!strcmp("CR", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FS_SEEK_CUR_RE;
		 } else {
            uprintf("Error\n\n");
			continue;
		 }

	     if(EfsFileSeek(atoi(paramBuffer + 1), paramBuffer[paramBufferPtr]) != EFS_STAT_OK) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uprintf("File seeked\n", paramBuffer + 1);
	     } 
		 
		 uputch('\n');

//-----------------------------------------------------------------------------
// fn
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FN)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		  
	     if(paramBuffer[0] != ' ' || EfsFileCreate(paramBuffer + 1, 1024) != EFS_STAT_OK) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uputs("File created\n");
	     }
		 
		 uputch('\n');	 

//-----------------------------------------------------------------------------
// rm
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_RM)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		  
	     if(paramBuffer[0] != ' ' || EfsFileDelete(paramBuffer + 1) != EFS_STAT_OK) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uputs("Dir/File deleted\n");
	     }
		 
		 uputch('\n');
	  }      



//-----------------------------------------------------------------------------
// di
//-----------------------------------------------------------------------------
  
/*	  if(!strcmp(cmd, CMD_DI)) {
         if(FSPIDisk(&drivesNb, drives) == NO) {
			uprintf("Error\n");
			continue;
		 } else {		
            uprintf("%d drives found:\n", drivesNb);
		 
            for(i = 0 ; i < drivesNb ; i++) {
               uprintf("%c:\n", drives[i]);
            }
		 }
			
		 uputch('\n');

//-----------------------------------------------------------------------------
// df
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_DF)) {
		 if(paramBufferPtr < 2 || paramBuffer[0] != ' ' || FSPIDF(paramBuffer[1], &total, &free) == NO) {
		    uprintf("Error\n");
		 } else {
	        uprintf("Drive total space : %d %d bytes\n", total, total >> 32);
		    uprintf("Drive free space : %d %d bytes\n", free, free >> 32);
		    // nevyp�u, proto�e velikosti jsou 64-bitov�
	     }
			
		 uputch('\n');
			
//-----------------------------------------------------------------------------
// ls
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_LS)) {
         k = 0;
			
		 while(1) {
			if(FSPIDir(buffer, k * 15, 15, &frBytesReadBlock) == NO) {
			   uprintf("Error\n\n");
			   break;
			}
				
			if(frBytesReadBlock == 0) {
			   uputch('\n');
			   break;
			}
				
									
			k++;
			   uprintf("Precteno %d ", frBytesReadBlock);
			bufferPtr = 0;
			
			while(bufferPtr < frBytesReadBlock) {
			   if(buffer[bufferPtr] == 1) {
			      uputs("Dir   ");
			   } else {
			      uputs("      ");
			   }
			    
			   bufferPtr++;
				
			   uputs(buffer + bufferPtr);
			 
			   while(buffer[bufferPtr++] != '\0');
				*/
				              /*
				              (dword *temp, dword ia)
				
				
				              Pro� nefunuje:

				              temp = buffer + bufferPtr;
				              ia = *temp;
			    
				              p��padn� p��mo:
				
				              hodnota = *((dword *) &buffer[bufferPtr]);

                              ?????????????????????????????????????????????????????????????
				              */ 
/*
			   uprintf(", size: %d bytes\n", (buffer[bufferPtr] << 24) + (buffer[bufferPtr + 1] << 16) + (buffer[bufferPtr + 2] << 8) + (buffer[bufferPtr + 3] << 0));
               bufferPtr += 4;
		    }
		 }
		 		 
//-----------------------------------------------------------------------------
// cd
//-----------------------------------------------------------------------------
		 
	  } else if(!strcmp(cmd, CMD_CD)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		  
	     if(paramBuffer[0] != ' ' || FSPICd(paramBuffer + 1) == NO) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uputs("Path changed\n");
	     }
		 
		 uputch('\n');
	  
//-----------------------------------------------------------------------------
// fo
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FO)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		 
		 if(paramBuffer < 3 || paramBuffer[0] != ' ') {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 while(paramBuffer[--paramBufferPtr] != ' ');

		 if(paramBufferPtr == 0) {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 paramBuffer[paramBufferPtr] = '\0';
		 paramBufferPtr++;

		 if(!strcmp("r", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FOPEN_MODE_R;
		 } else if(!strcmp("r+", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FOPEN_MODE_R_PLUS;
		 } else if(!strcmp("w", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FOPEN_MODE_W;
		 } else if(!strcmp("w+", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FOPEN_MODE_W_PLUS;
		 } else if(!strcmp("a", paramBuffer + paramBufferPtr)) {
			 paramBuffer[paramBufferPtr] = FOPEN_MODE_APPEND;
		 } else {
            uprintf("Error\n\n");
			continue;
		 }

	     if(FSPIFileOpen(paramBuffer + 1, paramBuffer[paramBufferPtr]) == NO) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uprintf("File %s opened\n", paramBuffer + 1);
	     } 
		 
		 uputch('\n');
	  
//-----------------------------------------------------------------------------
// fc
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FC)) {
	     if(FSPIFileClose() == NO) {
			uprintf("Error\n");
		 } else {
	        uputs("File closed\n");
	     }
		 
		 uputch('\n');	  

//-----------------------------------------------------------------------------
// fr
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_FR)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		 
		 frBytesTotal = atoi(paramBuffer + 1);
         frBytesReadTotal = 0;
		 
		 while(frBytesTotal > 0) {
			if(frBytesTotal > 500) {
				frBytesBlock = 500;
			} else {
				frBytesBlock = frBytesTotal;
			}
			
		    if(FSPIFileRead(buffer, frBytesBlock, &frBytesReadBlock) == NO){
			   uprintf("Error");
			   break;
		    } 
			
	        if(frBytesReadBlock == 0) {
			   break;
			}
// opravit - kdy� vynech�m v�pis, �ten� hned na druh�m vol�n� FSPIFileRead() zkolabuje
// = mus� b�t ur�it� prodleva mezi vol�n�mi FSPIFileRead()
			for(i = 0 ; i < frBytesReadBlock ; i++) {
			   uputch(buffer[i]);
			}
					 
			frBytesTotal -= frBytesReadBlock;
			frBytesReadTotal += frBytesReadBlock;
		 }
		 
		 uputs("\n\n");	
		 uprintf("Bytes read: %d\n", frBytesReadTotal);
		 
		 uputch('\n');

		 
//-----------------------------------------------------------------------------
// mk
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_MK)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		  
	     if(paramBuffer[0] != ' ' || FSPIMkdir(paramBuffer + 1) == NO) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uputs("Dir created\n");
	     }
		 
		 uputch('\n');

//-----------------------------------------------------------------------------
// rm
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_RM)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		  
	     if(paramBuffer[0] != ' ' || FSPIRmdir(paramBuffer + 1) == NO) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uputs("Dir/File deleted\n");
	     }
		 
		 uputch('\n');

//-----------------------------------------------------------------------------
// rn
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_RN)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		 
		 if(paramBuffer < 3 || paramBuffer[0] != ' ') {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 while(paramBuffer[--paramBufferPtr] != ' ');

		 if(paramBufferPtr == 0) {
            uprintf("Error\n\n");
			continue;
		 }
		 
		 paramBuffer[paramBufferPtr] = '\0';
		 paramBufferPtr++;
		  
	     if(FSPIRename(paramBuffer + 1, paramBuffer + paramBufferPtr) == NO) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uputs("Dir/File renamed\n");
	     }
		 
		 uputch('\n');
		 

	  
//-----------------------------------------------------------------------------
// to
//-----------------------------------------------------------------------------

	  } else if(!strcmp(cmd, CMD_TO)) {
		 paramBuffer[paramBufferPtr - 1] = '\0';
		  
	     if(paramBuffer[0] != ' ' || FSPITouch(paramBuffer + 1) == NO) { // there must be a space at the beginning
			 uprintf("Error\n");
		 } else {			 
	        uputs("File created\n");
	     }
		 
		 uputch('\n');	 
		 
//-----------------------------------------------------------------------------
// HELP
//-----------------------------------------------------------------------------
 
	  }	else {
		  Help();
	  }*/
   }  
}