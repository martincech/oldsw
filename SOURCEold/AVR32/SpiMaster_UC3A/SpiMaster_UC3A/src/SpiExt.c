#include "spi.h"
#include "SpiExt.h"  // Extended SPI library
#include "System.h"

byte *SpiTxPtr = NULL;
word SpiTxLength = 0;
word SpiTxCount = 0;
/*byte *SpiRxPtr = NULL;
word SpiRxLength = 0;
word SpiRxCount = 0;*/


void SpiTx(volatile avr32_spi_t *spi, byte *Buffer, word Length) {
   SpiTxLength = Length;
   SpiTxPtr = Buffer;
   SpiTxCount = 0;
   SpiEnableTxInt(spi); 
}

__attribute__((__interrupt__)) void Spi0ISR(void)
{
   if((AVR32_SPI0.sr & (1 << AVR32_SPI_TDRE_OFFSET))) {

   }
}



// vylep�it - zat�mco nad�azen� vrstva zpracov�v� p�ijat� byte, SPI m��e p�ij�mat dal��
TYesNo SpiReadChar(volatile avr32_spi_t *spi, byte *data) {
   unsigned int timeout = SPI_TIMEOUT;

   while (!(spi->sr & AVR32_SPI_SR_TDRE_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   spi->tdr = 0xFF << AVR32_SPI_TDR_TD_OFFSET;

   while((spi->sr & AVR32_SPI_SR_TXEMPTY_MASK) != 0);
   
   timeout = SPI_TIMEOUT;

   while((spi->sr & (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) !=
         (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   *data = spi->rdr >> AVR32_SPI_RDR_RD_OFFSET;

   return YES;
}