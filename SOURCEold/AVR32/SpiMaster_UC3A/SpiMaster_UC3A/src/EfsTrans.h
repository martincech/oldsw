#ifndef _EFS_TRANS_H_
#define _EFS_TRANS_H_

#include "System.h"

void EfsTransInit(void);

void *EfsTransGetTxBuffer(void);
TYesNo EfsTransSend(word Size);
TYesNo EfsTransReceive();
TYesNo EfsTransDataIsReady(void);
word EfsTransGetDataSize(void);
byte* EfsTransGetData(void);
TYesNo EfsTransWaitForDataReady(void);

#endif