#ifndef _FSPI_H_
#define _FSPI_H_

#define FSPI_MAX_DRIVES    5
#define MAX_FILE_PATH_LENGTH    255 // !!! upravit

void FSPIInit(void);
TYesNo FSPIDisk(byte *drivesNb, byte *drives);
TYesNo FSPIDF(byte drive, qword *total, qword *free);
TYesNo FSPIDir(byte *buffer, word offset, word count, dword *bytesRead);
TYesNo FSPICd(byte *path);
TYesNo FSPIFileOpen(byte *file, byte mode);
TYesNo FSPIFileClose(void);
TYesNo FSPIFileRead(byte *buffer, dword count, dword *bytesRead);
TYesNo FSPIFileWrite(byte *buffer, dword count, dword *bytesWritten);
TYesNo FSPIMkdir(byte *buffer);
TYesNo FSPIRmdir(byte *buffer);
TYesNo FSPIRename(byte *name0, byte *name1);
TYesNo FSPIFileSeek(dword pos, byte origin);
TYesNo FSPITouch(byte *name);



#define FILE_IO_MAX_DRIVES              FSPI_MAX_DRIVES
#define FILE_IO_MAX_FILE_PATH_LENGTH    MAX_FILE_PATH_LENGTH

//! \name File open modes
//! @{
#define  FOPEN_READ_ACCESS    0x01                                                                    // authorize the read access
#define  FOPEN_WRITE_ACCESS   0x02                                                                    // authorize the write access
#define  FOPEN_CLEAR_SIZE     0x04                                                                    // reset size
#define  FOPEN_CLEAR_PTR      0x08                                                                    // reset flux pointer
#define  FOPEN_MODE_R         (FOPEN_READ_ACCESS|FOPEN_CLEAR_PTR)                                     //!< R   access, flux pointer = 0, size not modify
#define  FOPEN_MODE_R_PLUS    (FOPEN_READ_ACCESS|FOPEN_WRITE_ACCESS|FOPEN_CLEAR_PTR)                  //!< R/W access, flux pointer = 0, size not modify
#define  FOPEN_MODE_W         (FOPEN_WRITE_ACCESS|FOPEN_CLEAR_PTR|FOPEN_CLEAR_SIZE)                   //!< W   access, flux pointer = 0, size = 0
#define  FOPEN_MODE_W_PLUS    (FOPEN_READ_ACCESS|FOPEN_WRITE_ACCESS|FOPEN_CLEAR_PTR|FOPEN_CLEAR_SIZE) //!< R/W access, flux pointer = 0, size = 0
#define  FOPEN_MODE_APPEND    (FOPEN_WRITE_ACCESS)                                                    //!< W   access, flux pointer = at the end, size not modify
//! @}

#define  FS_SEEK_SET       0x00  //!< start at the beginning
#define  FS_SEEK_END       0x01  //!< start at the end of file and rewind
#define  FS_SEEK_CUR_RE    0x02  //!< start at the current position and rewind
#define  FS_SEEK_CUR_FW    0x03  //!< start at the current position and foward


#endif;