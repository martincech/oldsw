#include "System.h"
#include "EfsTrans.h"
#include "EfsTransDef.h"
#include "Hardware.h"
#include "SpiExt.h"
#include "pdca.h"

#include "uconio.h"

#define EfsTransSendChar(ch)        spi_write(SPI_USBH_DEVICE, ch)
#define EfsTransReceiveChar(p_ch)    SpiReadChar(SPI_USBH_DEVICE, p_ch)




#define EFS_TRANS_READY_WAIT_TIMEOUT  1000000

byte EfsTransRxBuffer[EFS_TRANS_MAX_PACKET_LENGTH] __attribute__ ((aligned(sizeof(dword))));
TYesNo EfsTransDataReady = NO;
byte *EfsTransData = EfsTransRxBuffer;
word EfsTransDataSize;

byte EfsTransTxBuffer[EFS_TRANS_MAX_PACKET_LENGTH] __attribute__ ((aligned(sizeof(dword))));

static word CalculateCRC(byte *Data, byte Length);
static TYesNo EfsTransWaitForSlaveReady(void);


void EfsTransInit(void)
{
   spi_options_t spiOptions = {
      .reg          = SPI_USBH_NPCS,
      .baudrate     = SPI_USBH_MASTER_SPEED,
      .bits         = 8,
      .spck_delay   = 0,
      .trans_delay  = 0,
      .stay_act     = 1,
      .spi_mode     = 0,
      .modfdis      = 1
   };

   
   SpiUsbhPortInit();

   spi_initMaster( SPI_USBH_DEVICE, &spiOptions);
   spi_selectionMode( SPI_USBH_DEVICE, 0, 0, 0);
   spi_enable( SPI_USBH_DEVICE);
   spi_setupChipReg( SPI_USBH_DEVICE, &spiOptions, F_BUS_A);

   EfsTransSlaveStatusInit();

}

void *EfsTransGetTxBuffer(void) {
	return ((TEfsTransPacket *) EfsTransTxBuffer)->Data;
}


TYesNo EfsTransSend(word length) {
   TEfsTransPacket *Packet = (TEfsTransPacket *) EfsTransTxBuffer;
   word PacketLength = sizeof(*Packet) + length;
   word i = 0;
   word Crc;
   word MinusLength;
   
   if(length > EFS_TRANS_MAX_DATA_SIZE) {
	   return NO;
   }

   // SOF
   Packet->Sof = EFS_TRANS_SOF;
   
   // Length
   Packet->DataSize = length;
   Packet->MinusDataSize = -length;

   // CRC
   Packet->Crc = CalculateCRC(Packet->Data, Packet->DataSize);

   if(EfsTransWaitForSlaveReady() == NO) {
	   return NO;
   }

   spi_selectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);

   for(i = 0 ; i < PacketLength ; i++) {
	   spi_write(SPI_USBH_DEVICE, EfsTransTxBuffer[i]);
   }
   
   while(!spi_writeEndCheck(SPI_USBH_DEVICE));
   spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);
   
   return YES;
}

TYesNo EfsTransDataIsReady(void) {
   return EfsTransDataReady;
}

word EfsTransGetDataSize(void) {
   TEfsTransPacket *Packet = (TEfsTransPacket *) EfsTransRxBuffer;
   return Packet->DataSize;
}

byte* EfsTransGetData(void) {
   TEfsTransPacket *Packet = (TEfsTransPacket *) EfsTransRxBuffer;
   return Packet->Data;
}

TYesNo EfsTransReceive() {
   word i;
   word ch;
   
   byte *Buffer = EfsTransRxBuffer;
   TEfsTransPacket *Packet = (TEfsTransPacket *) EfsTransRxBuffer;
   
   word DataSize;
   word MinusDataSize;
   word CrcRec;
   word CrcCalc;
   
   if(EfsTransWaitForSlaveReady() == NO) {
	   return NO;
   }

   spi_selectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);

   // Search for SOF
   i = 0;
   
   while(1) {
      if(SpiReadChar(SPI_USBH_DEVICE, Buffer) == NO) {
		 spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);
         return;
      }

      if(Packet->Sof == EFS_TRANS_SOF) {
		 Buffer++;
         break;
      }
	  
	  i++;

	  if(i > 10) {
		 uprintf("SOF err");
		 spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);
	     return;
	  }
   }
	
   // Read all except SOF and Data

   i = sizeof(*Packet) - sizeof(Packet->Sof);

   while(i--) {
      if(EfsTransReceiveChar(Buffer++) == NO) {
	     spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);
         return;
      }
   }
   
   if(Packet->DataSize > EFS_TRANS_MAX_DATA_SIZE) {
	   spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);
	   return;
   }

   Packet->MinusDataSize = -Packet->MinusDataSize;

   if(Packet->DataSize != Packet->MinusDataSize) {
	   spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);
	   return;
   }
   
   i = Packet->DataSize;
   
   while(i--) {
      if(EfsTransReceiveChar(Buffer++) == NO) {
		 spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);
		 return;
	  }
   }

   
   
   spi_unselectChip(SPI_USBH_DEVICE, SPI_USBH_NPCS);

   if(Packet->Crc != CalculateCRC(Packet->Data, Packet->DataSize)) {
	  return;
   }

   // Everything OK
   EfsTransDataReady = YES;
}



static TYesNo EfsTransWaitForSlaveReady(void) {
   dword timeout = EFS_TRANS_READY_WAIT_TIMEOUT;
   
   while(EfsTransSlaveIsBusy()) {
      timeout--;
		
      if(!timeout) {
         return NO;
      }
   }
   
   return YES;
}

TYesNo EfsTransWaitForDataReady(void) {
   dword timeout = EFS_TRANS_TRANSACTION_TIMEOUT;
   
   while(EfsTransDataIsReady() == NO) {
      timeout--;
		
      if(!timeout) {
         return NO;
      }
   }
   
   return YES;
}