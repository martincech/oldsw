#include "System.h"

#include "Efs.h"
#include "EfsTrans.h" // transportní vrstva

#include <string.h> // memcpy

#include "uconio.h"


byte EfsFileOpen(byte *FileName, byte Mode) {
   EfsCmdFileOpen *Cmd = (EfsCmdFileOpen *) EfsTransGetTxBuffer();
   byte FileNameLength = strlen(FileName) + 1;
   EfsReplyFileOpen *Reply;

   Cmd->Code = EFS_CMD_FILEOPEN;
   Cmd->Mode = Mode;
   memcpy(Cmd->FileName, FileName, FileNameLength);

   if(EfsTransSend(sizeof(*Cmd) + FileNameLength) == NO) {
	   return EFS_STAT_ERR;
   }

   EfsTransReceive();
   
   if(EfsTransWaitForDataReady() == NO) {
	   return EFS_STAT_ERR;
   }
   
   Reply = EfsTransGetData();

   return Reply->Reply;
}

byte EfsFileClose(void) {
   EfsCmdFileClose *Cmd = (EfsCmdFileClose *) EfsTransGetTxBuffer();
   EfsReplyFileClose *Reply;

   Cmd->Code = EFS_CMD_FILECLOSE;

   if(EfsTransSend(sizeof(*Cmd)) == NO) {
	   return EFS_STAT_ERR;
   }

   EfsTransReceive();
   
   if(EfsTransWaitForDataReady() == NO) {
	   return EFS_STAT_ERR;
   }
   
   Reply = EfsTransGetData();

   return Reply->Reply;
}

byte EfsFileWrite(byte *Data, word Count, word *Written) {
   word i;
   EfsCmdFileWrite *Cmd = (EfsCmdFileWrite *) EfsTransGetTxBuffer();
   EfsReplyFileWrite *Reply;

   
   Cmd->Code = EFS_CMD_FILEWRITE;
   Cmd->Count = Count;
   memcpy(Cmd->Data, Data, Count); 

   if(EfsTransSend(sizeof(*Cmd) + Count) == NO) {
	   return EFS_STAT_ERR;
   }

   EfsTransReceive();
   
   if(EfsTransWaitForDataReady() == NO) {
	   return EFS_STAT_ERR;
   }
   
   Reply = EfsTransGetData();

   *Written = Reply->Written;

   return Reply->Reply;
}

byte EfsFileSeek(dword Pos, byte Whence) {
   EfsCmdFileSeek *Cmd = (EfsCmdFileSeek *) EfsTransGetTxBuffer();
   EfsReplyFileSeek *Reply;
   
   Cmd->Code = EFS_CMD_FILESEEK;
   Cmd->Pos = Pos;
   Cmd->Whence = Whence;

   if(EfsTransSend(sizeof(*Cmd)) == NO) {
	   return EFS_STAT_ERR;
   }

   EfsTransReceive();
   
   if(EfsTransWaitForDataReady() == NO) {
	   return EFS_STAT_ERR;
   }
   
   Reply = EfsTransGetData();

   return Reply->Reply;
}

byte EfsFileCreate(byte *FileName, dword Size) {
   EfsCmdFileCreate *Cmd = (EfsCmdFileCreate *) EfsTransGetTxBuffer();
   byte FileNameLength = strlen(FileName) + 1;
   EfsReplyFileCreate *Reply;

   Cmd->Code = EFS_CMD_FILECREATE;
   Cmd->Size = Size;
   memcpy(Cmd->FileName, FileName, FileNameLength);

   if(EfsTransSend(sizeof(*Cmd) + FileNameLength) == NO) {
	   return EFS_STAT_ERR;
   }

   EfsTransReceive();
   
   if(EfsTransWaitForDataReady() == NO) {
	   return EFS_STAT_ERR;
   }
   
   Reply = EfsTransGetData();

   return Reply->Reply;
}


byte EfsFileDelete(byte *FileName) {
   EfsCmdFileDelete *Cmd = (EfsCmdFileDelete *) EfsTransGetTxBuffer();
   byte FileNameLength = strlen(FileName) + 1;
   EfsReplyFileDelete *Reply;

   Cmd->Code = EFS_CMD_FILEDELETE;
   memcpy(Cmd->FileName, FileName, FileNameLength);

   if(EfsTransSend(sizeof(*Cmd) + FileNameLength) == NO) {
	   return EFS_STAT_ERR;
   }

   EfsTransReceive();
   
   if(EfsTransWaitForDataReady() == NO) {
	   return EFS_STAT_ERR;
   }
   
   Reply = EfsTransGetData();

   return Reply->Reply;
}

void EfsInit(void) {
	EfsTransInit();
}