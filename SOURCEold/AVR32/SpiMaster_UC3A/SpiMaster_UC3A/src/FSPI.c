// framework
#include "spi.h"
#include "SpiExt.h"
#include "hardware.h"
#include "System.h"
#include "uconio.h"
#include "FSPI.h"

#define FSPI_WAIT_TIMEOUT    300000 // smaz�n� adres��e trv� i sekundu

#define HOST_BUSY     0x55
#define HOST_READY         0x99
#define HOST_BAD_CMD       0x77

#define CMD_DISK        0x01
#define CMD_DF          0x02
#define CMD_DIR         0x03
#define CMD_CD          0x04
#define CMD_FILE_OPEN   0x05
#define CMD_FILE_CLOSE  0x06
#define CMD_FILE_READ   0x07
#define CMD_FILE_WRITE  0x08
#define CMD_MKDIR       0x09
#define CMD_RMDIR       0x0A
#define CMD_RENAME      0x0B
#define CMD_FILE_SEEK   0x0C
#define CMD_TOUCH       0x0D

#define SHELL_PARAMS_MAX_LENGTH  50
#define SHELL_CMD_DELIMITER  '\n'

static TYesNo FSPICmd(byte Cmd, byte *Param, word ParamLength, TYesNo terminate);
static TYesNo FSPICmdResponse();

//-----------------------------------------------------------------------------
// FSPICmd
//-----------------------------------------------------------------------------

static TYesNo FSPICmd(byte Cmd, byte *Param, word ParamLength, TYesNo terminate) {
   dword timeout = FSPI_WAIT_TIMEOUT;
    StatusLed0Tog();
   spi_write(SPI0_DEVICE, Cmd); // send cmd
   
   while(ParamLength--) { // send param
	  spi_write(SPI0_DEVICE, *Param);
      Param++;
   }
	
   if(terminate == YES) {
	  spi_write(SPI0_DEVICE, SHELL_CMD_DELIMITER);
   }
   
   while(!spi_writeEndCheck(SPI0_DEVICE)); // wait till transaction completes
   
   while(!SpiSlaveIsBusy()) {
	  if(!timeout--) {
		 uprintf("time cmd");
		 return NO;
	  }
	   
	  SysUDelay(1);
   }
   StatusLed0Tog();
   return YES;
}

//-----------------------------------------------------------------------------
// FSPICmdResponse
//-----------------------------------------------------------------------------

static TYesNo FSPICmdResponse() {
   dword timeout = FSPI_WAIT_TIMEOUT;
   word ch;
    StatusLed0Tog();
   while(SpiSlaveIsBusy()) {
	  if(!timeout--) {
		 uprintf("time resp");
		 return NO;
	  }
	   
	  SysUDelay(1);
   }
   StatusLed0Tog();
   if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
      return NO;
   }

   if(ch != 0) {
      return NO;
   }
	
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIInit
//-----------------------------------------------------------------------------

void FSPIInit(void)
{
   spi_options_t spiOptions = {
      .reg          = SPI0_NPCS,
      .baudrate     = SPI0_MASTER_SPEED,
      .bits         = 8,
      .spck_delay   = 0,
      .trans_delay  = 0,
      .stay_act     = 1,
      .spi_mode     = 0,
      .modfdis      = 1
   };
   
   Spi0PortInit();

   spi_initMaster( SPI0_DEVICE, &spiOptions);
   spi_selectionMode( SPI0_DEVICE, 0, 0, 0);
   spi_enable( SPI0_DEVICE);
   spi_setupChipReg( SPI0_DEVICE, &spiOptions, F_BUS_A);
   
   SpiSlaveStatusInit();
}

//-----------------------------------------------------------------------------
// FSPIDisk
//-----------------------------------------------------------------------------

TYesNo FSPIDisk(byte *drivesNb, byte *drives) {
   word ch;
   byte i;
   
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_DISK, NULL, 0, NO) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
	
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   // data block length
   if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
	  spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   // read drives count
   if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
	  spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   *drivesNb = (byte) ch;

   if(*drivesNb > FSPI_MAX_DRIVES) {
      drivesNb = FSPI_MAX_DRIVES;
   }
   
   // read drives names
   for(i = 0 ; i < *drivesNb ; i++) {

      if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
	     spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
         return NO;
      }
	  
	  drives[i] = (byte) ch;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIDF
//-----------------------------------------------------------------------------

TYesNo FSPIDF(byte drive, qword *total, qword *free) {
   word ch;
   byte i, j;
   qword *temp = total;
   
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_DF, &drive, 1, NO) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
	
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   // data block length
   if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
	  spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   // read two 64 bit values
   // first = total space, second = free space
   i = 2;
   while(i--) {
      *temp = 0;
	  
	  j = 8;
	  while(j--) {
		 *temp <<= 8;
		 
         if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
	        spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
            return NO;
         }
		 
		 *temp |= ch;
      }
	  
      temp = free;
   }   

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIDir
//-----------------------------------------------------------------------------

#define SHELL_DATA_MAX_LENGTH    255

TYesNo FSPIDir(byte *buffer, word offset, word count, dword *bytesRead) {
   word ch;
   word i;
   byte param[4];
   word bytesToBeRead;

   *((word *) (param + 0)) = offset;
   *((word *) (param + 2)) = count;

   *bytesRead = 0;

   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_DIR, param, 4, NO) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
	
   while(1) {
      if(FSPICmdResponse() == NO) {
         spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
         return NO;
      }

      // read number of bytes to be transmited
         if(SpiRead(SPI0_DEVICE, &bytesToBeRead) == NO) { 
	        spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
            return NO;
         }

		 
      // read data
      i = (byte) (bytesToBeRead);
	  
      while(i--) {
         if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
	        spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
            return NO;
         }
	  
	     *(buffer++) = (byte) ch;
      } 

	  *bytesRead += bytesToBeRead;

	  if(bytesToBeRead < SHELL_DATA_MAX_LENGTH) {
	     break;
	  }
   }

   return YES;
}

//-----------------------------------------------------------------------------
// FSPICd
//-----------------------------------------------------------------------------

TYesNo FSPICd(byte *path) {
   word count = 0;
   word ch;
   while(path[count++] != '\0');
   
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_CD, path, count, YES) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIFileOpen
//-----------------------------------------------------------------------------

TYesNo FSPIFileOpen(byte *file, byte mode) {
   word count = 0;
   word ch;
   byte buffer[FILE_IO_MAX_FILE_PATH_LENGTH + 1 + 1];
   
   buffer[0] = mode;
   // Mus�me p�ekop�rovat do jin�ho bufferu, proto�e na jeho za��tek pot�ebujeme um�stit mod...
   do {
	   buffer[count + 1] = file[count];
   } while(file[count++] != '\0');
   
   count++;
   
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_FILE_OPEN, buffer, count, YES) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIFileClose
//-----------------------------------------------------------------------------

TYesNo FSPIFileClose() {
   word ch;
	
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_FILE_CLOSE, NULL, 0, NO) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIFileRead
//-----------------------------------------------------------------------------

TYesNo FSPIFileRead(byte *buffer, dword count, dword *bytesRead) {
   word ch;
   word i;

   word bytesToBeRead;
   byte param[4] = {count >> 24, count >> 16, count >> 8, count >> 0};
   *bytesRead = 0;

   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_FILE_READ, param, 4, NO) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
	
   while(1) {
      if(FSPICmdResponse() == NO) {
         spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
         return NO;
      }
   
      // read number of bytes to be transmited
         if(SpiRead(SPI0_DEVICE, &bytesToBeRead) == NO) { 
	        spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
            return NO;
         }
   	     //uprintf("%d", bytesToBeRead);
	     //uputch(':');
      // read data
   

      i = bytesToBeRead;
	  
      while(i--) {
         if(SpiRead(SPI0_DEVICE, &ch) == NO) { 
	        spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
            return NO;
         }
	  
	     *(buffer++) = (byte) ch;
      } 
	  
	  *bytesRead += bytesToBeRead;
	  
	  if(bytesToBeRead < SHELL_DATA_MAX_LENGTH) {
	     break;
	  }
   }
   
   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIFileWrite
//-----------------------------------------------------------------------------

TYesNo FSPIFileWrite(byte *buffer, dword count, dword *bytesWritten) {
   word ch;
   word i;
   byte blockLength = SHELL_DATA_MAX_LENGTH;
   byte param[4] = {count >> 24, count >> 16, count >> 8, count >> 0};
   
   *bytesWritten = 0;

   if(count == 0) {
	   return YES;
   }

   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_FILE_WRITE, param, 4, NO) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
	
   while(count) {
      if(FSPICmdResponse() == NO) {
         spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
         return NO;
      }
	  
	  if(count < SHELL_DATA_MAX_LENGTH) {
	     blockLength = count;
	  } else {
		 blockLength = SHELL_DATA_MAX_LENGTH;
	  }

	  spi_write(SPI0_DEVICE, blockLength);

      i = blockLength;

      while(i--) {
         spi_write(SPI0_DEVICE, *(buffer++)); // co zkusit hranat� z�vorky?
      } 
	  
	  *bytesWritten += blockLength;
	  count -= blockLength;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIMkdir
//-----------------------------------------------------------------------------

TYesNo FSPIMkdir(byte *name) {
   word count = 0;
   word ch;
   byte buffer[FILE_IO_MAX_FILE_PATH_LENGTH + 1];
   

   // Mus�me p�ekop�rovat do jin�ho bufferu, proto�e na jeho za��tek pot�ebujeme um�stit mod...
   do {
	   buffer[count] = name[count];
   } while(name[count++] != '\0');
   
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_MKDIR, buffer, count, YES) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIRmdir
//-----------------------------------------------------------------------------

TYesNo FSPIRmdir(byte *name) {
   word count = 0;
   word ch;
   byte buffer[FILE_IO_MAX_FILE_PATH_LENGTH + 1];
   

   // Mus�me p�ekop�rovat do jin�ho bufferu, proto�e na jeho za��tek pot�ebujeme um�stit mod...
   do {
	   buffer[count] = name[count];
   } while(name[count++] != '\0');
   
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_RMDIR, buffer, count, YES) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIRename
//-----------------------------------------------------------------------------

TYesNo FSPIRename(byte *name0, byte *name1) {
   word count = 0;
   byte i = 0;
   word ch;
   byte buffer[2*FILE_IO_MAX_FILE_PATH_LENGTH + 2];
   

   // Mus�me p�ekop�rovat do jin�ho bufferu, proto�e na jeho za��tek pot�ebujeme um�stit mod...
   do {
	   buffer[count] = name0[count];
   } while(name0[count++] != '\0');

   do {
	   buffer[count++] = name1[i];
   } while(name1[i++] != '\0');
 
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_RENAME, buffer, count, YES) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPIFileSeek
//-----------------------------------------------------------------------------

TYesNo FSPIFileSeek(dword pos, byte origin) {
   byte param[5] = {pos >> 24, pos >> 16, pos >> 8, pos >> 0, origin};

   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_FILE_SEEK, param, 5, NO) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
	
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
  
   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

//-----------------------------------------------------------------------------
// FSPITouch
//-----------------------------------------------------------------------------

TYesNo FSPITouch(byte *name) {
   word count = 0;
   word ch;
   byte buffer[FILE_IO_MAX_FILE_PATH_LENGTH + 1];
   

   // Mus�me p�ekop�rovat do jin�ho bufferu, proto�e na jeho za��tek pot�ebujeme um�stit mod...
   do {
	   buffer[count] = name[count];
   } while(name[count++] != '\0');
   
   spi_selectChip(SPI0_DEVICE, SPI0_NPCS);
	
   if(FSPICmd(CMD_TOUCH, buffer, count, YES) == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }
   
   if(FSPICmdResponse() == NO) {
      spi_unselectChip( SPI0_DEVICE, SPI0_NPCS);
      return NO;
   }

   spi_unselectChip(SPI0_DEVICE, SPI0_NPCS);
   
   return YES;
}

// o�et�it p�ekop�rov�n� �et�zc� do buffer� - m��e nastat p�ete�en� buffer�