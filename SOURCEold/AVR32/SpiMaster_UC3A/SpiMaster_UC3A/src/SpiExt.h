//*****************************************************************************
//
//    SpiExt.h   SPI extended
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef _SPI_EXT_H_
#define _SPI_EXT_H_

#include "spi.h"
#include "System.h"

__attribute__((__interrupt__)) void Spi0ISR(void);

#define SpiEnableRxInt(spi) ((spi)->ier = (1 << AVR32_SPI_RDRF_OFFSET))

#define SpiEnableTxInt(spi) ((spi)->ier = (1 << AVR32_SPI_TDRE_OFFSET))

#define SpiDisableRxInt(spi) ((spi)->idr = (1 << AVR32_SPI_RDRF_OFFSET))

#define SpiDisableTxInt(spi) ((spi)->idr = (1 << AVR32_SPI_TDRE_OFFSET))

void SpiTx(volatile avr32_spi_t *spi, byte *buffer, word Length);

TYesNo SpiReadChar(volatile avr32_spi_t *spi, byte *data);

#endif