//*****************************************************************************
//
//    Hardware.h   Default hardware definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr32/io.h>        // CPU header

// Software framework :
#include "compiler.h"

// Project includes :
#include "Uni.h"
#include "Uc3.h"
#include <stddef.h>           // macro offsetof

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#define F_XTAL    12000000             // XTAL frequency 12.000 MHz

#define F_MCK     F_XTAL               // main clock
#define F_CPU     F_MCK                // system frequency
#define F_BUS_H   F_CPU                // high speed bus frequency
#define F_BUS_A   F_CPU                // peripheral bus A frequency
#define F_BUS_B   F_CPU                // peripheral bus B frequency

#define XTAL_STARTUP   AVR32_PM_OSCCTRL0_STARTUP_2048_RCOSC

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

#define WATCHDOG_INTERVAL 2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]

#define WATCHDOG_ENABLE 1              // Enable watchdog
//#define SLEEP_ENABLE    1              // Enable power save

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// Interrupt priority :
#define SYS_TIMER_PRIORITY   AVR32_INTC_INT0         // System timer IRQ


//#define UART_BINARY_ENABLE
//#define UART_ASCII_ENABLE
#define UART_NATIVE_ENABLE
#define UART_MODBUS_RTU_ENABLE
#define UART_HALF_DUPLEX_ENABLE

#define UART_SLAVE_ENABLE
#define UART_MASTER_ENABLE

#define UART_CHANNELS_COUNT   3



//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_PHY                AVR32_USART0
#define UART0_IRQ                AVR32_USART0_IRQ
#define UART0_RXD_PIN            AVR32_USART0_RXD_0_0_PIN
#define UART0_RXD_FUNCTION       AVR32_USART0_RXD_0_0_FUNCTION
#define UART0_TXD_PIN            AVR32_USART0_TXD_0_0_PIN
#define UART0_TXD_FUNCTION       AVR32_USART0_TXD_0_0_FUNCTION
#define UART0_RTS_PIN            AVR32_USART0_RTS_0_0_PIN
#define UART0_RTS_FUNCTION       AVR32_USART0_RTS_0_0_FUNCTION

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define UART1_PHY                AVR32_USART1
#define UART1_IRQ                AVR32_USART1_IRQ
#define UART1_RXD_PIN            AVR32_USART1_RXD_0_0_PIN
#define UART1_RXD_FUNCTION       AVR32_USART1_RXD_0_0_FUNCTION
#define UART1_TXD_PIN            AVR32_USART1_TXD_0_0_PIN
#define UART1_TXD_FUNCTION       AVR32_USART1_TXD_0_0_FUNCTION
#define UART1_RTS_PIN            AVR32_USART1_RTS_0_0_PIN
#define UART1_RTS_FUNCTION       AVR32_USART1_RTS_0_0_FUNCTION

//-----------------------------------------------------------------------------
// UART2
//-----------------------------------------------------------------------------

#define UART2_PHY                AVR32_USART2
#define UART2_IRQ                AVR32_USART2_IRQ
#define UART2_RXD_PIN            AVR32_USART2_RXD_0_0_PIN
#define UART2_RXD_FUNCTION       AVR32_USART2_RXD_0_0_FUNCTION
#define UART2_TXD_PIN            AVR32_USART2_TXD_0_0_PIN
#define UART2_TXD_FUNCTION       AVR32_USART2_TXD_0_0_FUNCTION
#define UART2_RTS_PIN            AVR32_USART2_RTS_0_0_PIN
#define UART2_RTS_FUNCTION       AVR32_USART2_RTS_0_0_FUNCTION

//-----------------------------------------------------------------------------
// UART3
//-----------------------------------------------------------------------------

#define UART3_PHY                AVR32_USART3
#define UART3_IRQ                AVR32_USART3_IRQ
#define UART3_RXD_PIN            AVR32_USART3_RXD_0_0_PIN
#define UART3_RXD_FUNCTION       AVR32_USART3_RXD_0_0_FUNCTION
#define UART3_TXD_PIN            AVR32_USART3_TXD_0_0_PIN
#define UART3_TXD_FUNCTION       AVR32_USART3_TXD_0_0_FUNCTION
#define UART3_RTS_PIN            AVR32_USART3_RTS_0_1_PIN
#define UART3_RTS_FUNCTION       AVR32_USART3_RTS_0_1_FUNCTION

#define STATUS_LED   AVR32_PIN_PB02

#define StatusLedTog()     GpioOutput( STATUS_LED); GpioToggle( STATUS_LED)

#endif
