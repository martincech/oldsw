//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
//#include "Uart.h"
//#include "UartBinary.h"
//#include "UartNative.h"
#include "Modbus.h";
#include <string.h>

#define BUFFER_SIZE  128
volatile int a;

int main (void)
{
   byte Buffer[BUFFER_SIZE];
   byte TxBuffer[BUFFER_SIZE];
   word RxSize;
   TYesNo Done;
   word i;

   CpuInit();
   WatchDogInit();
   InterruptEnable();
   cpu_irq_enable();
   
   
   ModbusSetup(MODBUS_MODBUS0, MODBUS_RTU_SLAVE, 19200, MODBUS_PARITY_EVEN);
   ModbusStart(MODBUS_MODBUS0);
   forever {
      ModbusExecute(MODBUS_MODBUS0);
   }
   /*
   UartInit( UART_UART2);
   UartSetup( UART_UART2, 57600, UART_8BIT);
   UartTimeoutSet( UART_UART2, UART_TIMEOUT_OFF, 10);




   UartInit( UART_UART1);
   UartSetup( UART_UART1, 57600, UART_8BIT);
   UartTimeoutSet( UART_UART1, 1000, UART_TIMEOUT_OFF);
   UartModeSet( UART_UART1, UART_MODE_NATIVE_SLAVE);
   
   UartReceive( UART_UART1, Buffer, BUFFER_SIZE);
   forever {
      switch( UartReceiveStatus( UART_UART1)){
		 case UART_RECEIVE_ACTIVE:
            break;
			   
         case UART_RECEIVE_FRAME :
               RxSize = UartReceiveSize( UART_UART1);
			   if(RxSize) {
				  UartNativeWrite( UART_UART1, 4, "REC: ", 5);
				  
				  for(i = 0 ; i < RxSize ; i++) {
					  TxBuffer[i] = Buffer[i];
				  }
				  
				  UartNativeWrite( UART_UART1, 4, TxBuffer, RxSize);
			   }

               UartReceive( UART_UART1, Buffer, BUFFER_SIZE);
            break;
            
         default :
            UartReceive( UART_UART1, Buffer, BUFFER_SIZE);
			UartNativeWrite( UART_UART1, 4, "ERR", 3);
            break;
	  }            
   }*/
   
   
   
   

/*
   UartInit( UART_UART1);
   UartSetup( UART_UART1, 57600, UART_8BIT);
   UartFramingSet( UART_UART1, 'A', 'Z');              // ramec zacina znakem $, konci znakem CR
   UartTimeoutSet( UART_UART1, UART_TIMEOUT_OFF, UART_TIMEOUT_OFF); // nekonecne cekani na kompletaci ramci
   UartModeSet( UART_UART1, UART_MODE_ASCII_SLAVE);
   
   UartReceive( UART_UART1, Buffer, BUFFER_SIZE);  // Size je maximalni velikost Bufferu
   forever {
      switch( UartReceiveStatus( UART_UART1)){
		 case UART_RECEIVE_ACTIVE:
            break;
			   
         case UART_RECEIVE_FRAME :
               // prijata alespon nejaka data
               RxSize = UartReceiveSize( UART_UART1);   // pocet skutecne prijatych znaku
               //... zpracovani prijatych dat
			   if(RxSize) {
				  UartSend( UART_UART1, " AAZ REC: Z ", 12);
				  
				  for(i = 0 ; i < RxSize ; i++) {
					  TxBuffer[i] = Buffer[i];
				  }
				  
				  UartSend( UART_UART1, TxBuffer, RxSize);
			   }

               UartReceive( UART_UART1, Buffer, BUFFER_SIZE);
            break;
            
         default :
            UartReceive( UART_UART1, Buffer, BUFFER_SIZE);
            break;
	  }            
   }
   
   
   
*/


/*
   
   UartInit( UART_UART1);
   UartSetup( UART_UART1, 57600, UART_8BIT);
   UartTimeoutSet( UART_UART1, UART_TIMEOUT_OFF, 500);
   
   // pokud u�ivatel nezavol� alespo� jednou mode, nen� nainstalovan� ��dn� ISR
   UartModeSet( UART_UART1, UART_MODE_BINARY_SLAVE);
   
   UartReceive( UART_UART1, Buffer, BUFFER_SIZE);  // Size je maximalni velikost Bufferu
   
   forever {
         switch( UartReceiveStatus( UART_UART1)){
			case UART_RECEIVE_ACTIVE:
			   break;
			   
            case UART_RECEIVE_TIMEOUT :
               // prijata alespon nejaka data
               RxSize = UartReceiveSize( UART_UART1);   // pocet skutecne prijatych znaku
               //... zpracovani prijatych dat
			   if(RxSize) {
				  UartSend( UART_UART1, TxBuffer, 5);
			   }

               UartReceive( UART_UART1, Buffer, BUFFER_SIZE);
               break;
			   
			   
            default : // error, communication has been stopped
			   UartReceive( UART_UART1, Buffer, BUFFER_SIZE);
			   break;
		 }          
   }*/
}