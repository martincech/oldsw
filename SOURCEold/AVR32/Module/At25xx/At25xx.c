//*****************************************************************************
//
//   Flash.c     Atmel AT25DF641 Flash Memory Driver
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "System.h"
#include "At25xx.h"
#include "SpiExt.h"
#include "uconio.h"
void SpiInit() {             
   spi_options_t spiOptions = {
      .reg          = SPI_FLASH_NPCS,
      .baudrate     = SPI_FLASH_MASTER_SPEED,
      .bits         = 8,
      .spck_delay   = 0,
      .trans_delay  = 0,
      .stay_act     = 1,
      .spi_mode     = 0,
      .modfdis      = 1
   };

   
   SpiFlashPortInit();

   spi_initMaster( SPI_FLASH_DEVICE, &spiOptions);
   spi_selectionMode( SPI_FLASH_DEVICE, 0, 0, 0);
   spi_enable( SPI_FLASH_DEVICE);
   spi_setupChipReg( SPI_FLASH_DEVICE, &spiOptions, F_BUS_A);
}   
   
#define SpiSelect()        spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS)
#define SpiRelease()       spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS)  
#define SpiByteRead(ch)    SpiReadChar(SPI_FLASH_DEVICE, ch)
#define SpiByteWrite(ch)   spi_write(SPI_FLASH_DEVICE, ch)

//-----------------------------------------------------------------------------
// Flash Commands
//-----------------------------------------------------------------------------

#define		READ_ARRAY1			0x1B
#define		READ_ARRAY2			0x0B
#define		READ_ARRAY3			0x03
#define		DO_READ_ARRAY		0x3B

#define		BLOCK4_ERASE		0x20
#define		BLOCK32_ERASE		0x52
#define		BLOCK64_ERASE		0xD8
#define		CHIP_ERASE			0x60
#define		PROGRAM				0x02
#define		DI_PROGRAM			0xA2
#define		SUSPEND				0xB0
#define		RESUME				0xD0

#define		WRITE_ENABLE		0x06
#define		WRITE_DISABLE		0x04
#define		PROTECT_SECTOR		0x36
#define		UNPROTECT_SECTOR	0x39
#define		READ_PROTECT_REG	0x3C

#define		SECTOR_LOCKDOWN		0x33
#define		LOCKDOWN_STATE		0x34
#define		READ_LOCKDOWN_STATE	0x35
#define		PROGRAM_OTP			0x9B
#define		READ_OTP			0x77

#define		READ_STATUS			0x05
#define		WRITE_STATUS_BYTE1	0x01
#define		WRITE_STATUS_BYTE2	0x31

#define		RESET				0xF0
#define		READ_ID				0x9F
#define		POWER_DOWN			0xB9
#define		POWER_DOWN_RESUME	0xAB

//-----------------------------------------------------------------------------
// Status Register Bit Masks Definitions
//-----------------------------------------------------------------------------

// Ready/Busy Status
// 0 = ready,  1 = busy
#define		STATUS_BYTE1_RDY_BSY	(1 << 0)

// Write Enable Latch Status
// 0 = not write enabled (default), 1 = write enabled
#define		STATUS_BYTE1_WEL		(1 << 1)

// Software Protection Status
// 00 = all sectors unprotected, 01 = some sectors software protected
// 11 = all sectors software protected (default)
#define		STATUS_BYTE1_SWP0		(1 << 2)
#define		STATUS_BYTE1_SWP1		(1 << 3)
#define		STATUS_BYTE1_SWP		(3 << 2)

// Write Protect (WP) Pin Status
// 0 = WP asserted, 1 = WP deasserted
#define		STATUS_BYTE1_WPP		(1 << 4)

// Erase/Program Error
// 0 = erase/program successfull, 1 = erase/program error
#define		STATUS_BYTE1_EPE		(1 << 5)

// Sector Protection Registers Locked
// 0 = protection registers unlocked (default), 1 = protection registers locked
#define		STATUS_BYTE1_SPRL		(1 << 7)

// Ready/Busy Status
// 0 = ready,  1 = busy
#define		STATUS_BYTE2_RDY_BSY	(1 << 0)

// Erase Suspend Status
// 0 = no sectors erase suspended, 1 = a sector erase suspended
#define		STATUS_BYTE2_ES		(1 << 1)

// Program Suspend Status
// 0 = no sectors program suspended, 1 = a sector program suspended
#define		STATUS_BYTE2_PS		(1 << 2)

// Sector Lockdown Enabled
// 0 = disabled (default), 1 = enabled
#define		STATUS_BYTE2_SLE		(1 << 3)

// Reset Enabled
// 0 = disabled (default), 1 = enabled
#define		STATUS_BYTE2_RSTE		(1 << 4)



static void FlashStatusByte1Write(byte ch);
static TYesNo FlashIsBusy(void);
static void FlashWaitWhileBusy(word Timeout);
static byte FlashIDRead(void);
static void FlashWriteEnable(void);
static void FlashWriteDisable(void);
static void FlashErase(dword Address, byte Command);

//-----------------------------------------------------------------------------
// Flash is busy
//-----------------------------------------------------------------------------

static TYesNo FlashIsBusy(void)
// Test whether flash memory is busy
{
   if(FlashStatusRead() & STATUS_BYTE1_RDY_BSY) {
      return YES;
   } else {
      return NO;
   }
} // FlashIsBusy

//-----------------------------------------------------------------------------
// Wait while flash is busy
//-----------------------------------------------------------------------------

static void FlashWaitWhileBusy(word Timeout)
// Wait till flash memory is idle
// dod�lat timeout...
{
   byte ch;
   
   SpiSelect();
   SpiByteWrite(READ_STATUS);

   do {
	  SpiByteRead(&ch);
   }
   while(ch & STATUS_BYTE1_RDY_BSY);

   SpiRelease();
} // FlashWaitWhileBusy

//-----------------------------------------------------------------------------
// Write enable
//-----------------------------------------------------------------------------

static void FlashWriteEnable(void)
// Enable write latch
{ 
   SpiSelect();
   SpiByteWrite(WRITE_ENABLE);
   SpiRelease();
} // FlashWriteEnable

//-----------------------------------------------------------------------------
// Write disable
//-----------------------------------------------------------------------------

static void FlashWriteDisable(void)
// Disable write latch
{ 
   SpiSelect();
   SpiByteWrite(WRITE_DISABLE);
   SpiRelease();
} // FlashWriteDisable

//-----------------------------------------------------------------------------
// Read status register
//-----------------------------------------------------------------------------

word FlashStatusRead(void)
// Read flash memory status register
{
	word temp = 0;
    byte ch;
	
	SpiSelect();
	SpiByteWrite(READ_STATUS);
	while(!spi_writeEndCheck(SPI_FLASH_DEVICE));
	SpiByteRead(&ch);
	temp = (byte) ch;
	SpiByteRead(&ch);
	temp |= (((byte) ch) << 8);
	SpiRelease();

	return temp;
} // FlashStatusRead

//-----------------------------------------------------------------------------
// Write status register
//-----------------------------------------------------------------------------

static void FlashStatusByte1Write(byte ch)
// Write flash memory status register - byte 1
{
   FlashWaitWhileBusy(0);

   FlashWriteEnable();

   SpiSelect();
   SpiByteWrite(WRITE_STATUS_BYTE1);
   SpiByteWrite(ch);
   SpiRelease();
} // FlashStatusByte1Write

//-----------------------------------------------------------------------------
// Read device ID
//-----------------------------------------------------------------------------

static byte FlashIDRead(void)
// Read flash Family Code and Density Code
{
   byte ch;

   SpiSelect();
   SpiByteWrite(READ_ID);
   SpiByteRead(&ch);
   SpiByteRead(&ch);
   SpiRelease();

   return ch;
} // FlashIDRead

//-----------------------------------------------------------------------------
// Erase
//-----------------------------------------------------------------------------

static void FlashErase(dword Address, byte Command)
// Erase one block specified by Address, size of the block specified by Command
// Command must be either BLOCK4_ERASE, BLOCK32_ERASE or BLOCK64_ERASE
{
   FlashWaitWhileBusy(0);

   FlashWriteEnable();

   SpiSelect();
   SpiByteWrite(Command);
   SpiByteWrite((byte) (Address >> 16));
   SpiByteWrite((byte) (Address >> 8 ));
   SpiByteWrite((byte) (Address >> 0 ));
   SpiRelease();
} // FlashErase
  
//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void FlashInit(void)
// Flash memory init
// Allow 10 ms between flash power-up and first program/erase
{
   SpiInit();
   
   FlashHoldDeassert(); // HOLD = 1

   FlashUnprotect();
} // FlashInit

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

void FlashRead(dword Address, byte *Buffer, word Count)
// Read data from flash
{
   word i;
   byte ch;
   
   FlashWaitWhileBusy(0);
   
   SpiSelect();
   SpiByteWrite(READ_ARRAY1);
   SpiByteWrite((byte) (Address >> 16));
   SpiByteWrite((byte) (Address >> 8 ));
   SpiByteWrite((byte) (Address >> 0 ));
   SpiByteWrite(0xFF);
   SpiByteWrite(0xFF);
   
   for(i = 0 ; i < Count ; i++) {
	  SpiByteRead(&ch);
      Buffer[i] = ch;
   }
   
   SpiRelease();
} // FlashRead

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void FlashWrite(dword Address, byte *Buffer, word Count)
// Write data to flash
// Pozor pokud by data mela prekrocit 256bytovou stranku
// dalsi zapis by pokracoval na zacatku te same stranky
// = nechtenny prepis puvodne ulozenych dat
{
   word i;

   if(Count > 256) {
      Count = 256;
   }

   FlashWaitWhileBusy(0);

   FlashWriteEnable();

   SpiSelect();
   SpiByteWrite(PROGRAM);
   SpiByteWrite((byte) (Address >> 16));
   SpiByteWrite((byte) (Address >> 8 ));
   SpiByteWrite((byte) (Address >> 0 ));

   for(i = 0 ; i < Count ; i++) {
      SpiByteWrite(Buffer[i]);
   }

   SpiRelease();
} // FlashWrite

//-----------------------------------------------------------------------------
// Erase 4 kb
//-----------------------------------------------------------------------------

void FlashBlock4Erase(dword Address)
// Erase 4kb block specified by address
// takto definovane funkce (FlashBlock4Erase vola FlashErase) nejsou idealni
// zbytecne volani dvou funkci by bylo lepsi nahradit volanim jednim
// pomoci #define ???
{
   FlashErase(Address, BLOCK4_ERASE);
} // FlashBlock4Erase

//-----------------------------------------------------------------------------
// Erase 32 kb
//-----------------------------------------------------------------------------

void FlashBlock32Erase(dword Address)
// Erase 32kb block specified by address
{
   FlashErase(Address, BLOCK32_ERASE);
} // FlashBlock32Erase

//-----------------------------------------------------------------------------
// Erase 64 kb
//-----------------------------------------------------------------------------

void FlashBlock64Erase(dword Address)
// Erase 64kb block specified by address
{
   FlashErase(Address, BLOCK64_ERASE);
} // FlashBlock64Erase

//-----------------------------------------------------------------------------
// Erase chip
//-----------------------------------------------------------------------------

void FlashChipErase(void)
// Erase entire flash
{
   FlashWaitWhileBusy(0);

   FlashWriteEnable();

   SpiSelect();
   SpiByteWrite(CHIP_ERASE);
   SpiRelease();
} // FlashChipErase

//-----------------------------------------------------------------------------
// Flash is protected
//-----------------------------------------------------------------------------

TYesNo FlashIsProtected(void)
// Test whether some sectors are protected
{
   if(FlashStatusRead() & STATUS_BYTE1_SWP) {
      return YES;
   } else {
      return NO;
   }
} // FlashIsProtected

//-----------------------------------------------------------------------------
// Unprotect flash
//-----------------------------------------------------------------------------

void FlashUnprotect(void)
// Globally unprotect flash memory
{
   FlashWPDeassert(); // make sure WP = 1
   
   // make sure SPRL = 0
   FlashStatusByte1Write(0);
   
   // global unprotect
   FlashStatusByte1Write(0);
} // FlashUnprotect