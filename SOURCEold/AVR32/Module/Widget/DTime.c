//******************************************************************************
//
//  DTime.c        Display time
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Wgt/Dtime.h"
#include "conio.h"         // putchar
#include "xTime.h"         // date & time

//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

void DTime( TTimestamp Now)
// Display time only
{
   xTime( putchar, Now);
} // DTime

//------------------------------------------------------------------------------
//  Short Time
//------------------------------------------------------------------------------

void DTimeShort( TTimestamp Now)
// Display time without seconds
{
   xTimeShort( putchar, Now);
} // DTimeShort

//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

void DDate( TTimestamp Now)
// Display date only
{
   xDate( putchar, Now);
} // DDate

//------------------------------------------------------------------------------
//  Date & Time
//------------------------------------------------------------------------------

void DDateTime( TTimestamp Now)
// Display date and time
{
   xDateTime( putchar, Now);
} // DDateTime

//------------------------------------------------------------------------------
//  Short Date & Time
//------------------------------------------------------------------------------

void DDateTimeShort( TTimestamp Now)
// Display date and time without seconds
{
   xDateTimeShort( putchar, Now);
} // DDateTimeShort
