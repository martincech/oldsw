//******************************************************************************
//
//  DProgress.c    Display progress bar
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Wgt/DProgress.h"
#include "Graphic.h"

//------------------------------------------------------------------------------
//  Progress
//------------------------------------------------------------------------------

void DProgress( int Percent, int x, int y, int Width, int Height)
// Display progress bar with <Percent>
{
   GSetColor( DCOLOR_PROGRESS);
   GRectangle( x, y, Width, Height);
   GSetColor( DCOLOR_PROGRESS_BAR);
   DProgressBar( Percent, x + 1, y + 1, Width - 2, Height - 2);
   GSetColor( DCOLOR_DEFAULT);
} // DProgress

//------------------------------------------------------------------------------
//  Progress right
//------------------------------------------------------------------------------

void DProgressRight( int Percent, int x, int y, int Width, int Height)
// Display progress bar with <Percent> from right to left
{
   GSetColor( DCOLOR_PROGRESS);
   GRectangle( x, y, Width, Height);
   GSetColor( DCOLOR_PROGRESS_BAR);
   DProgressBarRight( Percent, x + 1, y + 1, Width - 2, Height - 2);
   GSetColor( DCOLOR_DEFAULT);
} // DProcessRight

//------------------------------------------------------------------------------
//  Progress Bar
//------------------------------------------------------------------------------

void DProgressBar( int Percent, int x, int y, int Width, int Height)
// Display progress bar only with <Percent>
{
   GBox( x, y, (Width * Percent) / 100, Height);
} // DProgressBar

//------------------------------------------------------------------------------
//  Progress Bar right
//------------------------------------------------------------------------------

void DProgressBarRight( int Percent, int x, int y, int Width, int Height)
// Display progress bar only with <Percent> from right to left
{
int XBox;

   XBox = ((100 - Percent) * Width) / 100;
   GBox( x + XBox, y, Width - XBox, Height);
} // DProcessBarRight
