//******************************************************************************
//
//   DEnterList.c   Enter string list
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Wgt/DEnterList.h"
#include "Wgt/DEvent.h"
#include "Wgt/DLabel.h"
#include "Wgt/DProgress.h"
#include "Wgt/DGrid.h"
#include "Wgt/Beep.h"
#include "Graphic.h"
#include "Bcd.h"
#include "conio.h"
#include "Str.h"                        // strings from project directory

// Local functions :

static int GetListCount( const TUniStr *List);
// Returns number of items

//------------------------------------------------------------------------------
//  Enter List
//------------------------------------------------------------------------------

TYesNo DEnterList( int *InitialValue, const TUniStr *List, int x, int y, TCenterType Center)
// Display and edit <InitialValue>
{
int Width;
int Value;
int OldValue;
int ListCount;

   Width     = DEnterListWidth( List);
   ListCount = GetListCount( List);
   Value     = *InitialValue;
   OldValue  = -1;
   forever {
      if( Value != OldValue){
         // clear area
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y, Width, DENTER_H);
         GSetColor( DCOLOR_ENTER);
         DLabelListCenter( Value, List, x, y, Width, Center);
         GSetColor( DCOLOR_DEFAULT);
         OldValue = Value;             // remember value
         GFlush();                     // redraw
      }
      switch( DEventWait()){
         case K_UP | K_REPEAT :
         case K_UP :
            if( Value >= ListCount - 1){
               BeepError();
               break;
            }
            BeepKey();
            Value++;
            break;

         case K_DOWN | K_REPEAT :
         case K_DOWN :
            if( Value == 0){
               BeepError();
               break;
            }
            BeepKey();
            Value--;
            break;

         case K_ENTER :
            BeepKey();
            *InitialValue = Value;
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DEnterList

//------------------------------------------------------------------------------
//  List width
//------------------------------------------------------------------------------

int DEnterListWidth( const TUniStr *List)
// Returns pixel width of <List>
{
   return( DLabelListWidth( List) + 2);
} // DEnterListWidth

//------------------------------------------------------------------------------
//  Spinner
//------------------------------------------------------------------------------

TYesNo DEnterSpin( int *InitialValue, int MinValue, int MaxValue, TAction *OnChange, int x, int y)
// Enter value by spinner
{
int Width;
int Value;
int OldValue;
int Decimals;

   Width     = DEnterSpinWidth( MaxValue);
   Decimals  = FmtSetWidth( BcdGetWidth( MaxValue));
   Value     = *InitialValue;
   OldValue  = -1;
   forever {
      if( Value != OldValue){
         // clear area
         GSetColor( DCOLOR_ENTER_BG);
         GBox( x, y,  Width, DENTER_H);
         GSetColor( DCOLOR_ENTER);
         GSetNumericPitch();           // set nonproportional/numeric font
         GTextAt( x + 1, y);
         cprintdec( Value, Decimals);
         GSetNormalPitch();            // restore font setting
         GSetColor( DCOLOR_DEFAULT);
         OldValue = Value;             // remember value
         GFlush();                     // redraw
         if( OnChange){
            (*OnChange)( Value);       // callback
         }
      }
      switch( DEventWait()){
         case K_UP | K_REPEAT :
         case K_UP :
            if( Value >= MaxValue){
               BeepError();
               break;
            }
            BeepKey();
            Value++;
            break;

         case K_DOWN | K_REPEAT :
         case K_DOWN :
            if( Value <= MinValue){
               BeepError();
               break;
            }
            BeepKey();
            Value--;
            break;

         case K_ENTER :
            BeepKey();
            *InitialValue = Value;
            return( YES);

         case K_ESC :
            BeepKey();
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DEnterSpin

//------------------------------------------------------------------------------
//  Spinner width
//------------------------------------------------------------------------------

int DEnterSpinWidth( int MaxValue)
// Returns pixel width of spinner field
{
   return( BcdGetWidth( MaxValue) * GNumberWidth() + 1);
} // DEnterSpinWidth

//------------------------------------------------------------------------------
//  List count
//------------------------------------------------------------------------------

static int GetListCount( const TUniStr *List)
// Returns number of items
{
int Count;

   Count = 0;
   while( List[ Count]){
      Count++;
   }
   return( Count);
} // GetListCount
