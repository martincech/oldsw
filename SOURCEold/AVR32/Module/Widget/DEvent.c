//******************************************************************************
//
//   DEvent.c       Process events
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Wgt/DEvent.h"
#include "Wgt/Beep.h"
#include "System.h"

static int _Event = K_NULL;            // event buffer

//------------------------------------------------------------------------------
//  Discard
//------------------------------------------------------------------------------

void DEventDiscard( void)
// Discard waiting events
{
   _Event = K_NULL;                    // sink the event
} // DEventDiscard

//------------------------------------------------------------------------------
//  Wait
//------------------------------------------------------------------------------

int DEventWait( void)
// Wait for event
{
   forever {
      // last event was Timeout, loop indefinitely :
      if( _Event == K_TIMEOUT) {
         SysScheduler();                         // process yield
         return( K_TIMEOUT);                     // repeat again
      }
      _Event = SysWaitEvent();
      if( _Event == K_SHUTDOWN){
         _Event = K_TIMEOUT;                     // for menu translate to timeout
      }
      if( _Event == K_TIMEOUT){
         BeepTimeout();
      }
      return( _Event);
   }
} // DEventWait

//------------------------------------------------------------------------------
//  Enter
//------------------------------------------------------------------------------

void DEventWaitForEnter( void)
// Wait for Enter key
{
   forever {
      switch( DEventWait()){
         case K_ENTER :
         case K_ESC :
            BeepKey();
            return;
         case K_TIMEOUT :
            return;
      }
   }
} // DEventWaitForEnter

//------------------------------------------------------------------------------
//  Enter / Esc
//------------------------------------------------------------------------------

TYesNo DEventWaitForEnterEsc( void)
// Wait for Enter/Esc key
{
   forever {
      switch( DEventWait()){
         case K_ENTER :
            BeepKey();
            return( YES);
         case K_ESC :
            BeepKey();
            return( NO);
         case K_TIMEOUT :
            return( NO);
      }
   }
} // DEventWaitForEnterEsc
