//******************************************************************************
//
//  DLabel.c       Display text label
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Wgt/DLabel.h"
#include "Graphic.h"
#include "conio.h"
#include "xprint.h"
#include "sputchar.h"

//------------------------------------------------------------------------------
//  Label
//------------------------------------------------------------------------------

void DLabel( TUniStr Text, int x, int y)
// Display <Text> at <x,y>
{
   if( !Text){
      return;                // empty
   }
   GTextAt( x, y);
   cputsn( StrGet( Text));
} // DLabel

//------------------------------------------------------------------------------
//  Centered Label
//------------------------------------------------------------------------------

void DLabelCenter( TUniStr Text, int x, int y, int Width, int Height)
// Display <Text> centered at rectangle <x,y> with <Width,Height>
{
const char *p;
int  TxtHeight, TxtWidth;

   if( !Text){
      return;                // empty
   }
   p = StrGet( Text);
   if( Height > 0){
      // vertical centering
      TxtHeight = GTextHeight( p);
   } else {
      // omit vertical centering
      TxtHeight = 0;
   }
   if( Width > 0){
      // horizontal centering
      TxtWidth  = GTextWidth( p);
      if( Width < TxtWidth){
         Width = TxtWidth;
      }
   } else {
      // omit horizontal centering
      TxtWidth  = 0;
   }
   GTextAt( x + Width / 2 - TxtWidth / 2, y + Height / 2 - TxtHeight / 2);
   cputsn( p);
} // DLabelCenter

//------------------------------------------------------------------------------
//  Label Right
//------------------------------------------------------------------------------

void DLabelRight( TUniStr Text, int x, int y)
// Display <Text> with upper right corner at <x,y>
{
const char *p;
int         TxtWidth;

   if( !Text){
      return;                // empty
   }
   p = StrGet( Text);
   TxtWidth  = GTextWidth( p);
   GTextAt( x - TxtWidth, y);
   cputsn( p);
} // DLabelRight

//------------------------------------------------------------------------------
//  Label Format
//------------------------------------------------------------------------------

void DLabelFormat( int x, int y, const char *Format, ...)
// Display label by <Format> with current alignment
{
char Buffer[ DLABEL_MAX_LENGTH + 1];
va_list arg;

   va_start( arg, Format);
   sputcharbuffer( Buffer);
   xvprintf( sputchar, Format, arg);
   sputchar( '\0');                    // string terminator
#ifndef DALIGN_RIGHT
   DLabel( Buffer, x, y);
#else
   DLabelRight( Buffer, x, y);
#endif
} // DLabelFormat

//------------------------------------------------------------------------------
//  Fixed label
//------------------------------------------------------------------------------

void DLabelFixed( TUniStr Text, int x, int y)
// Display <Text> at <x,y> with fixed pitch
{
   if( !Text){
      return;                // empty
   }
   GSetFixedPitch();
   GTextAt( x, y);
   cputsn( StrGet( Text));
   GSetNormalPitch();
} // DLabelFixed

//------------------------------------------------------------------------------
//  Height
//------------------------------------------------------------------------------

int DLabelHeight( TUniStr Text)
// Returns height of <Text>
{
   if( !Text){
      return( 0);             // empty
   }
   return( GTextHeight( StrGet( Text)));
} // DLabelHeight

//------------------------------------------------------------------------------
//  Width
//------------------------------------------------------------------------------

int DLabelWidth( TUniStr Text)
// Returns width of <Text>
{
   if( !Text){
      return( 0);             // empty
   }
   return( GTextWidth( StrGet( Text)));
} // DLabelWidth

//------------------------------------------------------------------------------
//  Enum
//------------------------------------------------------------------------------

void DLabelEnum( int Enum, TUniStr Base, int x, int y)
// Display <Enum> value at <x,y> with current alignment
{
#ifndef DALIGN_RIGHT
   DLabel( Base + Enum, x, y);
#else
   DLabelRight( Base + Enum, x, y);
#endif
} // DLabelEnum

//------------------------------------------------------------------------------
//  Enum center
//------------------------------------------------------------------------------

void DLabelEnumCenter( int Enum, TUniStr Base, int x, int y, int Width, TCenterType Center)
// Display <Enum> value at <x,y> with <Width> by <Center> mode
{
   switch( Center){
      case CENTER_LEFT :
         DLabel( Base + Enum, x, y);
         break;
      case CENTER_MIDDLE :
         DLabelCenter( Base + Enum, x, y, Width, 0);
         break;
      case CENTER_RIGHT :
         DLabelRight( Base + Enum, x + Width, y);
         break;
   }
} // DLabelEnumCenter

//------------------------------------------------------------------------------
//  Enum width
//------------------------------------------------------------------------------

int DLabelEnumWidth( TUniStr Base, int EnumCount)
// Get enum width
{
int Width;
int MaxWidth;
int i;

   // get maximal width :
   MaxWidth = 0;
   for( i = 0; i < EnumCount; i++){
      Width = DLabelWidth( Base + i);
      if( Width > MaxWidth){
         MaxWidth = Width;
      }
   }
   return( MaxWidth);
} // DLabelEnumWidth

//------------------------------------------------------------------------------
//  List
//------------------------------------------------------------------------------

void DLabelList( int Index, const TUniStr *List, int x, int y)
// Display <Index> value by <List> at <x,y> with current alignment
{
#ifndef DALIGN_RIGHT
   DLabel( List[ Index], x, y);
#else
   DLabelRight( List[ Index], x, y);
#endif
} // DLabelList

//------------------------------------------------------------------------------
//  List center
//------------------------------------------------------------------------------

void DLabelListCenter( int Index, const TUniStr *List, int x, int y, int Width, TCenterType Center)
// Display <List> value at <x,y> with <Width> by <Center> mode
{
   switch( Center){
      case CENTER_LEFT :
         DLabel( List[ Index], x, y);
         break;
      case CENTER_MIDDLE :
         DLabelCenter( List[ Index], x, y, Width, 0);
         break;
      case CENTER_RIGHT :
         DLabelRight( List[ Index], x + Width, y);
         break;
   }
} // DLabelListCenter

//------------------------------------------------------------------------------
//  List width
//------------------------------------------------------------------------------

int DLabelListWidth( const TUniStr *List)
// Get <List> pixel width
{
int Width;
int MaxWidth;

   MaxWidth = 0;
   while( *List){
      Width = DLabelWidth( *List);
      if( Width > MaxWidth){
         MaxWidth = Width;
      }
      List++;
   }
   return( MaxWidth);
} // DLabelListWidth
