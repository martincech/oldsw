//******************************************************************************
//
//  StrUtil.c      String utilities
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "Wgt/StrDef.h"
#include <string.h>

//------------------------------------------------------------------------------
//  Get
//------------------------------------------------------------------------------

const char *StrGet( TUniStr Str)
// Get translated string
{
int Index;

   if( (Str < _STR_LOW_LIMIT) || (Str > _STR_HIGH_LIMIT)){
      // SRAM string or FLASH string
      return( Str);                    // without translation
   }
   Index  = (int)Str;                  // convert to index
   Index -= (int)_STR_LOW_LIMIT;       // move to base 0
#if LANGUAGE_COUNT > 1
   return( AllStrings[ Index][ ActiveLanguage]);
#else
   return( AllStrings[ Index]);
#endif
} // StrGet

//------------------------------------------------------------------------------
//  Trim right
//------------------------------------------------------------------------------

void StrTrimRight( char *String)
// Remove right spaces
{
int i;

   i = strlen( String);
   while( --i >= 0){
      if( String[ i] != ' '){
         return;
      }
      String[ i] = '\0';
   }
} // StrTrimRight

//------------------------------------------------------------------------------
//  Set width
//------------------------------------------------------------------------------

void StrSetWidth( char *String, int Width)
// Extend text with spaces, up to <Width>
{
int i;

   i = strlen( String);
   while( i < Width){
      String[ i] = ' ';
      i++;
   }
   String[ i] = '\0';
} // StrSetWidth
