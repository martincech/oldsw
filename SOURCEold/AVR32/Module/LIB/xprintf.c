//*****************************************************************************
//
//    xprintf.c    Simple formatted output template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"                  // conditional compilation
#include "xprint.h"
#include "Bcd.h"
#ifdef PRINTF_STRING
   #include "wgt/StrDef.h"
#endif


//-----------------------------------------------------------------------------
// xprintf
//-----------------------------------------------------------------------------

void xprintf( TPutchar *xputchar, const char *Format, ...)
// Simple formatted output
{
va_list arg;

   va_start( arg, Format);
   xvprintf( xputchar, Format, arg);
} // xprintf

//-----------------------------------------------------------------------------
// xvprintf
//-----------------------------------------------------------------------------

void xvprintf( TPutchar *xputchar, const char *Format, va_list Arg)
// Simple formatted output
{
int       Width;
int       Decimals;
unsigned  Flags;
char      ch;

#ifdef PRINTF_STRING
   Format = StrGet( Format);
#endif
   while( *Format){
      ch = *Format;
      if( ch != '%'){
         xputchar( ch);
         Format++;
         continue;
      }
      ch = *(++Format);                          // skip %
      Width    = 0;                              // without limit
      Decimals = 0;                              // Decimals
      Flags    = 0;                              // no additional Flags
      if( ch == '+'){
         Flags |= FMT_PLUS;
         ch = *(++Format);
      }
      // leading zero :
      if( ch == '0'){
         Flags |= FMT_LEADING_0;
         ch = *(++Format);
      }
      // total Width :
      if( (*Format >= '1') &&  (*Format <= '9')){
         Width = char2dec( ch);
         ch = *(++Format);
         if( (ch >= '0') &&  (ch <= '9')){
            Width *= 10;
            Width += char2dec( ch);
            ch = *(++Format);
         }
      }
      // Decimals :
      if( ch == '.'){
         ch = *(++Format);
         if( (ch >= '1') &&  (ch <= '9')){
            Decimals = char2dec( ch);
            ch = *(++Format);
         }
      }
      // variable type :
      switch( ch){
         case 'f' :
            xfloat( xputchar, va_arg( Arg, int), Width, Decimals, Flags);
            break;
         case 'x' :
            xprinthex( xputchar, (dword)va_arg( Arg, unsigned), Width | Flags);
            break;

         case 'u' :
            Width |= FMT_UNSIGNED;
            xprintdec( xputchar, (dword)va_arg( Arg, unsigned), Width | Flags);
            break;

         case 'd' :
            xprintdec( xputchar, (int32)va_arg( Arg, int), Width | Flags);
            break;

         case 'c' :
            xputchar( va_arg( Arg, int));
            break;

         case 's' :
            xputs( xputchar, va_arg( Arg, char *));
            break;

         case '%' :
         default :
            xputchar( ch);                        // single character only
            break;
      }
      Format++;
   }
   va_end( Arg);
} // xvprintf
