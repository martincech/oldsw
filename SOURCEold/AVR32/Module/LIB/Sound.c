//*****************************************************************************
//
//    Sound.c      Sound creation wia PWM hardware
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Sound.h"
#include "System.h"
#include "Cpu.h"
#include "Pwm.h"

volatile short _SoundDuration;          // duration counter

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void SoundInit( void)
// Initialisation
{
   PwmInit( SOUND_CHANNEL);
} // SoundInit

//-----------------------------------------------------------------------------
// Pipnuti
//-----------------------------------------------------------------------------

void SoundBeep( int Note, int Volume, int Duration)
// Plays asynchronously <Note> with <Volume> and <Duration> [ms]
{
   _SoundDuration = (Duration) / TIMER_PERIOD;
   SoundSound( Note, Volume);
}  // SoundBeep

//-----------------------------------------------------------------------------
// Synchronni pipnuti
//-----------------------------------------------------------------------------

void SoundSBeep( int Note, int Volume, int Duration)
// Plays synchronously <Note> with <Volume> and <Duration> [ms]
{
   SoundSound( Note, Volume);
   SysDelay( Duration);
   SoundNosound();
   SysDelay(10);
} // SoundSBeep

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void SoundNosound( void)
// Stops sound (inside interrupt)
{
   PwmStop( SOUND_CHANNEL);
} // SoundNosound

//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------

static const word _Frequency[ 12] = {
   /* NOTE_C4   */ 20930,
   /* NOTE_CIS4 */ 22175,
   /* NOTE_D4   */ 23493,
   /* NOTE_DIS4 */ 24890,
   /* NOTE_E4   */ 26370,
   /* NOTE_F4   */ 27938,
   /* NOTE_FIS4 */ 29600,
   /* NOTE_G4   */ 31360,
   /* NOTE_GIS4 */ 33224,
   /* NOTE_A4   */ 35200,
   /* NOTE_AIS4 */ 37293,
   /* NOTE_H4   */ 39511,
};

static const word _Gain[ SOUND_VOLUME_MAX + 1] = {
// 0,630,880,1250,1760,2500,3530,5000,7070,10000};    // 3dB step
   0,250,400,630,1000,1580,2510,3980,6310,10000};     // 4dB step
// 0,40,80,160,310,630,1250,2500,5000,10000};         // 6dB step

// Period ticks by frequency [0.1Hz] :
#define SoundFrequencyTicks( Frequency)  (PWM_FREQUENCY * 10 / (Frequency))

void SoundSound( int Note, int Volume)
// Starts play <Note> with <Volume>
{
unsigned Duty;
unsigned Period;
unsigned Octave;

   if( Volume == 0){
      SoundNosound();                                           // silent
      return;
   }
   Octave = Note / 12;                                          // get octave 0..OCTAVE_MAX
   Note  %= 12;                                                 // get note in octave
   if( Octave > SOUND_OCTAVE_MAX){
      Period = SoundFrequencyTicks( 2 * _Frequency[ Note]);     // play C5..H5 instead
   } else {
      Period = SoundFrequencyTicks( _Frequency[ Note] / (1 << (SOUND_OCTAVE_MAX - Octave)));
   }
   Duty   = (_Gain[ Volume] * PWM_DUTY_MAX) / 20000;            // pulse width (max 50%)
   if( Duty < 1){
      Duty = 1;                                                 // minimal width
   }
   PwmStart( SOUND_CHANNEL, Period, Duty);
} // SoundSound
