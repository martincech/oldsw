//*****************************************************************************
//
//    conio.c - simple formatted output
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"           // podminena kompilace
#include "conio.h"
#include "xprint.h"
#include "sputchar.h"

#ifdef CONIO_FLUSH
   #include "Graphic.h"
   #define Flush()  GFlush()
#else
   #define Flush()
#endif

//-----------------------------------------------------------------------------
// Znak
//-----------------------------------------------------------------------------

void cputch( char ch)
// vystup znaku <ch>
{
   putchar( ch);
   Flush();
} // cputch

//-----------------------------------------------------------------------------
// Retezec
//-----------------------------------------------------------------------------

void cputs( const char *string)
// vystup retezce <string>
{
   xputs( putchar, string);
   Flush();
} // cputs

//-----------------------------------------------------------------------------
// Retezec
//-----------------------------------------------------------------------------

void cputsn( const char *string)
// vystup retezce <string> az po LF
{
   xputsn( putchar, string);
   Flush();
} // cputs

//-----------------------------------------------------------------------------
// Hexadecimalni cislo
//-----------------------------------------------------------------------------

void cprinthex( dword x, dword flags)
// vystup hexa cisla <x>, <flags> koduje sirku
{
   xprinthex( putchar, x, flags);
   Flush();
} // cprinthex

//-----------------------------------------------------------------------------
// Dekadicke cislo
//-----------------------------------------------------------------------------

void cprintdec( int32 x, dword flags)
// vystup dekadickeho cisla <x>, <flags> koduje sirku
{
   xprintdec( putchar, x, flags);
   Flush();
} // cprintdec

//-----------------------------------------------------------------------------
// Float
//-----------------------------------------------------------------------------

void cfloat( int x, int w, int d, dword flags)
// Tiskne binarni cislo <x> jako float s celkovou sirkou <w>
// a <d> desetinnymi misty. <flags> jsou doplnujici atributy
// (FMT_PLUS)
{
   xfloat( putchar, x, w, d, flags);
   Flush();
} // cfloat

//-----------------------------------------------------------------------------
// Formatovany vystup
//-----------------------------------------------------------------------------

void cprintf( const char *Format, ...)
// jednoduchy formatovany vystup
{
va_list arg;

   va_start( arg, Format);
   xvprintf( putchar, Format, arg);
   Flush();
} // cprintf

#ifdef PRINTF_BUFFER
//-----------------------------------------------------------------------------
// Formatovany vystup do bufferu
//-----------------------------------------------------------------------------

void bprintf( char *Buffer, const char *Format, ...)
// formatovany vystup do <Bufferu>
{
va_list arg;

   va_start( arg, Format);
   sputcharbuffer( Buffer);
   xvprintf( sputchar, Format, arg);
   sputchar( '\0');                    // string terminator
} // sprintf
#endif
