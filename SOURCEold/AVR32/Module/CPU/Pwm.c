//*****************************************************************************
//
//    Pwm.c        PWM controller
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Pwm.h"

// Missing AVR32 PWM defines :
#define PWM_MODE_LEFT_ALIGNED   0      // Operate PWM channel in left aligned mode
#define PWM_MODE_CENTER_ALIGNED 1      // Operate PWM channel in center aligned mode
#define PWM_POLARITY_LOW        0      // PWM channel starts output low level
#define PWM_POLARITY_HIGH       1      // PWM channel starts output high level
#define PWM_UPDATE_DUTY         0      // PWM channel write in CUPDx updates duty cycle
#define PWM_UPDATE_PERIOD       1      // PWM channel write in CUPDx updates period

//-----------------------------------------------------------------------------

#define CMR_DEFAULT   ((AVR32_PWM_CPRE_MCK    << AVR32_PWM_CMR_CPRE_OFFSET) | \
                       (PWM_MODE_LEFT_ALIGNED << AVR32_PWM_CMR_CALG_OFFSET) | \
                       (PWM_UPDATE_DUTY       << AVR32_PWM_CMR_CPD_OFFSET)  | \
                       (PWM_POLARITY_LOW      << AVR32_PWM_CMR_CPOL_OFFSET))

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void PwmInit( int Channel)
// Initialisation
{
volatile avr32_pwm_channel_t *Pwm;
unsigned Mode;

   AVR32_PWM.mr  = 0;                  // disable CLK A, B
   AVR32_PWM.dis = 1 << Channel;       // disable channel
   Pwm           = &AVR32_PWM.channel[ Channel];
   Mode          = CMR_DEFAULT;
   switch( Channel){
      #ifdef PWM0_ACTIVE
      case PWM_CHANNEL_0 :
         GpioFunction( AVR32_PWM_0_PIN, AVR32_PWM_0_FUNCTION);
         #if PWM0_ACTIVE == 0
            Mode |= PWM_POLARITY_HIGH << AVR32_PWM_CMR_CPOL_OFFSET;
         #endif
         break;
      #endif
      #ifdef PWM1_ACTIVE
      case PWM_CHANNEL_1 :
         GpioFunction( AVR32_PWM_1_PIN, AVR32_PWM_1_FUNCTION);
         #if PWM1_ACTIVE == 0
            Mode |= PWM_POLARITY_HIGH << AVR32_PWM_CMR_CPOL_OFFSET;
         #endif
         break;
      #endif
      #ifdef PWM2_ACTIVE
      case PWM_CHANNEL_2 :
         GpioFunction( AVR32_PWM_2_PIN, AVR32_PWM_2_FUNCTION);
         #if PWM2_ACTIVE == 0
            Mode |= PWM_POLARITY_HIGH << AVR32_PWM_CMR_CPOL_OFFSET;
         #endif
         break;
      #endif
      #ifdef PWM3_ACTIVE
      case PWM_CHANNEL_3 :
         GpioFunction( AVR32_PWM_3_PIN, AVR32_PWM_3_FUNCTION);
         #if PWM3_ACTIVE == 0
            Mode |= PWM_POLARITY_HIGH << AVR32_PWM_CMR_CPOL_OFFSET;
         #endif
         break;
      #endif
      #ifdef PWM4_ACTIVE
      case PWM_CHANNEL_4 :
         GpioFunction( AVR32_PWM_4_1_PIN, AVR32_PWM_4_1_FUNCTION);
         #if PWM4_ACTIVE == 0
            Mode |= PWM_POLARITY_HIGH << AVR32_PWM_CMR_CPOL_OFFSET;
         #endif
         break;
      #endif
      #ifdef PWM5_ACTIVE
      case PWM_CHANNEL_5 :
         GpioFunction( AVR32_PWM_5_1_PIN, AVR32_PWM_5_1_FUNCTION);
         #if PWM5_ACTIVE == 0
            Mode |= PWM_POLARITY_HIGH << AVR32_PWM_CMR_CPOL_OFFSET;
         #endif
         break;
      #endif
      #ifdef PWM6_ACTIVE
      case PWM_CHANNEL_6 :
         GpioFunction( AVR32_PWM_6_PIN, AVR32_PWM_6_FUNCTION);
         #if PWM6_ACTIVE == 0
            Mode |= PWM_POLARITY_HIGH << AVR32_PWM_CMR_CPOL_OFFSET;
         #endif
         break;
      #endif
      default :
         return;
   }
   Pwm->cmr      = Mode;
} // PwmInit

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void PwmStop( int Channel)
// Stop PWM
{
   AVR32_PWM.dis =  1 << Channel;
} // PwmStop

//-----------------------------------------------------------------------------
// Run
//-----------------------------------------------------------------------------

void PwmStart( int Channel, unsigned PeriodTicks, unsigned Duty)
// Start PWM with <PeriodTicks> per period <Duty> cycle
{
volatile avr32_pwm_channel_t *Pwm;
unsigned                      DutyTicks;

   AVR32_PWM.dis =  1 << Channel;
   DutyTicks     =  PeriodTicks - (uint32)(((uint64)PeriodTicks * Duty) / PWM_DUTY_MAX);
   Pwm           =  &AVR32_PWM.channel[ Channel];
   Pwm->cprd     =  PeriodTicks;
   Pwm->cdty     =  DutyTicks;
   AVR32_PWM.ena =  1 << Channel;
} // PwmStart

//-----------------------------------------------------------------------------
// Duty
//-----------------------------------------------------------------------------

void PwmDutySet( int Channel, unsigned Duty)
// Set PWM <Duty> cycle
{
volatile avr32_pwm_channel_t *Pwm;
unsigned                      PeriodTicks;
unsigned                      DutyTicks;

   Pwm           = &AVR32_PWM.channel[ Channel];
   PeriodTicks   = Pwm->cprd;
   DutyTicks     = PeriodTicks - (uint32)(((uint64)PeriodTicks * Duty) / PWM_DUTY_MAX);
   // wait for previous write done :
   AVR32_PWM.isr;                      // clear old flag
   while( !(AVR32_PWM.isr & (1 << Channel)));
   // set new duty :
   Pwm->cupd     = DutyTicks;
} // PwmDutySet
