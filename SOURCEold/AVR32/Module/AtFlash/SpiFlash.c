//*****************************************************************************
//
//   SpiFlash.c   Flash memory SPI interface
//   Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "SpiFlash.h"
// framework :
#include "spi.h"

#define SPI_MASTER_SPEED    F_BUS_A     // SPI speed at bus clock
#define SPI_NPCS            0           // first chipselect
#define SPI_DEVICE         &AVR32_SPI0

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void SpiInit( void)
// Initialise
{
spi_options_t spiOptions = {
   .reg          = SPI_NPCS,
   .baudrate     = SPI_MASTER_SPEED,
   .bits         = 8,
   .spck_delay   = 0,
   .trans_delay  = 0,
   .stay_act     = 1,
   .spi_mode     = 0,
   .modfdis      = 1
};

   // GPIO functions :
   GpioFunction( AVR32_SPI0_MISO_0_0_PIN, AVR32_SPI0_MISO_0_0_FUNCTION);
   GpioFunction( AVR32_SPI0_MOSI_0_0_PIN, AVR32_SPI0_MOSI_0_0_FUNCTION);
   GpioFunction( AVR32_SPI0_SCK_0_0_PIN,  AVR32_SPI0_SCK_0_0_FUNCTION);
   GpioFunction( AVR32_SPI0_NPCS_0_0_PIN, AVR32_SPI0_NPCS_0_0_FUNCTION);

   spi_initMaster( SPI_DEVICE, &spiOptions);
   spi_selectionMode( SPI_DEVICE, 0, 0, 0);
   spi_enable( SPI_DEVICE);
   spi_setupChipReg( SPI_DEVICE, &spiOptions, F_BUS_A);
} // SpiInit

//-----------------------------------------------------------------------------
// Attach
//-----------------------------------------------------------------------------

void SpiAttach( void)
// Attach chipselect
{
   spi_selectChip( SPI_DEVICE, SPI_NPCS);
} // SpiAttach

//-----------------------------------------------------------------------------
// Release
//-----------------------------------------------------------------------------

void SpiRelease( void)
// Release chipselect
{
   spi_unselectChip( SPI_DEVICE, SPI_NPCS);
} // SpiRelease

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte SpiReadByte( void)
// Returns byte
{
uint16 Status;

   spi_write( SPI_DEVICE, 0xFF);       // dummy write
   spi_read( SPI_DEVICE, &Status);
   return( Status & 0xFF);
} // SpiReadByte

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void SpiWriteByte( byte Value)
// Write byte
{
   spi_write( SPI_DEVICE, Value);
} // SpiWriteByte
