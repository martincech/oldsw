//******************************************************************************
//
//   cTime.c        Print time to console
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include "cTime.h"
#include "conio.h"         // putchar
#include "xTime.h"         // date & time

#ifdef CONIO_FLUSH
   #include "Graphic.h"
   #define Flush()  GFlush()
#else
   #define Flush()
#endif

//------------------------------------------------------------------------------
//  Time
//------------------------------------------------------------------------------

void cTime( TTimestamp Now)
// Display time only
{
   xTime( putchar, Now);
   Flush();
} // cTime

//------------------------------------------------------------------------------
//  Date
//------------------------------------------------------------------------------

void cDate( TTimestamp Now)
// Display date only
{
   xDate( putchar, Now);
   Flush();
} // cDate

//------------------------------------------------------------------------------
//  Date & Time
//------------------------------------------------------------------------------

void cDateTime( TTimestamp Now)
// Display date and time
{
   xDateTime( putchar, Now);
   Flush();
} // cDateTime
