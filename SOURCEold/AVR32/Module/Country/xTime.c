//******************************************************************************
//
//  xTime.c        putchar based display date & time
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "xTime.h"
#include "xprint.h"
#include "Dt.h"

#ifndef TIME_SUFFIX_AM
   #define TIME_SUFFIX_AM "am"
#endif
#ifndef TIME_SUFFIX_PM
   #define TIME_SUFFIX_PM "pm"
#endif

//------------------------------------------------------------------------------
//   Time
//------------------------------------------------------------------------------

void xTime( TPutchar *xputchar, TTimestamp Now)
// Display time only
{
TLocalTime Local;
char Separator;
const char *PmStr;

   DtDecode( Now, &Local);
   Separator = CountryGetTimeSeparator();
   switch( CountryGetTimeFormat()){
      case TIME_FORMAT_24 :
         xprintf( xputchar, "%02d%c%02d%c%02d", Local.Hour, Separator, Local.Min, Separator, Local.Sec);
         break;

      case TIME_FORMAT_12 :
         if( Local.Hour >= 12){
            PmStr = xTimeSuffix( TIME_PM);
         } else {
            PmStr = xTimeSuffix( TIME_AM);
         }
         if( Local.Hour == 0){
            // 00:00 .. 00:59
            Local.Hour = 12;           // 12:xx am
         } else if( Local.Hour > 12){
            // 13:00 .. 23:59
            Local.Hour -= 12;
         } // else 01:00 .. 12.59
         xprintf( xputchar, "%02d%c%02d%c%02d%s", Local.Hour, Separator,
                                                  Local.Min,  Separator,
                                                  Local.Sec,  PmStr);
         break;
   }
} // xTime

//------------------------------------------------------------------------------
//   Time width
//------------------------------------------------------------------------------

int xTimeWidth( void)
// Returns character width of time
{
   switch( CountryGetTimeFormat()){
      case TIME_FORMAT_24 :
         return( 8);
      case TIME_FORMAT_12 :
         return( 10);
   }
   return( 0);
} // xTimeWidth

//------------------------------------------------------------------------------
//   Short Time
//------------------------------------------------------------------------------

void xTimeShort( TPutchar *xputchar, TTimestamp Now)
// Display time without seconds
{
TLocalTime Local;
char Separator;
const char *PmStr;

   DtDecode( Now, &Local);
   Separator = CountryGetTimeSeparator();
   switch( CountryGetTimeFormat()){
      case TIME_FORMAT_24 :
         xprintf( xputchar, "%02d%c%02d", Local.Hour, Separator, Local.Min);
         break;

      case TIME_FORMAT_12 :
         if( Local.Hour > 12){
            Local.Hour -= 12;
            PmStr = xTimeSuffix( YES);
         } else {
            PmStr = xTimeSuffix( NO);
         }
         xprintf( xputchar, "%02d%c%02d%s", Local.Hour, Separator, Local.Min, PmStr);
         break;
   }
} // xTimeShort

//------------------------------------------------------------------------------
//   Time width
//------------------------------------------------------------------------------

int xTimeShortWidth( void)
// Returns character width of time
{
   switch( CountryGetTimeFormat()){
      case TIME_FORMAT_24 :
         return( 5);
      case TIME_FORMAT_12 :
         return( 7);
   }
   return( 0);
} // xTimeShortWidth

//------------------------------------------------------------------------------
//   Time suffix
//------------------------------------------------------------------------------

const char *xTimeSuffix( TTimeSuffix Suffix)
// Returns suffix by <Suffix> enum
{
   if( Suffix == TIME_AM){
      return( TIME_SUFFIX_AM);
   } else {
      return( TIME_SUFFIX_PM);
   }
} // xTimeSuffix

#ifdef DATE_YEAR_YYYY
//------------------------------------------------------------------------------
//   Date
//------------------------------------------------------------------------------

void xDate( TPutchar *xputchar, TTimestamp Now)
// Display date only
{
TLocalTime Local;
char Separator1;
char Separator2;
const char *StrMonth;
int  Year;

   DtDecode( Now, &Local);
   Separator1 = CountryGetDateSeparator1();
   Separator2 = CountryGetDateSeparator2();
   StrMonth   = xDateMonth( Local.Month);
   Year       = Local.Year + 2000;
   switch( CountryGetDateFormat()){
      case DATE_FORMAT_DDMMYYYY :
         xprintf( xputchar, "%02d%c%02d%c%04d", Local.Day, Separator1, Local.Month, Separator2, Year);
         break;
      case DATE_FORMAT_MMDDYYYY :
         xprintf( xputchar, "%02d%c%02d%c%04d", Local.Month, Separator1, Local.Day, Separator2, Year);
         break;
      case DATE_FORMAT_YYYYMMDD :
         xprintf( xputchar, "%04d%c%02d%c%02d", Year, Separator1, Local.Month, Separator2, Local.Day);
         break;
      case DATE_FORMAT_YYYYDDMM :
         xprintf( xputchar, "%04d%c%02d%c%02d", Year, Separator1, Local.Day, Separator2, Local.Month);
         break;

      case DATE_FORMAT_DDMMMYYYY :
         xprintf( xputchar, "%02d%c%s%c%04d", Local.Day, Separator1, StrMonth, Separator2, Year);
         break;
      case DATE_FORMAT_MMMDDYYYY :
         xprintf( xputchar, "%s%c%02d%c%04d", StrMonth, Separator1, Local.Day, Separator2, Year);
         break;
      case DATE_FORMAT_YYYYMMMDD :
         xprintf( xputchar, "%04d%c%s%c%02d", Year, Separator1, StrMonth, Separator2, Local.Day);
         break;
      case DATE_FORMAT_YYYYDDMMM :
         xprintf( xputchar, "%04d%c%02d%c%s", Year, Separator1, Local.Day, Separator2, StrMonth);
         break;
   }
} // xDate

//------------------------------------------------------------------------------
//   Date width
//------------------------------------------------------------------------------

int xDateWidth( void)
// Returns character width of date
{
   if( CountryGetDateFormat() >= DATE_FORMAT_DDMMMYYYY){
      return( 11);
   } else {
      return( 10);
   }
} // xDateWidth
#else // DATE_YEAR_YY
//------------------------------------------------------------------------------
//   Date
//------------------------------------------------------------------------------

void xDate( TPutchar *xputchar, TTimestamp Now)
// Display date only
{
TLocalTime Local;
char Separator1;
char Separator2;
const char *StrMonth;

   DtDecode( Now, &Local);
   Separator1 = CountryGetDateSeparator1();
   Separator2 = CountryGetDateSeparator2();
   StrMonth   = xDateMonth( Local.Month);
   switch( CountryGetDateFormat()){
      case DATE_FORMAT_DDMMYYYY :
         xprintf( xputchar, "%02d%c%02d%c%02d", Local.Day, Separator1, Local.Month, Separator2, Local.Year);
         break;
      case DATE_FORMAT_MMDDYYYY :
         xprintf( xputchar, "%02d%c%02d%c%02d", Local.Month, Separator1, Local.Day, Separator2, Local.Year);
         break;
      case DATE_FORMAT_YYYYMMDD :
         xprintf( xputchar, "%02d%c%02d%c%02d", Local.Year, Separator1, Local.Month, Separator2, Local.Day);
         break;
      case DATE_FORMAT_YYYYDDMM :
         xprintf( xputchar, "%02d%c%02d%c%02d", Local.Year, Separator1, Local.Day, Separator2, Local.Month);
         break;

      case DATE_FORMAT_DDMMMYYYY :
         xprintf( xputchar, "%02d%c%s%c%02d", Local.Day, Separator1, StrMonth, Separator2, Local.Year);
         break;
      case DATE_FORMAT_MMMDDYYYY :
         xprintf( xputchar, "%s%c%02d%c%02d", StrMonth, Separator1, Local.Day, Separator2, Local.Year);
         break;
      case DATE_FORMAT_YYYYMMMDD :
         xprintf( xputchar, "%02d%c%s%c%02d", Local.Year, Separator1, StrMonth, Separator2, Local.Day);
         break;
      case DATE_FORMAT_YYYYDDMMM :
         xprintf( xputchar, "%02d%c%02d%c%s", Local.Year, Separator1, Local.Day, Separator2, StrMonth);
         break;
   }
} // xDate

//------------------------------------------------------------------------------
//   Date width
//------------------------------------------------------------------------------

int xDateWidth( void)
// Returns character width of date
{
   if( CountryGetDateFormat() >= DATE_FORMAT_DDMMMYYYY){
      return( 9);
   } else {
      return( 8);
   }
} // xDateWidth
#endif // DATE_YEAR_YY

//------------------------------------------------------------------------------
//   Date time
//------------------------------------------------------------------------------

void xDateTime( TPutchar *xputchar, TTimestamp Now)
// Display date and time
{
   xDate( xputchar, Now);
   (*xputchar)(' ');
   xTime( xputchar, Now);
} // xDateTime

//------------------------------------------------------------------------------
//   Date time width
//------------------------------------------------------------------------------

int xDateTimeWidth( void)
// Returns character width of date time
{
   return( xDateWidth() + xTimeWidth() + 1);
} // xDateTimeWidth

//------------------------------------------------------------------------------
//   Short Date time
//------------------------------------------------------------------------------

void xDateTimeShort( TPutchar *xputchar, TTimestamp Now)
// Display date and time without seconds
{
   xDate( xputchar, Now);
   (*xputchar)(' ');
   xTimeShort( xputchar, Now);
} // xDateTimeShort

//------------------------------------------------------------------------------
//   Date time width
//------------------------------------------------------------------------------

int xDateTimeShortWidth( void)
// Returns character width of date time
{
   return( xDateWidth() + xTimeShortWidth() + 1);
} // xDateTimeShortWidth

//------------------------------------------------------------------------------
//   Date month
//------------------------------------------------------------------------------

const char _MonthShortcut[_DT_MONTH_COUNT][XDATE_MONTH_SIZE + 1] = {
   "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

const char *xDateMonth( TDtMonth Month)
// Returns date shortcut
{
   return( _MonthShortcut[ Month - 1]); // January == 1
} // xDateMonth
