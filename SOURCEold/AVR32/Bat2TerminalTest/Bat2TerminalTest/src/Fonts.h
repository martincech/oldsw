//*****************************************************************************
//
//    Fonts.h      Project fonts
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __Fonts_H__
   #define __Fonts_H__

#ifndef __Font_H__
   #include "Font.h"
#endif

// Fonts enumeration :
typedef enum {
   FONT_8x8,
   FONT_LUCIDA6,
   FONT_TAHOMA8,
   FONT_TAHOMA16,
   _FONT_LAST
} TProjectFonts;

// fonts descriptors :
extern const TFontDescriptor const *Fonts[];

#endif
