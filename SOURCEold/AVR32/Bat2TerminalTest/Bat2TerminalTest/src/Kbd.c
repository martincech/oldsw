//*****************************************************************************
//
//    Kbd.c        User keyboard
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Kbd.h"             // Keyboard prototypes
#include "Kbd/Kbd.c"             // Keyboard template
#include "System.h" 

#define KBD_BUS_BASE KBD_ROW0
#define KBD_BUS_MASK 0x0E600000

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void KbdInit( void)
// Initialisation
{
   GpioInput(KBD_ROW0);
   GpioInput(KBD_ROW1);
   GpioInput(KBD_COL0);
   GpioInput(KBD_COL1);
   GpioInput(KBD_COL2);
   

   GpioPullup(KBD_COL0);
   GpioPullup(KBD_COL1);
   GpioPullup(KBD_COL2);
   
   GpioClr(KBD_ROW0);
   GpioClr(KBD_ROW1);
   
   /*
   GpioBusEnable( KBD_BUS_BASE, KBD_BUS_MASK);
   GpioBusInput(  KBD_BUS_BASE, KBD_BUS_MASK);
   GpioBusPullup(  KBD_BUS_BASE, KBD_BUS_MASK);*/
} // KbdInit

//-----------------------------------------------------------------------------
// Cteni
//-----------------------------------------------------------------------------

static int ReadKey( void)
// Read key
{
   int Key = K_RELEASED;

   GpioOutput(KBD_ROW0);
   // mus� b�t mal� prodleva, jinak se nap�t� nestihne ust�lit
   SysUDelay(1);
   
   // Double press Enter/Esc :
   if( !GpioGet( KBD_COL0) && !GpioGet( KBD_COL2)){
      Key = K_BOOT;
   } else if( !GpioGet( KBD_COL0)){
      Key = K_ESC;
   } else if( !GpioGet( KBD_COL1)){
      Key = K_UP;
   } else if( !GpioGet( KBD_COL2)){
      Key = K_ENTER;
   }
   
   GpioInput(KBD_ROW0);
   
   if(Key != K_RELEASED) {
	   return Key;
   }
   
   GpioOutput(KBD_ROW1);
   // mus� b�t mal� prodleva, jinak se nap�t� nestihne ust�lit
   SysUDelay(1);
   
   if( !GpioGet( KBD_COL2)){
      Key = K_RIGHT;
   } else if( !GpioGet( KBD_COL0)){
      Key = K_LEFT;
   } else if( !GpioGet( KBD_COL1)){
      Key = K_DOWN;
   }
   
   GpioInput(KBD_ROW1);
   
   
   return Key;
} // ReadKey
