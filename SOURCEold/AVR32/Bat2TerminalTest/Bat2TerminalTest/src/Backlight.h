//*****************************************************************************
//
//    Backlight.h   Backlight functions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Backlight_H__
   #define __Backlight_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define BACKLIGHT_MAX  20

void BacklightInit( void);
// Initialisation

void BacklightOn( void);
// Conditionaly on

void BacklightOff( void);
// Conditionaly off

void BacklightSet( int Intensity);
// Set intensity

void BacklightTest( int Intensity);
// Test backlight intensity

#endif
