//*****************************************************************************
//
//    Fonts.c      Project fonts
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Fonts.h"

#include "Font/Font8x8.c"
#include "Font/Lucida6.c"
#include "Font/Tahoma8.c"
#include "Font/Tahoma16.c"

const TFontDescriptor const *Fonts[] = {
   /* FONT_8x8 */   &Font8x8,
   /* Lucida6  */   &FontLucida6,
   /* Tahoma8  */   &FontTahoma8,
   /* Tahoma16  */  &FontTahoma16,
   /* last     */   0
};
