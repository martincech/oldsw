//*****************************************************************************
//
//    Hardware.h   Bat2 STK600 hardware definitions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr32/io.h>        // CPU header

// Software framework :
#include "compiler.h"

// Project includes :
#include "Uni.h"
#include "Uc3.h"
#include <stddef.h>           // macro offsetof

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#define F_XTAL    12000000             // XTAL frequency 12.000 MHz

#define F_MCK     F_XTAL               // main clock
#define F_CPU     F_MCK                // system frequency
#define F_BUS_H   F_CPU                // high speed bus frequency
#define F_BUS_A   F_CPU                // peripheral bus A frequency
#define F_BUS_B   F_CPU                // peripheral bus B frequency

#define XTAL_STARTUP   AVR32_PM_OSCCTRL0_STARTUP_2048_RCOSC

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

#define WATCHDOG_INTERVAL 2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]

#define WATCHDOG_ENABLE 1              // Enable watchdog
//#define SLEEP_ENABLE    1              // Enable power save

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// Interrupt priority :
#define SYS_TIMER_PRIORITY   AVR32_INTC_INT0         // System timer IRQ
#define UART0_PRIORITY       AVR32_INTC_INT1         // UART0 IRQ
#define UART1_PRIORITY       AVR32_INTC_INT1         // UART1 IRQ

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED0  AVR32_PIN_PB02
#define StatusLED1  AVR32_PIN_PB03

#define StatusLedInit()      GpioOutput( StatusLED0); GpioOutput( StatusLED1)

#define StatusLed0On()       GpioClr( StatusLED0)
#define StatusLed0Off()      GpioSet( StatusLED0)

#define StatusLed1On()       GpioClr( StatusLED1)
#define StatusLed1Off()      GpioSet( StatusLED1)

//-----------------------------------------------------------------------------
// Button
//-----------------------------------------------------------------------------

#define ButtonBT0   AVR32_PIN_PB00

#define ButtonInit()         GpioInput( ButtonBT0)

#define ButtonPressed()    (!GpioGet( ButtonBT0))

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_BAUD           9600       // baud rate
#define UART0_FORMAT         COM_8BITS  // default format
#define UART0_TIMEOUT        10         // intercharacter timeout [ms]

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define UART1_BAUD           9600       // baud rate
#define UART1_FORMAT         COM_8BITS  // default format
#define UART1_TIMEOUT        10         // intercharacter timeout [ms]

//-----------------------------------------------------------------------------
// UART2
//-----------------------------------------------------------------------------

#define UART2_BAUD           9600       // baud rate
#define UART2_FORMAT         COM_8BITS  // default format
#define UART2_TIMEOUT        10         // intercharacter timeout [ms]

//-----------------------------------------------------------------------------
// UART3
//-----------------------------------------------------------------------------

#define UART3_BAUD           9600       // baud rate
#define UART3_FORMAT         COM_8BITS  // default format
#define UART3_TIMEOUT        10         // intercharacter timeout [ms]

//-----------------------------------------------------------------------------
// Console
//-----------------------------------------------------------------------------

#define UCONIO_COM1          1

//------------------------------------------------------------------------------
// Keyboard
//------------------------------------------------------------------------------

#define KBD_ROW0    AVR32_PIN_PA21
#define KBD_ROW1    AVR32_PIN_PA22
#define KBD_COL0    AVR32_PIN_PA25
#define KBD_COL1    AVR32_PIN_PA26
#define KBD_COL2    AVR32_PIN_PA27

// key/events definition :
typedef enum {
   // system events :
   K_NULL  = 0,                        // menu & window excluded
   _K_FIRSTUSER,

   // keyboard :
   K_ENTER = _K_FIRSTUSER,             // Enter
   K_LEFT,                             // Left arrow
   K_ESC,                              // Esc
   K_UP,                               // Up arrow
   K_RIGHT,                            // Right arrow
   K_DOWN,                             // Down arrow
   K_BOOT,                             // Boot composed key

   // events :
   _K_EVENTS    = 0x40,                // start events
   K_FLASH1     = _K_EVENTS,           // flashing 1
   K_FLASH2,                           // flashing 2
   K_REDRAW,                           // 1s redraw
   K_SHUTDOWN,                         // power shutdown
   K_TIMEOUT,                          // inactivity timeout

   // system keys/flags :
   K_REPEAT       = 0x80,              // repeat key (ored with key)
   K_RELEASED     = 0xFE,              // release key (single and repeat)
   K_IDLE         = 0xFF               // internal use, empty read cycle
} TKeys;

// timing constants [ms] :
#define KBD_DEBOUNCE           20      // delay after first touch
#define KBD_AUTOREPEAT_START   300     // autorepeat delay
#define KBD_AUTOREPEAT_SPEED   200     // autorepeat repeat speed

//------------------------------------------------------------------------------
// Display controller ST7529
//------------------------------------------------------------------------------

// pins :
#define GPU_DATA_OFFSET  AVR32_PIN_PB16     // LSB bus D0..D7
#define GPU_CS           AVR32_PIN_PB12     // /CS
#define GPU_A0           AVR32_PIN_PB13     //  A0/RS
#define GPU_WR           AVR32_PIN_PB14     // /WR
#define GPU_RES          AVR32_PIN_PB15     // /RES

#define GpuInitPorts()  GpioOutput( GPU_CS); GpioOutput( GPU_A0); GpioOutput( GPU_WR);\
                        GpioOutput( GPU_RES);
#define GpuClearAll()   GpuClrRES(); GpuDeselect(); GpuSetWR(); GpuSetA0()

// chipselect :
#define GpuSelect()     GpioClr( GPU_CS)
#define GpuDeselect()   GpioSet( GPU_CS)
// control signals :
#define GpuSetRES()     GpioSet( GPU_RES)
#define GpuClrRES()     GpioClr( GPU_RES)
#define GpuSetWR()      GpioSet( GPU_WR)
#define GpuClrWR()      GpioClr( GPU_WR)
#define GpuSetRD()      GpioSet( GPU_RD)
#define GpuClrRD()      GpioClr( GPU_RD)
#define GpuSetA0()      GpioSet( GPU_A0)
#define GpuClrA0()      GpioClr( GPU_A0)

// Settings :
#define GPU_LF                0x00               // line cycles
#define GPU_EC_BASE           0xF0               // electronic contrast base
#define GPU_ANASET1           GPU_ANASET1_193
#define GPU_ANASET2           GPU_ANASET2_3K
#define GPU_ANASET3           GPU_ANASET3_10
#define GPU_DATSDR1           GPU_DATSDR1_CI
#define GPU_DATSDR2           GPU_DATSDR2_CLR
#define CPU_COLUMN_OFFSSET    5

// Color palette (31..0) :
#define G_INTENSITY_BLACK      31
#define G_INTENSITY_DARKGRAY   20
#define G_INTENSITY_LIGHTGRAY  10
#define G_INTENSITY_WHITE       0

//------------------------------------------------------------------------------
// Graphics
//------------------------------------------------------------------------------

#define G_WIDTH          240           // display width (X)
#define G_HEIGHT         160           // display height (Y)
#define G_PLANES           2           // color planes count

#define GRAPHIC_CONIO      1           // enable conio.h
//#define PRINTF_STRING      1           // enable string resource
#define GRAPHIC_LETTER_CENTERING 1     // fixed text centering
#define GRAPHIC_TEXT_INDENTATION 1     // indentation

#define CONIO_FLUSH        1           //!!! automaticky flush

//-----------------------------------------------------------------------------
//  BackLight
//-----------------------------------------------------------------------------

#define BACKLIGHT_CHANNEL     5        // backlight PWM channel
#define PACKLIGHT_PWM_PERIOD  33355      // backlight PWM period [us]
#define PWM5_ACTIVE           1        // backlight PWM active polarity 0/1

//-----------------------------------------------------------------------------
//  I2C by software
//-----------------------------------------------------------------------------

#define IicSDA     AVR32_TWI_SDA_0_0_PIN
#define IicSCL     AVR32_TWI_SCL_0_0_PIN

#define IicPortInit() GpioClr(    IicSDA); GpioClr(    IicSCL); \

#define IicSetSCL()   GpioInput(  IicSCL)
#define IicClrSCL()   GpioOutput( IicSCL)
#define IicSetSDA()   GpioInput(  IicSDA)
#define IicClrSDA()   GpioOutput( IicSDA)
#define IicGetSDA()   GpioGet(    IicSDA)

#define IIC_WAIT      20                // I2C half period [us]
#define IIC_READ      1                 // IicRead compilation
#define IIC_WRITE     1                 // IicWrite compilation

//-----------------------------------------------------------------------------
// Date/Time
//-----------------------------------------------------------------------------

#define DT_ENABLE_DST     1                   // enable daylight saving time compilation
#define DtGetDstType()    DT_DST_TYPE_OFF     // DT_DST_TYPE_EU

//-----------------------------------------------------------------------------
// Country
//-----------------------------------------------------------------------------

#define CountryGetDateFormat()     DATE_FORMAT_DDMMYYYY
#define CountryGetTimeFormat()     TIME_FORMAT_24
#define CountryGetTimeSeparator()  ':'
#define CountryGetDateSeparator1() '.'
#define CountryGetDateSeparator2() '.'

#include "CountryDef.h"



#endif
