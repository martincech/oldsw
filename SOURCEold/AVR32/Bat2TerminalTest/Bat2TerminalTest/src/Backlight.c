//*****************************************************************************
//
//    Backlight.c   Backlight functions
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Backlight.h"
#include "Pwm.h"

int _Intensity;

// Local functions :

static unsigned PwmPercent( int Intensity);
// PWM duty cycle by backlight <Intensity>

static unsigned PwmIntensity( void);
// PWM duty by current intensity

//------------------------------------------------------------------------------
//  Initialisation
//------------------------------------------------------------------------------

void BacklightInit( void)
// Initialisation
{
   PwmInit( BACKLIGHT_CHANNEL);
   PwmStart( BACKLIGHT_CHANNEL, PACKLIGHT_PWM_PERIOD, 0);
   _Intensity = 10;
} // BacklightInit

//------------------------------------------------------------------------------
//  On
//------------------------------------------------------------------------------

void BacklightOn( void)
// Conditionaly on
{
   PwmDutySet( BACKLIGHT_CHANNEL, PwmIntensity());
} // BacklightOn

//------------------------------------------------------------------------------
//  Off
//------------------------------------------------------------------------------

void BacklightOff( void)
// Conditionaly off
{
   PwmDutySet( BACKLIGHT_CHANNEL, 0);
} // BacklightOff

//------------------------------------------------------------------------------
//  Set
//------------------------------------------------------------------------------

void BacklightSet( int Intensity)
// Set intensity
{
   _Intensity = Intensity;
} // BacklightSet

//------------------------------------------------------------------------------
//  Test
//------------------------------------------------------------------------------

void BacklightTest( int Intensity)
// Test backlight intensity
{
   PwmDutySet( BACKLIGHT_CHANNEL, PwmPercent( Intensity));
} // BacklightTest

//*****************************************************************************

//------------------------------------------------------------------------------
//  Percent
//------------------------------------------------------------------------------

static const byte Percent[21] =
{ 0,1,2,3,4,5,6,8,10,12,14,17,21,25,31,38,46,56,68,82,100};

static unsigned PwmPercent( int Intensity)
// PWM duty cycle by backlight intensity
{
   return( ((unsigned)Percent[ Intensity] * PWM_DUTY_MAX) / 100);
} // PwmPercent

//------------------------------------------------------------------------------
//  Intensity
//------------------------------------------------------------------------------

static unsigned PwmIntensity( void)
// PWM duty by current intensity
{
   return( PwmPercent( _Intensity));
} // PwmIntensity
