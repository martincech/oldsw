//*****************************************************************************
//
//    Main.c       STK600 Main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"    // Operating system
#include "Cpu.h"       // CPU utility
#include "uconio.h"    // UART printf
#include "St7259.h"    // Display
#include "Graphic.h"   // graphic
#include "conio.h"     // console
#include "Fonts.h"     // project fonts
#include "Kbd.h"       // Keyboard
#include "Backlight.h" // Backlight

#include "Rx8025.h"    // RTC
#include "cTime.h"     // print time to console
#include "Dt.h"        // Date time utility

#define __COM1__
#include "Com.h"

#define BLINK_PERIOD  500         // blink period [ms]
#define MODE_COUNT    10

#define SetTimer1s()  Counter1s = 1000 / TIMER_PERIOD

// Local variables :
volatile TYesNo   Counter1s   = 0;         // 1 s expiration
volatile unsigned Timer1s     = 0;         // 1 s counter

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
TYesNo Toggle = NO;
byte   ch;
int    Font;
int    Backlight;
TTimestamp ts;
TLocalTime tm;

   CpuInit();
   StatusLedInit();
   UartInit( &AVR32_USART1);
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   uputs( "Start...\n");

   // peripherals init :
   KbdInit();
   BacklightInit();
   GInit();
   GpuContrast( GPU_EC_BASE);

   RtcInit();

   // start system :
   SetTimer1s();                       // start 1 s countdown
   SysStartTimer();
   WatchDogInit();
   InterruptEnable();
   StatusLed0On();
   StatusLed1Off();

   // main loop :
   Backlight = BACKLIGHT_MAX;
   BacklightSet( Backlight);
   BacklightOn();

   Font = FONT_8x8;
   GSetFont( Font);
   
   forever {
      if( ComRxWait( 0)){
         if( ComRxChar( &ch)){
            ComTxChar( ch);
         }
      }
      if( kbhit()){
         cgoto( 0, 0);
         cclreol();
         switch( getch()){
            case K_ENTER :
			   uprintf("Menim cas");
			   tm.Hour = 23;
               tm.Min  = 59;
               tm.Sec  = 55;
               tm.Day  = 15;
               tm.Month = 9;
               tm.Year  = 10;
               ts = DtEncode( &tm);
               RtcSave( ts);
               break;

            case K_ESC :
               cputs( "Esc");
               break;

            case K_RIGHT :
               cputs( "Right");
               break;

            case K_LEFT :
               cputs( "Left");
               break;

            case K_UP :
               Backlight++;
               if( Backlight > BACKLIGHT_MAX){
                  Backlight = BACKLIGHT_MAX;
               }
               cprintf( "Blt %d\n", Backlight);
               BacklightTest( Backlight);
               BacklightSet( Backlight);
               break;

            case K_DOWN :
               Backlight--;
               if( Backlight <= 0){
                  Backlight = 0;
               }
               cprintf( "Blt %d\n", Backlight);
               BacklightTest( Backlight);
               BacklightSet( Backlight);
               break;
         }
      }
      //------------------
      if( Timer1s){
         // 1s expired
         WatchDog();                   // refresh watchdog
         Timer1s = NO;                 // clear flag
         if( Toggle){
            StatusLed0On();
         } else {
            StatusLed0Off();
         }
         Toggle = !Toggle;
		 cgoto( 5, 0);
         cclreol();
         ts = RtcLoad();
         cDateTime( ts);
      }
   } // forever
} // main

//------------------------------------------------------------------------------
//   Timer
//------------------------------------------------------------------------------

#define SysTimerExecute()\
   if( Counter1s){                     \
      if( !(--Counter1s)){             \
         SetTimer1s();                 \
         Timer1s = YES;                \
      }                                \
   }                                   \
   KbdTrigger();

#include "CPU/SysTimer.c"