Bity RETTO (Reload and Start Time-out) a STTTO (Start Time-out) ovl�daj� spou�t�n� timeoutu: .
Dle blokov�ho sch�matu na str. 323 RETTO okam�it� provede na�ten� hodnoty timeoutu do ��ta�e a ��ta� spust�, STTTO slou�� k zastaven� ��ta�e. P�ijet� nov�ho znaku provede to sam�, co RETTO (na�ten� a spu�t�n� ��ta�e).

Timeout d�ky tomu m��e pracovat ve dvou modech:


1) Okam�it� spu�t�n� timeoutu:

Funkce UartRxChar( TUart Uart, byte *Char) �ek� na p�ijet� znaku do vypr�en� timeoutu. Timeout je spu�t�n pomoc� UartStartTimeoutImmediatelly(). Nev�m pro�, ale p�i definov�n�

#define UartStartTimeoutImmediatelly( Uart)      ((Uart)->cr |= (AVR32_USART_CR_RETTO_MASK))

timeout nefunguje, resp. UartIsTimeout( Uart) vrac� neust�le "TRUE" (bit TIMEOUT = 1).

P�i definov�n�:

#define UartStartTimeoutImmediatelly( Uart)      ((Uart)->cr |= (AVR32_USART_CR_STTTO_MASK | AVR32_USART_CR_RETTO_MASK))

v�e jede.



2) Spu�t�n� timeoutu po prvn�m p�ijat�m znaku:

Funkce UartRxFrame( TUart Uart, byte *Buffer) �ek� neomezen� dlouho na p�ijet� prvn�ho znaku, na ka�d� dal�� znak je �ek�no do vypr�en� timeoutu. Timeout je spu�t�n pomoc� UartStartTimeoutAfterFirstCharReceived( Uart)

#define UartStartTimeoutAfterFirstCharReceived( Uart)      ((Uart)->cr |= (AVR32_USART_CR_STTTO_MASK))

tj. �asov�n� timeoutu se vypne. Spou�t� jej prvn� a resetuje ka�d� dal�� p�ijat� znak.


------------------------------------------------------------------------------------------------------------

Z�v�r: Podle popisu v datasheetu bych o�ek�val, �e RETTO bude ihned spou�t�t �asov�n� timeoutu. Z n�jak�ho d�vodu se ale tak ned�je. Je nutn� pou��t RETTO | STTTO. Pokud se STTTO neprovede, pak neust�le TIMEOUT = 1 (timeout vypr�el). STTTO jakoby resetovalo TIMEOUT do 0. Datasheet se ale o ni�em takov�m nezmi�uje.

Pozn�mka: Timeout receiveru je pops�n v kapitole 26.7.4 (Synchronous receiver) a p�edpokl�d�m, �e tento popis se t�k� i asynchronn�ho receiveru. V kapitole o asynchronn�m receiveru nic o timeoutu nep�ou.
