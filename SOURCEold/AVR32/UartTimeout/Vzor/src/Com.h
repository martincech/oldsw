//*****************************************************************************
//
//    Com.h - RS232 template header
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Com_H__
   #define __Com_H__

#ifndef __Uart_H__
   #include "Uart.h"
#endif

#if defined( __COM0__)

   #define ComSetup(b,f, t)   UartSetup(  &AVR32_USART0, b, f, t)
   #define ComTxBusy()       UartTxBusy( &AVR32_USART0)
   #define ComTxChar(c)      UartTxChar( &AVR32_USART0, c)
   #define ComRxChar(c)      UartRxChar( &AVR32_USART0, c)
   #define ComFlushChars()   UartFlushChars( &AVR32_USART0)
   #define ComSetRxTimeout(t) UartSetRxTimeout( &AVR32_USART0, t)
   
#elif defined( __COM1__)

   #define ComSetup(b,f, t)   UartSetup(  &AVR32_USART1, b, f, t)
   #define ComTxBusy()       UartTxBusy( &AVR32_USART1)
   #define ComTxChar(c)      UartTxChar( &AVR32_USART1, c)
   #define ComRxChar(c)      UartRxChar( &AVR32_USART1, c)
   #define ComFlushChars()   UartFlushChars( &AVR32_USART1)
   #define ComSetRxTimeout(t) UartSetRxTimeout( &AVR32_USART1, t)
   
#elif defined( __COM2__)

   #define ComSetup(b,f, t)   UartSetup(  &AVR32_USART2, b, f, t)
   #define ComTxBusy()       UartTxBusy( &AVR32_USART2)
   #define ComTxChar(c)      UartTxChar( &AVR32_USART2, c)
   #define ComRxChar(c)      UartRxChar( &AVR32_USART2, c)
   #define ComFlushChars()   UartFlushChars( &AVR32_USART2)
   #define ComSetRxTimeout(t) UartSetRxTimeout( &AVR32_USART2, t)
   
#elif defined( __COM3__)

   #define ComSetup(b,f, t)   UartSetup(  &AVR32_USART3, b, f, t)
   #define ComTxBusy()       UartTxBusy( &AVR32_USART3)
   #define ComTxChar(c)      UartTxChar( &AVR32_USART3, c)
   #define ComRxChar(c)      UartRxChar( &AVR32_USART3, c)
   #define ComFlushChars()   UartFlushChars( &AVR32_USART3)
   #define ComSetRxTimeout(t) UartSetRxTimeout( &AVR32_USART3, t)
#else
   #error "Unknown COM number"
#endif

#endif