//*****************************************************************************
//
//    Uart.c       RS232 communication services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Uart.h"
#include "System.h"

/*#if !defined __AVR32_UC3A1512__ && !defined __AVR32_UC3A0512__
   #error "Unknown CPU type"
#endif*/

#define UART_PORTS_COUNT            4            // number of UARTS
#define UART_OVERSAMPLING_TRESHOLD  32           // oversampling treshold ( >= 16)

// CSR line error mask :
#define AVR32_USART_CSR_ERROR (AVR32_USART_CSR_OVRE_MASK | AVR32_USART_CSR_FRAME_MASK | \
                               AVR32_USART_CSR_PARE_MASK | AVR32_USART_CSR_RXBRK_MASK)
// CR reset mask :
#define AVR32_USART_CR_RESET  (AVR32_USART_CR_RSTRX_MASK   | AVR32_USART_CR_RSTTX_MASK  | \
                               AVR32_USART_CR_RSTSTA_MASK  | AVR32_USART_CR_RSTIT_MASK  | \
                               AVR32_USART_CR_RSTNACK_MASK | AVR32_USART_CR_DTRDIS_MASK | \
                               AVR32_USART_CR_RTSDIS_MASK);

#define UartResetStatus( Uart)  (Uart)->cr = AVR32_USART_CR_RSTSTA_MASK
#define UartRxReady( Uart)      ((Uart)->csr & AVR32_USART_CSR_RXRDY_MASK)
#define UartRxError( Uart)      ((Uart)->csr & AVR32_USART_CSR_ERROR)
#define UartTxReady( Uart)      ((Uart)->csr & AVR32_USART_CSR_TXRDY_MASK)
#define UartTxPut( Uart, c)     (Uart)->thr = ((unsigned)(c) << AVR32_USART_THR_TXCHR_OFFSET) & AVR32_USART_THR_TXCHR_MASK
#define UartRxGet( Uart)        (byte)(((Uart)->rhr & AVR32_USART_RHR_RXCHR_MASK) >> AVR32_USART_RHR_RXCHR_OFFSET)


// Timeout
#define UartStartTimeout( Uart)      ((Uart)->cr |= (AVR32_USART_CR_STTTO_MASK | AVR32_USART_CR_RETTO_MASK))
#define UartIsTimeout( Uart)         ((Uart)->csr & AVR32_USART_CSR_TIMEOUT_MASK)

#define UartTimeoutIntEnable( Uart)   ((Uart)->ier |= (AVR32_USART_IER_TIMEOUT_MASK))
#define UartTimeoutIntDisable( Uart)   ((Uart)->idr |= (AVR32_USART_IDR_TIMEOUT_MASK))


// UART Rx timeouts :
//static TTimer _UartRxTimeout[ UART_PORTS_COUNT];

typedef struct {
	unsigned int Timeout;
	unsigned int BaudRate;
} TUartStruct;

TUartStruct _Uart[UART_PORTS_COUNT];

// Local functions :

static TYesNo SetBaudRate( TUart Uart, int BaudRate);
// Set baud rate divider



static TTimer GetTimeout( TUart Uart);
// Returns <Uart> timeout
static byte UartGetID(TUart Uart);
//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void UartInit( TUart Uart)
// Communication initialisation
{
   switch( (int)Uart){
      case (int)&AVR32_USART0 :
         GpioFunction( UART0_RXD_PIN, UART0_RXD_FUNCTION);
         GpioFunction( UART0_TXD_PIN, UART0_TXD_FUNCTION);
         GpioPullup( UART0_TXD_PIN);
         break;

      case (int)&AVR32_USART1 :
         GpioFunction( UART1_RXD_PIN, UART1_RXD_FUNCTION);
         GpioFunction( UART1_TXD_PIN, UART1_TXD_FUNCTION);
         GpioPullup( UART1_TXD_PIN);
         break;

      case (int)&AVR32_USART2 :
         GpioFunction( UART2_RXD_PIN, UART2_RXD_FUNCTION);
         GpioFunction( UART2_TXD_PIN, UART2_TXD_FUNCTION);
         GpioPullup( UART2_TXD_PIN);
         break;

#if defined __AVR32_UC3A1512__ || defined __AVR32_UC3A0512__
      case (int)&AVR32_USART3 :
         GpioFunction( UART3_RXD_PIN, UART3_RXD_FUNCTION);
         GpioFunction( UART3_TXD_PIN, UART3_TXD_FUNCTION);
         GpioPullup( UART3_TXD_PIN);
         break;
#endif

      default :
         return;
   }
} // UartInit

//-----------------------------------------------------------------------------
// Setup
//-----------------------------------------------------------------------------

void UartSetup( TUart Uart, unsigned Baud, unsigned Format, unsigned Timeout)
// Set communication parameters <Format> is TComFormat enum
{
   // reset controller :
   Uart->mr   = 0;
   Uart->rtor = 0;
   Uart->ttgr = 0;
   Uart->cr   = AVR32_USART_CR_RESET;
   // set baud rate dividier :
   if( !SetBaudRate( Uart, Baud)){
	   SetBaudRate( Uart, 9600);        // unable set baud rate, set default
   }

   // save timeout by UART :
   UartSetRxTimeout( Uart, Timeout);
   
   // set bit width, parity and stop bits :
   switch( Format){
      default :
      case COM_8BITS :
         Uart->mr |= (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_NONE      << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_8BITS_EVEN :
         Uart->mr |= (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_EVEN      << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_8BITS_ODD :
         Uart->mr |= (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_ODD       << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_8BITS_MARK :
         Uart->mr |= (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_MARK      << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_8BITS_SPACE :
         Uart->mr |= (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_SPACE     << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_7BITS :
         Uart->mr |= (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_NONE      << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_7BITS_EVEN :
         Uart->mr |= (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_EVEN      << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_7BITS_ODD :
         Uart->mr |= (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_ODD       << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_7BITS_MARK :
         Uart->mr |= (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_MARK      << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case COM_7BITS_SPACE :
         Uart->mr |= (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                     (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                     (AVR32_USART_MR_PAR_SPACE     << AVR32_USART_MR_PAR_OFFSET) |
                     (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                     (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
   }
   // start communication :
   Uart->cr = AVR32_USART_CR_RXEN_MASK | AVR32_USART_CR_TXEN_MASK;
} // UartSetup

//-----------------------------------------------------------------------------
// Tx busy
//-----------------------------------------------------------------------------

TYesNo UartTxBusy( TUart Uart)
// Returns YES if transmitter is busy
{
   return( !UartTxReady( Uart));
} // UartTxBusy

//-----------------------------------------------------------------------------
// Tx
//-----------------------------------------------------------------------------

void UartTxChar( TUart Uart, byte Char)
// Transmit <Char> byte
{
   while( !UartTxReady( Uart));
   UartTxPut( Uart, Char);
} // UartTxChar

//-----------------------------------------------------------------------------
// Rx
//-----------------------------------------------------------------------------

TYesNo UartRxChar( TUart Uart, byte *Char)
// Returns YES and received byte on <Char>,
// or NO on timeout
{
//TTimer Now;

   UartStartTimeout(Uart);

   do {
      if( UartRxError( Uart)){
         UartResetStatus( Uart);       // reset line error
         return( NO);                  // line error
      }
      if( UartRxReady( Uart)){
         *Char = UartRxGet( Uart);
         return( YES);
      }
   } while( !UartIsTimeout(Uart));

   return( NO);                        // timeout
} // UartRxChar

//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

void UartFlushChars( TUart Uart)
// Skips all Rx chars up to intercharacter timeout
{
dword  Char;

   UartStartTimeout(Uart);

   do {
      if( UartRxError( Uart)){
         UartResetStatus( Uart);       // reset line error
         UartStartTimeout(Uart);       // timeout again
		 continue;                     // continue waiting
      }
      if( UartRxReady( Uart)){
         Char = Uart->rhr;             // get character
		 UartStartTimeout(Uart);       // timeout again
      }
   } while( !UartIsTimeout(Uart));
} // UartFlushChars

//*****************************************************************************

//-----------------------------------------------------------------------------
// Baud rate
//-----------------------------------------------------------------------------

static TYesNo SetBaudRate( TUart Uart, int BaudRate)
// Set baud rate divider
{
unsigned Oversampling;
unsigned Dividier;

   // check for 8x oversampling needed :
   Oversampling = F_BUS_A >= (BaudRate * UART_OVERSAMPLING_TRESHOLD) ? 16 : 8;
   // get fractional dividier * 2 :
   Dividier     = (F_BUS_A * (1 << AVR32_USART_BRGR_FP_SIZE) * 2) / (BaudRate * Oversampling);
   // round up :
   Dividier    += 1;
   Dividier   >>= 1;
   // check for range :
   if( Dividier < (1 << AVR32_USART_BRGR_FP_SIZE)){
      return( NO);                     // unable set baud rate
   }
   if( Dividier > ((1 << AVR32_USART_BRGR_CD_SIZE) - 1) << AVR32_USART_BRGR_FP_SIZE){
      return( NO);                     // unable set baud rate
   }
   // set mode register clock and oversampling :
   Uart->mr &= ~(AVR32_USART_MR_USCLKS_MASK |
                 AVR32_USART_MR_SYNC_MASK |
                 AVR32_USART_MR_OVER_MASK);
   Uart->mr |=   AVR32_USART_MR_USCLKS_MCK << AVR32_USART_MR_USCLKS_OFFSET |
               ((Oversampling == 16) ? AVR32_USART_MR_OVER_X16 : AVR32_USART_MR_OVER_X8) << AVR32_USART_MR_OVER_OFFSET;

   Uart->brgr = (Dividier >> AVR32_USART_BRGR_FP_SIZE)             << AVR32_USART_BRGR_CD_OFFSET |
                (Dividier & ((1 << AVR32_USART_BRGR_FP_SIZE) - 1)) << AVR32_USART_BRGR_FP_OFFSET;
				
   _Uart[UartGetID(Uart)].BaudRate = BaudRate;

   return( YES);
} // SetBaudRate

static byte UartGetID(TUart Uart) {
   switch( (int)Uart){
      case (int)&AVR32_USART0 :
         return 0;

      case (int)&AVR32_USART1 :
         return 1;

      case (int)&AVR32_USART2 :
         return 2;
		 
#if defined __AVR32_UC3A1512__ || defined __AVR32_UC3A0512__
      case (int)&AVR32_USART3 :
         return 3;
#endif;

      default:
	     return 0;
   }
};

//-----------------------------------------------------------------------------
// Timeout
//-----------------------------------------------------------------------------

void UartSetRxTimeout( TUart Uart, unsigned Timeout)
// Returns <Uart> timeout
{
   byte UartID = UartGetID(Uart);
   
   _Uart[UartID].Timeout = Timeout;
   
   if(Timeout) {
      Uart->rtor = ((_Uart[UartID].BaudRate * Timeout) / 1000) & AVR32_USART_RTOR_TO_MASK;
   } else {
	   Uart->rtor = 1;
   }
} // SetTimeout


static unsigned int GetRxTimeout( TUart Uart)
// Returns <Uart> timeout
{
   return _Uart[ UartGetID(Uart)].Timeout;
} // GetTimeout
