//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"

#include "Uart.h"

#define __COM1__
#include "Com.h"





int main (void)
{
   byte ch;

   CpuInit();
   WatchDogInit();
   
   UartInit( &AVR32_USART1);
   
   UartInit( &AVR32_USART2);

   UartSetup(&AVR32_USART2, UART2_BAUD, UART2_FORMAT, UART2_TIMEOUT);
   
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   uputs( "Start...\n");
   
   while(1) {
	  if(!ComRxChar(&ch)) {
		 UartTxChar(&AVR32_USART2, 'T'); // report timeout
	  } else {
		 UartTxChar(&AVR32_USART2, ch); // echo, don't send T !!!
	  }
	  
	  
	  if(UartRxChar(&AVR32_USART2, &ch)) {
         if(ch >= '0' && ch <= '9') { // set timeout
			 ComSetRxTimeout((ch - '0') * 100);
		 }
      }
   }  
}