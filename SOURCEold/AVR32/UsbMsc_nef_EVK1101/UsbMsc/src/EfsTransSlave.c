#include "System.h"

#include "EfsTransSlave.h"

#include "Hardware.h"
#include "SpiExt.h"

#include "uconio.h"

byte EfsTransRxBuffer[EFS_TRANS_MAX_PACKET_LENGTH];
byte EfsTransPacketReady = NO;
byte *EfsTransData;
word EfsTransDataSize;


TYesNo EfsTransPacketIsReady(void) {
   return EfsTransPacketReady;
}

word EfsTransGetDataSize(void) {
   return EfsTransDataSize;
}

byte* EfsTransGetData(void) {
   return EfsTransData;
}

void EfsTransPacketReadyCallback(void) {
   byte i = 0;
   word BytesReceived;
   byte *PacketStart;
   word DataSize;
   word MinusDataSize;
   word CrcRec;
   word CrcCalc;

   EfsTransStatusBusy();
   
   BytesReceived = SpiGetRxCount();
   
   if(BytesReceived == 0) {
	   SpiRx(SPI_DEVICE, EfsTransRxBuffer, EFS_TRANS_MAX_PACKET_LENGTH);
	   return;
   }
   
   // Search for SOF
   while(EfsTransRxBuffer[i++] != EFS_TRANS_SOF) {
	   if(i >= BytesReceived) {
		  SpiRx(SPI_DEVICE, EfsTransRxBuffer, EFS_TRANS_MAX_PACKET_LENGTH);
	      return;
	   }
   }

   PacketStart = &(EfsTransRxBuffer[i - 1]);

   // Read & check data size
   DataSize = (PacketStart[EFS_TRANS_SIZE_OFFSET] << 8) + PacketStart[EFS_TRANS_SIZE_OFFSET + 1];    

   if(DataSize > EFS_TRANS_MAX_DATA_SIZE) {
	   SpiRx(SPI_DEVICE, EfsTransRxBuffer, EFS_TRANS_MAX_PACKET_LENGTH);
	   return;
   }

   MinusDataSize = -((PacketStart[EFS_TRANS_SIZE_OFFSET + 2] << 8) + PacketStart[EFS_TRANS_SIZE_OFFSET + 3]);    

   if(DataSize != MinusDataSize) {
	   SpiRx(SPI_DEVICE, EfsTransRxBuffer, EFS_TRANS_MAX_PACKET_LENGTH);
	   return;
   }

   // Read & check CRC
   CrcRec = (PacketStart[EFS_TRANS_DATA_OFFSET + DataSize] << 8) + PacketStart[EFS_TRANS_DATA_OFFSET + DataSize + 1];
   CrcCalc = CalculateCRC(PacketStart, EFS_TRANS_DATA_OFFSET + DataSize);

   if(CrcCalc != CrcRec) {
	  SpiRx(SPI_DEVICE, EfsTransRxBuffer, EFS_TRANS_MAX_PACKET_LENGTH);
	  return;
   }
   
   // Everything OK
   EfsTransData = &(PacketStart[EFS_TRANS_DATA_OFFSET]);
   EfsTransDataSize = DataSize;
   
   /*
   
   for(i = 0 ; i < EfsTransDataSize; i++) {
	   uprintf("%c  %d\n", EfsTransData[i], EfsTransData[i]);
   }
   */
   EfsTransPacketReady = YES;
}


void EfsTransReceive(void) {
   EfsTransPacketReady = NO;
   SpiRx(SPI_DEVICE, EfsTransRxBuffer, EFS_TRANS_MAX_PACKET_LENGTH);
   EfsTransStatusReady();
}

void EfsTransInit(void)
{

   
   SpiPortInit();
   spi_initSlave( SPI_DEVICE, 8, 0);
   spi_enable( SPI_DEVICE);

   EfsTransStatusInit();
   EfsTransStatusBusy();


   SpiRegisterCallback(&EfsTransPacketReadyCallback);

   SpiEnableCSInt(SPI_DEVICE);

   Enable_global_interrupt();
   
   
}





