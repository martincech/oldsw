#include "System.h"

#include "Efs.h"
#include "EfsTransSlave.h" // transportní vrstva

#include "uconio.h"



void EfsShellInit(void) {
	EfsTransInit();
	EfsTransReceive();
}


void EfsShellTask(void) {
   word CmdSize;
   EfsCmdUnion *Cmd;

   word ReplySize;
   EfsReplyUnion *Reply;

   word i;
   
   if(EfsTransPacketIsReady() == NO) {
	   return;
   }

   CmdSize = EfsTransGetDataSize();
   Cmd = (EfsCmdUnion *) EfsTransGetData();
   
   uprintf("Prijato %d bytu\n", CmdSize);
   
   for(i = 0 ; i < CmdSize; i++) {
	   uprintf("%c  %d\n", Cmd[i], Cmd[i]);
   }
   
   Reply = EfsTransGetData();
   
   switch(Cmd->Cmd) {
      case FILECMD_FOPEN:
	     uprintf("Filename: %s\n", Cmd->FOpen.FileName);
		 uprintf("Mode: %d\n", Cmd->FOpen.Mode);
         //Reply->Reply = EfsFileOpen(Cmd->FOpen.FileName, Cmd->FOpen.Mode);
         ReplySize = sizeof(Reply->FOpen);
		 break;
   }
   
   EfsTransReceive();
}