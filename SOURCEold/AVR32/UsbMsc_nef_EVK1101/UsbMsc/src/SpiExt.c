#include "spi.h"
#include "SpiExt.h"  // Extended SPI library
#include "System.h"
#include "uconio.h"
byte *SpiTxPtr = NULL;
word SpiTxLength = 0;
word SpiTxCount = 0;

byte *SpiRxPtr = NULL;
word SpiRxLength = 0;
word SpiRxCount = 0;

TYesNo SpiReady = NO;

void (* SpiCallback)(void) = NULL;


void SpiRegisterCallback(void (* Callback)(void)) {
   SpiCallback = Callback;
}

__attribute__((__interrupt__)) void SpiISRTx(void)
{
   volatile dword SpiStatus = AVR32_SPI.sr;

   if((SpiStatus & (1 << AVR32_SPI_TDRE_OFFSET))) {
	  if(SpiTxCount < SpiTxLength) {
		 AVR32_SPI.tdr = *SpiTxPtr;
		 SpiTxCount++;
		 SpiTxPtr++;
	  } else {
		 AVR32_SPI.tdr = 0xFF;
	  }
   } else if((SpiStatus & (1 << AVR32_SPI_NSSR_OFFSET))) {
	  SpiReady = YES;
	  SpiDisableTxInt(&AVR32_SPI);
	  SpiCallback();
   }
}

__attribute__((__interrupt__)) void SpiISRRx(void)
{
   volatile dword SpiStatus = AVR32_SPI.sr;
   
   if((SpiStatus & (1 << AVR32_SPI_RDRF_OFFSET))) {
	  if(SpiRxCount < SpiRxLength) {
	     *SpiRxPtr = AVR32_SPI.rdr;
	     SpiRxCount++;
	     SpiRxPtr++;
	  } else {
		 (void) AVR32_SPI.rdr;
	  }
   }
   
   if((SpiStatus & (1 << AVR32_SPI_NSSR_OFFSET))) {
	  SpiReady = YES;
	  SpiDisableRxInt(&AVR32_SPI);
	  SpiCallback();
   }
}

word SpiGetRxCount(void) {
	return SpiRxCount;
}

void SpiTx(volatile avr32_spi_t *spi, byte *Buffer, word Length) {
   SpiTxLength = Length;
   SpiTxPtr = Buffer;
   SpiTxCount = 0;
   SpiReady = NO;
   
   INTC_register_interrupt((__int_handler) (&SpiISRTx), AVR32_SPI_IRQ, AVR32_INTC_INT1);
   
   SpiEnableTxInt(spi); 
}

void SpiRx(volatile avr32_spi_t *spi, byte *Buffer, word Length) {
   SpiRxLength = Length;
   SpiRxPtr = Buffer;
   SpiRxCount = 0;
   SpiReady = NO;
   
   INTC_register_interrupt((__int_handler) (&SpiISRRx), AVR32_SPI_IRQ, AVR32_INTC_INT1);
   
   SpiEnableRxInt(spi); 
}



// vylep�it - zat�mco nad�azen� vrstva zpracov�v� p�ijat� byte, SPI m��e p�ij�mat dal��
TYesNo SpiReadChar(volatile avr32_spi_t *spi, unsigned short *data) {
   unsigned int timeout = SPI_TIMEOUT;

   while (!(spi->sr & AVR32_SPI_SR_TDRE_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   spi->tdr = 0xFF << AVR32_SPI_TDR_TD_OFFSET;

   while((spi->sr & AVR32_SPI_SR_TXEMPTY_MASK) != 0);
   
   timeout = SPI_TIMEOUT;

   while((spi->sr & (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) !=
         (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   *data = spi->rdr >> AVR32_SPI_RDR_RD_OFFSET;

   return YES;
}