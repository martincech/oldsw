#ifndef _EFS_TRANS_SLAVE_H_
#define _EFS_TRANS_SLAVE_H_

#include "System.h"
#include "EfsTransDef.h"

void EfsTransInit(void);
void EfsTransReceive(void);
TYesNo EfsTransPacketIsReady(void);
word EfsTransGetDataSize(void);
byte* EfsTransGetData(void);
#endif