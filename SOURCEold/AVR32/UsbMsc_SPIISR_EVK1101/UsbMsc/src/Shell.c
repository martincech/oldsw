//*****************************************************************************
//
//    Shell.c   File Access Shell over SPI
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include <string.h>  
#include "Hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"    // UART printf
#include "ushell_task.h"    // UART printf !!!!! smazat
#include "navigation.h" // File interface
#include "file.h" // File interface

#include "SpiExt.h"     // Spi

#include "Shell.h"

//------------------------------------------------------------------------------
// CMD static functions
//------------------------------------------------------------------------------

static byte ShellCmdDisk(void);
static byte ShellCmdDF(void);
static byte ShellCmdDir(void);
static byte ShellCmdCd(void);
static byte ShellCmdFileOpen(void);
static byte ShellCmdFileClose(void);
static byte ShellCmdFileRead(void);
static byte ShellCmdFileWrite(void);
static byte ShellCmdMkdir(void);
static byte ShellCmdRmdir(void);
static byte ShellCmdRename(void);
static byte ShellCmdFileSeek(void);
static byte ShellCmdTouch(void);

//------------------------------------------------------------------------------
// Codes
//------------------------------------------------------------------------------

#define SHELL_HOST_ERR_OK               0x00
#define SHELL_HOST_ERR_BAD_CMD          0x77
#define SHELL_HOST_ERR_EXEC_ERROR       0x78
#define SHELL_HOST_ERR_NO_DRIVE         0x79
#define SHELL_HOST_ERR_DRIVE_ERROR      0x80
#define SHELL_HOST_ERR_DIR_ERROR        0x81
#define SHELL_HOST_ERR_NODIR_ERROR      0x82
#define SHELL_HOST_ERR_FILE_ERR         0x83
#define SHELL_HOST_ERR_WRITE_ERROR      0x84

//------------------------------------------------------------------------------
// Commands
//------------------------------------------------------------------------------

#define SHELL_CMD_DISK        0x01
#define SHELL_CMD_DF          0x02
#define SHELL_CMD_DIR         0x03
#define SHELL_CMD_CD          0x04
#define SHELL_CMD_FILE_OPEN   0x05
#define SHELL_CMD_FILE_CLOSE  0x06
#define SHELL_CMD_FILE_READ   0x07
#define SHELL_CMD_FILE_WRITE  0x08
#define SHELL_CMD_MKDIR       0x09
#define SHELL_CMD_RMDIR       0x0A
#define SHELL_CMD_RENAME      0x0B
#define SHELL_CMD_FILE_SEEK   0x0C
#define SHELL_CMD_TOUCH       0x0D

typedef const struct {
   byte cmd;               // CMD
   byte StaticLength;      // Static part of parameter length
   TYesNo VariableLength;  // Will be parameter of variable length present? 
   byte (*Function)(void)  // Command specific function
} TShellCmd;

// CMD structure	
TShellCmd ShellCommands[] = {
   { SHELL_CMD_DISK,        0, NO,  &ShellCmdDisk      },
   { SHELL_CMD_DF,          1, NO,  &ShellCmdDF        },
   { SHELL_CMD_DIR,         4, NO,  &ShellCmdDir       },
   { SHELL_CMD_CD,          0, YES, &ShellCmdCd        },
   { SHELL_CMD_FILE_OPEN,   1, YES, &ShellCmdFileOpen  },
   { SHELL_CMD_FILE_CLOSE,  0, NO,  &ShellCmdFileClose },
   { SHELL_CMD_FILE_READ,   4, NO,  &ShellCmdFileRead  },
   { SHELL_CMD_FILE_WRITE,  4, NO,  &ShellCmdFileWrite },
   { SHELL_CMD_MKDIR,       0, YES, &ShellCmdMkdir     },
   { SHELL_CMD_RMDIR,       0, YES, &ShellCmdRmdir     },
   { SHELL_CMD_RENAME,      0, YES, &ShellCmdRename    },
   { SHELL_CMD_FILE_SEEK,   5, NO,  &ShellCmdFileSeek  },
   { SHELL_CMD_TOUCH,       0, YES, &ShellCmdTouch     }
}; 

#define SHELL_COMMANDS_NB      (sizeof(ShellCommands)/sizeof(ShellCommands[0]))

//------------------------------------------------------------------------------
// Misc
//------------------------------------------------------------------------------

#define SPI_DEVICE          &AVR32_SPI

#define SHELL_CHAR_TIMEOUT           20000
#define SHELL_TRANSACTION_TIMEOUT    2000000
#define SHELL_PARAMS_MAX_LENGTH      50
#define SHELL_CMD_DELIMITER          '\n'
#define SHELL_BLOCK_LENGTH           255 // nem��e b�t v�c ne� 255, proto�e informace o d�lce bloku je p�en�ena v jednom bytu

#if SHELL_BLOCK_LENGTH > 255
   #error "Data block length can't exceed 255 bytes"
#endif

byte ShellParamBuffer[SHELL_PARAMS_MAX_LENGTH + 1]; // last byte for null termination

//------------------------------------------------------------------------------
// Static functions
//------------------------------------------------------------------------------

static TYesNo ShellTransmitData(byte *buffer, byte length, TYesNo TransmitLength, TYesNo WillContinue);
static void ShellCallbackTx(void);
static void ShellCallbackRx(void);

//! @brief Appends the '\' char at the end of path
//!
// N�kter� funkce ji vy�aduj� - upravit
static void ushell_path_valid_synta( char *path )
{
   uint8_t u8_tmp;
   
   // Compute size of substitute
   for( u8_tmp=0; u8_tmp<MAX_FILE_PATH_LENGTH; u8_tmp++ )
   {
      if( path[u8_tmp]==0)
         break;
   }
   // Append the '\' char for the nav_setcwd to enter the found directory
   if ( path[u8_tmp-1] != '\\')
   {
      path[u8_tmp]='\\';
      path[u8_tmp+1]=0;
   }
}

//-----------------------------------------------------------------------------
// ShellCallbackTx
//-----------------------------------------------------------------------------

static void ShellCallbackTx()
// Callback on trasmit complete
{
	ShellStatusBusy();
}

//-----------------------------------------------------------------------------
// ShellCallbackRx
//-----------------------------------------------------------------------------

static word charsReceived = 0;

static void ShellCallbackRx(void)
// Callback on char receive
{
	charsReceived++;
	
	if(charsReceived >= SHELL_BLOCK_LENGTH + 1) {
		ShellStatusBusy();
	}
}

//-----------------------------------------------------------------------------
// ShellTransmitData
//-----------------------------------------------------------------------------

// m� m�t shell vlastn� buffer, ze kter�ho jsou vys�l�na data, nebo jej p�esunout a� do SPI modulu?
byte ShellTransmitDataBuffer[280];

static TYesNo ShellTransmitData(byte *buffer, byte length, TYesNo TransmitLength, TYesNo WillContinue)
{
   //dword timeout = SPI_TRANSACTION_TIMEOUT;
   byte i;
   byte offset = 1;
   
   // wait for previous transaction
   if(SpiWaitForTransmitComplete(SPI_DEVICE) == NO) {
	  return NO;
   }
   
   // first byte - status
   ShellTransmitDataBuffer[0] = SHELL_HOST_ERR_OK;

   if(TransmitLength == YES) {
	  // optional second byte - length of block
	  offset = 2;
      ShellTransmitDataBuffer[1] = length;
	  
   }

   i = length;
   
   // copy to transmit buffer
   while(i--) {
	   ShellTransmitDataBuffer[i + offset] = buffer[i];
   }

   // transmit
   SpiStartTransmit(SPI_DEVICE, ShellTransmitDataBuffer, length + offset);
   ShellStatusReady();
   
   if(WillContinue == NO) {
	   if(SpiWaitForTransmitComplete(SPI_DEVICE) == NO) {
		   SpiStopTransmit(SPI_DEVICE);
		   return NO;
	   }
	   
	   SpiStopTransmit(SPI_DEVICE);
   }

   return YES;
}

//-----------------------------------------------------------------------------
// ShellTransmitError
//-----------------------------------------------------------------------------

static TYesNo ShellTransmitError(byte error)
{
   // wait for previous transaction
   if(SpiWaitForTransmitComplete(SPI_DEVICE) == NO) {
	  return NO;
   }
   
   ShellTransmitDataBuffer[0] = error;

   // transmit
   SpiStartTransmit(SPI_DEVICE, ShellTransmitDataBuffer, 1);
   ShellStatusReady();
   
   if(SpiWaitForTransmitComplete(SPI_DEVICE) == NO) {
      SpiStopTransmit(SPI_DEVICE);
      return NO;
   }
	   
   SpiStopTransmit(SPI_DEVICE);
   return YES;
}

//-----------------------------------------------------------------------------
// ShellReadChar
//-----------------------------------------------------------------------------

static TYesNo ShellReadChar(byte *ch)
// reads char with timeout
{
   word timeout = SHELL_CHAR_TIMEOUT;
   
   while(!SpiReadBufferChar(ch)) { // wait for next char till timeout expires
	  if(!timeout--) { // timeout, error
	     return NO;
	  }
				   
	  SysUDelay(1);

	  continue;
	}
	
	return YES;
}

//-----------------------------------------------------------------------------
// ShellCmdDisk
//-----------------------------------------------------------------------------

byte ShellCmdDisk(void)
{
   byte i;
   byte buffer[4];

   // drive count
   buffer[0] = nav_drive_nb();

   // drive names
   for(i = 0; i < buffer[0]; i++) {
	  buffer[i + 1] = 'a' + i;
   }
   
   // transmit data
   if(ShellTransmitData(buffer, i + 1, YES, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }

   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdDF
//-----------------------------------------------------------------------------

byte ShellCmdDF()
{
   qword buffer[2];
   Fs_index sav_index = nav_getindex(); // Save current position
   byte drive = ShellParamBuffer[0] - 'a';
   
   if(drive >= nav_drive_nb()) {
	  return SHELL_HOST_ERR_DRIVE_ERROR; 
   }

   nav_drive_set(drive); // Select drive
   
   if(!nav_partition_mount()) { // Mount drive
      return SHELL_HOST_ERR_DRIVE_ERROR;
   }

   // !!! prvn� vol�n� nav_partition_freespace() trv� velmi dlouho !!!!
   buffer[0] = (qword) (nav_partition_space() << FS_SHIFT_B_TO_SECTOR);
   buffer[1] = (qword) (nav_partition_freespace() << FS_SHIFT_B_TO_SECTOR);

   nav_gotoindex(&sav_index); // Restore position
   
   /* Mo�n� by bylo dobr� d�t sem
   
      if(CS == high) {
	     return HOST_ERR_OK
	  }
	  
      master data nevy�etl, proto�e v�, �e jejich p��prava m��e trvat dlouho, zept� se na n� n�sledn�, nov�m p��kazem
      pokud tady tento if nebude, funkce skon�� chybou, proto�e master bude t�ko tak dlouho �ekat na v�po�et voln�ho
	  m�sta a transakci ukon��.
   */
   
   if(ShellTransmitData((byte *) buffer, 16, YES, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdDir
//-----------------------------------------------------------------------------

byte ShellCmdDir()
{
   /* v bufferu je pot�eba m�sto na: 
      1) na datov� blok, jeho� velikost je SHELL_BLOCK_LENGTH
	  2) a� na jeden dal�� cel� z�znam (dir + name + size), kter� m��e p�et�ct do dal��ho datov�ho bloku, 
	     kter� bude vys�l�n a� v dal��m kroku
		 
	  ov��it
   */
   byte buffer[SHELL_BLOCK_LENGTH + 1 + MAX_FILE_PATH_LENGTH + 4 + 1];
   dword temp;
   word temp2, i, bufferPtr = 0;
   word offset = *((word *) (ShellParamBuffer + 0));
   word count = *((word *) (ShellParamBuffer + 2));

   if(!nav_filelist_reset()) {
      return SHELL_HOST_ERR_DRIVE_ERROR;
   }

   if(offset != 0) {
      if(!nav_filelist_set(offset - 1, FS_FIND_NEXT)) {
		 // offset too large
	     if(ShellTransmitData(NULL, 0, YES, NO) == NO) {
            return SHELL_HOST_ERR_EXEC_ERROR;
         }
 
         return SHELL_HOST_ERR_OK;
	  }
   }

   // For each file in list
   while(count-- && nav_filelist_set(0, FS_FIND_NEXT) )
   {
	  // obtain dir/file
      if(nav_file_isdir()) {
         buffer[bufferPtr] = 1;
      } else {
         buffer[bufferPtr] = 0;
      }

      bufferPtr++;

      // obtain file name
      nav_file_name((FS_STRING) buffer + bufferPtr, MAX_FILE_PATH_LENGTH, FS_NAME_GET, true);

	  while(buffer[bufferPtr++] != '\0');

      // obtain file size
	  temp = nav_file_lgt();
	  
	  buffer[bufferPtr] = temp >> 24;
	  buffer[bufferPtr+1] = temp >> 16;
	  buffer[bufferPtr+2] = temp >> 8;
	  buffer[bufferPtr+3] = temp >> 0;
	  //*((dword *) (buffer + bufferPtr)) = nav_file_lgt();
	  
	  bufferPtr += 4;

      
	  if(bufferPtr >= SHELL_BLOCK_LENGTH) {
		 // send block
         if(ShellTransmitData(buffer, SHELL_BLOCK_LENGTH, YES, YES) == NO) {
	         return SHELL_HOST_ERR_EXEC_ERROR;
         }
          
		 // some data overflown, copy them at the beginning of the buffer
		 temp2 = bufferPtr;
		 bufferPtr = 0;
		 for(i = SHELL_BLOCK_LENGTH; i < temp2; i++, bufferPtr++) {
			 buffer[bufferPtr] = buffer[i];
		 }
	  }
   }

   // send last block
   if(ShellTransmitData(buffer, bufferPtr, YES, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }
 
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdCd
//-----------------------------------------------------------------------------

byte ShellCmdCd(void)
{ 
   byte *path = ShellParamBuffer;					 

   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   if(!strcmp(path, "..")) { // parent directory
	   if(!nav_dir_gotoparent()) {
	     return SHELL_HOST_ERR_DIR_ERROR;
	   }
	   
   } else { // path
      ushell_path_valid_synta(path);
   
      // set path
      if(!nav_setcwd((FS_STRING) path, true, false)) {
         return SHELL_HOST_ERR_DIR_ERROR;
      }
   }
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileOpen
//-----------------------------------------------------------------------------

byte ShellCmdFileOpen(void)
{ 
   byte mode = ShellParamBuffer[0];	
   byte *path = ShellParamBuffer + 1;					 

   // not valid path
   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // select file
   if(!nav_setcwd((FS_STRING) path, true, false)) {
      return SHELL_HOST_ERR_FILE_ERR;
   }

   // open file
   if(!file_open(mode)) {
	   return SHELL_HOST_ERR_FILE_ERR;
   }

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileOpen
//-----------------------------------------------------------------------------

byte ShellCmdFileClose(void)
{ 
   file_close();
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileRead
//-----------------------------------------------------------------------------

byte ShellCmdFileRead(void)
{ 
   dword count =  *((dword *) ShellParamBuffer);
   byte buffer[SHELL_BLOCK_LENGTH];
   word bufferPtr = 0;
   
   if(0) { // kdy� nebyl otev�en soubor
      return SHELL_HOST_ERR_FILE_ERR;
   }

   while(count-- && (file_eof() == false))
   {
	  buffer[bufferPtr++] = file_getc(); // �ten� by asi �lo zrychlit pou�it�m fce file_read_buf

	  // send data
	  if(bufferPtr >= SHELL_BLOCK_LENGTH) {
         if(ShellTransmitData(buffer, SHELL_BLOCK_LENGTH, YES, YES) == NO) {
	         return SHELL_HOST_ERR_EXEC_ERROR;
         }
		 
		 bufferPtr = 0;
	  }
   }

   // send rest of data
   if(ShellTransmitData(buffer, bufferPtr, YES, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }
 
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileWrite
//-----------------------------------------------------------------------------

byte ShellCmdFileWrite(void)
{
   dword count = *((dword *) ShellParamBuffer);
   byte blockLength;
   byte ch;
   byte buffer[SHELL_BLOCK_LENGTH];
   byte i;

   if(0) { // kdy� nebyl otev�en soubor
      return SHELL_HOST_ERR_FILE_ERR;
   }

   // register SPI callback
   SPIRegisterCallbackRx(&ShellCallbackRx);
   
   // start receive
   SpiStartReceive(SPI_DEVICE);

   // tell master we are ready
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }

   // Read blocks
   while(count) {
	  if(count >= SHELL_BLOCK_LENGTH) {
	     blockLength = SHELL_BLOCK_LENGTH;
	  } else {
		  blockLength = count;
	  }
	  
	  count -= blockLength;     
	  
	  ShellReadChar(&buffer[0]); // first char is invalid
	  ShellReadChar(&buffer[0]); // read block size
	  
	  // read data chars
      for(i = 0 ; i < blockLength ; i++) {
         if(ShellReadChar(&buffer[i]) == NO) {
			 SPIUnregisterCallbackRx();
		     SpiStopReceive(SPI_DEVICE);
	         return SHELL_HOST_ERR_EXEC_ERROR;
         } 
      }

      if(count) {
		 // prepare for next block 
		 charsReceived = 0;

		 if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
			SPIUnregisterCallbackRx();
		    SpiStopReceive(SPI_DEVICE);
            return SHELL_HOST_ERR_EXEC_ERROR;
         }

	  }	else { // all data received
         SPIUnregisterCallbackRx();
	  }

      // write data
      if(!file_write_buf(buffer, blockLength)) {
		  SpiStopReceive(SPI_DEVICE);
		  return SHELL_HOST_ERR_WRITE_ERROR;
	  }
   }

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
      return SHELL_HOST_ERR_EXEC_ERROR;
   }

   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellMkdir
//-----------------------------------------------------------------------------

byte ShellCmdMkdir(void)
{
   byte *path = ShellParamBuffer;	
	
   // valid path
   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // make dir
   if(!nav_dir_make((FS_STRING) path)) {
      return SHELL_HOST_ERR_DIR_ERROR;
   }
	  
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellRmdir
//-----------------------------------------------------------------------------

// hl�s� ok i kdy� soubor/adres�� neexistuje

byte ShellCmdRmdir(void) // m��e slou�it i k odstran�n� soubor�
{
   byte *path = ShellParamBuffer;	
   Fs_index sav_index;

   if(path[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // Save the position
   sav_index = nav_getindex();
   
   while( 1 )
   {
      // Restore the position
      nav_gotoindex(&sav_index);
	  
      // Select file or directory
      if(!nav_setcwd((FS_STRING) path, true, false)) {
         break;
	  }
		 
      // Delete file or directory
      if(!nav_file_del(false)) {
         return SHELL_HOST_ERR_DIR_ERROR;
      }
   }

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellInit
//-----------------------------------------------------------------------------

byte ShellCmdRename(void)
{
   byte *name0 = ShellParamBuffer; // change this name
   byte *name1 = ShellParamBuffer; // to this name
	
   // valid name;
   if(name0[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // find second name
   while(*(++name1) != '\0');
   
   name1++;
   
   // valid
   if(name1[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // Select source file
   if(!nav_setcwd((FS_STRING) name0, true, false)) {
      return SHELL_HOST_ERR_FILE_ERR;
   }
   
   // Rename file or directory
   if(!nav_file_rename((FS_STRING) name1)) {
      return SHELL_HOST_ERR_FILE_ERR;
   }
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }   
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdFileSeek
//-----------------------------------------------------------------------------

byte ShellCmdFileSeek(void) {
	dword pos = *((dword *) ShellParamBuffer);
	byte origin = ShellParamBuffer[4];
	
	// Seek
	if(file_seek(pos, origin) == false) {
		return SHELL_HOST_ERR_FILE_ERR;
	}

   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }  
   
   return SHELL_HOST_ERR_OK;
}

//-----------------------------------------------------------------------------
// ShellCmdTouch
//-----------------------------------------------------------------------------

byte ShellCmdTouch(void) 
{
   byte *name = ShellParamBuffer;	
   
   // valid name
   if(name[0] == '\0') {
	   return SHELL_HOST_ERR_BAD_CMD;
   }
   
   // create file
   if(!nav_file_create((FS_STRING)name)) {
	   return SHELL_HOST_ERR_FILE_ERR;
   }
   
   if(ShellTransmitData(NULL, 0, NO, NO) == NO) {
	   return SHELL_HOST_ERR_EXEC_ERROR;
   }  
   
   return SHELL_HOST_ERR_OK;
}


//-----------------------------------------------------------------------------
// ShellInit
//-----------------------------------------------------------------------------

void ShellInit(void)
{

   
   SpiPortInit();
   spi_initSlave( SPI_DEVICE, 8, 0);
      
   SPIRegisterCallbackTx(&ShellCallbackTx);

   spi_enable( SPI_DEVICE);
  
   ShellStatusInit();
   ShellStatusReady();
  
   INTC_register_interrupt((__int_handler) (&SpiISR), AVR32_SPI_IRQ, AVR32_INTC_INT1); // level mus� b�t vy��� ne� level usb�ka
   InterruptEnable();

   SpiStartReceive(SPI_DEVICE);
}

//-----------------------------------------------------------------------------
// ShellScanCmd
//-----------------------------------------------------------------------------

void ShellScanCmd(void)
{
   byte ptr = 0;
   TShellCmd *cmd;
   byte ch;
   byte i;
   byte error = SHELL_HOST_ERR_OK;
   char buffer[100];
   dword timeout = SHELL_CHAR_TIMEOUT;

   // nothing to do
   if(SpiBufferIsEmpty()) {
      return;
   }

   if(SpiReadBufferChar(&ch)) { // read cmd
      // find command
	  for(i = 0 ; i < SHELL_COMMANDS_NB ; i++) {
			if(ShellCommands[i].cmd == (byte) ch) {
			   cmd = &ShellCommands[i];
			   break;
			}
	  }
	
	  // invalid command
	  if(i == SHELL_COMMANDS_NB) {
         error = SHELL_HOST_ERR_BAD_CMD;
	  }
			
	  ptr = 0;
	  ShellParamBuffer[0] = '\0';
			
	  while(error == SHELL_HOST_ERR_OK) { // read param
		 if(cmd->VariableLength == NO) {
			   if(cmd->StaticLength == ptr) {
				  break;
			   }
		 }
			   
		 if(!SpiReadBufferChar(&ch)) { // wait for next char till timeout expires
			   if(!timeout--) { // timeout, error
				  error = SHELL_HOST_ERR_BAD_CMD;
				  continue;
			   }
				   
			   SysUDelay(1);

			   continue;
		 }

		 timeout = SHELL_CHAR_TIMEOUT;
			   
		 if(ch == SHELL_CMD_DELIMITER) { // cmd delimiter received
            if(cmd->VariableLength == YES && ptr >= cmd->StaticLength) { // check if it is valid delimiter
			   if(ptr > 0 && ShellParamBuffer[ptr - 1] != '\0') { // null-terminate the string, for sure
	              ShellParamBuffer[ptr - 1] = '\0';
               }
   
			   break;
			}
		 }
			   
		 if(ptr >= SHELL_PARAMS_MAX_LENGTH) { // param exceeds its allowed length, error
			error = SHELL_HOST_ERR_BAD_CMD;
			break;
		 }
			   
		 // store char
		 ShellParamBuffer[ptr] = ch;
				   
		 ptr++;
	  }	

	  SpiStopReceive(SPI_DEVICE); // it is useless to receive data now		

	  if(error == SHELL_HOST_ERR_OK) {
		 // tell master we are busy
		 ShellStatusBusy();
		 SysDelay(1); // pokud v obsluze p��kazu bude p��li� brzy n�sledovat ready, master nestihne zaregistrovat busy = p��kaz se nezda��
		 error = cmd->Function();
	  } 

	  if(error != SHELL_HOST_ERR_OK) {
	     ShellTransmitError(error);
				
         uprintf("\n\nError: %d", error);			
         // ??? p�i jak�mkoliv erroru by bylo dobr� po�kat na CS = 1, teprve pak pokra�ovat ???
	  }

      /*
	     Nen� zaru�eno, �e v tomto okam�iku je posledn� SPI transakce �pln� ukon�ena
		 SPI transakce se vyhodnocuje jako ukon�en� v okam�iku, kdy nem�me dal�� byte k posl�n�
		 V posuvn�m registru se ale je�t� nach�z� posledn� byte, kter� je pos�l�n...
		 P�i pomal�ch SPI hodin�ch se stane, �e posledn� byte transakce bude p�ijat a vyhodnocen jako nov� p��kaz
	  */

      /*for(i = 0 ; i < 15 ; i++) {
	     uprintf("%d", SpiRB(i));
	  }*/
	  
	  SysDelay(1);

      SpiFlushBuffer();
	  ShellStatusReady();
	  SpiStartReceive(SPI_DEVICE);	
   }
}