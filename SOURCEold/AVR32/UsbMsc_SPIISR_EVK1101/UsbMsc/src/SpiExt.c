//*****************************************************************************
//
//    SpiExt.c   SPI extended
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "spi.h"
#include "spiExt.h"
#include "uconio.h"
#include "Hardware.h"


// Vylep�it p�ed�v�n� spi modulu jako parametru funkc�
// Nebo se na p�ed�v�n� spi modulu vyka�lat - alespo� budou vol�n� rychlej��
#define SPI_TRANSACTION_TIMEOUT  1000000 // in us

// receive buffer
#define SPI_BUFFER_LENGTH  256

#if SPI_BUFFER_LENGTH != 256
   #error "SPI buffer length must be 256 bytes"
#endif;

byte SpiBuffer[SPI_BUFFER_LENGTH];
volatile byte SpiBufferWritePtr = 0;
volatile byte SpiBufferReadPtr = 0;

// transmit buffer
volatile word SpiTxLength = 0;
volatile word SpiTxPtr = 0;
volatile byte *SpiTxBuffer = 0;
volatile byte SpiTxComplete = YES;

// callbacks
volatile void (*SPICallbackRx)(void) = NULL; 
volatile void (*SPICallbackTx)(void) = NULL;

volatile byte SpiRxIntEnabled = NO;

//-----------------------------------------------------------------------------
// SpiEnableRxInt
//-----------------------------------------------------------------------------

void SpiEnableRxInt(volatile avr32_spi_t *spi)
{
   // clear flag
   (void) (spi->sr & AVR32_SPI_RDRF_OFFSET);	
   (void) spi->rdr;	
   
   // enable interrupt
   spi->ier |= (1 << AVR32_SPI_RDRF_OFFSET);
   SpiRxIntEnabled = YES;
}

//-----------------------------------------------------------------------------
// SpiISR
//-----------------------------------------------------------------------------
__attribute__((__interrupt__)) void SpiISR(void)
{
   if((AVR32_SPI.sr & (1 << AVR32_SPI_RDRF_OFFSET))) { // char received
	  if(SpiRxIntEnabled == YES) {
		 // neo�et�ujeme p�ete�en� bufferu
	     SpiBuffer[SpiBufferWritePtr++] = AVR32_SPI.rdr;
	  
	     // notice higher application level that char has been received
	  	 if(SPICallbackRx != NULL) {
		    SPICallbackRx();
		 }
	  
	     return; // exit ISR immediatelly
	  }
   } 

   if(AVR32_SPI.sr & (1 << AVR32_SPI_TDRE_OFFSET)) { // transmit register is empty
      if(SpiTxComplete == NO) { // transaction in progress
         if(SpiTxPtr >= SpiTxLength) {
			// !!! Toto nen� �pln� konec transakce - posledn� byte se je�t� vys�l�
            SpiTxComplete = YES;
			SpiDisableTxInt(&AVR32_SPI);
			
			// notice higher application level that transmission is complete
			if(SPICallbackTx != NULL) {
				SPICallbackTx();
			}
         } else {
            AVR32_SPI.tdr = SpiTxBuffer[SpiTxPtr];
            SpiTxPtr++;			
         }
      } else {
		 AVR32_SPI.tdr = 0xFF; 
	  }
   }

   StatusLed2Tog();
}
//-----------------------------------------------------------------------------
// SpiBufferIsEmpty
//-----------------------------------------------------------------------------

TYesNo SpiBufferIsEmpty()
{
   if(SpiBufferReadPtr == SpiBufferWritePtr) {
      return YES;
   } else {
	  return NO;
   }
}

//-----------------------------------------------------------------------------
// SpiReadBufferChar
//-----------------------------------------------------------------------------

TYesNo SpiReadBufferChar(byte *ch)
{
   if(SpiBufferReadPtr == SpiBufferWritePtr) {
      return NO;
   }
      
   *ch = SpiBuffer[SpiBufferReadPtr];
	  
	SpiBufferReadPtr++;
	   
   if(SpiBufferReadPtr >= SPI_BUFFER_LENGTH) {
	   SpiBufferReadPtr = 0;
   }  

   return YES; 
}

//-----------------------------------------------------------------------------
// SpiReadBuffer
//-----------------------------------------------------------------------------

word SpiReadBuffer(byte *buffer, word length)
{
   word i = 0;

   while(SpiBufferReadPtr != SpiBufferWritePtr) {
      buffer[i] = SpiBuffer[SpiBufferReadPtr];
   
      i++;
   	
      SpiBufferReadPtr++;
	   
      if(SpiBufferReadPtr >= SPI_BUFFER_LENGTH) {
	      SpiBufferReadPtr = 0;
      }  
   }   

   return i; 
}

//-----------------------------------------------------------------------------
// SpiFlushBuffer
//-----------------------------------------------------------------------------

void SpiFlushBuffer()
{
	SpiBufferReadPtr = SpiBufferWritePtr = 0;
}	

//-----------------------------------------------------------------------------
// SpiWaitForTransmitComplete
//-----------------------------------------------------------------------------

TYesNo SpiWaitForTransmitComplete(volatile avr32_spi_t *spi)
{
	dword timeout = SPI_TRANSACTION_TIMEOUT;
	
	while(SpiTxComplete == NO) {
		if(!timeout--) {
			SpiStopTransmit(spi);
			return NO;
		}
		
		SysUDelay(1);
	}
	
	return YES;
}

//-----------------------------------------------------------------------------
// SpiStartTransmit
//-----------------------------------------------------------------------------

void SpiStartTransmit(volatile avr32_spi_t *spi, byte *buffer, word length)
// Starts interrupt driven SPI transaction
{
   SpiTxLength = length;
   SpiTxPtr = 0;
   SpiTxBuffer = buffer;
   SpiTxComplete = NO;

   SpiEnableTxInt(spi);
}





byte SpiRB(byte pos) {
	return SpiBuffer[pos];
}