//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef FREERTOS_USED
  #if (defined __GNUC__)
    #include "nlao_cpu.h"
  #endif
#else
  #include <stdio.h>
#endif
#include "compiler.h"
#include "preprocessor.h"
#include "main.h"
#include "board.h"
#include "print_funcs.h"
#include "intc.h"
#include "power_clocks_lib.h"
#include "gpio.h"
#include "ctrl_access.h"
#include "conf_usb.h"
#include "usb_task.h"
#if USB_DEVICE_FEATURE == true
  #include "device_mass_storage_task.h"
#endif
#if USB_HOST_FEATURE == true
  #include "host_mass_storage_task.h"
#endif
#include "ushell_task.h"


#include "spi.h"    // spi

#include "Hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"    // UART printf


#include <stdio.h>    // UART printf


#define __COM1__
#include "Com.h"



//_____ D E F I N I T I O N S ______________________________________________

/*! \name System Clock Frequencies
 */
//! @{
static pcl_freq_param_t pcl_freq_param =
{
  .cpu_f        = FCPU_HZ,
  .pba_f        = FPBA_HZ,
  .osc0_f       = FOSC0,
  .osc0_startup = OSC0_STARTUP
};
//! @}

/*! \name Hmatrix bus configuration
 */
void init_hmatrix(void)
{
  union
  {
    unsigned long                 scfg;
    avr32_hmatrix_scfg_t          SCFG;
  } u_avr32_hmatrix_scfg;

  // For the internal-flash HMATRIX slave, use last master as default.
  u_avr32_hmatrix_scfg.scfg = AVR32_HMATRIX.scfg[AVR32_HMATRIX_SLAVE_FLASH];
  u_avr32_hmatrix_scfg.SCFG.defmstr_type = AVR32_HMATRIX_DEFMSTR_TYPE_LAST_DEFAULT;
  AVR32_HMATRIX.scfg[AVR32_HMATRIX_SLAVE_FLASH] = u_avr32_hmatrix_scfg.scfg;
}


#ifndef FREERTOS_USED

  #if (defined __GNUC__)

/*! \brief Low-level initialization routine called during startup, before the
 *         main function.
 *
 * This version comes in replacement to the default one provided by the Newlib
 * add-ons library.
 * Newlib add-ons' _init_startup only calls init_exceptions, but Newlib add-ons'
 * exception and interrupt vectors are defined in the same section and Newlib
 * add-ons' interrupt vectors are not compatible with the interrupt management
 * of the INTC module.
 * More low-level initializations are besides added here.
 */
int _init_startup(void)
{
  // Import the Exception Vector Base Address.
  extern void _evba;

  // Load the Exception Vector Base Address in the corresponding system register.
  /*Set_system_register(AVR32_EVBA, (int)&_evba);

  // Enable exceptions.
  Enable_global_exception();

  // Initialize interrupt handling.
  INTC_init_interrupts();*/

  // Give the used CPU clock frequency to Newlib, so it can work properly.
  set_cpu_hz(pcl_freq_param.pba_f);

  // Don't-care value for GCC.
  return 1;
}

  #elif __ICCAVR32__

/*! \brief Low-level initialization routine called during startup, before the
 *         main function.
 */
/*int __low_level_init(void)
{

#if BOARD == UC3C_EK
	// Disable WDT At Startup
	AVR32_WDT.ctrl = 0x55000000;
	AVR32_WDT.ctrl = 0xAA000000;
#endif

  // Enable exceptions.
  Enable_global_exception();

  // Initialize interrupt handling.
  INTC_init_interrupts();

  // Request initialization of data segments.
  return 1;
}*/

  #endif  // Compiler

#endif  // FREERTOS_USED






/*

CMDS

*/





#define HOST_NOT_READY     0x55
#define HOST_READY         0x99



#define HOST_ERR_OK       0x00
#define HOST_ERR_BAD_CMD          0x77
#define HOST_ERR_EXEC_ERROR       0x78
#define HOST_ERR_NO_DRIVE       0x79
#define HOST_ERR_DRIVE_ERROR       0x80











/*

Dopln�k k SPI

*/


#define SPI_BYTE_TIMEOUT  10000 // in us
#define SPI_TRANSACTION_TIMEOUT  1000000 // in us

#define SPI_MASTER_SPEED    F_BUS_A     // SPI speed at bus clock
#define SPI_NPCS            2           // first chipselect
#define SPI_DEVICE          &AVR32_SPI

#define SPI_BUFFER_LENGTH  100

char SpiBuffer[SPI_BUFFER_LENGTH];
volatile unsigned char SpiBufferWritePtr = 0;
volatile unsigned char SpiBufferReadPtr = 0;
byte SpiTxDummyChar = 0xFF;

volatile word SpiTxLength = 0;
volatile word SpiTxPtr = 0;
volatile byte *SpiTxBuffer = 0;
volatile byte SpiTxComplete = YES;

#define SpiSetTxDummyChar(x)     (SpiTxDummyChar = x)
volatile byte ia = 0;
//-----------------------------------------------------------------------------
// SpiISR
//-----------------------------------------------------------------------------

__attribute__((__interrupt__)) static void SpiISR(void) {

   if((AVR32_SPI.sr & (1 << AVR32_SPI_RDRF_OFFSET))) { // byte received
	  SpiBuffer[SpiBufferWritePtr] = AVR32_SPI.rdr;

	  SpiBufferWritePtr++;
	   
	  if(SpiBufferWritePtr >= SPI_BUFFER_LENGTH) {
	      SpiBufferWritePtr = 0;
	  }
	  
	  if(SpiBufferWritePtr == SpiBufferReadPtr) {
	     SpiBufferReadPtr++;
		 
		 if(SpiBufferReadPtr >= SPI_BUFFER_LENGTH) {
		    SpiBufferReadPtr = 0;
		 }
	  }
   } else if((AVR32_SPI.sr & (1 << AVR32_SPI_TXEMPTY_OFFSET))) { // transmit register is empty
	   
	   if(SpiTxComplete == YES) { // pokud nejsou data k posl�n�, po�leme pr�zdn� znak
	     AVR32_SPI.tdr = HOST_NOT_READY;
	   } else { // prob�h� transakce, po�leme dal�� byte
		  if(SpiTxPtr >= SpiTxLength) {
		    SpiTxComplete = YES;
			return;
		  }
		   
		  AVR32_SPI.tdr = SpiTxBuffer[SpiTxPtr]; 
		 ia++;
		  SpiTxPtr++;
	   }
   }
}

//-----------------------------------------------------------------------------
// SpiBufferIsEmpty
//-----------------------------------------------------------------------------

TYesNo SpiBufferIsEmpty() {
   if(SpiBufferReadPtr == SpiBufferWritePtr) {
      return YES;
   } else {
	  return NO;
   }
}

//-----------------------------------------------------------------------------
// SpiReadBufferChar
//-----------------------------------------------------------------------------

TYesNo SpiReadBufferChar(byte *ch) {
   byte buffer[10];
   
   if(SpiBufferReadPtr == SpiBufferWritePtr) {
      return NO;
   }
      
   Disable_global_interrupt();
	     
   *ch = SpiBuffer[SpiBufferReadPtr];
	  
	SpiBufferReadPtr++;
	   
   if(SpiBufferReadPtr >= SPI_BUFFER_LENGTH) {
	   SpiBufferReadPtr = 0;
   }  
   
   Enable_global_interrupt();
   
   return YES; 
}

//-----------------------------------------------------------------------------
// SpiReadBuffer
//-----------------------------------------------------------------------------

word SpiReadBuffer(byte *buffer, word length) {
   word i = 0;

   Disable_global_interrupt();
   
   while(SpiBufferReadPtr != SpiBufferWritePtr) {
      buffer[i] = SpiBuffer[SpiBufferReadPtr];
   
      i++;
   	
      SpiBufferReadPtr++;
	   
      if(SpiBufferReadPtr >= SPI_BUFFER_LENGTH) {
	      SpiBufferReadPtr = 0;
      }  
   }   

   Enable_global_interrupt();

   return i; 
}

//-----------------------------------------------------------------------------
// SpiTransmit
//-----------------------------------------------------------------------------

TYesNo SpiTransmit(char *buffer, word length) {
	dword timeout = SPI_TRANSACTION_TIMEOUT;
	
	SpiTxLength = length;
	SpiTxPtr = 0;
	SpiTxBuffer = buffer;
	SpiTxComplete = NO;

	spi_TxInterruptEnable(SPI_DEVICE);
	
	while(SpiTxComplete == NO) {
		if(!timeout--) {
			spi_TxInterruptDisable(SPI_DEVICE);
			return NO;
		}
		
		SysUDelay(1);
	}
	
	spi_TxInterruptDisable(SPI_DEVICE);
	
	return YES;
}

//-----------------------------------------------------------------------------
// SpiFlushBuffer
//-----------------------------------------------------------------------------

void SpiFlushBuffer() {
	Disable_global_interrupt();
	SpiBufferReadPtr = SpiBufferWritePtr = 0;
	Enable_global_interrupt();
}	

TYesNo SpiWaitForTransmitComplete() {
	dword timeout = SPI_TRANSACTION_TIMEOUT;
	
	while(SpiTxComplete == NO) {
		if(!timeout--) {
			spi_TxInterruptDisable(SPI_DEVICE);
			/*spi_reset(SPI_DEVICE);
			spi_initSlave( SPI_DEVICE, 8, 0);
			spi_enable(SPI_DEVICE);*/
			SpiTxComplete = YES;
			return NO;
		}
		
		SysUDelay(1);
	}
	
	return YES;
}





#include "navigation.h"



//! @brief Appends the '\' char at the end of path
//!
void ushell_path_valid_syntac( char *path )
{
   uint8_t u8_tmp;
   
   // Compute size of substitute
   for( u8_tmp=0; u8_tmp<MAX_FILE_PATH_LENGTH; u8_tmp++ )
   {
      if( path[u8_tmp]==0)
         break;
   }
   // Append the '\' char for the nav_setcwd to enter the found directory
   if ( path[u8_tmp-1] != '\\')
   {
      path[u8_tmp]='\\';
      path[u8_tmp+1]=0;
   }
}








volatile byte a = 0;

byte ShellTransmitDataBuffer[300];

TYesNo ShellTransmitData(byte *buffer, word length, TYesNo WillContinue) {
   dword timeout = SPI_TRANSACTION_TIMEOUT;
   byte i;
   
   if(SpiWaitForTransmitComplete() == NO) {
	  return NO;
   }
   
   //spi_TxInterruptDisable(SPI_DEVICE);
   

   ShellTransmitDataBuffer[0] = HOST_READY;
   ShellTransmitDataBuffer[1] = (byte) length;

   i = length;
   while(i--) {
	   ShellTransmitDataBuffer[i + 2] = buffer[i];
   }

   SpiTxLength = length + 2;
   SpiTxPtr = 0;
   SpiTxBuffer = ShellTransmitDataBuffer;
   SpiTxComplete = NO;

   spi_TxInterruptEnable(SPI_DEVICE);
   
   if(WillContinue == NO) {
	   if(SpiWaitForTransmitComplete() == NO) {
		   spi_TxInterruptDisable(SPI_DEVICE);
		   return NO;
	   }
	   
	   spi_TxInterruptDisable(SPI_DEVICE);
   } else {
	   //spi_writeIM(SPI_DEVICE, HOST_NOT_READY);
   }

   return YES;
}

//-----------------------------------------------------------------------------
// ShellCmdDisk
//-----------------------------------------------------------------------------

byte ShellCmdDisk(void)
{
   byte i;
   byte buffer[4];

   //buffer[0] = HOST_READY;
   buffer[0] = nav_drive_nb();

   for(i = 0; i < buffer[0]; i++) {
	  buffer[i + 1] = 'a' + i;
   }
   
   if(ShellTransmitData(buffer, i + 1, NO) == YES) {
	   return HOST_ERR_OK;
   } else {
	   return HOST_ERR_EXEC_ERROR;
   }
   
   /*spi_writeIM(SPI_DEVICE, HOST_READY);
   
   if(SpiTransmit(buffer, i + 1) == YES) {
	   return HOST_ERR_OK;
   } else {
	   return HOST_ERR_EXEC_ERROR;
   }*/
}

//-----------------------------------------------------------------------------
// ShellCmdDF
//-----------------------------------------------------------------------------

byte ShellCmdDF(byte drive)
{
   qword buffer[2];
   Fs_index sav_index = nav_getindex();      // Save current position
   
   drive -= 'a';
   
   if(drive >= nav_drive_nb()) {
	  return HOST_ERR_NO_DRIVE; 
   }
   

   nav_drive_set( drive );      // Select drive
   if( !nav_partition_mount() ) { // Mount drive
      return HOST_ERR_DRIVE_ERROR;
   }
      

   // !!! vol�n� nav_partition_freespace() trv� velmi dlouho !!!!
   buffer[0] = (qword) (nav_partition_space() << FS_SHIFT_B_TO_SECTOR);
   buffer[1] = (qword) (nav_partition_freespace() << FS_SHIFT_B_TO_SECTOR);

   nav_gotoindex(&sav_index);// Restore position
   
   /* Mo�n� by bylo dobr� d�t sem
   
      if(CS == high) {
	     return HOST_ERR_OK
	  }
	  
      master data nevy�etl, proto�e v�, �e jejich p��prava m��e trvat dlouho, zept� se na n� n�sledn�, nov�m p��kazem
      pokud tady tento if nebude, funkce skon�� chybou, proto�e master bude t�ko tak dlouho �ekat na v�po�et voln�ho
	  m�sta a transakci ukon��.
   */
   
   if(ShellTransmitData((byte *) buffer, 16, NO) == YES) {
	   return HOST_ERR_OK;
   } else {
	   return HOST_ERR_EXEC_ERROR;
   }
}



#define SHELL_DATA_MAX_LENGTH    255

byte ShellCmdDir(word offset, word count)
{
   byte buffer[SHELL_DATA_MAX_LENGTH + 1 + MAX_FILE_PATH_LENGTH + 4 + 1];
   word bufferPtr = 0;
   dword temp;
   word temp2;
   word i;


   if(!nav_filelist_reset()) {
      return HOST_ERR_DRIVE_ERROR;
   }
   
   if(offset != 0) {
      nav_filelist_set( offset - 1, FS_FIND_NEXT );
   }

   // For each file in list
   while(count-- && nav_filelist_set(0, FS_FIND_NEXT) )
   {
      if(nav_file_isdir()) {
         buffer[bufferPtr] = 1;
      } else {
         buffer[bufferPtr] = 0;
      }

      bufferPtr++;

      nav_file_name((FS_STRING) buffer + bufferPtr, MAX_FILE_PATH_LENGTH, FS_NAME_GET, true);
	  uprintf("-%s-%d", (byte *) (buffer + bufferPtr), bufferPtr);
	  while(buffer[bufferPtr++] != '\0');

	  temp = nav_file_lgt();
	  
	  buffer[bufferPtr] = temp >> 24;
	  buffer[bufferPtr+1] = temp >> 16;
	  buffer[bufferPtr+2] = temp >> 8;
	  buffer[bufferPtr+3] = temp >> 0;
	  //*((dword *) (buffer + bufferPtr)) = nav_file_lgt();
	  
	  bufferPtr += 4;

      // data po�leme
	  if(bufferPtr > SHELL_DATA_MAX_LENGTH) {
		  uprintf("ptr.!%d!", bufferPtr);
         if(ShellTransmitData(buffer, SHELL_DATA_MAX_LENGTH, YES) == NO) {
	         return HOST_ERR_EXEC_ERROR;
         }
		 
		 // p�etekl� data zkop�rujeme na za��tek bufferu
		 temp2 = bufferPtr;
		 bufferPtr = 0;
		 for(i = SHELL_DATA_MAX_LENGTH; i < temp2; i++, bufferPtr++) {
			 buffer[bufferPtr] = buffer[i];
		 }
		 uprintf("ptr.!%d!", bufferPtr);
	  }
   }
   uprintf("ptr:!%d!", bufferPtr);
   if(ShellTransmitData(buffer, bufferPtr, NO) == NO) {
      return HOST_ERR_EXEC_ERROR;
   }
 
   // co kdy� posledn� datov� blok bude m�t p�esn� 255 byt� - master bude o�ek�vat dal�� data
   return HOST_ERR_OK;
}


byte ShellCmdCd(byte *path)
{ 
   if(path[0] == '\0') {
	   return HOST_ERR_BAD_CMD;
   }
   
   // Add '\' at the end of path, else the nav_setcwd select the directory but don't enter into.
   ushell_path_valid_syntac( path);
   
   // Call file system routine
   if( nav_setcwd((FS_STRING)path,true,false) == false )
   {
      return HOST_ERR_EXEC_ERROR;
   }
   
   if(ShellTransmitData(NULL, 0, NO) == YES) {
	   return HOST_ERR_OK;
   } else {
	   return HOST_ERR_EXEC_ERROR;
   }
}






#define CMD_UNSPECIFIED_LENGTH   0xFF


#define  CMD_DISK       0x01
#define  CMD_DF         0x02
#define  CMD_DIR        0x03
#define  CMD_CD         0x04

typedef struct {byte cmd; byte delim} TShellCmd;
	
TShellCmd ShellCommands[] = {
   { CMD_DISK,     CMD_UNSPECIFIED_LENGTH    },
   { CMD_DF,       1                         },	
   { CMD_DIR,      4                         },	
   { CMD_CD,       CMD_UNSPECIFIED_LENGTH    }		
};

#define ArrayLength(_array)   (sizeof(_array)/sizeof(_array[0]))
#define SHELL_COMMANDS_NB      ArrayLength(ShellCommands)











#define SHELL_PARAMS_MAX_LENGTH  50
#define SHELL_PARAM_DELIMITER  '*'
#define SHELL_CMD_DELIMITER  '\n'


byte ParamBuffer[SHELL_PARAMS_MAX_LENGTH];

//-----------------------------------------------------------------------------
// ShellScanCmd
//-----------------------------------------------------------------------------

void ShellScanCmd(void) {
	byte ptr = 0;
	TShellCmd *cmd;
	byte ch;
	byte i;
	byte error = HOST_ERR_OK;
	char buffer[100];
	word timeout = SPI_BYTE_TIMEOUT;
	
	if(!SpiBufferIsEmpty()) { // at least one char in buffer
		uputs("Neco prislo...");
		if(SpiReadBufferChar(&ch)) { // read cmd
			uputs("Precetl jsem prikaz");
			
			for(i = 0 ; i < SHELL_COMMANDS_NB ; i++) {
				if(ShellCommands[i].cmd == (byte) ch) {
					cmd = &ShellCommands[i];
					break;
				}
			}
			
			if(i == SHELL_COMMANDS_NB) {
				error = HOST_ERR_BAD_CMD;
			}
			
			ptr = 0;
			
			while(error == HOST_ERR_OK) { // read param
			   if(cmd->delim != CMD_UNSPECIFIED_LENGTH) {
				   if(cmd->delim == ptr) {
					   break;
				   }
			   }
			   
			   if(!SpiReadBufferChar(&ch)) { // wait for next char till timeout expires
				   if(!timeout--) { // timeout, error
					  error = HOST_ERR_BAD_CMD;
					  uputs("Err - timeout");
					  continue;
				   }
				   
				   SysUDelay(1);

				   continue;
			   }
			   
			   timeout = SPI_BYTE_TIMEOUT;
			   
			   if(ch == SHELL_CMD_DELIMITER) { // cmd delimiter received
                  if(cmd->delim == CMD_UNSPECIFIED_LENGTH) {
				     break;
				  }
			   }

			   
			   // store char
			   ParamBuffer[ptr] = ch;
				   
			   ptr++;
				   
			   if(ptr >= SHELL_PARAMS_MAX_LENGTH) { // param length exceeds its max length, error
			      uputs("Err - max param");
			      error = HOST_ERR_BAD_CMD;
			      break;
			   }
			}	

			
			if(error == HOST_ERR_OK) {
			   spi_RxInterruptDisable(SPI_DEVICE);
			   spi_writeIM(SPI_DEVICE, HOST_NOT_READY); // od te� dostane master odpov�� "NOT READY"
			   /*spi_TxInterruptEnable(SPI_DEVICE);
	           SpiSetTxDummyChar(HOST_NOT_READY);*/


			   uputs("\n\nCommand received: ");
			   uprintf("%d ", cmd);
			   uprintf("\nParam %d: ", ptr);
			   uprintf("%d ", ParamBuffer[0]);
			   uprintf("%d ", ParamBuffer[1]);
			   uprintf("%d ", ParamBuffer[2]);
			   uprintf("%d ", ParamBuffer[3]);
			   uprintf("%d ", ParamBuffer[4]);
			   uprintf("%d ", ParamBuffer[5]);

			
			   // vol�n� funkce p��slu�n� p�ijat�mu p��kazu
			   switch(cmd->cmd) {
			      case CMD_DISK:
			         uputs("\nCMD disk");
			         error = ShellCmdDisk();
				     break;
					 
			      case CMD_DF:
			         uputs("\nCMD disk free");
			         error = ShellCmdDF(ParamBuffer[0]);
				     break;

			      case CMD_DIR:
			         uputs("\nCMD dir");
			         error = ShellCmdDir(*((word *) (ParamBuffer + 0)), *((word *) (ParamBuffer + 2)));
				     break;
					 
			      case CMD_CD:
			         uputs("\nCMD dir");
					 
					 if(ptr > 0 && ParamBuffer[ptr - 1] != '\0') {
						 ParamBuffer[ptr] = '\0';
					 }
					 
			         error = ShellCmdCd(ParamBuffer);
				     break;
					 					 
				  default:
				     // program should never reach this
				     error = HOST_ERR_BAD_CMD;
				     break;
			   }	
			
               //spi_TxInterruptDisable(SPI_DEVICE);

			   for(i = 0 ; i < 15 ; i++) {
				   sprintf(buffer, "%d ", SpiBuffer[i]);
				   SpiBuffer[i] = 0;
			       uputs(buffer);
			   }
			
			   sprintf(buffer, "%d ", ptr);
			   uputs(buffer);
			
			   
			} 
			
			if(error != HOST_ERR_OK) {
				switch(error) { // tady by mohl b�t switch s v�emi errory
					case HOST_ERR_BAD_CMD:
					    break;
				}
				
				uprintf("\n\nError: %d", error);
				spi_writeIM(SPI_DEVICE, error);

                // p�i jak�mkoliv erroru by bylo dobr� po�kat na CS = 1, teprve pak pokra�ovat
				//SysDelay(1600);
			}
			
			
			SpiFlushBuffer(); 
			spi_RxInterruptEnable(SPI_DEVICE);
			
		}
	}	
	
		
}


int main (void)
{
   volatile unsigned short ch;
   char buffer[10];

   CpuInit();

   UartInit( &AVR32_USART1);
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   uputs( "Start...\n");
   

   SpiPortInit();
   spi_initSlave( SPI_DEVICE, 8, 0);
   
   INTC_register_interrupt((__int_handler) (&SpiISR), AVR32_SPI_IRQ, AVR32_INTC_INT0);
   InterruptEnable();
   spi_RxInterruptEnable(SPI_DEVICE);
   
   spi_enable( SPI_DEVICE);
   
   /*while(1) {
      if(spi_read(SPI_DEVICE, &ch) == SPI_OK) {
	     uputch(ch);
      }
   }*/

  if (pcl_configure_clocks(&pcl_freq_param) != PASS)
    return 42;

  // Initialize USART link.
  //init_dbg_rs232(pcl_freq_param.pba_f);


  // Init Hmatrix bus
  init_hmatrix();
  
  // Initialize USB clock.
  pcl_configure_usb_clock();

  // Initialize USB tasks.
  usb_task_init();
/*#if USB_DEVICE_FEATURE == true
  device_mass_storage_task_init();
#endif*/
#if USB_HOST_FEATURE == true
  host_mass_storage_task_init();
#endif
  //ushell_task_init(pcl_freq_param.pba_f);

StatusLedInit();

  while (true)
  {

    usb_task();
  /*#if USB_DEVICE_FEATURE == true
    device_mass_storage_task();
  #endif*/
  #if USB_HOST_FEATURE == true
    host_mass_storage_task();
  #endif
    ShellScanCmd();
	
	//ushell_task();
  }
}