//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef FREERTOS_USED
  #if (defined __GNUC__)
    #include "nlao_cpu.h"
  #endif
#else
  #include <stdio.h>
#endif
#include "compiler.h"
#include "preprocessor.h"
#include "main.h"
#include "board.h"
#include "print_funcs.h"
#include "intc.h"
#include "power_clocks_lib.h"
#include "gpio.h"
#include "ctrl_access.h"
#include "conf_usb.h"
#include "usb_task.h"
#if USB_DEVICE_FEATURE == true
  #include "device_mass_storage_task.h"
#endif
#if USB_HOST_FEATURE == true
  #include "host_mass_storage_task.h"
#endif
#include "ushell_task.h"


#include "Hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"    // UART printf

#include "spi.h"    // spi
#include <string.h>    // spi

#define __COM1__
#include "Com.h"

#include "EfsShell.h"

//_____ D E F I N I T I O N S ______________________________________________

/*! \name System Clock Frequencies
 */
//! @{
static pcl_freq_param_t pcl_freq_param =
{
  .cpu_f        = FCPU_HZ,
  .pba_f        = FPBA_HZ,
  .osc0_f       = FOSC0,
  .osc0_startup = OSC0_STARTUP
};
//! @}

/*! \name Hmatrix bus configuration
 */
void init_hmatrix(void)
{
  union
  {
    unsigned long                 scfg;
    avr32_hmatrix_scfg_t          SCFG;
  } u_avr32_hmatrix_scfg;

  // For the internal-flash HMATRIX slave, use last master as default.
  u_avr32_hmatrix_scfg.scfg = AVR32_HMATRIX.scfg[AVR32_HMATRIX_SLAVE_FLASH];
  u_avr32_hmatrix_scfg.SCFG.defmstr_type = AVR32_HMATRIX_DEFMSTR_TYPE_LAST_DEFAULT;
  AVR32_HMATRIX.scfg[AVR32_HMATRIX_SLAVE_FLASH] = u_avr32_hmatrix_scfg.scfg;
}


#ifndef FREERTOS_USED

  #if (defined __GNUC__)

/*! \brief Low-level initialization routine called during startup, before the
 *         main function.
 *
 * This version comes in replacement to the default one provided by the Newlib
 * add-ons library.
 * Newlib add-ons' _init_startup only calls init_exceptions, but Newlib add-ons'
 * exception and interrupt vectors are defined in the same section and Newlib
 * add-ons' interrupt vectors are not compatible with the interrupt management
 * of the INTC module.
 * More low-level initializations are besides added here.
 */
int _init_startup(void)
{
  // Import the Exception Vector Base Address.
  extern void _evba;

  // Load the Exception Vector Base Address in the corresponding system register.
  /*Set_system_register(AVR32_EVBA, (int)&_evba);

  // Enable exceptions.
  Enable_global_exception();

  // Initialize interrupt handling.
  INTC_init_interrupts();*/

  // Give the used CPU clock frequency to Newlib, so it can work properly.
  set_cpu_hz(pcl_freq_param.pba_f);

  // Don't-care value for GCC.
  return 1;
}

  #elif __ICCAVR32__

/*! \brief Low-level initialization routine called during startup, before the
 *         main function.
 */
/*int __low_level_init(void)
{

#if BOARD == UC3C_EK
	// Disable WDT At Startup
	AVR32_WDT.ctrl = 0x55000000;
	AVR32_WDT.ctrl = 0xAA000000;
#endif

  // Enable exceptions.
  Enable_global_exception();

  // Initialize interrupt handling.
  INTC_init_interrupts();

  // Request initialization of data segments.
  return 1;
}*/

  #endif  // Compiler

#endif  // FREERTOS_USED


int main (void)
{

   volatile unsigned short ch;
   char buffer[10];
   StatusLedInit();
   CpuInit();

   UartInit( &AVR32_USART1);
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   uputs( "Start...\n");
   
   EfsShellInit();
   
   /*while(1) {
      if(spi_read(SPI_DEVICE, &ch) == SPI_OK) {
	     uputch(ch);
      }
   }*/

  if (pcl_configure_clocks(&pcl_freq_param) != PASS)
    return 42;

  // Initialize USART link.
  //init_dbg_rs232(pcl_freq_param.pba_f);


  // Init Hmatrix bus
  init_hmatrix();
  
  // Initialize USB clock.
  pcl_configure_usb_clock();

  // Initialize USB tasks.
  usb_task_init();
/*#if USB_DEVICE_FEATURE == true
  device_mass_storage_task_init();
#endif*/
#if USB_HOST_FEATURE == true
  host_mass_storage_task_init();
#endif
  ushell_task_init(pcl_freq_param.pba_f);

StatusLedInit();

  while (true)
  {

    EfsShellTask();



       usb_task();
     /*#if USB_DEVICE_FEATURE == true
       device_mass_storage_task();
     #endif*/
     #if USB_HOST_FEATURE == true
       host_mass_storage_task();
     #endif
    
	
	   ushell_task();

	
  }
}