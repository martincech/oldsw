#include "System.h"

#include "EfsTransSlave.h"

#include "Hardware.h"
#include "SpiExt.h"

#include "uconio.h"

byte EfsTransRxBuffer[EFS_TRANS_MAX_PACKET_LENGTH] __attribute__ ((aligned (sizeof(dword))));
volatile TYesNo EfsTransDataReady = NO;
byte *EfsTransData;
word EfsTransDataSize;

byte EfsTransTxBuffer[EFS_TRANS_MAX_PACKET_LENGTH] __attribute__ ((aligned (sizeof(dword))));
volatile TYesNo EfsTransTransmitComplete = YES;






void EfsTransReceive(void) {
   EfsTransDataReady = NO;
   SpiRx(SPI_DEVICE, EfsTransRxBuffer, EFS_TRANS_MAX_PACKET_LENGTH);
   EfsTransStatusReady();
}

void EfsTransDataReadyCallback(void) {
   TEfsTransPacket *Packet;
   word i;
   word BytesReceived;
   byte *PacketStart;
   word DataSize;
   word MinusDataSize;
   word CrcRec;
   word CrcCalc;

   EfsTransStatusBusy();
   
   BytesReceived = SpiGetRxCount();

   if(BytesReceived == 0) {
	   EfsTransReceive();
	   return;
   }

   // Search for SOF
   i = 0;
   while(EfsTransRxBuffer[i++] != EFS_TRANS_SOF) {
	   if(i >= BytesReceived) {
		  EfsTransReceive();
	      return;
	   }
   }

   // Pokud je paket v bufferu posunut� mimo hranice dword, m�me probl�m
   if(((i - 1) % 4) != 0) {
	  EfsTransReceive();
	  return;
   }

   Packet = (TEfsTransPacket *) &(EfsTransRxBuffer[i - 1]);

   // Read & check data size
   if(Packet->DataSize > EFS_TRANS_MAX_DATA_SIZE) {
	   EfsTransReceive();
	   return;
   }

   Packet->MinusDataSize = -Packet->MinusDataSize;

   if(Packet->DataSize != Packet->MinusDataSize) {
	   EfsTransReceive();
	   return;
   }

   // Read & check CRC
   if(Packet->Crc != CalculateCRC(Packet->Data, Packet->DataSize)) {
	  EfsTransReceive();
	  return;
   }

   // Everything OK
   EfsTransDataReady = YES;
}

TYesNo EfsTransDataIsReady(void) {
   return EfsTransDataReady;
}

word EfsTransGetDataSize(void) {
   TEfsTransPacket *Packet = (TEfsTransPacket *) EfsTransRxBuffer;
   return Packet->Data;
}

byte* EfsTransGetData(void) {
   TEfsTransPacket *Packet = (TEfsTransPacket *) EfsTransRxBuffer;
   return Packet->Data;
}















void EfsTransTransmit(word size) {
   TEfsTransPacket *Packet = (TEfsTransPacket *) EfsTransTxBuffer;
   word PacketLength = EFS_TRANS_DATA_OFFSET + size + 2;
   word Crc;
   word MinusLength;
   word i = 0;
   EfsTransTransmitComplete = NO;

   if(size > EFS_TRANS_MAX_DATA_SIZE) {
	   return NO;
   }
   
   // SOF
   Packet->Sof = EFS_TRANS_SOF;
   
   // Length
   Packet->DataSize = size;
   Packet->MinusDataSize = -size;

   // CRC
   Packet->Crc = CalculateCRC(Packet->Data, Packet->DataSize);

   SpiTx(SPI_DEVICE, EfsTransTxBuffer, sizeof(*Packet) + size);
   EfsTransStatusReady();
}

void EfsTransTransmitCompleteCallback(void) {
   EfsTransStatusBusy();
   EfsTransTransmitComplete = YES;
}

byte* EfsTransGetTxBuffer(void) {
   return ((TEfsTransPacket *) EfsTransTxBuffer)->Data;
}

TYesNo EfsWaitForTransmitComplete(void) {
   dword timeout = EFS_TRANS_TRANSACTION_TIMEOUT;
   
   while(EfsTransTransmitIsComplete() == NO) {
      timeout--;
		
      if(!timeout) {
         return NO;
      }
   }

   return YES;
}

TYesNo EfsTransTransmitIsComplete(void) {
   return EfsTransTransmitComplete;
}



void EfsTransInit(void)
{

   
   SpiPortInit();
   spi_initSlave( SPI_DEVICE, 8, 0);
   spi_enable( SPI_DEVICE);

   EfsTransStatusInit();
   EfsTransStatusBusy();

// callbacky vy�e�it pomoc� defin� !!!
   SpiRegisterRxCallback(&EfsTransDataReadyCallback);
   SpiRegisterTxCallback(&EfsTransTransmitCompleteCallback);
   
   SpiEnableCSInt(SPI_DEVICE);

   Enable_global_interrupt();
}