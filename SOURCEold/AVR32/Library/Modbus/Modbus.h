#define MODBUS_CHANNELS_COUNT   1
#define MODBUS_RTU_ENABLED

#define MODBUS_DIAGNOSTICS_ENABLED






// UART receive status :
typedef enum {
#ifdef MODBUS_RTU_ENABLED
   MODBUS_RTU_MASTER,
   MODBUS_RTU_SLAVE,
#endif
#ifdef MODBUS_ASCII_ENABLED
   MODBUS_ASCII_MASTER,
   MODBUS_ASCII_SLAVE,
#endif
} EModbusMode;

// Modbus device address :
typedef enum {
#if MODBUS_CHANNELS_COUNT >= 1
   MODBUS_MODBUS0 = 0,
#endif
#if MODBUS_CHANNELS_COUNT >= 2
   MODBUS_MODBUS1,
#endif
   _MODBUS_LAST
} EModbusAddress;  

typedef enum {
   MODBUS_PARITY_EVEN,
   MODBUS_PARITY_ODD,
   MODBUS_PARITY_NONE
} EModbusParity;

void ModbusSetup(EModbusAddress Modbus, EModbusMode Mode, unsigned Baud, EModbusParity Parity);
void ModbusStart(EModbusAddress Modbus);
void ModbusExecute(EModbusAddress Modbus);
void ModbusStop(EModbusAddress Modbus);