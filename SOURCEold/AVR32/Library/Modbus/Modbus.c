#include "Modbus.h"
#include "ModbusDef.h"

#include "UartModbus.h"










#define MODBUS0_UART    UART_UART0
#define MODBUS1_UART    UART_UART1


#if MODBUS_CHANNELS_COUNT > 2
   #error "Unsupported channels count"
#endif



// Structure assigned to each individual UART channel
// Constant values to be stored in program memory
typedef volatile struct __packed PROGMEM { // !!! PACKED !!!
	EUartAddress    Uart;            // UART registers
} const TModbusConstDetails;


TModbusConstDetails _ModbusConst[] = {
#if MODBUS_CHANNELS_COUNT >= 1
   {
	  .Uart           = MODBUS0_UART, // warning: initialization discards qualifiers from pointer target type
   }
#endif

#if MODBUS_CHANNELS_COUNT >= 2
   ,
   {
	  .Uart           = MODBUS1_UART,
   }
#endif
};

typedef volatile struct __packed {
   EModbusMode Mode;
} TModbusDetails; // !!!!!! must be aligned to dword !!!!!!

TModbusDetails _Modbus[MODBUS_CHANNELS_COUNT]; // every member of array must begin on dword boundary !!! 


void ModbusSetup(EModbusAddress Modbus, EModbusMode Mode, unsigned Baud, EModbusParity Parity) {
   EUartAddress  UartAddress = _ModbusConst[Modbus].Uart;
   EUartMode     UartMode;
   word          UartBaud    = Baud;
   EUartFormat   UartFormat;           // Depends on Mode & parity



   UartStop(UartAddress);

   switch(Mode) {
	   
#ifdef MODBUS_RTU_ENABLED
      case MODBUS_RTU_MASTER:
	     UartMode = UART_MODE_MODBUS_RTU_MASTER;
		 goto rtu_parity;
		 
	  case MODBUS_RTU_SLAVE:
         UartMode = UART_MODE_MODBUS_RTU_SLAVE;
		 
rtu_parity:
	     switch(Parity) {
			case MODBUS_PARITY_EVEN:
			   UartFormat = UART_8BIT_EVEN;
			   break;
			   
			case MODBUS_PARITY_ODD:
			   UartFormat = UART_8BIT_ODD;
			   break;
			   
			case MODBUS_PARITY_NONE:
			   UartFormat = UART_8BIT_SPACE;
			   break;
			   
			default:
			   return;
		 }
	     break;
#endif

#ifdef MODBUS_ASCII_ENABLED
      case MODBUS_ASCII_MASTER:
	     UartMode = UART_MODE_MODBUS_ASCII_MASTER;
		 goto ascii_parity;
		 
	  case MODBUS_ASCII_SLAVE:
         UartMode = UART_MODE_MODBUS_ASCII_SLAVE;
		 
ascii_parity:
	     switch(Parity) {
			case MODBUS_PARITY_EVEN:
			   UartFormat = UART_7BIT_EVEN;
			   break;
			   
			case MODBUS_PARITY_ODD:
			   UartFormat = UART_7BIT_ODD;
			   break;
			   
			case MODBUS_PARITY_NONE:
			   UartFormat = UART_7BIT_SPACE;
			   break;
			   
			default:
			   return;
		 }
	     break;
		 
	  default:
	     return;
#endif
   }

   UartInit(UartAddress);
   _Modbus[Modbus].Mode = Mode;
   UartModeSet(UartAddress, UartMode);
   UartSetup(UartAddress, UartBaud, UartFormat);
}


void ModbusStart(EModbusAddress Modbus) {
   switch(_Modbus[Modbus].Mode) {
	   
#ifdef MODBUS_RTU_ENABLED
      case MODBUS_RTU_MASTER:
	  case MODBUS_RTU_SLAVE:
         UartModbusRtuInstall(_ModbusConst[Modbus].Uart);
		 break;
#endif 

#ifdef MODBUS_ASCII_ENABLED
      case MODBUS_ASCII_MASTER:
	  case MODBUS_ASCII_SLAVE:
	     //UartModbusAsciiInstall(_ModbusConst[Modbus].Uart);
	     break;
#endif 

      default:
	     return;
   }
   
   UartReceive(_ModbusConst[Modbus].Uart, 0, 0);
}

void ModbusProccessFrame() {
	/*#ifdef MB_ENABLE_SLAVE_ID
      case MBF_REPORT_SLAVE_ID :
         if( Size != sizeof( TSlaveId)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         SlaveId();
         break;
#endif // MB_ENABLE_SLAVE_ID*/
}



#ifdef MB_ENABLE_IDENTIFICATION
//-----------------------------------------------------------------------------
// Identification
//-----------------------------------------------------------------------------

static char code VendorName[]     = MB_VENDOR_NAME;
static char code ProductCode[]    = MB_PRODUCT_CODE;
static char code RevisionString[] = MB_REVISION_STRING;

static void Identification( void)
// Vrati identifikaci zarizeni
{
byte i;
byte Count;

   MBWriteAddress( MBCGetOwnAddress());
   MBWriteByte( MBGetFunctionCode());
   MBWriteByte( MB_MEI_DEVICE_IDENTIFICATION);   // MEIType
   MBWriteByte( MEI_BASIC_IDENTIFICATION);       // IdCode
   MBWriteByte( CL_BASIC_STREAM);                // ConformityLevel
   MBWriteByte( 0);                              // MoreFollows
   MBWriteByte( 0);                              // NextObjectId
   MBWriteByte( 3);                              // NumberOfObjects
   // object 0 - vendor :
   MBWriteByte( MEI_OBJECT_VENDOR);              // Id objektu
   Count = sizeof( VendorName) - 1;              // bez koncove nuly
   MBWriteByte( Count);                          // delka objektu
   for( i = 0; i < Count; i++){
      MBWriteByte( VendorName[ i]);
   }
   // object 1 - product :
   MBWriteByte( MEI_OBJECT_PRODUCT_CODE);        // Id objektu
   Count = sizeof( ProductCode) - 1;             // bez koncove nuly
   MBWriteByte( Count);                          // delka objektu
   for( i = 0; i < Count; i++){
      MBWriteByte( ProductCode[ i]);
   }
   // object 2 - revision :
   MBWriteByte( MEI_OBJECT_REVISION);            // Id objektu
   Count = sizeof( RevisionString) - 1;          // bez koncove nuly
   MBWriteByte( Count);                          // delka objektu
   for( i = 0; i < Count; i++){
      MBWriteByte( RevisionString[ i]);
   }
   MBWriteCrc();
} // Identification
#endif // MB_ENABLE_IDENTIFICATION


//-----------------------------------------------------------------------------
// Rx paket
//-----------------------------------------------------------------------------

static void RxPacketSlave( EModbusAddress Modbus)
// Zpracuje prijaty paket
{

TModbusPdu  *Pdu;
word        DataSize;
TYesNo      IsBroadcast;

   switch(_Modbus[Modbus].Mode) {
#ifdef MODBUS_RTU_ENABLED
	   case MODBUS_RTU_SLAVE:
	     Pdu = UartModbusRtuGetPdu(_ModbusConst[Modbus].Uart);
         DataSize = UartModbusRtuGetDataSize(_ModbusConst[Modbus].Uart);
         IsBroadcast = UartModbusRtuIsBroadcast(_ModbusConst[Modbus].Uart);
		 break;
#endif
		 
#ifdef MODBUS_ASCII_ENABLED
	   case MODBUS_ASCII_SLAVE:
	     Pdu = UartModbusAsciiGetPdu(_ModbusConst[Modbus].Uart);
         DataSize = UartModbusAsciiGetDataSize(_ModbusConst[Modbus].Uart);
         IsBroadcast = UartModbusAsciiIsBroadcast(_ModbusConst[Modbus].Uart);
		 break;
#endif
      default:
	     return;
   }

   switch( Pdu->FunctionCode){
	   /*
#ifdef MB_ENABLE_DISCRETE
      case MBF_READ_DISCRETE :
         if( Size != sizeof( TReadDiscrete)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRReadDiscrete( MBGetWord( Pdu->ReadDiscrete.Address), MBGetWord( Pdu->ReadDiscrete.Count));
         break;
#endif // MB_ENABLE_DISCRETE
#ifdef MB_ENABLE_COILS
      case MBF_READ_COILS :
         if( Size != sizeof( TReadCoils)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRReadCoils( MBGetWord( Pdu->ReadCoils.Address), MBGetWord( Pdu->ReadCoils.Count));
         break;

      case MBF_WRITE_SINGLE_COIL :
         if( Size != sizeof( TWriteSingleCoil)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRWriteSingleCoil( MBGetWord( Pdu->WriteSingleCoil.Address), MBGetWord( Pdu->WriteSingleCoil.Value));
         break;

      case MBF_WRITE_MULTIPLE_COILS :
         if( Pdu->WriteCoils.ByteCount !=  MBBitsToBytes( Pdu->WriteCoils.Count) ||
             Size != SizeofTWriteCoils() + Pdu->WriteCoils.ByteCount){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRWriteCoils( MBGetWord( Pdu->WriteCoils.Address), MBGetWord( Pdu->WriteCoils.Count), Pdu->WriteCoils.Data);
         break;
#endif // MB_ENABLE_COILS
#ifdef MB_ENABLE_INPUT_REGISTERS
      case MBF_READ_INPUT_REGISTERS :
         if( Size != sizeof( TReadInputRegisters)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRReadInputRegisters( MBGetWord( Pdu->ReadInputRegisters.Address), MBGetWord( Pdu->ReadInputRegisters.Count));
         break;
#endif // MB_ENABLE_INPUT_REGISTERS
#ifdef MB_ENABLE_REGISTERS
      case MBF_READ_HOLDING_REGISTERS :
         if( Size != sizeof( TReadRegisters)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRReadRegisters( MBGetWord( Pdu->ReadRegisters.Address), MBGetWord( Pdu->ReadRegisters.Count));
         break;

      case MBF_WRITE_MULTIPLE_REGISTERS :
         if( Pdu->WriteRegisters.ByteCount !=  2 * Pdu->WriteRegisters.Count ||
             Size != SizeofTWriteRegisters() + Pdu->WriteRegisters.ByteCount){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRWriteRegisters( MBGetWord( Pdu->WriteRegisters.Address), MBGetWord( Pdu->WriteRegisters.Count), Pdu->WriteRegisters.Data);
         break;

      case MBF_WRITE_SINGLE_REGISTER :
         if( Size != sizeof( TWriteSingleRegister)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRWriteSingleRegister( MBGetWord( Pdu->WriteSingleRegister.Address), MBGetWord( Pdu->WriteSingleRegister.Value));
         break;

      case MBF_MASK_WRITE_REGISTER :
         if( Size != sizeof( TMaskWriteRegister)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRMaskWriteRegister( MBGetWord( Pdu->MaskWriteRegister.Address), MBGetWord( Pdu->MaskWriteRegister.AndMask),
                               MBGetWord( Pdu->MaskWriteRegister.OrMask));
         break;

      case MBF_RW_MULTIPLE_REGISTERS :
         if( Pdu->ReadWriteRegisters.WriteByteCount !=  2 * Pdu->ReadWriteRegisters.WriteCount ||
             Size != SizeofTReadWriteRegisters() + Pdu->ReadWriteRegisters.WriteByteCount){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRReadWriteRegisters( MBGetWord( Pdu->ReadWriteRegisters.ReadAddress), MBGetWord( Pdu->ReadWriteRegisters.ReadCount),
                                           MBGetWord( Pdu->ReadWriteRegisters.WriteAddress), MBGetWord( Pdu->ReadWriteRegisters.WriteCount),
                                           Pdu->ReadWriteRegisters.Data);
         break;
#endif // MB_ENABLE_REGISTERS
#ifdef MB_ENABLE_FIFO
      case MBF_READ_FIFO_QUEUE :
         if( Size != sizeof( TReadFifoQueue)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         MBRReadFifoQueue( MBGetWord( Pdu->ReadFifoQueue.Address));
         break;
#endif // MB_ENABLE_FIFO
#ifdef MB_ENABLE_EXCEPTION_STATUS
      case MBF_READ_EXCEPTION_STATUS :
         if( Size != sizeof( TExceptionStatus)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         ExceptionStatus();
         break;
#endif // MB_ENABLE_EXCEPTION_STATUS
#ifdef MB_ENABLE_DIAGNOSTIC
      case MBF_DIAGNOSTIC :
         if( Size != sizeof( TDiagnostic)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         Diagnostic( MBGetWord( Pdu->Diagnostic.SubFunction), MBGetWord( Pdu->Diagnostic.Data));
         break;
#endif // MB_ENABLE_DIAGNOSTIC
#ifdef MB_ENABLE_COM_EVENT
      case MBF_GET_COM_EVENT_COUNTER :
         if( Size != sizeof( TComEventCounter)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         Status.ComEventCount--;       // nezapocitana zprava
         ComEventCounter();
         break;

      case MBF_GET_COM_EVENT_LOG :
         if( Size != sizeof( TComEventLog)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         Status.ComEventCount--;       // nezapocitana zprava
         ComEventLog();
         break;
#endif // MB_ENABLE_COM_EVENT
#ifdef MB_ENABLE_SLAVE_ID
      case MBF_REPORT_SLAVE_ID :
         if( Size != sizeof( TSlaveId)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         SlaveId();
         break;
#endif // MB_ENABLE_SLAVE_ID*/
#ifdef MB_ENABLE_IDENTIFICATION
      case MBF_READ_DEVICE_ID :
         if( Size != sizeof( TIdentification)){
            MBException( MBEX_SLAVE_DEVICE_FAILURE);
            break;
         }
         if( Pdu->Identification.MEIType != MB_MEI_DEVICE_IDENTIFICATION){
            // jina informace nez identifikace zarizeni
            MBException( MBEX_ILLEGAL_FUNCTION);
            break;
         }
         if( Pdu->Identification.IdCode != MEI_BASIC_IDENTIFICATION){
            // rozsirena identifikace zarizeni
            MBException( MBEX_ILLEGAL_FUNCTION);
            break;
         }
         if( Pdu->Identification.ObjectId != MEI_OBJECT_VENDOR){
            // podporujeme jen uplnou informaci
            MBException( MBEX_ILLEGAL_FUNCTION);
            break;
         }
         Identification();
         break;
#endif // MB_ENABLE_IDENTIFICATION

//-----------------------------------------------------------------------------
      default :
         MBException( MBEX_ILLEGAL_FUNCTION);
         break;
   }
   if( IsBroadcast == NO){
      //ComTxStart();
   }

   switch(_Modbus[Modbus].Mode) {
#ifdef MODBUS_RTU_ENABLED
	   case MODBUS_RTU_SLAVE:
	     UartModbusRtuFrameProccessed(_ModbusConst[Modbus].Uart);
		 break;
#endif
		 
#ifdef MODBUS_ASCII_ENABLED
	   case MODBUS_ASCII_SLAVE:
	     UartModbusAsciiFrameProccessed(_ModbusConst[Modbus].Uart);
		 break;
#endif
      default:
	     return;
   }

} // RxPacket


void ModbusExecute(EModbusAddress Modbus) {
   switch( UartReceiveStatus( _ModbusConst[Modbus].Uart)){
      case UART_RECEIVE_FRAME:
	     switch(_Modbus[Modbus].Mode) {
#ifdef MODBUS_RTU_ENABLED
			 case MODBUS_RTU_SLAVE:
#endif
#ifdef MODBUS_ASCII_ENABLED
			 case MODBUS_ASCII_SLAVE:
#endif
			   RxPacketSlave(Modbus);
			   break;
		 }
	     
		 
	     break;
   }
}

void ModbusStop(EModbusAddress Modbus) {
	UartStop(_ModbusConst[Modbus].Uart);
}




//#include "UartModbus.c"