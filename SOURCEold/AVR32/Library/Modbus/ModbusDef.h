#ifndef _ModbusDef_H_
#define _ModbusDef_H_



#define MODBUS_RTU_CHARACTER_LENGTH 11

#define MODBUS_FRAME_SIZE_MAX     256

#define MODBUS_DATA_SIZE_MAX_MAX  252

#define MODBUS_DATA_SIZE_MAX      252

#if MODBUS_DATA_SIZE_MAX % 2 != 0 // kv�li zarovn�n� struktur
   #error Data size must be an even number
#endif

#if MODBUS_DATA_SIZE_MAX > MODBUS_DATA_SIZE_MAX_MAX // kv�li zarovn�n� struktur
   #error Max of data size exceeded
#endif

#include "Uni.h"

typedef struct __packed { // !!! PACKED !!!
	byte FunctionCode;
	byte Data[MODBUS_DATA_SIZE_MAX];
} TModbusPdu;

#ifdef MODBUS_DIAGNOSTICS_ENABLED
   typedef struct __packed { // !!! PACKED !!!
      dword    ReturnBusMessageCount;
      dword    ReturnBusCommunicationErrorCount;
      dword    ReturnSlaveExceptionErrorCount;
      dword    ReturnSlaveMessageCount;
      dword    ReturnSlaveNoResponseCount;
      dword    ReturnSlaveNakCount;
      dword    ReturnSlaveBusyCount;
      dword    ReturnBusCharacterOverrunCount;
   } TModbusException;
#endif


#endif