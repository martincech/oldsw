//*****************************************************************************
//
//    UartAscii.c       UART ASCII mode
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

/*
 Nativn� m�d
 P�eh�zel jsem n�kter� polo�ky r�mce aby l�pe vych�zelo jejich zarovn�n� v pam�ti
 a nebylo tak nutn� pou��vat dummy byty ve struktu�e r�mce

   <SOF> <Address> <HeaderCrc> <Size LSB> <Size MSB> <payload DATA> <DataCrc LSB> <DataCrc MSB>
        |<-              Header                   ->|

 Slave mod zahazuje Rx r�mce s adresou odli�nou od nastaven� (UartAssignSlaveAddress())
 Slave m�d automaticky pos�l� svou vlastn� adresu v Tx r�mci (funkce UartSendAddress() nem� v slave m�du ��dn� vliv)

*/

#include "UartCommon.h"


typedef struct __packed { // !!! PACKED !!!
	byte Address;
	byte HeaderCrc;
    word DataSize;
	word DataCrc;
} TUartNativeFrame;


typedef struct __packed { // !!! PACKED !!!
	TUartDetailsCommon  UartCommon;
    TUartNativeFrame    RxFrame;
	word                RxCurrentCrc;  // Used for Rx CRC computation
	TUartNativeFrame    TxFrame;
	
#ifdef UART_DEBUG_ENABLE
    word               NativeAddressedFrames;
	word               NativeTotalFrames;
	word               NativeBadDataCrc;
	word               NativeBadHeaderCrc;
	word               NativeOverrun;
#endif	

#ifdef UART_SLAVE_ENABLE	
	byte                SlaveAddress;
#endif
} TUartDetailsNative;

#define UART_NATIVE_SOF    0x55

// Flags
// Make sure to manipulate only UART_RX_FLAGS part of the Flags variable
#define UartNativeIsRxDataState(Uart)         (_Uart[Uart].Flags & 0x08)
#define UartNativeRxDataState(Uart)           (_Uart[Uart].Flags |= 0x08)
#define UartNativeIsRxFrameState(Uart)        !UartNativeIsRxDataState(Uart)
#define UartNativeRxFrameState(Uart)          (_Uart[Uart].Flags &= ~0x08)
#define UartNativeGetRxFrameState(Uart)       (_Uart[Uart].Flags & 0x07)
#define UartNativeIncRxFramePointer(Uart)     (_Uart[Uart].Flags += 0x01)  
#define UartNativeGetRxFramePointer(Uart)     ((_Uart[Uart].Flags & UART_RX_FLAGS) - 1) 
                                              // Decrement for SOF - it is not present in the frame structure

// Make sure to manipulate only UART_TX_FLAGS part of the Flags variable										
#define UartNativeIsTxDataState(Uart)         (_Uart[Uart].Flags & 0x80)
#define UartNativeTxDataState(Uart)           (_Uart[Uart].Flags |= 0x80)
#define UartNativeIsTxFrameState(Uart)        !UartNativeIsTxDataState(Uart)
#define UartNativeTxFrameState(Uart)          (_Uart[Uart].Flags &= ~0x80)
#define UartNativeGetTxFrameState(Uart)       ((_Uart[Uart].Flags & 0x70) >> 4)
#define UartNativeIncTxFramePointer(Uart)     (_Uart[Uart].Flags += 0x10)  
#define UartNativeGetTxFramePointer(Uart)     (((_Uart[Uart].Flags & UART_TX_FLAGS) >> 4) - 1)
                                              // Decrement for SOF - it is not present in the frame structure


// Start of frame state
#define UART_NATIVE_SOF_STATE                (int)0

// Header CRC will be checked at this state and data reception will be initiated
#define UART_NATIVE_CHECK_HEADER_CRC_STATE   (int)&(((TUartNativeFrame*)0)->DataSize) + sizeof(((TUartNativeFrame*)0)->DataSize)
                                             // When last byte of Data Size is received
											 
// Data transmission will be initiated
#define UART_NATIVE_START_DATA_STATE         UART_NATIVE_CHECK_HEADER_CRC_STATE + 1
											 
// Data CRC will be checked at this state
#define UART_NATIVE_CHECK_DATA_CRC_STATE     (int)&(((TUartNativeFrame*)0)->DataCrc) + sizeof(((TUartNativeFrame*)0)->DataCrc)
                                             // When last byte of Data CRC is received
											 
											 
static byte UartNativeHeaderCrc(byte Address, word Size) {
   return -(Address + Size);
}

#define ProccessDataCrc(Crc, DataElement)    (Crc += DataElement)
#define FinishDataCrc(Crc)                   (Crc = -Crc)
#define UART_INIT_DATA_CRC                   0

volatile int a;
/************************************************************************/
/* ASCII ISR                                                            */
/************************************************************************/
void UartIsrNative( EUartAddress Uart) {
   byte ch;
   byte *Buffer;

   /************************************************************************/
   /* Timeout                                                              */
   /************************************************************************/
   if(UartIsTimeout(Uart)) { // ka�dou milisekundu
	   UartStartTimeout(Uart);

	   _Uart[Uart].IntercharacterTimer++;
       _Uart[Uart].ReplyTimer++;
	   
       if(_Uart[Uart].IntercharacterTimeout != UART_TIMEOUT_OFF && _Uart[Uart].IntercharacterTimer >= _Uart[Uart].IntercharacterTimeout) {
		 _Uart[Uart].IntercharacterTimer = 0;
		 UartResetRxFlags(Uart);
	   } else if(_Uart[Uart].ReplyTimeout != UART_TIMEOUT_OFF && _Uart[Uart].ReplyTimer >= _Uart[Uart].ReplyTimeout) {
	     /*
		 Blbost - ka�d� nov� p��choz� znak nuluje ��ta� a t�m prodlu�uje reply timeout
		 Reply timeout bude muset b�t ud�lan� s pou�it�m jin�ho ��ta�e
		 Intercharacter timeout pak bude realizov�n jednodu�e p��mo pomoc� UART ��ta�e, bez ��dn�ch dal��ch prom�nn�ch, ne jako te�
		 */
		 _Uart[Uart].RxStatus = UART_RECEIVE_REPLY_TIMEOUT;
	     UartDisableReceive(Uart);
	   }
   }
   /************************************************************************/
   /* Rx                                                                   */
   /************************************************************************/
   else if(UartIsRxReady(Uart)) {
		 _Uart[Uart].IntercharacterTimer = 0;
		 
		 ch = _UartConst[Uart].Phy->rhr;

		 if(UartNativeIsRxDataState(Uart)) { // Receiving frame data
               _Uart[Uart].RxBuffer[_Uart[Uart].RxPointer] = ch;
               _Uart[Uart].RxPointer++;
			   
			   ProccessDataCrc(_Uart[Uart].RxCurrentCrc, ch);
			   
			   if(_Uart[Uart].RxPointer == _Uart[Uart].NativeRxFrame.DataSize) {
				  UartNativeRxFrameState(Uart); // Receive rest of the frame
               }
		 } else { // Receiving frame
			if(UartNativeGetRxFrameState(Uart) == UART_NATIVE_SOF_STATE) {
			      if(ch == UART_NATIVE_SOF) {
				      UartNativeIncRxFramePointer(Uart);
			      }
			      return;
			} else  {         
			   Buffer = (byte *)&(_Uart[Uart].NativeRxFrame);
			   Buffer[UartNativeGetRxFramePointer(Uart)] = ch;

			   if(UartNativeGetRxFrameState(Uart) == UART_NATIVE_CHECK_HEADER_CRC_STATE) {
				  // LSB first
				  _Uart[Uart].NativeRxFrame.DataSize = (_Uart[Uart].NativeRxFrame.DataSize & 0xFF) << 8 | (_Uart[Uart].NativeRxFrame.DataSize & 0xFF00) >> 8;

                  // Check header CRC
			      if(_Uart[Uart].NativeRxFrame.HeaderCrc != UartNativeHeaderCrc(_Uart[Uart].NativeRxFrame.Address, _Uart[Uart].NativeRxFrame.DataSize)) {
				      UartResetRxFlags(Uart);
#ifdef UART_DEBUG_ENABLE
                      _Uart[Uart].NativeBadHeaderCrc++;
#endif
				      return;
			      }
			      
			      if(_Uart[Uart].NativeRxFrame.DataSize == 0) { // Valid zero length frame
				     _Uart[Uart].RxStatus = UART_RECEIVE_FRAME;
				     UartDisableReceive(Uart);
				     return;
			      } else if(_Uart[Uart].NativeRxFrame.DataSize > _Uart[Uart].RxBufferSize) { // Overrun will occur
                     _Uart[Uart].RxStatus = UART_RECEIVE_OVERRUN_ERROR;
                     UartDisableReceive(Uart);
#ifdef UART_DEBUG_ENABLE
                      _Uart[Uart].NativeOverrun++;
#endif
				     return;
			      }
				  
				  // Can receive
				  _Uart[Uart].RxCurrentCrc = UART_INIT_DATA_CRC;
				  _Uart[Uart].RxPointer = 0;
				  UartNativeRxDataState(Uart);
		 	   } else if(UartNativeGetRxFrameState(Uart) == UART_NATIVE_CHECK_DATA_CRC_STATE) {
				  FinishDataCrc(_Uart[Uart].RxCurrentCrc);
				  // LSB first
				  _Uart[Uart].NativeRxFrame.DataCrc = (_Uart[Uart].NativeRxFrame.DataCrc & 0xFF) << 8 | (_Uart[Uart].NativeRxFrame.DataCrc & 0xFF00) >> 8;
                  
			      if(_Uart[Uart].NativeRxFrame.DataCrc != _Uart[Uart].RxCurrentCrc) {
				      UartResetRxFlags(Uart);
#ifdef UART_DEBUG_ENABLE
                      _Uart[Uart].NativeBadDataCrc++;
#endif
					  return;
			      } else { // Check address

#ifdef UART_DEBUG_ENABLE
                      _Uart[Uart].NativeTotalFrames++;
#endif
					  
#ifdef UART_SLAVE_ENABLE	
/*
Mo�n� probl�m - nespr�vn� v�skyt overrun erroru v p��pad�, kdy m� slave kr�tk� buffer a master pos�l�
dlouh� r�mec, kter� nen� ur�en� pro dan� slave za��zen�
*/				  
					  if(_Uart[Uart].Mode == UART_MODE_NATIVE_SLAVE && _Uart[Uart].NativeRxFrame.Address != _Uart[Uart].NativeSlaveAddress) {
				        UartResetRxFlags(Uart);
					    return;
					  }
#endif					  

#ifdef UART_DEBUG_ENABLE
                      _Uart[Uart].NativeAddressedFrames++;
#endif
					  _Uart[Uart].RxStatus = UART_RECEIVE_FRAME;
				      UartDisableReceive(Uart);
			      }
				  
				  return;
			   }
			   
			   UartNativeIncRxFramePointer(Uart);
			}
		}	
   } 
   /************************************************************************/
   /* Tx                                                                   */
   /************************************************************************/
   else if(UartIsTxReady(Uart) && _Uart[Uart].TxStatus == UART_SEND_ACTIVE) {
		 if(UartNativeIsTxDataState(Uart)) { // Transmiting frame data
               if(_Uart[Uart].TxPointer < _Uart[Uart].TxDataSize) { // Transmit data
				  // Can't use _Uart[Uart].NativeTxFrame.DataSize because bytes can be swapped
	              _UartConst[Uart].Phy->thr = _Uart[Uart].TxBuffer[_Uart[Uart].TxPointer];
				  
				  ProccessDataCrc(_Uart[Uart].NativeTxFrame.DataCrc, _Uart[Uart].TxBuffer[_Uart[Uart].TxPointer]);
				  
			      _Uart[Uart].TxPointer++;
	           } else { // finish frame
				   FinishDataCrc(_Uart[Uart].NativeTxFrame.DataCrc);
				   
				   // LSB first
			       _Uart[Uart].NativeTxFrame.DataCrc = (_Uart[Uart].NativeTxFrame.DataCrc & 0xFF) << 8 | (_Uart[Uart].NativeTxFrame.DataCrc & 0xFF00) >> 8;
			   
				   UartNativeTxFrameState(Uart); // Transmit rest of the frame
			   }
		  } else { // Transmitting frame
			 if(UartNativeGetTxFrameState(Uart) == UART_NATIVE_SOF_STATE) { // Start of transmit
			   // Prepare frame data
			   _Uart[Uart].NativeTxFrame.DataSize = _Uart[Uart].TxDataSize;
			   _Uart[Uart].NativeTxFrame.HeaderCrc = UartNativeHeaderCrc(_Uart[Uart].NativeTxFrame.Address, _Uart[Uart].NativeTxFrame.DataSize);
			   
			   // LSB first
			   _Uart[Uart].NativeTxFrame.DataSize = (_Uart[Uart].NativeTxFrame.DataSize & 0xFF) << 8 | (_Uart[Uart].NativeTxFrame.DataSize & 0xFF00) >> 8;

#ifdef UART_SLAVE_ENABLE
			   if(_Uart[Uart].Mode == UART_MODE_NATIVE_SLAVE) {
				  // Slave will always transmit its address
			      _Uart[Uart].NativeTxFrame.Address = _Uart[Uart].NativeSlaveAddress;
			   }				   
#endif
			   // Transmit SOF
			   _UartConst[Uart].Phy->thr = UART_NATIVE_SOF;
			   UartNativeIncTxFramePointer(Uart);
			   return;
			 }

			 // Transmit one byte
			 Buffer = (byte *)&(_Uart[Uart].NativeTxFrame);
			 _UartConst[Uart].Phy->thr = Buffer[UartNativeGetTxFramePointer(Uart)];
		     UartNativeIncTxFramePointer(Uart);
			 
			 if(UartNativeGetTxFrameState(Uart) == UART_NATIVE_START_DATA_STATE) {
				 // Now data will be transmitted
				 // Init CRC
				 _Uart[Uart].NativeTxFrame.DataCrc = UART_INIT_DATA_CRC;
				 
				 // Do we have some data to transmit?
				 if(_Uart[Uart].NativeTxFrame.DataSize != 0) {
				     UartNativeTxDataState(Uart);
				 }
			 } else if(UartNativeGetTxFramePointer(Uart) == sizeof(TUartNativeFrame) + 1 - 1) { // End of transmit, decrement for SOF
				UartDisableTransmit(Uart);
			    
				// Now wait for last two bytes to be transmitted
				UartEnableCheckTxComplete( Uart);
			 }		 
		 }
   } else if(UartIsTxComplete(Uart)) { // All data have been transmitted
      _Uart[Uart].TxStatus = UART_SEND_DONE;
      UartDisableTransmitter(Uart);
      UartDisableTxEmpty(Uart);
   } else if(UartIsParityError(Uart) || UartIsFramingError(Uart) || UartIsOverrunError(Uart)) { // Error = new frame
      UartResetStatus(Uart);
      UartResetRxFlags(Uart);
   } else if(UartIsRxBreak( Uart)) { // Break = continue
      UartResetStatus(Uart);  
   }
}

/************************************************************************/
/* Individual channels ISRs                                             */
/************************************************************************/

#if UART_CHANNELS_COUNT >= 1
__attribute__((__interrupt__)) void Uart0IsrNative(void)
{
   UartIsrNative(UART_UART0);
}
#endif

#if UART_CHANNELS_COUNT >= 2
__attribute__((__interrupt__)) void Uart1IsrNative(void)
{
   UartIsrNative(UART_UART1);
}
#endif

#if UART_CHANNELS_COUNT >= 3
__attribute__((__interrupt__)) void Uart2IsrNative(void)
{
   UartIsrNative(UART_UART2);
}
#endif

#if UART_CHANNELS_COUNT >= 4
__attribute__((__interrupt__)) void Uart3IsrNative(void)
{
   UartIsrNative(UART_UART3);
}

#endif

TYesNo UartNativeRead(EUartAddress Uart, byte Address, void *Buffer, int Size)
// Read
{
   return NO;
}
TYesNo UartNativeWrite(EUartAddress Uart, byte Address, void *Buffer, int Size)
// Write
{
   UartModeSet(Uart, UART_MODE_NATIVE_MASTER);
   
   UartSendAddress(Uart, Address);
   
   UartSend( Uart, Buffer, Size);
   
   while(UartSendStatus( Uart) == UART_SEND_ACTIVE);
   
   return YES;
}