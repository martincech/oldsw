//*****************************************************************************
//
//    UartNative.c       UART native mode
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

TYesNo UartNativeRead(EUartAddress Uart, byte Address, void *Buffer, int Size);
// Read

TYesNo UartNativeWrite(EUartAddress Uart, byte Address, void *Buffer, int Size);
// Write