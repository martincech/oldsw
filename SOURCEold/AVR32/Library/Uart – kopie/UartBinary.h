//*****************************************************************************
//
//    UartBinary.c       UART binary mode
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

int UartBinaryRead(EUartAddress Uart, void *Buffer, int Size);
// Read

int UartBinaryWrite(EUartAddress Uart, void *Buffer, int Size);
// Write