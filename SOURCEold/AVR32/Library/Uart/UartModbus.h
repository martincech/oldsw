#include "Uart.h"

#include "ModbusDef.h"

typedef struct __packed { // !!! PACKED !!!
	byte Address;
	byte FunctionCode;
	byte Data[MODBUS_DATA_SIZE_MAX];
	word Crc;
} TModbusSerialPdu;

#ifdef UART_MODBUS_RTU_ENABLE
   void UartModbusRtuInstall(EUartAddress Uart);
   TModbusPdu *UartModbusRtuGetPdu(EUartAddress Uart);
   word UartModbusRtuGetDataSize(EUartAddress Uart);
   TYesNo UartModbusRtuIsBroadcast(EUartAddress Uart);
   void UartModbusRtuFrameProccessed(EUartAddress Uart);
#endif

void UartModbusResetErrorCount(EUartAddress Uart);