//*****************************************************************************
//
//    UartAscii.c       UART ASCII mode
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

/*
 Nativn� m�d
 P�eh�zel jsem n�kter� polo�ky r�mce aby l�pe vych�zelo jejich zarovn�n� v pam�ti
 a nebylo tak nutn� pou��vat dummy byty ve struktu�e r�mce

   <SOF> <Address> <HeaderCrc> <Size LSB> <Size MSB> <payload DATA> <DataCrc LSB> <DataCrc MSB>
        |<-              Header                   ->|

 Slave mod zahazuje Rx r�mce s adresou odli�nou od nastaven� (UartAssignSlaveAddress())
 Slave m�d automaticky pos�l� svou vlastn� adresu v Tx r�mci (funkce UartSendAddress() nem� v slave m�du ��dn� vliv)

*/

#include "UartCommon.h"


typedef struct __packed { // !!! PACKED !!!
	byte Address;
	byte HeaderCrc;
    word DataSize;
	word DataCrc;
} TUartNativeFrame;

typedef struct __packed { // !!! PACKED !!!
    word AddressedFrames;
	word TotalFrames;
	word BadDataCrc;
	word BadHeaderCrc;
	word Overrun;
} TUartNativeError;




typedef volatile struct __packed { // !!! PACKED !!!
	TUartDetailsCommon  Common;
    TUartNativeFrame    RxFrame;
	word                RxCurrentCrc;  // Used for Rx CRC computation
	TUartNativeFrame    TxFrame;
	
#ifdef UART_DEBUG_ENABLE
    TUartNativeError    Error;
#endif	

#ifdef UART_SLAVE_ENABLE	
	byte                SlaveAddress;
#endif
} TUartDetailsNative;

#define UART_NATIVE_SOF    0x55

// Flags
// Make sure to manipulate only UART_RX_FLAGS part of the Flags variable
#define UartNativeIsRxDataState(Uart)         (Handler->Common.Flags & 0x08)
#define UartNativeRxDataState(Uart)           (Handler->Common.Flags |= 0x08)
#define UartNativeIsRxFrameState(Uart)        !UartNativeIsRxDataState(Uart)
#define UartNativeRxFrameState(Uart)          (Handler->Common.Flags &= ~0x08)
#define UartNativeGetRxFrameState(Uart)       (Handler->Common.Flags & 0x07)
#define UartNativeIncRxFramePointer(Uart)     (Handler->Common.Flags += 0x01)  
#define UartNativeGetRxFramePointer(Uart)     ((Handler->Common.Flags & UART_RX_FLAGS) - 1) 
                                              // Decrement for SOF - it is not present in the frame structure

// Make sure to manipulate only UART_TX_FLAGS part of the Flags variable										
#define UartNativeIsTxDataState(Uart)         (Handler->Common.Flags & 0x80)
#define UartNativeTxDataState(Uart)           (Handler->Common.Flags |= 0x80)
#define UartNativeIsTxFrameState(Uart)        !UartNativeIsTxDataState(Uart)
#define UartNativeTxFrameState(Uart)          (Handler->Common.Flags &= ~0x80)
#define UartNativeGetTxFrameState(Uart)       ((Handler->Common.Flags & 0x70) >> 4)
#define UartNativeIncTxFramePointer(Uart)     (Handler->Common.Flags += 0x10)  
#define UartNativeGetTxFramePointer(Uart)     (((Handler->Common.Flags & UART_TX_FLAGS) >> 4) - 1)
                                              // Decrement for SOF - it is not present in the frame structure


// Start of frame state
#define UART_NATIVE_SOF_STATE                (int)0

// Header CRC will be checked at this state and data reception will be initiated
#define UART_NATIVE_CHECK_HEADER_CRC_STATE   (int)&(((TUartNativeFrame*)0)->DataSize) + sizeof(((TUartNativeFrame*)0)->DataSize)
                                             // When last byte of Data Size is received
											 
// Data transmission will be initiated
#define UART_NATIVE_START_DATA_STATE         UART_NATIVE_CHECK_HEADER_CRC_STATE + 1
											 
// Data CRC will be checked at this state
#define UART_NATIVE_CHECK_DATA_CRC_STATE     (int)&(((TUartNativeFrame*)0)->DataCrc) + sizeof(((TUartNativeFrame*)0)->DataCrc)
                                             // When last byte of Data CRC is received
											 
											 
static byte UartNativeHeaderCrc(byte Address, word Size) {
   return -(Address + Size);
}

#define ProccessDataCrc(Crc, DataElement)    (Crc += DataElement)
#define FinishDataCrc(Crc)                   (Crc = -Crc)
#define UART_INIT_DATA_CRC                   0

volatile TUartDetailsNative *Native0Handler;

volatile int a;
/************************************************************************/
/* Native ISR                                                            */
/************************************************************************/
void UartIsrNative( EUartAddress Uart) {
   byte ch;
   byte *Buffer;
   TUartDetailsNative *Handler = (TUartDetailsNative *) _Uart[Uart];
   
   /************************************************************************/
   /* Timeout                                                              */
   /************************************************************************/
   if(UartIsTimeout(Uart)) { // ka�dou milisekundu
	   UartStartTimeout(Uart);

	   Handler->Common.IntercharacterTimer++;
       Handler->Common.ReplyTimer++;
	   
       if(Handler->Common.IntercharacterTimeout != UART_TIMEOUT_OFF && Handler->Common.IntercharacterTimer >= Handler->Common.IntercharacterTimeout) {
		 Handler->Common.IntercharacterTimer = 0;
		 UartResetRxFlags(Uart);
	   } else if(Handler->Common.ReplyTimeout != UART_TIMEOUT_OFF && Handler->Common.ReplyTimer >= Handler->Common.ReplyTimeout) {
	     /*
		 Blbost - ka�d� nov� p��choz� znak nuluje ��ta� a t�m prodlu�uje reply timeout
		 Reply timeout bude muset b�t ud�lan� s pou�it�m jin�ho ��ta�e
		 Intercharacter timeout pak bude realizov�n jednodu�e p��mo pomoc� UART ��ta�e, bez ��dn�ch dal��ch prom�nn�ch, ne jako te�
		 */
		 Handler->Common.RxStatus = UART_RECEIVE_REPLY_TIMEOUT;
	     UartDisableReceive(Uart);
	   }
   }
   /************************************************************************/
   /* Rx                                                                   */
   /************************************************************************/
   else if(UartIsRxReady(Uart)) {
		 Handler->Common.IntercharacterTimer = 0;
		 
		 ch = _UartConst[Uart].Phy->rhr;

		 if(UartNativeIsRxDataState(Uart)) { // Receiving frame data
               Handler->Common.RxBuffer[Handler->Common.RxPointer] = ch;
               Handler->Common.RxPointer++;
			   
			   ProccessDataCrc(Handler->RxCurrentCrc, ch);
			   
			   if(Handler->Common.RxPointer == Handler->RxFrame.DataSize) {
				  UartNativeRxFrameState(Uart); // Receive rest of the frame
               }
		 } else { // Receiving frame
			if(UartNativeGetRxFrameState(Uart) == UART_NATIVE_SOF_STATE) {
			      if(ch == UART_NATIVE_SOF) {
				      UartNativeIncRxFramePointer(Uart);
			      }
			      return;
			} else  {         
			   Buffer = (byte *)&(Handler->RxFrame);
			   Buffer[UartNativeGetRxFramePointer(Uart)] = ch;

			   if(UartNativeGetRxFrameState(Uart) == UART_NATIVE_CHECK_HEADER_CRC_STATE) {
				  // LSB first
				  Handler->RxFrame.DataSize = (Handler->RxFrame.DataSize & 0xFF) << 8 | (Handler->RxFrame.DataSize & 0xFF00) >> 8;

                  // Check header CRC
			      if(Handler->RxFrame.HeaderCrc != UartNativeHeaderCrc(Handler->RxFrame.Address, Handler->RxFrame.DataSize)) {
				      UartResetRxFlags(Uart);
#ifdef UART_DEBUG_ENABLE
                      Handler->Error.BadHeaderCrc++;
#endif
				      return;
			      }
			      
			      if(Handler->RxFrame.DataSize == 0) { // Valid zero length frame
				     Handler->Common.RxStatus = UART_RECEIVE_FRAME;
				     UartDisableReceive(Uart);
				     return;
			      } else if(Handler->RxFrame.DataSize > Handler->Common.RxBufferSize) { // Overrun is going occur
                     Handler->Common.RxStatus = UART_RECEIVE_OVERRUN_ERROR;
                     UartDisableReceive(Uart);
#ifdef UART_DEBUG_ENABLE
                      Handler->Error.Overrun++;
#endif
				     return;
			      }
				  
				  // Can receive
				  Handler->RxCurrentCrc = UART_INIT_DATA_CRC;
				  Handler->Common.RxPointer = 0;
				  UartNativeRxDataState(Uart);
		 	   } else if(UartNativeGetRxFrameState(Uart) == UART_NATIVE_CHECK_DATA_CRC_STATE) {
				  FinishDataCrc(Handler->RxCurrentCrc);
				  // LSB first
				  Handler->RxFrame.DataCrc = (Handler->RxFrame.DataCrc & 0xFF) << 8 | (Handler->RxFrame.DataCrc & 0xFF00) >> 8;
                  
			      if(Handler->RxFrame.DataCrc != Handler->RxCurrentCrc) {
				      UartResetRxFlags(Uart);
#ifdef UART_DEBUG_ENABLE
                      Handler->Error.BadDataCrc++;
#endif
					  return;
			      } else { // Check address

#ifdef UART_DEBUG_ENABLE
                      Handler->Error.TotalFrames++;
#endif
					  
#ifdef UART_SLAVE_ENABLE	
/*
Mo�n� probl�m - nespr�vn� v�skyt overrun erroru v p��pad�, kdy m� slave kr�tk� buffer a master pos�l�
dlouh� r�mec, kter� nen� ur�en� pro dan� slave za��zen�
*/				  
					  if(Handler->Common.Mode == UART_MODE_NATIVE_SLAVE && Handler->RxFrame.Address != Handler->SlaveAddress) {
				        UartResetRxFlags(Uart);
					    return;
					  }
#endif					  

#ifdef UART_DEBUG_ENABLE
                      Handler->Error.AddressedFrames++;
#endif
					  Handler->Common.RxStatus = UART_RECEIVE_FRAME;
				      UartDisableReceive(Uart);
			      }
				  
				  return;
			   }
			   
			   UartNativeIncRxFramePointer(Uart);
			}
		}	
   } 
   /************************************************************************/
   /* Tx                                                                   */
   /************************************************************************/
   else if(UartIsTxReady(Uart) && Handler->Common.TxStatus == UART_SEND_ACTIVE) {
		 if(UartNativeIsTxDataState(Uart)) { // Transmiting frame data
               if(Handler->Common.TxPointer < Handler->Common.TxDataSize) { // Transmit data
				  // Can't use _Uart[Uart].NativeTxFrame.DataSize because bytes can be swapped
	              _UartConst[Uart].Phy->thr = Handler->Common.TxBuffer[Handler->Common.TxPointer];
				  
				  ProccessDataCrc(Handler->TxFrame.DataCrc, Handler->Common.TxBuffer[Handler->Common.TxPointer]);
				  
			      Handler->Common.TxPointer++;
	           } else { // finish frame
				   FinishDataCrc(Handler->TxFrame.DataCrc);
				   
				   // LSB first
			       Handler->TxFrame.DataCrc = (Handler->TxFrame.DataCrc & 0xFF) << 8 | (Handler->TxFrame.DataCrc & 0xFF00) >> 8;
			   
				   UartNativeTxFrameState(Uart); // Transmit rest of the frame
			   }
		  } else { // Transmitting frame
			 if(UartNativeGetTxFrameState(Uart) == UART_NATIVE_SOF_STATE) { // Start of transmit
			   // Prepare frame data
			   Handler->TxFrame.DataSize = Handler->Common.TxDataSize;
			   Handler->TxFrame.HeaderCrc = UartNativeHeaderCrc(Handler->TxFrame.Address, Handler->TxFrame.DataSize);
			   
			   // LSB first
			   Handler->TxFrame.DataSize = (Handler->TxFrame.DataSize & 0xFF) << 8 | (Handler->TxFrame.DataSize & 0xFF00) >> 8;

#ifdef UART_SLAVE_ENABLE
			   if(Handler->Common.Mode == UART_MODE_NATIVE_SLAVE) {
				  // Slave will always transmit its address
			      Handler->TxFrame.Address = Handler->SlaveAddress;
			   }				   
#endif
			   // Transmit SOF
			   _UartConst[Uart].Phy->thr = UART_NATIVE_SOF;
			   UartNativeIncTxFramePointer(Uart);
			   return;
			 }

			 // Transmit one byte
			 Buffer = (byte *)&(Handler->TxFrame);
			 _UartConst[Uart].Phy->thr = Buffer[UartNativeGetTxFramePointer(Uart)];
		     UartNativeIncTxFramePointer(Uart);
			 
			 if(UartNativeGetTxFrameState(Uart) == UART_NATIVE_START_DATA_STATE) {
				 // Now data will be transmitted
				 // Init CRC
				 Handler->TxFrame.DataCrc = UART_INIT_DATA_CRC;
				 
				 // Do we have some data to transmit?
				 if(Handler->TxFrame.DataSize != 0) {
				     UartNativeTxDataState(Uart);
				 }
			 } else if(UartNativeGetTxFramePointer(Uart) == sizeof(TUartNativeFrame) + 1 - 1) { // End of transmit, decrement for SOF
				UartDisableTransmit(Uart);
			    
				// Now wait for last two bytes to be transmitted
				UartEnableCheckTxComplete( Uart);
			 }		 
		 }
   } else if(UartIsTxComplete(Uart)) { // All data have been transmitted
      Handler->Common.TxStatus = UART_SEND_DONE;
      UartDisableTransmitter(Uart);
      UartDisableTxEmpty(Uart);
   } else if(UartIsParityError(Uart) || UartIsFramingError(Uart) || UartIsOverrunError(Uart)) { // Error = new frame
      UartResetStatus(Uart);
      UartResetRxFlags(Uart);
   } else if(UartIsRxBreak( Uart)) { // Break = continue
      UartResetStatus(Uart);  
   }
}

/************************************************************************/
/* Individual channels ISRs                                             */
/************************************************************************/

#if UART_CHANNELS_COUNT >= 1
__attribute__((__interrupt__)) void Uart0IsrNative(void)
{
   UartIsrNative(UART_UART0);
}
#endif

#if UART_CHANNELS_COUNT >= 2
__attribute__((__interrupt__)) void Uart1IsrNative(void)
{
   UartIsrNative(UART_UART1);
}
#endif

#if UART_CHANNELS_COUNT >= 3
__attribute__((__interrupt__)) void Uart2IsrNative(void)
{
   UartIsrNative(UART_UART2);
}
#endif

#if UART_CHANNELS_COUNT >= 4
__attribute__((__interrupt__)) void Uart3IsrNative(void)
{
   UartIsrNative(UART_UART3);
}

#endif

TYesNo UartNativeRead(EUartAddress Uart, byte Address, void *Buffer, int Size)
// Read
{
   return NO;
}

volatile EUartSendStatus STAT;

TYesNo UartNativeWrite(EUartAddress Uart, byte Address, void *Buffer, int Size)
// Write
{
   Native0Handler = (TUartDetailsNative *) _Uart[0];
   UartModeSet(Uart, UART_MODE_NATIVE_MASTER);
   
   UartSendAddress(Uart, Address);
   
   UartSend( Uart, Buffer, Size);

   //while((Native0Handler->Common.TxStatus & UART_TX_STATUS_VALID_PART) == UART_SEND_ACTIVE);

   while(UartSendStatus( Uart) == UART_SEND_ACTIVE);
   
   Native0Handler = (TUartDetailsNative *) _Uart[0];
   
   return YES;
}