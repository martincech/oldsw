//*****************************************************************************
//
//    UartModbusRtu.c  UART Modbus RTU mode
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

/*
   Modbus

*/

#include "UartCommon.h"
#include "UartModbus.h"

#ifdef MODBUS_DIAGNOSTICS_ENABLED
   #define  ModbusBusCommunicationError()    (Handler->Exception.ReturnBusCommunicationErrorCount++)
   #define  ModbusBusMessageCount()          (Handler->Exception.ReturnBusMessageCount++)
   #define  ModbusSlaveMessageCount()	     (Handler->Exception.ReturnSlaveMessageCount++) 
   #define  ModbusSlaveNoResponseCount()     (Handler->Exception.ReturnSlaveNoResponseCount++)
   #define  ModbusBusCharacterOverrunCount() (Handler->Exception.ReturnBusCharacterOverrunCount++)
#endif

typedef enum {
   UART_MODBUS_RTU_STATE_RX,
   UART_MODBUS_RTU_STATE_RX_CONTROL,
   UART_MODBUS_RTU_STATE_RX_PROCESSING,
   UART_MODBUS_RTU_STATE_INIT,
   UART_MODBUS_RTU_STATE_IDLE,
   UART_MODBUS_RTU_STATE_TX
} EUartModbusRtuState;


typedef volatile struct __packed { // !!! PACKED !!!
	TUartDetailsCommon     Common; // ��ste�n� na nic

    TModbusException       Exception;
    TModbusSerialPdu       RxFrame;
	
	word                   RxPointer;
	byte                   Timer;
	EUartModbusRtuState    State;
	byte                   RxSize;
	TYesNo                 CharacterError;
	TYesNo                 FrameFlag;
	byte                SlaveAddress;
} TUartDetailsModbusRtu;




static byte TblCrcHi[]  = {
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,
0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,
0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x01,0xC0,
0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,
0xC0,0x80,0x41,0x00,0xC1,0x81,0x40,0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,
0x00,0xC1,0x81,0x40,0x01,0xC0,0x80,0x41,0x01,0xC0,0x80,0x41,0x00,0xC1,0x81,
0x40
} ;

static byte TblCrcLo[] = {
0x00,0xC0,0xC1,0x01,0xC3,0x03,0x02,0xC2,0xC6,0x06,0x07,0xC7,0x05,0xC5,0xC4,
0x04,0xCC,0x0C,0x0D,0xCD,0x0F,0xCF,0xCE,0x0E,0x0A,0xCA,0xCB,0x0B,0xC9,0x09,
0x08,0xC8,0xD8,0x18,0x19,0xD9,0x1B,0xDB,0xDA,0x1A,0x1E,0xDE,0xDF,0x1F,0xDD,
0x1D,0x1C,0xDC,0x14,0xD4,0xD5,0x15,0xD7,0x17,0x16,0xD6,0xD2,0x12,0x13,0xD3,
0x11,0xD1,0xD0,0x10,0xF0,0x30,0x31,0xF1,0x33,0xF3,0xF2,0x32,0x36,0xF6,0xF7,
0x37,0xF5,0x35,0x34,0xF4,0x3C,0xFC,0xFD,0x3D,0xFF,0x3F,0x3E,0xFE,0xFA,0x3A,
0x3B,0xFB,0x39,0xF9,0xF8,0x38,0x28,0xE8,0xE9,0x29,0xEB,0x2B,0x2A,0xEA,0xEE,
0x2E,0x2F,0xEF,0x2D,0xED,0xEC,0x2C,0xE4,0x24,0x25,0xE5,0x27,0xE7,0xE6,0x26,
0x22,0xE2,0xE3,0x23,0xE1,0x21,0x20,0xE0,0xA0,0x60,0x61,0xA1,0x63,0xA3,0xA2,
0x62,0x66,0xA6,0xA7,0x67,0xA5,0x65,0x64,0xA4,0x6C,0xAC,0xAD,0x6D,0xAF,0x6F,
0x6E,0xAE,0xAA,0x6A,0x6B,0xAB,0x69,0xA9,0xA8,0x68,0x78,0xB8,0xB9,0x79,0xBB,
0x7B,0x7A,0xBA,0xBE,0x7E,0x7F,0xBF,0x7D,0xBD,0xBC,0x7C,0xB4,0x74,0x75,0xB5,
0x77,0xB7,0xB6,0x76,0x72,0xB2,0xB3,0x73,0xB1,0x71,0x70,0xB0,0x50,0x90,0x91,
0x51,0x93,0x53,0x52,0x92,0x96,0x56,0x57,0x97,0x55,0x95,0x94,0x54,0x9C,0x5C,
0x5D,0x9D,0x5F,0x9F,0x9E,0x5E,0x5A,0x9A,0x9B,0x5B,0x99,0x59,0x58,0x98,0x88,
0x48,0x49,0x89,0x4B,0x8B,0x8A,0x4A,0x4E,0x8E,0x8F,0x4F,0x8D,0x4D,0x4C,0x8C,
0x44,0x84,0x85,0x45,0x87,0x47,0x46,0x86,0x82,0x42,0x43,0x83,0x41,0x81,0x80,
0x40
};

static word CalcCrc( byte *Buffer, word Size)
// Vypocita zabezpeceni z <Size> bytu
{
byte CrcHi = 0xFF;
byte CrcLo = 0xFF;
byte Index;
word i;

   for( i = 0; i < Size; i++){
      Index = CrcLo ^ Buffer[ i];
      CrcLo = CrcHi ^ TblCrcHi[ Index];
      CrcHi = TblCrcLo[ Index];
   }
   return( (word)CrcHi << 8 | CrcLo);
} // CalcCrc




/*
t1.5 a t3.5 timery
t1.5 �asuje intern� timer uartu
t3.5 je p�ibli�n� realizov�n jako n�sobek t1.5
*/
#define     UART_MODBUS_RTU_T15  (word)(MODBUS_RTU_CHARACTER_LENGTH * 1.5 + 1) // pro velk� baudy doporu�eno pevn� 750us
#define     UART_MODBUS_RTU_T35  3

/************************************************************************/
/* Modbus RTU ISR                                                       */
/************************************************************************/
void UartIsrModbusRtu( EUartAddress Uart) {
   byte ch;
   byte *Buffer;
   TUartDetailsModbusRtu *Handler = (TUartDetailsModbusRtu *) _Uart[Uart];
   
   /************************************************************************/
   /* Timeout                                                              */
   /************************************************************************/
   if(UartIsTimeout(Uart)) { // ka�dou milisekundu
      UartStartTimeout(Uart);
	  Handler->Timer--;
	  
	  if(Handler->State == UART_MODBUS_RTU_STATE_RX) { // Do control of the frame once t1.5 expired
		 Handler->State = UART_MODBUS_RTU_STATE_RX_CONTROL;
         
		 Buffer = (byte *) &Handler->RxFrame;
		 
		 // Rearrange CRC
		 Handler->RxFrame.Crc = (word) ((Buffer[Handler->RxPointer - 1] << 8) | Buffer[Handler->RxPointer - 2]);

         // Frame control
		 if(Handler->CharacterError == YES || 
		    Handler->RxPointer < 3         ||
			CalcCrc((byte *) &Handler->RxFrame, Handler->RxPointer - 2) != Handler->RxFrame.Crc // nejde CRC
			) {
		     ModbusBusCommunicationError();
			 Handler->FrameFlag = NO;
			 return;
		 }

         ModbusBusMessageCount();
		 
		 // Address control
		 if(Handler->RxFrame.Address != 0 || Handler->RxFrame.Address != Handler->SlaveAddress) {
		    Handler->FrameFlag = NO;
			return;
		 }
		 
		 ModbusSlaveMessageCount();
		 
		 // Broadcast control
		 if(Handler->RxFrame.Address == 0) {
			ModbusSlaveNoResponseCount();
		 }
		 
		 Handler->FrameFlag = YES;
	  }
	  
	  if(Handler->Timer == 0) {
		  _UartConst[Uart].Phy->rtor = 0;
          UartStopTimeout(Uart); // the last timeout is still timing
		  
		  switch(Handler->State) {
			 case UART_MODBUS_RTU_STATE_RX_CONTROL:
			   if(Handler->FrameFlag == YES) { // Frame received
				   Handler->State = UART_MODBUS_RTU_STATE_RX_PROCESSING;
				   Handler->Common.RxStatus = UART_RECEIVE_FRAME;
				   return;
			   }
			   break;

			 case UART_MODBUS_RTU_STATE_INIT:
			   // t3.5 expired - go from idle to init
			   Handler->State = UART_MODBUS_RTU_STATE_IDLE;
			   break;
			   
			 default: // should not reach this
			   return;
		  }
	  }
   }
   /************************************************************************/
   /* Rx                                                                   */
   /************************************************************************/
   else if(UartIsRxReady(Uart)) {
      ch = _UartConst[Uart].Phy->rhr;
	  
	  switch(Handler->State) {
		 case UART_MODBUS_RTU_STATE_RX:   // Store character
		    goto store;
			
    	 case UART_MODBUS_RTU_STATE_IDLE: // Init reception and store character
		    _UartConst[Uart].Phy->rtor = UART_MODBUS_RTU_T1.5; // start t1.5 and t3.5 timers
			UartStartTimeout(Uart);
            Handler->State = UART_MODBUS_RTU_STATE_RX;
			Handler->CharacterError = NO;
			Handler->RxPointer = 0;
			goto store;
			
	     case UART_MODBUS_RTU_STATE_INIT: // Modbus has been started during bus activity, wait
            Handler->Timer = UART_MODBUS_RTU_T35;  // restart t3.5 timer
			break;

		 case UART_MODBUS_RTU_STATE_RX_PROCESSING:
		    ModbusBusCharacterOverrunCount();
			break;
			
		 default: // kdy� p�ijde znak p�i UART_MODBUS_RTU_STATE_RX_CONTROL (tj. v intervalu t3.5 kdy by nic p�ij�t nem�lo), po��tat jej do overrun???
		    break;
	  }
	  
	  return;
store:
      // restart t1.5 timer - it has been already restarted by the reception of the character
      Handler->Timer = UART_MODBUS_RTU_T35; // restart t3.5 timer
	  
	  if(Handler->RxPointer >= sizeof(TModbusSerialPdu)) { // overrun ?
		  if(sizeof(TModbusSerialPdu) < MODBUS_FRAME_SIZE_MAX) // overrun
		    ModbusBusCharacterOverrunCount();
	      }		
		  // else - not overrun - frame is longer than max allowed size  
		  return;
	  }
	  
	  Buffer = (byte *) &Handler->RxFrame;
	  
      Buffer[Handler->RxPointer++] = ch;
   } 
   /************************************************************************/
   /* Tx                                                                   */
   /************************************************************************/
   else if(UartIsTxReady(Uart) && Handler->Common.TxStatus == UART_SEND_ACTIVE) {

   } else if(UartIsTxComplete(Uart)) { // All data have been transmitted
      Handler->Common.TxStatus = UART_SEND_DONE;
      UartDisableTransmitter(Uart);
      UartDisableTxEmpty(Uart);
   } else if(UartIsParityError(Uart)) {
      Handler->CharacterError = YES;
	  UartResetStatus(Uart);
   } else if(UartIsFramingError(Uart)) {
      Handler->CharacterError = YES;
	  UartResetStatus(Uart);
   } else if(UartIsOverrunError(Uart)) {
	  Handler->Exception.ReturnBusCharacterOverrunCount++; // asi by cht�lo p�eru�it p��jem r�mce...
	  UartResetStatus(Uart);
   } else if(UartIsRxBreak( Uart)) { // Break = continue
      UartResetStatus(Uart);  
   }
}

/************************************************************************/
/* Individual channels ISRs                                             */
/************************************************************************/

#if UART_CHANNELS_COUNT >= 1
__attribute__((__interrupt__)) void Uart0IsrModbusRtu(void)
{
   UartIsrModbusRtu(UART_UART0);
}
#endif

#if UART_CHANNELS_COUNT >= 2
__attribute__((__interrupt__)) void Uart1IsrModbusRtu(void)
{
   UartIsrModbusRtu(UART_UART1);
}
#endif

#if UART_CHANNELS_COUNT >= 3
__attribute__((__interrupt__)) void Uart2IsrModbusRtu(void)
{
   UartIsrModbusRtu(UART_UART2);
}
#endif

#if UART_CHANNELS_COUNT >= 4
__attribute__((__interrupt__)) void Uart3IsrModbusRtu(void)
{
   UartIsrModbusRtu(UART_UART3);
}
#endif


void UartModbusRtuInstall(EUartAddress Uart) {
	TUartDetailsModbusRtu *Handler = (TUartDetailsModbusRtu *) _Uart[Uart];
	
	UartStop(Uart);

	// Install timeout
	_UartConst[Uart].Phy->rtor = UART_MODBUS_RTU_T15;
    Handler->Timer = UART_MODBUS_RTU_T35;
	
	Handler->State = UART_MODBUS_RTU_STATE_INIT;

#ifdef MODBUS_DIAGNOSTICS_ENABLED
	// Reset error counters 
    Handler->Exception.ReturnBusMessageCount = 0;
	Handler->Exception.ReturnBusCommunicationErrorCount = 0;
	Handler->Exception.ReturnSlaveExceptionErrorCount = 0;
	Handler->Exception.ReturnSlaveMessageCount = 0;
	Handler->Exception.ReturnSlaveNoResponseCount = 0;
	Handler->Exception.ReturnSlaveNakCount = 0;
	Handler->Exception.ReturnSlaveBusyCount = 0;
	Handler->Exception.ReturnBusCharacterOverrunCount = 0;
#endif
}

TModbusPdu *UartModbusRtuGetPdu(EUartAddress Uart) {
   TUartDetailsModbusRtu *Handler = (TUartDetailsModbusRtu *) _Uart[Uart];
	
   return (TModbusPdu *)&Handler->RxFrame.FunctionCode;
}

word UartModbusRtuGetDataSize(EUartAddress Uart) {
   TUartDetailsModbusRtu *Handler = (TUartDetailsModbusRtu *) _Uart[Uart];
	
   return Handler->RxPointer - (sizeof(TModbusSerialPdu) - MODBUS_DATA_SIZE_MAX);
}

TYesNo UartModbusRtuIsBroadcast(EUartAddress Uart) {
   TUartDetailsModbusRtu *Handler = (TUartDetailsModbusRtu *) _Uart[Uart];
	
   return (Handler->RxFrame.Address == 0);
}

void UartModbusRtuFrameProccessed(EUartAddress Uart) {
	TUartDetailsModbusRtu *Handler = (TUartDetailsModbusRtu *) _Uart[Uart];
	
	Handler->State = UART_MODBUS_RTU_STATE_IDLE;
	Handler->Common.RxStatus = UART_RECEIVE_ACTIVE;
}