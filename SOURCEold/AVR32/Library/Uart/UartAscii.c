//*****************************************************************************
//
//    UartAscii.c       UART ASCII mode
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

/*
   P��jem r�mc� ohrani�en�ch <SOF> a <EOF>
   P�i intercharacter timeout nebo erroru (parity, overrun, framing) za��n� nov� r�mec, star� (rozpracovan� je zahozen)
   P�i �sp�n�m dokon�en� p��jmu r�mce se p��jem ukon�uje, u�ivatel jej mus� obnovit vol�n�m UartReceive()
   Je mo�n� pos�lat/p�ij�mat SOF a EOF znaky v datech r�mce - zdvojov�n�
*/

#include "UartCommon.h"

typedef volatile struct __packed { // !!! PACKED !!!
	TUartDetailsCommon  Common;
	byte                Sof;
	byte                Eof;
} TUartDetailsAscii;

// Flags
#define UART_ASCII_SOF_RECEIVED   (0x01 & UART_RX_FLAGS)
#define UART_ASCII_LAST_CHAR_SOF  (0x02 & UART_RX_FLAGS)
#define UART_ASCII_LAST_CHAR_EOF  (0x04 & UART_RX_FLAGS)
#define UART_ASCII_SOF_SENT       (0x10 & UART_TX_FLAGS)
#define UART_ASCII_EOF_SENT       (0x20 & UART_TX_FLAGS)
#define UART_ASCII_DOUBLE_SENT    (0x40 & UART_TX_FLAGS)

#define UartAsciiIsSofSent( Uart)         (Handler->Common.Flags &   UART_ASCII_SOF_SENT)
#define UartAsciiSofSent( Uart)           (Handler->Common.Flags |=  UART_ASCII_SOF_SENT)
#define UartAsciiIsEofSent( Uart)         (Handler->Common.Flags &   UART_ASCII_EOF_SENT)
#define UartAsciiEofSent( Uart)           (Handler->Common.Flags |=  UART_ASCII_EOF_SENT)
#define UartAsciiIsDoubleSent( Uart)      (Handler->Common.Flags &   UART_ASCII_DOUBLE_SENT)
#define UartAsciiDoubleSent( Uart)        (Handler->Common.Flags |=  UART_ASCII_DOUBLE_SENT)
#define UartAsciiDoubleNotSent( Uart)     (Handler->Common.Flags &= ~UART_ASCII_DOUBLE_SENT)

#define UartAsciiIsSofReceived( Uart)     (Handler->Common.Flags &   UART_ASCII_SOF_RECEIVED)
#define UartAsciiSofReceived( Uart)       (Handler->Common.Flags |=  UART_ASCII_SOF_RECEIVED)
#define UartAsciiIsLastCharSof( Uart)     (Handler->Common.Flags &   UART_ASCII_LAST_CHAR_SOF)
#define UartAsciiLastCharSof( Uart)       (Handler->Common.Flags |=  UART_ASCII_LAST_CHAR_SOF)
#define UartAsciiLastCharNotSof( Uart)    (Handler->Common.Flags &= ~UART_ASCII_LAST_CHAR_SOF)
#define UartAsciiIsLastCharEof( Uart)     (Handler->Common.Flags &   UART_ASCII_LAST_CHAR_EOF)
#define UartAsciiLastCharEof( Uart)       (Handler->Common.Flags |=  UART_ASCII_LAST_CHAR_EOF)
#define UartAsciiLastCharNotEof( Uart)    (Handler->Common.Flags &= ~UART_ASCII_LAST_CHAR_EOF)

/************************************************************************/
/* ASCII ISR                                                            */
/************************************************************************/
void UartIsrAscii( EUartAddress Uart) {
   byte ch;
   TUartDetailsAscii *Handler = (TUartDetailsAscii *) _Uart[Uart];
   
   /************************************************************************/
   /* Timeout                                                              */
   /************************************************************************/
   if(UartIsTimeout(Uart)) {
	   UartStartTimeout(Uart);
	   
	   Handler->Common.IntercharacterTimer++;
       Handler->Common.ReplyTimer++;
	   
       if(Handler->Common.IntercharacterTimeout != UART_TIMEOUT_OFF && Handler->Common.IntercharacterTimer >= Handler->Common.IntercharacterTimeout) {
		 if(UartAsciiIsLastCharEof(Uart)) { // last char was EOF
	        Handler->Common.RxStatus = UART_RECEIVE_FRAME;
	        UartDisableReceive(Uart);
		 } else { // restart
			Handler->Common.IntercharacterTimer = 0;
		    UartResetRxFlags(Uart);
		 }
	   } else if(Handler->Common.ReplyTimeout != UART_TIMEOUT_OFF && Handler->Common.ReplyTimer >= Handler->Common.ReplyTimeout) {
	        Handler->Common.RxStatus = UART_RECEIVE_REPLY_TIMEOUT;
	        UartDisableReceive(Uart);
	   }
   }
   /************************************************************************/
   /* Rx                                                                   */
   /************************************************************************/
   else if(UartIsRxReady(Uart)) {
		 Handler->Common.IntercharacterTimer = 0;
		 
		 ch = _UartConst[Uart].Phy->rhr;

         // Handle SOF
		    if(ch == Handler->Sof) {
			   if(!UartAsciiIsLastCharSof(Uart)) {    // prev CHAR + now SOF = check if following char will also be SOF
				   UartAsciiLastCharSof(Uart);
				   return;
			   } else {                               // prev SOF + now SOF - two successive SOFs
			      if(UartAsciiIsSofReceived(Uart)) {  // only if real SOF has been already received
				     UartAsciiLastCharNotSof(Uart);
					 goto store;
			      }
			   }
			} else {
				if(UartAsciiIsLastCharSof(Uart)) {    // prev SOF + now CHAR = start of frame (real SOF)
					UartAsciiSofReceived(Uart);
					Handler->Common.RxPointer = 0;
					UartAsciiLastCharNotSof(Uart);
			}
			
			
			if(!UartAsciiIsSofReceived(Uart)) {
				return;
			}	
			
			// Only if real SOF has been received
						
			// Handle EOF			
			   if(ch == Handler->Eof) {
			      if(!UartAsciiIsLastCharEof(Uart)) {       // prev CHAR + now EOF = check if following char will also be EOF
				      UartAsciiLastCharEof(Uart);
				      UartStartTimeout(Uart);               // restart timer
				      return;
			      } else {                                  // prev EOF + now EOF - two successive EOFs
				      UartAsciiLastCharNotEof(Uart);
					  goto store;
			      }
			   } else {
				   if(UartAsciiIsLastCharEof(Uart) && UartAsciiIsSofReceived(Uart)) {       // prev EOF + now CHAR = end of frame (real EOF) - only if real SOF has been received
					   Handler->Common.RxStatus = UART_RECEIVE_FRAME;
					   UartDisableReceive(Uart);
					   return;
				   }
			   }

store:			
			   // Handle character
               if(Handler->Common.RxPointer < Handler->Common.RxBufferSize) {      // store character
                  Handler->Common.RxBuffer[Handler->Common.RxPointer] = ch;
                  Handler->Common.RxPointer++;		 
               } else {                                              // overrun
                  Handler->Common.RxStatus = UART_RECEIVE_OVERRUN_ERROR;
                  UartDisableReceive(Uart);
               }
	  }	  
   } 
   /************************************************************************/
   /* Tx                                                                   */
   /************************************************************************/
   else if(UartIsTxReady(Uart) && Handler->Common.TxStatus == UART_SEND_ACTIVE) {
	  if(!UartAsciiIsSofSent(Uart)) { // Send SOF
		  _UartConst[Uart].Phy->thr = Handler->Sof;
		  UartAsciiSofSent(Uart);
		  return;
	  }
	  
	  if(Handler->Common.TxPointer < Handler->Common.TxDataSize) {
	        _UartConst[Uart].Phy->thr = Handler->Common.TxBuffer[Handler->Common.TxPointer];
		    
			// double SOF and EOF characters
			if(Handler->Common.TxBuffer[Handler->Common.TxPointer] == Handler->Sof || Handler->Common.TxBuffer[Handler->Common.TxPointer] == Handler->Eof) {
			   if(!UartAsciiIsDoubleSent(Uart)) {
				   UartAsciiDoubleSent(Uart);
				   return; // don't increment, this char (SOF, EOF) will be doubled
			   } else {
				   UartAsciiDoubleNotSent(Uart);
			   }
			}

			Handler->Common.TxPointer++;
	     }	else {
	        if(!UartAsciiIsEofSent(Uart)) { // Send EOF
		        _UartConst[Uart].Phy->thr = Handler->Eof;
		        UartAsciiEofSent(Uart);
		        return;
	        }
	  
		    UartDisableTransmit(Uart);
			UartEnableCheckTxComplete( Uart);
	  }
   } else if(UartIsTxComplete(Uart)) { // All data have been transmitted
      Handler->Common.TxStatus = UART_SEND_DONE;
      UartDisableTransmitter(Uart);
      UartDisableTxEmpty(Uart);
   } else if(UartIsParityError(Uart) || UartIsFramingError(Uart) || UartIsOverrunError(Uart)) { // Error = new frame
      UartResetStatus(Uart);
      UartResetRxFlags(Uart);
   } else if(UartIsRxBreak( Uart)) { // Break = continue
      UartResetStatus(Uart);  
   }
}

/************************************************************************/
/* Individual channels ISRs                                             */
/************************************************************************/

#if UART_CHANNELS_COUNT >= 1
__attribute__((__interrupt__)) void Uart0IsrAscii(void)
{
   UartIsrAscii(UART_UART0);
}
#endif

#if UART_CHANNELS_COUNT >= 2
__attribute__((__interrupt__)) void Uart1IsrAscii(void)
{
   UartIsrAscii(UART_UART1);
}
#endif

#if UART_CHANNELS_COUNT >= 3
__attribute__((__interrupt__)) void Uart2IsrAscii(void)
{
   UartIsrAscii(UART_UART2);
}
#endif

#if UART_CHANNELS_COUNT >= 4
__attribute__((__interrupt__)) void Uart3IsrAscii(void)
{
   UartIsrAscii(UART_UART3);
}

#endif
