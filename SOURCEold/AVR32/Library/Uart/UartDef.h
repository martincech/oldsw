//*****************************************************************************
//
//    UartDef.h     RS232/485 parameters
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __UartDef_H__
   #define __UartDef_H__


// Format :
typedef enum {
   UART_8BIT,                // 8bit without parity
   UART_8BIT_EVEN,           // 8bit even parity
   UART_8BIT_ODD,            // 8bit odd parity
   UART_8BIT_MARK,           // 8bit constant parity - 2 stopbits
   UART_8BIT_SPACE,          // 8bit constant 0 parity
   UART_7BIT,                // 7bit without parity
   UART_7BIT_EVEN,           // 7bit even parity
   UART_7BIT_ODD,            // 7bit odd parity
   UART_7BIT_MARK,           // 7bit const parity - 2 stopbits
   UART_7BIT_SPACE,          // 7bit constant 0 parity
   _UART_FORMAT_COUNT
} EUartFormat;

#define UART_BAUD_MIN  1200            // slightes useful value
#define UART_BAUD_MAX  115200          // probably PC limited value

#endif
