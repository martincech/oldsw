#if UART_CHANNELS_COUNT > 11
   #error "Unsupported channels count"
#endif



TUartConstDetails _UartConst[] = {
#if UART_CHANNELS_COUNT >= 1
   {
	  .Phy           = &UART0_PHY, // warning: initialization discards qualifiers from pointer target type
	  .Irq           = UART0_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart0IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart0IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart0IsrNative,
   #endif
   #ifdef UART_MODBUS_RTU_ENABLE
	  .IsrModbusRtu  = &Uart0IsrModbusRtu,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 2
   ,
   {
	  .Phy           = &UART1_PHY,
	  .Irq           = UART1_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart1IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart1IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart1IsrNative,
   #endif
   #ifdef UART_MODBUS_RTU_ENABLE
	  .IsrModbusRtu  = &Uart1IsrModbusRtu,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 3
   ,
   {
	  .Phy           = &UART2_PHY,
	  .Irq           = UART2_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart2IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart2IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart2IsrNative,
   #endif
   #ifdef UART_MODBUS_RTU_ENABLE
	  .IsrModbusRtu  = &Uart2IsrModbusRtu,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 4
   ,
   {
	  .Phy           = &UART3_PHY,
	  .Irq           = UART3_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart3IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart3IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart3IsrNative,
   #endif
   #ifdef UART_MODBUS_RTU_ENABLE
	  .IsrModbusRtu  = &Uart3IsrModbusRtu,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 5
   ,
   {
	  .Phy           = &UART4_PHY,
	  .Irq           = UART4_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart4IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart4IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart4IsrNative,
   #endif
   #ifdef UART_MODBUS_RTU_ENABLE
	  .IsrModbusRtu  = &Uart4IsrModbusRtu,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 6
   ,
   {
	  .Phy           = &UART5_PHY,
	  .Irq           = UART5_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart5IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart5IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart5IsrNative,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 7
   ,
   {
	  .Phy           = &UART6_PHY,
	  .Irq           = UART6_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart6IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart6IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart6IsrNative,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 8
   ,
   {
	  .Phy           = &UART7_PHY,
	  .Irq           = UART7_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart7IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart7IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart7IsrNative,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 9
   ,
   {
	  .Phy           = &UART8_PHY,
	  .Irq           = UART8_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart8IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart8IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart8IsrNative,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 10
   ,
   {
	  .Phy           = &UART9_PHY,
	  .Irq           = UART9_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart9IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart9IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart9IsrNative,
   #endif
   }
#endif

#if UART_CHANNELS_COUNT >= 11
   ,
   {
	  .Phy           = &UART10_PHY,
	  .Irq           = UART10_IRQ,
   #ifdef UART_BINARY_ENABLE
	  .IsrBinary     = &Uart10IsrBinary,
   #endif
   #ifdef UART_ASCII_ENABLE
	  .IsrAscii     = &Uart10IsrAscii,
   #endif
   #ifdef UART_NATIVE_ENABLE
	  .IsrNative     = &Uart10IsrNative,
   #endif
   }
#endif
};