//*****************************************************************************
//
//    Uart.c       UART communication services
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "UartCommon.h"
//#include "System.h"
#include "Cpu.h" 

// Binary mode
#ifdef UART_BINARY_ENABLE
#include "UartBinary.c"
#endif

// ASCII mode
#ifdef UART_ASCII_ENABLE
#include "UartAscii.c"
#endif

// Native mode
#ifdef UART_NATIVE_ENABLE
#include "UartNative.c"
#endif

// Modbus RTU mode
#ifdef UART_MODBUS_RTU_ENABLE
#include "UartModbusRtu.c"
#endif

// Uart structures definitions
#include "UartCommon.c"

#define UART_OVERSAMPLING_TRESHOLD  32           // oversampling treshold ( >= 16)

#define AVR32_USART_CR_RESET  (AVR32_USART_CR_RSTRX_MASK   | AVR32_USART_CR_RSTTX_MASK  | \
                               AVR32_USART_CR_RSTSTA_MASK  | AVR32_USART_CR_RSTIT_MASK  | \
                               AVR32_USART_CR_RSTNACK_MASK | AVR32_USART_CR_DTRDIS_MASK | \
                               AVR32_USART_CR_RTSDIS_MASK);


static TYesNo SetBaudRate(EUartAddress Uart, unsigned Baud) {	
   unsigned Oversampling;
   unsigned Dividier;
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];
   
   // check for 8x oversampling needed :
   Oversampling = F_BUS_A >= (Baud * UART_OVERSAMPLING_TRESHOLD) ? 16 : 8;
   // get fractional dividier * 2 :
   Dividier     = (F_BUS_A * (1 << AVR32_USART_BRGR_FP_SIZE) * 2) / (Baud * Oversampling);
   // round up :
   Dividier    += 1;
   Dividier   >>= 1;
   // check for range :
   if( Dividier < (1 << AVR32_USART_BRGR_FP_SIZE)){
      return( NO);                     // unable set baud rate
   }
   if( Dividier > ((1 << AVR32_USART_BRGR_CD_SIZE) - 1) << AVR32_USART_BRGR_FP_SIZE){
      return( NO);                     // unable set baud rate
   }
   // set mode register clock and oversampling :
   _UartConst[Uart].Phy->mr &= ~(AVR32_USART_MR_USCLKS_MASK |
                 AVR32_USART_MR_SYNC_MASK |
                 AVR32_USART_MR_OVER_MASK);
   _UartConst[Uart].Phy->mr |=   AVR32_USART_MR_USCLKS_MCK << AVR32_USART_MR_USCLKS_OFFSET |
               ((Oversampling == 16) ? AVR32_USART_MR_OVER_X16 : AVR32_USART_MR_OVER_X8) << AVR32_USART_MR_OVER_OFFSET;

   _UartConst[Uart].Phy->brgr = (Dividier >> AVR32_USART_BRGR_FP_SIZE)             << AVR32_USART_BRGR_CD_OFFSET |
                (Dividier & ((1 << AVR32_USART_BRGR_FP_SIZE) - 1)) << AVR32_USART_BRGR_FP_OFFSET;
				
   Handler->BaudRate = Baud;
   
   // set timer
   _UartConst[Uart].Phy->rtor = ((Handler->BaudRate * UART_TIMEOUT_UNIT) / 1000) & AVR32_USART_RTOR_TO_MASK;

   return( YES);
}

void UartInit( EUartAddress Uart)
// Communication initialisation
{
   _UartConst[Uart].Phy->cr |= AVR32_USART_CR_RTSEN_MASK;
   
   switch( Uart){
	   
#if UART_CHANNELS_COUNT >= 1
      case UART_UART0 :
         GpioFunction( UART0_RXD_PIN, UART0_RXD_FUNCTION);
         GpioFunction( UART0_TXD_PIN, UART0_TXD_FUNCTION);
#ifdef UART_HALF_DUPLEX_ENABLE
         GpioFunction( UART0_RTS_PIN, UART0_RTS_FUNCTION);
#endif
         GpioPullup( UART0_TXD_PIN);
         break;
#endif

#if UART_CHANNELS_COUNT >= 2
      case UART_UART1 :
         GpioFunction( UART1_RXD_PIN, UART1_RXD_FUNCTION);
         GpioFunction( UART1_TXD_PIN, UART1_TXD_FUNCTION);
#ifdef UART_HALF_DUPLEX_ENABLE
         GpioFunction( UART1_RTS_PIN, UART1_RTS_FUNCTION);
#endif
         GpioPullup( UART1_TXD_PIN);
         break;
#endif

#if UART_CHANNELS_COUNT >= 3
      case UART_UART2 :
         GpioFunction( UART2_RXD_PIN, UART2_RXD_FUNCTION);
         GpioFunction( UART2_TXD_PIN, UART2_TXD_FUNCTION);
#ifdef UART_HALF_DUPLEX_ENABLE
         GpioFunction( UART2_RTS_PIN, UART2_RTS_FUNCTION);
#endif
         GpioPullup( UART2_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 4
      case UART_UART3 :
         GpioFunction( UART3_RXD_PIN, UART3_RXD_FUNCTION);
         GpioFunction( UART3_TXD_PIN, UART3_TXD_FUNCTION);
#ifdef UART_HALF_DUPLEX_ENABLE
         GpioFunction( UART3_RTS_PIN, UART3_RTS_FUNCTION);
#endif
         GpioPullup( UART3_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 5
      case UART_UART4 :
         GpioFunction( UART4_RXD_PIN, UART4_RXD_FUNCTION);
         GpioFunction( UART4_TXD_PIN, UART4_TXD_FUNCTION);
         GpioPullup( UART4_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 6
      case UART_UART5 :
         GpioFunction( UART5_RXD_PIN, UART5_RXD_FUNCTION);
         GpioFunction( UART5_TXD_PIN, UART5_TXD_FUNCTION);
         GpioPullup( UART5_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 7
      case UART_UART6 :
         GpioFunction( UART6_RXD_PIN, UART6_RXD_FUNCTION);
         GpioFunction( UART6_TXD_PIN, UART6_TXD_FUNCTION);
         GpioPullup( UART6_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 8
      case UART_UART7 :
         GpioFunction( UART7_RXD_PIN, UART7_RXD_FUNCTION);
         GpioFunction( UART7_TXD_PIN, UART7_TXD_FUNCTION);
         GpioPullup( UART7_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 9
      case UART_UART8 :
         GpioFunction( UART8_RXD_PIN, UART8_RXD_FUNCTION);
         GpioFunction( UART8_TXD_PIN, UART8_TXD_FUNCTION);
         GpioPullup( UART8_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 10
      case UART_UART9 :
         GpioFunction( UART9_RXD_PIN, UART9_RXD_FUNCTION);
         GpioFunction( UART9_TXD_PIN, UART9_TXD_FUNCTION);
         GpioPullup( UART9_TXD_PIN);
         break;
#endif
		 
#if UART_CHANNELS_COUNT >= 11
      case UART_UART10 :
         GpioFunction( UART10_RXD_PIN, UART10_RXD_FUNCTION);
         GpioFunction( UART10_TXD_PIN, UART10_TXD_FUNCTION);
         GpioPullup( UART10_TXD_PIN);
         break;
#endif

      default :
         break;
   }
}	

void UartSetup( EUartAddress Uart, unsigned Baud, EUartFormat Format)
// Set communication parameters
{
   // reset controller :
   _UartConst[Uart].Phy->mr   = 0;
   _UartConst[Uart].Phy->ttgr = 0;
   _UartConst[Uart].Phy->cr   = AVR32_USART_CR_RESET; // co ud�l� reset s RTS? (n�hodn� enable vys�la�e...??)
   UartDisableTransmitter(Uart);

   // set baud rate dividier :
   if( !SetBaudRate( Uart, Baud)){
	   SetBaudRate( Uart, 9600);        // unable set baud rate, set default
   }

   // set bit width, parity and stop bits :
   switch( Format){
      default :
      case UART_8BIT :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_NONE      << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_8BIT_EVEN :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_EVEN      << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_8BIT_ODD :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_ODD       << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_8BIT_MARK :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_MARK      << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_8BIT_SPACE :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_8        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_SPACE     << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_7BIT :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_NONE      << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_7BIT_EVEN :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_EVEN      << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_7BIT_ODD :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_ODD       << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_7BIT_MARK :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_MARK      << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
      case UART_7BIT_SPACE :
         _UartConst[Uart].Phy->mr |=  (AVR32_USART_MR_CHRL_7        << AVR32_USART_MR_CHRL_OFFSET) |
                                 (AVR32_USART_MR_NBSTOP_1      << AVR32_USART_MR_NBSTOP_OFFSET) |
                                 (AVR32_USART_MR_PAR_SPACE     << AVR32_USART_MR_PAR_OFFSET) |
                                 (AVR32_USART_MR_CHMODE_NORMAL << AVR32_USART_MR_CHMODE_OFFSET) |
                                 (AVR32_USART_MR_MODE_NORMAL   << AVR32_USART_MR_MODE_OFFSET);
         break;
   }
   
#ifdef UART_HALF_DUPLEX_ENABLE
   //_UartConst[Uart].Phy->mr |= AVR32_USART_MR_MODE_RS485;          // USART1 only
#endif
}	

void UartModeSet( EUartAddress Uart, EUartMode Mode)
// Set operating mode
{
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];

   switch(Mode) {
	   
#ifdef UART_BINARY_ENABLE
      case UART_MODE_BINARY_MASTER:
      case UART_MODE_BINARY_SLAVE:
	     INTC_register_interrupt((__int_handler) (_UartConst[Uart].IsrBinary), _UartConst[Uart].Irq, AVR32_INTC_INT1);
	     break;
#endif	

#ifdef UART_ASCII_ENABLE
	  case UART_MODE_ASCII_MASTER:
      case UART_MODE_ASCII_SLAVE:
	     INTC_register_interrupt((__int_handler) (_UartConst[Uart].IsrAscii), _UartConst[Uart].Irq, AVR32_INTC_INT1);
	     break;
#endif	

#ifdef UART_NATIVE_ENABLE
	  case UART_MODE_NATIVE_MASTER:
      case UART_MODE_NATIVE_SLAVE:
		 if(UartGetCharacterLength(Uart) == 8) {
			 INTC_register_interrupt((__int_handler) (_UartConst[Uart].IsrNative), _UartConst[Uart].Irq, AVR32_INTC_INT1);
		 }
	     break;
#endif	

#ifdef UART_MODBUS_RTU_ENABLE
	  case UART_MODE_MODBUS_RTU_MASTER:
      case UART_MODE_MODBUS_RTU_SLAVE:
	     INTC_register_interrupt((__int_handler) (_UartConst[Uart].IsrModbusRtu), _UartConst[Uart].Irq, AVR32_INTC_INT1);
	     break;
#endif	
		 
	  default:
	     return;
   }
   
   Handler->Mode = Mode;

   _UartConst[Uart].Phy->ier = AVR32_USART_IER_RXRDY_MASK | AVR32_USART_IER_TIMEOUT_MASK | AVR32_USART_IER_PARE_MASK | AVR32_USART_IER_FRAME_MASK | AVR32_USART_IER_OVRE;
   
   _UartConst[Uart].Phy->cr = AVR32_USART_CR_TXEN_MASK;
}	

void UartTimeoutSet( EUartAddress Uart, unsigned ReplyTimeout, unsigned IntercharacterTimeout)
// Set timeout based communication
{
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];

   Handler->IntercharacterTimeout = IntercharacterTimeout;
   Handler->ReplyTimeout = ReplyTimeout;
}

#ifdef UART_ASCII_ENABLE
void UartFramingSet( EUartAddress Uart, char LeaderCharacter, char TrailerCharacter)
// Set framing characters based communication
{
	TUartDetailsAscii *Handler = (TUartDetailsAscii *) _Uart[Uart];
	
	Handler->Sof = LeaderCharacter;
	Handler->Eof = TrailerCharacter;
}
#endif
/*
void UartSendDelay( EUartAddress Uart, unsigned Delay);
// Start send after <Delay>
*/
//-----------------------------------------------------------------------------

void UartReceive( EUartAddress Uart, void *Buffer, int Size)
// Start Rx with <Buffer> and <Size>
{
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];
   
#ifdef UART_HALF_DUPLEX_ENABLE
   while(UartSendStatus( Uart) == UART_SEND_ACTIVE);
#endif

   InterruptDisable();
   
   Handler->RxBuffer = Buffer;
   Handler->RxBufferSize = Size;
   Handler->RxPointer = 0;
   Handler->RxStatus = UART_RECEIVE_ACTIVE;

   Handler->Flags &= ~UART_RX_FLAGS;

   Handler->IntercharacterTimer = 0;
   Handler->ReplyTimer = 0;
   _UartConst[Uart].Phy->idr = AVR32_USART_IDR_RXBRK_MASK;
   UartStartTimeout(Uart);
   UartEnableReceive(Uart);
   
   InterruptEnable();
}

EUartReceiveStatus UartReceiveStatus( EUartAddress Uart)
// Returns receiving status
{
	TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];
	
	return Handler->RxStatus;
}

int UartReceiveSize( EUartAddress Uart)
// Returns received data size
{
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];
   
   return Handler->RxPointer;
}

#ifdef UART_NATIVE_ENABLE	
byte UartReceiveAddress( EUartAddress Uart)
// Returns protocol specific received address field
{
	TUartDetailsNative *Handler = (TUartDetailsNative *) _Uart[Uart];
	
	return Handler->RxFrame.Address;
}
#endif

//-----------------------------------------------------------------------------
volatile TUartDetailsCommon *HandlerVol;
void UartSend( EUartAddress Uart, void *Buffer, int Size)
// Start Tx with <Buffer> and <Size>
{
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];
   HandlerVol = Handler;
#ifdef UART_HALF_DUPLEX_ENABLE
   while(UartReceiveStatus( Uart) == UART_RECEIVE_ACTIVE);
#endif

   while(UartSendStatus(Uart) == UART_SEND_ACTIVE);
   
   Handler->TxBuffer = Buffer;
   Handler->TxDataSize = Size;
   Handler->TxPointer = 0;
   Handler->TxStatus = UART_SEND_ACTIVE;
   _UartConst[Uart].Phy->idr = AVR32_USART_IDR_RXBRK_MASK;
   
   Handler->Flags  &= ~UART_TX_FLAGS; 

   UartEnableTransmitter(Uart);
   UartEnableTransmit(Uart);	
}

volatile EUartSendStatus UartSendStatus( EUartAddress Uart)
// Returns send status
{
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];

   return Handler->TxStatus & UART_TX_STATUS_VALID_PART; // nejvy��� bit prom�nn� je pou��v�n ke kontrole konce vys�l�n�
}

#ifdef UART_NATIVE_ENABLE
void UartSendAddress( EUartAddress Uart, byte Address)
// Returns protocol specific received address field
{
	TUartDetailsNative *Handler = (TUartDetailsNative *) _Uart[Uart];
	
	Handler->TxFrame.Address = Address;
}
#endif
//-----------------------------------------------------------------------------

void UartFlush( EUartAddress Uart)
// Flush Rx/Tx buffer
{
	TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];
	
	InterruptDisable();
	
	/*
	  Pr�v� zpracov�van� r�mec ASCII, native modu by nebylo dobr� zahazovat
	  Vy�e�it
	*/
	
	Handler->RxPointer = 0;
	Handler->TxPointer = 0;
	Handler->Flags = 0;
	
	InterruptEnable();
}

void UartStop( EUartAddress Uart)
// Stop Rx/Tx discard Rx/Tx data
{
   TUartDetailsCommon *Handler = (TUartDetailsCommon *) _Uart[Uart];
   
   UartDisableTransmit( Uart);
   UartDisableReceive( Uart);

   Handler->RxStatus = UART_RECEIVE_IDLE;
   Handler->TxStatus = UART_SEND_IDLE;
}



#ifdef UART_NATIVE_ENABLE
void UartAssignSlaveAddress( EUartAddress Uart, byte Address) {
	TUartDetailsNative *Handler = (TUartDetailsNative *) _Uart[Uart];
	
	if(Handler->Common.Mode != UART_MODE_NATIVE_SLAVE) {
		return;
	}
	
	Handler->SlaveAddress = Address;
}
#endif