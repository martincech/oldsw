//*****************************************************************************
//
//    Uart.h       UART communication services
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************
#ifdef UART_MODBUS_RTU_ENABLE
//#error RTUH
#endif
#ifndef __Uart_H__
   #define __Uart_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __UartDef_H__
   #include "UartDef.h"
#endif

// Max channels
#define UART_CHANNELS_MAX  4

// Check
#if UART_CHANNELS_COUNT <= 0 || UART_CHANNELS_COUNT > UART_CHANNELS_MAX
   #error "UART: Channels count out of range"
#endif

#if !defined(UART_SLAVE_ENABLE) && !defined(UART_MASTER_ENABLE)
   #error "UART: Master or/and slave mode must be enabled"
#endif

#if !defined(UART_BINARY_ENABLE) && !defined(UART_ASCII_ENABLE) && !defined(UART_NATIVE_ENABLE) && !defined(UART_MODBUS_RTU_ENABLE)
   #error "UART: At least one mode must be enabled"
#endif
/*
// Defaults
#ifdef UART_BINARY_ENABLE 
   #ifdef UART_MASTER_ENABLE
      #define UART_DEFAULT_MODE    UART_MODE_BINARY_MASTER
   #else ifdef UART_SLAVE_ENABLE
      #define UART_DEFAULT_MODE    UART_MODE_BINARY_SLAVE
   #endif
#else ifdef UART_ASCII_ENABLE 
   #ifdef UART_MASTER_ENABLE
      #define UART_DEFAULT_MODE    UART_MODE_ASCII_MASTER
   #else ifdef UART_SLAVE_ENABLE
      #define UART_DEFAULT_MODE    UART_MODE_ASCII_SLAVE
   #endif
#else ifdef UART_NATIVE_ENABLE 
   #ifdef UART_MASTER_ENABLE
      #define UART_DEFAULT_MODE    UART_MODE_NATIVE_MASTER
   #else ifdef UART_SLAVE_ENABLE
      #define UART_DEFAULT_MODE    UART_MODE_NATIVE_SLAVE
   #endif
#endif

#define UART_DEFAULT_BAUD                      57600
#define UART_DEFAULT_FORMAT                    UART_8BIT
#define UART_DEFAULT_REPLY_TIMEOUT             1000
#define UART_DEFAULT_INTERCHARACTER_TIMEOUT    10
*/
// UART device address :
typedef enum {
#if UART_CHANNELS_COUNT >= 1
   UART_UART0 = 0,
#endif
#if UART_CHANNELS_COUNT >= 2
   UART_UART1,
#endif
#if UART_CHANNELS_COUNT >= 3
   UART_UART2,
#endif
#if UART_CHANNELS_COUNT >= 4
   UART_UART3,
#endif
#if UART_CHANNELS_COUNT >= 5
   UART_UART4,
#endif
#if UART_CHANNELS_COUNT >= 6
   UART_UART5,
#endif
#if UART_CHANNELS_COUNT >= 7
   UART_UART6,
#endif
#if UART_CHANNELS_COUNT >= 8
   UART_UART7,
#endif
#if UART_CHANNELS_COUNT >= 9
   UART_UART8,
#endif
#if UART_CHANNELS_COUNT >= 10
   UART_UART9,
#endif
#if UART_CHANNELS_COUNT >= 11
   UART_UART10,
#endif
   _UART_LAST
} EUartAddress;   

#define UART_TIMEOUT_UNIT  1
   
// UART receive status :
typedef enum {
   UART_RECEIVE_IDLE,             // stopped, no activity
   UART_RECEIVE_ACTIVE,           // receiving active
   UART_RECEIVE_PARITY_ERROR,     // receiving terminated with parity error
   UART_RECEIVE_OVERRUN_ERROR,    // received characters count greater than buffer size
   UART_RECEIVE_REPLY_TIMEOUT,    // no characters received up to reply timeout
   UART_RECEIVE_TIMEOUT,          // intercharacter timeout   
   UART_RECEIVE_FRAME,            // complete frame received
   _UART_RECEIVE_LAST
} EUartReceiveStatus;

// UART send status :
typedef enum {
   UART_SEND_IDLE,                // stopped, no activity
   UART_SEND_ACTIVE,              // send active
   UART_SEND_DONE,                // send done
   _UART_SEND_LAST
} EUartSendStatus;   
   

// UART operating mode :
typedef enum {
#ifdef UART_BINARY_ENABLE
   #ifdef UART_MASTER_ENABLE
      UART_MODE_BINARY_MASTER,       // binary mode master side
   #endif
   #ifdef UART_SLAVE_ENABLE
      UART_MODE_BINARY_SLAVE,        // binary mode slave side
   #endif
#endif

#ifdef UART_ASCII_ENABLE
   #ifdef UART_MASTER_ENABLE
      UART_MODE_ASCII_MASTER,        // ASCII framing mode master side
   #endif
   #ifdef UART_SLAVE_ENABLE
      UART_MODE_ASCII_SLAVE,         // ASCII framing mode slave size
   #endif
#endif

#ifdef UART_NATIVE_ENABLE
   #ifdef UART_MASTER_ENABLE
   UART_MODE_NATIVE_MASTER,       // native framing protocol master side
   #endif
   #ifdef UART_SLAVE_ENABLE
      UART_MODE_NATIVE_SLAVE,        // native framing protocol slave side
   #endif
#endif

#ifdef UART_MODBUS_RTU_ENABLE
   #ifdef UART_MASTER_ENABLE
   UART_MODE_MODBUS_RTU_MASTER,       // native framing protocol master side
   #endif
   #ifdef UART_SLAVE_ENABLE
      UART_MODE_MODBUS_RTU_SLAVE,        // native framing protocol slave side
   #endif
#endif

   _UART_MODE_LAST
} EUartMode;

// UART special timeout :
#define UART_TIMEOUT_OFF    0     // without timeout

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void UartInit( EUartAddress Uart);
// Communication initialisation

void UartSetup( EUartAddress Uart, unsigned Baud, EUartFormat Format);
// Set communication parameters

void UartModeSet( EUartAddress Uart, EUartMode);
// Set operating mode

void UartTimeoutSet( EUartAddress Uart, unsigned ReplyTimeout, unsigned IntercharacterTimeout);
// Set timeout based communication

#ifdef UART_ASCII_ENABLE
void UartFramingSet( EUartAddress Uart, char LeaderCharacter, char TrailerCharacter);
// Set framing characters based communication
#endif

void UartSendDelay( EUartAddress Uart, unsigned Delay);
// Start send after <Delay>

//-----------------------------------------------------------------------------

void UartReceive( EUartAddress Uart, void *Buffer, int Size);
// Start Rx with <Buffer> and <Size>

void UartReceiveConfirm( EUartAddress Uart, int Size);
// Notify UART that <Size> elements from buffer have been read out

EUartReceiveStatus UartReceiveStatus( EUartAddress Uart);
// Returns receiving status

int UartReceiveSize( EUartAddress Uart);
// Returns received data size

#ifdef UART_NATIVE_ENABLE
byte UartReceiveAddress( EUartAddress Uart);
// Returns protocol specific received address field
#endif

//-----------------------------------------------------------------------------

void UartSend( EUartAddress Uart, void *Buffer, int Size);
// Start Tx with <Buffer> and <Size>

volatile EUartSendStatus UartSendStatus( EUartAddress Uart);
// Returns send status

#ifdef UART_NATIVE_ENABLE
void UartSendAddress( EUartAddress Uart, byte Address);
// Send address
#endif
//-----------------------------------------------------------------------------

void UartFlush( EUartAddress Uart);
// Flush Rx/Tx buffer

void UartStop( EUartAddress Uart);
// Stop Rx/Tx discard Rx/Tx data

#ifdef UART_NATIVE_ENABLE
void UartAssignSlaveAddress( EUartAddress Uart, byte Address);

#endif

#endif