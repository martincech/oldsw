//*****************************************************************************
//
//    UartCommon.h     Common UART definitions
//    Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __UartCommon_H__
   #define __UartCommon_H__
   
#ifndef __Uart_H__
   #include "Uart.h"
#endif

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#if defined(UART_ASCII_ENABLE) || defined(UART_NATIVE_ENABLE)
   #define UART_RX_FLAGS   0x0F
   #define UART_TX_FLAGS   0xF0

   // Reset Rx flags
   #define UartResetRxFlags( Uart)      (Handler->Common.Flags &= ~UART_RX_FLAGS)

   // Reset Tx flags
   #define UartResetTxFlags( Uart)      (Handler->Common.Flags &= ~UART_TX_FLAGS)
#endif

// Character length being used by UART
#define UartGetCharacterLength(Uart)  (((_UartConst[Uart].Phy->mr & AVR32_USART_MR_CHRL_MASK) >> AVR32_USART_MR_CHRL_OFFSET) + 5)

// Start timer
#define UartStartTimeout( Uart)      (_UartConst[Uart].Phy->cr = (AVR32_USART_CR_STTTO_MASK | AVR32_USART_CR_RETTO_MASK))

// Stop timer
#define UartStopTimeout( Uart)      (_UartConst[Uart].Phy->cr = AVR32_USART_CR_STTTO_MASK)

// Enable receive
#define UartEnableReceive( Uart)         (_UartConst[Uart].Phy->cr = AVR32_USART_CR_RXEN_MASK)

// Disable receive (& timer)
#define UartDisableReceive( Uart)         (_UartConst[Uart].Phy->cr = (AVR32_USART_CR_RXDIS_MASK | AVR32_USART_CR_STTTO_MASK))

// Enable TXEMPTY interrupt
#define UartEnableTxEmpty( Uart)                 (_UartConst[Uart].Phy->ier = AVR32_USART_IER_TXEMPTY_MASK)

// Disable TXEMPTY interrupt
#define UartDisableTxEmpty( Uart)                 (_UartConst[Uart].Phy->idr = AVR32_USART_IDR_TXEMPTY_MASK)

// Enable remote transmitter circuit
#ifdef UART_HALF_DUPLEX_ENABLE
   #define UartEnableTransmitter( Uart)      (_UartConst[Uart].Phy->cr = AVR32_USART_CR_RTSDIS_MASK)
#else
   #define UartEnableTransmitter( Uart)      
#endif

#define UartEnableTransmit( Uart)         (_UartConst[Uart].Phy->ier = AVR32_USART_IER_TXRDY_MASK)

// Disable remote transmitter circuit
#ifdef UART_HALF_DUPLEX_ENABLE
   #define UartDisableTransmitter( Uart)      (_UartConst[Uart].Phy->cr = AVR32_USART_CR_RTSEN_MASK)
#else
   #define UartDisableTransmitter( Uart)      
#endif

// Disable transmit
#define UartDisableTransmit( Uart)         (_UartConst[Uart].Phy->idr = AVR32_USART_IDR_TXRDY_MASK)

// Reset the status bits parity, frame, overrun, MANERR and RXBRK in CSR.
#define UartResetStatus( Uart)         (_UartConst[Uart].Phy->cr = AVR32_USART_CR_RSTSTA_MASK)

// Check parity error
#define UartIsParityError( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_PARE_MASK)

// Check framing error
#define UartIsFramingError( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_FRAME_MASK)

// Check overrun error
#define UartIsOverrunError( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_OVRE_MASK)

// Check timeout
#define UartIsTimeout( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_TIMEOUT_MASK)

// Check Rx break
#define UartIsRxBreak( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_RXBRK_MASK)

// Check if char has been received
#define UartIsRxReady( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_RXRDY_MASK)

// Check if transmitter is empty
#define UartIsTxEmpty( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_TXEMPTY_MASK)

// Check if transmitter has space for new character
#define UartIsTxReady( Uart)         (_UartConst[Uart].Phy->csr & AVR32_USART_CSR_TXRDY_MASK)

// Check if new character is being awaited
//#define UartIsTx( Uart)        (UartIsTxReady(Uart) && !UartIsTxComplete(Uart))

#define UART_TX_STATUS_VALID_PART      0x7F
#define UART_TX_COMPLETE_CHECK         0x80

#define UartIsTxComplete( Uart)              (Handler->Common.TxStatus & UART_TX_COMPLETE_CHECK)

#define UartEnableCheckTxComplete( Uart)     UartEnableTxEmpty(Uart);Handler->Common.TxStatus |= UART_TX_COMPLETE_CHECK


// Structure assigned to each individual UART channel
// Constant values to be stored in program memory
typedef volatile struct __packed PROGMEM { // !!! PACKED !!!
	avr32_usart_t    *Phy;            // UART registers
	word Irq;                          // IRQ channel
#ifdef UART_BINARY_ENABLE
    void (*IsrBinary)(void);               // ISR pointer for binary mode
#endif
#ifdef UART_ASCII_ENABLE
    void (*IsrAscii)(void);                // ISR pointer for ASCII mode
#endif
#ifdef UART_NATIVE_ENABLE
    void (*IsrNative)(void);               // ISR pointer for native mode
#endif
#ifdef UART_MODBUS_RTU_ENABLE
    void (*IsrModbusRtu)(void);               // ISR pointer for Mdobus RTU mode
#endif
} const TUartConstDetails;

TUartConstDetails _UartConst[];



// Native frame structure
/* 
V ka�d� struktu�e TUartDetails je po jednom Tx a Rx r�mci
Kdyby byl p�ij�man�/pos�lan� r�mec zpracov�v�n real-time p��mo v interruptu
pak by n�kter� �leny struktury nebyly pot�eba:
- HeaderCrc
- DataSize pro Tx frame (vyu�ila by se TxDataSize z TUartDetails)
a ve sv�m d�sledku by pak TUartNativeFrame nebyla v�bec pot�eba
proto�e ISR by p�e�la do podoby stavov�ho automatu, kdy p��jem ka�d�ho bytu
by byl vlastn� stav

switch(stav) {
   case data:
      // ulo� data do bufferu
	  break;
	  
   case headerCrc:
      if(prijaty_znak = vypoctiHeaderCrc())...
	  DalsiStav();
	  break;
	  
   case dataSizeLsb
      _Uart[Uart].TxDataSize = prijaty_znak;
	  DalsiStav();
	  break;
	  
   case dataSizeLsb
      _Uart[Uart].TxDataSize |= prijaty_znak << 8;
	  DalsiStav();
	  break;
	  
	// atd...
}
*/

// Structure assigned to each individual UART channel
/* 
pokud bude dan� aplikace pou��vat v�ce m�du najednou, pak v�echny kan�ly
budou obsahovat i prom�nn�, kter� ve skute�nosti nepou��vaj�, a ty tak
budou jenom zbyte�n� zab�rat m�sto v pam�ti,
m�d ka�d�ho kan�lu UARTu by se mohl nastavovat pevn� p�i kompilaci,
dan�mu kan�lu by byla pevn� p�i�azena struktura dle jeho m�du
*/
typedef volatile struct __packed {
	byte               *RxBuffer;
    byte               *TxBuffer;
	word                RxBufferSize;
	word                TxDataSize;
	word                RxPointer;
	word                TxPointer;
    word                BaudRate;
	word                IntercharacterTimer;
	word                ReplyTimer;
    word                IntercharacterTimeout;
	word                ReplyTimeout;
	word                _Dummy0;
    EUartMode           Mode;
    EUartReceiveStatus  RxStatus;
	EUartSendStatus     TxStatus;
	byte                Flags;
} TUartDetailsCommon; // !!!!!! must be aligned to dword !!!!!!

byte _Uart[UART_CHANNELS_COUNT][320] __attribute__ ((aligned (sizeof(dword)))); // every member of array must begin on dword boundary !!! 

#endif