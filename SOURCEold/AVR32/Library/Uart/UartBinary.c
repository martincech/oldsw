//*****************************************************************************
//
//    UartBinary.c       UART binary mode
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "UartCommon.h"

/*
   Bin�rn� m�d
   Data jsou p�ij�m�na po dobu reply timeout a dokud nenastane intercharacter timeout nebo n�jak� error (overrun, framing, parity...)
   Pokud n�co z toho nastane, je p��jem zastaven, u�ivatel jej mus� obnovit vol�n�m UartReceive()
*/

typedef volatile struct __packed { // !!! PACKED !!!
	TUartDetailsCommon  Common;
} TUartDetailsBinary;

/************************************************************************/
/* Binary ISR                                                           */
/************************************************************************/

void UartIsrBinary( EUartAddress Uart) {
   TUartDetailsBinary *Handler = (TUartDetailsBinary *) _Uart[Uart];
   
   if(UartIsTimeout(Uart)) {
	   UartStartTimeout(Uart);
	   
	   Handler->Common.IntercharacterTimer++;
       Handler->Common.ReplyTimer++;
	   
	   if(Handler->Common.IntercharacterTimeout != UART_TIMEOUT_OFF && Handler->Common.IntercharacterTimer >= Handler->Common.IntercharacterTimeout) {
	     Handler->Common.RxStatus = UART_RECEIVE_TIMEOUT;
	     UartDisableReceive(Uart);
	   } else if(Handler->Common.ReplyTimeout != UART_TIMEOUT_OFF && Handler->Common.ReplyTimer >= Handler->Common.ReplyTimeout) {
	     Handler->Common.RxStatus = UART_RECEIVE_REPLY_TIMEOUT;
	     UartDisableReceive(Uart);
	   }
   } else if(UartIsRxReady(Uart)) {
	  if(Handler->Common.RxPointer < Handler->Common.RxBufferSize) {
	     Handler->Common.RxBuffer[Handler->Common.RxPointer] = _UartConst[Uart].Phy->rhr;
         
		 Handler->Common.RxPointer++;
		 
		 Handler->Common.IntercharacterTimer = 0;
	  } else {
		 Handler->Common.RxStatus = UART_RECEIVE_OVERRUN_ERROR;
	     UartDisableReceive(Uart);
	  }
   } else if(UartIsTxReady(Uart) && Handler->Common.TxStatus == UART_SEND_ACTIVE) {
	  if(Handler->Common.TxPointer < Handler->Common.TxDataSize) {
	     _UartConst[Uart].Phy->thr = Handler->Common.TxBuffer[Handler->Common.TxPointer];
		 
		 Handler->Common.TxPointer++;
	  }	else {
		 UartDisableTransmit(Uart);
		 UartEnableCheckTxComplete( Uart);
	  }
   } else if(UartIsTxComplete(Uart)) {
      Handler->Common.TxStatus = UART_SEND_DONE;
      UartDisableTransmitter(Uart);
      UartDisableTxEmpty(Uart);
   } else if(UartIsParityError(Uart)) {
      UartResetStatus(Uart);
      Handler->Common.RxStatus = UART_RECEIVE_PARITY_ERROR;
      UartDisableReceive(Uart);
   } else if(UartIsFramingError(Uart)) { // Dod�lat UART_RECEIVE_FRAMING_ERROR ??
      UartResetStatus(Uart);
      Handler->Common.RxStatus = UART_RECEIVE_PARITY_ERROR;
      UartDisableReceive(Uart);
   } else if(UartIsOverrunError( Uart)) {
      UartResetStatus(Uart);
      Handler->Common.RxStatus = UART_RECEIVE_OVERRUN_ERROR;
      UartDisableReceive(Uart);
   } else if(UartIsRxBreak( Uart)) {
      UartResetStatus(Uart);
   }  
}

/************************************************************************/
/* Individual channels ISRs                                             */
/************************************************************************/

#if UART_CHANNELS_COUNT >= 1
__attribute__((__interrupt__)) void Uart0IsrBinary(void)
{
   UartIsrBinary(UART_UART0);
}
#endif

#if UART_CHANNELS_COUNT >= 2
__attribute__((__interrupt__)) void Uart1IsrBinary(void)
{
   UartIsrBinary(UART_UART1);
}
#endif

#if UART_CHANNELS_COUNT >= 3
__attribute__((__interrupt__)) void Uart2IsrBinary(void)
{
   UartIsrBinary(UART_UART2);
}
#endif

#if UART_CHANNELS_COUNT >= 4
__attribute__((__interrupt__)) void Uart3IsrBinary(void)
{
   UartIsrBinary(UART_UART3);
}
#endif

/************************************************************************/
/* Binary read                                                          */
/************************************************************************/

int UartBinaryRead(EUartAddress Uart, void *Buffer, int Size) {
   UartModeSet(Uart, UART_MODE_BINARY_MASTER);
   
   UartReceive( Uart, Buffer, Size);
   
   while(1) {
         switch( UartReceiveStatus( UART_UART1)){
            case UART_RECEIVE_ACTIVE:
			   break;
			   
            case UART_RECEIVE_TIMEOUT :
			   UartStop(Uart);
               return UartReceiveSize( UART_UART1);
               break;

            default :
               return 0;
		 }     
   }
}

/************************************************************************/
/* Binary write                                                         */
/************************************************************************/

int UartBinaryWrite(EUartAddress Uart, void *Buffer, int Size) {
   TUartDetailsBinary *Handler = (TUartDetailsBinary *) _Uart[Uart];
   
   UartModeSet(Uart, UART_MODE_BINARY_MASTER);
   
   UartSend( Uart, Buffer, Size);
   
   while(UartSendStatus( Uart) == UART_SEND_ACTIVE);

   return Handler->Common.TxPointer;
}
