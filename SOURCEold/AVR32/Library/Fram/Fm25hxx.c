 //*****************************************************************************
//
//    Fm25hxx.c   Fram
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

// Vytvo�it lep�� PDCA driver

#include "Hardware.h"
#include "Fm25hxx.h"
#include "spi.h"
#include "pdca.h"
#include "System.h"

#define  FM25Hxx_WREN    0x06
#define  FM25Hxx_WRDI    0x04
#define  FM25Hxx_RDSR    0x05
#define  FM25Hxx_WRSR    0x01
#define  FM25Hxx_READ    0x03
#define  FM25Hxx_WRITE   0x02
#define  FM25Hxx_SLEEP   0xB9

#define FRAM_TX_DMA_CHANNEL   0
#define FRAM_RX_DMA_CHANNEL   1

#define FM25Hxx_SLEEP_RECOVERY 450

volatile EUFramStatus FramStatus = FRAM_IDLE;

static byte ControlBuffer[4];

TYesNo SpiReadChar(volatile avr32_spi_t *spi, byte *data) {
   unsigned int timeout = 10000;

   while (!(spi->sr & AVR32_SPI_SR_TDRE_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   spi->tdr = 0xFF << AVR32_SPI_TDR_TD_OFFSET;

   while((spi->sr & AVR32_SPI_SR_TXEMPTY_MASK) != 0);
   
   timeout = SPI_TIMEOUT;

   while((spi->sr & (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) !=
         (AVR32_SPI_SR_RDRF_MASK | AVR32_SPI_SR_TXEMPTY_MASK)) {
      if (!timeout--) {
         return NO;
      }
   }

   *data = spi->rdr >> AVR32_SPI_RDR_RD_OFFSET;

   return YES;
}

__attribute__((__interrupt__)) void FramPdcaISR(void) {
   pdca_disable(FRAM_TX_DMA_CHANNEL);
   pdca_disable_interrupt_transfer_complete(FRAM_RX_DMA_CHANNEL);
   pdca_disable_interrupt_transfer_complete(FRAM_TX_DMA_CHANNEL);
   spi_unselectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   FramStatus = FRAM_IDLE;
}

void FramInit(void) {
   spi_options_t spiOptions = {
      .reg          = SPI_FRAM_NPCS,
      .baudrate     = SPI_FRAM_MASTER_SPEED,
      .bits         = 8,
      .spck_delay   = 0,
      .trans_delay  = 0,
      .stay_act     = 1,
      .spi_mode     = 0,
      .modfdis      = 1
   };

   SpiFramPortInit();
	
   spi_initMaster( SPI_FRAM_DEVICE, &spiOptions);
   spi_selectionMode( SPI_FRAM_DEVICE, 0, 0, 0);
   spi_enable( SPI_FRAM_DEVICE);
   spi_setupChipReg( SPI_FRAM_DEVICE, &spiOptions, F_BUS_A);
   
   pdca_enable(FRAM_RX_DMA_CHANNEL); // can be always enabled

   INTC_register_interrupt((__int_handler) (&FramPdcaISR), AVR32_PDCA_IRQ_0, AVR32_INTC_INT1);
   INTC_register_interrupt((__int_handler) (&FramPdcaISR), AVR32_PDCA_IRQ_1, AVR32_INTC_INT1);
}

void FramSleep(void) {
	if(FramStatus == FRAM_SLEEP) {
		return;
	}
	
	while(FramStatus == FRAM_ACTIVE);
	
	spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
	spi_write(SPI_FRAM_DEVICE, FM25Hxx_SLEEP);
	while(!spi_writeEndCheck(SPI_FRAM_DEVICE));
    spi_unselectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
	
	FramStatus = FRAM_SLEEP;
}

void FramWakeUp(void) {
	if(FramStatus != FRAM_SLEEP) {
		return;
	}

	spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
	SysUDelay(FM25Hxx_SLEEP_RECOVERY);
    spi_unselectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
	
	FramStatus = FRAM_IDLE;
}

EUFramStatus FramGetStatus(void) {
	return FramStatus;
}

TYesNo FramReadNoWait(dword Address, byte *Buffer, dword Size) {
   while(FramStatus == FRAM_ACTIVE);
   
   // Prepare command & address data
   ControlBuffer[0] = FM25Hxx_READ;
   ControlBuffer[1] = (Address & 0x00FF0000) >> 16;
   ControlBuffer[2] = (Address & 0x0000FF00) >>  8;
   ControlBuffer[3] = (Address & 0x000000FF) >>  0;

   // there can be one character left in SPI Rx register after last transmission
   spi_get(SPI_FRAM_DEVICE); 
   
   // Both DMA channels must be used - Rx will not initiate transfer
   pdca_channel_options_t PdcaOptionsRx = {
      .addr          = ControlBuffer,
      .size          = sizeof(ControlBuffer),
      .r_addr        = Buffer,
      .r_size        = Size,
      .pid           = AVR32_PDCA_PID_SPI0_RX,
      .transfer_size = AVR32_PDCA_BYTE,
   };
   
   pdca_channel_options_t PdcaOptionsTx = {
      .addr          = ControlBuffer,
      .size          = sizeof(ControlBuffer),
      .r_addr        = Buffer,
      .r_size        = Size,
      .pid           = AVR32_PDCA_PID_SPI0_TX,
      .transfer_size = AVR32_PDCA_BYTE,
   };
   
   spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);

   FramStatus = FRAM_ACTIVE;

   pdca_init_channel(FRAM_RX_DMA_CHANNEL, &PdcaOptionsRx);
   pdca_init_channel(FRAM_TX_DMA_CHANNEL, &PdcaOptionsTx);
   pdca_enable(FRAM_TX_DMA_CHANNEL);
   pdca_enable_interrupt_transfer_complete(FRAM_RX_DMA_CHANNEL);
}	

dword FramRead(dword Address, byte *Buffer, dword Size) {
   FramReadNoWait(Address, Buffer, Size);

   while(FramStatus == FRAM_ACTIVE);

   return Size;
   /*word Ch;
   spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   
   spi_write(SPI_FRAM_DEVICE, FM25Hxx_READ);

   spi_write(SPI_FRAM_DEVICE, (Address & 0x00FF0000) >> 16);
   spi_write(SPI_FRAM_DEVICE, (Address & 0x0000FF00) >>  8);
   spi_write(SPI_FRAM_DEVICE, (Address & 0x000000FF) >>  0);
   
   while(Size--) {
	   spi_write(SPI_FRAM_DEVICE, 0xFF);
	   spi_read(SPI_FRAM_DEVICE, &Ch);
	   *Buffer++ = Ch;
   }

   spi_unselectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   
   return Size;*/
}

TYesNo FramWriteNoWait(dword Address, byte *Buffer, dword Size) {
   while(FramStatus == FRAM_ACTIVE);
   
   // Write enable latch
   spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   spi_write(SPI_FRAM_DEVICE, FM25Hxx_WREN);
   while(!spi_writeEndCheck(SPI_FRAM_DEVICE));
   spi_unselectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   
   // Prepare command & address data
   ControlBuffer[0] = FM25Hxx_WRITE;
   ControlBuffer[1] = (Address & 0x00FF0000) >> 16;
   ControlBuffer[2] = (Address & 0x0000FF00) >>  8;
   ControlBuffer[3] = (Address & 0x000000FF) >>  0;
   
   pdca_channel_options_t PdcaOptionsTx = {
      .addr          = ControlBuffer,
      .size          = sizeof(ControlBuffer),
      .r_addr        = Buffer,
      .r_size        = Size,
      .pid           = AVR32_PDCA_PID_SPI0_TX,
      .transfer_size = AVR32_PDCA_BYTE,
   };
   
   spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);

   FramStatus = FRAM_ACTIVE;

   pdca_init_channel(FRAM_TX_DMA_CHANNEL, &PdcaOptionsTx);
   pdca_enable(FRAM_TX_DMA_CHANNEL);
   pdca_enable_interrupt_transfer_complete(FRAM_TX_DMA_CHANNEL);
   
   return YES;
}

dword FramWrite(dword Address, byte *Buffer, dword Size) {
   FramWriteNoWait(Address, Buffer, Size);
   
   while(FramStatus == FRAM_ACTIVE);
   
   return Size;
   
/*
   // Write enable latch
   spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   spi_write(SPI_FRAM_DEVICE, FM25Hxx_WREN);
   while(!spi_writeEndCheck(SPI_FRAM_DEVICE));
   spi_unselectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   
   // Write
   spi_selectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);

   // Command
   spi_write(SPI_FRAM_DEVICE, FM25Hxx_WRITE);
   
   // Address
   spi_write(SPI_FRAM_DEVICE, (Address & 0x00FF0000) >> 16);
   spi_write(SPI_FRAM_DEVICE, (Address & 0x0000FF00) >>  8);
   spi_write(SPI_FRAM_DEVICE, (Address & 0x000000FF) >>  0);
   
   // Data
   while(Size--) {
	   spi_write(SPI_FRAM_DEVICE, *(Buffer++));
   }
   
   while(!spi_writeEndCheck(SPI_FRAM_DEVICE));
   spi_unselectChip(SPI_FRAM_DEVICE, SPI_FRAM_NPCS);
   
   return Size;*/
}