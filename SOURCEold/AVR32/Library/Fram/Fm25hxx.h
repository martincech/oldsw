//*****************************************************************************
//
//    Fm25hxx.h   Fram
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef Fm25hxx_H_
#define Fm25hxx_H_

#ifdef FRAM_FM25H20
   #define FRAM_SIZE 262144
#else 
   #error "Unsupported FRAM type"
#endif

// Current status of FRAM
typedef enum {
   FRAM_IDLE,                // no activity
   FRAM_SLEEP,               // sleeping
   FRAM_ACTIVE               // Rx/Tx active
} EUFramStatus; 

void FramInit(void);
// Init

void FramSleep(void);
// Make FRAM sleep

void FramWakeUp(void);
// Wake up

EUFramStatus FramGetStatus(void);
// Read current 

#define FramIsBusy      (FramGetStatus() == FRAM_ACTIVE)

TYesNo FramReadNoWait(dword Address, byte *Buffer, dword Size);
// Initiate read

dword FramRead(dword Address, byte *Buffer, dword Size);
// Perform read

TYesNo FramWriteNoWait(dword Address, byte *Buffer, dword Size);
// Initiate write

dword FramWrite(dword Address, byte *Buffer, dword Size);
// Perform write

#endif