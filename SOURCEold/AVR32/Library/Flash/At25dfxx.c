//*****************************************************************************
//
//   At25dfxx.c     Flash Memory Driver
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "At25dfxx.h"
#include "spi.h"
#include "pdca.h"

// Flash commands
#define  AT25DFxx_READ	            0x03 // no dummy, up to 50 MHz

#define  AT25DFxx_BLOCK_ERASE4	    0x20
#define  AT25DFxx_BLOCK_ERASE32	    0x52
#define  AT25DFxx_BLOCK_ERASE64	    0xD8
#define  AT25DFxx_CHIP_ERASE	    0x60

#define  AT25DFxx_READ_ID		    0x9F
#define  AT25DFxx_READ_STATUS	    0x05
#define  AT25DFxx_WRITE_STATUS1	    0x01
#define  AT25DFxx_WRITE_STATUS2	    0x31

#define  AT25DFxx_WRITE				0x02
#define  AT25DFxx_WRITE_ENABLE		0x06
#define  AT25DFxx_WRITE_DISABLE		0x04

#define FLASH_TX_DMA_CHANNEL   2
#define FLASH_RX_DMA_CHANNEL   3

volatile EFlashStatus FlashStatus = FLASH_IDLE;

static byte ControlBuffer[4];

__attribute__((__interrupt__)) void FlashPdcaISR(void) {
   pdca_disable(FLASH_TX_DMA_CHANNEL);
   pdca_disable_interrupt_transfer_complete(FLASH_RX_DMA_CHANNEL);
   pdca_disable_interrupt_transfer_complete(FLASH_TX_DMA_CHANNEL);
   spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
   FlashStatus = FLASH_IDLE;
}



static word FlashStatusRead(void);
static TYesNo FlashIsBusy(void);
static TYesNo FlashWaitWhileBusy(void);
static byte FlashIDRead(void);
static void FlashWriteEnable(void);
static void FlashWriteDisable(void);
static void FlashWriteStatus(byte ch);

static word FlashStatusRead(void)
// Read flash memory status register
{
   word Ch;
   word Status = 0;
	
   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
   spi_write(SPI_FLASH_DEVICE, AT25DFxx_READ_STATUS);
   spi_write(SPI_FLASH_DEVICE, 0xFF);
   spi_read(SPI_FLASH_DEVICE, &Status);
   spi_write(SPI_FLASH_DEVICE, 0xFF);
   spi_read(SPI_FLASH_DEVICE, &Ch);
   Status |= (Ch << 8);

   spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);

   return Status;
}

static TYesNo FlashIsBusy(void)
// Test whether flash memory is busy
{
	if(FlashStatusRead() & 0x0001) {
		return YES;
	} else {
		return NO;
	}
}

static TYesNo FlashWaitWhileBusy() // dod�lat timeout...
// Wait till flash memory is idle
{
   word Ch;
   
   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
   spi_write(SPI_FLASH_DEVICE, AT25DFxx_READ_STATUS);
   
   do {
	  spi_write(SPI_FLASH_DEVICE, 0xFF);
      spi_read(SPI_FLASH_DEVICE, &Ch);
   } while(Ch & 0x01);

   spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
}

static void FlashWriteEnable(void)
// Enable write latch
{ 
   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
   spi_write(SPI_FLASH_DEVICE, AT25DFxx_WRITE_ENABLE);
   while(!spi_writeEndCheck(SPI_FLASH_DEVICE));
   spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
}

/*static void FlashWriteDisable(void)
// Disable write latch
{ 
	SpiSelect();
	SpiByteWrite(AT25DFxx_WRITE_DISABLE);
	SpiRelease();
} */

static void FlashWriteStatus(byte ch)
// Write flash memory status register - byte 1
{
   FlashWaitWhileBusy();
	
   FlashWriteEnable();
   
   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
   spi_write(SPI_FLASH_DEVICE, AT25DFxx_WRITE_STATUS1);
   spi_write(SPI_FLASH_DEVICE, ch);
   while(!spi_writeEndCheck(SPI_FLASH_DEVICE));
   spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
}

/*static byte FlashIDRead(void)
// Read flash Family Code and Density Code
{
	byte temp;
	
	SpiSelect();
	SpiByteWrite(AT25DFxx_READ_ID);
	SpiByteRead();
	temp = SpiByteRead();
	SpiRelease();
	
	return temp;
}
*/




void FlashUnprotect(void)
// Globally unprotect flash memory
{
	FlashWriteStatus(0);
}

void FlashInit(void)
// Flash memory init
{
   spi_options_t spiOptions = {
      .reg          = SPI_FLASH_NPCS,
      .baudrate     = SPI_FLASH_MASTER_SPEED,
      .bits         = 8,
      .spck_delay   = 0,
      .trans_delay  = 0,
      .stay_act     = 1,
      .spi_mode     = 0,
      .modfdis      = 1
   };

   spi_initMaster( SPI_FLASH_DEVICE, &spiOptions);
   spi_selectionMode( SPI_FLASH_DEVICE, 0, 0, 0);
   spi_enable( SPI_FLASH_DEVICE);
   spi_setupChipReg( SPI_FLASH_DEVICE, &spiOptions, F_BUS_A);
	
   SpiFlashPortInit();

   pdca_enable(FLASH_RX_DMA_CHANNEL); // can be always enabled

   INTC_register_interrupt((__int_handler) (&FlashPdcaISR), AVR32_PDCA_IRQ_2, AVR32_INTC_INT1);
   INTC_register_interrupt((__int_handler) (&FlashPdcaISR), AVR32_PDCA_IRQ_3, AVR32_INTC_INT1);
	
	FlashUnprotect();
}

void FlashEraseChip(void)
// Erase whole chip
{
   while(FlashStatus == FLASH_ACTIVE);
   
   FlashWaitWhileBusy();
	
   FlashWriteEnable();

   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
   spi_write(SPI_FLASH_DEVICE, AT25DFxx_CHIP_ERASE);
   while(!spi_writeEndCheck(SPI_FLASH_DEVICE));
   spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
}

static void FlashErase(dword Address, byte Command)
// Erase one 4-kb block specified by address
{
   while(FlashStatus == FLASH_ACTIVE);
   
   FlashWaitWhileBusy();
	
   FlashWriteEnable();

   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
   spi_write(SPI_FLASH_DEVICE, Command);
   spi_write(SPI_FLASH_DEVICE, (byte) (Address >> 16));
   spi_write(SPI_FLASH_DEVICE, (byte) (Address >> 8 ));
   spi_write(SPI_FLASH_DEVICE, (byte) (Address >> 0 ));
   while(!spi_writeEndCheck(SPI_FLASH_DEVICE));
   spi_unselectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);
}

void FlashErase4kb(dword Address)
// Erase one 4-kb block specified by address
{
   FlashErase(Address, AT25DFxx_BLOCK_ERASE4);
}

void FlashErase32kb(dword Address)
// Erase one 32-kb block specified by address
{
   FlashErase(Address, AT25DFxx_BLOCK_ERASE32);
}

void FlashErase64kb(dword Address)
// Erase one 64-kb block specified by address
{
   FlashErase(Address, AT25DFxx_BLOCK_ERASE64);
}

TYesNo FlashWriteNoWait(dword Address, byte *Buffer, word Size)
// Write data to flash
{
	while(FlashStatus == FLASH_ACTIVE);
	
	if(Size > FLASH_PROGRAM_PAGE_SIZE) {
		Size = FLASH_PROGRAM_PAGE_SIZE;
	}
	
	FlashWaitWhileBusy();
	
	FlashWriteEnable();
	
   // Prepare command & address data
   ControlBuffer[0] = AT25DFxx_WRITE;
   ControlBuffer[1] = (Address & 0x00FF0000) >> 16;
   ControlBuffer[2] = (Address & 0x0000FF00) >>  8;
   ControlBuffer[3] = (Address & 0x000000FF) >>  0;
   
   pdca_channel_options_t PdcaOptionsTx = {
      .addr          = ControlBuffer,
      .size          = sizeof(ControlBuffer),
      .r_addr        = Buffer,
      .r_size        = Size,
      .pid           = AVR32_PDCA_PID_SPI1_TX,
      .transfer_size = AVR32_PDCA_BYTE,
   };
   
   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);

   FlashStatus = FLASH_ACTIVE;

   pdca_init_channel(FLASH_TX_DMA_CHANNEL, &PdcaOptionsTx);
   pdca_enable(FLASH_TX_DMA_CHANNEL);
   pdca_enable_interrupt_transfer_complete(FLASH_TX_DMA_CHANNEL);
   
   return YES;
}

dword FlashWrite(dword Address, byte *Buffer, word Size) {
   FlashWriteNoWait(Address, Buffer, Size);
   
   while(FlashStatus == FLASH_ACTIVE);
   
   return Size;
}

TYesNo FlashReadNoWait(dword Address, byte *Buffer, word Size)
// Read data from flash
{
   while(FlashStatus == FLASH_ACTIVE);

   FlashWaitWhileBusy();

   // Prepare command & address data
   ControlBuffer[0] = AT25DFxx_READ;
   ControlBuffer[1] = (Address & 0x00FF0000) >> 16;
   ControlBuffer[2] = (Address & 0x0000FF00) >>  8;
   ControlBuffer[3] = (Address & 0x000000FF) >>  0;

   // there can be one character left in SPI Rx register after last transmission
   spi_get(SPI_FLASH_DEVICE); 
   
   // Both DMA channels must be used - Rx will not initiate transfer
   pdca_channel_options_t PdcaOptionsRx = {
      .addr          = ControlBuffer,
      .size          = sizeof(ControlBuffer),
      .r_addr        = Buffer,
      .r_size        = Size,
      .pid           = AVR32_PDCA_PID_SPI1_RX,
      .transfer_size = AVR32_PDCA_BYTE,
   };
   
   pdca_channel_options_t PdcaOptionsTx = {
      .addr          = ControlBuffer,
      .size          = sizeof(ControlBuffer),
      .r_addr        = Buffer,
      .r_size        = Size,
      .pid           = AVR32_PDCA_PID_SPI1_TX,
      .transfer_size = AVR32_PDCA_BYTE,
   };
   
   spi_selectChip(SPI_FLASH_DEVICE, SPI_FLASH_NPCS);

   FlashStatus = FLASH_ACTIVE;

   pdca_init_channel(FLASH_RX_DMA_CHANNEL, &PdcaOptionsRx);
   pdca_init_channel(FLASH_TX_DMA_CHANNEL, &PdcaOptionsTx);
   pdca_enable(FLASH_TX_DMA_CHANNEL);
   pdca_enable_interrupt_transfer_complete(FLASH_RX_DMA_CHANNEL);
}

dword FlashRead(dword Address, byte *Buffer, word Size) {
   FlashReadNoWait(Address, Buffer, Size);

   while(FlashStatus == FLASH_ACTIVE);

   return Size;
}