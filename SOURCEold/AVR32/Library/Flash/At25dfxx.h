//*****************************************************************************
//
//   At25dfxx.h     Flash Memory Driver
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef At25dfxx_H_
#define At25dfxx_H_

#ifdef FLASH_AT25DF641A
   #define FLASH_SIZE                  8388608
   #define FLASH_PROGRAM_PAGE_SIZE     256
#else 
   #error "Unsupported FLASH type"
#endif

// Current status of FLASH
typedef enum {
   FLASH_IDLE,                // no activity
   FLASH_SLEEP,               // sleeping
   FLASH_ACTIVE               // Rx/Tx active
} EFlashStatus; 

void FlashInit(void);
void FlashErase4kb(dword Address);
void FlashErase32kb(dword Address);
void FlashErase64kb(dword Address);
void FlashEraseChip(void);
dword FlashWrite(dword Address, byte *Buffer, word Size);
TYesNo FlashWriteNoWait(dword Address, byte *Buffer, word Size);
dword FlashRead(dword Address, byte *Buffer, word Size);
TYesNo FlashReadNoWait(dword Address, byte *Buffer, word Size);
void FlashUnprotect(void);

#endif /* At45dbxx_H_ */