//*****************************************************************************
//
//    Hardware.h   Default hardware definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr32/io.h>        // CPU header

// Software framework :
#include "compiler.h"

// Project includes :
#include "Uni.h"
#include "Uc3.h"
#include <stddef.h>           // macro offsetof

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#define F_XTAL    12000000             // XTAL frequency 12.000 MHz

#define F_MCK     F_XTAL               // main clock
#define F_CPU     F_MCK                // system frequency
#define F_BUS_H   F_CPU                // high speed bus frequency
#define F_BUS_A   F_CPU                // peripheral bus A frequency
#define F_BUS_B   F_CPU                // peripheral bus B frequency

#define XTAL_STARTUP   AVR32_PM_OSCCTRL0_STARTUP_2048_RCOSC

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

#define WATCHDOG_INTERVAL 2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]

#define WATCHDOG_ENABLE 1              // Enable watchdog
//#define SLEEP_ENABLE    1              // Enable power save

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// Interrupt priority :
#define SYS_TIMER_PRIORITY   AVR32_INTC_INT0         // System timer IRQ

//-----------------------------------------------------------------------------
// SPI0
//-----------------------------------------------------------------------------
#define FRAM_FM25H20

#define SpiFramPortInit()       GpioFunction( AVR32_SPI0_MISO_0_0_PIN, AVR32_SPI0_MISO_0_0_FUNCTION);\
                             GpioFunction( AVR32_SPI0_MOSI_0_0_PIN, AVR32_SPI0_MOSI_0_0_FUNCTION);\
                             GpioFunction( AVR32_SPI0_SCK_0_0_PIN,  AVR32_SPI0_SCK_0_0_FUNCTION);\
                             GpioFunction( AVR32_SPI0_NPCS_0_0_PIN, AVR32_SPI0_NPCS_0_0_FUNCTION);

#define SPI_FRAM_MASTER_SPEED    F_BUS_A
#define SPI_FRAM_NPCS            0
#define SPI_FRAM_DEVICE          &AVR32_SPI0

#endif

//-----------------------------------------------------------------------------
// UART0
//-----------------------------------------------------------------------------

#define UART0_BAUD           57600       // baud rate
#define UART0_FORMAT         COM_8BITS  // default format
#define UART0_TIMEOUT        10         // intercharacter timeout [ms]
#define UART0_RXD_PIN        AVR32_USART0_RXD_0_0_PIN
#define UART0_RXD_FUNCTION   AVR32_USART0_RXD_0_0_FUNCTION
#define UART0_TXD_PIN        AVR32_USART0_TXD_0_0_PIN
#define UART0_TXD_FUNCTION   AVR32_USART0_TXD_0_0_FUNCTION

//-----------------------------------------------------------------------------
// UART1
//-----------------------------------------------------------------------------

#define UART1_BAUD           57600      // baud rate
#define UART1_FORMAT         COM_8BITS  // default format
#define UART1_TIMEOUT        1          // intercharacter timeout [ms]
#define UART1_RXD_PIN        AVR32_USART1_RXD_0_0_PIN
#define UART1_RXD_FUNCTION   AVR32_USART1_RXD_0_0_FUNCTION
#define UART1_TXD_PIN        AVR32_USART1_TXD_0_0_PIN
#define UART1_TXD_FUNCTION   AVR32_USART1_TXD_0_0_FUNCTION

//-----------------------------------------------------------------------------
// UART2
//-----------------------------------------------------------------------------

#define UART2_BAUD           57600       // baud rate
#define UART2_FORMAT         COM_8BITS  // default format
#define UART2_TIMEOUT        10         // intercharacter timeout [ms]
#define UART2_RXD_PIN        AVR32_USART2_RXD_0_0_PIN
#define UART2_RXD_FUNCTION   AVR32_USART2_RXD_0_0_FUNCTION
#define UART2_TXD_PIN        AVR32_USART2_TXD_0_0_PIN
#define UART2_TXD_FUNCTION   AVR32_USART2_TXD_0_0_FUNCTION

//-----------------------------------------------------------------------------
// UART3
//-----------------------------------------------------------------------------

#define UART3_BAUD           57600       // baud rate
#define UART3_FORMAT         COM_8BITS  // default format
#define UART3_TIMEOUT        10         // intercharacter timeout [ms]
#define UART3_RXD_PIN        AVR32_USART3_RXD_0_0_PIN
#define UART3_RXD_FUNCTION   AVR32_USART3_RXD_0_0_FUNCTION
#define UART3_TXD_PIN        AVR32_USART3_TXD_0_0_PIN
#define UART3_TXD_FUNCTION   AVR32_USART3_TXD_0_0_FUNCTION

//-----------------------------------------------------------------------------
// Console
//-----------------------------------------------------------------------------

#define UCONIO_COM1          1

#define StatusLED0  AVR32_PIN_PB05
#define StatusLED1  AVR32_PIN_PB06
#define StatusLED2  AVR32_PIN_PB07

#define StatusLedInit()      GpioOutput( StatusLED0); GpioOutput( StatusLED1); GpioOutput( StatusLED2)

#define StatusLed0On()       GpioClr( StatusLED0)
#define StatusLed0Tog()      GpioToggle( StatusLED0)
#define StatusLed0Off()      GpioSet( StatusLED0)

#define StatusLed1On()       GpioClr( StatusLED1)
#define StatusLed1Tog()      GpioToggle( StatusLED1)
#define StatusLed1Off()      GpioSet( StatusLED1)

#define StatusLed2On()       GpioClr( StatusLED2)
#define StatusLed2Tog()      GpioToggle( StatusLED2)
#define StatusLed2Off()      GpioSet( StatusLED2)