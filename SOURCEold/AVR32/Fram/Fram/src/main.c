//*****************************************************************************
//
//    main.c   FRAM test
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "Fm25hxx.h"
#include "uconio.h"

#define __COM1__
#include "Com.h"

/*
Z�pis cel� FRAM s DMA, pln� rychlost SPI
----------------------------------------
Buffer      Doba      Cykl�    Doba jednoho cyklu
256 b	    356 ms    1024     347 us
1024 b		302 ms    256      1180 us

Tj. re�ie jednoho DMA z�pisu je cca 65us


256 bytov� buffer, cel� FRAM, write & read
----------------------------
dma      1.47s
normal   2.87s


10 bajt�, write & read
----------------------
dma      145 us
normal   129 us


1 bajt, write & read
--------------------
normal   48 us
DMA      118us

*/

#define BUFFER_SIZE  256
volatile int i, j;
int main (void)
{
   byte Ch;
   TYesNo Error = NO;
   
   Byte BufferWrite[BUFFER_SIZE];
   Byte BufferRead[BUFFER_SIZE];
   
   CpuInit();
   WatchDogInit();
   InterruptEnable();
   
   UartInit( &AVR32_USART1);
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   
   uprintf("FM25HXX FRAM DEMO\n");
   
   FramInit();
   StatusLedInit();

   
   while(1) {
      if(ComRxWait(0)) {
		 Ch = 0;
         if(ComRxChar(&Ch)) {
            switch(Ch) {
		       case 'c':
			      uprintf("Compare test\n");
			      for(i = 0 ; i < BUFFER_SIZE ; i++) {
					  BufferWrite[i] = i;
				  }
				  
				  for(i = 0 ; i < (FRAM_SIZE / BUFFER_SIZE); i++) {
				     FramWrite(i * BUFFER_SIZE, BufferWrite, BUFFER_SIZE);
				  }
				  
				  for(i = 0 ; i < (FRAM_SIZE / BUFFER_SIZE); i++) {
				     FramRead(i * BUFFER_SIZE, BufferRead, BUFFER_SIZE);

					 for(j = 0 ; j < BUFFER_SIZE ; j++) {
					    if(BufferRead[j] != BufferWrite[j]) {
							uprintf("Compare failed at %d\n", i * BUFFER_SIZE + j);
							Error = YES;
							break;
						}
					 }
					 
					 if(Error == YES) {
					    break;
					 }
				  }

				  uprintf("Done\n");
			      break;  
				  
		       case 's':
			      uprintf("Compare test\n");
			      for(i = 0 ; i < BUFFER_SIZE ; i++) {
					  BufferWrite[i] = i;
				  }

				  FramWrite(0, BufferWrite, 1);

				  FramRead(0, BufferRead, 1);

			      for(j = 0 ; j < 1 ; j++) {
				     if(BufferRead[j] != BufferWrite[j]) {
					    uprintf("Compare failed at %d\n", i);
					    break;
		             }
		          }					

				  uprintf("Done\n");
			      break;  
		    }
		 }
	   }	
   }  
}