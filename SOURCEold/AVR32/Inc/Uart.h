//*****************************************************************************
//
//    Uart.h       RS232 communication services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Uart_H__
   #define __Uart_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __ComDef_H__
   #include "ComDef.h"
#endif

typedef volatile avr32_usart_t *TUart;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void UartInit( TUart Uart);
// Communication initialisation

void UartSetup( TUart Uart, unsigned Baud, unsigned Format, unsigned Timeout);
// Set communication parameters <Format> is TComFormat enum

TYesNo UartTxBusy( TUart Uart);
// Returns YES if transmitter is busy

void UartTxChar( TUart Uart, byte Char);
// Transmit <Char> byte

TYesNo UartRxChar( TUart Uart, byte *Char);
// Returns YES and received byte on <Char>,
// or NO on timeout

TYesNo UartRxWait( TUart Uart, unsigned Timeout);
// Waits for receive up to <Timeout> miliseconds

void UartFlushChars( TUart Uart);
// Skips all Rx chars up to intercharacter timeout

#endif
