//*****************************************************************************
//
//   SpiFlash.h   Flash memory SPI interface
//   Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __SpiFlash_H__
   #define __SpiFlash_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SpiInit( void);
// Initialise

void SpiAttach( void);
// Attach chipselect

void SpiRelease( void);
// Release chipselect

byte SpiReadByte( void);
// Returns byte

void SpiWriteByte( byte Value);
// Writes byte

#endif
