//******************************************************************************
//
//    Debug.h      Debugger utility
//    Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Debug_H__
   #define __Debug_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef __DEBUG__
   #include "uconio.h"           // UART console

   #define TRACE_INIT()
   #define TRACE( msg)      uprintf( "%s\n",       msg);
   #define TRACED( msg, n)  uprintf( "%s %d\n",    msg, n);
   #define TRACEX( msg, n)  uprintf( "%s %04X\n",  msg, n);
   #define TRACEXX( msg, n) uprintf( "%s %lX\n",   msg, n);
#else // nodebug
   #define TRACE_INIT()
   #define TRACE( msg)
   #define TRACED( msg, n)
   #define TRACEX( msg, n)
   #define TRACEXX( msg, n)
#endif

#endif
