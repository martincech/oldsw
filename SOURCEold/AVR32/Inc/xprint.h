//*****************************************************************************
//
//    xprint.h     Formatting basics template
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __xprint_H__
   #define __xprint_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __Fmt_H__
   #include "Fmt.h"
#endif

#ifndef __PutcharDef_H__
   #include "PutcharDef.h"
#endif

#include <stdarg.h>

//-----------------------------------------------------------------------------

void xputs( TPutchar *xputchar, const char *String);
// Print <String>

void xputsn(  TPutchar *xputchar, const char *String);
// Print <String> up to LF

void xprinthex( TPutchar *xputchar, dword x, unsigned Flags);
// Print hexadecimal number <x>, <Flags> is width

void xprintdec( TPutchar *xputchar, int32 x, unsigned Flags);
// Print decimal number <x> with <Flags>

void xfloat( TPutchar *xputchar, dword x, int Width, int Decimals, unsigned Flags);
// Print <x> as float with total <Width>, <Decimals> and additional <Flags>

void xprintf( TPutchar *xputchar, const char *Format, ...);
// Simple formatted output

void xvprintf( TPutchar *xputchar, const char *Format, va_list Arg);
// Simple formatted output

#endif
