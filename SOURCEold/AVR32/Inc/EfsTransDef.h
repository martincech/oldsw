#ifndef _EFS_TRANS_DEF_H_
#define _EFS_TRANS_DEF_H_



#define EFS_TRANS_TRANSACTION_TIMEOUT  1000000
#define EFS_TRANS_CHAR_TIMEOUT  15000



#define EFS_TRANS_SOF           0x19

// Packet
// | bSOF | wSize | -wSize | ...Data... | wCRC |

#define EFS_TRANS_SOF_OFFSET    0
#define EFS_TRANS_SIZE_OFFSET   1
#define EFS_TRANS_DATA_OFFSET   5








typedef struct {
   byte Sof;
   byte __dummy0__;
   word DataSize;
   word MinusDataSize;
   word Crc;
   byte Data[] __attribute__ ((aligned(sizeof(dword))));
} TEfsTransPacket;



#define EFS_TRANS_MAX_PACKET_LENGTH   600
#define EFS_TRANS_MAX_DATA_SIZE       EFS_TRANS_MAX_PACKET_LENGTH - sizeof(TEfsTransPacket)



static word CalculateCRC(byte *Data, byte Length) {
   word Crc = 0;

   while(Length--) {
      Crc += *Data;
      Data++;
   } 
   
   return -Crc;
}


#endif