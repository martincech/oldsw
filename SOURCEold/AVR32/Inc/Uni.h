//*****************************************************************************
//
//    Uni.h  - universal definitions (GNU ARM based)
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Uni_H__
   #define __Uni_H__

// exact size Intel-like data types :
typedef unsigned char      byte;
typedef unsigned short     word;
typedef unsigned int       dword;
typedef unsigned long long qword;

// exact size signed types :
typedef signed char        int8;       // signed byte
typedef short              int16;      // signed word
typedef int                int32;      // signed dword
typedef long long          int64;      // signed qword

// exact size unsigned types :
typedef unsigned char      uint8;      // byte
typedef unsigned short     uint16;     // word
typedef unsigned int       uint32;     // dword
typedef unsigned long long uint64;     // qword

// logical data types :
typedef dword         TYesNo;          // portable boolean

typedef enum {
   NO,
   YES
} TYesNoEnum;

// infinite loop :
#define forever     for(;;)

// mnemonic function aliases :
#define strequ( s1, s2)     !strcmp( s1, s2)
#define strnequ( s1, s2, n) !strncmp( s1, s2, n)
#define memequ( m1, m2, l)  !memcmp( m1, m2, l)

// string cut copy :
#define strncpyx( s1, s2, n) strncpy( s1, s2, n); s1[ n] = '\0';

// sizeof by type definition :
#define TSizeOf( type, item)   sizeof(((type *)0)->item)

// structure/union alignment :
#define __packed __attribute__ ((packed))

#endif
