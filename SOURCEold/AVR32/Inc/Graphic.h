//*****************************************************************************
//
//    Graphic.h     Graphic services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Graphic_H__
   #define __Graphic_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// maximal coordinate :
#define G_MAX_X          (G_WIDTH - 1)  // maximal coordinate X
#define G_MAX_Y          (G_HEIGHT - 1) // maximal coordinate Y

// drawing modes :
typedef enum {
   GMODE_NONE,                         // unused
   GMODE_REPLACE,                      // overwrite background
   GMODE_OR,                           // ORed with drawings
   GMODE_XOR,                          // XORed with drawings
   _GMODE_COUNT
} TGraphicMode;

// colors :
#if G_PLANES == 1

typedef enum {
   COLOR_WHITE,                        // white color (transparent background)
   COLOR_BLACK,                        // black color (active)
   _COLOR_COUNT
} TColor;

#elif G_PLANES == 2

typedef enum {
   COLOR_WHITE,                        // white color (transparent background)
   COLOR_LIGHTGRAY,                    // light gray
   COLOR_DARKGRAY,                     // dark gray
   COLOR_BLACK,                        // black color (active)
   _COLOR_COUNT
} TColor;

#else
   #error "Undefined G_PLANES count"
#endif

// patterns :
typedef enum {
   PATTERN_SOLID       = 0xFF,         // solid line
   PATTERN_DOTTED      = 0x55,         // dotted line
   PATTERN_DASHED      = 0x33,         // dashed line
   PATTERN_LONG_DASHED = 0x0F,         // long dashed line
   _PATTERN_COUNT
} TPattern;

// bitmap structure :
typedef struct {
   word Planes;                        // planes count
   word Width;                         // pixel width
   word Height;                        // pixel height
   byte Array[];                       // width * height bits
} TBitmap;

/* bitmap declaration :

   DefineBitmap( MyBitmap, 1, 10, 20)
      0x01, 0x02...
   EndBitmap()
*/   

#define DefineBitmap( name, p, w, h)   const TBitmap name = { p, w, h, {
#define EndBitmap()                    }};
   
//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void GInit( void);
// Initialisation

void GClear( void);
// Clear display

void GSetMode( int GraphicMode);
// Set drawing mode

void GSetColor( int Color);
// Set drawing color

void GSetPattern( int Pattern);
// Set drawing pattern

void GPixel( int x, int y);
// Draw pixel

void GLine( int x1, int y1, int x2, int y2);
// Draw line

void GBox( int x, int y, int width, int height);
// Draw filled box

void GBoxRound( int x, int y, int width, int height, int LeftRadius, int RightRadius);
// Draw filled box with rounded corners

void GRectangle( int x, int y, int width, int height);
// Draw empty rectangle

void GBitmap( int x, int y, const TBitmap *Bitmap);
// Draw bitmap at <x,y>

void GFlush( void);
// Copy buffer to display

void GRedraw( void);
// Redraw display from buffer

//------------------------------------------------------------------------------

void GSetFont( int FontIndex);
// Set working font

void GSetFixedPitch( void);
// Set nonproportional font

void GSetNumericPitch( void);
// Set nonproportional font with numeric spacing

void GSetNormalPitch( void);
// Enable proportional font

int GCharWidth( void);
// Returns maximal character width

int GNumberWidth( void);
// Returns maximal number width

int GCharHeight( void);
// Returns maximal character height

int GLetterWidth( int c);
// Returns letter width

int GTextWidth( const char *Text);
// Returns text width (pixels)

int GTextHeight( const char *Text);
// Returns text height (pixels)

void GTextAt( int x, int y);
// Left upper corner of the text

#ifndef __PUTCHAR__
   #define __PUTCHAR__
   
   int putchar( int c);
   // standard text output
#endif

#endif
