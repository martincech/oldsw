//*****************************************************************************
//
//    Rtc.h  -  Real time clock basic services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Rtc_H__
   #define __Rtc_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __DtDef_H__
   #include "DtDef.h"        // day of week constants
#endif

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

TYesNo RtcInit( void);
// Initialisation

//-----------------------------------------------------------------------------
// Utilities
//-----------------------------------------------------------------------------

TTimestamp RtcLoad( void);
// Returns RTC time

void RtcSave( TTimestamp Time);
// Set RTC time by <Time>

//-----------------------------------------------------------------------------
// Alarm
//-----------------------------------------------------------------------------

void RtcSetAlarm(byte HourBcd, byte MinuteBcd);
// Set alarm to <HourBcd>:<MinuteBcd>

TYesNo RtcCheckAlarm( void);
// Returns YES if alarm active (resets alarm also)

void RtcAlarmOff( void);
// Switch alarm off

#endif
