//******************************************************************************
//
//   cTime.h        Print time to console
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __cTime_H__
   #define __cTime_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DtDef_H__
   #include "DtDef.h"
#endif

void cTime( TTimestamp Now);
// Display time only

void cDate( TTimestamp Now);
// Display date only

void cDateTime( TTimestamp Now);
// Display date and time

#endif
