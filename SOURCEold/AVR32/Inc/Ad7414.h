//*****************************************************************************
//
//  Ad7414.h   AD7414 I2C thermometer services
//  Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __Ad7414_H__
   #define __Ad7414_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Trvani konverze :
#define TEMP_WAIT_CONVERSION 50	   // delka prevodu us


void TempStartConversion( void);
// Vysle prikaz na provedeni konverze

int16 TempRead( void);
// Vrati prectenou teplotu

int16 TempMeasure( void);
// Vrati teplotu * 10 C

#endif
