//*****************************************************************************
//
//    Twi.h        TWI services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Twi_H__
   #define __Twi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Chip internal address size :
#define TWI_ADDRESS_BYTE  1            // 8 bits  address
#define TWI_ADDRESS_WORD  2            // 16 bits address
#define TWI_ADDRESS_DWORD 3            // 24 bits address

// Rem : chip address is with R/~W bit inclusive

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void TwiInit( void);
// Initialisation

TYesNo TwiWriteByte( byte Chip, byte Data);
// Write <Data> to <Chip>

TYesNo TwiReadByte( byte Chip, byte *Data);
// Read <Data> from <Chip>

TYesNo TwiWrite( byte Chip, void *Data, int Size);
// Write <Data> with <Size> to <Chip>

TYesNo TwiRead( byte Chip, void *Data, int Size);
// Read <Data> with <Size> from <Chip>

TYesNo TwiWriteTo( byte Chip, unsigned Address, int AddressSize, void *Data, int Size);
// Write <Data> with <Size> to <Chip> at internal <Address>

TYesNo TwiReadFrom( byte Chip, unsigned Address, int AddressSize, void *Data, int Size);
// Write <Data> with <Size> to <Chip> from internal <Address>

#endif
