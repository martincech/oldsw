//*****************************************************************************
//
//    System.h     Operating system primitives
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __System_H__
   #define __System_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __DtDef_H__
   #include "DtDef.h"
#endif

#ifndef __Timer_H__
   #include "Timer.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

//-----------------------------------------------------------------------------
// Cooperative multitasking
//-----------------------------------------------------------------------------

void SysInit( void);
// Initialisation

int SysScheduler( void);
// Scheduler cycle, returns any event

int SysWaitEvent( void);
// Scheduler loop, returns nonempty event

void SysTickEnable( void);
// Enable timer interrupt

void SysTickDisable( void);
// Disable timer interrupt

//------------------------------------------------------------------------------
//   OS exception
//------------------------------------------------------------------------------

typedef enum {
   EXCEPTION_UNDEFINED,
} TExceptionType;

void SysException( int Type, int Address);
// CPU exception occured

//------------------------------------------------------------------------------
//   Inactivity Timeout
//------------------------------------------------------------------------------

void SysDisableTimeout( void);
// Disable timeout

void SysEnableTimeout( void);
// Enable timeout

void SysResetTimeout( void);
// Reset timeout

TYesNo SysIsTimeout( void);
// Returns YES if timeout occured

//------------------------------------------------------------------------------
//   System timer
//------------------------------------------------------------------------------

void SysStartTimer( void);
// Start system timer

void SysTimerExecute( void);
// External timer actions

TTimer SysTime( void);
// Returns milisecond timer

//------------------------------------------------------------------------------
//   Programmed delay
//------------------------------------------------------------------------------

void SysDelay( dword ms);
// Milisecond delay

void SysUDelay( dword us);
// Microsecond delay

void SysNDelay( dword ns);
// Nanosecond delay

//------------------------------------------------------------------------------
//   Clock
//------------------------------------------------------------------------------

void SysSetClock( TTimestamp Now);
// Set system time to <Now>

TTimestamp SysGetClock( void);
// Returns system date/time

TTimestamp SysGetClockNaked( void);
// Returns system date/time from interrupt

TYesNo Sys1sExpired( void);
// Returns YES after 1s expiration

//------------------------------------------------------------------------------
//   Flash timer
//------------------------------------------------------------------------------

TYesNo SysIsFlash1( void);
// Returns YES if flash 1 event occured

TYesNo SysIsFlash2( void);
// Returns YES if flash 2 event occured

void SysResetFlash( void);
// Reset flash events and start again

#ifdef __cplusplus
}
#endif

#endif
