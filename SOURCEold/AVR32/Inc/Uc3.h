//*****************************************************************************
//
//    Uc3.h        AVR32 UC3 CPU services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Uc3_H__
   #define __Uc3_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

//-----------------------------------------------------------------------------
// Intrinsics
//-----------------------------------------------------------------------------

#define _NOP()             __asm__ __volatile__ ( "nop")
#define _GLOBAL_DISABLE()  __asm__ __volatile__ ("ssrf\t%0" :  : "i" (AVR32_SR_GM_OFFSET))
#define _GLOBAL_ENABLE()   __asm__ __volatile__ ("csrf\t%0" :  : "i" (AVR32_SR_GM_OFFSET))

//-----------------------------------------------------------------------------
// GPIO
//-----------------------------------------------------------------------------

#define GpioSetRegister(  Gpio, Register)  AVR32_GPIO.port[ (Gpio) >> 5].Register =  1 << ((Gpio) & 0x1F)
#define GpioTestRegister( Gpio, Register) (AVR32_GPIO.port[ (Gpio) >> 5].Register & (1 << ((Gpio) & 0x1F)))

#define GpioInput( Gpio)     GpioSetRegister(  Gpio, gpers); GpioSetRegister( Gpio, oderc)
#define GpioOutput( Gpio)    GpioSetRegister(  Gpio, gpers); GpioSetRegister( Gpio, oders)

#define GpioPullup( Gpio)    GpioSetRegister(  Gpio, puers)
#define GpioNoPullup( Gpio)  GpioSetRegister(  Gpio, puerc)

#define GpioFunction( Gpio, Function) \
                           ((Function) & 0x01) ? (GpioSetRegister(  Gpio, pmr0s)) : (GpioSetRegister(  Gpio, pmr0c));\
                           ((Function) & 0x02) ? (GpioSetRegister(  Gpio, pmr1s)) : (GpioSetRegister(  Gpio, pmr1c));\
                             GpioSetRegister(  Gpio, gperc)

#define GpioSet( Gpio)       GpioSetRegister(  Gpio, ovrs)
#define GpioClr( Gpio)       GpioSetRegister(  Gpio, ovrc)
#define GpioToggle( Gpio)    GpioSetRegister(  Gpio, ovrt)
#define GpioGet( Gpio)       GpioTestRegister( Gpio, pvr)

//-----------------------------------------------------------------------------
// GPIO Bus
//-----------------------------------------------------------------------------

#define GpioSetBusRegister(  Gpio, Register, Mask)  AVR32_GPIO.port[ (Gpio) >> 5].Register =  (Mask) << ((Gpio) & 0x1F)
#define GpioTestBusRegister( Gpio, Register, Mask) (AVR32_GPIO.port[ (Gpio) >> 5].Register & ((Mask) << ((Gpio) & 0x1F)))

#define GpioBusEnable( Gpio, Mask)    GpioSetBusRegister( Gpio, gpers, Mask)
#define GpioBusInput( Gpio, Mask)     GpioSetBusRegister( Gpio, oderc, Mask)
#define GpioBusOutput( Gpio, Mask)    GpioSetBusRegister( Gpio, oders, Mask)

#define GpioBusPullup( Gpio, Mask)    GpioSetBusRegister(  Gpio, puers, Mask)
#define GpioBusNoPullup( Gpio, Mask)  GpioSetBusRegister(  Gpio, puerc, Mask)

#define GpioBusSet( Gpio, Mask)       GpioSetBusRegister(  Gpio, ovrs, Mask)
#define GpioBusClr( Gpio, Mask)       GpioSetBusRegister(  Gpio, ovrc, Mask)
#define GpioBusToggle( Gpio, Mask)    GpioSetBusRegister(  Gpio, ovrt, Mask)
#define GpioBusGet( Gpio, Mask)      (GpioTestBusRegister( Gpio, pvr,  Mask) >> ((Gpio) & 0x1F))

#endif
