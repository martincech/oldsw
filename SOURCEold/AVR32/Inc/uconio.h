//*****************************************************************************
//
//    uconio.h     UART simple formatted output
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __uconio_H__
   #define __uconio_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __Fmt_H__
   #include "Fmt.h"
#endif

// UART putchar definition :

int uputchar( int ch);
// output character

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void uputch( char ch);
// character output

void uputs( const char *string);
// string output

void uprinthex( dword x, dword flags);
// hexadecimal output

void uprintdec( int32 x, dword flags);
// decadic number output

void ufloat( int x, int w, int d, dword flags);
// floating number output

void uprintf( const char *Format, ...);
// simple formatted output

#endif
