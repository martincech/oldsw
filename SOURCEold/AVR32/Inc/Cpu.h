//*****************************************************************************
//
//    Cpu.h        CPU services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// interrupt function attribute :
#define __irq    __attribute__((__interrupt__))

// interrupt handler signature :
typedef void TIrqHandler( void);

#define CheckTrigger( counter, expression)  \
                     if( counter){          \
                        if( !--(counter)){  \
                            expression;     \
                        }                   \
                     }                      \

#ifdef SLEEP_ENABLE
   #define CpuIdleMode()    SLEEP( AVR32_PM_SMODE_FROZEN)
   // Enter IDLE mode
#else
   #define CpuIdleMode()
#endif

//-----------------------------------------------------------------------------
// Startup
//-----------------------------------------------------------------------------

void CpuInit( void);
// Cpu startup initialisation

//-----------------------------------------------------------------------------
// Interrupt controller
//-----------------------------------------------------------------------------

void CpuIrqAttach( int Irq, int Priority, TIrqHandler *Handler);
// Install IRQ <Handler> at <Irq> with <Priority> level

//-----------------------------------------------------------------------------
// Watch dog
//-----------------------------------------------------------------------------

void WatchDogInit( void);
// Initialize watchdog

void WatchDog( void);
// Refresh watchdog

//-----------------------------------------------------------------------------
// Interrupts
//-----------------------------------------------------------------------------

#define InterruptDisable()  _GLOBAL_DISABLE()              // Disable interrupts
#define InterruptEnable()   _GLOBAL_ENABLE()               // Enable interrupts

//-----------------------------------------------------------------------------
// Intrinsics
//-----------------------------------------------------------------------------

#define Nop()      _NOP()                                   // NOP instruction

#endif
