#ifndef _EFS_H_
#define _EFS_H_

#include "System.h"
#include "EfsDef.h"


void EfsInit(void);

byte EfsFileOpen(byte *FileName, byte Mode);
byte EfsFileClose(void);
byte EfsFileWrite(byte *Data, word Count, word *Written);
byte EfsFileSeek(dword Pos, byte Whence);
byte EfsFileCreate(byte *FileName, dword Size);
byte EfsFileDelete(byte *FileName);


#endif