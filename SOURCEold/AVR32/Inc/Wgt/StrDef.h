//******************************************************************************
//
//  StrDef.h       Text string definitions
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __StrDef_H__
   #define __StrDef_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#define _STR_LOW_LIMIT   (char *)0x40000000       // low resource limit (SRAM)
#define _STR_HIGH_LIMIT  (char *)0x80000000       // high resource limit (FLASH)


typedef const char *TUniStr;            // language independent string handle

#if LANGUAGE_COUNT > 1
   typedef const char const *TAllStrings[][ LANGUAGE_COUNT];    // dictionary
#else
   typedef const char const *TAllStrings[];                     // single list
#endif

extern  TAllStrings AllStrings;                  // global dictionary

const char *StrGet( TUniStr Str);
// Get translated string

void StrTrimRight( char *String);
// Remove right spaces

void StrSetWidth( char *String, int Width);
// Extend text with spaces, up to <Width>

#endif
