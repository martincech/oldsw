//*****************************************************************************
//
//    conio.h - simple display services
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __conio_H__
   #define __conio_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __Fmt_H__
   #include "Fmt.h"
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void ccursor( bool on);
// Je-li <on>=NO zhasne kurzor, jinak rozsviti

void cclear( void);
// Smaze display

void cgoto( int r, int c);
// Presune kurzor na zadanou textovou pozici <r> radek <c> sloupec

void cclreol( void);
// smaze az do konce radku, nemeni pozici kurzoru

#ifndef __PUTCHAR__
   #define __PUTCHAR__
   
   int putchar( int c);
   // standardni zapis znaku
#endif

//-----------------------------------------------------------------------------

void cputch( char ch);
// vystup znaku <ch>

void cputs( const char *string);
// vystup retezce <string>

void cputsn( const char *string);
// vystup retezce <string> az po LF

void cprinthex( dword x, dword flags);
// vystup hexa cisla <x>, <flags> koduje sirku

void cprintdec( int32 x, dword flags);
// vystup dekadickeho cisla <x>, <flags> koduje sirku

#define cbyte( x)  cprinthex( x, 2 | FMT_LEADING_0)
// Tiskne byte jako dve hexa cislice (pro BCD)

#define cword( x)  cprinthex( x, 4 | FMT_LEADING_0)
// Tiskne word jako ctyri hexa cislice (pro BCD)

#define cdword( x) cprinthex( x, 8 | FMT_LEADING_0)
// Tiskne dword jako osm hexa cislic (pro BCD)

#define cint8( x)   cprintdec( x, 4);
// Tiskne byte jako cislo se znamenkem

#define cint16( x)  cprintdec( x, 6);
// Tiskne int jako cislo se znamenkem

#define cint32( x)  cprintdec( x, 9);
// Tiskne long jako cislo se znamenkem
// (max 8 platnych cislic)

void cfloat( int x, int w, int d, dword flags);
// Tiskne binarni cislo <x> jako float s celkovou sirkou <w>
// a <d> desetinnymi misty. <flags> jsou doplnujici atributy
// (FMT_PLUS)

void cprintf( const char *Format, ...);
// jednoduchy formatovany vystup

void bprintf( char *Buffer, const char *Format, ...);
// formatovany vystup do <Bufferu>

#endif
