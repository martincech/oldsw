//*****************************************************************************
//
//    Kbd.h    Keyboard services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Kbd_H__
   #define __Kbd_H__

#ifndef __Hardware_H__
   #include "Hardware.h"               // keycodes
#endif

extern word _KbdCounter;              // timer counter

// timer interrupt handler "
#define KbdTrigger()   if( _KbdCounter){ \
                          --_KbdCounter; \
                       }

//------------------------------------------------------------------------------
//  Functions
//------------------------------------------------------------------------------

void KbdInit( void);
// Initialisation

int KbdPowerUpKey( void);
// Returns key holded after power up, or K_RELEASED

void KbdPowerUpRelease( void);
// Wait for power up key release

int KbdGet( void);
// Test for key, returns K_IDLE for no key pressed

void KbdDisable( void);
// Disable key, wait for release

//------------ standard input functions  :

TYesNo kbhit( void);
// Returns YES on key hit

int getch( void);
// Waits for key, returns it

#endif
