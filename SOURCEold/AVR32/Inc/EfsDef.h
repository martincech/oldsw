#ifndef _EFS_DEF_
#define _EFS_DEF_

#include "System.h"




typedef byte fFILE;
typedef byte fDIR;

#define  FOPEN_READ_ACCESS    0x01                                                                    // authorize the read access
#define  FOPEN_WRITE_ACCESS   0x02                                                                    // authorize the write access
#define  FOPEN_CLEAR_SIZE     0x04                                                                    // reset size
#define  FOPEN_CLEAR_PTR      0x08                                                                    // reset flux pointer
#define  FOPEN_MODE_R         (FOPEN_READ_ACCESS|FOPEN_CLEAR_PTR)                                     //!< R   access, flux pointer = 0, size not modify
#define  FOPEN_MODE_R_PLUS    (FOPEN_READ_ACCESS|FOPEN_WRITE_ACCESS|FOPEN_CLEAR_PTR)                  //!< R/W access, flux pointer = 0, size not modify
#define  FOPEN_MODE_W         (FOPEN_WRITE_ACCESS|FOPEN_CLEAR_PTR|FOPEN_CLEAR_SIZE)                   //!< W   access, flux pointer = 0, size = 0
#define  FOPEN_MODE_W_PLUS    (FOPEN_READ_ACCESS|FOPEN_WRITE_ACCESS|FOPEN_CLEAR_PTR|FOPEN_CLEAR_SIZE) //!< R/W access, flux pointer = 0, size = 0
#define  FOPEN_MODE_APPEND    (FOPEN_WRITE_ACCESS)    



#define  FS_SEEK_SET       0x00  //!< start at the beginning
#define  FS_SEEK_END       0x01  //!< start at the end of file and rewind
#define  FS_SEEK_CUR_RE    0x02  //!< start at the current position and rewind
#define  FS_SEEK_CUR_FW    0x03  //!< start at the current position and foward



#define EFS_CMD_DISK             0x01
#define EFS_CMD_DF               0x02
#define EFS_CMD_DIR              0x03
#define EFS_CMD_CD               0x04
#define EFS_CMD_FILEOPEN         0x05
#define EFS_CMD_FILECLOSE       0x06
#define EFS_CMD_FILEREAD        0x07
#define EFS_CMD_FILEWRITE        0x08
#define EFS_CMD_MKDIR            0x09
#define EFS_CMD_FILEDELETE            0x0A
#define EFS_CMD_RENAME           0x0B
#define EFS_CMD_FILESEEK        0x0C
#define EFS_CMD_FILECREATE            0x0D

#define EFS_CMD_DIR_IN     0x01
#define EFS_CMD_DIR_OUT    0x02





#define EFS_PATH_SIZE_MAX    100
#define EFS_DATA_SIZE_MAX    570



#define EFS_STAT_OK   0
#define EFS_STAT_ERR  1

#define __struct_packed  //  __packed

typedef struct __struct_packed {
   byte Code;
   byte Mode;
   byte FileName[];
} EfsCmdFileOpen;

typedef struct __struct_packed {
   byte Code;
} EfsCmdFileClose;

typedef struct __struct_packed {
   byte Code;
   byte __dummy0__;
   word Count;
   byte Data[];
} EfsCmdFileWrite;

typedef struct __struct_packed {
   byte Code;
   byte __dummy0__;
   byte __dummy1__;
   byte Whence;
   dword Pos;
} EfsCmdFileSeek;

typedef struct __struct_packed {
   byte Code;
   byte __dummy0__;
   byte __dummy1__;
   byte __dummy2__;
   dword Size;
   byte FileName[];
} EfsCmdFileCreate;

typedef struct __struct_packed {
   byte Code;
   byte FileName[];
} EfsCmdFileDelete;

typedef union {
   byte Cmd;
   EfsCmdFileOpen FileOpen;
   EfsCmdFileClose FileClose;
   EfsCmdFileWrite FileWrite;
   EfsCmdFileSeek FileSeek;
   EfsCmdFileCreate FileCreate;
   EfsCmdFileDelete FileDelete;
} EfsCmdUnion;





typedef struct __struct_packed {
	byte Reply;
} EfsReplyFileOpen;

typedef struct __struct_packed {
   byte Reply;
} EfsReplyFileClose;

typedef struct __struct_packed {
	byte Reply;
	word dummy1;
	byte dummy2;
	word Written;
} EfsReplyFileWrite;

typedef struct __struct_packed {
   byte Reply;
} EfsReplyFileSeek;

typedef struct __struct_packed {
   byte Reply;
} EfsReplyFileCreate;

typedef struct __struct_packed {
   byte Reply;
} EfsReplyFileDelete;

typedef union {
   byte Reply;
   EfsReplyFileOpen FileOpen;
   EfsReplyFileClose FileClose;
   EfsReplyFileWrite FileWrite;
   EfsReplyFileSeek FileSeek;
   EfsReplyFileCreate FileCreate;
   EfsReplyFileDelete FileDelete;
} EfsReplyUnion;

#endif