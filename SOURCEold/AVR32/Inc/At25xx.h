//*****************************************************************************
//
//   Flash.h     Atmel AT25DF641 Flash Memory Driver
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef _AT25XX_H_
#define _AT25XX_H_

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#if defined(__AT25DF641__)
   #define FLASH_PAGE_SIZE      256
   #define FLASH_PAGES          32768
   #define FLASH_SIGNATURE      
   #define FLASH_SIGNATURE_MASK 
#else
   #error "Unknown FLASH DATA device"
#endif


void FlashInit(void);

word FlashStatusRead(void);
void FlashWrite(dword Address, byte *Buffer, word Count);
void FlashRead(dword Address, byte *Buffer, word Count);
void FlashBlock4Erase(dword Address);
void FlashBlock32Erase(dword Address);
void FlashBlock64Erase(dword Address);
void FlashChipErase(void);

void FlashUnprotect(void);
TYesNo FlashIsProtected(void);




#endif /* _AT25XX_H_ */