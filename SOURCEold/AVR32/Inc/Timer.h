//*****************************************************************************
//
//    Timer.h      Timer calculations
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Timer_H__
   #define __Timer_H__

typedef dword TTimer;

// Time (dword) operations :
#define TimeAfter(     Actual, Requested)   ((long)(Requested)  - (long)(Actual)     <  0)
#define TimeBefore(    Actual, Requested)   ((long)(Actual)     - (long)(Requested)  <  0)
#define TimeAfterEq(   Actual, Requested)   ((long)(Actual)     - (long)(Requested)  >= 0)
#define TimeBeforeEq(  Actual, Requested)   ((long)(Requested)  - (long)(Actual)     >= 0)

#endif
