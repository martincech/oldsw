//******************************************************************************
//
//   Pwm.c          PWM controller
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef __Pwm_H__
   #define __Pwm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------

// PWM channels :
typedef enum {
   PWM_CHANNEL_0,
   PWM_CHANNEL_1,
   PWM_CHANNEL_2,
   PWM_CHANNEL_3,
   PWM_CHANNEL_4,
   PWM_CHANNEL_5,
   PWM_CHANNEL_6,
   _PWM_CHANNEL_COUNT
} TPwmChannel;

//-----------------------------------------------------------------------------

// PWM clock frequency :
#define PWM_FREQUENCY   F_MCK

// 100% PWM duty :
#define PWM_DUTY_MAX    0x100000U

// Period ticks by period [ns] :
#define PwmPeriodTicks( Period)        (uint32)(((uint64)(Period) * PWM_FREQUENCY) / 1000000000)

// Period ticks by frequency [Hz] :
#define PwmFrequencyTicks( Frequency)  (PWM_FREQUENCY / (Frequency))

//-----------------------------------------------------------------------------

void PwmInit( int Channel);
// Initialisation

void PwmStart( int Channel, unsigned PeriodTicks, unsigned Duty);
// Start PWM with <PeriodTicks> per period <Duty> cycle

void PwmDutySet( int Channel, unsigned Duty);
// Set PWM <Duty> cycle

void PwmStop( int Channel);
// Stop PWM

#endif
