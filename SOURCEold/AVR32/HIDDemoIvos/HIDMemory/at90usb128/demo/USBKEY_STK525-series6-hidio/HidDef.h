// HidDef.h

#ifndef __HidDef_H__
   #define __HidDef_H__

typedef enum {
   HID_FLASH_READ,
   _HID_LAST
} THidCommand;

typedef struct {
   Byte  Command;        // command
   DWord Address;        // memory address
   Word  Count;          // sectors count
} THidRequest;

#endif
