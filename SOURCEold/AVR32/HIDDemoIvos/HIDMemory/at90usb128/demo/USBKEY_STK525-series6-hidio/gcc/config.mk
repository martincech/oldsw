
# Project name
PROJECT = USBKEY_STK525-series6-hidio

# CPU architecture : {avr0|...|avr6}
# Parts : {at90usb646|at90usb647|at90usb1286|at90usb1287|at90usb162|at90usb82}
MCU = at90usb1287

# Source files
CSRCS = \
  ../../../../common/lib_mcu/wdt/wdt_drv.c\
  ../usb_descriptors.c\
  ../../../modules/usb/device_chap9/usb_device_task.c\
  ../../../lib_mcu/usb/usb_drv.c\
  ../../../lib_mcu/util/start_boot.c\
  ../main.c\
  ../usb_specific_request.c\
  ../../../modules/usb/device_chap9/usb_standard_request.c\
  ../../../modules/usb/usb_task.c\
  ../../../lib_board/usb_key/usb_key.c\
  ../hid_task.c\
  ../../../../common/modules/scheduler/scheduler.c\
  ../../../lib_mem/df/df_mem.c\
  ../../../lib_mem/df/df.c\
  ../../../../common/lib_mcu/uart/uart_lib.c\

# Assembler source files
ASSRCS = \

