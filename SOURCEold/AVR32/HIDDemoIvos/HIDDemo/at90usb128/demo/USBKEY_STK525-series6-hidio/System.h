//*****************************************************************************
//
//    System.h - "Operating system" primitives
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __System_H__
   #define __System_H__

#ifndef __Cpu_H__
   #include "Cpu.h"
#endif

//------------------------------------------------------------------------------
//   Funkce
//------------------------------------------------------------------------------

Byte SysWaitEvent( void);
// Wait for event

Byte SysYield( void);
// Switch context returns event

void SysFlushTimeout( void);
// Flush event timeout

void SysDisableTimeout( void);
// Disable event timeout

void SysEnableTimeout( void);
// Enable event timeout

void SysStartTimer( void);
// Start sytem timer

void SysRestartTimer( void);
// Restart system timer

void SysDelay( Word ms);
// Wait for <ms>

void SysUDelay( Byte us);
// Microsecond delay

DWord SysTime( void);
// Read system time

void SysSetTime( DWord Clock);
// Set system time

#endif
