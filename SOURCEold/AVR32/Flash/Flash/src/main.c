//*****************************************************************************
//
//    main.c   FRAM test
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "At25dfxx.h"
#include "uconio.h"

#define __COM1__
#include "Com.h"



#define BUFFER_SIZE  256

byte BufferWrite[BUFFER_SIZE];
byte BufferRead[BUFFER_SIZE];

static void PrepareBuffer(dword Block);

int main (void)
{
   byte Ch;
   TYesNo Error = NO;
   int i, j;

   
   CpuInit();
   WatchDogInit();
   InterruptEnable();
   
   UartInit( &AVR32_USART1);
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   
   uprintf("AT45DBxx FLASH DEMO\n");
   
   FlashInit();
   
   
   while(1) {
      if(ComRxWait(0)) {
		 Ch = 0;
         if(ComRxChar(&Ch)) {
            switch(Ch) {
		       case 'c':
			      uprintf("Compare test\n");
			      uprintf("Easing (up to 100 s)...\n");
				  
				  FlashEraseChip();

				  uprintf("Writing...\n");
				  
				  for(i = 0 ; i < (FLASH_SIZE / BUFFER_SIZE); i++) {
					 PrepareBuffer(i);
				     FlashWrite(i * BUFFER_SIZE, BufferWrite, BUFFER_SIZE);
				  }
				  
				  uprintf("Reading...\n");
				  
				  for(i = 0 ; i < (FLASH_SIZE / BUFFER_SIZE); i++) {
				     FlashRead(i * BUFFER_SIZE, BufferRead, BUFFER_SIZE);
                     
					 PrepareBuffer(i);
					 
					 for(j = 0 ; j < BUFFER_SIZE ; j++) {
					    if(BufferRead[j] != BufferWrite[j]) {
							uprintf("Compare failed at %d\n", i * BUFFER_SIZE + j);
							Error = YES;
							break;
						}
					 }
					 
					 if(Error == YES) {
					    break;
					 }
				  }

				  uprintf("Done\n");
			      break;  
		    }
		 }
	   }	
   }  
}


static void PrepareBuffer(dword Block) {
   int i;
   
   for(i = 0 ; i < BUFFER_SIZE ; i++) {
      BufferWrite[i] = i + Block;
   }
}