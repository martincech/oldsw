/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * AVR Software Framework (ASF).
 */
#include <asf.h>
#include "sleepmgr.h";
#include "sysclk.h";
#include "udc.h";

#include <string.h>

#include "Hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"    // UART printf

#define __COM1__
#include "Com.h"

byte OutBuffer[ UDI_HID_REPORT_OUT_SIZE];
byte InBuffer[ UDI_HID_REPORT_IN_SIZE];

volatile int ReportReceived = 0;

#define MULTIPLE_BURST_SIZE  1000         // copy by firmware !

static void PrepareBuffer( int n);
// Prepare out buffer

static void CompareBuffer( int n);
// Compare buffer

#define MODE_MULTIPLE_REPLY   'm'
#define MODE_MULTIPLE_QUERY   'q'
#define MODE_ECHO             'e'
#define FLUSH                 'f'

#define HELP  "\r\n" \
              "m - Multiple reply (read)\r\n" \
              "q - Multiple query (write)\r\n" \
			  "e - Echo\r\n" \
			  "f - Flush\r\n\r\n" \

int main (void)
{
	byte ch, mode;
	int ReportCount = 0;
	int i;
	  CpuInit();
      WatchDogInit();
	  //SysStartTimer();
   UartInit( &AVR32_USART1);
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   
   
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();

	sysclk_init();

	// Start USB stack to authorize VBus monitoring
	udc_start();


   PrepareBuffer(0);
   
   mode = MODE_MULTIPLE_REPLY;

   while (true) {
	   /* jakmile je nastaven nenulov� Rx timeout Uartu, USB nep�ijme ani jeden report
	     
	   */
      if(ComRxWait(0)) {
	      if(ComRxChar(&ch)) { 
			if(ch == FLUSH) { // do flush
				ReportCount = 0;
				while(udi_hid_generic_report_in_busy());
				uprintf("Flushed\r\n");
			} else {
	           switch(ch) { // switch mode
		          case MODE_MULTIPLE_REPLY:
				  case MODE_MULTIPLE_QUERY:
				  case MODE_ECHO:
			         mode = ch;
		             uprintf("Mode switched: %c\r\n", mode);
					 break;
					 
				  default:
				     uprintf(HELP);
		             break;
			   }
			}
	      } 
	   }
	   
	   if(ReportReceived) {
	      switch(mode) {
		      case MODE_MULTIPLE_REPLY:
                  for(i = 0 ; i < MULTIPLE_BURST_SIZE ; i++) {
			         InBuffer[0] = i;
			
			         while(udi_hid_generic_report_in_busy());
			
                     udi_hid_generic_send_report_in(InBuffer);
                  }

		          break;
		   
		      case MODE_MULTIPLE_QUERY:
			      
				  InBuffer[0] = ReportCount;
				  CompareBuffer( ReportCount);

			      ReportCount++;

	              if(ReportCount == MULTIPLE_BURST_SIZE) {
	                  PrepareBuffer(0);
					  
					  while(udi_hid_generic_report_in_busy());
			
                      if(udi_hid_generic_send_report_in(InBuffer)) {
                         //uprintf("SEND");
                      } else {
                         uprintf("ERROR");
                      }
			
		              ReportCount = 0;
	              }
				  
	              break;
				  
			case MODE_ECHO: //echo
			      while(udi_hid_generic_report_in_busy());
			
                  if(udi_hid_generic_send_report_in(OutBuffer)) {
                     //uprintf("SEND");
                  } else {
                     uprintf("ERROR");
                  }

	              break;
				  
			default:
			   break;
	      }
		  
		  ReportReceived = 0;
      }
   }
}

static void CompareBuffer( int n)
// Compare in/out buffer
{
int i;

   for( i = 0; i < UDI_HID_REPORT_OUT_SIZE; i++){
      if( InBuffer[ i] != OutBuffer[ i]){
         uprintf( "COMPARE %d failed at offset %d value out %d in %d\n", n, i, OutBuffer[ i], InBuffer[ i]);
      }
   }
} // CompareBuffer


U8 HidEnable() {
   uprintf("HID ENABLED\r\n");
   return 1;
}

void HidDisable() {
   uprintf("HID DISABLED\r\n");
}

void HidReportOut(U8 *buffer) {
   int i = 0;

   for(i = 0 ; i < UDI_HID_REPORT_OUT_SIZE ; i++) {
	   OutBuffer[i] = buffer[i];
   }

   ReportReceived = 1;
}

void HidPlugged(U8 b_vbus_high) {
   if (b_vbus_high) {
	  uprintf("PLUGGED\r\n");
      udc_attach();
   } else {
	  uprintf("UNPLUGGED\r\n");
      udc_detach();
   }
}

static void PrepareBuffer( int n)
// Prepare out buffer
{
int i;

   InBuffer[ 0] = n;
   for( i = 1; i < UDI_HID_REPORT_IN_SIZE; i++){
      InBuffer[ i] = i;
   }
} // PrepareBuffer