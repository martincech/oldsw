Ke zprovozn�n� Generic HID je t�eba:

1. Konfigurovat hodiny (conf_clock.h)
2. Nadefinovat u�ivatelsk� funkce, kter� budou vol�ny p�i ud�lostech
   (p�ipojen�/odpojen� USB, dokon�en� enumerace, p�ijet� OUT reportu). V conf_usb.h


�pravy p�vodn�ho k�du:

1. Generic HID m� pevn� definovan� interval report� 20 ms (usb_hid_generic.h).
   Nahradil jsem jej UDI_HID_REPORT_IN_INTERVAL a UDI_HID_REPORT_OUT_INTERVAL, kter� se definuj� v conf_usb.h
2. Do udi_hid_generic.c (.h) jsem p�idal bool udi_hid_generic_report_in_busy(); pro zji��ov�n�,
   zda lze poslat dal�� IN report (resp. zde je dokon�eno odes�l�n� p�edchoz�ho IN reportu).