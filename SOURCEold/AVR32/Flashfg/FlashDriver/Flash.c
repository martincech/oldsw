//*****************************************************************************
//
//   Flash.c     Flash Memory Driver
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "System.h"
#include "Flash.h"
#include "Spi.h"


// Flash commands
#define		FLASH_READ	0x1B

#define		FLASH_BLOCK_ERASE4	0x20
#define		FLASH_BLOCK_ERASE32	0x52
#define		FLASH_BLOCK_ERASE64	0xD8
#define		FLASH_CHIP_ERASE	0x60

#define		FLASH_READ_ID		0x9F
#define		FLASH_READ_STATUS	0x05
#define		FLASH_WRITE_STATUS1	0x01
#define		FLASH_WRITE_STATUS2	0x31

#define		FLASH_WRITE				0x02
#define		FLASH_WRITE_ENABLE		0x06
#define		FLASH_WRITE_DISABLE		0x04

#define		FLASH_PROTECT_SECTOR	0x36
#define		FLASH_UNPROTECT_SECTOR	0x39



static word FlashStatusRead(void);
static TYesNo FlashIsBusy(void);
static void FlashWaitWhileBusy(word timeout);
static byte FlashIDRead(void);
static void FlashWriteEnable(void);
static void FlashWriteDisable(void);
static void FlashWriteStatus(byte ch);

static word FlashStatusRead(void)
// Read flash memory status register
{
	word temp = 0;
	
	SpiSelect();
	SpiByteWrite(FLASH_READ_STATUS);
	temp = (byte) SpiByteRead();
	temp |= (SpiByteRead() << 8);
	SpiRelease();
	
	return temp;
}

static TYesNo FlashIsBusy(void)
// Test whether flash memory is busy
{
	if(FlashStatusRead() & 0x0001) {
		return YES;
	} else {
		return NO;
	}
}

static void FlashWaitWhileBusy(word timeout) // dod�lat timeout...
// Wait till flash memory is idle
{
	SpiSelect();
	SpiByteWrite(FLASH_READ_STATUS);
	
	while(SpiByteRead() & 0x0001);
	
	SpiRelease();
}

static void FlashWriteEnable(void)
// Enable write latch
{ 
	SpiSelect();
	SpiByteWrite(FLASH_WRITE_ENABLE);
	SpiRelease();
} 

static void FlashWriteDisable(void)
// Disable write latch
{ 
	SpiSelect();
	SpiByteWrite(FLASH_WRITE_DISABLE);
	SpiRelease();
} 

static void FlashWriteStatus(byte ch)
// Write flash memory status register - byte 1
{
	FlashWaitWhileBusy(0);
	
	FlashWriteEnable();
	
	SpiSelect();
	SpiByteWrite(FLASH_WRITE_STATUS1);
	SpiByteWrite(ch);
	SpiRelease();
}

static byte FlashIDRead(void)
// Read flash Family Code and Density Code
{
	byte temp;
	
	SpiSelect();
	SpiByteWrite(FLASH_READ_ID);
	SpiByteRead();
	temp = SpiByteRead();
	SpiRelease();
	
	return temp;
}








TYesNo FlashIsProtected(void)
// Test whether some sectors are protected
{
	if(FlashStatusRead() & 0x0C) {
		return YES;
	} else {
		return NO;
	}
}

void FlashUnprotect(void)
// Globally unprotect flash memory
{
	FlashWriteStatus(0);
}


void FlashInit(void)
// Flash memory init
{
	SpiInit();
	
	FlashPortInit();
	FlashUnhold();
	FlashWriteUnprotect();
}

void FlashErase(dword address)
// Erase one 4-kb block specified by address
{
	FlashWaitWhileBusy(0);
	
	FlashWriteEnable();
	
	SpiSelect();
	SpiByteWrite(FLASH_BLOCK_ERASE4);
	SpiByteWrite((byte) (address >> 16));
	SpiByteWrite((byte) (address >> 8 ));
	SpiByteWrite((byte) (address >> 0 ));
	SpiRelease();
}

void FlashWrite(dword address, char *buffer, word count)
// Write data to flash
{
	word i;
	
	if(count > 256) {
		count = 256;
	}
	
	FlashWaitWhileBusy(0);
	
	FlashWriteEnable();
	
	SpiSelect();
	SpiByteWrite(FLASH_WRITE);
	SpiByteWrite((byte) (address >> 16));
	SpiByteWrite((byte) (address >> 8 ));
	SpiByteWrite((byte) (address >> 0 ));
	
	for(i = 0 ; i < count ; i++) {
		SpiByteWrite(buffer[i]);
	}
	
	SpiRelease();
}

void FlashRead(dword address, char *buffer, word count)
// Read data from flash
{
	word i;

	FlashWaitWhileBusy(0);

	SpiSelect();
	SpiByteWrite(FLASH_READ);
	SpiByteWrite((byte) (address >> 16));
	SpiByteWrite((byte) (address >> 8 ));
	SpiByteWrite((byte) (address >> 0 ));
	SpiByteWrite(0xFF);
	SpiByteWrite(0xFF);
	
	for(i = 0 ; i < count ; i++) {
		buffer[i] = SpiByteRead();
	}
	
	SpiRelease();
}