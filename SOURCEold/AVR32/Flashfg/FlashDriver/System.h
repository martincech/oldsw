//*****************************************************************************
//
//    System.h - "Operating system" primitives
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __System_H__
   #define __System_H__

#ifndef __Cpu_H__
   #include "Cpu.h"
#endif

//------------------------------------------------------------------------------
//   Funkce
//------------------------------------------------------------------------------

byte SysWaitEvent( void);
// Wait for event

byte SysYield( void);
// Switch context returns event

void SysFlushTimeout( void);
// Flush event timeout

void SysDisableTimeout( void);
// Disable event timeout

void SysEnableTimeout( void);
// Enable event timeout

void SysStartTimer( void);
// Start sytem timer

void SysRestartTimer( void);
// Restart system timer

void SysDelay( word ms);
// Wait for <ms>

void SysUDelay( byte us);
// Microsecond delay

dword SysTime( void);
// Read system time

void SysSetTime( dword Clock);
// Set system time

#define SysDisableTimer()  TIMSK0 &= ~(1 << OCIE0A)
// Disable system timer

#define SysEnableTimer()   TIMSK0 |=  (1 << OCIE0A)
// Enable system timer

#endif
