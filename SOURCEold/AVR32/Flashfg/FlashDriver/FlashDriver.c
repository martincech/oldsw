//*****************************************************************************
//
//   FlashDriver.c     FlashDriver test application
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************


#include <avr/io.h>
#include "Hardware.h"
#include "Cpu.h"
#include "Uart0.h"
#include "Cstdio.h"
#include "Flash.h"
#include <stdio.h>

int main(void)
{
	byte i;           // promenna cyklu (max. 255)
	byte ch;
	char buffer[5] = "ABCD";
	
	Uart0Init();
	
	// inicializace flash
	FlashInit();
	
	if(FlashIsProtected() == YES) {
		FlashUnprotect();
	}
	
	// smazani prvniho bloku
	FlashErase(0);
	
	// zapis
	FlashWrite(0, buffer, 4);
	
	buffer[0] = '0';
	buffer[1] = '0';
	buffer[2] = '0';
	buffer[3] = '0';
	
	// cteni
	FlashRead(0, buffer, 4);
	

	Uart0TxChar(buffer[0]);
	Uart0TxChar(buffer[1]);
	Uart0TxChar(buffer[2]);
	Uart0TxChar(buffer[3]);
	Uart0TxChar('\r');
	Uart0TxChar('\n');



   // inicializace portu :
   StatusLedInit();                    // nastaveni smeru portu pro LED
   StatusLedOff();                     // zhasnuti LED
   CalButtonInit();                    // nastaveni smeru portu pro Kalibracni tlacitko

   // 5x blikni LED 
   //>>> Pozn. : (tuto variantu cykly prekladac lepe optimalizuje,
   //>>> pokud neni potreba sledovat i je lepe pouzit tuto konstrukci :
   i = 5;                              // pocet cyklu
   do {
      // i = 5..1
      StatusLedOn();                   // rozsviceni LED
      SysDelay( 200);                  // cekani 200ms
      StatusLedOff();                  // zhasnuti LED
      SysDelay( 200);                  // cekani 200ms
   } while( --i);
   
    while(1)
    {
      // po stisknuti tlacitka poslat text :
      if( CalButtonPressed()){
         SysDelay( 50);                // fitrace zakmitu
         if( CalButtonPressed()){
            cprintf( "Button\n");      // opravdu stisknut
         } // else jen zakmit
      } // else neni stisknuto

      // prijmout znak z UART0
      if( !Uart0RxChar( &ch)){
         continue;                     // zadny znak nebyl prijat skok na zacatek cyklu (forever)
      }
      Uart0TxChar( ch);                // echo prijateho znaku - zpatky do terminalu

      //>>> prikaz switch :
      switch( ch){
         //>>> sem skoci po prijeti pismene z :
         case 'z' :
            // zhasni LED
            StatusLedOff();
			SpiSelect();
            break;  // skoci za konec prikazu switch

         //>>> sem skoci po prijeti pismene r :
         case 'r' :
            // rozsvit LED
            StatusLedOn();
			SpiRelease();
            break;


         //>>> jeden blok muze chytat i vice hodnot :
         case '\r' :  // CR
         case '\n' :  // LF
            break;  // ignoruj <CR> a <LF>

         //>>> vsechny zbyvajici moznosti konci zde :
         default :
            cprintf( "\nz - zhasni LED\n");   // napred odradkuj a potom napis 'z - zhasni LED'
            cprintf( "r - rozsvit LED\n");
            cprintf( "f - funkce\n");
            break;
      } // switch
    }
}