//*****************************************************************************
//
//   Flash.h     Flash Memory Driver
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef FLASH_H_
#define FLASH_H_

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void FlashInit(void);
void FlashErase(dword address);
void FlashWrite(dword address, char *buffer, word count);
void FlashRead(dword address, char *buffer, word count);
void FlashUnprotect(void);
TYesNo FlashIsProtected(void);

#endif /* FLASH_H_ */