//*****************************************************************************
//
//    CStdio.h  - Standard input/output via UART
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __CStdio_H__
   #define __CStdio_H__

#ifndef _STDIO_H_
   #include <stdio.h>
#endif

// stdio file descriptor :
extern FILE StdioStream;

#define StdioInit()   stdout = &StdioStream
// initialize

#define cprintf( Format, ...) printf_P( PSTR( Format), ## __VA_ARGS__)
// print with <Format> from code memory

#endif
