//*****************************************************************************
//
//   Spi.h       SPI interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __Spi_H__
   #define __Spi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SpiInit( void);
// Initialize bus

byte SpiByteRead( void);
// Read byte from SPI

void SpiByteWrite( byte Value);
// Write byte to SPI

byte SpiByteTransceive( byte TxValue);
// Write/read SPI. Returns value read

#endif
