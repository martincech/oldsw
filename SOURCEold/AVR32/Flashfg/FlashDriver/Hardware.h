//*****************************************************************************
//
//    Hardware.h  - Bat2 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../../Inc/cpu.h"
#include "../../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED   7

#define StatusLedInit()  DDRD  |=  (1 << StatusLED)

#define StatusLedOn()    PORTD |=  (1 << StatusLED) 
#define StatusLedOff()   PORTD &= ~(1 << StatusLED)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           9600       // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM0

//------------------------------------------------------------------------------
// SPI
//------------------------------------------------------------------------------

//#define SPI_CLOCK			FXTAL / 64
#define SPI_CPOL			0
#define SPI_CPHA			0
#define SPI_CLOCK_DIVISOR	3
#define SPI_CLOCK_X2		0


#define SpiSSPin	0	
#define SpiSckPin	1	
#define SpiMosiPin	2	
#define SpiMisoPin	3	

#define SpiRelease()		PORTB |=  (1 << SpiSSPin)
#define SpiSelect()			PORTB &= ~(1 << SpiSSPin)

/*	MISO - IN
	others - OUT
*/
#define SpiPortInit()		DDRB &= ~(1 << SpiMisoPin);\
							DDRB |= (1 << SpiSSPin) | (1 << SpiSckPin) | (1 << SpiMosiPin)
			


#define FlashHoldPin   5			
														
#define FlashHold()			PORTB &= ~(1 << FlashHoldPin)
#define FlashUnhold()		PORTB |= (1 << FlashHoldPin)


#define FlashWPPin   4

#define FlashWriteProtect()		PORTB &= ~(1 << FlashWPPin)
#define FlashWriteUnprotect()	PORTB |= (1 << FlashWPPin)


#define FlashPortInit()		DDRB |= (1 << FlashHoldPin) | (1 << FlashWPPin)
							

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   6

#define CalButtonInit()     DDRA  &= ~(1 << CalButton);\
                            PORTA |=  (1 << CalButton)         // enable pullup

#define CalButtonPressed()  (!(PINA & (1 << CalButton)))       // active L


//-----------------------------------------------------------------------------

#endif
