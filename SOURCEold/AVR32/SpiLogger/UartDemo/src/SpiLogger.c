#include "System.h"

#include "SpiLogger.h"

#include "Hardware.h"
#include "SpiExt.h"

#include "uconio.h"

#define EFS_TRANS_MAX_PACKET_LENGTH 4000

byte EfsTransRxBuffer[EFS_TRANS_MAX_PACKET_LENGTH];
volatile TYesNo EfsTransDataReady = NO;
volatile word BytesReceived = 0;


void EfsTransReceive(void) {
   SpiRx(SPI_DEVICE, EfsTransRxBuffer + BytesReceived, EFS_TRANS_MAX_PACKET_LENGTH - BytesReceived);
}

void EfsTransDataReadyCallback(void) {
   BytesReceived += SpiGetRxCount();

   if(BytesReceived) {
	   BytesReceived = BytesReceived;
   }

   EfsTransReceive();
}

word EfsTransGetDataSize(void) {
   return BytesReceived;
}

byte* EfsTransGetData(void) {
   return EfsTransRxBuffer;
}







void EfsTransInit(void)
{
   SpiPortInit();
   spi_initSlave( SPI_DEVICE, 8, 1);
   
   spi_enable( SPI_DEVICE);


// callbacky vy�e�it pomoc� defin� !!!
   SpiRegisterRxCallback(&EfsTransDataReadyCallback);
   
   //SpiEnableCSInt(SPI_DEVICE);

   Enable_global_interrupt();
}