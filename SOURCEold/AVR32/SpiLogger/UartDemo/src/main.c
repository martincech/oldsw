//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "Hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "uconio.h"    // UART printf

#define __COM1__
#include "Com.h"

#include "SpiLogger.h"


int main (void)
{
   byte ch;

   CpuInit();
   WatchDogInit();

   EfsTransInit();
   EfsTransReceive();
   
   UartInit( &AVR32_USART1);
   ComSetup( UART1_BAUD, UART1_FORMAT, UART1_TIMEOUT);
   uputs( "Start...\n");
   
   while(1) {
	  if(ComRxChar(&ch)) {
		 switch(ch) {
		    case 's':
		       break;
		 }
	  }	 
   }  
}