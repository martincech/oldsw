//*****************************************************************************
//
//    main.c   Master
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "Uart.h"
#include "UartNative.h"

#include <string.h>

#define BUFFER_SIZE  128
volatile int a;

int main (void)
{
   byte Buffer[BUFFER_SIZE];
   byte i;
   TYesNo Done;
   
   CpuInit();
   WatchDogInit();
   InterruptEnable();
   cpu_irq_enable();

   #define MY_UART   UART_UART0  
   #define MY_DEBUG_UART   UART_UART1

   UartInit( MY_DEBUG_UART);
   UartSetup( MY_DEBUG_UART, 57600, UART_8BIT);
   UartTimeoutSet( MY_DEBUG_UART, 1000, 10);
   UartModeSet( MY_DEBUG_UART, UART_MODE_BINARY_SLAVE);
   
   UartInit( MY_UART);
   UartSetup( MY_UART, 57600, UART_8BIT);
   UartTimeoutSet( MY_UART, 1000, UART_TIMEOUT_OFF);
   UartModeSet( MY_UART, UART_MODE_NATIVE_MASTER);
   
   
   
   forever {
	  /*
	  1 master + 4 slave (adresy 0, 1, 2, 3) + 1 ru�i�ka
	  master se dotazuje ka�d�ho ze slave
	  */
      for(i = 0 ; i < 4 ; i++) {
		 SysDelay(100);
		  
		 Buffer[0] = i + '0';
		 UartSend(MY_DEBUG_UART, Buffer, 1);
         
		 UartNativeWrite( MY_UART, i, "A", 1);
         UartReceive( MY_UART, Buffer, BUFFER_SIZE);
         
		 Done = NO;
		 
         while(!Done) {
            switch( UartReceiveStatus( MY_UART)){
		       case UART_RECEIVE_ACTIVE:
                  break;
			   
               case UART_RECEIVE_FRAME :
			      UartSend(MY_DEBUG_UART, " OK\r\n", 5);
				  Done = YES;
                  break;
            
               default : // timeout
			      UartSend(MY_DEBUG_UART, " --\r\n", 5);
				  Done = YES;
                  break;
	        }            
         }
      }
   }
}