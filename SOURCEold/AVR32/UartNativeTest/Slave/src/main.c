//*****************************************************************************
//
//    main.c   Slave
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system
#include "Uart.h"
#include "UartNative.h"

#include <string.h>

#define BUFFER_SIZE  128

int main (void)
{
   byte Buffer[BUFFER_SIZE];
   byte TxBuffer[BUFFER_SIZE];
   word RxSize;

   CpuInit();
   WatchDogInit();
   InterruptEnable();
   cpu_irq_enable();
   NativeAddressInit();

   #define MY_UART   UART_UART0

   UartInit( MY_UART);
   UartSetup( MY_UART, 57600, UART_8BIT);
   UartTimeoutSet( MY_UART, 1000, 10);
   UartModeSet( MY_UART, UART_MODE_NATIVE_SLAVE);
   UartAssignSlaveAddress(MY_UART, NativeAddressGet());
   
   UartReceive( MY_UART, Buffer, BUFFER_SIZE);
   
   forever {
      switch( UartReceiveStatus( MY_UART)){
		 case UART_RECEIVE_ACTIVE:
            break;
			   
         case UART_RECEIVE_FRAME :
               RxSize = UartReceiveSize( MY_UART);

			   UartSend( MY_UART, TxBuffer, RxSize); // echo
               UartReceive( MY_UART, Buffer, BUFFER_SIZE); // slave m� po uko�en� odes�l�n� automaticky obnovit p��jem...
            break;
            
         default : // timeout
            UartReceive( MY_UART, Buffer, BUFFER_SIZE);
            break;
	  }            
   }
}