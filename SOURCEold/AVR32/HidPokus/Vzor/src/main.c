//*****************************************************************************
//
//    main.c   Application template
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#include "hardware.h"
#include "Cpu.h"       // CPU startup
#include "System.h"    // Operating system

#include "config.h"
#include "modules/scheduler/scheduler.h"
#include "lib_mcu/wdt/wdt_drv.h"
#include "lib_mcu/power/power_drv.h"

int main (void)
{
   CpuInit();
   WatchDogInit();
   
   //start_boot_if_required();
   wdtdrv_disable();
   Clear_prescaler();
   
   
   usb_task_init();
   hid_task_init();
   
   while(1) {
      usb_task();
      hid_task();
   }  
}



//! \name Procedure to speed up the startup code
//! This one increment the CPU clock before RAM initialisation
//! @{
#ifdef  __GNUC__
// Locate low level init function before RAM init (init3 section)
// and remove std prologue/epilogue
char __low_level_init(void) __attribute__ ((section (".init3"),naked));
#endif

#ifdef __cplusplus
extern "C" {
#endif
char __low_level_init()
{
  Clear_prescaler();
  return 1;
}
#ifdef __cplusplus
}
#endif
//! @}