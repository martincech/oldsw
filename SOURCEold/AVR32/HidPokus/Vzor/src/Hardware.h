//*****************************************************************************
//
//    Hardware.h   Default hardware definitions
//    Version 1.0  (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr32/io.h>        // CPU header

// Software framework :
#include "compiler.h"

// Project includes :
#include "Uni.h"
#include "Uc3.h"
#include <stddef.h>           // macro offsetof

//------------------------------------------------------------------------------
//   CPU parameters
//------------------------------------------------------------------------------

#define F_XTAL    12000000             // XTAL frequency 12.000 MHz

#define F_MCK     F_XTAL               // main clock
#define F_CPU     F_MCK                // system frequency
#define F_BUS_H   F_CPU                // high speed bus frequency
#define F_BUS_A   F_CPU                // peripheral bus A frequency
#define F_BUS_B   F_CPU                // peripheral bus B frequency

#define XTAL_STARTUP   AVR32_PM_OSCCTRL0_STARTUP_2048_RCOSC

//------------------------------------------------------------------------------
//   System parameters
//------------------------------------------------------------------------------

#define WATCHDOG_INTERVAL 2000         // Watchdog period [ms]
#define TIMER_PERIOD      1            // System timer period [ms]
#define TIMER_FLASH1      300          // Flash 1 delay [ms]
#define TIMER_FLASH2      200          // Flash 2 delay [ms]

#define WATCHDOG_ENABLE 1              // Enable watchdog
//#define SLEEP_ENABLE    1              // Enable power save

#define TIMER_SOUND       1            // Enable sound trigger
#define TIMER_KBD         1            // Enable keyboard trigger
#define TIMER_NO_TIMEOUT  1            // Disable inactivity timeout processing

// Interrupt priority :
#define SYS_TIMER_PRIORITY   AVR32_INTC_INT0         // System timer IRQ

#endif
