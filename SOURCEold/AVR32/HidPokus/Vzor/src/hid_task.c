/*This file is prepared for Doxygen automatic documentation generation.*/
//! \file *********************************************************************
//!
//! \brief This file manages the generic HID IN/OUT task.
//!
//! - Compiler:           IAR EWAVR and GNU GCC for AVR
//! - Supported devices:  AT90USB1287, AT90USB1286, AT90USB647, AT90USB646
//!
//! \author               Atmel Corporation: http://www.atmel.com \n
//!                       Support and FAQ: http://support.atmel.no/
//!
//! ***************************************************************************

/* Copyright (c) 2009 Atmel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an Atmel
 * AVR product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE EXPRESSLY AND
 * SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//_____  I N C L U D E S ___________________________________________________

#include "config.h"
#include "conf_usb.h"
#include "hid_task.h"
#include "lib_mcu/usb/usb_drv.h"
#include "usb_descriptors.h"
#include "modules/usb/device_chap9/usb_standard_request.h"
#include "usb_specific_request.h"



#define SINGLE_REPLY  1           // single ping reply/else multiple reply

// delay injection task :
//#define TASK_DELAY        120     // generate delay [ms]
//#define TASK_DELAY_PERIOD 1       // period counter
//#define TASK_DELAY        1       // generate delay [ms]
//#define TASK_DELAY_PERIOD 100     // period counter
#define TASK_DELAY        0         // disable delay
#define TASK_DELAY_PERIOD 0         // disable delay generation

#if TASK_DELAY_PERIOD != 0
   static Word DelayPeriod = TASK_DELAY_PERIOD;
#endif

//volatile U8 cpt_sof = 0;

static Byte Buffer[ HIDDEMO_IN_REPORT_SIZE];
static Bool IsBufferFull = false;      

#ifdef SINGLE_REPLY
#else
   #define PACKET_COUNT 100
   static Byte PacketIndex;
#endif

// Local functions :
void hid_report_out( void);
void hid_report_in( void);

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void hid_task_init( void)
{
   //Leds_init();
   //Joy_init();
}

//-----------------------------------------------------------------------------
// Task
//-----------------------------------------------------------------------------

void hid_task( void)
{
   if(!Is_device_enumerated())          // Check USB HID is enumerated
      return;

   hid_report_out();
   hid_report_in();
}


//-----------------------------------------------------------------------------
// Start of frame
//-----------------------------------------------------------------------------

void sof_action()
{
   //cpt_sof++;
} // sof_action

//-----------------------------------------------------------------------------
// Delay
//-----------------------------------------------------------------------------

void hid_task_delay()
{
#if TASK_DELAY_PERIOD != 0
   if( --DelayPeriod == 0){
      DelayPeriod = TASK_DELAY_PERIOD;
      //Led1_on();
      SysDelay( TASK_DELAY);
      //Led1_off();
   }
#endif
} // hid_task_delay

#ifdef SINGLE_REPLY
//-----------------------------------------------------------------------------
// Report OUT
//-----------------------------------------------------------------------------

void hid_report_out( void)
{
Byte i;

   Usb_select_endpoint(EP_HID_OUT);
   if( !Is_usb_receive_out()){
      return;                          // no data received
   }
   // read report from HID Generic Demo
   //Led0_on();
   if( IsBufferFull){
      return;                          // wait for buffer Tx
   }
   for( i = 0; i < HIDDEMO_OUT_REPORT_SIZE; i++){
      Buffer[ i] = Usb_read_byte();
   }
   Usb_ack_receive_out();
   IsBufferFull = true;
   //Led0_off();
} // hid_report_out

//-----------------------------------------------------------------------------
// Report IN
//-----------------------------------------------------------------------------

void hid_report_in( void)
{
Byte i;

   if( !IsBufferFull){
      return;                          // no data to send
   }

   Usb_select_endpoint( EP_HID_IN);
   if(!Is_usb_write_enabled()){
      return;                          // not ready to send report
   }
   // Send report
   //Led2_on();
   for( i = 0; i < HIDDEMO_IN_REPORT_SIZE; i++){
      Usb_write_byte( Buffer[ i]);     // send data
   }
   Usb_ack_in_ready();                 // send data over the USB
   IsBufferFull = false;               // buffer done
   //Led2_off();
} // hid_report_in

#else  // MULTIPLE_REPLY
//-----------------------------------------------------------------------------
// Report OUT
//-----------------------------------------------------------------------------

void hid_report_out( void)
{
Byte i;

   Usb_select_endpoint(EP_HID_OUT);
   if( !Is_usb_receive_out()){
      return;                          // no data received
   }
   // read report from HID Generic Demo
   //Led0_on();
   for( i = 0; i < HIDDEMO_OUT_REPORT_SIZE; i++){
      Buffer[ i] = Usb_read_byte();
   }
   Usb_ack_receive_out();
   IsBufferFull = true;
   PacketIndex  = 0;
  // Led0_off();
} // hid_report_out

//-----------------------------------------------------------------------------
// Report IN
//-----------------------------------------------------------------------------

void hid_report_in( void)
{
Byte i;

   if( !IsBufferFull){
      return;                          // no data to send
   }

   Usb_select_endpoint( EP_HID_IN);
   if(!Is_usb_write_enabled()){
      return;                          // not ready to send report
   }
   // Send report
   //Led2_on();
   Buffer[ 0] = PacketIndex;
   for( i = 0; i < HIDDEMO_IN_REPORT_SIZE; i++){
      Usb_write_byte( Buffer[ i]);     // send data
   }
   Usb_ack_in_ready();                 // send data over the USB
   if( ++PacketIndex == PACKET_COUNT){
      IsBufferFull = false;            // buffer done
   }
   //Led2_off();
} // hid_report_in

#endif // MULTIPLE_REPLY
