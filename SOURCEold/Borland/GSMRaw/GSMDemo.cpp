//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("GSMDemo.res");
USEFORM("Demo.cpp", MainForm);
USEUNIT("Com.cpp");
USEUNIT("Com_util.cpp");
USEUNIT("GSMraw.cpp");
USEUNIT("GSM.cpp");
USEUNIT("ComPkt.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
