//******************************************************************************
//
//   Demo.cpp     GSM demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Demo.h"
#include "../../Radio/ComPkt.h"
#include "../../TMC/TmcCmd.h"
#include <stdio.h>
#include "GSMRaw.h"

#define PACKET_MAX_DATA  504
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Gsm       = new TGsm;
   Context   = CONTEXT_UNCONNECTED;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt          = new TCrt( Memo);
   Logger       = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Gsm->Logger  = Logger;
} // FormCreate

//******************************************************************************
// Connect
//******************************************************************************

void __fastcall TMainForm::BtnConnectClick(TObject *Sender)
{
   if( !Gsm->Connect( PortName->Text)){
      Status->Caption = "Status : ERR";
      return;
   }
   if( !Gsm->Reset()){
      Status->Caption = "Status : reset ERR";
      return;
   }
   Status->Caption = "Status : OK";
   Context   = CONTEXT_CONNECTED;
} // BtnConnectClick

//******************************************************************************
// Check
//******************************************************************************

void __fastcall TMainForm::BtnCheckClick(TObject *Sender)
{
   if( !Gsm->Check()){
      Status->Caption = "Status : ERR";
      return;
   }
   Status->Caption = "Status : OK";
} // BtnCheckClick

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
   Status->Caption = "Status : ?";
   Status->Refresh();
   if( TxText->Text.Length() == 0){
      Status->Caption = "Status : No text";
      return;
   }
   if( TxTo->Text.Length() == 0){
      Status->Caption = "Status : No destination number";
      return;
   }
   Status->Caption = "Status : ?";
   Status->Refresh();
   if( !Gsm->SmsSend( TxTo->Text, TxText->Text.c_str())){
      Status->Caption = "Status : ERR";
      return;
   }
   Status->Caption = "Status : OK";
} // BtnSendClick

//******************************************************************************
// Receive
//******************************************************************************

void __fastcall TMainForm::BtnReceiveClick(TObject *Sender)
{
TString From, Message;

   if( !Gsm->SmsReceive( From, Message)){
      Status->Caption = "Status : ERR";
      return;
   }
   RxFrom->Text = From;
   RxText->Text = Message;
   Status->Caption = "Status : OK";
} // BtnReceiveClick

//******************************************************************************
// Attach
//******************************************************************************

void __fastcall TMainForm::BtnAttachClick(TObject *Sender)
{
   if( !Gsm->DataAttach()){
      Status->Caption = "Status : ERR";
      return;
   }
   Status->Caption = "Data : Ready";
   Context = CONTEXT_DATA;
} // BtnAttachClick

//******************************************************************************
// Release
//******************************************************************************

void __fastcall TMainForm::BtnReleaseClick(TObject *Sender)
{
   Gsm->DataRelease();
   Status->Caption = "Status : OK";
   Context = CONTEXT_READY;
} // BtnReleaseClick

//******************************************************************************
// Command
//******************************************************************************

void __fastcall TMainForm::BtnCommandClick(TObject *Sender)
{
unsigned Command;
unsigned Data;

   sscanf( EditCommand->Text.c_str(), "%X", &Command);
   sscanf( EditData->Text.c_str(),    "%X", &Data);
   ComTxPacket( (byte)Command, (dword)Data);

   byte  RCommand;
   dword RData;
/*
   unsigned RLength;
   byte     RBuffer[ CPKT_MAX_PACKET];
*/
   if( !ComRxPacket( &RCommand, &RData)){
      Crt->printf( "Reply error\n");
      return;
   }
   Crt->printf( "Reply : C %02X D %08X\n", RCommand, RData);
/*
   switch( RCommand){
      case TQHBAdapter::DATA_MESSAGE :
         Adapter->GetData( RLength, RBuffer);
         Crt->printf( "Data  : @ %02X L %02X\n", RAddress, RLength);
         break;
      case CMD_ERROR :
         Crt->printf( "Error : @ %02X C %02X D %08X\n", RAddress, RCommand, RData);
         break;
      default :
         Crt->printf( "Reply : C %02X D %08X\n", RCommand, RData);
         break;
   }
*/
} // BtnCommandClick

//******************************************************************************
// Timer
//******************************************************************************

#define PACKET_COUNT   (32768 / PACKET_MAX_DATA)

void __fastcall TMainForm::StatusTimerTick(TObject *Sender)
{
byte  RCommand;
dword RData;

   switch( Context){
      case CONTEXT_UNCONNECTED :
         return;

      case CONTEXT_CONNECTED :
         if( !Gsm->Reset()){
            LblRegistered->Caption = "Not present";
            LblOperator->Caption       = "?";
            LblSignalStrength->Caption = "?";
            return;
         }
//         Context = CONTEXT_READY;
         Context = CONTEXT_RING;
         break;

      case CONTEXT_READY :
         Context = CONTEXT_RING;
         break;

      case CONTEXT_RING :
         // kontrola zvoneni :
         if( Gsm->IsRinging()){
            LblRing->Caption = "RING";
         } else {
            LblRing->Caption = "         ";
         }
         return;

      case CONTEXT_DATA :
         if( !ComRxPacket( &RCommand, &RData)){
            return;
         }
         Crt->printf( "Command : C %02X D %08X\n", RCommand, RData);
         switch( RCommand){
            case TMCCMD_VERSION :
               RCommand |= TMCCMD_REPLY;
               RData = 0x00000100;
               break;
            case TMCCMD_READ :
               // datovy paket :
               StatusTimer->Enabled = false;
               ComTxBlockStart( PACKET_MAX_DATA);
               for( int i = 0; i < PACKET_MAX_DATA; i++){
                  ComTxBlockByte( (byte)i);
               }
               ComTxBlockEnd();
               Crt->printf( "Reply : Data\n");
               StatusTimer->Enabled = true;
               return;
            case TMCCMD_READ_ALL :
               StatusTimer->Enabled = false;
               for( int j = 0; j < PACKET_COUNT; j++){
                  ComTxBlockStart( PACKET_MAX_DATA);
                  for( int i = 0; i < PACKET_MAX_DATA; i++){
                     ComTxBlockByte( (byte)i);
                  }
                  ComTxBlockEnd();
                  Crt->printf( "Reply : Data\n");
               }
               StatusTimer->Enabled = true;
               return;
            default :
               RCommand = TMCCMD_ERROR;
               break;
         }
         ComTxPacket( RCommand, RData);
         Crt->printf( "Reply : C %02X D %08X\n", RCommand, RData);
         return;

      IDEFAULT
   }
   // sila signalu :
   int SignalStrength;
   Gsm->SignalStrength( SignalStrength);
   AnsiString SSNumber( SignalStrength);
   LblSignalStrength->Caption = SSNumber;

   // registrace :
   LblOperator->Caption   = "";
   if( Gsm->IsRegistered()){
      LblRegistered->Caption = "Registered";

      AnsiString Name;
      Gsm->Operator( Name);
      LblOperator->Caption = Name;
      Context = CONTEXT_RING;
   } else {
      LblRegistered->Caption = "Unregistered";
      Context = CONTEXT_CONNECTED;
   }
} // StatusTimerTick

//******************************************************************************
// Registered
//******************************************************************************

void __fastcall TMainForm::BtnRegisteredClick(TObject *Sender)
{
   if( Gsm->IsRegistered()){
      LblRegistered->Caption = "Registered";
   } else {
      LblRegistered->Caption = "Unregistered";
   }
} // BtnRegisteredClick

//******************************************************************************
// Pin ready
//******************************************************************************

void __fastcall TMainForm::BtnPinReadyClick(TObject *Sender)
{
   if( !GsmPinReady()){
      Crt->printf( "Pin not ready\n");
      return;
   }
   Crt->printf( "Pin OK\n");
} // BtnPinReadyClick

//******************************************************************************
// Pin enter
//******************************************************************************

void __fastcall TMainForm::BtnPinEnterClick(TObject *Sender)
{
   if( !GsmPinEnter( EdtPin->Text.c_str())){
      Crt->printf( "Pin ulock failed\n");
      return;
   }
   Crt->printf( "Pin unlock OK\n");
} // BtnPinEnterClick

//******************************************************************************
// Check SIM
//******************************************************************************

void __fastcall TMainForm::BtnSimClick(TObject *Sender)
{
   if( !GsmSimPresent()){
      Crt->printf( "SIM not present\n");
      return;
   }
   Crt->printf( "SIM inserted\n");
   return;
} // BtnSimClick

