//******************************************************************************
//
//   GSMr.cpp     GSM network wrap module (via GSMraw)
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "GSM.h"
#include "GSMraw.h"
#include "Com.h"

#pragma package(smart_init)

#define MAX_TEXT         256

//******************************************************************************
// Constructor
//******************************************************************************

TGsm::TGsm()
// Constructor
{
   ComInit( "COM10");
} //TGsm

//******************************************************************************
// Destructor
//******************************************************************************

TGsm::~TGsm()
// Destructor
{
} // ~TSsms

//******************************************************************************
// Connect
//******************************************************************************

bool TGsm::Connect( TString Port)
// Connect via <Port>
{
   return( true);
} // Connect

//******************************************************************************
// Reset
//******************************************************************************

bool TGsm::Reset()
// Initialize modem device
{
   return( GsmReset());
} // Reset

//******************************************************************************
// Check
//******************************************************************************

bool TGsm::Check()
// Check for modem device
{
   if( !SmsSendParameters()){
      return( false);
   }
   if( !SmsMemory( SMS_SIM_MEMORY)){
      return( false);
   }
   if( !SmsMemory( SMS_PHONE_MEMORY)){
      return( false);
   }
   if( !SmsMemory( SMS_SIM_MEMORY)){
      return( false);
   }
   return( true);
} // Check

//******************************************************************************
// IsRegistered
//******************************************************************************

bool TGsm::IsRegistered()
// Returns status of network registration
{
   return( GsmRegistered());
} // IsRegistered

//******************************************************************************
// Operator
//******************************************************************************

void TGsm::Operator( TString &Name)
// Returns registered operator name
{
char OperatorName[ SMS_MAX_OPERATOR + 1];

   if( !GsmOperator( OperatorName)){
      Name = "";
      return;
   }
   Name = AnsiString( OperatorName);
} // Operator

//******************************************************************************
// Sila signalu
//******************************************************************************

void TGsm::SignalStrength( int &Strength)
// Returns relative signal strength 10..31 or 99 if unknown
{
   Strength = GsmSignalStrength();
} // SignalStrength

//******************************************************************************
// Send
//******************************************************************************

bool TGsm::SmsSend( TString To, char *Format, ...)
// Send SMS to <To> number (printf format)
{
static char Text[ MAX_TEXT];         // text of message
static char DstNumber[ GSM_MAX_NUMBER + 1];

   // set destination number :
   strcpy( DstNumber, To.c_str());
   // expand text :
   va_list Arg;
   va_start( Arg, Format);
   vsprintf( Text, Format, Arg);
   byte MessageLength = strlen( Text);
   return( ::SmsSend( DstNumber, Text, MessageLength));
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TGsm::SmsReceive( TString &From, TString &Message)
// Read received message. Returns false if no message is waiting
{
static char Text[ MAX_TEXT];         // text of message
static char SrcNumber[ GSM_MAX_NUMBER + 1];
static byte i = 0;
int    r;

      r = ::SmsRead( i, SrcNumber, Text);
      i++;
      if( i >= 10){
         i = 0;
      }
      switch( r){
         case SMS_EMPTY_MESSAGE :
            From = "";
            Message = "** EMPTY **";
            return( true);
         case SMS_FORMAT_ERROR :
            From    = TString( SrcNumber);
            Message = "** ERR FORMAT **";
            return( true);
         case SMS_READ_ERROR :
            return( false);
         default :
            // cteni zpravy
            From = TString( SrcNumber);
            Message = TString( Text);
            // GsmDelete( i);
            return( true);
      }
} // Receive

//******************************************************************************
// Zvoneni
//******************************************************************************

bool TGsm::IsRinging()
// Returns YES at ringing
{
   return( GsmIsRinging());
} // IsRinging

//******************************************************************************
// Navazani spojeni
//******************************************************************************

bool TGsm::DataAttach()
// Connection setup
{
   return( GsmConnect());
} // DataAttach

//******************************************************************************
// Navazani spojeni
//******************************************************************************

void TGsm::DataRelease()
// Connection termination
{
   GsmDisconnect();
} // DataRelease

