//******************************************************************************
//
//   Demo.h       GSM demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef DemoH
#define DemoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "GSM.h"
#include "..\Serial\CrtLogger.h"
#include "..\Crt\Crt.h"

typedef enum {
   CONTEXT_UNCONNECTED,
   CONTEXT_CONNECTED,
   CONTEXT_RESET,
   CONTEXT_READY,
   CONTEXT_RING,
   CONTEXT_DATA,
} TMyContext;

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TButton *BtnConnect;
        TButton *BtnCheck;
        TMemo *Memo;
        TEdit *TxText;
        TEdit *RxText;
        TButton *BtnSend;
        TButton *BtnReceive;
        TEdit *PortName;
        TEdit *RxFrom;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *TxTo;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Status;
        TTimer *StatusTimer;
        TLabel *LblRegistered;
        TLabel *LblOperator;
        TLabel *LblSignalStrength;
        TLabel *LblRing;
        TButton *BtnAttach;
        TButton *BtnRelease;
        TEdit *EditCommand;
        TEdit *EditData;
        TLabel *Label5;
        TLabel *Label6;
        TButton *BtnCommand;
        TButton *BtnRegistered;
   TButton *BtnPinReady;
   TButton *BtnPinEnter;
   TEdit *EdtPin;
   TLabel *Label7;
   TButton *BtnSim;
   TButton *BtnCcid;
        void __fastcall BtnConnectClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnCheckClick(TObject *Sender);
        void __fastcall BtnSendClick(TObject *Sender);
        void __fastcall BtnReceiveClick(TObject *Sender);
        void __fastcall StatusTimerTick(TObject *Sender);
        void __fastcall BtnAttachClick(TObject *Sender);
        void __fastcall BtnReleaseClick(TObject *Sender);
        void __fastcall BtnCommandClick(TObject *Sender);
        void __fastcall BtnRegisteredClick(TObject *Sender);
   void __fastcall BtnPinReadyClick(TObject *Sender);
   void __fastcall BtnPinEnterClick(TObject *Sender);
   void __fastcall BtnSimClick(TObject *Sender);
   void __fastcall BtnCcidClick(TObject *Sender);
public:	// User declarations
   TGsm *Gsm;
   TMyContext Context;
   TCrt       *Crt;
   TCrtLogger *Logger;
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
