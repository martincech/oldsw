//******************************************************************************
//
//   Demo.h       GSM demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef TestH
#define TestH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "GSM.h"
#include "..\Serial\CrtLogger.h"
#include "..\Crt\Crt.h"

typedef enum {
   CONTEXT_UNCONNECTED,
   CONTEXT_CONNECTED,
   CONTEXT_RESET,
   CONTEXT_STOP,
} TMyContext;

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *Memo;
        TEdit *PortName;
        TLabel *Status;
        TTimer *StatusTimer;
        TLabel *LblRegistered;
        TCheckBox *BtnGo;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnGoClick(TObject *Sender);
        void __fastcall StatusTimerTimer(TObject *Sender);
public:	// User declarations
   TGsm *Gsm;
   TMyContext Context;
   TCrt       *Crt;
   TCrtLogger *Logger;
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
