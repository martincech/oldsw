object MainForm: TMainForm
  Left = 438
  Top = 228
  Width = 863
  Height = 684
  Caption = 'GSM demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 680
    Top = 496
    Width = 29
    Height = 13
    Caption = 'From :'
  end
  object Label2: TLabel
    Left = 680
    Top = 456
    Width = 19
    Height = 13
    Caption = 'To :'
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Receive :'
  end
  object Label4: TLabel
    Left = 16
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Send :'
  end
  object Status: TLabel
    Left = 16
    Top = 576
    Width = 45
    Height = 13
    Caption = 'Status : ?'
  end
  object LblRegistered: TLabel
    Left = 696
    Top = 192
    Width = 65
    Height = 13
    Caption = 'Unconnected'
  end
  object LblOperator: TLabel
    Left = 704
    Top = 216
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblSignalStrength: TLabel
    Left = 704
    Top = 240
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblRing: TLabel
    Left = 704
    Top = 264
    Width = 14
    Height = 13
    Caption = 'R?'
  end
  object Label5: TLabel
    Left = 673
    Top = 373
    Width = 27
    Height = 13
    Caption = 'Cmd :'
  end
  object Label6: TLabel
    Left = 673
    Top = 404
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object Label7: TLabel
    Left = 520
    Top = 616
    Width = 18
    Height = 13
    Caption = 'Pin:'
  end
  object BtnConnect: TButton
    Left = 688
    Top = 16
    Width = 75
    Height = 17
    Caption = 'Connect'
    TabOrder = 0
    OnClick = BtnConnectClick
  end
  object BtnCheck: TButton
    Left = 688
    Top = 40
    Width = 75
    Height = 17
    Caption = 'Check'
    TabOrder = 1
    OnClick = BtnCheckClick
  end
  object Memo: TMemo
    Left = 16
    Top = 112
    Width = 641
    Height = 457
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    TabOrder = 2
  end
  object TxText: TEdit
    Left = 16
    Top = 24
    Width = 641
    Height = 21
    TabOrder = 3
    Text = 'Ahoj.'
  end
  object RxText: TEdit
    Left = 16
    Top = 72
    Width = 641
    Height = 21
    Enabled = False
    TabOrder = 4
    Text = '?'
  end
  object BtnSend: TButton
    Left = 688
    Top = 72
    Width = 75
    Height = 17
    Caption = 'Send'
    TabOrder = 5
    OnClick = BtnSendClick
  end
  object BtnReceive: TButton
    Left = 688
    Top = 96
    Width = 75
    Height = 17
    Caption = 'Receive'
    TabOrder = 6
    OnClick = BtnReceiveClick
  end
  object PortName: TEdit
    Left = 680
    Top = 544
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 7
    Text = 'COM10'
  end
  object RxFrom: TEdit
    Left = 680
    Top = 512
    Width = 89
    Height = 21
    Enabled = False
    TabOrder = 8
    Text = '?'
  end
  object TxTo: TEdit
    Left = 680
    Top = 472
    Width = 89
    Height = 21
    TabOrder = 9
    Text = '420604967296'
  end
  object BtnAttach: TButton
    Left = 688
    Top = 302
    Width = 75
    Height = 17
    Caption = 'Attach'
    TabOrder = 10
    OnClick = BtnAttachClick
  end
  object BtnRelease: TButton
    Left = 688
    Top = 326
    Width = 75
    Height = 17
    Caption = 'Release'
    TabOrder = 11
    OnClick = BtnReleaseClick
  end
  object EditCommand: TEdit
    Left = 720
    Top = 368
    Width = 57
    Height = 21
    TabOrder = 12
    Text = '0'
  end
  object EditData: TEdit
    Left = 720
    Top = 400
    Width = 57
    Height = 21
    TabOrder = 13
    Text = '0'
  end
  object BtnCommand: TButton
    Left = 688
    Top = 430
    Width = 75
    Height = 17
    Caption = 'Command'
    TabOrder = 14
    OnClick = BtnCommandClick
  end
  object BtnRegistered: TButton
    Left = 688
    Top = 136
    Width = 75
    Height = 17
    Caption = 'Registered'
    TabOrder = 15
    OnClick = BtnRegisteredClick
  end
  object BtnPinReady: TButton
    Left = 680
    Top = 576
    Width = 75
    Height = 25
    Caption = 'Pin Ready'
    TabOrder = 16
    OnClick = BtnPinReadyClick
  end
  object BtnPinEnter: TButton
    Left = 680
    Top = 608
    Width = 75
    Height = 25
    Caption = 'Pin Enter'
    TabOrder = 17
    OnClick = BtnPinEnterClick
  end
  object EdtPin: TEdit
    Left = 552
    Top = 608
    Width = 121
    Height = 21
    TabOrder = 18
    Text = '1234'
  end
  object BtnSim: TButton
    Left = 760
    Top = 576
    Width = 75
    Height = 25
    Caption = 'Check SIM'
    TabOrder = 19
    OnClick = BtnSimClick
  end
  object BtnCcid: TButton
    Left = 760
    Top = 608
    Width = 75
    Height = 25
    Caption = 'Sim by CCID'
    TabOrder = 20
    OnClick = BtnCcidClick
  end
  object StatusTimer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = StatusTimerTick
    Left = 712
    Top = 168
  end
end
