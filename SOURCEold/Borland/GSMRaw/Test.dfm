object MainForm: TMainForm
  Left = 275
  Top = 233
  Width = 804
  Height = 627
  Caption = 'GSM demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Status: TLabel
    Left = 16
    Top = 576
    Width = 45
    Height = 13
    Caption = 'Status : ?'
  end
  object LblRegistered: TLabel
    Left = 688
    Top = 104
    Width = 22
    Height = 13
    Caption = 'Stop'
  end
  object Memo: TMemo
    Left = 16
    Top = 112
    Width = 641
    Height = 457
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    TabOrder = 0
  end
  object PortName: TEdit
    Left = 680
    Top = 544
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 1
    Text = 'COM1'
  end
  object BtnGo: TCheckBox
    Left = 32
    Top = 16
    Width = 97
    Height = 17
    Caption = 'Go'
    TabOrder = 2
    OnClick = BtnGoClick
  end
  object StatusTimer: TTimer
    OnTimer = StatusTimerTimer
    Left = 712
    Top = 144
  end
end
