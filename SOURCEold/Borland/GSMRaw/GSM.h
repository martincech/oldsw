//******************************************************************************
//
//   GSM.h        GSM network SMS service
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef GsmH
   #define GsmH

#ifndef UartH
   #include "../Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Serial/Logger.h"
#endif

#define SMS_MAX_REPLY 1024         // internal size of receive buffer

//******************************************************************************
// TSms
//******************************************************************************

class TGsm {
public :
   TGsm();
   // Constructor
   ~TGsm();
   // Destructor
   bool Connect( TString Port);
   // Connect via <Port>
   bool Reset();
   // Initialize modem device
   bool Check();
   // Check for modem device
   bool IsRegistered();
   // Returns status of network registration
   void Operator( TString &Name);
   // Returns registered operator name
   void SignalStrength( int &Strength);
   // Returns relative signal strength 10..31 or 99 if unknown
   bool SmsSend( TString To, char *Format, ...);
   // Send SMS to <To> number (printf format)
   bool SmsReceive( TString &From, TString &Message);
   // Read received message. Returns false if no message is waiting
   bool IsRinging();
   // Returns YES at ringing
   bool DataAttach();
   // Connection setup
   void DataRelease();
   // Connection termination

   __property TUart   *Port   = {read=FPort};
   __property TLogger *Logger = {read=FLogger, write=FLogger};

//------------------------------------------------------------------------------
protected :
   TUart   *FPort;                  // connection port
   TLogger *FLogger;                // raw data logger

#ifndef __GSM_RAW__
   int     FTotalTimeout;           // modem response time
   int     FIntercharacterTimeout;  // modem data flow break time
   int     FSmsCenterTimeout;       // SMS Center reply timeout
   int     FMemorySize;             // SIM memory size
   char    RxBuffer[ SMS_MAX_REPLY];// receive buffer
   char    TxBuffer[ SMS_MAX_REPLY];// receive buffer

   bool TxString( TString Data);
   // Send <Data> string
   bool RxString();
   // Receive <Data> string into <Buffer>
   void AsciiToGsm( char *Text);
   // Convert to GSM alphabet
   void GsmToAscii( char *Text, int Length);
   // Convert GSM alphabet to text
   byte GetHexNumber( char *Text);
   // Converts two characters into number
#endif
}; // TGsm

#endif
