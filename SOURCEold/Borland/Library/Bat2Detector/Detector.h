//******************************************************************************
//
//   Bat2Device.h  BAT2 device interface
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef Bat2DeviceH
#define Bat2DeviceH

#include <stdio.h>

#include "../Library/Serial/Uart/ComUart.h"
#include "../Library/Bat2Detector/Dp.h"

#define BD_COM_NAME    8
#define BD_PATH        512
#define BD_MAX_COMMENT 1024

//******************************************************************************
// TBat2Device
//******************************************************************************

class TBat2Detector {
public :
   TBat2Detector();
   // Constructor

   ~TBat2Detector();
   // Destructor

   void SetCom( char *Name);
   // Set communincation device name

   void SetFile( char *Name);
   // Set file <Name>

   bool SetParameters();
   // Send parameters to device

   bool Run(void (*Callback)(TDataSample *Data, int DataCount));
   // Run & save conversions

   bool Stop();
   // Stop conversions & close file

   bool ResetAdc();

   int FileGetSamples(double *Samples);

   bool Firmware(byte *Firmware, int Length);

   static double TWeightToG(TWeight Weight);
   static double GToTWeight(double Weight);

   bool ReadCalibrationData();
   bool Calibrate();

   int AdcAccuracy;                          // use CHOP mode / without CHOP
   int AdcRate;                         // use SINC3 filter / SINC4
   int AdcPrefilter;                    // signum extension / unsigned
   EPs081Filter AdcFilter;                          // conversion rate [Hz]

   EDetectorMode ModeMode;                // continuous mode / power save mode
   int ModeBurstSize;                 // bridge on delay [Samples]
   int ModeAveraging;                  // sampes burst count [Samples]

   int DetectionAveragingWindow;                // continuous mode / power save mode
   double DetectionRange;                 // bridge on delay [Samples]
   int DetectionStableWindow;                  // sampes burst count [Samples]

   double CalibrationZero;
   double CalibrationFullRange;
   int CalibrationRawZero;
   int CalibrationRawFullRange;
   word CalibrationDelay;
   word CalibrationDuration;

   int  SamplesDone;                   // sampling progress
   double *Samples;
   char   Comment[ BD_MAX_COMMENT + 1];// comment

//------------------------------------------------------------------------------
protected :
   TDp *Protocol;                         // COM interface
   char      ComName[ BD_COM_NAME + 1];    // COM port name
   char      FileName[ BD_PATH    + 1];    // COM port name
   FILE     *f;                            // active file
   bool      StopReceive;                  // stop packet receive loop

   bool PacketStatistics( int64 *Sum);
   // Parse & calculate statistics

   void SaveHeader();
   // Save file header
}; // TBat2Detector

#endif
