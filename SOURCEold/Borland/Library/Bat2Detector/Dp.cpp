//******************************************************************************
//
//   Dp.cpp  Detector protocol
//   Version 0.0  (c) Veit electronics
//
//******************************************************************************

#include "../Library/Bat2Detector/Dp.h"
#include "../../AVR8/Library/Bat2Detector/DpDef.h"

#define RX_TIMEOUT_REPLY           1000
#define RX_TIMEOUT_DATA               0
#define RX_TIMEOUT_INTERCHARACTER    10

//******************************************************************************
// Constructor
//******************************************************************************

TDp::TDp()
// Constructor
{
   FCom = 0;
} // TDp

//******************************************************************************
// Destructor
//******************************************************************************

TDp::~TDp()
// Destructor
{
   if( FCom){
      FCom->Close();
   }
} // ~TDp

//******************************************************************************
// Open
//******************************************************************************

bool TDp::Open( char *ComName)
// Open <ComName>
{
   if( FCom){
      FCom->Close();
   }
   TIdentifier Identifier;
   // COM setup
   FCom = new TComUart;
   if( !FCom->Locate( ComName, Identifier)){
      delete FCom;
      FCom = 0;
      return( false);
   }
   if( !FCom->Open( Identifier)){
      delete FCom;
      FCom = 0;
      return( false);
   }
   // default parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate     = 38400;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   FCom->SetParameters( Parameters);
   // common init :
   FCom->SetRxWait(RX_TIMEOUT_REPLY, RX_TIMEOUT_INTERCHARACTER);
   FCom->Flush();

   return( true);
} // Open

//******************************************************************************
// AdcParametersPicostrain
//******************************************************************************

bool TDp::AdcParametersPs081( byte Accuracy, byte Rate, EPs081Filter Filter, byte Prefilter)
// AdcParametersPicostrain
{
   TDpQuery Query;
   TDpReply Reply;
   int ReplyLength;

   Query.Command = ADC_PARAMETERS;
   Query.Parameters.Adc.Accuracy = Accuracy;
   Query.Parameters.Adc.Rate = Rate;
   Query.Parameters.Adc.Filter = Filter;
   Query.Parameters.Adc.Prefilter = Prefilter;

   if(!SendQuery((byte *)&Query, sizeof(TAdcParameters) + 1)) {
      return false;
   }

   ReplyLength = ReceiveReply((byte *)&Reply, sizeof(TParametersReply));

   if(ReplyLength != sizeof(TParametersReply)) {
      return false;
   }

   if(Reply.Command != ADC_PARAMETERS || Reply.Parameters.Status != STATUS_OK) {
      return false;
   }

   return true;
} // AdcParametersPicostrain

bool TDp::DetectionParameters( byte AveragingWindow, byte AbsoluteRange, byte StableWindow)
// AdcParametersPicostrain
{
   TDpQuery Query;
   TDpReply Reply;
   int ReplyLength;

   Query.Command = DETECTION;
   Query.Parameters.Detection.AveragingWindow = AveragingWindow;
   Query.Parameters.Detection.AbsoluteRange = AbsoluteRange;
   Query.Parameters.Detection.StableWindow = StableWindow;

   if(!SendQuery((byte *)&Query, sizeof(TDetectionParameters) + 1)) {
      return false;
   }

   ReplyLength = ReceiveReply((byte *)&Reply, sizeof(TParametersReply));

   if(ReplyLength != sizeof(TParametersReply)) {
      return false;
   }

   if(Reply.Command != DETECTION || Reply.Parameters.Status != STATUS_OK) {
      return false;
   }

   return true;
} // AdcParametersPicostrain

//******************************************************************************
// AdcParametersPicostrain
//******************************************************************************

bool TDp::Start()
{
   return SimpleCommand(START);
}
    
//******************************************************************************
// AdcParametersPicostrain
//******************************************************************************

bool TDp::Stop()
{
   return SimpleCommand(STOP);
}
//******************************************************************************
// AdcParametersPicostrain
//******************************************************************************

bool TDp::Mode( EDetectorMode Mode, word BurstSize, byte Averaging)
// AdcParametersPicostrain
{
   TDpQuery Query;
   TDpReply Reply;
   int ReplyLength;

   Query.Command = DETECTOR_MODE;
   Query.Parameters.Mode.Mode = Mode;
   Query.Parameters.Mode.BurstSize = BurstSize;
   Query.Parameters.Mode.Averaging = Averaging;

   if(!SendQuery((byte *)&Query, sizeof(TModeParameters) + 1)) {
      return false;
   }

   ReplyLength = ReceiveReply((byte *)&Reply, sizeof(TParametersReply));

   if(ReplyLength != sizeof(TParametersReply)) {
      return false;
   }

   if(Reply.Command != DETECTOR_MODE || Reply.Parameters.Status != STATUS_OK) {
      return false;
   }

   return true;
} // AdcParametersPicostrain


bool TDp::SendQuery(byte *QueryData, int QueryLength) {
   FCom->Flush();

   int Written = FCom->Write(QueryData, QueryLength);

   if(Written != QueryLength) {
      return false;
   }

   return true;
}

int TDp::ReceiveReply(byte *ReplyData, int MaxReplyLength) {
   FCom->SetRxWait(RX_TIMEOUT_REPLY, RX_TIMEOUT_INTERCHARACTER);
   return FCom->Read(ReplyData, MaxReplyLength);
}

int TDp::ReadAllData(TWeight **DataBuffer) {
   int Count;
   int CorrectCount;

   FCom->SetRxWait(0, 1);

   Count = FCom->Read(&Data, sizeof(Data));

   if(Count == 0) {
      return 0;
   }

   CorrectCount = sizeof(Data.Command) + sizeof(Data.Data.DataCount) + Data.Data.DataCount * sizeof(Data.Data.DataAll[0]);

   if(Count != CorrectCount) {
      return 0;
   }

   if(Data.Command != DATA) {
      return 0;
   }

   *DataBuffer = Data.Data.DataAll;

   return Data.Data.DataCount;
}

int TDp::ReadStableData(TStableWeight **DataBuffer) {
   int Count;
   int CorrectCount;

   FCom->SetRxWait(100, 1);

   Count = FCom->Read(&Data, sizeof(Data));

   if(Count == 0) {
      return 0;
   }

   CorrectCount = sizeof(Data.Command) + sizeof(Data.Data.DataCount) + Data.Data.DataCount * sizeof(Data.Data.DataStable[0]);

   if(Count != CorrectCount) {
      return 0;
   }

   if(Data.Command != DATA) {
      return 0;
   }

   *DataBuffer = Data.Data.DataStable;

   return Data.Data.DataCount;
}

int TDp::ReadPs081Data(TPs081Weight **DataBuffer) {
   int Count;
   int CorrectCount;

   FCom->SetRxWait(0, 1);

   Count = FCom->Read(&Data, sizeof(Data));

   if(Count == 0) {
      return 0;
   }

   CorrectCount = sizeof(Data.Command) + sizeof(Data.Data.DataCount) + Data.Data.DataCount * sizeof(Data.Data.DataPs081[0]);

   if(Count != CorrectCount) {
      return 0;
   }

   if(Data.Command != DATA) {
      return 0;
   }

   *DataBuffer = Data.Data.DataPs081;

   return Data.Data.DataCount;
}

bool TDp::Firmware(byte *Firmware, int Length)
{
   TDpQuery Query;
   TDpReply Reply;
   int ReplyLength;

   if(!SimpleCommand(FIRMWARE_START)) {
      return false;
   }

   Query.Command = FIRMWARE_CHUNK;

   int ChunkSize;

   for(int i = 0 ; i < Length ; i += PS081_FIRMWARE_CHUNK_SIZE) {
      if(i + PS081_FIRMWARE_CHUNK_SIZE <= Length) {
         ChunkSize = PS081_FIRMWARE_CHUNK_SIZE;
      } else {          // for the last incomplete chunk
         ChunkSize = Length - i;
      }

      memcpy(Query.FirmwareChunk.Chunk, (word *) (Firmware + i), ChunkSize);
      Query.FirmwareChunk.ChunkNum = i / PS081_FIRMWARE_CHUNK_SIZE;

      if(!SendQuery((byte *)&Query, sizeof(TFirmwareChunkQuery))) {
         return false;
      }

      ReplyLength = ReceiveReply((byte *)&Reply, sizeof(TFirmwareChunkReply));

      if(ReplyLength != sizeof(TFirmwareChunkReply)) {
         return false;
      }

      if(Reply.Command != FIRMWARE_CHUNK || Reply.SimpleCommand.Status != STATUS_OK) {
         return false;
      }
   }

   if(!SimpleCommand(FIRMWARE_STOP)) {
      return false;
   }

   return true;
}

bool TDp::SimpleCommand(EDetectorCommand Command) {
   TDpQuery Query;
   TDpReply Reply;
   int ReplyLength;

   Query.Command = Command;

   if(!SendQuery((byte *)&Query, sizeof(TSimpleCommandQuery))) {
      return false;
   }

   ReplyLength = ReceiveReply((byte *)&Reply, sizeof(TSimpleCommandReply));

   if(ReplyLength != sizeof(TSimpleCommandReply)) {
      return false;
   }

   if(Reply.Command != Command || Reply.SimpleCommand.Status != STATUS_OK) {
      return false;
   }

   return true;
}

bool TDp::StartCalibration(TWeight Zero, TWeight FullRange, word Delay, word Duration) {
   TDpQuery Query;

   Query.Command = CALIBRATION;
   Query.Calibration.Parameters.Zero = Zero;
   Query.Calibration.Parameters.FullRange = FullRange;
   Query.Calibration.Parameters.Delay = Delay;
   Query.Calibration.Parameters.Duration = Duration;

   if(!SendQuery((byte *)&Query, sizeof(TCalibrationQuery))) {
      return false;
   }

   return true;
}

bool TDp::CalibrationReply(bool *Success, TWeight *Zero, dword *RawZero, TWeight *FullRange, dword *RawFullRange) {
   TDpReply Reply;
   int ReplyLength;

   ReplyLength = ReceiveReply((byte *)&Reply, sizeof(TCalibrationReply));

   if(ReplyLength != sizeof(TCalibrationReply)) {
      return false;
   }

   if(Reply.Command != CALIBRATION) {
      return false;
   }

   *Success = true;

   if(Reply.Calibration.Status != STATUS_OK) {
      *Success = false;
   }

   *Zero = Reply.Calibration.CalibrationData.Zero;
   *RawZero = Reply.Calibration.CalibrationData.RawZeroHigh;
   *FullRange = Reply.Calibration.CalibrationData.FullRange;
   *RawFullRange = Reply.Calibration.CalibrationData.RawFullRangeHigh;

   return true;
}

bool TDp::ReadCalibrationData(TWeight *Zero, dword *RawZero, TWeight *FullRange, dword *RawFullRange) {
   TDpQuery Query;
   TDpReply Reply;
   int ReplyLength;

   Query.Command = CALIBRATION_DATA;

   if(!SendQuery((byte *)&Query, sizeof(TCalibrationDataQuery))) {
      return false;
   }

   ReplyLength = ReceiveReply((byte *)&Reply, sizeof(TCalibrationDataReply));

   if(ReplyLength != sizeof(TCalibrationDataReply)) {
      return false;
   }

   if(Reply.Command != CALIBRATION_DATA) {
      return false;
   }

   *Zero = Reply.CalibrationData.CalibrationData.Zero;
   *RawZero = Reply.CalibrationData.CalibrationData.RawZeroHigh;
   *FullRange = Reply.CalibrationData.CalibrationData.FullRange;
   *RawFullRange = Reply.CalibrationData.CalibrationData.RawFullRangeHigh;

   return true;
}
