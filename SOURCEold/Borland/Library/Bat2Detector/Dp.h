//******************************************************************************
//
//   Dp.h  Detector protocol
//   Version 0.0  (c) Veit Electronics
//
//******************************************************************************

#ifndef __Dp_H__
   #define __Dp_H__
   
#include "../Library/Serial/UART/ComUart.h"
#include "../../AVR8/Library/Bat2Detector/DetectorDef.h"
#include "../../AVR8/Library/Bat2Detector/DpDef.h"

class TDp {
public :
   TDp();
   // Constructor

   ~TDp();
   // Destructor

   bool Open( char *Name);
   // Set communincation device name

   bool AdcParametersPs081( byte Accuracy, byte Rate, EPs081Filter Filter, byte Prefilter);
   bool DetectionParameters( byte AveragingWindow, byte AbsoluteRange, byte StableWindow);
   bool Start();
   bool Stop();
   bool TDp::Firmware(byte *Firmware, int Length);
   bool Mode( EDetectorMode Mode, word BurstSize, byte Averaging);
   int ReadAllData(TWeight **DataBuffer);
   int ReadStableData(TStableWeight **DataBuffer);
   int ReadPs081Data(TPs081Weight **DataBuffer);
   bool ReadCalibrationData(TWeight *Zero, dword *RawZero, TWeight *FullRange, dword *RawFullRange);
   bool StartCalibration(TWeight Zero, TWeight FullRange, word Delay, word Duration);
   bool CalibrationReply(bool *Success, TWeight *Zero, dword *RawZero, TWeight *FullRange, dword *RawFullRange);

   //------------------------------------------------------------------------------
protected :
   bool SendQuery( byte *Data, int Length);
   int ReceiveReply(byte *ReplyData, int MaxReplyLength);
   bool SimpleCommand(EDetectorCommand Command);
   TComUart *FCom;                         // COM interface
   TDpDataReply Data;
}; // TDp

#endif