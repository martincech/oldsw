//******************************************************************************
//
//   Detector.cpp  BAT2 detector interface
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "../Library/Bat2Detector/Detector.h"
#include "../Library/Bat2Detector/Dp.h"

#pragma package(smart_init)

double TBat2Detector::TWeightToG(TWeight Weight) {
   return Weight / 10.0;
}

double TBat2Detector::GToTWeight(double Weight) {
   return (TWeight) (Weight * 10);
}

//******************************************************************************
// Constructor
//******************************************************************************

TBat2Detector::TBat2Detector()
// Constructor
{
   AdcAccuracy  = 50;
   AdcRate      = 10;
   AdcPrefilter = 1;
   AdcFilter    = PS081_FILTER_NONE;

   ModeMode = DETECTOR_MODE_ALL;
   ModeBurstSize  = 10;
   ModeAveraging   = 1;

   DetectionAveragingWindow = 10;
   DetectionRange = 2.0;
   DetectionStableWindow = 10;

   CalibrationZero = 0.0;
   CalibrationFullRange = 100000.0;
   CalibrationRawZero = 16775464;
   CalibrationRawFullRange = 1199748;
   CalibrationDelay = 10;
   CalibrationDuration = 10;

   Protocol = new TDp();
   strcpy( ComName, "COM1");
   strcpy( FileName, "");
   strcpy( Comment, "No comment");
   f = 0;
} // TBat2Device

//******************************************************************************
// Destructor
//******************************************************************************

TBat2Detector::~TBat2Detector()
// Destructor
{
} // ~TBat2Device

//******************************************************************************
// Set COM
//******************************************************************************

void TBat2Detector::SetCom( char *Name)
// Set communincation device name
{
   strncpyx( ComName, Name, BD_COM_NAME);
} // SetCom

//******************************************************************************
// Set file
//******************************************************************************

void TBat2Detector::SetFile( char *Name)
// Set file <Name>
{
   strncpyx( FileName, Name, BD_PATH);
} // SetFile

//******************************************************************************
// Set parameters
//******************************************************************************

bool TBat2Detector::SetParameters()
// Send parameters to device
{
   if(!Protocol->AdcParametersPs081(AdcAccuracy, AdcRate, AdcFilter, AdcPrefilter)) {
      return false;
   }

   if(!Protocol->Mode(ModeMode, ModeBurstSize, ModeAveraging)) {
      return false;
   }

   if(!Protocol->DetectionParameters(DetectionAveragingWindow, GToTWeight(DetectionRange), DetectionStableWindow)) {
      return false;
   }

   return true;
} // SetParameters

//******************************************************************************
// Run
//******************************************************************************

bool TBat2Detector::Run(void (*Callback)(TDataSample *Data, int DataCount))
// Run & save conversions
{
   if(!Protocol->Open(ComName)) {
      return false;
   }

   if( strlen( FileName) == 0){
      return( FALSE);
   }

   f = fopen( FileName, "w");
   if( !f){
      return( FALSE);
   }

   SaveHeader();                       // create file header

   // set parameters :
   if( !SetParameters()){
      return( FALSE);
   }

   if( !Protocol->Start()){
      return( FALSE);
   }

   // start conversion :
   StopReceive = FALSE;
   SamplesDone = 0;
   char StringBuffer[40];
   int Count;
   TDataSample *Buffer;
   TWeight *WeightBuffer = &Buffer->Weight;
   TStableWeight *StableWeightBuffer = &Buffer->StableWeight;
   TPs081Weight *Ps081WeightBuffer = &Buffer->Ps081;

   do {
      Application->ProcessMessages();  // windows message loop

      switch(ModeMode) {
         case DETECTOR_MODE_ALL:
            Count = Protocol->ReadAllData(&WeightBuffer);

            if(Count == 0) {
               break;
            }

            for(int i = 0 ; i < Count ; i++) {
               fprintf(f, "%d\r\n", WeightBuffer[i]);
            }

            Callback((TDataSample *)WeightBuffer, Count);
            break;

         case DETECTOR_MODE_STABLE:
            Count = Protocol->ReadStableData(&StableWeightBuffer);

            if(Count == 0) {
               break;
            }

            for(int i = 0 ; i < Count ; i++) {
               fprintf(f, "%0.2f,%d\r\n", TWeightToG(StableWeightBuffer[i].Weight), StableWeightBuffer[i].Time);
            }

            Callback((TDataSample *)StableWeightBuffer, Count);
            break;

         case DETECTOR_MODE_PS081:
            Count = Protocol->ReadPs081Data(&Ps081WeightBuffer);

            if(Count == 0) {
               break;
            }

            for(int i = 0 ; i < Count ; i++) {
               if(Ps081WeightBuffer[i].Stable) {
                  memcpy(StringBuffer, "*", 2);
               } else {
                  memcpy(StringBuffer, " ", 2);
               }

               fprintf(f, "%0.2f,%s,%d,%0.2f,%0.2f,%d,%0.2f,%d\r\n",
                  TWeightToG(Ps081WeightBuffer[i].Weight),
                  StringBuffer,
                  Ps081WeightBuffer[i].Raw,
                  TWeightToG(Ps081WeightBuffer[i].LowPassInput),
                  TWeightToG(Ps081WeightBuffer[i].LowPassOutput),
                  Ps081WeightBuffer[i].PreAvgCounter,
                  TWeightToG(Ps081WeightBuffer[i].HighPassAbs),
                  Ps081WeightBuffer[i].StableWindowCounter);
            }

            Callback((TDataSample *)Ps081WeightBuffer, Count);
            break;
      }

      SamplesDone += Count;

      if(Count) {

      }
   } while( !StopReceive);
   return( TRUE);
} // Run

bool TBat2Detector::ResetAdc() {
   if(!Protocol->Stop()) {
      return false;
   }

   if(!Protocol->AdcParametersPs081(AdcAccuracy, AdcRate, AdcFilter, AdcPrefilter)) {
      return false;
   }

   if(!Protocol->Start()) {
      return false;
   }
}

bool TBat2Detector::Firmware(byte *Firmware, int Length) {
   if(!Protocol->Open(ComName)) {
      return false;
   }

   return Protocol->Firmware(Firmware, Length);
}
//******************************************************************************
// Stop
//******************************************************************************

bool TBat2Detector::Stop()
// Stop conversions & close file
{
   for( int i = 0; i < 3; i++){
      if(Protocol->Stop()) {
         break;
      }
   }
   if( f){
      fclose( f);
      f = 0;
   }
   StopReceive = TRUE;
   return( TRUE);
} // Stop

//------------------------------------------------------------------------------

//******************************************************************************
// Packet statistics
//******************************************************************************

bool TBat2Detector::PacketStatistics( int64 *Sum)
// Parse & calculate statistics
{
/*byte *Src;
dword Value;
int   ActiveSamples;

   Src   = (byte *)&Buffer[ 1];         // packet contents start
   *Sum  = 0;
   ActiveSamples = 0;
   // scan Values array :
   for( int i = 0; i < SamplesCount; i++){
      Value = (dword)Src[ ADC_VALUE_SIZE * i]           |
              (dword)Src[ ADC_VALUE_SIZE * i + 1] << 8  |
              (dword)Src[ ADC_VALUE_SIZE * i + 2] << 16;
      SignumExtension( &Value);
      if( SamplesDone + i < Delay){
         continue;                     // skip sample
      }
      ActiveSamples++;                 // samples in sum
      *Sum += (int)Value;              // add to sum
   }
   if( !ActiveSamples){
      return( FALSE);                  // invalid average
   }  */
   return( TRUE);                      // average OK
} // PacketStatistics

//******************************************************************************
// Save header
//******************************************************************************

void TBat2Detector::SaveHeader()
// Save file header
{
   /*fprintf( f, "%d,%d,%d,%d\n", AdcAccuracy, AdcRate, AdcPrefilter, AdcFilter);
   fprintf( f, "%d,%d,%d\n", ModeMode, ModeBurstSize, ModeAveraging);
   fprintf( f, "%d,%d,%d\n", DetectionAveragingWindow, DetectionRange, DetectionStableWindow);
   fprintf( f, "%s\n", Comment);    */
                                  
   TWeight Zero =  GToTWeight(CalibrationZero);
   TWeight FullRange =  GToTWeight(CalibrationFullRange);

   fprintf( f, "%d,%d,%d\n", AdcRate, 0, 0);
   fprintf( f, "%d,%d,%d,%d\n", 0, ModeBurstSize, 0, 0);
   fprintf( f, "%d,%d\n", 0, 0);
   fprintf( f, "%lf,%d\n", CalibrationZero, Zero);
   fprintf( f, "%lf,%d\n", CalibrationFullRange, FullRange);
   fprintf( f, "%s\n", Comment);




   /*
   fprintf( f, "%d,%d,%d\n", Rate, Chop, Sinc3);
   fprintf( f, "%d,%d,%d,%d\n", ContinuousMode, SamplesCount, BridgeOnDelay, Space);
   fprintf( f, "%d,%d\n", Duration, Delay);
   fprintf( f, "%lf,%d\n", CalibrationZero, CalibrationRawZero);
   fprintf( f, "%lf,%d\n", CalibrationFullRange, CalibrationRawFullRange);
   fprintf( f, "%s\n", Comment);   */
} // SaveHeader



bool TBat2Detector::ReadCalibrationData() {
   if(!Protocol->Open(ComName)) {
      return false;
   }

   TWeight ZeroTWeight;
   dword RawZero;
   TWeight FullRangeTWeight;
   dword RawFullRange;

   if(!Protocol->ReadCalibrationData(&ZeroTWeight, &RawZero, &FullRangeTWeight, &RawFullRange)) {
      return false;
   }

   CalibrationZero = TWeightToG(ZeroTWeight);
   CalibrationRawZero = RawZero;
   CalibrationFullRange = TWeightToG(FullRangeTWeight);
   CalibrationRawFullRange = RawFullRange;

   return true;
}

bool TBat2Detector::Calibrate() {
   if(!Protocol->Open(ComName)) {
      return false;
   }

   if( !SetParameters()){
      return( FALSE);
   }

   StopReceive = FALSE;

   if( !Protocol->StartCalibration(GToTWeight(CalibrationZero), GToTWeight(CalibrationFullRange), CalibrationDelay, CalibrationDuration)){
      return( FALSE);
   }

   bool Success;
   TWeight Zero;
   dword RawZero;
   TWeight FullRange;
   dword RawFullRange;

   do {
      Application->ProcessMessages();  // windows message loop

      if(!Protocol->CalibrationReply(&Success, &Zero, &RawZero, &FullRange, &RawFullRange)) {
         continue;
      }

      if(Success) {
         CalibrationZero = TWeightToG(Zero);
         CalibrationFullRange = TWeightToG(FullRange);
         CalibrationRawZero = RawZero;
         CalibrationRawFullRange = RawFullRange;
         
         return true;
      } else {
         return false;
      }
   } while( !StopReceive);

   return false;
}

int TBat2Detector::FileGetSamples(double *Samples) {
   
}
