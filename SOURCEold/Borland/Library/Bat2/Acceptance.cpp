//******************************************************************************
//
//   Acceptance.cpp     Acceptance
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************
 
#include "Acceptance.h"
#include <math.h>

//******************************************************************************
// Constructor
//******************************************************************************

TAcceptance::TAcceptance()
// Constructor
{
   _IsInit = false;
   Target = 1.0;
   Above = 30.0;
   Under = 30.0;
   Mode = DETECT_BOTH;
}

//******************************************************************************
// Destructor
//******************************************************************************

TAcceptance::~TAcceptance() {
}

//******************************************************************************
// Start
//******************************************************************************

bool TAcceptance::Start() {
   LastDetected = -10000000;
   _IsInit = true;
   return true;
}

//******************************************************************************
// Accept
//******************************************************************************

bool TAcceptance::Accept(double StableWeight, TAcceptedSample *Sample)
// Detect stable weight
{
double Delta;

   if( !_IsInit){
      return false;
   }

   Delta = StableWeight - LastDetected;      // weight increment
   LastDetected = StableWeight;              // save as last weight
   if( fabs( Delta) > Target + (Above * Target / 100)){
      return false;                          // step to large
   }
   if( fabs( Delta) < Target - (Under * Target / 100)){
      return false;                          // step to small
   }
   Sample->Direction = DETECT_ENTER;
   switch( Mode){
      case DETECT_ENTER :
         if( Delta < 0) {
            return false;                    // leave
         }
         break;

      case DETECT_LEAVE :
         if( Delta > 0) {
            return false;                    // enter
         }
         Delta     = -Delta;           // correct signum
         Sample->Direction = DETECT_LEAVE;
         break;

      case DETECT_BOTH  :
         if( Delta < 0){
            Delta     = -Delta;        // correct signum
            Sample->Direction = DETECT_LEAVE;
         }
         break;                        // always save
   }

   Sample->Weight = Delta;

   return true;
} // DetectWeight

//******************************************************************************
// Stop
//******************************************************************************

void TAcceptance::Stop() {
}
