//******************************************************************************
//
//   Histogram.h     Histogram
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#ifndef __Histogram_H__
#define __Histogram_H__

#include <stdio.h>
#include <Series.hpp>

class THistogram {
public :
   THistogram(int _BarCount, TBarSeries *Series, double _Min, double _Max);
   // Constructor

   ~THistogram();
   // Destructor

   void Clear();

   void Add(double Num);

   void Draw();
//------------------------------------------------------------------------------
protected :
   int GetHistogramBar(double Num);
   TBarSeries *MySeries;
   int BarCount;
   double Min;
   double Max;
   int *Bars;
};

#endif
 