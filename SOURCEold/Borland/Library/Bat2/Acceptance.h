//******************************************************************************
//
//   Acceptance.h     Acceptance
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#ifndef __Acceptance_H__
#define __Acceptance_H__

#include <stdio.h>

class TAcceptance {
public :
// detection mode
typedef enum {
   DETECT_ENTER,
   DETECT_LEAVE,
   DETECT_BOTH,
} TDetectionMode;

typedef struct {
   double Weight;
   TDetectionMode Direction;
} TAcceptedSample;

   TAcceptance();
   // Constructor

   ~TAcceptance();
   // Destructor

   bool Start();

   bool Accept(double StableWeight, TAcceptedSample *Sample);

   void Stop();

   double Target;
   double Above;
   double Under;
   TDetectionMode Mode;
//------------------------------------------------------------------------------
protected :
   bool _IsInit;
   double LastDetected;
};

#endif