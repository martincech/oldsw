//******************************************************************************
//
//   Histogram.cpp     Histogram
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#include <stdlib.h>
#include "Histogram.h"


//******************************************************************************
// Constructor
//******************************************************************************

THistogram::THistogram(int _BarCount, TBarSeries *_Series, double _Min, double _Max) {
   BarCount = _BarCount;
   MySeries = _Series;
   Min = _Min;
   Max = _Max;

   Bars = (int *)malloc(_BarCount * sizeof(_BarCount));
}

THistogram::~THistogram() {
   if(!Bars) {
      return;
   }

   free(Bars);
}

void THistogram::Add(double Num) {


   int Bar = GetHistogramBar(Num);

   if(Bar == -1) {
      return;
   }

   Bars[Bar]++;
}
void THistogram::Clear() {
   for(int i = 0 ; i < BarCount ; i++) {
      Bars[i] = 0;
   }
}

void THistogram::Draw() {
   char StringBuffer[20];
   double Weight;
   double WeightPerBar = (Max - Min) / BarCount;

   MySeries->Clear();

   for(int i = 0 ; i < BarCount ; i++) {
      Weight = Min + i * WeightPerBar;
      sprintf(StringBuffer, "%0.3f", Weight);
      MySeries->AddXY(Min + i * WeightPerBar, Bars[i], StringBuffer, clTeeColor);
   }
}

int THistogram::GetHistogramBar(double Num) {
   double WeightPerBar = (Max - Min) / BarCount;

   for(double i = Min, j = 0; i < Max, j < BarCount; i += WeightPerBar, j++) {
      if(Num - i < WeightPerBar) {
         return j;
      }
   }

   return -1;
}
