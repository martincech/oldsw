//******************************************************************************
//
//   Properties.cpp   Tester Configuration Dialog
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "uni.h"

#include "Properties.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TPropertiesForm *PropertiesForm;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TPropertiesForm::TPropertiesForm(TComponent* Owner)
        : TForm(Owner)
{
} // TTesterPropertiesForm

//******************************************************************************
// Zobrazeni dialogu
//******************************************************************************

bool __fastcall TPropertiesForm::Execute()
// Zobrazi dialog editovani parametru
{
   // Inicializace prvku :
   PropertiesPort->ItemIndex    = PropertiesPort->Items->IndexOf( Workplace->PortName);
   PropertiesDisplay->ItemIndex = (int)Workplace->Mode;
   PropertiesRxTimeout->Text    = IntToStr( Workplace->ReceiveTimeout);
   PropertiesDataSize->Text     = IntToStr( Workplace->DataSize);
   // Default Button je Cancel :
   this->ActiveControl = PropertiesCancel;
   // Zobrazeni dialogu :
   if( ShowModal() != mrOk){
      return( false);  // cancel
   }
   // Zmena nastaveni :
   Workplace->Mode           = (TCrtLogger::TDisplayMode)PropertiesDisplay->ItemIndex;
   Workplace->PortName       = PropertiesPort->Text;
   Workplace->ReceiveTimeout = PropertiesRxTimeout->Text.ToIntDef(0);
   Workplace->DataSize       = PropertiesDataSize->Text.ToIntDef(1024);
   return( true);
} // Execute

//******************************************************************************
// COM Settings
//******************************************************************************

void __fastcall TPropertiesForm::PortSettingsClick( TObject *Sender)
{
   UartSetupDialog->Execute( *Workplace->Parameters, "COM parameters");
} // PortSettingsClick


