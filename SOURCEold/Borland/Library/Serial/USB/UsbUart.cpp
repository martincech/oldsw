//******************************************************************************
//
//   UsbUart.cpp  Serial via USB port services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "UsbUart.h"
#include <stdio.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

// Check API return value :

#define if_fterr( status)  if( (status) != FT_OK)

#define DEFAULT_LATENCY 16      // default latency is 16ms

//******************************************************************************
// Locator
//******************************************************************************

bool TUsbUart::Locate( TName Name, TIdentifier &Identifier)
// Find device by <Name>, returns <Identifier>
{
   // Count of FTDI devices :
   DWORD      NumDevices;
   if_fterr( FT_ListDevices( &NumDevices, NULL, FT_LIST_NUMBER_ONLY)){
      return( false);                  // unable get
   }
   if( NumDevices == 0){
      return( false);                  // no devices
   }

   // Search by device name
   int   Count    = 0;                 // count of devices found
   char  DeviceName[ 256];             // device name
   DWORD UsbIndex = -1;                // index of last device
   for( DWORD i = 0; i < NumDevices; i++){
      if_fterr( FT_ListDevices( (PVOID)i, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
         continue;
      }
      if( Name != DeviceName){
         continue;
      }
      Count++;
      UsbIndex = i;
   }
   if( Count == 0){
      return( false);                  // no device
   }
   if( Count > 1){
      return( false);                  // more devices
   }
   Identifier = UsbIndex;              // one device only
   return( true);
} // Locate

//******************************************************************************
// Access rights
//******************************************************************************

bool TUsbUart::Access( TIdentifier Identifier)
// Check device access by <Identifier>
{
   FT_HANDLE TmpHandle;
   if_fterr( FT_Open( Identifier, &TmpHandle)){
      return( false);
   }
   FT_Close( TmpHandle);
   return( true);
} // Access

//******************************************************************************
// Constructor
//******************************************************************************

TUsbUart::TUsbUart() : TUart()
// Constructor
{
   FIdentifier   = INVALID_IDENTIFIER;
   FHandle       = INVALID_HANDLE_VALUE;
   FDtr = 0;
   FRts = 0;
   FTxD = 0;
   FParameters.BaudRate  = 9600;
   FParameters.DataBits  = 8;
   FParameters.StopBits  = 10;
   FParameters.Parity    = NO_PARITY;
   FParameters.Handshake = NO_HANDSHAKE;
   ImmediateRead         = YES;
   FTxLatency            = DEFAULT_LATENCY;
} // TUsbUart

//******************************************************************************
// Destructor
//******************************************************************************

TUsbUart::~TUsbUart()
// Destructor
{
   Close();
} // ~TUsbUart


//******************************************************************************
// Open
//******************************************************************************

bool TUsbUart::Open( TIdentifier Identifier)
// Open device by <Identifier>
{
   if( Identifier == INVALID_IDENTIFIER){
      IERROR;                          // invalid access
   }
   if( IsOpen){
      Close();                         // close previous
   }
   // Check text data first (inaccessible after open) :
   char  DeviceName[ 256];
   if_fterr( FT_ListDevices( (PVOID)Identifier, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
      return( false);
   }
   FUsbName = TString( DeviceName);
   char SerialNumber[ 32];
   if_fterr( FT_ListDevices( (PVOID)Identifier, SerialNumber, FT_LIST_BY_INDEX | FT_OPEN_BY_SERIAL_NUMBER)){
      return( false);
   }
   FSerialNumber = TString( SerialNumber);
   // Now open it :
   if_fterr( FT_Open( Identifier, &FHandle)){
      return( false);
   }
   FT_SetLatencyTimer( FHandle, FTxLatency);    // device latency
   FIdentifier = Identifier;
   return( true);
} // Open

//******************************************************************************
// Close
//******************************************************************************

void TUsbUart::Close()
// Close device
{
   if( IsOpen){
      FT_W32_CloseHandle( FHandle);
   }
   FHandle     = INVALID_HANDLE_VALUE;
   FIdentifier = INVALID_IDENTIFIER;
} // Close

//******************************************************************************
// Write
//******************************************************************************

int TUsbUart::Write( void *Data, int Length)
// Write <Data> of size <Length>, returns written length (or 0 if error)
{
   if( !IsOpen){
      IERROR;
   }
   DWORD w;
   if( !FT_W32_WriteFile( FHandle, Data, Length, &w, NULL)){
      return( 0);   // timeout or error
   }
   return( w);
} // Write

//******************************************************************************
// Write Wait
//******************************************************************************

bool TUsbUart::WriteWait()
// Write for data trasmission
{
DWORD CommEvent;

   // wait for transmitter empty :
   if( !FT_W32_WaitCommEvent( FHandle, &CommEvent, NULL)){
      return( false);
   }
   // last character may be transmitted :
   if( FParameters.BaudRate >= 9600){
      Sleep( 1);                       // fixed 1ms for last character
      return( true);
   }
   Sleep( 1000 * 10 / FParameters.BaudRate + 1);  // wait for last character
   return( true);
} // WriteWait

//******************************************************************************
// Read
//******************************************************************************

int TUsbUart::Read( void *Data, int Length)
// Read data <Data> of size <Length>, returns true length (or 0 if error)
{
   if( !IsOpen){
      IERROR;
   }
   DWORD r;
   if( ImmediateRead){
      // RxNowait
      DWORD Available;
      if_fterr( FT_GetQueueStatus( FHandle, &Available)){
         return( 0);
      }
      if( Available == 0){
         return( 0);                   // queue empty
      }
      if( Available < (DWORD)Length){
         Length = Available;           // short read
      }
      if( !FT_W32_ReadFile( FHandle, Data, Length, &r, NULL)){
         return( 0);
      }
      return( r);
   }
   // RxWait, wait for first character :
   if( !FT_W32_ReadFile( FHandle, Data, 1, &r, NULL)){
      return( 0);
   }
   if( r != 1){
      return( 0);
   }
   if( Length == 1){
      return( 1);            // requested 1 byte only
   }
   // read remainder :
   Length--;
   ((byte *)Data)++;
   if( !FT_W32_ReadFile( FHandle, Data, Length, &r, NULL)){
      return( 0);
   }
   return( r+1);
} // Read

//******************************************************************************
// Flush
//******************************************************************************

void TUsbUart::Flush()
// Make input/output queue empty
{
   if( !IsOpen){
      IERROR;
   }
   /* with special thanks to FTDI :
   FT_W32_PurgeComm( FHandle, PURGE_RXCLEAR | PURGE_TXCLEAR);
   */
   // Wait for data :
   for( int i = 0; i < 10; i++){
      FT_Purge( FHandle, FT_PURGE_RX | FT_PURGE_TX);
      DWORD Available;
      if_fterr( FT_GetQueueStatus( FHandle, &Available)){
         return;
      }
      if( Available == 0){
         return;
      }
      Sleep( 50);
   }
   IERROR;        // unable to make empty
} // Flush

//******************************************************************************
// Set Rx Timing
//******************************************************************************

void TUsbUart::SetRxNowait()
// Set timing of receiver - return collected data immediately
{
   if( !IsOpen){
      IERROR;
   }
   ImmediateRead = true;
   FTTIMEOUTS  to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   to.ReadIntervalTimeout         =  MAXDWORD; // without waiting
   to.ReadTotalTimeoutConstant    =  0;
   to.ReadTotalTimeoutMultiplier  =  0;
   FT_W32_SetCommTimeouts( FHandle,&to);
} // SetRxNowait

void TUsbUart::SetRxWait( int TotalTime, int IntercharacterTime)
// Set timing of receiver - waits for first character up to <TotalTime>
// returns after <IntercharacterTime> break
{
   if( !IsOpen){
      IERROR;
   }
   if( TotalTime == 0 && IntercharacterTime == 0){
      IERROR;
   }
   ImmediateRead = false;
   if( TotalTime <= IntercharacterTime){
      // too short for Windows OS
      TotalTime = 20 * IntercharacterTime;     // patch it
   }
   FTTIMEOUTS  to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   to.ReadIntervalTimeout         =  0;
   to.ReadTotalTimeoutMultiplier  =  IntercharacterTime;
   to.ReadTotalTimeoutConstant    =  TotalTime;
   FT_W32_SetCommTimeouts( FHandle,&to);
} // SetRxWait

//******************************************************************************
// Set Parameters
//******************************************************************************

bool  TUsbUart::SetParameters( const TParameters &Parameters)
// Set parameters of communication
{
   if( !IsOpen){
      IERROR;
   }
   if_fterr( FT_SetBaudRate( FHandle, Parameters.BaudRate)){
      return( false);
   }
   // Translate Byte size :
   UCHAR WordBits;
   switch( Parameters.DataBits){
      case 7 :
         WordBits = FT_BITS_7;
         break;
      case 8 :
         WordBits = FT_BITS_8;
         break;
      default :
         return( false); // invalid size
   }
   // Translate Stop Bits :
   UCHAR StopBits;
   switch( Parameters.StopBits){
      case 10 :
         StopBits = FT_STOP_BITS_1;
         break;
      case 20 :
         StopBits = FT_STOP_BITS_2;
         break;
      default :
         return( false);  // invalid stop bits count
   }
   // Translate Parity :
   UCHAR Parity;
   switch( Parameters.Parity){
      case NO_PARITY :
         Parity = FT_PARITY_NONE;
         break;
      case ODD_PARITY :
         Parity = FT_PARITY_ODD;
         break;
      case EVEN_PARITY :
         Parity = FT_PARITY_EVEN;
         break;
      case MARK_PARITY :
         Parity = FT_PARITY_MARK;
         break;
      case SPACE_PARITY :
         Parity = FT_PARITY_SPACE;
         break;
      IDEFAULT
   }
   if_fterr( FT_SetDataCharacteristics( FHandle, WordBits, StopBits, Parity)){
      return( false);
   }
   USHORT FlowControl;
   UCHAR  XonChar  = 11;
   UCHAR  XoffChar = 13;
   switch( Parameters.Handshake){
      case NO_HANDSHAKE :
         FlowControl = FT_FLOW_NONE;
         break;
      case HARDWARE_HANDSHAKE :
         FlowControl = FT_FLOW_RTS_CTS;
         break;
      case XON_XOFF_HANDSHAKE :
         FlowControl = FT_FLOW_XON_XOFF;
         break;
      case HALF_DUPLEX_HANDSHAKE :
         FlowControl = FT_FLOW_NONE;
         break;
      IDEFAULT
   }
   if_fterr( FT_SetFlowControl( FHandle, FlowControl, XonChar, XoffChar)){
      return( false);
   }
   FParameters = Parameters;   // remember
   // Safe timimg :
   SafeTxTiming();
   SetRxNowait();
   // prepare Tx empty event catching :
   if( !FT_W32_SetCommMask( FHandle, EV_TXEMPTY)){
      return( false);
   }
   return( true);
} // SetParameters

//******************************************************************************
// Get Parameters
//******************************************************************************

void TUsbUart::GetParameters( TParameters &Parameters)
// Get parameters of communication
{
   Parameters = FParameters;
} // GetParameters

#ifdef __PERSISTENT__
//******************************************************************************
// Load
//******************************************************************************

#define PLoadInteger( name)   FParameters.name = Memory->LoadInteger(#name);
#define PLoadIntegerCast( name, cast) \
                              FParameters.name = (cast)Memory->LoadInteger(#name);

bool TUsbUart::Load( TObjectMemory *Memory)
// Load setup from <Memory>
{
   TName FullName = GetFullName();
   if( !Memory->Locate( FullName)){
      return( false);  // unable to find
   }
   PLoadInteger( BaudRate);
   PLoadInteger( DataBits);
   PLoadInteger( StopBits);
   PLoadIntegerCast( Parity, TParity);
   PLoadIntegerCast( Handshake, THandshake);
   SetParameters( FParameters);
   return( true);
} // Load

//******************************************************************************
// Save
//******************************************************************************

#define PSaveInteger( name)  Memory->SaveInteger(#name,FParameters.name)

void TUsbUart::Save( TObjectMemory *Memory)
// Save setup to <Memory>
{
   TName FullName = GetFullName();
   Memory->Create( FullName);
   PSaveInteger( BaudRate);
   PSaveInteger( DataBits);
   PSaveInteger( StopBits);
   PSaveInteger( Parity);
   PSaveInteger( Handshake);
} // Save

//******************************************************************************
// Get Full Name
//******************************************************************************

TName TUsbUart::GetFullName()
// Get full device name
{
   TName FullName = SERIAL_NAME + "/USB";
   return( FullName);
} // GetFullName
#endif

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Name
//******************************************************************************

TName TUsbUart::GetName()
// Get device name by <Identifier>
{
   return( FUsbName);
} // GetName

//******************************************************************************
// Property Is Open
//******************************************************************************

bool TUsbUart::GetIsOpen()
// Check if device is opened
{
   return( FIdentifier != INVALID_IDENTIFIER);
} // GetIsOpen

//******************************************************************************
// Property CTS
//******************************************************************************

bool TUsbUart::GetCTS()
{
   return( GetModemStatus() & MS_CTS_ON);
} // GetCTS

//******************************************************************************
// Property DSR
//******************************************************************************

bool TUsbUart::GetDSR()
{
   return( GetModemStatus() & MS_DSR_ON);
} // GetDSR

//******************************************************************************
// Property RI
//******************************************************************************

bool TUsbUart::GetRI()
{
   return( GetModemStatus() & MS_RING_ON);
} // GetRI

//******************************************************************************
// Property DCD
//******************************************************************************

bool TUsbUart::GetDCD()
{
   return( GetModemStatus() & MS_RLSD_ON);
} // GetDCD

//******************************************************************************
// Property DTR
//******************************************************************************

void TUsbUart::SetDTR( bool Status)
{

   if( !IsOpen){
      IERROR;
   }
/* with special thanks to FTDI :
   FT_W32_EscapeCommFunction( FHandle,
                              Status ? SETDTR : CLRDTR);
*/
   if( Status){
      FT_SetDtr( FHandle);
   } else {
      FT_ClrDtr( FHandle);
   }
   FDtr = Status;
} // SetDTR

//******************************************************************************
// Property RTS
//******************************************************************************

void TUsbUart::SetRTS( bool Status)
{
   if( !IsOpen){
      IERROR;
   }
/* with special thanks to FTDI :
   FT_W32_EscapeCommFunction( FHandle,
                              Status ? SETRTS : CLRRTS);
*/
   if( Status){
      FT_SetRts( FHandle);
   } else {
      FT_ClrRts( FHandle);
   }
   FRts = Status;
} // SetRTS

//******************************************************************************
// Property TxD
//******************************************************************************

void TUsbUart::SetTxD( bool Status)
{
   if( !IsOpen){
      IERROR;
   }
/* with special thanks to FTDI :
   FT_W32_EscapeCommFunction( FHandle,
                              Status ? SETBREAK : CLRBREAK);
*/
   if( Status){
      FT_SetBreakOn( FHandle);
   } else {
      FT_SetBreakOff( FHandle);
   }
   FTxD = Status;
} // SetTxD

//******************************************************************************
// Tx timing
//******************************************************************************

void  TUsbUart::SafeTxTiming()
// Set safe timing of transmitter
{
   if( !IsOpen){
      IERROR;
   }
   FTTIMEOUTS to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   // Calculate via us :
   int CharTime;
   if( FParameters.BaudRate > 0 && FParameters.DataBits > 0){
      CharTime = (1000000 / FParameters.BaudRate) * FParameters.DataBits + 3;
      if( CharTime < 1){
         CharTime = 1;
      }
   } else {
      CharTime = 1;
   }
   // convert to ms :
   CharTime /= 1000;
   if( CharTime <= 0){
      CharTime = 1;
   }
   CharTime *= 2;   // security multiplier

   // maximum time for send :
   to.WriteTotalTimeoutMultiplier = CharTime;
   to.WriteTotalTimeoutConstant   = 100;
   FT_W32_SetCommTimeouts( FHandle, &to);
} // SafeTxTiming

//******************************************************************************
// Tx Latency
//******************************************************************************

void TUsbUart::SetTxLatency( int Latency)
// Set devices output buffer latency
{
   if( Latency < 1 || Latency > 255){
      FTxLatency = DEFAULT_LATENCY;             // out of range, set default
   } else {
      FTxLatency = Latency;
   }
   // device latency
   if_fterr( FT_SetLatencyTimer( FHandle, FTxLatency)){
      FTxLatency = DEFAULT_LATENCY;             // error, remember default
   }
} // SetTxLatency

//******************************************************************************
// Modem Status
//******************************************************************************

DWORD TUsbUart::GetModemStatus()
// Read modem status
{
   if( !IsOpen){
      return( 0);
   }
   DWORD d = 0;
   if( !FT_W32_GetCommModemStatus( FHandle, &d)){
      return( 0);
   }
   //
   return( d);
} // GetModemStatus

