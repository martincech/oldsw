//******************************************************************************
//
//   UsbUart.h    Serial via USB port services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef UsbUartH
#define UsbUartH

#ifndef UartH
   #include "../Uart.h"
#endif

#include "FTD2XX.h"

//******************************************************************************
// TUsbUart
//******************************************************************************

class TUsbUart : public TUart
{
public:
   //--- class functions :
   static bool Locate( TName Name, TIdentifier &Identifier);
   // Find device by <Name>, returns <Identifier>
   static bool Access( TIdentifier Identifier);
   // Check device access by <Identifier>

   TUsbUart();
   // Constructor
   virtual ~TUsbUart();
   // Destructor

   //--- inherited from TSerial :

   virtual bool Open( TIdentifier Identifier);
   // Open device by <Identifier>
   virtual void Close();
   // Close device
   virtual int Write( void *Data, int Length);
   // Write <Data> of size <Length>, returns written length (or 0 if error)
   virtual bool WriteWait();
   // Write for data trasmission
   virtual int Read( void *Data, int Length);
   // Read data <Data> of size <Length>, returns true length (or 0 if error)
   virtual void Flush();
   // Make input/output queue empty

#ifdef __PERSISTENT__
   virtual bool Load( TObjectMemory *Memory);
   // Load setup from <Memory>
   virtual void Save( TObjectMemory *Memory);
   // Save setup to <Memory>
#endif

   //--- USB functionality :

   virtual void SetRxNowait();
   // Set timing of receiver - returns collected data immediately
   virtual void SetRxWait( int ReplyTime, int IntercharacterTime);
   // Set timing of receiver :
   // If <IntercharacterTime> = 0 waits <ReplyTime> for whole message
   // If <IntercharacterTime> > 0 waits <ReplyTime> for first
   // character, next waits <IntercharacterTime> * count of characters
   bool SetParameters( const TParameters &Parameters);
   // Set parameters of communication
   void GetParameters( TParameters &Parameters);
   // Get parameters of communication

   // USB implementation :
  __property TString     SerialNumber          = {read=FSerialNumber};
  __property int         TxLatency             = {read=FTxLatency, write=SetTxLatency};

//------------------------------------------------------------------------------
protected:
   FT_HANDLE   FHandle;            // USB device handle
   bool        ImmediateRead;      // RxNowait
   TString     FUsbName;           // USB device name
   TString     FSerialNumber;      // USB device serial number
   int         FTxLatency;         // USB device output buffer latency

   TString GetSerialNumber();
   // Get device serial number

   virtual TName GetName();
   virtual bool  GetIsOpen();
   virtual bool  GetCTS();
   virtual bool  GetDSR();
   virtual bool  GetRI();
   virtual bool  GetDCD();
   virtual void  SetDTR(bool Status);
   virtual void  SetRTS(bool Status);
   virtual void  SetTxD(bool Status);

   DWORD GetModemStatus();
   // Read modem status
   void SafeTxTiming();
   // Set safe timing of transmitter
   void SetTxLatency( int Latency);
   // Set devices output buffer latency

#ifdef __PERSISTENT__
   TName GetFullName();
   // Get full device name
#endif
}; // TUsbUart

#endif
