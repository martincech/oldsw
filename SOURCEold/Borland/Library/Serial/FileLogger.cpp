//******************************************************************************
//
//   FileLogger.h   Raw data file logger
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "uni.h"

#include "FileLogger.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TFileLogger::TFileLogger( char *Prefix)
// Konstruktor
{
   File    = 0;
   FPrefix = Prefix;
   NewFileName();
   if( !QueryPerformanceFrequency( &Frequency)){
      Frequency.QuadPart = 1;
   }
} // TFileLogger

//******************************************************************************
// Zapis dat
//******************************************************************************

void TFileLogger::Write( TDataType Type, void *Buffer, int Length)
// Zapis dat
{
   if_null( File){
      return;
   }
   if( Length <= 0){
      return;
   }
   if_null( Buffer){
      IERROR;
   }
   Timestamp();
   switch( Type){
      case UNKNOWN :
         // neznama data
         fprintf( File, "UNKNOWN :\n");
         break;
      case RX_DATA :
         // prijata data
         fprintf( File, "RX :\n");
         break;
      case TX_DATA :
         // vyslana data
         fprintf( File, "TX :\n");
         break;
      case RX_GARBAGE :
         // prijate smeti
         fprintf( File, "RX GARBAGE :\n");
         break;
   }
   BinaryWrite( (char *)Buffer, Length);
} // Write

//******************************************************************************
// Zapis zpravy
//******************************************************************************

void TFileLogger::Report( char *Format, ...)
// Zapis textu
{
va_list Arg;

   if_null( File){
      return;
   }
   Timestamp();
   va_start( Arg, Format);
   vfprintf( File, Format, Arg);
} // Report

//******************************************************************************
// Binarni zapis dat
//******************************************************************************

void  TFileLogger::BinaryWrite( char *Buffer, int Length)
// Binarni zapis dat
{
   int i = 0;
   while( 1){
      for( int j = 0; j < 16; j++){
         fprintf( File, "%02X ", (unsigned char)Buffer[ i++]);
         if( i >= Length){
            goto next;
         }
      }
      fprintf( File, "\n");
   }
   next :
   fprintf( File, "\n");
} // BinaryWrite

//******************************************************************************
// Novy nazev souboru
//******************************************************************************

void TFileLogger::NewFileName()
// zkontroluj a uprav nazev souboru
{
AnsiString FileName;
bool       NewFile;

   unsigned short Year, Month, Day, Hour, Min, Sec, Msec;
   TDateTime dt = TDateTime::CurrentDateTime();
   dt.DecodeDate( &Year, &Month, &Day);
   dt.DecodeTime( &Hour, &Min, &Sec, &Msec);
   if( !File){
      // soubor jeste neni otevren
      FileHour = Hour;         // cas zalozeni
      NewFile  = true;
   } else {
      // stary je otevren
      if( FileHour != Hour){
         FileHour = Hour;      // cas zalozeni
         NewFile = true;       // uzavri hodinu
         fclose( File);
         File = 0;
      } else {
         NewFile = false;      // zapis do existujiciho
      }
   }
   if( NewFile){
      // zaloz novy soubor
      FileName.printf( "%s%02d%02d%02d_%02d.rwd", FPrefix.c_str(), Year %100, Month, Day, Hour);
      File = fopen( FileName.c_str(), "a");
      if( !File){
         return;
      }
      fprintf( File, "\n//*** New File %02d.%02d.%02d %02d:%02d:%02d.%03d\n",
               Day, Month, Year %100, Hour, Min, Sec, Msec);
      QueryPerformanceCounter( &Counter);
   }
} // NewFileName

//******************************************************************************
// Cas
//******************************************************************************

void TFileLogger::Timestamp()
// zapise cas
{
   LARGE_INTEGER NewCounter;
   QueryPerformanceCounter( &NewCounter);

   unsigned short Hour, Min, Sec, Msec;
   TDateTime dt = TDateTime::CurrentDateTime();
   dt.DecodeTime( &Hour, &Min, &Sec, &Msec);

   fprintf( File, "%02d:%02d.%03d %07Ld ", Min, Sec, Msec, ((NewCounter.QuadPart - Counter.QuadPart) * 1000000) / Frequency.QuadPart);
   Counter.QuadPart = NewCounter.QuadPart;
} // Timestamp

