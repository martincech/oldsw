//******************************************************************************
//
//   AsyUart.h    Asynchronous serial device RS232/RS422/RS485
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef AsyUartH
   #define AsyUartH

#ifndef UartH
   #include "../Uart.h"
#endif

//******************************************************************************
// TAsyUart
//******************************************************************************

class TAsyUart : public TUart
{
public :
   //--- class functions :
   static bool Locate( TName Name, TIdentifier &Identifier);
   // Find device by <Name>, returns <Identifier>
   static bool Access( TIdentifier Identifier);
   // Check device access by <Identifier>

   TAsyUart();
   // Constructor
   virtual ~TAsyUart();
   // Destructor

   //--- inherited from TSerial :

   virtual bool Open( TIdentifier Identifier);
   // Open device by <Identifier>
   virtual void Close();
   // Close device
   virtual int Write( void *Data, int Length);
   // Write <Data> of size <Length>, returns written length (or 0 if error)
   virtual int Read( void *Data, int Length);
   // Read data <Data> of size <Length>, returns true length (or 0 if error)
   virtual void Flush();
   // Make input/output queue empty
#ifdef __PERSISTENT__
   virtual bool Load( TObjectMemory *Memory);
   // Load setup from <Memory>
   virtual void Save( TObjectMemory *Memory);
   // Save setup to <Memory>
#endif

   //--- UART functionality :

   virtual void SetRxNowait();
   // Set timing of receiver - returns collected data immediately
   virtual void SetRxWait( int ReplyTime, int IntercharacterTime);
   // Set timing of receiver :
   // If <IntercharacterTime> = 0 waits <ReplyTime> for whole message
   // If <IntercharacterTime> > 0 waits <ReplyTime> for first
   // character, next waits <IntercharacterTime> * count of characters
   virtual bool SetParameters( const TParameters &Parameters);
   // Set parameters of communication
   virtual void GetParameters( TParameters &Parameters);
   // Get parameters of communication

   //--- Windows special :

   bool Clone( HANDLE Handle);
   // "Open" by <Handle>
protected :
   TIdentifier FIdentifier;
   //---------------- Uart implementation
   HANDLE      FHandle;
   bool        ImmediateRead;      // RxNowait
   OVERLAPPED  Overlapped;         // Async. data

   //--- Inherited from TSerial :
   virtual TName GetName();
   virtual bool  GetIsOpen();
   //--- Own property setters/getters :
   virtual bool  GetCTS();
   virtual bool  GetDSR();
   virtual bool  GetRI();
   virtual bool  GetDCD();
   virtual void  SetDTR(bool Status);
   virtual void  SetRTS(bool Status);
   virtual void  SetTxD(bool Status);

   void  SafeTxTiming();
   // Set safe timing of transmitter
   DWORD GetModemStatus();
   // Read modem status
   static char *ConvertComName( TIdentifier Identifier);
   // Encrypt com name
#ifdef __PERSISTENT__
   TName GetFullName();
   // Get full device name
#endif
}; // TAsyUart

#endif
