//******************************************************************************
//
//   ObjectMemory.cpp   Persistent memory for objects
//   Version 1.0        (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "ObjectMemory.h"
#include "../Unisys/Counter.h"

#include <stdio.h>

// Local constants :

#define CURRENT_VERSION 0x100
#define PVersion        "Version"

#pragma package(smart_init)

//******************************************************************************
// Constructor
//******************************************************************************

TObjectMemory::TObjectMemory()
// Constructor
{
   File          = new TIniFile( ChangeFileExt( Application->ExeName, ".ini" ));
   CurrentObject = ChangeFileExt( ExtractFileName( Application->ExeName), "");
   FEmpty        = false;
   int Version = LoadHexadecimal( PVersion);
   if( Version != CURRENT_VERSION){
       SaveHexadecimal( PVersion, CURRENT_VERSION);
       FEmpty = true;        // probably new file
   }
} // TObjectMemory

//******************************************************************************
// Destructor
//******************************************************************************

TObjectMemory::~TObjectMemory()
// Destructor
{
   if( !File){
      return;
   }
   Save();
   delete File;
} // TObjectMemory

//******************************************************************************
// Save
//******************************************************************************

void TObjectMemory::Save()
// Save data
{
   File->UpdateFile();
} // Save

//******************************************************************************
// Locate object
//******************************************************************************

bool TObjectMemory::Locate( TName Name)
// Locate object by <Name>
{
   if( !File->SectionExists( Name)){
      return( false);
   }
   CurrentObject = Name;
   return( true);
} // Locate

//******************************************************************************
// Create object
//******************************************************************************

void TObjectMemory::Create( TName Name)
// Create object by <Name>
{
   CurrentObject = Name;
} // Create

//******************************************************************************
// Integer
//******************************************************************************

void TObjectMemory::SaveInteger( TName Name, int Value)
{
   File->WriteInteger( CurrentObject, Name, Value);
} // SaveInteger

int TObjectMemory::LoadInteger( TName Name)
{
   return( File->ReadInteger( CurrentObject, Name, 0));
} // LoadInteger

//******************************************************************************
// Hexadecimal
//******************************************************************************

void TObjectMemory::SaveHexadecimal( TName Name, dword Value)
{
   AnsiString SValue;
   SValue = IntToHex( (int)Value, 1);
   File->WriteString( CurrentObject, Name, SValue);
} // SaveHexadecimal

dword TObjectMemory::LoadHexadecimal( TName Name)
{
   AnsiString SValue;
   SValue = File->ReadString( CurrentObject, Name, "0");
   dword Value = 0;
   sscanf( SValue.c_str(), "%x", &Value);
   return( Value);
} // LoadHexadecimal

//******************************************************************************
// String
//******************************************************************************

void TObjectMemory::SaveString( TName Name, TString Value)
{
   File->WriteString( CurrentObject, Name, Value);
} // SaveString

TString TObjectMemory::LoadString( TName Name)
{
   return( File->ReadString( CurrentObject, Name, 0));
} // LoadString

//******************************************************************************
// Float
//******************************************************************************

void TObjectMemory::SaveFloat( TName Name, double Value)
{
   File->WriteFloat( CurrentObject, Name, Value);
} // SaveFloat

double TObjectMemory::LoadFloat( TName Name)
{
   return( File->ReadFloat( CurrentObject, Name, 0));
} // LoadFloat

//******************************************************************************
// Bool
//******************************************************************************

void TObjectMemory::SaveBool( TName Name, bool Value)
{
   File->WriteBool( CurrentObject, Name, Value);
} // SaveBool

bool TObjectMemory::LoadBool( TName Name)
{
   return( File->ReadBool( CurrentObject, Name, 0));
} // LoadBool

//******************************************************************************
// Integer
//******************************************************************************

void TObjectMemory::SaveDateTime( TName Name, TDateTime Value)
{
   File->WriteString( CurrentObject, Name, DTUnifiedString( Value));
} // SaveDateTime

TDateTime TObjectMemory::LoadDateTime( TName Name)
{
   AnsiString Date;
   Date = File->ReadString( CurrentObject, Name, "");
   return( DTUnifiedDateTime( Date));
} // LoadDateTime

