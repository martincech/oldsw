//******************************************************************************
//
//   DTUtil.cpp    Zpracovani data a casu
//   Version 0.0  (c) Vymos
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Counter.h"

//******************************************************************************
// Konverze DateTime
//******************************************************************************

AnsiString DTString( TDateTime dt)
// Konvertuje TDateTime na lokalizovany retezec
{
   if( (double)dt == 0.0){
      return( AnsiString( ""));
   }
   return( dt.DateString() + AnsiString( " ") + dt.TimeString());
} // DTString

//******************************************************************************
// Konverze DateTime na minuty
//******************************************************************************

AnsiString DTStringMin( TDateTime dt)
// Konvertuje TDateTime na lokalizovany retezec (presnost na minuty)
{
   if( (double)dt == 0.0){
      return( AnsiString( ""));
   }
   return( dt.DateString() + AnsiString( " ") + dt.FormatString( AnsiString("t")));
} // DTStringMin

//******************************************************************************
// Konverze DateTime
//******************************************************************************

AnsiString DTUnifiedString( TDateTime dt)
// Konvertuje TDateTime na unifikovany retezec
{
   if( (double)dt == 0.0){
      return( AnsiString( ""));
   }
   unsigned short Year, Month, Day, Hour, Min, Sec, Msec;
   dt.DecodeDate( &Year, &Month, &Day);
   dt.DecodeTime( &Hour, &Min, &Sec, &Msec);
   AnsiString Result;
   Result.printf( "%04d%02d%02d %02d%02d%02d", Year, Month, Day, Hour, Min, Sec);
   return( Result);
} // DTUnifiedString

#define ToN( i)  (p[ i] - '0')

TDateTime DTUnifiedDateTime( AnsiString Date)
// Konvertuje unifikovany retezec na TDateTime
{
   if( Date.Length() < 15){
      return( TDateTime( 0.0));
   }
   unsigned short Year, Month, Day, Hour, Min, Sec, Msec;
   char *p = Date.c_str();
   Year  = (unsigned short)(1000 * ToN(0) + 100 * ToN( 1) + 10 * ToN( 2) + ToN( 3));
   Month = (unsigned short)(10 * ToN( 4)  + ToN( 5));
   Day   = (unsigned short)(10 * ToN( 6)  + ToN( 7));
   Hour  = (unsigned short)(10 * ToN( 9)  + ToN( 10));
   Min   = (unsigned short)(10 * ToN( 11) + ToN( 12));
   Sec   = (unsigned short)(10 * ToN( 13) + ToN( 14));
   Msec = 0;
   return( TDateTime( Year, Month, Day) + TDateTime( Hour, Min, Sec, Msec));
} // DTUnifiedDateTime

