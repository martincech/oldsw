//******************************************************************************
//
//   Dt.h         Data/Time utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Dt_H__
   #define __Dt_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DtDef_H__
   #include "DtDef.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

TTimestamp DtEncode( TLocalTime *Local);
// Encode date/time <Local> to internal representation with DST

void DtDecode( TTimestamp Timestamp, TLocalTime *Local);
// Decode date/time from <Timestamp> to <Local> with DST

int DtDow( TTimestamp Timestamp);
// Returns day of week

int DtWeekOfMonth( TTimestamp Timestamp);
// Returns week of month (1..5)

TYesNo DtLastWeekOfMonth( TTimestamp Timestamp);
// Returns YES if <Timestamp> is in the last week of month

TYesNo DtLastDowOfMonth( TTimestamp Timestamp);
// Returns YES if <Timestamp> is last day of week of month

int DtDowOrder( TTimestamp Timestamp);
// Returns day of week order number in month

TYesNo DtDowLast( TTimestamp Timestamp);
// Return YES if <Timestamp> is the last day of week of month

TTimestamp DtAddDays( TTimestamp Timestamp, int Days);
// Returns date <Days> later, at same time

int DtValidTime( TLocalTime *Local);
// Check time validity. Returns 0 if OK, else wrong field index

int DtValid( TLocalTime *Local);
// Check date/time validity. Returns 0 if OK, else wrong field index

//------------------------------------------------------------------------------
// Internal use only
//------------------------------------------------------------------------------

TTimestamp DtCompose( TLocalTime *Local);
// Compose date/time <Local> to internal representation

void DtSplit( TTimestamp Timestamp, TLocalTime *Local);
// Split date/time from <Timestamp> to <Local>

#ifdef __cplusplus
}
#endif

#endif
