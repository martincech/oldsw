//******************************************************************************
//
//   MainBat.cpp  Bat2 bridge diagnostics
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainBat.h"
#include "../Library/Serial/UartSetup.h"
#include "../Library/Serial/CrtLogger.h"
#include "MBridgeDef.h"
#include <stdio.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define BRIDGE_DATA_SIZE 512

byte Data[ BRIDGE_DATA_SIZE];
byte CurrentSlot = 0xFF;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TBat2Com;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt             = new TCrt( MainMemo);
   Adapter->Logger = new TCrtLogger( Crt);
   MainPort->ItemIndex = 1;
   Adapter->Connect( MainPort->Text);
} // Create

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TMainForm::Restart()
// Open port
{
   Crt->Clear();
   TString PortName = MainPort->Text;
   if( !Adapter->Connect( PortName)){
      Crt->printf( "Unable open %s adapter\n", PortName.c_str());
      return;
   }
   Crt->printf( "Open %s\n", PortName.c_str());
} // Restart

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::MainSetupClick(TObject *Sender)
{
   TUartSetupDialog *Dialog = new TUartSetupDialog( this);
   TUart::TParameters Parameters;
   Parameters = *Adapter->Parameters;
   if( !Dialog->Execute( Parameters, MainPort->Text + " Setup")){
      return;
   }
   *Adapter->Parameters = Parameters;
   Restart();
} // MainSetupClick

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainPortChange(TObject *Sender)
{
   Restart();
} // MainPortChange

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::MainSendClick(TObject *Sender)
{
   unsigned DataOffset = MainData->Text.ToInt();
   unsigned DataSize   = MainSize->Text.ToInt();
   for( int i = 0; i < BRIDGE_DATA_SIZE; i++){
      Data[ i] = (byte)(i + DataOffset);
   }
   bool Cycle = MainCycle->Checked;
   do {
      if( !Adapter->Send( Data, DataSize)){
         Crt->printf( "Unable send\n");
         return;
      }
      int RLength;
      if( !Adapter->Receive( Data, &RLength)){
         Crt->printf( "Reply error\n");
      } else {
         Crt->printf( "Reply : Length %d\n", RLength);
      }
      Application->ProcessMessages();
      Cycle = MainCycle->Checked;
   } while( Cycle);
   Crt->printf( "Stopped...\n");
} // MainSendClick

//------------------------------------------------------------------------------
// Commands
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnMaleDataClick(TObject *Sender)
{
TBridgeCommandDataSet Cmd;
TBridgeReplyDataSet   *Reply;

   Cmd.Command = BRIDGE_COMMAND_MALE_DATA_SET;
   byte *p = (byte *)&Cmd.Data;
   for( int i = 0; i < sizeof( TBat2Data); i++){
      *p = i  + 1;
      p++;
   }
   if( !Adapter->Send( &Cmd, sizeof( TBridgeCommandDataSet))){
      Crt->printf( "Unable send\n");
      return;
   }
   int RLength;
   if( !Adapter->Receive( Data, &RLength)){
      Crt->printf( "Reply error\n");
      return;
   }
   Reply = (TBridgeReplyDataSet *)Data;
   Crt->printf( "Reply OK %02X\n", Reply->Reply);
} // BtnMaleDataClick

//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnFemaleDataClick(TObject *Sender)
{
TBridgeCommandDataSet Cmd;
TBridgeReplyDataSet   *Reply;

   Cmd.Command = BRIDGE_COMMAND_FEMALE_DATA_SET;
   byte *p = (byte *)&Cmd.Data;
   for( int i = 0; i < sizeof( TBat2Data); i++){
      *p = i  + 1;
      p++;
   }
   if( !Adapter->Send( &Cmd, sizeof( TBridgeCommandDataSet))){
      Crt->printf( "Unable send\n");
      return;
   }
   int RLength;
   if( !Adapter->Receive( Data, &RLength)){
      Crt->printf( "Reply error\n");
      return;
   }
   Reply = (TBridgeReplyDataSet *)Data;
   Crt->printf( "Reply OK %02X\n", Reply->Reply);
} // BtnFemaleDataClick

//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnSmsCountClick(TObject *Sender)
{
TBridgeCommandSmsCount Cmd;
TBridgeReplySmsCount   *Reply;

   Cmd.Command = BRIDGE_COMMAND_SMS_COUNT;
   if( !Adapter->Send( &Cmd, sizeof( TBridgeCommandSmsCount))){
      Crt->printf( "Unable send\n");
      return;
   }
   int RLength;
   if( !Adapter->Receive( Data, &RLength)){
      Crt->printf( "Reply error\n");
      return;
   }
   Reply = (TBridgeReplySmsCount *)Data;
   Crt->printf( "Reply count : %d\n", Reply->Count);
} // BtnSmsCountClick
//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnSmsPhoneGetClick(TObject *Sender)
{
TBridgeCommandSmsPhoneGet  Cmd;
TBridgeReplySmsPhoneGet   *Reply;

   Cmd.Command = BRIDGE_COMMAND_SMS_PHONE_GET;
   if( !Adapter->Send( &Cmd, sizeof( TBridgeCommandSmsPhoneGet))){
      Crt->printf( "Unable send\n");
      return;
   }
   int RLength;
   if( !Adapter->Receive( Data, &RLength)){
      Crt->printf( "Reply error\n");
      return;
   }
   Reply = (TBridgeReplySmsPhoneGet *)Data;
   CurrentSlot = Reply->Slot;
   if( Reply->Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      Crt->printf( "Reply slot : INVALID\n");
   } else {
      Crt->printf( "Reply slot : %d\n", Reply->Slot);
   }
} // BtnSmsPhoneGetClick
//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnSmsTextGetClick(TObject *Sender)
{
TBridgeCommandSmsTextGet  Cmd;
TBridgeReplySmsTextGet   *Reply;

   Cmd.Command = BRIDGE_COMMAND_SMS_TEXT_GET;
   if( !Adapter->Send( &Cmd, sizeof( TBridgeCommandSmsTextGet))){
      Crt->printf( "Unable send\n");
      return;
   }
   int RLength;
   if( !Adapter->Receive( Data, &RLength)){
      Crt->printf( "Reply error\n");
      return;
   }
   Reply = (TBridgeReplySmsTextGet *)Data;
   CurrentSlot = Reply->Slot;
   if( Reply->Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      Crt->printf( "Reply slot : INVALID\n");
   } else {
      Crt->printf( "Reply slot : %d\n", Reply->Slot);
   }
} // BtnSmsTextGetClick
//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnSmsStatusClick(TObject *Sender)
{
TBridgeCommandSmsStatus  Cmd;
TBridgeReplySmsStatus   *Reply;

   Cmd.Command = BRIDGE_COMMAND_SMS_STATUS;
   Cmd.Status  = BAT2_MEGAVI_SMS_STATUS_SENDED;
   Cmd.Slot    = CurrentSlot;
   if( !Adapter->Send( &Cmd, sizeof( TBridgeCommandSmsStatus))){
      Crt->printf( "Unable send\n");
      return;
   }
   int RLength;
   if( !Adapter->Receive( Data, &RLength)){
      Crt->printf( "Reply error\n");
      return;
   }
   Reply = (TBridgeReplySmsStatus *)Data;
   Crt->printf( "Reply OK %02X\n", Reply->Reply);
} // BtnSmsStatusClick

