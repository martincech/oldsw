//******************************************************************************
//
//   Bat2Com.cpp   Bat2Com protocol adapter
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Bat2Com.h"
#include "uni.h"

#include "../Library/Serial/Uart/ComUart.h"
#include "../Library/Serial/Usb/UsbUart.h"

#define USB_NAME        "USB"                  // USB adapter identification
#define USB_DEVICE_NAME "Bat2 adapter"         // USB adapter name

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TBat2Com::TBat2Com()
// Constructor
{
   FPort   = new TComUart;
   FLogger = 0;

   FRxTimeout               = 1000;
   FPortName                = "";
   // default parameters :
   FParameters.BaudRate     = 9600;
   FParameters.DataBits     = 8;
   FParameters.StopBits     = 10;
   FParameters.Parity       = TUart::NO_PARITY;
   FParameters.Handshake    = TUart::NO_HANDSHAKE;
   FUsbDevice               = USB_DEVICE_NAME;
} // TBat2Com

//******************************************************************************
// Destructor
//******************************************************************************

TBat2Com::~TBat2Com()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TBat2Com

//******************************************************************************
// Connect
//******************************************************************************

bool TBat2Com::Connect( TName Port)
// Connect via <Port>
{
   // disconnect first :
   Disconnect();
   // remember name only :
   FPortName = Port;
   CheckConnect();        // try open, don't check success
   return( true);
} // Connect

//******************************************************************************
// Send
//******************************************************************************

bool TBat2Com::Send( void *Data, int Size)
// Send message
{
   if( !CheckConnect()){
      return( false);
   }
   Buffer[ BRIDGE_LEADER_OFFSET] = BRIDGE_LEADER_CHAR;
   Buffer[ BRIDGE_SIZE1_OFFSET]  = (byte)Size;
   Buffer[ BRIDGE_SIZE2_OFFSET]  = (byte)Size;
   memcpy( &Buffer[ BRIDGE_DATA_OFFSET], Data, Size);
   Buffer[ Size + BRIDGE_CRC_OFFSET] = CalcCrc( Size);

   int TotalSize = Size + BRIDGE_FRAME_SIZE;
   FPort->Flush();
   if( FPort->Write( Buffer, TotalSize) != TotalSize){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   // packet data :
   RwdTx( &Buffer, TotalSize);
   return( true);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TBat2Com::Receive( void *Data, int *Size)
// Receive message
{
int ReplySize, DataSize;

   if( !CheckConnect()){
      return( false);
   }
   if( FPort->Read( Buffer, 1) != 1){
      return( false);
   }
   if( Buffer[ BRIDGE_LEADER_OFFSET] != BRIDGE_LEADER_CHAR){
      RwdGarbage( Buffer, 1);
      return( false);
   }
   // size fields :
   DataSize  = 2;
   ReplySize = FPort->Read( &Buffer[ BRIDGE_SIZE1_OFFSET], DataSize);
   if(  ReplySize != DataSize){
      RwdGarbage( Buffer, ReplySize + 1);
      return( false);
   }
   if( Buffer[ BRIDGE_SIZE1_OFFSET] != Buffer[ BRIDGE_SIZE2_OFFSET]){
      RwdGarbage( Buffer, ReplySize + 1);
      return( false);
   }
   // read data inclusive CRC :
   DataSize  = Buffer[ BRIDGE_SIZE1_OFFSET];
   ReplySize = FPort->Read( &Buffer[ BRIDGE_DATA_OFFSET], DataSize + 1);
   if(  ReplySize != DataSize + 1){
      RwdGarbage( Buffer, ReplySize + 3);
      return( false);
   }
   ReplySize += 3;                    // inclusive leader, Size1, 2
   if( Buffer[ DataSize + BRIDGE_CRC_OFFSET] != CalcCrc( DataSize)){
      RwdGarbage( Buffer, ReplySize);
      return( false);
   }
   RwdRx( Buffer, ReplySize);
   memcpy( Data, &Buffer[ BRIDGE_DATA_OFFSET], DataSize);
   *Size = DataSize;
   return( true);
} // Receive

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Identifier
//******************************************************************************

TIdentifier TBat2Com::GetIdentifier()
// GetIdentifier
{
   if( !FPort){
      return( INVALID_IDENTIFIER);
   }
   return( FPort->Identifier);
} // GetIdentifier

//******************************************************************************
// Property Name
//******************************************************************************

TName TBat2Com::GetName()
// Get device name
{
   if( !FPort){
      return("No adapter");
   }
   return( FPort->Name);
} // GetName

//******************************************************************************
// Property IsOpen
//******************************************************************************

bool  TBat2Com::GetIsOpen()
// Check if device is opened
{
   if( !FPort){
      return( false);
   }
   return( FPort->IsOpen);
} // GetIsOpen

//******************************************************************************
// Check Connection
//******************************************************************************

bool TBat2Com::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( true);
   }
   if( FPort){
      IERROR;                          // always opened !
   }
   TIdentifier Identifier;
   TUart    *Uart;
   TUsbUart *Usb = 0;
   if( PortName == USB_NAME){
      // USB setup
      Usb = new TUsbUart;
      if( !Usb->Locate( FUsbDevice, Identifier)){
         delete Usb;
         return( false);
      }
      Uart = Usb;
   } else {
      // COM setup :
      TComUart *Com = new TComUart;
      if( !Com->Locate( PortName, Identifier)){
         delete Com;
         return( false);
      }
      Uart = Com;
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   // common init :
   Uart->SetParameters( FParameters);
   Uart->SetRxWait( FRxTimeout, 0);
   Uart->Flush();
   FPort = Uart;
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TBat2Com::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

//******************************************************************************
// CRC
//******************************************************************************

byte TBat2Com::CalcCrc( int Size)
// Calculate CRC
{
byte *Data;

   Data     = &Buffer[ BRIDGE_SIZE1_OFFSET];
   byte Crc = 0;
   Size    += 2;                  // add Size1, 2
   for( int i = 0; i < Size; i++){
      Crc += *Data;
      Data++;
   }
   return( ~Crc);
} // CalcCrc

