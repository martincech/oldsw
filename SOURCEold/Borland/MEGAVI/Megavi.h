//******************************************************************************
//
//   Megavi.h      MEGAVI protocol adapter
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef MegaviH
   #define MegaviH

#ifndef UartH
   #include "../Library/Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Library/../Serial/Logger.h"
#endif

#include "MegaviDef.h"

//******************************************************************************
// TQHBAdapter
//******************************************************************************

class TMegavi {
public :
   TMegavi();
   // Constructor
   ~TMegavi();
   // Destructor

   bool Connect( TName Port);
   // Connect via <Port>

   bool Send( unsigned Id, void *Data, int Size);
   // Send message

   bool Receive( unsigned &Id, void *Data, int Size);
   // Receive message

   __property TIdentifier Identifier           = {read=GetIdentifier};
   __property TName       Name                 = {read=GetName};
   __property bool        IsOpen               = {read=GetIsOpen};

   __property TUart              *Port         = {read=FPort};
   __property TString             PortName     = {read=FPortName};
   __property TLogger            *Logger       = {read=FLogger, write=FLogger};
   __property TUart::TParameters *Parameters   = {read=GetParameters};
   __property TString             UsbDevice    = {read=FUsbDevice, write=FUsbDevice};
//------------------------------------------------------------------------------

protected :
   TUart   *FPort;                     // connection port
   TString  FPortName;
   TLogger *FLogger;                   // raw data logger
   int      FRxTimeout;                // Rx timeout
   byte     Buffer[ 1024];             // packet buffer
   TUart::TParameters FParameters;     // Communication parameters
   TString  FUsbDevice;                // USB device name

   TIdentifier GetIdentifier();
   // GetIdentifier
   TName GetName();
   // Get device name
   bool  GetIsOpen();
   // Check if device is opened

   bool CheckConnect();
   // Check if adapter is ready
   void Disconnect();
   // Disconnect port
   byte CalcCrc( byte *Data, int Size);
   // Calculate CRC
   TUart::TParameters *GetParameters(){ return( &FParameters);}
}; // TMegavi

#endif
