//******************************************************************************
//
//   Bat2Com.h     Bat2Com protocol adapter
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef Bat2ComH
   #define Bat2ComH

#ifndef UartH
   #include "../Library/Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Library/../Serial/Logger.h"
#endif

#include "BridgeDef.h"

//******************************************************************************
// TQHBAdapter
//******************************************************************************

class TBat2Com {
public :
   TBat2Com();
   // Constructor
   ~TBat2Com();
   // Destructor

   bool Connect( TName Port);
   // Connect via <Port>

   bool Send( void *Data, int Size);
   // Send message

   bool Receive( void *Data, int *Size);
   // Receive message

   __property TIdentifier Identifier           = {read=GetIdentifier};
   __property TName       Name                 = {read=GetName};
   __property bool        IsOpen               = {read=GetIsOpen};

   __property TUart              *Port         = {read=FPort};
   __property TString             PortName     = {read=FPortName};
   __property TLogger            *Logger       = {read=FLogger, write=FLogger};
   __property TUart::TParameters *Parameters   = {read=GetParameters};
   __property TString             UsbDevice    = {read=FUsbDevice, write=FUsbDevice};
//------------------------------------------------------------------------------

protected :
   TUart   *FPort;                     // connection port
   TString  FPortName;
   TLogger *FLogger;                   // raw data logger
   int      FRxTimeout;                // Rx timeout
   byte     Buffer[ 1024];             // packet buffer
   TUart::TParameters FParameters;     // Communication parameters
   TString  FUsbDevice;                // USB device name

   TIdentifier GetIdentifier();
   // GetIdentifier
   TName GetName();
   // Get device name
   bool  GetIsOpen();
   // Check if device is opened

   bool CheckConnect();
   // Check if adapter is ready
   void Disconnect();
   // Disconnect port
   byte CalcCrc( int Size);
   // Calculate CRC
   TUart::TParameters *GetParameters(){ return( &FParameters);}
}; // TBat2Com

#endif
