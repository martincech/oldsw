//******************************************************************************
//
//   Main.cpp     QH Bus diagnostics
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "../Library/Serial/UartSetup.h"
#include "../Library/Serial/CrtLogger.h"
#include <stdio.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define MEGAVI_DATA_SIZE 250

byte Data[ MEGAVI_DATA_SIZE];

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TMegavi;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt             = new TCrt( MainMemo);
   Adapter->Logger = new TCrtLogger( Crt);
   MainPort->ItemIndex = 1;
   Adapter->Connect( MainPort->Text);
} // Create

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TMainForm::Restart()
// Open port
{
   Crt->Clear();
   TString PortName = MainPort->Text;
   if( !Adapter->Connect( PortName)){
      Crt->printf( "Unable open %s adapter\n", PortName.c_str());
      return;
   }
   Crt->printf( "Open %s\n", PortName.c_str());
} // Restart

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::MainSetupClick(TObject *Sender)
{
   TUartSetupDialog *Dialog = new TUartSetupDialog( this);
   TUart::TParameters Parameters;
   Parameters = *Adapter->Parameters;
   if( !Dialog->Execute( Parameters, MainPort->Text + " Setup")){
      return;
   }
   *Adapter->Parameters = Parameters;
   Restart();
} // MainSetupClick

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainPortChange(TObject *Sender)
{
   Restart();
} // MainPortChange

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::MainSendClick(TObject *Sender)
{
   unsigned Id         = MainId->Text.ToInt();
   unsigned DataOffset = MainData->Text.ToInt();
   unsigned DataSize   = MainSize->Text.ToInt();
   for( int i = 0; i < MEGAVI_DATA_SIZE; i++){
      Data[ i] = (byte)(i + DataOffset);
      if( Data[ i] == MEGAVI_LEADER_CHAR || Data[ i] == MEGAVI_TRAILER_CHAR){
         Data[ i] = 0x33;          // eliminate frame control characters
      }
   }
   bool Cycle = MainCycle->Checked;
   do {
      if( !Adapter->Send( Id, Data, DataSize)){
         Crt->printf( "Unable send\n");
         return;
      }
      unsigned RId;
      int      RLength;
      RLength = DataSize;
      if( !Adapter->Receive( RId, Data, RLength)){
         Crt->printf( "Reply error\n");
      } else {
         Crt->printf( "Reply : ID %02X Length %d\n", RId, RLength);
      }
      Application->ProcessMessages();
      Cycle = MainCycle->Checked;
   } while( Cycle);
   Crt->printf( "Stopped...\n");
} // MainSendClick

