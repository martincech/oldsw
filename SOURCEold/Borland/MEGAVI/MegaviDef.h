//******************************************************************************
//
//   MegaviDef.h   MEGAVI protocol definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef MegaviDefH
   #define MegaviDefH

#pragma pack( push, 1)                    // byte alignment
   #include "../AVR8/Inc/MegaviDef.h"     // protocol definition
#pragma pack( pop)                        // original alignment

#endif
