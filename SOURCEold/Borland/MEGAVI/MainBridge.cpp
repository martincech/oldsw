//******************************************************************************
//
//   Main.cpp     QH Bus diagnostics
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainBridge.h"
#include "../Library/Serial/UartSetup.h"
#include "../Library/Serial/CrtLogger.h"
#include <stdio.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define MEGAVI_DATA_SIZE 250

byte Data[ MEGAVI_DATA_SIZE];

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   MBridge = new TMBridge;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt                      = new TCrt( MainMemo);
   MBridge->Adapter->Logger = new TCrtLogger( Crt);
   MainPort->ItemIndex = 1;
//   MBridge->Connect( MainPort->Text);
   Restart();
} // Create

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TMainForm::Restart()
// Open port
{
   Crt->Clear();
   TString PortName = MainPort->Text;
   if( !MBridge->Connect( PortName)){
      Crt->printf( "Unable open %s adapter\n", PortName.c_str());
      return;
   }
   Crt->printf( "Open %s\n", PortName.c_str());
} // Restart

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::MainSetupClick(TObject *Sender)
{
   TUartSetupDialog *Dialog = new TUartSetupDialog( this);
   TUart::TParameters Parameters;
   Parameters = *MBridge->Adapter->Parameters;
   if( !Dialog->Execute( Parameters, MainPort->Text + " Setup")){
      return;
   }
   *MBridge->Adapter->Parameters = Parameters;
   Restart();
} // MainSetupClick

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainPortChange(TObject *Sender)
{
   Restart();
} // MainPortChange

//******************************************************************************
// Get version
//******************************************************************************

void __fastcall TMainForm::MainVersionClick(TObject *Sender)
{
unsigned Id, Major, Minor;

   try {
      Id = MainId->Text.ToInt();
   } catch( ...){
      Id = 10;
      MainId->Text = AnsiString( Id);
   }
   MBridge->MegaviId = Id;

   bool Cycle = MainCycle->Checked;
   do {
      if( !MBridge->VersionGet( &Major, &Minor)){
         Crt->printf( "Reply error\n");
      } else {
         Crt->printf( "Reply version : %d.%02d\n", Major, Minor);
      }
      Application->ProcessMessages();
      Cycle = MainCycle->Checked;
   } while( Cycle);
   Crt->printf( "Stopped...\n");
} // MainVersionClick

//------------------------------------------------------------------------------
// Get male data
//------------------------------------------------------------------------------

void __fastcall TMainForm::MainMaleDataClick(TObject *Sender)
{
unsigned  Id;
TBat2Data Data;
char      SmsRequest;

   try {
      Id = MainId->Text.ToInt();
   } catch( ...){
      Id = 10;
      MainId->Text = AnsiString( Id);
   }
   MBridge->MegaviId = Id;

   bool Cycle = MainCycle->Checked;
   do {
      if( !MBridge->MaleDataGet( &SmsRequest, &Data)){
         Crt->printf( "Reply error\n");
      } else {
         Crt->printf( "Reply : OK\n\n");
         DumpData( SmsRequest, &Data);
      }
      Application->ProcessMessages();
      Cycle = MainCycle->Checked;
   } while( Cycle);
   Crt->printf( "Stopped...\n");
} // MainMaleDataClick

//------------------------------------------------------------------------------
// Get female Data
//------------------------------------------------------------------------------

void __fastcall TMainForm::MainFemaleDataClick(TObject *Sender)
{
unsigned  Id;
TBat2Data Data;
char      SmsRequest;

   try {
      Id = MainId->Text.ToInt();
   } catch( ...){
      Id = 10;
      MainId->Text = AnsiString( Id);
   }
   MBridge->MegaviId = Id;

   bool Cycle = MainCycle->Checked;
   do {
      if( !MBridge->FemaleDataGet( &SmsRequest, &Data)){
         Crt->printf( "Reply error\n");
      } else {
         Crt->printf( "Reply : OK\n\n");
         DumpData( SmsRequest, &Data);
      }
      Application->ProcessMessages();
      Cycle = MainCycle->Checked;
   } while( Cycle);
   Crt->printf( "Stopped...\n");
} // MainFemaleDataClick

//------------------------------------------------------------------------------
// SMS send
//------------------------------------------------------------------------------

void __fastcall TMainForm::MainSmsClick(TObject *Sender)
{
unsigned  Id;
unsigned  Slot;

   try {
      Id = MainId->Text.ToInt();
   } catch( ...){
      Id = 10;
      MainId->Text = AnsiString( Id);
   }
   MBridge->MegaviId = Id;
   // phone number :
   char Target;
   Target = MainTarget->Text.c_str()[ 0];
/*
   unsigned  ch;
   for( int i = 0; i < BAT2_SMS_SIZE; i++){
      ch = ' ' + i;
      if( ch > '~'){
         ch -= '~' + 1;
         ch += ' ';
      }
      Sms.Text[ i] = (char)ch;
   }
*/
   // SMS text :
   char Text[ BAT2_SMS_SIZE + 1];
   strcpy( Text, MainSmsText->Text.c_str());
   if( !MBridge->SmsSend( Target, Text, &Slot)){
      Crt->printf( "Reply error\n");
      return;
   }
   if( Slot == BAT2_MEGAVI_SMS_TARGET_ERROR){
      Crt->printf( "Reply error : wrong SMS Target\n");
   } else if( Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      Crt->printf( "Reply error : all slots full\n");
   } else if( Slot == BAT2_MEGAVI_SMS_PHONEBOOK_ERROR){
      Crt->printf( "Reply error : phonebook corrupted\n");
   } else {
      Crt->printf( "Reply slot : %d\n", Slot);
      EdtSlot->Text = AnsiString( Slot);
   }
} // MainSmsClick

//------------------------------------------------------------------------------
// SMS status
//------------------------------------------------------------------------------

void __fastcall TMainForm::MainSmsStatusClick(TObject *Sender)
{
unsigned Id, Slot, Status;

   try {
      Id = MainId->Text.ToInt();
   } catch( ...){
      Id = 10;
      MainId->Text = AnsiString( Id);
   }
   try {
      Slot = EdtSlot->Text.ToInt();
   } catch( ...){
      Slot = 1;
      EdtSlot->Text = AnsiString( Slot);
   }
   MBridge->MegaviId = Id;

   bool Cycle = MainCycle->Checked;
   char StatusText[ 20];
   do {
      if( !MBridge->SmsStatusGet( Slot, &Status)){
         Crt->printf( "Reply error\n");
      } else {
         switch( Status){
            case BAT2_MEGAVI_SMS_STATUS_EMPTY :
               strcpy( StatusText, "EMPTY");
               break;
            case BAT2_MEGAVI_SMS_STATUS_PENDING :
               strcpy( StatusText, "PENDING");
               break;
            case BAT2_MEGAVI_SMS_STATUS_SENDED :
               strcpy( StatusText, "SENDED");
               break;
            case BAT2_MEGAVI_SMS_STATUS_ERROR :
               strcpy( StatusText, "ERROR");
               break;
            default :
               strcpy( StatusText, "???");
               break;
         }
         Crt->printf( "Reply status : '%s' (%d)\n", StatusText, Status);
      }
      Application->ProcessMessages();
      Cycle = MainCycle->Checked;
   } while( Cycle);
   Crt->printf( "Stopped...\n");
} // MainSmsStatusClick

//------------------------------------------------------------------------------
// Dump data
//------------------------------------------------------------------------------

void TMainForm::DumpData( char SmsRequest, TBat2Data *Data)
// print <Data> contents
{
   if( SmsRequest == BAT2_MEGAVI_SMS_REQUEST_INVALID){
      Crt->printf( "SMS request : <NONE>\n");
   } else if( SmsRequest == BAT2_MEGAVI_SMS_REQUEST_SHORT){
      Crt->printf( "SMS request : MEGAVI\n");
   } else {
      Crt->printf( "SMS request : MEGAVI '%c'\n", SmsRequest);
   }
   Crt->printf( "Target    : %d\n", Data->Statistics.Target);
   Crt->printf( "Count     : %d\n", Data->Statistics.Count);
   Crt->printf( "Average   : %d\n", Data->Statistics.Average);
   Crt->printf( "Gain      : %d\n", Data->Statistics.Gain);
   Crt->printf( "Sigma     : %d\n", Data->Statistics.Sigma);
   Crt->printf( "Uniformity: %d\n", Data->Statistics.Uniformity);
   for( int i = 0; i < BAT2_WEIGHT_COUNT; i++){
      if( Data->Weight[ i] == 0){
         break;
      }
      Crt->printf( "   Weight%02d : %d\n", i, Data->Weight[ i]);
   }
} // DumpData

