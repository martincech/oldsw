object MainForm: TMainForm
  Left = 375
  Top = 251
  AutoScroll = False
  Caption = 'Bat2 Test'
  ClientHeight = 547
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 534
    Top = 16
    Width = 25
    Height = 13
    Caption = 'Port :'
  end
  object Label4: TLabel
    Left = 115
    Top = 16
    Width = 60
    Height = 13
    Caption = 'Data Offset :'
  end
  object Label2: TLabel
    Left = 267
    Top = 16
    Width = 26
    Height = 13
    Caption = 'Size :'
  end
  object MainPort: TComboBox
    Left = 566
    Top = 13
    Width = 98
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    OnChange = MainPortChange
    Items.Strings = (
      'USB'
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15'
      'COM16'
      'COM17'
      'COM18'
      'COM19'
      'COM20'
      'COM21'
      'COM22'
      'COM23'
      'COM24'
      'COM25'
      'COM26'
      'COM27'
      'COM28'
      'COM29'
      'COM30')
  end
  object MainSetup: TButton
    Left = 670
    Top = 12
    Width = 76
    Height = 21
    Caption = 'Setup'
    TabOrder = 3
    OnClick = MainSetupClick
  end
  object MainMemo: TMemo
    Left = 0
    Top = 64
    Width = 646
    Height = 464
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 4
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 528
    Width = 761
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object MainSend: TButton
    Left = 432
    Top = 14
    Width = 76
    Height = 21
    Caption = 'Send'
    TabOrder = 1
    OnClick = MainSendClick
  end
  object MainData: TEdit
    Left = 179
    Top = 14
    Width = 73
    Height = 21
    TabOrder = 0
    Text = '0'
  end
  object MainSize: TEdit
    Left = 331
    Top = 14
    Width = 73
    Height = 21
    TabOrder = 6
    Text = '20'
  end
  object MainCycle: TCheckBox
    Left = 432
    Top = 40
    Width = 97
    Height = 17
    Caption = 'Cycle'
    TabOrder = 7
  end
  object BtnMaleData: TButton
    Left = 656
    Top = 64
    Width = 97
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Male Data Send'
    TabOrder = 8
    OnClick = BtnMaleDataClick
  end
  object BtnSmsCount: TButton
    Left = 656
    Top = 168
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'SMS Count'
    TabOrder = 9
    OnClick = BtnSmsCountClick
  end
  object BtnSmsPhoneGet: TButton
    Left = 656
    Top = 208
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'SMS Phone Get'
    TabOrder = 10
    OnClick = BtnSmsPhoneGetClick
  end
  object BtnSmsStatus: TButton
    Left = 656
    Top = 288
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'SMS Status'
    TabOrder = 11
    OnClick = BtnSmsStatusClick
  end
  object BtnSmsTextGet: TButton
    Left = 656
    Top = 248
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'SMS Text Get'
    TabOrder = 12
    OnClick = BtnSmsTextGetClick
  end
  object BtnFemaleData: TButton
    Left = 656
    Top = 104
    Width = 97
    Height = 25
    Caption = 'Female Data Send'
    TabOrder = 13
    OnClick = BtnFemaleDataClick
  end
end
