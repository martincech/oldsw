//******************************************************************************
//
//   MainBat.h       QH Bus diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainBatH
#define MainBatH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Library/Crt/Crt.h"
#include "Bat2Com.h"

#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TComboBox *MainPort;
   TLabel *Label1;
   TButton *MainSetup;
   TMemo *MainMemo;
   TStatusBar *StatusBar1;
   TButton *MainSend;
   TLabel *Label4;
   TEdit *MainData;
   TLabel *Label2;
   TEdit *MainSize;
   TCheckBox *MainCycle;
   TButton *BtnMaleData;
   TButton *BtnSmsCount;
   TButton *BtnSmsPhoneGet;
   TButton *BtnSmsStatus;
   TButton *BtnSmsTextGet;
   TButton *BtnFemaleData;
   void __fastcall Create(TObject *Sender);
   void __fastcall MainSetupClick(TObject *Sender);
   void __fastcall MainPortChange(TObject *Sender);
   void __fastcall MainSendClick(TObject *Sender);
   void __fastcall MemoResize(TObject *Sender);
   void __fastcall BtnMaleDataClick(TObject *Sender);
   void __fastcall BtnSmsCountClick(TObject *Sender);
   void __fastcall BtnSmsPhoneGetClick(TObject *Sender);
   void __fastcall BtnSmsStatusClick(TObject *Sender);
   void __fastcall BtnSmsTextGetClick(TObject *Sender);
   void __fastcall BtnFemaleDataClick(TObject *Sender);
private:	// User declarations
   TCrt          *Crt;
   TBat2Com      *Adapter;

   void __fastcall Restart();
   // Open port
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------

#endif
