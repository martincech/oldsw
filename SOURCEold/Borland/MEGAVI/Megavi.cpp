//******************************************************************************
//
//   Megavi.cpp    MEGAVI protocol adapter
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Megavi.h"
#include "uni.h"

#include "../Library/Serial/Uart/ComUart.h"
#include "../Library/Serial/Usb/UsbUart.h"

#define USB_NAME        "USB"                  // USB adapter identification
#define USB_DEVICE_NAME "MEGAVI adapter"       // USB adapter name

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TMegavi::TMegavi()
// Constructor
{
   FPort   = new TComUart;
   FLogger = 0;

   FRxTimeout               = 1000;
   FPortName                = "";
   // default parameters :
   FParameters.BaudRate     = 19200;
   FParameters.DataBits     = 8;
   FParameters.StopBits     = 10;
   FParameters.Parity       = TUart::MEGAVI_CONTROL_PARITY;
   FParameters.Handshake    = TUart::NO_HANDSHAKE;
   FUsbDevice               = USB_DEVICE_NAME;
} // TMegavi

//******************************************************************************
// Destructor
//******************************************************************************

TMegavi::~TMegavi()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TMegavi

//******************************************************************************
// Connect
//******************************************************************************

bool TMegavi::Connect( TName Port)
// Connect via <Port>
{
   // disconnect first :
   Disconnect();
   // remember name only :
   FPortName = Port;
   CheckConnect();        // try open, don't check success
   return( true);
} // Connect

//******************************************************************************
// Send
//******************************************************************************

bool TMegavi::Send( unsigned Id, void *Data, int Size)
// Send message
{
byte *p;

   if( !CheckConnect()){
      return( false);
   }
   p = (byte *)Data;
   Buffer[ MEGAVI_LEADER_OFFSET] = MEGAVI_LEADER_CHAR;
   Buffer[ MEGAVI_ID_OFFSET]     = (byte)Id;
   for( int i = 0; i < Size; i++){
      Buffer[ i + MEGAVI_DATA_OFFSET] = p[ i];
   }
   Buffer[ Size + MEGAVI_CRC_OFFSET]     = CalcCrc( (byte *)Data, Size);
   Buffer[ Size + MEGAVI_TRAILER_OFFSET] = MEGAVI_TRAILER_CHAR;

   int TotalSize = Size + MEGAVI_FRAME_SIZE - 2; // without leader/trailer
   // Leader character :
   FPort->Flush();
   FParameters.Parity = TUart::MEGAVI_CONTROL_PARITY;
   FPort->SetParameters( FParameters);
   FPort->SetRxWait( FRxTimeout, 0);
   if( FPort->Write( Buffer, 1) != 1){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   // packet data :
   FPort->WriteWait();                 // wait for transmission
   FParameters.Parity = TUart::MEGAVI_NORMAL_PARITY;
   FPort->SetParameters( FParameters);
   FPort->SetRxWait( FRxTimeout, 0);
   if( FPort->Write( &Buffer[ MEGAVI_ID_OFFSET], TotalSize) != TotalSize){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   // Trailer character :
   FPort->WriteWait();                 // wait for transmission
   FParameters.Parity = TUart::MEGAVI_CONTROL_PARITY;
   FPort->SetParameters( FParameters);
   FPort->SetRxWait( FRxTimeout, 0);
   if( FPort->Write( &Buffer[ Size + MEGAVI_TRAILER_OFFSET], 1) != 1){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   RwdTx( &Buffer, TotalSize + 2);     // size with leader/trailer
   return( true);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TMegavi::Receive( unsigned &Id, void *Data, int Size)
// Receive message
{
int ReplySize, ReqSize;

   if( !CheckConnect()){
      return( false);
   }
   FPort->SetRxWait( FRxTimeout, 0);     // wait for first char
   if( FPort->Read( Buffer, 1) != 1){
      return( false);
   }
   if( Buffer[ MEGAVI_LEADER_OFFSET] != MEGAVI_LEADER_CHAR){
      RwdGarbage( Buffer, 1);
      return( false);
   }
   // short packet
   ReqSize   = Size + 3;
   ReplySize = FPort->Read( &Buffer[ MEGAVI_LEADER_OFFSET + 1], ReqSize);
   if(  ReplySize != ReqSize){
      RwdGarbage( Buffer, ReplySize + 1);
      return( false);
   }
   ReplySize++;                          // add leader
   if( Buffer[ Size + MEGAVI_TRAILER_OFFSET] != MEGAVI_TRAILER_CHAR){
      RwdGarbage( Buffer, ReplySize);
      return( false);
   }
   if( Buffer[ Size + MEGAVI_CRC_OFFSET] != CalcCrc( &Buffer[ MEGAVI_DATA_OFFSET], Size)){
      RwdGarbage( Buffer, ReplySize);
      return( false);
   }
   RwdRx( Buffer, ReplySize);
   Id = Buffer[ 1];
   memcpy( Data, &Buffer[ MEGAVI_DATA_OFFSET], Size);
   return( true);
} // Receive

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Identifier
//******************************************************************************

TIdentifier TMegavi::GetIdentifier()
// GetIdentifier
{
   if( !FPort){
      return( INVALID_IDENTIFIER);
   }
   return( FPort->Identifier);
} // GetIdentifier

//******************************************************************************
// Property Name
//******************************************************************************

TName TMegavi::GetName()
// Get device name
{
   if( !FPort){
      return("No adapter");
   }
   return( FPort->Name);
} // GetName

//******************************************************************************
// Property IsOpen
//******************************************************************************

bool  TMegavi::GetIsOpen()
// Check if device is opened
{
   if( !FPort){
      return( false);
   }
   return( FPort->IsOpen);
} // GetIsOpen

//******************************************************************************
// Check Connection
//******************************************************************************

bool TMegavi::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( true);
   }
   if( FPort){
      IERROR;                          // always opened !
   }
   TIdentifier Identifier;
   TUart    *Uart;
   TUsbUart *Usb = 0;
   if( PortName == USB_NAME){
      // USB setup
      Usb = new TUsbUart;
      if( !Usb->Locate( FUsbDevice, Identifier)){
         delete Usb;
         return( false);
      }
      Uart = Usb;
   } else {
      // COM setup :
      TComUart *Com = new TComUart;
      if( !Com->Locate( PortName, Identifier)){
         delete Com;
         return( false);
      }
      Uart = Com;
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   // common init :
   Uart->SetParameters( FParameters);
   Uart->SetRxWait( FRxTimeout, 0);
   Uart->Flush();
   FPort = Uart;
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TMegavi::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

//******************************************************************************
// CRC
//******************************************************************************

byte TMegavi::CalcCrc( byte *Data, int Size)
// Calculate CRC
{
   byte Crc = 0xFF;
   for( int i = 0; i < Size; i++){
      Crc ^= *Data;
      Data++;
   }
   return( Crc);
} // CalcCrc

