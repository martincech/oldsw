//******************************************************************************
//
//   Main.h       QH Bus diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainBridgeH
#define MainBridgeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Library/Crt/Crt.h"
#include "MBridge.h"

#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TComboBox *MainPort;
   TLabel *Label1;
   TButton *MainSetup;
   TMemo *MainMemo;
   TStatusBar *StatusBar1;
   TButton *MainVersion;
   TLabel *Label3;
   TEdit *MainId;
   TCheckBox *MainCycle;
   TButton *MainMaleData;
   TButton *MainSms;
   TButton *MainSmsStatus;
   TEdit *EdtSlot;
   TLabel *Label2;
   TButton *MainFemaleData;
   TEdit *MainSmsText;
   TLabel *Label4;
   TEdit *MainTarget;
   TLabel *Label5;
   void __fastcall Create(TObject *Sender);
   void __fastcall MainSetupClick(TObject *Sender);
   void __fastcall MainPortChange(TObject *Sender);
   void __fastcall MainVersionClick(TObject *Sender);
   void __fastcall MemoResize(TObject *Sender);
   void __fastcall MainMaleDataClick(TObject *Sender);
   void __fastcall MainSmsClick(TObject *Sender);
   void __fastcall MainSmsStatusClick(TObject *Sender);
   void __fastcall MainFemaleDataClick(TObject *Sender);
private:	// User declarations
   TCrt          *Crt;
   TMBridge      *MBridge;

   void __fastcall Restart();
   // Open port
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
   void DumpData( char SmsRequest, TBat2Data *Data);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------

#endif
