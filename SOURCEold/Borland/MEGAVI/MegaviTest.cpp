//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("MegaviTest.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Megavi.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USELIB("..\Library\Serial\USB\ftd2xx.lib");
USEFORM("..\Library\Serial\UartSetup.cpp", UartSetupDialog);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->CreateForm(__classid(TUartSetupDialog), &UartSetupDialog);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
