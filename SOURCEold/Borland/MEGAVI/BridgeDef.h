//******************************************************************************
//
//   BridgeDef.h   Bat2 bridge protocol definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef BridgeDefH
   #define BridgeDefH

#pragma pack( push, 1)                             // byte alignment
   #include "../AVR8/Inc/Megavi/BridgeDef.h"       // protocol definition
#pragma pack( pop)                                 // original alignment

#endif
