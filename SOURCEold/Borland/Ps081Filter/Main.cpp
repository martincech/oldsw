//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#include <math.h>
#include "Main.h"
#include "Data.h"
#include "Ps081.h"
#include "Histogram.h"
#pragma hdrstop

#define STABLE_VALUE     120
#define LAST_STABLE 1        // last stable hold
#define HISTOGRAM_BARS 30

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
   : TForm(Owner)
{
   Ps081Device = new TPs081();
   Accept = new TAcceptance();

   DecimalSeparator='.'; // Change locale

   char StringBuffer[20];

   sprintf(StringBuffer, "%d", Ps081Device->AccuracyFine);
   AccuracyFineEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Ps081Device->AccuracyCoarse);
   AccuracyCoarseEdit->Text = StringBuffer;
   PrefilterEdit->Text = "1";

   switch(Ps081Device->Filter) {
      case PS081_FILTER_SINC3:
         AdcFilterSinc3RadioButton->Checked = true;
         break;

      case PS081_FILTER_SINC5:
         AdcFilterSinc5RadioButton->Checked = true;
         break;

      default:
         AdcFilterNoneRadioButton->Checked = true;
         break;
   }

   sprintf(StringBuffer, "%d", Ps081Device->AveragingWindowFine);
   AveragingWindowFineEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Ps081Device->AveragingWindowCoarse);
   AveragingWindowCoarseEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Ps081Device->StableWindowFine);
   StableWindowFineEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Ps081Device->StableWindowCoarse);
   StableWindowCoarseEdit->Text = StringBuffer;

   sprintf(StringBuffer, "%0.2f", Accept->Target);
   AcceptanceTargetEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%0.2f", Accept->Above);
   AcceptanceAboveEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%0.2f", Accept->Under);
   AcceptanceUnderEdit->Text = StringBuffer;

   AcceptanceModeRg->ItemIndex = Accept->Mode;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
   TAcceptance *Accept = new TAcceptance();
   Accept->Mode = AcceptanceModeRg->ItemIndex;
   Accept->Target = AcceptanceTargetEdit->Text.ToDouble();
   Accept->Above = AcceptanceAboveEdit->Text.ToDouble();
   Accept->Under = AcceptanceUnderEdit->Text.ToDouble();

   Accept->Start();

   double LowMargin = Accept->Target * (1 - HistogramRangeEdit->Text.ToInt() / 100.0);
   double HighMargin = Accept->Target * (1 + HistogramRangeEdit->Text.ToInt() / 100.0);
   THistogram *Hist = new THistogram(HISTOGRAM_BARS, HistogramSeries, LowMargin, HighMargin);
   Hist->Clear();

   int StartIndex;
      // Read display window :
   int WindowWidth = EdtWidth->Text.ToInt();
   int WindowOffset = EdtOffset->Text.ToInt();

   Ps081Device->Stop();
   // read filter data :

   if(AdcFilterNoneRadioButton->Checked == true) {
      Ps081Device->Filter = PS081_FILTER_NONE;
   } else if(AdcFilterSinc3RadioButton->Checked == true) {
      Ps081Device->Filter = PS081_FILTER_SINC3;
   } else {
      Ps081Device->Filter = PS081_FILTER_SINC5;
   }

   Ps081Device->Prefilter = PrefilterEdit->Text.ToInt();

   Ps081Device->AccuracyFine = AccuracyFineEdit->Text.ToInt();
   Ps081Device->AccuracyCoarse = AccuracyCoarseEdit->Text.ToInt();

   Ps081Device->AveragingWindowFine = AveragingWindowFineEdit->Text.ToInt();
   Ps081Device->AveragingWindowCoarse = AveragingWindowCoarseEdit->Text.ToInt();
   Ps081Device->RangeFine = (RangeFineEdit->Text.ToInt() / 100.0) * (Accept->Target * 10000);
   Ps081Device->RangeCoarse = (RangeCoarseEdit->Text.ToInt() / 100.0) * (Accept->Target * 10000);
   Ps081Device->StableWindowFine = StableWindowFineEdit->Text.ToInt();
   Ps081Device->StableWindowCoarse = StableWindowCoarseEdit->Text.ToInt();
   Ps081Device->SwitchoverRangeFine = (SwitchoverRangeFineEdit->Text.ToInt() / 100.0) * (Accept->Target * 10000);
   Ps081Device->SwitchoverRangeCoarse = (SwitchoverRangeCoarseEdit->Text.ToInt() / 100.0) * (Accept->Target * 10000);

   // Clear old graph :
   WeightSeries->Clear();
   LowPassSeries->Clear();
   HighPassSeries->Clear();
   StepSeries->Clear();
   StableSeries->Clear();
   StableWeightSeries->Clear();
   LastStableWeightSeries->Clear();
   AcceptedSeries->Clear();
   // draw graph :
   StartIndex = WindowOffset;
   // scroll at offset :
   BatData->Use();
   BatData->Begin();
   for( int i = 0; i < WindowOffset; i++){
      if( !BatData->Next()){
         return;      // end of database
      }
   }

   Ps081Device->Start((TWeight)(BatData->Weight * 10000));
   TWeight LastStableOutput = -1000000;

   double AverageWeight = 0;
   int AcceptedHeads = 0;

   while(1){
      Ps081Device->NextSample( (TWeight)(BatData->Weight * 10000));    // process filter

      WeightSeries->AddXY( StartIndex,
                           Ps081Device->Weight / 10,
                            "", clTeeColor);
      LowPassSeries->AddXY( StartIndex,
                           Ps081Device->AveragingOutput / 10,
                            "", clTeeColor);
      HighPassSeries->AddXY( StartIndex,
                           Ps081Device->HighPassOutput / 10,
                            "", clTeeColor);
      StableSeries->AddXY( StartIndex,
                           Ps081Device->Stable ? STABLE_VALUE : 0,
                            "", clTeeColor);
      StableWeightSeries->AddXY( StartIndex,
                           Ps081Device->StableOutput / 10,
                            "", clTeeColor);

      if( Ps081Device->StableOutput != LastStableOutput){
         LastStableOutput = Ps081Device->StableOutput;

         TAcceptance::TAcceptedSample AccSample;

         if(Accept->Accept(Ps081Device->StableOutput / 10000.0, &AccSample)) {
            AverageWeight += AccSample.Weight;
            AcceptedHeads++;
            Hist->Add(AccSample.Weight);
            AcceptedSeries->AddXY( StartIndex,
                           Ps081Device->StableOutput / 10,
                            "", clTeeColor);
         }
      }

      StartIndex++;
      if( StartIndex >= (WindowWidth + WindowOffset)){
         break;
      }
      if( !BatData->Next()){
         break;
      }
   }
   BatData->Release();
   
   Hist->Draw();

   char StringBuffer[20];
   sprintf(StringBuffer, "%d %", Ps081Device->GetAverageAccuracy());
   AccuracyProportionPs081Label->Caption = StringBuffer;

   if(AcceptedHeads) {
      AverageWeight /= AcceptedHeads;
   }

   sprintf(StringBuffer, "%d", AcceptedHeads);
   HeadsDetectedPs081Label->Caption = StringBuffer;
   sprintf(StringBuffer, "%0.3f", AverageWeight);
   AverageWeightPs081Label->Caption = StringBuffer;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::FileButtonClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   BatData->SetFile( FileOpenDialog->FileName);

   DataFileLabel->Caption = FileOpenDialog->FileName;
}

//******************************************************************************
// Backward
//******************************************************************************

void __fastcall TForm1::BtnBackwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset -= WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
   EdtOffset->Text = AnsiString( WindowOffset);
   Button1Click( Sender);
} // BtnBackwardClick

//******************************************************************************
// Forward
//******************************************************************************

void __fastcall TForm1::BtnForwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset += WindowWidth;
   EdtOffset->Text = AnsiString( WindowOffset);
   Button1Click( Sender);
} // BtnForwardClick

//******************************************************************************
// First
//******************************************************************************

void __fastcall TForm1::BtnFirstClick(TObject *Sender)
{
   int WindowOffset = 0;
   EdtOffset->Text = AnsiString( WindowOffset);
   Button1Click( Sender);
} // BtnFirstClick
