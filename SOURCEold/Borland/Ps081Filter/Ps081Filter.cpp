//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USEFORM("Main.cpp", Form1);
USEFORM("Data.cpp", BatData); /* TDataModule: File Type */
USEUNIT("Ps081.cpp");
USEUNIT("Fifo.cpp");
USEUNIT("Acceptance.cpp");
USEUNIT("..\Library\Bat2\Histogram.cpp");
USERES("Ps081Filter.res");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TForm1), &Form1);
       Application->CreateForm(__classid(TBatData), &BatData);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
