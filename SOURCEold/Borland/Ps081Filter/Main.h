//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>

#include "Ps081.h"
#include "Acceptance.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
   TDBChart *Graph;
   TLineSeries *WeightSeries;
   TLineSeries *LowPassSeries;
   TLineSeries *HighPassSeries;
   TLineSeries *StepSeries;
   TLineSeries *StableSeries;
   TLineSeries *StableWeightSeries;
   TLineSeries *LastStableWeightSeries;
   TPointSeries *AcceptedSeries;
   TDBChart *HistogramGraph;
   TBarSeries *HistogramSeries;
   TPanel *Panel1;
   TLabel *AccuracyProportionPs081Label;
   TLabel *Label31;
   TLabel *HeadsDetectedPs081Label;
   TLabel *AverageWeightPs081Label;
   TLabel *Label33;
   TLabel *Label35;
   TPanel *Panel3;
   TLabel *Label4;
   TLabel *Label5;
   TLabel *Label37;
   TLabel *Label38;
   TLabel *DataFileLabel;
   TButton *Button1;
   TEdit *EdtOffset;
   TEdit *EdtWidth;
   TButton *FileButton;
   TPanel *Panel5;
   TLabel *Label22;
   TLabel *Label23;
   TLabel *Label25;
   TLabel *Label26;
   TLabel *Label27;
   TLabel *Label28;
   TLabel *Label32;
   TEdit *AcceptanceTargetEdit;
   TEdit *AcceptanceAboveEdit;
   TEdit *AcceptanceUnderEdit;
   TRadioGroup *AcceptanceModeRg;
   TEdit *HistogramRangeEdit;
   TOpenDialog *FileOpenDialog;
   TPanel *Panel4;
   TLabel *Label17;
   TLabel *Label1;
   TLabel *Label10;
   TLabel *Label15;
   TLabel *Label12;
   TLabel *Label19;
   TLabel *Label29;
   TLabel *Label21;
   TLabel *Label24;
   TLabel *Label20;
   TLabel *Label30;
   TLabel *Label18;
   TEdit *AccuracyCoarseEdit;
   TEdit *AveragingWindowCoarseEdit;
   TEdit *RangeCoarseEdit;
   TEdit *StableWindowCoarseEdit;
   TEdit *SwitchoverRangeCoarseEdit;
   TEdit *SwitchoverRangeFineEdit;
   TEdit *StableWindowFineEdit;
   TEdit *RangeFineEdit;
   TEdit *AveragingWindowFineEdit;
   TEdit *AccuracyFineEdit;
   TLabel *Label8;
   TLabel *Label9;
   TEdit *PrefilterEdit;
   TPanel *Panel2;
   TRadioButton *AdcFilterNoneRadioButton;
   TRadioButton *AdcFilterSinc3RadioButton;
   TRadioButton *AdcFilterSinc5RadioButton;
   TButton *BtnBackward;
   TButton *BtnFirst;
   TButton *BtnForward;
   void __fastcall Button1Click(TObject *Sender);
   void __fastcall FileButtonClick(TObject *Sender);
   void __fastcall BtnFirstClick(TObject *Sender);
   void __fastcall BtnBackwardClick(TObject *Sender);
   void __fastcall BtnForwardClick(TObject *Sender);
private:	// User declarations
   TPs081 *Ps081Device;
   TAcceptance *Accept;
public:		// User declarations
   __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
