object Form1: TForm1
  Left = 234
  Top = 154
  Width = 1185
  Height = 963
  Caption = 'Ps081Filter'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Graph: TDBChart
    Left = 0
    Top = 177
    Width = 1169
    Height = 473
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.DateTimeFormat = 'h:mm'
    Legend.Visible = False
    View3D = False
    Align = alClient
    TabOrder = 0
    object WeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      Title = 'WeightSeriesh'
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HMOTNOST'
    end
    object LowPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'LO_PASS'
    end
    object HighPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 16744703
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HI_PASS'
    end
    object StepSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlue
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clLime
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableWeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clMaroon
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object LastStableWeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object AcceptedSeries: TPointSeries
      Marks.ArrowLength = 0
      Marks.Visible = False
      SeriesColor = clWhite
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object HistogramGraph: TDBChart
    Left = 0
    Top = 650
    Width = 1169
    Height = 250
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TDBChart')
    Title.Visible = False
    View3D = False
    View3DWalls = False
    Align = alBottom
    TabOrder = 1
    object HistogramSeries: TBarSeries
      Marks.ArrowLength = 20
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Bar'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 900
    Width = 1169
    Height = 25
    Align = alBottom
    TabOrder = 2
    object AccuracyProportionPs081Label: TLabel
      Left = 412
      Top = 4
      Width = 21
      Height = 13
      Caption = '0.00'
    end
    object Label31: TLabel
      Left = 304
      Top = 4
      Width = 91
      Height = 13
      Caption = 'Average Accuracy:'
    end
    object HeadsDetectedPs081Label: TLabel
      Left = 112
      Top = 4
      Width = 6
      Height = 13
      Caption = '0'
    end
    object AverageWeightPs081Label: TLabel
      Left = 252
      Top = 4
      Width = 21
      Height = 13
      Caption = '0.00'
    end
    object Label33: TLabel
      Left = 152
      Top = 4
      Width = 80
      Height = 13
      Caption = 'Average Weight:'
    end
    object Label35: TLabel
      Left = 8
      Top = 4
      Width = 76
      Height = 13
      Caption = 'Heads detected'
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 1169
    Height = 177
    Align = alTop
    TabOrder = 3
    object Label4: TLabel
      Left = 64
      Top = 104
      Width = 28
      Height = 13
      Caption = 'Width'
    end
    object Label5: TLabel
      Left = 64
      Top = 128
      Width = 28
      Height = 13
      Caption = 'Offset'
    end
    object Label37: TLabel
      Left = 8
      Top = 152
      Width = 82
      Height = 13
      Caption = 'Histogram Range'
    end
    object Label38: TLabel
      Left = 173
      Top = 152
      Width = 62
      Height = 13
      Caption = '+- % of target'
    end
    object DataFileLabel: TLabel
      Left = 8
      Top = 8
      Width = 48
      Height = 13
      Caption = 'File: None'
    end
    object Label8: TLabel
      Left = 372
      Top = 32
      Width = 82
      Height = 13
      Caption = 'Prefilter [samples]'
      Color = clBtnFace
      ParentColor = False
    end
    object Label9: TLabel
      Left = 379
      Top = 72
      Width = 22
      Height = 13
      Caption = 'Filter'
      Color = clBtnFace
      ParentColor = False
    end
    object Button1: TButton
      Left = 192
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Redraw'
      TabOrder = 0
      OnClick = Button1Click
    end
    object EdtOffset: TEdit
      Left = 96
      Top = 120
      Width = 73
      Height = 21
      TabOrder = 1
      Text = '0'
    end
    object EdtWidth: TEdit
      Left = 96
      Top = 96
      Width = 73
      Height = 21
      TabOrder = 2
      Text = '20000'
    end
    object FileButton: TButton
      Left = 8
      Top = 24
      Width = 75
      Height = 25
      Caption = 'File'
      TabOrder = 3
      OnClick = FileButtonClick
    end
    object Panel5: TPanel
      Left = 873
      Top = 14
      Width = 281
      Height = 99
      BevelOuter = bvLowered
      TabOrder = 4
      object Label22: TLabel
        Left = 8
        Top = 24
        Width = 37
        Height = 13
        Caption = 'Target :'
      end
      object Label23: TLabel
        Left = 173
        Top = 24
        Width = 12
        Height = 13
        Caption = 'kg'
      end
      object Label25: TLabel
        Left = 8
        Top = 48
        Width = 37
        Height = 13
        Caption = 'Above :'
      end
      object Label26: TLabel
        Left = 173
        Top = 48
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label27: TLabel
        Left = 8
        Top = 72
        Width = 35
        Height = 13
        Caption = 'Under :'
      end
      object Label28: TLabel
        Left = 173
        Top = 72
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label32: TLabel
        Left = 85
        Top = 2
        Width = 69
        Height = 13
        Caption = 'Acceptance'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object AcceptanceTargetEdit: TEdit
        Left = 77
        Top = 20
        Width = 89
        Height = 21
        TabOrder = 0
      end
      object AcceptanceAboveEdit: TEdit
        Left = 77
        Top = 44
        Width = 89
        Height = 21
        TabOrder = 1
      end
      object AcceptanceUnderEdit: TEdit
        Left = 77
        Top = 68
        Width = 89
        Height = 21
        TabOrder = 2
      end
      object AcceptanceModeRg: TRadioGroup
        Left = 192
        Top = 16
        Width = 81
        Height = 65
        Caption = 'Mode'
        ItemIndex = 0
        Items.Strings = (
          'Enter'
          'Leave'
          'Both')
        TabOrder = 3
      end
    end
    object HistogramRangeEdit: TEdit
      Left = 96
      Top = 144
      Width = 73
      Height = 21
      TabOrder = 5
      Text = '40'
    end
    object Panel4: TPanel
      Left = 504
      Top = 8
      Width = 361
      Height = 153
      TabOrder = 6
      object Label17: TLabel
        Left = 139
        Top = 8
        Width = 33
        Height = 13
        Caption = 'Coarse'
      end
      object Label1: TLabel
        Left = 60
        Top = 32
        Width = 45
        Height = 13
        Caption = 'Accuracy'
      end
      object Label10: TLabel
        Left = 57
        Top = 56
        Width = 48
        Height = 13
        Caption = 'Averaging'
      end
      object Label15: TLabel
        Left = 40
        Top = 80
        Width = 65
        Height = 13
        Caption = 'Stable Range'
      end
      object Label12: TLabel
        Left = 75
        Top = 104
        Width = 30
        Height = 13
        Caption = 'Stable'
      end
      object Label19: TLabel
        Left = 17
        Top = 128
        Width = 88
        Height = 13
        Caption = 'Switchover Range'
      end
      object Label29: TLabel
        Left = 271
        Top = 128
        Width = 50
        Height = 13
        Caption = '% of target'
      end
      object Label21: TLabel
        Left = 273
        Top = 104
        Width = 40
        Height = 13
        Caption = 'Samples'
      end
      object Label24: TLabel
        Left = 271
        Top = 80
        Width = 50
        Height = 13
        Caption = '% of target'
      end
      object Label20: TLabel
        Left = 273
        Top = 56
        Width = 40
        Height = 13
        Caption = 'Samples'
      end
      object Label30: TLabel
        Left = 273
        Top = 32
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label18: TLabel
        Left = 214
        Top = 8
        Width = 20
        Height = 13
        Caption = 'Fine'
      end
      object AccuracyCoarseEdit: TEdit
        Left = 112
        Top = 24
        Width = 73
        Height = 21
        TabOrder = 0
      end
      object AveragingWindowCoarseEdit: TEdit
        Left = 112
        Top = 48
        Width = 73
        Height = 21
        TabOrder = 1
      end
      object RangeCoarseEdit: TEdit
        Left = 112
        Top = 72
        Width = 73
        Height = 21
        TabOrder = 2
        Text = '6'
      end
      object StableWindowCoarseEdit: TEdit
        Left = 112
        Top = 96
        Width = 73
        Height = 21
        TabOrder = 3
      end
      object SwitchoverRangeCoarseEdit: TEdit
        Left = 112
        Top = 120
        Width = 73
        Height = 21
        TabOrder = 4
        Text = '3'
      end
      object SwitchoverRangeFineEdit: TEdit
        Left = 192
        Top = 120
        Width = 73
        Height = 21
        TabOrder = 5
        Text = '6'
      end
      object StableWindowFineEdit: TEdit
        Left = 192
        Top = 96
        Width = 73
        Height = 21
        TabOrder = 6
      end
      object RangeFineEdit: TEdit
        Left = 192
        Top = 72
        Width = 73
        Height = 21
        TabOrder = 7
        Text = '3'
      end
      object AveragingWindowFineEdit: TEdit
        Left = 192
        Top = 48
        Width = 73
        Height = 21
        TabOrder = 8
      end
      object AccuracyFineEdit: TEdit
        Left = 192
        Top = 24
        Width = 73
        Height = 21
        TabOrder = 9
      end
    end
    object PrefilterEdit: TEdit
      Left = 376
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 7
    end
    object Panel2: TPanel
      Left = 376
      Top = 88
      Width = 73
      Height = 81
      TabOrder = 8
      object AdcFilterNoneRadioButton: TRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'None'
        TabOrder = 0
      end
      object AdcFilterSinc3RadioButton: TRadioButton
        Left = 8
        Top = 32
        Width = 113
        Height = 17
        Caption = 'Sinc3'
        TabOrder = 1
      end
      object AdcFilterSinc5RadioButton: TRadioButton
        Left = 8
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Sinc5'
        TabOrder = 2
      end
    end
    object BtnBackward: TButton
      Left = 192
      Top = 56
      Width = 75
      Height = 25
      Caption = '<<<'
      TabOrder = 9
      OnClick = BtnBackwardClick
    end
    object BtnFirst: TButton
      Left = 112
      Top = 56
      Width = 75
      Height = 25
      Caption = '|<<<'
      TabOrder = 10
      OnClick = BtnFirstClick
    end
    object BtnForward: TButton
      Left = 272
      Top = 56
      Width = 75
      Height = 25
      Caption = '>>>'
      TabOrder = 11
      OnClick = BtnForwardClick
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'db'
    FileName = 'demo'
    Filter = 'Database|*.db'
    Left = 8
    Top = 40
  end
end
