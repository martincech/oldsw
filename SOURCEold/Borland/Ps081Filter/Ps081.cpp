//******************************************************************************
//
//   Ps081.cpp     Ps081
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************
 
#include "Ps081.h"
#include <time.h>

#define PS081_AVERAGING_MAX  17    // max length of averaging fifo
#define SAMPLES_IN_COARSE_MAX 500  // max succeeding samples at coarse accuracy

#define SWITCH_TO_FINE_PEAK      10
#define SWITCH_TO_COARSE_PEAK    -100

#define NOISE_PATTERN_LENGTH 5

// noise = f(accuracy), accuracy must begin with ADC_ACCURACY_MIN and end with ADC_ACCURACY_MAX
static int PatternAccuracy[NOISE_PATTERN_LENGTH] = {ADC_ACCURACY_MIN, 5, 10, 20, ADC_ACCURACY_MAX};

// Use max 1 decimal digit
static TWeight PatternNoise[NOISE_PATTERN_LENGTH] = {300, 100, 50, 30, 5}; // Amplitude
static double GetNoise(int Accuracy);

//******************************************************************************
// Constructor
//******************************************************************************

TPs081::TPs081()
// Constructor
{
   AccuracyFine = ADC_ACCURACY_MAX;
   AccuracyCoarse = 20;
   Filter = PS081_FILTER_NONE;

   AveragingWindowFine = 9;
   AveragingWindowCoarse = 3;
   RangeFine = 100;
   RangeCoarse = 200;
   StableWindowFine = 9;
   StableWindowCoarse = 3;
   SwitchoverRangeFine = 150;
   SwitchoverRangeCoarse = 400;

   FilterFifo = 0;
   PreAveragingFifo = 0;
   AveragingFineFifo = 0;
   AveragingCoarseFifo = 0;

   InitOK = false;
}

//******************************************************************************
// Destructor
//******************************************************************************

TPs081::~TPs081() {
}

//******************************************************************************
// Start
//******************************************************************************

bool TPs081::Start( TWeight InitValue) {
   // Init Filter fifo
   int FilterLength;

   switch(Filter) {
      case PS081_FILTER_SINC3:
         FilterLength = 3;
         break;

      case PS081_FILTER_SINC5:
         FilterLength = 5;
         break;

      case PS081_FILTER_NONE:
         FilterLength = 1;
         break;
   }

   PrefilterFifo = new TFifo(Prefilter, 0);

   FilterFifo = new TFifo(FilterLength, InitValue);

   // Init Fine and Coarse averaging fifo
   if(AveragingWindowFine < AveragingWindowCoarse) {
      return false;
   }

   int AveragingWindowFineInternal = AveragingWindowFine;
   int PreAveraging = 1;

   while(AveragingWindowFineInternal > PS081_AVERAGING_MAX) {
      AveragingWindowFineInternal /= 2;
      PreAveraging *= 2;
   }

   int AveragingWindowCoarseInternal = AveragingWindowCoarse / PreAveraging;

   if(AveragingWindowCoarseInternal == 0) {
      AveragingWindowCoarseInternal = 1;
   }

   PreAveragingFifo = new TFifo(PreAveraging, 0);
   AveragingFineFifo = new TFifo(AveragingWindowFineInternal, InitValue);
   AveragingCoarseFifo = new TFifo(AveragingWindowCoarseInternal, InitValue);

   AveragingOutput = InitValue;
   StableOutput = InitValue;
   Stable = false;

   StableCounter = 0;

   _IsFine = true;
   _LastIsFine = !_IsFine;

   SamplesInFineTotal = 0;
   SamplesInCoarseTotal = 0;
   SamplesInCoarse = 0;

   srand(time(NULL));
   NoiseCoarse = GetNoise(AccuracyCoarse);
   NoiseFine = GetNoise(AccuracyFine);

   InitOK = true;

   return true;
}

//******************************************************************************
// Stop
//******************************************************************************

void TPs081::Stop( void) {
   if(!InitOK) {
      return;
   }
   
   delete FilterFifo;
   delete PreAveragingFifo;
   delete AveragingFineFifo;
   delete AveragingCoarseFifo;
   delete PrefilterFifo;
   InitOK = false;
}

//******************************************************************************
// Sample
//******************************************************************************

void TPs081::NextSample( TWeight Sample)
// Process next sample
{
   if(!InitOK) {
      return;
   }

   double Noise;

   if(_IsFine) {
      SamplesInFineTotal++;
      Noise = NoiseFine;
   } else {
      SamplesInCoarseTotal++;
      SamplesInCoarse++;
      Noise = NoiseCoarse;
   }

   Noise = (2 * ((rand() % (int)(Noise * 10))) - (Noise * 10) / 2) / 10;

   // Add noise
   Weight = Sample + Noise;

   if(_LastIsFine != _IsFine) {
       _LastIsFine = _IsFine;
      //return;
      if(_IsFine) {
         Weight += SWITCH_TO_FINE_PEAK;
      } else {
         Weight += SWITCH_TO_COARSE_PEAK;
      }
   }

   PrefilterFifo->Insert(Weight);

   if(!PrefilterFifo->IsFull()) {
      return;
   }

   Weight = PrefilterFifo->Average();

   FilterFifo->Insert(Weight);
   FilterOutput = FilterFifo->Average();

   PreAveragingFifo->Insert(FilterOutput);

   if(PreAveragingFifo->IsFull()) {
      TWeight PreAveragingOutput = PreAveragingFifo->Average();

      AveragingFineFifo->Insert(PreAveragingOutput);
      AveragingCoarseFifo->Insert(PreAveragingOutput);

      if(_IsFine) {
         AveragingOutput = AveragingFineFifo->Average();
      } else {
         AveragingOutput = AveragingCoarseFifo->Average();
      }
   }

   HighPassOutput = FilterOutput - AveragingOutput;
   HighPassAbsOutput = abs(HighPassOutput);

   int Range;
   int StableWindow;

   if(_IsFine) {
      Range = RangeFine;
      StableWindow = StableWindowFine;
   } else {
      Range = RangeCoarse;
      StableWindow = StableWindowCoarse;
   }

   if( HighPassAbsOutput < Range){
      if( StableCounter < StableWindow){
         StableCounter++;
      } // else saturation
   } else {
      Stable = false;
      StableCounter = 0;
   }

   if(_IsFine) { // switch back to coarse?
      if(HighPassAbsOutput > SwitchoverRangeFine) {
         _IsFine = false;
      }
   } else { // unlock?
      if(abs(FilterOutput - StableOutput) > SwitchoverRangeCoarse) {
         _IsLocked = false;
      }
   }

   int FifoFinePrev[20];
   int FifoCoarsePrev[20];
   int FifoFine[20];
   int FifoCoarse[20];

   if(StableCounter == StableWindow) {
      if(_IsFine) {
         Stable = true;
         StableOutput = AveragingOutput;
         _IsFine = false;
         StableCounter = 0;
         _IsLocked = true;
      } else if(!_IsLocked || SamplesInCoarse > SAMPLES_IN_COARSE_MAX) {
         _IsLocked = false;
         SamplesInCoarse = 0;
         _IsFine = true;
         //AveragingFineFifo->Init(AveragingOutput);
         //FilterFifo->Init(AveragingOutput);
         //PreAveragingFifo->Init(0);
         
         for(int FinePtr = 0 ; FinePtr < AveragingFineFifo->GetLength() ; FinePtr++) {
            FifoFinePrev[FinePtr] = AveragingFineFifo->GetItem(FinePtr);
         }



         for(int CoarsePtr = 0 ; CoarsePtr < AveragingCoarseFifo->GetLength() ; CoarsePtr++) {
            FifoCoarsePrev[CoarsePtr] = AveragingCoarseFifo->GetItem(CoarsePtr);
         }

         int FineIncrementPerCoarseSample = AveragingFineFifo->GetLength() * 10 / AveragingCoarseFifo->GetLength();
         int CoarsePtr = 0;
         int FineCounter = 0;

         for(int FinePtr = 0 ; FinePtr < AveragingFineFifo->GetLength() ; FinePtr++) {
            AveragingFineFifo->SetItem(FinePtr, AveragingCoarseFifo->GetItem(CoarsePtr));
            FineCounter += 10;

            if(FineCounter >= FineIncrementPerCoarseSample) {
               FineCounter = FineCounter - FineIncrementPerCoarseSample;

               if(CoarsePtr != AveragingCoarseFifo->GetLength() - 1) {
                  CoarsePtr++;
               }
            }
         }


         for(int FinePtr = 0 ; FinePtr < AveragingFineFifo->GetLength() ; FinePtr++) {
            FifoFine[FinePtr] = AveragingFineFifo->GetItem(FinePtr);
         }



         for(int CoarsePtr = 0 ; CoarsePtr < AveragingCoarseFifo->GetLength() ; CoarsePtr++) {
            FifoCoarse[CoarsePtr] = AveragingCoarseFifo->GetItem(CoarsePtr);
         } 

         StableCounter = 0;
      }
   }
}

//******************************************************************************
// IsFine
//******************************************************************************

bool TPs081::IsFine() {
   return _IsFine;
}

//******************************************************************************
// GetAverageAccuracy
//******************************************************************************

int TPs081::GetAverageAccuracy() {
   if(SamplesInCoarseTotal + SamplesInFineTotal == 0) {
      return 0;
   }

   return (AccuracyFine * SamplesInFineTotal + AccuracyCoarse * SamplesInCoarseTotal) / (SamplesInFineTotal + SamplesInCoarseTotal);
}

//******************************************************************************
// GetNoise
//******************************************************************************

static double GetNoise(int Accuracy) {
   double NoiseAmplitude;

   if(Accuracy < PatternAccuracy[0] || Accuracy > PatternAccuracy[NOISE_PATTERN_LENGTH - 1]) {
      return PatternNoise[0];
   }

   for(int i = 0 ; i < NOISE_PATTERN_LENGTH ; i++) {
      if(PatternAccuracy[i] >= Accuracy) {
         if(PatternAccuracy[i] == Accuracy) {
            NoiseAmplitude = PatternNoise[i];
         } else {
            NoiseAmplitude = PatternNoise[i] + (PatternAccuracy[i] - Accuracy) * (PatternNoise[i - 1] - PatternNoise[i]) / ((double)PatternAccuracy[i] - PatternAccuracy[i - 1]);
         }
         return NoiseAmplitude;
      }
   }
}
