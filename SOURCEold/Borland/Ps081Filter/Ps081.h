//******************************************************************************
//
//   Ps081.h     Ps081
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#ifndef __Ps081_H__
#define __Ps081_H__

#include "../Library/Unisys/Uni.h"
#include "../../AVR8/Library/Bat2Detector/Adc/AdcPs081Def.h"
#include "Fifo.h"

typedef sdword TWeight;
typedef sdword TLongWeight;

class TPs081 {
public :
   TPs081();
   // Constructor

   ~TPs081();
   // Destructor

   void Stop( void);
   // Stop

   bool Start( TWeight InitValue);
   // Initialize

   void NextSample( TWeight Sample);
   // Process next sample

   bool IsFine();
   int GetAverageAccuracy();

   int AccuracyFine;
   int Prefilter;
   int AccuracyCoarse;
   EPs081Filter Filter;
   int AveragingWindowFine;
   int AveragingWindowCoarse;
   TWeight RangeFine;
   TWeight RangeCoarse;
   int StableWindowFine;
   int StableWindowCoarse;
   int SwitchoverRangeFine;
   int SwitchoverRangeCoarse;

   TWeight Weight;
   TWeight FilterOutput;
   TWeight AveragingOutput;
   TWeight HighPassOutput;
   TWeight HighPassAbsOutput;
   TWeight StableOutput;
   bool Stable;

//------------------------------------------------------------------------------
protected :
   bool     InitOK;
   bool     _IsFine;
   bool     _LastIsFine;
   bool     _IsLocked;

   TFifo    *PrefilterFifo;
   TFifo    *FilterFifo;
   TFifo    *PreAveragingFifo;
   TFifo    *AveragingFineFifo;
   TFifo    *AveragingCoarseFifo;

   double NoiseFine;
   double NoiseCoarse;

   int SamplesInFineTotal;
   int SamplesInCoarseTotal;
   int SamplesInCoarse;

   int StableCounter;
}; // TPs081

#endif
