//******************************************************************************
//
//   Fifo.h     Integer Fifo
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#ifndef __Fifo_H__
#define __Fifo_H__

class TFifo {
public :
   TFifo(int _Length, int InitValue);
   // Constructor

   ~TFifo();
   // Destructor

   void Insert( int Value);

   void Init( int InitValueValue);

   double Average( void);

   bool IsFull( void);

   int GetLength( void);
   
   int GetItem( int Pointer);

   void SetItem( int Pointer, int Value);
//------------------------------------------------------------------------------
protected :
   int   Length;
   int   *Fifo;
   int   FifoPointer;
   long  FifoSum;
}; // TPs081

#endif
 