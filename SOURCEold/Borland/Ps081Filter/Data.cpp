//******************************************************************************
//
//   Data.cpp     Databaze
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Data.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBatData *BatData;

// Klouzavy prumer pro simulaci chovani prevodniku ADS123x
double Samples[PREFILTER_MAX_SAMPLES];  // Klouzavy prumer
int Prefilter;                          // Delka klouzaveho prumeru
int SamplesCount;                       // Pocet vzorku ulozenych v Samples[]
int SamplesIndex;                       // Aktualni pozice v Samples[]

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TBatData::TBatData(TComponent* Owner)
        : TDataModule(Owner)
{
   Prefilter    = 1;
} // TKurData

//******************************************************************************
// Begin
//******************************************************************************

void TBatData::Begin()
// pred prvni zaznam
{
   DataTable->First();
   SamplesCount = 0;
   SamplesIndex = 0;
} // Begin

//******************************************************************************
// Next
//******************************************************************************

bool TBatData::Next()
// na dalsi zaznam
{
   if( DataTable->Eof){
      return( false);
   }
   DataTable->Next();
   return( true);
} // Next

//******************************************************************************
// End of table
//******************************************************************************

bool TBatData::Eof()
// konec databaze
{
   return( DataTable->Eof);
} // Eof

//******************************************************************************
// Use
//******************************************************************************

bool TBatData::Use()
// konec databaze
{
   DataTable->Active = true;
} // Use

//******************************************************************************
// Release
//******************************************************************************

bool TBatData::Release()
// konec databaze
{
   DataTable->Active = false;
} // Release

//******************************************************************************
// Hmotnost
//******************************************************************************

double TBatData::GetWeight()
{
   double Average;

   // Pridam vzorek do filtru
   Samples[SamplesIndex] = DataTable->FieldValues[ "WEIGHT"];
   SamplesIndex++;
   if (SamplesIndex >= Prefilter) {
     SamplesIndex = 0;          // Ukladam zase od zacatku
   }
   if (SamplesCount < Prefilter) {
     SamplesCount++;
   }

   // Vypoctu prumer
   Average = 0;
   for (int i = 0; i < SamplesCount; i++) {
     Average += Samples[i];
   }
   Average /= (double)SamplesCount;

   return Average;
} // GetWeight

//******************************************************************************
// Set file
//******************************************************************************

void TBatData::SetFile( AnsiString Filename)
// Set new database file
{
   SamplesCount = 0;
   SamplesIndex = 0;

   DataTable->Active = false;
   DataTable->TableName = Filename;
} // SetFile

//******************************************************************************
// Predfiltrace
//******************************************************************************

void TBatData::SetPrefilter(int Count) {
  // Nastavi predfiltraci na <Count> vzorku
  if (Count < 0 || Count > PREFILTER_MAX_SAMPLES) {
    return;
  }
  Prefilter = Count;
  SamplesCount = 0;
  SamplesIndex = 0;
}

