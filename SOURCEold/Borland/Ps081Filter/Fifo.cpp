//******************************************************************************
//
//   Fifo.cpp     Integer Fifo
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

#include <stdlib.h>
#include "Fifo.h"

//******************************************************************************
// Constructor
//******************************************************************************

TFifo::TFifo(int _Length, int InitValue) {
   Length = _Length;

   Fifo = (int *) malloc(sizeof(int) * Length);

   Init(InitValue);
}

//******************************************************************************
// Init
//******************************************************************************

void TFifo::Init( int InitValue) {
   FifoSum = 0;
   FifoPointer = 0;

   for(int i = 0 ; i < Length ; i++) {
      Fifo[i] = InitValue;
      FifoSum += Fifo[i];
   }
}

//******************************************************************************
// Destructor
//******************************************************************************

TFifo::~TFifo() {
   if(!Fifo) {
      return;
   }

   free(Fifo);
}

//******************************************************************************
// Insert
//******************************************************************************

void TFifo::Insert( int Value) {
   if(FifoPointer >= Length) {
      FifoPointer = 0;
   }

   FifoSum -= Fifo[FifoPointer];

   Fifo[FifoPointer] = Value;

   FifoSum += Fifo[FifoPointer];

   FifoPointer++;
}

//******************************************************************************
// Average
//******************************************************************************

double TFifo::Average( void) {
   return FifoSum / Length;
}

//******************************************************************************
// IsFull
//******************************************************************************

bool TFifo::IsFull( void) {
   return FifoPointer == Length;
}

//******************************************************************************
// GetLength
//******************************************************************************

int TFifo::GetLength( void) {
   return Length;
}

//******************************************************************************
// GetItem
//******************************************************************************

int TFifo::GetItem( int Pointer) {
   if(Pointer >= Length) {
      return 0;
   }

   int Ptr = FifoPointer - 1 - Pointer;

   if(Ptr < 0) {
      Ptr = Length + Ptr;
   }

   return Fifo[Ptr];
}

//******************************************************************************
// SetItem
//******************************************************************************

void TFifo::SetItem( int Pointer, int Value) {
   if(Pointer >= Length) {
      return;
   }

   int Ptr = FifoPointer - 1 - Pointer;

   if(Ptr < 0) {
      Ptr = Length + Ptr;
   }

   FifoSum -= Fifo[Ptr];

   Fifo[Ptr] = Value;

   FifoSum += Value;
}
