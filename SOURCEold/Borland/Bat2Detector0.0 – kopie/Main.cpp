//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include <stdio.h>
#include <math.h>
#include <string>
#include "../Library/Bat2Detector/Dp.h"
#include "../Library/deelx.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#define Status(str)  StatusLabel->Caption = str
double StdDev(double *Data, int Count) {
   int i;
   double Mean = 0;
   double StdDev = 0;

   for(i = 0 ; i < Count ; i++) {
      Mean += Data[i];
   }

   Mean /= Count;

   for(i = 0 ; i < Count ; i++) {
      StdDev += (Data[i] - Mean) * (Data[i] - Mean);
   }

   StdDev /= Count;

   return sqrt(StdDev);
}

TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
   : TForm(Owner)
{
   Dp = new TDp();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ConnectButtonClick(TObject *Sender)
{
   Dp->Open(ComEdit->Text.c_str());
}
//---------------------------------------------------------------------------

char FileName[200] = "";
FILE     *f;
EDetectorMode Mode = DETECTOR_MODE_ALL;
//---------------------------------------------------------------------------
void __fastcall TForm1::StartButtonClick(TObject *Sender)
{
   EPs081Filter Filter;

   if(AdcFilterNoneRadioButton->Checked == true) {
      Filter = PS081_FILTER_NONE;
   } else if(AdcFilterSinc3RadioButton->Checked == true) {
      Filter = PS081_FILTER_SINC3;
   } else {
      Filter = PS081_FILTER_SINC5;
   }

   if(!Dp->AdcParametersPs081(AccuracyEdit->Text.ToInt(), RateEdit->Text.ToInt(), Filter, PrefilterEdit->Text.ToInt())) {
      Status("ADC Error");
      return;
   }

   if(DefaultModeRadioButton->Checked == true) {
      Mode = DETECTOR_MODE_DEFAULT;

      DataStringGrid->RowCount = 1;

      DataStringGrid->Cells[ 0 ][ 0 ] = "unimplemented";

   } else if(AllModeRadioButton->Checked == true) {
      Mode = DETECTOR_MODE_ALL;

      DataStringGrid->Cells[ 0 ][ 0 ] = "Weight [g]";
   } else if(StableModeRadioButton->Checked == true) {
      Mode = DETECTOR_MODE_STABLE;

      DataStringGrid->RowCount = 2;

      DataStringGrid->Cells[ 0 ][ 0 ] = "Weight [g]";
      DataStringGrid->Cells[ 0 ][ 0 ] = "Time";
   } else {
      Mode = DETECTOR_MODE_PS081;

      DataStringGrid->RowCount = 10;

      DataStringGrid->Cells[ 0 ][ 0 ] = "Weight [g]";
      DataStringGrid->Cells[ 0 ][ 1 ] = "Stable [* = YES]";
      DataStringGrid->Cells[ 0 ][ 2 ] = "Raw [LSB]";
      DataStringGrid->Cells[ 0 ][ 3 ] = "LP In [g]";
      DataStringGrid->Cells[ 0 ][ 4 ] = "LP Out [g]";
      DataStringGrid->Cells[ 0 ][ 5 ] = "PreAvg Cnt";
      DataStringGrid->Cells[ 0 ][ 6 ] = "HighPass abs [g]";
      DataStringGrid->Cells[ 0 ][ 7 ] = "Stable Cnt";
      DataStringGrid->Cells[ 0 ][ 8 ] = "Stable Cnt";
      DataStringGrid->Cells[ 0 ][ 9 ] = "Stable Cnt";
   }

   if(!Dp->Mode(Mode, ModeBurstSizeEdit->Text.ToInt(), ModeAveragingEdit->Text.ToInt())) {
      Status("Mode Error");
      return;
   }

   if(!Dp->DetectionParameters(AveragingWindowEdit->Text.ToInt(), AbsoluteRangeEdit->Text.ToInt(), StableWindowEdit->Text.ToInt())) {
      Status("Detection Error");
      return;
   }

   if(!Dp->Start()) {
      Status("Start Error");
      return;
   }

   ReadTimer->Enabled = true;
   Status("Start OK");

   if( strlen( FileName) == 0){
      return;
   }

   f = fopen( FileName, "w");
}

//---------------------------------------------------------------------------

void __fastcall TForm1::StopButtonClick(TObject *Sender)
{
   if(!Dp->Stop()) {
      Status("Stop Error");
   }

   ReadTimer->Enabled = false;
   Status("Stop OK");

   if(f) {
      fclose(f);
   }
}
//---------------------------------------------------------------------------

static double TWeightToKg(TWeight Weight) {
   return ((int)Weight) / 10.0;
}



void __fastcall TForm1::ReadTimerTimer(TObject *Sender)
{
   char StringBuffer[40];
   int Count;
   double WeightSamples[DATA_BURST_SIZE_MAX];


   switch(Mode) {
      case DETECTOR_MODE_ALL:
         TWeight *AllBuffer;

         Count = Dp->ReadAllData(&AllBuffer);

         if(Count == 0) {
            return;
         }

         DataStringGrid->ColCount = Count + 1;

         for(int i = 0 ; i < Count ; i++) {
            sprintf(StringBuffer, "%0.2f", TWeightToKg(AllBuffer[i]));
            DataStringGrid->Cells[ i + 1 ][ 0 ] = StringBuffer;

            WeightSamples[i] = TWeightToKg(AllBuffer[i]);

            if( !f){
               continue;
            }

            fprintf(f, "%0.2f,", TWeightToKg(AllBuffer[i]));
         }


         break;

      case DETECTOR_MODE_STABLE:
         TStableWeight *StableBuffer;

         Count = Dp->ReadStableData(&StableBuffer);

         if(Count == 0) {
            return;
         }

         DataStringGrid->ColCount = Count + 1;

         for(int i = 0 ; i < Count ; i++) {
            sprintf(StringBuffer, "%0.2f", TWeightToKg(StableBuffer[i].Weight));
            DataStringGrid->Cells[ i + 1 ][ 0 ] = StringBuffer;
            sprintf(StringBuffer, "%d", StableBuffer[i].Time);
            DataStringGrid->Cells[ i + 1 ][ 1 ] = StringBuffer;
         }
         break;

      case DETECTOR_MODE_PS081:
         TPs081Weight *Ps081Buffer;

         Count = Dp->ReadPs081Data(&Ps081Buffer);

         if(Count == 0) {
            return;
         }

         DataStringGrid->ColCount = Count + 1;

         for(int i = 0 ; i < Count ; i++) {
            sprintf(StringBuffer, "%0.2f", TWeightToKg(Ps081Buffer[i].Weight));
            DataStringGrid->Cells[ i + 1 ][ 0 ] = StringBuffer;

            if(Ps081Buffer[i].Stable) {
               DataStringGrid->Cells[ i + 1 ][ 1 ] = "*";
            } else {
               DataStringGrid->Cells[ i + 1 ][ 1 ] = "";
            }

            sprintf(StringBuffer, "%d", Ps081Buffer[i].Raw);
            DataStringGrid->Cells[ i + 1 ][ 2 ] = StringBuffer;

            sprintf(StringBuffer, "%0.2f", TWeightToKg(Ps081Buffer[i].LowPassInput));
            DataStringGrid->Cells[ i + 1 ][ 3 ] = StringBuffer;

            sprintf(StringBuffer, "%0.2f", TWeightToKg(Ps081Buffer[i].LowPassOutput));
            DataStringGrid->Cells[ i + 1 ][ 4 ] = StringBuffer;

            sprintf(StringBuffer, "%d", Ps081Buffer[i].PreAvgCounter);
            DataStringGrid->Cells[ i + 1 ][ 5 ] = StringBuffer;

            sprintf(StringBuffer, "%0.2f", TWeightToKg(Ps081Buffer[i].HighPassAbs));
            DataStringGrid->Cells[ i + 1 ][ 6 ] = StringBuffer;

            sprintf(StringBuffer, "%d", Ps081Buffer[i].StableWindowCounter);
            DataStringGrid->Cells[ i + 1 ][ 7 ] = StringBuffer;

            /*sprintf(StringBuffer, "%d", Ps081Buffer[i].Temp0);
            DataStringGrid->Cells[ i + 1 ][ 8 ] = StringBuffer;

            sprintf(StringBuffer, "%d", Ps081Buffer[i].Temp1);
            DataStringGrid->Cells[ i + 1 ][ 9 ] = StringBuffer;   */

            WeightSamples[i] = TWeightToKg(Ps081Buffer[i].Weight);
         }
         break;
   }

   sprintf(StringBuffer, "%0.2f", StdDev(WeightSamples, Count));
   StdDevLabel->Caption = StringBuffer;
}

//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

void __fastcall TForm1::PauseButtonClick(TObject *Sender)
{
   ReadTimer->Enabled= !ReadTimer->Enabled;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button1Click(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }

   strcpy(FileName, FileOpenDialog->FileName.c_str());
}
//---------------------------------------------------------------------------
static void Replace(byte *Src, int SrcLength, byte *Search, byte *ReplaceBy, int PieceLength);
static void SwapWord(word *Src, int SrcLength);
void __fastcall TForm1::ProgramButtonClick(TObject *Sender)
{
   const int CodeLength = 2048;
   byte Code[CodeLength];
   int LineNum = 0;
   char Line[] = "0000";
   byte Byte[] = "00";

   // declare
   static CRegexpT <char> regexp("[0-9A-F]{4}: [0-9A-F]{2} [0-9A-F]{2}", IGNORECASE | MULTILINE);

   char *text = HexMemo->Lines->GetText();
   MatchResult result = regexp.Match(text);

   while( result.IsMatched() ) {
      memcpy(Line, text + result.GetStart(), 4);

      sscanf(Line, "%X", &LineNum);

      if(LineNum >= CodeLength / 2) {
         result = regexp.Match(text, result.GetEnd());
         continue;
      }

      memcpy(Byte, text + result.GetStart() + 6, 2);
      sscanf(Byte, "%X", &Code[LineNum]);

      memcpy(Byte, text + result.GetStart() + 9, 2);
      sscanf (Byte, "%X", &Code[LineNum + 1]);

      // get next
      result = regexp.Match(text, result.GetEnd());
   }

   if(LineNum < 2) {
      return;
   }

   int Iterator = (LineNum + 1) * 2;
   const byte JSubRetOpCode = 0xCF;
   bool Found = false;

   while(Iterator-- > 0) {
      if(Code[Iterator] == JSubRetOpCode && Code[Iterator - 1] == JSubRetOpCode) {
         Found = true;
         break;
      }
   }

   if(Found && false) {
      word FakeRollAvg = Iterator;
      word FakeInitAvg = Iterator - 1;

      const word RollAvg = 0xC21;
      const word InitAvg = 0xC30;
      const byte JSubOpCode = 0xE0;

      word RollSearch = ((word)JSubOpCode << 8) + FakeRollAvg;
      word RollReplaceBy = ((word)JSubOpCode << 8) + RollAvg;
      word InitSearch = ((word)JSubOpCode << 8) + FakeInitAvg;
      word InitReplaceBy = ((word)JSubOpCode << 8) + InitAvg;

      byte Search[2];
      byte ReplaceBy[2];

      Search[0] = RollSearch >> 8;
      Search[1] = RollSearch;
      ReplaceBy[0] = RollReplaceBy >> 8;
      ReplaceBy[1] = RollReplaceBy;
      Replace(Code, CodeLength, Search, ReplaceBy, 2);

      Search[0] = InitSearch >> 8;
      Search[1] = InitSearch;
      ReplaceBy[0] = InitReplaceBy >> 8;
      ReplaceBy[1] = InitReplaceBy;
      Replace(Code, CodeLength, Search, ReplaceBy, 2);
   }

   //SwapWord((word *) Code, CodeLength / 2);

   if(!Dp->Firmware(Code, CodeLength)) {
      Status("Firmware error");
      return;
   }

   Status("Firmware OK");
}

//---------------------------------------------------------------------------
static void SwapWord(word *Src, int SrcLength) {
   while(SrcLength--) {
      *Src = (*Src << 8) | (*Src >> 8);
      Src++;
   }
}
//---------------------------------------------------------------------------
static void Replace(byte *Src, int SrcLength, byte *Search, byte *ReplaceBy, int PieceLength) {
   if(SrcLength <= 0) {
      return;
   }

   if(PieceLength > SrcLength) {
      return;
   }

   int SrcPointer = 0;
   int PiecePointer = 0;

   while(SrcPointer < SrcLength) {
      if(Src[SrcPointer] == Search[PiecePointer]) {
         PiecePointer++;

         if(PiecePointer == PieceLength) {
            memcpy(Src + SrcPointer - PieceLength + 1, ReplaceBy, PieceLength);
            PiecePointer = 0;
         }
      } else {
         PiecePointer = 0;
      }

      SrcPointer++;
   }
}
