object Form1: TForm1
  Left = 243
  Top = 213
  Width = 1241
  Height = 599
  AutoSize = True
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object StatusLabel: TLabel
    Left = 0
    Top = 32
    Width = 30
    Height = 13
    Caption = 'Status'
  end
  object StdDevLabel: TLabel
    Left = 400
    Top = 16
    Width = 21
    Height = 13
    Caption = '0.00'
  end
  object Label3: TLabel
    Left = 248
    Top = 16
    Width = 117
    Height = 13
    Caption = 'Burst standard deviation:'
  end
  object ComEdit: TEdit
    Left = 0
    Top = 0
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'COM3'
  end
  object ConnectButton: TButton
    Left = 128
    Top = 0
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 1
    OnClick = ConnectButtonClick
  end
  object StartButton: TButton
    Left = 0
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 2
    OnClick = StartButtonClick
  end
  object StopButton: TButton
    Left = 80
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 3
    OnClick = StopButtonClick
  end
  object DataStringGrid: TStringGrid
    Left = 0
    Top = 248
    Width = 1225
    Height = 313
    DefaultColWidth = 65
    RowCount = 1
    FixedRows = 0
    TabOrder = 4
  end
  object PauseButton: TButton
    Left = 472
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Pause'
    TabOrder = 5
    OnClick = PauseButtonClick
  end
  object Button1: TButton
    Left = 160
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Open File'
    TabOrder = 6
    OnClick = Button1Click
  end
  object GroupBox1: TGroupBox
    Left = 488
    Top = 56
    Width = 153
    Height = 153
    Caption = 'Detection'
    TabOrder = 7
    object Label5: TLabel
      Left = 11
      Top = 24
      Width = 134
      Height = 13
      Caption = 'Averaging window [samples]'
    end
    object Label6: TLabel
      Left = 12
      Top = 64
      Width = 117
      Height = 13
      Caption = '+- Range [TWeight LSB]'
    end
    object Label7: TLabel
      Left = 13
      Top = 108
      Width = 116
      Height = 13
      Caption = 'Stable window [samples]'
    end
    object AveragingWindowEdit: TEdit
      Left = 8
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '10'
    end
    object AbsoluteRangeEdit: TEdit
      Left = 8
      Top = 84
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '20'
    end
    object StableWindowEdit: TEdit
      Left = 8
      Top = 124
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '10'
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 56
    Width = 233
    Height = 153
    Caption = 'ADC'
    TabOrder = 8
    object Label1: TLabel
      Left = 11
      Top = 24
      Width = 62
      Height = 13
      Caption = 'Accuracy [%]'
    end
    object Label2: TLabel
      Left = 12
      Top = 64
      Width = 45
      Height = 13
      Caption = 'Rate [Hz]'
    end
    object Label8: TLabel
      Left = 12
      Top = 104
      Width = 82
      Height = 13
      Caption = 'Prefilter [samples]'
    end
    object Label9: TLabel
      Left = 147
      Top = 32
      Width = 22
      Height = 13
      Caption = 'Filter'
    end
    object AccuracyEdit: TEdit
      Left = 8
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '50'
    end
    object RateEdit: TEdit
      Left = 8
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '10'
    end
    object Panel2: TPanel
      Left = 144
      Top = 56
      Width = 73
      Height = 81
      TabOrder = 2
      object AdcFilterNoneRadioButton: TRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'None'
        Checked = True
        TabOrder = 0
        TabStop = True
      end
      object AdcFilterSinc3RadioButton: TRadioButton
        Left = 8
        Top = 32
        Width = 113
        Height = 17
        Caption = 'Sinc3'
        TabOrder = 1
      end
      object AdcFilterSinc5RadioButton: TRadioButton
        Left = 8
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Sinc5'
        TabOrder = 2
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 240
    Top = 56
    Width = 241
    Height = 153
    Caption = 'Mode'
    TabOrder = 9
    object Label4: TLabel
      Left = 100
      Top = 16
      Width = 45
      Height = 13
      Caption = 'Burst size'
    end
    object Label10: TLabel
      Left = 100
      Top = 56
      Width = 95
      Height = 13
      Caption = 'Averaging [samples]'
    end
    object Panel1: TPanel
      Left = 8
      Top = 16
      Width = 73
      Height = 105
      TabOrder = 0
      object DefaultModeRadioButton: TRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'Default'
        TabOrder = 0
      end
      object AllModeRadioButton: TRadioButton
        Left = 8
        Top = 32
        Width = 113
        Height = 17
        Caption = 'All'
        TabOrder = 1
      end
      object StableModeRadioButton: TRadioButton
        Left = 8
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Stable'
        TabOrder = 2
      end
      object Ps081ModeRadioButton: TRadioButton
        Left = 8
        Top = 80
        Width = 113
        Height = 17
        Caption = 'Ps081'
        Checked = True
        TabOrder = 3
        TabStop = True
      end
    end
    object ModeBurstSizeEdit: TEdit
      Left = 96
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '10'
    end
    object ModeAveragingEdit: TEdit
      Left = 96
      Top = 76
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '1'
    end
  end
  object PrefilterEdit: TEdit
    Left = 8
    Top = 176
    Width = 121
    Height = 21
    TabOrder = 10
    Text = '1'
  end
  object HexMemo: TMemo
    Left = 656
    Top = 24
    Width = 89
    Height = 185
    Lines.Strings = (
      'HexMemo')
    TabOrder = 11
  end
  object ProgramButton: TButton
    Left = 1128
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Program'
    TabOrder = 12
    OnClick = ProgramButtonClick
  end
  object ReadTimer: TTimer
    Enabled = False
    Interval = 50
    OnTimer = ReadTimerTimer
    Left = 480
    Top = 16
  end
  object FileOpenDialog: TOpenDialog
    Left = 448
    Top = 16
  end
end
