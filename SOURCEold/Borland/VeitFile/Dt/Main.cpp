//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Dt.h"

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

#include <stdio.h>

//******************************************************************************
//
//   Dt.c         Data/Time utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

// helper macros :
#define MonthDay( m)      _MDay[ (m) - 1]        // days of month
#define YearDay( m)       _YDay[ (m) - 1]        // days from begin of year
#define IsLeapYear( y)    (!((y) & 0x03))        // leap year

// constants :
#define DOW_1_1_2000      DT_SATURDAY            // dow for 1.1.2000
#define FOUR_YEAR_DAYS    1461                   // days of four years

// date internal representation :
typedef word TDtDate;                            // days from 1.1.2000

// local data :

// month days :
static const byte _MDay[12] = {
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

// month days from start of year :
static const word _YDay[12] = {
  0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};

#ifdef DT_ENABLE_DST

typedef struct {
   byte Month;
   byte Week;
   byte Dow;
   byte Hour;
   byte Min;
} TTransitionDay;

typedef struct {
   TTransitionDay Start;
   TTransitionDay End;
} TDstTransition;

static const TDstTransition DstTransition[ _DT_DST_TYPE_COUNT - 1] = {
// EU
{
   { DT_MARCH,   5, DT_SUNDAY, 2, 0},
   { DT_OCTOBER, 5, DT_SUNDAY, 2, 0}
},
// US
{
   { DT_MARCH,    2, DT_SUNDAY, 2, 0},
   { DT_NOVEMBER, 1, DT_SUNDAY, 2, 0}
}
};

#endif

// Local functions :

static TDtDate EncodeDate( byte Year, byte Month, byte Day);
// Encode <Local> to internal date representation

static void DecodeDate( TDtDate Date, byte *Year, byte *Month, byte *Day);
// Decode internal <Date> to split representation

static int GetDow( TDtDate Date);
// Returns dow code for <Date>

static int GetMonthDays( byte Year, byte Month);
// Returns month days

#ifdef DT_ENABLE_DST

static TYesNo EnableDst( void);
// Returns Yes if DST is enabled

static TYesNo IsDst( TLocalTime *Local);
// Returns YES for summer time. Input values are winter time

static byte GetTransitionDay( byte Year, const TTransitionDay *Transition);
// Returns transition day of month

#endif // DT_ENABLE_DST

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
/*
void __fastcall TForm1::Button1Click(TObject *Sender)
{
int Day, Month, Year;
TTransitionDay Transition;
int TrMonth, Week, Dow;

   sscanf( EdtDate->Text.c_str(), "%d.%d.%d", &Day, &Month, &Year);
   TrMonth = EdtMonth->Text.ToInt();
   Week  = EdtWeek->Text.ToInt();
   Dow   = EdtDow->Text.ToInt();
   Transition.Month = TrMonth;
   Transition.Week  = Week;
   Transition.Dow   = Dow;
   int TrDay = GetTransitionDay( 8, &Transition);
   LblResult->Caption = AnsiString( TrDay);
}
*/

void __fastcall TForm1::Button1Click(TObject *Sender)
{
int Day, Month, Year;
int Hour, Min;
TLocalTime Local;

   sscanf( EdtDate->Text.c_str(), "%d.%d.%d %d:%d", &Day, &Month, &Year, &Hour, &Min);
   Local.Day = Day;
   Local.Month = Month;
   Local.Year  = Year;
   Local.Hour  = Hour;
   Local.Min   = Min;
   Local.Sec   = 0;
   if( IsDst( &Local)){
      LblResult->Caption = "YES";
   } else {
      LblResult->Caption = "NO";
   }
}


//------------------------------------------------------------------------------
// Encode
//------------------------------------------------------------------------------

TTimestamp DtEncode( TLocalTime *Local)
// Encode date/time <Local> to internal representation
{
TTimestamp Timestamp;
TDtDate    Date;

   Date = EncodeDate( Local->Year, Local->Month, Local->Day);
#ifdef DT_ENABLE_DST
   if( EnableDst() && IsDst( Local)){
      // is summertime
      if( Local->Hour != 0){
         Local->Hour--;
      } else {
         // 00h - one day back
         Local->Hour = 23;
         Date--;
      }
   }
#endif // DT_ENABLE_DST
   Timestamp  = Local->Sec  * DT_SEC;
   Timestamp += Local->Min  * DT_MIN;
   Timestamp += Local->Hour * DT_HOUR;
   Timestamp += (TTimestamp)Date * DT_DAY;
   return( Timestamp);
} // DtEncode

//------------------------------------------------------------------------------
// Decode
//------------------------------------------------------------------------------

void DtDecode( TTimestamp Timestamp, TLocalTime *Local)
// Decode date/time from <Timestamp> to <Local>
{
   Local->Sec  = Timestamp % 60;       // seconds
   Timestamp  /= 60;                   // remove seconds
   Local->Min  = Timestamp % 60;       // minutes
   Timestamp  /= 60;                   // remove minutes
   Local->Hour = Timestamp % 24;       // hours
   Timestamp  /= 24;                   // remove hours
   DecodeDate( (TDtDate)Timestamp, &Local->Year, &Local->Month, &Local->Day);
#ifdef DT_ENABLE_DST
   if( EnableDst() && IsDst( Local)){
      // is summertime, add one hour
      Local->Hour++;
      if( Local->Hour == 24){
         // next day, compute date again
         Local->Hour = 0;
         DecodeDate( (TDtDate)Timestamp + 1,  &Local->Year, &Local->Month, &Local->Day);
      }
   }
#endif // DT_ENABLE_DST
} // DtDecode

//------------------------------------------------------------------------------
// Day of week
//------------------------------------------------------------------------------

int DtDow( TTimestamp Timestamp)
// Returns day of week
{
   Timestamp /= DT_DAY;                // remove time
   return( GetDow( (TDtDate)Timestamp));
} // DtDow

//------------------------------------------------------------------------------
// Time Validity
//------------------------------------------------------------------------------

int DtValidTime( TLocalTime *Local)
// Check time validity. Returns 0 if OK, else wrong field index
{
   if( Local->Sec > 59){
      return( DTE_SEC);
   }
   if( Local->Min > 59){
      return( DTE_MIN);
   }
   if( Local->Hour > 23){
      return( DTE_HOUR);
   }
   return( DTE_OK);
} // DtValidTime

//------------------------------------------------------------------------------
// Validity
//------------------------------------------------------------------------------

int DtValid( TLocalTime *Local)
// Check date/time validity. Returns 0 if OK, else wrong field index
{
int Result;
int LastDay;

   // check for time :
   Result = DtValidTime( Local);
   if( Result != DTE_OK){
      return( Result);
   }
   // check for date :
   if( Local->Month < DT_JANUARY || Local->Month > DT_DECEMBER){
      return( DTE_MONTH);
   }
   if( Local->Year > 99){
      return( DTE_YEAR);
   }
   LastDay = GetMonthDays( Local->Year, Local->Month);
   if( Local->Day < 1 || Local->Day > LastDay){
      return( DTE_DAY);
   }
   return( DTE_OK);
} // DtValid

//******************************************************************************

//------------------------------------------------------------------------------
// Encode date
//------------------------------------------------------------------------------

static TDtDate EncodeDate( byte Year, byte Month, byte Day)
// Encode <Local> to internal date representation
{
int Leaps;                  // count of leap days
int Tmp;

   Leaps = (Year + 4) / 4;             // 2000 (0) is first leap
   if( IsLeapYear( Year) && Month < DT_MARCH){
      Leaps--;                         // this year is leap, but low date
   }
   Tmp = Leaps + --Day;                // day of month startig at 0
   return( Tmp + (TDtDate)Year * 365 +  YearDay( Month));
} // EncodeDate

//******************************************************************************
// Na Datum
//******************************************************************************

static void DecodeDate( TDtDate Date, byte *Year, byte *Month, byte *Day)
// Decode internal <Date> to split representation
{
int Tmp;
int Y, M, D;
int YDay;
int YDays; // days of year

   Tmp  = Date / FOUR_YEAR_DAYS;     // count of four years
   YDay = Date % FOUR_YEAR_DAYS;     // offset by begin of four years
   Y    = Tmp * 4;                   // near low start of four years
   // correct year :
   while( 1){
      YDays = IsLeapYear( Y) ? 366 : 365;
      if( YDay < YDays){
         break;                      // ofset inside of year
      }
      Y++;                           // next year
      YDay -= YDays;
   }
   *Year = Y;
   // YDay is day of year, convert to MM:DD
   YDay++;                           // date starts at 1
   if( IsLeapYear( Y)){
      if( YDay > (31 + 29)){
         YDay--;                     // short by leap day
      } else if( YDay == (31 + 29)){
         *Day   = 29;                // is leap day now
         *Month = DT_FEBRUARY;
         return;
      }
   }
   M = DT_JANUARY;
   while( 1){
      D = MonthDay( M);              // day of month
      if( D >= YDay){
         break;
      }
      YDay -= D;                     // substract day of month
      M++;                           // next month
   }
   *Month = M;
   *Day   = (byte)YDay;
} // DecodeDate

//******************************************************************************
// Day of week
//******************************************************************************

static int GetDow( TDtDate Date)
// Returns dow code for <Date>
{
   return( (Date + DOW_1_1_2000) % 7);
} // GetDow

//******************************************************************************
// Month days
//******************************************************************************

static int GetMonthDays( byte Year, byte Month)
// Returns month days
{
   if( Month == DT_FEBRUARY && IsLeapYear( Year)){
      // february & leap year
      return( 29);
   } // other months, inclusive normal february
   return( MonthDay( Month));
} // GetMonthDays

#ifdef DT_ENABLE_DST

//******************************************************************************
// Enable
//******************************************************************************

static TYesNo EnableDst( void)
// Returns Yes if DST is enabled
{
   return( DtGetDstType() != DT_DST_TYPE_OFF);
} // EnableDst

//******************************************************************************
// Letni cas
//******************************************************************************

static TYesNo IsDst( TLocalTime *Local)
// Returns YES for summer time. Input values are winter time
{
int StartDay;
const TTransitionDay *Start, *End;

   Start = &DstTransition[ DtGetDstType() - 1].Start;
   End   = &DstTransition[ DtGetDstType() - 1].End;
   if( Local->Month < Start->Month || Local->Month > End->Month){
      return( NO);       // out of summer time
   }
   if( Local->Month > Start->Month && Local->Month < End->Month){
      return( YES);      // surely summer time
   }
   if( Local->Month == Start->Month){
      // start of summer time
      StartDay = GetTransitionDay( Local->Year, Start);
      if( Local->Day > StartDay){
         return( YES);   // day after start
      }
      if( Local->Day < StartDay){
         return( NO);    // day before start
      }
      // transition day :
      if( Local->Hour > Start->Hour){
         return( YES);   // hour after transition
      }
      if( Local->Hour < Start->Hour){
         return( NO);
      }
      // transition hour :
      if( Local->Min  >= Start->Min){
         return( YES);   // min after transition
      } else {
         return( NO);    // min before transition
      }
   } else { // Month == DST_END_MONTH
      // end of summertime
      StartDay = GetTransitionDay( Local->Year, End);
      if( Local->Day > StartDay){
         return( NO);    // day after end
      }
      if( Local->Day < StartDay){
         return( YES);   // day before end
      }
      // transition day :
      if( Local->Hour > End->Hour){
         return( NO);    // hour after transition
      }
      if( Local->Hour < End->Hour){
         return( YES);   // hour before transition
      }
      // transition hour :
      if( Local->Min >= End->Min){
         return( NO);    // min after transition
      } else {
         return( YES);   // min before transition
      }
   }
} // IsDst

//******************************************************************************
// Transition Day
//******************************************************************************

static byte GetTransitionDay( byte Year, const TTransitionDay *Transition)
// Returns transition day of month
{
byte    LastDay;
byte    Day;
TDtDate Date;
byte    Dow;

   LastDay = GetMonthDays( Year, Transition->Month);
   Day   = Transition->Week * _DT_DOW_COUNT;            // get end of the week date
   if( Day > LastDay){
      Day = LastDay;                                    // month days saturation
   }
   Date  = EncodeDate( Year, Transition->Month, Day);   // internal representation
   Dow   = GetDow( Date);                   // day of week
   if( Dow < Transition->Dow){
      Dow += _DT_DOW_COUNT;                 // transition day is before Dow - rool back one week
   }
   Day  -= Dow;                             // monday
   Day  += Transition->Dow;                 // this day of week
   return( Day);
} // GetTransitionDay

#endif

