//******************************************************************************
//
//   Dt.h         Data/Time utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Dt_H__
   #define __Dt_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __DtDef_H__
   #include "DtDef.h"
#endif

TTimestamp DtEncode( TLocalTime *Local);
// Encode date/time <Local> to internal representation

void DtDecode( TTimestamp Timestamp, TLocalTime *Local);
// Decode date/time from <Timestamp> to <Local>

int DtDow( TTimestamp Timestamp);
// Returns day of week

int DtValidTime( TLocalTime *Local);
// Check time validity. Returns 0 if OK, else wrong field index

int DtValid( TLocalTime *Local);
// Check date/time validity. Returns 0 if OK, else wrong field index

#endif
