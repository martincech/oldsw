object Form1: TForm1
  Left = 329
  Top = 331
  Width = 498
  Height = 294
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LblResult: TLabel
    Left = 72
    Top = 64
    Width = 44
    Height = 13
    Caption = 'LblResult'
  end
  object Label1: TLabel
    Left = 224
    Top = 88
    Width = 30
    Height = 13
    Caption = 'Month'
  end
  object Label2: TLabel
    Left = 224
    Top = 128
    Width = 29
    Height = 13
    Caption = 'Week'
  end
  object Label3: TLabel
    Left = 224
    Top = 160
    Width = 22
    Height = 13
    Caption = 'Dow'
  end
  object Button1: TButton
    Left = 256
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object EdtDate: TEdit
    Left = 72
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '25.03.07 02:00'
  end
  object EdtMonth: TEdit
    Left = 264
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '1'
  end
  object EdtWeek: TEdit
    Left = 264
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '1'
  end
  object EdtDow: TEdit
    Left = 264
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '6'
  end
end
