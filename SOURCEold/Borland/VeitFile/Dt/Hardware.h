//******************************************************************************
//
//   Hardware.h    Parameters
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "../inc/uni.h"             // zakladni datove typy

#define EEP_PAGE_SIZE    64            // velikost stranky
#define EEP_SIZE      32768            // celkova kapacita

//------------------------------------------------------------------------------
// Keyboard
//------------------------------------------------------------------------------

// key/events definition :
typedef enum {
   // system events :
   K_NULL  = 0,                        // menu & window excluded
   _K_FIRSTUSER,

   // keyboard :
   K_ENTER = _K_FIRSTUSER,             // Enter
   K_LEFT,                             // Left arrow
   K_ESC,                              // Esc
   K_UP,                               // Up arrow
   K_RIGHT,                            // Right arrow
   K_DOWN,                             // Down arrow
   K_ON,                               // Power on/tara
   K_OFF,                              // Power off

   // events :
   _K_EVENTS    = 0x40,                // start events
   K_FLASH1     = _K_EVENTS,           // flashing 1
   K_FLASH2,                           // flashing 2
   K_REDRAW,                           // 1s redraw
   K_SHUTDOWN,                         // power shutdown
   K_TIMEOUT,                          // inactivity timeout

   // system keys/flags :
   K_REPEAT       = 0x80,              // repeat key (ored with key)
   K_RELEASED     = 0xFE,              // release key (single and repeat)
   K_IDLE         = 0xFF               // internal use, empty read cycle
} TKeys;

//-----------------------------------------------------------------------------
// Date/Time
//-----------------------------------------------------------------------------

#define DT_ENABLE_DST  1        // enable daylight saving time compilation
#define DtGetDstType()        DT_DST_TYPE_EU

//-----------------------------------------------------------------------------
// File System
//-----------------------------------------------------------------------------

#define	FDIR_SIZE         10                     // Maximum number of files (directory entries)
#define FS_BLOCK_RECORDS  4                      // records per block
#define FAT_SIZE          250                    // FAT size (total blocks count)

#define FS_OFFSET         offsetof( TEeprom, Filesystem)  // filesystem offset (from EEP start)

//-----------------------------------------------------------------------------
// Statistika
//-----------------------------------------------------------------------------

typedef double TNumber;                 // zakladni datovy typ

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

typedef word THistogramCount;           // pocet vzorku
typedef long THistogramValue;           // hodnota vzorku, musi byt signed
#define HISTOGRAM_MIN_STEP    1         // Minimalni hodnota kroku v THistogramValue

#define HISTOGRAM_SLOTS       39        // pocet sloupcu - sude i liche cislo (max.254)

//-----------------------------------------------------------------------------

//#include "MemoryDef.h"     // EEPROM offset calculations

#endif
