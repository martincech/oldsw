//******************************************************************************
//
//   DtDef.h      Date/Time definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DtDef_H__
   #define __DtDef_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

// Internal representation of date/time :
typedef int32 TTimestamp;

// Internal representation constants :
#define DT_SEC  1
#define DT_MIN  60
#define DT_HOUR 3600
#define DT_DAY  86400

// Date/time structure :
typedef struct {
   byte Sec;                 // 0..59
   byte Min;                 // 0..59
   byte Hour;                // 0..23
   byte Day;                 // 1..31
   byte Month;               // 1..12
   byte Year;                // year = 2000 + <Year>
} TLocalTime;

// Day of week :
typedef enum {
   DT_MONDAY,
   DT_TUESDAY,
   DT_WEDNESDAY,
   DT_THURSDAY,
   DT_FRIDAY,
   DT_SATURDAY,
   DT_SUNDAY,
   _DT_DOW_COUNT
} TDtDow;

// Months :
typedef enum {
   DT_JANUARY = 1,
   DT_FEBRUARY,
   DT_MARCH,
   DT_APRIL,
   DT_MAY,
   DT_JUNE,
   DT_JULE,
   DT_AUGUST,
   DT_SEPTEMBER,
   DT_OCTOBER,
   DT_NOVEMBER,
   DT_DECEMBER,
   _DT_MONTH_COUNT
} TDtMonth;

// Field errors :
typedef enum {
   DTE_OK,                    // date/time OK
   DTE_SEC,                   // wrong second
   DTE_MIN,                   // wrong minute
   DTE_HOUR,                  // wrong hour
   DTE_DAY,                   // wrong day
   DTE_MONTH,                 // wrong month
   DTE_YEAR                   // wrong year
} TDtError;

// Daylight saving :
typedef enum {
   DT_DST_TYPE_OFF,
   DT_DST_TYPE_EU,
   DT_DST_TYPE_US,
   _DT_DST_TYPE_COUNT
} TDtDstType;

#endif
