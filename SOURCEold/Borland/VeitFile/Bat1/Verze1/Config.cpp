//******************************************************************************
//
//   Config.c     Configuration utilities
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "Config.h"
#include "../Inc/Eep.h"
#include "Bat1.h"
#include "MemoryDef.h"              // EEPROM layout
#include <string.h>


TConfig Config;              // global configuration data

#define CONFIG_ADDRESS  offsetof( TEeprom, Config)

// Local functions :

static TConfigCrc ConfigCrc( void);
// Calculate CRC

static void ConfigDefault( void);
// Set default values

//------------------------------------------------------------------------------
// Load
//------------------------------------------------------------------------------

void ConfigLoad( void)
// Load configuration from EEPROM
{
   EepLoadData( CONFIG_ADDRESS, &Config, sizeof( TConfig));
   if( Config.Version != BAT1_VERSION){
      // version chage, set defaults
      ConfigDefault();
      ConfigSave();
      return;
   }
   if( ConfigCrc() != Config.CheckSum){
      // wrong checksum, set defaults
      ConfigDefault();
      ConfigSave();
      return;
   }
   if( Config.Build != BAT1_BUILD || Config.HwVersion != BAT1_HW_VERSION){
      // hardware change or new build
      ConfigSave();                    // save all
   }
} // ConfigLoad

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

void ConfigSave( void)
// Save configuration to EEPROM
{
   Config.CheckSum = ConfigCrc();
   if( EepMatchData( CONFIG_ADDRESS, &Config, sizeof( TConfig))){
      return;                          // match ok, already saved
   }
   EepSaveData( CONFIG_ADDRESS, &Config, sizeof( TConfig));
} // ConfigSave

//------------------------------------------------------------------------------
// Save Fragment
//------------------------------------------------------------------------------

void ConfigSaveFragment( int Offset, int Size)
// Save configuration fragment
{
   Config.CheckSum = ConfigCrc();
   if( EepMatchData( CONFIG_ADDRESS + Offset, (byte *)&Config + Offset, Size)){
      return;                          // match ok, already saved
   }
   // save data fragment :
   EepSaveData( CONFIG_ADDRESS + Offset, (byte *)&Config + Offset, Size);
   // update CRC :
   EepSaveData( CONFIG_ADDRESS + offsetof( TConfig, CheckSum), &Config.CheckSum, sizeof( TConfigCrc));
} // ConfigSaveFragment

//------------------------------------------------------------------------------
// File Config
//------------------------------------------------------------------------------

void ConfigFillFileConfig( TFileConfig *FileConfig)
// Load current file config data to <FileConfig>
{
   *FileConfig = Config.FileConfig;   // fill with Config defaults
} // ConfigFillFileConfig

//******************************************************************************

//------------------------------------------------------------------------------
// CRC
//------------------------------------------------------------------------------

#define CONFIG_DATA_SIZE (int)(sizeof( TConfig) - sizeof( TConfigCrc))

static TConfigCrc ConfigCrc( void)
// Calculate CRC
{
byte    *p;
TCalCrc Crc;
int     i;

   p   = (byte *)&Config;
   Crc = 0;
   for( i = 0; i < CONFIG_DATA_SIZE; i++){
      Crc += *p;
      p++;
   }
   return( ~Crc);
} // ConfigCrc

//------------------------------------------------------------------------------
// Default
//------------------------------------------------------------------------------

static void ConfigDefault( void)
// Set default values
{
   memset( &Config, 0, sizeof( TConfig));
   // version data :
   Config.Version           = BAT1_VERSION;
   Config.Build             = BAT1_BUILD;
   Config.HwVersion         = BAT1_HW_VERSION;
   strcpy( Config.ScaleName, BAT1_SCALE_NAME);
   memset( Config.Password, K_ENTER, sizeof( Config.Password));

   // country :
   Config.Country           = BAT1_DEFAULT_COUNTRY;

   // units :
/*
   Config.Units.Units       = UNITS_G;
   Config.Units.Range       = UNITS_G_RANGE;
   Config.Units.Decimals    = UNITS_G_DECIMALS;
*/
   Config.Units.Units       = UNITS_KG;
   Config.Units.Range       = UNITS_KG_RANGE;
   Config.Units.Decimals    = UNITS_KG_DECIMALS;
   Config.Units.MaxDivision = UNITS_KG_DIVISION;
   // sound :
   Config.Sounds.Volume    = VOLUME_MAX;

   // display :
   Config.Display.Mode                = DISPLAY_MODE_BASIC;
   Config.Display.Backlight.Mode      = BACKLIGHT_MODE_AUTO;
   Config.Display.Backlight.Intensity = BACKLIGHT_MAX;

   // last working file :
   Config.LastFile = FDIR_INVALID;

   // default file configuration :
   Config.FileConfig.SavingMode = SAVING_MODE_MANUAL;

   Config.FileConfig.WeightLimits.Mode      = WEIGHT_LIMITS_NONE;
   Config.FileConfig.WeightLimits.LowLimit  = 0;
   Config.FileConfig.WeightLimits.HighLimit = 0;

   Config.FileConfig.SavingParameters.Inertia       = 10;
   Config.FileConfig.SavingParameters.Sustain       = 20;
   Config.FileConfig.SavingParameters.Accuracy      = 10;
   Config.FileConfig.SavingParameters.Trigger       = 60;
   Config.FileConfig.SavingParameters.Empty         = 50;

   Config.FileConfig.StatisticParameters.UniformityRange  = 10;
   Config.FileConfig.StatisticParameters.Histogram.Range  = 10;
   Config.FileConfig.StatisticParameters.Histogram.Step   =  0;
} // ConfigDefault
