//*****************************************************************************
//
//    Calc.h - Statistic calculations
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __Calc_H__
   #define __Calc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Sample_H__
   #include "SampleDef.h"              // weight sample definition
#endif

#ifndef __FileCfg_H__
   #include "FileCfg.h"                // Statistic parameters
#endif

#ifndef __Histogram_H__
   #include "../Inc/Histogram.h"
#endif

//-----------------------------------------------------------------------------
// Data
//-----------------------------------------------------------------------------

// Calculation results :

typedef struct {
   word       Count;             // Samples count
   TWeight    Average;           // Average weight
   TWeight    MinWeight;         // Minimal weight
   TWeight    MaxWeight;         // Maximal weight
   TWeight    Sigma;             // Sigma [format 0.000]
   byte       Cv;                // CV [percent]
   byte       Uniformity;        // Uniformity [percent]
   THistogram Histogram;         // Histogram
} TCalc;

extern TCalc Calc;

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void CalcClear(void);
// Clear results

void CalcPrimary( TSampleFlag Flag);
// Calculate primary statistics

void CalcSecondary( TSampleFlag Flag, TStatisticParameters *Parameters);
// Calculate secondary statistics

void CalcStatistics( TSampleFlag Flag, TStatisticParameters *Parameters);
// Calculate statistic of the dataset, get samples with <Flag> only (FLAG_NONE = all samples)

void CalcAppend( TWeight Weight, TStatisticParameters *Parameters);
// Append sample & recalculate

#endif

