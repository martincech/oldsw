//******************************************************************************
//
//   Main.c       Tester main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainFs.h"
#include <stdio.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------

#include "../Inc/File/Fdir.h"
#include "../Inc/File/Fs.h"
#include "../Inc/File/Fd.h"
#include "../Inc/System.h"
#include "SampleDef.h"


#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

#define DEMO_CLASS 1

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Initialisation
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   FsInit();
}  // FormCreate

//******************************************************************************
// Format
//******************************************************************************

void __fastcall TMainForm::BtnFormatClick(TObject *Sender)
{
   FsFormat();
   LbFile->Clear();
   LblFile->Caption = "File : ?";
   DirectoryRedraw();
   StatusOk();
} // BtnFormatClick

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::BtnCreateClick(TObject *Sender)
{
char Name[ FDIR_NAME_LENGTH + 1];

   strcpy( Name, EdtName->Text.c_str());
   if( FdExists( Name)){
      Status() + "File already exists";
      return;
   }
   if( !FsCreate( Name, DEMO_CLASS)){
      Status() + "Directory full";
      return;
   }
   DirectoryRedraw();
   FileRedraw();
   StatusOk();
} // BtnCreateClick

//******************************************************************************
// Open
//******************************************************************************

void __fastcall TMainForm::BtnOpenClick(TObject *Sender)
{
TFdirHandle Handle;
TFdirInfo   Item;

   if( LbDirectory->ItemIndex < 0) {
      Status() + "No file selected";
      return;
   }
   AnsiString ItemName = LbDirectory->Items->Strings[ LbDirectory->ItemIndex];
   Handle = FdirSearch( ItemName.c_str(), DEMO_CLASS);
   if( !Handle){
      Status() + ItemName + " unable find";
      return;
   }
   FsOpen( Handle, NO);
   Status() + ItemName + " opened OK";
   FileRedraw();
} // BtnOpenClick

//******************************************************************************
// Close
//******************************************************************************

void __fastcall TMainForm::BtnCloseClick(TObject *Sender)
{
   FsClose();
   LbFile->Clear();
   LblFile->Caption = "File : ?";
   StatusOk();
} // BtnCloseClick

//******************************************************************************
// Delete
//******************************************************************************

void __fastcall TMainForm::BtnDeleteClick(TObject *Sender)
{
   FsDelete();
   LbFile->Clear();
   LblFile->Caption = "File : ?";
   DirectoryRedraw();
   StatusOk();
} // BtnDeleteClick

//******************************************************************************
// Empty
//******************************************************************************

void __fastcall TMainForm::BtnEmptyClick(TObject *Sender)
{
   FsEmpty();
   FileRedraw();
   StatusOk();
} // BtnEmptyClick

//******************************************************************************
// Fill file
//******************************************************************************

void __fastcall TMainForm::BtnFileFillClick(TObject *Sender)
{
int Count;
TSample Record;

   Count = EdtCount->Text.ToInt();
   Record.Flag = 0;
   StatusOk();
   for( int i = 0; i < Count; i++){
      Record.Weight = i;
      if( !FsWrite( &Record, sizeof( Record))){
         Status() + "Unable append record";
         break;
      }
   }
   FileRedraw();
} // BtnFileFillClick

//******************************************************************************
// Redraw Directory
//******************************************************************************

void TMainForm::DirectoryRedraw( void)
{
TFdirHandle Handle;
char Name[ FDIR_NAME_LENGTH + 1];

   LbDirectory->Clear();
   FdSetClass( DEMO_CLASS);
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      FdGetName( Handle, Name);
      LbDirectory->Items->Add( Name);
   }
} // DirectoryRedraw

//******************************************************************************
// Redraw File
//******************************************************************************

void TMainForm::FileRedraw( void)
{
TSample Record;

   TFdirInfo *Dir = FsInfo();
   if( Dir){
      LblFile->Caption = AnsiString("File : ") + Dir->Name;
   } else {
      LblFile->Caption = "File : ?";
   }
   LbFile->Clear();
   FsRSeek( 0, FS_SEEK_SET);
   while( FsRead( &Record, sizeof( Record))){
      LbFile->Items->Add( AnsiString( Record.Weight));
   }
   LblFree->Caption = AnsiString( "Free Space : ") + FsAvailable();
} // FileRedraw

//******************************************************************************
// Fill directory
//******************************************************************************

#define DIR_ITEMS 10
#define REC_ITEMS 100

void __fastcall TMainForm::BtnFillClick(TObject *Sender)
{
char Name[ FDIR_NAME_LENGTH + 1];
TSample Record;

   FsFormat();
   Record.Flag = 0;
   for( int i = 0; i < DIR_ITEMS; i++){
      sprintf( Name, "FILE%02d", i);
      if( !FsCreate( Name, DEMO_CLASS)){
         Status() + "Directory full";
         return;
      }
      for( int j = 0; j < REC_ITEMS; j++){
         Record.Weight = i * 100 + j;
         if( !FsWrite( &Record, sizeof( Record))){
            Status() + "Unable append record";
            break;
         }
      }
      FsClose();
   }
   DirectoryRedraw();
   LbFile->Clear();
   StatusOk();
} // BtbFillClick

//******************************************************************************
// Fd Count
//******************************************************************************

void __fastcall TMainForm::BtnFdCountClick(TObject *Sender)
{
   int Count = FdCount();
   EdtFdCount->Text = AnsiString( Count);
   StatusOk();
} // BtnFdCountClick


//******************************************************************************
// Fd Move
//******************************************************************************

void __fastcall TMainForm::BtnFdMoveClick(TObject *Sender)
{
   int Index = EdtFdIndex->Text.ToInt();
   LblFdName->Caption = "?";
   if( !FdMoveAt( Index)){
      Status() + " Invalid index";
      return;
   }
   TFdirHandle Handle;
   if( (Handle = FdFindNext()) == FDIR_INVALID){
      Status() + " no item found";
      return;
   }
   char Name[ FDIR_NAME_LENGTH + 1];
   FdGetName( Handle, Name);
   LblFdName->Caption = Name;
   StatusOk();
} // BtnFdMoveClick

//******************************************************************************
// Time
//******************************************************************************

void __fastcall TMainForm::BtnTimeClick(TObject *Sender)
{
TTimestamp Ts;
TLocalTime Local;

   Ts = SysGetClock();
   DtDecode( Ts, &Local);
   AnsiString Text;
   Text.printf( "%02d.%02d.%02d %02d:%02d:%02d", Local.Day, Local.Month, Local.Year,
                                                 Local.Hour, Local.Min, Local.Sec);
   EdtDate->Text = Text;
   TTimestamp TsNew = DtEncode( &Local);
   LblTime->Caption = AnsiString( TsNew);
   StatusOk();
} // BtnTimeClick

//******************************************************************************
// Truncate
//******************************************************************************

void __fastcall TMainForm::BtnTruncateClick(TObject *Sender)
{
   int Size = EdtSize->Text.ToInt();
   FsTruncate( Size);
   FileRedraw();
   StatusOk();
} // BtnTruncateClick

