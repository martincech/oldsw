object MainForm: TMainForm
  Left = 133
  Top = 177
  Width = 962
  Height = 640
  Caption = 'FileTester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Directory :'
  end
  object LblFile: TLabel
    Left = 400
    Top = 8
    Width = 22
    Height = 13
    Caption = 'File :'
  end
  object Label3: TLabel
    Left = 272
    Top = 88
    Width = 34
    Height = 13
    Caption = 'Count :'
  end
  object Label4: TLabel
    Left = 272
    Top = 136
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object LblFree: TLabel
    Left = 264
    Top = 8
    Width = 70
    Height = 13
    Caption = 'Free Space : ?'
  end
  object Label2: TLabel
    Left = 552
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Data Set :'
  end
  object LblData: TLabel
    Left = 680
    Top = 8
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object Label6: TLabel
    Left = 816
    Top = 80
    Width = 28
    Height = 13
    Caption = 'Count'
  end
  object Label7: TLabel
    Left = 816
    Top = 120
    Width = 40
    Height = 13
    Caption = 'Average'
  end
  object Label8: TLabel
    Left = 816
    Top = 160
    Width = 17
    Height = 13
    Caption = 'Min'
  end
  object Label9: TLabel
    Left = 816
    Top = 200
    Width = 20
    Height = 13
    Caption = 'Max'
  end
  object Label10: TLabel
    Left = 816
    Top = 240
    Width = 29
    Height = 13
    Caption = 'Sigma'
  end
  object Label11: TLabel
    Left = 816
    Top = 280
    Width = 13
    Height = 13
    Caption = 'Cv'
  end
  object Label12: TLabel
    Left = 816
    Top = 320
    Width = 46
    Height = 13
    Caption = 'Uniformity'
  end
  object LblFdName: TLabel
    Left = 272
    Top = 448
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblTime: TLabel
    Left = 272
    Top = 504
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LbDirectory: TListBox
    Left = 24
    Top = 32
    Width = 121
    Height = 289
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = LbDirectoryDblClick
  end
  object BtnFormat: TButton
    Left = 32
    Top = 440
    Width = 75
    Height = 25
    Caption = 'Format'
    TabOrder = 1
    OnClick = BtnFormatClick
  end
  object BtnCreate: TButton
    Left = 32
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Create'
    TabOrder = 2
    OnClick = BtnCreateClick
  end
  object BtnOpen: TButton
    Left = 32
    Top = 520
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 3
    OnClick = BtnOpenClick
  end
  object BtnDelete: TButton
    Left = 176
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 4
    OnClick = BtnDeleteClick
  end
  object EdtName: TEdit
    Left = 24
    Top = 336
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'FILE'
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 594
    Width = 954
    Height = 19
    Panels = <
      item
        Text = 'Status : ?'
        Width = 200
      end>
    SimplePanel = False
  end
  object BtnFill: TButton
    Left = 32
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Fill'
    TabOrder = 7
    OnClick = BtnFillClick
  end
  object BtnClose: TButton
    Left = 32
    Top = 560
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 8
    OnClick = BtnCloseClick
  end
  object BtnEmpty: TButton
    Left = 176
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Empty'
    TabOrder = 9
    OnClick = BtnEmptyClick
  end
  object LbFile: TListBox
    Left = 400
    Top = 24
    Width = 121
    Height = 561
    ItemHeight = 13
    TabOrder = 10
    OnDblClick = LbFileDblClick
  end
  object BtnFileFill: TButton
    Left = 176
    Top = 112
    Width = 75
    Height = 25
    Caption = 'File Fill'
    TabOrder = 11
    OnClick = BtnFileFillClick
  end
  object EdtCount: TEdit
    Left = 272
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 12
    Text = '100'
  end
  object EdtData: TEdit
    Left = 272
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 13
    Text = '?'
  end
  object LbFileSet: TListBox
    Left = 544
    Top = 24
    Width = 121
    Height = 289
    ItemHeight = 13
    TabOrder = 14
  end
  object BtnDsClear: TButton
    Left = 544
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Clear'
    TabOrder = 15
    OnClick = BtnDsClearClick
  end
  object BtnDsDelete: TButton
    Left = 544
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 16
    OnClick = BtnDsDeleteClick
  end
  object BtnDsProcess: TButton
    Left = 544
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Process'
    TabOrder = 17
    OnClick = BtnDsProcessClick
  end
  object LbData: TListBox
    Left = 672
    Top = 24
    Width = 121
    Height = 561
    ItemHeight = 13
    TabOrder = 18
  end
  object BtnSetCurrent: TButton
    Left = 544
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Set Current'
    TabOrder = 19
    OnClick = BtnSetCurrentClick
  end
  object BtnStatistics: TButton
    Left = 544
    Top = 512
    Width = 75
    Height = 25
    Caption = 'Statistics'
    TabOrder = 20
    OnClick = BtnStatisticsClick
  end
  object EdtSamplesCount: TEdit
    Left = 816
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 21
    Text = '0'
  end
  object EdtAverage: TEdit
    Left = 816
    Top = 136
    Width = 121
    Height = 21
    TabOrder = 22
    Text = '0'
  end
  object EdtMin: TEdit
    Left = 816
    Top = 176
    Width = 121
    Height = 21
    TabOrder = 23
    Text = '0'
  end
  object EdtMax: TEdit
    Left = 816
    Top = 216
    Width = 121
    Height = 21
    TabOrder = 24
    Text = '0'
  end
  object EdtSigma: TEdit
    Left = 816
    Top = 256
    Width = 121
    Height = 21
    TabOrder = 25
    Text = '0'
  end
  object EdtCv: TEdit
    Left = 816
    Top = 296
    Width = 121
    Height = 21
    TabOrder = 26
    Text = '0'
  end
  object EdtUniformity: TEdit
    Left = 816
    Top = 336
    Width = 121
    Height = 21
    TabOrder = 27
    Text = '0'
  end
  object Panel1: TPanel
    Left = 808
    Top = 368
    Width = 145
    Height = 97
    TabOrder = 28
    object BtnDefaults: TButton
      Left = 40
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Set Defaults'
      TabOrder = 0
      OnClick = BtnDefaultsClick
    end
    object BtnSaveCfg: TButton
      Left = 40
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Save Config'
      TabOrder = 1
      OnClick = BtnSaveCfgClick
    end
  end
  object BtnFdCount: TButton
    Left = 176
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Dir Count'
    TabOrder = 29
    OnClick = BtnFdCountClick
  end
  object BtnFdMove: TButton
    Left = 176
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Dir Move'
    TabOrder = 30
    OnClick = BtnFdMoveClick
  end
  object EdtFdCount: TEdit
    Left = 272
    Top = 368
    Width = 121
    Height = 21
    TabOrder = 31
    Text = '0'
  end
  object EdtFdIndex: TEdit
    Left = 272
    Top = 408
    Width = 121
    Height = 21
    TabOrder = 32
    Text = '0'
  end
  object EdtDate: TEdit
    Left = 272
    Top = 472
    Width = 121
    Height = 21
    TabOrder = 33
    Text = '1.1.07'
  end
  object BtnTime: TButton
    Left = 176
    Top = 472
    Width = 75
    Height = 25
    Caption = 'Time'
    TabOrder = 34
    OnClick = BtnTimeClick
  end
  object BtnDeleteItem: TButton
    Left = 176
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Delete Item'
    TabOrder = 35
    OnClick = BtnDeleteItemClick
  end
end
