//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TListBox *LbDirectory;
        TLabel *Label1;
        TButton *BtnFormat;
        TButton *BtnCreate;
        TButton *BtnOpen;
        TButton *BtnDelete;
        TEdit *EdtName;
        TStatusBar *StatusBar;
        TButton *BtnFill;
        TButton *BtnClose;
        TButton *BtnEmpty;
        TListBox *LbFile;
        TLabel *LblFile;
        TButton *BtnFileFill;
        TEdit *EdtCount;
        TLabel *Label3;
        TEdit *EdtData;
        TLabel *Label4;
        TLabel *LblFree;
        TListBox *LbFileSet;
        TLabel *Label2;
        TButton *BtnDsClear;
        TButton *BtnDsDelete;
        TButton *BtnDsProcess;
        TListBox *LbData;
        TLabel *LblData;
        TButton *BtnSetCurrent;
        TButton *BtnStatistics;
        TEdit *EdtSamplesCount;
        TLabel *Label6;
        TLabel *Label7;
        TEdit *EdtAverage;
        TLabel *Label8;
        TEdit *EdtMin;
        TLabel *Label9;
        TEdit *EdtMax;
        TLabel *Label10;
        TEdit *EdtSigma;
        TLabel *Label11;
        TEdit *EdtCv;
        TLabel *Label12;
        TEdit *EdtUniformity;
        TPanel *Panel1;
        TButton *BtnDefaults;
        TButton *BtnSaveCfg;
        TButton *BtnFdCount;
        TButton *BtnFdMove;
        TEdit *EdtFdCount;
        TEdit *EdtFdIndex;
        TLabel *LblFdName;
        TEdit *EdtDate;
        TButton *BtnTime;
        TLabel *LblTime;
        TButton *BtnDeleteItem;
        void __fastcall BtnFormatClick(TObject *Sender);
        void __fastcall BtnCreateClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnOpenClick(TObject *Sender);
        void __fastcall BtnDeleteClick(TObject *Sender);
        void __fastcall BtnFillClick(TObject *Sender);
        void __fastcall BtnCloseClick(TObject *Sender);
        void __fastcall BtnEmptyClick(TObject *Sender);
        void __fastcall BtnFileFillClick(TObject *Sender);
        void __fastcall LbDirectoryDblClick(TObject *Sender);
        void __fastcall BtnDsClearClick(TObject *Sender);
        void __fastcall BtnDsDeleteClick(TObject *Sender);
        void __fastcall BtnDsProcessClick(TObject *Sender);
        void __fastcall BtnSetCurrentClick(TObject *Sender);
        void __fastcall BtnStatisticsClick(TObject *Sender);
        void __fastcall BtnDefaultsClick(TObject *Sender);
        void __fastcall BtnSaveCfgClick(TObject *Sender);
        void __fastcall BtnFdCountClick(TObject *Sender);
        void __fastcall BtnFdMoveClick(TObject *Sender);
        void __fastcall BtnTimeClick(TObject *Sender);
        void __fastcall BtnDeleteItemClick(TObject *Sender);
        void __fastcall LbFileDblClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
        void DirectoryRedraw( void);
        void FileRedraw( void);
        void CalcDisplay( void);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
