//******************************************************************************
//
//   FsRecord.h   File system record definition
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __FsRecord_H__
   #define __FsRecord_H__

#ifndef __SampleDef_H__
   #include "SampleDef.h"
#endif

// data record :
typedef TSample TFileRecord;

#endif
 
