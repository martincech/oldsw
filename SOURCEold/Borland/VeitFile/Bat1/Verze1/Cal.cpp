//******************************************************************************
//
//   Cal.c        Calibration utilities
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
#endif

#include "Cal.h"
#include "../Inc/Eep.h"             // EEPROM services
#include "MemoryDef.h"              // EEPROM layout

static TCalibration Calibration;

// EEPROM address :
#define CAL_ADDRESS  offsetof( TEeprom, Calibration)

// Local functions :

static void CalDefault( void);
// Default values

static TCalCrc CalCrc( void);
// Calculate CRC

//------------------------------------------------------------------------------
// Load
//------------------------------------------------------------------------------

void CalLoad( void)
// Load calibration from EEPROM
{
   EepLoadData( CAL_ADDRESS, &Calibration, sizeof( TCalibration));
   if( CalCrc() == Calibration.CheckSum){
      return;                          // load ok
   }
   // wrong checksum, set defaults
   CalDefault();
   CalSave();
} // CalLoad

//------------------------------------------------------------------------------
// Save
//------------------------------------------------------------------------------

void CalSave( void)
// Save calibration to EEPROM
{
   Calibration.CheckSum = CalCrc();
   EepSaveData( CAL_ADDRESS, &Calibration, sizeof( TCalibration));
} // CalSave

//------------------------------------------------------------------------------
// Update
//------------------------------------------------------------------------------

void CalUpdate( TRawWeight RawZero, TRawWeight RawRange, TWeight Range)
// Update calibration : ADC value <RawZero> by zero, <RawRange> by physical <Range>
{
   Calibration.Offset = RawZero;
   Calibration.Factor = RawRange - RawZero;
   Calibration.Range  = Range;
} // CalUpdate

//------------------------------------------------------------------------------
// Division
//------------------------------------------------------------------------------

void CalDivision( TWeight Division)
// Set <Division>
{
   Calibration.Division = Division;
} // CalDivision

//------------------------------------------------------------------------------
// Get range
//------------------------------------------------------------------------------

TWeight CalGetRange( void)
// Returns physical range
{
   return( Calibration.Range);
} // CalGetRange

//------------------------------------------------------------------------------
// Get division
//------------------------------------------------------------------------------

TWeight CalGetDivision( void)
// Returns division
{
   return( Calibration.Division);
} // CalGetDivision

//------------------------------------------------------------------------------
// Weight
//------------------------------------------------------------------------------

TWeight CalWeight( TRawWeight RawValue)
// Calculate physical weight by <RawValue>
{
TWeight Weight;

   RawValue -= Calibration.Offset;
   Weight = (TWeight)(((TMulWeight)RawValue * Calibration.Range) / Calibration.Factor);
   if( Calibration.Division <= 1){
      return( Weight);                 // no division
   }
   // Add division/2 for round up :
   Weight <<= 1;                       // Weight * 2
   if( Weight >= 0){
      Weight  += Calibration.Division; // add division
   } else {
      Weight  -= Calibration.Division; // add division
   }
   // Make division :
   Weight  /= (Calibration.Division << 1); // Weight / 2
   Weight  *=  Calibration.Division;
   return( Weight);
} // CalWeight

//------------------------------------------------------------------------------
// Raw Weight
//------------------------------------------------------------------------------

TWeight CalRawWeight( TWeight Value)
// Calculate raw weight by physical <Value>
{
TRawWeight RawValue;

   RawValue  = (TRawWeight)(((TMulWeight)Value * Calibration.Factor) / Calibration.Range);
   RawValue += Calibration.Offset;
   return( RawValue);
} // CalRawWeight

//******************************************************************************

//------------------------------------------------------------------------------
// Default
//------------------------------------------------------------------------------

static void CalDefault( void)
// Default values
{
   Calibration.Offset   = 0;
   Calibration.Factor   = 1000;
   Calibration.Range    = 1000;
   Calibration.Division = 1;
} // CalDefault

//------------------------------------------------------------------------------
// CRC
//------------------------------------------------------------------------------

#define CAL_DATA_SIZE (int)(sizeof( TCalibration) - sizeof( TCalCrc))

static TCalCrc CalCrc( void)
// Calculate CRC
{
byte    *p;
TCalCrc Crc;
int     i;

   p   = (byte *)&Calibration;
   Crc = 0;
   for( i = 0; i < CAL_DATA_SIZE; i++){
      Crc += *p;
      p++;
   }
   return( ~Crc);
} // CalCrc

