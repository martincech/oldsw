//******************************************************************************
//
//   Config.h     Configuration utilities
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __Config_H__
   #define __Config_H__

#ifndef __ConfigDef_H__
   #include "ConfigDef.h"
#endif

void ConfigLoad( void);
// Load configuration from EEPROM

void ConfigSave( void);
// Save configuration to EEPROM

void ConfigSaveFragment( int Offset, int Size);
// Save configuration fragment

#define ConfigSaveSection( Section) \
        ConfigSaveFragment( offsetof( TConfig, Section), sizeof( Config.Section));
// Save section of the configuration

void ConfigFillFileConfig( TFileConfig *FileConfig);
// Load current file config data to <FileConfig>

#endif
