//******************************************************************************
//
//   DbRecord.h   Database record definition
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DbRecord_H__
   #define __DbRecord_H__

#ifndef __SampleDef_H__
   #include "SampleDef.h"
#endif

// data record :
typedef TSample TDbRecord;

// record flag :
typedef TSampleFlag TDbFlag;
#define DB_FLAG_OFFSET        offsetof( TDbRecord, Flag)
#define DB_FLAG_SIZE          sizeof( TDbFlag)
#define DbIsDeleted( Flag)    ((Flag)  & FLAG_DELETED) // deleted record
#define DbSetDeleted( Flag)     Flag  |= FLAG_DELETED  // mark as deleted

#endif
 
