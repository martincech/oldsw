//*****************************************************************************
//
//    Bat1.h   -  Bat1 project definitions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Bat1_H__
   #define __Bat1_H__

// Device data :
#define BAT1_VERSION          0x0101         // Version number
#define BAT1_BUILD            0x01           // Build number
#define BAT1_HW_VERSION       0x01           // Hardware version
#define BAT1_SCALE_NAME       "BAT1"         // Default scale name

// Country data :
#define BAT1_DEFAULT_COUNTRY  COUNTRY_INTERNATIONAL

// Units :
#define UNITS_KG_RANGE       20000
#define UNITS_KG_DECIMALS    3
#define UNITS_KG_DIVISION    100

#define UNITS_G_RANGE        20000
#define UNITS_G_DECIMALS     0
#define UNITS_G_DIVISION     100

#define UNITS_LB_RANGE       60000
#define UNITS_LB_DECIMALS    3
#define UNITS_LB_DIVISION    100

#define UNITS_OZ_RANGE       60000
#define UNITS_OZ_DECIMALS    1
#define UNITS_OZ_DIVISION    100

// Saving parameters :
#define SAVING_MAX_INERTIA   50         // 0.1s
#define SAVING_MAX_SUSTAIN   50         // 0.1s

#endif
