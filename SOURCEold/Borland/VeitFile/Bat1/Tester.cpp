//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Tester.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Calc.cpp");
USEUNIT("Cal.cpp");
USEUNIT("Config.cpp");
USEUNIT("..\Moduly\Borland\System.cpp");
USEUNIT("..\Moduly\Borland\Eep.cpp");
USEUNIT("..\Moduly\Country\Dt.cpp");
USEUNIT("..\Moduly\File\Fs.cpp");
USEUNIT("..\Moduly\File\Db.cpp");
USEUNIT("..\Moduly\File\Ds.cpp");
USEUNIT("..\Moduly\File\Fat.cpp");
USEUNIT("..\Moduly\File\Fd.cpp");
USEUNIT("..\Moduly\File\Fdir.cpp");
USEUNIT("..\Moduly\Stat\Statistics.cpp");
USEUNIT("..\Moduly\Stat\Histogram.cpp");
USEUNIT("Sdb.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
