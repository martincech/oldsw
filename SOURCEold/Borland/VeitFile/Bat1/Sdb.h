//******************************************************************************
//
//   Sdb.h        Samples database
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __Sdb_H__
   #define __Sdb_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Db_H__
   #include "../Inc/File/Db.h"
#endif

#ifndef __FileCfg_H__
   #include "FileCfg.h"
#endif

#ifndef __SampleDef_H__
   #include "SampleDef.h"
#endif

extern const TDbDescriptor SdbDescriptor;        // database descriptor
#define SDB_CLASS          1                     // database class

//------------------------------------------------------------------------------
// Database overrides
//------------------------------------------------------------------------------

TYesNo SdbCreate( char *Name);
// Create new database

void SdbOpen( TFdirHandle Handle, TYesNo RandomAccess, TYesNo ReadOnly);
// Open database

#define SdbEmpty()                     DbEmpty()
// Delete all records

#define SdbDelete()                    FsDelete()
// Delete current database

void SdbCompress( void);
// Compress database

void SdbClose( void);
// Close current database

#define SdbInfo()                      FsInfo()
// Returns directory entry of the database

TFileConfig *SdbConfig( void);
// Returns working configuration

#define SdbBegin()                     DbBegin()
// Set before start of the table

#define SdbNext( Record)               DbNext( Record)
// Get next record

#define SdbCount()                     DbCount()
// Returns items count

#define SdbMoveAt( RecordIndex)        DbMoveAt( RecordIndex)
// Move current record pointer at <RecordIndex>

#define SdbDeleteRecord( RecordIndex)  DbDeleteRecord( RecordIndex)
// Delete record at <RecordIndex> position

#define SdbDeleteLastRecord()          DbDeleteLastRecord()
// Delete last record

#define SdbAppend( Record)             DbAppend( Record)
// Append <Record> at end of databease

#endif

 