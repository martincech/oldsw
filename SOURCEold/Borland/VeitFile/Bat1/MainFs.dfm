object MainForm: TMainForm
  Left = 133
  Top = 177
  Width = 644
  Height = 662
  Caption = 'FileTester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 8
    Width = 48
    Height = 13
    Caption = 'Directory :'
  end
  object LblFile: TLabel
    Left = 400
    Top = 8
    Width = 22
    Height = 13
    Caption = 'File :'
  end
  object Label3: TLabel
    Left = 272
    Top = 88
    Width = 34
    Height = 13
    Caption = 'Count :'
  end
  object Label4: TLabel
    Left = 272
    Top = 136
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object LblFree: TLabel
    Left = 264
    Top = 8
    Width = 70
    Height = 13
    Caption = 'Free Space : ?'
  end
  object LblFdName: TLabel
    Left = 272
    Top = 448
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblTime: TLabel
    Left = 272
    Top = 504
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LbDirectory: TListBox
    Left = 24
    Top = 32
    Width = 121
    Height = 289
    ItemHeight = 13
    TabOrder = 0
  end
  object BtnFormat: TButton
    Left = 32
    Top = 440
    Width = 75
    Height = 25
    Caption = 'Format'
    TabOrder = 1
    OnClick = BtnFormatClick
  end
  object BtnCreate: TButton
    Left = 32
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Create'
    TabOrder = 2
    OnClick = BtnCreateClick
  end
  object BtnOpen: TButton
    Left = 32
    Top = 520
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 3
    OnClick = BtnOpenClick
  end
  object BtnDelete: TButton
    Left = 176
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 4
    OnClick = BtnDeleteClick
  end
  object EdtName: TEdit
    Left = 24
    Top = 336
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'FILE'
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 616
    Width = 636
    Height = 19
    Panels = <
      item
        Text = 'Status : ?'
        Width = 200
      end>
    SimplePanel = False
  end
  object BtnFill: TButton
    Left = 32
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Fill'
    TabOrder = 7
    OnClick = BtnFillClick
  end
  object BtnClose: TButton
    Left = 32
    Top = 560
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 8
    OnClick = BtnCloseClick
  end
  object BtnEmpty: TButton
    Left = 176
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Empty'
    TabOrder = 9
    OnClick = BtnEmptyClick
  end
  object LbFile: TListBox
    Left = 400
    Top = 24
    Width = 121
    Height = 561
    ItemHeight = 13
    TabOrder = 10
  end
  object BtnFileFill: TButton
    Left = 176
    Top = 112
    Width = 75
    Height = 25
    Caption = 'File Fill'
    TabOrder = 11
    OnClick = BtnFileFillClick
  end
  object EdtCount: TEdit
    Left = 272
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 12
    Text = '100'
  end
  object EdtData: TEdit
    Left = 272
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 13
    Text = '?'
  end
  object BtnFdCount: TButton
    Left = 176
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Dir Count'
    TabOrder = 14
    OnClick = BtnFdCountClick
  end
  object BtnFdMove: TButton
    Left = 176
    Top = 408
    Width = 75
    Height = 25
    Caption = 'Dir Move'
    TabOrder = 15
    OnClick = BtnFdMoveClick
  end
  object EdtFdCount: TEdit
    Left = 272
    Top = 368
    Width = 121
    Height = 21
    TabOrder = 16
    Text = '0'
  end
  object EdtFdIndex: TEdit
    Left = 272
    Top = 408
    Width = 121
    Height = 21
    TabOrder = 17
    Text = '0'
  end
  object EdtDate: TEdit
    Left = 272
    Top = 472
    Width = 121
    Height = 21
    TabOrder = 18
    Text = '1.1.07'
  end
  object BtnTime: TButton
    Left = 176
    Top = 472
    Width = 75
    Height = 25
    Caption = 'Time'
    TabOrder = 19
    OnClick = BtnTimeClick
  end
  object BtnTruncate: TButton
    Left = 176
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Truncate'
    TabOrder = 20
    OnClick = BtnTruncateClick
  end
  object EdtSize: TEdit
    Left = 272
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 21
    Text = '0'
  end
  object EdtClass: TEdit
    Left = 152
    Top = 336
    Width = 41
    Height = 21
    TabOrder = 22
    Text = '1'
  end
  object EdtDirectoryClass: TEdit
    Left = 152
    Top = 296
    Width = 41
    Height = 21
    TabOrder = 23
    Text = '255'
    OnChange = EdtDirectoryClassChange
  end
end
