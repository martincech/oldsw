//******************************************************************************
//
//   Sdb.c        Samples database
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include "Sdb.h"
#include "../Inc/File/Db.h"
#include <stddef.h>

// Database descriptor :
DefDbDescriptor( SdbDescriptor)
   sizeof( TFileConfig),
   sizeof( TSample),
   offsetof( TSample, Flag),
   FLAG_DELETED
EndDbDescriptor()

TFileConfig _Config;                   // Working configuration
bool        _SaveConfig;               // Save configuration on close

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TYesNo SdbCreate( char *Name)
// Create new database
{
   _SaveConfig = YES;
   return( DbCreate( &SdbDescriptor, Name, SDB_CLASS, &_Config));
} // SdbCreate

//------------------------------------------------------------------------------
// Open
//------------------------------------------------------------------------------

void SdbOpen( TFdirHandle Handle, TYesNo RandomAccess, TYesNo ReadOnly)
// Open database
{
   _SaveConfig = RandomAccess && !ReadOnly;
   DbOpen( &SdbDescriptor, Handle, RandomAccess, ReadOnly);
   DbLoadConfig( &_Config);
} // SdbOpen

//------------------------------------------------------------------------------
// Compress
//------------------------------------------------------------------------------

void SdbCompress( void)
// Compress database
{
TSample Record;

   DbCompress( &Record);
} // SdbCompress

//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------

void SdbClose( void)
// Close current database
{
   if( _SaveConfig){
      DbSaveConfig( &_Config);
   }
   DbClose();
} // SdbClose

//------------------------------------------------------------------------------
// Config
//------------------------------------------------------------------------------

TFileConfig *SdbConfig( void)
// Returns working configuration
{
   return( &_Config);
} // SdbConfig

