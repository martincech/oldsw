//******************************************************************************
//
//   DsUser.h      Data Set user definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DsUser_H__
   #define __DsUser_H__

#ifndef __DbDef_H__
   #include "../Inc/File/DbDef.h"
#endif

#ifndef __Sdb_H__
   #include "Sdb.h"
#endif

#define DS_DESCRIPTOR      &SdbDescriptor        // dataset current descriptor
#define DS_CLASS            SDB_CLASS            // dataset current class

#endif
 