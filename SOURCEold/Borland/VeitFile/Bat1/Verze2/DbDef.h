//******************************************************************************
//
//   DbDef.h      Database data definition
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __DbRecord_H__
   #define __DbRecord_H__

#ifndef __SampleDef_H__
   #include "SampleDef.h"
#endif

#ifndef __FileCfg_H__
   #include "FileCfg.h"
#endif

#define DB_CONFIG_SIZE        sizeof( TFileConfig)
#define DB_RECORD_SIZE        sizeof( TSample)
#define DB_FLAG_OFFSET        offsetof( TSample, Flag)
#define DB_FLAG_DELETED       FLAG_DELETED


#endif
 
