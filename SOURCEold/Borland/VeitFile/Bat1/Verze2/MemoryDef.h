//*****************************************************************************
//
//    MemoryDef.h  -  External EEPROM structure
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __MemoryDef_H__
  #define __MemoryDef_H__

#ifndef __CalDef_H__
   #include "CalDef.h"       // Calibration
#endif

#ifndef __ConfigDef_H__
   #include "ConfigDef.h"    // Device configuration
#endif

#ifndef __FsDef_H__
   #include "../inc/File/FsDef.h"        // Filesystem definition
#endif

//-----------------------------------------------------------------------------
// Definition
//-----------------------------------------------------------------------------

// Spare bytes at the end of EEPROM
#define EEP_SPARE       ( EEP_SIZE              \
                        - sizeof(TCalibration)  \
                        - sizeof(TConfig)       \
                        - sizeof(TFsData))      \

typedef struct {
   TCalibration Calibration;                     // Calibration data
   TConfig      Config;                          // Device configuration
   byte         Spare[ EEP_SPARE];               // Spare bytes
   TFsData      Filesystem;                      // Filesystem
} TEeprom;

#endif

