//---------------------------------------------------------------------------

#ifndef MainFsH
#define MainFsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TListBox *LbDirectory;
        TLabel *Label1;
        TButton *BtnFormat;
        TButton *BtnCreate;
        TButton *BtnOpen;
        TButton *BtnDelete;
        TEdit *EdtName;
        TStatusBar *StatusBar;
        TButton *BtnFill;
        TButton *BtnClose;
        TButton *BtnEmpty;
        TListBox *LbFile;
        TLabel *LblFile;
        TButton *BtnFileFill;
        TEdit *EdtCount;
        TLabel *Label3;
        TEdit *EdtData;
        TLabel *Label4;
        TLabel *LblFree;
        TButton *BtnFdCount;
        TButton *BtnFdMove;
        TEdit *EdtFdCount;
        TEdit *EdtFdIndex;
        TLabel *LblFdName;
        TEdit *EdtDate;
        TButton *BtnTime;
        TLabel *LblTime;
        TButton *BtnTruncate;
        TEdit *EdtSize;
        TEdit *EdtClass;
        TEdit *EdtDirectoryClass;
        void __fastcall BtnFormatClick(TObject *Sender);
        void __fastcall BtnCreateClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnOpenClick(TObject *Sender);
        void __fastcall BtnDeleteClick(TObject *Sender);
        void __fastcall BtnFillClick(TObject *Sender);
        void __fastcall BtnCloseClick(TObject *Sender);
        void __fastcall BtnEmptyClick(TObject *Sender);
        void __fastcall BtnFileFillClick(TObject *Sender);
        void __fastcall BtnFdCountClick(TObject *Sender);
        void __fastcall BtnFdMoveClick(TObject *Sender);
        void __fastcall BtnTimeClick(TObject *Sender);
        void __fastcall BtnTruncateClick(TObject *Sender);
        void __fastcall EdtDirectoryClassChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
        void DirectoryRedraw( void);
        void FileRedraw( void);
        void CalcDisplay( void);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
