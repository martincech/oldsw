//******************************************************************************
//
//   Main.c       Tester main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include <stdio.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------

#include "../Inc/File/Fdir.h"
#include "../Inc/File/Fs.h"
#include "../Inc/File/Fd.h"
#include "../Inc/File/Ds.h"
#include "Calc.h"
#include "Cal.h"
#include "Config.h"
#include "../Inc/System.h"


#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

#define DEMO_CLASS 1

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Initialisation
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   FsInit();
}  // FormCreate

//******************************************************************************
// Format
//******************************************************************************

void __fastcall TMainForm::BtnFormatClick(TObject *Sender)
{
   FsFormat();
   LbFile->Clear();
   LblFile->Caption = "File : ?";
   DirectoryRedraw();
   StatusOk();
} // BtnFormatClick

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::BtnCreateClick(TObject *Sender)
{
char Name[ FDIR_NAME_LENGTH + 1];
TFileConfig Config;

   strcpy( Name, EdtName->Text.c_str());
   memset( &Config, 0, sizeof( Config));
   if( FdExists( Name)){
      Status() + "File already exists";
      return;
   }
   if( !DbCreate( Name, DEMO_CLASS, &Config)){
      Status() + "Directory full/not enough space";
      return;
   }
   DirectoryRedraw();
   FileRedraw();
   StatusOk();
} // BtnCreateClick

//******************************************************************************
// Open
//******************************************************************************

void __fastcall TMainForm::BtnOpenClick(TObject *Sender)
{
TFdirHandle Handle;
TFdirInfo   Item;

   if( LbDirectory->ItemIndex < 0) {
      Status() + "No file selected";
      return;
   }
   AnsiString ItemName = LbDirectory->Items->Strings[ LbDirectory->ItemIndex];
   Handle = FdirSearch( ItemName.c_str());
   if( !Handle){
      Status() + ItemName + " unable find";
      return;
   }
   DbOpen( Handle, YES, NO);
   Status() + ItemName + " opened OK";
   FileRedraw();
} // BtnOpenClick

//******************************************************************************
// Close
//******************************************************************************

void __fastcall TMainForm::BtnCloseClick(TObject *Sender)
{
   DbClose();
   LbFile->Clear();
   LblFile->Caption = "File : ?";
   StatusOk();
} // BtnCloseClick

//******************************************************************************
// Delete
//******************************************************************************

void __fastcall TMainForm::BtnDeleteClick(TObject *Sender)
{
   DbDelete();
   LbFile->Clear();
   LblFile->Caption = "File : ?";
   DirectoryRedraw();
   StatusOk();
} // BtnDeleteClick

//******************************************************************************
// Empty
//******************************************************************************

void __fastcall TMainForm::BtnEmptyClick(TObject *Sender)
{
   DbEmpty();
   FileRedraw();
   StatusOk();
} // BtnEmptyClick

//******************************************************************************
// Fill file
//******************************************************************************

void __fastcall TMainForm::BtnFileFillClick(TObject *Sender)
{
int Count;
TSample Record;

   Count = EdtCount->Text.ToInt();
   Record.Flag = 0;
   StatusOk();
   for( int i = 0; i < Count; i++){
      Record.Weight = i;
      if( !DbAppend( &Record)){
         Status() + "Unable append record";
         break;
      }
   }
   FileRedraw();
} // BtnFileFillClick

//******************************************************************************
// Delete item
//******************************************************************************

void __fastcall TMainForm::BtnDeleteItemClick(TObject *Sender)
{
   if( LbFile->ItemIndex < 0) {
      Status() + "No item selected";
      return;
   }
   int Index = LbFile->ItemIndex;
   DbDeleteRecord( Index);
   FileRedraw();
   StatusOk();
} // BtnDeleteItemClick

//******************************************************************************
//  Delete Last
//******************************************************************************

void __fastcall TMainForm::BtnDeleteLastClick(TObject *Sender)
{
   if( !DbDeleteLastRecord()){
      Status() + "No items to delete";
      return;
   }
   FileRedraw();
   StatusOk();
} // BtnDeleteLastClick

//******************************************************************************
// File Doubleclick
//******************************************************************************

void __fastcall TMainForm::LbFileDblClick(TObject *Sender)
{
TSample Record;

   EdtData->Text = "?";
   int Index = LbFile->ItemIndex;
   if( !DbMoveAt( Index)){
      Status() + "move at failed";
      return;
   }
   if( !DbNext( &Record)){
      Status() + "unable read record";
      return;
   }
   EdtData->Text = AnsiString( Record.Weight);
   StatusOk();
} // LbFileDblClick

//******************************************************************************
// Redraw Directory
//******************************************************************************

void TMainForm::DirectoryRedraw( void)
{
TFdirHandle Handle;
char Name[ FDIR_NAME_LENGTH + 1];

   LbDirectory->Clear();
   FdSetClass( FDIR_CLASS_WILDCARD);
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      FdGetName( Handle, Name);
      LbDirectory->Items->Add( Name);
   }
} // DirectoryRedraw

//******************************************************************************
// Redraw File
//******************************************************************************

void TMainForm::FileRedraw( void)
{
TSample Record;

   TFdirInfo *Dir = DbInfo();
   if( Dir){
      LblFile->Caption = AnsiString("File : ") + Dir->Name;
   } else {
      LblFile->Caption = "File : ?";
   }
   LbFile->Clear();
   DbBegin();
   while( DbNext( &Record)){
      LbFile->Items->Add( AnsiString( Record.Weight));
   }
   LblFree->Caption = AnsiString( "Free Space : ") + FsAvailable();
} // FileRedraw

//******************************************************************************
// Fill directory
//******************************************************************************

#define DIR_ITEMS 10
#define REC_ITEMS 100

void __fastcall TMainForm::BtnFillClick(TObject *Sender)
{
char Name[ FDIR_NAME_LENGTH + 1];
TSample Record;
TFileConfig Config;

   FsFormat();
   Record.Flag = 0;
   memset( &Config, 0, sizeof( Config));
   StatusOk();
   for( int i = 0; i < DIR_ITEMS; i++){
      sprintf( Name, "FILE%02d", i);
      if( !DbCreate( Name, DEMO_CLASS, &Config)){
         Status() + "Directory full/not enough space";
         break;
      }
      for( int j = 0; j < REC_ITEMS; j++){
         Record.Weight = i * 100 + j;
         if( !DbAppend( &Record)){
            Status() + "Unable append record";
            break;
         }
      }
      DbClose();
   }
   DirectoryRedraw();
   LbFile->Clear();
} // BtbFillClick

//******************************************************************************
// Directory dbl click
//******************************************************************************

void __fastcall TMainForm::LbDirectoryDblClick(TObject *Sender)
{
   LbFileSet->Items->Add( LbDirectory->Items->Strings[ LbDirectory->ItemIndex]);
} // LbDirectoryDblClick

//******************************************************************************
// Clear Data Set
//******************************************************************************

void __fastcall TMainForm::BtnDsClearClick(TObject *Sender)
{
   LbFileSet->Clear();
} // BtnDsClearClick

//******************************************************************************
// Delete Data Set
//******************************************************************************

void __fastcall TMainForm::BtnDsDeleteClick(TObject *Sender)
{
   if( LbDirectory->ItemIndex < 0) {
      Status() + "No file selected";
      return;
   }
   LbFileSet->Items->Delete( LbFileSet->ItemIndex);
} // BtnDsDeleteClick

//******************************************************************************
// Data Set Process
//******************************************************************************


void __fastcall TMainForm::BtnDsProcessClick(TObject *Sender)
{
TFDataSet DataSet;

   int Count = LbFileSet->Items->Count;
   if( !Count){
      Status() + "No files";
      return;
   }
   DsClear( DataSet);      // empty dataset
   for( int i = 0; i < Count; i++){
      AnsiString Name = LbFileSet->Items->Strings[ i];
      TFdirHandle Handle = FdSearch( Name.c_str());
      if( !Handle){
         Status() + "Invalid list";
         return;
      }
      DsAdd( DataSet, Handle);
   }
   if( DsIsEmpty( DataSet)){
      Status() + "Empty dataset";
      return;
   }
   // run thru databases :
   DsSet( DataSet);
   LbData->Clear();
   TSample Record;
   DsBegin();
   while( DsNext( &Record)){
      LbData->Items->Add( AnsiString( Record.Weight));
   }
   StatusOk();
} // BtnDsProcessClick

//******************************************************************************
// Data Set Current
//******************************************************************************

void __fastcall TMainForm::BtnSetCurrentClick(TObject *Sender)
{
   TFdirInfo *Dir = DbInfo();
   if( !Dir){
      Status() + "no opened file";
      return;
   }
   DsSetCurrent();
   // run thru databases :
   LbData->Clear();
   TSample Record;
   DsBegin();
   while( DsNext( &Record)){
      LbData->Items->Add( AnsiString( Record.Weight));
   }
   StatusOk();
} // BtnSetCurrentClick

//******************************************************************************
// Statistics
//******************************************************************************

void __fastcall TMainForm::BtnStatisticsClick(TObject *Sender)
{
TStatisticParameters Parameters;

   Parameters.UniformityRange = 10;
   Parameters.Histogram.Range = 100;
   CalcClear();
   CalcDisplay();
   CalcStatistics( FLAG_NONE, &Parameters);
   CalcDisplay();
   StatusOk();
} // BtnStatisticsClick


//******************************************************************************
// Display Calc
//******************************************************************************

void TMainForm::CalcDisplay( void)
// Display Calc results
{
   EdtSamplesCount->Text = AnsiString( Calc.Count);
   EdtAverage->Text      = AnsiString( Calc.Average);
   EdtMin->Text          = AnsiString( Calc.MinWeight);
   EdtMax->Text          = AnsiString( Calc.MaxWeight);
   EdtSigma->Text        = AnsiString( Calc.Sigma);
   EdtCv->Text           = AnsiString( Calc.Cv);
   EdtUniformity->Text   = AnsiString( Calc.Uniformity);
} // CalcDisplay

//******************************************************************************
// Set defaults
//******************************************************************************

void __fastcall TMainForm::BtnDefaultsClick(TObject *Sender)
{
   ConfigLoad();
   CalLoad();
} // BtnDefaultsClick


//******************************************************************************
// Save configuration
//******************************************************************************

void __fastcall TMainForm::BtnSaveCfgClick(TObject *Sender)
{
   Config.FileConfig.StatisticParameters.UniformityRange            = 20;
   Config.FileConfig.StatisticParameters.Histogram.Range  = 20;
   Config.FileConfig.StatisticParameters.Histogram.Step   =  0;

   ConfigSaveSection( FileConfig);
} // BtnSaveCfgClick


//******************************************************************************
// Fd Count
//******************************************************************************

void __fastcall TMainForm::BtnFdCountClick(TObject *Sender)
{
   int Count = FdCount();
   EdtFdCount->Text = AnsiString( Count);
   StatusOk();
} // BtnFdCountClick


//******************************************************************************
// Fd Move
//******************************************************************************

void __fastcall TMainForm::BtnFdMoveClick(TObject *Sender)
{
   int Index = EdtFdIndex->Text.ToInt();
   LblFdName->Caption = "?";
   if( !FdMoveAt( Index)){
      Status() + " Invalid index";
      return;
   }
   TFdirHandle Handle;
   if( (Handle = FdFindNext()) == FDIR_INVALID){
      Status() + " no item found";
      return;
   }
   char Name[ FDIR_NAME_LENGTH + 1];
   FdGetName( Handle, Name);
   LblFdName->Caption = Name;
   StatusOk();
} // BtnFdMoveClick

//******************************************************************************
// Time
//******************************************************************************

void __fastcall TMainForm::BtnTimeClick(TObject *Sender)
{
TTimestamp Ts;
TLocalTime Local;

   Ts = SysGetClock();
   DtDecode( Ts, &Local);
   AnsiString Text;
   Text.printf( "%02d.%02d.%02d %02d:%02d:%02d", Local.Day, Local.Month, Local.Year,
                                                 Local.Hour, Local.Min, Local.Sec);
   EdtDate->Text = Text;
   TTimestamp TsNew = DtEncode( &Local);
   LblTime->Caption = AnsiString( TsNew);
   StatusOk();
} // BtnTimeClick

