//*****************************************************************************
//
//    FileCfg.c  -  File configuration
//    Version 1.0   (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __FileCfg_H__
  #define __FileCfg_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

//-----------------------------------------------------------------------------
// File-specific configuration
//-----------------------------------------------------------------------------

// Saving mode
typedef enum {
  SAVING_MODE_AUTOMATIC,		// Automatic saving
  SAVING_MODE_MANUAL,		        // Manual saving
  SAVING_MODE_2OPTIONS,		        // Manual selection from 2 options
  SAVING_MODE_3OPTIONS,                 // Manual selection from 3 options
  _SAVING_MODE_COUNT
} TSavingMode;

// Mode of weight limits
typedef enum {
  WEIGHT_LIMITS_NONE,		        // Limits not in use
  WEIGHT_LIMITS_LOW,		        // Only one limit used (LowLimit)
  WEIGHT_LIMITS_HIGH,                   // Only one limite used (HighLimit)
  WEIGHT_LIMITS_BOTH,                   // Both limits used
  _WEIGHT_LIMITS_COUNT
} TWeightLimitsMode;

// Weight limits
typedef struct {
  byte    Mode;		               // TWeightLimitsMode - Mode of limits used
  TWeight LowLimit;		       // Low limit
  TWeight HighLimit;		       // High limit
} TWeightLimits;

// Saving parameters
typedef struct {
  byte    Inertia;                     // Averaging time [0.1s]
  byte    Sustain;                     // Stable duration [0.1s]
  TWeight Trigger;                     // Trigger treshold [kg]
  TWeight Accuracy;                    // Stable range [kg]
  TWeight Empty;                       // Empty weight [kg] - discharge resolution
} TSavingParameters;

// Histogram parameters
typedef struct {
  byte    Range;		       // Histogram step in +/- perecents of average weight (or 0 if not used)
  TWeight Step;		               // Histogram step in kg (or 0 if not used)
} THistogramParameters;

// Parameters for statistic calculations
typedef struct {
  byte                 UniformityRange;// Uniformity range in +/- percents of average weight
  THistogramParameters Histogram;      // Histogram parameters
} TStatisticParameters;

// File-specific configuration
typedef struct {
  byte                 SavingMode;              // TSavingMode Saving mode
  TWeightLimits        WeightLimits;		// Weight limits mode and weights
  TSavingParameters    SavingParameters;	// Saving parameters
  TStatisticParameters StatisticParameters;	// Parameters for statistic calculations
} TFileConfig;

#endif
