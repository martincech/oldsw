//*****************************************************************************
//
//    Calc.c - Statistic calculations
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
#endif

#include "Calc.h"
#include "../Inc/File/Ds.h"
#include "../Inc/Statistics.h"

// Global data :
TCalc Calc;

// Local data :
static TStatistic Statistics;


// Local functions :

static void AddSample( TWeight Weight);
// Add sample to primary

static void UpdatePrimary( void);
// Recalculate primary statistics

//-----------------------------------------------------------------------------
// Clear
//-----------------------------------------------------------------------------

void CalcClear(void)
// Clear results
{
  StatClear( &Statistics);
  HistogramClear( &Calc.Histogram);
  Calc.Count      = 0;
  Calc.Average    = 0;
  Calc.MinWeight  = WEIGHT_MAX;
  Calc.MaxWeight  = WEIGHT_MIN;
  Calc.Sigma      = 0;
  Calc.Cv         = 0;
  Calc.Uniformity = 0;
} // CalcClear

//-----------------------------------------------------------------------------
// Primary statistics
//-----------------------------------------------------------------------------

void CalcPrimary( TSampleFlag Flag)
// Calculate primary statistics
{
TSample Record;

   DsBegin();
   while( DsNext( &Record)){
      if( Flag != FLAG_NONE && Record.Flag != Flag){
         continue;                     // invalid sample
      }
      AddSample( Record.Weight);
   }
   UpdatePrimary();
} // CalcPrimary

//-----------------------------------------------------------------------------
// Secondary statistics
//-----------------------------------------------------------------------------

void CalcSecondary( TSampleFlag Flag, TStatisticParameters *Parameters)
// Calculate secondary statistics
{
TSample Record;

   // Uniformity & histogram calculation :
   StatUniformityInit( Calc.Average * (TNumber)(100 - Parameters->UniformityRange) / 100.0,
                       Calc.Average * (TNumber)(100 + Parameters->UniformityRange) / 100.0);
   Calc.Histogram.Center = Calc.Average;
   if( Parameters->Histogram.Range > 0) {
      // by range [percent]
      HistogramSetStep( &Calc.Histogram, Parameters->Histogram.Range);
   } else {
      // by step
      Calc.Histogram.Step = Parameters->Histogram.Step;
      // check left column for negative value
      if( Calc.Histogram.Center < Calc.Histogram.Step * (THistogramValue)HISTOGRAM_SLOTS / (THistogramValue)2){
         // Negative value, shift center
         Calc.Histogram.Center = Calc.Histogram.Step * (THistogramValue)HISTOGRAM_SLOTS / (THistogramValue)2;
      }
   }
   // scan samples :
   DsBegin();
   while( DsNext( &Record)){
      if( Flag != FLAG_NONE && Record.Flag != Flag){
         continue;                     // invalid sample
      }
      StatUniformityAdd( Record.Weight);
      HistogramAdd( &Calc.Histogram, Record.Weight);
   }
   Calc.Uniformity = StatUniformityGet();
} // CalcSecondary

//-----------------------------------------------------------------------------
// Statistics
//-----------------------------------------------------------------------------

void CalcStatistics( TSampleFlag Flag, TStatisticParameters *Parameters)
// Calc statistic of the dataset, get samples with <Flag> only (FLAG_NONE = all samples)
{
   CalcClear();
   CalcPrimary( Flag);
   CalcSecondary( Flag, Parameters);
} // CalcStatistics

//-----------------------------------------------------------------------------
// Append
//-----------------------------------------------------------------------------

void CalcAppend( TWeight Weight, TStatisticParameters *Parameters)
// Append sample & recalculate
{
   AddSample( Weight);
   UpdatePrimary();
   CalcSecondary( FLAG_NONE, Parameters);
} // CalcAppend

//*****************************************************************************

//-----------------------------------------------------------------------------
// Add sample
//-----------------------------------------------------------------------------

static void AddSample( TWeight Weight)
// Add sample to primary
{
   StatAdd(&Statistics, Weight);
   // Minimal & maximal weight
   if( Weight < Calc.MinWeight) {
      Calc.MinWeight = Weight;
   }
   if( Weight > Calc.MaxWeight) {
      Calc.MaxWeight = Weight;
   }
} // AddSample

//-----------------------------------------------------------------------------
// Update primary
//-----------------------------------------------------------------------------

static void UpdatePrimary( void)
// Recalculate primary statistics
{
   Calc.Count   = Statistics.Count;
   Calc.Average = StatAverage( &Statistics);
   Calc.Sigma   = StatSigma( &Statistics);
   Calc.Cv      = StatVariation( Calc.Average, Calc.Sigma);
} // UpdatePrimary
