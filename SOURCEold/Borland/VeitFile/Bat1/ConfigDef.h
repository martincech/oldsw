//*****************************************************************************
//
//    ConfigDef.h  -  Configuration definition
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __ConfigDef_H__
  #define __ConfigDef_H__

#ifndef __FdirCfg_H__
   #include "../Inc/File/FdirDef.h"      // directory entry handle
#endif

#ifndef __FileCfg_H__
   #include "FileCfg.h"                  // file specific configuration
#endif

#ifndef __CountryDef_H__
   #include "../Inc/CountryDef.h"
#endif

//-----------------------------------------------------------------------------
// Weighing configuration
//-----------------------------------------------------------------------------

// Weighing units :
typedef enum {
   UNITS_KG,
   UNITS_G,
   UNITS_LB,
   UNITS_OZ,
   _UNITS_COUNT
} TUnitsEnum;

// Units descriptor :
typedef struct {
   dword Range;                        // weighing range
   byte  Units;                        // weighing units TUnitsEnum
   byte  Decimals;                     // decimals count
   word  MaxDivision;                  // division limit
} TUnits;

//-----------------------------------------------------------------------------
// Device configuration
//-----------------------------------------------------------------------------

#define VOLUME_MAX     10 // volume 0..MAX

// Sounds configuration
typedef struct {
   byte KeyTone;		// Tone when a key is pressed
   byte LimitBelowTone;	// Tone when a bird with weight below the limit is saved
   byte LimitMiddleTone;	// Tone when a bird within limits is saved
   byte LimitAboveTone;	// Tone when a bird with weight above the limit is saved
   byte Volume;		// Volume of the speaker
} TSoundsConfig;	// 5 bytes

// Display mode
typedef enum {
   DISPLAY_MODE_BASIC,	// Basic display mode
   DISPLAY_MODE_ADVANCED	// Advanced display mode
} TDisplayMode;

#define BACKLIGHT_MAX  10         // backlight intensity 0..MAX

// Backlight mode
typedef enum {
   BACKLIGHT_MODE_AUTO,  // Backlight in automatic mode
   BACKLIGHT_MODE_ON,	// Backlight is always on
   BACKLIGHT_MODE_OFF,	// Backlight is always off
   _BACKLIGHT_MODE_COUNT
} TBacklightMode;

// Backlight configuration
typedef struct {
   byte Mode;		// TBacklightMode Backlight mode
   byte Intensity;	// Backlight intensity
} TBacklightConfig;

// Display configuration
typedef struct {
   byte             Mode;	// TDisplayMode Display mode
   TBacklightConfig Backlight;	// Backlight setup
} TDisplayConfig;

// Print configuration
typedef struct {

} TPrintConfig;

// Quick buttons configuration
typedef struct {

} TQuickButtonsConfig;

#define	CONFIG_RESERVED_SIZE	16	// Number of reserved bytes in TConfig
#define	SCALE_NAME_LENGTH	15	// Maximum length of the scale name
#define PASSWORD_LENGTH         4       // Number of password keys

typedef word TConfigCrc;                // Configuration checksum

// Global configuration
typedef struct {
   word Version;                                 // Version
   byte Build;                                   // Build
   byte HwVersion;                               // HW version

   char ScaleName[ SCALE_NAME_LENGTH + 1];	 // Scale name + ending zero
   char Password[ PASSWORD_LENGTH];              // Service password

   TCountry            Country;                  // Country data
   TUnits              Units;                    // Weighing units
   TSoundsConfig       Sounds;			 // Setup of sounds
   TDisplayConfig      Display;			 // Setup of display
   TQuickButtonsConfig QuickButtons;		 // Setup of quick buttons

   TFdirHandle         LastFile;                  // Last working file
   TFileConfig         FileConfig;                // Default file-specific configuration

   byte Reserved[ CONFIG_RESERVED_SIZE];          // For future use

   TConfigCrc          CheckSum;                  // Check sum
} TConfig;

extern TConfig Config;

#endif
