//*****************************************************************************
//
//    SampleDef.h  -  Weight sample definition
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __SampleDef_H__
  #define __SampleDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#ifndef __DtDef_H__
   #include "../Inc/DtDef.h"                  // date and time
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"
#endif

//-----------------------------------------------------------------------------
// Sample definition
//-----------------------------------------------------------------------------

// Flags used in samples
typedef enum {
  FLAG_NONE,		               // No flag saved (flags not in use during weighing)
  FLAG_BELOW,		               // Below lower limit
  FLAG_MIDDLE,	                       // Within lower and higher limit
  FLAG_ABOVE,		               // Above higher limit
  FLAG_DELETED = 0x80                  // Deleted sample
} TSampleFlagEnum;

typedef byte TSampleFlag;

// One sample of weight
typedef struct {
  TTimestamp  Timestamp;               // Date and time
  TWeight     Weight;                  // Weight of the sample
  TSampleFlag Flag;                    // Flag of the sample (TSampleFlagEnum)
} TSample;


#endif
