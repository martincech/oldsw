//******************************************************************************
//
//   Fat.c         File allocation table utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/File/Fat.h"
#include "../../Inc/File/FsDef.h"   // Filesystem layout
#include "../../Inc/Eep.h"          // EEPROM
#include "MemoryDef.h"              // project directory EEPROM layout

#define __DEBUG__

#ifdef __DEBUG__
   #ifdef __GNUC__
      #include "../Inc/conio.h"
      #define IERROR      cgoto( 0, 0); cprintf( "Err FAT %d", __LINE__);
   #endif
#endif

#define FAT_INVALID_INDEX ((TFatItem)-1)

#define GetFatIndex( Index)   (Index    + 1)     // index to FAT index
#define GetIndex( FatIndex)   (FatIndex - 1)     // FAT index to index

static short _LastFree;                          // last free entry

// Local functions :

static void TableFillFree( void);
// Fill table with free tags

static void TablePut( int Index, TFatItem Value);
// Put <Value> at <Index>

static TFatItem TableGet( int Index);
// Return value from <Index>

static void TableLastFree( void);
// Find last free item

static int TableFindFree( void);
// Returns index of free item

static int TableCount( TFatItem Pattern);
// Counts <Pattern> items

//------------------------------------------------------------------------------
// Initialisation
//------------------------------------------------------------------------------

void FatInit( void)
// Initialisation
{
   TableLastFree();
} // FatInit

//------------------------------------------------------------------------------
// Format
//------------------------------------------------------------------------------

void FatFormat( void)
// Set all block empty
{
   TableFillFree();
   _LastFree = -1;
} // FatFormat

//------------------------------------------------------------------------------
// Append block
//------------------------------------------------------------------------------

TFatIndex FatAppend( TFatIndex Index)
// Append block at <Index>, if <Index> == FAT_INVALID, allocate first block
{
TFatIndex FreeIndex;

   FreeIndex = TableFindFree();
   if( FreeIndex == FAT_INVALID_INDEX){
      return( FAT_INVALID);                      // table full
   }
   TablePut( FreeIndex, FAT_EOF);                // mark new as EOF
   if( Index){
      TablePut( GetIndex( Index), FreeIndex);    // chain new block
   }
   return( GetFatIndex( FreeIndex));
} // FatAppend

//------------------------------------------------------------------------------
// Free chain
//------------------------------------------------------------------------------

void FatFree( TFatIndex Start)
// Release block chain by <Start> inclusive
{
TFatItem Item;
TFatItem PreviousItem;

   if( Start == FAT_INVALID){
      return;                                    // empty chain
   }
   Item         = GetIndex( Start);
   PreviousItem = FAT_INVALID_INDEX;
   do {
      if( PreviousItem != FAT_INVALID_INDEX){
         TablePut( PreviousItem, FAT_FREE);      // release previous item
      }
      PreviousItem = Item;                       // remember as previous
      Item = TableGet( Item);                    // move at next item
#ifdef __DEBUG__
      if( Item == FAT_FREE){
         IERROR;
      }
#endif
   } while( Item != FAT_EOF);
   TablePut( PreviousItem, FAT_FREE);            // release last item
} // FatFree

//------------------------------------------------------------------------------
// Truncate
//------------------------------------------------------------------------------

void FatTruncate( TFatIndex Last)
// Mark <Last> as eof, release next chain
{
TFatItem Next;

   if( Last == FAT_INVALID){
      return;
   }
   Next = FatNext( Last);
   if( Next == FAT_INVALID){
      return;                          // doesn't continue - done
   }
   FatFree( Next);                     // release chain at next position
   TablePut( GetIndex( Last), FAT_EOF);// mark as eof
} // FatTruncate

//------------------------------------------------------------------------------
// Offset block
//------------------------------------------------------------------------------

TFatIndex FatOffset( TFatIndex Start, int Offset)
// Find block by <Start> at <Offset> position
{
int      i;
TFatItem Item;

   if( Start == FAT_INVALID){
      return( FAT_INVALID);
   }
   Item = GetIndex( Start);
   for( i = 0; i < Offset; i++){
      Item = TableGet( Item);          // move at next item
#ifdef __DEBUG__
      if( Item == FAT_FREE){
         IERROR;
      }
#endif
   }
   return( GetFatIndex( Item));
} // FatOffset

//------------------------------------------------------------------------------
// Next block
//------------------------------------------------------------------------------

TFatIndex FatNext( TFatIndex Index)
// Get next block
{
TFatItem Item;

   if( Index == FAT_INVALID){
      return( FAT_INVALID);
   }
   Item = TableGet( GetIndex( Index)); // move at next item
   if( Item == FAT_EOF){
      return( FAT_INVALID);            // end of file
   }
#ifdef __DEBUG__
   if( Item == FAT_FREE){
      IERROR;
   }
#endif
   return( GetFatIndex( Item));
} // FatNext

//------------------------------------------------------------------------------
// Unused blocks
//------------------------------------------------------------------------------

int FatUnused( void)
// Returns total count of free blocks
{
   return( TableCount( FAT_FREE));
} // FatUnused

//******************************************************************************

#define FAT_ADDRESS   (FS_OFFSET + offsetof( TFsData, Fat))

//------------------------------------------------------------------------------
// Fill
//------------------------------------------------------------------------------

#if (FAT_FREE >> 8) != (FAT_FREE & 0xFF)
   #error "Wrong FAT_FREE pattern
#endif

static void TableFillFree( void)
// Fill table with free tags
{
   EepFillData( FAT_ADDRESS, (byte)FAT_FREE, FAT_SIZE * sizeof( TFatItem));
} // TableFillFree

//------------------------------------------------------------------------------
// Put
//------------------------------------------------------------------------------

static void TablePut( int Index, TFatItem Value)
// Put <Value> at <Index>
{
   EepPageWriteStart( FAT_ADDRESS + Index * sizeof( TFatItem));
   EepPageWriteData( Value & 0x00FF);
   EepPageWriteData( Value >> 8);
   EepPageWritePerform();
} // TablePut

//------------------------------------------------------------------------------
// Put
//------------------------------------------------------------------------------

static TFatItem TableGet( int Index)
// Return value from <Index>
{
TFatItem Item;

   EepBlockReadStart( FAT_ADDRESS + Index * sizeof( TFatItem));
   Item  = EepBlockReadData();
   Item |= EepBlockReadData() << 8;
   EepBlockReadStop();
   return( Item);
} // TablePut

//------------------------------------------------------------------------------
// Last free
//------------------------------------------------------------------------------

static void TableLastFree( void)
// Find last free item
{
int i;
TFatItem Item;

   // search for last valid :
   _LastFree = -1;
   EepBlockReadStart( FAT_ADDRESS);
   for( i = 0; i < FAT_SIZE; i++){
      Item  = EepBlockReadData();
      Item |= EepBlockReadData() << 8;
      if( Item != FAT_FREE){
         _LastFree = i;                // save last valid
      }
   }
   EepBlockReadStop();
   if( _LastFree < 0){
      return;                          // table full
   }
   _LastFree++;                        // move after last valid
   if( _LastFree >= FAT_SIZE){
      _LastFree = -1;                  // out of capacity, next start at begin
   }
} // TableLastFree

//------------------------------------------------------------------------------
// Free
//------------------------------------------------------------------------------

static int TableFindFree( void)
// Returns index of free item
{
int i;
TFatItem Item;

   // search from LastFree to end of the table :
   _LastFree++;                        // move after last free
   EepBlockReadStart( FAT_ADDRESS + _LastFree * sizeof( TFatItem));
   for( i = _LastFree; i < FAT_SIZE; i++){
      Item  = EepBlockReadData();
      Item |= EepBlockReadData() << 8;
      if( Item == FAT_FREE){
         _LastFree = i;                // save last free
         EepBlockReadStop();
         return( _LastFree);
      }
   }
   EepBlockReadStop();
   // search from start of the table :
   EepBlockReadStart( FAT_ADDRESS);
   for( i = 0; i < FAT_SIZE; i++){
      Item  = EepBlockReadData();
      Item |= EepBlockReadData() << 8;
      if( Item == FAT_FREE){
         _LastFree = i;                // save last free
         EepBlockReadStop();
         return( _LastFree);
      }
   }
   EepBlockReadStop();
   _LastFree = -1;                     // next time start at begin of the table
   return( FAT_INVALID_INDEX);         // table full
} // TableFindFree

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

static int TableCount( TFatItem Pattern)
// Counts <Pattern> items
{
int i;
int Count;
TFatItem Item;

   Count = 0;
   EepBlockReadStart( FAT_ADDRESS);
   for( i = 0; i < FAT_SIZE; i++){
      Item  = EepBlockReadData();
      Item |= EepBlockReadData() << 8;
      if( Item == Pattern){
         Count++;
      }
   }
   EepBlockReadStop();
   return( Count);
} // TableCount

