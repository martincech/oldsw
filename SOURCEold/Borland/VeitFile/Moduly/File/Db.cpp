//******************************************************************************
//
//   Db.c          Database utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/File/Db.h"
#include "../../Inc/File/Fs.h"

static int                  _ItemsCount;         // undeleted items count
static const TDbDescriptor *_Dsc;                // database descriptor

// Local functions :

static TYesNo RecordIsDeleted( void *Record);
// Test for deleted <Record>

static TYesNo FlagIsDeleted( byte Flag);
// Test for deleted <Flag>

static void FlagSetDeleted( byte *Flag);
// Set deleted <Flag>

static int CountItems( void);
// Count undeleted items

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TYesNo DbCreate( const TDbDescriptor *Descriptor,
                 char *Name, TFdirClass Class, void *Config)
// Create table with <Name>
{
   if( !FsCreate( Name, Class)){
      return( NO);
   }
   _ItemsCount = 0;
   _Dsc        = Descriptor;
   if( !FsWrite( Config, _Dsc->ConfigSize)){
      FsDelete();                      // not enough space
      return( NO);
   }
   return( YES);
} // DbCreate

//------------------------------------------------------------------------------
// Open
//------------------------------------------------------------------------------

void DbOpen( const TDbDescriptor *Descriptor,
             TFdirHandle Handle, TYesNo RandomAccess, TYesNo ReadOnly)
// Open table with <Handle>. Enable <RandomAccess>, set <ReadOnly>
{
   FsOpen( Handle, ReadOnly);
   _ItemsCount = 0;
   _Dsc        = Descriptor;
   if( RandomAccess){
      _ItemsCount = CountItems();
   }
   if( !ReadOnly){
      FsWSeek( 0, FS_SEEK_END);
   }
} // DbOpen

//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------

void DbClose( void)
// Close current table
{
   FsClose();
} // DbClose

//------------------------------------------------------------------------------
// Empty
//------------------------------------------------------------------------------

void DbEmpty( void)
// Delete all records
{
   FsTruncate( _Dsc->ConfigSize);
   FsWSeek( 0, FS_SEEK_END);
} // DbEmpty

//------------------------------------------------------------------------------
// Compress
//------------------------------------------------------------------------------

void DbCompress( void *Record)
// Compress database. <Record> is record cache
{
byte  *Flag;
TYesNo FirstTime;

   Flag  = (byte *)Record;
   Flag += _Dsc->FlagOffset;                     // flag address
   FirstTime = YES;
   FsRSeek( _Dsc->ConfigSize, FS_SEEK_SET);      // at start of records
   while( FsRead( Record, _Dsc->RecordSize)){
      if( FlagIsDeleted( *Flag)){
         if( FirstTime){
            FirstTime = NO;
            FsWSeek( FsRTell() - _Dsc->RecordSize, FS_SEEK_SET);  // before deleted record
         }
         continue;
      }
      if( FirstTime){
         continue;                               // continuously undeleted
      }
      FsWrite( Record, _Dsc->RecordSize);        // save undeleted
   }
   if( !FirstTime){
      FsTruncate( FsWTell());                    // truncate at last record
   } // else nothing to truncate
   FsWSeek( 0, FS_SEEK_END);
} // DbCompress

//------------------------------------------------------------------------------
// Load config
//------------------------------------------------------------------------------

void DbLoadConfig( void *Config)
// Load configuration record <Config>
{
   FsRSeek( 0, FS_SEEK_SET);
   FsRead( Config, _Dsc->ConfigSize);
} // DbLoadConfig

//******************************************************************************
// Save config
//******************************************************************************

void DbSaveConfig( void *Config)
// Save configuration record <Config>
{
int    i;
TYesNo SameData;
byte   Data;
byte   *p;
int    Size;

   // check for old data :
   SameData = YES;
   p        = (byte *)Config;
   Size     = _Dsc->ConfigSize;
   FsRSeek( 0, FS_SEEK_SET);
   for( i = 0; i < Size; i++){
      if( !FsRead( &Data, 1)){
         SameData = NO;                // short file
         break;
      }
      if( Data != *p){
         SameData = NO;                // short file
         break;
      }
      p++;
   }
   if( SameData){
      return;                          // already saved
   }
   FsWSeek( 0, FS_SEEK_SET);
   FsWrite( Config, _Dsc->ConfigSize);
   FsWSeek( 0, FS_SEEK_END);
} // DbSaveConfig

//------------------------------------------------------------------------------
// Begin
//------------------------------------------------------------------------------

void DbBegin( void)
// Set before start of the table
{
   FsRSeek( _Dsc->ConfigSize, FS_SEEK_SET);        // at start of records
} // DbBegin

//------------------------------------------------------------------------------
// Next
//------------------------------------------------------------------------------

TYesNo DbNext( void *Record)
// Get next record
{
   forever {
      if( !FsRead( Record, _Dsc->RecordSize)){
         return( NO);
      }
      if( !RecordIsDeleted( Record) ){
         return( YES);
      }
   }
} // DbNext

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

int DbCount( void)
// Returns items count
{
   return( _ItemsCount);
} // DbCount

//------------------------------------------------------------------------------
// Move
//------------------------------------------------------------------------------

TYesNo DbMoveAt( int RecordIndex)
// Move current record pointer at <RecordIndex>
{
byte Flag;
int  Seek;
int  Index;

   if( RecordIndex < 0 || RecordIndex >= _ItemsCount){
      return( NO);
   }
   Seek  = _Dsc->ConfigSize + _Dsc->FlagOffset - _Dsc->RecordSize;   // before first record flag
   Index = -1;
   do {
      Seek += _Dsc->RecordSize;
      FsRSeek( Seek, FS_SEEK_SET);
      if( !FsRead( &Flag, 1)){
         return( NO);                            // end of file
      }
      if( !FlagIsDeleted(Flag)){
         Index++;                                // valid item
      }
   } while( Index != RecordIndex);
   FsRSeek( Seek - _Dsc->FlagOffset, FS_SEEK_SET); // back to start of item
   return( YES);
} // DbMoveAt

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void DbDeleteRecord( int RecordIndex)
// Delete record at <RecordIndex> position
{
byte Flag;
int  Seek;
int  Index;

   Seek  = _Dsc->ConfigSize + _Dsc->FlagOffset - _Dsc->RecordSize;   // before first record flag
   Index = -1;
   do {
      Seek += _Dsc->RecordSize;
      FsRSeek( Seek, FS_SEEK_SET);
      if( !FsRead( &Flag, 1)){
         return;                                 // end of file
      }
      if( !FlagIsDeleted(Flag)){
         Index++;                                // valid item
      }
   } while( Index != RecordIndex);
   FlagSetDeleted( &Flag);
   FsWSeek( Seek, FS_SEEK_SET);                  // return to flag position
   FsWrite( &Flag, 1);                           // overwrite flag
   FsWSeek( 0, FS_SEEK_END);                     // back to end
   FsRSeek( Seek - _Dsc->FlagOffset, FS_SEEK_SET); // back to start of deleted item
   _ItemsCount--;
} // DbDeleteRecord

//------------------------------------------------------------------------------
// Delete last
//------------------------------------------------------------------------------

TYesNo DbDeleteLastRecord( void)
// Delete last record
{
   if( !_ItemsCount){
      return( NO);                     // nothing to delete
   }
   _ItemsCount--;
   FsTruncate( FsWTell() - _Dsc->RecordSize);
   FsWSeek( 0, FS_SEEK_END);
   return( YES);
} // DbDeleteLastRecord

//------------------------------------------------------------------------------
// Append
//------------------------------------------------------------------------------

TYesNo DbAppend( void *Record)
// Append <Record> at end of databease
{
   _ItemsCount++;
   return( FsWrite( Record, _Dsc->RecordSize));
} // DbAppend

//******************************************************************************

//------------------------------------------------------------------------------
// Set Flag deleted
//------------------------------------------------------------------------------

static void FlagSetDeleted( byte *Flag)
// Set deleted <Flag>
{
   *Flag |= _Dsc->FlagDeleted;
} // FlagSetDeleted

//------------------------------------------------------------------------------
// Flag deleted
//------------------------------------------------------------------------------

static TYesNo FlagIsDeleted( byte Flag)
// Test for deleted <Flag>
{
   return( Flag & _Dsc->FlagDeleted);
} // FlagIsDeleted

//------------------------------------------------------------------------------
// Record deleted
//------------------------------------------------------------------------------

static TYesNo RecordIsDeleted( void *Record)
// Test for deleted <Record>
{
byte *p;

   p = (byte *)Record;
   return( p[ _Dsc->FlagOffset] & _Dsc->FlagDeleted);
} // RecordIsDeleted

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

static int CountItems( void)
// Count undeleted items
{
byte Flag;
int  Count;

   FsRSeek( _Dsc->ConfigSize + _Dsc->FlagOffset, FS_SEEK_SET);    // first record flag
   Count = 0;
   while( FsRead( &Flag, 1)){
      if( !FlagIsDeleted( Flag)){
         Count++;
      }
      FsRSeek( _Dsc->RecordSize - 1, FS_SEEK_CUR); // next record (one byte already read)
   }
   return( Count);
} // CountItems

