//******************************************************************************
//
//   Fd.c         File Directory utilites
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../Inc/File/Fd.h"
#include "../Inc/File/FsDef.h"
#include "../Inc/Eep.h"
#include <string.h>
#include "MemoryDef.h"              // project directory EEPROM layout

// Remark : FdirHandle is 1..FDIR_SIZE

static short      _CurrentItem;         // working item
static short      _ItemsCount;          // actual dictionary size
static TFdirClass _Class;               // working class

// Local functions :

static void DictFormat( void);
// Format

static void DictInsert( char *Name, TFdirHandle Handle, TFdirClass Class);
// Insert <Handle>

static void DictRemove( TFdirHandle Handle, TFdirClass *Class);
// Remove <Handle> returns <Class>

static void DictGet( int Index, TFdirHandle *Handle, TFdirClass *Class);
// Returns item by <Index>

static void DictPut( int Index, TFdirHandle Handle, TFdirClass Class);
// Save item at <Index>

static int DictSearch( TFdirHandle Handle, TFdirClass *Class);
// Search for <Handle> returns index & <Class>

static void DictExpand( int Start);
// Move dictionary one position up from <Start>

static void DictShrink( int Start);
// Move dictionary one position down up to <Start>

static int CompareName( char *Name1, char *Name2);
// Compare <Name1>, <Name2>. Returns -1, 0, 1

//------------------------------------------------------------------------------
//  Format
//------------------------------------------------------------------------------

void FdFormat( void)
// Formatting
{
   FdirFormat();
   DictFormat();
} // FdFormat

//------------------------------------------------------------------------------
//  Create
//------------------------------------------------------------------------------

TFdirHandle FdCreate( char *Name, TFdirClass Class)
// Create directory entry with <Name>
{
TFdirHandle Handle;

   Handle = FdirCreate( Name);
   if( Handle == FDIR_INVALID){
      return( Handle);                 // directory full
   }
   DictInsert( Name, Handle, Class);
   return( Handle);
} // FdCreate

//------------------------------------------------------------------------------
//  Rename
//------------------------------------------------------------------------------

TYesNo FdRename( TFdirHandle Handle, char *Name)
// Rename with <Name> at <Handle>
{
TFdirHandle NewHandle;
TFdirClass  Class;

   NewHandle = FdirSearch( Name);
   if( NewHandle){
      if( NewHandle == Handle){
         return( YES);                 // rename to same name
      }
      return( NO);                     // name already exists
   }
   DictRemove( Handle, &Class);
   FdirRename( Handle, Name);
   DictInsert( Name, Handle, Class);
   return( YES);
} // FdRename

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void FdDelete( TFdirHandle Handle)
// Delete directory entry
{
   FdirDelete( Handle);
   DictRemove( Handle, 0);
} // FdDelete


//******************************************************************************


//------------------------------------------------------------------------------
// Class
//------------------------------------------------------------------------------

void FdSetClass( TFdirClass Class)
// Set working class
{
int         i;
TFdirHandle ItemHandle;
TFdirClass  ItemClass;

   _Class = Class;                     // actual class
   // count items
   _ItemsCount = 0;
   for( i = 0; i < FDIR_SIZE; i++){
      DictGet( i, &ItemHandle, &ItemClass);
      if( ItemHandle == FDIR_INVALID){
         break;                        // end of dictionary
      }
      if( _Class == FDIR_CLASS_WILDCARD){
         _ItemsCount++;                // don't check class
      }
      if( _Class == ItemClass){
         _ItemsCount++;                // match class
      }
   }
} // FdSetClass

//------------------------------------------------------------------------------
// Valid
//------------------------------------------------------------------------------

TYesNo FdValid( TFdirHandle Handle)
// Check for <Handle> validity
{
TFdirClass Class;

   if( !Handle){
      return( NO);
   }
   if( DictSearch( Handle, &Class) < 0){
      return( NO);                     // handle not found
   }
   if( _Class != FDIR_CLASS_WILDCARD && _Class != Class){
      return( NO);
   }
   return( YES);
} // FdValid

//------------------------------------------------------------------------------
// Find begin
//------------------------------------------------------------------------------

void FdFindBegin( void)
// Prepare sequential search of the directory entries
{
   _CurrentItem = -1;
} // FdFindBegin

//------------------------------------------------------------------------------
// Find next
//------------------------------------------------------------------------------

TFdirHandle FdFindNext( void)
// Find next directory item
{
TFdirHandle ItemHandle;
TFdirClass  ItemClass;

   while( ++_CurrentItem < FDIR_SIZE){
      DictGet( _CurrentItem, &ItemHandle, &ItemClass);
      if( ItemHandle == FDIR_INVALID){
         return( ItemHandle);          // end of dictionary
      }
      if( _Class == FDIR_CLASS_WILDCARD){
         return( ItemHandle);          // don't check class
      }
      if( _Class == ItemClass){
         return( ItemHandle);          // match class
      }
   }
   return( FDIR_INVALID);
} // FdFindNext

//------------------------------------------------------------------------------
// Count
//------------------------------------------------------------------------------

int FdCount( void)
// Returns count of the directory items
{
   return( _ItemsCount);
} // FdCount

//------------------------------------------------------------------------------
// Move at
//------------------------------------------------------------------------------

TYesNo FdMoveAt( int Index)
// Move current directory pointer at <Index>
{
int         i;
int         Count;
TFdirHandle ItemHandle;
TFdirClass  ItemClass;

   if( Index < 0 || Index >= FDIR_SIZE){
      return( NO);
   }
   _CurrentItem = -1;                  // before first item
   Count = 0;                          // no valid item
   for( i = 0; i < FDIR_SIZE; i++){
      if( Count >= Index){
         return( YES);
      }
      DictGet( i, &ItemHandle, &ItemClass);
      if( ItemHandle == FDIR_INVALID){
         break;                        // end of dictionary
      }
      if( _Class == FDIR_CLASS_WILDCARD || _Class == ItemClass){
         _CurrentItem = i;
         Count++;                      // don't check class or match class
      }
   }
   _CurrentItem = -1;
   return( NO);
} // FdMoveAt

//------------------------------------------------------------------------------
// Get
//------------------------------------------------------------------------------

TFdirHandle FdGet( int Index)
// Returns handle by <Index>
{
   if( !FdMoveAt( Index)){
      return( FDIR_INVALID);
   }
   return( FdFindNext());
} // GdGet

//******************************************************************************

#define FDICT_ADDRESS FS_OFFSET + offsetof( TFsData, Dictionary)

//------------------------------------------------------------------------------
// Format
//------------------------------------------------------------------------------

static void DictFormat( void)
// Format
{
   EepFillData( FDICT_ADDRESS, 0, FDIR_SIZE * sizeof( TFdirDictionary));
} // DictFormat

//------------------------------------------------------------------------------
// Insert
//------------------------------------------------------------------------------

static void DictInsert( char *Name, TFdirHandle Handle, TFdirClass Class)
// Insert <Handle>
{
TFdirHandle OldHandle;
char        OldName[ FDIR_NAME_LENGTH + 1];
int         i;
int         Position;

   // search sorted position for <Name> :
   Position = -1;
   for( i = 0; i < FDIR_SIZE; i++){
      DictGet( i, &OldHandle, 0);
      if( OldHandle == FDIR_INVALID){
         Position = i;                 // end of dictionary
         break;
      }
      FdirGetName( OldHandle, OldName);
      if( CompareName( OldName, Name) > 0){
         Position = i;                 // previous items are less or equal
         break;
      }
   }
#ifdef __DEBUG__
   if( Position < 0){
      IERROR;
   }
#endif
   DictExpand( Position);              // shift dictionary
   DictPut( Position, Handle, Class);  // insert new item
} // DictInsert

//------------------------------------------------------------------------------
// Remove
//------------------------------------------------------------------------------

static void DictRemove( TFdirHandle Handle, TFdirClass *Class)
// Remove <Handle> returns <Class>
{
TFdirHandle OldHandle;
int         i;
int         Position;

   Position = DictSearch( Handle, Class);        // search for handle
#ifdef __DEBUG__
   if( Position < 0){
      IERROR;
   }
#endif
   DictShrink( Position);                        // shift dictionary
} // DictRemove

//------------------------------------------------------------------------------
// Get
//------------------------------------------------------------------------------

static void DictGet( int Index, TFdirHandle *Handle, TFdirClass *Class)
// Returns item by <Index>
{
TFdirDictionary Item;

   EepLoadData( FDICT_ADDRESS + Index * sizeof( TFdirDictionary), &Item, sizeof( TFdirDictionary));
   if( Handle){
      *Handle = Item.Handle;
   }
   if( Class){
      *Class  = Item.Class;
   }
} // DictGet

//------------------------------------------------------------------------------
// Get
//------------------------------------------------------------------------------

static void DictPut( int Index, TFdirHandle Handle, TFdirClass Class)
// Save item at <Index>
{
TFdirDictionary Item;

   Item.Handle = Handle;
   Item.Class  = Class;
   EepSaveData( FDICT_ADDRESS + Index * sizeof( TFdirDictionary), &Item, sizeof( TFdirDictionary));
} // DictPut

//------------------------------------------------------------------------------
// Search
//------------------------------------------------------------------------------

static int DictSearch( TFdirHandle Handle, TFdirClass *Class)
// Search for <Handle> returns index & <Class>
{
TFdirHandle OldHandle;
int         i;

   for( i = 0; i < FDIR_SIZE; i++){
      DictGet( i , &OldHandle, Class);
      if( OldHandle == Handle){
         return( i);
      }
   }
   return( -1);
} // DictSearch

//------------------------------------------------------------------------------
// Expand
//------------------------------------------------------------------------------

static void DictExpand( int Start)
// Move dictionary one position up from <Start>
{
TFdirDictionary Item;
int             i;

   for( i = FDIR_SIZE - 2; i >= Start; i--){
      EepLoadData( FDICT_ADDRESS +  i      * sizeof( TFdirDictionary), &Item, sizeof( TFdirDictionary));
      EepSaveData( FDICT_ADDRESS + (i + 1) * sizeof( TFdirDictionary), &Item, sizeof( TFdirDictionary));
   }
} // DictExpand

//------------------------------------------------------------------------------
// Shrink
//------------------------------------------------------------------------------

static void DictShrink( int Start)
// Move dictionary one position down up to <Start>
{
TFdirDictionary Item;
int             i;

   for( i = Start; i < FDIR_SIZE - 1; i++){
      EepLoadData( FDICT_ADDRESS + (i + 1) * sizeof( TFdirDictionary), &Item, sizeof( TFdirDictionary));
      EepSaveData( FDICT_ADDRESS +  i      * sizeof( TFdirDictionary), &Item, sizeof( TFdirDictionary));
   }
   DictPut( FDIR_SIZE - 1, 0, 0);      // clear last item
} // DictShrink

//------------------------------------------------------------------------------
// Compare
//------------------------------------------------------------------------------

static int CompareName( char *Name1, char *Name2)
// Compare <Name1>, <Name2>. Returns -1, 0, 1
{
   return( strcmp( Name1, Name2));
} // CompareName

