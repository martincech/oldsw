//******************************************************************************
//
//   Fs.c          File System utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/File/Fs.h"
#include "../../Inc/File/Fd.h"
#include "../../Inc/File/Fat.h"
#include "../../Inc/File/FsDef.h"
#include "../../Inc/Eep.h"
#include "MemoryDef.h"              // project directory EEPROM layout

#define __DEBUG__
#ifdef __DEBUG__
   #ifdef __GNUC__
      #include "../Inc/conio.h"
      #define IERROR      cgoto( 1, 0); cprintf( "Err FS %d", __LINE__);
   #endif
#endif

// Remark :
// First position in the new block is marked as
// Block = previous block, Offset = invalid
// Empty file Block = invalid, Offset = invalid

#if EEP_PAGE_SIZE % FS_BLOCK_SIZE != 0
   #error "Wrong block size"
#endif

#define FS_DATA_ADDRESS     (FS_OFFSET + offsetof( TFsData, Data))
#define FS_OFFSET_INVALID   -1                         // invalid block offset

#if FS_DATA_ADDRESS % EEP_PAGE_SIZE != 0
//!!!   #error "Data must start at page boundary"
#endif

// Seek descriptor :
typedef struct {
   int         Seek;                   // absolute seek position
   TFatIndex   Block;                  // block address
   short       Offset;                 // offset address
} TSeekData;

// File descriptor :
typedef struct {
   TFdirInfo   DirInfo;                // directory entry
   TFdirHandle DirHandle;              // directory entry handle
   TSeekData   Read;                   // read seek data
   TSeekData   Write;                  // write seek data
   byte        ReadOnly;               // Read only flag
} TFsDescriptor;

static TFsDescriptor _Fsd;

// Local functions :

static void HeaderWrite( TFsHeader *Header);
// Save filesystem <Header>

static void SeekAbsolute( TSeekData *Seek);
// Process absolute seek by <Seek> data

static TYesNo WriteCheckBlock( TYesNo FirstTime);
// Write check for block boundary

static void ReadCheckBlock( TYesNo FirstTime);
// Read check for block boundary

//------------------------------------------------------------------------------
// Initialisation
//------------------------------------------------------------------------------

void FsInit( void)
// Filesystem initialisation
{
   FdInit();
   FatInit();
   _Fsd.DirHandle = FDIR_INVALID;      // no opened file
} // FsInit

//------------------------------------------------------------------------------
// Format
//------------------------------------------------------------------------------

void FsFormat( void)
// Format filesystem
{
TFsHeader Header;

   Header.Version   = FS_VERSION;
   Header.FatWidth  = 16;
   Header.DirSize   = FDIR_SIZE;
   Header.BlockSize = FS_BLOCK_SIZE;
   Header.FatSize   = FAT_SIZE;
   HeaderWrite( &Header);
   FdFormat();
   FatFormat();
   _Fsd.DirHandle = FDIR_INVALID;      // no opened file
} // FsFormat

//------------------------------------------------------------------------------
// Free space
//------------------------------------------------------------------------------

int FsAvailable( void)
// Returns size of free space
{
   return( FatUnused() * FS_BLOCK_SIZE);
} // FsAvailable

//------------------------------------------------------------------------------
// Create
//------------------------------------------------------------------------------

TYesNo FsCreate( char *Name, TFdirClass Class)
// Create new file with <Name>
{
   _Fsd.DirHandle = FdCreate( Name, Class);      // create directory item
   if( !_Fsd.DirHandle){
      return( NO);                               // directory full or duplicate name
   }
   FdLoad( _Fsd.DirHandle, &_Fsd.DirInfo);       // read directory item
   _Fsd.DirInfo.Start  = FAT_INVALID;            // no starting block
   _Fsd.DirInfo.Size   = 0;                      // empty file
   FsRSeek( 0, FS_SEEK_SET);
   FsWSeek( 0, FS_SEEK_END);
   _Fsd.ReadOnly = NO;
   return( YES);
} // FsCreate

//------------------------------------------------------------------------------
// Open
//------------------------------------------------------------------------------

void FsOpen( TFdirHandle Handle, TYesNo ReadOnly)
// Open file with <Handle> as <ReadOnly>
{
   _Fsd.DirHandle = Handle;                      // search for directory item
   FdLoad( _Fsd.DirHandle, &_Fsd.DirInfo);       // read directory item
   FsRSeek( 0, FS_SEEK_SET);
   if( !ReadOnly){
      FsWSeek( 0, FS_SEEK_END);
   }
   _Fsd.ReadOnly = ReadOnly;
} // FsOpen

//------------------------------------------------------------------------------
// Directory
//------------------------------------------------------------------------------

TFdirInfo *FsInfo( void)
// Returns directory entry of the opened file or NULL
{
   if( _Fsd.DirHandle == FDIR_INVALID){
      return( NULL);
   }
   return( &_Fsd.DirInfo);
} // FsDirectoryEntry

//------------------------------------------------------------------------------
// Close
//------------------------------------------------------------------------------

void FsClose( void)
// Close current file
{
   if( !_Fsd.ReadOnly){
      // write enable
      FdSave( _Fsd.DirHandle, &_Fsd.DirInfo);
   }
   _Fsd.DirHandle = FDIR_INVALID;      // no opened file
} // FsClose

//------------------------------------------------------------------------------
// Rename
//------------------------------------------------------------------------------

TYesNo FsRename( TFdirHandle Handle, char *Name)
// Rename file with <Handle> to <Name>
{
   return( FdRename( Handle, Name));
} // FsRename

//------------------------------------------------------------------------------
// Truncate
//------------------------------------------------------------------------------

void FsTruncate( int Size)
// Truncate file to <Size>
{
TFatIndex NextBlock;
TSeekData Seek;

   if( Size >= _Fsd.DirInfo.Size){
      return;                          // cannot grow
   }
   _Fsd.DirInfo.Size = Size;           // set new size
   if( Size == 0){
      if( _Fsd.DirInfo.Start){
         FatFree( _Fsd.DirInfo.Start); // free all blocks
      }
      _Fsd.DirInfo.Start  = FAT_INVALID;// no data block
      return;
   }
   Seek.Seek = Size - 1;               // seek at last valid byte
   SeekAbsolute( &Seek);
#ifdef __DEBUG__
   if( Seek.Offset == FS_OFFSET_INVALID){
      IERROR;
   }
#endif
   FatTruncate( Seek.Block);
} // FsTruncate

//------------------------------------------------------------------------------
// Delete
//------------------------------------------------------------------------------

void FsDelete( void)
// Delete file
{
   FsTruncate( 0);                     // free all blocks
   FdDelete( _Fsd.DirHandle);          // delete directory entry
   _Fsd.DirHandle = FDIR_INVALID;      // no opened file
} // FsDelete

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------

TYesNo FsWrite( void *Data, int Size)
// Write <Date> of <Size> bytes at current position
{
int       i;
byte     *p;
TYesNo    FirstTime;
TYesNo    WriteOk;
TSeekData OldSeek;

#ifdef __DEBUG__
   if( !Size){
      IERROR;
   }
#endif
   OldSeek   = _Fsd.Write;                       // remember seek
   p         = (byte *)Data;
   WriteOk   = YES;
   FirstTime = YES;
   for( i = 0; i < Size; i++){
      if( !WriteCheckBlock( FirstTime)){         // block write start or start next block
         WriteOk = NO;                           // not enough space
         break;
      }
      FirstTime = NO;
      EepPageWriteData( *p);
      p++;
      _Fsd.Write.Offset++;                       // move offset
      if( _Fsd.Write.Offset >= FS_BLOCK_SIZE){
         EepPageWritePerform();                  // terminate block
         _Fsd.Write.Offset = FS_OFFSET_INVALID;  // request next block
      }
   }
   if( _Fsd.Write.Offset != FS_OFFSET_INVALID){
      EepPageWritePerform();                     // terminate write inside block
   }
   if( !WriteOk){
      _Fsd.Write = OldSeek;                      // seek recovery
      FatTruncate( OldSeek.Block);               // free newly allocated blocks
      return( NO);
   }
   _Fsd.Write.Seek += Size;                      // move seek
   if( _Fsd.Write.Seek > _Fsd.DirInfo.Size){
      _Fsd.DirInfo.Size = _Fsd.Write.Seek;       // new file size
   }
   return( YES);
} // FsWrite

//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------

TYesNo FsRead( void *Data, int Size)
// Read <Data> of <Size> bytes from current position
{
int    i;
byte  *p;
TYesNo FirstTime;

#ifdef __DEBUG__
   if( !Size){
      IERROR;
   }
#endif
   if( _Fsd.Read.Seek + Size > _Fsd.DirInfo.Size){
      return( NO);                               // past end of file
   }
   p         = (byte *)Data;
   FirstTime = YES;
   for( i = 0; i < Size; i++){
      ReadCheckBlock( FirstTime);                // block read start or start next block
      FirstTime = NO;
      *p = EepBlockReadData();
      p++;
      _Fsd.Read.Offset++;                        // move offset
      if( _Fsd.Read.Offset >= FS_BLOCK_SIZE){
         EepBlockReadStop();                     // terminate block
         _Fsd.Read.Offset = FS_OFFSET_INVALID;   // request next block
      }
   }
   _Fsd.Read.Seek += Size;                       // move seek
   if( _Fsd.Read.Offset != FS_OFFSET_INVALID){
      EepBlockReadStop();                        // terminate read inside block
   }
   return( YES);
} // FsRead

//------------------------------------------------------------------------------
// Read Seek
//------------------------------------------------------------------------------

TYesNo FsRSeek( int Offset, int Type)
// Seek current position by <Type> and <Offset>
{
   switch( Type){
      case FS_SEEK_SET :
         _Fsd.Read.Seek  = Offset;
         break;
      case FS_SEEK_CUR :
         _Fsd.Read.Seek += Offset;
         break;
      case FS_SEEK_END :
         _Fsd.Read.Seek  = _Fsd.DirInfo.Size + Offset;
         break;
   }
   if( _Fsd.Read.Seek < 0 || _Fsd.Read.Seek > _Fsd.DirInfo.Size){
      return( NO);
   }
   SeekAbsolute( &_Fsd.Read);
   return( YES);
} // FsRSeek

//------------------------------------------------------------------------------
// Write Seek
//------------------------------------------------------------------------------

TYesNo FsWSeek( int Offset, int Type)
// Seek current position by <Type> and <Offset>
{
   switch( Type){
      case FS_SEEK_SET :
         _Fsd.Write.Seek  = Offset;
         break;
      case FS_SEEK_CUR :
         _Fsd.Write.Seek += Offset;
         break;
      case FS_SEEK_END :
         _Fsd.Write.Seek  = _Fsd.DirInfo.Size + Offset;
         break;
   }
   if( _Fsd.Write.Seek < 0 || _Fsd.Write.Seek > _Fsd.DirInfo.Size){
      return( NO);
   }
   SeekAbsolute( &_Fsd.Write);
   return( YES);
} // FsWSeek

//******************************************************************************
// Read tell
//******************************************************************************

int FsRTell( void)
// Returns current read seek position
{
   return( _Fsd.Read.Seek);
} // FsRTell

//******************************************************************************
// Write tell
//******************************************************************************

int FsWTell( void)
// Returns current write seek position
{
   return( _Fsd.Write.Seek);
} // FsRTell

//******************************************************************************

//------------------------------------------------------------------------------
// Header
//------------------------------------------------------------------------------

static void HeaderWrite( TFsHeader *Header)
// Save filesystem <Header>
{
   EepSaveData( FS_OFFSET + offsetof( TFsData, Header), Header, sizeof( TFsHeader));
} // HeaderWrite

//------------------------------------------------------------------------------
// Seek
//------------------------------------------------------------------------------

static void SeekAbsolute( TSeekData *Seek)
// Process absolute seek by <Seek> data
{
int       FatIndex;
TFatIndex FatLast;

   if( Seek->Seek < FS_BLOCK_SIZE){
      // first block
      Seek->Block  = _Fsd.DirInfo.Start;
      Seek->Offset = (short)Seek->Seek;
      if( _Fsd.DirInfo.Start == FAT_INVALID){
         Seek->Offset = FS_OFFSET_INVALID;
      }
      return;
   }
   FatIndex     = Seek->Seek / FS_BLOCK_SIZE;
   Seek->Offset = Seek->Seek % FS_BLOCK_SIZE;
   FatLast      = FatOffset( _Fsd.DirInfo.Start, FatIndex - 1); // last but one block
#ifdef __DEBUG__
   if( FatLast == FAT_INVALID){
      IERROR;                          // last but one block is invalid
   }
#endif
   Seek->Block = FatNext( FatLast);
   if( Seek->Block != FAT_INVALID){
      return;                          // position inside block
   }
#ifdef __DEBUG__
   if( Seek->Offset != 0){
      IERROR;                          // seek past the end of the file
   }
#endif
   Seek->Block  = FatLast;             // set last but one block
   Seek->Offset = FS_OFFSET_INVALID;   // with invalid offset
} // SeekAbsolute

//------------------------------------------------------------------------------
// Write Check
//------------------------------------------------------------------------------

static TYesNo WriteCheckBlock( TYesNo FirstTime)
// Write check for block boundary
{
TFatIndex NewBlock;

   if( _Fsd.Write.Offset == FS_OFFSET_INVALID){
      // at end of block
      NewBlock = FatNext( _Fsd.Write.Block);
      if( NewBlock == FAT_INVALID){
         // past end of file
         NewBlock = FatAppend( _Fsd.Write.Block);
         if( NewBlock == FAT_INVALID){
            return( NO);               // not enough space
         }
         if( _Fsd.Write.Block == FAT_INVALID){
            _Fsd.DirInfo.Start = NewBlock;  // first block
         }
      }
      _Fsd.Write.Block  = NewBlock;    // append new block
      _Fsd.Write.Offset = 0;           // at start of block
      FirstTime        = YES;          // start write of the new block
   }
   if( FirstTime){
      EepPageWriteStart( FS_DATA_ADDRESS +
                         FatGetBlock( _Fsd.Write.Block) * FS_BLOCK_SIZE +
                         _Fsd.Write.Offset);
   }
   return( YES);
} // WriteCheckBlock

//------------------------------------------------------------------------------
// Read Check
//------------------------------------------------------------------------------

static void ReadCheckBlock( TYesNo FirstTime)
// Read check for block boundary
{
TFatIndex NextBlock;

   if( _Fsd.Read.Offset == FS_OFFSET_INVALID){
      // at end of block
      NextBlock = FatNext( _Fsd.Read.Block);
#ifdef __DEBUG__
      if( NextBlock == FAT_INVALID){
         IERROR;                       // past end of file
      }
#endif
      _Fsd.Read.Block  = NextBlock;    // set next block
      _Fsd.Read.Offset = 0;            // at start of block
      FirstTime       = YES;           // start read of the new block
   }
   if( FirstTime){
      EepBlockReadStart( FS_DATA_ADDRESS +
                         FatGetBlock( _Fsd.Read.Block) * FS_BLOCK_SIZE +
                         _Fsd.Read.Offset);
   }
} // ReadCheckBlock

