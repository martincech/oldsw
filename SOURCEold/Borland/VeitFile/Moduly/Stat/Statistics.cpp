//*****************************************************************************
//
//    Statistics.c  -  Statistics utility
//    Version 1.0, (c) Vymos & P.Veit
//
//*****************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../Inc/Statistics.h"
#include <math.h>

#define MIN_AVERAGE        1e-03         // mensi hodnota povazovana za 0
#define MIN_SIGMA          1e-03         // mensi hodnota povazovana za 0
#define DEFAULT_UNIFORMITY 100           // implicitni uniformita (pri chybe)

static TNumber StatRealUniMin;
static TNumber StatRealUniMax;
static word StatRealUniCountIn;
static word StatRealUniCountOut;

//******************************************************************************
// Mazani
//******************************************************************************

void StatClear( TStatistic *statistic)
// smaze statistiku
{
   statistic->XSuma  = 0;
   statistic->X2Suma = 0;
   statistic->Count  = 0;
} // StatClear

//******************************************************************************
// Pridani
//******************************************************************************

void StatAdd( TStatistic *statistic, TNumber x)
// prida do statistiky
{
   statistic->XSuma  += x;
   statistic->X2Suma += x * x;
   statistic->Count++;
} // StatAdd

//******************************************************************************
// Prumer
//******************************************************************************

TNumber StatAverage( TStatistic *statistic)
// vrati stredni hodnotu
{
   if( statistic->Count == 0){
      return( 0);
   }
   return( statistic->XSuma / statistic->Count);
} // StatAverage

//******************************************************************************
// Odchylka
//******************************************************************************

TNumber StatSigma( TStatistic *statistic)
// vrati smerodatnou odchylku
{
TNumber tmp;

   if (statistic->Count <= 1) {
      return( 0);
   }
   tmp = statistic->XSuma * statistic->XSuma / statistic->Count;
   tmp = statistic->X2Suma - tmp;
   if( tmp <= MIN_SIGMA){
      return( 0);
   }
   return( sqrt( 1/(TNumber)(statistic->Count - 1) * tmp));
} // StatSigma

//******************************************************************************
// Variace
//******************************************************************************

TNumber StatVariation( TNumber average, TNumber sigma)
// vrati variaci (procentni smerodatna odchylka)
{
   if( average < MIN_AVERAGE){
      return( 0);         // "deleni nulou"
   }
   return( sigma / average * 100);
} // StatVariation

//******************************************************************************
// Vypocet presne uniformity primo ze zadanych vzorku
//******************************************************************************

// Vyuziva tyto globalni promenne (kvuli rychlosti je nepredavam parametrem):
//  - TNumber StatRealUniMin: spodni mez okoli prumeru
//  - TNumber StatRealUniMax: horni mez okoli prumeru
//  - word StatRealUniCountIn: pocet vzorku v pasmu okoli
//  - word StatRealUniCountOut: pocet vzorku mimo pasmo okoli

void StatUniformityInit(TNumber Min, TNumber Max)
{
  // Inicializuje globalni promenne
  StatRealUniMin = Min;
  StatRealUniMax = Max;
  StatRealUniCountOut = 0;
  StatRealUniCountIn  = 0;
}  // StatUniformityInit

void StatUniformityAdd(TNumber Number)
{
  // Prida novy vzorek do uniformity
  if (Number < StatRealUniMin || Number > StatRealUniMax) {
    // Vzorek je mimo pasmo
    StatRealUniCountOut++;
    return;
  }//if
  // Vzorek je v pasmu
  StatRealUniCountIn++;
} // StatUniformityAdd

byte StatUniformityGet( void)
{
  // Vypocte uniformitu v celych procentech, vrati cislo 0 - 100
  if (StatRealUniCountIn == 0 && StatRealUniCountOut == 0) {
    return DEFAULT_UNIFORMITY;
  } else {
    return (dword)(100L * (dword)StatRealUniCountIn / (dword)((dword)StatRealUniCountIn + (dword)StatRealUniCountOut));
  }//else
} // StatUniformityGet

