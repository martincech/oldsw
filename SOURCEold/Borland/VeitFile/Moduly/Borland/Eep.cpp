//*****************************************************************************
//
//    Eep.c        Virtual EEPROM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/Eep.h"

static byte Buffer[ EEP_SIZE];
static word BlockAddress;
static byte _ReadStart;
static byte _WriteStart;

void EepInit( void)
// Nastavi klidove hodnoty na sbernici, inicializuje pamet
{
  BlockAddress = 0;
  _ReadStart   = NO;
  _WriteStart  = NO;
} // EepInit

byte EepReadByte( word Address)
// Precte byte z EEPROM <address>
{
  return Buffer[ Address];
} // EepReadByte

TYesNo EepWriteByte( word Address, byte Value)
// Zapise byte <value> na <address> v EEPROM
// Vraci NO neni-li zapis mozny
{
  if( Address >= EEP_SIZE) {
    return NO;
  }
  Buffer[ Address]  = Value;
  return( YES);
} // EepWriteByte

//------ Blokove cteni -----------------------------

void EepBlockReadStart( word Address)
// Zahaji blokove cteni z EEPROM <address>
{
   if( _ReadStart){
      IERROR;        // missing block read stop
   }
   BlockAddress = Address;
   _ReadStart   = YES;
} // EepBlockReadStart

byte EepBlockReadData( void)
// Cteni bytu bloku
{
   if( !_ReadStart){
      IERROR;       // missing blok rad start
   }
   byte Result;
   Result = Buffer[ BlockAddress];
   BlockAddress++;
   return( Result);
} // EepBlockReadData

void EepBlockReadStop( void)
// Ukonceni cteni bloku
{
   if( !_ReadStart){
      IERROR;      // stop without start
   }
   _ReadStart = NO;
} // EepBlockReadStop

//------ Strankovy zapis -----------------------------

TYesNo EepPageWriteStart( word Address)
// Zahaji zapis stranky od <address> v EEPROM.
// Vraci NO neni-li zapis mozny
{

   if( _WriteStart){
      IERROR;        // missing page write stop
   }
   if( Address >= EEP_SIZE) {
     return NO;
   }
   BlockAddress = Address;
   _WriteStart  = YES;
   return( YES);
} // EepPageWriteStart

void EepPageWriteData( byte Value)
// Zapis bytu do stranky
{
   if( !_WriteStart){
      IERROR;        // missing page write start
   }
   Buffer[BlockAddress++] = Value;
} // EepPageWriteData

void EepPageWritePerform( void)
// Odstartuje fyzicky zapis stranky do EEPROM
{
   if( !_WriteStart){
      IERROR;        // stop without write start
   }
   _WriteStart = NO;
} // EepPageWritePerform

//-----------------------------------------------------------------------------
// Ulozeni dat
//-----------------------------------------------------------------------------

#define DataPtr   ((byte *)Data)

TYesNo EepSaveData( word Address, void *Data, int Size)
// Zapise <Data> o velikosti <Size> na <Address>
{
TYesNo NewPage;

   NewPage = YES;                      // prvni blok
   while( Size-- > 0){
      if( NewPage){
         // Zacatek nove stranky
         NewPage = NO;
         if (!EepPageWriteStart(Address)){
            return( NO);
         }
      }
      EepPageWriteData( *DataPtr);
      DataPtr++;
      Address++;
      if( Address % EEP_PAGE_SIZE == 0){
         // Nova adresa uz lezi v nove strance, provedu zapis stranky
         EepPageWritePerform();        // predchozi stranka
         NewPage = YES;                // start zapisu nove stranky
      }
   }
   if (!NewPage) {
      EepPageWritePerform();           // zbytek posledni stranky
   }
   return( YES);
} // EepSaveData

//-----------------------------------------------------------------------------
// Nacteni dat
//-----------------------------------------------------------------------------

void EepLoadData( word Address, void *Data, int Size)
// Nacte <Data> o velikosti <Size> z <Address>
{

   EepBlockReadStart( Address);
   while( Size-- > 0){
      *DataPtr = EepBlockReadData();
      DataPtr++;
   }
   EepBlockReadStop();
} // EepLoadData

//-----------------------------------------------------------------------------
// Vyplneni pameti hodnotou
//-----------------------------------------------------------------------------

TYesNo EepFillData( word Address, byte Pattern, int Size)
// Vyplni prostor od <Address> v delce <Size> pomoci <Pattern>
{
TYesNo NewPage;

   NewPage = YES;                 // prvni blok
   while( Size-- > 0) {
      if (NewPage) {
         // Zacatek nove stranky
         NewPage = NO;
         if( !EepPageWriteStart( Address)){
            return( NO);
         }
      }
      EepPageWriteData( Pattern);
      Address++;
      if( Address % EEP_PAGE_SIZE == 0) {
         // Nova adresa uz lezi v nove strance, provedu zapis stranky
         EepPageWritePerform();        // predchozi stranka
         NewPage = YES;                // start zapisu nove stranky
      }
   }
   if( !NewPage) {
     EepPageWritePerform();            // zbytek posledni stranky
   }
   return( YES);
} // EepFillData

//-----------------------------------------------------------------------------
// Komparace
//-----------------------------------------------------------------------------

TYesNo EepMatchData( word Address, void *Data, int Size)
// Komparuje <Data> o velikosti <Size> na <Address>
{
   EepBlockReadStart( Address);
   while( Size-- > 0){
      if( *DataPtr != EepBlockReadData()){
         EepBlockReadStop();
         return( NO);
      }
      DataPtr++;
   }
   EepBlockReadStop();
   return( YES);
} // EepMatchData

