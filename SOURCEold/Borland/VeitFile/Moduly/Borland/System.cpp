//******************************************************************************
//
//   System.cpp   Operating system utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

// Borland only
#include <vcl.h>
#pragma hdrstop

#include "../../Inc/System.h"
#include <time.h>

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------

TTimestamp SysGetClock( void)
// Get actual time
{
struct tm Tm;

   Tm.tm_sec   = 0;
   Tm.tm_min   = 0;
   Tm.tm_hour  = 0;
   Tm.tm_mday  = 1;
   Tm.tm_mon   = 1     - 1;
   Tm.tm_year  = 2000  - 1900;
   Tm.tm_isdst = -1;
   time_t Offset = mktime( &Tm);
   return( time(NULL) - Offset);
} // SysGetClock

