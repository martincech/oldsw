//******************************************************************************
//
//   System.h     Operating system utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __System_H__
   #define __System_H__

#include "Dt.h"

TTimestamp SysGetClock( void);
// Get actual time

#endif

