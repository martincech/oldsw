//*****************************************************************************
//
//    Stat.h  -  Statistics utility
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Statistics_H__
   #define __Statistics_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Popisovac statistiky :
typedef struct {
   TNumber XSuma;
   TNumber X2Suma;
   word    Count;
} TStatistic;  // 10 bajtu

//-----------------------------------------------------------------------------

void StatClear( TStatistic *statistic);
// smaze statistiku

void StatAdd( TStatistic *statistic, TNumber x);
// prida do statistiky

TNumber StatAverage( TStatistic *statistic);
// vrati stredni hodnotu

TNumber StatSigma( TStatistic *statistic);
// vrati smerodatnou odchylku

TNumber StatVariation( TNumber average, TNumber sigma);
// vrati variaci (procentni smerodatna odchylka)

void StatUniformityInit( TNumber Min, TNumber Max);
// Inicializuje globalni promenne

void StatUniformityAdd( TNumber Number);
// Prida novy vzorek do uniformity

byte StatUniformityGet( void);
// Vypocte uniformitu v celych procentech

#endif
