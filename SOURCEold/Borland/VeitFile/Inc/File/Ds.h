//******************************************************************************
//
//   Ds.h          Data Set utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Ds_H__
   #define __Ds_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Db_H__
   #include "Db.h"
#endif

#define DS_SIZE     (((FDIR_SIZE + 1) + 7) / 8)  // dataset bytes
typedef byte TFDataSet[ DS_SIZE];

//------------------------------------------------------------------------------
// Dataset utility
//------------------------------------------------------------------------------

void DsClear( TFDataSet DataSet);
// Clear dataset

void DsAdd( TFDataSet DataSet, TFdirHandle Handle);
// Add file to dataset

void DsRemove( TFDataSet DataSet, TFdirHandle Handle);
// Remove file from dataset

TYesNo DsContains( TFDataSet DataSet, TFdirHandle Handle);
// Returns YES if <DataSet> contains <Handle>

TYesNo DsIsEmpty( TFDataSet DataSet);
// Empty dataset

//------------------------------------------------------------------------------
// Working data set
//------------------------------------------------------------------------------

void DsSetCurrent( void);
// Set dataset to currently opened file

void DsSet( TFDataSet DataSet);
// Set <DataSet> as current

//------------------------------------------------------------------------------
// Location in working data set
//------------------------------------------------------------------------------

void DsBegin( void);
// Set before start of the first table

TYesNo DsNext( void *Record);
// Get next record

#endif

