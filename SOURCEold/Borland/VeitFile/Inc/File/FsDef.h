//******************************************************************************
//
//   FsDef.h      File system common definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __FsDef_H__
   #define __FsDef_H__

#ifndef __FdirDef_H__
   #include "FdirDef.h"
#endif

#define FS_VERSION 0x20                // filesystem version

// Filesystem parameters :

typedef struct {
   byte Version;
   byte FatWidth;
   byte DirSize;
   byte BlockSize;
   word FatSize;
} TFsHeader;

// Filesystem structure :

typedef struct {
   TFsHeader       Header;                           // parameters
   TFdirDictionary Dictionary[ FDIR_SIZE];           // directory dictionary
   TFdirInfo       Directory[ FDIR_SIZE];            // directory entries
   TFatItem        Fat[ FAT_SIZE];                   // FAT table
   byte            Data[ FAT_SIZE][ FS_BLOCK_SIZE];  // data records
} TFsData;

#endif
