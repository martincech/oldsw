//******************************************************************************
//
//   Fat.h         File allocation table utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Fat_H__
   #define __Fat_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FatDef_H__
   #include "FatDef.h"
#endif

void FatInit( void);
// Initialisation

void FatFormat( void);
// Set all block empty

TFatIndex FatAppend( TFatIndex Index);
// Append block at <Index>, if <Index> == FAT_INVALID, allocate first block

void FatFree( TFatIndex Start);
// Release block chain by <Start> inclusive

void FatTruncate( TFatIndex Last);
// Mark <Last> as eof, release next chain

TFatIndex FatOffset( TFatIndex Start, int Offset);
// Find block by <Start> at <Offset> position

TFatIndex FatNext( TFatIndex Index);
// Get next block

int FatUnused( void);
// Returns total count of free blocks

#endif
