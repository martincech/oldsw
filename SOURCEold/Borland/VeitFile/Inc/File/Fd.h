//******************************************************************************
//
//   Fd.h         File Directory utilites
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Fd_H__
   #define __Fd_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Fdir_H__
   #include "Fdir.h"
#endif

//------------------------------------------------------------------------------
// Location functions
//------------------------------------------------------------------------------

// TFdirHandle == FDIR_INVALID (0) when function failed
// Warning : use FdSetClass() first !

void FdSetClass( TFdirClass Class);
// Set working class

TYesNo FdValid( TFdirHandle Handle);
// Check for <Handle> validity

void FdFindBegin( void);
// Prepare sequential search of the directory entries

TFdirHandle FdFindNext( void);
// Find next directory item

int FdCount( void);
// Returns count of the directory items

TYesNo FdMoveAt( int Index);
// Move current directory pointer at <Index>

TFdirHandle FdGet( int Index);
// Returns handle by <Index>

#define FdSearch( Name)             FdirSearch( Name)
// Returns handle by <Name>

#define FdExists( Name)             FdirSearch( Name)
// Chech for <Name> existence

#define FdGetName( Handle, Name)    FdirGetName( Handle, Name)
// Fills <Name> by <Handle>

//------------------------------------------------------------------------------
// Internal functions (by Fs only)
//------------------------------------------------------------------------------

#define FdInit()                       FdirInit()
// Initialisation

void FdFormat( void);
// Formatting

TFdirHandle FdCreate( char *Name, TFdirClass Class);
// Create directory entry with <Name>

TYesNo FdRename( TFdirHandle Handle, char *Name);
// Rename with <Name> at <Handle>

void FdDelete( TFdirHandle Handle);
// Delete directory entry

#define FdLoad( Handle, Item)          FdirLoad( Handle, Item)
// Get directory <Item> by <Handle>

#define FdSave( Handle, Item)          FdirSave( Handle, Item)
// Save directory entry <Item> at <Handle>

#define FdGetName( Handle, Name)       FdirGetName( Handle, Name)
// Fills <Name> by <Handle>

#endif

