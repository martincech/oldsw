//******************************************************************************
//
//   Fs.h          File System utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Fs_H__
   #define __Fs_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __FsDef_H__
   #include "FsDef.h"                  // filesystem structure
#endif

// FsSeek constants :
typedef enum {
   FS_SEEK_SET,
   FS_SEEK_CUR,
   FS_SEEK_END
} TFsSeekEnum;

void FsInit( void);
// Filesystem initialisation

void FsFormat( void);
// Format filesystem

int FsAvailable( void);
// Returns size of free space

TYesNo FsCreate( char *Name, TFdirClass Class);
// Create new file with <Name> and <Class>

void FsOpen( TFdirHandle Handle, TYesNo ReadOnly);
// Open file with <Handle> as <ReadOnly>

TFdirInfo *FsInfo( void);
// Returns directory entry of the opened file or NULL

void FsClose( void);
// Close current file

TYesNo FsRename( TFdirHandle Handle, char *Name);
// Rename file with <Handle> to <Name>

void FsTruncate( int Size);
// Truncate file to <Size>

void FsDelete( void);
// Delete file

TYesNo FsWrite( void *Data, int Size);
// Write <Date> of <Size> bytes at current position

TYesNo FsRead( void *Data, int Size);
// Read <Data> of <Size> bytes from current position

TYesNo FsRSeek( int Offset, int Type);
// Seek current position by <Type> and <Offset>

TYesNo FsWSeek( int Offset, int Type);
// Seek current position by <Type> and <Offset>

int FsRTell( void);
// Returns current read seek position

int FsWTell( void);
// Returns current write seek position

#endif
