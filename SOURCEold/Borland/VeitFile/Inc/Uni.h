//******************************************************************************
//
//   Uni.H  - Universal declarations
//   Version 0.0  (c) VymOs
//
//******************************************************************************


#ifndef UniH
   #define UniH
   #define __Uni_H__

// Systemove nezavisle definice : ----------------------------------------------

#define if__err( function) if( (function) != 0)
#define if__ok( function)  if( (function) == 0)

#ifdef __WIN32__
#define if_errh( function) if( (function) == INVALID_HANDLE_VALUE)
#define if_okh(  function) if( (function) != INVALID_HANDLE_VALUE)
#endif

#define if_null( function) if( (function) == NULL)
#define if_nonull( function) if( (function) != NULL)

#define while_nonull( function) while( (function) != NULL)

#define set_nonull( var, value) if( var != NULL){ *var = value;}

#define forever  while( 1)

// Nepouzite parametry, kvuli hlaseni kompilatoru :
#define arg_used( a)   a = a

// funkce :

#define strequ( s1, s2)    !strcmp( s1, s2)
#define strnequ( s1, s2, n)!strncmp( s1, s2, n)
#define memequ( m1, m2, l) !memcmp( m1, m2, l)

#define NO   false
#define YES  true
typedef bool TYesNo;

// Chybova hlaseni : -----------------------------------------------------------

#if defined( __CONSOLE__) || defined( __MSDOS__)
   // DOS a Win32 console
   #define IERROR   printf( "Error on file %s line %d\n", __FILE__, __LINE__);
   #define IDEFAULT default : printf( "Error switch file %s line %d\n", __FILE__, __LINE__);
   #ifdef __WIN32__
      #define ErrorMessage() \
           {LPVOID lpMsgBuf;\
            FormatMessage(\
               FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,\
               NULL,\
               GetLastError(),\
               MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),\
               (LPTSTR) &lpMsgBuf,\
               0,\
               NULL\
            );\
            printf( "System error : %s\n", (LPCSTR)lpMsgBuf);\
            LocalFree( lpMsgBuf );}
   #else
      #define ErrorMessage() printf( "System error : %d", errno);
   #endif
#else
   // Win32 GUI
   #define IERROR \
      throw Exception( AnsiString( "Error on file ")+__FILE__+" line "+__LINE__);

   #define IDEFAULT \
      default : throw Exception( AnsiString( "Error switch file ")+__FILE__+" line "+__LINE__);

   #define ErrorMessage()\
        {LPVOID lpMsgBuf;\
         FormatMessage(\
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,\
            NULL,\
            GetLastError(),\
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),\
            (LPTSTR) &lpMsgBuf,\
            0,\
            NULL\
         );\
         ::MessageBox(NULL, (LPCSTR)lpMsgBuf, "System Error", MB_OK|MB_ICONINFORMATION);\
         LocalFree( lpMsgBuf );}
#endif

// datove typy : ---------------------------------------------------------------

#ifdef __DEFINE_BOOL__
   typedef int bool;
   #define false 0
   #define true  1
#endif

#ifdef __MSDOS__
typedef unsigned char byte;
typedef unsigned int  word;
typedef unsigned long dword;
typedef signed char   sbyte;
typedef signed int    sword;
typedef signed long   sdword;

typedef struct {
   byte b[ 8];
} qword;

#define __fastcall
#define Sleep( ms) delay( ms)

#include <stdio.h>
#ifdef __cplusplus
   #include "AnsiStr.h"
   #include "DateTime.h"
#endif   

#endif

#ifdef __WIN32__
typedef unsigned char  byte;
typedef unsigned short word;
typedef unsigned long  dword;
typedef signed char    sbyte;
typedef signed short   sword;
typedef signed long    sdword;
typedef unsigned long  native;      // by register size

typedef unsigned __int64 qword;

typedef signed char  int8;             // znamenkovy integer - byte
typedef short        int16;            // znamenkovy integer - word
typedef int          int32;            // znamenkovy integer - dword
typedef __int64      int64;            // znamenkovy integer - qword

#endif

// union na predavani velicin  : -----------------------------------------------


typedef union {
   qword  qw;         // 64 bitu long
   dword  dw;
   word   w;
   byte   b;
   float  f;
   double d;
   byte   array[ 8];
} TDataConvertor;

//-----------------------------------------------------------------------------
// operace s nibbly :
//-----------------------------------------------------------------------------

#define lnibble( x)     ((x) & 0x0F)
#define hnibble( x)     (((x) >> 4) & 0x0F)

#endif

