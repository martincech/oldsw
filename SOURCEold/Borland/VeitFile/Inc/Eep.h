//*****************************************************************************
//
//    Eep.h        Virtual EEPROM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Eep_H__
   #define __Eep_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void EepInit( void);
// Nastavi klidove hodnoty na sbernici, inicializuje pamet

byte EepReadByte( word Address);
// Precte byte z EEPROM <Address>

TYesNo EepWriteByte( word Address, byte Value);
// Zapise byte <Value> na <Address> v EEPROM
// Vraci NO neni-li zapis mozny

//------ Blokove cteni -----------------------------

void EepBlockReadStart( word Address);
// Zahaji blokove cteni z EEPROM <Address>

byte EepBlockReadData( void);
// Cteni bytu bloku

void EepBlockReadStop( void);
// Ukonceni cteni bloku

//------ Strankovy zapis -----------------------------
// Pozor, hranice stranek hlida uzivatel API
// EEP_PAGE_SIZE je v Hardware.h

TYesNo EepPageWriteStart( word Address);
// Zahaji zapis stranky od <Address> v EEPROM.
// Vraci NO neni-li zapis mozny

void EepPageWriteData( byte Value);
// Zapis bytu do stranky

void EepPageWritePerform( void);
// Odstartuje fyzicky zapis stranky do EEPROM

//------ Obecny zapis/cteni --------------------------

TYesNo EepSaveData( word Address, void *Data, int Size);
// Zapise <Data> o velikosti <Size> na <Address>

void EepLoadData( word Address, void *Data, int Size);
// Nacte <Data> o velikosti <Size> z <Address>

TYesNo EepFillData( word Address, byte Pattern, int Size);
// Vyplni prostor od <Address> v delce <Size> pomoci <Pattern>

TYesNo EepMatchData( word Address, void *Data, int Size);
// Komparuje <Data> o velikosti <Size> na <Address>

#endif
