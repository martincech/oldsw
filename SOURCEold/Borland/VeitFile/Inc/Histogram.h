//*****************************************************************************
//
//    Histogram.h   -  Histogram calculations
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Histogram_H__
   #define __Histogram_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef struct {
   THistogramValue Center;                  // Stredni hodnota, kolem ktere se histogram vytvari - je treba zadat pri inicializaci
   THistogramValue Step;                    // Krok histogramu - je treba zadat pri inicializaci
   THistogramCount Slot[ HISTOGRAM_SLOTS];
} THistogram;   // 86 bajtu

void HistogramClear( THistogram *Histogram);
// Mazani histogramu

void HistogramAdd( THistogram *Histogram, THistogramValue Value);
// Prida hodnotu do histogramu

THistogramValue HistogramGetDenominator(THistogram *Histogram, THistogramValue MaxValue);
// Vrati cislo, kterym je treba podelit vsechny sloupce, aby se histogram normoval na maximalni hodnotu <MaxValue>

void HistogramSetStep(THistogram *Histogram, byte Range);
// Vypocte a ulozi do zadaneho histogramu krok podle nastaveneho stredu a rozsahu. Rozsah se zadava v +-%, tj. Range = 10 znamena +-10% okolo stredu.

TYesNo HistogramEmpty(THistogram *Histogram);
// Pokud je histogram prazdny, vrati YES

THistogramValue HistogramGetValue(THistogram *Histogram, byte SlotIndex);
// Vrati hodnotu sloupce s indexem <SlotIndex>

#endif
