//******************************************************************************
//
//   CountryDef.h   Country specific locale definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef __CountryDef_H__
   #define __CountryDef_H__

#ifndef __DtDef_H__
   #include "DtDef.h"
#endif

// Country :
typedef enum {
   COUNTRY_INTERNATIONAL,
   COUNTRY_US,
   COUNTRY_CZECH,
   COUNTRY_SLOVAK,
   COUNTRY_JAPAN,
   _COUNTRY_COUNT
} TCountryEnum;

// Language :
typedef enum {
   LNG_ENGLISH,
   LNG_CZECH,
   LNG_SLOVAK,
   LNG_JAPAN,
   _LNG_COUNT
} TLanguageEnum;

// Code pages :
typedef enum {
   CPG_LATIN,
   CPG_JAPAN,
   _CPG_COUNT
} TCodePageEnum;

// Date formats :
typedef enum {
   DATE_FORMAT_DDMMYY,
   DATE_FORMAT_MMDDYY,
   _DATE_FORMAT_COUNT
} TDateFormatEnum;


// Time formats :
typedef enum {
   TIME_FORMAT_24,
   TIME_FORMAT_12,
   _TIME_FORMAT_COUNT
} TTimeFormatEnum;

// Locales descriptor :
typedef struct {
   byte CodePage;           // TCodePageEnum
   byte DateFormat;         // date format
   char DateSeparator;      // date separator character
   byte TimeFormat;         // time format
   char TimeSeparator;      // time separator character
   byte DstType;            // daylight saving type
} TLocale;

// Country descriptor :
typedef word TCountry;

#endif
