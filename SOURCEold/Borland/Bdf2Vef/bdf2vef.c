//---------------------------------------------------------------------------
//
//   Bdf2Vef   BDF font convertor
//   Version 1.0  (c) Vymos
//
//---------------------------------------------------------------------------

#include <windows.h>
#pragma hdrstop

#include "uni.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define PATH_MAX 255
#define LINE_MAX 1024
#define SEPARATORS " \n\r\t\h"

static char infile[ PATH_MAX + 1];
static char outfile[ PATH_MAX + 1];
static FILE *fi, *fo;
static int comment = 1;

// Lokalni funkce :

static void convert_font( void);
// transformuje font BDF do VEF

//---------------------------------------------------------------------------
// Main
//---------------------------------------------------------------------------

#pragma argsused
int main( int argc, char* argv[])
{
char *p;

   if( argc < 2){
      printf( "Use : bdf2vef <source_file>[.bdf] [disable_comment]\n");
      return( 3);
   }
   if( argc > 2){
      comment = 0;
   }
   strcpy( infile, argv[ 1]);
   if_null( strrchr( infile, '.')){
      strcat( infile, ".bdf");
   }
   strcpy( outfile, infile);
   // odsekni priponu :
   p = strrchr( outfile, '.');
   *p = '\0';
   strcat( outfile, ".vef");

   if_null( fi = fopen( infile, "r")){
      printf( "Unable open input file : %s\n", infile);
      return( 8);
   }
   if_null( fo = fopen( outfile, "w")){
      printf( "Unable open output file : %s\n", outfile);
      return( 8);
   }
   convert_font();
   fclose( fi);
   fclose( fo);
   return 0;
} // main


static void convert_font( void)
// transformuje font BDF do VEF
{
static char buff[ LINE_MAX];
char *p;
int  is_bitmap = 0;
char ch        = '\0';
int  line_no   = 0;

   fprintf( fo, "// Font XXX BDF import :\n");
   fprintf( fo, "static byte_t code FontXXX[] = {\n");
   while( 1){
      if_null( fgets( buff, LINE_MAX, fi)){
         fprintf( fo, "};\n");
         return;
      }
      line_no++;
      p = strtok( buff, SEPARATORS);
      if_null( p){
         continue; // jen separatory na radku
      }
      if( strequ( p, "STARTCHAR")){
         // priprava na novy znak
         is_bitmap = 0;  // pro jistotu
         continue;
      } else if( strequ( p, "ENCODING")){
         // kodove cislo znaku
         p = strtok( NULL, SEPARATORS);
         if_null( p){
            printf( "Syntax error near line %d", line_no);
            return;
         }
         ch = atoi( p);
         if( comment){
            if( ch >= ' '){
               fprintf( fo, "   /* 0x%02X '%c' */", ch & 0xFF, ch);
            } else {
               fprintf( fo, "   /* 0x%02X '?' */",  ch & 0xFF);
            }
         } else {
            fprintf( fo, "   ");
         }
         continue;
      } else if( strequ( p, "BITMAP")){
         // zacatek bitove mapy znaku
         is_bitmap = 1;
         continue;
      } else if( strequ( p, "ENDCHAR")){
         // konec znaku
         is_bitmap = 0;
         fprintf( fo, "\n");
         fflush( fo);
         continue;
      } // else jine klicove slovo nebo radek
      if( is_bitmap){
         fprintf( fo, " 0x%s,", p);
      }
   }
} // convert_font

