//---------------------------------------------------------------------------
//
//   R90.c    90 grad Font Rotation
//   Version 1.0  (c) Vymos
//
//---------------------------------------------------------------------------

#include <windows.h>
#pragma hdrstop

#include "uni.h"
#include "fnt_prs.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>

//---------------------------------------------------------------------------
#pragma argsused

static char infile[ 256];
static char outfile[ 256];
FILE *fo;
int  FntWidth  = 8;
int  FntHeight = 8;
int  FntIndex  = 0;
int  CharIndex = 0;
int  IsAsm     = 1;

int main( int argc, char* argv[])
{
char *p;

   if( argc < 4){
      printf( "Use : R90 <width> <height> <source_file>[.fnt] [C-mode]\n");
      return( 3);
   }
   if( argc > 4){
      IsAsm = 0;  // rezim 'C'
   }
   FntWidth  = atoi( argv[ 1]);
   FntHeight = atoi( argv[ 2]);

   strcpy( infile, argv[ 3]);
   if_null( strrchr( infile, '.')){
      strcat( infile, ".fnt");
   }
   strcpy( outfile, infile);
   // odsekni priponu :
   p = strrchr( outfile, '.');
   *p = '\0';
   strcat( outfile, ".r90");
   if( freopen( infile, "r", stdin) == NULL){
       printf( "can't reopen stdin onto %s\n", infile);
       return( 8);
   }
   if_null( fo = fopen( outfile, "w")){
      printf( "Unable open output file : %s\n", outfile);
      return( 8);
   }
   fprintf( fo, "// Rotated font %s\n", infile);
   yyparse();
   fclose( fo);
   return( 0);
}  // main

