/***************************************************************************/
/*                                                                         */
/*     FNT_prs.h    Font compiler parser                                   */
/*     Version 0.0  (c) Vymos                                              */
/*                                                                         */
/***************************************************************************/

#ifndef __FNT_prs_H
#define __FNT_prs_H

#include <stdio.h>
#define FNT_MAX_TEXT 127

// Symboly :

# define S_INTEGER 257

typedef union {
   int    i;
   char   s[ FNT_MAX_TEXT + 1];
   double f;
} YYSTYPE;

extern YYSTYPE yylval;

// Globalni definice :

extern FILE *fo;
extern int  FntWidth;
extern int  FntHeight;
extern int  FntIndex;
extern int  CharIndex;
extern int  IsAsm;

int yyparse( void );

#endif

