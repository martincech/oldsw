//******************************************************************************
//
//   Main.cpp     Bat1Reader main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "../Serial/CrtLogger.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TPktAdapter;
   Device  = new TBatDevice;
   Device->Adapter = Adapter;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Adapter->Logger = new TCrtLogger( Crt);
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
int Cmd, Data;

   if( !Adapter->Send( USB_CMD_VERSION, 0)){
      Status() + "Unable send";
      return;
   }
   if( !Adapter->Receive( Cmd, Data)){
      Status() + "Unable receive";
      return;
   }
   Crt->printf( "Version %x.%02x", Data >> 8, Data & 0xFF);
   StatusOk();
} // BtnSendClick

//******************************************************************************
// Send data
//******************************************************************************

byte Buffer[ 10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

void __fastcall TMainForm::BtnSendDataClick(TObject *Sender)
{
int Cmd, Data;

   if( !Adapter->SendData( Buffer, sizeof(Buffer))){
      Status() + "Unable send";
      return;
   }
   if( !Adapter->Receive( Cmd, Data)){
      Status() + "Unable receive";
      return;
   }
   Crt->printf( "Address : %d", Data);
   StatusOk();
} // BtnSendData

//******************************************************************************
// Version
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   int Version;
   if( !Device->GetVersion( Version)){
      Status() + "Unable get version";
      return;
   }
   Crt->printf( "Version %x.%02x", Version >> 8, Version & 0xFF);
   StatusOk();
} // BtnVersionClick

//******************************************************************************
// Read
//******************************************************************************

byte Memory[ EEP_SIZE];

#define TEST_SIZE EEP_SIZE

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   Screen->Cursor = crHourGlass;
   if( !Device->ReadMemory( 0, Memory, TEST_SIZE)){
      Screen->Cursor = crDefault;
      Status() + "Unable read";
      return;
   }
   Screen->Cursor = crDefault;
   StatusOk();
} // BtnReadClick

//******************************************************************************
// Write
//******************************************************************************

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   Device->WriteVerification = CbEnableVerify->Checked;
   Screen->Cursor = crHourGlass;
   if( !Device->WriteMemory( 0, Memory, TEST_SIZE)){
      Screen->Cursor = crDefault;
      Status() + "Unable write";
      return;
   }
   Screen->Cursor = crDefault;
   StatusOk();
} // BtnWriteClick

