//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#include "BatDevice.h"
#include "../Crt/Crt.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnSend;
        TButton *BtnSendData;
        TButton *BtnVersion;
        TButton *BtnRead;
        TButton *BtnWrite;
        TCheckBox *CbEnableVerify;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnSendClick(TObject *Sender);
        void __fastcall BtnSendDataClick(TObject *Sender);
        void __fastcall BtnVersionClick(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
        void __fastcall BtnWriteClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt          *Crt;
   TPktAdapter   *Adapter;
   TBatDevice    *Device;
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
