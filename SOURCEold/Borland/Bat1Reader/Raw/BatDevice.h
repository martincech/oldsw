//******************************************************************************
//
//   BatDevice.cpp    Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************
#ifndef BatDeviceH
   #define BatDeviceH

#ifndef PktAdapterH
   #include "PktAdapter.h"
#endif

//******************************************************************************
// BatDevice
//******************************************************************************

class TBatDevice {
public :
   TBatDevice();
   // Constructor
   ~TBatDevice();
   // Destructor
   bool Check();
   // Check for device connected
   bool GetVersion( int &Version);
   // Get bat version
   bool ReadMemory( int Address, void *Buffer, int Size);
   // Read EEPROM
   bool WriteMemory( int Address, void *Buffer, int Size);
   // Write EEPROM data
   __property TPktAdapter *Adapter           = {read=FAdapter,write=FAdapter};
   __property bool         WriteVerification = {read=FWriteVerification,write=FWriteVerification};
   __property bool         IsAlive           = {read=GetAlive};
//------------------------------------------------------------------------------
protected :
   TPktAdapter         *FAdapter;           // Connection adapter
   bool                 FWriteVerification; // Enable verification on memory write

   bool SendCommand( int &Command, int &Data);
   // Send command with error recovery
   bool WriteFragment( int Address, void *Buffer, int Size);
   // Write fragment of data with recovery
   bool GetAlive();
   // Property IsAlive - adapter OK
}; // BatDevice

#endif


 