//******************************************************************************
//
//   Hardware.h    Parameters
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\Unisys\uni.h"             // zakladni datove typy

#define __packed                       // empty definition

#define EEP_PAGE_SIZE    64            // velikost stranky
#define EEP_SIZE      32768            // celkova kapacita

//-----------------------------------------------------------------------------
// File System
//-----------------------------------------------------------------------------

#define	FDIR_SIZE         10                     // Maximum number of files (directory entries)
#define FS_BLOCK_RECORDS  4                      // records per block
#define FAT_SIZE          250                    // FAT size (total blocks count)

#define FS_OFFSET         offsetof( TEeprom, Filesystem)  // filesystem offset (from EEP start)

//-----------------------------------------------------------------------------
// Statistika
//-----------------------------------------------------------------------------

typedef float TNumber;                 // zakladni datovy typ

//#define STAT_UNIFORMITY      1         // povoleni vypoctu uniformity
#define STAT_REAL_UNIFORMITY 1         // povoleni presneho vypoctu uniformity

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

typedef word THistogramCount;           // pocet vzorku
typedef long THistogramValue;           // hodnota vzorku, musi byt signed
#define HISTOGRAM_MIN_STEP    1         // Minimalni hodnota kroku v THistogramValue

#define HISTOGRAM_SLOTS       39        // pocet sloupcu - sude i liche cislo (max.254)

#define HISTOGRAM_GETDENOMINATOR 1
#define HISTOGRAM_SET_STEP       1
#define HISTOGRAM_EMPTY          1
#define HISTOGRAM_GET_VALUE      1

//-----------------------------------------------------------------------------
// Date/Time
//-----------------------------------------------------------------------------

#define DT_ENABLE_DST 1

//-----------------------------------------------------------------------------

//#include "MemoryDef.h"     // EEPROM offset calculations

#endif
