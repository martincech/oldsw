object MainForm: TMainForm
  Left = 212
  Top = 169
  Width = 802
  Height = 645
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainMemo: TMemo
    Left = 0
    Top = 88
    Width = 641
    Height = 497
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 599
    Width = 794
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnSend: TButton
    Left = 656
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 2
    OnClick = BtnSendClick
  end
  object BtnSendData: TButton
    Left = 656
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Send Data'
    TabOrder = 3
    OnClick = BtnSendDataClick
  end
  object BtnVersion: TButton
    Left = 656
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Version'
    TabOrder = 4
    OnClick = BtnVersionClick
  end
  object BtnRead: TButton
    Left = 656
    Top = 224
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 5
    OnClick = BtnReadClick
  end
  object BtnWrite: TButton
    Left = 656
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Write'
    TabOrder = 6
    OnClick = BtnWriteClick
  end
  object CbEnableVerify: TCheckBox
    Left = 648
    Top = 16
    Width = 97
    Height = 17
    Caption = 'Enable Verify'
    TabOrder = 7
  end
end
