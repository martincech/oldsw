object MainForm: TMainForm
  Left = 472
  Top = 206
  Width = 802
  Height = 645
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainMemo: TMemo
    Left = 0
    Top = 88
    Width = 641
    Height = 497
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 599
    Width = 794
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnSend: TButton
    Left = 656
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 2
    OnClick = BtnSendClick
  end
  object BtnSendData: TButton
    Left = 656
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Send Data'
    TabOrder = 3
    OnClick = BtnSendDataClick
  end
  object BtnVersion: TButton
    Left = 656
    Top = 184
    Width = 75
    Height = 17
    Caption = 'Version'
    TabOrder = 4
    OnClick = BtnVersionClick
  end
  object BtnRead: TButton
    Left = 656
    Top = 400
    Width = 75
    Height = 17
    Caption = 'Read'
    TabOrder = 5
    OnClick = BtnReadClick
  end
  object BtnWrite: TButton
    Left = 656
    Top = 424
    Width = 75
    Height = 17
    Caption = 'Write'
    TabOrder = 6
    OnClick = BtnWriteClick
  end
  object CbEnableVerify: TCheckBox
    Left = 648
    Top = 16
    Width = 97
    Height = 17
    Caption = 'Enable Verify'
    TabOrder = 7
  end
  object BtnStatus: TButton
    Left = 656
    Top = 208
    Width = 75
    Height = 17
    Caption = 'Status'
    TabOrder = 8
    OnClick = BtnStatusClick
  end
  object BtnPowerOff: TButton
    Left = 656
    Top = 232
    Width = 75
    Height = 17
    Caption = 'Power OFF'
    TabOrder = 9
    OnClick = BtnPowerOffClick
  end
  object BtnDownload: TButton
    Left = 656
    Top = 464
    Width = 75
    Height = 25
    Caption = 'Download'
    TabOrder = 10
    OnClick = BtnDownloadClick
  end
  object BtnUpload: TButton
    Left = 656
    Top = 496
    Width = 75
    Height = 25
    Caption = 'Upload'
    TabOrder = 11
    OnClick = BtnUploadClick
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'bat1'
    FileName = 'NONAME'
    Filter = 'Bat1 file|bat1'
    Left = 432
    Top = 8
  end
  object FileSaveDialog: TSaveDialog
    DefaultExt = 'bat1'
    FileName = '*.bat1'
    Filter = 'Bat1 file|bat1'
    Left = 480
    Top = 8
  end
end
