//******************************************************************************
//
//   Main.cpp     Bat1Reader main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Main.h"
#include "../Serial/CrtLogger.h"

#include "MemoryDef.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TPktAdapter;
   Device  = new TBatDevice;
   Device->Adapter = Adapter;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Adapter->Logger = new TCrtLogger( Crt);
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
int Cmd, Data;

   if( !Adapter->Send( USB_CMD_VERSION, 0)){
      Status() + "Unable send";
      return;
   }
   if( !Adapter->Receive( Cmd, Data)){
      Status() + "Unable receive";
      return;
   }
   Crt->printf( "Version %x.%02x", Data >> 8, Data & 0xFF);
   StatusOk();
} // BtnSendClick

//******************************************************************************
// Send data
//******************************************************************************

byte Buffer[ 10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};

void __fastcall TMainForm::BtnSendDataClick(TObject *Sender)
{
int Cmd, Data;

   if( !Adapter->SendData( Buffer, sizeof(Buffer))){
      Status() + "Unable send";
      return;
   }
   if( !Adapter->Receive( Cmd, Data)){
      Status() + "Unable receive";
      return;
   }
   Crt->printf( "Address : %d", Data);
   StatusOk();
} // BtnSendData

//******************************************************************************
// Version
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   int Version;
   if( !Device->GetVersion( Version)){
      Status() + "Unable get version";
      return;
   }
   Crt->printf( "Version %x.%02x", Version >> 8, Version & 0xFF);
   StatusOk();
} // BtnVersionClick

//******************************************************************************
// Status
//******************************************************************************

void __fastcall TMainForm::BtnStatusClick(TObject *Sender)
{
   bool PowerOn;
   if( !Device->GetStatus( PowerOn)){
      Status() + "Unable get status";
      return;
   }
   Crt->printf( "Power : %s", PowerOn ? "ON" : "OFF");
   StatusOk();
} // BtnStatusClick

//******************************************************************************
// Power OFF
//******************************************************************************

void __fastcall TMainForm::BtnPowerOffClick(TObject *Sender)
{
   if( !Device->PowerOff()){
      Status() + "Unable set power off";
      return;
   }
   Crt->printf( "Power : OFF");
   StatusOk();
} // BtnPowerOffClick

//******************************************************************************
// Read
//******************************************************************************

TEeprom Memory;

//#define READ_ADDRESS  0
//#define TEST_SIZE     sizeof( Memory)
//#define TEST_SIZE     offsetof( TEeprom, Spare)

#define READ_ADDRESS    offsetof( TEeprom, Spare)
#define TEST_SIZE       200

#define WRITE_SIZE      200
#define WRITE_ADDRESS   offsetof( TEeprom, Spare)

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   Screen->Cursor = crHourGlass;
   if( !Device->ReadMemory( READ_ADDRESS, &Memory, TEST_SIZE)){
      Screen->Cursor = crDefault;
      Status() + "Unable read";
      return;
   }
   Screen->Cursor = crDefault;
   StatusOk();
} // BtnReadClick

//******************************************************************************
// Write
//******************************************************************************


byte WriteBuffer[ WRITE_SIZE];

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   // prepare data :
   for( int i = 0; i < WRITE_SIZE; i++){
      WriteBuffer[ i] = (byte)i;
   }
   // write data :
   Device->WriteVerification = CbEnableVerify->Checked;
   Screen->Cursor = crHourGlass;
   if( !Device->WriteMemory( WRITE_ADDRESS, WriteBuffer, WRITE_SIZE)){
      Screen->Cursor = crDefault;
      Status() + "Unable write";
      return;
   }
   Screen->Cursor = crDefault;
   StatusOk();
} // BtnWriteClick

//******************************************************************************
// Dowload
//******************************************************************************

void __fastcall TMainForm::BtnDownloadClick(TObject *Sender)
{
   // select file :
   if( !FileOpenDialog->Execute()){
      return;
   }
   FILE *f;
   f = fopen( FileOpenDialog->FileName.c_str(), "wb");
   if( !f){
      char buff[ 120];
      sprintf( buff, "Unable open '%s'", FileOpenDialog->FileName.c_str());
      Application->MessageBox( buff, "Error", MB_OK);
      return;
   }
   // read EEPROM :
   Screen->Cursor = crHourGlass;
   if( !Device->ReadMemory( 0, &Memory, sizeof( Memory))){
      Screen->Cursor = crDefault;
      Status() + "Unable read";
      return;
   }
   Screen->Cursor = crDefault;
   // save to file :
   fwrite( &Memory, sizeof( Memory), 1, f);
   fclose( f);
   StatusOk();
} // BtnDownloadClick

//******************************************************************************
// Upload
//******************************************************************************

void __fastcall TMainForm::BtnUploadClick(TObject *Sender)
{
char buff[ 120];

   // select file :
   if( !FileSaveDialog->Execute()){
      return;
   }
   // read file :
   FILE *f;
   f = fopen( FileSaveDialog->FileName.c_str(), "rb");
   if( !f){
      sprintf( buff, "Unable open '%s'", FileOpenDialog->FileName.c_str());
      Application->MessageBox( buff, "Error", MB_OK);
      return;
   }
   if( fread( &Memory, sizeof( Memory), 1, f) != 1){
      sprintf( buff, "Unable read '%s'", FileOpenDialog->FileName.c_str());
      Application->MessageBox( buff, "Error", MB_OK);
      return;
   }
   fclose( f);
   // write EEPROM :
   Device->WriteVerification = CbEnableVerify->Checked;
   Screen->Cursor = crHourGlass;
   if( !Device->WriteMemory( 0, &Memory, sizeof( Memory))){
      Screen->Cursor = crDefault;
      Status() + "Unable write";
      return;
   }
   Screen->Cursor = crDefault;
   StatusOk();
} // BtnUploadClick

