//******************************************************************************
//
//   BatDevice.cpp    Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************
#ifndef BatDeviceH
   #define BatDeviceH

#ifndef PktAdapterH
   #include "PktAdapter.h"
#endif

//******************************************************************************
// BatDevice
//******************************************************************************

class TBatDevice {
public :
   TBatDevice();
   // Constructor
   ~TBatDevice();
   // Destructor
   bool Check();
   // Check for device connected
   bool GetVersion( int &Version);
   // Get bat version
   bool GetStatus( bool &PowerOn);
   // Get power status
   bool PowerOff();
   // Switch power off
   bool ReloadConfig();
   // Reload configuration by EEPROM
   bool GetTime( TTimestamp &Timestamp);
   // Get device time
   bool SetTime( TTimestamp Timestamp);
   // Set device time
   bool ReadMemory( int Address, void *Buffer, int Size);
   // Read EEPROM
   bool WriteMemory( int Address, void *Buffer, int Size);
   // Write EEPROM data
   bool DirectoryBegin( int &Count);
   // Read directory begin. Returns <Count> of directory items
   bool DirectoryNext( TUsbDirectoryItem *Item);
   // Read directory entry. Fills <Item>
   bool FileOpen( int Handle);
   // Open file by <Handle>
   bool FileClose();
   // Close Current file
   bool FileCreate( char *Name, char *Note, int Class, int &Handle);
   // Create file by <Name>, <Note>, <Class>. Returns <Handle>
   bool FileRead( void *Buffer, int Size);
   // Read file
   bool FileWrite( void *Buffer, int Size);
   // Write file
   bool FormatFilesystem();
   // Format filesystem

   __property TPktAdapter *Adapter           = {read=FAdapter,write=FAdapter};
   __property bool         WriteVerification = {read=FWriteVerification,write=FWriteVerification};
//------------------------------------------------------------------------------
protected :
   TPktAdapter         *FAdapter;           // Connection adapter
   bool                 FWriteVerification; // Enable verification on memory write

   bool SendCommand( int &Command, int &Data);
   // Send command with error recovery
   bool WriteFragment( int WriteCommand, void *Buffer, int Size);
   // Write fragment of data
}; // BatDevice

#endif


 