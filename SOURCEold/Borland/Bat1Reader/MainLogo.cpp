//******************************************************************************
//
//   Main.cpp     Bat1Reader main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainLogo.h"
#include "../Serial/CrtLogger.h"

#include "MemoryDef.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

TLogo Logo;          // Global logo buffer

// Color planes :
//            White   Light gray   Dark Gray Black
// Plane 0    0       1            0         1
// Plane 1    0       0            1         1

// Color transformation :
#define COLOR_BLACK      0x00000000
#define COLOR_MIDDLEGRAY 0x007F7F7F
#define COLOR_WHITE      0x00FFFFFF
#define COLOR_LIGHTGRAY  0x00555555
#define COLOR_DARKGRAY   0x00AAAAAA

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   MyBitmap = 0;
   Adapter  = new TPktAdapter;
   Device   = new TBatDevice;
   Device->Adapter = Adapter;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
   Adapter->Logger = Logger;
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Version
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   int Version;
   if( !Device->GetVersion( Version)){
      Status() + "Unable get version";
      return;
   }
   Crt->printf( "Version %x.%02x\n", Version >> 8, Version & 0xFF);
   StatusOk();
} // BtnVersionClick

//******************************************************************************
// Status
//******************************************************************************

void __fastcall TMainForm::BtnStatusClick(TObject *Sender)
{
   bool PowerOn;
   if( !Device->GetStatus( PowerOn)){
      Status() + "Unable get status";
      return;
   }
   Crt->printf( "Power : %s\n", PowerOn ? "ON" : "OFF");
   StatusOk();
} // BtnStatusClick

//******************************************************************************
// Power OFF
//******************************************************************************

void __fastcall TMainForm::BtnPowerOffClick(TObject *Sender)
{
   if( !Device->PowerOff()){
      Status() + "Unable set power off";
      return;
   }
   Crt->printf( "Power : OFF\n");
   StatusOk();
} // BtnPowerOffClick

//******************************************************************************
// Open
//******************************************************************************

void __fastcall TMainForm::BtnOpenClick(TObject *Sender)
{
   // select file :
   if( !FileOpenDialog->Execute()){
      return;
   }
   Image->Picture->LoadFromFile( FileOpenDialog->FileName);
} // BtnOpenClick

//******************************************************************************
// Dowload
//******************************************************************************

void __fastcall TMainForm::BtnDownloadClick(TObject *Sender)
{
   // read EEPROM :
   Screen->Cursor = crHourGlass;
   if( !Device->ReadMemory( offsetof( TEeprom, Logo), Logo, sizeof( Logo))){
      Screen->Cursor = crDefault;
      Status() + "Unable read";
      return;
   }
   Screen->Cursor = crDefault;
   Crt->printf( "Download OK\n");
   // create bitmap :
   if( MyBitmap){
      delete MyBitmap;
   }
   MyBitmap               = new Graphics::TBitmap;
   MyBitmap->Height       = LOGO_HEIGHT;
   MyBitmap->Width        = LOGO_WIDTH;
   Image->Picture->Bitmap = MyBitmap;
   // fill bitmap :
   for( int x = 0; x < LOGO_WIDTH; x++){
      for( int y = 0; y < LOGO_HEIGHT; y ++){
         int Yaddr = y / 8;
         int Ymask = 1 << (y % 8);
         TColor Color;
         if( Logo[ 0][ Yaddr][x] & Ymask){
            if( Logo[ 1][ Yaddr][x] & Ymask){
               Color = TColor( COLOR_BLACK);
            } else {
               Color = TColor( COLOR_LIGHTGRAY);
            }
         } else {
            if( Logo[ 1][ Yaddr][x] & Ymask){
               Color = TColor( COLOR_DARKGRAY);
            } else {
               Color = TColor( COLOR_WHITE);
            }
         }
         Image->Picture->Bitmap->Canvas->Pixels[ x][ y] = Color;
      }
   }
   StatusOk();
} // BtnDownloadClick

//******************************************************************************
// Upload
//******************************************************************************

void __fastcall TMainForm::BtnUploadClick(TObject *Sender)
{
   // prepare data :
   memset( Logo, 0, sizeof( Logo));
   for( int x = 0; x < LOGO_WIDTH; x++){
      for( int y = 0; y < LOGO_HEIGHT; y ++){
         TColor Color = Image->Picture->Bitmap->Canvas->Pixels[ x][ y];
         int Yaddr = y / 8;
         int Ymask = 1 << (y % 8);
         if( Color == COLOR_WHITE){
            continue;
         }
         // plane 0
         if( Color == COLOR_BLACK || Color >= COLOR_MIDDLEGRAY){ // light gray
            Logo[ 0][ Yaddr][x] |= Ymask;
         }
         // plane 1
         if( Color == COLOR_BLACK || Color <  COLOR_MIDDLEGRAY){ // dark gray
            Logo[ 1][ Yaddr][x] |= Ymask;
         }
      }
   }
   // write EEPROM :
   Screen->Cursor = crHourGlass;
   if( !Device->WriteMemory( offsetof( TEeprom, Logo), &Logo, sizeof( Logo))){
      Screen->Cursor = crDefault;
      Status() + "Unable write";
      return;
   }
   Screen->Cursor = crDefault;
   Crt->printf( "Upload OK\n");
   StatusOk();
} // BtnUploadClick

//******************************************************************************
//  Enable Logger
//******************************************************************************

void __fastcall TMainForm::BtnEnableLoggerClick(TObject *Sender)
{
   if( BtnEnableLogger->Checked){
      Adapter->Logger = Logger;
   } else {
      Adapter->Logger = 0;
   }
} // BtnEnableLoggerClick

