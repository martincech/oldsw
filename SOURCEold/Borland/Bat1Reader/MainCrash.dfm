object MainForm: TMainForm
  Left = 472
  Top = 206
  Width = 416
  Height = 261
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 Crash'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 215
    Width = 408
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnVersion: TButton
    Left = 16
    Top = 16
    Width = 89
    Height = 25
    Caption = 'Version'
    TabOrder = 1
    OnClick = BtnVersionClick
  end
  object BtnCrash: TButton
    Left = 16
    Top = 56
    Width = 89
    Height = 25
    Caption = 'Crash'
    TabOrder = 2
    OnClick = BtnCrashClick
  end
  object Panel1: TPanel
    Left = 144
    Top = 16
    Width = 217
    Height = 97
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Exception'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Date :'
    end
    object Label3: TLabel
      Left = 8
      Top = 40
      Width = 44
      Height = 13
      Caption = 'Address :'
    end
    object Label4: TLabel
      Left = 8
      Top = 56
      Width = 30
      Height = 13
      Caption = 'Type :'
    end
    object LblExceptionPpr: TLabel
      Left = 8
      Top = 80
      Width = 22
      Height = 13
      Caption = 'PPR'
    end
    object LblExceptionChg: TLabel
      Left = 48
      Top = 80
      Width = 23
      Height = 13
      Caption = 'CHG'
    end
    object LblExceptionUsbon: TLabel
      Left = 88
      Top = 80
      Width = 38
      Height = 13
      Caption = 'USBON'
    end
    object LblExceptionDate: TLabel
      Left = 64
      Top = 24
      Width = 6
      Height = 13
      Caption = '?'
    end
    object LblExceptionAddress: TLabel
      Left = 64
      Top = 40
      Width = 6
      Height = 13
      Caption = '?'
    end
    object LblExceptionType: TLabel
      Left = 64
      Top = 56
      Width = 6
      Height = 13
      Caption = '?'
    end
  end
  object Panel2: TPanel
    Left = 144
    Top = 128
    Width = 217
    Height = 73
    BevelOuter = bvLowered
    TabOrder = 4
    object Label5: TLabel
      Left = 4
      Top = 4
      Width = 65
      Height = 13
      Caption = 'Watch Dog'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 8
      Top = 24
      Width = 29
      Height = 13
      Caption = 'Date :'
    end
    object LblWatchDogPpr: TLabel
      Left = 8
      Top = 48
      Width = 22
      Height = 13
      Caption = 'PPR'
    end
    object LblWatchDogChg: TLabel
      Left = 48
      Top = 48
      Width = 23
      Height = 13
      Caption = 'CHG'
    end
    object LblWatchDogUsbon: TLabel
      Left = 88
      Top = 48
      Width = 38
      Height = 13
      Caption = 'USBON'
    end
    object LblWatchDogDate: TLabel
      Left = 64
      Top = 24
      Width = 6
      Height = 13
      Caption = '?'
    end
  end
  object BtnCleanup: TButton
    Left = 16
    Top = 96
    Width = 89
    Height = 25
    Caption = 'Cleanup'
    TabOrder = 5
    OnClick = BtnCleanupClick
  end
  object FileSaveDialog: TSaveDialog
    DefaultExt = 'bat1'
    FileName = '*.bat1'
    Filter = 'Bat1 file|bat1'
    Left = 344
  end
end
