//******************************************************************************
//
//   MainCrash.cpp  Bat1Crash main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainCrash.h"
#include "../Serial/CrtLogger.h"
#include "../Unisys/Dt.h"

#include "MemoryDef.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status( msg)  StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : " + msg
#define StatusOk()    Status( "OK")

// crash data :
static TExceptionInfo ExceptionInfo;
static TWatchDogInfo  WatchDogInfo;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TPktAdapter;
   Device  = new TBatDevice;
   Device->Adapter = Adapter;
} // TMainForm

//******************************************************************************
// Version
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   int Version;
   if( !Device->GetVersion( Version)){
      Status( "Unable get version");
      return;
   }
   AnsiString Txt;
   Txt.printf( "Version %x.%02x", Version >> 8, Version & 0xFF);
   Status( Txt);
} // BtnVersionClick

//******************************************************************************
// Crash
//******************************************************************************

void __fastcall TMainForm::BtnCrashClick(TObject *Sender)
{
/*
   int i;
   i = sizeof( TFsData);
   i = offsetof( TEeprom, Filesystem);
   i = EEP_SPARE;
*/
   if( !Device->ReadMemory( offsetof( TEeprom, Exception), &ExceptionInfo, sizeof( TExceptionInfo))){
      Status( "Unable read exception");
      return;
   }
   if( !Device->ReadMemory( offsetof( TEeprom, WatchDog),  &WatchDogInfo,  sizeof( TWatchDogInfo))){
      Status( "Unable read watchdog");
      return;
   }
   LblExceptionDate->Caption    = PrintTime( ExceptionInfo.Timestamp);
   LblExceptionAddress->Caption = AnsiString( ExceptionInfo.Address);
   LblExceptionType->Caption    = AnsiString( ExceptionInfo.Type);
   LblExceptionPpr->Visible     = ExceptionInfo.Status & CRASH_PPR;
   LblExceptionChg->Visible     = ExceptionInfo.Status & CRASH_CHG;
   LblExceptionUsbon->Visible   = ExceptionInfo.Status & CRASH_USBON;

   LblWatchDogDate->Caption    = PrintTime( WatchDogInfo.Timestamp);
   LblWatchDogPpr->Visible     = WatchDogInfo.Status & CRASH_PPR;
   LblWatchDogChg->Visible     = WatchDogInfo.Status & CRASH_CHG;
   LblWatchDogUsbon->Visible   = WatchDogInfo.Status & CRASH_USBON;
   StatusOk();
} // BtnCrashClick

//******************************************************************************
// Cleanup
//******************************************************************************

void __fastcall TMainForm::BtnCleanupClick(TObject *Sender)
{
   if( Application->MessageBox( "Really clear info ?", "Confirmation", MB_YESNO) != IDYES){
      return;
   }
   memset( &ExceptionInfo, 0xFF, sizeof( TExceptionInfo));
   memset( &WatchDogInfo,  0xFF, sizeof( TWatchDogInfo));
   if( !Device->WriteMemory( offsetof( TEeprom, Exception), &ExceptionInfo, sizeof( TExceptionInfo))){
      Status( "Unable write exception");
      return;
   }
   if( !Device->WriteMemory( offsetof( TEeprom, WatchDog),  &WatchDogInfo,  sizeof( TWatchDogInfo))){
      Status( "Unable read watchdog");
      return;
   }
   StatusOk();
} // BtnCleanupClick

//******************************************************************************
// Print Time
//******************************************************************************

AnsiString TMainForm::PrintTime( TTimestamp Timestamp)
// Print <Timestamp>
{
   TLocalTime Local;
   DtDecode( Timestamp, &Local);
   AnsiString Txt;
   Txt.printf( "%02d.%02d.%02d %02d:%02d:%02d", Local.Day, Local.Month, Local.Year,
                                                Local.Hour, Local.Min, Local.Sec);
   return( Txt);
} // DecodeTime

