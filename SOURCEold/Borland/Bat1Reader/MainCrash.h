//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainCrashH
#define MainCrashH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#include "BatDevice.h"
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar;
        TButton *BtnVersion;
        TSaveDialog *FileSaveDialog;
        TButton *BtnCrash;
        TPanel *Panel1;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *LblExceptionPpr;
        TLabel *LblExceptionChg;
        TLabel *LblExceptionUsbon;
        TLabel *LblExceptionDate;
        TLabel *LblExceptionAddress;
        TLabel *LblExceptionType;
        TPanel *Panel2;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *LblWatchDogPpr;
        TLabel *LblWatchDogChg;
        TLabel *LblWatchDogUsbon;
        TLabel *LblWatchDogDate;
        TButton *BtnCleanup;
        void __fastcall BtnVersionClick(TObject *Sender);
        void __fastcall BtnCrashClick(TObject *Sender);
        void __fastcall BtnCleanupClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TPktAdapter   *Adapter;
   TBatDevice    *Device;
   __fastcall TMainForm(TComponent* Owner);
   AnsiString PrintTime( TTimestamp Timestamp);
   // Print <Timestamp>
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
