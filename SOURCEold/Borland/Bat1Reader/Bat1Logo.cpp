//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Logo.res");
USEFORM("MainLogo.cpp", MainForm);
USEUNIT("PktAdapter.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("BatDevice.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
