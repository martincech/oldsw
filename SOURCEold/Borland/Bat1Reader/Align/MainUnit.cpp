//******************************************************************************
//
//   MainUnit.cpp Align demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#pragma hdrstop
#include "../Library/Unisys/uni.h"
#include <stdlib.h>

#define __packed

#pragma pack( push, 1)                    // byte alignment

typedef struct {
   byte One;
   byte Two;
   byte Three;
} __packed TStructOne;

typedef struct {
   byte  One;
   byte  Two;
   dword Three;
} __packed TStructTwo;

typedef struct {
   byte  Two;
   dword Three;
} __packed TStructThree;

typedef struct {
   dword One;
   byte Two;
} __packed TStructFour;

typedef struct {
   dword One;
   dword Two;
   byte  Three;
} __packed TStructFive;

typedef struct {
   word Two;
   dword Three;
   byte Four;
   word Five;
   dword Six;
   byte Seven;
} __packed TStructSix;

typedef struct {
   byte  One;
   byte  Two;
   byte  Three;
   byte  Four;
   dword Five;
} __packed TStructSeven;

typedef struct {
   byte One;
} __packed TStructEight;


#pragma pack( pop)                        // original alignment

typedef struct {
   dword One;
} TStructNine;

typedef struct {
   word One;
} TStructTen;

typedef struct {
   TStructOne One;
   TStructSix Two;
} TStructMixed;

typedef struct {
   TStructEight One;
   TStructEight Two;
   TStructEight Three;
} TStructByte;

typedef struct {
   TStructEight One;
   TStructTen   Two;
} TStructWord;

typedef struct {
   TStructEight One;
   TStructNine  Two;
} TStructDword;

//******************************************************************************
// Main
//******************************************************************************

int main()
{
int i;

   i = sizeof( TStructOne);        // 3

   i = sizeof( TStructTwo);
   i = offsetof( TStructTwo, Three);

   i = sizeof( TStructThree);
   i = offsetof( TStructThree, Three);

   i = sizeof( TStructFour);
   i = offsetof( TStructFour, Two);

   i = sizeof( TStructFive);
   i = offsetof( TStructFive, Two);
   i = offsetof( TStructFive, Three);

   i = sizeof( TStructSix);         // 0E
   i = offsetof( TStructSix, Two);
   i = offsetof( TStructSix, Three);
   i = offsetof( TStructSix, Four);
   i = offsetof( TStructSix, Five);
   i = offsetof( TStructSix, Six);

   i = sizeof( TStructSeven);
   i = offsetof( TStructSeven, Five);

   i = sizeof( TStructEight);

   i = sizeof( TStructMixed);             // 0x11
   i = offsetof( TStructMixed, Two);      // 0x3
   i = offsetof( TStructMixed, Two.Seven);// 0x10

   i = sizeof( TStructByte);              // 3
   i = offsetof( TStructByte, Two);       // 1
   i = offsetof( TStructByte, Three);     // 2

   i = sizeof( TStructWord);               // 4
   i = offsetof( TStructWord, Two);        // 2

   i = sizeof( TStructDword);              // 8
   i = offsetof( TStructDword, Two);       // 4
   return( 0);
} // main

