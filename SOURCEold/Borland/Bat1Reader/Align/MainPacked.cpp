//******************************************************************************
//                                                                            
//  Main.c         Align tests
//  Version 1.0    (c) VymOs
//
//******************************************************************************

#include "../Inc/Uni.h"
#include <stddef.h>           // macro offsetof

typedef struct {
   byte One;
   byte Two;
   byte Three;
} __packed TStructOne;

typedef struct {
   byte  One;
   byte  Two;
   dword Three;
} __packed TStructTwo;

typedef struct {
   byte  Two;
   dword Three;
} __packed TStructThree;

typedef struct {
   dword One;
   byte Two;
} __packed TStructFour;

typedef struct {
   dword One;
   dword Two;
   byte  Three;
} __packed TStructFive;

typedef struct {
   word Two;
   dword Three;
   byte Four;
   word Five;
   dword Six;
   byte Seven;
} __packed TStructSix;

typedef struct {
   byte  One;
   byte  Two;
   byte  Three;
   byte  Four;
   dword Five;
} __packed TStructSeven;

//------------------------------------------------------------------------------
//   Main
//------------------------------------------------------------------------------

int main( void)
{
int i;
TStructSix Six;
TStructFive Five;
TStructSeven Seven;

   i = sizeof( TStructOne);

   i = sizeof( TStructTwo);
   i = offsetof( TStructTwo, Three);

   i = sizeof( TStructThree);
   i = offsetof( TStructThree, Three);

   i = sizeof( TStructFour);
   i = offsetof( TStructFour, Two);

   i = sizeof( TStructFive);
   i = offsetof( TStructFive, Two);
   i = offsetof( TStructFive, Three);

   i = sizeof( TStructSix);
   i = offsetof( TStructSix, Two);
   i = offsetof( TStructSix, Three);
   i = offsetof( TStructSix, Four);
   i = offsetof( TStructSix, Five);
   i = offsetof( TStructSix, Six);

   i = Six.Two;
   i = Six.Three;
   i = Six.Four;
   i = Six.Five;
   i = Six.Six;

   i = Five.One;
   i = Five.Two;
   i = Five.Three;

   i = Seven.One;
   i = Seven.Five;
   return( 0);
} // main
