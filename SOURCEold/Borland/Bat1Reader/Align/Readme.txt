   Aligning


Borland :
- pamatuje si, zda struktura obsahuje 
  - jen byty  : sizeof je v modulu 1
  - word      : sizeof je v modulu 2
  - dword     : sizeof je v modulu 4
- pakovane struktury maji modul vzdy 1
- ve slozenych strukturach umistuje podstruktury
  na hranici jejich modulu. Velikost struktury
  je nasobkem nejvetsiho modulu
- pakovane slozene struktury nemaji zadne vyplne,
  velikost je s modulem 1


GNU
- velikost struktury je v modulu 4
- pakovane struktury maji modul 1
- ve slozenych strukturach umistuje podstruktury
  s modulem 4, pakovane podstruktury s modulem 1
  velikost je nasobkem 4
- pakovane slozene struktury nemaji zadne vyplne,
  velikost je s modulem 1
