//******************************************************************************
//
//   MainUnit.cpp Align demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#pragma hdrstop
#include "../Library/Unisys/uni.h"
#include <stdlib.h>

typedef struct {
   byte One;
   byte Two;
   byte Three;
} TStructOne;

typedef struct {
   byte  One;
   byte  Two;
   dword Three;
} TStructTwo;

typedef struct {
   byte  Two;
   dword Three;
} TStructThree;

typedef struct {
   dword One;
   byte Two;
} TStructFour;

typedef struct {
   dword One;
   byte Two;
   dword Three;
   byte Four;
} TStructFive;

typedef struct {
   word Two;
   dword Three;
   byte Four;
   word Five;
   dword Six;
} TStructSix;

typedef struct {
   word One;
   word Two;
   word Three;
} TStructSeven;

typedef struct {
   TStructOne One;
   TStructSeven Seven;
} TStructEight;

typedef struct {
   byte         One;
   TStructThree Two;
} TStructNine;

typedef struct {
   TStructSeven One;
   TStructSeven Two;
} TStructTen;

typedef struct {
   byte         One;
   byte         Two;
   TStructOne   Three;
} TStructEleven;

typedef struct {
   byte One;
} TStructByte;

typedef struct {
   word One;
} TStructWord;

typedef struct {
   dword One;
} TStructDword;

typedef struct {
   TStructByte  One;    // 0
   TStructWord  Two;    // 2
   TStructByte  Three;  // 4
   TStructDword Four;   // 8
   TStructByte  Five;   // 0x0C
} TStructMixed;         // size 0x10

//******************************************************************************
// Main
//******************************************************************************

int main()
{
int i;

   i = sizeof( TStructOne);         // 3

   i = sizeof( TStructTwo);
   i = offsetof( TStructTwo, Three);

   i = sizeof( TStructThree);
   i = offsetof( TStructThree, Three);

   i = sizeof( TStructFour);
   i = offsetof( TStructFour, Two);

   i = sizeof( TStructFive);
   i = offsetof( TStructFive, Two);
   i = offsetof( TStructFive, Three);
   i = offsetof( TStructFive, Four);

   i = sizeof( TStructSix);
   i = offsetof( TStructSix, Two);
   i = offsetof( TStructSix, Three);
   i = offsetof( TStructSix, Four);
   i = offsetof( TStructSix, Five);
   i = offsetof( TStructSix, Six);

   i = sizeof( TStructSeven);          // 6

   i = sizeof( TStructEight);          // 0A
   i = offsetof( TStructEight, Seven); // 4

   i = sizeof( TStructNine);           // 0C
   i = offsetof( TStructNine, Two);    // 4

   i = sizeof( TStructTen);            // 0C
   i = offsetof( TStructTen, Two);     // 6

   i = sizeof( TStructEleven);         // 5
   i = offsetof( TStructEleven, Three);// 2

   i = sizeof( TStructByte);           // 1
   i = sizeof( TStructWord);           // 2
   i = sizeof( TStructDword);          // 4

   i = sizeof( TStructMixed);           // 0x10
   i = offsetof( TStructMixed, Two);    // 2
   i = offsetof( TStructMixed, Three);  // 4
   i = offsetof( TStructMixed, Four);   // 8
   i = offsetof( TStructMixed, Five);   // 0x0C
} // main

