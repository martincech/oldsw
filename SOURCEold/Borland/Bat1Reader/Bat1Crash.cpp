//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Crash.res");
USEUNIT("PktAdapter.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("BatDevice.cpp");
USEFORM("MainCrash.cpp", MainForm);
USEUNIT("..\Library\Unisys\Dt.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1Crash";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
