//******************************************************************************
//
//   BatDevice.cpp    Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "BatDevice.h"
#include <time.h>
#include <stdio.h>

#pragma package(smart_init)

#define TRIALS 3         // number of trials

//******************************************************************************
// Constructor
//******************************************************************************

TBatDevice::TBatDevice()
// Constructor
{
   FAdapter   = 0;
   FWriteVerification = false;
} // TBatDevice

//******************************************************************************
// Destructor
//******************************************************************************

TBatDevice::~TBatDevice()
// Destructor
{
} // ~TBatDevice

//******************************************************************************
// Check
//******************************************************************************

bool TBatDevice::Check()
// Check for device
{
   int Version;
   return( GetVersion( Version));
} // Check

//******************************************************************************
// Version
//******************************************************************************

bool TBatDevice::GetVersion( int &Version)
// Get bat version
{
   int Cmd  = USB_CMD_VERSION;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   Version = Data;
   return( true);
} // GetVersion

//******************************************************************************
// Status
//******************************************************************************

bool TBatDevice::GetStatus( bool &PowerOn)
// Get power status
{
   int Cmd  = USB_CMD_GET_STATUS;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   PowerOn = Data;
   return( true);
} // GetStatus

//******************************************************************************
// Power off
//******************************************************************************

bool TBatDevice::PowerOff()
// Switch power off
{
   int Cmd  = USB_CMD_POWER_OFF;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // PowerOff

//******************************************************************************
// Reload Configuration
//******************************************************************************

bool TBatDevice::ReloadConfig()
// Reload configuration by EEPROM
{
   int Cmd  = USB_CMD_RELOAD;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // ReloadConfig

//******************************************************************************
// Get Time
//******************************************************************************

bool TBatDevice::GetTime( TTimestamp &Timestamp)
// Get device time
{
   int Cmd  = USB_CMD_GET_TIME;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   Timestamp = Data;
   return( true);
} // GetTime

//******************************************************************************
// Set Time
//******************************************************************************

bool TBatDevice::SetTime( TTimestamp Timestamp)
// Set device time
{
   int Cmd  = USB_CMD_SET_TIME;
   int Data = Timestamp;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // SetTime

//******************************************************************************
// Read Memory
//******************************************************************************

bool TBatDevice::ReadMemory( int Address, void *Buffer, int Size)
// Read EEPROM
{
   if( !FAdapter){
      return( false);
   }
   int Command = USB_CMD_ADDRESS;
   int Data    = Address;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   // Split into packets :
   int Length = Size > USB_MAX_DATA ? USB_MAX_DATA : Size;
   int TotalLength = Size;
   int RLength;
   int Offset      = 0;
   do {
      Data    = Length;
      Command = USB_CMD_READ;
      if( !SendCommand( Command, Data)){
         return( false);
      }
      if( Command != TPktAdapter::DATA_MESSAGE){
         return( false);
      }
      // check for length :
      Adapter->GetData( RLength, 0);
      if( RLength != Length){
         return( false);
      }
      // copy data :
      Adapter->GetData( RLength, &((char *)Buffer)[ Offset]);
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_MAX_DATA){
         Length = USB_MAX_DATA;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   if( Data != (Address + Size)){
      return( false);
   }
   return( true);
} // ReadMemory

//******************************************************************************
// Write Memory
//******************************************************************************

bool TBatDevice::WriteMemory( int Address, void *Buffer, int Size)
// Write EEPROM data
{
   if( !FAdapter){
      return( false);
   }
   // set address counter before write :
   int Command = USB_CMD_ADDRESS;
   int Data    = Address;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   // Split into packets :
   int Length = Size > USB_WRITE_SIZE ? USB_WRITE_SIZE : Size;
   int TotalLength = Size;
   int Offset      = 0;
   do {
      if( !WriteFragment( USB_CMD_WRITE, &((char *)Buffer)[ Offset], Length)){
         return( false);
      }
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_WRITE_SIZE){
         Length = USB_WRITE_SIZE;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   if( Data != (Address + Size)){
      return( false);
   }
   //----- verification :
   if( !FWriteVerification){
      return( true);          // without verification
   }
   byte *TmpBuffer  = new byte[ Size];
   if( !ReadMemory( Address, TmpBuffer, Size)){
      delete[] TmpBuffer;
      return( false);
   }
   if( !memequ( Buffer, TmpBuffer, Size)){
      delete[] TmpBuffer;
      return( false);        // different contents
   }
   delete[] TmpBuffer;
   return( true);
} // WriteMemory

//******************************************************************************
// Directory Begin
//******************************************************************************

bool TBatDevice::DirectoryBegin( int &Count)
// Read directory begin. Returns <Count> of directory items
{
   if( !Adapter){
      return( false);
   }
   int Cmd  = USB_CMD_DIRECTORY_BEGIN;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   Count = Data;
   return( true);
} // DirectoryBegin

//******************************************************************************
// Directory Next
//******************************************************************************

bool TBatDevice::DirectoryNext( TUsbDirectoryItem *Item)
// Read directory entry. Fills <Item>
{
   if( !Adapter){
      return( false);
   }
   int Command = USB_CMD_DIRECTORY_NEXT;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   if( Command != TPktAdapter::DATA_MESSAGE){
      return( false);
   }
   // check for length :
   int RLength;
   Adapter->GetData( RLength, 0);
   if( RLength != sizeof( TUsbDirectoryItem)){
      return( false);
   }
   // copy data :
   Adapter->GetData( RLength, Item);
   return( true);
} // DirectoryNext

//******************************************************************************
// File Open
//******************************************************************************

bool TBatDevice::FileOpen( int Handle)
// Open file by <Handle>
{
   if( !Adapter){
      return( false);
   }
   int Cmd  = USB_CMD_FILE_OPEN;
   int Data = Handle;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // FileOpen

//******************************************************************************
// File Close
//******************************************************************************

bool TBatDevice::FileClose()
// Close Current file
{
   if( !Adapter){
      return( false);
   }
   int Cmd  = USB_CMD_FILE_CLOSE;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // FileClose

//******************************************************************************
// File Create
//******************************************************************************

bool TBatDevice::FileCreate( char *Name, char *Note, int Class, int &Handle)
// Create file by <Name>, <Note>, <Class>. Returns <Handle>
{
   int RCommand, RData;
   TUsbCmdUnion  CmdUnion;
   // send create data :
   CmdUnion.Create.Cmd   = USB_CMD_FILE_CREATE;
   CmdUnion.Create.Class = Class;
   strcpy( CmdUnion.Create.Name, Name);
   strcpy( CmdUnion.Create.Note, Note);
   if( !Adapter->SendData( &CmdUnion, sizeof( TUsbCmdFileCreate))){
      return( false);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( false);
   }
   if( !(RCommand & USB_CMD_REPLY)){
      return( false);
   }
   if( (RCommand & ~USB_CMD_REPLY) != USB_CMD_FILE_CREATE){
      return( false);
   }
   Handle = RData;
   return( true);
} // FileCreate

//******************************************************************************
// File Read
//******************************************************************************

bool TBatDevice::FileRead( void *Buffer, int Size)
// Read file
{
   if( !Adapter){
      return( false);
   }
   // clear address counter before read :
   int Command = USB_CMD_ADDRESS;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   // Split into packets :
   int Length = Size > USB_MAX_DATA ? USB_MAX_DATA : Size;
   int TotalLength = Size;
   int RLength;
   int Offset      = 0;
   do {
      Data    = Length;
      Command = USB_CMD_FILE_READ;
      if( !SendCommand( Command, Data)){
         return( false);
      }
      if( Command != TPktAdapter::DATA_MESSAGE){
         return( false);
      }
      // check for length :
      Adapter->GetData( RLength, 0);
      if( RLength != Length){
         return( false);
      }
      // copy data :
      Adapter->GetData( RLength, &((char *)Buffer)[ Offset]);
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_MAX_DATA){
         Length = USB_MAX_DATA;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   if( Data != Size){
      return( false);
   }
   return( true);
} // FileRead

//******************************************************************************
// File Write
//******************************************************************************

bool TBatDevice::FileWrite( void *Buffer, int Size)
// Write file
{
   if( !Adapter){
      return( false);
   }
   // clear address counter before write :
   int Command = USB_CMD_ADDRESS;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   // Split into packets :
   int Length = Size > USB_WRITE_SIZE ? USB_WRITE_SIZE : Size;
   int TotalLength = Size;
   int Offset      = 0;
   do {
      if( !WriteFragment( USB_CMD_FILE_WRITE, &((char *)Buffer)[ Offset], Length)){
         return( false);
      }
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_WRITE_SIZE){
         Length = USB_WRITE_SIZE;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   Command = USB_CMD_GET_ADDRESS;
   Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   if( Data != Size){
      return( false);
   }
   return( true);
} // FileWrite

//******************************************************************************
// Format
//******************************************************************************

bool TBatDevice::FormatFilesystem()
// Format filesystem
{
   int Cmd  = USB_CMD_FORMAT;
   int Data = USB_FORMAT_MAGIC;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // FormatFilesystem

//------------------------------------------------------------------------------

//******************************************************************************
// Send command
//******************************************************************************

bool TBatDevice::SendCommand( int &Command, int &Data)
// Send command with error recovery
{
   if( !FAdapter){
      return( false);
   }
   int RCommand, RData;
   for( int i = 0; i < TRIALS; i++){
      if( !Adapter->Send( Command, Data)){
         continue;
      }
      if( !Adapter->Receive( RCommand, RData)){
         continue;
      }
      if( RCommand != TPktAdapter::DATA_MESSAGE &&
         (RCommand & ~USB_CMD_REPLY) != Command){
         return( false);
      }
      Data    = RData;
      Command = RCommand;
      return( true);
   }
   return( false);
} // SendCommand

//******************************************************************************
// Write Fragment
//******************************************************************************

bool TBatDevice::WriteFragment( int WriteCommand, void *Buffer, int Size)
// Write fragment of data
{
   int RCommand, RData;
   TUsbCmdUnion  CmdUnion;
   // send data :
   CmdUnion.Write.Cmd = WriteCommand;
   memcpy( CmdUnion.Write.Data, Buffer, Size);
   if( !Adapter->SendData( &CmdUnion, Size + USB_CMD_SIZE)){
      return( false);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( false);
   }
   if( !(RCommand & USB_CMD_REPLY)){
      return( false);
   }
   if( (RCommand & ~USB_CMD_REPLY) != WriteCommand){
      return( false);
   }
   return( true);
} // WriteFragment

