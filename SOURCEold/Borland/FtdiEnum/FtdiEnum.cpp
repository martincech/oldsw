//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("FtdiEnum.res");
USEFORM("Main.cpp", MainForm);
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("..\Library\CRT\Crt.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "FTDI Enumerator";
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
