//******************************************************************************
//
//   Main.cpp      FtdiEnum main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Main.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//---------------------------------------------------------------------------
#include "../Library/Serial/USB/FTD2XX.h"

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Crt = new TCrt( MainMemo);
} // TMainForm

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   this->Enabled  = false;             // disable main window
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // StartCom

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   this->Enabled  = true;              // enable main window
   Screen->Cursor = crDefault;         // normal cursor
} // StopComm

//******************************************************************************
//  Upgrade
//******************************************************************************

void __fastcall TMainForm::BtnRunClick(TObject *Sender)
{
   StartComm();
   Status() + "?";
   StatusBar->Refresh();
   Crt->Clear();
   MainMemo->Refresh();
   Crt->printf( "Run...\n");
   // Get devices count :
   DWORD      NumDevices;
   if_fterr( FT_ListDevices( &NumDevices, NULL, FT_LIST_NUMBER_ONLY)){
      Crt->printf( "FTDI list error\n");
      StopComm();
      StatusOk();
      return;
   }
   if( NumDevices == 0){
      Crt->printf( "No devices found\n");
      StopComm();
      StatusOk();
      return;
   }
   Crt->printf( "Devices count = %d\n", NumDevices);
   // FTDI data :
   char      DeviceName[ 256];     // device name
   FT_HANDLE Handle;               // raw FTDI handle
   // Buffers :
   FT_PROGRAM_DATA PgmData;
   char ManufacturerBuf[32];
   char ManufacturerIdBuf[16];
   char DescriptionBuf[64];
   char SerialNumberBuf[16];
   // Search for device :
   for( DWORD i = 0; i < NumDevices; i++){
      if_fterr( FT_ListDevices( (PVOID)i, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
         Crt->printf( "\nDevice %d : unable get name\n", i);
         continue;
      }
      // print device name :
      Crt->printf( "\nDevice %d : '%s'\n", i, DeviceName);
      // open device :
      if_fterr( FT_Open( i, &Handle)){
         Crt->printf( "   FTDI open error\n");
         continue;
      }
      // prepare buffers :
      PgmData.Signature1     = 0x00000000;
      PgmData.Signature2     = 0xffffffff;
      PgmData.Version        = 2;                  // FT232R extensions
      PgmData.Manufacturer   = ManufacturerBuf;
      PgmData.ManufacturerId = ManufacturerIdBuf;
      PgmData.Description    = DescriptionBuf;
      PgmData.SerialNumber   = SerialNumberBuf;
      // read EEPROM :
      if_fterr( FT_EE_Read( Handle, &PgmData)){
         FT_Close( Handle);
         Crt->printf( "   FTDI read EEPROM error\n");
         continue;
      }
      // print EEPROM data :
      Crt->printf( "   VID          : %04X\n", PgmData.VendorId);
      Crt->printf( "   PID          : %04X\n", PgmData.ProductId);
      Crt->printf( "   Description  : '%s'\n", PgmData.Description);
      Crt->printf( "   Manufacturer : '%s'\n", PgmData.Manufacturer);
      Crt->printf( "   Mfg id       : '%s'\n", PgmData.ManufacturerId);
      Crt->printf( "   Serial       : '%s'\n", PgmData.SerialNumber);
      // close device :
      FT_Close( Handle);
   }
   StopComm();
   StatusOk();
} // BtnStartClick

