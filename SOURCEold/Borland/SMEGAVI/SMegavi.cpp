//******************************************************************************
//
//   SMegavi.cpp   MEGAVI simulator adapter
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "SMegavi.h"
#include "uni.h"

#include "../Library/Serial/Uart/ComUart.h"
#include "../Library/Serial/Usb/UsbUart.h"

#define USB_NAME        "USB"                  // USB adapter identification
#define USB_DEVICE_NAME "MEGAVI adapter"       // USB adapter name

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TSMegavi::TSMegavi()
// Constructor
{
   FPort   = new TComUart;
   FLogger = 0;

   FRxTimeout               = 2000;
   FPortName                = "";
   // default parameters :
   FParameters.BaudRate     = 57600;
   FParameters.DataBits     = 8;
   FParameters.StopBits     = 10;
   FParameters.Parity       = TUart::NO_PARITY;
   FParameters.Handshake    = TUart::NO_HANDSHAKE;
   FUsbDevice               = USB_DEVICE_NAME;
} // TSMegavi

//******************************************************************************
// Destructor
//******************************************************************************

TSMegavi::~TSMegavi()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TSMegavi

//******************************************************************************
// Connect
//******************************************************************************

bool TSMegavi::Connect( TName Port)
// Connect via <Port>
{
   // disconnect first :
   Disconnect();
   // remember name only :
   FPortName = Port;
   CheckConnect();        // try open, don't check success
   return( true);
} // Connect

//******************************************************************************
// Send
//******************************************************************************

bool TSMegavi::Send( unsigned Id, void *Data, int Size)
// Send message
{
   if( !CheckConnect()){
      return( false);
   }
   Buffer[ SMEGAVI_LEADER_OFFSET] = SMEGAVI_LEADER_CHAR;
   Buffer[ SMEGAVI_SIZE1_OFFSET]  = (byte)Size + 1;   // with Id
   Buffer[ SMEGAVI_SIZE2_OFFSET]  = (byte)Size + 1;   // with Id
   Buffer[ SMEGAVI_ID_OFFSET]     = (byte)Id;
   memcpy( &Buffer[ SMEGAVI_DATA_OFFSET], Data, Size);
   Buffer[ Size + SMEGAVI_CRC_OFFSET] = CalcCrc( Size);

   int TotalSize = Size + SMEGAVI_FRAME_SIZE;
   FPort->Flush();
   if( FPort->Write( Buffer, TotalSize) != TotalSize){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   // packet data :
   RwdTx( &Buffer, TotalSize);
   return( true);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TSMegavi::Receive( unsigned *Id, void *Data, int *Size)
// Receive message
{
int ReplySize, DataSize;

   if( !CheckConnect()){
      return( false);
   }
   if( FPort->Read( Buffer, 1) != 1){
      return( false);
   }
   if( Buffer[ SMEGAVI_LEADER_OFFSET] != SMEGAVI_LEADER_CHAR){
      RwdGarbage( Buffer, 1);
      return( false);
   }
   // size fields :
   DataSize  = 2;
   ReplySize = FPort->Read( &Buffer[ SMEGAVI_SIZE1_OFFSET], DataSize);
   if(  ReplySize != DataSize){
      RwdGarbage( Buffer, ReplySize + 1);
      return( false);
   }
   if( Buffer[ SMEGAVI_SIZE1_OFFSET] != Buffer[ SMEGAVI_SIZE2_OFFSET]){
      RwdGarbage( Buffer, ReplySize + 1);
      return( false);
   }
   // read data inclusive CRC :
   DataSize  = Buffer[ SMEGAVI_SIZE1_OFFSET];     // with ID
   ReplySize = FPort->Read( &Buffer[ SMEGAVI_ID_OFFSET], DataSize + 1); // with ID & CRC
   if(  ReplySize != DataSize + 1){
      RwdGarbage( Buffer, ReplySize + 3);
      return( false);
   }
   ReplySize += 3;                    // inclusive leader, Size1, 2
   DataSize--;                        // payload data size (without ID)
   if( Buffer[ DataSize + SMEGAVI_CRC_OFFSET] != CalcCrc( DataSize)){
      RwdGarbage( Buffer, ReplySize);
      return( false);
   }
   RwdRx( Buffer, ReplySize);
   memcpy( Data, &Buffer[ SMEGAVI_DATA_OFFSET], DataSize);
   *Id   = Buffer[ SMEGAVI_ID_OFFSET];
   *Size = DataSize;
   return( true);
} // Receive

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Identifier
//******************************************************************************

TIdentifier TSMegavi::GetIdentifier()
// GetIdentifier
{
   if( !FPort){
      return( INVALID_IDENTIFIER);
   }
   return( FPort->Identifier);
} // GetIdentifier

//******************************************************************************
// Property Name
//******************************************************************************

TName TSMegavi::GetName()
// Get device name
{
   if( !FPort){
      return("No adapter");
   }
   return( FPort->Name);
} // GetName

//******************************************************************************
// Property IsOpen
//******************************************************************************

bool  TSMegavi::GetIsOpen()
// Check if device is opened
{
   if( !FPort){
      return( false);
   }
   return( FPort->IsOpen);
} // GetIsOpen

//******************************************************************************
// Check Connection
//******************************************************************************

bool TSMegavi::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( true);
   }
   if( FPort){
      IERROR;                          // always opened !
   }
   TIdentifier Identifier;
   TUart    *Uart;
   TUsbUart *Usb = 0;
   if( PortName == USB_NAME){
      // USB setup
      Usb = new TUsbUart;
      if( !Usb->Locate( FUsbDevice, Identifier)){
         delete Usb;
         return( false);
      }
      Uart = Usb;
   } else {
      // COM setup :
      TComUart *Com = new TComUart;
      if( !Com->Locate( PortName, Identifier)){
         delete Com;
         return( false);
      }
      Uart = Com;
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   // common init :
   Uart->SetParameters( FParameters);
   Uart->SetRxWait( FRxTimeout, 0);
   Uart->Flush();
   FPort = Uart;
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TSMegavi::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

//******************************************************************************
// CRC
//******************************************************************************

byte TSMegavi::CalcCrc( int Size)
// Calculate CRC
{
byte *Data;

   Data     = &Buffer[ SMEGAVI_SIZE1_OFFSET];
   byte Crc = 0;
   Size    += 3;                  // add Size1, 2, Id
   for( int i = 0; i < Size; i++){
      Crc += *Data;
      Data++;
   }
   return( ~Crc);
} // CalcCrc

