//******************************************************************************
//
//   MBridgeDef.h  Bat2 MEGAVI bridge definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef MBridgeDefH
   #define MBridgeDefH

#pragma pack( push, 1)                             // byte alignment
   #include "../AVR8/Inc/Megavi/MegaviUnion.h"     // protocol definition
#pragma pack( pop)                                 // original alignment

#endif
