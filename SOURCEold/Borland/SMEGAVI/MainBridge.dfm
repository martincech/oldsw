object MainForm: TMainForm
  Left = 352
  Top = 161
  AutoScroll = False
  Caption = 'MBridge Test'
  ClientHeight = 547
  ClientWidth = 879
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 594
    Top = 32
    Width = 25
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Port :'
  end
  object Label3: TLabel
    Left = 3
    Top = 8
    Width = 15
    Height = 13
    Caption = 'Id :'
  end
  object Label2: TLabel
    Left = 760
    Top = 272
    Width = 24
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Slot :'
  end
  object Label4: TLabel
    Left = 20
    Top = 36
    Width = 29
    Height = 13
    Caption = 'SMS :'
  end
  object Label5: TLabel
    Left = 80
    Top = 8
    Width = 63
    Height = 13
    Caption = 'SMS Target :'
  end
  object MainPort: TComboBox
    Left = 634
    Top = 29
    Width = 98
    Height = 21
    Style = csDropDownList
    Anchors = [akTop, akRight]
    ItemHeight = 13
    TabOrder = 2
    OnChange = MainPortChange
    Items.Strings = (
      'USB'
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15'
      'COM16'
      'COM17'
      'COM18'
      'COM19'
      'COM20'
      'COM21'
      'COM22'
      'COM23'
      'COM24'
      'COM25'
      'COM26'
      'COM27'
      'COM28'
      'COM29'
      'COM30')
  end
  object MainSetup: TButton
    Left = 746
    Top = 28
    Width = 76
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Setup'
    TabOrder = 3
    OnClick = MainSetupClick
  end
  object MainMemo: TMemo
    Left = 0
    Top = 64
    Width = 732
    Height = 465
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 528
    Width = 879
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object MainVersion: TButton
    Left = 750
    Top = 70
    Width = 87
    Height = 27
    Anchors = [akTop, akRight]
    Caption = 'Version'
    TabOrder = 1
    OnClick = MainVersionClick
  end
  object MainId: TEdit
    Left = 19
    Top = 8
    Width = 33
    Height = 21
    TabOrder = 0
    Text = '10'
  end
  object MainCycle: TCheckBox
    Left = 636
    Top = 8
    Width = 97
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Cycle'
    TabOrder = 6
  end
  object MainMaleData: TButton
    Left = 752
    Top = 112
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Male Data'
    TabOrder = 7
    OnClick = MainMaleDataClick
  end
  object MainSms: TButton
    Left = 752
    Top = 192
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'SMS Send'
    TabOrder = 8
    OnClick = MainSmsClick
  end
  object MainSmsStatus: TButton
    Left = 752
    Top = 232
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'SMS Status'
    TabOrder = 9
    OnClick = MainSmsStatusClick
  end
  object EdtSlot: TEdit
    Left = 752
    Top = 288
    Width = 121
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 10
    Text = '1'
  end
  object MainFemaleData: TButton
    Left = 752
    Top = 152
    Width = 89
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Female Data'
    TabOrder = 11
    OnClick = MainFemaleDataClick
  end
  object MainSmsText: TEdit
    Left = 64
    Top = 32
    Width = 521
    Height = 21
    MaxLength = 160
    TabOrder = 12
    Text = 'Hello from Bat2 MEGAVI'
  end
  object MainTarget: TEdit
    Left = 168
    Top = 8
    Width = 121
    Height = 21
    MaxLength = 1
    TabOrder = 13
    Text = '0'
  end
end
