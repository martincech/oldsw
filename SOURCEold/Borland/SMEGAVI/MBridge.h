//******************************************************************************
//
//   MBridge.h     Bat2 MEGAVI bridge 
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef MBridgeH
   #define MBridgeH

#ifndef SMegaviH
   #include "SMegavi.h"
#endif

#ifndef MBridgeDefH
   #include "MBridgeDef.h"
#endif

#define MBRIDGE_REPLY_SIZE  1024

//******************************************************************************
// TMBridge
//******************************************************************************

class TMBridge {
public :
   TMBridge();
   // Constructor
   ~TMBridge();
   // Destructor

   bool Connect( TName Port);
   // Connect via <Port>

   bool VersionGet( unsigned *Major, unsigned *Minor);
   // Get device version

   bool MaleDataGet( char *SmsRequest, TBat2Data *Data);
   // Get device male <Data> and <SmsRequest> code

   bool FemaleDataGet( char *SmsRequest, TBat2Data *Data);
   // Get device female <Data> and <SmsRequest> code

   bool SmsSend( char Target, char *Text, unsigned *Slot);
   // Send SMS <Text> to <Target>, returns <Slot>

   bool SmsStatusGet( unsigned Slot, unsigned *Status);
   // Get SMS <Slot> <Status>

   __property byte      MegaviId          = {read=FMegaviId, write=FMegaviId};
   __property TSMegavi *Adapter           = {read=FAdapter};
//------------------------------------------------------------------------------
protected :
   TSMegavi *FAdapter;         // MEGAVI protocol adapter
   byte      FMegaviId;        // MEGAVI ID - device address

   byte      FBuffer[ MBRIDGE_REPLY_SIZE];  // reply buffer

   bool CommandSend( void *Command, int CommandSize, int ReplySize);
   // Send <Command> with <CommandSize> to buffer with expected <ReplySize>

}; // TMBridge

#endif

