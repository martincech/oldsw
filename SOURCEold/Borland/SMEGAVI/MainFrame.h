//******************************************************************************
//
//   MainFrame.h  MEGAVI simulator
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainFrameH
#define MainFrameH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Library/Crt/Crt.h"
#include "SMegavi.h"

#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TComboBox *MainPort;
   TLabel *Label1;
   TButton *MainSetup;
   TMemo *MainMemo;
   TStatusBar *StatusBar1;
   TButton *MainSend;
   TLabel *Label4;
   TEdit *MainData;
   TLabel *Label2;
   TEdit *MainSize;
   TCheckBox *MainCycle;
   TLabel *Label3;
   TEdit *MainId;
   void __fastcall Create(TObject *Sender);
   void __fastcall MainSetupClick(TObject *Sender);
   void __fastcall MainPortChange(TObject *Sender);
   void __fastcall MainSendClick(TObject *Sender);
   void __fastcall MemoResize(TObject *Sender);
private:	// User declarations
   TCrt       *Crt;
   TSMegavi   *Adapter;

   void __fastcall Restart();
   // Open port
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------

#endif
