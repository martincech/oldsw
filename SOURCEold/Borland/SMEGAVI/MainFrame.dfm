object MainForm: TMainForm
  Left = 375
  Top = 251
  AutoScroll = False
  Caption = 'SMegavi Frame'
  ClientHeight = 547
  ClientWidth = 761
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 534
    Top = 16
    Width = 25
    Height = 13
    Caption = 'Port :'
  end
  object Label4: TLabel
    Left = 115
    Top = 16
    Width = 60
    Height = 13
    Caption = 'Data Offset :'
  end
  object Label2: TLabel
    Left = 267
    Top = 16
    Width = 26
    Height = 13
    Caption = 'Size :'
  end
  object Label3: TLabel
    Left = 3
    Top = 16
    Width = 15
    Height = 13
    Caption = 'Id :'
  end
  object MainPort: TComboBox
    Left = 566
    Top = 13
    Width = 98
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    OnChange = MainPortChange
    Items.Strings = (
      'USB'
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15'
      'COM16'
      'COM17'
      'COM18'
      'COM19'
      'COM20'
      'COM21'
      'COM22'
      'COM23'
      'COM24'
      'COM25'
      'COM26'
      'COM27'
      'COM28'
      'COM29'
      'COM30')
  end
  object MainSetup: TButton
    Left = 670
    Top = 12
    Width = 76
    Height = 21
    Caption = 'Setup'
    TabOrder = 3
    OnClick = MainSetupClick
  end
  object MainMemo: TMemo
    Left = 0
    Top = 64
    Width = 646
    Height = 464
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 4
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 528
    Width = 761
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object MainSend: TButton
    Left = 432
    Top = 14
    Width = 76
    Height = 21
    Caption = 'Send'
    TabOrder = 1
    OnClick = MainSendClick
  end
  object MainData: TEdit
    Left = 179
    Top = 14
    Width = 73
    Height = 21
    TabOrder = 0
    Text = '0'
  end
  object MainSize: TEdit
    Left = 331
    Top = 14
    Width = 73
    Height = 21
    TabOrder = 6
    Text = '20'
  end
  object MainCycle: TCheckBox
    Left = 432
    Top = 40
    Width = 97
    Height = 17
    Caption = 'Cycle'
    TabOrder = 7
  end
  object MainId: TEdit
    Left = 67
    Top = 14
    Width = 33
    Height = 21
    TabOrder = 8
    Text = '10'
  end
end
