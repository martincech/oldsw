//******************************************************************************
//
//   MBridge.cpp    Bat2 MEGAVI bridge 
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MBridge.h"
#include "uni.h"

#define MEGAVI_ID_DEFAULT  0x0A

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------

TMBridge::TMBridge()
// Constructor
{
   FAdapter  = new TSMegavi();
   FMegaviId = MEGAVI_ID_DEFAULT;
} // TMBridge

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------

TMBridge::~TMBridge()
// Destructor
{
   if( FAdapter){
      delete FAdapter;
   }
} // ~TMBridge

//------------------------------------------------------------------------------
// Connect
//------------------------------------------------------------------------------

bool TMBridge::Connect( TName Port)
// Connect via <Port>
{
   return( FAdapter->Connect( Port));
} // Connect

//------------------------------------------------------------------------------
// Version
//------------------------------------------------------------------------------

bool TMBridge::VersionGet( unsigned *Major, unsigned *Minor)
// Get device version
{
TBat2MegaviCommand       Cmd;
TBat2MegaviReplyVersion *Reply;

   Cmd.Command = BAT2_MEGAVI_COMMAND_VERSION;
   if( !CommandSend( &Cmd, sizeof( TBat2MegaviCommand), sizeof( TBat2MegaviReplyVersion))){
      return( false);
   }
   Reply  = (TBat2MegaviReplyVersion *)FBuffer;
   *Major = Reply->Major;
   *Minor = Reply->Minor;
   return( true);
} // VersionGet

//------------------------------------------------------------------------------
// Get Male Data
//------------------------------------------------------------------------------

bool TMBridge::MaleDataGet( char *SmsRequest, TBat2Data *Data)
// Get device male <Data> and <SmsRequest> code
{
TBat2MegaviCommand    Cmd;
TBat2MegaviReplyData *Reply;

   Cmd.Command = BAT2_MEGAVI_COMMAND_MALE_DATA_GET;
   if( !CommandSend( &Cmd, sizeof( TBat2MegaviCommand), sizeof( TBat2MegaviReplyData))){
      return( false);
   }
   Reply  = (TBat2MegaviReplyData *)FBuffer;
   *SmsRequest = Reply->SmsRequest;
   memcpy( Data, &Reply->Data, sizeof( TBat2Data));
   return( true);
} // MaleDataGet

//------------------------------------------------------------------------------
// Get Female Data
//------------------------------------------------------------------------------

bool TMBridge::FemaleDataGet( char *SmsRequest, TBat2Data *Data)
// Get device female <Data> and <SmsRequest> code
{
TBat2MegaviCommand    Cmd;
TBat2MegaviReplyData *Reply;

   Cmd.Command = BAT2_MEGAVI_COMMAND_FEMALE_DATA_GET;
   if( !CommandSend( &Cmd, sizeof( TBat2MegaviCommand), sizeof( TBat2MegaviReplyData))){
      return( false);
   }
   Reply  = (TBat2MegaviReplyData *)FBuffer;
   *SmsRequest = Reply->SmsRequest;
   memcpy( Data, &Reply->Data, sizeof( TBat2Data));
   return( true);
} // FemaleDataGet

//------------------------------------------------------------------------------
// SMS send
//------------------------------------------------------------------------------

bool TMBridge::SmsSend( char Target, char *Text, unsigned *Slot)
// Send SMS <Text> to <Target>, returns <Slot>
{
TBat2MegaviCommandSmsSend  Cmd;
TBat2MegaviReplySmsSend   *Reply;

   Cmd.Command = BAT2_MEGAVI_COMMAND_SMS_SEND;
   Cmd.Target  = Target;
   memset( Cmd.Text, 0, BAT2_SMS_SIZE);
   strncpy( Cmd.Text, Text, BAT2_SMS_SIZE);
   if( !CommandSend( &Cmd, sizeof( TBat2MegaviCommandSmsSend), sizeof( TBat2MegaviReplySmsSend))){
      return( false);
   }
   Reply  = (TBat2MegaviReplySmsSend *)FBuffer;
   *Slot = Reply->Slot;
   return( true);
} // SmsSend

//------------------------------------------------------------------------------
// SMS status
//------------------------------------------------------------------------------

bool TMBridge::SmsStatusGet( unsigned Slot, unsigned *Status)
// Get SMS <Slot> <Status>
{
TBat2MegaviCommandSmsStatusGet  Cmd;
TBat2MegaviReplySmsStatus      *Reply;

   Cmd.Command = BAT2_MEGAVI_COMMAND_SMS_STATUS_GET;
   Cmd.Slot = (byte)Slot;
   if( !CommandSend( &Cmd, sizeof( TBat2MegaviCommandSmsStatusGet), sizeof( TBat2MegaviReplySmsStatus))){
      return( false);
   }
   Reply   = (TBat2MegaviReplySmsStatus *)FBuffer;
   *Status = Reply->Status;
   return( true);
} // SmsStatusGet

//******************************************************************************

//------------------------------------------------------------------------------
// Send Command
//------------------------------------------------------------------------------

bool TMBridge::CommandSend( void *Command, int CommandSize, int ReplySize)
// Send <Command> with <CommandSize> to buffer with expected <ReplySize>
{
   if( !FAdapter->Send( FMegaviId, Command, CommandSize)){
      return( false);
   }
   unsigned RId;
   int      RSize;
   if( !FAdapter->Receive( &RId, FBuffer, &RSize)){
      return( false);
   }
   if( (byte)RId != FMegaviId){
      return( false);
   }
   if( RSize != ReplySize){
      return( false);
   }
   return( true);
} // CommandSend

