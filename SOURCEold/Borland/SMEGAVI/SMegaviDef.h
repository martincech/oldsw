//******************************************************************************
//
//   SMegaviDef.h  MEGAVI simulator definitions
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef SMegaviDefH
   #define SMegaviDefH

#pragma pack( push, 1)                             // byte alignment
   #include "../AVR8/Inc/Megavi/SMegaviDef.h"      // protocol definition
#pragma pack( pop)                                 // original alignment

#endif
