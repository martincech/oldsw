//******************************************************************************
//
//   Filter.cpp   Filtering
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include <math.h>         // abs
#include "Filter.h"
#include "Data.h"

TDataRecord DataRecord;
static int  StableCounter;

TWeight Fifo[ FILTER_MAX_AVERAGING];
int     FifoPointer;

// Local functions :
static TWeight ReadConversion( void);
// Read conversion by A/D convertor

void FifoInitialize( void);
// Initialize FIFO data

TWeight FifoAverage( void);
// Calculate average of the contents

void FifoPut( TWeight Weight);
// Put to FIFO

//******************************************************************************
// Initialize
//******************************************************************************

void FilterInitialize( void)
// Initialize filter data
{
   FifoInitialize();
   DataRecord.Status = FILTER_IDLE;
   StableCounter = 0;
} // FilterInitialize

//******************************************************************************
// Sample
//******************************************************************************

void NextSample( void)
// Process next sample
{
   DataRecord.RawWeight = ReadConversion();           // read sample
   FifoPut( DataRecord.RawWeight);                    // save sample
   // filtering :
   DataRecord.LowPass  = FifoAverage();
   DataRecord.HighPass = DataRecord.RawWeight - DataRecord.LowPass;
   // check step :
   DataRecord.Status = FILTER_IDLE;
   if( DataRecord.HighPass > 0){
      // positive value
      if( DataRecord.HighPass > DataRecord.StepRange){
         DataRecord.Status = FILTER_STEP_UP;
      }
   } else {
      // negative value
      if( DataRecord.HighPass < -DataRecord.StepRange){
         DataRecord.Status = FILTER_STEP_DOWN;
      }
   }
   // check for stability :
   TWeight AbsValue = fabs( DataRecord.HighPass);
   if( AbsValue < DataRecord.StableRange){
      StableCounter++;
   } else {
      StableCounter = 0;
   }
   if( StableCounter >= DataRecord.StableWindow){
      DataRecord.Status = FILTER_STABLE;
      DataRecord.Weight = DataRecord.LowPass;
   }
} // NextSample

//******************************************************************************
// Read conversion
//******************************************************************************

static TWeight ReadConversion( void)
// Read conversion by A/D convertor
{
   TWeight Weight = BatData->Weight;
   BatData->Next();
   return( Weight);
} // ReadConversion

//******************************************************************************
// Fifo initialize
//******************************************************************************

void FifoInitialize( void)
// Initialize FIFO data
{
   for( int i = 0; i < DataRecord.AveragingWindow; i++){
      Fifo[ i] = BatData->Weight;       // fill with first record value
   }
   FifoPointer = 0;
} // FifoInitialize

//******************************************************************************
// Fifo average
//******************************************************************************

TWeight FifoAverage( void)
// Calculate average of the contents
{
   TLongWeight Sum = 0;
   for( int i = 0; i < DataRecord.AveragingWindow; i++){
      Sum += Fifo[ i];
   }
   return( Sum / DataRecord.AveragingWindow);
} // FifoAverage

//******************************************************************************
// Fifo put
//******************************************************************************

void FifoPut( TWeight Weight)
// Put to FIFO
{
   Fifo[ FifoPointer++] = Weight;
   if( FifoPointer >= DataRecord.AveragingWindow){
      FifoPointer = 0;
   }
} // FifoPut
