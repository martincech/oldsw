//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TDBChart *Graph;
        TLineSeries *LowPassSeries;
        TLineSeries *HighPassSeries;
        TLineSeries *WeightSeries;
        TLineSeries *StepSeries;
        TLineSeries *StableSeries;
        TLineSeries *StableWeightSeries;
        TLineSeries *LastStableWeightSeries;
        TPanel *Panel1;
        TLabel *Label3;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *LblFileName;
        TLabel *Label13;
        TEdit *EdtAveraging;
        TButton *BtnRedraw;
        TEdit *EdtWidth;
        TEdit *EdtOffset;
        TButton *BtnBackward;
        TButton *BtnForward;
        TButton *BtnFirst;
        TEdit *EdtStepRange;
        TEdit *EdtStableRange;
        TEdit *EdtStable;
        TCheckBox *CbxStepDown;
        TButton *BtnFile;
        TCheckBox *CbxStepUp;
        TEdit *EdtPrefilter;
        TOpenDialog *FileOpenDialog;
        void __fastcall BtnRedrawClick(TObject *Sender);
        void __fastcall BtnBackwardClick(TObject *Sender);
        void __fastcall BtnForwardClick(TObject *Sender);
        void __fastcall BtnFirstClick(TObject *Sender);
        void __fastcall BtnFileClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
