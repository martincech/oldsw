//******************************************************************************
//
//   Main.cpp      Bat2 filter demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Data.h"
#include "Filter.h"
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define TOTAL_SAMPLES 2000

#define LAST_STABLE 1        // last stable hold

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Normal
//******************************************************************************

#define STEP_UP_VALUE    100
#define STEP_DOWN_VALUE -100
#define STABLE_VALUE     120

void __fastcall TMainForm::BtnRedrawClick(TObject *Sender)
{
   FilterStop();
   // read filter data :
   FilterRecord.AveragingWindow = EdtAveraging->Text.ToInt();
   if( FilterRecord.AveragingWindow > FILTER_MAX_AVERAGING){
      // value out of limit - set limit
      FilterRecord.AveragingWindow = FILTER_MAX_AVERAGING;
      EdtAveraging->Text = AnsiString( FilterRecord.AveragingWindow);
   }
   if( FilterRecord.AveragingWindow < 1){
      // value out of limit - set limit
      FilterRecord.AveragingWindow = 1;
      EdtAveraging->Text = AnsiString( FilterRecord.AveragingWindow);
   }

   TRawWeight StableRange;
   try {
     StableRange = (TRawWeight)(EdtStableRange->Text.ToDouble() * 1000);
   } catch( ...){
     Application->MessageBox( "Invalid Stable Range format", "Error", MB_OK);
     return;
   }
   FilterRecord.StableRange = StableRange;

   FilterRecord.StableWindow = EdtStable->Text.ToInt();
   if( FilterRecord.StableWindow < 1){
      FilterRecord.StableWindow = 1;
      EdtStable->Text = AnsiString( FilterRecord.StableWindow);
   }
   // Predfiltrace
   int Prefilter;
   try {
     Prefilter = EdtPrefilter->Text.ToInt();
   } catch( ...){
     Application->MessageBox( "Invalid Prefilter", "Error", MB_OK);
     return;
   }
   BatData->SetPrefilter( Prefilter);

   // Clear old graph :
   WeightSeries->Clear();
   LowPassSeries->Clear();
   HighPassSeries->Clear();
   StepSeries->Clear();
   StableSeries->Clear();
   StableWeightSeries->Clear();
   LastStableWeightSeries->Clear();
   // Read display window :
   int WindowWidth = EdtWidth->Text.ToInt();
   int WindowOffset = EdtOffset->Text.ToInt();
   // scroll at offset :
   BatData->Begin();
   for( int i = 0; i < WindowOffset; i++){
      if( !BatData->Next()){
         return;      // end of database
      }
   }
   FilterRecord.ZeroWeight = (TRawWeight)(BatData->Weight * 1000); // first = initial value
   // initialize filter :
   FilterStart();
   // draw graph :
   int StartIndex = WindowOffset;
   TRawWeight StableWeight = 0;
#ifdef LAST_STABLE
   TRawWeight LastStableWeight = 0;
#endif
   while(1){
      FilterNextSample( (TRawWeight)(BatData->Weight * 1000));    // process filter
      WeightSeries->AddXY( StartIndex,
                           FilterRecord.RawWeight,
                            "", clTeeColor);
      LowPassSeries->AddXY( StartIndex,
                           FilterRecord.LowPass,
                            "", clTeeColor);
      HighPassSeries->AddXY( StartIndex,
                           FilterRecord.HighPass,
                            "", clTeeColor);
      StableSeries->AddXY( StartIndex,
                           GetFilterStatus() == FILTER_STABLE ? STABLE_VALUE : 0,
                            "", clTeeColor);
      StableWeightSeries->AddXY( StartIndex,
                           GetFilterReady() ? FilterRecord.Weight : StableWeight,
                            "", clTeeColor);
      LastStableWeightSeries->AddXY( StartIndex,
                           GetFilterReady() ? FilterRecord.LastStableWeight : LastStableWeight,
                            "", clTeeColor);
      if( GetFilterReady()){
         StableWeight = FilterRecord.Weight;    // remember value
         LastStableWeight = FilterRecord.LastStableWeight;
         ClrFilterReady();                      // handshaking
      }
      StartIndex++;
      if( StartIndex >= (WindowWidth + WindowOffset)){
         return;
      }
      if( !BatData->Next()){
         return;
      }
   }
} // BtnRedrawClick

//******************************************************************************
// Backward
//******************************************************************************

void __fastcall TMainForm::BtnBackwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset -= WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnBackwardClick

//******************************************************************************
// Forward
//******************************************************************************

void __fastcall TMainForm::BtnForwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset += WindowWidth;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnForwardClick

//******************************************************************************
// First
//******************************************************************************

void __fastcall TMainForm::BtnFirstClick(TObject *Sender)
{
   int WindowOffset = 0;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnFirstClick

//******************************************************************************
// File
//******************************************************************************

void __fastcall TMainForm::BtnFileClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   LblFileName->Caption = FileOpenDialog->FileName;
   BatData->SetFile( FileOpenDialog->FileName);
} // BtnFileClick

//******************************************************************************
// Export
//******************************************************************************

void __fastcall TMainForm::BtnExportClick(TObject *Sender)
{
   if( !FileSaveDialog->Execute()){
      return;
   }
   FILE *f;
   f = fopen( FileSaveDialog->FileName.c_str(), "w");
   if( !f){
      Application->MessageBox( "Unable create file", "Error", MB_OK);
      return;
   }
   Application->ProcessMessages();
   Screen->Cursor        = crHourGlass;// wait cursor
   // convert all data :
   BatData->Begin();
   forever {
      if( !BatData->Next()){
         break;      // end of database
      }
      fprintf( f, "%.4lf\n", BatData->Weight);
   }
   fclose( f);
   Screen->Cursor        = crDefault;  // normal cursor
} // BtnExportClick

