//******************************************************************************
//
//   Main.cpp      Bat2 filter demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Data.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Copy
//******************************************************************************

void __fastcall TMainForm::BtnCopyClick(TObject *Sender)
{
   TDateTime From, To;
   try {
      TDateTime dt( EditFrom->Text);
      From = dt;
   } catch( ...){
      Application->MessageBox( "Invalid Date format", "Error", MB_OK);
      ActiveControl = EditFrom;
      return;
   }
   try {
      TDateTime dt( EditTo->Text);
      To = dt;
   } catch( ...){
      Application->MessageBox( "Invalid Date format", "Error", MB_OK);
      ActiveControl = EditTo;
      return;
   }
   int Averaging = EditAveraging->Text.ToInt();
   BatData->Copy( From, To, Averaging);
} // BtnCopyClick

//******************************************************************************
// Low Pass
//******************************************************************************

void __fastcall TMainForm::BtnLowPassClick(TObject *Sender)
{
   LowPassSeries->Clear();                   // smazani starych hodnot

   int WindowSamples = EditWindow->Text.ToInt();
   int StartIndex = 0;
   while(1){
      // rewind :
      BatData->Begin();
      for( int i = 0; i < StartIndex; i++){
         if( !BatData->Next()){
            return;
         }
      }
      // average :
      double Value = 0;
      for( int i = 0; i < WindowSamples; i++){
         Value += BatData->Hmotnost;
         if( !BatData->Next()){
            return;
         }
      }
      LowPassSeries->AddXY( BatData->Ms,
                            Value / WindowSamples,
                            "", clTeeColor);
      StartIndex++;
   }
} // BtnLowPassClick

//******************************************************************************
// High Pass
//******************************************************************************

void __fastcall TMainForm::BtnHighPassClick(TObject *Sender)
{
   HighPassSeries->Clear();                   // smazani starych hodnot

   int WindowSamples = EditWindow->Text.ToInt();
   int WindowSkew    = EditSkew->Text.ToInt();
   if( WindowSkew > WindowSamples){
      WindowSkew = WindowSamples;
      EditSkew->Text = WindowSkew;
   }
   WindowSkew--;

   int StartIndex = 0;
   while(1){
      // rewind :
      BatData->Begin();
      for( int i = 0; i < StartIndex; i++){
         if( !BatData->Next()){
            return;
         }
      }
      // average :
      double Value    = 0;
      double RefValue = 0;
      int    Ms;
      for( int i = 0; i < WindowSamples; i++){
         Value += BatData->Hmotnost;
         if( i == WindowSkew){
            Ms = BatData->Ms;
            RefValue = BatData->Hmotnost;
         }
         if( !BatData->Next()){
            return;
         }
      }
      HighPassSeries->AddXY( BatData->Ms,
                            RefValue - Value / WindowSamples,
                            "", clTeeColor);
      StartIndex++;
   }
} // BtnHighPassClick

