object BatData: TBatData
  OldCreateOrder = False
  Left = 312
  Top = 170
  Height = 672
  Width = 644
  object RawDataTable: TTable
    Active = True
    DatabaseName = 'BAT2FILTER'
    FieldDefs = <
      item
        Name = 'DATUM'
        DataType = ftDateTime
      end
      item
        Name = 'MS'
        DataType = ftInteger
      end
      item
        Name = 'HMOTNOST'
        DataType = ftFloat
      end>
    StoreDefs = True
    TableName = 'bat1.DB'
    Left = 152
    Top = 40
    object RawDataTableDATUM: TDateTimeField
      FieldName = 'DATUM'
    end
    object RawDataTableMS: TIntegerField
      FieldName = 'MS'
    end
    object RawDataTableHMOTNOST: TFloatField
      FieldName = 'HMOTNOST'
    end
  end
  object RawDataSource: TDataSource
    DataSet = RawDataTable
    Left = 152
    Top = 96
  end
  object DataTable: TTable
    Active = True
    DatabaseName = 'BAT2FILTER'
    FieldDefs = <
      item
        Name = 'DATUM'
        DataType = ftDateTime
      end
      item
        Name = 'MS'
        DataType = ftInteger
      end
      item
        Name = 'HMOTNOST'
        DataType = ftFloat
      end>
    StoreDefs = True
    TableName = 'bat2.DB'
    Left = 40
    Top = 40
    object DataTableDATUM: TDateTimeField
      FieldName = 'DATUM'
    end
    object DataTableMS: TIntegerField
      FieldName = 'MS'
    end
    object DataTableHMOTNOST: TFloatField
      FieldName = 'HMOTNOST'
    end
  end
  object DataSource: TDataSource
    DataSet = DataTable
    Left = 56
    Top = 112
  end
end
