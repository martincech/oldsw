//******************************************************************************
//
//   Data.cpp     Databaze
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Data.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBatData *BatData;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TBatData::TBatData(TComponent* Owner)
        : TDataModule(Owner)
{
} // TKurData

//******************************************************************************
// Begin
//******************************************************************************

void TBatData::Begin()
// pred prvni zaznam
{
   DataTable->First();
} // Begin

//******************************************************************************
// Next
//******************************************************************************

bool TBatData::Next()
// na dalsi zaznam
{
   if( DataTable->Eof){
      return( false);
   }
   DataTable->Next();
   return( true);
} // Next

//******************************************************************************
// Copy
//******************************************************************************

void TBatData::Copy( TDateTime From, TDateTime To, int Averaging)
// kopie tabulky
{
   if( Averaging < 0){
      Averaging == 1;
   }
   DataTable->Edit();
   // vyprazdni tabulku :
   for( DataTable->First(); !DataTable->Eof; ){
      DataTable->Delete();
   }
   // Filtr na zdrojovou tabulku :
   RawDataTable->Filter = "Datum >= '" + From + "' and " +
                          "Datum <= '" + To + "'";
   RawDataTable->Filtered = true;
   RawDataTable->First();
   if( Averaging == 1){
      // prosta kopie
      while( !RawDataTable->Eof){
         DataTable->Edit();
         DataTable->Append();
         DataTable->FieldValues[ "DATUM"]    = RawDataTable->FieldValues[ "DATUM"];
         DataTable->FieldValues[ "MS"]       = RawDataTable->FieldValues[ "MS"];
         DataTable->FieldValues[ "HMOTNOST"] = RawDataTable->FieldValues[ "HMOTNOST"];
         DataTable->Post();
         RawDataTable->Next();
      }
      return;
   }
   // prumerovani
   while( 1){
      double Average = 0;
      for( int i = 0; i < Averaging; i++){
         double Value = RawDataTable->FieldValues[ "HMOTNOST"];
         Average += Value;
         RawDataTable->Next();
         if( RawDataTable->Eof){
            return;
         }
      }
      DataTable->Edit();
      DataTable->Append();
      DataTable->FieldValues[ "DATUM"]    = RawDataTable->FieldValues[ "DATUM"];
      DataTable->FieldValues[ "MS"]       = RawDataTable->FieldValues[ "MS"];
      DataTable->FieldValues[ "HMOTNOST"] = Average / Averaging;
      DataTable->Post();
   }
} // Copy

//******************************************************************************
// Datum
//******************************************************************************

TDateTime TBatData::GetDatum()
{
   return( DataTable->FieldValues[ "DATUM"]);
} // GetDatum

//******************************************************************************
// Ms
//******************************************************************************

unsigned TBatData::GetMs()
{
   return( DataTable->FieldValues[ "MS"]);
} // GetMs

//******************************************************************************
// Hmotnost
//******************************************************************************

double TBatData::GetHmotnost()
{
   return( DataTable->FieldValues[ "HMOTNOST"]);
} // GetHmotnost




