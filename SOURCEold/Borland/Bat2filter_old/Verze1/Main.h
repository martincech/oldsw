//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TDBChart *Graph;
        TLineSeries *LowPassSeries;
        TLineSeries *HighPassSeries;
        TEdit *EditFrom;
        TLabel *Label1;
        TEdit *EditTo;
        TLabel *Label2;
        TButton *BtnCopy;
        TLineSeries *HmotnostSeries;
        TButton *BtnLowPass;
        TEdit *EditWindow;
        TLabel *Label3;
        TButton *BtnHighPass;
        TEdit *EditSkew;
        TLabel *Label4;
        TEdit *EditAveraging;
        TLabel *Label5;
        void __fastcall BtnCopyClick(TObject *Sender);
        void __fastcall BtnLowPassClick(TObject *Sender);
        void __fastcall BtnHighPassClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
