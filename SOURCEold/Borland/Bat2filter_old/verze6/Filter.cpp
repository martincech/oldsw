//******************************************************************************
//
//   Filter.c     Filtering
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __C51__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
   #include "Filter.h"
#else
   #include "..\inc\Filter.h"
#endif


#ifdef FILTER_VISIBLE
   #define LowPass  FilterRecord.LowPass
   #define HighPass FilterRecord.HighPass
#endif

TFilterRecord __xdata__ FilterRecord;
static byte             StableCounter;

TRawWeight  __xdata__ Fifo[ FILTER_MAX_AVERAGING];
byte                  FifoPointer;
TLongWeight           FifoSum;

// Local functions :

void FifoInitialize( TRawWeight Fill);
// Initialize FIFO data

TRawWeight FifoAverage( void);
// Calculate average of the contents

void FifoPut( TRawWeight Weight);
// Put to FIFO

//******************************************************************************
// Start
//******************************************************************************

void FilterStart( void)
// Initialize & start filtering
{
   FifoInitialize( FilterRecord.ZeroWeight);
   SetFilterStatus( FILTER_WAIT);
   StableCounter = 0;
} // FilterStart

//******************************************************************************
// Stop
//******************************************************************************

void FilterStop( void)
// Stop filtering
{
   SetFilterStatus( FILTER_STOP);
} // FilterStop

//******************************************************************************
// Sample
//******************************************************************************

void FilterNextSample( TRawWeight Sample)
// Process next sample
{
#ifndef FILTER_VISIBLE
   TRawWeight LowPass;
   TRawWeight HighPass;
#endif
byte       LastStatus;

   FilterRecord.RawWeight = Sample;                     // read sample
   FifoPut( Sample);                                    // save sample
   // filtering :
   LowPass    = FifoAverage();
   HighPass   = FilterRecord.RawWeight - LowPass;
   LastStatus = GetFilterStatus();                      // remember old status
   // check step :
   SetFilterStatus( FILTER_WAIT);
   if( HighPass > 0){
      // positive value
      if( HighPass > FilterRecord.StepRange){
         SetFilterStatus( FILTER_STEP_UP);
      }
   } else {
      // negative value
      if( HighPass < -FilterRecord.StepRange){
         SetFilterStatus( FILTER_STEP_DOWN);
      }
   }
   if( GetFilterStatus() != FILTER_WAIT){
      return;      // done - step up/down
   }
   if( LastStatus == FILTER_WAIT){
      return;      // still waiting for step
   }
   // check for stability range :
   if( HighPass < 0){
      HighPass = -HighPass;   // absolute value
   }
   if( HighPass < FilterRecord.StableRange){
      StableCounter++;
   } else {
      StableCounter = 0;
   }
   // check for stability duration :
   if( StableCounter >= FilterRecord.StableWindow){
      // stable value, previous context is step up/down or stable
      if( !IsFilterOnStepDown() && (LastStatus == FILTER_STEP_DOWN)){
         SetFilterStatus( FILTER_WAIT); // step down + stable = ignore
         return;
      } // else resolve step down also
      if( !IsFilterOnStepUp()   && (LastStatus == FILTER_STEP_UP)){
         SetFilterStatus( FILTER_WAIT); // step up + stable = ignore
         return;
      } // else resolve step up also
      SetFilterStatus( FILTER_STABLE);  // stable value
      FilterRecord.Weight = LowPass;    // set on averaged value
   } else {
      // instable value
      if( LastStatus == FILTER_STABLE){
         SetFilterStatus( FILTER_WAIT); // end of stable data
      } else {
         SetFilterStatus( LastStatus);  // continue step up/down
      }
   }
} // FilterNextSample

//******************************************************************************
// Fifo initialize
//******************************************************************************

void FifoInitialize( TRawWeight Fill)
// Initialize FIFO data
{
TSamplesCount i;

   FifoSum = 0;
   for( i = 0; i < FilterRecord.AveragingWindow; i++){
      Fifo[ i] = Fill;
      FifoSum += Fill;
   }
   FifoPointer = 0;
} // FifoInitialize

//******************************************************************************
// Fifo average
//******************************************************************************

TRawWeight FifoAverage( void)
// Calculate average of the contents
{
   return( FifoSum / FilterRecord.AveragingWindow);
} // FifoAverage

//******************************************************************************
// Fifo put
//******************************************************************************

void FifoPut( TRawWeight Weight)
// Put to FIFO
{
   FifoSum -= Fifo[ FifoPointer];           // remove old value
   Fifo[ FifoPointer++] = Weight;           // save new value
   FifoSum += Weight;                       // add new value
   if( FifoPointer >= FilterRecord.AveragingWindow){
      FifoPointer = 0;
   }
} // FifoPut
