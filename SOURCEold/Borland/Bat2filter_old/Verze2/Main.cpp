//******************************************************************************
//
//   Main.cpp      Bat2 filter demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Data.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define TOTAL_SAMPLES 2000

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Low Pass
//******************************************************************************

void __fastcall TMainForm::BtnLowPassClick(TObject *Sender)
{
   LowPassSeries->Clear();                   // smazani starych hodnot

   int WindowSamples = EditWindow->Text.ToInt();
   int StartIndex = 0;
   while(1){
      // rewind :
      BatData->Begin();
      for( int i = 0; i < StartIndex; i++){
         if( !BatData->Next()){
            return;
         }
      }
      // average :
      double Value = 0;
      for( int i = 0; i < WindowSamples; i++){
         Value += BatData->Weight;
         if( !BatData->Next()){
            return;
         }
      }
      LowPassSeries->AddXY( StartIndex,
                            Value / WindowSamples,
                            "", clTeeColor);
      StartIndex++;
      if( StartIndex >= TOTAL_SAMPLES){
         return;
      }
   }
} // BtnLowPassClick

//******************************************************************************
// High Pass
//******************************************************************************

void __fastcall TMainForm::BtnHighPassClick(TObject *Sender)
{
   HighPassSeries->Clear();                   // smazani starych hodnot

   int WindowSamples = EditWindow->Text.ToInt();
   int WindowSkew    = EditSkew->Text.ToInt();
   if( WindowSkew > WindowSamples){
      WindowSkew = WindowSamples;
      EditSkew->Text = WindowSkew;
   }
   WindowSkew--;

   int StartIndex = 0;
   while(1){
      // rewind :
      BatData->Begin();
      for( int i = 0; i < StartIndex; i++){
         if( !BatData->Next()){
            return;
         }
      }
      // average :
      double Value    = 0;
      double RefValue = 0;
      for( int i = 0; i < WindowSamples; i++){
         Value += BatData->Weight;
         if( i == WindowSkew){
            RefValue = BatData->Weight;
         }
         if( !BatData->Next()){
            return;
         }
      }
      HighPassSeries->AddXY( StartIndex,
                            RefValue - Value / WindowSamples,
                            "", clTeeColor);
      StartIndex++;
      if( StartIndex >= TOTAL_SAMPLES){
         return;
      }
   }
} // BtnHighPassClick

//******************************************************************************
// Normal
//******************************************************************************

void __fastcall TMainForm::BtnNormalClick(TObject *Sender)
{
   HmotnostSeries->Clear();                   // smazani starych hodnot
   int WindowSamples = EditWindow->Text.ToInt();

   int StartIndex = 0;
   BatData->Begin();
   while(1){
      HmotnostSeries->AddXY( StartIndex -  WindowSamples,
                             BatData->Weight,
                            "", clTeeColor);
      StartIndex++;
      if( StartIndex >= TOTAL_SAMPLES){
         return;
      }
      if( !BatData->Next()){
         return;
      }
   }
} // BtnNormalClick

