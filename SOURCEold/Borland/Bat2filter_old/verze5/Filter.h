//******************************************************************************
//
//   Filter.h     Filtering
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef FilterH
#define FilterH

// Data limits :
#define FILTER_MAX_AVERAGING 100   // max. width of averaging window

// Basic data types :
typedef long   TWeight;            // weight data
typedef long   TLongWeight;        // weight sum
typedef int    TSamplesCount;      // samples counter

// Filter status :
typedef enum {
   FILTER_STOP,        // weighing disabled
   FILTER_WAIT,        // wait for step
   FILTER_STEP_UP,     // step up
   FILTER_STEP_DOWN,   // step down
   FILTER_STABLE,      // weighing done
   _FILTER_LAST
} TFilterStatus;

// Data record :
typedef struct {
   TFilterStatus Status;           // filter status
   TWeight       RawWeight;        // filter input
   TWeight       ZeroWeight;       // zero weight - fifo initials
   TWeight       LowPass;          // low pass output
   TWeight       HighPass;         // high pass output
   TSamplesCount AveragingWindow;  // moving average window
   TSamplesCount StableWindow;     // stability windows
   TWeight       StepRange;        // greater value is step
   TWeight       StableRange;      // less value is stable
   bool          OnStepDown;       // weighing on step down also
   bool          OnStepUp;         // weighing on step up also
   TWeight       Weight;           // stable value
} TDataRecord;

// Global data :
extern TDataRecord DataRecord;

//******************************************************************************
// Functions
//******************************************************************************

void FilterInitialize( void);
// Initialize filter data

void NextSample( void);
// Process next sample

#endif
