//******************************************************************************
//
//   Main.cpp     Bitmap main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

// Color planes :
//            White   Light gray   Dark Gray Black
// Plane 0    0       1            0         1
// Plane 1    0       0            1         1

// Color transformation :
#define COLOR_BLACK      0x00000000
#define COLOR_MIDDLEGRAY 0x007F7F7F
#define COLOR_WHITE      0x00FFFFFF

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
}


//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadClick(TObject *Sender)
{
   if( !OpenDialog->Execute()){
      return;
   }
   Image->Picture->LoadFromFile( OpenDialog->FileName);
   SaveDialog->FileName = ChangeFileExt( OpenDialog->FileName, ".c" );
   LblWidth->Caption  = AnsiString( Image->Picture->Bitmap->Width);
   LblHeight->Caption = AnsiString( Image->Picture->Bitmap->Height);
   EdtName->Text      = ExtractFileName( OpenDialog->FileName);
   EdtName->Text      = ChangeFileExt( EdtName->Text, "");
   // zoom :
   RgZoom->ItemIndex = 0;
   Image->Width  = Image->Picture->Bitmap->Width;
   Image->Height = Image->Picture->Bitmap->Height;
   Image->Stretch = false;
} // BtnLoadClick

//******************************************************************************
// Save
//******************************************************************************

#define LINE_COUNT 16  // bytes per line

void __fastcall TMainForm::BtnSaveClick(TObject *Sender)
{

   if( !SaveDialog->Execute()){
      return;
   }
   if( FileExists( SaveDialog->FileName)){
      if( Application->MessageBox( "File exists, overwrite ?", "Confirmation", MB_OKCANCEL) != IDOK){
         return;
      }
   }
   f = fopen( SaveDialog->FileName.c_str(), "w");
   if( !f){
      Application->MessageBox( "Unable open file", "Error", MB_OK);
      return;
   }

   int Planes = Cb4Colors->Checked ? 2 : 1;
   int Width  = Image->Picture->Bitmap->Width;
   int Height = Image->Picture->Bitmap->Height;

   fprintf( f, "DefineBitmap( %s, %d, %d, %d)", EdtName->Text.c_str(), Planes, Width, Height);
   for( int p = 0; p < Planes; p++){
      InitBit();
      fprintf( f, "\n//---- Plane %d ----\n", p);
      for( int x = 0; x < Width; x++){
         for( int y = 0; y < Height; y ++){
            TColor Color = Image->Picture->Bitmap->Canvas->Pixels[ x][ y];
            if( p == 0){
               if( Color == COLOR_WHITE){
                  SaveBit( false);
                  continue;
               }
               if( Color == COLOR_BLACK || Color >= COLOR_MIDDLEGRAY){ // light gray
                  SaveBit( true);
               } else {
                  SaveBit( false);
               }
            } else { // plane 1
               if( Color == COLOR_WHITE){
                  SaveBit( false);
                  continue;
               }
               if( Color == COLOR_BLACK || Color <  COLOR_MIDDLEGRAY){ // dark gray
                  SaveBit( true);
               } else {
                  SaveBit( false);
               }
            }
         } // for y
      } // for x
      // terminate/fill last byte :
      int BitCount = (Width * Height) % 8;      // last byte bits
      if( BitCount){
         BitCount = 8 - BitCount;               // up to byte width
         for( int i = 0; i < BitCount; i++){
            SaveBit( false);
         }
      } // else last byte finalized
   } // for p
   fprintf( f, "\nEndBitmap()\n");
   fclose( f);
} // BtnSaveClick

//******************************************************************************
// Zoom
//******************************************************************************

void __fastcall TMainForm::RgZoomClick(TObject *Sender)
{
int Zoom;

   switch( RgZoom->ItemIndex){
      case 0 :
         Zoom = 1;
         break;
      case 1 :
         Zoom = 2;
         break;
      case 2 :
         Zoom = 4;
         break;
      case 3 :
         Zoom = 8;
         break;
   }
   Image->Width   = Image->Picture->Bitmap->Width  * Zoom;
   Image->Height  = Image->Picture->Bitmap->Height * Zoom;
   Image->Stretch = true;
} // RgZoomClick

//******************************************************************************
// Init bit
//******************************************************************************

void __fastcall TMainForm::InitBit()
// Init bit stream
{
   Offset      = 0;
   Value       = 0;
   LineCounter = 0;
} // InitBit

//******************************************************************************
// Save bit
//******************************************************************************

void __fastcall TMainForm::SaveBit( bool BitValue)
// Save bit into stream
{
   if( !LineCounter && !(Offset % 8)){
      fprintf( f, "/* %04x */ ", Offset / 8);    // start of line
   }
   if( BitValue){
      Value |= 1 << (Offset % 8);
   }
   Offset++;
   if( !(Offset % 8)){
      // prepare new byte
      fprintf( f, "0x%02X,", Value);
      Value = 0;
      LineCounter++;
      if( LineCounter >= LINE_COUNT){
         LineCounter = 0;
         fprintf( f, "\n");
      }
   }
} // SaveBit

