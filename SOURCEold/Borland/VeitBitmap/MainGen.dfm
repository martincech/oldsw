object MainForm: TMainForm
  Left = 226
  Top = 210
  Width = 548
  Height = 722
  Caption = 'Font Generator'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImage
    Left = 288
    Top = 16
    Width = 100
    Height = 25600
  end
  object Label1: TLabel
    Left = 40
    Top = 162
    Width = 34
    Height = 13
    Caption = 'Width :'
  end
  object Label2: TLabel
    Left = 40
    Top = 184
    Width = 61
    Height = 13
    Caption = 'Bmp Height :'
  end
  object LblWidth: TLabel
    Left = 112
    Top = 162
    Width = 6
    Height = 13
    Caption = '0'
  end
  object LblHeight: TLabel
    Left = 112
    Top = 184
    Width = 6
    Height = 13
    Caption = '0'
  end
  object Label3: TLabel
    Left = 34
    Top = 392
    Width = 48
    Height = 13
    Caption = 'H Space :'
  end
  object Label4: TLabel
    Left = 34
    Top = 424
    Width = 47
    Height = 13
    Caption = 'V Space :'
  end
  object Label5: TLabel
    Left = 34
    Top = 456
    Width = 56
    Height = 13
    Caption = 'Avg Width :'
  end
  object Label6: TLabel
    Left = 37
    Top = 243
    Width = 58
    Height = 13
    Caption = 'Font Name :'
  end
  object Label7: TLabel
    Left = 40
    Top = 208
    Width = 50
    Height = 13
    Caption = 'Byte Size :'
  end
  object LblByteSize: TLabel
    Left = 112
    Top = 208
    Width = 6
    Height = 13
    Caption = '0'
  end
  object Label10: TLabel
    Left = 32
    Top = 480
    Width = 3
    Height = 13
  end
  object Label8: TLabel
    Left = 30
    Top = 568
    Width = 59
    Height = 13
    Caption = 'Top margin :'
  end
  object Label9: TLabel
    Left = 30
    Top = 600
    Width = 74
    Height = 13
    Caption = 'Bottom Margin :'
  end
  object Label11: TLabel
    Left = 120
    Top = 536
    Width = 85
    Height = 13
    Caption = 'Character clipping'
  end
  object Label12: TLabel
    Left = 152
    Top = 184
    Width = 62
    Height = 13
    Caption = 'Real Height :'
  end
  object LblRealHeight: TLabel
    Left = 224
    Top = 184
    Width = 6
    Height = 13
    Caption = '0'
  end
  object Label13: TLabel
    Left = 120
    Top = 360
    Width = 91
    Height = 13
    Caption = 'Additional font data'
  end
  object Label14: TLabel
    Left = 34
    Top = 488
    Width = 74
    Height = 13
    Caption = 'Number Width :'
  end
  object BtnLoad: TButton
    Left = 48
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Load'
    TabOrder = 0
    OnClick = BtnLoadClick
  end
  object BtnSave: TButton
    Left = 48
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 1
    OnClick = BtnSaveClick
  end
  object EdtName: TEdit
    Left = 109
    Top = 235
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '?'
  end
  object RgCodePage: TRadioGroup
    Left = 44
    Top = 272
    Width = 105
    Height = 73
    Caption = 'Code Page'
    ItemIndex = 0
    Items.Strings = (
      'Latin'
      'Japan'
      'Numeric')
    TabOrder = 3
  end
  object BtnCheck: TButton
    Left = 48
    Top = 67
    Width = 75
    Height = 25
    Caption = 'Check'
    TabOrder = 4
    OnClick = BtnCheckClick
  end
  object EdtHSpace: TEdit
    Left = 110
    Top = 388
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '1'
  end
  object EdtVSpace: TEdit
    Left = 110
    Top = 420
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '0'
  end
  object EdtAvgWidth: TEdit
    Left = 110
    Top = 452
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '10'
  end
  object EdtTopMargin: TEdit
    Left = 110
    Top = 564
    Width = 121
    Height = 21
    TabOrder = 8
    Text = '0'
  end
  object EdtBottomMargin: TEdit
    Left = 110
    Top = 596
    Width = 121
    Height = 21
    TabOrder = 9
    Text = '0'
  end
  object RgZoom: TRadioGroup
    Left = 160
    Top = 24
    Width = 105
    Height = 105
    Caption = 'Zoom'
    ItemIndex = 0
    Items.Strings = (
      '1x'
      '2x'
      '4x'
      '8x')
    TabOrder = 10
    OnClick = RgZoomClick
  end
  object EdtNumWidth: TEdit
    Left = 110
    Top = 484
    Width = 121
    Height = 21
    TabOrder = 11
    Text = '10'
  end
  object OpenDialog: TOpenDialog
    Filter = 'Font picture|*.bmp'
    Left = 152
    Top = 24
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '*.c'
    FileName = 'Bitmap.c'
    Filter = 'Source|*.c'
    Title = 'Save Font'
    Left = 152
    Top = 112
  end
end
