//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Font.res");
USEFORM("MainFont.cpp", MainForm);
USEUNIT("..\ArmParser\CodePage.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
