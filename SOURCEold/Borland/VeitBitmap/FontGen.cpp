//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("FontGen.res");
USEFORM("MainGen.cpp", MainForm);
USEUNIT("..\ArmParser\CodePage.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bitmap";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
