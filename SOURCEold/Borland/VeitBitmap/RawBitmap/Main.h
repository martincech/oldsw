//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image;
        TButton *BtnLoad;
        TButton *BtnSave;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *LblWidth;
        TLabel *LblHeight;
        void __fastcall BtnLoadClick(TObject *Sender);
        void __fastcall BtnSaveClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
