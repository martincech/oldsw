//******************************************************************************
//
//   Main.cpp     Bitmap main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;
#include <stdio.h>

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
}


//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadClick(TObject *Sender)
{
   if( !OpenDialog->Execute()){
      return;
   }
   Image->Picture->LoadFromFile( OpenDialog->FileName);
   LblWidth->Caption  = AnsiString( Image->Picture->Bitmap->Width);
   LblHeight->Caption = AnsiString( Image->Picture->Bitmap->Height);
}

//******************************************************************************
// Save
//******************************************************************************

void __fastcall TMainForm::BtnSaveClick(TObject *Sender)
{
FILE *f;
int  Width, Height;
byte Value;

   if( !SaveDialog->Execute()){
      return;
   }
   if( FileExists( SaveDialog->FileName)){
      if( Application->MessageBox( "File exists, overwrite ?", "Confirmation", MB_OKCANCEL) != IDOK){
         return;
      }
   }
   f = fopen( SaveDialog->FileName.c_str(), "w");
   if( !f){
      Application->MessageBox( "Unable open file", "Error", MB_OK);
   }
   Width  = Image->Picture->Bitmap->Width;
   Height = Image->Picture->Bitmap->Height;
   fprintf( f, "const byte Bitmap[ %d][ %d] = {\n", Height, Width / 8);
   for( int y = 0; y < Height; y++){
      fprintf( f, "/* %2d */ {", y);
      char ColumnChar = ' ';
      for( int x = 0; x < Width; x += 8){
         Value = 0;
         for( int i = 0; i < 8; i++){
            if( Image->Picture->Bitmap->Canvas->Pixels[ x + i][ y] != clBlack){
               continue;
            }
            Value |= (1 << i);
         }
         fprintf( f, "%c0x%02X", ColumnChar, Value);
         ColumnChar = ',';
      }
      fprintf( f, "},\n");
   }
   fprintf( f, "};\n");
   fclose( f);
}

