object MainForm: TMainForm
  Left = 418
  Top = 219
  Width = 579
  Height = 734
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImage
    Left = 240
    Top = 10
    Width = 300
    Height = 5000
  end
  object Label1: TLabel
    Left = 40
    Top = 136
    Width = 34
    Height = 13
    Caption = 'Width :'
  end
  object Label2: TLabel
    Left = 40
    Top = 168
    Width = 37
    Height = 13
    Caption = 'Height :'
  end
  object LblWidth: TLabel
    Left = 112
    Top = 136
    Width = 6
    Height = 13
    Caption = '0'
  end
  object LblHeight: TLabel
    Left = 112
    Top = 168
    Width = 6
    Height = 13
    Caption = '0'
  end
  object BtnLoad: TButton
    Left = 48
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Load'
    TabOrder = 0
    OnClick = BtnLoadClick
  end
  object BtnSave: TButton
    Left = 48
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 1
    OnClick = BtnSaveClick
  end
  object OpenDialog: TOpenDialog
    Filter = 'obr|*.bmp'
    Left = 152
    Top = 24
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '*.c'
    FileName = 'Demo.c'
    Filter = 'Source|*.c'
    Left = 152
    Top = 72
  end
end
