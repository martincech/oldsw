object MainForm: TMainForm
  Left = 418
  Top = 219
  Width = 472
  Height = 570
  Caption = 'Font to bmp'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImage
    Left = 288
    Top = 16
    Width = 100
    Height = 25600
  end
  object Label3: TLabel
    Left = 14
    Top = 28
    Width = 27
    Height = 13
    Caption = 'Font :'
  end
  object Label4: TLabel
    Left = 13
    Top = 60
    Width = 37
    Height = 13
    Caption = 'Height :'
  end
  object Label1: TLabel
    Left = 48
    Top = 235
    Width = 34
    Height = 13
    Caption = 'Width :'
  end
  object LblWidth: TLabel
    Left = 120
    Top = 235
    Width = 6
    Height = 13
    Caption = '0'
  end
  object Label2: TLabel
    Left = 48
    Top = 255
    Width = 37
    Height = 13
    Caption = 'Height :'
  end
  object LblHeight: TLabel
    Left = 120
    Top = 255
    Width = 6
    Height = 13
    Caption = '0'
  end
  object Label5: TLabel
    Left = 48
    Top = 281
    Width = 59
    Height = 13
    Caption = 'Left Margin :'
  end
  object LblRealWidth: TLabel
    Left = 176
    Top = 235
    Width = 6
    Height = 13
    Caption = '0'
  end
  object BtnSave: TButton
    Left = 48
    Top = 480
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 0
    OnClick = BtnSaveClick
  end
  object BtnGenerate: TButton
    Left = 48
    Top = 440
    Width = 75
    Height = 25
    Caption = 'Generate'
    TabOrder = 1
    OnClick = BtnGenerateClick
  end
  object CbxFont: TComboBox
    Left = 56
    Top = 24
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object EdtHeight: TEdit
    Left = 56
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '20'
  end
  object CbBold: TCheckBox
    Left = 56
    Top = 88
    Width = 97
    Height = 17
    Caption = 'Bold'
    TabOrder = 4
  end
  object CbItalic: TCheckBox
    Left = 56
    Top = 112
    Width = 97
    Height = 17
    Caption = 'Italic'
    TabOrder = 5
  end
  object RgCodePage: TRadioGroup
    Left = 48
    Top = 144
    Width = 105
    Height = 73
    Caption = 'Code Page'
    ItemIndex = 0
    Items.Strings = (
      'Latin'
      'Japan'
      'Numeric')
    TabOrder = 6
  end
  object RgZoom: TRadioGroup
    Left = 48
    Top = 312
    Width = 105
    Height = 105
    Caption = 'Zoom'
    ItemIndex = 0
    Items.Strings = (
      '1x'
      '2x'
      '4x'
      '8x')
    TabOrder = 7
    OnClick = RgZoomClick
  end
  object EdtLeftMargin: TEdit
    Left = 120
    Top = 277
    Width = 73
    Height = 21
    TabOrder = 8
    Text = '0'
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '*.bmp'
    FileName = 'Font.bmp'
    Filter = 'Bitmap|*.bmp'
    Title = 'Save Bitmap'
    Left = 152
    Top = 480
  end
end
