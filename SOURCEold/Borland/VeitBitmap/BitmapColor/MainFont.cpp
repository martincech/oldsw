//******************************************************************************
//
//   MainFont.cpp  Font main
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainFont.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   MyBitmap = 0;
   CodePage = new TCodePage;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   // get font names :
   for( int i = 0; i < Screen->Fonts->Count; i++){
      CbxFont->Items->Add( Screen->Fonts->Strings[ i]);
   }
   CbxFont->ItemIndex = 0;
   // open codepage tables :
   AnsiString ExePath = ExtractFilePath( Application->ExeName);
   if( !CodePage->Load( 0, ExePath + "INTER.CPG")){  // normal
      Application->MessageBox( "Unable open file INTER.CPG", "Error", MB_OK);
   }
   if( !CodePage->Load( 1, ExePath + "JAPAN.CPG")){  // japan
      Application->MessageBox( "Unable open file JAPAN.CPG", "Error", MB_OK);
   }
   if( !CodePage->Load( 2, ExePath + "NUMERIC.CPG")){  // numeric
      Application->MessageBox( "Unable open file NUMERIC.CPG", "Error", MB_OK);
   }
} // FormCreate

//******************************************************************************
// Save
//******************************************************************************

void __fastcall TMainForm::BtnSaveClick(TObject *Sender)
{

   if( !SaveDialog->Execute()){
      return;
   }
   if( FileExists( SaveDialog->FileName)){
      if( Application->MessageBox( "File exists, overwrite ?", "Confirmation", MB_OKCANCEL) != IDOK){
         return;
      }
   }
   Image->Picture->SaveToFile( SaveDialog->FileName);
} // BtnSaveClick

//******************************************************************************
// Test
//******************************************************************************

void __fastcall TMainForm::BtnGenerateClick(TObject *Sender)
{
   // create a new bitmap :
   if( MyBitmap){
      delete MyBitmap;
   }
   MyBitmap  = new Graphics::TBitmap;
   MyBitmap->Height     = 100;
   MyBitmap->Width      = 100;
   MyBitmap->Monochrome = true;
   Image->Picture->Bitmap = MyBitmap;
   TCanvas  *Canvas  = Image->Picture->Bitmap->Canvas;
   // font name :
   Canvas->Font->Name   = CbxFont->Items->Strings[ CbxFont->ItemIndex];
   // font height :
   Canvas->Font->Height = EdtHeight->Text.ToInt();
   // font styles :
   TFontStyles fs;
   if( CbBold->Checked){
      fs += TFontStyles() << fsBold;
   }
   if( CbItalic->Checked){
      fs += TFontStyles() << fsItalic;
   }
   Canvas->Font->Style = fs;
   // get text metrics :
   TEXTMETRIC tm;
   GetTextMetrics( Canvas->Handle, &tm);
   int Height    = tm.tmHeight;
   int Width     = tm.tmMaxCharWidth;
   int RealWidth = Width - EdtLeftMargin->Text.ToInt();
   LblHeight->Caption    = AnsiString( Height);
   LblWidth->Caption     = AnsiString( Width);
   LblRealWidth->Caption = AnsiString( RealWidth);
//------------------------------------------------------------------------------
   // active code page :
   int CPage  = RgCodePage->ItemIndex;
   int CPSize = CodePage->Size( CPage);
   // update bitmap size :
   delete MyBitmap;
   MyBitmap  = new Graphics::TBitmap;
   MyBitmap->Height     = CPSize * Height;
   MyBitmap->Width      = RealWidth;
   MyBitmap->Monochrome = true;
   Image->Picture->Bitmap = MyBitmap;
   Canvas  = Image->Picture->Bitmap->Canvas;
   // clear bitmap :
   Canvas->Brush->Color = clWhite;
   Canvas->Brush->Style = bsSolid;
   Canvas->Pen->Color   = clWhite;
   Canvas->Rectangle( Bounds( 0, 0, RealWidth, CPSize * Height));
   // restore font :
   Canvas->Font->Name   = CbxFont->Items->Strings[ CbxFont->ItemIndex];
   Canvas->Font->Height = EdtHeight->Text.ToInt();
   Canvas->Font->Style  = fs;
   // text colors :
   Canvas->Brush->Color = clWhite;
   Canvas->Brush->Style = bsSolid;
   Canvas->Pen->Color   = clBlack;
   // generate text :
   wchar_t Text[ 2];
   int     y = 0;
   wchar_t wch;
   for( int i = 0; i < CPSize; i++){
      wch = CodePage->CharFrom( CPage, i);
      if( wch != L' ' && wch != L'\0' && wch != L'\n' && wch != L'\r'){
         Text[ 0] = wch;
         Text[ 1] = L'\0';
         ::TextOutW( Canvas->Handle, 0, y, Text, 1);
      } // else control character as space
      y += Height;
   }
   // zoom :
   RgZoom->ItemIndex = 0;
   Image->Width  = Image->Picture->Bitmap->Width;
   Image->Height = Image->Picture->Bitmap->Height;
   Image->Stretch = false;
} // BtnTestClick

//******************************************************************************
// Zoom
//******************************************************************************

void __fastcall TMainForm::RgZoomClick(TObject *Sender)
{
int Zoom;

   switch( RgZoom->ItemIndex){
      case 0 :
         Zoom = 1;
         break;
      case 1 :
         Zoom = 2;
         break;
      case 2 :
         Zoom = 4;
         break;
      case 3 :
         Zoom = 8;
         break;
   }
   Image->Width   = Image->Picture->Bitmap->Width  * Zoom;
   Image->Height  = Image->Picture->Bitmap->Height * Zoom;
   Image->Stretch = true;
} // RgZoomClick

