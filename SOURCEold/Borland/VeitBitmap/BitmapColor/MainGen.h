//---------------------------------------------------------------------------

#ifndef MainGenH
#define MainGenH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <stdio.h>

#include "CodePage.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image;
        TButton *BtnLoad;
        TButton *BtnSave;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *LblWidth;
        TLabel *LblHeight;
        TEdit *EdtName;
        TRadioGroup *RgCodePage;
        TButton *BtnCheck;
        TEdit *EdtHSpace;
        TLabel *Label3;
        TLabel *Label4;
        TEdit *EdtVSpace;
        TLabel *Label5;
        TEdit *EdtAvgWidth;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *LblByteSize;
        TLabel *Label10;
        TLabel *Label8;
        TEdit *EdtTopMargin;
        TLabel *Label9;
        TEdit *EdtBottomMargin;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *LblRealHeight;
        TRadioGroup *RgZoom;
        TLabel *Label13;
        void __fastcall BtnLoadClick(TObject *Sender);
        void __fastcall BtnSaveClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnCheckClick(TObject *Sender);
        void __fastcall RgZoomClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);

protected :
   AnsiString FileName;
   TCodePage *CodePage;
   // font parameters :
   int CPage;         // active code page
   int CharCount;     // character count
   int MaxHeight;     // character height
   int MaxWidth;      // character max width
   int ByteSize;      // character byte size
   int TopMargin;     // character clipping top
   int BottomMargin;  // character clipping bottom
   // generator :
   int   Offset;      // bit offset
   byte  Value;       // byte output value
   int   LineCounter; // bytes per line
   bool  FirstRow;    // first data row
   FILE *f;

void __fastcall InitBit();
// Init bit stream

void __fastcall SaveBit( bool BitValue);
// Save bit into stream

int __fastcall GetMaxWidth();
// Get maximal character width

int __fastcall GetWidth( int ch);
// Get character width

};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
