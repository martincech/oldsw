//---------------------------------------------------------------------------

#ifndef MainFontH
#define MainFontH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>

#include "CodePage.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image;
        TButton *BtnSave;
        TSaveDialog *SaveDialog;
        TButton *BtnGenerate;
        TComboBox *CbxFont;
        TLabel *Label3;
        TEdit *EdtHeight;
        TLabel *Label4;
        TCheckBox *CbBold;
        TCheckBox *CbItalic;
        TLabel *Label1;
        TLabel *LblWidth;
        TLabel *Label2;
        TLabel *LblHeight;
        TRadioGroup *RgCodePage;
        TRadioGroup *RgZoom;
        TLabel *Label5;
        TEdit *EdtLeftMargin;
        TLabel *LblRealWidth;
        void __fastcall BtnSaveClick(TObject *Sender);
        void __fastcall BtnGenerateClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall RgZoomClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);

protected :
   TCodePage          *CodePage;
   Graphics::TBitmap  *MyBitmap;
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
