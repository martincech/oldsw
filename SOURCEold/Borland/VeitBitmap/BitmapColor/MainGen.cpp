//******************************************************************************
//
//   Main.cpp     Font generator main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainGen.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define COLOR_BLACK  clBlack
#define NUMERIC_PAGE 2

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   CodePage = new TCodePage;
   FileName = "";
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   // open codepage tables :
   AnsiString ExePath = ExtractFilePath( Application->ExeName);
   if( !CodePage->Load( 0, ExePath + "INTER.CPG")){  // normal
      Application->MessageBox( "Unable open file INTER.CPG", "Error", MB_OK);
   }
   if( !CodePage->Load( 1, ExePath + "JAPAN.CPG")){  // japan
      Application->MessageBox( "Unable open file JAPAN.CPG", "Error", MB_OK);
   }
   if( !CodePage->Load( 2, ExePath + "NUMERIC.CPG")){  // numeric
      Application->MessageBox( "Unable open file NUMERIC.CPG", "Error", MB_OK);
   }
} // FormCreate

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadClick(TObject *Sender)
{
   if( !OpenDialog->Execute()){
      return;
   }
   Image->Picture->LoadFromFile( OpenDialog->FileName);
   EdtName->Text        = ExtractFileName( OpenDialog->FileName);
   EdtName->Text        = ChangeFileExt( EdtName->Text, "");
   SaveDialog->FileName = ChangeFileExt( OpenDialog->FileName, ".c" );
   FileName = OpenDialog->FileName;      // save file name
   // zoom :
   RgZoom->ItemIndex = 0;
   Image->Width  = Image->Picture->Bitmap->Width;
   Image->Height = Image->Picture->Bitmap->Height;
   Image->Stretch = false;
} // BtnLoadClick

//******************************************************************************
// Check
//******************************************************************************

void __fastcall TMainForm::BtnCheckClick(TObject *Sender)
{
   Image->Picture->LoadFromFile( FileName); // reload - clear
   CPage        = RgCodePage->ItemIndex;
   CharCount    = CodePage->Size( CPage);
   MaxHeight    = Image->Picture->Bitmap->Height / CharCount;
   MaxWidth     = GetMaxWidth();
   TopMargin    = EdtTopMargin->Text.ToInt();
   BottomMargin = EdtBottomMargin->Text.ToInt();
   ByteSize     = ((MaxHeight - TopMargin - BottomMargin) * MaxWidth + 7) / 8;
   LblWidth->Caption      = AnsiString( MaxWidth);
   LblHeight->Caption     = AnsiString( MaxHeight);
   LblByteSize->Caption   = AnsiString( ByteSize);
   LblRealHeight->Caption = AnsiString( MaxHeight - TopMargin - BottomMargin);
   EdtAvgWidth->Text      = AnsiString((MaxWidth * 8) / 10);

   TCanvas *Canvas      = Image->Picture->Bitmap->Canvas;
   Canvas->Brush->Color = clBlack;
   Canvas->Brush->Style = bsSolid;
   Canvas->Pen->Color   = clBlack;
   // left margin :
   int LeftMargin = Image->Picture->Bitmap->Width - MaxWidth;
   if( LeftMargin > 0){
      Canvas->Rectangle( Bounds( MaxWidth, 0, LeftMargin, CharCount * MaxHeight));
   }
   int yBase;
   // top margin :
   if( TopMargin > 0){
      yBase = 0;
      for( int c = 0; c < CharCount; c++){
         Canvas->Rectangle( Bounds( 0, yBase,
                                    Image->Picture->Bitmap->Width, TopMargin));
         yBase += MaxHeight;
      }
   }
   // bottom margin :
   if( BottomMargin > 0){
      yBase = 0;
      for( int c = 0; c < CharCount; c++){
         Canvas->Rectangle( Bounds( 0, yBase + (MaxHeight - BottomMargin),
                                    Image->Picture->Bitmap->Width, BottomMargin));
         yBase += MaxHeight;
      }
   }
} // BtnCheckClick

//******************************************************************************
// Save
//******************************************************************************

#define LINE_COUNT 16  // bytes per line

void __fastcall TMainForm::BtnSaveClick(TObject *Sender)
{

   if( !SaveDialog->Execute()){
      return;
   }
   if( FileExists( SaveDialog->FileName)){
      if( Application->MessageBox( "File exists, overwrite ?", "Confirmation", MB_OKCANCEL) != IDOK){
         return;
      }
   }
   f = fopen( SaveDialog->FileName.c_str(), "w");
   if( !f){
      Application->MessageBox( "Unable open file", "Error", MB_OK);
      return;
   }
   BtnCheckClick( 0);                       // calculate parameters
   Image->Picture->LoadFromFile( FileName); // reload - clear

   fprintf( f, "//*****************************************************************************\n");
   fprintf( f, "//\n");
   fprintf( f, "//   %s.c  Font %s\n", EdtName->Text.c_str(), EdtName->Text.c_str());
   fprintf( f, "//   Version 1.0\n");
   fprintf( f, "//\n");
   fprintf( f, "//*****************************************************************************\n\n");
   fprintf( f, "#include \"../../inc/Font.h\"\n\n");
   fprintf( f, "const byte %sArray [ %d * %d]= {", EdtName->Text.c_str(), CharCount, ByteSize + 1);
   int yBase = 0;
   int ActualHeight = MaxHeight - TopMargin - BottomMargin; // scan rows
   for( int c = 0; c < CharCount; c++){
      int CharWidth = GetWidth( c);
      if( !CharWidth){
         CharWidth = MaxWidth / 3 + 1;   // space width
      }
      InitBit();
      fprintf( f, "\n/* 0x%02X */ 0x%02X,", c, CharWidth);
      for( int x = 0; x < MaxWidth; x++){
         for( int y = 0; y < ActualHeight; y++){
            if( Image->Picture->Bitmap->Canvas->Pixels[ x][ y + TopMargin + yBase] == COLOR_BLACK){
               SaveBit( true);
            } else {
               SaveBit( false);
            }
         } // for y
      } // for x
      // terminate/fill last byte :
      int BitCount = (MaxWidth * ActualHeight) % 8;// last byte bits
      if( BitCount){
         BitCount = 8 - BitCount;               // up to byte width
         for( int i = 0; i < BitCount; i++){
            SaveBit( false);
         }
      } // else last byte finalized
      yBase += MaxHeight;
   } // for c
   fprintf( f, "\n};\n\n");
   fprintf( f, "const TFontDescriptor Font%s = {\n", EdtName->Text.c_str());
   if( CPage == NUMERIC_PAGE){
      fprintf( f, "   /* Type     */ FONT_NUMERIC,\n");
   } else {
      fprintf( f, "   /* Type     */ FONT_PROPORTIONAL,\n");
   }
   fprintf( f, "   /* Width    */ %d,\n",     MaxWidth);
   fprintf( f, "   /* Height   */ %d,\n",     ActualHeight);
   fprintf( f, "   /* Offset   */ %d,\n",     CPage == NUMERIC_PAGE ? 48 : 0);
   fprintf( f, "   /* Size     */ %d,\n",     ByteSize + 1);
   fprintf( f, "   /* AvgWidth */ %s,\n",     EdtAvgWidth->Text.c_str());
   fprintf( f, "   /* HSpace   */ %s,\n",     EdtHSpace->Text.c_str());
   fprintf( f, "   /* VSpace   */ %s,\n",     EdtVSpace->Text.c_str());
   fprintf( f, "   /* Array    */ %sArray\n", EdtName->Text.c_str());
   fprintf( f, "};\n\n");
   fclose( f);
} // BtnSaveClick

//******************************************************************************
// Zoom
//******************************************************************************

void __fastcall TMainForm::RgZoomClick(TObject *Sender)
{
int Zoom;

   switch( RgZoom->ItemIndex){
      case 0 :
         Zoom = 1;
         break;
      case 1 :
         Zoom = 2;
         break;
      case 2 :
         Zoom = 4;
         break;
      case 3 :
         Zoom = 8;
         break;
   }
   Image->Width   = Image->Picture->Bitmap->Width  * Zoom;
   Image->Height  = Image->Picture->Bitmap->Height * Zoom;
   Image->Stretch = true;
} // RgZoomClick

//******************************************************************************
// Init bit
//******************************************************************************

void __fastcall TMainForm::InitBit()
// Init bit stream
{
   Offset      = 0;
   Value       = 0;
   LineCounter = 0;
   FirstRow    = true;
} // InitBit

//******************************************************************************
// Save bit
//******************************************************************************

void __fastcall TMainForm::SaveBit( bool BitValue)
// Save bit into stream
{
   if( !LineCounter && !(Offset % 8)){
      if( FirstRow){
         FirstRow = false;
      } else {
         fprintf( f, "                ");
      }
   }
   if( BitValue){
      Value |= 1 << (Offset % 8);
   }
   Offset++;
   if( !(Offset % 8)){
      // prepare new byte
      fprintf( f, "0x%02X,", Value);
      Value = 0;
      LineCounter++;
      if( LineCounter >= LINE_COUNT){
         LineCounter = 0;
         fprintf( f, "\n");
      }
   }
} // SaveBit

//******************************************************************************
// Max Width
//******************************************************************************

int __fastcall TMainForm::GetMaxWidth()
// Get maximal character width
{
   int TotalHeight = Image->Picture->Bitmap->Height;
   int Width  = Image->Picture->Bitmap->Width;
   for( int x = Width - 1; x >= 0; x--){
      for( int y = 0; y < TotalHeight; y++){
         if( Image->Picture->Bitmap->Canvas->Pixels[ x][ y] == COLOR_BLACK){
            return( x + 1);
         }
      }
   }
   return( 0);
} // GetMaxWidth

//******************************************************************************
// Get Width
//******************************************************************************

int __fastcall TMainForm::GetWidth( int ch)
// Get character width
{
   int Width  = Image->Picture->Bitmap->Width;
   int yBase  = ch * MaxHeight;
   for( int x = Width - 1; x >= 0; x--){
      for( int y = 0; y < MaxHeight; y++){
         if( Image->Picture->Bitmap->Canvas->Pixels[ x][ y + yBase] == COLOR_BLACK){
            return( x + 1);
         }
      }
   }
   return( 0);
} // GetWidth

