object MainForm: TMainForm
  Left = 418
  Top = 219
  Width = 586
  Height = 353
  Caption = 'Bitmap Convertor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImage
    Left = 232
    Top = 16
    Width = 320
    Height = 240
  end
  object Label1: TLabel
    Left = 40
    Top = 136
    Width = 34
    Height = 13
    Caption = 'Width :'
  end
  object Label2: TLabel
    Left = 40
    Top = 168
    Width = 37
    Height = 13
    Caption = 'Height :'
  end
  object LblWidth: TLabel
    Left = 112
    Top = 136
    Width = 6
    Height = 13
    Caption = '0'
  end
  object LblHeight: TLabel
    Left = 112
    Top = 168
    Width = 6
    Height = 13
    Caption = '0'
  end
  object BtnLoad: TButton
    Left = 48
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Load'
    TabOrder = 0
    OnClick = BtnLoadClick
  end
  object BtnSave: TButton
    Left = 48
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 1
    OnClick = BtnSaveClick
  end
  object EdtName: TEdit
    Left = 40
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '?'
  end
  object Cb4Colors: TCheckBox
    Left = 40
    Top = 240
    Width = 97
    Height = 17
    Caption = '4 Colors'
    TabOrder = 3
  end
  object OpenDialog: TOpenDialog
    Filter = 'obr|*.bmp'
    Left = 152
    Top = 24
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '*.c'
    FileName = 'Bitmap.c'
    Filter = 'Source|*.c'
    Left = 152
    Top = 72
  end
end
