//---------------------------------------------------------------------------

#ifndef FontGenH
#define FontGenH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <stdio.h>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image;
        TButton *BtnLoad;
        TButton *BtnSave;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *LblWidth;
        TLabel *LblHeight;
        TEdit *EdtName;
        TCheckBox *Cb4Colors;
        void __fastcall BtnLoadClick(TObject *Sender);
        void __fastcall BtnSaveClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);

protected :
int   Offset;      // bit offset
byte  Value;       // byte output value
int   LineCounter; // bytes per line
FILE *f;

void __fastcall TMainForm::InitBit();
// Init bit stream

void __fastcall TMainForm::SaveBit( bool BitValue);
// Save bit into stream

};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
