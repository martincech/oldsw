//******************************************************************************
//
//   MainUsb.cpp  Bat1 USB Update main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainUsb.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//---------------------------------------------------------------------------
#define USB_VEIT_NAME          "VEIT BAT1 Poultry Scale"
#define USB_VEIT_MANUFACTURER  "VEIT"

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Usb = new TUsbTester;
} // TMainForm

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   this->Enabled  = false;             // disable main window
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // StartCom

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   this->Enabled  = true;              // enable main window
   Screen->Cursor = crDefault;         // normal cursor
} // StopComm

//******************************************************************************
//  Upgrade
//******************************************************************************

void __fastcall TMainForm::BtnUpgradeClick(TObject *Sender)
{
   StartComm();
   Status() + "?";
   StatusBar->Refresh();
   // programming device :
   if( !Usb->WaitForProgrammedDevice()){
      Status() + "ERROR "+ Usb->Status;
      StopComm();
      return;
   }
   Status() + "Device " + Usb->AdapterName;
   StatusBar->Refresh();
   Sleep( 1000);          // show status
   if( !Usb->WriteEeprom( USB_VEIT_NAME, USB_VEIT_MANUFACTURER, false)){
      Status() + "ERROR " + Usb->Status;
      StopComm();
      return;
   }
   StopComm();
   Status() + "Upgrade OK";
} // BtnUpgradeClick

