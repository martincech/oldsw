//******************************************************************************
//
//   MasterCom.h      Master communication functions
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef MasterComH
#define MasterComH

#include "../Library/Serial/UART/ComUart.h"

//******************************************************************************
// Master COM
//******************************************************************************

class TMasterCom {
public :
   TMasterCom();
   ~TMasterCom();

   bool Open( char *ComName);
   // Open <ComName>
   void Flush();
   // Flush COM data

   bool TestUart();
   // Communication test

   bool TestDisplay();
   // Run display test
   bool TestMemory();
   // Run memory test
   bool TestRtc();
   // Run RTC test
   bool TestSound();
   // Run sound test

   bool ReadAdc( dword &Value);
   // Read external ADC
   bool ReadIadc( dword &Value);
   // Read internal ADC

   bool StartUsb();
   // Start USB test
   bool UsbResult();
   // Return USB test result

   bool EnableCharger( bool Enable);
   // <Enable>/disable charger
   bool TestPower( bool &PPR, bool &CHG, bool &USBON, bool &ENNAB);
   // Read power status
   bool SwitchOff();
   // Switch device OFF

   // properties
   AnsiString Status;
//------------------------------------------------------------------------------
protected :
   TComUart *FCom;

   bool StartTest( int Command);
   // Start test by <Command>
   bool TestResult( int Timeout);
   // Wait <Timeout> for the test result
   bool Test( int Command, int Timeout);
   // Run simple test by <Command>
   bool ReadHex( int Timeout, dword &Value);
   // Read hex reply
}; // TMasterCom

#endif
