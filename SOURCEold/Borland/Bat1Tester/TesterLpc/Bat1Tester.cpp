//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Tester.res");
USEFORM("Main.cpp", MainForm);
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("UsbTester.cpp");
USEUNIT("TestAdapter.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USEUNIT("MasterCom.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 Tester";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
