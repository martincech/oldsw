//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Crt/Crt.h"
#include "TestAdapter.h"
#include "UsbTester.h"
#include "MasterCom.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnStart;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnStartClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt          *Crt;
   TUsbTester    *Usb;
   TTestAdapter  *Adapter;
   TMasterCom    *Com;

   __fastcall TMainForm(TComponent* Owner);
   bool TesterDefaults();
   bool FtdiProgramming();
   bool WriteTestFirmware();
   bool MasterComTest();
   bool DeviceHardwareTest();
   bool AdcCalibrationTest();
   bool IadcCalibrationTest();
   bool PowerTest();
   bool WriteBatFirmware();
   bool TestDone();

   bool WriteFirmware( AnsiString FileName);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
