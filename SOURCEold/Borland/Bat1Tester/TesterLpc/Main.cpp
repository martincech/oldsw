//******************************************************************************
//
//   Main.cpp     Bat1 Tester main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Main.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Usb     = new TUsbTester;
   Adapter = new TTestAdapter;
   Com     = new TMasterCom;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt = new TCrt( MainMemo);
   if( !Com->Open( "COM1")){   //!!!
      Application->MessageBox( "Nelze otevrit COM", "Chyba", MB_OK);
      Close();
   }
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
//  Detect
//******************************************************************************

void __fastcall TMainForm::BtnStartClick(TObject *Sender)
{
   Screen->Cursor = crHourGlass;       // wait cursor
   if( !TesterDefaults()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !FtdiProgramming()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !WriteTestFirmware()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !MasterComTest()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !DeviceHardwareTest()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !AdcCalibrationTest()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !IadcCalibrationTest()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !PowerTest()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !WriteBatFirmware()){
      Screen->Cursor = crDefault;
      return;
   }
   if( !TestDone()){
      Screen->Cursor = crDefault;
      return;
   }
   Crt->printf( "QC PASSED !\n");
   Screen->Cursor = crDefault;         // normal cursor
   StatusOk();
} // BtnStartClick

//******************************************************************************
// Tester defaults
//******************************************************************************

bool TMainForm::TesterDefaults()
{
   Crt->printf( "Start...\n");
/*
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Adapter->ChargerConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Adapter->KeyboardOn( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Adapter->AccuVoltage( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Adapter->AdcVoltage( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
*/
   return( true);
} // TesterDefaults

//******************************************************************************
//  FTDI programming
//******************************************************************************

bool TMainForm::FtdiProgramming()
{
   //---------------------------------------------------------------------------
   Crt->printf( "FTDI programming...\n");
   // USB power on :
   if( !Adapter->UsbConnect( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   // programming device :
   if( !Usb->WaitForNewDevice()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   Crt->printf( "Device : %s\n", Usb->AdapterName.c_str());
   if( !Usb->WriteEeprom()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "FTDI driver installation...\n");
   // USB power off :
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDisconnect()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   if( !Adapter->UsbConnect( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDevice()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   Crt->printf( "Device : %s\n", Usb->AdapterName.c_str());
   Com->Flush();                       // garbage after reset
   return( true);
} // FtdiProgramming

//******************************************************************************
// Write test firmware
//******************************************************************************

bool TMainForm::WriteTestFirmware()
{
   return( WriteFirmware( "Data\\Bat1Test.hex"));
} // WriteTestFirmware

//******************************************************************************
// Master COM test
//******************************************************************************

bool TMainForm::MasterComTest()
{
   Crt->printf( "COM test...\n");
   if( !Com->TestUart()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   return( true);
} // MasterComTest

//******************************************************************************
// Device HW test
//******************************************************************************

bool TMainForm::DeviceHardwareTest()
{
   //---------------------------------------------------------------------------
   Crt->printf( "USB test...\n");
   if( !Com->StartUsb()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   if( !Usb->TestUart()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   if( !Com->UsbResult()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "DISPLAY test...\n");
   if( !Com->TestDisplay()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "MEMORY test...\n");
   if( !Com->TestMemory()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "RTC test...\n");
   if( !Com->TestRtc()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "SOUND test...\n");
   if( !Com->TestSound()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   return( true);
} // DeviceHardwareTest

//******************************************************************************
// ADC calibration test
//******************************************************************************

bool TMainForm::AdcCalibrationTest()
{
   dword Value;
   Crt->printf( "ADC test...\n");
   //---------------------------------------------------------------------------
   // test zero
   if( !Adapter->AdcVoltage( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   Sleep( 1000);
   if( !Com->ReadAdc( Value)){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "ADC : %08X\n", Value);
   //---------------------------------------------------------------------------
   // test max
   if( !Adapter->AdcVoltage( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   Sleep( 1000);
   if( !Com->ReadAdc( Value)){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "ADC : %08X\n", Value);
   //---------------------------------------------------------------------------
   // back to zero
   if( !Adapter->AdcVoltage( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   return( true);
} // AdcCalibrationTest

//******************************************************************************
// Internal ADC calibration test
//******************************************************************************

bool TMainForm::IadcCalibrationTest()
{
   dword Value;
   Crt->printf( "IADC test...\n");
   //---------------------------------------------------------------------------
   // test discharged
   if( !Adapter->AccuVoltage( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   Sleep( 1000);
   if( !Com->ReadIadc( Value)){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "IADC : %08X\n", Value);
   //---------------------------------------------------------------------------
   // test charged
   if( !Adapter->AccuVoltage( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   Sleep( 1000);
   if( !Com->ReadIadc( Value)){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "IsADC : %08X\n", Value);
   return( true);
} // IadcCalibrationTest

//******************************************************************************
// Power test
//******************************************************************************

bool TMainForm::PowerTest()
{
   bool PPR, CHG, USBON, ENNAB;
   //---------------------------------------------------------------------------
   Crt->printf( "ACCU test...\n");
   // disconnect USB power :
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDisconnect()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   if( !Com->TestPower( PPR, CHG, USBON, ENNAB)){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "PPR : %c, CHG : %c, USBON : %c\n",
                PPR   ? 'H' : 'L',
                CHG   ? 'H' : 'L',
                USBON ? 'H' : 'L');
   //---------------------------------------------------------------------------
   Crt->printf( "CHARGER test...\n");
   if( !Adapter->ChargerConnect( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Com->TestPower( PPR, CHG, USBON, ENNAB)){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "PPR : %c, CHG : %c, USBON : %c\n",
                PPR   ? 'H' : 'L',
                CHG   ? 'H' : 'L',
                USBON ? 'H' : 'L');
   if( !Adapter->ChargerConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "USB CHARGER test...\n");
   if( !Adapter->UsbConnect( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDevice()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   Sleep( 2000);
   Com->Flush();                       // garbage after reset
   if( !Com->TestPower( PPR, CHG, USBON, ENNAB)){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "PPR : %c, CHG : %c, USBON : %c\n",
                PPR   ? 'H' : 'L',
                CHG   ? 'H' : 'L',
                USBON ? 'H' : 'L');
   return( true);
} // PowerTest

//******************************************************************************
// Write BAT firmware
//******************************************************************************

bool TMainForm::WriteBatFirmware()
{
   return( WriteFirmware( "Data\\Bat1.hex"));
} // WriteBatFirmware

//******************************************************************************
// Test done
//******************************************************************************

bool TMainForm::TestDone()
{
   Crt->printf( "BAT1 test...\n");
   // switch power on :
   if( !Adapter->KeyboardOn( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   Sleep( 500);
   if( !Adapter->KeyboardOn( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   Sleep( 5000);                       // wait scale ready
   // switch power off :
   if( !Adapter->KeyboardOn( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   Sleep( 3000);
   if( !Adapter->KeyboardOn( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   // disconnect USB power :
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   return( true);
} // TestDone

//******************************************************************************
// Write Firmware
//******************************************************************************

bool TMainForm::WriteFirmware( AnsiString FileName)
{
   Crt->printf( "FIRMWARE Write : %s\n", FileName.c_str());
   char Buffer[ 128];
   sprintf( Buffer, "lpc21isp -control %s com9 19200 18432", FileName.c_str());
   if( system( Buffer) != 0){
      Crt->printf( "ERROR : Unable run programming");
      return( false);
   }
   Sleep( 2000);                       // wait for reset
   Com->Flush();                       // clear garbage
   return( true);
} // WriteFirmware

