//******************************************************************************
//
//   TestAdapter.h    Tester adapter functions
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef TestAdapterH
#define TestAdapterH

//******************************************************************************
// Tester adapter
//******************************************************************************

class TTestAdapter {
public :
   TTestAdapter();
   ~TTestAdapter();

   //bool Open();
   // Open connection to adapter

   bool UsbConnect( bool Connect);
   // <Connect>/disconnect USB power & data
   bool ChargerConnect( bool Connect);
   // <Connect>/disconnect charger
   bool KeyboardOn( bool Press);
   // <Press>/release keyboard
   bool AccuVoltage( bool Charged);
   // Set accumulator voltage to <Charged>
   bool AdcVoltage( bool High);
   // Set ADC voltage to <High>/zero

//------------------------------------------------------------------------------
protected :
}; // TTestAdapter

#endif
