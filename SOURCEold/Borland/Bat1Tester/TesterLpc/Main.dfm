object MainForm: TMainForm
  Left = 559
  Top = 304
  Width = 670
  Height = 646
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 Tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainMemo: TMemo
    Left = 0
    Top = 48
    Width = 497
    Height = 537
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 600
    Width = 662
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnStart: TButton
    Left = 520
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 2
    OnClick = BtnStartClick
  end
end
