//******************************************************************************
//
//   UsbTester.h   USB tester functions
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef UsbTesterH
#define UsbTesterH

#include "../Library/Serial/USB/UsbUart.h"

//******************************************************************************
// USB Tester
//******************************************************************************

class TUsbTester {
public :
   TUsbTester();
   ~TUsbTester();

   bool WaitForNewDevice();
   // Wait for empty device (leave opened)
   bool WaitForDevice();
   // Wait for programmed device (leave closed)
   bool WaitForDisconnect();
   // Wait for disconnected device (leave closed)

   bool WriteEeprom();
   // Write device EEPROM
   bool TestUart();
   // Test device UART

   // properties
   AnsiString AdapterName;
   AnsiString Status;

//------------------------------------------------------------------------------
protected :
   FT_HANDLE   FHandle;   // raw FTDI handle

   bool Detect( bool NewDevice, bool ProgrammedDevice);
   // Search and open device
   bool Wait( bool NewDevice, bool ProgrammedDevice, int Timeout);
   // Wait for device
}; // TUsbTester

#endif
