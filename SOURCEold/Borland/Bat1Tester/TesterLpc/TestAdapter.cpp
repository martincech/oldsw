//******************************************************************************
//
//   TestAdapter.cpp  Tester adapter functions
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "../Library/Unisys/Uni.h"
#include "TestAdapter.h"

#pragma package(smart_init)

//******************************************************************************
// Constructor
//******************************************************************************

TTestAdapter::TTestAdapter()
{
} // TTestAdapter

//******************************************************************************
// Destructor
//******************************************************************************

TTestAdapter::~TTestAdapter()
{
} // ~TTestAdapter

//******************************************************************************
// USB connect
//******************************************************************************

bool TTestAdapter::UsbConnect( bool Connect)
// <Connect>/disconnect USB power & data
{
   if( Connect){
      Application->MessageBox( "Pripojte USB", "Informace", MB_OK);
   } else {
      Application->MessageBox( "Odpojte USB", "Informace", MB_OK);
   }
   return( true);
} // UsbConnect

//******************************************************************************
// Charger connect
//******************************************************************************

bool TTestAdapter::ChargerConnect( bool Connect)
// <Connect>/disconnect charger
{
   if( Connect){
      Application->MessageBox( "Pripojte nabijec", "Informace", MB_OK);
   } else {
      Application->MessageBox( "Odpojte nabijec", "Informace", MB_OK);
   }
   return( true);
} // ChargerConnect

//******************************************************************************
// Keyboard ON
//******************************************************************************

bool TTestAdapter::KeyboardOn( bool Press)
// <Press>/release keyboard
{
   if( Press){
      Application->MessageBox( "Stisknete klavesu ON", "Informace", MB_OK);
   } else {
      Application->MessageBox( "Uvolnene klavesu ON", "Informace", MB_OK);
   }
   return( true);
} // KeyboardOn

//******************************************************************************
// Accu voltage
//******************************************************************************

bool TTestAdapter::AccuVoltage( bool Charged)
// Set accumulator voltage to <Charged>
{
   if( Charged){
      Application->MessageBox( "Pripojte nabitou baterii", "Informace", MB_OK);
   } else {
      Application->MessageBox( "Pripojte vybitou baterii", "Informace", MB_OK);
   }
   return( true);
} // AccuVoltage

//******************************************************************************
// Adc voltage
//******************************************************************************

bool TTestAdapter::AdcVoltage( bool High)
// Set ADC voltage to <High>/zero
{
   if( High){
      Application->MessageBox( "Sundejte propojku mustku", "Informace", MB_OK);
   } else {
      Application->MessageBox( "Nasadte propojku mustku", "Informace", MB_OK);
   }
   return( true);
} // AdcVoltage

