//******************************************************************************
//
//   UsbTester.cpp    USB tester functions
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "../Library/Unisys/Uni.h"
#include "UsbTester.h"

#pragma package(smart_init)

#define if_fterr( status)  if( (status) != FT_OK)

#define USB_FTDI_NAME    "USB HS Serial Converter"
#define USB_VEIT_NAME    "VEIT Bat1 Poultry Scale"

#define USB_CONNECT_DELAY 5      // wait for device connect/disconnect [s]
#define USB_TIMEOUT       1000   // wait for UART reply

//******************************************************************************
// Constructor
//******************************************************************************

TUsbTester::TUsbTester()
{
   FHandle = INVALID_HANDLE_VALUE;
   AdapterName = "?";
   Status      = "No device";
} // TUsbTester

//******************************************************************************
// Destructor
//******************************************************************************

TUsbTester::~TUsbTester()
{
   if_okh( FHandle){
      FT_Close( FHandle);
   }
} // ~TUsbTester

//******************************************************************************
// Wait for Device
//******************************************************************************

bool TUsbTester::WaitForNewDevice()
// Wait for empty device (leave opened)
{
   return( Wait( true, true, USB_CONNECT_DELAY));
} // WaitForNewDevice

bool TUsbTester::WaitForDevice()
// Wait for programmed device (leave closed)
{
   if( !Wait( false, true, USB_CONNECT_DELAY)){
      return( false);
   }
   if_errh( FHandle){
      IERROR;
   }
   // pulse reset :
   FT_ClrRts( FHandle);                // clear P0.14
   FT_SetDtr( FHandle);                // do Reset
   Sleep( 100);                        // reset pulse width
   FT_ClrDtr( FHandle);                // clear Reset
   // close device :
   FT_Close( FHandle);
   FHandle = INVALID_HANDLE_VALUE;
   return( true);
} // WaitForDevice

bool TUsbTester::WaitForDisconnect()
// Wait for disconnected device (leave closed)
{
   for( int i = 0; i < USB_CONNECT_DELAY; i++){
      if( !Detect( true, true)){
         return( true);
      }
      Sleep( 1000);
   }
   Status = "Device connected";
   return( false);
} // WaitForDisconnect

//******************************************************************************
// Write EEPROM
//******************************************************************************

bool TUsbTester::WriteEeprom()
// Write device EEPROM
{
   if_errh( FHandle){
      IERROR;
   }
   // prepare buffers :
   FT_PROGRAM_DATA PgmData;
   char ManufacturerBuf[32];
   char ManufacturerIdBuf[16];
   char DescriptionBuf[64];
   char SerialNumberBuf[16];

   PgmData.Signature1     = 0x00000000;
   PgmData.Signature2     = 0xffffffff;
   PgmData.Version        = 2;                  // FT232R extensions
   PgmData.Manufacturer   = ManufacturerBuf;
   PgmData.ManufacturerId = ManufacturerIdBuf;
   PgmData.Description    = DescriptionBuf;
   PgmData.SerialNumber   = SerialNumberBuf;
   // read EEPROM :
   if_fterr( FT_EE_Read( FHandle, &PgmData)){
      FT_Close( FHandle);
      FHandle     = INVALID_HANDLE_VALUE;
      Status      = "FTDI read error";
      AdapterName = "?";
      return( false);
   }
   // new values :
   PgmData.Signature1   = 0x00000000;
   PgmData.Signature2   = 0xffffffff;
   PgmData.Version      = 2;			// FT232R extensions
   PgmData.VendorId     = 0x0403;
   PgmData.ProductId    = 0x6012;
   strcpy( PgmData.Manufacturer,   "VEIT");
   strcpy( PgmData.ManufacturerId, "00");
   strcpy( PgmData.Description,    USB_VEIT_NAME);
   strcpy( PgmData.SerialNumber,   "00000001");
   PgmData.MaxPower     = 0;
   PgmData.PnP          = 0;
   PgmData.SelfPowered  = 1;
   PgmData.RemoteWakeup = 0;
   // revision 4
   PgmData.Rev4              = 0;
   PgmData.IsoIn             = 0;
   PgmData.IsoOut            = 0;
   PgmData.PullDownEnable    = 0;
   PgmData.SerNumEnable      = 0;
   PgmData.USBVersionEnable  = 0;
   PgmData.USBVersion        = 0x0110;
   // FT2232C extensions
   PgmData.Rev5              = 0;
   PgmData.IsoInA            = 0;
   PgmData.IsoInB            = 0;
   PgmData.IsoOutA           = 0;
   PgmData.IsoOutB           = 0;
   PgmData.PullDownEnable5   = 0;
   PgmData.SerNumEnable5     = 0;
   PgmData.USBVersionEnable5 = 0;
   PgmData.USBVersion5       = 0x0110;
   PgmData.AIsHighCurrent    = 0;
   PgmData.BIsHighCurrent    = 0;
   PgmData.IFAIsFifo         = 0;
   PgmData.IFAIsFifoTar      = 0;
   PgmData.IFAIsFastSer      = 0;
   PgmData.AIsVCP            = 0;
   PgmData.IFBIsFifo         = 0;
   PgmData.IFBIsFifoTar      = 0;
   PgmData.IFBIsFastSer      = 0;
   PgmData.BIsVCP            = 0;
   // FT232R extensions
   PgmData.UseExtOsc         = 0;
   PgmData.HighDriveIOs      = 0;
   PgmData.EndpointSize      = 64;
   PgmData.PullDownEnableR   = 0;
   PgmData.SerNumEnableR     = 1;
   PgmData.InvertTXD         = 0;
   PgmData.InvertRXD         = 0;
   PgmData.InvertRTS         = 0;
   PgmData.InvertCTS         = 0;
   PgmData.InvertDTR         = 0;
   PgmData.InvertDSR         = 0;
   PgmData.InvertDCD         = 0;
   PgmData.InvertRI          = 0;
   PgmData.Cbus0             = 3;
   PgmData.Cbus1             = 2;
   PgmData.Cbus2             = 0;
   PgmData.Cbus3             = 1;
   PgmData.Cbus4             = 5;
   PgmData.RIsVCP            = 0;
   // write EEPROM :
   if_fterr( FT_EE_Program( FHandle, &PgmData)){
      FT_Close( FHandle);
      FHandle     = INVALID_HANDLE_VALUE;
      Status      = "FTDI programming error";
      AdapterName = "?";
      return( false);
   }
   Status      = "OK";
   AdapterName = "?";
   // new detection :
   FT_Close( FHandle);
   FHandle     = INVALID_HANDLE_VALUE;
   return( true);
} // WriteEeprom

//******************************************************************************
// Test UART
//******************************************************************************

#define TEST_DATA_SIZE 256

bool TUsbTester::TestUart()
// Test device UART
{
   TIdentifier Identifier;
   TUsbUart *Usb = 0;
   // USB setup
   Usb = new TUsbUart;
   if( !Usb->Locate( USB_VEIT_NAME, Identifier)){
      delete Usb;
      Status      = "UART Locate error";
      return( false);
   }
   if( !Usb->Open( Identifier)){
      delete Usb;
      Status      = "UART Open error";
      return( false);
   }
   // default parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate     = 38400;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   Usb->SetParameters( Parameters);
   // common init :
   Usb->RTS = false;
   Usb->DTR = false;
   Usb->SetRxWait( USB_TIMEOUT, 0);
   Usb->Flush();
   byte Buffer[ TEST_DATA_SIZE];
   // prepare data :
   for( int i = 0; i < TEST_DATA_SIZE; i++){
      Buffer[ i] = i;
   }
   // send data :
   if( Usb->Write( Buffer, TEST_DATA_SIZE) != TEST_DATA_SIZE){
      Usb->Close();
      Status      = "UART send error";
      return( false);
   }
   // receive reply :
   if( Usb->Read( Buffer, TEST_DATA_SIZE) != TEST_DATA_SIZE){
      Usb->Close();
      Status      = "UART receive timeout";
      return( false);
   }
   // verify data :
   for( int i = 0; i < TEST_DATA_SIZE; i++){
      if( Buffer[ i] != (byte)i){
         Usb->Close();
         Status      = "UART Rx data mismatch";
         return( false);
      }
   }
   Usb->Close();
   return( true);
} // TestUart

//------------------------------------------------------------------------------

//******************************************************************************
// Detection
//******************************************************************************

bool TUsbTester::Detect( bool NewDevice, bool ProgrammedDevice)
// Search and open device
{
   if_okh( FHandle){
      DWORD Size;
      if_fterr( FT_GetQueueStatus( FHandle, &Size)){
         FT_Close( FHandle);
         FHandle     = INVALID_HANDLE_VALUE;
         Status      = "No device";
         AdapterName = "?";
         return( false);
      }
      return( true);
   }
   // Get devices count :
   DWORD      NumDevices;
   if_fterr( FT_ListDevices( &NumDevices, NULL, FT_LIST_NUMBER_ONLY)){
      Status      = "FTDI list error";
      AdapterName = "?";
      return( false);
   }
   if( NumDevices == 0){
      Status      = "No device";
      AdapterName = "?";
      return( false);
   }
   // Search for device :
   char  DeviceName[ 256];     // device name
   DWORD UsbIndex = -1;        // index of the found
   for( DWORD i = 0; i < NumDevices; i++){
      if_fterr( FT_ListDevices( (PVOID)i, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
         continue;
      }
      if( ProgrammedDevice && strequ( DeviceName, USB_VEIT_NAME)){
         UsbIndex = i;
         break;
      }
      if( NewDevice && strequ( DeviceName, USB_FTDI_NAME)){
         UsbIndex = i;
         break;
      }
   }
   if( UsbIndex == -1){
      Status      = "No device";
      AdapterName = "?";
      return( false);
   }
   if_fterr( FT_Open( UsbIndex, &FHandle)){
      Status      = "FTDI open error";
      AdapterName = "?";
      return( false);
   }
   Status      = "OK";
   AdapterName = DeviceName;
   return( true);
} // Detect

//******************************************************************************
// Wait
//******************************************************************************

bool TUsbTester::Wait( bool NewDevice, bool ProgrammedDevice, int Timeout)
// Wait for device
{
   for( int i = 0; i < Timeout; i++){
      if( Detect( NewDevice, ProgrammedDevice)){
         return( true);
      }
      Sleep( 1000);
   }
   return( false);
} // Wait

