//******************************************************************************
//
//   TestAdapter.h    Tester adapter functions
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef TestAdapterH
#define TestAdapterH

#include "../Library/Serial/UART/ComUart.h"

// Power mode :
typedef enum {
   POWER_OFF,
   POWER_NORMAL,
   POWER_INVERTED
} TPowerMode;

#define VOLTAGE_STEPS 32         // max. number of voltage steps

//******************************************************************************
// Tester adapter
//******************************************************************************

class TTestAdapter {
public :
   TTestAdapter();
   ~TTestAdapter();

   bool Open( char *ComName);
   // Open tester COM

   bool VoltageMin( void);
   // Power voltage up
   bool VoltageUp( void);
   // Power voltage up
   bool VoltageDown( void);
   // Power voltage up
   bool VoltageMax( void);
   // Power voltage up

   bool SetPower( int Mode);
   // Set power <Mode>

   bool SetCurrent( bool Sensitive);
   // Set <Sensitive> current measuring

   bool Bridge( bool Maximum);
   // Set bridge voltage to <Maximum> or zero


   bool KeyboardOn( bool Press);
   // <Press>/release keyboard
   bool KeyboardK0( bool Press);
   // <Press>/release keyboard
   bool KeyboardK1( bool Press);
   // <Press>/release keyboard
   bool KeyboardK2( bool Press);
   // <Press>/release keyboard

   bool UsbConnect( bool Connect);
   // <Connect>/disconnect USB power & data
   bool ChargerConnect( bool Connect);
   // <Connect>/disconnect charger

   bool GetVoltage( int &Voltage);
   // Get voltage [mV]
   bool GetVoltageInverted( int &Voltage);
   // Get reverse voltage [mV]
   bool GetCurrent( int &Current);
   // Get current [0.01uA]/[mA]
   bool GetCurrentInverted( int &Current);
   // Get reverse current [0.01uA]/[mA]

   bool SetDefaults();
   // Set default values

// properties
   AnsiString Status;                  // operation status
   bool       SensitiveCurrent;        // sensitive current measuring

//------------------------------------------------------------------------------

protected :
   TComUart *FCom;

   bool SendCommand( byte Command);
   // Send command with reply

   bool SendMeasuring( byte Command, int &Value);
   // Read measured value
}; // TTestAdapter

#endif
