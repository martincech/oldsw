object MainForm: TMainForm
  Left = 328
  Top = 193
  Width = 767
  Height = 681
  Caption = 'Bat1 tester calibration'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 543
    Top = 29
    Width = 36
    Height = 13
    Caption = 'Printer :'
  end
  object Label2: TLabel
    Left = 545
    Top = 61
    Width = 36
    Height = 13
    Caption = 'Tester :'
  end
  object LblVoltage: TLabel
    Left = 664
    Top = 304
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblCurrent: TLabel
    Left = 664
    Top = 328
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblAdcZeroVoltage: TLabel
    Left = 624
    Top = 192
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblAdcMaxVoltage: TLabel
    Left = 624
    Top = 209
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblIadcLowVoltage: TLabel
    Left = 624
    Top = 233
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblIadcHighVoltage: TLabel
    Left = 624
    Top = 252
    Width = 6
    Height = 13
    Caption = '?'
  end
  object MainMemo: TMemo
    Left = 8
    Top = 24
    Width = 497
    Height = 593
    TabOrder = 0
  end
  object CbPrinterPort: TComboBox
    Left = 608
    Top = 24
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object CbTesterPort: TComboBox
    Left = 608
    Top = 56
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object BtnOpen: TButton
    Left = 536
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 3
    OnClick = BtnOpenClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 635
    Width = 759
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnAdcTest: TButton
    Left = 536
    Top = 200
    Width = 75
    Height = 25
    Caption = 'ADC test'
    TabOrder = 5
    OnClick = BtnAdcTestClick
  end
  object BtnIadcTest: TButton
    Left = 536
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Int ADC test'
    TabOrder = 6
    OnClick = BtnIadcTestClick
  end
  object BtnNormal: TButton
    Left = 536
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Normal'
    TabOrder = 7
    OnClick = BtnNormalClick
  end
  object BtnDark: TButton
    Left = 536
    Top = 344
    Width = 75
    Height = 25
    Caption = 'Dark'
    TabOrder = 8
    OnClick = BtnDarkClick
  end
  object BtnUsbPowerOn: TButton
    Left = 536
    Top = 152
    Width = 75
    Height = 25
    Caption = 'USB power on'
    TabOrder = 9
    OnClick = BtnUsbPowerOnClick
  end
end
