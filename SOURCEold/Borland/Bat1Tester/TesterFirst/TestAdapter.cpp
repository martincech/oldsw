//******************************************************************************
//
//   TestAdapter.cpp  Tester adapter functions
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>

#include "../Library/Unisys/Uni.h"
#include "../AVRGCC/Bat1Tester/BatBoard.h"

#include "TestAdapter.h"

#pragma package(smart_init)

#define TRIALS       3          // command trials
#define RX_TIMEOUT   500        // Rx timeout

//******************************************************************************
// Constructor
//******************************************************************************

TTestAdapter::TTestAdapter()
{
   FCom = 0;
   SensitiveCurrent = false;
} // TTestAdapter

//******************************************************************************
// Destructor
//******************************************************************************

TTestAdapter::~TTestAdapter()
{
   if( FCom){
      FCom->Close();
      delete FCom;
      FCom = 0;
   }
} // ~TTestAdapter

//******************************************************************************
// Open
//******************************************************************************

bool TTestAdapter::Open( char *ComName)
// Open tester COM
{
   if( FCom){
      FCom->Close();
   }
   TIdentifier Identifier;
   // COM setup
   FCom = new TComUart;
   if( !FCom->Locate( ComName, Identifier)){
      delete FCom;
      FCom = 0;
      Status      = "COM Locate error";
      return( false);
   }
   if( !FCom->Open( Identifier)){
      delete FCom;
      FCom = 0;
      Status      = "COM Open error";
      return( false);
   }
   // default parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate     = 9600;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   FCom->SetParameters( Parameters);
   // common init :
   FCom->SetRxWait( RX_TIMEOUT, 0);
   FCom->Flush();
   return( true);
} // Open

//******************************************************************************
// Min
//******************************************************************************

bool TTestAdapter::VoltageMin( void)
// Power voltage up
{
   return( SendCommand( BB_FLOOR));
} // VoltageMin

//******************************************************************************
// Up
//******************************************************************************

bool TTestAdapter::VoltageUp( void)
// Power voltage up
{
   return( SendCommand( BB_UP));
} // VoltageUp

//******************************************************************************
// Down
//******************************************************************************

bool TTestAdapter::VoltageDown( void)
// Power voltage up
{
   return( SendCommand( BB_DOWN));
} // VoltageDown

//******************************************************************************
// Max
//******************************************************************************

bool TTestAdapter::VoltageMax( void)
// Power voltage up
{
   return( SendCommand( BB_CEIL));
} // VoltageMax

//******************************************************************************
// Set power
//******************************************************************************

bool TTestAdapter::SetPower( int Mode)
// Set power <Mode>
{
   // switch both off :
   if( !SendCommand( BB_INVERT_OFF)){
      return( false);
   }
   if( !SendCommand( BB_NORMAL_OFF)){
      return( false);
   }
   // switch right on :
   switch( Mode){
      case POWER_OFF :
         break;        // done

      case POWER_NORMAL :
         if( !SendCommand( BB_NORMAL_ON)){
            return( false);
         }
         break;

      case POWER_INVERTED :
         if( !SendCommand( BB_INVERT_ON)){
            return( false);
         }
         break;
   }
   return( true);
} // SetPower

//******************************************************************************
// Set current
//******************************************************************************

bool TTestAdapter::SetCurrent( bool Sensitive)
// Set <Sensitive> current measuring
{
   SensitiveCurrent = Sensitive;        // remember
   if( Sensitive){
      return( SendCommand( BB_CURRENT_SENSITIVE));
   } else {
      return( SendCommand( BB_CURRENT_NORMAL));
   }
} // SetCurrent

//******************************************************************************
// Bridge
//******************************************************************************

bool TTestAdapter::Bridge( bool Maximum)
// Set bridge voltage to <Maximum> or zero
{
   if( Maximum){
      return( SendCommand( BB_BRIDGE_MAX));
   } else {
      return( SendCommand( BB_BRIDGE_ZERO));
   }
} // Bridge

//******************************************************************************
// Keyboard ON
//******************************************************************************

bool TTestAdapter::KeyboardOn( bool Press)
// <Press>/release keyboard
{
   if( Press){
      return( SendCommand( BB_KBD_ON));
   } else {
      return( SendCommand( BB_KBD_RELEASE));
   }
} // KeyboardOn

//******************************************************************************
// Keybard K0
//******************************************************************************

bool TTestAdapter::KeyboardK0( bool Press)
// <Press>/release keyboard
{
   if( Press){
      return( SendCommand( BB_KBD_K0));
   } else {
      return( SendCommand( BB_KBD_RELEASE));
   }
} // KeyboardK0

//******************************************************************************
// Keyboard K1
//******************************************************************************

bool TTestAdapter::KeyboardK1( bool Press)
// <Press>/release keyboard
{
   if( Press){
      return( SendCommand( BB_KBD_K1));
   } else {
      return( SendCommand( BB_KBD_RELEASE));
   }
} // KeyboardK1

//******************************************************************************
// Keyboard K2
//******************************************************************************

bool TTestAdapter::KeyboardK2( bool Press)
// <Press>/release keyboard
{
   if( Press){
      return( SendCommand( BB_KBD_K2));
   } else {
      return( SendCommand( BB_KBD_RELEASE));
   }
} // KeyboardK2

//******************************************************************************
// USB connect
//******************************************************************************

bool TTestAdapter::UsbConnect( bool Connect)
// <Connect>/disconnect USB power & data
{
   if( Connect){
      return( SendCommand( BB_USB_ON));
   } else {
      return( SendCommand( BB_USB_OFF));
   }
} // UsbConnect

//******************************************************************************
// Charger connect
//******************************************************************************

bool TTestAdapter::ChargerConnect( bool Connect)
// <Connect>/disconnect charger
{
   if( Connect){
      return( SendCommand( BB_CHARGER_ON));
   } else {
      return( SendCommand( BB_CHARGER_OFF));
   }
} // ChargerConnect

//******************************************************************************
// Get voltage
//******************************************************************************

bool TTestAdapter::GetVoltage( int &Voltage)
// Get voltage [mV]
{
   int RawVoltage;
   if( !SendMeasuring( BB_VOLTAGE, RawVoltage)){
      return( false);
   }
   Voltage = (RawVoltage * 5000) / 4096;
   return( true);
} // GetVoltage

//******************************************************************************
// Get reverse voltage
//******************************************************************************

bool TTestAdapter::GetVoltageInverted( int &Voltage)
// Get reverse voltage [mV]
{
   int RawVoltage;
   if( !SendMeasuring( BB_VOLTAGE_MINUS, RawVoltage)){
      return( false);
   }
   Voltage = (RawVoltage * 5000) / 4096;
   return( true);
} // GetVoltageInverted

//******************************************************************************
// Get current
//******************************************************************************

bool TTestAdapter::GetCurrent( int &Current)
// Get current [0.01uA]/[mA]
{
   int RawCurrent;
   if( !SendMeasuring( BB_CURRENT_DPLUS, RawCurrent)){
      return( false);
   }
   if( SensitiveCurrent){
      Current = (RawCurrent * (500000 / 22)) / 4096; // 0.01uA, 22kOhm
   } else {
      Current = (RawCurrent * 10000) / 4096;         // 1mA, 0.5Ohm
   }
   return( true);
} // GetCurrent

//******************************************************************************
// Get current
//******************************************************************************

bool TTestAdapter::GetCurrentInverted( int &Current)
// Get reverse current [0.01uA]/[mA]
{
   int RawCurrent;
   if( !SendMeasuring( BB_CURRENT, RawCurrent)){
      return( false);
   }
   if( SensitiveCurrent){
      Current = (RawCurrent * (500000 / 22)) / 4096; // 0.01uA, 22kOhm
   } else {
      Current = (RawCurrent * 10000) / 4096;         // 1mA, 0.5Ohm
   }
   return( true);
} // GetCurrent

//******************************************************************************
// Defaults
//******************************************************************************

bool TTestAdapter::SetDefaults()
// Set default values
{
   SensitiveCurrent = false;
   if( !SendCommand( BB_SHUTDOWN)){
      return( false);
   }
   return( VoltageMin());
} // SetDefaults

//------------------------------------------------------------------------------

//******************************************************************************
// Send command
//******************************************************************************

bool TTestAdapter::SendCommand( byte Command)
// Send command with reply
{
   byte Buffer;
   bool Ok = false;
   for( int i = 0; i < TRIALS; i++){
      FCom->Flush();
      if( FCom->Write( &Command, 1) != 1){
         Status      = "COM send error";
         continue;
      }
      if( FCom->Read( &Buffer, 1) != 1){
         Status      = "COM receive timeout";
         continue;
      }
      if( Buffer != Command){
         Status      = "COM wrong reply";
         continue;
      }
      Status = "OK";
      Ok     = true;
      break;
   }
   return( Ok);
} // SendCommand

//******************************************************************************
// Send measuring
//******************************************************************************

bool TTestAdapter::SendMeasuring( byte Command, int &Value)
// Read measured value
{
   byte Buffer[ 8];
   bool Ok = false;
   for( int i = 0; i < TRIALS; i++){
      FCom->Flush();
      if( FCom->Write( &Command, 1) != 1){
         Status      = "COM send error";
         continue;
      }
      // signum + 4 hex digits :
      if( FCom->Read( &Buffer, 5) != 5){
         Status      = "COM receive timeout";
         continue;
      }
      Buffer[ 5] = '\0';
      int RawValue;
      sscanf( &Buffer[1], "%x", &RawValue);
      if( Buffer[0] == '-'){
         Value = -RawValue;
      } else {
         Value =  RawValue;
      }
      Ok = true;
      break;
   }
   return( Ok);
} // SendMeasuring

