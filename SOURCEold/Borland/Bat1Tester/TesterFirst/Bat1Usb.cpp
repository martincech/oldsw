//******************************************************************************
//
//   Bat1Usb.cpp      Bat1 USB communication
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Bat1Usb.h"

// Bat1 definitions :
#include "../Library/Unisys/Dt.h"
#include "../Bat1Reader/MemoryDef.h"
#include "../Bat1Reader/SdbDef.h"
#include "../Bat1Reader/Bat1.h"
#include "../Bat1Reader/BeepDef.h"
#include "../Bat1Reader/ConfigSet.c"  // factory default config

// Color transformation :
#define COLOR_BLACK      0x00000000
#define COLOR_MIDDLEGRAY 0x007F7F7F
#define COLOR_WHITE      0x00FFFFFF
#define COLOR_LIGHTGRAY  0x00555555
#define COLOR_DARKGRAY   0x00AAAAAA

//******************************************************************************
// Constructor
//******************************************************************************

TBat1Usb::TBat1Usb()
{
   TPktAdapter *PAdapter = new TPktAdapter;
   Bat          = new TBatDevice;
   Bat->Adapter = PAdapter;
} // TBat1Usb

//******************************************************************************
// Destructor
//******************************************************************************

TBat1Usb::~TBat1Usb()
{
   if( !Bat){
      return;
   }
   if( Bat->Adapter){
      delete Bat->Adapter;
   }
   delete Bat;
} // ~TBat1Usb

//******************************************************************************
// Default configuration
//******************************************************************************

void TBat1Usb::DefaultConfig()
// Prepare factory defaults
{
   memset( &BatConfig, 0, sizeof( BatConfig));
   BatConfig.Config = _DefaultConfig;  // copy default configuration
} // DefaultConfig

//******************************************************************************
// Prepare logo
//******************************************************************************

void TBat1Usb::PrepareLogo( TImage *Image)
// Prepare logo from <Image>
{
   // prepare data :
   memset( Logo, 0, sizeof( Logo));
   for( int x = 0; x < LOGO_WIDTH; x++){
      for( int y = 0; y < LOGO_HEIGHT; y ++){
         TColor Color = Image->Picture->Bitmap->Canvas->Pixels[ x][ y];
         int Yaddr = y / 8;
         int Ymask = 1 << (y % 8);
         if( Color == COLOR_WHITE){
            continue;
         }
         // plane 0
         if( Color == COLOR_BLACK || Color >= COLOR_MIDDLEGRAY){ // light gray
            Logo[ 0][ Yaddr][x] |= Ymask;
         }
         // plane 1
         if( Color == COLOR_BLACK || Color <  COLOR_MIDDLEGRAY){ // dark gray
            Logo[ 1][ Yaddr][x] |= Ymask;
         }
      }
   }
} // PrepareLogo

//******************************************************************************
// Get version
//******************************************************************************

AnsiString TBat1Usb::GetVersion()
// Returns version string
{
   AnsiString Version;
   Version.printf( "%d.%02d.%d", (BatConfig.Config.Version >> 8) & 0xFF,
                                  BatConfig.Config.Version       & 0xFF,
                                  BatConfig.Config.Build);
   return( Version);
} // GetVersion

//******************************************************************************
// Is connected
//******************************************************************************

bool TBat1Usb::IsConnected()
// Check for Bat1 presence
{
   // find device :
   bool BatFound = false;
   for( int i = 0; i < 5; i++){
      if( Bat->Check()){
         BatFound = true;
         break;
      }
   }
   return( BatFound);
} // IsConnected

//******************************************************************************
// Set time
//******************************************************************************

bool TBat1Usb::SetTime()
// Set Bat1 time by PC
{
   // get current date/time :
   TDateTime Dt = TDateTime::CurrentDateTime();
   unsigned short Day, Month, Year, Hour, Min, Sec, Msec;
   Dt.DecodeDate( &Year, &Month, &Day);
   Dt.DecodeTime( &Hour, &Min, &Sec, &Msec);
   // internal representation
   TLocalTime Local;
   Local.Day   = Day;
   Local.Month = Month;
   Local.Year  = Year - 2000;
   Local.Hour  = Hour;
   Local.Min   = Min;
   Local.Sec   = Sec;
   TTimestamp ts = DtEncode( &Local);
   if( !Bat->SetTime( ts)){
      return( false);
   }
   return( true);
} // SetTime

//******************************************************************************
// Format filesystem
//******************************************************************************

bool TBat1Usb::FormatFilesystem()
// Format Bat1 filesystem
{
   return( Bat->FormatFilesystem());
} // FormatFilesystem

//******************************************************************************
// Create file
//******************************************************************************

bool TBat1Usb::CreateFile( char *FileName)
// Create <FileName>
{
   int Handle;
   if( !Bat->FileCreate( FileName, "", SDB_CLASS, Handle)){
      Bat->FileClose();
      return( false);
   }
   if( !Bat->FileWrite( &BatConfig.Config.WeighingParameters, sizeof( TSdbConfig))){
       Bat->FileClose();
       return( false);
   }
   if( !Bat->FileClose()){
      return( false);
   }
   BatConfig.Config.LastFile = Handle; // active file
   return( true);
} // CreateFile

//******************************************************************************
// Write config
//******************************************************************************

bool TBat1Usb::WriteConfig()
// Write current configuration to Bat1
{
byte       *p;
TConfigCrc Crc;

   // update CRC :
   Crc  = 0;
   p   = (byte *)&BatConfig;
   for( int i = 0; i < CONFIG_DATA_SIZE; i++){
      Crc += *p;
      p++;
   }
   BatConfig.Spare.CheckSum = ~Crc;
   // write configuration :
   if( !Bat->WriteMemory( offsetof( TEeprom, Config), &BatConfig, sizeof( BatConfig))){
      return( false);
   }
   if( !Bat->ReloadConfig()){                  // update working configuration
      return( false);
   }
   return( true);
} // WriteConfig

//******************************************************************************
// Write logo
//******************************************************************************

bool TBat1Usb::WriteLogo()
// Write current logo to Bat1
{
   return( Bat->WriteMemory( offsetof( TEeprom, Logo), &Logo, sizeof( Logo)));
} // WriteLogo

//------------------------------------------------------------------------------

//******************************************************************************
// Get config
//******************************************************************************

TConfig *TBat1Usb::GetConfig()
{
   return( &BatConfig.Config);
} // GetConfig

