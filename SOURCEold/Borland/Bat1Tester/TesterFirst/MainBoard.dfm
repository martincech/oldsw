object MainForm: TMainForm
  Left = 393
  Top = 204
  Width = 603
  Height = 518
  Caption = 'BAT1 tester board'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 320
    Top = 19
    Width = 25
    Height = 13
    Caption = 'Port :'
  end
  object Label10: TLabel
    Left = 16
    Top = 472
    Width = 45
    Height = 13
    Caption = 'Status :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblStatus: TLabel
    Left = 80
    Top = 472
    Width = 46
    Height = 13
    Caption = 'Unknown'
  end
  object CbPort: TComboBox
    Left = 360
    Top = 16
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = CbPortChange
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object CbAutomatic: TCheckBox
    Left = 32
    Top = 19
    Width = 137
    Height = 17
    Caption = 'Automatic measuring'
    TabOrder = 1
  end
  object Panel1: TPanel
    Left = 32
    Top = 80
    Width = 313
    Height = 241
    BevelOuter = bvLowered
    TabOrder = 2
    object Label2: TLabel
      Left = 112
      Top = 8
      Width = 76
      Height = 13
      Caption = 'Power supply'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 184
      Top = 40
      Width = 85
      Height = 13
      Caption = 'Voltage regulation'
    end
    object Label4: TLabel
      Left = 16
      Top = 144
      Width = 42
      Height = 13
      Caption = 'Voltage :'
    end
    object LblVoltage: TLabel
      Left = 72
      Top = 144
      Width = 6
      Height = 13
      Caption = '?'
    end
    object RgPowerMode: TRadioGroup
      Left = 8
      Top = 40
      Width = 121
      Height = 81
      Caption = 'Power mode'
      ItemIndex = 0
      Items.Strings = (
        'Power off'
        'Power normal'
        'Power invert')
      TabOrder = 0
      OnClick = RgPowerModeClick
    end
    object BtnMax: TButton
      Left = 184
      Top = 72
      Width = 75
      Height = 25
      Caption = 'Max'
      TabOrder = 1
      OnClick = BtnMaxClick
    end
    object BtnUp: TButton
      Left = 184
      Top = 104
      Width = 75
      Height = 25
      Caption = 'Up'
      TabOrder = 2
      OnClick = BtnUpClick
    end
    object BtnDown: TButton
      Left = 184
      Top = 136
      Width = 75
      Height = 25
      Caption = 'Down'
      TabOrder = 3
      OnClick = BtnDownClick
    end
    object BtnMin: TButton
      Left = 184
      Top = 168
      Width = 75
      Height = 25
      Caption = 'Min'
      TabOrder = 4
      OnClick = BtnMinClick
    end
    object BtnGetVoltage: TButton
      Left = 16
      Top = 184
      Width = 75
      Height = 25
      Caption = 'Get voltage'
      TabOrder = 5
      OnClick = BtnGetVoltageClick
    end
  end
  object Panel2: TPanel
    Left = 32
    Top = 336
    Width = 313
    Height = 121
    BevelOuter = bvLowered
    TabOrder = 3
    object Label5: TLabel
      Left = 104
      Top = 8
      Width = 103
      Height = 13
      Caption = 'Current measuring'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 144
      Top = 56
      Width = 40
      Height = 13
      Caption = 'Current :'
    end
    object LblCurrent: TLabel
      Left = 192
      Top = 56
      Width = 6
      Height = 13
      Caption = '?'
    end
    object CbSensitive: TCheckBox
      Left = 16
      Top = 32
      Width = 97
      Height = 17
      Caption = 'High Sensitivity'
      TabOrder = 0
      OnClick = CbSensitiveClick
    end
    object BtnGetCurrent: TButton
      Left = 16
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Get current'
      TabOrder = 1
      OnClick = BtnGetCurrentClick
    end
  end
  object Panel3: TPanel
    Left = 360
    Top = 80
    Width = 217
    Height = 105
    BevelOuter = bvLowered
    TabOrder = 4
    object Label7: TLabel
      Left = 48
      Top = 8
      Width = 97
      Height = 13
      Caption = 'Bridge simulation'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object RgBridge: TRadioGroup
      Left = 16
      Top = 32
      Width = 129
      Height = 57
      Caption = 'Bridge voltage'
      ItemIndex = 0
      Items.Strings = (
        'Zero'
        'Maximum')
      TabOrder = 0
      OnClick = RgBridgeClick
    end
  end
  object Panel4: TPanel
    Left = 360
    Top = 208
    Width = 217
    Height = 137
    BevelOuter = bvLowered
    TabOrder = 5
    object Label8: TLabel
      Left = 72
      Top = 6
      Width = 54
      Height = 13
      Caption = 'Keyboard'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CbKeyOn: TCheckBox
      Left = 16
      Top = 34
      Width = 97
      Height = 17
      Caption = 'Keyboard ON'
      TabOrder = 0
      OnClick = CbKeyOnClick
    end
    object CbKeyK0: TCheckBox
      Left = 16
      Top = 58
      Width = 97
      Height = 17
      Caption = 'Keyboard K0'
      TabOrder = 1
      OnClick = CbKeyK0Click
    end
    object CbKeyK1: TCheckBox
      Left = 16
      Top = 82
      Width = 97
      Height = 17
      Caption = 'Keyboard K1'
      TabOrder = 2
      OnClick = CbKeyK1Click
    end
    object CbKeyK2: TCheckBox
      Left = 16
      Top = 106
      Width = 97
      Height = 17
      Caption = 'Keyboard K2'
      TabOrder = 3
      OnClick = CbKeyK2Click
    end
  end
  object Panel5: TPanel
    Left = 360
    Top = 360
    Width = 217
    Height = 97
    BevelOuter = bvLowered
    TabOrder = 6
    object Label9: TLabel
      Left = 56
      Top = 8
      Width = 85
      Height = 13
      Caption = 'External power'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CbUsb: TCheckBox
      Left = 16
      Top = 37
      Width = 97
      Height = 17
      Caption = 'Connect USB'
      TabOrder = 0
      OnClick = CbUsbClick
    end
    object CbCharger: TCheckBox
      Left = 16
      Top = 61
      Width = 97
      Height = 17
      Caption = 'Connect Charger'
      TabOrder = 1
      OnClick = CbChargerClick
    end
  end
  object BtnDefaults: TButton
    Left = 168
    Top = 16
    Width = 97
    Height = 25
    Caption = 'Reset Defaults'
    TabOrder = 7
    OnClick = BtnDefaultsClick
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 560
    Top = 8
  end
end
