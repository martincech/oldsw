//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Tester.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("UsbTester.cpp");
USEUNIT("TestAdapter.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USEUNIT("MasterCom.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\ArmPgm\LpcIsp.cpp");
USEUNIT("..\ArmPgm\HexFile.cpp");
USEUNIT("..\ArmPgm\LpcFlash.cpp");
USEFORM("Setup.cpp", SetupForm);
USEUNIT("..\Bat1Reader\PktAdapter.cpp");
USEUNIT("..\Bat1Reader\BatDevice.cpp");
USEUNIT("..\Library\Unisys\Dt.cpp");
USEUNIT("TestPower.cpp");
USEFORM("Calibration.cpp", CalibrationForm);
USEUNIT("Bat1Usb.cpp");
USELIB("..\Library\Serial\USB\ftd2xx.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 Tester";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
