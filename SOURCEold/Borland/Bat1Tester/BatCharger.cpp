//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("BatCharger.res");
USEFORM("MainCharger.cpp", MainForm);
USEUNIT("MasterCom.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->Title = "Bat1 Charger Test";
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
