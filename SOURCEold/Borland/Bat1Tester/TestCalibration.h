//******************************************************************************
//
//   TestCalibration.h   Bat1 power tests
//   Version 1.0        (c) VymOs
//
//******************************************************************************

#ifndef TestCalibrationH
#define TestCalibrationH

#include "TestAdapter.h"
#include "../Library/Crt/Crt.h"
#include "MasterCom.h"

//******************************************************************************
// Test calibration
//******************************************************************************

class TTestCalibration {
public :
   TTestCalibration( TCrt *Crt, TMasterCom *Master);
   ~TTestCalibration();

//--- basic functions : --------------------------------------------------------

   bool Open( char *ComName);
   // Open tester COM

   bool UsbPowerOn();
   // Set defaults & USB power

   bool KeyboardPowerOn();
   // Press/release ON key

   bool AdcCalibration();
   // Test ADC calibration

   bool IadcCalibration();
   // Test IADC calibration

   bool NormalCurrent();
   // Check current with backlight

   bool DarkCurrent();
   // Check current without backlight

// properties
   // measuring :
   int        Voltage;                 // last measured voltage
   int        Current;                 // last measured current
   bool       SensitiveCurrent;        // sensitive current range
   int        AdcZeroVoltage;          // ADC zero [LSB]
   int        AdcMaxVoltage;           // ADC max [LSB]
   int        IadcLowVoltage;          // IADC low [mV]
   int        IadcHighVoltage;         // IADC high [mV]
   // settings :
   int        IadcLow;
   int        IadcHigh;

//------------------------------------------------------------------------------

protected :
   TTestAdapter *FAdapter;
   TCrt         *FCrt;
   TMasterCom   *FMaster;

   bool PowerOn( bool On);
   // Switch power <On>

   bool SetVoltage( int Voltage);
   // Set power <Voltage> [mV]
}; // TTestCalibration

#endif
