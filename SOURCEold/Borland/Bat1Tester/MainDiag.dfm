object MainForm: TMainForm
  Left = 456
  Top = 185
  Width = 856
  Height = 647
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 Diagnostics'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00EEEE
    EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    EEEEEE77777777777EEEEEEEEEEEEEEEEEEEEE7CCCCCCCCC7EEEEEEEEEEEEEEE
    EEEEEE7CCCCCCCCC7EEEEEEEEEEEEEEEEEEEEE7CCCCCCCCC7EEEEEEEEEEEEEEE
    EEEE777CCCCCCCCC777EEEEEEEEEEEEEEEEEE7CCCCCCCCCCC7EEEEEEEEEEEEEE
    EEEEEE7CCCCCCCCC7EEEEEEEEEEEEEEEEEEEEEE7CCCCCCC7EEEEEEEEEEEEEEEE
    EEEEEEEE7CCCCC7EEEEEEEEEEEEEEEEEEEEEEEEEE7CCC7EEEEEEEEEEEEEEEEEE
    EEEEEEEEEE7C7EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE7EEEEEEEEEEEEEEEE0000
    0000000000080000000000000000000000000000008F80000000000000000000
    0000000008FFF8000000000000000000000000008FFFFF800000000000000000
    00000008FFFFFFF800000000000000000000008FFFFFFFFF8000000000000000
    000008FFFFFFFFFFF8000000000000000000888FFFFFFFFF8880000000000000
    0000008FFFFFFFFF80000000000000000000008FFFFFFFFF8000000000000000
    0000008FFFFFFFFF800000000000000000000088888888888000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object LblPhase: TLabel
    Left = 230
    Top = 8
    Width = 6
    Height = 13
    Caption = '?'
    Visible = False
  end
  object LblPercent: TLabel
    Left = 294
    Top = 48
    Width = 38
    Height = 13
    Caption = '100.0 %'
    Visible = False
  end
  object Image: TImage
    Left = 2
    Top = 4
    Width = 113
    Height = 65
    Stretch = True
  end
  object Label1: TLabel
    Left = 128
    Top = 56
    Width = 41
    Height = 13
    Caption = 'Version :'
  end
  object LblVersion: TLabel
    Left = 184
    Top = 56
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label2: TLabel
    Left = 688
    Top = 144
    Width = 65
    Height = 13
    Caption = 'Power Test'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 544
    Top = 8
    Width = 70
    Height = 13
    Caption = 'Preparation '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 544
    Top = 144
    Width = 55
    Height = 13
    Caption = 'Hardware'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 544
    Top = 464
    Width = 62
    Height = 13
    Caption = 'Production'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object MainMemo: TMemo
    Left = 0
    Top = 72
    Width = 497
    Height = 513
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 601
    Width = 848
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object ProgressBar: TProgressBar
    Left = 230
    Top = 24
    Width = 150
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 2
    Visible = False
  end
  object BtnSetup: TButton
    Left = 520
    Top = 560
    Width = 75
    Height = 25
    Caption = 'Setup'
    TabOrder = 3
    OnClick = BtnSetupClick
  end
  object BtnCalibration: TButton
    Left = 720
    Top = 560
    Width = 75
    Height = 25
    Caption = 'Calibration'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnClick = BtnCalibrationClick
  end
  object BtnInitializeTester: TButton
    Left = 544
    Top = 32
    Width = 105
    Height = 25
    Caption = 'Initialize Tester'
    TabOrder = 5
    OnClick = BtnInitializeTesterClick
  end
  object BtnFtdiProgramming: TButton
    Left = 544
    Top = 64
    Width = 105
    Height = 25
    Caption = 'FTDI Programming'
    TabOrder = 6
    OnClick = BtnFtdiProgrammingClick
  end
  object BtnTestFirmware: TButton
    Left = 544
    Top = 96
    Width = 105
    Height = 25
    Caption = 'Write Test Firmware'
    TabOrder = 7
    OnClick = BtnTestFirmwareClick
  end
  object BtnPrinterCom: TButton
    Left = 544
    Top = 168
    Width = 105
    Height = 25
    Caption = 'Printer COM Test'
    TabOrder = 8
    OnClick = BtnPrinterComClick
  end
  object BtnUsbTest: TButton
    Left = 544
    Top = 200
    Width = 105
    Height = 25
    Caption = 'USB Test'
    TabOrder = 9
    OnClick = BtnUsbTestClick
  end
  object BtnDisplayTest: TButton
    Left = 544
    Top = 232
    Width = 105
    Height = 25
    Caption = 'Display Test'
    TabOrder = 10
    OnClick = BtnDisplayTestClick
  end
  object BtnMemoryTest: TButton
    Left = 544
    Top = 264
    Width = 105
    Height = 25
    Caption = 'Memory Test'
    TabOrder = 11
    OnClick = BtnMemoryTestClick
  end
  object BtnRtcTest: TButton
    Left = 544
    Top = 296
    Width = 105
    Height = 25
    Caption = 'RTC Test'
    TabOrder = 12
    OnClick = BtnRtcTestClick
  end
  object BtnSoundTest: TButton
    Left = 544
    Top = 328
    Width = 105
    Height = 25
    Caption = 'Sound Test'
    TabOrder = 13
    OnClick = BtnSoundTestClick
  end
  object BtnAdcTest: TButton
    Left = 544
    Top = 360
    Width = 105
    Height = 25
    Caption = 'ADC Test'
    TabOrder = 14
    OnClick = BtnAdcTestClick
  end
  object BtnIadcTest: TButton
    Left = 544
    Top = 392
    Width = 105
    Height = 25
    Caption = 'Internal ADC Test'
    TabOrder = 15
    OnClick = BtnIadcTestClick
  end
  object BtnKeyboardTest: TButton
    Left = 544
    Top = 424
    Width = 105
    Height = 25
    Caption = 'Keyboard Test'
    TabOrder = 16
    OnClick = BtnKeyboardTestClick
  end
  object BtnBatFirmware: TButton
    Left = 544
    Top = 488
    Width = 121
    Height = 25
    Caption = 'Write BAT1 Firmware'
    TabOrder = 17
    OnClick = BtnBatFirmwareClick
  end
  object BtnBatTest: TButton
    Left = 544
    Top = 520
    Width = 121
    Height = 25
    Caption = 'BAT1 Test'
    TabOrder = 18
    OnClick = BtnBatTestClick
  end
  object BtnInverted: TButton
    Left = 688
    Top = 168
    Width = 129
    Height = 25
    Caption = 'Inverted power'
    TabOrder = 19
    OnClick = BtnInvertedClick
  end
  object BtnIdle: TButton
    Left = 688
    Top = 200
    Width = 129
    Height = 25
    Caption = 'Idle power'
    TabOrder = 20
    OnClick = BtnIdleClick
  end
  object BtnNormal: TButton
    Left = 688
    Top = 232
    Width = 129
    Height = 25
    Caption = 'Normal power'
    TabOrder = 21
    OnClick = BtnNormalClick
  end
  object BtnDark: TButton
    Left = 688
    Top = 264
    Width = 129
    Height = 25
    Caption = 'Without Backlight power'
    TabOrder = 22
    OnClick = BtnDarkClick
  end
  object BtnLowVoltage: TButton
    Left = 688
    Top = 296
    Width = 129
    Height = 25
    Caption = 'Low Voltage Test'
    TabOrder = 23
    OnClick = BtnLowVoltageClick
  end
  object BtnCharger: TButton
    Left = 688
    Top = 328
    Width = 129
    Height = 25
    Caption = 'Charger Connection'
    TabOrder = 24
    OnClick = BtnChargerClick
  end
  object BtnUsbCharger: TButton
    Left = 688
    Top = 360
    Width = 129
    Height = 25
    Caption = 'USB Charger Connection'
    TabOrder = 25
    OnClick = BtnUsbChargerClick
  end
  object BtnChargerDone: TButton
    Left = 688
    Top = 392
    Width = 129
    Height = 25
    Caption = 'Charger OFF voltage'
    TabOrder = 26
    OnClick = BtnChargerDoneClick
  end
  object BtnUsbDone: TButton
    Left = 688
    Top = 424
    Width = 129
    Height = 25
    Caption = 'USB Charger OFF voltage'
    TabOrder = 27
    OnClick = BtnUsbDoneClick
  end
  object BtnPowerOn: TButton
    Left = 688
    Top = 64
    Width = 121
    Height = 25
    Caption = 'USB Power On'
    TabOrder = 28
    OnClick = BtnPowerOnClick
  end
  object BtnPowerOff: TButton
    Left = 688
    Top = 96
    Width = 121
    Height = 25
    Caption = 'USB Power Off'
    TabOrder = 29
    OnClick = BtnPowerOffClick
  end
end
