//******************************************************************************
//
//   Setup.cpp    Tester setup
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "SetupDiag.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TSetupForm *SetupForm;
//---------------------------------------------------------------------------

#define CURRENT_VERSION 0x100

// Setup Write macros :
#define StpWriteString( Section, Parameter) \
   SetupFile->WriteString(  #Section, #Parameter, Parameter)

#define StpWriteInteger( Section, Parameter) \
   SetupFile->WriteInteger(  #Section, #Parameter, Parameter)

#define StpWriteBool( Section, Parameter) \
   SetupFile->WriteBool(  #Section, #Parameter, Parameter)

#define StpWriteFloat( Section, Parameter) \
   SetupFile->WriteFloat(  #Section, #Parameter, Parameter)

#define StpWriteDateTime( Section, Parameter) \
   SetupFile->WriteDateTime(  #Section, #Parameter, Parameter)

// Setup Read macros :
#define StpReadString( Section, Parameter) \
   Parameter = SetupFile->ReadString( #Section, #Parameter, "")

#define StpReadInteger( Section, Parameter) \
   Parameter = SetupFile->ReadInteger( #Section, #Parameter, 0)

#define StpReadBool( Section, Parameter) \
   Parameter = SetupFile->ReadBool( #Section, #Parameter, false)

#define StpReadFloat( Section, Parameter) \
   Parameter = SetupFile->ReadFloat( #Section, #Parameter, 0.0)

#define StpReadDateTime( Section, Parameter) \
   Parameter = SetupFile->ReadDateTime( #Section, #Parameter, 0)

//---------------------------------------------------------------------------

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TSetupForm::TSetupForm(TComponent* Owner)
        : TForm(Owner)
{
   WaitForUsb = false;
   strcpy( PrinterPort, "COM1");
   strcpy( TesterPort,  "COM2");
   strcpy( DataPath,    "Data");
} // TSetupForm

//******************************************************************************
// Execute
//******************************************************************************

bool TSetupForm::Execute()
// Execute dialog
{
   CbWaitForUsb->Checked              = WaitForUsb;
   CbPrinterPort->ItemIndex           = CbPrinterPort->Items->IndexOf( PrinterPort);
   CbTesterPort->ItemIndex            = CbTesterPort->Items->IndexOf( TesterPort);
   EdtDataPath->Text                  = DataPath;
   if( ShowModal() != mrOk){
      return( false);
   }
   WaitForUsb = CbWaitForUsb->Checked;
   strcpy( PrinterPort, CbPrinterPort->Text.c_str());
   strcpy( TesterPort,  CbTesterPort->Text.c_str());
   strcpy( DataPath,    EdtDataPath->Text.c_str());
   int Length = strlen( DataPath);
   if( DataPath[ Length - 1] == '\\'){
      DataPath[ Length - 1] = '\0';    // cut trailing backslash
   }
   return( true);
} // Execute

//******************************************************************************
// Save
//******************************************************************************

void TSetupForm::Save()
{
   Version = CURRENT_VERSION;
   // Configuration data :
   StpWriteInteger( Properties, Version);
   StpWriteBool( Properties, WaitForUsb);
   StpWriteString( Properties, PrinterPort);
   StpWriteString( Properties, TesterPort);
   StpWriteString( Properties, DataPath);
} // Save

//******************************************************************************
// Load
//******************************************************************************

void TSetupForm::Load()
{
   AnsiString SetupFileName = ChangeFileExt( Application->ExeName, ".INI" );
   SetupFile    = new TIniFile( SetupFileName);
   // Configuration data :
   StpReadInteger( Properties, Version);
   if( Version != CURRENT_VERSION){
      // invalid file, fill with defaults
      WaitForUsb = false;
      strcpy( PrinterPort, "COM1");
      strcpy( TesterPort,  "COM2");
      strcpy( DataPath,    "Data");
      return;
   }
   // read settings :
   StpReadBool( Properties, WaitForUsb);
   AnsiString String = SetupFile->ReadString( "Properties", "PrinterPort", "");
   strcpy( PrinterPort, String.c_str());
   String = SetupFile->ReadString( "Properties", "TesterPort", "");
   strcpy( TesterPort, String.c_str());
   String = SetupFile->ReadString( "Properties", "DataPath", "");
   strcpy( DataPath, String.c_str());
} // Load

