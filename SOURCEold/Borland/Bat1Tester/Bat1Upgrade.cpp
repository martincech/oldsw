//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Upgrade.res");
USEFORM("MainUpgrade.cpp", MainForm);
USEUNIT("UsbTester.cpp");
USEUNIT("Bat1Usb.cpp");
USEUNIT("..\Bat1Reader\PktAdapter.cpp");
USEUNIT("..\Bat1Reader\BatDevice.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USEUNIT("..\Library\Unisys\Dt.cpp");
USELIB("..\Library\Serial\USB\ftd2xx.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 Upgrade";
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
