//******************************************************************************
//
//   TestPower.h    Bat1 power tests
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef TestPowerH
#define TestPowerH

#include "TestAdapter.h"
#include "../Library/Crt/Crt.h"
#include "MasterCom.h"

//******************************************************************************
// Test power
//******************************************************************************

class TTestPower {
public :
   TTestPower( TCrt *Crt, TMasterCom *Master);
   ~TTestPower();

//--- basic functions : --------------------------------------------------------

   bool Open( char *ComName);
   // Open tester COM

   bool Calibration();
   // Tester calibration

   bool UsbPowerOn();
   // Set defaults & USB power

   bool KeyboardPowerOn();
   // Press/release ON key

   bool KeyboardPowerOff();
   // Press/hold/release ON key

   void Shutdown();
   // Tester shutdown

   bool UsbConnect( bool Connect);
   // <Connect>/disconnect USB power & data

//--- testing : ----------------------------------------------------------------

   bool AdcCalibration();
   // Test ADC calibration

   bool IadcCalibration();
   // Test IADC calibration

   bool Keyboard();
   // Test keyboard

   bool Power();
   // Run all power tests

//--- single tests : -----------------------------------------------------------

   bool InvertedPower();
   // Inverted power test

   bool IdleCurrent();
   // Check power off current

   bool NormalCurrent();
   // Check current with backlight

   bool DarkCurrent();
   // Check current without backlight

   bool LowVoltage();
   // Check power off on low voltage

   bool Charger();
   // Test charger connection

   bool UsbCharger();
   // Test USB charger connection

   bool ChargerDone();
   // Test charger done condition

   bool UsbChargerDone();
   // Test charging from USB done

// properties
   bool       EnableHardware;          // enable tester hardware
   // measuring :
   int        Voltage;                 // last measured voltage
   int        Current;                 // last measured current
   bool       SensitiveCurrent;        // sensitive current range
   int        AdcZeroVoltage;          // ADC zero [LSB]
   int        AdcMaxVoltage;           // ADC max [LSB]
   int        IadcLowVoltage;          // IADC low [mV]
   int        IadcHighVoltage;         // IADC high [mV]
   // test tresholds :
   int        CurrentInverted;
   int        CurrentInvertedRange;
   int        CurrentIdle;
   int        CurrentIdleRange;
   int        CurrentNormal;
   int        CurrentNormalRange;
   int        CurrentDark;
   int        CurrentDarkRange;
   int        CurrentOff;
   int        AdcZero;
   int        AdcZeroRange;
   int        AdcMax;
   int        AdcMaxRange;
   int        IadcLow;
   int        IadcLowRange;
   int        IadcHigh;
   int        IadcHighRange;

//------------------------------------------------------------------------------

protected :
   TTestAdapter *FAdapter;
   TCrt         *FCrt;
   TMasterCom   *FMaster;

   bool PowerOn( bool On);
   // Switch power <On>

   bool SetVoltage( int Voltage);
   // Set power <Voltage> [mV]

   bool ChargerStop( bool Usb);
   // Test charger/<Usb> charger stop

   bool InRange( int Value, int Nominal, int Delta);
   // Check if the <Value> is <Nominal> +- <Delta>
}; // TTestAdapter

#endif
