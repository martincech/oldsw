//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainDiagH
#define MainDiagH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Crt/Crt.h"
#include "TestPower.h"
#include "UsbTester.h"
#include "MasterCom.h"
#include "Bat1Usb.h"
#include "../ArmPgm/HexFile.h"
#include "../ArmPgm/LpcFlash.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TLabel *LblPhase;
        TProgressBar *ProgressBar;
        TLabel *LblPercent;
        TButton *BtnSetup;
        TImage *Image;
        TLabel *Label1;
        TLabel *LblVersion;
   TButton *BtnCalibration;
   TButton *BtnInitializeTester;
   TButton *BtnFtdiProgramming;
   TButton *BtnTestFirmware;
   TButton *BtnPrinterCom;
   TButton *BtnUsbTest;
   TButton *BtnDisplayTest;
   TButton *BtnMemoryTest;
   TButton *BtnRtcTest;
   TButton *BtnSoundTest;
   TButton *BtnAdcTest;
   TButton *BtnIadcTest;
   TButton *BtnKeyboardTest;
   TButton *BtnBatFirmware;
   TButton *BtnBatTest;
   TButton *BtnInverted;
   TButton *BtnIdle;
   TButton *BtnNormal;
   TButton *BtnDark;
   TButton *BtnLowVoltage;
   TButton *BtnCharger;
   TButton *BtnUsbCharger;
   TButton *BtnChargerDone;
   TButton *BtnUsbDone;
   TLabel *Label2;
   TLabel *Label3;
   TLabel *Label4;
   TLabel *Label5;
   TButton *BtnPowerOn;
   TButton *BtnPowerOff;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnSetupClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall BtnCalibrationClick(TObject *Sender);
   void __fastcall BtnInitializeTesterClick(TObject *Sender);
   void __fastcall BtnFtdiProgrammingClick(TObject *Sender);
   void __fastcall BtnTestFirmwareClick(TObject *Sender);
   void __fastcall BtnPrinterComClick(TObject *Sender);
   void __fastcall BtnUsbTestClick(TObject *Sender);
   void __fastcall BtnDisplayTestClick(TObject *Sender);
   void __fastcall BtnMemoryTestClick(TObject *Sender);
   void __fastcall BtnRtcTestClick(TObject *Sender);
   void __fastcall BtnSoundTestClick(TObject *Sender);
   void __fastcall BtnAdcTestClick(TObject *Sender);
   void __fastcall BtnIadcTestClick(TObject *Sender);
   void __fastcall BtnKeyboardTestClick(TObject *Sender);
   void __fastcall BtnBatFirmwareClick(TObject *Sender);
   void __fastcall BtnBatTestClick(TObject *Sender);
   void __fastcall BtnInvertedClick(TObject *Sender);
   void __fastcall BtnIdleClick(TObject *Sender);
   void __fastcall BtnNormalClick(TObject *Sender);
   void __fastcall BtnDarkClick(TObject *Sender);
   void __fastcall BtnLowVoltageClick(TObject *Sender);
   void __fastcall BtnChargerClick(TObject *Sender);
   void __fastcall BtnUsbChargerClick(TObject *Sender);
   void __fastcall BtnChargerDoneClick(TObject *Sender);
   void __fastcall BtnUsbDoneClick(TObject *Sender);
   void __fastcall BtnPowerOnClick(TObject *Sender);
   void __fastcall BtnPowerOffClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt          *Crt;
   TUsbTester    *Usb;
   TTestPower    *Adapter;
   TMasterCom    *Com;
   THexFile      *Hex;
   TLpcFlash     *Lpc;
   TBat1Usb      *Bat1Usb;

   __fastcall TMainForm(TComponent* Owner);
   void Progress( int Percent);
   void StartComm();
   void ContinueComm();
   void StopComm();

   bool TesterDefaults();
   bool FtdiProgramming();
   bool WriteTestFirmware();
   bool MasterComTest();
   bool AdcCalibrationTest();
   bool IadcCalibrationTest();
   bool KeyboardTest();
   bool WriteBatFirmware();
   bool BatTest();
   bool WriteFirmware( AnsiString FileName);
   void Play( int SoundType);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
