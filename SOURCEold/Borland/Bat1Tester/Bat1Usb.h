//******************************************************************************
//
//   Bat1Usb.h       Bat1 USB communication
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef Bat1UsbH
#define Bat1UsbH

#include "../Bat1Reader/BatDevice.h"
#include "../Bat1Reader/MemoryDef.h"

//******************************************************************************
// Bat1Usb class
//******************************************************************************

class TBat1Usb {
public :
   TBat1Usb();
   ~TBat1Usb();

   void DefaultConfig();
   // Prepare factory defaults
   void PrepareLogo( TImage *Image);
   // Prepare logo from <Image>
   AnsiString GetVersion();
   // Returns version string
   bool IsConnected();
   // Check for Bat1 presence
   bool SetTime();
   // Set Bat1 time by PC
   bool FormatFilesystem();
   // Format Bat1 filesystem
   bool CreateFile( char *FileName);
   // Create <FileName>
   bool WriteConfig();
   // Write current configuration to Bat1
   bool WriteLogo();
   // Write current logo to Bat1

   __property TConfig *Config = {read=GetConfig};
protected :
   TBatDevice  *Bat;                 // USB Communication
   TLogo        Logo;                // Logo buffer
   TConfigUnion BatConfig;           // Bat configuration

   TConfig *GetConfig();             // pointer to BatConfig.Config
}; // TBat1Usb

#endif
