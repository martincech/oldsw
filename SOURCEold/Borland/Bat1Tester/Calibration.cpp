//******************************************************************************
//
//   Calibration.cpp  Calibration parameters form
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Calibration.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TCalibrationForm *CalibrationForm;
//---------------------------------------------------------------------------

#define CURRENT_VERSION 0x100

// Setup Write macros :
#define StpWriteString( Section, Parameter) \
   SetupFile->WriteString(  #Section, #Parameter, Parameter)

#define StpWriteInteger( Section, Parameter) \
   SetupFile->WriteInteger(  #Section, #Parameter, Parameter)

#define StpWriteBool( Section, Parameter) \
   SetupFile->WriteBool(  #Section, #Parameter, Parameter)

#define StpWriteFloat( Section, Parameter) \
   SetupFile->WriteFloat(  #Section, #Parameter, Parameter)

#define StpWriteDateTime( Section, Parameter) \
   SetupFile->WriteDateTime(  #Section, #Parameter, Parameter)

// Setup Read macros :
#define StpReadString( Section, Parameter) \
   Parameter = SetupFile->ReadString( #Section, #Parameter, "")

#define StpReadInteger( Section, Parameter) \
   Parameter = SetupFile->ReadInteger( #Section, #Parameter, 0)

#define StpReadBool( Section, Parameter) \
   Parameter = SetupFile->ReadBool( #Section, #Parameter, false)

#define StpReadFloat( Section, Parameter) \
   Parameter = SetupFile->ReadFloat( #Section, #Parameter, 0.0)

#define StpReadDateTime( Section, Parameter) \
   Parameter = SetupFile->ReadDateTime( #Section, #Parameter, 0)

//---------------------------------------------------------------------------

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TCalibrationForm::TCalibrationForm(TComponent* Owner)
   : TForm(Owner)
{
} // TCalibrationForm

//******************************************************************************
// Execute
//******************************************************************************

bool TCalibrationForm::Execute()
// Execute dialog
{
   // set items :
   EdtCurrentNormal->Text      = AnsiString( CurrentNormal);
   EdtCurrentNormalRange->Text = AnsiString( CurrentNormalRange);
   EdtCurrentDark->Text        = AnsiString( CurrentDark);
   EdtCurrentDarkRange->Text   = AnsiString( CurrentDarkRange);
   EdtAdcZero->Text            = AnsiString( AdcZero);
   EdtAdcZeroRange->Text       = AnsiString( AdcZeroRange);
   EdtAdcMax->Text             = AnsiString( AdcMax);
   EdtAdcMaxRange->Text        = AnsiString( AdcMaxRange);
   EdtIadcLow->Text            = AnsiString( IadcLow);
   EdtIadcLowRange->Text       = AnsiString( IadcLowRange);
   EdtIadcHigh->Text           = AnsiString( IadcHigh);
   EdtIadcHighRange->Text      = AnsiString( IadcHighRange); 
   if( ShowModal() != mrOk){
      return( false);
   }
   // get items :
   CurrentNormal        = EdtCurrentNormal->Text.ToInt();
   CurrentNormalRange   = EdtCurrentNormalRange->Text.ToInt();
   CurrentDark          = EdtCurrentDark->Text.ToInt();
   CurrentDarkRange     = EdtCurrentDarkRange->Text.ToInt();
   AdcZero              = EdtAdcZero->Text.ToInt();
   AdcZeroRange         = EdtAdcZeroRange->Text.ToInt();
   AdcMax               = EdtAdcMax->Text.ToInt();
   AdcMaxRange          = EdtAdcMaxRange->Text.ToInt();
   IadcLow              = EdtIadcLow->Text.ToInt();
   IadcLowRange         = EdtIadcLowRange->Text.ToInt();
   IadcHigh             = EdtIadcHigh->Text.ToInt();
   IadcHighRange        = EdtIadcHighRange->Text.ToInt();
   return( true);
} // Execute

//******************************************************************************
// Save
//******************************************************************************

void TCalibrationForm::Save()
{
   Version = CURRENT_VERSION;
   // Configuration data :
   StpWriteInteger( Properties, Version);
   StpWriteInteger( Properties, CurrentNormal);
   StpWriteInteger( Properties, CurrentNormalRange);
   StpWriteInteger( Properties, CurrentDark);
   StpWriteInteger( Properties, CurrentDarkRange);
   StpWriteInteger( Properties, AdcZero);
   StpWriteInteger( Properties, AdcZeroRange);
   StpWriteInteger( Properties, AdcMax);
   StpWriteInteger( Properties, AdcMaxRange);
   StpWriteInteger( Properties, IadcLow);
   StpWriteInteger( Properties, IadcLowRange);
   StpWriteInteger( Properties, IadcHigh);
   StpWriteInteger( Properties, IadcHighRange);
} // Save

//******************************************************************************
// Load
//******************************************************************************

void TCalibrationForm::Load()
{
   AnsiString SetupFileName = ChangeFileExt( Application->ExeName, ".CAL" );
   SetupFile    = new TIniFile( SetupFileName);
   // Configuration data :
   StpReadInteger( Properties, Version);
   if( Version != CURRENT_VERSION){
      // invalid file, fill with defaults
      CurrentNormal        = 100;   // mA
      CurrentNormalRange   = 100;   // mA
      CurrentDark          = 100;   // mA
      CurrentDarkRange     = 100;   // mA
      AdcZero              = 0;     // LSB
      AdcZeroRange         = 3000;
      AdcMax               = 87000;
      AdcMaxRange          = 3000;
      IadcLow              = 3400;
      IadcLowRange         = 500;
      IadcHigh             = 4200;
      IadcHighRange        = 500;
      return;
   }
   // read settings :
   StpReadInteger( Properties, Version);
   StpReadInteger( Properties, CurrentNormal);
   StpReadInteger( Properties, CurrentNormalRange);
   StpReadInteger( Properties, CurrentDark);
   StpReadInteger( Properties, CurrentDarkRange);
   StpReadInteger( Properties, AdcZero);
   StpReadInteger( Properties, AdcZeroRange);
   StpReadInteger( Properties, AdcMax);
   StpReadInteger( Properties, AdcMaxRange);
   StpReadInteger( Properties, IadcLow);
   StpReadInteger( Properties, IadcLowRange);
   StpReadInteger( Properties, IadcHigh);
   StpReadInteger( Properties, IadcHighRange);
} // Load

