//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("BatCalibration.res");
USEFORM("MainCalibration.cpp", MainForm);
USEUNIT("TestAdapter.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("TestCalibration.cpp");
USEUNIT("MasterCom.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
