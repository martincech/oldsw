//******************************************************************************
//
//   MainCharger.cpp  Bat1/NT charger test
//   Version 1.0  (c) VymOs
//
//******************************************************************************


#include <vcl.h>
#pragma hdrstop

#include "MainCharger.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
   Com     = new TMasterCom;
   Running = NO;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   BtnStop->Enabled = false;
   CbEnnab->Checked = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::BtnRunClick(TObject *Sender)
{
   if( !PrinterPortOpen()){
      Application->MessageBox( "Unable open Printer Port", "Error", MB_OK);
      return;
   }
   if( !Com->EnableCharger( CbEnnab->Checked)){
      Application->MessageBox( "Unable control charger", "Error", MB_OK);
      return;
   }
   Running = YES;
   LblStatus->Caption = "Running...";
   // update buttons :
   BtnRun->Enabled  = false;
   BtnStop->Enabled = true;
   EdtPrinterPort->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::BtnStopClick(TObject *Sender)
{
   LblStatus->Caption = "Stopped...";
   Running = NO;
   // update buttons :
   BtnRun->Enabled  = true;
   BtnStop->Enabled = false;
   EdtPrinterPort->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::TimerTimer(TObject *Sender)
{
   if( !Running){
      return;
   }
   bool PPR, CHG, USBON, ENNAB;
   if( !Com->TestPower( PPR, CHG, USBON, ENNAB)){
      LblStatus->Caption = "COM error !";
      CbEnnab->Caption  = "?";
      LblUsbon->Caption = "?";
      LblPpr->Caption   = "?";
      LblChg->Caption   = "?";
      PrinterPortOpen();   // retry
      return;
   }
   CbEnnab->Caption  = ENNAB ? "H" : "L";
   LblUsbon->Caption = USBON ? "H" : "L";
   LblPpr->Caption   = PPR   ? "H" : "L";
   LblChg->Caption   = CHG   ? "H" : "L";
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::CbEnnabClick(TObject *Sender)
{
   if( !Running){
      return;
   }
   if( !Com->EnableCharger( CbEnnab->Checked)){
      LblStatus->Caption = "COM error !";
      return;
   }
}
//---------------------------------------------------------------------------

TYesNo TMainForm::PrinterPortOpen( void)
{
   if( !Com->Open( EdtPrinterPort->Text.c_str())){
      LblStatus->Caption = "COM error !";
      return( NO);
   }
   if( Running){
      LblStatus->Caption = "Running...";
   } else {
      LblStatus->Caption = "Stopped...";
   }
   return( YES);
} // PrinterPortOpen
