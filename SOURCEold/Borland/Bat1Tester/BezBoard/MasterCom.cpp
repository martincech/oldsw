//******************************************************************************
//
//   MasterCom.cpp  Master communication functions
//   Version 0.0    (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "../Library/Unisys/Uni.h"
#include "MasterCom.h"
#include "TestDef.h"
#include <ctype.h>
#include <stdio.h>

#define COM_TIMEOUT 2000

//******************************************************************************
// Constructor
//******************************************************************************

TMasterCom::TMasterCom()
{
   FCom = 0;
} // TMasterCom

//******************************************************************************
// Destructor
//******************************************************************************

TMasterCom::~TMasterCom()
{
   if( FCom){
      FCom->Close();
   }
} // ~TMasterCom

//******************************************************************************
// Open
//******************************************************************************

bool TMasterCom::Open( char *ComName)
// Open <ComName>
{
   if( FCom){
      FCom->Close();
   }
   TIdentifier Identifier;
   // COM setup
   FCom = new TComUart;
   if( !FCom->Locate( ComName, Identifier)){
      delete FCom;
      FCom = 0;
      Status      = "COM Locate error";
      return( false);
   }
   if( !FCom->Open( Identifier)){
      delete FCom;
      FCom = 0;
      Status      = "COM Open error";
      return( false);
   }
   // default parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate     = 9600;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   FCom->SetParameters( Parameters);
   // common init :
   FCom->SetRxNowait();
   FCom->Flush();
   return( true);
} // Open

//******************************************************************************
// Flush
//******************************************************************************

void TMasterCom::Flush()
// Flush COM data
{
   FCom->Flush();
} // Flush

//******************************************************************************
// Test UART
//******************************************************************************

#define TEST_DATA_SIZE 256

bool TMasterCom::TestUart()
// Communication test
{
   byte Buffer[ TEST_DATA_SIZE];
   FCom->SetRxWait( COM_TIMEOUT, 0);
   // prepare data :
   for( int i = 0; i < TEST_DATA_SIZE; i++){
      Buffer[ i] = (byte)tolower( i);
   }
   // send data :
   if( FCom->Write( Buffer, TEST_DATA_SIZE) != TEST_DATA_SIZE){
      Status      = "COM send error";
      return( false);
   }
   // receive reply :
   if( FCom->Read( Buffer, TEST_DATA_SIZE) != TEST_DATA_SIZE){
      Status      = "COM receive timeout";
      return( false);
   }
   // verify data :
   for( int i = 0; i < TEST_DATA_SIZE; i++){
      if( Buffer[ i] != (byte)tolower( i)){
         Status      = "COM Rx data mismatch";
         return( false);
      }
   }
   return( true);
} // TestUart

//******************************************************************************
// Test display
//******************************************************************************

bool TMasterCom::TestDisplay()
// Run display test
{
   return( Test( TEST_DISPLAY, 3));
} // TestDisplay

//******************************************************************************
// Test Memory
//******************************************************************************

bool TMasterCom::TestMemory()
// Run memory test
{
   return( Test( TEST_MEMORY, 24));
} // TestMemory

//******************************************************************************
// Test RTC
//******************************************************************************

bool TMasterCom::TestRtc()
// Run RTC test
{
   return( Test( TEST_RTC, 5));
} // TestRtc

//******************************************************************************
// Test Sound
//******************************************************************************

bool TMasterCom::TestSound()
// Run sound test
{
   return( Test( TEST_SOUND, 2));
} // TestSound

//******************************************************************************
// Read ADC
//******************************************************************************

bool TMasterCom::ReadAdc( dword &Value)
// Read external ADC
{
   if( !StartTest( TEST_ADC)){
      return( false);
   }
   return( ReadHex( 3, Value));
} // ReadAdc

//******************************************************************************
// Read Iadc
//******************************************************************************

bool TMasterCom::ReadIadc( dword &Value)
// Read internal ADC
{
   if( !StartTest( TEST_IADC)){
      return( false);
   }
   return( ReadHex( 3, Value));
} // ReadIadc

//******************************************************************************
// Start USB
//******************************************************************************

bool TMasterCom::StartUsb()
// Start USB test
{
   return( StartTest( TEST_USB));
} // StartUsb

//******************************************************************************
// USB result
//******************************************************************************

bool TMasterCom::UsbResult()
// Return USB test result
{
   return( TestResult( 2));
} // UsbResult

//******************************************************************************
// Enable Charger
//******************************************************************************

bool TMasterCom::EnableCharger( bool Enable)
// <Enable>/disable charger
{
   if( Enable){
      return( Test( TEST_CHARGER_ON,  2));
   } else {
      return( Test( TEST_CHARGER_OFF, 2));
   }
} // EnableCharger

//******************************************************************************
// Test Power
//******************************************************************************

bool TMasterCom::TestPower( bool &PPR, bool &CHG, bool &USBON, bool &ENNAB)
// Read power status
{
   if( !StartTest( TEST_POWER)){
      return( false);
   }
   FCom->SetRxWait( 500, 0);
   // receive reply :
   byte Buffer[ TEST_POWER_REPLY_SIZE];
   if( FCom->Read( Buffer, TEST_POWER_REPLY_SIZE) != TEST_POWER_REPLY_SIZE){
      Status      = "COM receive timeout";
      return( false);
   }
   if( Buffer[ TEST_POWER_REPLY_SIZE - 1] != TEST_OK){
      Status      = "TEST error";
      return( false);
   }
   PPR   = Buffer[ 0] == 'H';
   CHG   = Buffer[ 1] == 'H';
   USBON = Buffer[ 2] == 'H';
   ENNAB = Buffer[ 0] == 'H';
   return( true);
} // TestPower

//******************************************************************************
// Switch OFF
//******************************************************************************

bool TMasterCom::SwitchOff()
// Switch device OFF
{
   return( Test( TEST_OFF, 1));
} // SwitchOff

//------------------------------------------------------------------------------

//******************************************************************************
// Start Test
//******************************************************************************

bool TMasterCom::StartTest( int Command)
// Start test by <Command>
{
   // send data :
   byte Buffer;
   Buffer = (byte)Command;
   if( FCom->Write( &Buffer, 1) != 1){
      Status      = "COM send error";
      return( false);
   }
   return( true);
} // StartTest

//******************************************************************************
// Test result
//******************************************************************************

bool TMasterCom::TestResult( int Timeout)
// Wait <Timeout> for the test result
{
   FCom->SetRxWait( Timeout * 1000, 0);
   // receive reply :
   byte Buffer;
   if( FCom->Read( &Buffer, 1) != 1){
      Status      = "COM receive timeout";
      return( false);
   }
   if( Buffer != TEST_OK){
      Status      = "TEST error";
      return( false);
   }
   return( true);
} // TestResult

//******************************************************************************
// Test
//******************************************************************************

bool TMasterCom::Test( int Command, int Timeout)
// Run simple test by <Command>
{
   if( !StartTest( Command)){
      return( false);
   }
   return( TestResult( Timeout));
} // Test

//******************************************************************************
// Read hex
//******************************************************************************

bool TMasterCom::ReadHex( int Timeout, dword &Value)
// Read hex reply
{
   FCom->SetRxWait( Timeout * 1000, 0);
   // receive reply :
   byte Buffer[ TEST_HEX_REPLY_SIZE];
   if( FCom->Read( Buffer, TEST_HEX_REPLY_SIZE) != TEST_HEX_REPLY_SIZE){
      Status      = "COM receive timeout";
      return( false);
   }
   if( Buffer[ TEST_HEX_REPLY_SIZE - 1] != TEST_OK){
      Status      = "TEST error";
      return( false);
   }
   Buffer[ TEST_HEX_REPLY_SIZE - 1] = '\0';
   sscanf( Buffer, "%x", &Value);
   return( true);
} // ReadHex

