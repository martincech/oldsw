//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1UsbUpgd.res");
USEFORM("MainUsb.cpp", MainForm);
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("UsbTester.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 USB Upgrade";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
