object SetupForm: TSetupForm
  Left = 600
  Top = 301
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Tester Setup'
  ClientHeight = 246
  ClientWidth = 329
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 57
    Height = 13
    Caption = 'Printer port :'
  end
  object CbPrinterPort: TComboBox
    Left = 136
    Top = 24
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'COM1'
      'COM2')
  end
  object CbEnableTester: TCheckBox
    Left = 32
    Top = 72
    Width = 137
    Height = 17
    Caption = 'Enable tester hardware'
    TabOrder = 1
  end
  object CbFtdiProgramming: TCheckBox
    Left = 32
    Top = 104
    Width = 193
    Height = 17
    Caption = 'Enable FTDI Programming'
    TabOrder = 2
  end
  object CbHardwareTesting: TCheckBox
    Left = 32
    Top = 136
    Width = 153
    Height = 17
    Caption = 'Enable Hardware Testing'
    TabOrder = 3
  end
  object CbBatProgramming: TCheckBox
    Left = 32
    Top = 168
    Width = 161
    Height = 17
    Caption = 'Enable Bat1 Programming'
    TabOrder = 4
  end
  object BtnSetupOk: TButton
    Left = 32
    Top = 208
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 5
  end
  object BtnSetupCancel: TButton
    Left = 224
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
end
