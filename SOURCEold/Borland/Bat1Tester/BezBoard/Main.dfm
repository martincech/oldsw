object MainForm: TMainForm
  Left = 436
  Top = 226
  Width = 785
  Height = 647
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 Tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object LblPhase: TLabel
    Left = 518
    Top = 112
    Width = 6
    Height = 13
    Caption = '?'
    Visible = False
  end
  object LblPercent: TLabel
    Left = 582
    Top = 160
    Width = 38
    Height = 13
    Caption = '100.0 %'
    Visible = False
  end
  object Image: TImage
    Left = 512
    Top = 232
    Width = 240
    Height = 160
  end
  object Label1: TLabel
    Left = 512
    Top = 416
    Width = 41
    Height = 13
    Caption = 'Version :'
  end
  object LblVersion: TLabel
    Left = 568
    Top = 416
    Width = 6
    Height = 13
    Caption = '?'
  end
  object MainMemo: TMemo
    Left = 0
    Top = 48
    Width = 497
    Height = 537
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 601
    Width = 777
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnStart: TButton
    Left = 520
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 2
    OnClick = BtnStartClick
  end
  object ProgressBar: TProgressBar
    Left = 518
    Top = 136
    Width = 150
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 3
    Visible = False
  end
  object BtnSetup: TButton
    Left = 520
    Top = 560
    Width = 75
    Height = 25
    Caption = 'Setup'
    TabOrder = 4
    OnClick = BtnSetupClick
  end
end
