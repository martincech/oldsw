//******************************************************************************
//
//   Setup.cpp    Tester setup
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Setup.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TSetupForm *SetupForm;
//---------------------------------------------------------------------------

#define CURRENT_VERSION 0x100

// Setup Write macros :
#define StpWriteString( Section, Parameter) \
   SetupFile->WriteString(  #Section, #Parameter, Parameter)

#define StpWriteInteger( Section, Parameter) \
   SetupFile->WriteInteger(  #Section, #Parameter, Parameter)

#define StpWriteBool( Section, Parameter) \
   SetupFile->WriteBool(  #Section, #Parameter, Parameter)

#define StpWriteFloat( Section, Parameter) \
   SetupFile->WriteFloat(  #Section, #Parameter, Parameter)

#define StpWriteDateTime( Section, Parameter) \
   SetupFile->WriteDateTime(  #Section, #Parameter, Parameter)

// Setup Read macros :
#define StpReadString( Section, Parameter) \
   Parameter = SetupFile->ReadString( #Section, #Parameter, "")

#define StpReadInteger( Section, Parameter) \
   Parameter = SetupFile->ReadInteger( #Section, #Parameter, 0)

#define StpReadBool( Section, Parameter) \
   Parameter = SetupFile->ReadBool( #Section, #Parameter, false)

#define StpReadFloat( Section, Parameter) \
   Parameter = SetupFile->ReadFloat( #Section, #Parameter, 0.0)

#define StpReadDateTime( Section, Parameter) \
   Parameter = SetupFile->ReadDateTime( #Section, #Parameter, 0)

//---------------------------------------------------------------------------

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TSetupForm::TSetupForm(TComponent* Owner)
        : TForm(Owner)
{
   EnableTester    = true;

   FtdiProgramming = true;
   HardwareTesting = true;
   BatProgramming  = true;
   strcpy( PrinterPort, "COM1");
} // TSetupForm

//******************************************************************************
// Execute
//******************************************************************************

bool TSetupForm::Execute()
// Execute dialog
{
   CbEnableTester->Checked    = EnableTester;
   CbFtdiProgramming->Checked = FtdiProgramming;
   CbHardwareTesting->Checked = HardwareTesting;
   CbBatProgramming->Checked  = BatProgramming;
   CbPrinterPort->ItemIndex   = CbPrinterPort->Items->IndexOf( PrinterPort);
   if( ShowModal() != mrOk){
      return( false);
   }
   EnableTester      = CbEnableTester->Checked;
   FtdiProgramming   = CbFtdiProgramming->Checked;
   HardwareTesting   = CbHardwareTesting->Checked;
   BatProgramming    = CbBatProgramming->Checked;
   strcpy( PrinterPort, CbPrinterPort->Text.c_str());
   return( true);
} // Execute

//******************************************************************************
// Save
//******************************************************************************

void TSetupForm::Save()
{
   Version = CURRENT_VERSION;
   // Configuration data :
   StpWriteInteger( Properties, Version);
   StpWriteBool( Properties, EnableTester);
   StpWriteBool( Properties, FtdiProgramming);
   StpWriteBool( Properties, HardwareTesting);
   StpWriteBool( Properties, BatProgramming);
   StpWriteString( Properties, PrinterPort);
} // Save

//******************************************************************************
// Load
//******************************************************************************

void TSetupForm::Load()
{
   AnsiString SetupFileName = ChangeFileExt( Application->ExeName, ".INI" );
   SetupFile    = new TIniFile( SetupFileName);
   // Configuration data :
   StpReadInteger( Properties, Version);
   if( Version != CURRENT_VERSION){
      // invalid file, fill with defaults
      EnableTester    = true;
      FtdiProgramming = true;
      HardwareTesting = true;
      BatProgramming  = true;
      strcpy( PrinterPort, "COM1");
      return;
   }
   // read settings :
   StpReadBool( Properties, EnableTester);
   StpReadBool( Properties, FtdiProgramming);
   StpReadBool( Properties, HardwareTesting);
   StpReadBool( Properties, BatProgramming);
   AnsiString String = SetupFile->ReadString( "Properties", "PrinterPort", "");
   strcpy( PrinterPort, String.c_str());
} // Load

