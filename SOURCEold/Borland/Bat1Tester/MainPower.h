//******************************************************************************
//
//   MainPower.h   Bat1 power tester
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainPowerH
#define MainPowerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "TestPower.h"
#include <ComCtrls.hpp>

//******************************************************************************
// TMainForm
//******************************************************************************

class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TComboBox *CbPrinterPort;
        TLabel *Label1;
        TLabel *Label2;
        TComboBox *CbTesterPort;
        TButton *BtnOpen;
        TStatusBar *StatusBar;
        TButton *BtnAdcTest;
        TButton *BtnIadcTest;
   TLabel *LblVoltage;
   TLabel *LblCurrent;
   TButton *BtnInverted;
   TButton *BtnIdle;
   TButton *BtnNormal;
   TButton *BtnDark;
   TButton *BtnLowVoltage;
   TButton *BtnCharger;
   TButton *BtnUsbCharger;
   TButton *BtnChargerDone;
   TButton *BtnUsbDone;
   TButton *BtnAll;
   TButton *BtnUsbPowerOn;
   TButton *BtnKeyboard;
   TLabel *LblAdcZeroVoltage;
   TLabel *LblAdcMaxVoltage;
   TLabel *LblIadcLowVoltage;
   TLabel *LblIadcHighVoltage;
   TButton *BtnCalibration;
   void __fastcall FormResize(TObject *Sender);
   void __fastcall BtnOpenClick(TObject *Sender);
   void __fastcall BtnAdcTestClick(TObject *Sender);
   void __fastcall BtnIadcTestClick(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall BtnInvertedClick(TObject *Sender);
   void __fastcall BtnIdleClick(TObject *Sender);
   void __fastcall BtnNormalClick(TObject *Sender);
   void __fastcall BtnDarkClick(TObject *Sender);
   void __fastcall BtnLowVoltageClick(TObject *Sender);
   void __fastcall BtnChargerClick(TObject *Sender);
   void __fastcall BtnUsbChargerClick(TObject *Sender);
   void __fastcall BtnChargerDoneClick(TObject *Sender);
   void __fastcall BtnUsbDoneClick(TObject *Sender);
   void __fastcall BtnAllClick(TObject *Sender);
   void __fastcall BtnUsbPowerOnClick(TObject *Sender);
   void __fastcall BtnKeyboardClick(TObject *Sender);
   void __fastcall BtnCalibrationClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   void UpdateVoltage();
   void UpdateCurrent();
   void UpdateAdcVoltage();
   void UpdateIadcVoltage();

   TCrt          *Crt;
   TTestPower    *Adapter;
   TMasterCom    *Com;
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
