object SetupForm: TSetupForm
  Left = 600
  Top = 301
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Tester Setup'
  ClientHeight = 199
  ClientWidth = 341
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 57
    Height = 13
    Caption = 'Printer port :'
  end
  object Label2: TLabel
    Left = 32
    Top = 56
    Width = 57
    Height = 13
    Caption = 'Tester port :'
  end
  object Label3: TLabel
    Left = 32
    Top = 96
    Width = 53
    Height = 13
    Caption = 'Data path :'
  end
  object CbPrinterPort: TComboBox
    Left = 136
    Top = 20
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object BtnSetupOk: TButton
    Left = 32
    Top = 159
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
  end
  object BtnSetupCancel: TButton
    Left = 248
    Top = 151
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object CbTesterPort: TComboBox
    Left = 136
    Top = 52
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object CbWaitForUsb: TCheckBox
    Left = 32
    Top = 128
    Width = 193
    Height = 17
    Caption = 'Wait for USB driver installation'
    TabOrder = 4
  end
  object EdtDataPath: TEdit
    Left = 104
    Top = 91
    Width = 225
    Height = 21
    MaxLength = 255
    TabOrder = 5
    Text = 'Data'
  end
end
