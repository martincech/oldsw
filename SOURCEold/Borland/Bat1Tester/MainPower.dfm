object MainForm: TMainForm
  Left = 328
  Top = 193
  Width = 767
  Height = 681
  Caption = 'Bat1 power tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 543
    Top = 29
    Width = 36
    Height = 13
    Caption = 'Printer :'
  end
  object Label2: TLabel
    Left = 545
    Top = 61
    Width = 36
    Height = 13
    Caption = 'Tester :'
  end
  object LblVoltage: TLabel
    Left = 656
    Top = 376
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblCurrent: TLabel
    Left = 656
    Top = 400
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblAdcZeroVoltage: TLabel
    Left = 624
    Top = 192
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblAdcMaxVoltage: TLabel
    Left = 624
    Top = 209
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblIadcLowVoltage: TLabel
    Left = 624
    Top = 233
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblIadcHighVoltage: TLabel
    Left = 624
    Top = 252
    Width = 6
    Height = 13
    Caption = '?'
  end
  object MainMemo: TMemo
    Left = 8
    Top = 24
    Width = 497
    Height = 593
    TabOrder = 0
  end
  object CbPrinterPort: TComboBox
    Left = 608
    Top = 24
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object CbTesterPort: TComboBox
    Left = 608
    Top = 56
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object BtnOpen: TButton
    Left = 536
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 3
    OnClick = BtnOpenClick
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 635
    Width = 759
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnAdcTest: TButton
    Left = 536
    Top = 200
    Width = 75
    Height = 25
    Caption = 'ADC test'
    TabOrder = 5
    OnClick = BtnAdcTestClick
  end
  object BtnIadcTest: TButton
    Left = 536
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Int ADC test'
    TabOrder = 6
    OnClick = BtnIadcTestClick
  end
  object BtnInverted: TButton
    Left = 536
    Top = 336
    Width = 75
    Height = 25
    Caption = 'Inverted'
    TabOrder = 7
    OnClick = BtnInvertedClick
  end
  object BtnIdle: TButton
    Left = 536
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Idle'
    TabOrder = 8
    OnClick = BtnIdleClick
  end
  object BtnNormal: TButton
    Left = 536
    Top = 400
    Width = 75
    Height = 25
    Caption = 'Normal'
    TabOrder = 9
    OnClick = BtnNormalClick
  end
  object BtnDark: TButton
    Left = 536
    Top = 432
    Width = 75
    Height = 25
    Caption = 'Dark'
    TabOrder = 10
    OnClick = BtnDarkClick
  end
  object BtnLowVoltage: TButton
    Left = 536
    Top = 464
    Width = 75
    Height = 25
    Caption = 'Low Voltage'
    TabOrder = 11
    OnClick = BtnLowVoltageClick
  end
  object BtnCharger: TButton
    Left = 536
    Top = 496
    Width = 75
    Height = 25
    Caption = 'Charger'
    TabOrder = 12
    OnClick = BtnChargerClick
  end
  object BtnUsbCharger: TButton
    Left = 536
    Top = 528
    Width = 75
    Height = 25
    Caption = 'USB Charger'
    TabOrder = 13
    OnClick = BtnUsbChargerClick
  end
  object BtnChargerDone: TButton
    Left = 536
    Top = 560
    Width = 75
    Height = 25
    Caption = 'Charger Done'
    TabOrder = 14
    OnClick = BtnChargerDoneClick
  end
  object BtnUsbDone: TButton
    Left = 536
    Top = 592
    Width = 75
    Height = 25
    Caption = 'USB Done'
    TabOrder = 15
    OnClick = BtnUsbDoneClick
  end
  object BtnAll: TButton
    Left = 656
    Top = 336
    Width = 75
    Height = 25
    Caption = 'All tests'
    TabOrder = 16
    OnClick = BtnAllClick
  end
  object BtnUsbPowerOn: TButton
    Left = 536
    Top = 160
    Width = 75
    Height = 25
    Caption = 'USB power on'
    TabOrder = 17
    OnClick = BtnUsbPowerOnClick
  end
  object BtnKeyboard: TButton
    Left = 536
    Top = 264
    Width = 75
    Height = 25
    Caption = 'Keyboard'
    TabOrder = 18
    OnClick = BtnKeyboardClick
  end
  object BtnCalibration: TButton
    Left = 536
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Calibration'
    TabOrder = 19
    OnClick = BtnCalibrationClick
  end
end
