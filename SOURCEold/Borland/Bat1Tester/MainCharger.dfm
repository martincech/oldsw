object MainForm: TMainForm
  Left = 539
  Top = 298
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Bat1 Charger Test'
  ClientHeight = 291
  ClientWidth = 352
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 19
    Width = 57
    Height = 13
    Caption = 'Printer port :'
  end
  object Label2: TLabel
    Left = 32
    Top = 88
    Width = 102
    Height = 13
    Caption = 'CHARGER_ENNAB :'
  end
  object Label3: TLabel
    Left = 32
    Top = 120
    Width = 72
    Height = 13
    Caption = 'USB_USBON :'
  end
  object LblUsbon: TLabel
    Left = 161
    Top = 120
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label4: TLabel
    Left = 32
    Top = 152
    Width = 88
    Height = 13
    Caption = 'CHARGER_CHG :'
  end
  object LblChg: TLabel
    Left = 161
    Top = 152
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label5: TLabel
    Left = 32
    Top = 184
    Width = 81
    Height = 13
    Caption = 'CHARGER_PPR'
  end
  object LblPpr: TLabel
    Left = 160
    Top = 184
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label6: TLabel
    Left = 8
    Top = 224
    Width = 45
    Height = 13
    Caption = 'Status :'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LblStatus: TLabel
    Left = 64
    Top = 224
    Width = 40
    Height = 13
    Caption = 'Stopped'
  end
  object Label7: TLabel
    Left = 184
    Top = 184
    Width = 134
    Height = 13
    Caption = '(Bat1 board version 7.x only)'
  end
  object EdtPrinterPort: TEdit
    Left = 104
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'COM1'
  end
  object CbEnnab: TCheckBox
    Left = 144
    Top = 86
    Width = 49
    Height = 17
    Caption = '?'
    Checked = True
    State = cbChecked
    TabOrder = 1
    OnClick = CbEnnabClick
  end
  object BtnRun: TButton
    Left = 8
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 2
    OnClick = BtnRunClick
  end
  object BtnStop: TButton
    Left = 272
    Top = 256
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 3
    OnClick = BtnStopClick
  end
  object Timer: TTimer
    Interval = 500
    OnTimer = TimerTimer
    Left = 272
    Top = 16
  end
end
