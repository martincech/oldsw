   Pridani noveho nazvu Bat1
   
1. Pripravit novy INI soubor podle UPGRADE.INI
2. Pripravit nove logo
3. Spustit Bat1Upgrade na prikazove radce nazev 
   noveho INI souboru
4. Do modulu UsbTester.cpp doplnit novy nazev
   USB zarizeni USB_VEIT_NAMEx a prelozit
   znovu vsechny projekty v adresari Bat1Tester
5. Do modulu PktAdapter (Bat1Reader) doplnit
   nove jmeno do CheckConnect()
6. Do modulu MainPgm (Bat1Isp) doplnit nove
   jmeno   
 
Pozn: soubory INI a logo musi byt v adresari, kde
      je Bat1Upgrade.exe
	  