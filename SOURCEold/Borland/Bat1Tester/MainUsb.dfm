object MainForm: TMainForm
  Left = 724
  Top = 374
  Width = 306
  Height = 207
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 USB Upgrade - Windows 7'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 161
    Width = 298
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnUpgrade: TButton
    Left = 176
    Top = 16
    Width = 75
    Height = 25
    Anchors = [akTop, akRight]
    Caption = 'Upgrade'
    TabOrder = 1
    OnClick = BtnUpgradeClick
  end
end
