//******************************************************************************
//
//   TestCalibration.cpp Bat1 power tests
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "TestCalibration.h"

#define USB_CONNECT_DELAY  2000        // USB connect/disconnect delay

// power constants :
#define VOLTAGE_NOMINAL    3600        // nominal accu voltage
#define VOLTAGE_LOW        3400        // low voltage start

// timing :
#define VOLTAGE_DELAY         10       // delay after voltage change
#define INVERTED_POWER_DELAY  2000     // delay after inverted power on
#define IDLE_POWER_DELAY      3000     // delay after idle power on
#define NORMAL_POWER_DELAY    3000     // delay after normal power on
#define BACKLIGHT_DELAY       1000     // delay after backlight change
#define ON_KEY_DELAY          500      // ON key press for switch on
#define OFF_KEY_DELAY         3000     // ON key press for switch off
#define CHARGER_DELAY         1000     // delay after charger/USB switch
#define USB_POWER_ON_DELAY    2000     // delay after USB power on
#define ADC_DELAY             800      // delay after ADC change
#define IADC_DELAY            500      // delay after IADC change

// IADC data :
#define R1         15                  // upper resistor [kOhm]
#define R2         47                  // lower resitor  [kOhm]
#define VREF     3300                  // voltage reference [mV]
#define PACCU_ADC_RANGE    1024        // ADC range [LSB]

#define PACCU_RANGE ((VREF * (R1 + R2)) / R2) // calibration = input voltage range

//******************************************************************************
// Constructor
//******************************************************************************

TTestCalibration::TTestCalibration( TCrt *Crt, TMasterCom *Master)
// Constructor
{
   Voltage  = 0;
   Current  = 0;
   SensitiveCurrent = false;

   FAdapter = new TTestAdapter;
   FCrt     = Crt;
   FMaster  = Master;
   // settings :
   IadcLow              = 3400;
   IadcHigh             = 4200;
} // TTestCalibration

//******************************************************************************
// Destructor
//******************************************************************************

TTestCalibration::~TTestCalibration()
// Destructor
{
   if( FAdapter){
      delete FAdapter;
   }
} // ~TTestCalibration


//******************************************************************************
// Open
//******************************************************************************

bool TTestCalibration::Open( char *ComName)
// Open tester COM
{
   return( FAdapter->Open( ComName));
} // Open

//******************************************************************************
// USB power on
//******************************************************************************

bool TTestCalibration::UsbPowerOn()
// Set defaults & USB power
{
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // switch accumulator on :
   if( !PowerOn( true)){
      return( false);
   }
   // connect USB :
   if( !FAdapter->UsbConnect( true)){
      FCrt->printf( "Tester : Unable connect USB\n");
      return( false);
   }
   Sleep( USB_POWER_ON_DELAY);
   return( true);
} // UsbPowerOn

//******************************************************************************
// Keyboard power on
//******************************************************************************

bool TTestCalibration::KeyboardPowerOn()
// Press/release ON key
{
   // press/release ON key :
   if( !FAdapter->KeyboardOn( true)){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( ON_KEY_DELAY);
   if( !FAdapter->KeyboardOn( false)){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   return( true);
} // KeyboardPowerOn

//******************************************************************************
// ADC calibration
//******************************************************************************

bool TTestCalibration::AdcCalibration()
// Test ADC calibration
{
   int Value;
   //---------------------------------------------------------------------------
   // test zero :
   if( !FAdapter->Bridge( false)){
      FCrt->printf( "Tester : unable control bridge\n");
      return( false);
   }
   Sleep( 2 * ADC_DELAY);                  // wait for stable data
   if( !FMaster->ReadAdc( Value)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   AdcZeroVoltage = Value;
   //---------------------------------------------------------------------------
   // test max
   if( !FAdapter->Bridge( true)){
      FCrt->printf( "Tester : unable control bridge\n");
      return( false);
   }
   Sleep( 2 * ADC_DELAY);                  // wait for stable data
   if( !FMaster->ReadAdc( Value)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   AdcMaxVoltage = Value;
   return( true);
} // AdcCalibration

//******************************************************************************
// Internal ADC calibration
//******************************************************************************

bool TTestCalibration::IadcCalibration()
// Test IADC calibration
{
   int Value;
   //---------------------------------------------------------------------------
   // test low :
   if( !SetVoltage( IadcLow)){
      return( false);
   }
   Sleep( 2 * IADC_DELAY);
   if( !FMaster->ReadIadc( Value)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   Value = (Value * PACCU_RANGE) / PACCU_ADC_RANGE;
   IadcLowVoltage = Value;
   //---------------------------------------------------------------------------
   // test max
   if( !SetVoltage( IadcHigh)){
      return( false);
   }
   Sleep( 2 * IADC_DELAY);
   if( !FMaster->ReadIadc( Value)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   Value = (Value * PACCU_RANGE) / PACCU_ADC_RANGE;
   IadcHighVoltage = Value;
   return( true);
} // IadcCalibration

//******************************************************************************
// Normal current
//******************************************************************************

bool TTestCalibration::NormalCurrent()
// Check current with backlight
{
   FCrt->printf( "POWER normal...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // get current :
   int MCurrent;
   if( !FAdapter->GetCurrent( MCurrent)){
      FCrt->printf( "Tester : unable read current\n");
      return( false);
   }
   SensitiveCurrent = false;
   Current          = MCurrent;
   return( true);
} // NormalCurrent

//******************************************************************************
// Dark current
//******************************************************************************

bool TTestCalibration::DarkCurrent()
// Check current without backlight
{
   FCrt->printf( "POWER without backlight...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // switch backlight off :
   if( !FMaster->SetBacklight( false)){
      FCrt->printf( "COM : unable switch backlight off\n");
      return( false);
   }
   Sleep( BACKLIGHT_DELAY);
   // get current :
   int MCurrent;
   if( !FAdapter->GetCurrent( MCurrent)){
      FCrt->printf( "Tester : unable read current\n");
      return( false);
   }
   SensitiveCurrent = false;
   Current          = MCurrent;
   return( true);
} // DarkCurrent

//------------------------------------------------------------------------------

//******************************************************************************
// Power on
//******************************************************************************

bool TTestCalibration::PowerOn( bool On)
// Switch power <On>
{
   if( On){
      // set minimum voltage :
      if( !FAdapter->VoltageMin()){
         FCrt->printf( "Tester : unable set minimum voltage\n");
         return( false);
      }
      if( !FAdapter->SetPower( POWER_NORMAL)){
         FCrt->printf( "Tester : unable set power ON\n");
         return( false);
      }
      // set nominal voltage
      if( !SetVoltage( VOLTAGE_NOMINAL)){
         return( false);
      }
      return( true);
   } else {
      // switch power off :
      if( !FAdapter->SetPower( POWER_OFF)){
         FCrt->printf( "Tester : unable set power OFF\n");
         return( false);
      }
      // set defaults :
      if( !FAdapter->SetDefaults()){
         FCrt->printf( "Tester : set defaults failure\n");
         return( false);
      }
      return( true);
   }
} // PowerOn

//******************************************************************************
// Set voltage
//******************************************************************************

bool TTestCalibration::SetVoltage( int Voltage)
// Set power <Voltage> [mV]
{
int MVoltage;

   if( !FAdapter->VoltageMin()){
      FCrt->printf( "Tester : unable set minimum voltage\n");
      return( false);
   }
   Sleep( VOLTAGE_DELAY);
   for( int i = 0; i < VOLTAGE_STEPS; i++){
      if( !FAdapter->GetVoltage( MVoltage)){
         FCrt->printf( "Tester : unable read voltage\n");
         return( false);
      }
      this->Voltage = MVoltage;
      if( MVoltage >= Voltage){
         return( true);
      }
      if( !FAdapter->VoltageUp()){
         FCrt->printf( "Tester : unable set voltage\n");
         return( false);
      }
      Sleep( VOLTAGE_DELAY);
   }
   FCrt->printf( "Tester : unable set voltage %d mV\n", Voltage);
   return( false);
} // SetVoltage


