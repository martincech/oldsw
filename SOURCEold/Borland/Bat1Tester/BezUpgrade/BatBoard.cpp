//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("BatBoard.res");
USEFORM("MainBoard.cpp", MainForm);
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("TestAdapter.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 tester board";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
