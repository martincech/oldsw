//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Crt/Crt.h"
#include "TestPower.h"
#include "UsbTester.h"
#include "MasterCom.h"
#include "../ArmPgm/HexFile.h"
#include "../ArmPgm/LpcFlash.h"
#include "../Bat1Reader/BatDevice.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnStart;
        TLabel *LblPhase;
        TProgressBar *ProgressBar;
        TLabel *LblPercent;
        TButton *BtnSetup;
        TImage *Image;
        TLabel *Label1;
        TLabel *LblVersion;
   TButton *BtnCalibration;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnStartClick(TObject *Sender);
        void __fastcall BtnSetupClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall BtnCalibrationClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt          *Crt;
   TUsbTester    *Usb;
   TTestPower    *Adapter;
   TMasterCom    *Com;
   THexFile      *Hex;
   TLpcFlash     *Lpc;
   TBatDevice    *Bat;

   __fastcall TMainForm(TComponent* Owner);
   void Progress( int Percent);
   void StartComm();
   void ContinueComm();
   void StopComm();

   bool TesterDefaults();
   void TesterShutdown();
   bool FtdiProgramming();
   bool WriteTestFirmware();
   bool MasterComTest();
   bool DeviceHardwareTest();
   bool AdcCalibrationTest();
   bool IadcCalibrationTest();
   bool KeyboardTest();
   bool PowerTest();
   bool WriteBatFirmware();
   bool BatTest();

   bool WriteFirmware( AnsiString FileName);
   void PrepareLogo();
   bool BatSetTime();
   bool BatCreateFile( char *FileName);
   bool BatWriteLogo();
   bool BatResetDefaults();

   void Play( int SoundType);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
