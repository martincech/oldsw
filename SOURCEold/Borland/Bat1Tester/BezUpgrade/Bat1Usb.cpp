//******************************************************************************
//
//   Bat1Usb.cpp      Bat1 USB communication
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Bat1Usb.h"

// Bat1 definitions :
#include "../Library/Unisys/Dt.h"
#include "../Bat1Reader/MemoryDef.h"
#include "../Bat1Reader/SdbDef.h"
#include "../Bat1Reader/Bat1.h"
#include "../Bat1Reader/BeepDef.h"
#include "../Bat1Reader/ConfigSet.c"  // factory default config

// Color transformation :
#define COLOR_BLACK      0x00000000
#define COLOR_MIDDLEGRAY 0x007F7F7F
#define COLOR_WHITE      0x00FFFFFF
#define COLOR_LIGHTGRAY  0x00555555
#define COLOR_DARKGRAY   0x00AAAAAA

//******************************************************************************
// Constructor
//******************************************************************************

TBat1Usb::TBat1Usb()
{
   TPktAdapter *PAdapter = new TPktAdapter;
   Bat          = new TBatDevice;
   Bat->Adapter = PAdapter;
} // TBat1Usb

//******************************************************************************
// Destructor
//******************************************************************************

TBat1Usb::~TBat1Usb()
{
   if( !Bat){
      return;
   }
   if( Bat->Adapter){
      delete Bat->Adapter;
   }
   delete Bat;
} // ~TBat1Usb

//------------------------------------------------------------------------------

//******************************************************************************
// Get config
//******************************************************************************

TConfig *TBat1Usb::GetConfig()
{
   return( &BatConfig.Config);
} // GetConfig

