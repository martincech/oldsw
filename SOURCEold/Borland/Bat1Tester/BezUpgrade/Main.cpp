//******************************************************************************
//
//   Main.cpp     Bat1 Tester main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>
#include <mmsystem.h>

#include "Main.h"
#include "Setup.h"
#include "Calibration.h"
#include "../Library/Unisys/Dt.h"

// Bat1 definitions :
#include "../Bat1Reader/MemoryDef.h"
#include "../Bat1Reader/SdbDef.h"
#include "../Bat1Reader/Bat1.h"
#include "../Bat1Reader/BeepDef.h"
#include "../Bat1Reader/ConfigSet.c"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//---------------------------------------------------------------------------

#define USB_VEIT_NAME          "VEIT BAT1 Poultry Scale"
#define USB_VEIT_MANUFACTURER  "VEIT"

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

TLogo        Logo;                // Global logo buffer
TConfigUnion BatConfig;           // Global bat configuration

// Color transformation :
#define COLOR_BLACK      0x00000000
#define COLOR_MIDDLEGRAY 0x007F7F7F
#define COLOR_WHITE      0x00FFFFFF
#define COLOR_LIGHTGRAY  0x00555555
#define COLOR_DARKGRAY   0x00AAAAAA

// sound types :
typedef enum {
   SOUND_OK,
   SOUND_FAILURE,
} TSoundType;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Crt = new TCrt( MainMemo);
   // tester :
   Usb     = new TUsbTester;
   Com     = new TMasterCom;
   Adapter = new TTestPower( Crt, Com);
   // programmer :
   Hex     = new THexFile( 512 * 1024);
   Lpc     = new TLpcFlash;
   Lpc->BaudRate               = 57600;
   Lpc->CrystalFrequency       = 18432000;
   strcpy( Lpc->DeviceName, USB_VEIT_NAME);
   Lpc->ViaUsb                 = true;
   Lpc->ProgressFunction       = this->Progress;
   Lpc->EnableFlashComparation = true;
   // bat interface :
   TPktAdapter *PAdapter = new TPktAdapter;
   Bat  = new TBatDevice;
   Bat->Adapter = PAdapter;
   // setup form :
   SetupForm = new TSetupForm( MainForm);
   // calibration form :
   CalibrationForm = new TCalibrationForm( MainForm);
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   // load & prepare logo :
   Image->Picture->LoadFromFile( "Data\\Logo.bmp");
   PrepareLogo();
   // prepare configuration :
   memset( &BatConfig, 0, sizeof( BatConfig));
   BatConfig.Config = _DefaultConfig;  // copy default configuration
   // show version number :
   AnsiString Version;
   Version.printf( "%d.%02d.%d", (BatConfig.Config.Version >> 8) & 0xFF,
                                  BatConfig.Config.Version       & 0xFF,
                                  BatConfig.Config.Build);
   LblVersion->Caption = Version;
   // load setup data :
   SetupForm->Load();
   CalibrationForm->Load();
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Close
//******************************************************************************

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
   SetupForm->Save();                  // save setup data
   CalibrationForm->Save();            // save calibration data
} // FormClose

//******************************************************************************
// Progress
//******************************************************************************

void TMainForm::Progress( int Percent)
// Update progress bar
{
   ProgressBar->Position = Percent / 10;
   AnsiString String;
   String.printf( "%d %%", Percent / 10);
   LblPercent->Caption = String;
   Screen->Cursor = crDefault;         // restore cursor
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // Progress

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   this->Enabled  = false;             // disable main window
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // StartCom

//******************************************************************************
// Communication flow
//******************************************************************************

void TMainForm::ContinueComm()
// Communication flow
{
     Screen->Cursor = crDefault;       // restore cursor
     Application->ProcessMessages();
     Screen->Cursor = crHourGlass;
} // ContinueComm

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   this->Enabled  = true;              // enable main window
   Screen->Cursor = crDefault;         // normal cursor
} // StopComm

//******************************************************************************
//  Start
//******************************************************************************

void __fastcall TMainForm::BtnStartClick(TObject *Sender)
{
   Crt->Clear();
   StartComm();
   if( !TesterDefaults()){
      goto fail;
   }
   if( SetupForm->FtdiProgramming){
      if( !FtdiProgramming()){
         goto fail;
      }
      ContinueComm();
   }
   if( SetupForm->HardwareTesting){
      if( !Com->Open( SetupForm->PrinterPort)){
         Crt->printf( "Error : Unable open printer port %s\n", SetupForm->PrinterPort);
         goto fail;
      }
      if( SetupForm->TestFirmwareProgramming){
         if( !WriteTestFirmware()){
            goto fail;
         }
         ContinueComm();
      }
      if( !MasterComTest()){
         goto fail;
      }
      ContinueComm();
      if( !DeviceHardwareTest()){
         goto fail;
      }
      ContinueComm();
      if( !AdcCalibrationTest()){
         goto fail;
      }
      ContinueComm();
      if( !IadcCalibrationTest()){
         goto fail;
      }
      ContinueComm();
      if( !KeyboardTest()){
         goto fail;
      }
      ContinueComm();
      if( !PowerTest()){
         goto fail;
      }
      ContinueComm();
      // switch USB power on :
      if( SetupForm->EnableTester){
         if( !Adapter->UsbPowerOn()){
            goto fail;
         }
         Sleep( 1000);
      }
   }
   if( SetupForm->BatProgramming){
      if( !WriteBatFirmware()){
         goto fail;
      }
      ContinueComm();
      if( !BatTest()){
         goto fail;
      }
   }
   Play( SOUND_OK);
   Crt->printf( "Q.C. PASSED !\n");
   StatusOk();
   TesterShutdown();
   StopComm();
   return;
fail :
   Play( SOUND_FAILURE);
   TesterShutdown();
   StopComm();
} // BtnStartClick

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::BtnSetupClick(TObject *Sender)
{
   SetupForm->Execute();
} // BtnSetupClick

//******************************************************************************
// Calibration
//******************************************************************************

void __fastcall TMainForm::BtnCalibrationClick(TObject *Sender)
{
   CalibrationForm->Execute();
} // BtnCalibrationClick

//******************************************************************************
// Tester defaults
//******************************************************************************

bool TMainForm::TesterDefaults()
{
   Crt->printf( "Start...\n");
   // copy configuration & calibration
   Adapter->EnableHardware     = SetupForm->EnableTester;
   Adapter->CurrentNormal      = CalibrationForm->CurrentNormal;
   Adapter->CurrentNormalRange = CalibrationForm->CurrentNormalRange;
   Adapter->CurrentDark        = CalibrationForm->CurrentDark;
   Adapter->CurrentDarkRange   = CalibrationForm->CurrentDarkRange;
   Adapter->AdcZero            = CalibrationForm->AdcZero;
   Adapter->AdcZeroRange       = CalibrationForm->AdcZeroRange;
   Adapter->AdcMax             = CalibrationForm->AdcMax;
   Adapter->AdcMaxRange        = CalibrationForm->AdcMaxRange;
   Adapter->IadcLow            = CalibrationForm->IadcLow;
   Adapter->IadcLowRange       = CalibrationForm->IadcLowRange;
   Adapter->IadcHigh           = CalibrationForm->IadcHigh;
   Adapter->IadcHighRange      = CalibrationForm->IadcHighRange;
   // setup tester hardware :
   if( SetupForm->EnableTester){
      // open tester port :
      if( !Adapter->Open( SetupForm->TesterPort)){
         Crt->printf( "Error : Unable open tester port %s\n", SetupForm->TesterPort);
         return( false);
      }
      // calibrate tester :
      if( !Adapter->Calibration()){
         return( false);
      }
      // switch USB power on :
      if( !Adapter->UsbPowerOn()){
         return( false);
      }
   }
   return( true);
} // TesterDefaults

//******************************************************************************
// Tester shutdown
//******************************************************************************

void TMainForm::TesterShutdown()
{
   Crt->printf( "Shutdown...\n");
   if( SetupForm->EnableTester){
      // disconnect USB :
      Adapter->Shutdown();
   }
} // TesterShutdwon

//******************************************************************************
//  FTDI programming
//******************************************************************************

bool TMainForm::FtdiProgramming()
{
   //---------------------------------------------------------------------------
   Crt->printf( "FTDI programming...\n");
   // USB power on :
   if( !Adapter->UsbConnect( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( SetupForm->WaitForUsb){
      Application->MessageBox( "Wait for USB driver installation", "Information", MB_OK);
   }
   // programming device :
   if( !Usb->WaitForNewDevice()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   Crt->printf( "Device : %s\n", Usb->AdapterName.c_str());
   if( !Usb->WriteEeprom( USB_VEIT_NAME, USB_VEIT_MANUFACTURER)){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "FTDI driver installation...\n");
   // USB power off :
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDisconnect()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   if( !Adapter->UsbConnect( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDevice()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   Crt->printf( "Device : %s\n", Usb->AdapterName.c_str());
   return( true);
} // FtdiProgramming

//******************************************************************************
// Write test firmware
//******************************************************************************

bool TMainForm::WriteTestFirmware()
{
   if( !WriteFirmware( "Data\\Bat1Test.hex")){
      return( false);
   }
   return( true);
} // WriteTestFirmware

//******************************************************************************
// Master COM test
//******************************************************************************

bool TMainForm::MasterComTest()
{
   Crt->printf( "COM test...\n");
   if( !Com->TestUart()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   return( true);
} // MasterComTest

//******************************************************************************
// Device HW test
//******************************************************************************

bool TMainForm::DeviceHardwareTest()
{
   //---------------------------------------------------------------------------
   Crt->printf( "USB test...\n");
   if( !Com->StartUsb()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   if( !Usb->TestUart()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   if( !Com->UsbResult()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   ContinueComm();
   //---------------------------------------------------------------------------
   Crt->printf( "DISPLAY test...\n");
   if( !Com->TestDisplay()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "MEMORY test...\n");
   if( !Com->TestMemory()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   ContinueComm();
   //---------------------------------------------------------------------------
   Crt->printf( "RTC test...\n");
   if( !Com->TestRtc()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "SOUND test...\n");
   if( !Com->TestSound()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   return( true);
} // DeviceHardwareTest

//******************************************************************************
// ADC calibration test
//******************************************************************************

bool TMainForm::AdcCalibrationTest()
{
   int Value;
   Crt->printf( "ADC test...\n");
   if( !SetupForm->EnableTester){
      if( !Com->ReadAdc( Value)){
         Crt->printf( "Error : %s\n", Com->Status.c_str());
         return( false);
      }
      Crt->printf( "   ADC : %08X\n", Value);
      return( true);
   }
   // with hardware tester :
   return( Adapter->AdcCalibration());
} // AdcCalibrationTest

//******************************************************************************
// Internal ADC calibration test
//******************************************************************************

bool TMainForm::IadcCalibrationTest()
{
   int Value;
   Crt->printf( "IADC test...\n");
   if( !SetupForm->EnableTester){
      if( !Com->ReadIadc( Value)){
         Crt->printf( "Error : %s\n", Com->Status.c_str());
         return( false);
      }
      Crt->printf( "   IADC : %08X\n", Value);
      return( true);
   }
   // with hardware tester :
   return( Adapter->IadcCalibration());
} // IadcCalibrationTest

//******************************************************************************
// Keyboard test
//******************************************************************************

bool TMainForm::KeyboardTest()
{
   bool On, K0, K1, K2;
   Crt->printf( "KBD test...\n");
   if( !SetupForm->EnableTester){
      if( !Com->TestKeyboard( On, K0, K1, K2)){
         Crt->printf( "Error : %s\n", Com->Status.c_str());
         return( false);
      }
      Crt->printf( "   KBD : ON : %c K0 %c K1 %c K2 %c\n",
                   On ? 'H' : 'L',
                   K0 ? 'H' : 'L',
                   K1 ? 'H' : 'L',
                   K2 ? 'H' : 'L');
      return( true);
   }
   // with hardware tester :
   return( Adapter->Keyboard());
} // IadcKeyboardTest

//******************************************************************************
// Power test
//******************************************************************************

bool TMainForm::PowerTest()
{
   bool PPR, CHG, USBON, ENNAB;
   if( !SetupForm->EnableTester){
      Crt->printf( "POWER test...\n");
      if( !Com->TestPower( PPR, CHG, USBON, ENNAB)){
         Crt->printf( "Error : %s\n", Com->Status.c_str());
         return( false);
      }
      Crt->printf( "   PPR : %c, CHG : %c, USBON : %c\n",
                   PPR   ? 'H' : 'L',
                   CHG   ? 'H' : 'L',
                   USBON ? 'H' : 'L');
      return( true);
   }
   // with hardware tester :
   return( Adapter->Power());
} // PowerTest

//******************************************************************************
// Write BAT firmware
//******************************************************************************

bool TMainForm::WriteBatFirmware()
{
   return( WriteFirmware( "Data\\Bat1.hex"));
} // WriteBatFirmware

//******************************************************************************
// Test done
//******************************************************************************

bool TMainForm::BatTest()
{
   Crt->printf( "BAT1 test...\n");
   // find device :
   bool BatFound = false;
   for( int i = 0; i < 3; i++){
      if( Bat->Check()){
         BatFound = true;
         break;
      }
   }
   if( !BatFound){
      Crt->printf( "Error : Unable connect Bat1\n");
      return( false);
   }
   // clock
   Crt->printf( "BAT1 clock...\n");
   if( !BatSetTime()){
      Crt->printf( "Error : Unable set clock\n");
      return( false);
   }
   // format :
   Crt->printf( "BAT1 format...\n");
   if( !Bat->FormatFilesystem()){
      Crt->printf( "Error : Unable format filesystem\n");
      return( false);
   }
   // create file :
   Crt->printf( "BAT1 create file...\n");
   if( !BatCreateFile( "FILE000")){
      Crt->printf( "Error : Unable create file\n");
      return( false);
   }
   // reset defaults :
   Crt->printf( "BAT1 save configuration...\n");
   if( !BatResetDefaults()){
      Crt->printf( "Error : Unable save configuration\n");
      return( false);
   }
   // write logo :
   Crt->printf( "BAT1 write logo...\n");
   if( !BatWriteLogo()){
      Crt->printf( "Error : Unable write logo\n");
      return( false);
   }
   if( SetupForm->EnableTester){
      Crt->printf( "BAT1 power test...\n");
      // switch power on :
      if( !Adapter->KeyboardPowerOn()){
         return( false);
      }
      Sleep( 5000);                       // wait for scale ready
      // switch power off :
      if( !Adapter->KeyboardPowerOff()){
         return( false);
      }
   }
   // disconnect USB power :
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   return( true);
} // BatTest

//******************************************************************************
// Write Firmware
//******************************************************************************

bool TMainForm::WriteFirmware( AnsiString FileName)
{
   Crt->printf( "FIRMWARE Write : %s\n", FileName.c_str());
   if( !Hex->Load( FileName.c_str())){
      Crt->printf( "ERROR : open file '%s'\n", FileName.c_str());
      return( false);
   }
   // show progress :
   ProgressBar->Visible = true;
   LblPercent->Visible  = true;
   LblPhase->Visible    = true;
   Progress( 0);
   // programming :
   LblPhase->Caption = "Check :   ";
   LblPhase->Refresh();
   if( !Lpc->GetDeviceInfo()){
      Crt->printf( "Unable get device info\n");
      return( false);
   }
   LblPhase->Caption = "Write :   ";
   LblPhase->Refresh();
   if( !Lpc->WriteFlash( 0, Hex->Code, Hex->CodeSize)){
      Crt->printf( "ERROR : Unable write code\n");
      return( false);
   }
   LblPhase->Caption = "Verify :  ";
   LblPhase->Refresh();
   if( !Lpc->CompareFlash( 0, Hex->Code, Hex->CodeSize)){
      Crt->printf( "ERROR : Verification failed\n");
      return( false);
   }
   LblPhase->Caption = "Reset :   ";
   LblPhase->Refresh();
   if( !Lpc->Reset()){
      Crt->printf( "ERROR : reset failed\n");
      return( false);
   }
   Lpc->Close();                       // release port
   // hide progress :
   Progress( 0);
   ProgressBar->Visible  = false;
   LblPercent->Visible   = false;
   LblPhase->Visible     = false;
   LblPhase->Caption     = "?";
   // wait for reset :
   Sleep( 2000);
   if( SetupForm->HardwareTesting){
      Com->Flush();                    // clear garbage on master com
   }
   return( true);
} // WriteFirmware

//******************************************************************************
// Prepare logo
//******************************************************************************

void TMainForm::PrepareLogo()
{
   // prepare data :
   memset( Logo, 0, sizeof( Logo));
   for( int x = 0; x < LOGO_WIDTH; x++){
      for( int y = 0; y < LOGO_HEIGHT; y ++){
         TColor Color = Image->Picture->Bitmap->Canvas->Pixels[ x][ y];
         int Yaddr = y / 8;
         int Ymask = 1 << (y % 8);
         if( Color == COLOR_WHITE){
            continue;
         }
         // plane 0
         if( Color == COLOR_BLACK || Color >= COLOR_MIDDLEGRAY){ // light gray
            Logo[ 0][ Yaddr][x] |= Ymask;
         }
         // plane 1
         if( Color == COLOR_BLACK || Color <  COLOR_MIDDLEGRAY){ // dark gray
            Logo[ 1][ Yaddr][x] |= Ymask;
         }
      }
   }
} // PrepareLogo

//******************************************************************************
// Set Time
//******************************************************************************

bool TMainForm::BatSetTime()
{
   // get current date/time :
   TDateTime Dt = TDateTime::CurrentDateTime();
   unsigned short Day, Month, Year, Hour, Min, Sec, Msec;
   Dt.DecodeDate( &Year, &Month, &Day);
   Dt.DecodeTime( &Hour, &Min, &Sec, &Msec);
   // internal representation
   TLocalTime Local;
   Local.Day   = Day;
   Local.Month = Month;
   Local.Year  = Year - 2000;
   Local.Hour  = Hour;
   Local.Min   = Min;
   Local.Sec   = Sec;
   TTimestamp ts = DtEncode( &Local);
   if( !Bat->SetTime( ts)){
      return( false);
   }
   return( true);
} // BatSetTime

//******************************************************************************
// Create File
//******************************************************************************

bool TMainForm::BatCreateFile( char *FileName)
{
   int Handle;
   if( !Bat->FileCreate( FileName, "", SDB_CLASS, Handle)){
      Bat->FileClose();
      return( false);
   }
   if( !Bat->FileWrite( &BatConfig.Config.WeighingParameters, sizeof( TSdbConfig))){
       Bat->FileClose();
       return( false);
   }
   if( !Bat->FileClose()){
      return( false);
   }
   BatConfig.Config.LastFile = Handle; // active file
   return( true);
} // BatCreateFile

//******************************************************************************
// Write Logo
//******************************************************************************

bool TMainForm::BatWriteLogo()
{
   if( !Bat->WriteMemory( offsetof( TEeprom, Logo), &Logo, sizeof( Logo))){
      return( false);
   }
   return( true);
} // BatWriteLogo

//******************************************************************************
// Reset defaults
//******************************************************************************

bool TMainForm::BatResetDefaults()
{
byte       *p;
TConfigCrc Crc;

   // update CRC :
   Crc  = 0;
   p   = (byte *)&BatConfig;
   for( int i = 0; i < CONFIG_DATA_SIZE; i++){
      Crc += *p;
      p++;
   }
   BatConfig.Spare.CheckSum = ~Crc;
   // write configuration :
   if( !Bat->WriteMemory( offsetof( TEeprom, Config), &BatConfig, sizeof( BatConfig))){
      return( false);
   }
   if( !Bat->ReloadConfig()){                  // update working configuration
      return( false);
   }
   return( true);
} // BatResetDefaults

//******************************************************************************
// Play
//******************************************************************************

void TMainForm::Play( int SoundType)
{
   AnsiString Path;
   AnsiString FileName;
   UINT       Beep;
   Path = ExtractFilePath( Application->ExeName);
   switch( SoundType){
      case SOUND_OK :
         FileName = "Success.wav";
         Beep     = MB_OK;
         break;

      case SOUND_FAILURE :
         FileName = "Error.wav";
         Beep     = MB_ICONEXCLAMATION;
         break;
   }
   Path += FileName;
   if( !FileExists( Path)){
      MessageBeep( Beep);
      return;
   }
   ::PlaySound( Path.c_str(), NULL, SND_FILENAME | SND_ASYNC);
} // Play


