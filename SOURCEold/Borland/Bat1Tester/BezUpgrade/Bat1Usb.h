//******************************************************************************
//
//   Bat1Usb.h       Bat1 USB communication
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef Bat1UsbH
#define Bat1UsbH

#include "../Bat1Reader/BatDevice.h"
#include "../Bat1Reader/MemoryDef.h"

//******************************************************************************
// Bat1Usb class
//******************************************************************************

class TBat1Usb {
public :
   TBat1Usb();
   ~TBat1Usb();

   __property TConfig *Config = {read=GetConfig};
protected :
   TBatDevice  *Bat;                 // USB Communication
   TLogo        Logo;                // Logo buffer
   TConfigUnion BatConfig;           // Bat configuration

   TConfig *GetConfig();             // pointer to BatConfig.Config
}; // TBat1Usb

#endif
