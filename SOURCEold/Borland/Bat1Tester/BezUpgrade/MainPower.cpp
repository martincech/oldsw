//******************************************************************************
//
//   MainPower.cpp  Bat1 power tests
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainPower.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";
#define StatusQ()    Status() + "?";StatusBar->Refresh()

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Crt     = new TCrt( MainMemo);
   Com     = new TMasterCom;
   Adapter = new TTestPower( Crt, Com);
   Adapter->EnableHardware = true;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   CbPrinterPort->ItemIndex = 0;
   CbTesterPort->ItemIndex  = 1;
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Open
//******************************************************************************

void __fastcall TMainForm::BtnOpenClick(TObject *Sender)
{
   if( !Com->Open( CbPrinterPort->Text.c_str())){
      Crt->printf( "Error : unable open printer port\n");
      return;
   }
   if( !Adapter->Open( CbTesterPort->Text.c_str())){
      Crt->printf( "Error : unable open tester port\n");
      return;
   }
   StatusOk();
} // BtnOpenClick

//******************************************************************************
// Calibration
//******************************************************************************

void __fastcall TMainForm::BtnCalibrationClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->Calibration()){
      return;
   }
   StatusOk();
}

//******************************************************************************
// ADC test
//******************************************************************************

void __fastcall TMainForm::BtnAdcTestClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->AdcCalibration()){
      UpdateAdcVoltage();
      return;
   }
   UpdateAdcVoltage();
   StatusOk();
}

//******************************************************************************
// IADC test
//******************************************************************************


void __fastcall TMainForm::BtnIadcTestClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->IadcCalibration()){
      UpdateVoltage();
      UpdateIadcVoltage();
      return;
   }
   UpdateVoltage();
   UpdateIadcVoltage();
   StatusOk();
}

//******************************************************************************
// Inverted
//******************************************************************************

void __fastcall TMainForm::BtnInvertedClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->InvertedPower()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
   StatusOk();
}

//******************************************************************************
// Idle
//******************************************************************************

void __fastcall TMainForm::BtnIdleClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->IdleCurrent()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
   StatusOk();
}

//******************************************************************************
// Normal
//******************************************************************************

void __fastcall TMainForm::BtnNormalClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->NormalCurrent()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
   StatusOk();
}

//******************************************************************************
// Dark
//******************************************************************************

void __fastcall TMainForm::BtnDarkClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->DarkCurrent()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
   StatusOk();
}

//******************************************************************************
// Low voltage
//******************************************************************************

void __fastcall TMainForm::BtnLowVoltageClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->LowVoltage()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
   StatusOk();
}

//******************************************************************************
// Charger
//******************************************************************************

void __fastcall TMainForm::BtnChargerClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->Charger()){
      return;
   }
   StatusOk();
}

//******************************************************************************
// USB charger
//******************************************************************************

void __fastcall TMainForm::BtnUsbChargerClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->UsbCharger()){
      return;
   }
   StatusOk();
}

//******************************************************************************
// Charger done
//******************************************************************************

void __fastcall TMainForm::BtnChargerDoneClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->ChargerDone()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
   StatusOk();
}

//******************************************************************************
// USB done
//******************************************************************************

void __fastcall TMainForm::BtnUsbDoneClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->UsbChargerDone()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
   StatusOk();
}

//******************************************************************************
// Test all
//******************************************************************************

void __fastcall TMainForm::BtnAllClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->Power()){
      UpdateVoltage();
      UpdateCurrent();
      return;
   }
   StatusOk();
}

//******************************************************************************
// USB power on
//******************************************************************************

void __fastcall TMainForm::BtnUsbPowerOnClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->UsbPowerOn()){
      return;
   }
   StatusOk();
}

//******************************************************************************
// Keyboard
//******************************************************************************

void __fastcall TMainForm::BtnKeyboardClick(TObject *Sender)
{
   StatusQ();
   if( !Adapter->Keyboard()){
      return;
   }
   StatusOk();
}

//------------------------------------------------------------------------------

//******************************************************************************
// Update measuring
//******************************************************************************

void TMainForm::UpdateVoltage()
{
   LblVoltage->Caption = FloatToStrF( (double)Adapter->Voltage / 1000, ffFixed, 6, 3) + AnsiString( "V");
} // UpdateVoltage

void TMainForm::UpdateCurrent()
{
   if( Adapter->SensitiveCurrent){
      LblCurrent->Caption = FloatToStrF( (double)Adapter->Current / 100, ffFixed, 7, 2) + AnsiString( " uA");
   } else {
      LblCurrent->Caption = IntToStr( Adapter->Current) + AnsiString( " mA");
   }
} // UpdateCurrent

//******************************************************************************
// ADC voltage
//******************************************************************************

void TMainForm::UpdateAdcVoltage()
{
   LblAdcZeroVoltage->Caption = IntToStr( Adapter->AdcZeroVoltage) + AnsiString( " LSB");
   LblAdcMaxVoltage->Caption  = IntToStr( Adapter->AdcMaxVoltage)  + AnsiString( " LSB");
} // UpdateAdcVoltage

//******************************************************************************
// IADC voltage
//******************************************************************************

void TMainForm::UpdateIadcVoltage()
{
   LblIadcLowVoltage->Caption  = FloatToStrF( (double)Adapter->IadcLowVoltage  / 1000, ffFixed, 6, 3)  + AnsiString( "V");
   LblIadcHighVoltage->Caption = FloatToStrF( (double)Adapter->IadcHighVoltage / 1000, ffFixed, 6, 3) + AnsiString( "V");
} // UpdateIadcVoltage

