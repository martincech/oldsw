KALIBRACE TESTERU
-------------------------------------------------------------

1. Tester pripojen k PC a napajeni

2. Pripojit Bat1 k testeru

3. Spustit BatBoard.exe

4. Nastavit spravny COM

5. Stisknout Reset Defaults

6. Stisknout Power mode = Power normal

7. Zaskrtnout Automatic measuring

8. Pod boxem Power mode se zobrazuje napeti
   Voltage : xxx V
   
9. Stisknout Voltage regulation : Min a trimrem
   R11 (LO) nastavit napeti 3.000 V
   
10. Stisknout Voltage regulation : Max a trimrem
   R12 (HI) nastavit napeti 4.500 V
   
11. Opakovat kroky 9 a 10 tak dlouho az budou napeti
    odpovidat (trimry se vzajemne ovlivnuji).
	(staci s presnosti na cca 10mV)
	
12. Zaskrtnout Keyboard : Keyboard ON (Bat1 se zapne)
    Zkontrolovat zda po stisknuti 
    - Voltage regulation Min je napeti max 3.100 V
	- Voltage regulation Max je napeti min 4.400 V
	

TESTOVANI TESTERU

-----------------------------------------------------

1. Tester pripojen k PC a napajeni

2. Pripojit Bat1 k testeru. V Bat1 musi byt
   vypalen testovaci firmware Bat1test.hex,
   musi byt naprogramovat USB chip

3. Spustit BatPower.exe

4. Nastavit spravny COM pro tester a Bat1

5. Stisknout Open, na spodni radce programu
   se zobrazi Status : OK
   
6. Stisknout USB power on ( zkontrolovat Status : OK)

7. Stisknout ADC test (trva nekolik sekund),
   zkontrolovat Status : OK
   
8. Stisknout Int ADC test zkontrolovat Status : OK

9. Stisknout Keyboard zkontrolovat Status : OK

10. Stisknout All tests. Pokud nektery test neprojde,
   je mozne spustit jednotlive testy tlacitky Inverted,
   Idle... a zjistit ktery test havaruje

NASTAVENI JMENOVITYCH HODNOT DO TESTOVACIHO PROGRAMU

------------------------------------------------------

1. Tester pripojen k PC a napajeni

2. Pripojit Bat1 k testeru. V Bat1 musi byt
   vypalen testovaci firmware Bat1test.hex,
   musi byt naprogramovat USB chip

3. Spustit BatCalibration.exe

4. Nastavit spravny COM pro tester a Bat1

5. Stisknout Open, na spodni radce programu
   se zobrazi Status : OK
   
6. Stisknout USB power on ( zkontrolovat Status : OK)

7. Stisknout ADC test (trva nekolik sekund),
   zkontrolovat Status : OK. Vedle tlacitka se zobrazi
   namerene hodnoty AD prevodniku. Tyto hodnoty se
   zadaji do programu Bat1tester.exe - v dialogu Calibration
   jako ADC Zero a ADC Max
   
8. Stisknout Int ADC test zkontrolovat Status : OK
   Vedle tlacitka se zobrazi
   namerene hodnoty interniho AD prevodniku. Tyto hodnoty se
   zadaji do programu Bat1tester.exe - v dialogu Calibration
   jako Internal ADC low a Internal ADC high
   
9. Stisknout Normal, zkontrolovat Status : OK
   Vedle tlacitka se zobrazi odber Bat1 s podsvitem
   Hodnota se zada do programu Bat1tester.exe - v dialogu Calibration
   jako Normal Current
   
10. Stisknout Dark, zkontrolovat Status : OK
   Vedle tlacitka se zobrazi odber Bat1 bez podsvitu
   Hodnota se zada do programu Bat1tester.exe - v dialogu Calibration
   jako Current w/o backlight
   