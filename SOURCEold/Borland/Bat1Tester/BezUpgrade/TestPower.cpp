//******************************************************************************
//
//   TestPower.cpp Bat1 power tests
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "TestPower.h"

#define USB_CONNECT_DELAY  2000        // USB connect/disconnect delay

// tester voltage range :
#define VOLTAGE_MIN        3200        // tester minimal voltage
#define VOLTAGE_MAX        4300        // tester maximal voltage

// power constants :
#define VOLTAGE_NOMINAL    3600        // nominal accu voltage
#define VOLTAGE_LOW        3400        // low voltage start

// timing :
#define VOLTAGE_DELAY         10       // delay after voltage change
#define INVERTED_POWER_DELAY  2000     // delay after inverted power on
#define IDLE_POWER_DELAY      3000     // delay after idle power on
#define NORMAL_POWER_DELAY    3000     // delay after normal power on
#define BACKLIGHT_DELAY       1000     // delay after backlight change
#define ON_KEY_DELAY          500      // ON key press for switch on
#define OFF_KEY_DELAY         3000     // ON key press for switch off
#define CHARGER_DELAY         1000     // delay after charger/USB switch
#define USB_POWER_ON_DELAY    2000     // delay after USB power on
#define ADC_DELAY             800      // delay after ADC change
#define IADC_DELAY            500      // delay after IADC change

// IADC data :
#define R1         15                  // upper resistor [kOhm]
#define R2         47                  // lower resitor  [kOhm]
#define VREF     3300                  // voltage reference [mV]
#define PACCU_ADC_RANGE    1024        // ADC range [LSB]

#define PACCU_RANGE ((VREF * (R1 + R2)) / R2) // calibration = input voltage range

//******************************************************************************
// Constructor
//******************************************************************************

TTestPower::TTestPower( TCrt *Crt, TMasterCom *Master)
// Constructor
{
   EnableHardware = false;
   Voltage  = 0;
   Current  = 0;
   SensitiveCurrent = false;

   FAdapter = new TTestAdapter;
   FCrt     = Crt;
   FMaster  = Master;

   // test defaults :
   CurrentInverted      = 0;     // 0.01 uA
   CurrentInvertedRange = 100;   // 0.01 uA

   CurrentIdle          = 250;   // 0.01 uA
   CurrentIdleRange     = 250;   // 0.01 uA

   CurrentNormal        = 100;   // mA
   CurrentNormalRange   = 100;   // mA

   CurrentDark          = 100;   // mA
   CurrentDarkRange     = 100;   // mA

   CurrentOff           = 2;     // mA

   AdcZero              = 0;     // LSB
   AdcZeroRange         = 3000;
   AdcMax               = 87000;
   AdcMaxRange          = 3000;

   IadcLow              = 3400;
   IadcLowRange         = 500;
   IadcHigh             = 4200;
   IadcHighRange        = 500;
} // TTestPower

//******************************************************************************
// Destructor
//******************************************************************************

TTestPower::~TTestPower()
// Destructor
{
   if( FAdapter){
      delete FAdapter;
   }
} // ~TTestPower


//******************************************************************************
// Open
//******************************************************************************

bool TTestPower::Open( char *ComName)
// Open tester COM
{
   if( !EnableHardware){
      return( false);
   }
   return( FAdapter->Open( ComName));
} // Open

//******************************************************************************
// Calibration
//******************************************************************************

#define CAL_TRIALS_COUNT  5
#define CAL_SUCCESS_COUNT 4

bool TTestPower::Calibration()
// Tester calibration
{
int MVoltage;

   if( !EnableHardware){
      return( false);
   }
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // switch power on :
   if( !FAdapter->SetPower( POWER_NORMAL)){
      FCrt->printf( "Tester : unable set power ON\n");
      return( false);
   }
   // run calibration trials :
   int Count = 0;
   for( int i = 0; i < CAL_TRIALS_COUNT; i++){
      // set minimum voltage :
      if( !FAdapter->VoltageMin()){
         FCrt->printf( "Tester : unable set minimum voltage\n");
         return( false);
      }
      Sleep( VOLTAGE_DELAY);
      if( !FAdapter->GetVoltage( MVoltage)){
         FCrt->printf( "Tester : unable read voltage\n");
         return( false);
      }
      this->Voltage = MVoltage;
      if( MVoltage > VOLTAGE_MIN){
         continue;
      }
      // set maximum voltage :
      if( !FAdapter->VoltageMax()){
         FCrt->printf( "Tester : unable set maximum voltage\n");
         return( false);
      }
      Sleep( VOLTAGE_DELAY);
      if( !FAdapter->GetVoltage( MVoltage)){
         FCrt->printf( "Tester : unable read voltage\n");
         return( false);
      }
      this->Voltage = MVoltage;
      if( MVoltage < VOLTAGE_MAX){
         continue;
      }
      Count++;
   }
   // return to nominal voltage :
   if( !SetVoltage( VOLTAGE_NOMINAL)){
      return( false);
   }
   if( Count < CAL_SUCCESS_COUNT){
      FCrt->printf( "Tester : unable calibrate voltage\n");
      return( false);
   }
   return( true);
} // Calibration

//******************************************************************************
// USB power on
//******************************************************************************

bool TTestPower::UsbPowerOn()
// Set defaults & USB power
{
   if( !EnableHardware){
      return( false);
   }
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // switch accumulator on :
   if( !PowerOn( true)){
      return( false);
   }
   // connect USB :
   if( !FAdapter->UsbConnect( true)){
      FCrt->printf( "Tester : Unable connect USB\n");
      return( false);
   }
   Sleep( USB_POWER_ON_DELAY);
   return( true);
} // UsbPowerOn

//******************************************************************************
// Keyboard power on
//******************************************************************************

bool TTestPower::KeyboardPowerOn()
// Press/release ON key
{
   if( !EnableHardware){
      return( false);
   }
   // press/release ON key :
   if( !FAdapter->KeyboardOn( true)){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( ON_KEY_DELAY);
   if( !FAdapter->KeyboardOn( false)){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   return( true);
} // KeyboardPowerOn

//******************************************************************************
// Keyboard power off
//******************************************************************************

bool TTestPower::KeyboardPowerOff()
// Press/hold/release ON key
{
   if( !EnableHardware){
      return( false);
   }
   // press/release ON key :
   if( !FAdapter->KeyboardOn( true)){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( OFF_KEY_DELAY);
   if( !FAdapter->KeyboardOn( false)){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   return( true);
} // KeyboardPowerOff

//******************************************************************************
// Shutdown
//******************************************************************************

void TTestPower::Shutdown()
// Tester shutdown
{
   if( !EnableHardware){
      return;
   }
   if( !FAdapter->SetDefaults()){
      return;
   }
} // Shutdown

//******************************************************************************
// USB connect
//******************************************************************************

bool TTestPower::UsbConnect( bool Connect)
// <Connect>/disconnect USB power & data
{
  if( !EnableHardware){
      if( Connect){
         Application->MessageBox( "Connect USB", "Information", MB_OK);
      } else {
         Application->MessageBox( "Disconnect USB", "Information", MB_OK);
      }
      return( true);
   }
   if( !FAdapter->UsbConnect( Connect)){
      return( false);
   }
   Sleep( USB_CONNECT_DELAY);
   return( true);
} // UsbConnect

//******************************************************************************
// ADC calibration
//******************************************************************************

#define TRIALS_COUNT  5
#define SUCCESS_COUNT 4

bool TTestPower::AdcCalibration()
// Test ADC calibration
{
   int Value;
   if( !EnableHardware){
      return( false);
   }
   //---------------------------------------------------------------------------
   // test zero :
   if( !FAdapter->Bridge( false)){
      FCrt->printf( "Tester : unable control bridge\n");
      return( false);
   }
   Sleep( ADC_DELAY);                  // wait for stable data
   // read trials :
   int Count = 0;
   for( int i = 0; i < TRIALS_COUNT; i++){
      Sleep( ADC_DELAY);
      if( !FMaster->ReadAdc( Value)){
         FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
         return( false);
      }
      if( InRange( Value, AdcZero, AdcZeroRange)){
         AdcZeroVoltage = Value;
         Count++;
      }
   }
   if( Count < SUCCESS_COUNT){
      FCrt->printf( "Error : ADC zero out of range\n");
      return( false);
   }
   //---------------------------------------------------------------------------
   // test max
   if( !FAdapter->Bridge( true)){
      FCrt->printf( "Tester : unable control bridge\n");
      return( false);
   }
   Sleep( ADC_DELAY);                  // wait for stable data
   // read trials :
   Count = 0;
   for( int i = 0; i < TRIALS_COUNT; i++){
      Sleep( ADC_DELAY);
      if( !FMaster->ReadAdc( Value)){
         FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
         return( false);
      }
      if( InRange( Value, AdcMax, AdcMaxRange)){
         AdcMaxVoltage = Value;
         Count++;
      }
   }
   if( Count < SUCCESS_COUNT){
      FCrt->printf( "Error : ADC maximum out of range\n");
      return( false);
   }
   return( true);
} // AdcCalibration

//******************************************************************************
// Internal ADC calibration
//******************************************************************************

bool TTestPower::IadcCalibration()
// Test IADC calibration
{
   int Value;
   if( !EnableHardware){
      return( false);
   }
   //---------------------------------------------------------------------------
   // test low :
   if( !SetVoltage( IadcLow)){
      return( false);
   }
   Sleep( IADC_DELAY);
   // read trials :
   int Count = 0;
   for( int i = 0; i < TRIALS_COUNT; i++){
      Sleep( IADC_DELAY);
      if( !FMaster->ReadIadc( Value)){
         FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
         return( false);
      }
      Value = (Value * PACCU_RANGE) / PACCU_ADC_RANGE;
      if( InRange( Value, IadcLow, IadcLowRange)){
         IadcLowVoltage = Value;
         Count++;
      }
   }
   if( Count < SUCCESS_COUNT){
      FCrt->printf( "Error : IADC low voltage out of range\n");
      return( false);
   }
   //---------------------------------------------------------------------------
   // test max
   if( !SetVoltage( IadcHigh)){
      return( false);
   }
   Sleep( IADC_DELAY);
   // read trials :
   Count = 0;
   for( int i = 0; i < TRIALS_COUNT; i++){
      Sleep( IADC_DELAY);
      if( !FMaster->ReadIadc( Value)){
         FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
         return( false);
      }
      Value = (Value * PACCU_RANGE) / PACCU_ADC_RANGE;
      if( InRange( Value, IadcHigh, IadcHighRange)){
         IadcHighVoltage = Value;
         Count++;
      }
   }
   if( Count < SUCCESS_COUNT){
      FCrt->printf( "Error : ADC zero out of range\n");
      return( false);
   }
   return( true);
} // IadcCalibration

//******************************************************************************
// Keyboard
//******************************************************************************

bool TTestPower::Keyboard()
// Test keyboard
{
   bool On, K0, K1, K2;
   if( !EnableHardware){
      return( false);
   }
   //---- release all keys : ---------------------------------------------------
   if( !FAdapter->KeyboardOn( false)){
      FCrt->printf( "Tester : unable release ON key\n");
      return( false);
   }
   if( !FAdapter->KeyboardK0( false)){
      FCrt->printf( "Tester : unable release K0 key\n");
      return( false);
   }
   if( !FAdapter->KeyboardK1( false)){
      FCrt->printf( "Tester : unable release K1 key\n");
      return( false);
   }
   if( !FAdapter->KeyboardK2( false)){
      FCrt->printf( "Tester : unable release K2 key\n");
      return( false);
   }
   // read status :
   if( !FMaster->TestKeyboard( On, K0, K1, K2)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( !On){
      FCrt->printf( "Error : frozen ON key\n");
      return( false);
   }
   if( !K0){
      FCrt->printf( "Error : frozen K0 key\n");
      return( false);
   }
   if( !K1){
      FCrt->printf( "Error : frozen K1 key\n");
      return( false);
   }
   if( !K2){
      FCrt->printf( "Error : frozen K2 key\n");
      return( false);
   }
   //---- press ON key : -------------------------------------------------------
   if( !FAdapter->KeyboardOn( true)){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   // read status :
   if( !FMaster->TestKeyboard( On, K0, K1, K2)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( On){
      FCrt->printf( "Error : frozen ON key\n");
      return( false);
   }
   if( !K0){
      FCrt->printf( "Error : frozen K0 key\n");
      return( false);
   }
   if( !K1){
      FCrt->printf( "Error : frozen K1 key\n");
      return( false);
   }
   if( !K2){
      FCrt->printf( "Error : frozen K2 key\n");
      return( false);
   }
   // release key :
   if( !FAdapter->KeyboardOn( false)){
      FCrt->printf( "Tester : unable release ON key\n");
      return( false);
   }
   //---- press K0 key : -------------------------------------------------------
   if( !FAdapter->KeyboardK0( true)){
      FCrt->printf( "Tester : unable press K0 key\n");
      return( false);
   }
   // read status :
   if( !FMaster->TestKeyboard( On, K0, K1, K2)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( !On){
      FCrt->printf( "Error : frozen ON key\n");
      return( false);
   }
   if( K0){
      FCrt->printf( "Error : frozen K0 key\n");
      return( false);
   }
   if( !K1){
      FCrt->printf( "Error : frozen K1 key\n");
      return( false);
   }
   if( !K2){
      FCrt->printf( "Error : frozen K2 key\n");
      return( false);
   }
   // release key :
   if( !FAdapter->KeyboardK0( false)){
      FCrt->printf( "Tester : unable release K0 key\n");
      return( false);
   }
   //---- press K1 key : -------------------------------------------------------
   if( !FAdapter->KeyboardK1( true)){
      FCrt->printf( "Tester : unable press K1 key\n");
      return( false);
   }
   // read status :
   if( !FMaster->TestKeyboard( On, K0, K1, K2)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( !On){
      FCrt->printf( "Error : frozen ON key\n");
      return( false);
   }
   if( !K0){
      FCrt->printf( "Error : frozen K0 key\n");
      return( false);
   }
   if( K1){
      FCrt->printf( "Error : frozen K1 key\n");
      return( false);
   }
   if( !K2){
      FCrt->printf( "Error : frozen K2 key\n");
      return( false);
   }
   // release key :
   if( !FAdapter->KeyboardK1( false)){
      FCrt->printf( "Tester : unable release K1 key\n");
      return( false);
   }
   //---- press K2 key : -------------------------------------------------------
   if( !FAdapter->KeyboardK2( true)){
      FCrt->printf( "Tester : unable press K2 key\n");
      return( false);
   }
   // read status :
   if( !FMaster->TestKeyboard( On, K0, K1, K2)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( !On){
      FCrt->printf( "Error : frozen ON key\n");
      return( false);
   }
   if( !K0){
      FCrt->printf( "Error : frozen K0 key\n");
      return( false);
   }
   if( !K1){
      FCrt->printf( "Error : frozen K1 key\n");
      return( false);
   }
   if( K2){
      FCrt->printf( "Error : frozen K2 key\n");
      return( false);
   }
   // release key :
   if( !FAdapter->KeyboardK2( false)){
      FCrt->printf( "Tester : unable release K2 key\n");
      return( false);
   }
   return( true);
} // Keyboard

//******************************************************************************
// Power tests
//******************************************************************************

bool TTestPower::Power()
// Run all power tests
{
   if( !EnableHardware){
      return( false);
   }
   if( !InvertedPower()){
      return( false);
   }
   if( !IdleCurrent()){
      return( false);
   }
   if( !NormalCurrent()){
      return( false);
   }
   if( !DarkCurrent()){
      return( false);
   }
   if( !LowVoltage()){
      return( false);
   }
   if( !Charger()){
      return( false);
   }
   if( !UsbCharger()){
      return( false);
   }
   if( !ChargerDone()){
      return( false);
   }
   if( !UsbChargerDone()){
      return( false);
   }
   return( true);
} // Power

//******************************************************************************
// Inverted power
//******************************************************************************

bool TTestPower::InvertedPower()
// Inverted power test
{
   FCrt->printf( "POWER inversion...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // sensitive current :
   if( !FAdapter->SetCurrent( true)){
      FCrt->printf( "Tester : unable set sensitive current\n");
      return( false);
   }
   // inverted power :
   if( !FAdapter->SetPower( POWER_INVERTED)){
      FCrt->printf( "Tester : unable set inverted power\n");
      return( false);
   }
   Sleep( INVERTED_POWER_DELAY);
   // get voltage :
   int MVoltage;
   if( !FAdapter->GetVoltageInverted( MVoltage)){
      FCrt->printf( "Tester : unable read voltage\n");
      return( false);
   }
   Voltage = MVoltage;
   // get current :
   int MCurrent;
   if( !FAdapter->GetCurrentInverted( MCurrent)){
      FCrt->printf( "Tester : unable read current\n");
      return( false);
   }
   SensitiveCurrent = true;
   Current          = MCurrent;
   // test range :
   if( !InRange( -MCurrent, CurrentInverted, CurrentInvertedRange)){
      FCrt->printf( "Error : inverted current out of range\n");
      return( false);
   }
   return( true);
} // InvertedPower

//******************************************************************************
// Idle current
//******************************************************************************

bool TTestPower::IdleCurrent()
// Check power off current
{
   FCrt->printf( "POWER idle...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // sensitive current :
   if( !FAdapter->SetCurrent( true)){
      FCrt->printf( "Tester : unable set sensitive current\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   Sleep( IDLE_POWER_DELAY);
   // get current :
   int MCurrent;
   if( !FAdapter->GetCurrent( MCurrent)){
      FCrt->printf( "Tester : unable read current\n");
      return( false);
   }
   SensitiveCurrent = true;
   Current          = MCurrent;
   // test range :
   if( !InRange( MCurrent, CurrentIdle, CurrentIdleRange)){
      FCrt->printf( "Error : inverted current out of range\n");
      return( false);
   }
   return( true);
} // IdleCurrent

//******************************************************************************
// Normal current
//******************************************************************************

bool TTestPower::NormalCurrent()
// Check current with backlight
{
   FCrt->printf( "POWER normal...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // get current :
   int MCurrent;
   if( !FAdapter->GetCurrent( MCurrent)){
      FCrt->printf( "Tester : unable read current\n");
      return( false);
   }
   SensitiveCurrent = false;
   Current          = MCurrent;
   // test range :
   if( !InRange( MCurrent, CurrentNormal, CurrentNormalRange)){
      FCrt->printf( "Error : normal current out of range\n");
      return( false);
   }
   return( true);
} // NormalCurrent

//******************************************************************************
// Dark current
//******************************************************************************

bool TTestPower::DarkCurrent()
// Check current without backlight
{
   FCrt->printf( "POWER without backlight...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // switch backlight off :
   if( !FMaster->SetBacklight( false)){
      FCrt->printf( "COM : unable switch backlight off\n");
      return( false);
   }
   Sleep( BACKLIGHT_DELAY);
   // get current :
   int MCurrent;
   if( !FAdapter->GetCurrent( MCurrent)){
      FCrt->printf( "Tester : unable read current\n");
      return( false);
   }
   SensitiveCurrent = false;
   Current          = MCurrent;
   // test range :
   if( !InRange( MCurrent, CurrentDark, CurrentDarkRange)){
      FCrt->printf( "Error : dark current out of range\n");
      return( false);
   }
   return( true);
} // DarkCurrent

//******************************************************************************
// Low voltage
//******************************************************************************

bool TTestPower::LowVoltage()
// Check power off on low voltage
{
   FCrt->printf( "POWER low voltage...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   if( !SetVoltage( VOLTAGE_LOW)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // lower voltage up to power off :
   int MCurrent;
   int MVoltage;
   SensitiveCurrent = false;
   for( int i = 0; i < VOLTAGE_STEPS; i++){
      if( !FAdapter->GetCurrent( MCurrent)){
         FCrt->printf( "Tester : unable read current\n");
         return( false);
      }
      Current = MCurrent;
      if( MCurrent < CurrentOff){
         return( true);
      }
      if( !FAdapter->VoltageDown()){
         FCrt->printf( "Tester : unable set voltage\n");
         return( false);
      }
      Sleep( VOLTAGE_DELAY);
      if( !FAdapter->GetVoltage( MVoltage)){
         FCrt->printf( "Tester : unable read voltage\n");
         return( false);
      }
      Voltage = MVoltage;
   }
   FCrt->printf( "Error : power low switch\n");
   return( false);
} // LowVoltage

//******************************************************************************
// Charger
//******************************************************************************

bool TTestPower::Charger()
// Test charger connection
{
   FCrt->printf( "CHARGER on/off...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // read power status :
   bool PPR, CHG, USBON, ENNAB;
   if( !FMaster->TestPower( PPR, CHG, USBON, ENNAB)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( !PPR){
      FCrt->printf( "Error : PPR status without charger is LOW\n");
      return( false);
   }
   if( USBON){
      FCrt->printf( "Error : USBON status without charger is HIGH\n");
      return( false);
   }
   // switch charger on :
   if( !FAdapter->ChargerConnect( true)){
      FCrt->printf( "Tester : unable switch charger ON\n");
      return( false);
   }
   Sleep( CHARGER_DELAY);
   if( !FMaster->TestPower( PPR, CHG, USBON, ENNAB)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( PPR){
      FCrt->printf( "Error : PPR status with charger is HIGH\n");
      return( false);
   }
   if( USBON){
      FCrt->printf( "Error : USBON status without USB is HIGH\n");
      return( false);
   }
   return( true);
} // Charger

//******************************************************************************
// USB charger
//******************************************************************************

bool TTestPower::UsbCharger()
// Test USB charger connection
{
   FCrt->printf( "USB charger on/off...\n");
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // read power status :
   bool PPR, CHG, USBON, ENNAB;
   if( !FMaster->TestPower( PPR, CHG, USBON, ENNAB)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( !PPR){
      FCrt->printf( "Error : PPR status without USB is LOW\n");
      return( false);
   }
   if( USBON){
      FCrt->printf( "Error : USBON status without USB is HIGH\n");
      return( false);
   }
   // switch charger on :
   if( !FAdapter->UsbConnect( true)){
      FCrt->printf( "Tester : unable switch USB ON\n");
      return( false);
   }
   Sleep( CHARGER_DELAY);
   if( !FMaster->TestPower( PPR, CHG, USBON, ENNAB)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( PPR){
      FCrt->printf( "Error : PPR status with USB is HIGH\n");
      return( false);
   }
   if( !USBON){
      FCrt->printf( "Error : USBON status with USB is LOW\n");
      return( false);
   }
   return( true);
} // UsbCharger

//******************************************************************************
// Charger done
//******************************************************************************

bool TTestPower::ChargerDone()
// Test charger done condition
{
   FCrt->printf( "CHARGER stop...\n");
   return( ChargerStop( false));
} // ChargerDone

//******************************************************************************
// USB charger done
//******************************************************************************

bool TTestPower::UsbChargerDone()
// Test charging from USB done
{
   FCrt->printf( "USB charger stop...\n");
   return( ChargerStop( true));
} // UsbChargerDone

//------------------------------------------------------------------------------

//******************************************************************************
// Power on
//******************************************************************************

bool TTestPower::PowerOn( bool On)
// Switch power <On>
{
   if( !EnableHardware){
      return( true);
   }
   if( On){
      // set minimum voltage :
      if( !FAdapter->VoltageMin()){
         FCrt->printf( "Tester : unable set minimum voltage\n");
         return( false);
      }
      if( !FAdapter->SetPower( POWER_NORMAL)){
         FCrt->printf( "Tester : unable set power ON\n");
         return( false);
      }
      // set nominal voltage
      if( !SetVoltage( VOLTAGE_NOMINAL)){
         return( false);
      }
      return( true);
   } else {
      // switch power off :
      if( !FAdapter->SetPower( POWER_OFF)){
         FCrt->printf( "Tester : unable set power OFF\n");
         return( false);
      }
      // set defaults :
      if( !FAdapter->SetDefaults()){
         FCrt->printf( "Tester : set defaults failure\n");
         return( false);
      }
      return( true);
   }
} // PowerOn

//******************************************************************************
// Set voltage
//******************************************************************************

bool TTestPower::SetVoltage( int Voltage)
// Set power <Voltage> [mV]
{
int MVoltage;

   if( !FAdapter->VoltageMin()){
      FCrt->printf( "Tester : unable set minimum voltage\n");
      return( false);
   }
   Sleep( VOLTAGE_DELAY);
   for( int i = 0; i < VOLTAGE_STEPS; i++){
      if( !FAdapter->GetVoltage( MVoltage)){
         FCrt->printf( "Tester : unable read voltage\n");
         return( false);
      }
      this->Voltage = MVoltage;
      if( MVoltage >= Voltage){
         return( true);
      }
      if( !FAdapter->VoltageUp()){
         FCrt->printf( "Tester : unable set voltage\n");
         return( false);
      }
      Sleep( VOLTAGE_DELAY);
   }
   FCrt->printf( "Tester : unable set voltage %d mV\n", Voltage);
   return( false);
} // SetVoltage

//******************************************************************************
// Charger stop
//******************************************************************************

bool TTestPower::ChargerStop( bool Usb)
// Test charger/<Usb> charger stop
{
   // set defaults :
   if( !FAdapter->SetDefaults()){
      FCrt->printf( "Tester : set defaults failure\n");
      return( false);
   }
   // normal power :
   if( !PowerOn( true)){
      return( false);
   }
   // press/release ON key :
   if( !KeyboardPowerOn()){
      FCrt->printf( "Tester : unable press ON key\n");
      return( false);
   }
   Sleep( NORMAL_POWER_DELAY);
   // read power status with charger off :
   bool PPR, CHG, USBON, ENNAB;
   if( !FMaster->TestPower( PPR, CHG, USBON, ENNAB)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( !PPR){
      FCrt->printf( "Error : PPR status without charger is LOW\n");
      return( false);
   }
   if( USBON){
      FCrt->printf( "Error : USBON status without USB is HIGH\n");
      return( false);
   }
   // switch USB/charger on :
   if( Usb){
      if( !FAdapter->UsbConnect( true)){
         FCrt->printf( "Tester : unable switch USB ON\n");
         return( false);
      }
   } else {
      if( !FAdapter->ChargerConnect( true)){
         FCrt->printf( "Tester : unable switch charger ON\n");
         return( false);
      }
   }
   Sleep( CHARGER_DELAY);
   // read power status after charger on :
   if( !FMaster->TestPower( PPR, CHG, USBON, ENNAB)){
      FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
      return( false);
   }
   if( Usb){
      if( PPR){
         FCrt->printf( "Error : PPR status with USB is HIGH\n");
         return( false);
      }
      if( !USBON){
         FCrt->printf( "Error : USBON status with USB is LOW\n");
         return( false);
      }
   } else {
      if( PPR){
         FCrt->printf( "Error : PPR status with charger is HIGH\n");
         return( false);
      }
      if( USBON){
         FCrt->printf( "Error : USBON status without USB is HIGH\n");
         return( false);
      }
   }
   if( CHG){
      FCrt->printf( "Error : CHG status on low voltage is HIGH\n");
      return( false);
   }
   // higher voltage up to max :
   int MVoltage;
   for( int i = 0; i < VOLTAGE_STEPS; i++){
      if( !FAdapter->GetVoltage( MVoltage)){
         FCrt->printf( "Tester : unable read voltage\n");
         return( false);
      }
      Voltage = MVoltage;
      if( !FMaster->TestPower( PPR, CHG, USBON, ENNAB)){
         FCrt->printf( "COM : %s\n", FMaster->Status.c_str());
         return( false);
      }
      if( CHG){
         return( true);
      }
      if( !FAdapter->VoltageUp()){
         FCrt->printf( "Tester : unable set voltage\n");
         return( false);
      }
      Sleep( VOLTAGE_DELAY);
   }
   FCrt->printf( "Error : CHG signal frozen LOW\n");
   return( false);
} // ChargerStop

//******************************************************************************
// In range
//******************************************************************************

bool TTestPower::InRange( int Value, int Nominal, int Delta)
// Check if the <Value> is <Nominal> +- <Delta>
{
   if( Value < Nominal - Delta){
      return( false);
   }
   if( Value > Nominal + Delta){
      return( false);
   }
   return( true);
} // InRange

