//******************************************************************************
//
//   MainUpgrade.h  Bat1 Upgrade main
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#ifndef MainUpgradeH
#define MainUpgradeH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "UsbTester.h"
#include "Bat1Usb.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar;
        TButton *BtnUpgrade;
   TImage *Image;
   TLabel *Label1;
   TLabel *LblUsbName;
   TLabel *Label2;
   TLabel *LblScaleName;
   TLabel *Label3;
   TLabel *LblVersion;
   TLabel *Label4;
   TLabel *LblUsbManufacturer;
        void __fastcall BtnUpgradeClick(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TUsbTester    *Usb;
   TBat1Usb      *Bat1;

   __fastcall TMainForm(TComponent* Owner);
   void StartComm();
   void StopComm();

   // setup data :
   AnsiString LogoName;
   AnsiString UsbName;
   AnsiString UsbManufacturer;
   AnsiString ScaleName;
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
