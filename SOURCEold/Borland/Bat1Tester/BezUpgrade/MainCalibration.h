//******************************************************************************
//
//   MainPower.h   Bat1 power tester
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainCalibrationH
#define MainCalibrationH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "TestCalibration.h"
#include <ComCtrls.hpp>

//******************************************************************************
// TMainForm
//******************************************************************************

class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TComboBox *CbPrinterPort;
        TLabel *Label1;
        TLabel *Label2;
        TComboBox *CbTesterPort;
        TButton *BtnOpen;
        TStatusBar *StatusBar;
        TButton *BtnAdcTest;
        TButton *BtnIadcTest;
   TLabel *LblVoltage;
   TLabel *LblCurrent;
   TButton *BtnNormal;
   TButton *BtnDark;
   TButton *BtnUsbPowerOn;
   TLabel *LblAdcZeroVoltage;
   TLabel *LblAdcMaxVoltage;
   TLabel *LblIadcLowVoltage;
   TLabel *LblIadcHighVoltage;
   void __fastcall FormResize(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall BtnOpenClick(TObject *Sender);
   void __fastcall BtnUsbPowerOnClick(TObject *Sender);
   void __fastcall BtnAdcTestClick(TObject *Sender);
   void __fastcall BtnIadcTestClick(TObject *Sender);
   void __fastcall BtnNormalClick(TObject *Sender);
   void __fastcall BtnDarkClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   void UpdateVoltage();
   void UpdateCurrent();
   void UpdateAdcVoltage();
   void UpdateIadcVoltage();

   TCrt             *Crt;
   TTestCalibration *Adapter;
   TMasterCom       *Com;
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
