//******************************************************************************
//
//   MainUpgrade.cpp  Bat1 Upgrade main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainUpgrade.h"
#include <IniFiles.hpp>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;


// Status line :
#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

// Setup Read macros :
#define SETUP_VERSION 0x100

#define StpReadString( Section, Parameter) \
   Parameter = SetupFile->ReadString( #Section, #Parameter, "")

#define StpReadInteger( Section, Parameter) \
   Parameter = SetupFile->ReadInteger( #Section, #Parameter, 0)

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Usb     = new TUsbTester;
   Bat1Usb = new TBat1Usb;
   UsbName         = "?";
   UsbManufacturer = "?";
   ScaleName       = "?";
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   // load setup :
   if( _argc <= 1){
      Status() + "Use 'Bat1Upgrade <INI_FILE.INI>'";
      return;
   }
   AnsiString FileName = _argv[ 1];
   TIniFile *SetupFile;
   AnsiString SetupFileName = ExtractFilePath( Application->ExeName) + FileName;
   SetupFile = new TIniFile( SetupFileName);
   // check for setup version / empty setup :
   int Version;
   StpReadInteger( Properties, Version);
   if( Version != SETUP_VERSION){
      Status() + "Unable read UPGRADE.INI";
      return;
   }
   // read setup data :
   StpReadString( Properties, LogoName);
   StpReadString( Properties, UsbName);
   StpReadString( Properties, UsbManufacturer);
   StpReadString( Properties, ScaleName);
   delete SetupFile;
   // load logo :
   try {
      Image->Picture->LoadFromFile( ExtractFilePath( Application->ExeName) + LogoName);
   } catch(...){
      Status() + "Unable read '" + LogoName + "'";
      return;
   }
   // show setup :
   LblUsbName->Caption         = UsbName;
   LblUsbManufacturer->Caption = UsbManufacturer;
   LblScaleName->Caption       = ScaleName;
   Status() + "Setup OK";
} // FormCreate

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   this->Enabled  = false;             // disable main window
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // StartCom

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   this->Enabled  = true;              // enable main window
   Screen->Cursor = crDefault;         // normal cursor
} // StopComm

//******************************************************************************
//  Upgrade
//******************************************************************************

void __fastcall TMainForm::BtnUpgradeClick(TObject *Sender)
{
   StartComm();
   Status() + "?";
   StatusBar->Refresh();
   // programming device :
   if( !Usb->WaitForNewDevice()){
      Status() + "ERROR "+ Usb->Status;
      StopComm();
      return;
   }
   Status() + "Device " + Usb->AdapterName;
   StatusBar->Refresh();
   if( !Usb->WriteEeprom( UsbName.c_str(), UsbManufacturer.c_str())){
      Status() + "ERROR " + Usb->Status;
      StopComm();
      return;
   }
   StopComm();
   Status() + "Upgrade OK";
} // BtnUpgradeClick

