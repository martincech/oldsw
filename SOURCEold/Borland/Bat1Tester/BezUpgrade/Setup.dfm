object SetupForm: TSetupForm
  Left = 600
  Top = 301
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Tester Setup'
  ClientHeight = 319
  ClientWidth = 345
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 24
    Width = 57
    Height = 13
    Caption = 'Printer port :'
  end
  object Label2: TLabel
    Left = 32
    Top = 56
    Width = 57
    Height = 13
    Caption = 'Tester port :'
  end
  object CbPrinterPort: TComboBox
    Left = 136
    Top = 20
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object CbEnableTester: TCheckBox
    Left = 32
    Top = 96
    Width = 137
    Height = 17
    Caption = 'Enable tester hardware'
    TabOrder = 1
  end
  object CbFtdiProgramming: TCheckBox
    Left = 32
    Top = 128
    Width = 193
    Height = 17
    Caption = 'Enable FTDI Programming'
    TabOrder = 2
  end
  object CbHardwareTesting: TCheckBox
    Left = 32
    Top = 224
    Width = 153
    Height = 17
    Caption = 'Enable Hardware Testing'
    TabOrder = 3
  end
  object CbBatProgramming: TCheckBox
    Left = 32
    Top = 256
    Width = 161
    Height = 17
    Caption = 'Enable Bat1 Programming'
    TabOrder = 4
  end
  object BtnSetupOk: TButton
    Left = 32
    Top = 287
    Width = 75
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 5
  end
  object BtnSetupCancel: TButton
    Left = 224
    Top = 287
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 6
  end
  object CbTesterPort: TComboBox
    Left = 136
    Top = 52
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10'
      'COM11'
      'COM12'
      'COM13'
      'COM14'
      'COM15')
  end
  object CbTestFirmwareProgramming: TCheckBox
    Left = 32
    Top = 192
    Width = 193
    Height = 17
    Caption = 'Enable test firmware programming'
    TabOrder = 8
  end
  object CbWaitForUsb: TCheckBox
    Left = 32
    Top = 160
    Width = 193
    Height = 17
    Caption = 'Wait for USB driver installation'
    TabOrder = 9
  end
end
