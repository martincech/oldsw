//******************************************************************************
//
//   MainCharger.h    Bat1/NT charger test
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainChargerH
#define MainChargerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "MasterCom.h"
#include <ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TLabel *Label1;
   TEdit *EdtPrinterPort;
   TLabel *Label2;
   TCheckBox *CbEnnab;
   TLabel *Label3;
   TLabel *LblUsbon;
   TLabel *Label4;
   TLabel *LblChg;
   TButton *BtnRun;
   TButton *BtnStop;
   TTimer *Timer;
   TLabel *Label5;
   TLabel *LblPpr;
   TLabel *Label6;
   TLabel *LblStatus;
   TLabel *Label7;
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall BtnRunClick(TObject *Sender);
   void __fastcall BtnStopClick(TObject *Sender);
   void __fastcall TimerTimer(TObject *Sender);
   void __fastcall CbEnnabClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TMasterCom    *Com;
   bool          Running;

   __fastcall TMainForm(TComponent* Owner);
   TYesNo PrinterPortOpen( void);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
