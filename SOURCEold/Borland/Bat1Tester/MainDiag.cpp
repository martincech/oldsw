//******************************************************************************
//
//   Main.cpp     Bat1 Tester main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>
#include <mmsystem.h>

#include "MainDiag.h"
#include "SetupDiag.h"
#include "Calibration.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//---------------------------------------------------------------------------
#ifdef __BAT1_NT__
   #define USB_BUS_POWERED true
#else
   #define USB_BUS_POWERED false
#endif

#define USB_VEIT_NAME          "VEIT BAT1 Poultry Scale"
#define USB_VEIT_MANUFACTURER  "VEIT"
#define DEFAULT_FILE           "FILE000"

#define Status()      StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()    Status() + "OK"
#define StatusError() Status() + "ERROR"

#define MkDataFile( f)      AnsiString( SetupForm->DataPath) + AnsiString( '\\') + AnsiString( f)

// sound types :
typedef enum {
   SOUND_OK,
   SOUND_FAILURE,
} TSoundType;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Crt = new TCrt( MainMemo);
   // tester :
   Usb     = new TUsbTester;
   Com     = new TMasterCom;
   Adapter = new TTestPower( Crt, Com);
   // programmer :
   Hex     = new THexFile( 512 * 1024);
   Lpc     = new TLpcFlash;
   Lpc->BaudRate               = 57600;
   Lpc->CrystalFrequency       = 18432000;
   strcpy( Lpc->DeviceName, USB_VEIT_NAME);
   Lpc->ViaUsb                 = true;
   Lpc->ProgressFunction       = this->Progress;
   Lpc->EnableFlashComparation = true;
   // bat interface :
   Bat1Usb  = new TBat1Usb;
   // setup form :
   SetupForm = new TSetupForm( MainForm);
   // calibration form :
   CalibrationForm = new TCalibrationForm( MainForm);
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   // load setup data :
   SetupForm->Load();
   CalibrationForm->Load();
   // load & prepare logo :
   AnsiString Logo = MkDataFile( "Logo.bmp");
   try {
      Image->Picture->LoadFromFile( Logo);
   } catch( ...){
      Application->MessageBox( "Unable read LOGO.BMP", "Error", MB_OK);
   }
   Bat1Usb->PrepareLogo( Image);       // recalculate logo
   Bat1Usb->DefaultConfig();           // prepare factory defaults
   // show version number :
   LblVersion->Caption = Bat1Usb->GetVersion();
#ifdef __BAT1_NT__
   Caption = "Bat1 NT Diagnostics";
   BtnCharger->Visible = NO;
   BtnChargerDone->Visible = NO;
#endif
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Close
//******************************************************************************

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
   SetupForm->Save();                  // save setup data
   CalibrationForm->Save();            // save calibration data
} // FormClose

//******************************************************************************
// Progress
//******************************************************************************

void TMainForm::Progress( int Percent)
// Update progress bar
{
   ProgressBar->Position = Percent / 10;
   AnsiString String;
   String.printf( "%d %%", Percent / 10);
   LblPercent->Caption = String;
   Screen->Cursor = crDefault;         // restore cursor
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // Progress

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   this->Enabled  = false;             // disable main window
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // StartCom

//******************************************************************************
// Communication flow
//******************************************************************************

void TMainForm::ContinueComm()
// Communication flow
{
     Screen->Cursor = crDefault;       // restore cursor
     Application->ProcessMessages();
     Screen->Cursor = crHourGlass;
} // ContinueComm

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   this->Enabled  = true;              // enable main window
   Screen->Cursor = crDefault;         // normal cursor
} // StopComm

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::BtnSetupClick(TObject *Sender)
{
   SetupForm->Execute();
} // BtnSetupClick

//******************************************************************************
// Calibration
//******************************************************************************

void __fastcall TMainForm::BtnCalibrationClick(TObject *Sender)
{
   CalibrationForm->Execute();
} // BtnCalibrationClick

//******************************************************************************
// Tester defaults
//******************************************************************************

bool TMainForm::TesterDefaults()
{
   Crt->printf( "Initialize tester...\n");
   // initialize printer port :
   if( !Com->Open( SetupForm->PrinterPort)){
      Crt->printf( "Error : Unable open printer port %s\n", SetupForm->PrinterPort);
      return( false);
   }
   // copy configuration & calibration
   Adapter->EnableHardware     = true;
   Adapter->CurrentNormal      = CalibrationForm->CurrentNormal;
   Adapter->CurrentNormalRange = CalibrationForm->CurrentNormalRange;
   Adapter->CurrentDark        = CalibrationForm->CurrentDark;
   Adapter->CurrentDarkRange   = CalibrationForm->CurrentDarkRange;
   Adapter->AdcZero            = CalibrationForm->AdcZero;
   Adapter->AdcZeroRange       = CalibrationForm->AdcZeroRange;
   Adapter->AdcMax             = CalibrationForm->AdcMax;
   Adapter->AdcMaxRange        = CalibrationForm->AdcMaxRange;
   Adapter->IadcLow            = CalibrationForm->IadcLow;
   Adapter->IadcLowRange       = CalibrationForm->IadcLowRange;
   Adapter->IadcHigh           = CalibrationForm->IadcHigh;
   Adapter->IadcHighRange      = CalibrationForm->IadcHighRange;
   // setup tester hardware :
   // open tester port :
   if( !Adapter->Open( SetupForm->TesterPort)){
      Crt->printf( "Error : Unable open tester port %s\n", SetupForm->TesterPort);
      return( false);
   }
   // calibrate tester :
   if( !Adapter->Calibration()){
      return( false);
   }
   // switch USB power on :
   if( !Adapter->UsbPowerOn()){
      return( false);
   }
   Crt->printf( "Tester O.K.\n");
   return( true);
} // TesterDefaults

//******************************************************************************
//  FTDI programming
//******************************************************************************

bool TMainForm::FtdiProgramming()
{
   //---------------------------------------------------------------------------
   Crt->printf( "FTDI programming...\n");
   // USB power on :
   if( !Adapter->UsbConnect( true)){
      Crt->printf( "Tester : Adapter failure\n");
      return( false);
   }
   if( SetupForm->WaitForUsb){
      Application->MessageBox( "Wait for USB driver installation", "Information", MB_OK);
   }
   // programming device :
   if( !Usb->WaitForNewDevice()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   Crt->printf( "Device : %s\n", Usb->AdapterName.c_str());
   if( !Usb->WriteEeprom( USB_VEIT_NAME, USB_VEIT_MANUFACTURER, USB_BUS_POWERED)){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   Crt->printf( "FTDI driver installation...\n");
   // USB power off :
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Tester : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDisconnect()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   //---------------------------------------------------------------------------
   if( !Adapter->UsbConnect( true)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   if( !Usb->WaitForDevice()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      return( false);
   }
   Crt->printf( "Device : %s\n", Usb->AdapterName.c_str());
   Crt->printf( "FTDI programming O.K.\n");
   return( true);
} // FtdiProgramming

//******************************************************************************
// Write test firmware
//******************************************************************************

bool TMainForm::WriteTestFirmware()
{
#ifndef __BAT1_NT__
   if( !WriteFirmware( MkDataFile( "Bat1Test.hex"))){
      return( false);
   }
#else
   if( !WriteFirmware( MkDataFile( "Bat1NTTest.hex"))){
      return( false);
   }
#endif
   return( true);
} // WriteTestFirmware

//******************************************************************************
// Master COM test
//******************************************************************************

bool TMainForm::MasterComTest()
{
   Crt->printf( "COM test...\n");
   if( !Com->TestUart()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      return( false);
   }
   Crt->printf( "COM test O.K.\n");
   return( true);
} // MasterComTest

//******************************************************************************
// ADC calibration test
//******************************************************************************

bool TMainForm::AdcCalibrationTest()
{
   Crt->printf( "ADC test...\n");
   if( !Adapter->AdcCalibration()){
      return( false);
   }
   Crt->printf( "ADC O.K.\n");
   return( true);
} // AdcCalibrationTest

//******************************************************************************
// Internal ADC calibration test
//******************************************************************************

bool TMainForm::IadcCalibrationTest()
{
   Crt->printf( "IADC test...\n");
   if( !Adapter->IadcCalibration()){
      return( false);
   }
   Crt->printf( "IADC O.K.\n");
   return( true);
} // IadcCalibrationTest

//******************************************************************************
// Keyboard test
//******************************************************************************

bool TMainForm::KeyboardTest()
{
   Crt->printf( "KBD test...\n");
   if( !Adapter->Keyboard()){
      return( false);
   }
   Crt->printf( "KBD O.K.\n");
   return( true);
} // IadcKeyboardTest

//******************************************************************************
// Write BAT firmware
//******************************************************************************

bool TMainForm::WriteBatFirmware()
{
#ifndef __BAT1_NT__
   return( WriteFirmware( MkDataFile( "Bat1.hex")));
#else
   return( WriteFirmware( MkDataFile( "Bat1NT.hex")));
#endif
} // WriteBatFirmware

//******************************************************************************
// Test done
//******************************************************************************

bool TMainForm::BatTest()
{
   Crt->printf( "BAT1 test...\n");
   // find device :
   if( !Bat1Usb->IsConnected()){
      Crt->printf( "Error : Unable connect Bat1\n");
      return( false);
   }
   // clock
   Crt->printf( "BAT1 clock...\n");
   if( !Bat1Usb->SetTime()){
      Crt->printf( "Error : Unable set clock\n");
      return( false);
   }
   // format :
   Crt->printf( "BAT1 format...\n");
   if( !Bat1Usb->FormatFilesystem()){
      Crt->printf( "Error : Unable format filesystem\n");
      return( false);
   }
   // create file :
   Crt->printf( "BAT1 create file...\n");
   if( !Bat1Usb->CreateFile( DEFAULT_FILE)){
      Crt->printf( "Error : Unable create file\n");
      return( false);
   }
   // save configuration :
   Crt->printf( "BAT1 save configuration...\n");
   if( !Bat1Usb->WriteConfig()){
      Crt->printf( "Error : Unable save configuration\n");
      return( false);
   }
   // write logo :
   Crt->printf( "BAT1 write logo...\n");
   if( !Bat1Usb->WriteLogo()){
      Crt->printf( "Error : Unable write logo\n");
      return( false);
   }
   Crt->printf( "BAT1 power test...\n");
   // switch power on :
   if( !Adapter->KeyboardPowerOn()){
      return( false);
   }
   Sleep( 5000);                       // wait for scale ready
   // switch power off :
   if( !Adapter->KeyboardPowerOff()){
      return( false);
   }
   // disconnect USB power :
   if( !Adapter->UsbConnect( false)){
      Crt->printf( "Error : Adapter failure\n");
      return( false);
   }
   return( true);
} // BatTest

//******************************************************************************
// Write Firmware
//******************************************************************************

bool TMainForm::WriteFirmware( AnsiString FileName)
{
   Crt->printf( "FIRMWARE Write : %s\n", FileName.c_str());
   if( !Hex->Load( FileName.c_str())){
      Crt->printf( "ERROR : open file '%s'\n", FileName.c_str());
      return( false);
   }
   // show progress :
   ProgressBar->Visible = true;
   LblPercent->Visible  = true;
   LblPhase->Visible    = true;
   Progress( 0);
   // programming :
   LblPhase->Caption = "Check :   ";
   LblPhase->Refresh();
   if( !Lpc->GetDeviceInfo()){
      Crt->printf( "ERROR : Unable get device info\n");
      return( false);
   }
   LblPhase->Caption = "Write :   ";
   LblPhase->Refresh();
   if( !Lpc->WriteFlash( 0, Hex->Code, Hex->CodeSize)){
      Crt->printf( "ERROR : Unable write code\n");
      return( false);
   }
   LblPhase->Caption = "Verify :  ";
   LblPhase->Refresh();
   if( !Lpc->CompareFlash( 0, Hex->Code, Hex->CodeSize)){
      Crt->printf( "ERROR : Verification failed\n");
      return( false);
   }
   LblPhase->Caption = "Reset :   ";
   LblPhase->Refresh();
   if( !Lpc->Reset()){
      Crt->printf( "ERROR : reset failed\n");
      return( false);
   }
   Lpc->Close();                       // release port
   // hide progress :
   Progress( 0);
   ProgressBar->Visible  = false;
   LblPercent->Visible   = false;
   LblPhase->Visible     = false;
   LblPhase->Caption     = "?";
   // wait for reset :
   Sleep( 2000);
   Com->Flush();                    // clear garbage on master com
   Crt->printf( "FIRMWARE O.K.\n");
   return( true);
} // WriteFirmware

//******************************************************************************
// Play
//******************************************************************************

void TMainForm::Play( int SoundType)
{
   AnsiString Path;
   AnsiString FileName;
   UINT       Beep;
   Path = ExtractFilePath( Application->ExeName);
   switch( SoundType){
      case SOUND_OK :
         FileName = "Success.wav";
         Beep     = MB_OK;
         break;

      case SOUND_FAILURE :
         FileName = "Error.wav";
         Beep     = MB_ICONEXCLAMATION;
         break;
   }
   Path += FileName;
   if( !FileExists( Path)){
      MessageBeep( Beep);
      return;
   }
   ::PlaySound( Path.c_str(), NULL, SND_FILENAME | SND_ASYNC);
} // Play

//------------------------------------------------------------------------------
// Initialize Tester
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnInitializeTesterClick(TObject *Sender)
{
   StartComm();
   if( !TesterDefaults()){
      StopComm();
      return;
   }
   //!!! enable buttons
   StopComm();
} // BtnInitializeTesterClick

//------------------------------------------------------------------------------
// FTDI programming
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnFtdiProgrammingClick(TObject *Sender)
{
   StartComm();
   if( !FtdiProgramming()){
      StopComm();
      return;
   }
   StopComm();
} // BtnFtdiProgammingClick

//------------------------------------------------------------------------------
// Test firmware
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnTestFirmwareClick(TObject *Sender)
{
   StartComm();
   if( !WriteTestFirmware()){
      StopComm();
      return;
   }
   StopComm();
} // BtnTestFirmwareClick

//------------------------------------------------------------------------------
// Printer COM
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnPrinterComClick(TObject *Sender)
{
   StartComm();
   if( !MasterComTest()){
      StopComm();
      return;
   }
   StopComm();
} // BtnPrinterComClick

//------------------------------------------------------------------------------
// USB test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnUsbTestClick(TObject *Sender)
{
   StartComm();
   Crt->printf( "USB test...\n");
   if( !Com->StartUsb()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      StopComm();
      return;
   }
   if( !Usb->TestUart()){
      Crt->printf( "Error : %s\n", Usb->Status.c_str());
      StopComm();
      return;
   }
   if( !Com->UsbResult()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      StopComm();
      return;
   }
   Crt->printf( "USB O.K.\n");
   StopComm();
} // BtnUsbTestClick

//------------------------------------------------------------------------------
// Display test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnDisplayTestClick(TObject *Sender)
{
   StartComm();
   Crt->printf( "DISPLAY test...\n");
   if( !Com->TestDisplay()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      StopComm();
      return;
   }
   Crt->printf( "DISPLAY O.K.\n");
   StopComm();
} // BtnDisplayTestClick

//------------------------------------------------------------------------------
// Memory test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnMemoryTestClick(TObject *Sender)
{
   StartComm();
   Crt->printf( "MEMORY test...\n");
   if( !Com->TestMemory()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      StopComm();
      return;
   }
   Crt->printf( "MEMORY O.K.\n");
   StopComm();
} // BtnMemoryTestClick

//------------------------------------------------------------------------------
// RTC test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnRtcTestClick(TObject *Sender)
{
   StartComm();
   Crt->printf( "RTC test...\n");
   if( !Com->TestRtc()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      StopComm();
      return;
   }
   Crt->printf( "RTC O.K.\n");
   StopComm();
} // BtnRtcTestClick

//------------------------------------------------------------------------------
// Sound test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnSoundTestClick(TObject *Sender)
{
   StartComm();
   Crt->printf( "SOUND test...\n");
   if( !Com->TestSound()){
      Crt->printf( "Error : %s\n", Com->Status.c_str());
      StopComm();
      return;
   }
   Crt->printf( "SOUND O.K.\n");
   StopComm();
} // BtnSoundTestClick

//------------------------------------------------------------------------------
// ADC test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnAdcTestClick(TObject *Sender)
{
   StartComm();
   if( !AdcCalibrationTest()){
      StopComm();
      return;
   }
   StopComm();
} // BtnAdcTestClick

//------------------------------------------------------------------------------
// IADC test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnIadcTestClick(TObject *Sender)
{
   StartComm();
   if( !IadcCalibrationTest()){
      StopComm();
      return;
   }
   StopComm();
} // BtnIadcTestClick

//------------------------------------------------------------------------------
// Keyboard test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnKeyboardTestClick(TObject *Sender)
{
   StartComm();
   if( !KeyboardTest()){
      StopComm();
      return;
   }
   StopComm();
} // BtnKeyboardTestClick

//------------------------------------------------------------------------------
// BAT firmware
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnBatFirmwareClick(TObject *Sender)
{
   StartComm();
   Crt->printf( "BAT Firmware write...\n");
   if( !Adapter->UsbPowerOn()){
      Crt->printf( "Error : Unable switch USB power on\n");
      return;
   }
   Sleep( 1000);
   if( !WriteBatFirmware()){
      StopComm();
      return;
   }
   Adapter->Shutdown();
   Crt->printf( "BAT Firmware O.K.\n");
   StopComm();
} // BtnBatFirmwareClick

//------------------------------------------------------------------------------
// BAT test
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnBatTestClick(TObject *Sender)
{
   StartComm();
   Crt->printf( "BAT Test...\n");
   if( !Adapter->UsbPowerOn()){
      Crt->printf( "Error : Unable switch USB power on\n");
      return;
   }
   Sleep( 1000);
   if( !BatTest()){
      StopComm();
      return;
   }
   Adapter->Shutdown();
   Crt->printf( "BAT O.K.\n");
   StopComm();
} // BtnBatTestClick

//------------------------------------------------------------------------------
// Inverted power
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnInvertedClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->InvertedPower()){
      StopComm();
      return;
   }
   Crt->printf( "POWER inversion O.K.\n");
   StopComm();
} // BtnInvertedClick

//------------------------------------------------------------------------------
// Idle power
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnIdleClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->IdleCurrent()){
      StopComm();
      return;
   }
   Crt->printf( "POWER idle O.K.\n");
   StopComm();
} // BtnIdleClick

//------------------------------------------------------------------------------
// Normal power
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnNormalClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->NormalCurrent()){
      StopComm();
      return;
   }
   Crt->printf( "POWER normal O.K.\n");
   StopComm();
} // BtnNormalClick

//------------------------------------------------------------------------------
// Without Backlight
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnDarkClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->DarkCurrent()){
      StopComm();
      return;
   }
   Crt->printf( "POWER without backlight O.K.\n");
   StopComm();
} // BtnDarkClick

//------------------------------------------------------------------------------
// Low voltage
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnLowVoltageClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->LowVoltage()){
      StopComm();
      return;
   }
   Crt->printf( "POWER low voltage O.K.\n");
   StopComm();
} // BtnLowVoltageClick

//------------------------------------------------------------------------------
// Charger connected
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnChargerClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->Charger()){
      StopComm();
      return;
   }
   Crt->printf( "CHARGER on/off O.K.\n");
   StopComm();
} // BtnChargerClick

//------------------------------------------------------------------------------
// USB Charger connected
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnUsbChargerClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->UsbCharger()){
      StopComm();
      return;
   }
   Crt->printf( "USB charger on/off O.K.\n");
   StopComm();
} // BtnUsbChargerClick

//------------------------------------------------------------------------------
// Charger OFF
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnChargerDoneClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->ChargerDone()){
      StopComm();
      return;
   }
   Crt->printf( "CHARGER stop O.K.\n");
   StopComm();
} // BtnChargerDoneClick

//------------------------------------------------------------------------------
// USB charger OFF
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnUsbDoneClick(TObject *Sender)
{
   StartComm();
   if( !Adapter->UsbChargerDone()){
      StopComm();
      return;
   }
   Crt->printf( "USB charger stop O.K.\n");
   StopComm();
} // BtnUsbDoneClick

//------------------------------------------------------------------------------
// USB power On/Off
//------------------------------------------------------------------------------

void __fastcall TMainForm::BtnPowerOnClick(TObject *Sender)
{
   if( !Adapter->UsbPowerOn()){
      Crt->printf( "Error : Unable switch USB power ON\n");
      return;
   }
   Crt->printf( "USB Power ON\n");
} // BtnPowerOnClick


void __fastcall TMainForm::BtnPowerOffClick(TObject *Sender)
{
   Adapter->Shutdown();
   Crt->printf( "USB Power OFF\n");
} // BtnPowerOffClick

