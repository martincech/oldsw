//******************************************************************************
//
//   Setup.h      Tester setup
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef SetupH
#define SetupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <IniFiles.hpp>

//******************************************************************************
// TSetupForm
//******************************************************************************

class TSetupForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TComboBox *CbPrinterPort;
        TCheckBox *CbEnableTester;
        TCheckBox *CbFtdiProgramming;
        TCheckBox *CbHardwareTesting;
        TCheckBox *CbBatProgramming;
        TButton *BtnSetupOk;
        TButton *BtnSetupCancel;
        TLabel *Label2;
        TComboBox *CbTesterPort;
   TCheckBox *CbTestFirmwareProgramming;
   TCheckBox *CbWaitForUsb;
   TLabel *Label3;
   TEdit *EdtDataPath;
public:
   __fastcall TSetupForm(TComponent* Owner);
   bool Execute();
   void Save();
   void Load();

// properties
   bool EnableTester;              // tester HW present
   bool FtdiProgramming;           // programming FTDI
   bool WaitForUsb;                // wait for USB driver installation
   bool TestFirmwareProgramming;   // write test firmware
   bool HardwareTesting;           // bat hardware test
   bool BatProgramming;            // bat programming
   char PrinterPort[ 16];          // printer port name
   char TesterPort[ 16];           // tester port name
   char DataPath[ 256];            // tester data path
private :
   int         Version;        // setup file version
   TIniFile   *SetupFile;      // INI file with parameters
}; // TSetupForm


//---------------------------------------------------------------------------
extern PACKAGE TSetupForm *SetupForm;
//---------------------------------------------------------------------------
#endif
