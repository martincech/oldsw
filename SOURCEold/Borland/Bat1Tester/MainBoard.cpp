//******************************************************************************
//
//   MainBoard.cpp Bat1 tester board
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainBoard.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#include "TestAdapter.h"

static TTestAdapter *Adapter;

#define Status( msg)  LblStatus->Caption = msg;
#define StatusOk()    Status( "OK");

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TTestAdapter;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   CbPort->ItemIndex = 0;
   if( !Adapter->Open( CbPort->Text.c_str())){
      Status( "Unable open COM");
   }
   Status( CbPort->Text);
} // FormCreate

//******************************************************************************
// Port
//******************************************************************************

void __fastcall TMainForm::CbPortChange(TObject *Sender)
{
   if( !Adapter->Open( CbPort->Text.c_str())){
      Status( "Unable open COM");
   }
   Status( CbPort->Text);
} // CbPortChange

//******************************************************************************
// Power mode
//******************************************************************************

void __fastcall TMainForm::RgPowerModeClick(TObject *Sender)
{
   if( !Adapter->SetPower( RgPowerMode->ItemIndex)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // RgPowerModeClick

//******************************************************************************
// Power regulation
//******************************************************************************

void __fastcall TMainForm::BtnMaxClick(TObject *Sender)
{
   if( !Adapter->VoltageMax()){
      Status( Adapter->Status);
      return;
   }
   Sleep( 50);
   StatusOk();
   UpdateVoltage();
} // BtnMaxClick

void __fastcall TMainForm::BtnUpClick(TObject *Sender)
{
   if( !Adapter->VoltageUp()){
      Status( Adapter->Status);
      return;
   }
   Sleep( 50);
   StatusOk();
   UpdateVoltage();
} // BtnUpClick

void __fastcall TMainForm::BtnDownClick(TObject *Sender)
{
   if( !Adapter->VoltageDown()){
      Status( Adapter->Status);
      return;
   }
   Sleep( 50);
   StatusOk();
   UpdateVoltage();
} // BtnDownClick

void __fastcall TMainForm::BtnMinClick(TObject *Sender)
{
   if( !Adapter->VoltageMin()){
      Status( Adapter->Status);
      return;
   }
   Sleep( 50);
   StatusOk();
   UpdateVoltage();
} // BtnMinClick

//******************************************************************************
//   Get voltage
//******************************************************************************

void __fastcall TMainForm::BtnGetVoltageClick(TObject *Sender)
{
   UpdateVoltage();
} // BtnGetVoltageClick

//******************************************************************************
// Sensitive
//******************************************************************************

void __fastcall TMainForm::CbSensitiveClick(TObject *Sender)
{
   if( !Adapter->SetCurrent( CbSensitive->Checked)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
   UpdateCurrent();
} // CbSensitiveClick

//******************************************************************************
// Get current
//******************************************************************************

void __fastcall TMainForm::BtnGetCurrentClick(TObject *Sender)
{
   UpdateCurrent();
} // GetCurrentClick

//******************************************************************************
// Bridge
//******************************************************************************

void __fastcall TMainForm::RgBridgeClick(TObject *Sender)
{
   if( !Adapter->Bridge( RgBridge->ItemIndex)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // RgBridgeClick

//******************************************************************************
// Keyboard
//******************************************************************************

void __fastcall TMainForm::CbKeyOnClick(TObject *Sender)
{
   if( !Adapter->KeyboardOn( CbKeyOn->Checked)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // CbKeyOnClick

void __fastcall TMainForm::CbKeyK0Click(TObject *Sender)
{
   if( !Adapter->KeyboardK0( CbKeyK0->Checked)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // CbKeyK0Click

void __fastcall TMainForm::CbKeyK1Click(TObject *Sender)
{
   if( !Adapter->KeyboardK1( CbKeyK1->Checked)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // CbKeyK1Click

void __fastcall TMainForm::CbKeyK2Click(TObject *Sender)
{
   if( !Adapter->KeyboardK2( CbKeyK2->Checked)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // CbKeyK2Click

//******************************************************************************
// USB
//******************************************************************************

void __fastcall TMainForm::CbUsbClick(TObject *Sender)
{
   if( !Adapter->UsbConnect( CbUsb->Checked)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // CbUsbClick

//******************************************************************************
// Charger
//******************************************************************************

void __fastcall TMainForm::CbChargerClick(TObject *Sender)
{
   if( !Adapter->ChargerConnect( CbCharger->Checked)){
      Status( Adapter->Status);
      return;
   }
   StatusOk();
} // CbChargerClick

//******************************************************************************
// Defaults
//******************************************************************************

void __fastcall TMainForm::BtnDefaultsClick(TObject *Sender)
{
   // power supply
   RgPowerMode->ItemIndex = 0;
   BtnMinClick( 0);
   // current measuring
   CbSensitive->Checked = false;
   // bridge
   RgBridge->ItemIndex = 0;
   // keyboard
   CbKeyOn->Checked = false;
   CbKeyK0->Checked = false;
   CbKeyK1->Checked = false;
   CbKeyK2->Checked = false;
   // external power
   CbUsb->Checked     = false;
   CbCharger->Checked = false;
   if( !Adapter->SetDefaults()){
      Status( Adapter->Status);
   }
   UpdateVoltage();
   UpdateCurrent();
} // BtnDefaultsClick

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TMainForm::TimerTimer(TObject *Sender)
{
   if( !CbAutomatic->Checked){
      return;
   }
   UpdateVoltage();
   UpdateCurrent();
} // TimerTimer

//------------------------------------------------------------------------------

//******************************************************************************
// Update voltage
//******************************************************************************

void TMainForm::UpdateVoltage()
// measure & redraw voltage
{
   int Voltage;
   if( RgPowerMode->ItemIndex == 2){
      // inverted voltage
      if( !Adapter->GetVoltageInverted( Voltage)){
         LblVoltage->Caption = '?';
         LblVoltage->Refresh();
         return;
      }
   } else {
      // normal voltage
      if( !Adapter->GetVoltage( Voltage)){
         LblVoltage->Caption = '?';
         LblVoltage->Refresh();
         return;
      }
   }
   LblVoltage->Caption = FloatToStrF( (double)Voltage / 1000, ffFixed, 6, 3) + AnsiString( "V");
   LblVoltage->Refresh();
} // UpdateVoltage

//******************************************************************************
// UpdateCurrent
//******************************************************************************

void TMainForm::UpdateCurrent()
// measure & redreaw current
{
   int Current;
   if( RgPowerMode->ItemIndex == 2){
      // inverted voltage
     if( !Adapter->GetCurrentInverted( Current)){
        LblCurrent->Caption = '?';
        LblCurrent->Refresh();
        return;
     }
   } else {
      // normal voltage
     if( !Adapter->GetCurrent( Current)){
        LblCurrent->Caption = '?';
        LblCurrent->Refresh();
        return;
     }
   }
   if( Adapter->SensitiveCurrent){
      LblCurrent->Caption = FloatToStrF( (double)Current / 100, ffFixed, 7, 2) + AnsiString( " uA");
   } else {
      LblCurrent->Caption = IntToStr( Current) + AnsiString( " mA");
   }
   LblCurrent->Refresh();
} // UpdateCurrent

