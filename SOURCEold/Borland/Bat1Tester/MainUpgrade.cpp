//******************************************************************************
//
//   MainUpgrade.cpp  Bat1 Upgrade main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainUpgrade.h"
#include <IniFiles.hpp>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;


// Status line :
#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

// Setup Read macros :
#define SETUP_VERSION 0x100

// Bat1 default file name
#define DEFAULT_FILE           "FILE000"

#define StpReadString( Section, Parameter) \
   Parameter = SetupFile->ReadString( #Section, #Parameter, "")

#define StpReadInteger( Section, Parameter) \
   Parameter = SetupFile->ReadInteger( #Section, #Parameter, 0)

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Usb     = new TUsbTester;
   Bat1Usb = new TBat1Usb;
   UsbName         = "?";
   UsbManufacturer = "?";
   ScaleName       = "?";
   UsbDisconnect   = false;     // don't request USB disconnect
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   // load setup :
   if( _argc <= 1){
      Status() + "Use 'Bat1Upgrade <C:\\DATA\\INI_FILE.INI>'";
      return;
   }
   if( _argc > 2){
      UsbDisconnect = true;
   }
   AnsiString  SetupFileName = _argv[ 1];
   TIniFile   *SetupFile;
   SetupFile = new TIniFile( SetupFileName);
   // check for setup version / empty setup :
   int Version;
   StpReadInteger( Properties, Version);
   if( Version != SETUP_VERSION){
      Status() + "Unable read " + SetupFileName;
      return;
   }
   // read setup data :
   StpReadString( Properties, LogoName);
   StpReadString( Properties, UsbName);
   StpReadString( Properties, UsbManufacturer);
   StpReadString( Properties, ScaleName);
   delete SetupFile;
   // check for scale name
   if( strlen( ScaleName.c_str()) > SCALE_NAME_LENGTH){
      Status() + "Scale name too long";
      return;
   }
   // load logo :
   try {
      Image->Picture->LoadFromFile( LogoName);
   } catch(...){
      Status() + "Unable read '" + LogoName + "'";
      return;
   }
   Bat1Usb->PrepareLogo( Image);                            // recalculate logo
   Bat1Usb->DefaultConfig();                                // prepare factory defaults
   strcpy( Bat1Usb->Config->ScaleName, ScaleName.c_str());  // set new scale name
   // show setup :
   LblVersion->Caption         = Bat1Usb->GetVersion();
   LblUsbName->Caption         = UsbName;
   LblUsbManufacturer->Caption = UsbManufacturer;
   LblScaleName->Caption       = ScaleName;
   Status() + "Setup OK";
} // FormCreate

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   this->Enabled  = false;             // disable main window
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;       // wait cursor
} // StartCom

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   this->Enabled  = true;              // enable main window
   Screen->Cursor = crDefault;         // normal cursor
} // StopComm

//******************************************************************************
//  Upgrade
//******************************************************************************

void __fastcall TMainForm::BtnUpgradeClick(TObject *Sender)
{
   StartComm();
   Status() + "?";
   StatusBar->Refresh();

   //-------------------- programming USB device :
   if( !Usb->WaitForProgrammedDevice()){
      Status() + "ERROR "+ Usb->Status;
      StopComm();
      return;
   }
   Status() + "Device " + Usb->AdapterName;
   StatusBar->Refresh();
   if( !Usb->WriteEeprom( UsbName.c_str(), UsbManufacturer.c_str(), false)){
      Status() + "ERROR " + Usb->Status;
      StopComm();
      return;
   }
   // disconnect device :
   if( UsbDisconnect){
      Application->MessageBox( "Disconnect USB", "Information", MB_OK);
      if( !Usb->WaitForDisconnect()){
         Status() + "ERROR " + Usb->Status;
         StopComm();
         return;
      }
      Sleep( 500);
      Application->MessageBox( "Connect USB", "Information", MB_OK);
      Sleep( 500);
   }
   //-------------------- programming Bat1 :
   if( !Bat1Usb->IsConnected()){
      Status() + "ERROR Unable connect Bat1";
      StopComm();
      return;
   }
   // clock
   Status() + "BAT1 clock...";
   StatusBar->Refresh();
   if( !Bat1Usb->SetTime()){
      Status() + "ERROR Unable set clock";
      StopComm();
      return;
   }
   // format :
   Status() + "BAT1 format...";
   StatusBar->Refresh();
   if( !Bat1Usb->FormatFilesystem()){
      Status() + "ERROR Unable format filesystem";
      StopComm();
      return;
   }
   // create file :
   Status() + "BAT1 create file...";
   StatusBar->Refresh();
   if( !Bat1Usb->CreateFile( DEFAULT_FILE)){
      Status() + "ERROR Unable create file";
      StopComm();
      return;
   }
   // save configuration :
   Status() + "BAT1 save configuration...";
   StatusBar->Refresh();
   if( !Bat1Usb->WriteConfig()){
      Status() + "ERROR Unable save configuration";
      StopComm();
      return;
   }
   // write logo :
   Status() + "BAT1 write logo...";
   StatusBar->Refresh();
   if( !Bat1Usb->WriteLogo()){
      Status() + "ERROR Unable write logo";
      StopComm();
      return;
   }
   //------- done
   StopComm();
   Status() + "Upgrade OK";
} // BtnUpgradeClick

