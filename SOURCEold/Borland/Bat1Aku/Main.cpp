//******************************************************************************
//
//   Main.cpp     Bat1Aku main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TPktAdapter;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize


//******************************************************************************
// Read
//******************************************************************************

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   Screen->Cursor = crHourGlass;
   if( !Adapter->Read( EdtFilename->Text)){
      Screen->Cursor = crDefault;
      Status() + "Unable read";
      return;
   }
   Screen->Cursor = crDefault;
   StatusOk();
} // BtnReadClick

//******************************************************************************
// Enable Dump
//******************************************************************************

void __fastcall TMainForm::CbEnableDumpClick(TObject *Sender)
{
   if( CbEnableDump->Checked){
      Adapter->Logger = Logger;
   } else {
      Adapter->Logger = 0;
   }
} // CbEnableDumpClick

