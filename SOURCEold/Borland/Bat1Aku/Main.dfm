object MainForm: TMainForm
  Left = 212
  Top = 169
  Width = 802
  Height = 645
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainMemo: TMemo
    Left = 0
    Top = 88
    Width = 641
    Height = 497
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 599
    Width = 794
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnRead: TButton
    Left = 656
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 2
    OnClick = BtnReadClick
  end
  object EdtFilename: TEdit
    Left = 16
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 3
    Text = 'Data\Test.txt'
  end
  object CbEnableDump: TCheckBox
    Left = 544
    Top = 24
    Width = 97
    Height = 17
    Caption = 'Enable Dump'
    TabOrder = 4
    OnClick = CbEnableDumpClick
  end
end
