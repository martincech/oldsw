//******************************************************************************
//
//   PktAdapter.cpp   Packet adapter
//   Version 0.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "PktAdapter.h"
#include <time.h>
#include <stdio.h>

#include "../Serial/Usb/UsbUart.h"

// Configuration names :

#define NAME          "Adapter"
#define ADAPTER_NAME  "PktAdapter"
#define TYPE          "Type"
#define PORT          "Port"

#define USB_DEVICE_NAME "VEIT Bat1 Connection"        // USB adapter name

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TPktAdapter::TPktAdapter()
// Constructor
{
   FPort       = 0;
   FLogger     = 0;

   FRxTimeout               = 2000;
   // default parameters :
   FParameters.BaudRate     = 38400;
   FParameters.DataBits     = 8;
   FParameters.StopBits     = 10;
   FParameters.Parity       = TUart::NO_PARITY;
   FParameters.Handshake    = TUart::NO_HANDSHAKE;
   FUsbDevice               = USB_DEVICE_NAME;
} // TPktAdapter

//******************************************************************************
// Destructor
//******************************************************************************

TPktAdapter::~TPktAdapter()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TPktAdapter


//******************************************************************************
// Receive
//******************************************************************************

#define RX_WAIT 5        // wait for Rx [s]

bool TPktAdapter::Read( TString FileName)
// Receive message
{
int Size;

   if( !CheckConnect()){
      return( false);
   }
   FPort->SetRxNowait();
   time_t Now     = time( NULL) + RX_WAIT;
   bool   Timeout = YES;
   do {
      if( FPort->Read( Buffer, 1) == 1){
         Timeout = NO;
         RwdRx( Buffer, 1);
         break;
      }
   } while( time( NULL) < Now);
   if( Timeout){
      return( false);
   }
   FILE *f = fopen( FileName.c_str(), "wb");
   if( !f){
      return( false);
   }
   fwrite( Buffer, 1, 1, f);
   FPort->SetRxWait( FRxTimeout, 0);
   while( 1){
      Size = FPort->Read( Buffer, sizeof( Buffer));
      if( !Size){
         break;
      }
      RwdRx( Buffer, Size);
      fwrite( Buffer, Size, 1, f);
   }
   fclose( f);
   return( true);
} // Receive

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Identifier
//******************************************************************************

TIdentifier TPktAdapter::GetIdentifier()
// GetIdentifier
{
   if( !FPort){
      return( INVALID_IDENTIFIER);
   }
   return( FPort->Identifier);
} // GetIdentifier

//******************************************************************************
// Property Name
//******************************************************************************

TName TPktAdapter::GetName()
// Get device name
{
   if( !FPort){
      return("No adapter");
   }
   return( FPort->Name);
} // GetName

//******************************************************************************
// Property IsOpen
//******************************************************************************

bool  TPktAdapter::GetIsOpen()
// Check if device is opened
{
   if( !FPort){
      return( false);
   }
   return( FPort->IsOpen);
} // GetIsOpen

//******************************************************************************
// Check Connection
//******************************************************************************

bool TPktAdapter::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( true);
   }
   if( FPort){
      IERROR;                          // always opened !
   }
   TIdentifier Identifier;
   TUsbUart *Usb = 0;
   // USB setup
   Usb = new TUsbUart;
   if( !Usb->Locate( FUsbDevice, Identifier)){
      delete Usb;
      return( false);
   }
   if( !Usb->Open( Identifier)){
      delete Usb;
      return( false);
   }
   Usb->SetParameters( FParameters);
   // common init :
   Usb->DTR = false;
   Usb->RTS = false;
   Usb->SetRxNowait();
   Usb->Flush();
   FPort = Usb;
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TPktAdapter::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

