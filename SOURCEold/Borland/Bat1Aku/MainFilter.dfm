object MainForm: TMainForm
  Left = 333
  Top = 364
  Width = 539
  Height = 236
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 376
    Top = 19
    Width = 17
    Height = 13
    Caption = 'Min'
  end
  object EdtFilename: TEdit
    Left = 24
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'Data\Test.txt'
  end
  object BtnCalculate: TButton
    Left = 24
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Calculate'
    TabOrder = 1
    OnClick = BtnCalculateClick
  end
  object EdtMin: TEdit
    Left = 248
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '5'
  end
end
