//******************************************************************************
//
//   PktAdapter.h     Packet adapter
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef PktAdapterH
   #define PktAdapterH

#ifndef UartH
   #include "../Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Serial/Logger.h"
#endif

#define PKTA_BUFFER_SIZE 1024

//******************************************************************************
// TPktAdapter
//******************************************************************************

class TPktAdapter {
public :
   TPktAdapter();
   // Constructor
   ~TPktAdapter();
   // Destructor
   bool Read( TString FileName);
   // Read data to file

   __property TIdentifier Identifier           = {read=GetIdentifier};
   __property TName       Name                 = {read=GetName};
   __property bool        IsOpen               = {read=GetIsOpen};

   __property TUart   *Port                    = {read=FPort};
   __property int      RxTimeout               = {read=FRxTimeout, write=FRxTimeout};

   __property TLogger *Logger       = {read=FLogger, write=FLogger};
   __property TUart::TParameters *Parameters   = {read=GetParameters};
   __property TString             UsbDevice    = {read=FUsbDevice, write=FUsbDevice};
//------------------------------------------------------------------------------

protected :
   TUart   *FPort;                     // connection port
   TLogger *FLogger;                   // raw data logger
   int      FRxTimeout;                // Rx timeout
   byte     Buffer[ PKTA_BUFFER_SIZE]; // packet buffer
   TUart::TParameters FParameters;     // Communication parameters
   TString  FUsbDevice;                // USB device name

   TIdentifier GetIdentifier();
   // GetIdentifier
   TName GetName();
   // Get device name
   bool  GetIsOpen();
   // Check if device is opened

   bool CheckConnect();
   // Check if adapter is ready
   void Disconnect();
   // Disconnect port
   TUart::TParameters *GetParameters(){ return( &FParameters);}
};

#endif
