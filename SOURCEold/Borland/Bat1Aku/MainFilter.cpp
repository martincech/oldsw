//******************************************************************************
//
//   MainFilter.cpp Samples filter
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainFilter.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
#include <stdio.h>

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Calculate
//******************************************************************************


void __fastcall TMainForm::BtnCalculateClick(TObject *Sender)
{
   // decimation interval :
   int  DecMin = EdtMin->Text.ToInt();
   // input file :
   FILE *f;
   f = fopen( EdtFilename->Text.c_str(), "rb");
   if( !f){
      Application->MessageBox( "Unable open input file", "Error", MB_OK);
      return;
   }
   // output file :
   FILE *fo;
   AnsiString FileName = EdtFilename->Text;
   FileName = ChangeFileExt( FileName, ".csv" );
   fo = fopen( FileName.c_str(), "wb");
   if( !fo){
      Application->MessageBox( "Unable open output file", "Error", MB_OK);
      return;
   }
   // read lines :
   char Buffer[ 150];
   int Hour, Min, Sec;
   while( fgets( Buffer, sizeof( Buffer) - 1, f)){
      sscanf( Buffer, "%d:%d:%d", &Hour, &Min, &Sec);
      if( Sec){
         continue;         // whole min only
      }
      if( Min % DecMin != 0){
         continue;         // modul only
      } 
      fputs( Buffer, fo);
   }
   fclose( f);
   fclose( fo);
} // BtnCalculateClick

