object MainForm: TMainForm
  Left = 238
  Top = 155
  Width = 910
  Height = 654
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 240
    Top = 24
    Width = 92
    Height = 13
    Caption = 'Window (samples) :'
  end
  object Label4: TLabel
    Left = 240
    Top = 56
    Width = 33
    Height = 13
    Caption = 'Skew :'
  end
  object Graph: TDBChart
    Left = 16
    Top = 120
    Width = 873
    Height = 497
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.DateTimeFormat = 'h:mm'
    View3D = False
    TabOrder = 0
    object HmotnostSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HMOTNOST'
    end
    object LowPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'LO_PASS'
    end
    object HighPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HI_PASS'
    end
  end
  object BtnLowPass: TButton
    Left = 24
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Low Pass'
    TabOrder = 1
    OnClick = BtnLowPassClick
  end
  object EditWindow: TEdit
    Left = 352
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '40'
  end
  object BtnHighPass: TButton
    Left = 24
    Top = 56
    Width = 75
    Height = 25
    Caption = 'High Pass'
    TabOrder = 3
    OnClick = BtnHighPassClick
  end
  object EditSkew: TEdit
    Left = 352
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '40'
  end
  object BtnNormal: TButton
    Left = 24
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Normal'
    TabOrder = 5
    OnClick = BtnNormalClick
  end
end
