//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Dialogs.hpp>
#include "Acceptance.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *Label3;
        TLabel *Label1;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *LblFileName;
        TLabel *Label13;
        TEdit *EdtAveraging;
        TButton *BtnRedraw;
        TEdit *EdtWidth;
        TEdit *EdtOffset;
        TButton *BtnBackward;
        TButton *BtnForward;
        TButton *BtnFirst;
        TEdit *EdtStableRange;
        TEdit *EdtStable;
        TButton *BtnFile;
        TEdit *EdtPrefilter;
        TOpenDialog *FileOpenDialog;
   TButton *BtnExport;
   TSaveDialog *FileSaveDialog;
   TDBChart *HistogramGraph;
   TBarSeries *HistogramSeries;
   TPanel *Panel5;
   TLabel *Label22;
   TLabel *Label23;
   TLabel *Label25;
   TLabel *Label26;
   TLabel *Label27;
   TLabel *Label28;
   TLabel *Label32;
   TEdit *AcceptanceTargetEdit;
   TEdit *AcceptanceAboveEdit;
   TEdit *AcceptanceUnderEdit;
   TRadioGroup *AcceptanceModeRg;
   TLabel *Label37;
   TLabel *Label38;
   TEdit *HistogramRangeEdit;
   TDBChart *Graph;
   TLineSeries *WeightSeries;
   TLineSeries *LowPassSeries;
   TLineSeries *HighPassSeries;
   TLineSeries *StepSeries;
   TLineSeries *StableSeries;
   TLineSeries *StableWeightSeries;
   TLineSeries *LastStableWeightSeries;
   TPointSeries *AcceptedSeries;
   TPanel *Panel2;
   TLabel *HeadsDetectedPs081Label;
   TLabel *AverageWeightPs081Label;
   TLabel *Label33;
   TLabel *Label35;
   TLabel *Label6;

        void __fastcall BtnRedrawClick(TObject *Sender);
        void __fastcall BtnBackwardClick(TObject *Sender);
        void __fastcall BtnForwardClick(TObject *Sender);
        void __fastcall BtnFirstClick(TObject *Sender);
        void __fastcall BtnFileClick(TObject *Sender);
   void __fastcall BtnExportClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
        TAcceptance *Accept;
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
