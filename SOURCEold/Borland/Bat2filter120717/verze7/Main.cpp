//******************************************************************************
//
//   Main.cpp      Bat2 filter demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Data.h"
#include "Filter.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define TOTAL_SAMPLES 2000

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Normal
//******************************************************************************

#define STEP_UP_VALUE    100
#define STEP_DOWN_VALUE -100
#define STABLE_VALUE     120

void __fastcall TMainForm::BtnRedrawClick(TObject *Sender)
{
   FilterStop();
   // read filter data :
   FilterRecord.AveragingWindow = EdtAveraging->Text.ToInt();
   if( FilterRecord.AveragingWindow > FILTER_MAX_AVERAGING){
      // value out of limit - set limit
      FilterRecord.AveragingWindow = FILTER_MAX_AVERAGING;
      EdtAveraging->Text = AnsiString( FilterRecord.AveragingWindow);
   }
   if( FilterRecord.AveragingWindow < 1){
      // value out of limit - set limit
      FilterRecord.AveragingWindow = 1;
      EdtAveraging->Text = AnsiString( FilterRecord.AveragingWindow);
   }

   TRawWeight StepRange;
   try {
     StepRange = (TRawWeight)(EdtStepRange->Text.ToDouble() * 1000);
   } catch( ...){
     Application->MessageBox( "Invalid Step Range format", "Error", MB_OK);
     return;
   }
   FilterRecord.StepRange = StepRange;

   TRawWeight StableRange;
   try {
     StableRange = (TRawWeight)(EdtStableRange->Text.ToDouble() * 1000);
   } catch( ...){
     Application->MessageBox( "Invalid Stable Range format", "Error", MB_OK);
     return;
   }
   FilterRecord.StableRange = StableRange;

   FilterRecord.StableWindow = EdtStable->Text.ToInt();
   if( FilterRecord.StableWindow < 1){
      FilterRecord.StableWindow = 1;
      EdtStable->Text = AnsiString( FilterRecord.StableWindow);
   }

   if( CbxStepDown->Checked){
      SetFilterOnStepDown();
   } else {
      ClrFilterOnStepDown();
   }
   if( CbxStepUp->Checked){
      SetFilterOnStepUp();
   } else {
      ClrFilterOnStepUp();
   }
   // Predfiltrace
   int Prefilter;
   try {
     Prefilter = EdtPrefilter->Text.ToInt();
   } catch( ...){
     Application->MessageBox( "Invalid Prefilter", "Error", MB_OK);
     return;
   }
   BatData->SetPrefilter( Prefilter);

   // Clear old graph :
   WeightSeries->Clear();
   LowPassSeries->Clear();
   HighPassSeries->Clear();
   StepSeries->Clear();
   StableSeries->Clear();
   StableWeightSeries->Clear();
   // Read display window :
   int WindowWidth = EdtWidth->Text.ToInt();
   int WindowOffset = EdtOffset->Text.ToInt();
   // scroll at offset :
   BatData->Begin();
   for( int i = 0; i < WindowOffset; i++){
      if( !BatData->Next()){
         return;      // end of database
      }
   }
   FilterRecord.ZeroWeight = (TRawWeight)(BatData->Weight * 1000); // first = initial value
   // initialize filter :
   FilterStart();
   // draw graph :
   int StartIndex = WindowOffset;
   TRawWeight StableWeight = 0;
   while(1){
      FilterNextSample( (TRawWeight)(BatData->Weight * 1000));    // process filter
      WeightSeries->AddXY( StartIndex,
                           FilterRecord.RawWeight,
                            "", clTeeColor);
      LowPassSeries->AddXY( StartIndex,
                           FilterRecord.LowPass,
                            "", clTeeColor);
      HighPassSeries->AddXY( StartIndex,
                           FilterRecord.HighPass,
                            "", clTeeColor);
      StepSeries->AddXY( StartIndex,
                         GetFilterStatus() == FILTER_STEP_UP   ? STEP_UP_VALUE :
                        (GetFilterStatus() == FILTER_STEP_DOWN ? STEP_DOWN_VALUE : 0),
                            "", clTeeColor);
      StableSeries->AddXY( StartIndex,
                           GetFilterStatus() == FILTER_STABLE ? STABLE_VALUE : 0,
                            "", clTeeColor);
      StableWeightSeries->AddXY( StartIndex,
                           GetFilterStatus() == FILTER_STABLE ? FilterRecord.Weight : StableWeight,
                            "", clTeeColor);
      if( GetFilterStatus() == FILTER_STABLE){
         StableWeight = FilterRecord.Weight;
      }
      StartIndex++;
      if( StartIndex >= (WindowWidth + WindowOffset)){
         return;
      }
      if( !BatData->Next()){
         return;
      }
   }
} // BtnRedrawClick

//******************************************************************************
// Backward
//******************************************************************************

void __fastcall TMainForm::BtnBackwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset -= WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnBackwardClick

//******************************************************************************
// Forward
//******************************************************************************

void __fastcall TMainForm::BtnForwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset += WindowWidth;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnForwardClick

//******************************************************************************
// First
//******************************************************************************

void __fastcall TMainForm::BtnFirstClick(TObject *Sender)
{
   int WindowOffset = 0;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnFirstClick

//******************************************************************************
// File
//******************************************************************************

void __fastcall TMainForm::BtnFileClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   LblFileName->Caption = FileOpenDialog->FileName;
   BatData->SetFile( FileOpenDialog->FileName);
} // BtnFileClick

