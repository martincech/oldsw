//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TDBChart *Graph;
        TLineSeries *LowPassSeries;
        TLineSeries *HighPassSeries;
        TLineSeries *WeightSeries;
        TEdit *EdtAveraging;
        TLabel *Label3;
        TButton *BtnRedraw;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label4;
        TEdit *EdtWidth;
        TLabel *Label5;
        TEdit *EdtOffset;
        TButton *BtnBackward;
        TButton *BtnForward;
        TButton *BtnFirst;
        TLineSeries *StepSeries;
        TLineSeries *StableSeries;
        TLabel *Label6;
        TLabel *Label7;
        TEdit *EdtStepRange;
        TLabel *Label8;
        TEdit *EdtStableRange;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TEdit *EdtStable;
        TLineSeries *StableWeightSeries;
        TCheckBox *CbxStepDown;
        TLabel *Label12;
        TLabel *LblFileName;
        TButton *BtnFile;
        TOpenDialog *FileOpenDialog;
        TCheckBox *CbxStepUp;
        TEdit *EdtPrefilter;
        TLabel *Label13;
        void __fastcall BtnRedrawClick(TObject *Sender);
        void __fastcall BtnBackwardClick(TObject *Sender);
        void __fastcall BtnForwardClick(TObject *Sender);
        void __fastcall BtnFirstClick(TObject *Sender);
        void __fastcall BtnFileClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
