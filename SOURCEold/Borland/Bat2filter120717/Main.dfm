object MainForm: TMainForm
  Left = 237
  Top = 141
  Width = 1246
  Height = 967
  Caption = 'Bat2Filter'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1230
    Height = 145
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 536
      Top = 40
      Width = 48
      Height = 13
      Caption = 'Averaging'
    end
    object Label1: TLabel
      Left = 720
      Top = 40
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label4: TLabel
      Left = 64
      Top = 80
      Width = 28
      Height = 13
      Caption = 'Width'
    end
    object Label5: TLabel
      Left = 64
      Top = 104
      Width = 28
      Height = 13
      Caption = 'Offset'
    end
    object Label8: TLabel
      Left = 528
      Top = 64
      Width = 60
      Height = 13
      Caption = 'Stable range'
    end
    object Label9: TLabel
      Left = 720
      Top = 64
      Width = 50
      Height = 13
      Caption = '% of target'
    end
    object Label10: TLabel
      Left = 552
      Top = 88
      Width = 30
      Height = 13
      Caption = 'Stable'
    end
    object Label11: TLabel
      Left = 720
      Top = 88
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label12: TLabel
      Left = 8
      Top = 8
      Width = 22
      Height = 13
      Caption = 'File :'
    end
    object LblFileName: TLabel
      Left = 40
      Top = 8
      Width = 41
      Height = 13
      Caption = 'demo.db'
    end
    object Label13: TLabel
      Left = 560
      Top = 16
      Width = 22
      Height = 13
      Caption = 'Filter'
    end
    object Label37: TLabel
      Left = 8
      Top = 128
      Width = 82
      Height = 13
      Caption = 'Histogram Range'
    end
    object Label38: TLabel
      Left = 177
      Top = 128
      Width = 62
      Height = 13
      Caption = '+- % of target'
    end
    object Label6: TLabel
      Left = 720
      Top = 16
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object EdtAveraging: TEdit
      Left = 592
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '10'
    end
    object BtnRedraw: TButton
      Left = 312
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Redraw'
      TabOrder = 1
      OnClick = BtnRedrawClick
    end
    object EdtWidth: TEdit
      Left = 96
      Top = 72
      Width = 73
      Height = 21
      TabOrder = 2
      Text = '20000'
    end
    object EdtOffset: TEdit
      Left = 96
      Top = 96
      Width = 73
      Height = 21
      TabOrder = 3
      Text = '0'
    end
    object BtnBackward: TButton
      Left = 312
      Top = 56
      Width = 75
      Height = 25
      Caption = '<<<'
      TabOrder = 4
      OnClick = BtnBackwardClick
    end
    object BtnForward: TButton
      Left = 392
      Top = 56
      Width = 75
      Height = 25
      Caption = '>>>'
      TabOrder = 5
      OnClick = BtnForwardClick
    end
    object BtnFirst: TButton
      Left = 232
      Top = 56
      Width = 75
      Height = 25
      Caption = '|<<<'
      TabOrder = 6
      OnClick = BtnFirstClick
    end
    object EdtStableRange: TEdit
      Left = 592
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 7
      Text = '3'
    end
    object EdtStable: TEdit
      Left = 592
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 8
      Text = '10'
    end
    object BtnFile: TButton
      Left = 8
      Top = 24
      Width = 75
      Height = 25
      Caption = 'File'
      TabOrder = 9
      OnClick = BtnFileClick
    end
    object EdtPrefilter: TEdit
      Left = 592
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 10
      Text = '1'
    end
    object BtnExport: TButton
      Left = 96
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Export'
      TabOrder = 11
      OnClick = BtnExportClick
    end
    object Panel5: TPanel
      Left = 805
      Top = 14
      Width = 281
      Height = 99
      BevelOuter = bvLowered
      TabOrder = 12
      object Label22: TLabel
        Left = 8
        Top = 24
        Width = 37
        Height = 13
        Caption = 'Target :'
      end
      object Label23: TLabel
        Left = 173
        Top = 24
        Width = 12
        Height = 13
        Caption = 'kg'
      end
      object Label25: TLabel
        Left = 8
        Top = 48
        Width = 37
        Height = 13
        Caption = 'Above :'
      end
      object Label26: TLabel
        Left = 173
        Top = 48
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label27: TLabel
        Left = 8
        Top = 72
        Width = 35
        Height = 13
        Caption = 'Under :'
      end
      object Label28: TLabel
        Left = 173
        Top = 72
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label32: TLabel
        Left = 85
        Top = 2
        Width = 69
        Height = 13
        Caption = 'Acceptance'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object AcceptanceTargetEdit: TEdit
        Left = 77
        Top = 20
        Width = 89
        Height = 21
        TabOrder = 0
      end
      object AcceptanceAboveEdit: TEdit
        Left = 77
        Top = 44
        Width = 89
        Height = 21
        TabOrder = 1
      end
      object AcceptanceUnderEdit: TEdit
        Left = 77
        Top = 68
        Width = 89
        Height = 21
        TabOrder = 2
      end
      object AcceptanceModeRg: TRadioGroup
        Left = 192
        Top = 16
        Width = 81
        Height = 65
        Caption = 'Mode'
        ItemIndex = 0
        Items.Strings = (
          'Enter'
          'Leave'
          'Both')
        TabOrder = 3
      end
    end
    object HistogramRangeEdit: TEdit
      Left = 96
      Top = 120
      Width = 73
      Height = 21
      TabOrder = 13
      Text = '40'
    end
  end
  object HistogramGraph: TDBChart
    Left = 0
    Top = 654
    Width = 1230
    Height = 250
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TDBChart')
    Title.Visible = False
    View3D = False
    View3DWalls = False
    Align = alBottom
    TabOrder = 1
    object HistogramSeries: TBarSeries
      Marks.ArrowLength = 20
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Bar'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Graph: TDBChart
    Left = 0
    Top = 145
    Width = 1230
    Height = 509
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.DateTimeFormat = 'h:mm'
    Legend.Visible = False
    View3D = False
    Align = alClient
    TabOrder = 2
    object WeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HMOTNOST'
    end
    object LowPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'LO_PASS'
    end
    object HighPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 16744703
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HI_PASS'
    end
    object StepSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlue
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clLime
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableWeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clMaroon
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object LastStableWeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object AcceptedSeries: TPointSeries
      Marks.ArrowLength = 0
      Marks.Visible = False
      SeriesColor = clGray
      Pointer.Brush.Color = clWhite
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = True
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 904
    Width = 1230
    Height = 25
    Align = alBottom
    TabOrder = 3
    object HeadsDetectedPs081Label: TLabel
      Left = 112
      Top = 4
      Width = 6
      Height = 13
      Caption = '0'
    end
    object AverageWeightPs081Label: TLabel
      Left = 252
      Top = 4
      Width = 21
      Height = 13
      Caption = '0.00'
    end
    object Label33: TLabel
      Left = 152
      Top = 4
      Width = 80
      Height = 13
      Caption = 'Average Weight:'
    end
    object Label35: TLabel
      Left = 8
      Top = 4
      Width = 76
      Height = 13
      Caption = 'Heads detected'
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'db'
    FileName = 'demo'
    Filter = 'Database|*.db'
    Top = 8
  end
  object FileSaveDialog: TSaveDialog
    DefaultExt = 'csv'
    FileName = 'export'
    Filter = 'CSV export|*.csv'
    Title = 'Save export file'
    Left = 168
    Top = 24
  end
end
