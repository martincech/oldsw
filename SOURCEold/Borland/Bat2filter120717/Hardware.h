//******************************************************************************
//
//   Hardware.h   Dummy hardware description
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __Hardware_H__
#define __Hardware_H__

#include "../Library/Unisys/uni.h"

#define __xdata__

//-----------------------------------------------------------------------------
// Filtrace
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  50       // max. width of averaging window

// Basic data types :
typedef long   TRawWeight;             // weight data
typedef long   TLongWeight;            // weight sum
typedef byte   TSamplesCount;          // samples counter

#define FILTER_VISIBLE    1            // show filter data

//-----------------------------------------------------------------------------

#endif
