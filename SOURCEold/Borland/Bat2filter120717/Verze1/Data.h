//******************************************************************************
//
//   Data.cpp     Databaze
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef DataH
#define DataH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------
class TBatData : public TDataModule
{
__published:	// IDE-managed Components
        TTable *RawDataTable;
        TDataSource *RawDataSource;
        TTable *DataTable;
        TDataSource *DataSource;
        TDateTimeField *DataTableDATUM;
        TIntegerField *DataTableMS;
        TFloatField *DataTableHMOTNOST;
        TDateTimeField *RawDataTableDATUM;
        TIntegerField *RawDataTableMS;
        TFloatField *RawDataTableHMOTNOST;
private:	// User declarations
public:		// User declarations
   __fastcall TBatData(TComponent* Owner);
   void Begin();
   // pred prvni zaznam
   bool Next();
   // na dalsi zaznam
   void Copy( TDateTime From, TDateTime To, int Averaging);
   // kopie tabulky
   __property TDateTime Datum    = {read=GetDatum};
   __property unsigned  Ms       = {read=GetMs};
   __property double    Hmotnost = {read=GetHmotnost};
protected :
   TDateTime GetDatum();
   unsigned  GetMs();
   double    GetHmotnost();
};
//---------------------------------------------------------------------------
extern PACKAGE TBatData *BatData;
//---------------------------------------------------------------------------
#endif
