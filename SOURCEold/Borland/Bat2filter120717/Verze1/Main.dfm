object MainForm: TMainForm
  Left = 238
  Top = 155
  Width = 910
  Height = 654
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 680
    Top = 24
    Width = 29
    Height = 13
    Caption = 'From :'
  end
  object Label2: TLabel
    Left = 680
    Top = 56
    Width = 19
    Height = 13
    Caption = 'To :'
  end
  object Label3: TLabel
    Left = 240
    Top = 24
    Width = 92
    Height = 13
    Caption = 'Window (samples) :'
  end
  object Label4: TLabel
    Left = 240
    Top = 56
    Width = 33
    Height = 13
    Caption = 'Skew :'
  end
  object Label5: TLabel
    Left = 680
    Top = 80
    Width = 54
    Height = 13
    Caption = 'Averaging :'
  end
  object Graph: TDBChart
    Left = 16
    Top = 120
    Width = 873
    Height = 497
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.DateTimeFormat = 'h:mm'
    TabOrder = 0
    object HmotnostSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      DataSource = BatData.DataTable
      SeriesColor = clRed
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HMOTNOST'
    end
    object LowPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'LO_PASS'
    end
    object HighPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HI_PASS'
    end
  end
  object EditFrom: TEdit
    Left = 744
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '24.9.2004  10:18:40'
  end
  object EditTo: TEdit
    Left = 744
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '24.9.2004  10:20:20'
  end
  object BtnCopy: TButton
    Left = 568
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Copy'
    TabOrder = 3
    OnClick = BtnCopyClick
  end
  object BtnLowPass: TButton
    Left = 24
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Low Pass'
    TabOrder = 4
    OnClick = BtnLowPassClick
  end
  object EditWindow: TEdit
    Left = 352
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '40'
  end
  object BtnHighPass: TButton
    Left = 24
    Top = 56
    Width = 75
    Height = 25
    Caption = 'High Pass'
    TabOrder = 6
    OnClick = BtnHighPassClick
  end
  object EditSkew: TEdit
    Left = 352
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '40'
  end
  object EditAveraging: TEdit
    Left = 744
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 8
    Text = '1'
  end
end
