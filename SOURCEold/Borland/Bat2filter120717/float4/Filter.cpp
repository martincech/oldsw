//******************************************************************************
//
//   Filter.cpp   Filtering
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include <math.h>         // abs
#include "Filter.h"
#include "Data.h"

TDataRecord DataRecord;
static int  StableCounter;

TWeight Fifo[ FILTER_MAX_AVERAGING];
int     FifoPointer;

// Local functions :
static TWeight ReadConversion( void);
// Read conversion by A/D convertor

void FifoInitialize( void);
// Initialize FIFO data

TWeight FifoAverage( void);
// Calculate average of the contents

void FifoPut( TWeight Weight);
// Put to FIFO

//******************************************************************************
// Initialize
//******************************************************************************

void FilterInitialize( void)
// Initialize filter data
{
   FifoInitialize();
   DataRecord.Status = FILTER_WAIT;
   StableCounter = 0;
} // FilterInitialize

//******************************************************************************
// Sample
//******************************************************************************

void NextSample( void)
// Process next sample
{
   DataRecord.RawWeight = ReadConversion();           // read sample
   FifoPut( DataRecord.RawWeight);                    // save sample
   // filtering :
   DataRecord.LowPass  = FifoAverage();
   DataRecord.HighPass = DataRecord.RawWeight - DataRecord.LowPass;
   TFilterStatus LastStatus = DataRecord.Status;      // remember old status
   // check step :
   DataRecord.Status = FILTER_WAIT;
   if( DataRecord.HighPass > 0){
      // positive value
      if( DataRecord.HighPass > DataRecord.StepRange){
         DataRecord.Status = FILTER_STEP_UP;
      }
   } else {
      // negative value
      if( DataRecord.HighPass < -DataRecord.StepRange){
         DataRecord.Status = FILTER_STEP_DOWN;
      }
   }
   if( DataRecord.Status != FILTER_WAIT){
      return;      // done - step up/down
   }
   if( LastStatus == FILTER_WAIT){
      return;      // still waiting for step
   }
   // check for stability :
   TWeight AbsValue = fabs( DataRecord.HighPass);
   if( AbsValue < DataRecord.StableRange){
      StableCounter++;
   } else {
      StableCounter = 0;
   }
   if( StableCounter >= DataRecord.StableWindow){
      // stable value, previous context is step up/down or stable
      if( !DataRecord.OnStepDown && (LastStatus == FILTER_STEP_DOWN)){
         DataRecord.Status = FILTER_WAIT; // step down + stable = ignore
         return;
      } // else resolve step down also
      if( !DataRecord.OnStepUp   && (LastStatus == FILTER_STEP_UP)){
         DataRecord.Status = FILTER_WAIT; // step up + stable = ignore
         return;
      } // else resolve step down also
      DataRecord.Status = FILTER_STABLE;
      DataRecord.Weight = DataRecord.LowPass;
   } else {
      // instable value
      if( LastStatus == FILTER_STABLE){
         DataRecord.Status = FILTER_WAIT; // end of stable data
      } else {
         DataRecord.Status = LastStatus;  // continue step up/down
      }
   }
} // NextSample

//******************************************************************************
// Read conversion
//******************************************************************************

static TWeight ReadConversion( void)
// Read conversion by A/D convertor
{
   TWeight Weight = BatData->Weight;
   BatData->Next();
   return( Weight);
} // ReadConversion

//******************************************************************************
// Fifo initialize
//******************************************************************************

void FifoInitialize( void)
// Initialize FIFO data
{
   for( int i = 0; i < DataRecord.AveragingWindow; i++){
      Fifo[ i] = BatData->Weight;       // fill with first record value
   }
   FifoPointer = 0;
} // FifoInitialize

//******************************************************************************
// Fifo average
//******************************************************************************

TWeight FifoAverage( void)
// Calculate average of the contents
{
   TLongWeight Sum = 0;
   for( int i = 0; i < DataRecord.AveragingWindow; i++){
      Sum += Fifo[ i];
   }
   return( Sum / DataRecord.AveragingWindow);
} // FifoAverage

//******************************************************************************
// Fifo put
//******************************************************************************

void FifoPut( TWeight Weight)
// Put to FIFO
{
   Fifo[ FifoPointer++] = Weight;
   if( FifoPointer >= DataRecord.AveragingWindow){
      FifoPointer = 0;
   }
} // FifoPut
