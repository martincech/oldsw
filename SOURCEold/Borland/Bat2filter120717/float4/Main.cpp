//******************************************************************************
//
//   Main.cpp      Bat2 filter demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Data.h"
#include "Filter.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define TOTAL_SAMPLES 2000

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Normal
//******************************************************************************

#define STEP_UP_VALUE    0.1
#define STEP_DOWN_VALUE -0.1
#define STABLE_VALUE     0.12

void __fastcall TMainForm::BtnRedrawClick(TObject *Sender)
{
   // read filter data :
   DataRecord.AveragingWindow = EdtAveraging->Text.ToInt();
   if( DataRecord.AveragingWindow > FILTER_MAX_AVERAGING){
      // value out of limit - set limit
      DataRecord.AveragingWindow = FILTER_MAX_AVERAGING;
      EdtAveraging->Text = AnsiString( DataRecord.AveragingWindow);
   }
   if( DataRecord.AveragingWindow < 1){
      // value out of limit - set limit
      DataRecord.AveragingWindow = 1;
      EdtAveraging->Text = AnsiString( DataRecord.AveragingWindow);
   }

   TWeight StepRange;
   try {
     StepRange = EdtStepRange->Text.ToDouble();
   } catch( ...){
     Application->MessageBox( "Invalid Step Range format", "Error", MB_OK);
     return;
   }
   DataRecord.StepRange = StepRange;

   TWeight StableRange;
   try {
     StableRange = EdtStableRange->Text.ToDouble();
   } catch( ...){
     Application->MessageBox( "Invalid Stable Range format", "Error", MB_OK);
     return;
   }
   DataRecord.StableRange = StableRange;

   DataRecord.StableWindow = EdtStable->Text.ToInt();
   if( DataRecord.StableWindow < 1){
      DataRecord.StableWindow = 1;
      EdtStable->Text = AnsiString( DataRecord.StableWindow);
   }

   DataRecord.OnStepDown = CbxStepDown->Checked;
   DataRecord.OnStepUp   = CbxStepUp->Checked;
   // initialize filter :
   FilterInitialize();

   // Clear old graph :
   WeightSeries->Clear();
   LowPassSeries->Clear();
   HighPassSeries->Clear();
   StepSeries->Clear();
   StableSeries->Clear();
   StableWeightSeries->Clear();
   // Read display window :
   int WindowWidth = EdtWidth->Text.ToInt();
   int WindowOffset = EdtOffset->Text.ToInt();
   // scroll at offset :
   BatData->Begin();
   for( int i = 0; i < WindowOffset; i++){
      if( !BatData->Next()){
         return;      // end of database
      }
   }
   // draw graph :
   int StartIndex = WindowOffset;
   TWeight StableWeight = 0;
   while(1){
      NextSample();          // process filter
      WeightSeries->AddXY( StartIndex,
                           DataRecord.RawWeight,
                            "", clTeeColor);
      LowPassSeries->AddXY( StartIndex,
                           DataRecord.LowPass,
                            "", clTeeColor);
      HighPassSeries->AddXY( StartIndex,
                           DataRecord.HighPass,
                            "", clTeeColor);
      StepSeries->AddXY( StartIndex,
                         DataRecord.Status == FILTER_STEP_UP   ? STEP_UP_VALUE :
                        (DataRecord.Status == FILTER_STEP_DOWN ? STEP_DOWN_VALUE : 0),
                            "", clTeeColor);
      StableSeries->AddXY( StartIndex,
                           DataRecord.Status == FILTER_STABLE ? STABLE_VALUE : 0,
                            "", clTeeColor);
      StableWeightSeries->AddXY( StartIndex,
                           DataRecord.Status == FILTER_STABLE ? DataRecord.Weight : StableWeight,
                            "", clTeeColor);
      if( DataRecord.Status == FILTER_STABLE){
         StableWeight = DataRecord.Weight;
      }
      StartIndex++;
      if( StartIndex >= (WindowWidth + WindowOffset)){
         return;
      }
      if( BatData->Eof()){
         return;
      }
   }
} // BtnRedrawClick

//******************************************************************************
// Backward
//******************************************************************************

void __fastcall TMainForm::BtnBackwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset -= WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnBackwardClick

//******************************************************************************
// Forward
//******************************************************************************

void __fastcall TMainForm::BtnForwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset += WindowWidth;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnForwardClick

//******************************************************************************
// First
//******************************************************************************

void __fastcall TMainForm::BtnFirstClick(TObject *Sender)
{
   int WindowOffset = 0;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnFirstClick

//******************************************************************************
// File
//******************************************************************************

void __fastcall TMainForm::BtnFileClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   LblFileName->Caption = FileOpenDialog->FileName;
   BatData->SetFile( FileOpenDialog->FileName);
} // BtnFileClick

