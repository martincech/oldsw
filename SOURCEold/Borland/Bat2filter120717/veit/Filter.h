//******************************************************************************
//
//   Filter.h     Filtering
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __Filter_H__
#define __Filter_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Filter status :
typedef enum {
   FILTER_STOP,        // weighing disabled
   FILTER_WAIT,        // wait for step
   FILTER_STEP_UP,     // step up
   FILTER_STEP_DOWN,   // step down
   FILTER_STABLE,      // weighing done
   _FILTER_LAST
} TFilterStatus;

// Status mask :
#define FILTER_STATUS_STATUS       0x07
#define FILTER_STATUS_ON_STEP_UP   0x40
#define FILTER_STATUS_ON_STEP_DOWN 0x80

#define GetFilterStatus()      (FilterRecord.Status &   FILTER_STATUS_STATUS)
#define SetFilterStatus( New)   FilterRecord.Status &= ~FILTER_STATUS_STATUS;\
                                FilterRecord.Status |=  (New)

#define SetFilterOnStepUp()     FilterRecord.Status |=  FILTER_STATUS_ON_STEP_UP
#define ClrFilterOnStepUp()     FilterRecord.Status &= ~FILTER_STATUS_ON_STEP_UP
#define IsFilterOnStepUp()     (FilterRecord.Status &   FILTER_STATUS_ON_STEP_UP)

#define SetFilterOnStepDown()   FilterRecord.Status |=  FILTER_STATUS_ON_STEP_DOWN
#define ClrFilterOnStepDown()   FilterRecord.Status &= ~FILTER_STATUS_ON_STEP_DOWN
#define IsFilterOnStepDown()   (FilterRecord.Status &   FILTER_STATUS_ON_STEP_DOWN)

// Data record :
typedef struct {
   byte          Status;           // filter status
   TSamplesCount AveragingWindow;  // moving average window
   TSamplesCount StableWindow;     // stability windows
#ifdef FILTER_VISIBLE
   TRawWeight    LowPass;          // low pass filter value
   TRawWeight    HighPass;         // high pass filter value
#endif
   TRawWeight    RawWeight;        // filter input
   TRawWeight    ZeroWeight;       // zero weight - fifo initials
   TRawWeight    StepRange;        // greater value is step
   TRawWeight    StableRange;      // less value is stable
   TRawWeight    LastStableWeight; // last stabilised value
   TRawWeight    Weight;           // stable value
} TFilterRecord;

// Global data :
extern TFilterRecord __xdata__ FilterRecord;

//******************************************************************************
// Functions
//******************************************************************************

void FilterStop( void);
// Stop filtering

void FilterStart( void);
// Initialize & start filtering

void FilterNextSample( TRawWeight Sample);
// Process next sample

#endif
