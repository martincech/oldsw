//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat2filter.res");
USEFORM("Main.cpp", MainForm);
USEFORM("Data.cpp", BatData); /* TDataModule: File Type */
USEUNIT("Filter.cpp");
USEUNIT("..\Library\Bat2\Acceptance.cpp");
USEUNIT("..\Library\Bat2\Histogram.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->CreateForm(__classid(TBatData), &BatData);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
