//******************************************************************************
//
//   Filter.h     Filtering
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef FilterH
#define FilterH

// Data limits :
#define FILTER_MAX_AVERAGING 100   // max. width of averaging window

// Basic data types :
typedef double TWeight;            // weight data
typedef double TLongWeight;        // weight sum
typedef int    TSamplesCount;      // samples counter

// Filter status :
typedef enum {
   FILTER_IDLE,
   FILTER_STEP_UP,
   FILTER_STEP_DOWN,
   FILTER_STABLE,
   _FILTER_LAST
} TFilterStatus;

// Data record :
typedef struct {
   TFilterStatus Status;           // filter status
   TWeight       RawWeight;        // filter input
   TWeight       LowPass;          // low pass output
   TWeight       HighPass;         // high pass output
   TSamplesCount AveragingWindow;  // moving average window
   TSamplesCount StableWindow;     // stability windows
   TWeight       StepRange;        // greater value is step
   TWeight       StableRange;      // less value is stable
   TWeight       Weight;           // stable value
} TDataRecord;

// Global data :
extern TDataRecord DataRecord;

//******************************************************************************
// Functions
//******************************************************************************

void FilterInitialize( void);
// Initialize filter data

void NextSample( void);
// Process next sample

#endif
