object MainForm: TMainForm
  Left = 200
  Top = 111
  Width = 931
  Height = 803
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 656
    Top = 56
    Width = 48
    Height = 13
    Caption = 'Averaging'
  end
  object Label1: TLabel
    Left = 856
    Top = 56
    Width = 40
    Height = 13
    Caption = 'Samples'
  end
  object Label2: TLabel
    Left = 80
    Top = 24
    Width = 34
    Height = 13
    Caption = 'Display'
  end
  object Label4: TLabel
    Left = 24
    Top = 64
    Width = 28
    Height = 13
    Caption = 'Width'
  end
  object Label5: TLabel
    Left = 24
    Top = 112
    Width = 28
    Height = 13
    Caption = 'Offset'
  end
  object Label6: TLabel
    Left = 632
    Top = 96
    Width = 72
    Height = 13
    Caption = 'Step Up/Down'
  end
  object Label7: TLabel
    Left = 864
    Top = 96
    Width = 12
    Height = 13
    Caption = 'kg'
  end
  object Label8: TLabel
    Left = 648
    Top = 128
    Width = 60
    Height = 13
    Caption = 'Stable range'
  end
  object Label9: TLabel
    Left = 864
    Top = 128
    Width = 12
    Height = 13
    Caption = 'kg'
  end
  object Label10: TLabel
    Left = 680
    Top = 168
    Width = 30
    Height = 13
    Caption = 'Stable'
  end
  object Label11: TLabel
    Left = 856
    Top = 168
    Width = 40
    Height = 13
    Caption = 'Samples'
  end
  object Graph: TDBChart
    Left = 16
    Top = 264
    Width = 873
    Height = 497
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.DateTimeFormat = 'h:mm'
    Legend.Visible = False
    View3D = False
    TabOrder = 0
    object WeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HMOTNOST'
    end
    object LowPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'LO_PASS'
    end
    object HighPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HI_PASS'
    end
    object StepSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlue
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clLime
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableWeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clWhite
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object EdtAveraging: TEdit
    Left = 720
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '20'
  end
  object BtnRedraw: TButton
    Left = 80
    Top = 152
    Width = 75
    Height = 25
    Caption = 'Redraw'
    TabOrder = 2
    OnClick = BtnRedrawClick
  end
  object EdtWidth: TEdit
    Left = 64
    Top = 56
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '2000'
  end
  object EdtOffset: TEdit
    Left = 64
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '0'
  end
  object BtnBackward: TButton
    Left = 96
    Top = 200
    Width = 75
    Height = 25
    Caption = '<<<'
    TabOrder = 5
    OnClick = BtnBackwardClick
  end
  object BtnForward: TButton
    Left = 176
    Top = 200
    Width = 75
    Height = 25
    Caption = '>>>'
    TabOrder = 6
    OnClick = BtnForwardClick
  end
  object BtnFirst: TButton
    Left = 16
    Top = 200
    Width = 75
    Height = 25
    Caption = '|<<<'
    TabOrder = 7
    OnClick = BtnFirstClick
  end
  object EdtStepRange: TEdit
    Left = 720
    Top = 88
    Width = 121
    Height = 21
    TabOrder = 8
    Text = '0,05'
  end
  object EdtStableRange: TEdit
    Left = 720
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 9
    Text = '0,01'
  end
  object EdtStable: TEdit
    Left = 720
    Top = 160
    Width = 121
    Height = 21
    TabOrder = 10
    Text = '10'
  end
end
