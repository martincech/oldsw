//******************************************************************************
//
//   Rdb.h        Realtime database
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef RdbH
#define RdbH

typedef struct {
   int StatusOk;        // nonzero if communication OK
   int PositionOk;      // nonzeor if position valid
   // coordinates
   int Latitude;
   int LatitudeNord;
   int Longitude;
   int LongitudeWest;
   int Speed;
   int Course;
   // clock
   int Hour;
   int Min;
   int Sec;
   int Day;
   int Month;
   int Year;
} TRdb;

extern TRdb Rdb;

#endif
