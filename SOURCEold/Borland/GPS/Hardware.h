//******************************************************************************
//
//   Hardware.h   Hardware definitions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "../Library/Unisys/Uni.h"
#include "Rdb.h"

//******************************************************************************
// GPS
//******************************************************************************

#define GPS_CRC       1         // conditional compilation - CRC check
#define GPS_SPEED_KMH 1         // show speed as km/h

#define GpsSetStatus( s)         Rdb.StatusOk = (s)
#define GpsSetPosition( s)       Rdb.PositionOk = (s)

#define GpsSetHour( h)           Rdb.Hour = (h)
#define GpsSetMin( m)            Rdb.Min = (m)
#define GpsSetSec( s)            Rdb.Sec = (s)
#define GpsSetDay( d)            Rdb.Day = (d)
#define GpsSetMonth( m)          Rdb.Month = (m)
#define GpsSetYear( y)           Rdb.Year  = (y) + 2000

#define GpsSetLatitude(  l)      Rdb.Latitude  = (l)
#define GpsSetLongitude( l)      Rdb.Longitude = (l)
#define GpsSetLatitudeNord( n)   Rdb.LatitudeNord  = (n)
#define GpsSetLongitudeWest( w)  Rdb.LongitudeWest = (w)
#define GpsSetSpeed( s)          Rdb.Speed  = (s)
#define GpsSetCourse( c)         Rdb.Course = (c)

#endif

