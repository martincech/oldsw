//******************************************************************************
//
//   Main.cpp      GPS demo main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Rdb.h"
#include "Com.h"
#include "Gps.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";
#define StatusQ()    Status() + "?";StatusBar->Refresh()

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
}

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   CbCom->ItemIndex = 0;
   CbComChange( 0);
}

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TMainForm::TimerTimer(TObject *Sender)
{
   StatusQ();
   if( !ComReceive()){
      LblStatus->Caption = "TIMEOUT";
      Status() + "COM unable read";
      return;
   }
/*
   byte *Buffer = ComGetBuffer();
   Buffer[ ComGetSize()] = '\0';          // terminate string
   LblStatus->Caption = AnsiString( (char *)Buffer);
   return;
*/
   if( !GpsParse()){
      LblStatus->Caption = "ERROR";
      Status() + "GPS unable parse info";
      return;
   }

   //--- redraw GPS data : -----------------------------------------------------
   if( Rdb.StatusOk){
      LblStatus->Caption = "OK";
   } else {
      LblStatus->Caption = "ERROR";
   }

   if( Rdb.PositionOk){
      LblPosition->Caption = "OK";
   } else {
      LblPosition->Caption = "INVALID";
   }

   LblLatitude->Caption = AnsiString( Rdb.Latitude);
   if( Rdb.LatitudeNord){
      LblLatitudeIndicator->Caption = "N";
   } else {
      LblLatitudeIndicator->Caption = "S";
   }

   LblLongitude->Caption = AnsiString( Rdb.Longitude);
   if( Rdb.LongitudeWest){
      LblLongitudeIndicator->Caption = "W";
   } else {
      LblLongitudeIndicator->Caption = "E";
   }

   LblSpeed->Caption  = AnsiString( Rdb.Speed)  + " km/h";
   LblCourse->Caption = AnsiString( Rdb.Course) + " �";

   AnsiString Tmp;
   Tmp.printf( "%02d.%02d.%04d %02d:%02d:%02d", Rdb.Day,  Rdb.Month, Rdb.Year,
                                                Rdb.Hour, Rdb.Min,   Rdb.Sec);
   LblClock->Caption = Tmp;
}

//******************************************************************************
// Com change
//******************************************************************************

void __fastcall TMainForm::CbComChange(TObject *Sender)
{
   ComClose();
   if( !ComOpen( CbCom->Text.c_str())){
      Status() + "Unable open COM";
      return;
   }
   if( !ComStart()){
      Status() + "Unable start COM";
      return;
   }   
}

