//******************************************************************************
//
//   GpsSetup.h   GPS setup functions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef GpsSetupH
#define GpsSetupH

TYesNo GpsSetBaud( int BaudRate);
// Set GPS baud

TYesNo GpsSetRate( void);
// Set GPS rate

#endif
