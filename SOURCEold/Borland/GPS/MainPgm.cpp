//******************************************************************************
//
//   Main.cpp      GPS demo main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainPgm.h"
#include "Rdb.h"
#include "Com.h"
#include "GpsSetup.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";
#define StatusQ()    Status() + "?";StatusBar->Refresh()

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
}

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   CbCom->ItemIndex  = 0;
   CbBaud->ItemIndex = 3;
   CbComChange( 0);
}

//******************************************************************************
// Go
//******************************************************************************

void __fastcall TMainForm::BtnGoClick(TObject *Sender)
{
   int BaudRate = CbBaud->Text.ToInt();
   ComSetBaud( BaudRate);
   if( !GpsSetBaud( 9600)){
      Status() + "Unable set Baud rate";
      return;
   }
   if( !GpsSetRate()){
      Status() + "Unable set GPS rate";
      return;
   }
}

//******************************************************************************
// COM change
//******************************************************************************

void __fastcall TMainForm::CbComChange(TObject *Sender)
{
   ComClose();
   if( !ComOpen( CbCom->Text.c_str())){
      Status() + "Unable open COM";
      return;
   }
}

