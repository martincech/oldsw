//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("GpsPgm.res");
USEFORM("MainPgm.cpp", MainForm);
USEUNIT("Com.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("GpsSetup.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
