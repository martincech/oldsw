//******************************************************************************
//
//   Gps.c        GPS information parser
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   #include <vcl.h>
   #pragma hdrstop

   #include "Com.h"
   #include "Gps.h"
#endif

// RMC sentence :
#define RMC_MIN_SIZE      9
#define RMC_HEADER        "$GPRMC,"
#define RMC_HEADER_SIZE  (sizeof( RMC_HEADER) - 1)

#define KNOT_TO_KMH      1852          // knot to km/h coefficient

// Macros :
#ifdef __WIN32__
   #define char2dec( ch)  ((ch) - '0')
   #define char2hex( ch)  ((ch) <= '9' ? (ch) - '0' : (ch) - 'A' + 10)
#endif

// Local functions :

static TYesNo ParseTime( byte *Pointer, int *Index);
// Parse time at <Pointer>, update <Index>

static TYesNo ParseStatus( byte *Pointer, int *Index, byte *Status);
// Parse status

static TYesNo ParseNumber( byte *Pointer, int *Index, int *Number, int Decimals);
// Parse number

static TYesNo ParseDate( byte *Pointer, int *Index);
// Parse date

static TYesNo IsNumber( byte Ch);
// Returns YES on <Ch> numberic

#ifdef GPS_CRC
   static TYesNo MessageCrc( byte *Buffer, int Size);
   // Check for message CRC
#endif // GPS_CRC

//******************************************************************************
// Parse
//******************************************************************************

TYesNo GpsParse( void)
// Parse GPS informations
{
byte *Buffer;
byte  Status;
int   Size;
int   Index;
int   Value;

   // get COM data :
   Buffer = ComGetBuffer();
   Size   = ComGetSize();
   GpsSetStatus( NO);                  // invalid status
   if( Size < RMC_MIN_SIZE){
      return( NO);                     // too short data
   }
#ifdef GPS_CRC
   // check for message CRC :
   if( !MessageCrc( Buffer, Size)){
      return( NO);                     // CRC error
   }
#endif // GPS_CRC
   // check for message header :
   if( !strnequ( RMC_HEADER, Buffer, RMC_HEADER_SIZE)){
      return( NO);                     // not RMC command
   }
   Index = RMC_HEADER_SIZE;
   // time :
   if( !ParseTime( &Buffer[ Index], &Index)){
      return( NO);
   }
   // position status :
   if( !ParseStatus( &Buffer[ Index], &Index, &Status)){
      return( NO);
   }
   if( Status == 'A'){
      GpsSetPosition( YES);            // valid position
   } else {
      GpsSetPosition( NO);             // invalid position
   }
   // latitude :
   if( ParseNumber( &Buffer[ Index], &Index, &Value, 4)){
      GpsSetLatitude( Value);
   }
   // latitude indicator :
   if( ParseStatus( &Buffer[ Index], &Index, &Status)){
      if( Status == 'N'){
         GpsSetLatitudeNord( YES);        // nord
      } else {
         GpsSetLatitudeNord( NO);         // south
      }
   }
   // longitude :
   if( ParseNumber( &Buffer[ Index], &Index, &Value, 4)){
      GpsSetLongitude( Value);
   }
   // longitude indicator :
   if( ParseStatus( &Buffer[ Index], &Index, &Status)){
      if( Status == 'W'){
         GpsSetLongitudeWest( YES);       // west
      } else {
         GpsSetLongitudeWest( NO);        // east
      }
   }
   // speed :
   if( ParseNumber( &Buffer[ Index], &Index, &Value, 2)){
#ifdef GPS_SPEED_KMH
      GpsSetSpeed( (Value * KNOT_TO_KMH) / 1000);
#else
      GpsSetSpeed( Value);             // knot
#endif
   }
   // course :
   if( ParseNumber( &Buffer[ Index], &Index, &Value, 2)){
      GpsSetCourse( Value);
   }
   // date :
   if( !ParseDate( &Buffer[ Index], &Index)){
      return( NO);
   }
   GpsSetStatus( YES);
   return( YES);
} // GpsParse

//------------------------------------------------------------------------------

//******************************************************************************
// Time
//******************************************************************************

static TYesNo ParseTime( byte *Pointer, int *Index)
// Parse time at <Pointer>, upadate <Index>
{
byte Value;

   if( *Pointer == ','){
      (*Index)++;                      // move at next character
      return( NO);                     // empty field
   }
   // decode hour :
   Value  = char2dec( *Pointer++) * 10;
   Value += char2dec( *Pointer++);
   GpsSetHour( Value);
   // decode min :
   Value  = char2dec( *Pointer++) * 10;
   Value += char2dec( *Pointer++);
   GpsSetMin( Value);
   // decode sec :
   Value  = char2dec( *Pointer++) * 10;
   Value += char2dec( *Pointer++);
   GpsSetSec( Value);
   (*Index) += 11;                     // field size
   return( YES);
} // ParseTime

//******************************************************************************
// Status
//******************************************************************************

static TYesNo ParseStatus( byte *Pointer, int *Index, byte *Status)
// Parse status
{
   if( *Pointer == ','){
      (*Index)++;                      // move at next character
      return( NO);                     // empty field
   }
   *Status   = *Pointer;
   (*Index) += 2;
   return( YES);
} // ParseStatus

//******************************************************************************
// Number
//******************************************************************************

static TYesNo ParseNumber( byte *Pointer, int *Index, int *Number, int Decimals)
// Parse number
{
int Value;
int Width;
int i;

   if( *Pointer == ','){
      (*Index)++;                      // move at next character
      return( NO);                     // empty field
   }
   Value = 0;
   Width = 0;                          // field width
   while( IsNumber( *Pointer)){
      Value *= 10;
      Value += char2dec( *Pointer);
      Pointer++;
      Width++;
   }
   if( *Pointer == '.'){
      // read decimals
      Pointer++;                       // skip dot
      Width++;                         // dot width
      i = Decimals;
      while( i--){
         Value *= 10;
         if( !IsNumber( *Pointer)){
            continue;
         }
         Value += char2dec( *Pointer);
         Pointer++;
         Width++;
      };
   } else if( Decimals > 0){
      // decimal part requesterd but not present, shift Value only
      i = Decimals;
      do {
         Value *= 10;
      } while( --i);
   }
   // skip rest of decimal numbers
   while( IsNumber( *Pointer)){
      Pointer++;
      Width++;
   }
   *Number  = Value;
   *Index  += Width + 1;
   return( YES);
} // ParseNumber

//******************************************************************************
// Date
//******************************************************************************

static TYesNo ParseDate( byte *Pointer, int *Index)
// Parse date
{
byte Value;

   if( *Pointer == ','){
      (*Index)++;                      // move at next character
      return( NO);                     // empty field
   }
   // decode day :
   Value  = char2dec( *Pointer++) * 10;
   Value += char2dec( *Pointer++);
   GpsSetDay( Value);
   // decode month :
   Value  = char2dec( *Pointer++) * 10;
   Value += char2dec( *Pointer++);
   GpsSetMonth( Value);
   // decode year :
   Value  = char2dec( *Pointer++) * 10;
   Value += char2dec( *Pointer++);
   GpsSetYear( Value);
   (*Index) += 7;                      // field size
   return( YES);
} // ParseDate

//******************************************************************************
// Is Number
//******************************************************************************

static TYesNo IsNumber( byte Ch)
// Returns YES on <Ch> numberic
{
   return( (Ch >= '0') && (Ch <= '9'));
} // IsNumber

#ifdef GPS_CRC
//******************************************************************************
// Message CRC
//******************************************************************************

static TYesNo MessageCrc( byte *Buffer, int Size)
// Check for message CRC
{
byte Checksum;
byte MsgChecksum;
byte *p;
int  i;

   // check message frame :
   if( Buffer[ 0] != '$'){
      return( NO);
   }
   if( Buffer[ Size - 1] != '\n'){
      return( NO);
   }
   if( Buffer[ Size - 2] != '\r'){
      return( NO);
   }
   if( Buffer[ Size - 5] != '*'){
      return( NO);
   }
   // compute message crc :
   Checksum = 0;
   p = Buffer + 1;                     // skip leader char
   i = Size - 6;                       // without leader and trailer
   do {
      Checksum ^= *p;
      p++;
   } while( --i);
   // read message crc :
   MsgChecksum   = char2hex( Buffer[ Size - 4]);
   MsgChecksum <<= 4;
   MsgChecksum  |= char2hex( Buffer[ Size - 3]);
   // compare :
   if( MsgChecksum != Checksum){
      return( NO);
   }
   return( YES);
} // MessageCrc
#endif // GPS_CRC

