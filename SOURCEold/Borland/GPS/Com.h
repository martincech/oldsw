//******************************************************************************
//
//   Com.h        COM driver
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef ComH
#define ComH

#ifndef __Uni_H__
   #include "../Library/Unisys/Uni.h"
#endif

TYesNo ComOpen( char *ComName);
// Open <ComName> port

void ComSetBaud( int BaudRate);
// Set communication speed

TYesNo ComStart( void);
// Start communication

TYesNo ComReceive( void);
// Receive data

TYesNo ComSend( void *Buffer, int Size);
// Send data

byte *ComGetBuffer( void);
// Returns buffer

int ComGetSize( void);
// Returns size of the message

void ComClose( void);
// Close active port

#endif
