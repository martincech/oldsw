object MainForm: TMainForm
  Left = 542
  Top = 272
  Width = 415
  Height = 311
  Caption = 'GPS programmer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 192
    Top = 24
    Width = 25
    Height = 13
    Caption = 'Port :'
  end
  object Label2: TLabel
    Left = 160
    Top = 68
    Width = 78
    Height = 13
    Caption = 'Initial baud rate :'
  end
  object CbCom: TComboBox
    Left = 248
    Top = 16
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = CbComChange
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10')
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 265
    Width = 407
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object BtnGo: TButton
    Left = 16
    Top = 152
    Width = 75
    Height = 25
    Caption = 'Go !'
    TabOrder = 2
    OnClick = BtnGoClick
  end
  object CbBaud: TComboBox
    Left = 248
    Top = 64
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    Items.Strings = (
      '2400'
      '4800'
      '9600'
      '19200'
      '38400'
      '57600'
      '115200')
  end
end
