object MainForm: TMainForm
  Left = 447
  Top = 436
  Width = 415
  Height = 311
  Caption = 'GPS demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 152
    Width = 44
    Height = 13
    Caption = 'Latitude :'
  end
  object LblLatitude: TLabel
    Left = 80
    Top = 152
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblLatitudeIndicator: TLabel
    Left = 160
    Top = 152
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label2: TLabel
    Left = 24
    Top = 176
    Width = 53
    Height = 13
    Caption = 'Longitude :'
  end
  object LblLongitude: TLabel
    Left = 80
    Top = 176
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblLongitudeIndicator: TLabel
    Left = 160
    Top = 176
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label3: TLabel
    Left = 24
    Top = 208
    Width = 37
    Height = 13
    Caption = 'Speed :'
  end
  object LblSpeed: TLabel
    Left = 81
    Top = 209
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label4: TLabel
    Left = 24
    Top = 232
    Width = 39
    Height = 13
    Caption = 'Course :'
  end
  object LblCourse: TLabel
    Left = 81
    Top = 233
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblClock: TLabel
    Left = 80
    Top = 104
    Width = 12
    Height = 13
    Caption = '??'
  end
  object Label5: TLabel
    Left = 24
    Top = 40
    Width = 36
    Height = 13
    Caption = 'Status :'
  end
  object LblStatus: TLabel
    Left = 80
    Top = 40
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label6: TLabel
    Left = 24
    Top = 104
    Width = 33
    Height = 13
    Caption = 'Clock :'
  end
  object Label7: TLabel
    Left = 24
    Top = 64
    Width = 43
    Height = 13
    Caption = 'Position :'
  end
  object LblPosition: TLabel
    Left = 80
    Top = 64
    Width = 6
    Height = 13
    Caption = '?'
  end
  object CbCom: TComboBox
    Left = 248
    Top = 16
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = CbComChange
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10')
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 265
    Width = 407
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 256
    Top = 48
  end
end
