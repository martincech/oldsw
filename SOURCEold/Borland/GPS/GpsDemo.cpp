//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("GpsDemo.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Rdb.cpp");
USEUNIT("Com.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("Gps.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
