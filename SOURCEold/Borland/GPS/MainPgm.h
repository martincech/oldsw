//---------------------------------------------------------------------------

#ifndef MainPgmH
#define MainPgmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TComboBox *CbCom;
   TStatusBar *StatusBar;
   TButton *BtnGo;
   TLabel *Label1;
   TComboBox *CbBaud;
   TLabel *Label2;
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall CbComChange(TObject *Sender);
   void __fastcall BtnGoClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
