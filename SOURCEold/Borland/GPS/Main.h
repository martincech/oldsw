//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TComboBox *CbCom;
   TLabel *Label1;
   TLabel *LblLatitude;
   TLabel *LblLatitudeIndicator;
   TLabel *Label2;
   TLabel *LblLongitude;
   TLabel *LblLongitudeIndicator;
   TLabel *Label3;
   TLabel *LblSpeed;
   TLabel *Label4;
   TLabel *LblCourse;
   TLabel *LblClock;
   TLabel *Label5;
   TLabel *LblStatus;
   TLabel *Label6;
   TTimer *Timer;
   TLabel *Label7;
   TLabel *LblPosition;
   TStatusBar *StatusBar;
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall TimerTimer(TObject *Sender);
   void __fastcall CbComChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
