//******************************************************************************
//
//   Com.cpp      COM driver
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Com.h"

#include "../Library/Serial/Uart/ComUart.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define MAX_BUFFER_SIZE 128
#define RX_TIMEOUT      200

static TComUart *FCom = 0;
static byte FComBuffer[ MAX_BUFFER_SIZE];
static int  FSize;

static TYesNo WaitForChar( byte ch);
// Wait for <ch>

//******************************************************************************
// Open
//******************************************************************************

TYesNo ComOpen( char *ComName)
// Open <ComName> port
{
   ComClose();
   TIdentifier Identifier;
   // COM setup
   FCom = new TComUart;
   if( !FCom->Locate( ComName, Identifier)){
      delete FCom;
      FCom = 0;
      return( NO);
   }
   if( !FCom->Open( Identifier)){
      delete FCom;
      FCom = 0;
      return( NO);
   }
   ComSetBaud( 9600);
   return( YES);
} // ComOpen

//******************************************************************************
// Set Baud
//******************************************************************************

void ComSetBaud( int BaudRate)
// Set communication speed
{
   if( !FCom){
      return;
   }
   // set parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate     = BaudRate;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   FCom->SetParameters( Parameters);
   FCom->SetRxNowait();
   // common init :
   FCom->Flush();
} // ComSetBaud

//******************************************************************************
// Start
//******************************************************************************

TYesNo ComStart( void)
// Start communication
{
   if( !FCom){
      return( NO);
   }
   for( int i = 0; i < 3; i++){
      if( WaitForChar( '\n')){
         return( YES);
      }
   }
   return( NO);
} // ComStart

//******************************************************************************
// Receive
//******************************************************************************

TYesNo ComReceive( void)
// Receive data
{
   if( !FCom){
      return( NO);
   }
   if( !WaitForChar( '$')){
      return( NO);
   }
   FComBuffer[ 0] = '$';
   FCom->SetRxWait( RX_TIMEOUT, 0);
   FSize = FCom->Read( FComBuffer + 1, MAX_BUFFER_SIZE - 2);
   if( FSize == 0){
      return( NO);
   }
   FSize++;                            // add leader char
   return( YES);
} // ComReceive

//******************************************************************************
// Send
//******************************************************************************

TYesNo ComSend( void *Buffer, int Size)
// Send data
{
   if( FCom->Write( Buffer, Size) != Size){
      return( NO);
   }
   return( YES);
} // ComSend

//******************************************************************************
// Get buffer
//******************************************************************************

byte *ComGetBuffer( void)
// Returns buffer
{
   return( FComBuffer);
} // ComGetBuffer

//******************************************************************************
// Get size
//******************************************************************************

int ComGetSize( void)
// Returns size of the message
{
   return( FSize);
} // ComGetSize

//******************************************************************************
// Close
//******************************************************************************

void ComClose( void)
// Close active port
{
   if( FCom){
      FCom->Close();
      delete FCom;
      FCom = 0;
   }
} // ComClose

//******************************************************************************
// Wait for char
//******************************************************************************

static TYesNo WaitForChar( byte ch)
// Wait for <ch>
{
   byte Buffer;
   if( !FCom){
      return( NO);
   }
   // try wait for LF :
   FCom->SetRxNowait();
   FCom->Flush();
   for( int i = 0; i < 10; i++){
      if( FCom->Read( &Buffer, 1) != 1){
         Sleep( 100);
         continue;
      }
      if( Buffer != ch){
         continue;
      }
      return( YES);
   }
   return( NO);
} // WaitForChar
