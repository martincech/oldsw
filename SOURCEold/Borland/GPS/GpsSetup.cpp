//******************************************************************************
//
//   GpsSetup.cpp  GPS setup functions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   #include <vcl.h>
   #pragma hdrstop

   #include "Com.h"
#endif

#include "GpsSetup.h"
#include <stdio.h>

// Message identification :
typedef enum {
   MSG_GGA,
   MSG_GLL,
   MSG_GSA,
   MSG_GSV,
   MSG_RMC,
   MSG_VTG,
   MSG_MSS,
   MSG_UNDEFINED1,
   MSG_ZDA,
   MSG_UNDEFINED2
} TMessageIdentification;

#define GPS_MAX_BUFFER 127

static byte Buffer[ GPS_MAX_BUFFER + 1];

// Macros :
#define nibble2hex( n)   ((n) > 9 ? 'A' + (n) - 10 : '0' + (n))

// Local functions :

static TYesNo RateControl( int Message, int Rate);
// Set <Rate> at <Message>

static TYesNo SendFrame( byte *Buffer, int Size);
// Send <Buffer> append CRC

//******************************************************************************
// Set Baud
//******************************************************************************

TYesNo GpsSetBaud( int BaudRate)
// Set GPS baud
{
   sprintf( Buffer, "$PSRF100,1,%d,8,1,0", BaudRate);
   if( !SendFrame( Buffer, strlen( Buffer))){
      return( NO);
   }
   Sleep( 500);
   ComSetBaud( BaudRate);
   return( YES);
} // GpsSetBaud

//******************************************************************************
// Set Rate
//******************************************************************************

TYesNo GpsSetRate( void)
// Set GPS rate
{
   if( !RateControl( MSG_GGA, 0)){
      return( NO);
   }
   if( !RateControl( MSG_GLL, 0)){
      return( NO);
   }
   if( !RateControl( MSG_GSA, 0)){
      return( NO);
   }
   if( !RateControl( MSG_GSV, 0)){
      return( NO);
   }
   if( !RateControl( MSG_RMC, 1)){
      return( NO);
   }
   if( !RateControl( MSG_VTG, 0)){
      return( NO);
   }
   if( !RateControl( MSG_MSS, 0)){
      return( NO);
   }
   return( YES);
} // GpsSetRate

//------------------------------------------------------------------------------

//******************************************************************************
// Rate control
//******************************************************************************

static TYesNo RateControl( int Message, int Rate)
// Set <Rate> at <Message>
{
   sprintf( Buffer, "$PSRF103,%02d,00,%02d,01", Message, Rate);
   if( !SendFrame( Buffer, strlen( Buffer))){
      return( NO);
   }
   return( YES);
} // RateControl

//******************************************************************************
// Send frame
//******************************************************************************

static TYesNo SendFrame( byte *Buffer, int Size)
// Send <Buffer> append CRC
{
int   i;
byte *p;
byte Checksum;

   i = Size - 1;                       // skip leader character
   p = (byte *)Buffer + 1;
   Checksum = 0;
   do {
      Checksum ^= *p;
      p++;
   } while( --i);
   Buffer[ Size++] = '*';
   Buffer[ Size++] = nibble2hex( Checksum >> 4);
   Buffer[ Size++] = nibble2hex( Checksum & 0x0F);
   Buffer[ Size++] = '\r';
   Buffer[ Size++] = '\n';
   if( !ComSend( Buffer, Size)){
      return( NO);
   }
   return( YES);
} // SendFrame
