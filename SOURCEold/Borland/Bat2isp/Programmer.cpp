//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Programmer.res");
USEFORM("ProgrammerMain.cpp", ProgrammerMainForm);
USEUNIT("PgmWorkplace.cpp");
USEFORM("PgmProgress.cpp", PgmProgressForm);
USEUNIT("Boot.cpp");
USEUNIT("Isp.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("CodeFile.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TProgrammerMainForm), &ProgrammerMainForm);
       Application->CreateForm(__classid(TPgmProgressForm), &PgmProgressForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
