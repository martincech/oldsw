//******************************************************************************
//
//   Isp.cpp      ISP basic commands
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Isp.h"

#pragma package(smart_init)

// Implementacni konstanty :

#define ISP_TRIALS                3   // pocet pokusu pri komunikaci
#define ISP_MAX_DISPLAY       0x400   // max. velikost cteneho (display) bloku

#define VERIFICATION_PAGE_SIZE  256   // velikost stranky pro verifikaci
#define DEFAULT_TIMEOUT         500   // timeout odpovedi pro zakladni prikazy
#define ERASE_TIMEOUT          7000   // timeout odpovedi pro mazani

//******************************************************************************
// Konstruktor
//******************************************************************************

TIsp::TIsp()
// Konstruktor
{
   FDevice = new TBoot;         // komunikacni modul
} // TIsp

//******************************************************************************
// Destruktor
//******************************************************************************

TIsp::~TIsp()
// Destruktor
{
   if( FDevice){
      delete FDevice;
   }
} // ~TIsp

//******************************************************************************
// Port
//******************************************************************************

bool TIsp::SetPort( TString PortName)
// Nastaveni k portu
{
   return( FDevice->Connect( PortName));
} // SetPort

//******************************************************************************
// Kontrola
//******************************************************************************

bool TIsp::CheckDevice()
// Kontrola pripojeni
{
   return( FDevice->Check());
} // CheckDevice

//******************************************************************************
// Programovani Flash
//******************************************************************************

void TIsp::StartProgramCode( int StartAddress, int Length, byte *Code)
// Zahajeni programovani kodu
{
   if( StartAddress % ISP_PAGE_SIZE != 0){
      IERROR;
   }
   if( Length > ISP_CODE_SIZE){
      IERROR;
   }
   if( StartAddress + Length > ISP_CODE_SIZE){
      IERROR;
   }
   // zapamatuj do kontextovych promennych :
   WStartAddress = StartAddress;
   WLength       = Length;
   WRemainder    = Length;
   WData         = Code;
   WIndex        = 0;
} // StartProgramCode

bool TIsp::DoProgramCode( bool &Done)
// Cyklicke volani az po <Done> = true
{
   byte PageSize;
   if( WRemainder > ISP_PAGE_SIZE){
      PageSize = ISP_PAGE_SIZE;
   } else {
      PageSize = (byte)WRemainder;
   }
   if( !WriteCommand( 0x00, (word)WStartAddress, PageSize, &WData[ WIndex])){
      return( false);
   }
   WRemainder -= PageSize;             // zbyvajici delka
   if( WRemainder == 0){
      Done = true;                     // hotovo
      return( true);
   }
   WStartAddress += PageSize;          // posun adresy Flash
   WIndex        += PageSize;          // posun indexu v bufferu dat
   Done           = false;
   return( true);
} // DoProgramCode

//******************************************************************************
// Verifikace flash
//******************************************************************************

void TIsp::StartVerifyCode( int StartAddress, int Length, byte *Code)
// Zahajeni verifikace kodu
{
   if( StartAddress % ISP_PAGE_SIZE != 0){
      IERROR;
   }
   if( Length > ISP_CODE_SIZE){
      IERROR;
   }
   if( StartAddress + Length > ISP_CODE_SIZE){
      IERROR;
   }
   // zapamatuj do kontextovych promennych :
   WStartAddress = StartAddress;
   WLength       = Length;
   WRemainder    = Length;
   WData         = Code;
   WIndex        = 0;
} // StartVerifyCode

bool TIsp::DoVerifyCode( bool &Done)
// Cyklicke volani az po <Done> = true
{
   word PageSize;
   byte PageData[ VERIFICATION_PAGE_SIZE];
   if( WRemainder > VERIFICATION_PAGE_SIZE){
      PageSize = VERIFICATION_PAGE_SIZE;
   } else {
      PageSize = (word)WRemainder;
   }
   // nacteni stranky kodu do pomocneho bufferu :
   if( !DisplayFunction( (word)WStartAddress, PageSize, 0x00, PageData)){
      return( false);
   }
   // komparace dat proti originalu :
   if( !memequ( PageData, &WData[ WIndex], PageSize)){
      return( false);
   }
   WRemainder -= PageSize;             // zbyvajici delka
   if( WRemainder == 0){
      Done = true;                     // hotovo
      return( true);
   }
   WStartAddress += PageSize;          // posun adresy Flash
   WIndex        += PageSize;          // posun indexu v bufferu dat
   Done           = false;
   return( true);
} // DoVerifyCode

//******************************************************************************
// Programovani EEPROM
//******************************************************************************

bool TIsp::ProgramEeprom( word StartAddress, word Length, byte *Code)
// Programovani EEPROM
{
//   WriteCommand( 0x07, StartAddress, Length, Code);
   return( false);
} // ProgramEeprom

//******************************************************************************
// Reset
//******************************************************************************

bool TIsp::Reset()
// Reset procesoru
{
   WriteFunction( 0x03, 0x00);  // ignoruj odpoved - neni korektni
   return( true);
} // Reset

//******************************************************************************
// Mazani cipu
//******************************************************************************

bool TIsp::Erase()
// Mazani procesoru (trva az 6s)
{
   FDevice->RxTimeout = ERASE_TIMEOUT;
   bool Result = WriteFunction( 0x07, 0x00);
   FDevice->RxTimeout = DEFAULT_TIMEOUT;
   return( Result);
} // Erase

//******************************************************************************
// Nastaveni SSB
//******************************************************************************

bool TIsp::SetSSB( TSecurity Value)
// Nastaveni ochrany Software Security Byte
{
   switch( Value){
      case NO_SECURITY :
         // flash - povoleni cteni i zapisu
         return( true);         // implicitni nastaveni po Erase

      case WRITE_SECURITY :
         // flash - zakaz zapisu
         return( WriteFunction( 0x05, 0x00));

      case RD_WR_SECURITY :
         // flash - zakaz zapisu i cteni
         return( WriteFunction( 0x05, 0x01));

      IDEFAULT
   }
} // SetSSB

//******************************************************************************
// Nastaveni BSB
//******************************************************************************

bool TIsp::SetBSB( byte Value)
// Nastaveni registru Boot Status Byte
{
   return( WriteValue( 0x06, 0x00, Value));
} // SetBSB

//******************************************************************************
// Typove cislo
//******************************************************************************

bool TIsp::GetSignature( TSignature &Signature)
// Cteni typoveho cisla
{
byte Value;

   if( !ReadFunction( 0x00, 0x00, Value)){
      return( false);
   }
   Signature.Manufacturer = Value;
   if( !ReadFunction( 0x00, 0x01, Value)){
      return( false);
   }
   Signature.Device1 = Value;
   if( !ReadFunction( 0x00, 0x02, Value)){
      return( false);
   }
   Signature.Device2 = Value;
   if( !ReadFunction( 0x00, 0x03, Value)){
      return( false);
   }
   Signature.Device3 = Value;
   return( true);
} // GetSignature

//******************************************************************************
// Cteni SSB
//******************************************************************************

bool TIsp::GetSSB( byte &Value)
// Cteni Software Security Byte
{
   return( ReadFunction( 0x07, 0x00, Value));
} // GetSSB

//******************************************************************************
// Cteni BSB
//******************************************************************************

bool TIsp::GetBSB( byte &Value)
// Cteni Boot Status Byte
{
   return( ReadFunction( 0x07, 0x01, Value));
} // GetBSB

//******************************************************************************
// Cteni SBV
//******************************************************************************

bool TIsp::GetSBV( byte &Value)
// Cteni Software Boot Vector
{
   return( ReadFunction( 0x07, 0x02, Value));
} // GetSBV

//------------------------------------------------------------------------------

//******************************************************************************
// Zapisova funkce
//******************************************************************************

bool TIsp::WriteFunction( byte Data0, byte Data1)
// Zapis (spusteni) funkce
{
   byte Data[ 2];
   Data[ 0] = Data0;
   Data[ 1] = Data1;
   return( WriteCommand( 0x03, 0, sizeof( Data), &Data));
} // WriteFunction

//******************************************************************************
// Zapisova funkce
//******************************************************************************

bool TIsp::WriteValue( byte Data0, byte Data1, byte Value)
// Zapis hodnoty
{
   byte Data[ 3];
   Data[ 0] = Data0;
   Data[ 1] = Data1;
   Data[ 2] = Value;
   return( WriteCommand( 0x03, 0, sizeof( Data), &Data));
} // WriteFunction

//******************************************************************************
// Prikaz zapisu
//******************************************************************************

bool TIsp::WriteCommand( byte RecordType, word Address, byte Length, void *Data)
// Prikaz pro zapis
{
   // pocet pokusu o spravny prijem :
   for( int i = 0; i < ISP_TRIALS; i++){
      if( !Send( Length, Address, RecordType, Data)){
         return( false);    // hw problem
      }
      if( strnequ( RxBuffer, "P\r\n", 3)){
         return( false);    // security error
      }
      if( !strnequ( RxBuffer, ".\r\n", 3)){
         continue;          // syntaxe odpovedi
      }
      return( true);
   }
   return( false);
} // WriteCommand

//******************************************************************************
// Cteni bloku
//******************************************************************************

#define ISP_ROW_SIZE        16      // pocet bytu v jednom radku

bool TIsp::DisplayFunction( word StartAddress, word Length, byte Type, byte *Data)
// Cteni bloku dat
{
   if( Length > ISP_MAX_DISPLAY){
      IERROR;
   }
   byte Cmd[ 5];
   Cmd[ 0] = StartAddress >> 8;        // start MSB
   Cmd[ 1] = StartAddress & 0xFF;      // start LSB
   word EndAddress = StartAddress + Length - 1;  // cte vcetne koncove adresy
   Cmd[ 2] = EndAddress   >> 8;        // end MSB
   Cmd[ 3] = EndAddress   & 0xFF;      // end LSB
   Cmd[ 4] = Type;                     // typ code/blank/EEPROM
   // pocet pokusu o spravny prijem :
   for( int i = 0; i < ISP_TRIALS; i++){
      if( !Send( sizeof( Cmd), 0, 0x04, Cmd)){
         return( false);               // hw problem
      }
      if( strnequ( RxBuffer, "L\r\n", 3)){
         return( false);               // security error
      }
      // uvodni CR LF :
      if( !strnequ( RxBuffer, "\r\n", 2)){
         continue;                     // syntaxe odpovedi
      }
      // blok dat
      int  RowSize;                    // pocet cisel na radku
      int  Remainder = Length;         // zbytek do konce
      int  DIndex    = 0;              // index do datoveho bufferu
      while( 1){
         if( Remainder > ISP_ROW_SIZE){
            RowSize = ISP_ROW_SIZE;
         } else {
            RowSize = Remainder;
         }
         // cteni a rozbor radku odpovedi :
         if( !FDevice->Receive( RxBuffer)){
            break;                     // timeout - znovu
         }
         RxIndex = 0;                  // zacatek radku
         word Address;                 // adresa pocatku radku
         if( !ReadWord( Address)){
            break;                     // syntaxe - znovu
         }
         if( Address != StartAddress){
            break;                     // necekany vyvoj adresy - znovu
         }
         if( RxBuffer[ RxIndex++] != '='){
            break;                     // syntaxe - znovu
         }
         // hodnoty na radku :
         byte Value;                   // aktualni byte
         bool LineOk =  true;          // uspesne nacteni radku
         for( int j = 0; j < RowSize; j++){
            if( !ReadByte( Value)){
               LineOk = false;
               break;
            }
            Data[ DIndex++] = Value;
         }
         if( !LineOk){
            break;                     // chyba syntaxe radku - znovu
         }
         // koncove CR LF :
         if( !strnequ( &RxBuffer[ RxIndex], "\r\n", 2)){
            break;                     // chybny konec radku - znovu
         }
         // priprava na novy radek :
         Remainder    -= RowSize;
         StartAddress += RowSize;
         if( Remainder == 0){
            return( true); // nacteno vse
         }
      }
      Sleep( 500);          // dobeh stare odpovedi
      continue;             // novy pokus
   }
   return( false);
} // DisplayFunction

//******************************************************************************
// Cteni hodnoty
//******************************************************************************

bool TIsp::ReadFunction( byte Data0, byte Data1, byte &Value)
// Cteni hodnoty
{
   byte Data[ 2];
   Data[ 0] = Data0;
   Data[ 1] = Data1;
   // pocet pokusu o spravny prijem :
   for( int i = 0; i < ISP_TRIALS; i++){
      if( !Send( sizeof( Data), 0, 0x05, &Data)){
         return( false);    // hw problem
      }
      if( strnequ( RxBuffer, "L\r\n", 3)){
         return( false);    // security error
      }
      RxIndex = 0;
      if( !ReadByte( Value)){
         continue;          // syntaxe odpovedi
      }
      if( !strnequ( &RxBuffer[ RxIndex], ".\r\n", 3)){
         continue;          // syntaxe odpovedi
      }
      return( true);
   }
   return( false);
} // ReadFunction

//******************************************************************************
// Zakladni komunikace
//******************************************************************************

bool TIsp::Send( int Reclen, int LoadOffset, int RecordType, void *Data)
// Vysilani a prijem odpovedi do <RxBuffer>
{
   for( int i = 0; i < ISP_TRIALS; i++){
      if( !FDevice->Send( Reclen, LoadOffset, RecordType, Data)){
         continue;      // novy pokus o odeslani
      }
      if( !FDevice->Receive( RxBuffer)){
         continue;      // novy pokus o odeslani
      }
      if( strnequ( RxBuffer, "X\r\n", 3)){
         continue;      // checksum error
      }
      return( true);
   }
   return( false);      // timeout echa nebo odpovedi
} // Send

//******************************************************************************
// Cteni bytu
//******************************************************************************

bool TIsp::ReadByte( byte &Value)
// V bufferu dekoduje hodnotu bytu
{
   byte x;
   Value = 0;
   x     = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= x << 4;
   x     = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= x;
   return( true);
} // ReadByte

//******************************************************************************
// Cteni bytu
//******************************************************************************

bool TIsp::ReadWord( word &Value)
// V bufferu dekoduje hodnotu slova
{
   byte x;
   Value = 0;
   x     = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= (word)x << 12;
   x     = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= (word)x << 8;
   x     = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= (word)x << 4;
   x     = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= (word)x;
   return( true);
} // ReadWord

//******************************************************************************
// Cteni nibblu
//******************************************************************************

byte TIsp::ReadNibble()
// Cte jednu hexa cislici, vraci 0xFF pri chybe
{
   char c = RxBuffer[ RxIndex++];
   if( c >= '0' && c <= '9'){
      return( c - '0');
   }
   if( c >= 'A' && c <= 'F'){
      return( c - 'A' + 10);
   }
   return( 0xFF);
} // ReadNibble

//******************************************************************************
// Prubeh operace
//******************************************************************************

int TIsp::GetProgress()
// Vraci stav prubehu operace v procentech
{
   return( (WLength - WRemainder) * 100 / WLength);
} // GetProgress

