//******************************************************************************
//
//   PgmProgress.cpp  Programming progress frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "PgmProgress.h"
#include "PgmWorkplace.h"

//---------------------------------------------------------------------
#pragma resource "*.dfm"
TPgmProgressForm *PgmProgressForm;

//******************************************************************************
//  Konstruktor
//******************************************************************************

__fastcall TPgmProgressForm::TPgmProgressForm(TComponent* AOwner)
	: TForm(AOwner)
{
   DoCancel      = false;
   FResult       = OK;
   FVerification = true;        // implicitne zapnuto
} // TReadProgressForm

//******************************************************************************
//  Modalni dialog
//******************************************************************************

bool __fastcall TPgmProgressForm::Execute( TForm *Parent)
{
   // Inicializace formulare :
   AnsiString Progress;                     // procenta -  text
   DoCancel  = false;                       // predcasne ukonceni
   LblPhase->Caption       = "?";
   LblProgress->Caption    = "100%";
   bool Done = false;                       // konec cyklu
   // Modalni zobrazeni a blokovani rodice :
   Parent->Enabled         = false;         // zakaz interakci s rodicem
   this->Show();                            // zobraz toto okno
   Application->ProcessMessages();          // vyber cekajici zpravy
   Screen->Cursor          = crHourGlass;   // presypaci hodiny
   //---------------------------------------------------------------------------
   // Mazani zarizeni :
   ProgressBar->Position   = 0;
   LblPhase->Caption       = "Erasing...";
   LblPhase->Refresh();
   if( !Workplace->Isp->Erase()){
      FResult = ERASE_ERROR;
      Done = false;
      goto DoReturn;
   }
   //---------------------------------------------------------------------------
   // Programovani :
   ProgressBar->Position   = 0;
   LblPhase->Caption       = "Programming...";
   LblPhase->Refresh();
   Workplace->Isp->StartProgramCode( Workplace->CodeStartAddress, Workplace->CodeLength, Workplace->Code);
   Done = false;
   while( !Done){
      if( !Workplace->Isp->DoProgramCode( Done)){
         FResult = PROGRAMMING_ERROR;
         Done = false;
         goto DoReturn;
      }
      ProgressBar->Position = Workplace->Isp->Progress;
      Progress.printf( "%3d%%", Workplace->Isp->Progress);
      LblProgress->Caption = Progress;
      // vyber cekajici zpravy :
      Screen->Cursor = crDefault;           // zrus hodiny
      Application->ProcessMessages();
      Screen->Cursor = crHourGlass;
      if( DoCancel){
         // predcasne ukonceni
         FResult = CANCEL;
         Done = false;
         goto DoReturn;
      }
   }
   //---------------------------------------------------------------------------
   // Verifikace :
   if( FVerification){
      ProgressBar->Position   = 0;
      LblPhase->Caption       = "Verification...";
      LblPhase->Refresh();
      Workplace->Isp->StartVerifyCode( Workplace->CodeStartAddress, Workplace->CodeLength,
                                       Workplace->Code);
      Done = false;
      while( !Done){
         if( !Workplace->Isp->DoVerifyCode( Done)){
            FResult = VERIFICATION_ERROR;
            Done = false;
            goto DoReturn;
         }
         ProgressBar->Position = Workplace->Isp->Progress;
         Progress.printf( "%3d%%", Workplace->Isp->Progress);
         LblProgress->Caption = Progress;
         // vyber cekajici zpravy :
         Screen->Cursor = crDefault;           // zrus hodiny
         Application->ProcessMessages();
         Screen->Cursor = crHourGlass;
         if( DoCancel){
            // predcasne ukonceni
            FResult = CANCEL;
            Done = false;
            goto DoReturn;
         }
      }
   } // FVerification
   //---------------------------------------------------------------------------
   // Zapis registru :
   ProgressBar->Position   = 0;
   LblPhase->Caption       = "Registers...         ";
   LblPhase->Refresh();
   Done = false;
   // disable user boot :
   if( !Workplace->Isp->SetBSB( 0x00)){
      FResult = REGISTER_ERROR;
      goto DoReturn;
   }
   // protect read/write flash :
   if( !Workplace->Isp->SetSSB( TIsp::RD_WR_SECURITY)){
      FResult = REGISTER_ERROR;
      goto DoReturn;
   }
   // restart device :
   Workplace->Isp->Reset();
   Done = true;                             // registry uspesne zapsany
   //---------------------------------------------------------------------------
DoReturn :
   // uklid formulare :
   this->Visible   = false;                 // schovej toto okno
   Parent->Enabled = true;                  // odblokuj rodice
   Screen->Cursor  = crDefault;             // zrus hodiny
   // dokoncovaci operace :
   if( !Done){
      return( false);
   }
   return( true);
} // Execute


//******************************************************************************
//  Ukonceni
//******************************************************************************

void __fastcall TPgmProgressForm::Cancel(TObject *Sender)
{
   DoCancel = true;
} // Cancel

