//******************************************************************************
//
//   Boot.h       Bootloader level
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef BootH
#define BootH

#ifndef UartH
   #include "../Library/Serial/Uart/ComUart.h"
#endif

#ifndef LoggerH
   #include "../Library/Serial/Logger.h"
#endif

#define BOOT_MAX_MESSAGE 600       // max pocet znaku ve vyslane zprave / na radce odpovedi

//******************************************************************************
// TBoot
//******************************************************************************

class TBoot {
public :
   TBoot();
   // Konstruktor
   ~TBoot();
   // Destruktor
   bool Connect( TString Port);
   // Pripojeni pres <Port>
   bool SetBaud( int Baud);
   // Nastaveni baudove rychlosti
   bool Check();
   // Kontrola pritomnosti, nastaveni autobaud
   bool Send( byte Reclen, word LoadOffset, byte RecordType, void *Data);
   // Vysilani
   bool Receive( char *Message);
   // Prijem jedne radky (ukoncena '\0')
   // pozor delka <Message> > BOOT_MAX_MESSAGE
   __property TComUart *Port     = {read=FPort};
   __property TLogger  *Logger   = {read=FLogger,    write=FLogger};
   __property int      RxTimeout = {read=FRxTimeout, write=FRxTimeout};
//---------------------------------------------------------------------------

protected :
   TComUart *FPort;                   // connection port
   TLogger  *FLogger;                 // raw data logger
   int     FRxTimeout;                // Rx celkovy timeout
   int     FRxEchoTimeout;            // Rx timeout echa prikazu
   int     FRxIntercharacterTimeout;  // Rx intercharacter timeout

   char    Buffer[ BOOT_MAX_MESSAGE]; // pracovni buffer
   int     Index;                     // pracovni index
   byte    CheckSum;                  // kontrolni soucet

   char NibbleToChar( byte Nibble);
   // Vrati hexa reprezentaci dolniho nibblu
   void ConvertByte( byte Value);
   // zapise hexa reprezentaci <Value> do <Buffer>, posune <Index>
   void ConvertWord( word Value);
   // zapise hexa reprezentaci <Value> do <Buffer>, posune <Index>
   void ConvertData( byte *Data, int Length);
   // zapise hexa reprezentaci pole <Data> o delce <Length>
}; // TBoot

//---------------------------------------------------------------------------
#endif
