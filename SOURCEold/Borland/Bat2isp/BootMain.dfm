object BootForm: TBootForm
  Left = 349
  Top = 270
  Width = 678
  Height = 551
  Caption = 'BootForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainStatus: TLabel
    Left = 16
    Top = 504
    Width = 65
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Unconnected'
  end
  object MainPort: TComboBox
    Left = 534
    Top = 13
    Width = 98
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10')
  end
  object Connect: TButton
    Left = 8
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 1
    OnClick = ConnectClick
  end
  object Check: TButton
    Left = 8
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Check'
    TabOrder = 2
    OnClick = CheckClick
  end
  object SendSingle: TButton
    Left = 104
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Send single'
    TabOrder = 3
    OnClick = SendSingleClick
  end
  object MainMemo: TMemo
    Left = 12
    Top = 80
    Width = 654
    Height = 409
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 4
  end
  object Receive: TButton
    Left = 200
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Receive'
    TabOrder = 5
    OnClick = ReceiveClick
  end
  object SendMultiline: TButton
    Left = 200
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Send multiline'
    TabOrder = 6
    OnClick = SendMultilineClick
  end
end
