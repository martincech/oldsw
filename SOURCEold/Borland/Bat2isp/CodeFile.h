//******************************************************************************
//
//   CodeFile.h    Processor code file
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#ifndef CodeFileH
#define CodeFileH

#include "../Library/unisys/sysdef.h"
#include <stdio.h>

//******************************************************************************
// TCodeFile
//******************************************************************************

class TCodeFile {
public :
   #define CODE_MAX_DESCRIPTION  63                // max. delka popisu souboru
   // signatura souboru :
   #pragma pack( push, 1)                          // byte alignment
   typedef struct {
      char  Description[ CODE_MAX_DESCRIPTION + 1]; // popis kodu
      word  Version;                                // cislo verze 0x0100 = 1.00
      word  StartAddress;                           // zacatek kodu
      dword Length;                                 // delka kodu
      word  Crc;                                    // zabezpeceni kodu
      byte  Spare;                                  // vyplnovy byte
      byte  Checksum;                               // zabezpeceni signatury
   } TSignature;
   #pragma pack( pop)                              // original alignment

   TCodeFile();
   // Konstruktor
   ~TCodeFile();
   // Destruktor
   bool LoadHex( TString FileName);
   // Nacte HEX soubor
   bool Load( TString FileName);
   // Nacte binarni soubor
   void Save( TString FileName);
   // Ulozi binarni soubor

   __property TSignature Signature        = {read=FSignature,write=FSignature};
   __property word       CodeStartAddress = {read=FSignature.StartAddress};
   __property dword      CodeLength       = {read=FSignature.Length};
   __property byte      *Code             = {read=FCode};

//------------------------------------------------------------------------------
protected :
   TSignature FSignature;
   byte      *FCode;
   // dekodovani HEX radku :
   #define HEX_MAX_LINE 600              // max. delka radku HEX souboru
   char Line[ HEX_MAX_LINE + 1];         // buffer radku
   int  LineLength;                      // delka radku
   int  LineIndex;                       // ukazatel na radku
   byte LineChecksum;                    // kontrolni soucet radku

   byte CalcChecksum();
   // vypocte zabezpeceni signatury
   word CalcCrc();
   // vypocte zabezpeceni kodu
   bool ReadLine( FILE *f, byte &Reclen, word &LoadOffset, byte &RecordType, byte *Data);
   // cteni radku HEX souboru
   byte ReadNibble();
   // konverze hexa znaku, vraci 0xFF pri chybe
   bool ReadByte( byte &Value);
   // cteni bytu z radku
   bool ReadWord( word &Value);
   // cteni slova z radku
}; // TCodeFile
//------------------------------------------------------------------------------

#endif
