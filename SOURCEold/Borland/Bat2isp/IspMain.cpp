//******************************************************************************
//
//   BootMain.cpp  Test bootloaderu
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "IspMain.h"
#include "PgmProgress.h"
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TIspForm *IspForm;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TIspForm::TIspForm(TComponent* Owner)
        : TForm(Owner)
{
} // TBootForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TIspForm::Create(TObject *Sender)
{
   Crt          = new TCrt( MainMemo);        // CRT emulator
   Logger       = new TCrtLogger( Crt);       // monitor komunikace
   Logger->Mode = TCrtLogger::MIXED;
   Workplace    = new TWorkplace;
   Workplace->Initialize();
   MainPort->ItemIndex = 0;                   // implicitni port
   HideDumpClick( 0);
   MainPortChange( 0);                        // nastaveni portu
} // Create

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TIspForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   TCanvas *Canvas = this->Canvas;
   // rows count :

   Canvas->Font = MainMemo->Font;
   int Height = Canvas->TextHeight( "Ag");
   Crt->Rows   = Height > 0 ? MainMemo->Height / Height : 1;

   // cols count :
   int Width  = Canvas->TextWidth( "MMWW");
   Width  /= 4;
   Crt->Cols   = Width > 0 ? MainMemo->Width / Width : 1;

   // clear window :
   Crt->Clear();
} // Resize

//******************************************************************************
// Zmena portu
//******************************************************************************

void __fastcall TIspForm::MainPortChange(TObject *Sender)
{
   TString  PortName = MainPort->Text;
   if( !Workplace->Isp->SetPort( PortName)){
      Crt->printf( "Unable open %s port\n", PortName.c_str());
      MainStatus->Caption = "Unconnected";
      return;
   }
   Crt->Clear();
   Crt->printf( "Open %s\n", PortName.c_str());
   MainStatus->Caption = "Connected";
} // MainPortChange

//******************************************************************************
// Check
//******************************************************************************

void __fastcall TIspForm::CheckClick(TObject *Sender)
{
   if( !Workplace->Isp->CheckDevice()){
      Crt->printf( "Unable find device (set PSEN ?)\n");
      return;
   }
   Crt->printf( "Device found");
   MainStatus->Caption = "Ready";
} // Check

//******************************************************************************
// Send Single
//******************************************************************************

void __fastcall TIspForm::ReadClick(TObject *Sender)
{
   MainStatus->Caption = "Tx";
   MainStatus->Refresh();
   TIsp::TSignature Signature;
   if( !Workplace->Isp->GetSignature( Signature)){
      Crt->printf( "Unable get signature\n");
      return;
   }
   LblManufacturer->Caption = IntToHex( Signature.Manufacturer, 2);
   LblDevice1->Caption      = IntToHex( Signature.Device1, 2);
   LblDevice2->Caption      = IntToHex( Signature.Device2, 2);
   LblDevice3->Caption      = IntToHex( Signature.Device3, 2);
   byte Value;
   if( !Workplace->Isp->GetSSB( Value)){
      Crt->printf( "Unable get SSB\n");
      return;
   }
   // Pozn.: dalsi registry nejdou cist pri ochrane RD/WR
   LblSSB->Caption = IntToHex( Value, 2);
   if( !Workplace->Isp->GetBSB( Value)){
      Crt->printf( "Unable get BSB\n");
      return;
   }
   EditBSB->Text = IntToHex( Value, 2);
   if( !Workplace->Isp->GetSBV( Value)){
      Crt->printf( "Unable get SBV\n");
      return;
   }
   LblSBV->Caption = IntToHex( Value, 2);
   MainStatus->Caption = "OK";
} // ReadClick

//******************************************************************************
// Write BSB
//******************************************************************************

void __fastcall TIspForm::WriteBSBClick(TObject *Sender)
{
   AnsiString TxtValue = EditBSB->Text;
   int Value;
   sscanf( TxtValue.c_str(), "%x", &Value);
   MainStatus->Caption = "Tx";
   MainStatus->Refresh();
   if( !Workplace->Isp->SetBSB( (byte)Value)){
      Crt->printf( "Unable set BSB\n");
      return;
   }
   MainStatus->Caption = "Rx";
   MainStatus->Refresh();
   byte NewValue;
   if( !Workplace->Isp->GetBSB( NewValue)){
      Crt->printf( "Unable get BSB\n");
      return;
   }
   EditBSB->Text = IntToHex( NewValue, 2);
   MainStatus->Caption = "OK";
} // WriteBSBClick

//******************************************************************************
// Programming
//******************************************************************************

void __fastcall TIspForm::ProgrammingClick(TObject *Sender)
{
   for( int i = 0; i < ISP_CODE_SIZE; i++){
      Workplace->Code[ i] = (byte)(i % ISP_PAGE_SIZE + i / ISP_PAGE_SIZE);
   }
   Workplace->CodeLength       = 255;//ISP_CODE_SIZE;
   Workplace->CodeStartAddress = 0;
   if( !PgmProgressForm->Execute( this)){
      Crt->printf( "Error or Cancel programming\n");
      return;
   }
   MainStatus->Caption = "OK";
} // ProgrammingClick

//******************************************************************************
// Reset
//******************************************************************************

void __fastcall TIspForm::ResetClick(TObject *Sender)
{
   if( !Workplace->Isp->Reset()){
      Crt->printf( "Unable reset device\n");
      return;
   }
   MainStatus->Caption = "OK";
} // ResetClick

//******************************************************************************
// Erase
//******************************************************************************

void __fastcall TIspForm::EraseClick(TObject *Sender)
{
   Screen->Cursor          = crHourGlass;   // presypaci hodiny
   if( !Workplace->Isp->Erase()){
      Screen->Cursor = crDefault;           // zrus hodiny
      Crt->printf( "Unable erase device\n");
      return;
   }
   Screen->Cursor = crDefault;              // zrus hodiny
   MainStatus->Caption = "OK";
} // EraseClick


//******************************************************************************
// Hide Dump
//******************************************************************************

void __fastcall TIspForm::HideDumpClick(TObject *Sender)
{
   if( HideDump->Checked){
      Workplace->Isp->Device->Logger = 0;
   } else {
      Workplace->Isp->Device->Logger = Logger;   // vypisy komunikace
   }
} // HideDumpClick

