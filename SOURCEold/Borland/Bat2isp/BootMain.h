//******************************************************************************
//
//   BootMain.h   Test bootloaderu
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef BootMainH
#define BootMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "Boot.h"
#include "../Library/Crt/Crt.h"
#include "../Library/Serial/CrtLogger.h"

//---------------------------------------------------------------------------
class TBootForm : public TForm
{
__published:	// IDE-managed Components
        TComboBox *MainPort;
        TButton *Connect;
        TButton *Check;
        TButton *SendSingle;
        TMemo *MainMemo;
        TLabel *MainStatus;
        TButton *Receive;
        TButton *SendMultiline;
        void __fastcall Create(TObject *Sender);
        void __fastcall MemoResize(TObject *Sender);
        void __fastcall ConnectClick(TObject *Sender);
        void __fastcall CheckClick(TObject *Sender);
        void __fastcall SendSingleClick(TObject *Sender);
        void __fastcall ReceiveClick(TObject *Sender);
        void __fastcall SendMultilineClick(TObject *Sender);
private:	// User declarations
   TCrt       *Crt;
   TCrtLogger *Logger;
   TBoot      *Boot;
public:		// User declarations
   __fastcall TBootForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TBootForm *BootForm;
//---------------------------------------------------------------------------
#endif
