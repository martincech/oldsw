//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("IspTest.res");
USEFORM("IspMain.cpp", IspForm);
USEUNIT("Boot.cpp");
USEUNIT("Isp.cpp");
USEFORM("PgmProgress.cpp", PgmProgressForm);
USEUNIT("PgmWorkplace.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("CodeFile.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TIspForm), &IspForm);
       Application->CreateForm(__classid(TPgmProgressForm), &PgmProgressForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
