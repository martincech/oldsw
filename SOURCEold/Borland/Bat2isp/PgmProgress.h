//******************************************************************************
//
//   PgmProgress.h    Programming progress frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef PgmProgressH
#define PgmProgressH

//----------------------------------------------------------------------------
#include <vcl\System.hpp>
#include <vcl\Windows.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "../Library/Unisys/uni.h"

//----------------------------------------------------------------------------

class TPgmProgressForm : public TForm
{
__published:
        TButton *BtnCancel;
	TBevel *Bevel1;
        TProgressBar *ProgressBar;
        TLabel *LblProgress;
        TLabel *LblPhase;
        void __fastcall Cancel(TObject *Sender);
public:
   // chybove kody pri programovani :
   typedef enum {
      OK,
      CANCEL,
      ERASE_ERROR,
      PROGRAMMING_ERROR,
      VERIFICATION_ERROR,
      REGISTER_ERROR,
   } TResult;

   virtual __fastcall TPgmProgressForm(TComponent* AOwner);
   bool __fastcall Execute( TForm *Parent);
   __property TResult Result = {read=FResult};
   __property bool    Verification = {read=FVerification,write=FVerification};

private:
   bool    DoCancel;        // predcasne ukonceni
   TResult FResult;         // kod posledni chyby
   bool    FVerification;   // po vypaleni verifikuj
};
//----------------------------------------------------------------------------
extern PACKAGE TPgmProgressForm *PgmProgressForm;
//----------------------------------------------------------------------------
#endif    
