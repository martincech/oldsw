//******************************************************************************
//
//   PgmWorkplace.cpp  Programmer working platform
//   Version 1.0  (c)  VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "PgmWorkplace.h"

TWorkplace *Workplace = 0;

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TWorkplace::TWorkplace()
{
   FIsp      = new TIsp;
   FCodeFile = new TCodeFile;
   Workplace = this;                // globalni pointer
} // TWorkplace

//******************************************************************************
// Inicializace
//******************************************************************************

void TWorkplace::Initialize()
// Uvodni inicializace
{
} // Initialize
