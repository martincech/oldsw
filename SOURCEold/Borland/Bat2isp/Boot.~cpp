//******************************************************************************
//
//   Boot.cpp     Bootloader level
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Boot.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------

// Implementacni konstanty :

#define CHECK_TRIALS  5        // pocet pokusu o navazani spojeni

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Konstruktor
//******************************************************************************

TBoot::TBoot()
// Konstruktor
{
   FPort   = new TComUart;
   FLogger = 0;

   FRxTimeout               = 500;
   FRxEchoTimeout           = 500;
   FRxIntercharacterTimeout = 100;
} // TBoot

//******************************************************************************
// Destruktor
//******************************************************************************

TBoot::~TBoot()
// Destruktor
{
   if( FPort){
      delete FPort;
   }
} // ~TBoot

//******************************************************************************
// Connect
//******************************************************************************

bool TBoot::Connect( TString Port)
// Connect via <Port>
{
   // UART setup :
   if( FPort){
      delete FPort;
      FPort = 0;
   }
   TUart *Uart = new TComUart;
   TIdentifier Identifier;
   if( !Uart->Locate( Port, Identifier)){
      delete Uart;
      return( false);
   }
   if( !Uart->Open( Identifier)){
      return( false);
   }
   FPort = Uart;
   if( !SetBaud( 115200)){
      return( false);
   }
   return( true);
} // Connect

//******************************************************************************
// Set baud
//******************************************************************************

bool TBoot::SetBaud( int Baud)
// Nastaveni baudove rychlosti
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   TUart::TParameters Parameters;
   Parameters.BaudRate  = Baud;
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   if( !FPort->SetParameters( Parameters)){
      return( false);
   }
   // common init :
   FPort->SetRxNowait();
   FPort->Flush();
   return( true);
} // SetBaud

//******************************************************************************
// Pritomnost
//******************************************************************************

bool TBoot::Check()
// Kontrola pritomnosti, nastaveni autobaud
{

   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   FPort->Flush();
   FPort->SetRxWait( FRxIntercharacterTimeout, 0);     // cekej znak
   char TestChar  = 'U';
   char ReplyChar;
   for( int i = 0; i < CHECK_TRIALS; i++){
      // vyslani testovaciho znaku :
      if( FPort->Write( &TestChar, sizeof( TestChar)) != sizeof( TestChar)){
         continue;
      }
      RwdTx( &TestChar, sizeof( TestChar));
      // prijem odpovedi :
      if( FPort->Read( &ReplyChar, sizeof( ReplyChar)) != sizeof( ReplyChar)){
         continue;
      }
      if( ReplyChar != 'U'){
         RwdGarbage( &ReplyChar, sizeof( ReplyChar));
         continue;
      }
      RwdRx( &ReplyChar, sizeof( ReplyChar));
      return( true);
   }
   return( false);
} // Check

//******************************************************************************
// Vysilani
//******************************************************************************

bool TBoot::Send( byte Reclen, word LoadOffset, byte RecordType, void *Data)
// Vysilani
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   Index    = 0;
   CheckSum = 0;
   Buffer[ Index++] = ':';             // uvodni naraznik
   ConvertByte( (byte)Reclen);         // delka dat
   ConvertWord( (word)LoadOffset);     // ofset/adresa
   ConvertByte( (byte)RecordType);     // typ zaznamu
   ConvertData( (byte *)Data, Reclen); // data delky <Reclen>
   ConvertByte( (byte)(-CheckSum));    // kontrolni soucet
   int Length = Index;                 // index obsahuje delku zpravy
   FPort->Flush();
   if( FPort->Write( Buffer, Index) != Length){
      return( false);
   }
   RwdTx( Buffer, Length);
   // prijem echa :
   char ReplyBuffer[ BOOT_MAX_MESSAGE];
   FPort->SetRxWait( FRxEchoTimeout, 0);// cekej zpravu
   if( FPort->Read( ReplyBuffer, Length) != Length){
      RwdGarbage( &ReplyBuffer, sizeof( ReplyBuffer));
      return( false);                  // timeout odpovedi
   }
   if( !memequ( Buffer, ReplyBuffer, Length)){
      RwdGarbage( ReplyBuffer, Length);
      return( false);                  // nesouhlas echa
   }
   RwdRx( ReplyBuffer, Length);
   return( true);
} // Send

//******************************************************************************
// Prijem
//******************************************************************************

bool TBoot::Receive( char *Message)
// Prijem jedne radky (ukoncena '\0')
// pozor delka <Message> > BOOT_MAX_MESSAGE
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   Index = 0;
   FPort->SetRxWait( FRxTimeout, 0);   // cekej odpoved
   char ReplyChar;
   while( 1){
      // prijem znaku :
      if( FPort->Read( &ReplyChar, sizeof( ReplyChar)) != sizeof( ReplyChar)){
         RwdGarbage( Buffer, Index);
         return( false);               // timeout
      }
      FPort->SetRxWait( FRxIntercharacterTimeout, 0);     // cekej znak
      Buffer[ Index++] = ReplyChar;
      if( ReplyChar == '\n'){
         break;                        // konec radku
      }
   }
   int Length = Index;                 // celkova delka
   memcpy( Message, Buffer, Length);
   Message[ Length] = '\0';            // ukoncovaci znak
   RwdRx( Buffer, Length);
   return( true);
} // Receive

//------------------------------------------------------------------------------

//******************************************************************************
// Konverze Nibblu
//******************************************************************************

char TBoot::NibbleToChar( byte Nibble)
// Vrati hexa reprezentaci dolniho nibblu
{
   Nibble &= 0x0F;
   if( Nibble < 10){
      return( Nibble + '0');
   }
   return( Nibble - 10 + 'A');
} // NibbleToChar

//******************************************************************************
// Konverze Bytu
//******************************************************************************

void TBoot::ConvertByte( byte Value)
// zapise hexa reprezentaci <Value> do <Buffer>, posune <Index>
{
   CheckSum += Value;
   Buffer[ Index++] = NibbleToChar( Value >> 4);   // horni nibble
   Buffer[ Index++] = NibbleToChar( Value & 0x0F); // dolni nibble
} // ConvertByte

//******************************************************************************
// Konverze Slova
//******************************************************************************

void TBoot::ConvertWord( word Value)
// zapise hexa reprezentaci <Value> do <Buffer>, posune <Index>
{
   byte HiByte = Value >> 8;                        // horni byte
   byte LoByte = Value & 0xFF;                      // dolni byte
   ConvertByte( HiByte);
   ConvertByte( LoByte);
} // ConvertWord

//******************************************************************************
// Konverze pole dat
//******************************************************************************

void TBoot::ConvertData( byte *Data, int Length)
// zapise hexa reprezentaci pole <Data> o delce <Length>
{
   for( int i = 0; i < Length; i++){
      ConvertByte( Data[ i]);
   }
} // ConvertData
