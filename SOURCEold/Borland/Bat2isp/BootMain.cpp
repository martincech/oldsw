//******************************************************************************
//
//   BootMain.cpp  Test bootloaderu
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "BootMain.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBootForm *BootForm;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TBootForm::TBootForm(TComponent* Owner)
        : TForm(Owner)
{
} // TBootForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TBootForm::Create(TObject *Sender)
{
   Crt          = new TCrt( MainMemo);        // CRT emulator
   Logger       = new TCrtLogger( Crt);       // monitor komunikace
   Logger->Mode = TCrtLogger::MIXED;
   Boot         = new TBoot;                  // testovany modul
   Boot->Logger = Logger;                     // vypisy komunikace
   MainPort->ItemIndex = 0;                   // implicitni port
} // Create

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TBootForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   TCanvas *Canvas = this->Canvas;
   // rows count :

   Canvas->Font = MainMemo->Font;
   int Height = Canvas->TextHeight( "Ag");
   Crt->Rows   = Height > 0 ? MainMemo->Height / Height : 1;

   // cols count :
   int Width  = Canvas->TextWidth( "MMWW");
   Width  /= 4;
   Crt->Cols   = Width > 0 ? MainMemo->Width / Width : 1;

   // clear window :
   Crt->Clear();
} // Resize

//******************************************************************************
// Connect
//******************************************************************************

void __fastcall TBootForm::ConnectClick(TObject *Sender)
{
   Crt->Clear();
   TString  PortName = MainPort->Text;
   if( !Boot->Connect( PortName)){
      Crt->printf( "Unable open %s port\n", PortName.c_str());
      MainStatus->Caption = "Unconnected";
      return;
   }
   if( !Boot->SetBaud( 9600)){
      Crt->printf( "Unable set BAUD\n");
      MainStatus->Caption = "Unconnected";
      return;
   }
   Crt->printf( "Open %s\n", PortName.c_str());
   MainStatus->Caption = "Connected";
} // ConnectClick

//******************************************************************************
// Check
//******************************************************************************

void __fastcall TBootForm::CheckClick(TObject *Sender)
{
   if( !Boot->Check()){
      Crt->printf( "Unable find device (set PSEN ?)\n");
      return;
   }
   Crt->printf( "Device found");
   MainStatus->Caption = "Ready";
} // Check

//******************************************************************************
// Send Single
//******************************************************************************

void __fastcall TBootForm::SendSingleClick(TObject *Sender)
{
   MainStatus->Caption = "Tx";
   MainStatus->Refresh();
   byte Data[ 2];
   Data[ 0] = 0;
   Data[ 1] = 0;
   if( !Boot->Send( sizeof( Data), 0, 5, &Data)){
      Crt->printf( "Unable send\n");
      return;
   }
   MainStatus->Caption = "Rx";
   MainStatus->Refresh();
   char Buffer[ BOOT_MAX_MESSAGE + 1];
   if( !Boot->Receive( Buffer)){
      Crt->printf( "Unable receive\n");
      MainStatus->Caption = "ERR";
      return;
   }
   MainStatus->Caption = "OK";
} // SendSingle

//******************************************************************************
// Send Multiline
//******************************************************************************

void __fastcall TBootForm::SendMultilineClick(TObject *Sender)
{
   MainStatus->Caption = "Tx";
   MainStatus->Refresh();
   byte Data[ 5];
   Data[ 0] = 0;     // start address
   Data[ 1] = 0x30;
   Data[ 2] = 0;     // end address
   Data[ 3] = 0x55;
   Data[ 4] = 0;     // display code
   if( !Boot->Send( sizeof( Data), 0, 4, &Data)){
      Crt->printf( "Unable send\n");
      return;
   }
   MainStatus->Caption = "Rx";
   MainStatus->Refresh();
   char Buffer[ BOOT_MAX_MESSAGE + 1];
   if( !Boot->Receive( Buffer)){
      Crt->printf( "Unable receive\n");
      MainStatus->Caption = "ERR";
      return;
   }
   MainStatus->Caption = "OK";
} // SendMultiline

//******************************************************************************
// Receive
//******************************************************************************

void __fastcall TBootForm::ReceiveClick(TObject *Sender)
{
   MainStatus->Caption = "Rx";
   MainStatus->Refresh();
   char Buffer[ BOOT_MAX_MESSAGE + 1];
   if( !Boot->Receive( Buffer)){
      Crt->printf( "Unable receive\n");
      MainStatus->Caption = "ERR";
      return;
   }
   MainStatus->Caption = "OK";
} // Receive

