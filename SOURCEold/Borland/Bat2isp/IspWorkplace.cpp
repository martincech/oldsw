//******************************************************************************
//
//   Workplace.cpp  Flash module working platform
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "IspWorkplace.h"

TWorkplace *Workplace = 0;

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TWorkplace::TWorkplace()
{
   FIsp              = new TIsp;
   FCodeStartAddress = 0;
   FCodeLength       = 0;
   FCode             = new byte[ ISP_CODE_SIZE];
   Workplace = this;                // globalni pointer
} // TWorkplace

//******************************************************************************
// Inicializace
//******************************************************************************

void TWorkplace::Initialize()
// Uvodni inicializace
{
} // Initialize
