//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("HexConvertor.res");
USEFORM("ConvertorMain.cpp", ConvertorMainForm);
USEUNIT("CodeFile.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TConvertorMainForm), &ConvertorMainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
