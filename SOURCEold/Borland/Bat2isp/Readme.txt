                Programovani ED2 pre ISP


Program HexConvertor :
- nacte Intel HEX soubor
- ulozi jej jako binarni
- na zacatek souboru umisti zahlavi s popisem


Program Programmer :
- nacte binarni soubor (vytvoreny HexConvertor-em)
- pokud je pripojen procesor pres RS232 a pripraven PSEN,
  muze se spustit programovani
- po ukonceni programovani je procesor resetovan


Pokud behem programovani dojde k chybe (viz stavova radka programu),
je nutne znovu podrzet PSEN a resetovat procesor. Pote je mozne
se znovu pokusit o naprogramovani. (Bootloader sice po chybe
odpovida na identifikaci, ale nedokaze spravne interpretovat prikazy)

Program prijima na prikazove radce nasledujici klice :

/h          povoli v menu prime nacitani HEX souboru (rezim jako Flip)
/b38400     zmena baudove rychlosti (implicitne 115200).
            Bez mezery za pismenem 'b'
/v          vypaleni bez nasledne verifikace


Programy IspTest a BootTest jsou pro ladici ucely

