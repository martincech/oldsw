//******************************************************************************
//
//   Isp.h        ISP basic commands
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef IspH
#define IspH

#ifndef BootH
   #include "Boot.h"               // komunikace s bootloaderem
#endif

//******************************************************************************
// TIsp
//******************************************************************************

class TIsp {
public :
   #define ISP_PAGE_SIZE   128         // velikost stranky pro zapis Flash/EEPROM
   #define ISP_CODE_SIZE 65536         // celkova velikost Flash
   // Signatura cipu :
   typedef struct {
      byte Manufacturer;
      byte Device1;
      byte Device2;
      byte Device3;
   } TSignature;

   // kody registru SSB :
   typedef enum {
      NO_SECURITY    = 0xFF,        // flash - povoleni cteni i zapisu
      WRITE_SECURITY = 0xFE,        // flash - zakaz zapisu
      RD_WR_SECURITY = 0xFC         // flash - zakaz zapisu i cteni
   } TSecurity;

//------------------------------------------------------------------------------
   TIsp();
   // Konstruktor
   ~TIsp();
   // Destruktor
   bool SetPort( TString PortName);
   // Nastaveni k portu
   bool CheckDevice();
   // Kontrola pripojeni

   void StartProgramCode( int StartAddress, int Length, byte *Code);
   // Zahajeni programovani kodu
   bool DoProgramCode( bool &Done);
   // Cyklicke volani az po <Done> = true

   void StartVerifyCode( int StartAddress, int Length, byte *Code);
   // Zahajeni verifikace kodu
   bool DoVerifyCode( bool &Done);
   // Cyklicke volani az po <Done> = true

   bool ProgramEeprom( word StartAddress, word Length, byte *Code);
   // Programovani EEPROM

   bool Reset();
   // Reset procesoru
   bool Erase();
   // Mazani procesoru (trva az 6s)
   bool SetSSB( TSecurity Value);
   // Nastaveni ochrany Software Security Byte
   bool SetBSB( byte Value);
   // Nastaveni registru Boot Status Byte

   bool GetSignature( TSignature &Signature);
   // Cteni typoveho cisla
   bool GetSSB( byte &Value);
   // Cteni Software Security Byte
   bool GetBSB( byte &Value);
   // Cteni Boot Status Byte
   bool GetSBV( byte &Value);
   // Cteni Software Boot Vector

   __property TBoot *Device  = {read=FDevice};        // spojeni na zarizeni
   __property int   Progress = {read=GetProgress};    // prubeh operace - procenta
//------------------------------------------------------------------------------

protected :
   TBoot *FDevice;                              // spojeni pres bootloader
   char  RxBuffer[ BOOT_MAX_MESSAGE + 1];       // buffer odpovedi
   int   RxIndex;                               // index do bufferu

   int   WStartAddress;                         // pocatecni adresa pro zapis/verifikaci
   int   WLength;                               // delka bloku pro zapis/verifikaci
   int   WRemainder;                            // zbytkova delka pro zapis/verifikaci
   byte  *WData;                                // data pro zapis/verifikaci
   int   WIndex;                                // index do datoveho bufferu

   bool WriteFunction( byte Data0, byte Data1);
   // Zapis (spusteni) funkce
   bool WriteValue( byte Data0, byte Data1, byte Value);
   // Zapis hodnoty
   bool WriteCommand( byte RecordType, word Address, byte Length, void *Data);
   // Prikaz pro zapis
   bool DisplayFunction( word StartAddress, word Length, byte Type, byte *Data);
   // Cteni bloku dat
   bool ReadFunction( byte Data0, byte Data1, byte &Value);
   // Cteni hodnoty
   bool Send( int Reclen, int LoadOffset, int RecordType, void *Data);
   // Vysilani a prijem odpovedi do <RxBuffer>
   bool ReadByte( byte &Value);
   // V bufferu dekoduje hodnotu bytu
   bool ReadWord( word &Value);
   // V bufferu dekoduje hodnotu slova
   byte ReadNibble();
   // Cte jednu hexa cislici, vraci 0xFF pri chybe
   int GetProgress();
   // Vraci stav prubehu operace v procentech
}; // TIsp

//------------------------------------------------------------------------------
#endif
