//******************************************************************************
//
//   ConvertorMain.cpp  Hex convertor main module
//   Version 0.0       (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "ConvertorMain.h"
#pragma package(smart_init)
#pragma resource "*.dfm"

TConvertorMainForm *ConvertorMainForm;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TConvertorMainForm::TConvertorMainForm(TComponent* Owner)
        : TForm(Owner)
{
   CodeFile = new TCodeFile;
} // TConvertorMainForm

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TConvertorMainForm::LoadHexClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   if( !FileExists( FileOpenDialog->FileName)){
      LblStatus->Caption = "ERR : File doesn't exists";
      return;
   }
   if( !CodeFile->LoadHex( FileOpenDialog->FileName)){
      LblStatus->Caption = "ERR : Unable open/decode file";
      return;
   }
   LblStartAddress->Caption = "0x" + IntToHex( CodeFile->CodeStartAddress, 4);
   LblLength->Caption       = AnsiString( CodeFile->CodeLength);
   LblStatus->Caption       = "Load OK";
} // LoadHexClick

//******************************************************************************
// Save
//******************************************************************************

void __fastcall TConvertorMainForm::SaveClick(TObject *Sender)
{
   // kontrola popisu :
   EditDescription->Text.Trim();
   if( EditDescription->Text.Length() > CODE_MAX_DESCRIPTION){
      LblStatus->Caption       = "ERR : Description too long";
      return;
   }
   if( EditDescription->Text.Length() == 0){
      LblStatus->Caption       = "ERR : Empty description";
      return;
   }
   // kontrola cisla verze :
   char Buf[ 20];
   strcpy( Buf, EditVersion->Text.c_str());
   int Version, SubVersion;
   sscanf( Buf, "%d.%d", &Version, &SubVersion);
   Version    &= 0xFF;
   SubVersion &= 0xFF;
   if( !Version && !SubVersion){
      LblStatus->Caption       = "ERR : Invalid version number";
      return;
   }
   // ulozeni souboru :
   if( !FileSaveDialog->Execute()){
      return;
   }
   if( FileExists( FileSaveDialog->FileName)){
      if( Application->MessageBox( "File already exists,\nIt will be owerwritten",
          "Confirmation", MB_OKCANCEL) != IDOK){
          LblStatus->Caption       = "File save canceled";
          return;
      }
   }
   strcpy( CodeFile->Signature.Description, EditDescription->Text.c_str());
   CodeFile->Signature.Version = (Version  << 8) | SubVersion;
   CodeFile->Save( FileSaveDialog->FileName);
   LblStatus->Caption       = "Save OK";
} // SaveClick

