//******************************************************************************
//
//   IspWorkplace.h   Isp test working platform
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef IspWorkplaceH
#define IspWorkplaceH

#include "Isp.h"

//******************************************************************************
// TWorkplace
//******************************************************************************

class TWorkplace {
public :
   TWorkplace();
   // Konstruktor
   void Initialize();
   // Uvodni inicializace

   __property TIsp *Isp = {read=FIsp};
   __property int  CodeStartAddress = {read=FCodeStartAddress, write=FCodeStartAddress};
   __property int  CodeLength       = {read=FCodeLength, write=FCodeLength};
   __property byte *Code            = {read=FCode};
//---------------------------------------------------------------------------
protected :
   TIsp *FIsp;
   int  FCodeStartAddress;
   int  FCodeLength;
   byte *FCode;
}; // TWorkplace
//---------------------------------------------------------------------------

extern TWorkplace *Workplace;

//---------------------------------------------------------------------------
#endif
