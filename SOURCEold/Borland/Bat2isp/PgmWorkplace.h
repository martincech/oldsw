//******************************************************************************
//
//   PgmWorkplace.h   Programmer working platform
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef PgmWorkplaceH
#define PgmWorkplaceH

#include "Isp.h"
#include "CodeFile.h"

//******************************************************************************
// TWorkplace
//******************************************************************************

class TWorkplace {
public :
   TWorkplace();
   // Konstruktor
   void Initialize();
   // Uvodni inicializace

   __property TIsp      *Isp              = {read=FIsp};
   __property TCodeFile *CodeFile         = {read=FCodeFile};
   __property word       CodeStartAddress = {read=GetStartAddress};
   __property dword      CodeLength       = {read=GetLength};
   __property byte      *Code             = {read=GetCode};
//---------------------------------------------------------------------------
protected :
   TIsp      *FIsp;
   TCodeFile *FCodeFile;

   word   GetStartAddress();
   dword  GetLength();
   byte  *GetCode();
}; // TWorkplace
//---------------------------------------------------------------------------

inline word TWorkplace::GetStartAddress()
{
   return( FCodeFile->CodeStartAddress);
} // GetStartAddress

inline dword TWorkplace::GetLength()
{
   return( FCodeFile->CodeLength);
} // GetLength

inline byte *TWorkplace::GetCode()
{
   return( FCodeFile->Code);
} // GetCode

extern TWorkplace *Workplace;

//---------------------------------------------------------------------------
#endif
