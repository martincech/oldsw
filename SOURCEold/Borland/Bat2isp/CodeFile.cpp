//******************************************************************************
//
//   CodeFile.cpp  Processor code file
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "CodeFile.h"
#include "isp.h"              // code limits

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TCodeFile::TCodeFile()
// Konstruktor
{
   memset( &FSignature, 0, sizeof( FSignature));
   FCode             = new byte[ ISP_CODE_SIZE];
} // TCodeFile

//******************************************************************************
// Destruktor
//******************************************************************************

TCodeFile::~TCodeFile()
// Destruktor
{
   if( FCode){
      delete[] FCode;
   }
} // TCodeFile

//******************************************************************************
// Cteni Hex
//******************************************************************************

bool TCodeFile::LoadHex( TString FileName)
// Nacte HEX soubor
{
   if( !FileExists( FileName)){
      return( false);
   }
   FILE *f;
   f = fopen( FileName.c_str(), "r");
   if( !f){
      return( false);
   }
   memset( FCode, 0xFF, ISP_CODE_SIZE);          // implicitni vypln
   byte Reclen;
   word LoadOffset;
   byte RecordType;
   byte Data[ 256];
   word MaxAddress = 0;
   word MinAddress = ISP_CODE_SIZE - 1;
   bool Fail = false;
   while( 1){
      if( !ReadLine( f, Reclen, LoadOffset, RecordType, Data)){
         Fail = true;  // predcasny konec souboru
         break;
      }
      if( RecordType == 1){
         break;        // konec souboru (?)
      }
      if( RecordType != 0){
         Fail = true;  // neznamy typ zaznamu
         break;
      }
      if( Reclen == 0){
         Fail = true;  // zaznam nulove delky
         break;
      }
      memcpy( &FCode[ LoadOffset], Data, Reclen);
      // minimalni adresa :
      if( LoadOffset < MinAddress){
         MinAddress = LoadOffset;
      }
      // maximalni adresa :
      word EndAddress = LoadOffset + (Reclen - 1);
      if( EndAddress > MaxAddress){
         MaxAddress = EndAddress;
      }
   }
   fclose( f);
   FSignature.StartAddress = MinAddress;
   FSignature.Length       = (dword)MaxAddress + 1;
   return( !Fail);
} // LoadHex

//******************************************************************************
// Cteni binarni
//******************************************************************************

bool TCodeFile::Load( TString FileName)
// Nacte binarni soubor
{
   if( !FileExists( FileName)){
      return( false);
   }
   FILE *f;
   f = fopen( FileName.c_str(), "rb");
   if( !f){
      return( false);
   }
   // nacteni a kontrola zahlavi :
   fread( &FSignature, sizeof( FSignature), 1, f);
   if( CalcChecksum() != FSignature.Checksum){
      return( false);
   }
   // nacteni a kontrola kodu :
   memset( FCode, 0xFF, ISP_CODE_SIZE);          // implicitni vypln
   fread( FCode, FSignature.Length, 1, f);       // nacteni kodu
   if( CalcCrc() != FSignature.Crc){
      return( false);
   }
   fclose( f);
   return( true);
} // Load

//******************************************************************************
// Zapis binarni
//******************************************************************************

void TCodeFile::Save( TString FileName)
// Ulozi binarni soubor
{
   FSignature.Crc      = CalcCrc();              // zabezpeceni kodu
   FSignature.Checksum = CalcChecksum();         // zabezpeceni zahlavi
   FILE *f;
   f = fopen( FileName.c_str(), "wb");
   fwrite( &FSignature, sizeof( FSignature), 1, f);
   fwrite( FCode, FSignature.Length, 1, f);
   fclose( f);
} // Save

//------------------------------------------------------------------------------

//******************************************************************************
// Kontrolni suma
//******************************************************************************

byte TCodeFile::CalcChecksum()
// vypocte zabezpeceni signatury
{
   byte *p = (byte *)&FSignature;
   byte Checksum = 0;
   for( int i = 0; i < sizeof( FSignature) - 1; i++){
      Checksum += p[ i];
   }
   return( Checksum);
} // CalcChecksum

//******************************************************************************
// Zabezpeceni kodu
//******************************************************************************

word TCodeFile::CalcCrc()
// vypocte zabezpeceni kodu
{
   word Crc = 0;
   for( dword i = 0; i < FSignature.Length; i++){
      Crc += FCode[ i];
   }
   return( Crc);
} // CalcCrc

//******************************************************************************
// Cteni HEX radku
//******************************************************************************


bool TCodeFile::ReadLine( FILE *f, byte &Reclen, word &LoadOffset, byte &RecordType, byte *Data)
// cteni radku HEX souboru
{
   if( !fgets( Line, HEX_MAX_LINE, f)){
      return( false);                // EOF
   }
   LineLength = strlen( Line);
   if( !LineLength){
      return( false);                // nulova delka ?
   }
   LineLength--;                     // ukonceni radku
   if( Line[ LineLength] != '\n'){
      return( false);                // prilis dlouhy radek
   }
   Line[ LineLength] = '\0';         // odseknuti ukonceni radku
   // dekodovani radku :
   LineIndex    = 0;
   LineChecksum = 0;
   if( Line[ LineIndex++] != ':'){
      return( false);                // neplatny radek
   }
   if( !ReadByte( Reclen)){
      return( false);
   }
   if( !ReadWord( LoadOffset)){
      return( false);
   }
   if( !ReadByte( RecordType)){
      return( false);
   }
   for( int i = 0; i < Reclen; i++){
      if( !ReadByte( Data[ i])){
         return( false);
      }
   }
   byte Checksum;
   if( !ReadByte( Checksum)){
      return( false);
   }
   if( LineChecksum != 0){
      return( false);
   }
   return( true);
} // ReadLine

//******************************************************************************
// Cteni nibble
//******************************************************************************

byte TCodeFile::ReadNibble()
// konverze hexa znaku, vraci 0xFF pri chybe
{
   if( LineIndex >= LineLength){
      return( 0xFF);
   }
   char c = Line[ LineIndex++];
   if( c >= '0' && c <= '9'){
      return( c - '0');
   }
   if( c >= 'A' && c <= 'F'){
      return( c - 'A' + 10);
   }
   return( 0xFF);
} // ReadNibble

//******************************************************************************
// Cteni bytu
//******************************************************************************

bool TCodeFile::ReadByte( byte &Value)
// cteni bytu z radku
{
   Value = 0;
   byte x = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= x << 4;
   x = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= x;
   LineChecksum += Value;
   return( true);
} // ReadByte

//******************************************************************************
// Cteni slova
//******************************************************************************

bool TCodeFile::ReadWord( word &Value)
// cteni slova z radku
{
   byte HiByte;
   byte LoByte;
   if( !ReadByte( HiByte)){
      return( false);
   }
   if( !ReadByte( LoByte)){
      return( false);
   }
   Value = ((word)HiByte << 8) | LoByte;
   return( true);
} // ReadWord
