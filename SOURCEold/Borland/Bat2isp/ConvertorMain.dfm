object ConvertorMainForm: TConvertorMainForm
  Left = 393
  Top = 324
  Width = 504
  Height = 345
  Caption = 'Hex Convertor'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 72
    Width = 59
    Height = 13
    Caption = 'Description :'
  end
  object Label2: TLabel
    Left = 16
    Top = 104
    Width = 41
    Height = 13
    Caption = 'Version :'
  end
  object Label3: TLabel
    Left = 16
    Top = 144
    Width = 68
    Height = 13
    Caption = 'Code start @ :'
  end
  object LblStartAddress: TLabel
    Left = 120
    Top = 144
    Width = 35
    Height = 13
    Caption = '0x0000'
  end
  object Label4: TLabel
    Left = 16
    Top = 176
    Width = 63
    Height = 13
    Caption = 'Code length :'
  end
  object LblLength: TLabel
    Left = 120
    Top = 176
    Width = 30
    Height = 13
    Caption = '65536'
  end
  object Label5: TLabel
    Left = 24
    Top = 296
    Width = 36
    Height = 13
    Caption = 'Status :'
  end
  object LblStatus: TLabel
    Left = 72
    Top = 296
    Width = 30
    Height = 13
    Caption = 'No file'
  end
  object LoadHex: TButton
    Left = 16
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Load Hex'
    TabOrder = 0
    OnClick = LoadHexClick
  end
  object Save: TButton
    Left = 408
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Save'
    TabOrder = 1
    OnClick = SaveClick
  end
  object EditDescription: TEdit
    Left = 120
    Top = 64
    Width = 289
    Height = 21
    TabOrder = 2
    Text = 'Bat2 weight'
  end
  object EditVersion: TEdit
    Left = 120
    Top = 96
    Width = 41
    Height = 21
    TabOrder = 3
    Text = '1.00'
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'hex'
    Filter = 'Intel HEX files (*.hex)|*.hex'
    Options = [ofHideReadOnly, ofFileMustExist, ofEnableSizing]
    Title = 'Load HEX file'
    Left = 168
    Top = 16
  end
  object FileSaveDialog: TSaveDialog
    DefaultExt = 'bfw'
    FileName = 'Noname'
    Filter = 'Firmware files (*.bfw)|*.bfw'
    Title = 'Save binary file'
    Left = 296
    Top = 16
  end
end
