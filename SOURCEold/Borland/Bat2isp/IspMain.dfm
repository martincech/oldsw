object IspForm: TIspForm
  Left = 288
  Top = 166
  Width = 761
  Height = 823
  Caption = 'IspForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainStatus: TLabel
    Left = 16
    Top = 771
    Width = 65
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = 'Unconnected'
  end
  object Label1: TLabel
    Left = 496
    Top = 16
    Width = 51
    Height = 13
    Caption = 'Signature :'
  end
  object LblManufacturer: TLabel
    Left = 552
    Top = 16
    Width = 12
    Height = 13
    Caption = 'FF'
  end
  object LblDevice1: TLabel
    Left = 568
    Top = 16
    Width = 12
    Height = 13
    Caption = 'FF'
  end
  object LblDevice2: TLabel
    Left = 584
    Top = 16
    Width = 12
    Height = 13
    Caption = 'FF'
  end
  object LblDevice3: TLabel
    Left = 600
    Top = 16
    Width = 12
    Height = 13
    Caption = 'FF'
  end
  object Label2: TLabel
    Left = 496
    Top = 40
    Width = 27
    Height = 13
    Caption = 'SSB :'
  end
  object Label3: TLabel
    Left = 496
    Top = 62
    Width = 27
    Height = 13
    Caption = 'BSB :'
  end
  object Label4: TLabel
    Left = 496
    Top = 82
    Width = 27
    Height = 13
    Caption = 'SBV :'
  end
  object LblSSB: TLabel
    Left = 552
    Top = 40
    Width = 12
    Height = 13
    Caption = 'FF'
  end
  object LblSBV: TLabel
    Left = 552
    Top = 82
    Width = 12
    Height = 13
    Caption = 'FF'
  end
  object MainPort: TComboBox
    Left = 638
    Top = 13
    Width = 98
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    OnChange = MainPortChange
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10')
  end
  object Check: TButton
    Left = 8
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Check'
    TabOrder = 1
    OnClick = CheckClick
  end
  object Read: TButton
    Left = 104
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 2
    OnClick = ReadClick
  end
  object MainMemo: TMemo
    Left = 12
    Top = 184
    Width = 737
    Height = 577
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 3
  end
  object WriteBSB: TButton
    Left = 192
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Write BSB'
    TabOrder = 4
    OnClick = WriteBSBClick
  end
  object EditBSB: TEdit
    Left = 549
    Top = 60
    Width = 22
    Height = 21
    TabOrder = 5
    Text = 'FF'
  end
  object Programming: TButton
    Left = 192
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Programming'
    TabOrder = 6
    OnClick = ProgrammingClick
  end
  object Reset: TButton
    Left = 280
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Reset'
    TabOrder = 7
    OnClick = ResetClick
  end
  object Erase: TButton
    Left = 104
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Erase'
    TabOrder = 8
    OnClick = EraseClick
  end
  object HideDump: TCheckBox
    Left = 640
    Top = 64
    Width = 97
    Height = 17
    Caption = 'Hide Dump'
    TabOrder = 9
    OnClick = HideDumpClick
  end
end
