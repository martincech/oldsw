//******************************************************************************
//
//   ProgrammerMain.cpp  Programmer main module
//   Version 0.0         (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "ProgrammerMain.h"
#include "PgmWorkplace.h"
#include "PgmProgress.h"

#pragma package(smart_init)
#pragma resource "*.dfm"

TProgrammerMainForm *ProgrammerMainForm;

#define Status( msg)  StatusBar->Panels->Items[ 0]->Text = (msg);

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TProgrammerMainForm::TProgrammerMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // ProgrammerMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TProgrammerMainForm::FormCreate(TObject *Sender)
{
   // uvodni inicializace :
   new TWorkplace;
   Baud         = 115200;        // implicitni baud
   Verification = true;          // implicitne verifikuj
   Workplace->Initialize();
   // prikazova radka :
   for( int i = 1; i < _argc; i++){
      if( strequ( _argv[ i], "/h")){
         FileLoadHex->Visible = true;              // zobraz Load HEX
         continue;
      }
      if( strequ( _argv[ i], "/v")){
         Verification = false;                     // zakaz verifikace
         continue;
      }
      if( _argv[ i][ 0] == '/' && _argv[ i][ 1] == 'b'){
         Baud = atoi( &_argv[ i][ 2]);             // baudova rychlost
         continue;
      }
   }
   MainPort->ItemIndex = 0;                        // implicitni port
   MainPortChange( 0);
} // FormCreate

//******************************************************************************
// Open file
//******************************************************************************

void __fastcall TProgrammerMainForm::FileOpenExecute(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   if( !FileExists( FileOpenDialog->FileName)){
      Status( "ERR : File doesn't exists");
      return;
   }
   LblDescription->Caption = "?";
   LblVersion->Caption     = "0.00";
   if( !Workplace->CodeFile->Load( FileOpenDialog->FileName)){
      Status( "ERR : Corrupted file");
      return;
   }
   LblDescription->Caption = Workplace->CodeFile->Signature.Description;
   AnsiString Version;
   Version.printf( "%d.%02d", Workplace->CodeFile->Signature.Version >> 8,
                              Workplace->CodeFile->Signature.Version & 0xFF);
   LblVersion->Caption = Version;
   LblSize->Caption    = AnsiString( Workplace->CodeFile->Signature.Length) + " B";
   Timer->Enabled      = true;
   Status( "Load OK");
} // FileOpenExecute

//******************************************************************************
// Load HEX
//******************************************************************************

void __fastcall TProgrammerMainForm::FileLoadHexExecute(TObject *Sender)
{
   if( !HexOpenDialog->Execute()){
      return;
   }
   if( !FileExists( HexOpenDialog->FileName)){
      Status( "ERR : File doesn't exists");
      return;
   }
   LblDescription->Caption = ExtractFileName( HexOpenDialog->FileName);
   LblVersion->Caption     = "0.00";
   if( !Workplace->CodeFile->LoadHex( HexOpenDialog->FileName)){
      Status( "ERR : Corrupted Hex file");
      return;
   }
   LblSize->Caption    = AnsiString( Workplace->CodeFile->Signature.Length) + " B";
   Timer->Enabled      = true;
   Status( "HEX Load OK");
} // FileLoadHexExecute

//******************************************************************************
// Exit
//******************************************************************************

void __fastcall TProgrammerMainForm::FileExitExecute(TObject *Sender)
{
   Close();
} // FileExitExecute

//******************************************************************************
// Programming
//******************************************************************************

void __fastcall TProgrammerMainForm::FileProgrammingExecute( TObject *Sender)
{
   if( Application->MessageBox( "Do You really want to program device ?",
          "Confirmation", MB_YESNO) != IDYES){
          Status( "Programming canceled");
          return;
   }
   Timer->Enabled = false;
   PgmProgressForm->Verification = Verification;
   if( !PgmProgressForm->Execute( this)){
      Timer->Enabled = true;
      switch( PgmProgressForm->Result){
         case TPgmProgressForm::CANCEL :
            Status( "Programming canceled");
            break;        // user break;
         case TPgmProgressForm::ERASE_ERROR :
            Status( "ERR : unable to erase device");
            break;
         case TPgmProgressForm::PROGRAMMING_ERROR :
            Status( "ERR : unable to programming device");
            break;
         case TPgmProgressForm::VERIFICATION_ERROR :
            Status( "ERR : verification failed");
            break;
         case TPgmProgressForm::REGISTER_ERROR :
            Status( "ERR : unable to write device registers");
            break;
         IDEFAULT
      }
      return;
   }
   Timer->Enabled = true;
   Status( "Programming OK");
} // FileProgrammingExecute

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TProgrammerMainForm::TimerTimer(TObject *Sender)
{
   if( !Workplace->Isp->CheckDevice()){
      FileProgramming->Enabled = false;
      return;
   }
   Status( "Device ready");
   FileProgramming->Enabled = true;
} // TimerTimer

//******************************************************************************
// Zmena portu
//******************************************************************************

void __fastcall TProgrammerMainForm::MainPortChange(TObject *Sender)
{
   TString  PortName = MainPort->Text;
   if( !Workplace->Isp->SetPort( PortName)){
      Status( "ERR : Unable open port : " + PortName);
      return;
   }
   if( !Workplace->Isp->Device->SetBaud( Baud)){
      Application->MessageBox( "Invalid baud rate", "Error", MB_OK);
   }
   Status( "Port : " + PortName + " idle");
} // MainPortChange

