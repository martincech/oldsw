//******************************************************************************
//
//   BootMain.h   Test bootloaderu
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef IspMainH
#define IspMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "IspWorkplace.h"
#include "../Library/Crt/Crt.h"
#include "../Library/Serial/CrtLogger.h"

//---------------------------------------------------------------------------
class TIspForm : public TForm
{
__published:	// IDE-managed Components
        TComboBox *MainPort;
        TButton *Check;
        TButton *Read;
        TMemo *MainMemo;
        TLabel *MainStatus;
        TLabel *Label1;
        TLabel *LblManufacturer;
        TLabel *LblDevice1;
        TLabel *LblDevice2;
        TLabel *LblDevice3;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *LblSSB;
        TLabel *LblSBV;
        TButton *WriteBSB;
        TEdit *EditBSB;
        TButton *Programming;
        TButton *Reset;
        TButton *Erase;
        TCheckBox *HideDump;
        void __fastcall Create(TObject *Sender);
        void __fastcall MemoResize(TObject *Sender);
        void __fastcall CheckClick(TObject *Sender);
        void __fastcall ReadClick(TObject *Sender);
        void __fastcall MainPortChange(TObject *Sender);
        void __fastcall WriteBSBClick(TObject *Sender);
        void __fastcall ResetClick(TObject *Sender);
        void __fastcall EraseClick(TObject *Sender);
        void __fastcall ProgrammingClick(TObject *Sender);
        void __fastcall HideDumpClick(TObject *Sender);
private:	// User declarations
   TCrt       *Crt;
   TCrtLogger *Logger;
   TWorkplace *Workplace;
public:		// User declarations
   __fastcall TIspForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TIspForm *IspForm;
//---------------------------------------------------------------------------
#endif
