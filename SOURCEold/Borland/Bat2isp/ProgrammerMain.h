//******************************************************************************
//
//   ProgrammerMain.h    Programmer main module
//   Version 0.0         (c) VymOs
//
//******************************************************************************

#ifndef ProgrammerMainH
#define ProgrammerMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TProgrammerMainForm : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu;
        TActionList *ActionList;
        TAction *FileOpen;
        TAction *FileProgramming;
        TAction *FileExit;
        TMenuItem *MenuFile;
        TMenuItem *Open1;
        TMenuItem *Programming1;
        TMenuItem *Exit1;
        TPanel *Panel1;
        TStatusBar *StatusBar;
        TLabel *Label1;
        TLabel *LblDescription;
        TLabel *Label2;
        TLabel *LblVersion;
        TComboBox *MainPort;
        TLabel *Label3;
        TOpenDialog *FileOpenDialog;
        TTimer *Timer;
        TLabel *Label4;
        TLabel *LblSize;
        TAction *FileLoadHex;
        TOpenDialog *HexOpenDialog;
        TMenuItem *LoadHEX1;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FileOpenExecute(TObject *Sender);
        void __fastcall FileExitExecute(TObject *Sender);
        void __fastcall FileProgrammingExecute(TObject *Sender);
        void __fastcall TimerTimer(TObject *Sender);
        void __fastcall MainPortChange(TObject *Sender);
        void __fastcall FileLoadHexExecute(TObject *Sender);
private:	// User declarations
   int  Baud;
   bool Verification;
public:		// User declarations
   __fastcall TProgrammerMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TProgrammerMainForm *ProgrammerMainForm;
//---------------------------------------------------------------------------
#endif
