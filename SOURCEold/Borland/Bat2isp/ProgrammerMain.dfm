object ProgrammerMainForm: TProgrammerMainForm
  Left = 379
  Top = 312
  Width = 515
  Height = 339
  Caption = 'Programmer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 507
    Height = 274
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 32
      Width = 104
      Height = 13
      Caption = 'Firmware Description :'
    end
    object LblDescription: TLabel
      Left = 142
      Top = 32
      Width = 6
      Height = 13
      Caption = '?'
    end
    object Label2: TLabel
      Left = 24
      Top = 56
      Width = 85
      Height = 13
      Caption = 'Firmware version :'
    end
    object LblVersion: TLabel
      Left = 144
      Top = 56
      Width = 21
      Height = 13
      Caption = '0.00'
    end
    object Label3: TLabel
      Left = 360
      Top = 30
      Width = 25
      Height = 13
      Caption = 'Port :'
    end
    object Label4: TLabel
      Left = 25
      Top = 80
      Width = 69
      Height = 13
      Caption = 'Firmware size :'
    end
    object LblSize: TLabel
      Left = 144
      Top = 80
      Width = 16
      Height = 13
      Caption = '0 B'
    end
    object MainPort: TComboBox
      Left = 401
      Top = 25
      Width = 98
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = MainPortChange
      Items.Strings = (
        'COM1'
        'COM2'
        'COM3'
        'COM4'
        'COM5'
        'COM6'
        'COM7'
        'COM8'
        'COM9'
        'COM10')
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 274
    Width = 507
    Height = 19
    Panels = <
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object MainMenu: TMainMenu
    Left = 224
    Top = 8
    object MenuFile: TMenuItem
      Caption = '&File'
      object LoadHEX1: TMenuItem
        Action = FileLoadHex
      end
      object Open1: TMenuItem
        Action = FileOpen
      end
      object Programming1: TMenuItem
        Action = FileProgramming
      end
      object Exit1: TMenuItem
        Action = FileExit
      end
    end
  end
  object ActionList: TActionList
    Left = 264
    Top = 8
    object FileOpen: TAction
      Category = 'File'
      Caption = '&Open'
      OnExecute = FileOpenExecute
    end
    object FileProgramming: TAction
      Category = 'File'
      Caption = '&Programming'
      Enabled = False
      OnExecute = FileProgrammingExecute
    end
    object FileExit: TAction
      Category = 'File'
      Caption = 'E&xit'
      OnExecute = FileExitExecute
    end
    object FileLoadHex: TAction
      Category = 'File'
      Caption = '&Load HEX'
      Visible = False
      OnExecute = FileLoadHexExecute
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'bfw'
    Filter = 'Firmware file (*.bfw)|*.bfw'
    Title = 'Open File'
    Left = 224
    Top = 56
  end
  object Timer: TTimer
    Enabled = False
    Interval = 2000
    OnTimer = TimerTimer
    Left = 264
    Top = 56
  end
  object HexOpenDialog: TOpenDialog
    DefaultExt = 'hex'
    Filter = 'Intel HEX file (*.hex)|*.hex'
    Title = 'Open File'
    Left = 224
    Top = 96
  end
end
