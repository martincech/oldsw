Komentar k souborum
-------------------


ArmPgm - programator ARM Bat1 (vypali HEX soubor do CPU)
Bat1isp - programator ARM Bat1 (specialni binarni format do CPU)
Bat1Reader - program na komunikaci s Bat1 USB, Logo, zakladni komunikace
Bat1Tester - kolekce programu na testovani a diagnostiku Bat1 a Bat1 NT
Library - pomocne funkce pro programy


Bat1 Tester
-----------

Bat1Tester.exe testovani Bat1 pomoci testeru castecne i bez testeru
Bat1NTTester.exe testovani Bat1 NT pomoci testeru castecne i bez testeru
Bat1Diag.exe diagnostika Bat1 pomoci testeru
Bat1NTDiag.exe diagnostika Bat1 NT pomoci testeru

Poznamka : programy Bat1Tester.exe a Bat1Diag maji data ulozena v adresari
           Data. Programy Bat1NTTester.exe a Bat1NTDiag maji data ulozena
           v adresari DataNT. Pozor adresar je nutno zadat v Setupu programu.

BatBoard.exe zakladni ovladani testeru
BatCalibration.exe kontrola a nastaveni kalibrace testeru
BatPower.exe jednotlive testy Bat1 s pomoci testeru

BatCharger.exe - diagnostika zavady nabijece Bat1, Bat1 NT


BatCharger obsluha programu
---------------------------

1. zhotovit nulovy modem Cannon 9 F/Cannon 9 F (dve samice)
   prekrizit Rx Tx (2-3, 3-2) spojit GND 5-5

2. Pripojit k PC a na printer port Bat1, Bat1 NT


3. Do Bat1 nahrat Bat1Test.hex, Do Bat1 NT nahrat Bat1NTTest.hex
   (programem ArmPgm.exe nebo pomoci Bat1Diag, Bat1NTDiag)

4. spustit BatCharger, zadat COMx na ktery je pripojen
   Bat1, Bat1 NT

5. Po stisknuti Run s periodou 0.5s prekresluje stav signalu,
   umozni vypnout/zapnout nabijec.

6. Pokud se v prubehu Run vypne napajeni Bat1, po zapnuti opet
   navaze komunikaci

