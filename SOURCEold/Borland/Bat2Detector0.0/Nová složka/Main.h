//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Bat2Detector/Detector.h"
#include <Grids.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
   TEdit *ComEdit;
   TLabel *StatusLabel;
   TButton *StartButton;
   TButton *StopButton;
   TStringGrid *DataStringGrid;
   TLabel *StdDevLabel;
   TOpenDialog *FileOpenDialog;
   TButton *Button1;
   TGroupBox *GroupBox1;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TEdit *AveragingWindowEdit;
   TEdit *AbsoluteRangeEdit;
   TEdit *StableWindowEdit;
   TGroupBox *GroupBox2;
   TLabel *Label1;
   TLabel *Label2;
   TEdit *AccuracyEdit;
   TEdit *RateEdit;
   TGroupBox *GroupBox3;
   TLabel *Label4;
   TPanel *Panel1;
   TRadioButton *DefaultModeRadioButton;
   TRadioButton *AllModeRadioButton;
   TRadioButton *StableModeRadioButton;
   TRadioButton *Ps081ModeRadioButton;
   TEdit *ModeBurstSizeEdit;
   TLabel *Label3;
   TEdit *PrefilterEdit;
   TLabel *Label8;
   TPanel *Panel2;
   TRadioButton *AdcFilterNoneRadioButton;
   TRadioButton *AdcFilterSinc3RadioButton;
   TRadioButton *AdcFilterSinc5RadioButton;
   TLabel *Label9;
   TEdit *ModeAveragingEdit;
   TLabel *Label10;
   TMemo *HexMemo;
   TButton *ProgramButton;
   TGroupBox *GroupBox4;
   TEdit *ZeroEdit;
   TEdit *FullRangeEdit;
   TEdit *RawZeroEdit;
   TEdit *RawFullRangeEdit;
   TButton *Button2;
   TButton *CalibrateButton;
   void __fastcall StartButtonClick(TObject *Sender);
   void __fastcall StopButtonClick(TObject *Sender);
   void __fastcall Button1Click(TObject *Sender);
   void __fastcall ProgramButtonClick(TObject *Sender);
   void __fastcall Button2Click(TObject *Sender);
   void __fastcall CalibrateButtonClick(TObject *Sender);
private:	// User declarations
   TBat2Detector *Detector;
public:		// User declarations
   __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
