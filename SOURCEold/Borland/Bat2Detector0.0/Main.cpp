//******************************************************************************
//
//   Main.cpp     Bat2 Detector main
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include <stdio.h>
#include <float.h>
#include <math.h>
#include "../Library/Bat2Detector/Detector.h"
#include "../Library/Picostrain/Alcs350.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#define Status(str)  StatusLabel->Caption = str
#define ClearStatus()  StatusLabel->Caption = "";

static void Replace(byte *Src, int SrcLength, byte *Search, byte *ReplaceBy, int PieceLength);
static double StdDev(double *Data, int Count);
static double DeltaMax(double *Data, int Count);
static void DataCallback(TDataSample *Data, int DataCount);
static double Max(double *Data, int Count);
static double Min(double *Data, int Count);
static double Mean(double *Data, int Count);

TForm1 *Form1;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TForm1::TForm1(TComponent* Owner)
   : TForm(Owner)
{
   DecimalSeparator='.'; // Change locale

   Detector = new TBat2Detector();
   Simulator = new TAlcs350();

   CodeLength = 0;

   // Init GUI
   char StringBuffer[20];

   sprintf(StringBuffer, "%d", Detector->AdcAccuracy);
   AccuracyEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Detector->AdcRate);
   RateEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Detector->AdcPrefilter);
   PrefilterEdit->Text = StringBuffer;

   switch(Detector->AdcFilter) {
      case PS081_FILTER_SINC3:
         AdcFilterSinc3RadioButton->Checked = true;
         break;

      case PS081_FILTER_SINC5:
         AdcFilterSinc5RadioButton->Checked = true;
         break;

      default:
         AdcFilterNoneRadioButton->Checked = true;
         break;
   }

   switch(Detector->ModeMode) {
      case DETECTOR_MODE_ALL:
         AllModeRadioButton->Checked = true;
         break;

      case DETECTOR_MODE_STABLE:
         StableModeRadioButton->Checked = true;
         break;

      case DETECTOR_MODE_PS081:
         Ps081ModeRadioButton->Checked = true;
         break;

      default:
         DefaultModeRadioButton->Checked = true;
         break;
   }

   sprintf(StringBuffer, "%d", Detector->ModeBurstSize);
   ModeBurstSizeEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Detector->ModeAveraging);
   ModeAveragingEdit->Text = StringBuffer;

   sprintf(StringBuffer, "%d", Detector->DetectionAveragingWindow);
   AveragingWindowEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%0.2f", Detector->DetectionRange);
   AbsoluteRangeEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Detector->DetectionStableWindow);
   StableWindowEdit->Text = StringBuffer;

   sprintf(StringBuffer, "%0.2f", Detector->CalibrationZero);
   ZeroEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%0.2f", Detector->CalibrationFullRange);
   FullRangeEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Detector->CalibrationRawZero);
   RawZeroEdit->Text = StringBuffer;
   sprintf(StringBuffer, "%d", Detector->CalibrationRawFullRange);
   RawFullRangeEdit->Text = StringBuffer;
}

//******************************************************************************
// Start
//******************************************************************************

void __fastcall TForm1::StartButtonClick(TObject *Sender)
{
   ClearStatus();

   if(AdcFilterNoneRadioButton->Checked == true) {
      Detector->AdcFilter = PS081_FILTER_NONE;
   } else if(AdcFilterSinc3RadioButton->Checked == true) {
      Detector->AdcFilter = PS081_FILTER_SINC3;
   } else {
      Detector->AdcFilter = PS081_FILTER_SINC5;
   }

   Detector->AdcAccuracy = AccuracyEdit->Text.ToInt();
   Detector->AdcRate = RateEdit->Text.ToInt();
   Detector->AdcPrefilter = PrefilterEdit->Text.ToInt();

   if(DefaultModeRadioButton->Checked == true) {
      Detector->ModeMode = DETECTOR_MODE_DEFAULT;

      DataStringGrid->RowCount = 1;

      DataStringGrid->Cells[ 0 ][ 0 ] = "unimplemented";

   } else if(AllModeRadioButton->Checked == true) {
      Detector->ModeMode = DETECTOR_MODE_ALL;

      DataStringGrid->RowCount = 1;

      DataStringGrid->Cells[ 0 ][ 0 ] = "Weight [g]";
   } else if(StableModeRadioButton->Checked == true) {
      Detector->ModeMode = DETECTOR_MODE_STABLE;

      DataStringGrid->RowCount = 2;

      DataStringGrid->Cells[ 0 ][ 0 ] = "Weight [g]";
      DataStringGrid->Cells[ 0 ][ 1 ] = "Time";
   } else {
      Detector->ModeMode = DETECTOR_MODE_PS081;

      DataStringGrid->RowCount = 10;

      DataStringGrid->Cells[ 0 ][ 0 ] = "Weight [g]";
      DataStringGrid->Cells[ 0 ][ 1 ] = "Stable [* = YES]";
      DataStringGrid->Cells[ 0 ][ 2 ] = "Raw [LSB]";
      DataStringGrid->Cells[ 0 ][ 3 ] = "LP In [g]";
      DataStringGrid->Cells[ 0 ][ 4 ] = "LP Out [g]";
      DataStringGrid->Cells[ 0 ][ 5 ] = "PreAvg Cnt";
      DataStringGrid->Cells[ 0 ][ 6 ] = "HighPass abs [g]";
      DataStringGrid->Cells[ 0 ][ 7 ] = "Stable Cnt";
      DataStringGrid->Cells[ 0 ][ 8 ] = "Stable Cnt";
      DataStringGrid->Cells[ 0 ][ 9 ] = "Stable Cnt";
   }

   Detector->ModeBurstSize = ModeBurstSizeEdit->Text.ToInt();
   Detector->ModeAveraging = ModeAveragingEdit->Text.ToInt();

   Detector->DetectionAveragingWindow = AveragingWindowEdit->Text.ToInt();
   Detector->DetectionRange = AbsoluteRangeEdit->Text.ToDouble();
   Detector->DetectionStableWindow = StableWindowEdit->Text.ToInt();

   Detector->SetCom(ComEdit->Text.c_str());

   if(SimulationEnableCheckBox->Checked) {
      FILE *SimulatorFile = fopen(SimulatorFileName.c_str(), "r");

      if(SimulatorFile) {
         double Strain;
         int Seconds;
         int Num;

         SimulationProfileLength = 0;
         while(!feof(SimulatorFile)) {
            Num = fscanf(SimulatorFile, "%f:%d", &SimulationProfile[SimulationProfileLength].Strain, &SimulationProfile[SimulationProfileLength].Seconds);

            if(Num != 2) {
               fgetc(SimulatorFile);
            } else {
               SimulationProfileLength++;

               if(SimulationProfileLength == SIMULATION_PROFILE_LENGTH_MAX) {
                  break;
               }
            }
         }

         fclose(SimulatorFile);

         if(SimulationProfileLength) {
            Simulator->Open(SimulatorComEdit->Text.c_str());
            SimulationProfileRepeat = false;
            SimulatorTimer->Enabled = true;
            SimulatorTicks = 0;
            SimulationProfilePtr = 0;
            Simulator->Set(SimulationProfile[0].Strain);

            // 0:0 on the last line means repeat
            if(SimulationProfile[SimulationProfileLength - 1].Strain == 0 && SimulationProfile[SimulationProfileLength - 1].Seconds == 0) {
               SimulationProfileLength--;
               SimulationProfileRepeat = true;
            }
         }
      }
   }

   AdcTicks = 0;
   AdcResetTimer->Enabled = true;

   int CommentLength = strlen(CommentEdit->Text.c_str());
   
   memcpy(Detector->Comment, CommentEdit->Text.c_str(), (CommentLength > BD_MAX_COMMENT ? BD_MAX_COMMENT : CommentLength) + 1);

   if(!Detector->Run(DataCallback)) {
      Status("Run error");
      SimulatorTimer->Enabled = false;
      AdcResetTimer->Enabled = false;
      return;
   }

   AdcResetTimer->Enabled = false;
   SimulatorTimer->Enabled = false;
   Status("Run OK");
}

//******************************************************************************
// Stop
//******************************************************************************

void __fastcall TForm1::StopButtonClick(TObject *Sender)
{
   Detector->Stop();
}

//******************************************************************************
// Read calibration
//******************************************************************************

void __fastcall TForm1::ReadCalibrationButtonClick(TObject *Sender)
{
   double Zero;
   dword RawZero;
   double FullRange;
   dword RawFullRange;

   ClearStatus();

   Detector->SetCom(ComEdit->Text.c_str());

   if(!Detector->ReadCalibrationData()) {
      Status("Calibration data error");
      return;
   }

   UpdateCalibrationData();

   Status("Calibration data OK");
}

//******************************************************************************
// Calibrate
//******************************************************************************

void __fastcall TForm1::CalibrateButtonClick(TObject *Sender)
{
   ClearStatus();

   Detector->SetCom(ComEdit->Text.c_str());

   Detector->CalibrationZero = ZeroEdit->Text.ToDouble();
   Detector->CalibrationFullRange = FullRangeEdit->Text.ToDouble();

   if(!Detector->Calibrate()) {
      Status("Calibration error");
      return;
   }

   UpdateCalibrationData();

   Status("Calibration OK");
}

//******************************************************************************
// Program
//******************************************************************************

void __fastcall TForm1::ProgramButtonClick(TObject *Sender)
{
   if(!CodeLength) {
      Status("Firmware Error");
      return;
   }

   if(!Detector->Firmware(Code, CodeLength)) {
      Status("Firmware Error");
      return;
   }

   Status("Firmware OK");
}

//******************************************************************************
// Parse
//******************************************************************************

void __fastcall TForm1::ParseButtonClick(TObject *Sender)
{
   ClearStatus();

   FILE *HexFile = fopen(HexFileName.c_str(), "r");

   if(!HexFile) {
      Status("Invalid file");
      return;
   }

   CodeLength = 0;

   int LineNum;
   int OpCode0;
   int OpCode1;
   int Num = 0;

   while(!feof(HexFile)) {
      Num = fscanf(HexFile, "%X: %X %X //---", &LineNum, &OpCode0, &OpCode1);

      if(Num != 3) {
         fgetc(HexFile);
      } else {
         Code[CodeLength] = OpCode0;
         Code[CodeLength + 1] = OpCode1;
         CodeLength += 2;
      }
   }

   fclose(HexFile);

   // Expect 2 successive jsubret opcodes in the end of the file
   if(CodeLength < 2) {
      return;
   }

   int Iterator = CodeLength + 1;
   const byte JSubRetOpCode = 0xCF;
   bool Found = false;

   while(Iterator-- > 0) {
      if(Code[Iterator] == JSubRetOpCode && Code[Iterator - 1] == JSubRetOpCode) {
         Found = true;
         break;
      }
   }

   /*
      If jsubrets found replace addresses of calls to that jsubrets
      with RollAvg and InitAvg routines start
   */
   if(Found) {
      word FakeRollAvg = Iterator;
      word FakeInitAvg = Iterator - 1;

      const word RollAvg = 0xC21;      // Start of RollAvg
      const word InitAvg = 0xC30;      // Start of InitAvg
      const byte JSubOpCode = 0xE0;
      byte Search[2];
      byte ReplaceBy[2];

      word RollSearch = ((word)JSubOpCode << 8) + FakeRollAvg;
      word RollReplaceBy = ((word)JSubOpCode << 8) + RollAvg;
      Search[0] = RollSearch >> 8;
      Search[1] = RollSearch;
      ReplaceBy[0] = RollReplaceBy >> 8;
      ReplaceBy[1] = RollReplaceBy;
      Replace(Code, CodeLength, Search, ReplaceBy, 2);

      word InitSearch = ((word)JSubOpCode << 8) + FakeInitAvg;
      word InitReplaceBy = ((word)JSubOpCode << 8) + InitAvg;
      Search[0] = InitSearch >> 8;
      Search[1] = InitSearch;
      ReplaceBy[0] = InitReplaceBy >> 8;
      ReplaceBy[1] = InitReplaceBy;
      Replace(Code, CodeLength, Search, ReplaceBy, 2);
   }

   // Write it back
   HexFile = fopen(HexFileName.c_str(), "w");

   if(HexFile) {
      for(int i = 0 ; i < CodeLength ; i += 2) {
         fprintf(HexFile, "%04X: %02X %02X\n", i, Code[i], Code[i+1]);
      }

      fclose(HexFile);
   }

   Status("Parse OK");
}

//******************************************************************************
// Simulator Update
//******************************************************************************

void __fastcall TForm1::SimulatorTimerTimer(TObject *Sender)
{
   SimulatorTicks++;

   if(SimulatorTicks >= SimulationProfile[SimulationProfilePtr].Seconds) {
      SimulatorTicks = 0;
      SimulationProfilePtr++;

      if(SimulationProfilePtr >= SimulationProfileLength) {
         if(SimulationProfileRepeat == true) {
            SimulationProfilePtr = 0;
            Simulator->Set(SimulationProfile[0].Strain);
         } else {
            SimulatorTimer->Enabled = false;
         }
      } else {
         Simulator->Set(SimulationProfile[SimulationProfilePtr].Strain);
      }
   }
}

//******************************************************************************
// File Open
//******************************************************************************

void __fastcall TForm1::FileOpenButtonClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }

   Detector->SetFile(FileOpenDialog->FileName.c_str());
   FileLabel->Caption = FileOpenDialog->FileName;
}

//******************************************************************************
// Hex File Open
//******************************************************************************

void __fastcall TForm1::HexFileOpenButtonClick(TObject *Sender)
{
   if( !HexFileOpenDialog->Execute()){
      return;
   }

   HexFileName = HexFileOpenDialog->FileName;
   HexFileLabel->Caption = HexFileOpenDialog->FileName;
}

//******************************************************************************
// Simulator File Open
//******************************************************************************

void __fastcall TForm1::SimulatorFileOpenButtonClick(TObject *Sender)
{
   if( !SimulatorFileOpenDialog->Execute()){
      return;
   }

   SimulatorFileName = SimulatorFileOpenDialog->FileName;
   SimulatorFileLabel->Caption = SimulatorFileOpenDialog->FileName;
}

//******************************************************************************
// Data callback
//******************************************************************************

void DataCallback(TDataSample *Data, int DataCount) {
   if(DataCount == 0) {
      return;
   }

   char StringBuffer[20];
   TWeight *Weight = &Data->Weight;
   TStableWeight *StableBuffer = &Data->StableWeight;
   TPs081Weight *Ps081Weight = &Data->Ps081;

   double *WeightSamples = (double *)malloc(sizeof(double) * DataCount);

   Form1->DataStringGrid->ColCount = DataCount + 1;

   switch(Form1->Detector->ModeMode) {
      case DETECTOR_MODE_ALL:
         for(int i = 0 ; i < DataCount ; i++) {
            sprintf(StringBuffer, "%0.2f", TBat2Detector::TWeightToG(Weight[i]));
            Form1->DataStringGrid->Cells[ i + 1 ][ 0 ] = StringBuffer;

            WeightSamples[i] = TBat2Detector::TWeightToG(Weight[i]);
         }
         break;

      case DETECTOR_MODE_STABLE:
         for(int i = 0 ; i < DataCount ; i++) {
            sprintf(StringBuffer, "%0.2f", TBat2Detector::TWeightToG(StableBuffer[i].Weight));
            Form1->DataStringGrid->Cells[ i + 1 ][ 0 ] = StringBuffer;
            sprintf(StringBuffer, "%d", StableBuffer[i].Time);
            Form1->DataStringGrid->Cells[ i + 1 ][ 1 ] = StringBuffer;

            WeightSamples[i] = TBat2Detector::TWeightToG(StableBuffer[i].Weight);
         }
         break;

      case DETECTOR_MODE_PS081:
         for(int i = 0 ; i < DataCount ; i++) {
            sprintf(StringBuffer, "%0.2f", TBat2Detector::TWeightToG(Ps081Weight[i].Weight));
            Form1->DataStringGrid->Cells[ i + 1 ][ 0 ] = StringBuffer;

            if(Ps081Weight[i].Stable) {
               Form1->DataStringGrid->Cells[ i + 1 ][ 1 ] = "*";
            } else {
               Form1->DataStringGrid->Cells[ i + 1 ][ 1 ] = "";
            }

            sprintf(StringBuffer, "%d", Ps081Weight[i].Raw);
            Form1->DataStringGrid->Cells[ i + 1 ][ 2 ] = StringBuffer;

            sprintf(StringBuffer, "%0.2f", TBat2Detector::TWeightToG(Ps081Weight[i].LowPassInput));
            Form1->DataStringGrid->Cells[ i + 1 ][ 3 ] = StringBuffer;

            sprintf(StringBuffer, "%0.2f", TBat2Detector::TWeightToG(Ps081Weight[i].LowPassOutput));
            Form1->DataStringGrid->Cells[ i + 1 ][ 4 ] = StringBuffer;

            sprintf(StringBuffer, "%d", Ps081Weight[i].PreAvgCounter);
            Form1->DataStringGrid->Cells[ i + 1 ][ 5 ] = StringBuffer;

            sprintf(StringBuffer, "%0.2f", TBat2Detector::TWeightToG(Ps081Weight[i].HighPassAbs));
            Form1->DataStringGrid->Cells[ i + 1 ][ 6 ] = StringBuffer;

            sprintf(StringBuffer, "%d", Ps081Weight[i].StableWindowCounter);
            Form1->DataStringGrid->Cells[ i + 1 ][ 7 ] = StringBuffer;

            WeightSamples[i] = TBat2Detector::TWeightToG(Ps081Weight[i].Weight);
         }
         break;
   }

   sprintf(StringBuffer, "%0.2f", StdDev(WeightSamples, DataCount));
   Form1->StdDevLabel->Caption = StringBuffer;

   sprintf(StringBuffer, "%0.2f", DeltaMax(WeightSamples, DataCount));
   Form1->DeltaMaxLabel->Caption = StringBuffer;

   sprintf(StringBuffer, "%0.2f", Mean(WeightSamples, DataCount));
   Form1->MeanLabel->Caption = StringBuffer;

   free(WeightSamples);
}

//******************************************************************************
// Update Calibration Data
//******************************************************************************

void TForm1::UpdateCalibrationData() {
   char StringBuffer[10];

   sprintf(StringBuffer, "%0.2f", Detector->CalibrationZero);
   ZeroEdit->Text = StringBuffer;

   sprintf(StringBuffer, "%d", Detector->CalibrationRawZero);
   RawZeroEdit->Text = StringBuffer;

   sprintf(StringBuffer, "%0.2f", Detector->CalibrationFullRange);
   FullRangeEdit->Text = StringBuffer;

   sprintf(StringBuffer, "%d", Detector->CalibrationRawFullRange);
   RawFullRangeEdit->Text = StringBuffer;
}


static void Replace(byte *Src, int SrcLength, byte *Search, byte *ReplaceBy, int PieceLength) {
   if(SrcLength <= 0) {
      return;
   }

   if(PieceLength > SrcLength) {
      return;
   }

   int SrcPointer = 0;
   int PiecePointer = 0;

   while(SrcPointer < SrcLength) {
      if(Src[SrcPointer] == Search[PiecePointer]) {
         PiecePointer++;

         if(PiecePointer == PieceLength) {
            memcpy(Src + SrcPointer - PieceLength + 1, ReplaceBy, PieceLength);
            PiecePointer = 0;
         }
      } else {
         PiecePointer = 0;
      }

      SrcPointer++;
   }
}

static double StdDev(double *Data, int Count) {
   int i = Count;
   double MeanValue = Mean(Data, Count);
   double StdDev = 0;

   while(i--) {
      StdDev += (Data[i] - MeanValue) * (Data[i] - MeanValue);
   }

   return sqrt(StdDev / Count);
}

static double Mean(double *Data, int Count) {
   int i = Count;
   double Mean = 0;

   while(i--) {
      Mean += Data[i];
   }

   return Mean /= Count;
}

static double DeltaMax(double *Data, int Count) {
   return Max(Data, Count) - Min(Data, Count);
}

static double Max(double *Data, int Count)
{
   double Maximum = -DBL_MAX ;

   while(Count--) {
      if(Data[Count] > Maximum) {
         Maximum = Data[Count];
      }
   }

   return Maximum;
}

static double Min(double *Data, int Count)
{
   double Minimum = DBL_MAX;

   while(Count--) {
      if(Data[Count] < Minimum) {
         Minimum = Data[Count];
      }
   }
                       
   return Minimum;
}
void __fastcall TForm1::AdcConfigFileButtonClick(TObject *Sender)
{
   if( !SimulatorFileOpenDialog->Execute()){
      return;
   }

   SimulatorFileName = SimulatorFileOpenDialog->FileName;
   SimulatorFileLabel->Caption = SimulatorFileOpenDialog->FileName;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::AdcResetTimerTimer(TObject *Sender)
{
   AdcTicks++;

   //if(AdcTicks >)
   //Detector->AdcAccuracy++;

   //Detector->ResetAdc();
}
//---------------------------------------------------------------------------


