object Form1: TForm1
  Left = 2085
  Top = 221
  Width = 1265
  Height = 599
  AutoSize = True
  Caption = 'Bat2Detector - Ps081'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object StatusLabel: TLabel
    Left = 136
    Top = 8
    Width = 97
    Height = 38
    Caption = 'Status'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Arial Black'
    Font.Style = []
    ParentFont = False
  end
  object StdDevLabel: TLabel
    Left = 1040
    Top = 224
    Width = 21
    Height = 13
    Caption = '0.00'
  end
  object Label3: TLabel
    Left = 976
    Top = 224
    Width = 31
    Height = 13
    Caption = 'Stdev:'
  end
  object FileLabel: TLabel
    Left = 248
    Top = 224
    Width = 46
    Height = 13
    Caption = 'File: none'
  end
  object Label15: TLabel
    Left = 1128
    Top = 224
    Width = 50
    Height = 13
    Caption = 'Delta max:'
  end
  object DeltaMaxLabel: TLabel
    Left = 1200
    Top = 224
    Width = 21
    Height = 13
    Caption = '0.00'
  end
  object Label16: TLabel
    Left = 832
    Top = 224
    Width = 27
    Height = 13
    Caption = 'Mean'
  end
  object MeanLabel: TLabel
    Left = 888
    Top = 224
    Width = 21
    Height = 13
    Caption = '0.00'
  end
  object ComEdit: TEdit
    Left = 0
    Top = 0
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'COM3'
  end
  object StartButton: TButton
    Left = 0
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 1
    OnClick = StartButtonClick
  end
  object StopButton: TButton
    Left = 80
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Stop + Save'
    TabOrder = 2
    OnClick = StopButtonClick
  end
  object DataStringGrid: TStringGrid
    Left = 0
    Top = 248
    Width = 1225
    Height = 313
    DefaultColWidth = 65
    RowCount = 1
    FixedRows = 0
    TabOrder = 3
  end
  object FileOpenButton: TButton
    Left = 160
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Open File'
    TabOrder = 4
    OnClick = FileOpenButtonClick
  end
  object GroupBox1: TGroupBox
    Left = 488
    Top = 56
    Width = 153
    Height = 153
    Caption = 'Detection'
    TabOrder = 5
    object Label5: TLabel
      Left = 11
      Top = 24
      Width = 134
      Height = 13
      Caption = 'Averaging window [samples]'
    end
    object Label6: TLabel
      Left = 12
      Top = 64
      Width = 59
      Height = 13
      Caption = '+- Range [g]'
    end
    object Label7: TLabel
      Left = 13
      Top = 108
      Width = 116
      Height = 13
      Caption = 'Stable window [samples]'
    end
    object AveragingWindowEdit: TEdit
      Left = 8
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object AbsoluteRangeEdit: TEdit
      Left = 8
      Top = 84
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object StableWindowEdit: TEdit
      Left = 8
      Top = 124
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 56
    Width = 233
    Height = 153
    Caption = 'ADC'
    TabOrder = 6
    object Label1: TLabel
      Left = 11
      Top = 24
      Width = 62
      Height = 13
      Caption = 'Accuracy [%]'
    end
    object Label2: TLabel
      Left = 12
      Top = 64
      Width = 45
      Height = 13
      Caption = 'Rate [Hz]'
    end
    object Label8: TLabel
      Left = 12
      Top = 104
      Width = 82
      Height = 13
      Caption = 'Prefilter [samples]'
      Color = clBtnFace
      ParentColor = False
    end
    object Label9: TLabel
      Left = 147
      Top = 32
      Width = 22
      Height = 13
      Caption = 'Filter'
      Color = clBtnFace
      ParentColor = False
    end
    object AccuracyEdit: TEdit
      Left = 8
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object RateEdit: TEdit
      Left = 8
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Panel2: TPanel
      Left = 144
      Top = 56
      Width = 73
      Height = 81
      TabOrder = 2
      object AdcFilterNoneRadioButton: TRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'None'
        TabOrder = 0
      end
      object AdcFilterSinc3RadioButton: TRadioButton
        Left = 8
        Top = 32
        Width = 113
        Height = 17
        Caption = 'Sinc3'
        TabOrder = 1
      end
      object AdcFilterSinc5RadioButton: TRadioButton
        Left = 8
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Sinc5'
        TabOrder = 2
      end
    end
  end
  object GroupBox3: TGroupBox
    Left = 240
    Top = 56
    Width = 241
    Height = 153
    Caption = 'Mode'
    TabOrder = 7
    object Label4: TLabel
      Left = 100
      Top = 16
      Width = 45
      Height = 13
      Caption = 'Burst size'
    end
    object Label10: TLabel
      Left = 100
      Top = 56
      Width = 95
      Height = 13
      Caption = 'Averaging [samples]'
      Color = clRed
      ParentColor = False
    end
    object Panel1: TPanel
      Left = 8
      Top = 16
      Width = 73
      Height = 105
      TabOrder = 0
      object DefaultModeRadioButton: TRadioButton
        Left = 8
        Top = 8
        Width = 113
        Height = 17
        Caption = 'Default'
        Color = clRed
        ParentColor = False
        TabOrder = 0
      end
      object AllModeRadioButton: TRadioButton
        Left = 8
        Top = 32
        Width = 113
        Height = 17
        Caption = 'All'
        TabOrder = 1
      end
      object StableModeRadioButton: TRadioButton
        Left = 8
        Top = 56
        Width = 113
        Height = 17
        Caption = 'Stable'
        TabOrder = 2
      end
      object Ps081ModeRadioButton: TRadioButton
        Left = 8
        Top = 80
        Width = 113
        Height = 17
        Caption = 'Ps081'
        TabOrder = 3
      end
    end
    object ModeBurstSizeEdit: TEdit
      Left = 96
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object ModeAveragingEdit: TEdit
      Left = 96
      Top = 76
      Width = 121
      Height = 21
      TabOrder = 2
    end
  end
  object PrefilterEdit: TEdit
    Left = 8
    Top = 176
    Width = 121
    Height = 21
    TabOrder = 8
  end
  object GroupBox4: TGroupBox
    Left = 648
    Top = 56
    Width = 265
    Height = 153
    Caption = 'Calibration data'
    TabOrder = 9
    object Label11: TLabel
      Left = 12
      Top = 24
      Width = 37
      Height = 13
      Caption = 'Zero [g]'
    end
    object Label12: TLabel
      Left = 12
      Top = 64
      Width = 61
      Height = 13
      Caption = 'Full range [g]'
    end
    object Label13: TLabel
      Left = 140
      Top = 24
      Width = 74
      Height = 13
      Caption = 'Raw zero [LSB]'
    end
    object Label14: TLabel
      Left = 140
      Top = 64
      Width = 97
      Height = 13
      Caption = 'Raw full range [LSB]'
    end
    object ZeroEdit: TEdit
      Left = 8
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object FullRangeEdit: TEdit
      Left = 8
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object RawZeroEdit: TEdit
      Left = 136
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object RawFullRangeEdit: TEdit
      Left = 136
      Top = 80
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object ReadCalibrationButton: TButton
      Left = 152
      Top = 112
      Width = 99
      Height = 25
      Caption = 'Read calibration'
      TabOrder = 4
      OnClick = ReadCalibrationButtonClick
    end
    object CalibrateButton: TButton
      Left = 32
      Top = 112
      Width = 75
      Height = 25
      Caption = 'Calibrate'
      TabOrder = 5
      OnClick = CalibrateButtonClick
    end
  end
  object GroupBox5: TGroupBox
    Left = 920
    Top = 56
    Width = 137
    Height = 153
    Caption = 'Ps081firmware'
    TabOrder = 10
    object HexFileLabel: TLabel
      Left = 11
      Top = 64
      Width = 30
      Height = 13
      Caption = 'No file'
    end
    object HexFileOpenButton: TButton
      Left = 6
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Select file'
      TabOrder = 0
      OnClick = HexFileOpenButtonClick
    end
    object ProgramButton: TButton
      Left = 6
      Top = 112
      Width = 75
      Height = 25
      Caption = 'Program'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBtnText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = ProgramButtonClick
    end
    object ParseButton: TButton
      Left = 8
      Top = 80
      Width = 75
      Height = 25
      Caption = 'Parse'
      TabOrder = 2
      OnClick = ParseButtonClick
    end
  end
  object GroupBox6: TGroupBox
    Left = 1064
    Top = 56
    Width = 185
    Height = 153
    Caption = 'ALCS-350'
    TabOrder = 11
    object SimulatorFileLabel: TLabel
      Left = 11
      Top = 84
      Width = 46
      Height = 13
      Caption = 'File: none'
    end
    object SimulatorComEdit: TEdit
      Left = 8
      Top = 20
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'COM2'
    end
    object SimulatorFileOpenButton: TButton
      Left = 6
      Top = 48
      Width = 75
      Height = 25
      Caption = 'Simulator file'
      TabOrder = 1
      OnClick = SimulatorFileOpenButtonClick
    end
    object SimulationEnableCheckBox: TCheckBox
      Left = 8
      Top = 120
      Width = 97
      Height = 17
      Caption = 'Enable'
      TabOrder = 2
    end
  end
  object AdcConfigFileButton: TButton
    Left = 374
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Adc Config file'
    TabOrder = 12
    OnClick = AdcConfigFileButtonClick
  end
  object CommentEdit: TEdit
    Left = 528
    Top = 16
    Width = 113
    Height = 21
    TabOrder = 13
    Text = 'No Comment'
  end
  object FileOpenDialog: TOpenDialog
    Left = 304
    Top = 216
  end
  object HexFileOpenDialog: TOpenDialog
    Left = 1008
    Top = 72
  end
  object SimulatorTimer: TTimer
    Enabled = False
    OnTimer = SimulatorTimerTimer
    Left = 1160
    Top = 104
  end
  object SimulatorFileOpenDialog: TOpenDialog
    Left = 1200
    Top = 104
  end
  object AdcConfigFileOpenDialog: TOpenDialog
    Left = 328
    Top = 8
  end
  object AdcResetTimer: TTimer
    Enabled = False
    Interval = 4000
    OnTimer = AdcResetTimerTimer
    Left = 192
    Top = 72
  end
end
