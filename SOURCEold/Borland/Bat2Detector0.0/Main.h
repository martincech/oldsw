//******************************************************************************
//
//   Main.h     Bat2 Detector main
//   Version 1.0  (c) Veit Electronics
//
//******************************************************************************

//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Library/Bat2Detector/Detector.h"
#include "../Library/Picostrain/Alcs350.h"
#include <Grids.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>

#define SIMULATION_PROFILE_LENGTH_MAX   1000
#define CODE_MAX_LENGTH          2048
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
   TEdit *ComEdit;
   TLabel *StatusLabel;
   TButton *StartButton;
   TButton *StopButton;
   TStringGrid *DataStringGrid;
   TLabel *StdDevLabel;
   TOpenDialog *FileOpenDialog;
   TButton *FileOpenButton;
   TGroupBox *GroupBox1;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TEdit *AveragingWindowEdit;
   TEdit *AbsoluteRangeEdit;
   TEdit *StableWindowEdit;
   TGroupBox *GroupBox2;
   TLabel *Label1;
   TLabel *Label2;
   TEdit *AccuracyEdit;
   TEdit *RateEdit;
   TGroupBox *GroupBox3;
   TLabel *Label4;
   TPanel *Panel1;
   TRadioButton *DefaultModeRadioButton;
   TRadioButton *AllModeRadioButton;
   TRadioButton *StableModeRadioButton;
   TRadioButton *Ps081ModeRadioButton;
   TEdit *ModeBurstSizeEdit;
   TLabel *Label3;
   TEdit *PrefilterEdit;
   TLabel *Label8;
   TPanel *Panel2;
   TRadioButton *AdcFilterNoneRadioButton;
   TRadioButton *AdcFilterSinc3RadioButton;
   TRadioButton *AdcFilterSinc5RadioButton;
   TLabel *Label9;
   TEdit *ModeAveragingEdit;
   TLabel *Label10;
   TGroupBox *GroupBox4;
   TEdit *ZeroEdit;
   TEdit *FullRangeEdit;
   TEdit *RawZeroEdit;
   TEdit *RawFullRangeEdit;
   TButton *ReadCalibrationButton;
   TButton *CalibrateButton;
   TLabel *FileLabel;
   TLabel *Label11;
   TLabel *Label12;
   TLabel *Label13;
   TLabel *Label14;
   TGroupBox *GroupBox5;
   TButton *HexFileOpenButton;
   TButton *ProgramButton;
   TLabel *HexFileLabel;
   TOpenDialog *HexFileOpenDialog;
   TTimer *SimulatorTimer;
   TOpenDialog *SimulatorFileOpenDialog;
   TGroupBox *GroupBox6;
   TEdit *SimulatorComEdit;
   TButton *SimulatorFileOpenButton;
   TLabel *SimulatorFileLabel;
   TButton *ParseButton;
   TCheckBox *SimulationEnableCheckBox;
   TLabel *Label15;
   TLabel *DeltaMaxLabel;
   TLabel *Label16;
   TLabel *MeanLabel;
   TOpenDialog *AdcConfigFileOpenDialog;
   TButton *AdcConfigFileButton;
   TTimer *AdcResetTimer;
   TEdit *CommentEdit;
   void __fastcall StartButtonClick(TObject *Sender);
   void __fastcall StopButtonClick(TObject *Sender);
   void __fastcall FileOpenButtonClick(TObject *Sender);
   void __fastcall ProgramButtonClick(TObject *Sender);
   void __fastcall ReadCalibrationButtonClick(TObject *Sender);
   void __fastcall CalibrateButtonClick(TObject *Sender);
   void __fastcall HexFileOpenButtonClick(TObject *Sender);
   void __fastcall SimulatorTimerTimer(TObject *Sender);
   void __fastcall SimulatorFileOpenButtonClick(TObject *Sender);
   void __fastcall ParseButtonClick(TObject *Sender);
   void __fastcall AdcConfigFileButtonClick(TObject *Sender);
   void __fastcall AdcResetTimerTimer(TObject *Sender);
private:	// User declarations
   // Simulator
   typedef struct {
      float Strain;
      int Seconds;
   } TSimulationProfile;

   TSimulationProfile SimulationProfile[SIMULATION_PROFILE_LENGTH_MAX];
   int SimulationProfileLength;
   bool SimulationProfileRepeat;
   int SimulationProfilePtr;
   long SimulatorTicks;

   long AdcTicks;

   // Files
   AnsiString HexFileName;
   AnsiString SimulatorFileName;

   // Code
   byte Code[CODE_MAX_LENGTH];
   int CodeLength;

   void UpdateCalibrationData();

   TAlcs350 *Simulator;
public:		// User declarations
   __fastcall TForm1(TComponent* Owner);
   TBat2Detector *Detector;
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
