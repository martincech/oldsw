//******************************************************************************
//
//   Main.h     Conversion utility main form
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainCsvH
#define MainCsvH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <Dialogs.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>

#include "UniParser.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TActionList *ActionList;
        TAction *FileOpen;
        TAction *FileExit;
        TAction *FileSave;
        TMainMenu *MainMenu;
        TMenuItem *FileMenu;
        TMenuItem *FileOpenItem;
        TMenuItem *FileSaveItem;
        TMenuItem *FileExitItem;
        TOpenDialog *FileOpenDialog;
        TSaveDialog *FileSaveDialog;
        TStatusBar *StatusBar;
        TPageControl *MainPageControl;
        TTabSheet *MainInputSheet;
        TTabSheet *MainHeaderSheet;
        TMemo *MainInput;
        TMemo *MainHeader;
        TTabSheet *MainSourceSheet;
        TMemo *MainSource;
        TCheckBox *CbCharFormat;
   TCheckBox *CbAvr32;
        void __fastcall FileOpenExecute(TObject *Sender);
        void __fastcall FileSaveExecute(TObject *Sender);
        void __fastcall FileExitExecute(TObject *Sender);
        void __fastcall CreateMain(TObject *Sender);
        void __fastcall CbCharFormatClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
   AnsiString FileName;    // nazev pracovniho souboru
   bool       Touch;
   TUniParser *Parser;
   bool __fastcall ConvertData( AnsiString FileName);
   // konverze dat
   void __fastcall ShowInput();
   // zobrazeni vstupu
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
