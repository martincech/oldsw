//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

typedef enum {
   // system :
   STR_NULL, // undefined string

// Bat1 texts

   STR_OK              , 
   STR_ERR             , // Error
// New line
   STR_CANCEL          , 
   STR_NEW             , 


   // system :
   _STR_LAST // strings count
} TStrEnum;

#endif
