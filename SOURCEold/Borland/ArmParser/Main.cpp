//******************************************************************************
//
//   Main.cpp     Conversion utility main form
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"

#include <string.h>
#include <stdio.h>
#include "uni.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define NONAME_FILE AnsiString("noname")

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   FileName = NONAME_FILE;
   Touch    = false;
   Parser   = new TTxtParser;
} // TMainForm

//******************************************************************************
// File Open
//******************************************************************************

void __fastcall TMainForm::FileOpenExecute(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   MainPageControl->ActivePage = MainInputSheet;   // Aktivni vstupni stranka
   try {
      MainInput->Lines->LoadFromFile( FileOpenDialog->FileName);
      FileName = FileOpenDialog->FileName;
   } catch(...){
      char buff[ 512];
      sprintf( buff, "Unable open '%s'", FileOpenDialog->FileName.c_str());
      Application->MessageBox( buff, "Error", MB_OK);
      FileName = NONAME_FILE;
   }
   StatusBar->Panels->Items[ 1]->Text = FileName;
   Touch    = true;          // modifikovano
} // FileOpenExecute

//******************************************************************************
// File Save
//******************************************************************************

void __fastcall TMainForm::FileSaveExecute(TObject *Sender)
{
   FileSaveDialog->FileName = ChangeFileExt( FileName, ".h" );
   if( !FileSaveDialog->Execute()){
      return;
   }
   if( FileExists( FileSaveDialog->FileName)){
      if( Application->MessageBox( "File already exists, overwrite ?",
          "Confirmation", MB_OKCANCEL) != IDOK){
          return;
      }
   }
   if( Touch){
      // neni konvertovan
      Screen->Cursor = crHourGlass;
      if( !ConvertData()){
         Screen->Cursor = crDefault;    // zrus hodiny
         return;                        // nelze konvertovat
      }
      Screen->Cursor = crDefault;       // zrus hodiny
      Touch = false;                    // zkonvertovano
   }
   MainHeader->Lines->SaveToFile( FileSaveDialog->FileName);
   FileSaveDialog->FileName = ChangeFileExt( FileName, ".c" );
   MainSource->Lines->SaveToFile( FileSaveDialog->FileName);
   FileSaveDialog->FileName = ChangeFileExt( FileName, ".csv" );
   MainCsv->Lines->SaveToFile( FileSaveDialog->FileName);
} // FileSaveExecute

//******************************************************************************
// File Exit
//******************************************************************************

void __fastcall TMainForm::FileExitExecute(TObject *Sender)
{
   Close();
} // FileExitExecute

//******************************************************************************
// Pred zmenou zalozky
//******************************************************************************

void __fastcall TMainForm::MainPageControlChanging(TObject *Sender,
      bool &AllowChange)
{
   if( MainPageControl->ActivePageIndex != 0){
      return;        // zalozka Input
   }
   if( !Touch){
      return;        // vstupni soubor nezmenen
   }
   Screen->Cursor = crHourGlass;
   if( !ConvertData()){
      Screen->Cursor = crDefault;    // zrus hodiny
      return;                        // nelze konvertovat
   }
   Screen->Cursor = crDefault;       // zrus hodiny
   Touch = false;                    // zkonvertovano
} // MainPageControlChanging

//******************************************************************************
// Konverze
//******************************************************************************

bool __fastcall TMainForm::ConvertData()
{
   MainHeader->Lines->Clear();
   MainSource->Lines->Clear();
   MainCsv->Lines->Clear();
   Parser->CharPtrMode = CbCharFormat->Checked;
   Parser->Avr32Mode   = CbAvr32->Checked;
   if( !Parser->Parse()){
      Application->MessageBox( "Syntax error", "Error", MB_OK);
      return( false);
   }
   return( true);
} // ConvertData

//******************************************************************************
// OnCreate
//******************************************************************************

void __fastcall TMainForm::CreateMain(TObject *Sender)
{
   Parser->InStream  = new TInStream( MainInput->Lines);
   Parser->HeaderStream = MainHeader->Lines;
   Parser->SourceStream = MainSource->Lines;
   Parser->CsvStream    = MainCsv->Lines;
} // CreateMain

//******************************************************************************
// Char format
//******************************************************************************

void __fastcall TMainForm::CbCharFormatClick(TObject *Sender)
{
   // neni konvertovan
   Screen->Cursor = crHourGlass;
   if( !ConvertData()){
      Screen->Cursor = crDefault;    // zrus hodiny
      return;                        // nelze konvertovat
   }
   Screen->Cursor = crDefault;       // zrus hodiny
   Touch = false;                    // zkonvertovano
} // CbCharFormatClick

