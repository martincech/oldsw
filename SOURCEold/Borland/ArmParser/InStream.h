//******************************************************************************
//
//   InStream.h Input stream reader
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef InStreamH
#define InStreamH

//******************************************************************************
// TInStream
//******************************************************************************

class TInStream
{
private:
   TStrings *Input;
   int      RowsCount;     // aktualni pocet radku
   int      ColumnsCount;  // aktualni pocet sloupcu na radku

protected:
   int CurrentRow;
   int CurrentColumn;

public:
   typedef enum {
      INS_EOF = -1,        // end of file
      INS_EOL = -2,        // end of line
   } TSpecialCharacter;

   __fastcall TInStream( TStrings *Text);
   // Konstruktor

   int __fastcall GetCharacter();
   // nacteni znaku ze vstupniho proudu
   void __fastcall Restart();
   // znovu od zacatku textu

   __property int Row    = {read=CurrentRow};    // aktualni pozice 1..max
   __property int Column = {read=CurrentColumn};
};

#endif
