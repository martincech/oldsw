//******************************************************************************
//
//   CodePage.cpp Code Page translations
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "uni.h"
#include "CodePage.h"
#include <stdio.h>

#define OCTAL_ESCAPE  1         // escape characters with octal base (else hexadecimal)

//******************************************************************************
// Constructor
//******************************************************************************

TCodePage::TCodePage()
// Constructor
{
   memset( Table, 0, sizeof( Table));
} // TCodePage

//******************************************************************************
// Load
//******************************************************************************

#define BUFFER_SIZE 256

bool TCodePage::Load( int CodePage, String FileName)
// Load from file
{
FILE   *f;
static wchar_t Buffer[ BUFFER_SIZE + 1];

   if( CodePage >= CODE_PAGE_COUNT){
      IERROR;
   }
   f = fopen( FileName.c_str(), "rb");
   if( !f){
      return( false);
   }
   // unicode file signature :
   int ch;
   ch = fgetc( f);
   if( ch == EOF){
      fclose( f);
      return( false);
   }
   if( ch != 0xFF){
      fclose( f);
      return( false);
   }
   ch = fgetc( f);
   if( ch == EOF){
      fclose( f);
      return( false);
   }
   if( ch != 0xFE){
      fclose( f);
      return( false);
   }
   // read table :
   for( int i = 0; i < CODE_PAGE_CHARACTERS; i++){
      if( !fgetws( Buffer, BUFFER_SIZE, f)){
         break;
      }
      Table[ CodePage][ i] = Buffer[ 0];
   }
   Table[ CodePage]['\0'] = L'\0';     // replace null character (space) with zero
   Table[ CodePage]['\n'] = L'\n';     // replace LF character
   fclose( f);
   return( true);
} // Load

//******************************************************************************
// To
//******************************************************************************

void TCodePage::To( int CodePage, char *Dst, wchar_t *Src)
// Translate to codepage
{
   if( CodePage >= CODE_PAGE_COUNT){
      IERROR;
   }
   if( !FWithEscape){
      int i = 0;
      while( Src[ i]){
         Dst[ i] = CharTo( CodePage, Src[ i]);
         i++;
      }
      Dst[ i] = 0;
      return;
   }
   int i = 0;
   int j = 0;
   while( Src[ i]){
      EscapeCharTo( CodePage, Src[ i], Dst, j);
      i++;
   }
   Dst[ j] = 0;
} // To

//******************************************************************************
// From
//******************************************************************************

void TCodePage::From( int CodePage, wchar_t *Dst, char *Src)
// Translate from codepage
{
   if( CodePage >= CODE_PAGE_COUNT){
      IERROR;
   }
   if( !FWithEscape){
      int i = 0;
      while( Src[ i]){
         Dst[ i] = CharFrom( CodePage, Src[ i]);
         i++;
      }
      Dst[ i] = 0;
      return;
   }
   int i = 0;
   int j = 0;
   forever {
      if( !EscapeCharFrom( CodePage, Src, i, Dst[ j])){
         break;
      }
      j++;
   }
   Dst[ j] = 0;
} // From

//******************************************************************************
// To
//******************************************************************************

byte TCodePage::CharTo( int CodePage, wchar_t ch)
// Translate character to codepage
{
   // printable ASCII :
   if( ch >= L' ' && ch <= L'~'){
      return( (byte)ch);
   }
   // by codepage :
   for( int i = 0; i < CODE_PAGE_CHARACTERS; i++){
      if( Table[ CodePage][ i] == ch){
         return( i);
      }
   }
   return( CHAR_UNDEFINED);  // unknown char
} // CharTo

//******************************************************************************
// From
//******************************************************************************

wchar_t TCodePage::CharFrom( int CodePage, char ch)
// Translate character from codepage
{
   return( Table[ CodePage][ ch & 0xFF]);
} // CharFrom

//******************************************************************************
// Size
//******************************************************************************

int TCodePage::Size( int CodePage)
// Returns real table size
{
   for( int i = CODE_PAGE_CHARACTERS - 1; i >= 0; i--){
      if( Table[ CodePage][ i] != L'\0'){
         return( i + 1);
      }
   }
   return( 0);
} // Size

//------------------------------------------------------------------------------

//******************************************************************************
// Escape To
//******************************************************************************

void TCodePage::EscapeCharTo( int CodePage, wchar_t ch, char *Dst, int &Index)
// Translate character to codepage
{
byte CpChar;

   // printable ASCII :
   if( ch >= L' ' && ch <= L'~'){
      Dst[ Index++] = (byte)ch;
      return;
   }
   // by codepage :
   CpChar = 0;
   for( int i = 0; i < CODE_PAGE_CHARACTERS; i++){
      if( Table[ CodePage][ i] == ch){
         CpChar = i;
         break;
      }
   }
   if( CpChar == 0){
      Dst[ Index++] = CHAR_UNDEFINED;  // unknown char
      return;
   }
   if( !IsEscape( CpChar)){
      Dst[ Index++] = CpChar;
      return;
   }
#ifndef OCTAL_ESCAPE
   sprintf( &Dst[ Index], "\\x%02X", (unsigned)CpChar);
   Index += 4;
#else
   sprintf( &Dst[ Index], "\\%03o", (unsigned)CpChar);
   Index += 4;
#endif
} // EscapeCharTo

//******************************************************************************
// Escape From
//******************************************************************************

bool TCodePage::EscapeCharFrom( int CodePage, char *Src, int &Index, wchar_t &ch)
// Translate character from codepage
{
int CpChar;

   if( Src[ Index] == L'\0'){
      return( false);
   }
#ifndef OCTAL_ESCAPE
   if( Src[ Index] == '\\' && Src[ Index + 1] == 'x'){
      // escape
      if( Src[ Index + 2] == L'\0' ||
          Src[ Index + 3] == L'\0'){
         Index++;
         ch = Table[ CodePage][ '.'];         // scan error
         return( true);
      }
      CpChar  = CharToHex( Src[ Index + 2]);
      CpChar *= 16;
      CpChar += CharToHex( Src[ Index + 3]);
      Index  += 4;
#else
   if( Src[ Index] == '\\'){
      // escape
      if( Src[ Index + 1] == L'\0' ||
          Src[ Index + 2] == L'\0' ||
          Src[ Index + 2] == L'\0'){
         Index++;
         ch = Table[ CodePage][ '.'];         // scan error
         return( true);
      }
      CpChar  = CharToHex( Src[ Index + 1]);
      CpChar *= 8;
      CpChar += CharToHex( Src[ Index + 2]);
      CpChar *= 8;
      CpChar += CharToHex( Src[ Index + 3]);
      Index  += 4;
#endif
      ch = Table[ CodePage][ CpChar];      // scan error
      if( CpChar < CODE_PAGE_CHARACTERS){
         ch = Table[ CodePage][ CpChar];
         return( true);
      }
      ch = Table[ CodePage][ '.'];         // scan error
      return( true);
   }
   ch = Table[ CodePage][ Src[ Index] & 0xFF];
   Index++;
   return( true);
} // EscapeCharFrom

//******************************************************************************
// To Hex
//******************************************************************************

int TCodePage::CharToHex( char ch)
// Convert char to hex
{
   if( ch >= '0' && ch <= '9'){
      return( ch - '0');
   }
   if( ch >= 'A' && ch <= 'F'){
      return( ch - 'A' + 10);
   }
   if( ch >= 'a' && ch <= 'f'){
      return( ch - 'a' + 10);
   }
   return( 0);
} // CharToHex

//******************************************************************************
// Is escape
//******************************************************************************

bool TCodePage::IsEscape( char ch)
// Check for escape characters
{
   if( ch >= ' ' && ch <= '~'){
      return( false);
   }
   return( true);
} // IsEscape

