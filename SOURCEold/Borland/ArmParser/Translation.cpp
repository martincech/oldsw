//******************************************************************************
//
//   Translation.cpp   Translation table
//   Version 0.0       (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Translation.h"
#include <stdio.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Nacteni
//******************************************************************************

bool __fastcall TTranslation::LoadFromFile( AnsiString FileName)
// nacte slovnik z disku
{
FILE   *f;
static char Buffer[ 512];
static char First[ 512], Second[512];

   f = fopen( FileName.c_str(), "r");
   if( !f){
      return( false);
   }
   while( 1){
      if( !fgets( Buffer, sizeof(Buffer), f)){
         break;
      }
      // dekodovani radku :
      int Index = 0;
      if( !ParseWord( Buffer, sizeof( Buffer) - 1, Index, First)){
         continue;
      }
      if( !ParseWord( Buffer, sizeof( Buffer) - 1, Index, Second)){
         continue;
      }
      string Keyword( First);
      string Data( Second);
      Translation[ Keyword] = Data;
   }
   fclose( f);
   return( true);
} // LoadFromFile

//******************************************************************************
// Cteni slova
//******************************************************************************

bool __fastcall TTranslation::ParseWord( char *Line, int LineLength, int &Index, char *Word)
// vyhleda slovo na radku od <Index>
{
   int Start = -1;
   for( int i = Index; i < LineLength; i++){
      switch( Line[ i]){
         case ' ' :
         case '\t' :
            continue;
         case '\"' :
            Start = i + 1;
            break;
         case '\r' :
         case '\n' :
            break;
         default :
            Start = i;
            break;
      }
      break;
   }
   if( Start < 0){
      return( false);       // dalsi radek
   }
   int Stop = -1;
   for( int i = Start; i < LineLength; i++){
      switch( Line[ i]){
         case '\t' :
         case '\"' :
         case '\r' :
         case '\n' :
            Stop = i;
            break;
         default :
            continue;
      }
      break;
   }
   if( Stop <= Start){
      return( false);
   }
   memcpy( Word, &Line[ Start], Stop - Start);
   Word[ Stop - Start] = '\0';
   Index = Stop + 1;
   return( true);
} // ParseWord

//******************************************************************************
// Preklad
//******************************************************************************

bool __fastcall TTranslation::Translate( AnsiString &Keyword, AnsiString &Result)
// prelozi <Keyword> a vrati jej v <Result>. Vrati false neni-li
// ve slovniku
{
   string Index( Keyword.c_str());
   string Data;
   try {
       Data = Translation[ Index];
   } catch( ...){
      return( false);
   }
   if( Data == string("")){
      return( false);
   }
   Result = AnsiString( Data.c_str());
   return( true);
} // Translate

