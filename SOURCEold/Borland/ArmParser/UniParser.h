//******************************************************************************
//
//   UniParser.h  Unicode CSV parser
//   Version 0.0  (c) Vymos
//
//******************************************************************************

#ifndef UniParserH
#define UniParserH

#include "CodePage.h"

#define MAX_LANGUAGE_COUNT  30

//******************************************************************************
//  DFM Parser
//******************************************************************************

class TUniParser
{
public:
   TUniParser();
   // Konstruktor
   ~TUniParser();
   // Destruktor
   bool Parse( AnsiString FileName);
   // konverze dat
   __property TStrings  *HeaderStream  = {write=Header};
   __property TStrings  *SourceStream  = {write=Source};
   __property bool       CharPtrMode   = {write=FPtrMode};
   __property bool       Avr32Mode     = {write=FAvrMode};
   __property TCodePage *Translator    = {read=FTranslator};
//------------------------------------------------------------------------------
private:
   // implementacni konstanty :
   typedef enum {
      MAX_KEY_LENGTH     = 128,    // max. delka klice
      MAX_STRING_LENGTH  = 128,    // max. delka retezce
   } TConstants;
protected :
   // typy lexikalnich symbolu :
   typedef enum {
      UNDEFINED      = 512,
      // jednoducha slova :
      STRING,
      COMMENT,
      NEWLINE,
   } TTokenType;

   typedef char TStringArray[ MAX_LANGUAGE_COUNT][ MAX_STRING_LENGTH + 1];

   // lokalni data :
   TStrings  *Header;
   TStrings  *Source;
   int       FLanguageCount;                  // count of languages
   int       FCodePage[ MAX_LANGUAGE_COUNT];  // code page number for language
   bool      FPtrMode;                        // generate char constants
   bool      FAvrMode;                        // generate AVR32 constants
   int       StrIndex;                        // ptr mode string index
   char      Key[ MAX_KEY_LENGTH + 1];
   TStringArray String;
   TCodePage *FTranslator;                    // code page translator

   TTokenType ParseWord( wchar_t *Line, int LineLength, int &Index, wchar_t *Word);
   // vyhleda slovo na radku od <Index>
   int CodePageToIndex( char *PageName);
   // konvertuje nazev kodove stranky na index
   void GenProlog();
   // inicializace pred zacatkem dekodovani
   void Error( int RowNumber, int ColumnNumber);
   // chyba syntaxe
   void GenNewline();
   // generovani noveho radku
   void GenComment( char *Comment);
   // generovani komentare
   void GenCouple( char *Key, TStringArray String, char *Comment);
   // generovani paru <Key> = <String>
   void GenEpilog();
   // generovani zaveru souboru
}; // UniParser

#endif

