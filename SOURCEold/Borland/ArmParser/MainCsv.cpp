//******************************************************************************
//
//   MainCsv.cpp  Conversion utility main form
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainCsv.h"

#include <string.h>
#include <stdio.h>
#include "uni.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define NONAME_FILE AnsiString("noname")

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   FileName = NONAME_FILE;
   Touch    = false;
   Parser   = new TUniParser;
} // TMainForm

//******************************************************************************
// File Open
//******************************************************************************

void __fastcall TMainForm::FileOpenExecute(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   MainPageControl->ActivePage = MainInputSheet;   // Aktivni vstupni stranka
   FileName = FileOpenDialog->FileName;
   if( !FileExists( FileOpenDialog->FileName)){
      char buff[ 512];
      sprintf( buff, "Unable open '%s'", FileOpenDialog->FileName.c_str());
      Application->MessageBox( buff, "Error", MB_OK);
      FileName = NONAME_FILE;
   }
   ShowInput();
   StatusBar->Panels->Items[ 1]->Text = FileName;
   Screen->Cursor = crHourGlass;
   ConvertData( FileName);
   Screen->Cursor = crDefault;       // zrus hodiny
} // FileOpenExecute

//******************************************************************************
// File Save
//******************************************************************************

void __fastcall TMainForm::FileSaveExecute(TObject *Sender)
{
   FileSaveDialog->FileName = ChangeFileExt( FileName, ".h" );
   if( !FileSaveDialog->Execute()){
      return;
   }
   if( FileExists( FileSaveDialog->FileName)){
      if( Application->MessageBox( "File already exists, overwrite ?",
          "Confirmation", MB_OKCANCEL) != IDOK){
          return;
      }
   }
   MainHeader->Lines->SaveToFile( FileSaveDialog->FileName);
   FileSaveDialog->FileName = ChangeFileExt( FileName, ".c" );
   MainSource->Lines->SaveToFile( FileSaveDialog->FileName);
} // FileSaveExecute

//******************************************************************************
// File Exit
//******************************************************************************

void __fastcall TMainForm::FileExitExecute(TObject *Sender)
{
   Close();
} // FileExitExecute

//******************************************************************************
// Konverze
//******************************************************************************

bool __fastcall TMainForm::ConvertData( AnsiString FileName)
{
   MainHeader->Lines->Clear();
   MainSource->Lines->Clear();
   Parser->CharPtrMode = CbCharFormat->Checked;
   Parser->Avr32Mode   = CbAvr32->Checked;
   if( !Parser->Parse( FileName)){
      Application->MessageBox( "Syntax error", "Error", MB_OK);
      return( false);
   }
   return( true);
} // ConvertData

//******************************************************************************
// OnCreate
//******************************************************************************

void __fastcall TMainForm::CreateMain(TObject *Sender)
{
   Parser->HeaderStream = MainHeader->Lines;
   Parser->SourceStream = MainSource->Lines;
   AnsiString ExePath = ExtractFilePath( Application->ExeName);
   if( !Parser->Translator->Load( 0, ExePath + "INTER.CPG")){  // normal
      Application->MessageBox( "Unable open file INTER.CPG", "Error", MB_OK);
   }
   if( !Parser->Translator->Load( 1, ExePath + "JAPAN.CPG")){  // japan
      Application->MessageBox( "Unable open file JAPAN.CPG", "Error", MB_OK);
   }
   if( !Parser->Translator->Load( 2, ExePath + "CYRILIC.CPG")){  // cyrilic
      Application->MessageBox( "Unable open file CYRILIC.CPG", "Error", MB_OK);
   }
   Parser->Translator->WithEscape = true;
} // CreateMain

//******************************************************************************
// Char format
//******************************************************************************

void __fastcall TMainForm::CbCharFormatClick(TObject *Sender)
{
   ConvertData( FileName);
} // CbCharFormatClick

//******************************************************************************
// Input
//******************************************************************************

#define BUFFER_SIZE 511

void __fastcall TMainForm::ShowInput()
{
FILE   *f;
static wchar_t Buffer[ BUFFER_SIZE + 1];
static char    Ansi[   BUFFER_SIZE + 1];

   MainInput->Lines->Clear();
   f = fopen( FileName.c_str(), "rb");
   if( !f){
      return;
   }
   // signatura unicode souboru :
   int ch;
   ch = fgetc( f);
   if( ch == EOF){
      fclose( f);
      Application->MessageBox( "Not unicode file", "Error", MB_OK);
      return;
   }
   if( ch != 0xFF){
      fclose( f);
      Application->MessageBox( "Not unicode file", "Error", MB_OK);
      return;
   }
   ch = fgetc( f);
   if( ch == EOF){
      fclose( f);
      Application->MessageBox( "Not unicode file", "Error", MB_OK);
      return;
   }
   if( ch != 0xFE){
      fclose( f);
      Application->MessageBox( "Not unicode file", "Error", MB_OK);
      return;
   }
   while( 1){
      if( !fgetws( Buffer, BUFFER_SIZE, f)){
         break;
      }
      int Length = wcslen( Buffer);
      Buffer[ Length - 2] = L'\0';     // skip \r\n
      // preved na multibyte :
      if( !::WideCharToMultiByte( 1250, 0, Buffer,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
         break;
      }
      MainInput->Lines->Append( Ansi);
   }
   fclose( f);
} // ShowInput

//******************************************************************************
// Code page From : test
//******************************************************************************

void __fastcall TMainForm::Button1Click(TObject *Sender)
{
FILE   *f;
FILE   *fo;
static wchar_t Buffer[ BUFFER_SIZE + 1];
static char    Ansi[   BUFFER_SIZE + 1];

   AnsiString ExePath = ExtractFilePath( Application->ExeName);
   f  = fopen( (ExePath + "Msg1.c").c_str(), "rb");
   fo = fopen( (ExePath + "Msg1.u").c_str(), "wb");
   fputc( 0xFF, fo);
   fputc( 0xFE, fo);
   while( 1){
      if( !fgets( Ansi, BUFFER_SIZE, f)){
         fclose( f);
         fclose( fo);
         return;
      }
      Parser->Translator->From( 1, Buffer, Ansi);
      fputws( Buffer, fo);
   }
} // Click

