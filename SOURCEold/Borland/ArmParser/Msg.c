//******************************************************************************
//
//  Str.c          Strings translations
//  Version 1.0    (c) Robot
//
//******************************************************************************

#include "Str.h"

//------------------------------------------------------------------------------
//   String definitions
//------------------------------------------------------------------------------

TAllStrings AllStrings = {
// system :
/* STR_NULL */
{
   "",
   ""
},

// Message Box :
/* STR_OK               */ 
{
   "Ok",
   "Ok"
},
/* STR_CANCEL           */ 
{
   "Cancel",
   "Cancel"
},
/* STR_YES              */ 
{
   "Yes",
   "Yes"
},
/* STR_NO               */ 
{
   "No",
   "No"
},

// Standard title :
/* STR_ERROR            */ 
{
   "Error",
   "Error"
},
/* STR_INFO             */ 
{
   "Info",
   "Info"
},
/* STR_CONFIRMATION     */ 
{
   "Confirmation",
   "Confirmation"
},

// Input box :
/* STR_INPUT            */ 
{
   "Input",
   "Input"
},
/* STR_OUT_OF_LIMITS    */ 
{
   "Out of limits",
   "Out of limits"
},

// Directory box :
/* STR_DIRECTORY_EMPTY  */ 
{
   "Directory empty",
   "Directory empty"
},

// Database box :
/* STR_REALLY_DELETE_RECORD */ 
{
   "Really delete record ?",
   "Really delete record ?"
},

// Text box :
/* STR_STRING_EMPTY     */ 
{
   "String empty",
   "String empty"
},

// User defined :
/* STR_ENTER_VALUE      */ 
{
   "Enter value 0..100",
   "Enter value 0..100"
},
/* STR_REALLY_CLEAR_ALL */ 
{
   "Really clear all ?",
   "Really clear all ?"
},
/* STR_FILE_EXISTS      */ 
{
   "File already exists",
   "File already exists"
},
/* STR_DIRECTORY_FULL   */ 
{
   "Directory full",
   "Directory full"
},
/* STR_REALLY_CLEAR     */ 
{
   "Really clear file ?",
   "Really clear file ?"
},
/* STR_REALLY_DELETE    */ 
{
   "Really delete file ?",
   "Really delete file ?"
},
/* STR_AUTOMATIC_MODE   */ 
{
   "Select automatic mode ?",
   "Select automatic mode ?"
},
/* STR_ENTER_DATE       */ 
{
   "Enter date",
   "Enter date"
},
/* STR_ENTER_TIME       */ 
{
   "Enter time",
   "Enter time"
},
/* STR_ENTER_PASSWORD   */ 
{
   "Enter password",
   "Enter password"
},
/* STR_REALLY_FORMAT    */ 
{
   "Really format ?",
   "Really format ?"
},
/* STR_ENTER_NAME       */ 
{
   "Enter name",
   "Enter name"
},
/* STR_ENTER_INERTIA    */ 
{
   "Enter inertia",
   "Enter inertia"
},
/* STR_SET_INTENSITY    */ 
{
   "Set intensity",
   "Set intensity"
},
// calibration :
/* STR_ENTER_WEIGHT     */ 
{
   "Enter weight",
   "Enter weight"
},
/* STR_RELEASE_WEIGHT   */ 
{
   "Release weight",
   "Release weight"
},
/* STR_CHARGE_WEIGHT    */ 
{
   "Charge weight",
   "Charge weight"
},
/* STR_WAIT             */ 
{
   "Wait...",
   "Wait..."
},

// Units enum :
/* STR_UNITS_KG         */ 
{
   "kg",
   "kg"
},
/* STR_UNITS_G          */ 
{
   "g",
   "g"
},
/* STR_UNITS_LB         */ 
{
   "lb",
   "lb"
},
/* STR_UNITS_OZ         */ 
{
   "Oz",
   "Oz"
},

// Saving mode enum :
/* STR_SAVING_MODE_AUTOMATIC */ 
{
   "Automatic",
   "Automatic"
},
/* STR_SAVING_MODE_MANUAL */ 
{
   "Manual 1",
   "Manual 1"
},
/* STR_SAVING_MODE_2OPTIONS */ 
{
   "Manual 2",
   "Manual 2"
},
/* STR_SAVING_MODE_3OPTIONS */ 
{
   "Manual 3",
   "Manual 3"
},

// Backlight mode enum :
/* STR_BACKLIGHT_MODE_AUTO */ 
{
   "Automatic",
   "Automatic"
},
/* STR_BACKLIGHT_MODE_ON */ 
{
   "Always on",
   "Always on"
},
/* STR_BACKLIGHT_MODE_OFF */ 
{
   "Always off",
   "Always off"
},

// Main menu :
/* STR_SELECT_FILE      */ 
{
   "Select file",
   "Select file"
},
/* STR_SHOW_DATA        */ 
{
   "Show data",
   "Show data"
},
/* STR_CLEAR_DATA       */ 
{
   "Clear data",
   "Clear data"
},
/* STR_PARAMETERS       */ 
{
   "Parameters",
   "Parameters"
},
/* STR_FILE_MANAGER     */ 
{
   "File manager",
   "File manager"
},
/* STR_OPTIONS          */ 
{
   "Options",
   "Options"
},

// File menu :
/* STR_EDIT_DATA        */ 
{
   "Edit data",
   "Edit data"
},
//STR_STATISTIC
/* STR_CREATE           */ 
{
   "Create",
   "Create"
},
/* STR_RENAME           */ 
{
   "Rename",
   "Rename"
},
/* STR_CLEAR            */ 
{
   "Clear",
   "Clear"
},
/* STR_DELETE           */ 
{
   "Delete",
   "Delete"
},
/* STR_CLEAR_ALL        */ 
{
   "Clear all",
   "Clear all"
},

// Options menu :
/* STR_SET_CLOCK        */ 
{
   "Set clock",
   "Set clock"
},
/* STR_SET_VOLUME       */ 
{
   "Set volume",
   "Set volume"
},
/* STR_SET_BACKLIGHT    */ 
{
   "Set backlight",
   "Set backlight"
},
/* STR_CALIBRATE        */ 
{
   "Calibrate",
   "Calibrate"
},
/* STR_FORMAT           */ 
{
   "Format",
   "Format"
},

// Parameters menu :
/* STR_MODE             */ 
{
   "Mode",
   "Mode"
},
/* STR_INERTIA          */ 
{
   "Inertia",
   "Inertia"
},
/* STR_LIMITS           */ 
{
   "Limits",
   "Limits"
},
/* STR_SAVING           */ 
{
   "Saving",
   "Saving"
},
/* STR_STATISTIC        */ 
{
   "Statistic",
   "Statistic"
},

// Backlight menu :
// STR_MODE
/* STR_INTENSITY        */ 
{
   "Intensity",
   "Intensity"
},

// Units :
/* STR_SECONDS          */ 
{
   "s",
   "s"
},
// system :
/* _STR_LAST */
{
   "?",
   "?"
}
};
