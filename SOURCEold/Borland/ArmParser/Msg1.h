//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

typedef enum {
   // system :
   STR_NULL, // undefined string

// Message Box :
   STR_OK              ,	
   STR_CANCEL          ,	
   STR_YES             ,	
   STR_NO              ,	


   // system :
   _STR_LAST // strings count
} TStrEnum;

#endif
