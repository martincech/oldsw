//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("UnicodeParser.res");
USEUNIT("UniParser.cpp");
USEFORM("MainCsv.cpp", MainForm);
USEUNIT("CodePage.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "DFM Ripper";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
