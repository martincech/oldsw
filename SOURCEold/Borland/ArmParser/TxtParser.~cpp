//******************************************************************************
//
//   TxtParser.cpp Txt resource parser
//   Version 0.0  (c) Vymos
//
//******************************************************************************


#include <vcl.h>
#pragma hdrstop

#include "uni.h"
#include "TxtParser.h"
#include <stdio.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TTxtParser::TTxtParser()
// Konstruktor
{
   Input = 0;
} // TTxtParser

//******************************************************************************
// Parse
//******************************************************************************

bool TTxtParser::Parse()
// konverze dat
{
char   *p;

   if(!Input){
      IERROR;
   }
   Input->Restart();
   GenProlog();
   Character    = 0;
   while( 1){
      if( !GetToken( Token)){
         GenEpilog();
         return( true);
      }
      if( Token.Type == NEWLINE){
         GenNewline();
         continue;
      }
      if( Token.Type == COMMENT){
         GenComment( Token.Value);
         continue;
      }
      if( Token.Type != KEY){
         Error( Input->Row, Input->Column);
         return( false);
      }
      strcpy( Key, Token.Value);     // zapamatuj key
      if( !GetToken( Token)){
         Error( Input->Row, Input->Column);
         return( false);
      }
      if( Token.Type != STRING){
         Error( Input->Row, Input->Column);
         return( false);
      }
      strcpy( String, Token.Value);  // zapamatuj string
      if( !GetToken( Token)){
         Error( Input->Row, Input->Column);
         return( false);
      }
      if( Token.Type == NEWLINE){
         GenCouple( Key, String, 0);
         continue;
      }
      if( Token.Type == COMMENT){
         GenCouple( Key, String, Token.Value);
         continue;
      }
      Error( Input->Row, Input->Column);
   } // while
} // Parse

//******************************************************************************
// Cteni slova
//******************************************************************************

bool TTxtParser::GetToken( TToken &Token)
// dekodovani slova
{
static char Line[ MAX_KEY_LENGTH];
int         Index;
int         Number;

   Token.Type  = UNDEFINED;
   Token.Value = Line;
   Index       = 0;
   Line[ Index]= '\0';
   // skip whitespace :
   while( 1){
      switch( Character){
         case 0    : // nacti novy
         case ' '  :
         case ',' :
         case '\t' :
         case '\h' :
            // separator
            Character = Input->GetCharacter();
            continue;        // preskoc
         case '\r' :
         case '\n' :
            IERROR;
         default :
            break;
      }
      break;
   }
   // test for newline :
   if( Character == TInStream::INS_EOL){
      Token.Type     = NEWLINE;
      Character = Input->GetCharacter();
      return( true);
   }
   // test for key :
   if( (Character >= 'A' && Character <= 'Z') ||
       (Character >= 'a' && Character <= 'z') ||
       (Character == '_')){
      Token.Type     = KEY;
      Line[ Index++] = Character;
      Character = Input->GetCharacter();
      while( (Character >= 'A' && Character <= 'Z') ||
             (Character >= 'a' && Character <= 'z') ||
             (Character >= '0' && Character <= '9') ||
             (Character == '_')){
         Line[ Index++] = Character;
         Character = Input->GetCharacter();
      }
      Line[ Index] = '\0';
      return( true);
   }
   // test for string :
   if( Character == '"'){
      Token.Type     = STRING;
      Character = Input->GetCharacter();
      while( 1){
         switch( Character){
            case '"' :
               Line[ Index] = '\0';
               Character = Input->GetCharacter();
               return( true);
            case '\\' :
               Line[ Index++] = Character;
               Line[ Index] = '\0';
               Character = Input->GetCharacter();
               if( Character == '"'){
                  Line[ Index++] = Character;
                  Line[ Index] = '\0';
                  Character = Input->GetCharacter();
               }
               break;
            case TInStream::INS_EOL :
               // missing "
               Line[ Index] = '\0';
               Character = Input->GetCharacter();
               return( true);
            default :
               Line[ Index++] = Character;
               Character = Input->GetCharacter();
               break;
         }
      }
   }
   // test for comment :
   if( Character == '/'){
      Token.Type     = (TTokenType)Character;
      Line[ Index++] = Character;
      Character = Input->GetCharacter();
      if( Character != '/'){
         return( true);
      }
      Token.Type     = COMMENT;
      Line[ Index++] = Character;
      Character = Input->GetCharacter();
      while( Character != TInStream::INS_EOL){
         Line[ Index++] = Character;
         Character = Input->GetCharacter();
      }
      Character = Input->GetCharacter();
      Line[ Index] = '\0';
      return( true);
   }
   // end of file :
   if( Character == TInStream::INS_EOF){
      return( false);
   }
   // any character :
   Token.Type = (TTokenType)Character;
   Character  = Input->GetCharacter();
   return( true);
} // GetToken

//******************************************************************************
// Restart
//******************************************************************************

void TTxtParser::GenProlog()
// inicializace pred zacatkem dekodovani
{
   char buff[ 120];
   if( Avr32Mode){
      StrIndex = 0x40000000;
   } else {
      StrIndex = 0;
   }
// Header ----------------------------------------------------------------------
   Header->Append( "//******************************************************************************");
   Header->Append( "//");
   Header->Append( "//  Str.h          String codes");
   Header->Append( "//  Version 1.0    (c) Robot");
   Header->Append( "//");
   Header->Append( "//******************************************************************************");
   Header->Append( "");
   Header->Append( "#ifndef __Str_H__");
   Header->Append( "   #define __Str_H__");
   Header->Append( "");
   Header->Append( "#ifndef __StrDef_H__");
   Header->Append( "   #include \"../inc/wgt/StrDef.h\"");
   Header->Append( "#endif");
   Header->Append( "");
   if( PtrMode){
      sprintf( buff, "#define STR_NULL (char *)%d // undefined string", StrIndex++);
      Header->Append( buff);
   } else {
      Header->Append( "typedef enum {");
      Header->Append( "   // system :");
      Header->Append( "   STR_NULL, // undefined string");
      Header->Append( "");
   }
// Source ----------------------------------------------------------------------
   Source->Append( "//******************************************************************************");
   Source->Append( "//");
   Source->Append( "//  Str.c          Strings translations");
   Source->Append( "//  Version 1.0    (c) Robot");
   Source->Append( "//");
   Source->Append( "//******************************************************************************");
   Source->Append( "");
   Source->Append( "#include \"Str.h\"");
   Source->Append( "");
   Source->Append( "//------------------------------------------------------------------------------");
   Source->Append( "//   String definitions");
   Source->Append( "//------------------------------------------------------------------------------");
   Source->Append( "");
   Source->Append( "TAllStrings AllStrings = {");
   Source->Append( "// system :");
   Source->Append( "/* STR_NULL */   \"\",");
   Source->Append( "");
// CSV -------------------------------------------------------------------------
   Csv->Append( "\"LANGUAGE\",\"LNG_ENGLISH\"");
   Csv->Append( "\"CODEPAGE\",\"NORMAL\"");
   Csv->Append( "\"\"");
} // GenProlog

//******************************************************************************
// Error
//******************************************************************************

void TTxtParser::Error( int RowNumber, int ColumnNumber)
// chyba syntaxe
{
   char buff[ 120];
   sprintf( buff, "***ERROR*** on line %d column %d", RowNumber, ColumnNumber);
   Header->Append( buff);
   Source->Append( buff);
   Csv->Append( buff);
} // Error

//******************************************************************************
// New line
//******************************************************************************

void TTxtParser::GenNewline()
// generovani noveho radku
{
// Header ----------------------------------------------------------------------
   Header->Append( "");
// Source ----------------------------------------------------------------------
   Source->Append( "");
// CSV -------------------------------------------------------------------------
   Csv->Append( "\"\"");
} // GenNewline

//******************************************************************************
// Comment
//******************************************************************************

void TTxtParser::GenComment( char *Comment)
// generovani komentare
{
   char buff[ 128];
// Header ----------------------------------------------------------------------
   Header->Append( Comment);
// Source ----------------------------------------------------------------------
   Source->Append( Comment);
// CSV -------------------------------------------------------------------------
   sprintf( buff, "\"%s\"", Comment);
   Csv->Append( buff);
} // GenComment

//******************************************************************************
// Couple
//******************************************************************************

void TTxtParser::GenCouple( char *Key, char *String, char *Comment)
// generovani paru <Key> = <String>
{
   char buff[ 128];
// Header ----------------------------------------------------------------------
   if( PtrMode){
      sprintf( buff, "#define %-20s (char *)%d\t%s", Key, StrIndex++, Comment ? Comment : "");
      Header->Append( buff);
   } else {
      sprintf( buff, "   %-20s,\t%s", Key, Comment ? Comment : "");
      Header->Append( buff);
   }
// Source ----------------------------------------------------------------------
   sprintf( buff, "/* %-20s */ \"%s\",\t%s", Key, String, Comment ? Comment : "");
   Source->Append( buff);
// CSV -------------------------------------------------------------------------
   if( Comment){
      sprintf( buff, "\"%s\",\"%s\",\"%s\"", Key, String, Comment);
   } else {
      sprintf( buff, "\"%s\",\"%s\"", Key, String);
   }
   Csv->Append( buff);
} // GenCouple

//******************************************************************************
// Epilog
//******************************************************************************

void TTxtParser::GenEpilog()
// generovani zaveru souboru
{
   char buff[ 128];
// Header ----------------------------------------------------------------------
   Header->Append( "");
   if( PtrMode){
      Header->Append( "// system :");
      sprintf( buff, "#define _STR_LAST (char *)%d // strings count", StrIndex++);
      Header->Append( buff);
   } else {
      Header->Append( "   // system :");
      Header->Append( "   _STR_LAST // strings count");
      Header->Append( "} TStrEnum;");
   }
   Header->Append( "");
   Header->Append( "#endif");
// Source ----------------------------------------------------------------------
   Source->Append( "// system :");
   Source->Append( "/* _STR_LAST */ \"?\",");
   Source->Append( "};");
// CSV -------------------------------------------------------------------------
} // GenEpilog

