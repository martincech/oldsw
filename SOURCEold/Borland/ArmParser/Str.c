//******************************************************************************
//
//  Str.c          Strings translations
//  Version 1.0    (c) Robot
//
//******************************************************************************

#include "Str.h"

//------------------------------------------------------------------------------
//   String definitions
//------------------------------------------------------------------------------

TAllStrings AllStrings = {
// system :
/* STR_NULL */   "",

// Message Box :
/* STR_OK               */ "Ok",	
/* STR_CANCEL           */ "Cancel",	
/* STR_YES              */ "Yes",	
/* STR_NO               */ "No",	

// Standard title :
/* STR_ERROR            */ "Error",	
/* STR_INFO             */ "Info",	
/* STR_CONFIRMATION     */ "Confirmation",	

// Input box :
/* STR_INPUT            */ "Input",	
/* STR_OUT_OF_LIMITS    */ "Out of limits",	

// Directory box :
/* STR_DIRECTORY_EMPTY  */ "Directory empty",	

// Database box :
/* STR_REALLY_DELETE_RECORD */ "Really delete record ?",	

// Text box :
/* STR_STRING_EMPTY     */ "String empty",	

// User defined :
/* STR_ENTER_VALUE      */ "Enter value 0..100",	
/* STR_REALLY_CLEAR_ALL */ "Really clear all ?",	
/* STR_FILE_EXISTS      */ "File already exists",	
/* STR_DIRECTORY_FULL   */ "Directory full",	
/* STR_REALLY_CLEAR     */ "Really clear file ?",	
/* STR_REALLY_DELETE    */ "Really delete file ?",	
/* STR_AUTOMATIC_MODE   */ "Select automatic mode ?",	
/* STR_ENTER_DATE       */ "Enter date",	
/* STR_ENTER_TIME       */ "Enter time",	
/* STR_ENTER_PASSWORD   */ "Enter password",	
/* STR_REALLY_FORMAT    */ "Really format ?",	
/* STR_ENTER_NAME       */ "Enter name",	
/* STR_ENTER_INERTIA    */ "Enter inertia",	
/* STR_SET_INTENSITY    */ "Set intensity",	
// calibration :
/* STR_ENTER_WEIGHT     */ "Enter weight",	
/* STR_RELEASE_WEIGHT   */ "Release weight",	
/* STR_CHARGE_WEIGHT    */ "Charge weight",	
/* STR_WAIT             */ "Wait...",	

// Units enum :
/* STR_UNITS_KG         */ "kg",	
/* STR_UNITS_G          */ "g",	
/* STR_UNITS_LB         */ "lb",	
/* STR_UNITS_OZ         */ "Oz",	

// Saving mode enum :
/* STR_SAVING_MODE_AUTOMATIC */ "Automatic",	
/* STR_SAVING_MODE_MANUAL */ "Manual 1",	
/* STR_SAVING_MODE_2OPTIONS */ "Manual 2",	
/* STR_SAVING_MODE_3OPTIONS */ "Manual 3",	

// Backlight mode enum :
/* STR_BACKLIGHT_MODE_AUTO */ "Automatic",	
/* STR_BACKLIGHT_MODE_ON */ "Always on",	
/* STR_BACKLIGHT_MODE_OFF */ "Always off",	

// Main menu :
/* STR_SELECT_FILE      */ "Select file",	
/* STR_SHOW_DATA        */ "Show data",	
/* STR_CLEAR_DATA       */ "Clear data",	
/* STR_PARAMETERS       */ "Parameters",	
/* STR_FILE_MANAGER     */ "File manager",	
/* STR_OPTIONS          */ "Options",	

// File menu :
/* STR_EDIT_DATA        */ "Edit data",	
//STR_STATISTIC
/* STR_CREATE           */ "Create",	
/* STR_RENAME           */ "Rename",	
/* STR_CLEAR            */ "Clear",	
/* STR_DELETE           */ "Delete",	
/* STR_CLEAR_ALL        */ "Clear all",	

// Options menu :
/* STR_SET_CLOCK        */ "Set clock",	
/* STR_SET_VOLUME       */ "Set volume",	
/* STR_SET_BACKLIGHT    */ "Set backlight",	
/* STR_CALIBRATE        */ "Calibrate",	
/* STR_FORMAT           */ "Format",	

// Parameters menu :
/* STR_MODE             */ "Mode",	
/* STR_INERTIA          */ "Inertia",	
/* STR_LIMITS           */ "Limits",	
/* STR_SAVING           */ "Saving",	
/* STR_STATISTIC        */ "Statistic",	

// Backlight menu :
// STR_MODE
/* STR_INTENSITY        */ "Intensity",	

// Units :
/* STR_SECONDS          */ "s",	
// system :
/* _STR_LAST */ "?",
};
