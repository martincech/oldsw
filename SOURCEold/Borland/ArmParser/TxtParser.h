//******************************************************************************
//
//   TxtParser.cpp Txt resource parser
//   Version 0.0  (c) Vymos
//
//******************************************************************************

#ifndef TxtParserH
#define TxtParserH

#include "InStream.h"

//******************************************************************************
//  DFM Parser
//******************************************************************************

class TTxtParser
{
private:
   // implementacni konstanty :
   typedef enum {
      MAX_KEY_LENGTH     = 128,    // max. delka klice
      MAX_STRING_LENGTH  = 128,    // max. delka retezce
   } TConstants;
protected :
   // typy lexikalnich symbolu :
   typedef enum {
      UNDEFINED      = 512,
      // jednoducha slova :
      KEY,
      STRING,
      COMMENT,
      NEWLINE,
   } TTokenType;
   // popisovac lexikalniho symbolu :
   typedef struct {
      TTokenType Type;
      char       *Value;
   } TToken;
   // lokalni data :
   TInStream *Input;
   TStrings  *Header;
   TStrings  *Source;
   TStrings  *Csv;
   bool      PtrMode;        // generate char constants
   bool      AvrMode;        // generate AVR32 constants
   int       StrIndex;       // ptr mode string index
   int       Character;
   TToken    Token;
   char      Key[ MAX_KEY_LENGTH + 1];
   char      String[ MAX_STRING_LENGTH + 1];

   bool GetToken( TToken &Token);
   // dekodovani slova
   void GenProlog();
   // inicializace pred zacatkem dekodovani
   void Error( int RowNumber, int ColumnNumber);
   // chyba syntaxe
   void GenNewline();
   // generovani noveho radku
   void GenComment( char *Comment);
   // generovani komentare
   void GenCouple( char *Key, char *String, char *Comment);
   // generovani paru <Key> = <String>
   void GenEpilog();
   // generovani zaveru souboru
public:
    TTxtParser();
   // Konstruktor
   bool Parse();
   // konverze dat
   __property TInStream *InStream      = {write=Input};
   __property TStrings  *HeaderStream  = {write=Header};
   __property TStrings  *SourceStream  = {write=Source};
   __property TStrings  *CsvStream     = {write=Csv};
   __property bool       CharPtrMode   = {write=PtrMode};
   __property bool       Avr32Mode     = {write=AvrMode};
}; // TxtParser

#endif

