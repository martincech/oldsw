//******************************************************************************
//
//  Str.c          Strings translations
//  Version 1.0    (c) Robot
//
//******************************************************************************

#include "Str.h"

//------------------------------------------------------------------------------
//   String definitions
//------------------------------------------------------------------------------

TAllStrings AllStrings = {
// system :
/* STR_NULL */
{
   "",
   ""
},

// Message Box :
/* STR_OK               */ 
{
   "Ok",
   "Ok"
},
/* STR_CANCEL           */ 
{
   "Cancel",
   "Cancel"
},
/* STR_YES              */ 
{
   "\x7F\x80\x81\x82\xBF",
   "Yes"
},
/* STR_NO               */ 
{
   "No",
   "\x1F\x7F\xAD\xAE\xAF\xB0\xB1\xB2\xB3\xB4\xB5\xB6\xB7\xB8\xB9\xBA\xBB\xBC\xBD\xBE\xBF\xC0\xC1\xC2\xC3\xC4\xC5\xC6\xC7\xC8\xC9\xCA"
},

// system :
/* _STR_LAST */
{
   "?",
   "?"
}
};
