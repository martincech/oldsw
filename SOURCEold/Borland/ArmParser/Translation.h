//******************************************************************************
//
//   Translation.h     Translation table
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#ifndef TranslationH
#define TranslationH

#include <map.h>

typedef map<string, string, less<string> > TTranslationMap;
typedef TTranslationMap::value_type TEntry;

//******************************************************************************
// TTranslation
//******************************************************************************

class TTranslation
{
public:
   virtual bool LoadFromFile( AnsiString FileName) = 0;
   // nacte slovnik z disku
   virtual bool Translate( AnsiString &Keyword, AnsiString &Result) = 0;
   // prelozi <Keyword> a vrati jej v <Result>. Vrati false neni-li
   // ve slovniku
protected:
   TTranslationMap Translation;
}; // TTranslation

//---------------------------------------------------------------------------
#endif
