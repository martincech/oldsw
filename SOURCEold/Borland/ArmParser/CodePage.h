//******************************************************************************
//
//   CodePage.h   Code Page translations
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef CodePageH
   #define CodePageH

#define CODE_PAGE_COUNT  5
#define CODE_PAGE_CHARACTERS 256

#define CHAR_UNDEFINED '\xFF'

class TCodePage
{
public :
   TCodePage();
   // Constructor
   bool Load( int CodePage, String FileName);
   // Load from file

   void To( int CodePage, char *Dst, wchar_t *Src);
   // Translate to codepage
   void From( int CodePage, wchar_t *Dst, char *Src);
   // Translate from codepage

   byte CharTo( int CodePage, wchar_t ch);
   // Translate character to codepage
   wchar_t CharFrom( int CodePage, char ch);
   // Translate character from codepage

   int Size( int CodePage);
   // Returns real table size

   __property bool WithEscape = {write=FWithEscape};
//------------------------------------------------------------------------------
protected :
   wchar_t Table[ CODE_PAGE_COUNT][ CODE_PAGE_CHARACTERS]; // translation table
   bool    FWithEscape;

   void EscapeCharTo( int CodePage, wchar_t ch, char *Dst, int &Index);
   // Translate character to codepage
   bool EscapeCharFrom( int CodePage, char *Src, int &Index, wchar_t &ch);
   // Translate character from codepage
   int CharToHex( char ch);
   // Convert char to hex
   bool IsEscape( char ch);
   // Check for escape characters
}; // CodePage

#endif

 