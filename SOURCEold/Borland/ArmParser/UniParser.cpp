//******************************************************************************
//
//   UniParser.cpp  Unicode CSV parser
//   Version 0.0  (c) Vymos
//
//******************************************************************************


#include <vcl.h>
#pragma hdrstop

#include "uni.h"
#include "UniParser.h"
#include <stdio.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

#define BUFFER_SIZE 2048

//******************************************************************************
// Konstruktor
//******************************************************************************

TUniParser::TUniParser()
// Konstruktor
{
   Input = 0;
   memset( FCodePage, 0, sizeof( FCodePage));
   FTranslator = new TCodePage;
} // TUniParser

//******************************************************************************
// Destruktor
//******************************************************************************

TUniParser::~TUniParser()
// Destruktor
{
   if( FTranslator){
      delete FTranslator;
   }
} // TUniParser

//******************************************************************************
// Parse
//******************************************************************************

bool TUniParser::Parse( AnsiString FileName)
// konverze dat
{
FILE   *f;
static wchar_t Buffer[ BUFFER_SIZE + 1];
static wchar_t Word[   BUFFER_SIZE + 1];
static char    Ansi[   BUFFER_SIZE + 1];
TTokenType     Token;

   f = fopen( FileName.c_str(), "rb");
   if( !f){
      return( false);
   }
   // signatura unicode souboru :
   int ch;
   ch = fgetc( f);
   if( ch == EOF){
      fclose( f);
      return( false);
   }
   if( ch != 0xFF){
      fclose( f);
      Error( 0, 0);
      return( false);
   }
   ch = fgetc( f);
   if( ch == EOF){
      fclose( f);
      return( false);
   }
   if( ch != 0xFE){
      fclose( f);
      Error( 0, 0);
      return( false);
   }
   // cteni hlavicky :
   bool Result = false;
   int  Row    = 0;
   // seznam jazyku : ----------------------------------------------------------
   if( !fgetws( Buffer, BUFFER_SIZE, f)){
      return( false);
   }
   Row++;
   int Index = 0;
   // LANGUAGE keyword
   Token = ParseWord( Buffer, sizeof( Buffer) - 1, Index, Word);
   if( Token != STRING){
      Error( Row, Index + 1);
      return( false);
   }
   if( !::WideCharToMultiByte( 1250, 0, Word,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
      Error( Row, Index + 1);
      return( false);
   }
   if( !strequ( Ansi, "LANGUAGE")){
      Error( Row, Index + 1);
      return( false);
   }
   // get language count :
   FLanguageCount = 0;
   while( 1){
      Token = ParseWord( Buffer, sizeof( Buffer) - 1, Index, Word);
      if( Token != STRING){
         break;
      }
      if( !::WideCharToMultiByte( 1250, 0, Word,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
         Error( Row, Index + 1);
         return( false);
      }
      FLanguageCount++;
   }
   // kodove stranky : ---------------------------------------------------------
   if( !fgetws( Buffer, BUFFER_SIZE, f)){
      return( false);
   }
   Row++;
   Index = 0;
   // CODEPAGE keyword
   Token = ParseWord( Buffer, sizeof( Buffer) - 1, Index, Word);
   if( Token != STRING){
      Error( Row, Index + 1);
      return( false);
   }
   if( !::WideCharToMultiByte( 1250, 0, Word,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
      Error( Row, Index + 1);
      return( false);
   }
   if( !strequ( Ansi, "CODEPAGE")){
      Error( Row, Index + 1);
      return( false);
   }
   // get codepages :
   int LanguageIndex = 0;
   while( 1){
      Token = ParseWord( Buffer, sizeof( Buffer) - 1, Index, Word);
      if( Token != STRING){
         break;
      }
      if( !::WideCharToMultiByte( 1250, 0, Word,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
         Error( Row, Index + 1);
         return( false);
      }
      FCodePage[ LanguageIndex++] = CodePageToIndex( Ansi);
   }
   // prazdny radek : ----------------------------------------------------------
   Token = ParseWord( Buffer, sizeof( Buffer) - 1, Index, Word);
   if( Token != NEWLINE){
      Error( Row, Index + 1);
      return( false);
   }
   // cteni radku : ------------------------------------------------------------
   GenProlog();
   bool TranslationError;
   while( 1){
      if( !fgetws( Buffer, BUFFER_SIZE, f)){
         GenEpilog();
         Result = true;
         break;
      }
      Row++;
      Index = 0;
      TranslationError = false;
      Token = ParseWord( Buffer, sizeof( Buffer) - 1, Index, Word);
      // empty line :
      if( Token == NEWLINE){
         GenNewline();
         continue;
      }
      // comment only :
      if( Token == COMMENT){
         if( !::WideCharToMultiByte( 1250, 0, Word,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
            Error( Row, Index + 1);
            break;
         }
         GenComment( Ansi);
         continue;
      }
      //---- translation row :
      if( Token != STRING){
         Error( Row, Index + 1);
         break;
      }
      // convert key :
      if( !::WideCharToMultiByte( 1250, 0, Word,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
         Error( Row, Index + 1);
         break;
      }
      strncpy( Key, Ansi, MAX_KEY_LENGTH);     // zapamatuj key
      Key[ MAX_KEY_LENGTH] = '\0';
      // get strings :
      memset( String, 0, sizeof( String));
      int StringIndex = 0;
      while( 1){
         Token = ParseWord( Buffer, sizeof( Buffer) - 1, Index, Word);
         if( Token != STRING){
            break;
         }
         FTranslator->To( FCodePage[ StringIndex], Ansi, Word);
         strncpy( String[ StringIndex], Ansi, MAX_STRING_LENGTH);
         String[ StringIndex][ MAX_STRING_LENGTH] = '\0';
         if( strchr( String[ StringIndex], CHAR_UNDEFINED)){
            TranslationError = true;
         }
         StringIndex++;
         if( StringIndex >= MAX_LANGUAGE_COUNT){
            Error( Row, Index + 1);
            goto Done;
         }
      }
      if( StringIndex > FLanguageCount){
         Error( Row, Index + 1);
         break;
      }
      if( Token == NEWLINE){
         GenCouple( Key, String, 0);
         if( TranslationError){
            Error( Row, 0);
         }
         continue;
      }
      if( Token == COMMENT){
         if( !::WideCharToMultiByte( 1250, 0, Word,  -1, Ansi, BUFFER_SIZE, NULL, NULL)){
            Error( Row, Index + 1);
            break;
         }
         GenCouple( Key, String, Ansi);
         if( TranslationError){
            Error( Row, 0);
         }
         continue;
      }
      Error( Row, Index + 1);
      break;
   } // while
Done :
   fclose( f);
   return( Result);
} // Parse

//******************************************************************************
// Cteni slova
//******************************************************************************

TUniParser::TTokenType TUniParser::ParseWord( wchar_t *Line, int LineLength, int &Index, wchar_t *Word)
// vyhleda slovo na radku od <Index>
{
   int Start = -1;
   for( int i = Index; i < LineLength; i++){
      switch( Line[ i]){
         case L' ' :
         case L'\t' :
         case L',' :
         case L'\r' :
            continue;
         case L'\"' :
            Start = i + 1;
            break;
         case L'\n' :
            return( NEWLINE);
         default :
            Start = i;
            break;
      }
      break;
   }
   int Stop = -1;
   for( int i = Start; i < LineLength; i++){
      switch( Line[ i]){
         case L'\"' :
         case L'\t' :
         case L',' :
         case L'\n' :
         case L'\r' :
            Stop = i;
            break;
         default :
            continue;
      }
      break;
   }
   if( Stop <= Start){
      return( UNDEFINED);
   }
   memcpy( Word, &Line[ Start], (Stop - Start) * sizeof( wchar_t));
   Word[ Stop - Start] = L'\0';
   Index = Stop + 1;
   if( Word[ 0] == L'/' && Word[ 1] == L'/'){
      return( COMMENT);
   }
   return( STRING);
} // ParseWord

//******************************************************************************
// Code Page
//******************************************************************************

int TUniParser::CodePageToIndex( char *PageName)
// konvertuje nazev kodove stranky na index
{
   if( strequ( PageName, "JAPAN")){
      return( 1);
   }
   if( strequ( PageName, "CYRILIC")){
      return( 2);
   }
   return( 0);
} // CodePageToIndex

//******************************************************************************
// Restart
//******************************************************************************

void TUniParser::GenProlog()
// inicializace pred zacatkem dekodovani
{
   char buff[ 120];
   if( FAvrMode){
      StrIndex = 0x40000000;
   } else {
      StrIndex = 0;
   }
// Header ----------------------------------------------------------------------
   Header->Append( "//******************************************************************************");
   Header->Append( "//");
   Header->Append( "//  Str.h          String codes");
   Header->Append( "//  Version 1.0    (c) Robot");
   Header->Append( "//");
   Header->Append( "//******************************************************************************");
   Header->Append( "");
   Header->Append( "#ifndef __Str_H__");
   Header->Append( "   #define __Str_H__");
   Header->Append( "");
   Header->Append( "#ifndef __StrDef_H__");
   Header->Append( "   #include \"../inc/wgt/StrDef.h\"");
   Header->Append( "#endif");
   Header->Append( "");
   if( FPtrMode){
      sprintf( buff, "#define STR_NULL (char *)%d // undefined string", StrIndex++);
      Header->Append( buff);
   } else {
      Header->Append( "typedef enum {");
      Header->Append( "   // system :");
      Header->Append( "   STR_NULL, // undefined string");
      Header->Append( "");
   }
// Source ----------------------------------------------------------------------
   Source->Append( "//******************************************************************************");
   Source->Append( "//");
   Source->Append( "//  Str.c          Strings translations");
   Source->Append( "//  Version 1.0    (c) Robot");
   Source->Append( "//");
   Source->Append( "//******************************************************************************");
   Source->Append( "");
   Source->Append( "#include \"Str.h\"");
   Source->Append( "");
   Source->Append( "//------------------------------------------------------------------------------");
   Source->Append( "//   String definitions");
   Source->Append( "//------------------------------------------------------------------------------");
   Source->Append( "");
   Source->Append( "TAllStrings AllStrings = {");
   Source->Append( "// system :");
   Source->Append( "/* STR_NULL */");
   Source->Append( "{");
   for( int i = 0; i < FLanguageCount; i++){
      if( i != FLanguageCount - 1){
         Source->Append( "   \"\",");
      } else {
         Source->Append( "   \"\"");
      }
   }
   Source->Append( "},");
   Source->Append( "");
} // GenProlog

//******************************************************************************
// Error
//******************************************************************************

void TUniParser::Error( int RowNumber, int ColumnNumber)
// chyba syntaxe
{
   char buff[ 120];
   sprintf( buff, "***ERROR*** on line %d column %d", RowNumber, ColumnNumber);
   Header->Append( buff);
   Source->Append( buff);
} // Error

//******************************************************************************
// New line
//******************************************************************************

void TUniParser::GenNewline()
// generovani noveho radku
{
// Header ----------------------------------------------------------------------
   Header->Append( "");
// Source ----------------------------------------------------------------------
   Source->Append( "");
} // GenNewline

//******************************************************************************
// Comment
//******************************************************************************

void TUniParser::GenComment( char *Comment)
// generovani komentare
{
   char buff[ 128];
// Header ----------------------------------------------------------------------
   Header->Append( Comment);
// Source ----------------------------------------------------------------------
   Source->Append( Comment);
} // GenComment

//******************************************************************************
// Couple
//******************************************************************************

void TUniParser::GenCouple( char *Key, TStringArray String, char *Comment)
// generovani paru <Key> = <String>
{
   char buff[ 128];
// Header ----------------------------------------------------------------------
   if( FPtrMode){
      sprintf( buff, "#define %-20s (char *)%d\t%s", Key, StrIndex++, Comment ? Comment : "");
      Header->Append( buff);
   } else {
      sprintf( buff, "   %-20s,\t%s", Key, Comment ? Comment : "");
      Header->Append( buff);
   }
// Source ----------------------------------------------------------------------
   sprintf( buff, "/* %-20s */ %s", Key, Comment ? Comment : "");
   Source->Append( buff);
   sprintf( buff, "{");
   Source->Append( buff);
   for( int i = 0; i < FLanguageCount; i++){
      if( i != FLanguageCount - 1){
         sprintf( buff, "   \"%s\",", String[ i]);
      } else {
         sprintf( buff, "   \"%s\"",  String[ i]); // last item without comma
      }
      Source->Append( buff);
   }
   sprintf( buff, "},");
   Source->Append( buff);
} // GenCouple

//******************************************************************************
// Epilog
//******************************************************************************

void TUniParser::GenEpilog()
// generovani zaveru souboru
{
   char buff[ 128];
// Header ----------------------------------------------------------------------
   Header->Append( "");
   if( FPtrMode){
      Header->Append( "// system :");
      sprintf( buff, "#define _STR_LAST (char *)%d // strings count", StrIndex++);
      Header->Append( buff);
   } else {
      Header->Append( "   // system :");
      Header->Append( "   _STR_LAST // strings count");
      Header->Append( "} TStrEnum;");
   }
   Header->Append( "");
   Header->Append( "#endif");
// Source ----------------------------------------------------------------------
   Source->Append( "// system :");
   Source->Append( "/* _STR_LAST */");
   Source->Append( "{");
   for( int i = 0; i < FLanguageCount; i++){
      if( i != FLanguageCount - 1){
         Source->Append( "   \"?\",");
      } else {
         Source->Append( "   \"?\"");
      }
   }
   Source->Append( "}");
   Source->Append( "};");
} // GenEpilog

