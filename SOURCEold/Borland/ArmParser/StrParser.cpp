//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("StrParser.res");
USEUNIT("TxtParser.cpp");
USEUNIT("InStream.cpp");
USEFORM("Main.cpp", MainForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "DFM Ripper";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
