//******************************************************************************
//
//   Main.h     Conversion utility main form
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <Dialogs.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>

#include "TxtParser.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TActionList *ActionList;
        TAction *FileOpen;
        TAction *FileExit;
        TAction *FileSave;
        TMainMenu *MainMenu;
        TMenuItem *FileMenu;
        TMenuItem *FileOpenItem;
        TMenuItem *FileSaveItem;
        TMenuItem *FileExitItem;
        TOpenDialog *FileOpenDialog;
        TSaveDialog *FileSaveDialog;
        TStatusBar *StatusBar;
        TPageControl *MainPageControl;
        TTabSheet *MainInputSheet;
        TTabSheet *MainHeaderSheet;
        TMemo *MainInput;
        TMemo *MainHeader;
        TTabSheet *MainSourceSheet;
        TMemo *MainSource;
        TCheckBox *CbCharFormat;
        TTabSheet *MainCsvSheet;
        TMemo *MainCsv;
   TCheckBox *CbAvr32;
        void __fastcall FileOpenExecute(TObject *Sender);
        void __fastcall FileSaveExecute(TObject *Sender);
        void __fastcall FileExitExecute(TObject *Sender);
        void __fastcall MainPageControlChanging(TObject *Sender,
          bool &AllowChange);
        void __fastcall CreateMain(TObject *Sender);
        void __fastcall CbCharFormatClick(TObject *Sender);
private:	// User declarations
   AnsiString FileName;    // nazev pracovniho souboru
   bool       Touch;
   TTxtParser *Parser;
   bool __fastcall ConvertData();
   // konverze dat
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
