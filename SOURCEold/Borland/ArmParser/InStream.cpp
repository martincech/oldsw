//******************************************************************************
//
//   InStream.cpp Input stream reader
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "InStream.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TInStream::TInStream( TStrings *Text)
// Konstruktor
{
   Input = Text;
   Restart();
} // TInStream

//******************************************************************************
// Cteni znaku
//******************************************************************************

int __fastcall TInStream::GetCharacter()
// nacteni znaku ze vstupniho proudu
{
   if( ColumnsCount < 0){
      // nacti novy radek
      CurrentRow++;
      CurrentColumn = 0;
      if( CurrentRow > RowsCount){
         return( INS_EOF);       // konec textu
      }
      ColumnsCount = Input->Strings[ CurrentRow - 1].Length();
   }
   CurrentColumn++;
   if( CurrentColumn > ColumnsCount){
      ColumnsCount = -1;     // nacti novy radek
      return( INS_EOL);
   }
   return( Input->Strings[ CurrentRow-1][ CurrentColumn]);
} // GetCharacter

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TInStream::Restart()
// znovu od zacatku textu
{
   CurrentRow    = 0;
   RowsCount     = Input->Count;
   ColumnsCount  = -1;           // neni nacten radek
} // Restart
