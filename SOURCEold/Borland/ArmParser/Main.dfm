object MainForm: TMainForm
  Left = 360
  Top = 203
  AutoScroll = False
  Caption = 'Text parser'
  ClientHeight = 600
  ClientWidth = 850
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  OnCreate = CreateMain
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TStatusBar
    Left = 0
    Top = 581
    Width = 850
    Height = 19
    Panels = <
      item
        Alignment = taRightJustify
        Text = '999:99 '
        Width = 50
      end
      item
        Width = 150
      end>
    SimplePanel = False
  end
  object MainPageControl: TPageControl
    Left = 0
    Top = 0
    Width = 845
    Height = 577
    ActivePage = MainInputSheet
    Anchors = [akLeft, akTop, akRight, akBottom]
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnChanging = MainPageControlChanging
    object MainInputSheet: TTabSheet
      Caption = 'Input'
      object MainInput: TMemo
        Left = 0
        Top = 0
        Width = 837
        Height = 549
        Anchors = [akLeft, akTop, akRight, akBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object MainHeaderSheet: TTabSheet
      Caption = 'Header'
      ImageIndex = 1
      object MainHeader: TMemo
        Left = 0
        Top = 0
        Width = 837
        Height = 549
        Anchors = [akLeft, akTop, akRight, akBottom]
        Color = clAqua
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object MainSourceSheet: TTabSheet
      Caption = 'Source'
      ImageIndex = 2
      object MainSource: TMemo
        Left = 0
        Top = 0
        Width = 837
        Height = 549
        Anchors = [akLeft, akTop, akRight, akBottom]
        Color = clAqua
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object MainCsvSheet: TTabSheet
      Caption = 'CSV'
      ImageIndex = 3
      object MainCsv: TMemo
        Left = 0
        Top = 0
        Width = 837
        Height = 549
        Anchors = [akLeft, akTop, akRight, akBottom]
        Color = clAqua
        Font.Charset = EASTEUROPE_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object CbCharFormat: TCheckBox
    Left = 208
    Top = 0
    Width = 97
    Height = 17
    Caption = 'Char * format'
    Checked = True
    State = cbChecked
    TabOrder = 2
    OnClick = CbCharFormatClick
  end
  object CbAvr32: TCheckBox
    Left = 344
    Top = 0
    Width = 97
    Height = 17
    Caption = 'AVR32 UC3'
    TabOrder = 3
  end
  object ActionList: TActionList
    Left = 768
    Top = 8
    object FileOpen: TAction
      Category = 'File'
      Caption = '&Open'
      OnExecute = FileOpenExecute
    end
    object FileExit: TAction
      Category = 'File'
      Caption = 'E&xit'
      OnExecute = FileExitExecute
    end
    object FileSave: TAction
      Category = 'File'
      Caption = '&Save'
      OnExecute = FileSaveExecute
    end
  end
  object MainMenu: TMainMenu
    Left = 808
    Top = 8
    object FileMenu: TMenuItem
      Caption = '&File'
      object FileOpenItem: TMenuItem
        Action = FileOpen
      end
      object FileSaveItem: TMenuItem
        Action = FileSave
      end
      object FileExitItem: TMenuItem
        Action = FileExit
      end
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'txt'
    Filter = 'Text|*.txt'
    Title = 'Open File'
    Left = 768
    Top = 48
  end
  object FileSaveDialog: TSaveDialog
    DefaultExt = 'h'
    FileName = 'export.h'
    Filter = 'Export|*.h'
    Title = 'Save File'
    Left = 808
    Top = 48
  end
end
