//******************************************************************************
//
//  Str.h          String codes
//  Version 1.0    (c) Robot
//
//******************************************************************************

#ifndef __Str_H__
   #define __Str_H__

#ifndef __StrDef_H__
   #include "../inc/wgt/StrDef.h"
#endif

typedef enum {
   // system :
   STR_NULL, // undefined string

// Message Box :
   STR_OK              ,	
   STR_CANCEL          ,	
   STR_YES             ,	
   STR_NO              ,	

// Standard title :
   STR_ERROR           ,	
   STR_INFO            ,	
   STR_CONFIRMATION    ,	

// Input box :
   STR_INPUT           ,	
   STR_OUT_OF_LIMITS   ,	

// Directory box :
   STR_DIRECTORY_EMPTY ,	

// Database box :
   STR_REALLY_DELETE_RECORD,	

// Text box :
   STR_STRING_EMPTY    ,	

// User defined :
   STR_ENTER_VALUE     ,	
   STR_REALLY_CLEAR_ALL,	
   STR_FILE_EXISTS     ,	
   STR_DIRECTORY_FULL  ,	
   STR_REALLY_CLEAR    ,	
   STR_REALLY_DELETE   ,	
   STR_AUTOMATIC_MODE  ,	
   STR_ENTER_DATE      ,	
   STR_ENTER_TIME      ,	
   STR_ENTER_PASSWORD  ,	
   STR_REALLY_FORMAT   ,	
   STR_ENTER_NAME      ,	
   STR_ENTER_INERTIA   ,	
   STR_SET_INTENSITY   ,	
// calibration :
   STR_ENTER_WEIGHT    ,	
   STR_RELEASE_WEIGHT  ,	
   STR_CHARGE_WEIGHT   ,	
   STR_WAIT            ,	

// Units enum :
   STR_UNITS_KG        ,	
   STR_UNITS_G         ,	
   STR_UNITS_LB        ,	
   STR_UNITS_OZ        ,	

// Saving mode enum :
   STR_SAVING_MODE_AUTOMATIC,	
   STR_SAVING_MODE_MANUAL,	
   STR_SAVING_MODE_2OPTIONS,	
   STR_SAVING_MODE_3OPTIONS,	

// Backlight mode enum :
   STR_BACKLIGHT_MODE_AUTO,	
   STR_BACKLIGHT_MODE_ON,	
   STR_BACKLIGHT_MODE_OFF,	

// Main menu :
   STR_SELECT_FILE     ,	
   STR_SHOW_DATA       ,	
   STR_CLEAR_DATA      ,	
   STR_PARAMETERS      ,	
   STR_FILE_MANAGER    ,	
   STR_OPTIONS         ,	

// File menu :
   STR_EDIT_DATA       ,	
//STR_STATISTIC
   STR_CREATE          ,	
   STR_RENAME          ,	
   STR_CLEAR           ,	
   STR_DELETE          ,	
   STR_CLEAR_ALL       ,	

// Options menu :
   STR_SET_CLOCK       ,	
   STR_SET_VOLUME      ,	
   STR_SET_BACKLIGHT   ,	
   STR_CALIBRATE       ,	
   STR_FORMAT          ,	

// Parameters menu :
   STR_MODE            ,	
   STR_INERTIA         ,	
   STR_LIMITS          ,	
   STR_SAVING          ,	
   STR_STATISTIC       ,	

// Backlight menu :
// STR_MODE
   STR_INTENSITY       ,	

// Units :
   STR_SECONDS         ,	

   // system :
   _STR_LAST // strings count
} TStrEnum;

#endif
