//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat2Measure.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Bat2Device.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->Title = "BAT2 Measure";
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
