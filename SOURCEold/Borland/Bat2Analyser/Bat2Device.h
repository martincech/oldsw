//******************************************************************************
//
//   Bat2Device.h  BAT2 device interface
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef Bat2DeviceH
#define Bat2DeviceH

#include <stdio.h>

#include "../Library/Serial/Uart/ComUart.h"
#include "../Library/Serial/Logger.h"
#include "AdcTesterDef.h"

#define BD_COM_NAME    8
#define BD_PATH        512
#define BD_MAX_BUFFER  AdcPacketSize( ADC_CONVERSION_COUNT)
#define BD_MAX_COMMENT 1024

//******************************************************************************
// TBat2Device
//******************************************************************************

class TBat2Device {
public :
   TBat2Device();
   // Constructor

   ~TBat2Device();
   // Destructor

   void SetCom( char *Name);
   // Set communincation device name

   BOOL CalibrateZero( dword *Value);
   // Calibrate zero, returns <Value>

   BOOL CalibrateFullScale( dword *Value);
   // Calibrate full scale, returns <Value>

   BOOL SetZero( dword Value);
   // Set zero calibration

   BOOL SetFullScale( dword Value);
   // Set full scale calibration

   BOOL MeasureZero();
   // Measure zero level

   BOOL MeasureFullScale();
   // Measure full scale level

   void SetFile( char *Name);
   // Set file <Name>

   BOOL SetParameters();
   // Send parameters to device

   BOOL Run();
   // Run & save conversions

   BOOL Stop();
   // Stop conversions & close file

   BOOL Chop;                          // use CHOP mode / without CHOP
   BOOL Sinc3;                         // use SINC3 filter / SINC4
   BOOL WithSignum;                    // signum extension / unsigned
   int  Rate;                          // conversion rate [Hz]

   BOOL ContinuousMode;                // continuous mode / power save mode
   int  BridgeOnDelay;                 // bridge on delay [Samples]
   int  SamplesCount;                  // sampes burst count [Samples]
   int  Space;                         // space between conversions [ms]

   int  SamplesDone;                   // sampling progress

   double ZeroKg;                      // zero weight
   double FullKg;                      // full scale weight
   int    ZeroLsb;                     // zero conversions average
   int    FullLsb;                     // full scale conversions average
   int    Duration;                    // calibration packet count (*  SamplesCount)
   int    Delay;                       // calibration delay samples count

   char   Comment[ BD_MAX_COMMENT + 1];// comment

   TLogger  *Logger;                   // raw data logger

//------------------------------------------------------------------------------
protected :
   TComUart *Uart;                         // COM interface
   char      ComName[ BD_COM_NAME + 1];    // COM port name
   char      FileName[ BD_PATH    + 1];    // COM port name
   byte      Buffer[ BD_MAX_BUFFER + 1];   // COM data buffer
   FILE     *f;                            // active file
   BOOL      StopReceive;                  // stop packet receive loop       

   BOOL CheckConnect();
   // Locate and open device

   void Disconnect();
   // Close device

   BOOL Command( byte Code);
   // Send command <Code>

   BOOL CommandByte( byte Code, byte Value);
   // Send command <Code> with byte <Value>

   BOOL CommandWord( byte Code, dword Value);
   // Send command <Code> with wrod <Value>

   BOOL SendCommand( char *Command);
   // Send command to device

   BOOL ReceiveWord( dword *Value);
   // Read reply from device

   BOOL ReceivePacket();
   // Read reply packet from device

   void SavePacket();
   // Parse & save packet

   BOOL PacketStatistics( int64 *Sum);
   // Parse & calculate statistics

   void SaveHeader();
   // Save file header

   BOOL Measure( int Duration, int *Value);
   // Measure <Duration> * SamplesCount burst, returns <Value>

   void SignumExtension( dword *Value);
   // Do signum extension of <Value>
}; // TBat2Device

#endif
