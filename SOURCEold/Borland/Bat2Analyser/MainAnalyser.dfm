object MainForm: TMainForm
  Left = 479
  Top = 176
  Width = 1151
  Height = 749
  Caption = 'BAT2 Analyser'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 5
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object LblDataName: TLabel
    Left = 48
    Top = 5
    Width = 47
    Height = 13
    Caption = 'NONAME'
  end
  object Label4: TLabel
    Left = 528
    Top = 4
    Width = 34
    Height = 13
    Caption = 'Setup :'
  end
  object LblSetupName: TLabel
    Left = 568
    Top = 5
    Width = 47
    Height = 13
    Caption = 'NONAME'
  end
  object Panel1: TPanel
    Left = 860
    Top = 24
    Width = 281
    Height = 89
    Anchors = [akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 0
    object Label5: TLabel
      Left = 108
      Top = 5
      Width = 27
      Height = 13
      Caption = 'Files'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BtnLoadData: TButton
      Left = 8
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Load Data'
      TabOrder = 0
      OnClick = BtnLoadDataClick
    end
    object BtnLoadSetup: TButton
      Left = 8
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Load Setup'
      TabOrder = 1
      OnClick = BtnLoadSetupClick
    end
    object BtnSaveSetup: TButton
      Left = 88
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Save Setup'
      TabOrder = 2
      OnClick = BtnSaveSetupClick
    end
    object BtnSetDefaults: TButton
      Left = 88
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Set Defaults'
      TabOrder = 3
      OnClick = BtnSetDefaultsClick
    end
  end
  object Graph: TChart
    Left = 8
    Top = 24
    Width = 846
    Height = 645
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    View3D = False
    TabOrder = 1
    Anchors = [akLeft, akTop, akRight, akBottom]
    object SamplesSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 8388863
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object PrefilterSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object LowPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clLime
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object HighPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlue
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlack
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 678
    Width = 846
    Height = 33
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
    object Label2: TLabel
      Left = 4
      Top = 9
      Width = 34
      Height = 13
      Caption = 'Width :'
    end
    object Label3: TLabel
      Left = 121
      Top = 8
      Width = 34
      Height = 13
      Caption = 'Offset :'
    end
    object EdtWidth: TEdit
      Left = 42
      Top = 5
      Width = 79
      Height = 21
      TabOrder = 0
      Text = '100'
      OnChange = EdtWidthChange
    end
    object BtnBegin: TButton
      Left = 360
      Top = 4
      Width = 75
      Height = 25
      Caption = '|<'
      TabOrder = 1
      OnClick = BtnBeginClick
    end
    object BtnPrev: TButton
      Left = 448
      Top = 4
      Width = 75
      Height = 25
      Caption = '<'
      TabOrder = 2
      OnClick = BtnPrevClick
    end
    object BtnNext: TButton
      Left = 536
      Top = 4
      Width = 75
      Height = 25
      Caption = '>'
      TabOrder = 3
      OnClick = BtnNextClick
    end
    object BtnEnd: TButton
      Left = 624
      Top = 4
      Width = 75
      Height = 25
      Caption = '>|'
      TabOrder = 4
      OnClick = BtnEndClick
    end
    object EdtOffset: TEdit
      Left = 161
      Top = 5
      Width = 87
      Height = 21
      TabOrder = 5
      Text = '0'
      OnChange = EdtOffsetChange
    end
    object BtnRedraw: TButton
      Left = 250
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Redraw'
      TabOrder = 6
      OnClick = BtnRedrawClick
    end
  end
  object Panel3: TPanel
    Left = 860
    Top = 116
    Width = 282
    Height = 153
    Anchors = [akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 3
    object Label6: TLabel
      Left = 80
      Top = 8
      Width = 28
      Height = 13
      Caption = 'Data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 24
      Width = 23
      Height = 13
      Caption = 'Min :'
    end
    object LblMin: TLabel
      Left = 35
      Top = 25
      Width = 6
      Height = 13
      Caption = '?'
    end
    object LblMax: TLabel
      Left = 35
      Top = 41
      Width = 6
      Height = 13
      Caption = '?'
    end
    object Label9: TLabel
      Left = 8
      Top = 40
      Width = 26
      Height = 13
      Caption = 'Max :'
    end
    object Label8: TLabel
      Left = 8
      Top = 80
      Width = 60
      Height = 13
      Caption = 'Range max :'
    end
    object Label10: TLabel
      Left = 8
      Top = 64
      Width = 57
      Height = 13
      Caption = 'Range min :'
    end
    object Label24: TLabel
      Left = 104
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Count :'
    end
    object LblCount: TLabel
      Left = 144
      Top = 24
      Width = 6
      Height = 13
      Caption = '?'
    end
    object LblRangeMin: TLabel
      Left = 80
      Top = 64
      Width = 6
      Height = 13
      Caption = '?'
    end
    object LblRangeMax: TLabel
      Left = 80
      Top = 80
      Width = 6
      Height = 13
      Caption = '?'
    end
    object LblSinc3: TLabel
      Left = 72
      Top = 104
      Width = 31
      Height = 13
      Caption = 'SINC4'
    end
    object LblChop: TLabel
      Left = 112
      Top = 104
      Width = 46
      Height = 13
      Caption = 'NOCHOP'
    end
    object LblRate: TLabel
      Left = 168
      Top = 104
      Width = 31
      Height = 13
      Caption = '600Hz'
    end
    object Label11: TLabel
      Left = 8
      Top = 119
      Width = 33
      Height = 13
      Caption = 'Mode :'
    end
    object LblMode: TLabel
      Left = 72
      Top = 117
      Width = 82
      Height = 13
      Caption = 'PWS:256:10:300'
    end
    object Label12: TLabel
      Left = 8
      Top = 104
      Width = 28
      Height = 13
      Caption = 'ADC :'
    end
    object Label13: TLabel
      Left = 8
      Top = 132
      Width = 50
      Height = 13
      Caption = 'Comment :'
    end
    object LblComment: TLabel
      Left = 72
      Top = 132
      Width = 60
      Height = 13
      Caption = 'No comment'
    end
    object CbShowData: TCheckBox
      Left = 204
      Top = 62
      Width = 53
      Height = 17
      Caption = 'Show'
      Checked = True
      State = cbChecked
      TabOrder = 0
      OnClick = CbShowClick
    end
  end
  object Panel4: TPanel
    Left = 861
    Top = 392
    Width = 280
    Height = 169
    Anchors = [akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 4
    object Label14: TLabel
      Left = 8
      Top = 24
      Width = 41
      Height = 13
      Caption = 'Prefilter :'
    end
    object Label15: TLabel
      Left = 152
      Top = 24
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label16: TLabel
      Left = 8
      Top = 50
      Width = 45
      Height = 13
      Caption = 'Window :'
    end
    object Label17: TLabel
      Left = 152
      Top = 50
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label18: TLabel
      Left = 8
      Top = 77
      Width = 38
      Height = 13
      Caption = 'Range :'
    end
    object Label19: TLabel
      Left = 152
      Top = 77
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label20: TLabel
      Left = 8
      Top = 103
      Width = 46
      Height = 13
      Caption = 'Duration :'
    end
    object Label21: TLabel
      Left = 152
      Top = 103
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label31: TLabel
      Left = 81
      Top = 2
      Width = 29
      Height = 13
      Caption = 'Filter'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EdtPrefilter: TEdit
      Left = 56
      Top = 20
      Width = 89
      Height = 21
      TabOrder = 0
      Text = '1'
      OnChange = EdtPrefilterChange
    end
    object CbShowPrefilter: TCheckBox
      Left = 204
      Top = 22
      Width = 53
      Height = 17
      Caption = 'Show'
      TabOrder = 1
      OnClick = CbShowClick
    end
    object EdtWindow: TEdit
      Left = 56
      Top = 46
      Width = 89
      Height = 21
      TabOrder = 2
      Text = '10'
      OnChange = EdtWindowChange
    end
    object CbShowWindow: TCheckBox
      Left = 204
      Top = 48
      Width = 53
      Height = 17
      Caption = 'Show'
      TabOrder = 3
      OnClick = CbShowClick
    end
    object EdtRange: TEdit
      Left = 56
      Top = 73
      Width = 89
      Height = 21
      TabOrder = 4
      Text = '0,003'
      OnChange = EdtRangeChange
    end
    object CbShowRange: TCheckBox
      Left = 204
      Top = 75
      Width = 53
      Height = 17
      Caption = 'Show'
      TabOrder = 5
      OnClick = CbShowClick
    end
    object BtnRecalculate: TButton
      Left = 8
      Top = 133
      Width = 75
      Height = 25
      Caption = 'Recalculate'
      TabOrder = 6
      OnClick = BtnRecalculateClick
    end
    object EdtStabilisationDuration: TEdit
      Left = 56
      Top = 99
      Width = 89
      Height = 21
      TabOrder = 7
      Text = '3'
      OnChange = EdtStabilisationDurationChange
    end
    object CbShowDuration: TCheckBox
      Left = 204
      Top = 101
      Width = 53
      Height = 17
      Caption = 'Show'
      TabOrder = 8
      OnClick = CbShowClick
    end
  end
  object Panel5: TPanel
    Left = 860
    Top = 558
    Width = 281
    Height = 151
    Anchors = [akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 5
    object Label22: TLabel
      Left = 8
      Top = 24
      Width = 37
      Height = 13
      Caption = 'Target :'
    end
    object Label23: TLabel
      Left = 173
      Top = 24
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label25: TLabel
      Left = 8
      Top = 48
      Width = 37
      Height = 13
      Caption = 'Above :'
    end
    object Label26: TLabel
      Left = 173
      Top = 48
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label27: TLabel
      Left = 8
      Top = 72
      Width = 35
      Height = 13
      Caption = 'Under :'
    end
    object Label28: TLabel
      Left = 173
      Top = 72
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label29: TLabel
      Left = 8
      Top = 96
      Width = 62
      Height = 13
      Caption = 'Stabilisation :'
    end
    object Label30: TLabel
      Left = 173
      Top = 96
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label32: TLabel
      Left = 85
      Top = 2
      Width = 56
      Height = 13
      Caption = 'Detection'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EdtTarget: TEdit
      Left = 77
      Top = 20
      Width = 89
      Height = 21
      TabOrder = 0
      Text = '1,000'
      OnChange = EdtTargetChange
    end
    object EdtAboveLimit: TEdit
      Left = 77
      Top = 44
      Width = 89
      Height = 21
      TabOrder = 1
      Text = '30'
      OnChange = EdtAboveLimitChange
    end
    object EdtUnderLimit: TEdit
      Left = 77
      Top = 68
      Width = 89
      Height = 21
      TabOrder = 2
      Text = '30'
      OnChange = EdtUnderLimitChange
    end
    object EdtStabilisation: TEdit
      Left = 77
      Top = 92
      Width = 89
      Height = 21
      TabOrder = 3
      Text = '3'
      OnChange = EdtStabilisationChange
    end
    object BtnDetect: TButton
      Left = 8
      Top = 120
      Width = 75
      Height = 25
      Caption = 'Detect'
      TabOrder = 4
      OnClick = BtnDetectClick
    end
    object RgDetectionMode: TRadioGroup
      Left = 192
      Top = 48
      Width = 81
      Height = 65
      Caption = 'Mode'
      ItemIndex = 0
      Items.Strings = (
        'Enter'
        'Leave'
        'Both')
      TabOrder = 5
      OnClick = RgDetectionModeClick
    end
  end
  object Panel6: TPanel
    Left = 861
    Top = 272
    Width = 282
    Height = 113
    Anchors = [akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 6
    object Label33: TLabel
      Left = 80
      Top = 8
      Width = 53
      Height = 13
      Caption = 'Statistics'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label34: TLabel
      Left = 8
      Top = 56
      Width = 19
      Height = 13
      Caption = 'Std:'
    end
    object Label35: TLabel
      Left = 120
      Top = 56
      Width = 30
      Height = 13
      Caption = 'Mean:'
    end
    object StdLabel: TLabel
      Left = 80
      Top = 56
      Width = 15
      Height = 13
      Caption = '0.0'
    end
    object MeanLabel: TLabel
      Left = 176
      Top = 56
      Width = 15
      Height = 13
      Caption = '0.0'
    end
    object Label36: TLabel
      Left = 8
      Top = 72
      Width = 48
      Height = 13
      Caption = 'DeltaMax:'
    end
    object DeltaMaxLabel: TLabel
      Left = 80
      Top = 72
      Width = 15
      Height = 13
      Caption = '0.0'
    end
    object TLabel
      Left = 8
      Top = 32
      Width = 3
      Height = 13
    end
    object Label37: TLabel
      Left = 8
      Top = 24
      Width = 26
      Height = 13
      Caption = 'From:'
    end
    object Label38: TLabel
      Left = 96
      Top = 24
      Width = 16
      Height = 13
      Caption = 'To:'
    end
    object StatisticsFromEdit: TEdit
      Left = 40
      Top = 24
      Width = 33
      Height = 21
      TabOrder = 0
      Text = '0'
    end
    object StatisticsToEdit: TEdit
      Left = 120
      Top = 24
      Width = 33
      Height = 21
      TabOrder = 1
      Text = '10'
    end
    object StatisticsCalculateButton: TButton
      Left = 176
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Calculate'
      TabOrder = 2
      OnClick = StatisticsCalculateButtonClick
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'csv'
    Filter = 'CSV data|*.csv'
    Title = 'Open File'
    Left = 1080
    Top = 32
  end
  object SetupOpenDialog: TOpenDialog
    DefaultExt = 'stp'
    FileName = 'DEFAULT'
    Filter = 'Setup file|*.stp'
    Title = 'Load Setup'
    Left = 1086
    Top = 64
  end
  object SetupSaveDialog: TSaveDialog
    DefaultExt = 'stp'
    FileName = 'DEFAULT'
    Filter = 'Setup file|*.stp'
    Title = 'Save Setup'
    Left = 1038
    Top = 80
  end
  object DetectSaveDialog: TSaveDialog
    DefaultExt = 'csv'
    FileName = 'Detect'
    Filter = 'Detection file|*.csv'
    Title = 'Save Detection'
    Left = 1088
    Top = 568
  end
end
