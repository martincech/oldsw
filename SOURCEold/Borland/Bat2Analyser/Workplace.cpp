//******************************************************************************
//
//   Workplace.cpp   Bat2 generator workplace
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include "../Library/Unisys/Uni.h"
#include "Workplace.h"

#pragma package(smart_init)

#define DEFAULT_WIDTH 100          // default window width
#define DELTA_RANGE   1.0e-6       // minimum samples range

#define NONAME_FILE     "NONAME"

//******************************************************************************
// Constructor
//******************************************************************************

TWorkplace::TWorkplace()
// Constructor
{
   Samples = new TSamples;
   SetDefaults();
} // TWorkplace

//******************************************************************************
// Destructor
//******************************************************************************

TWorkplace::~TWorkplace()
// Destructor
{
   if( Samples){
      delete Samples;
   }
} // TWorkplace

//******************************************************************************
// Defaults
//******************************************************************************

void TWorkplace::SetDefaults()
// Set default parameters
{
   Samples->Clear();

   strcpy( DataFileName,  NONAME_FILE);
   strcpy( SetupFileName, NONAME_FILE);

   WindowOffset = 0;
   WindowWidth  = DEFAULT_WIDTH;
} // SetDefaults

//******************************************************************************
// Load data
//******************************************************************************

BOOL TWorkplace::LoadData( char *FileName)
// Load data from <FileName>
{
   if( !Samples->Load( FileName)){
      strcpy( DataFileName, NONAME_FILE);
      return( FALSE);
   }
   strncpyx( DataFileName, FileName, PATH_MAX);
   return( TRUE);
} // LoadData

//******************************************************************************
// Load setup
//******************************************************************************

BOOL TWorkplace::LoadSetup( char *FileName)
// Load setup data
{
   FILE *f;
   SetDefaults();
   // open file :
   f = fopen( FileName, "r");
   if( !f){
      return( FALSE);
   }
   fscanf( f, "%s\n",  DataFileName);

   fscanf( f, "%d\n",  &WindowOffset);
   fscanf( f, "%d\n",  &WindowWidth);

   fscanf( f, "%d,%d,%lf,%d\n",  &Samples->Prefilter, &Samples->FloatingWindow,
                                 &Samples->StabilisationRange, &Samples->StabilisationDuration);

   fscanf( f, "%lf,%lf,%lf,%lf,%d\n", &Samples->Target, &Samples->AboveLimit, &Samples->UnderLimit,
                                      &Samples->Stabilisation, &Samples->DetectionMode);
   strncpyx( SetupFileName, FileName, PATH_MAX);
   fclose( f);
   if( !LoadData( DataFileName)){
      return( FALSE);
   }
   Samples->Filter();
   return( TRUE);
} // LoadSetup

//******************************************************************************
// Save setup
//******************************************************************************

BOOL TWorkplace::SaveSetup( char *FileName)
// Save setup data
{
   FILE *f;
   strcpy( SetupFileName, NONAME_FILE);
   // open file :
   f = fopen( FileName, "w");
   if( !f){
      return( FALSE);
   }

   fprintf( f, "%s\n",  DataFileName);

   fprintf( f, "%d\n",  WindowOffset);
   fprintf( f, "%d\n",  WindowWidth);

   fprintf( f, "%d,%d,%lf,%d\n",  Samples->Prefilter, Samples->FloatingWindow,
                                  Samples->StabilisationRange, Samples->StabilisationDuration);

   fprintf( f, "%lf,%lf,%lf,%lf,%d\n", Samples->Target, Samples->AboveLimit, Samples->UnderLimit,
                                       Samples->Stabilisation, Samples->DetectionMode);
   fclose( f);
   strncpyx( SetupFileName, FileName, PATH_MAX);
   return( TRUE);
} // SaveSetup

//******************************************************************************
// Seek
//******************************************************************************

void TWorkplace::Begin()
// Seek to begin
{
   WindowOffset = 0;
} // Begin

void TWorkplace::Next()
// Seek to next
{
   WindowOffset += WindowWidth;
   if( WindowOffset + WindowWidth > Samples->Count()){
      End();
      return;
   }
} // Next

void TWorkplace::Previous()
// Seek to previous
{
   WindowOffset -= WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
} // Previous

void TWorkplace::End()
// Seek to end
{
   WindowOffset = Samples->Count() - WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
} // End

//------------------------------------------------------------------------------

