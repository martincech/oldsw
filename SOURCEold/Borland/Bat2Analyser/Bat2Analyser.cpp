//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat2Analyser.res");
USEFORM("MainAnalyser.cpp", MainForm);
USEUNIT("Samples.cpp");
USEUNIT("Workplace.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->Title = "BAT2 Analyser";
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
