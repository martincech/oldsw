//******************************************************************************
//
//   Samples.cpp  Samples database
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Samples.h"
#include <stdlib.h>
#include <math.h>
#include "../Library/Unisys/Uni.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//#define SA_PAGE_COUNT  1024                                    // items per allocation page
#define SA_PAGE_COUNT  1                                    // items per allocation page
#define PageBytes( Size) ((Size) * sizeof( TSamplesItem))      // bytes per <Size> items
#define SA_PAGE_SIZE     PageBytes(SA_PAGE_COUNT)              // bytes per allocation page

#define LINE_SIZE  160

//******************************************************************************
// Constructor
//******************************************************************************

TSamples::TSamples()
// Constructor
{
   Size       = 0;
   ItemsCount = 0;
   Index      = 0;
   List       = 0;
   // data :
   Chop = false;
   Sinc3 = false;
   Rate = 10;
   ContinuousMode = false;
   BridgeOnDelay = 0;
   SamplesCount = 10;
   Space = 0;
   ZeroKg = 0.0;
   FullKg = 10.0;
   ZeroLsb = 0;
   FullLsb = 1000000;
   Duration = 10;
   strcpy( Comment, "No comment");
   // filter :
   Prefilter             = 1;
   FloatingWindow        = 10;
   StabilisationRange    = 0.003;
   StabilisationDuration = 3;
   // detection :
   Target        = 1.0;
   AboveLimit    = 30.0;
   UnderLimit    = 30.0;
   Stabilisation = 3;
   DetectionMode = DETECT_ENTER;
   LastDetected  = 0.0;
   fd            = 0;

   // statistics:
   WindowFrom = 0;
   WindowTo = 0;
} // TSamples

//******************************************************************************
// Destructor
//******************************************************************************

TSamples::~TSamples()
// Destructor
{
   FreeList();
} // ~TSamples

//******************************************************************************
// Load
//******************************************************************************

BOOL TSamples::Load( char *Name)
// Load from file <Name>
{
FILE *f;

   // open file :
   f = fopen( Name, "r");
   if( !f){
      return( FALSE);
   }
   // allocate list :
   if( List){
      free( List);
   }
   List  = (TSamplesItem *)calloc( 1, SA_PAGE_SIZE);
   Size  = SA_PAGE_COUNT;
   if( !List){
      return( FALSE);
   }
   if( !ReadHeader( f)){
      fclose( f);
      FreeList();
      return( FALSE);
   }
   // read items
   int Value;
   ItemsCount = 0;
   forever {
      if( !ReadLine( f, &Value)){
         break;
      }
      // check for space :
      if( ItemsCount >= Size){
         Size += SA_PAGE_COUNT;
         List = (TSamplesItem *)realloc( List, PageBytes( Size));
         if( !List){
            fclose( f);
            FreeList();
            return( FALSE);
         }
      }
      List[ ItemsCount].Time     = ItemsCount;
      List[ ItemsCount].Raw      = Value;
      List[ ItemsCount].Value    = ToKg( Value);
      List[ ItemsCount].Average  = 0;
      List[ ItemsCount].LowPass  = 0;
      List[ ItemsCount].HighPass = 0;
      List[ ItemsCount].Stable   = 0;
      ItemsCount++;
   }
   fclose( f);
   Filter();
   return( TRUE);   
} // Load

//******************************************************************************
// Seek
//******************************************************************************

BOOL TSamples::Seek( int Offset)
// Seek to <Offset>
{
   if( Offset >= ItemsCount){
      return( FALSE);
   }
   if( Offset < 0){
      return( FALSE);
   }
   Index = Offset;
   return( TRUE);
} // Seek

//******************************************************************************
// Count
//******************************************************************************

int TSamples::Count()
// Returns samples count
{
   return( ItemsCount);
} // Count

//******************************************************************************
// Minimum
//******************************************************************************

double TSamples::Min()
// Returns samples minimum
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   double Min = List[ 0].Value;
   for( int i = 1; i < ItemsCount; i++){
      if( List[ i].Value < Min){
         Min = List[ i].Value;
      }
   }
   return( Min);
} // Min

//******************************************************************************
// Maximum
//******************************************************************************

double TSamples::Max()
// Returns samples maximum
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   double Max = List[ 0].Value;
   for( int i = 1; i < ItemsCount; i++){
      if( List[ i].Value > Max){
         Max = List[ i].Value;
      }
   }
   return( Max);
} // Max

//******************************************************************************
// Standard deviation
//******************************************************************************

double TSamples::Std()
// Returns std deviation
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   if(ItemsCount <= WindowTo){
      return( 0.0);
   }


   double MeanValue = Mean();
   double StdDev = 0;

   for(int i = WindowFrom ; i <= WindowTo ; i++) {
       StdDev += (List[ i].Value - MeanValue) * (List[ i].Value - MeanValue);
   }

   return sqrt(StdDev / (WindowTo - WindowFrom + 1));
} // Std

//******************************************************************************
// Mean
//******************************************************************************

double TSamples::Mean()
// Returns std deviation
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   if(ItemsCount <= WindowTo){
      return( 0.0);
   }

   double Mean = 0;

   for(int i = WindowFrom ; i <= WindowTo ; i++) {
      Mean += List[ i].Value;
   }

   return Mean / (WindowTo - WindowFrom + 1);
} // Mean

//******************************************************************************
// DeltaMax
//******************************************************************************

double TSamples::DeltaMax()
// Returns std deviation
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   if(ItemsCount <= WindowTo){
      return( 0.0);
   }


   return MaxWindow() - MinWindow();
} // DeltaMax

//******************************************************************************
// MaxWindow
//******************************************************************************

double TSamples::MaxWindow()
// Returns std deviation
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   if(ItemsCount <= WindowTo){
      return( 0.0);
   }

   double Maximum = List[ WindowFrom].Value;

   for(int i = WindowFrom ; i <= WindowTo ; i++) {
      if(List[ i].Value > Maximum) {
         Maximum = List[ i].Value;
      }
   }

   return Maximum;
} // MaxWindow

//******************************************************************************
// MinWindow
//******************************************************************************

double TSamples::MinWindow()
// Returns std deviation
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   if(ItemsCount <= WindowTo){
      return( 0.0);
   }

   double Minimum = List[ WindowFrom].Value;

   for(int i = WindowFrom ; i <= WindowTo ; i++) {
      if(List[ i].Value < Minimum) {
         Minimum = List[ i].Value;
      }
   }

   return Minimum;
} // MinWindow

//******************************************************************************
// Next
//******************************************************************************

BOOL TSamples::Next( TSamplesItem *Item)
// Returns next sample <Item>
{
   if( Index >= ItemsCount){
      return( FALSE);
   }
   *Item = List[ Index];
   Index++;
   return( TRUE);
} // Next

//******************************************************************************
// Filter
//******************************************************************************

#define GetItem( i)  (((i) < 0) ? &List[ 0] : &List[ i])

void TSamples::Filter()
// Run data filter
{
TSamplesItem *Item;
double Average;
double Sum;
BOOL   Stable;

   if( ItemsCount == 0){
      return;
   }
   if( Prefilter == 0){
      return;
   }
   if( FloatingWindow == 0){
      return;
   }
   // averaging :
   Average = List[ 0].Value;                  // previous average
   Sum = 0;
   for( int i = 0; i < ItemsCount; i++){
      Item = &List[ i];
      Sum += Item->Value;
      if( (i != 0) && (i % Prefilter == 0)){
         Average = Sum / Prefilter;
         Sum = 0;
      }
      Item->Average = Average;
   }

   // Low pass input averaging
   int LowPassPeriod = 1; // count of samples to average
   double LowPassInput;

   // floating window :
   Average = List[ 0].Value;                  // previous average
   for( int i = 0; i < ItemsCount; i++){
      if( (i != 0) && (i % (Prefilter * LowPassPeriod) == 0)){
         Sum = 0;
         for( int j = i - FloatingWindow * Prefilter * LowPassPeriod; j < i; j += Prefilter * LowPassPeriod){
            LowPassInput = 0;

            for(int k = j + LowPassPeriod - 1; k >= j ; k--) {
               Item = GetItem( k);
               LowPassInput += Item->Average;
            }

            Sum += LowPassInput / LowPassPeriod;
         }
         Average = Sum / FloatingWindow;
      }
      Item = &List[ i];
      Item->LowPass  = Average;
      Item->HighPass = Item->Average - Average;
   }

   // stabilisation :
   Average      = ZeroKg;                   // set to zero
   LastDetected = ZeroKg;                   // last detected weight
   for( int i = 0; i < ItemsCount; i++){
      if( (i != 0) && (i % Prefilter == 0)){
         Stable = YES;
         for( int j = i - StabilisationDuration * Prefilter; j < i; j += Prefilter){
             Item = GetItem( j);
             if( fabs( Item->HighPass) > StabilisationRange){
                Stable = NO;
                break;
             }
         }
         if( Stable){
            Average = List[ i].LowPass;     // set stable weight
            DetectWeight( i, Average);      // detect weight
         }
      }
      Item = &List[ i];
      Item->Stable  = Average;
   }
} // Filter

//******************************************************************************
// Detect
//******************************************************************************

BOOL TSamples::Detect( char *Name)
// Run detection to file <Name>
{
   // open file :
   fd = fopen( Name, "w");
   if( !fd){
      return( FALSE);
   }
   // write header data :
   fprintf( fd, "%lf,%lf,%lf\n", Target, AboveLimit, UnderLimit);
   fprintf( fd, "%d,%lf,%d,%d\n", Prefilter, Stabilisation, FloatingWindow, StabilisationDuration);
   // run filter & detection :
   Filter();
   // close data :
   fclose( fd);
   fd = 0;
   return( TRUE);
} // Detect

//******************************************************************************
// Clear
//******************************************************************************

void TSamples::Clear()
// Clear all samples
{
   FreeList();
} // Clear

//******************************************************************************
// Free list
//******************************************************************************

void TSamples::FreeList()
// Free list memory
{
   if( List){
      free( List);
   }
   Size       = 0;
   List       = 0;
   ItemsCount = 0;
} // FreeList

//******************************************************************************
// Read header
//******************************************************************************

BOOL TSamples::ReadHeader( FILE *f)
// Read file header
{
char Buffer[ LINE_SIZE + 1];

   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   sscanf( Buffer, "%d,%d,%d\n", &Rate, &Chop, &Sinc3);
   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   sscanf( Buffer, "%d,%d,%d,%d\n", &ContinuousMode, &SamplesCount, &BridgeOnDelay, &Space);
   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   sscanf( Buffer, "%d,%d\n", &Duration, &Delay);
   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   sscanf( Buffer, "%lf,%d\n", &ZeroKg, &ZeroLsb);
   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   sscanf( Buffer, "%lf,%d\n", &FullKg, &FullLsb);
   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   char *p = strchr( Buffer, '\n');
   if( !p){
      strcpy( Buffer, "?");
   } else {
      *p = '\0';
      strcpy( Comment, Buffer);
   }
   return( TRUE);
} // ReadHeader

//******************************************************************************
// Read line
//******************************************************************************

BOOL TSamples::ReadLine( FILE *f, int *Value)
// Read file line
{
char Buffer[ LINE_SIZE + 1];

   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   sscanf( Buffer, "%d", Value);
   return( TRUE); 
} // ReadLine

//******************************************************************************
// To kg
//******************************************************************************

double TSamples::ToKg( int Value)
// Convert <Value> to kg scale
{
double Kg;

   if( abs( FullLsb - ZeroLsb) == 0){
      return( 0.0);
   } 
   Value -= ZeroLsb;
   Kg = (double)Value * (FullKg - ZeroKg) / (FullLsb - ZeroLsb);
   Kg += ZeroKg;
   return( Kg);
} // ToKg

//******************************************************************************
// Detect weight
//******************************************************************************

void TSamples::DetectWeight( int Index, double Weight)
// Detect stable weight
{
double Delta;

   if( !fd){
      return;                          // file disabled
   }
   Delta = Weight - LastDetected;      // weight increment
   LastDetected = Weight;              // save as last weight
   if( fabs( Delta) > Target + (AboveLimit * Target / 100)){
      return;                          // step to large
   }
   if( fabs( Delta) < Target - (UnderLimit * Target / 100)){
      return;                          // step to small
   }
   char *Direction = "Enter";
   switch( DetectionMode){
      case DETECT_ENTER :
         if( Delta < 0){
            return;                    // leave
         }
         break;

      case DETECT_LEAVE :
         if( Delta > 0){
            return;                    // enter
         }
         Delta     = -Delta;           // correct signum
         Direction = "Leave";
         break;

      case DETECT_BOTH  :
         if( Delta < 0){
            Delta     = -Delta;        // correct signum
            Direction = "Leave";
         }
         break;                        // always save
   }
   fprintf( fd, "%d,%lf,%s\n", Index, Delta, Direction);
} // DetectWeight
