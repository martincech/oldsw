//******************************************************************************
//
//   Samples.cpp  Samples database
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef SamplesH
#define SamplesH

#include "Bat2Device.h"
#include <stdio.h>

// samples item :
typedef struct {
   int    Time;
   dword  Raw;
   double Value;
   double Average;
   double LowPass;
   double HighPass;
   double Stable;
} TSamplesItem;

// detection mode
typedef enum {
   DETECT_ENTER,
   DETECT_LEAVE,
   DETECT_BOTH,
} TDetectionMode;

//******************************************************************************
// TSamples
//******************************************************************************

class TSamples {
public :
   TSamples();
   // Constructor

   ~TSamples();
   // Destructor

   BOOL Load( char *Name);
   // Load from file <Name>

   BOOL Seek( int Offset);
   // Seek to <Offset>

   int Count();
   // Returns samples count

   double Min();
   // Returns samples minimum

   double Max();
   // Returns samples maximum

   double Std();

   double Mean();

   double DeltaMax();

   double MaxWindow();

   double MinWindow();

   BOOL Next( TSamplesItem *Item);
   // Returns next sample <Item>

   void Filter();
   // Run data filter

   BOOL Detect( char *Name);
   // Run detection to file <Name>

   void Clear();
   // Clear all samples

   // input file data :
   BOOL Chop;                          // use CHOP mode / without CHOP
   BOOL Sinc3;                         // use SINC3 filter / SINC4
   int  Rate;                          // conversion rate [Hz]

   BOOL ContinuousMode;                // continuous mode / power save mode
   int  BridgeOnDelay;                 // bridge on delay [Samples]
   int  SamplesCount;                  // sampes burst count [Samples]
   int  Space;                         // space between conversions [ms]

   double ZeroKg;                      // zero weight
   double FullKg;                      // full scale weight
   int    ZeroLsb;                     // zero conversions average
   int    FullLsb;                     // full scale conversions average
   int    Duration;                    // calibration packet count (*  SamplesCount)
   int    Delay;                       // calibration delay samples count

   char   Comment[ BD_MAX_COMMENT + 1];// comment

   // filter data :
   int    Prefilter;                   // initial averaging [Samples]
   int    FloatingWindow;              // floating average window [Prefilter samples]
   double StabilisationRange;          // stabilisation range [kg]
   int    StabilisationDuration;       // stabilisation samples count

   // detection data :
   double Target;                      // target weitght [kg]
   double AboveLimit;                  // limit above target [%]
   double UnderLimit;                  // limit under target [%]
   double Stabilisation;               // stabilisation range [%] of Target
   TDetectionMode DetectionMode;       // detection mode

   double LastDetected;                // last detected weight

   // Statistics
   int WindowFrom;
   int WindowTo;

//------------------------------------------------------------------------------
protected :
   TSamplesItem *List;              // array of items
   int           Size;              // size of array
   int           ItemsCount;        // items count of array
   int           Index;             // actual index
   FILE         *fd;                // detection file

   void FreeList();
   // Free list memory

   BOOL ReadHeader( FILE *f);
   // Read file header

   BOOL ReadLine( FILE *f, int *Value);
   // Read file line

   double ToKg( int Value);
   // Convert <Value> to kg scale

   void DetectWeight( int Index, double Weight);
   // Detect stable weight
}; // TSamples

#endif
