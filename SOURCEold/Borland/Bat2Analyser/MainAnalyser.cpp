//******************************************************************************
//
//   Main.cpp     Bat2 analyser main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainAnalyser.h"
#include <values.h>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;


//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
   Workplace = new TWorkplace;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   UpdateData();
} // FormCreate

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadDataClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;
   if( !Workplace->LoadData( FileOpenDialog->FileName.c_str())){
      Screen->Cursor = crDefault;
      Redraw();
      return;
   }
   Screen->Cursor = crDefault;
   Workplace->Begin();                                      // at first sample
   Workplace->WindowWidth = Workplace->Samples->Count();    // show all samples
   Redraw();
} // BtnLoadClick

//******************************************************************************
// Set defaults
//******************************************************************************

void __fastcall TMainForm::BtnSetDefaultsClick(TObject *Sender)
{
   Workplace->SetDefaults();
   Redraw();
} // BtnSetDefaultsClick

//******************************************************************************
// Load setup
//******************************************************************************

void __fastcall TMainForm::BtnLoadSetupClick(TObject *Sender)
{
   if( !SetupOpenDialog->Execute()){
      return;
   }
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;
   if( !Workplace->LoadSetup( SetupOpenDialog->FileName.c_str())){
      Screen->Cursor = crDefault;
      Redraw();
      return;
   }
   Screen->Cursor = crDefault;
   Redraw();
} // BtnLoadSetupClick

//******************************************************************************
// Save setup
//******************************************************************************

void __fastcall TMainForm::BtnSaveSetupClick(TObject *Sender)
{
   SetupSaveDialog->FileName = Workplace->SetupFileName;
   if( !SetupSaveDialog->Execute()){
      return;
   }
   if( !Workplace->SaveSetup( SetupSaveDialog->FileName.c_str())){
      Redraw();
      return;
   }
   Redraw();
} // BtnSaveSetupClick

//******************************************************************************
// Redraw
//******************************************************************************

void TMainForm::Redraw()
// Redraw graph
{
   UpdateData();
   TSamplesItem Item;
   SamplesSeries->Clear();
   PrefilterSeries->Clear();
   LowPassSeries->Clear();
   HighPassSeries->Clear();
   StableSeries->Clear();
   if( !Workplace->Samples->Seek( Workplace->WindowOffset)){
      return;
   }
   Screen->Cursor = crHourGlass;
   for( int i = 0; i < Workplace->WindowWidth; i++){
      if( !Workplace->Samples->Next( &Item)){
         break;
      }
      if( CbShowData->Checked){
         SamplesSeries->AddXY( Item.Time,
                               Item.Value,
                               "", clTeeColor);
      }
      if( CbShowPrefilter->Checked){
         PrefilterSeries->AddXY( Item.Time,
                               Item.Average,
                               "", clTeeColor);
      }
      if( CbShowWindow->Checked){
         LowPassSeries->AddXY( Item.Time,
                               Item.LowPass,
                               "", clTeeColor);
      }
      if( CbShowRange->Checked){
         HighPassSeries->AddXY( Item.Time,
                                Item.HighPass,
                                "", clTeeColor);
      }
      if( CbShowDuration->Checked){
         StableSeries->AddXY( Item.Time,
                                Item.Stable,
                                "", clTeeColor);
      }
   }
   Screen->Cursor = crDefault;
/*
   try {
      // disable range checking :
      Graph->LeftAxis->Maximum =  MAXDOUBLE;
      Graph->LeftAxis->Minimum = -MAXDOUBLE;
      // set range :
      Graph->LeftAxis->Maximum = Workplace->Samples->Max();
      Graph->LeftAxis->Minimum = Workplace->Samples->Min();
   } catch( ...){
      Application->MessageBox( "There is something wrong with the fucking graph scales", "Error", MB_OK);
   }
*/
} // Redraw

//******************************************************************************
// Update data window
//******************************************************************************

void TMainForm::UpdateData()
{
   // resorce names :
   LblDataName->Caption  = Workplace->DataFileName;
   LblSetupName->Caption = Workplace->SetupFileName;
   // window boundary :
   EdtOffset->Text = AnsiString( Workplace->WindowOffset);
   EdtWidth->Text  = AnsiString( Workplace->WindowWidth);
   // data span :
   LblMin->Caption   = FloatToStrF( Workplace->Samples->Min(), ffFixed, 9, 4);
   LblMax->Caption   = FloatToStrF( Workplace->Samples->Max(), ffFixed, 9, 4);
   LblCount->Caption = AnsiString( Workplace->Samples->Count());
   // data range :
   LblRangeMin->Caption = AnsiString( Workplace->Samples->ZeroKg) + " kg";
   LblRangeMax->Caption = AnsiString( Workplace->Samples->FullKg) + " kg";
   LblSinc3->Caption    = Workplace->Samples->Sinc3 ? "SINC3" : "SINC4";
   LblChop->Caption     = Workplace->Samples->Chop  ? "CHOP" : "NOCHOP";
   LblRate->Caption     = AnsiString( Workplace->Samples->Rate) + "Hz";
   AnsiString Mode;
   if( Workplace->Samples->ContinuousMode){
      Mode.printf( "CONT:%d", Workplace->Samples->SamplesCount);
   } else {
      Mode.printf( "PWSV:%d:%d:%d", Workplace->Samples->BridgeOnDelay,
                                    Workplace->Samples->SamplesCount,
                                    Workplace->Samples->Space);
   }
   LblMode->Caption = Mode;
   LblComment->Caption = Workplace->Samples->Comment;
   // Filter :
   EdtPrefilter->Text = AnsiString( Workplace->Samples->Prefilter);
   EdtWindow->Text    = AnsiString( Workplace->Samples->FloatingWindow);
   EdtRange->Text     = AnsiString( Workplace->Samples->StabilisationRange);
   EdtStabilisationDuration->Text  = AnsiString( Workplace->Samples->StabilisationDuration);
   // Detection :
   EdtTarget->Text        = AnsiString( Workplace->Samples->Target);
   EdtAboveLimit->Text    = AnsiString( Workplace->Samples->AboveLimit);
   EdtUnderLimit->Text    = AnsiString( Workplace->Samples->UnderLimit);
   EdtStabilisation->Text = AnsiString( Workplace->Samples->Stabilisation);
   RgDetectionMode->ItemIndex = (int)Workplace->Samples->DetectionMode;
} // UpdateData

//******************************************************************************
// Redraw
//******************************************************************************

void __fastcall TMainForm::BtnRedrawClick(TObject *Sender)
{
   Redraw();
} // BtnRedrawClick

//******************************************************************************
// Begin
//******************************************************************************

void __fastcall TMainForm::BtnBeginClick(TObject *Sender)
{
   Workplace->Begin();
   Redraw();
} // BtnBeginClick

//******************************************************************************
// Previous
//******************************************************************************

void __fastcall TMainForm::BtnPrevClick(TObject *Sender)
{
   Workplace->Previous();
   Redraw();
} // BtnPrevClick

//******************************************************************************
// Next
//******************************************************************************

void __fastcall TMainForm::BtnNextClick(TObject *Sender)
{
   Workplace->Next();
   Redraw();
} // BtnNextClick

//******************************************************************************
// End
//******************************************************************************

void __fastcall TMainForm::BtnEndClick(TObject *Sender)
{
   Workplace->End();
   Redraw();
} // BtnEndClick

//******************************************************************************
// Width
//******************************************************************************

void __fastcall TMainForm::EdtWidthChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtWidth->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->WindowWidth = Value;
} // EdtWidthChange

//******************************************************************************
// Offset
//******************************************************************************

void __fastcall TMainForm::EdtOffsetChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtOffset->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->WindowOffset = Value;
} // EdtOffsetChange

//******************************************************************************
// Prefilter
//******************************************************************************

void __fastcall TMainForm::EdtPrefilterChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtPrefilter->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->Samples->Prefilter = Value;
}

//******************************************************************************
// Window
//******************************************************************************

void __fastcall TMainForm::EdtWindowChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtWindow->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->Samples->FloatingWindow = Value;
}

//******************************************************************************
//  Stabilisation range
//******************************************************************************

void __fastcall TMainForm::EdtRangeChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtRange->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->Samples->StabilisationRange = Value;
}

//******************************************************************************
// Stabilisation width
//******************************************************************************

void __fastcall TMainForm::EdtStabilisationDurationChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtStabilisationDuration->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->Samples->StabilisationDuration = Value;
}

//******************************************************************************
// Recalculate
//******************************************************************************

void __fastcall TMainForm::BtnRecalculateClick(TObject *Sender)
{
   Workplace->Samples->Filter();
   Redraw();
}

//******************************************************************************
// Show click
//******************************************************************************

void __fastcall TMainForm::CbShowClick(TObject *Sender)
{
   Redraw();
}

//******************************************************************************
// Target
//******************************************************************************

void __fastcall TMainForm::EdtTargetChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtTarget->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->Samples->Target = Value;
   // update stabilisation :
   Workplace->Samples->StabilisationRange = Workplace->Samples->Stabilisation * Value / 100;
   EdtRange->Text = AnsiString( Workplace->Samples->StabilisationRange);
}

//******************************************************************************
// Above limit
//******************************************************************************

void __fastcall TMainForm::EdtAboveLimitChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtAboveLimit->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->Samples->AboveLimit = Value;
}

//******************************************************************************
// Under limit
//******************************************************************************

void __fastcall TMainForm::EdtUnderLimitChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtUnderLimit->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->Samples->UnderLimit = Value;
}

//******************************************************************************
// Stabilisation
//******************************************************************************

void __fastcall TMainForm::EdtStabilisationChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtStabilisation->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->Samples->Stabilisation = Value;
   // update stabilisation :
   Workplace->Samples->StabilisationRange = Value * Workplace->Samples->Target / 100;
   EdtRange->Text = AnsiString( Workplace->Samples->StabilisationRange);
}

//******************************************************************************
// Mode
//******************************************************************************

void __fastcall TMainForm::RgDetectionModeClick(TObject *Sender)
{
   Workplace->Samples->DetectionMode = (TDetectionMode)RgDetectionMode->ItemIndex;
}

//******************************************************************************
// Detect
//******************************************************************************

void __fastcall TMainForm::BtnDetectClick(TObject *Sender)
{
   if( !DetectSaveDialog->Execute()){
      return;
   }
   if( !Workplace->Samples->Detect( DetectSaveDialog->FileName.c_str())){
      return;
   }
}

//******************************************************************************
// Statistics
//******************************************************************************

void __fastcall TMainForm::StatisticsCalculateButtonClick(TObject *Sender)
{


   char StringBuffer[50];

   Workplace->Samples->WindowFrom = StatisticsFromEdit->Text.ToInt();
   Workplace->Samples->WindowTo = StatisticsToEdit->Text.ToInt();

   sprintf(StringBuffer, "%0.2f", Workplace->Samples->Std());
   StdLabel->Caption = StringBuffer;

   sprintf(StringBuffer, "%0.2f", Workplace->Samples->Mean());
   MeanLabel->Caption = StringBuffer;

   sprintf(StringBuffer, "%0.2f", Workplace->Samples->DeltaMax());
   DeltaMaxLabel->Caption = StringBuffer;
}
//---------------------------------------------------------------------------

