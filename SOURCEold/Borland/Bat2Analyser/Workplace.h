//******************************************************************************
//
//   Workplace.h     Bat2 generator workplace
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef WorkplaceH
#define WorkplaceH

#include "Samples.h"

#define PATH_MAX 255

//******************************************************************************
// TWorkplace
//******************************************************************************

class TWorkplace {
public :
   TWorkplace();
   // Constructor

   ~TWorkplace();
   // Destructor

   void SetDefaults();
   // Set default parameters

   BOOL LoadData( char *FileName);
   // Load data from <FileName>

   BOOL LoadSetup( char *FileName);
   // Load setup data

   BOOL SaveSetup( char *FileName);
   // Save setup data

   void Begin();
   // Seek to begin

   void Next();
   // Seek to next

   void Previous();
   // Seek to previous

   void End();
   // Seek to end

   char      DataFileName[  PATH_MAX + 1];
   char      SetupFileName[ PATH_MAX + 1];

   int       WindowOffset;
   int       WindowWidth;

   TSamples *Samples;
//------------------------------------------------------------------------------
protected :
}; // TWorkplace

#endif
