//******************************************************************************
//
//   Main.h       BAT2 measure main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainRecorderH
#define MainRecorderH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Library/Crt/Crt.h"
#include "../Library/Serial/CrtLogger.h"
#include "Bat2Device.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TMemo *MainMemo;
   TButton *BtnConnect;
   TEdit *EdtCom;
   TPanel *Panel1;
   TLabel *Label1;
   TEdit *EdtRate;
   TRadioGroup *RgMode;
   TCheckBox *CbChop;
   TCheckBox *CbSinc3;
   TLabel *Label4;
   TLabel *Label5;
   TLabel *Label2;
   TEdit *EdtBridgeOnDelay;
   TLabel *Label3;
   TLabel *Label6;
   TEdit *EdtSamplesCount;
   TLabel *Label7;
   TLabel *Label8;
   TEdit *EdtSpace;
   TLabel *Label9;
   TOpenDialog *FileOpenDialog;
   TLabel *Label10;
   TLabel *LblFileName;
   TCheckBox *CbLogger;
   TTimer *Timer;
   TPanel *Panel2;
   TButton *BtnSelectFile;
   TButton *BtnRun;
   TLabel *LblCounter;
   TButton *BtnStop;
   TPanel *Panel3;
   TButton *BtnZero;
   TEdit *EdtZeroKg;
   TLabel *Label11;
   TEdit *EdtZeroLsb;
   TLabel *Label12;
   TButton *BtnFull;
   TEdit *EdtFullKg;
   TLabel *Label13;
   TEdit *EdtFullLsb;
   TLabel *Label14;
   TLabel *Label15;
   TEdit *EdtDuration;
   TLabel *Label16;
   TLabel *Label17;
   TEdit *EdtComment;
   TLabel *Label18;
   TEdit *EdtDelay;
   TLabel *Label19;
   void __fastcall FormResize(TObject *Sender);
   void __fastcall CbLoggerClick(TObject *Sender);
   void __fastcall BtnConnectClick(TObject *Sender);
   void __fastcall CbChopClick(TObject *Sender);
   void __fastcall CbSinc3Click(TObject *Sender);
   void __fastcall EdtRateChange(TObject *Sender);
   void __fastcall RgModeClick(TObject *Sender);
   void __fastcall EdtSamplesCountChange(TObject *Sender);
   void __fastcall EdtBridgeOnDelayChange(TObject *Sender);
   void __fastcall EdtSpaceChange(TObject *Sender);
   void __fastcall BtnSelectFileClick(TObject *Sender);
   void __fastcall BtnRunClick(TObject *Sender);
   void __fastcall BtnStopClick(TObject *Sender);
   void __fastcall TimerTimer(TObject *Sender);
   void __fastcall BtnZeroClick(TObject *Sender);
   void __fastcall BtnFullClick(TObject *Sender);
   void __fastcall EdtZeroKgChange(TObject *Sender);
   void __fastcall EdtFullKgChange(TObject *Sender);
   void __fastcall EdtDurationChange(TObject *Sender);
   void __fastcall EdtCommentChange(TObject *Sender);
   void __fastcall EdtDelayChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   TCrt        *Crt;
   TCrtLogger  *Logger;
   TBat2Device *Bat;
   BOOL        Running;
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
