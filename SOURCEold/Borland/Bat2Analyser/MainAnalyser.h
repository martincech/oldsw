//******************************************************************************
//
//   Main.h       Bat2 analyser main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainAnalyserH
#define MainAnalyserH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//---------------------------------------------------------------------------

#include "Workplace.h"

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TPanel *Panel1;
   TButton *BtnLoadData;
   TOpenDialog *FileOpenDialog;
   TChart *Graph;
   TLineSeries *SamplesSeries;
   TPanel *Panel2;
   TEdit *EdtWidth;
   TLabel *Label2;
   TButton *BtnBegin;
   TButton *BtnPrev;
   TButton *BtnNext;
   TButton *BtnEnd;
   TEdit *EdtOffset;
   TLabel *Label3;
   TButton *BtnRedraw;
   TLabel *Label1;
   TLabel *LblDataName;
   TButton *BtnLoadSetup;
   TButton *BtnSaveSetup;
   TLabel *Label4;
   TLabel *LblSetupName;
   TOpenDialog *SetupOpenDialog;
   TSaveDialog *SetupSaveDialog;
   TPanel *Panel3;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TLabel *LblMin;
   TLabel *LblMax;
   TLabel *Label9;
   TLabel *Label8;
   TLabel *Label10;
   TPanel *Panel4;
   TButton *BtnSetDefaults;
   TLabel *Label24;
   TLabel *LblCount;
   TLabel *LblRangeMin;
   TLabel *LblRangeMax;
   TLabel *LblSinc3;
   TLabel *LblChop;
   TLabel *LblRate;
   TLabel *Label11;
   TLabel *LblMode;
   TLabel *Label12;
   TLabel *Label13;
   TLabel *LblComment;
   TLabel *Label14;
   TEdit *EdtPrefilter;
   TLabel *Label15;
   TCheckBox *CbShowPrefilter;
   TLabel *Label16;
   TEdit *EdtWindow;
   TLabel *Label17;
   TCheckBox *CbShowWindow;
   TLabel *Label18;
   TEdit *EdtRange;
   TLabel *Label19;
   TCheckBox *CbShowRange;
   TButton *BtnRecalculate;
   TCheckBox *CbShowData;
   TLineSeries *PrefilterSeries;
   TLineSeries *LowPassSeries;
   TLineSeries *HighPassSeries;
   TLabel *Label20;
   TEdit *EdtStabilisationDuration;
   TLabel *Label21;
   TCheckBox *CbShowDuration;
   TLineSeries *StableSeries;
   TPanel *Panel5;
   TLabel *Label22;
   TEdit *EdtTarget;
   TLabel *Label23;
   TLabel *Label25;
   TEdit *EdtAboveLimit;
   TLabel *Label26;
   TLabel *Label27;
   TEdit *EdtUnderLimit;
   TLabel *Label28;
   TLabel *Label29;
   TEdit *EdtStabilisation;
   TLabel *Label30;
   TButton *BtnDetect;
   TSaveDialog *DetectSaveDialog;
   TRadioGroup *RgDetectionMode;
   TLabel *Label31;
   TLabel *Label32;
   TPanel *Panel6;
   TLabel *Label33;
   TEdit *StatisticsFromEdit;
   TEdit *StatisticsToEdit;
   TButton *StatisticsCalculateButton;
   TLabel *Label34;
   TLabel *Label35;
   TLabel *StdLabel;
   TLabel *MeanLabel;
   TLabel *Label36;
   TLabel *DeltaMaxLabel;
   TLabel *Label37;
   TLabel *Label38;
   void __fastcall BtnLoadDataClick(TObject *Sender);
   void __fastcall BtnBeginClick(TObject *Sender);
   void __fastcall BtnPrevClick(TObject *Sender);
   void __fastcall BtnNextClick(TObject *Sender);
   void __fastcall BtnEndClick(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall EdtWidthChange(TObject *Sender);
   void __fastcall EdtOffsetChange(TObject *Sender);
   void __fastcall BtnRedrawClick(TObject *Sender);
   void __fastcall BtnLoadSetupClick(TObject *Sender);
   void __fastcall BtnSaveSetupClick(TObject *Sender);
   void __fastcall BtnSetDefaultsClick(TObject *Sender);
   void __fastcall EdtPrefilterChange(TObject *Sender);
   void __fastcall EdtWindowChange(TObject *Sender);
   void __fastcall EdtRangeChange(TObject *Sender);
   void __fastcall BtnRecalculateClick(TObject *Sender);
   void __fastcall EdtStabilisationDurationChange(TObject *Sender);
   void __fastcall CbShowClick(TObject *Sender);
   void __fastcall EdtTargetChange(TObject *Sender);
   void __fastcall EdtAboveLimitChange(TObject *Sender);
   void __fastcall EdtUnderLimitChange(TObject *Sender);
   void __fastcall EdtStabilisationChange(TObject *Sender);
   void __fastcall BtnDetectClick(TObject *Sender);
   void __fastcall RgDetectionModeClick(TObject *Sender);
   void __fastcall StatisticsCalculateButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   void Redraw();
   void UpdateData();
   void EnablePlay( BOOL Enable);

   TWorkplace *Workplace;
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
