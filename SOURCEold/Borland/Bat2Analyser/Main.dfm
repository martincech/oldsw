object MainForm: TMainForm
  Left = 504
  Top = 251
  Width = 654
  Height = 563
  Caption = 'BAT2 Measure'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 8
    Top = 512
    Width = 22
    Height = 13
    Caption = 'File :'
  end
  object LblFileName: TLabel
    Left = 40
    Top = 512
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblZero: TLabel
    Left = 528
    Top = 448
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblFullScale: TLabel
    Left = 528
    Top = 472
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblCounter: TLabel
    Left = 504
    Top = 376
    Width = 6
    Height = 13
    Caption = '0'
  end
  object MainMemo: TMemo
    Left = 8
    Top = 8
    Width = 393
    Height = 497
    Lines.Strings = (
      '')
    TabOrder = 0
  end
  object BtnConnect: TButton
    Left = 408
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 1
    OnClick = BtnConnectClick
  end
  object EdtCom: TEdit
    Left = 488
    Top = 9
    Width = 89
    Height = 21
    TabOrder = 2
    Text = 'COM1'
  end
  object Panel1: TPanel
    Left = 408
    Top = 40
    Width = 233
    Height = 281
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 84
      Top = 16
      Width = 92
      Height = 13
      Caption = 'ADC parameters'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 20
      Top = 80
      Width = 29
      Height = 13
      Caption = 'Rate :'
    end
    object Label5: TLabel
      Left = 140
      Top = 80
      Width = 13
      Height = 13
      Caption = 'Hz'
    end
    object Label2: TLabel
      Left = 12
      Top = 216
      Width = 81
      Height = 13
      Caption = 'Bridge On delay :'
    end
    object Label3: TLabel
      Left = 180
      Top = 216
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label6: TLabel
      Left = 12
      Top = 184
      Width = 76
      Height = 13
      Caption = 'Samples count :'
    end
    object Label7: TLabel
      Left = 180
      Top = 184
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label8: TLabel
      Left = 12
      Top = 240
      Width = 37
      Height = 13
      Caption = 'Space :'
    end
    object Label9: TLabel
      Left = 180
      Top = 240
      Width = 13
      Height = 13
      Caption = 'ms'
    end
    object EdtRate: TEdit
      Left = 64
      Top = 76
      Width = 65
      Height = 21
      TabOrder = 0
      Text = '50'
      OnChange = EdtRateChange
    end
    object RgMode: TRadioGroup
      Left = 16
      Top = 104
      Width = 185
      Height = 57
      Caption = 'Conversion Mode'
      ItemIndex = 1
      Items.Strings = (
        'Continuous'
        'Power save')
      TabOrder = 1
      OnClick = RgModeClick
    end
    object CbChop: TCheckBox
      Left = 24
      Top = 32
      Width = 57
      Height = 17
      Caption = 'Chop'
      TabOrder = 2
      OnClick = CbChopClick
    end
    object CbSinc3: TCheckBox
      Left = 24
      Top = 48
      Width = 57
      Height = 17
      Caption = 'Sinc3'
      TabOrder = 3
      OnClick = CbSinc3Click
    end
    object EdtBridgeOnDelay: TEdit
      Left = 104
      Top = 212
      Width = 65
      Height = 21
      TabOrder = 4
      Text = '0'
      OnChange = EdtBridgeOnDelayChange
    end
    object EdtSamplesCount: TEdit
      Left = 104
      Top = 180
      Width = 65
      Height = 21
      TabOrder = 5
      Text = '10'
      OnChange = EdtSamplesCountChange
    end
    object EdtSpace: TEdit
      Left = 104
      Top = 236
      Width = 65
      Height = 21
      TabOrder = 6
      Text = '0'
      OnChange = EdtSpaceChange
    end
  end
  object BtnSelectFile: TButton
    Left = 408
    Top = 336
    Width = 75
    Height = 25
    Caption = 'Select File'
    TabOrder = 4
    OnClick = BtnSelectFileClick
  end
  object BtnRun: TButton
    Left = 408
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 5
    OnClick = BtnRunClick
  end
  object BtnStop: TButton
    Left = 408
    Top = 400
    Width = 75
    Height = 25
    Caption = 'Stop'
    Enabled = False
    TabOrder = 6
    OnClick = BtnStopClick
  end
  object BtnCalibrateZero: TButton
    Left = 408
    Top = 440
    Width = 97
    Height = 25
    Caption = 'Calibrate Zero'
    TabOrder = 7
    OnClick = BtnCalibrateZeroClick
  end
  object BtnCalibrateFullScale: TButton
    Left = 408
    Top = 472
    Width = 97
    Height = 25
    Caption = 'Calibrate Full Scale'
    TabOrder = 8
    OnClick = BtnCalibrateFullScaleClick
  end
  object CbLogger: TCheckBox
    Left = 544
    Top = 512
    Width = 97
    Height = 17
    Caption = 'Enable logger'
    Checked = True
    State = cbChecked
    TabOrder = 9
    OnClick = CbLoggerClick
  end
  object BtnSendParameters: TButton
    Left = 536
    Top = 400
    Width = 97
    Height = 25
    Caption = 'Send Parameters'
    TabOrder = 10
    OnClick = BtnSendParametersClick
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'csv'
    FileName = 'NONAME'
    Filter = 'CSV samples|*.csv'
    Title = 'Select File'
    Left = 552
    Top = 336
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 608
    Top = 344
  end
end
