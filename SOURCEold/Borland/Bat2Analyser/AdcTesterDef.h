//*****************************************************************************
//
//    AdcTesterDef.h  Bat2 ADC tester definitions
//    Version 1.0     (c) VymOs
//
//*****************************************************************************

#ifndef AdcTesterDefH
#define AdcTesterDefH

#pragma pack( push, 1)                    // byte alignment
   #include "../../AVR8/Bat2/AdcTesterDef.h"
#pragma pack( pop)                        // original alignment

#endif
