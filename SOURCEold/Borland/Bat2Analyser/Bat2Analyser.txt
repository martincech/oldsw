BAT2 Analyser

Program slouzi k predbezne analyze dat nahranych
programem Bat2Recorder.

Tlacitkem Load Data nacteme datovy soubor, zaznamenany
Bat2Recorder. Tlacitkem Save Setup se ulozi nastaveni
programu. Tlacitkem Load Setup vyvolame ulozene
nastaveni programu.

V panelu Data jsou parametry dat a AD prevodniku ziskane
v programu Bat2Recorder. Pole Show zobrazi v grafu
vzorky prepocitane na hmotnost).

V panelu Filter jsou parametry filtrace :

Prefilter pocet vzorku, ze kterych se spocita
aritmeticky prumer a je pouzit jako jeden vzorek
do dalsiho zpracovani. Tlacitkem Show se zobrazi
prubeh do grafu.

Window pocet predfiltrovanych vzorku pro klouzavy
prumer (dolni propust). Tlacitkem Show se zobrazi
v grafu.

Range je rozsah ustaleni. Tlacitkem Show se zobrazi
prubeh horni propusti v grafu.

Duration je pocet predfiltrovanych vzorku, ktere,
pokud jsou v rozsahu Range, zpusobi ulozeni
hmotnosti. Tlacitkem Show zobrazime ulozenou
hmotnost v grafu.

POZOR : pri zmene parametru filtrace je treba
stisknout tlacitko Recalculate, aby se vypocital
novy prubeh.

V panelu Detection jsou parametry detekce nastupu/seskoku

V poli Target se zadava cilova hmotnost.
V poli Above se zadava horni mez cilove hmotnosti
(v % z Target)
V poli Under se zadava dolni mez cilove hmotnosti
(v % z Target)
V poli Stabilisation je rozsah stabilni hodnoty
(v % z Target)
V poli Mode zadame rezim detekce (nastup, seskok, nastup i seskok)

Pozn : pri zmene Target a Stabilisation se meni nastaveni 
pole Range v panelu filtru.

Tlacitkem Detect zvolime soubor do ktereho se ulozi
navazene hmotnosti. Soubor je textovy a je mozne 
jej zpracovat napr. v Excelu.

S grafem se da pohybovat pravym tlacitkem mysi,
je-li kurzor uvnitr grafu. Pravym tlacitkem
mysi je mozne zvolit okno, ktere se zvetsi
na celou plochu grafu. Puvodni zobrazeni
se provede pokud okno zadame z praveho
spodniho rohu do leveho horniho rohu
