//******************************************************************************
//
//   Main.h       BAT2 measure main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Library/Crt/Crt.h"
#include "../Library/Serial/CrtLogger.h"
#include "Bat2Device.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TMemo *MainMemo;
   TButton *BtnConnect;
   TEdit *EdtCom;
   TPanel *Panel1;
   TLabel *Label1;
   TEdit *EdtRate;
   TRadioGroup *RgMode;
   TCheckBox *CbChop;
   TCheckBox *CbSinc3;
   TLabel *Label4;
   TLabel *Label5;
   TLabel *Label2;
   TEdit *EdtBridgeOnDelay;
   TLabel *Label3;
   TLabel *Label6;
   TEdit *EdtSamplesCount;
   TLabel *Label7;
   TLabel *Label8;
   TEdit *EdtSpace;
   TLabel *Label9;
   TButton *BtnSelectFile;
   TButton *BtnRun;
   TButton *BtnStop;
   TOpenDialog *FileOpenDialog;
   TLabel *Label10;
   TLabel *LblFileName;
   TButton *BtnCalibrateZero;
   TButton *BtnCalibrateFullScale;
   TLabel *LblZero;
   TLabel *LblFullScale;
   TCheckBox *CbLogger;
   TButton *BtnSendParameters;
   TTimer *Timer;
   TLabel *LblCounter;
   void __fastcall FormResize(TObject *Sender);
   void __fastcall CbLoggerClick(TObject *Sender);
   void __fastcall BtnConnectClick(TObject *Sender);
   void __fastcall CbChopClick(TObject *Sender);
   void __fastcall CbSinc3Click(TObject *Sender);
   void __fastcall EdtRateChange(TObject *Sender);
   void __fastcall RgModeClick(TObject *Sender);
   void __fastcall EdtSamplesCountChange(TObject *Sender);
   void __fastcall EdtBridgeOnDelayChange(TObject *Sender);
   void __fastcall EdtSpaceChange(TObject *Sender);
   void __fastcall BtnSelectFileClick(TObject *Sender);
   void __fastcall BtnRunClick(TObject *Sender);
   void __fastcall BtnStopClick(TObject *Sender);
   void __fastcall BtnCalibrateZeroClick(TObject *Sender);
   void __fastcall BtnCalibrateFullScaleClick(TObject *Sender);
   void __fastcall BtnSendParametersClick(TObject *Sender);
   void __fastcall TimerTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   TCrt        *Crt;
   TCrtLogger  *Logger;
   TBat2Device *Bat;
   BOOL        Running;
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
