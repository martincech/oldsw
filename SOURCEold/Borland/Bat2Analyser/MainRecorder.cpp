//******************************************************************************
//
//   Main.cpp     BAT2 measure main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainRecorder.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
   Crt          = new TCrt( MainMemo);
   Logger       = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Bat          = new TBat2Device;
   Bat->Logger  = 0;
   Bat->SetCom( "COM1");
   Bat->ContinuousMode = FALSE;
   Bat->Rate    = 50;
   Running      = FALSE;
} // TMainForm

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Enable Logger
//******************************************************************************

void __fastcall TMainForm::CbLoggerClick(TObject *Sender)
{
   if( CbLogger->Checked){
      Bat->Logger = Logger;
   } else {
      Bat->Logger = 0;
   }
}
//******************************************************************************
// Connect
//******************************************************************************

void __fastcall TMainForm::BtnConnectClick(TObject *Sender)
{
   Bat->SetCom( EdtCom->Text.c_str());
}

//******************************************************************************
// Chop
//******************************************************************************

void __fastcall TMainForm::CbChopClick(TObject *Sender)
{
   Bat->Chop = CbChop->Checked;
}

//******************************************************************************
// Sinc3
//******************************************************************************

void __fastcall TMainForm::CbSinc3Click(TObject *Sender)
{
   Bat->Sinc3 = CbSinc3->Checked;
}

//******************************************************************************
// Rate
//******************************************************************************

void __fastcall TMainForm::EdtRateChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtRate->Text.ToInt();
   } catch( ...){
      return;
   }
   Bat->Rate = Value;
}

//******************************************************************************
// Mode
//******************************************************************************

void __fastcall TMainForm::RgModeClick(TObject *Sender)
{
   Bat->ContinuousMode = RgMode->ItemIndex == 0;
   if( Bat->ContinuousMode){
      EdtBridgeOnDelay->Enabled = false;
      EdtSpace->Enabled = false;
   } else {
      EdtBridgeOnDelay->Enabled = true;
      EdtSpace->Enabled = true;
   }
}

//******************************************************************************
// Samples count
//******************************************************************************

void __fastcall TMainForm::EdtSamplesCountChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtSamplesCount->Text.ToInt();
   } catch( ...){
      return;
   }
   Bat->SamplesCount = Value;
}

//******************************************************************************
// Bridge on
//******************************************************************************

void __fastcall TMainForm::EdtBridgeOnDelayChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtBridgeOnDelay->Text.ToInt();
   } catch( ...){
      return;
   }
   Bat->BridgeOnDelay = Value;
}

//******************************************************************************
// Space
//******************************************************************************

void __fastcall TMainForm::EdtSpaceChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtSpace->Text.ToInt();
   } catch( ...){
      return;
   }
   Bat->Space = Value;
}

//******************************************************************************
// Select file
//******************************************************************************

void __fastcall TMainForm::BtnSelectFileClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   Bat->SetFile( FileOpenDialog->FileName.c_str());
   LblFileName->Caption = FileOpenDialog->FileName;
}

//******************************************************************************
// Run
//******************************************************************************

void __fastcall TMainForm::BtnRunClick(TObject *Sender)
{
   // read Full/Zero LSB :
   int Value;
   try {
      Value = EdtZeroLsb->Text.ToInt();
   } catch( ...){
      Crt->printf( "Invalid Zero LSB\n");
      return;
   }
   Bat->ZeroLsb = Value;
   try {
      Value = EdtFullLsb->Text.ToInt();
   } catch( ...){
      Crt->printf( "Invalid Full LSB\n");
      return;
   }
   Bat->FullLsb = Value;
   // update status :
   BtnStop->Enabled = true;
   BtnRun->Enabled = false;
   LblCounter->Caption = "0";
   LblCounter->Refresh();
   // run record :
   Running      = TRUE;
   if( !Bat->Run()){
      Crt->printf( "Unable run (Do You enter file name ?)\n");
   }
   Running      = FALSE;
   BtnStop->Enabled = false;
   BtnRun->Enabled = true;
}

//******************************************************************************
// Stop
//******************************************************************************

void __fastcall TMainForm::BtnStopClick(TObject *Sender)
{
   BtnStop->Enabled = false;
   BtnRun->Enabled = true;
   Running      = FALSE;
   Bat->Stop();
}

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TMainForm::TimerTimer(TObject *Sender)
{
   if( !Running){
      return;
   }
   LblCounter->Caption = AnsiString( Bat->SamplesDone);
   LblCounter->Refresh();
}

//******************************************************************************
// Zero
//******************************************************************************

void __fastcall TMainForm::BtnZeroClick(TObject *Sender)
{
   LblCounter->Caption = "0";
   LblCounter->Refresh();
   Running = TRUE;   // show progress
   if( !Bat->MeasureZero()){
      Running = FALSE;
      Crt->printf( "Unable measure zero\n");
      return;
   }
   Running = FALSE;
   LblCounter->Caption = AnsiString( Bat->SamplesDone);
   EdtZeroLsb->Text = AnsiString( Bat->ZeroLsb);
}
//******************************************************************************
// Full
//******************************************************************************

void __fastcall TMainForm::BtnFullClick(TObject *Sender)
{
   LblCounter->Caption = "0";
   LblCounter->Refresh();
   Running = TRUE;   // show progress
   if( !Bat->MeasureFullScale()){
      Running      = FALSE;
      Crt->printf( "Unable measure full scale\n");
      return;
   }
   Running = FALSE;
   LblCounter->Caption = AnsiString( Bat->SamplesDone);
   EdtFullLsb->Text = AnsiString( Bat->FullLsb);
}

//******************************************************************************
// Edit zero kg
//******************************************************************************

void __fastcall TMainForm::EdtZeroKgChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtZeroKg->Text.ToDouble();
   } catch( ...){
      return;
   }
   Bat->ZeroKg = Value;
}

//******************************************************************************
// Edit full kg
//******************************************************************************

void __fastcall TMainForm::EdtFullKgChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtFullKg->Text.ToDouble();
   } catch( ...){
      return;
   }
   Bat->FullKg = Value;
}

//******************************************************************************
// Edit duration
//******************************************************************************

void __fastcall TMainForm::EdtDurationChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtDuration->Text.ToInt();
   } catch( ...){
      return;
   }
   Bat->Duration = Value;
}

//******************************************************************************
// Edit Delay
//******************************************************************************

void __fastcall TMainForm::EdtDelayChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtDuration->Text.ToInt();
   } catch( ...){
      return;
   }
   Bat->Delay = Value;
}

//******************************************************************************
// Edit comment
//******************************************************************************

void __fastcall TMainForm::EdtCommentChange(TObject *Sender)
{
   strncpyx( Bat->Comment, EdtComment->Text.c_str(), BD_MAX_COMMENT);
}


