//******************************************************************************
//
//   Bat2Device.cpp  BAT2 device interface
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Bat2Device.h"

#pragma package(smart_init)

// Raw data logger :
#define RwdTx( Buffer, Length)                             \
   if( Logger){                                            \
      Logger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                             \
   if( Logger){                                            \
      Logger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                        \
   if( Logger){                                            \
      Logger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                   \
   if( Logger){                                            \
      Logger->Report( Text"\n");                           \
   }

#define RX_TIMEOUT             3000           // total Rx timeout
#define RX_ICH_TIMEOUT         0              // Rx intercharacter timeout
#define RX_CALIBRATION_TIMEOUT 8000           // calibration timeout
#define COMMAND_SPACE          10

#define BD_DWORD_SIZE   6                     // dword characters
#define STOP_TRIALS     3                     // stop command trials

//******************************************************************************
// Constructor
//******************************************************************************

TBat2Device::TBat2Device()
// Constructor
{
   Chop       = FALSE;
   Sinc3      = FALSE;
   WithSignum = TRUE;
   Rate       = 10;

   ContinuousMode = FALSE;
   BridgeOnDelay  = 0;
   SamplesCount   = 10;
   Space          = 0;

   ZeroKg   = 0.0;
   FullKg   = 10.0;
   ZeroLsb  = 0;
   FullLsb  = 1000000;
   Duration = 10;
   Delay    = 5;

   Logger = 0;

   Uart = new TComUart;
   strcpy( ComName, "COM1");
   strcpy( FileName, "");
   strcpy( Comment, "No comment");
   f = 0;
} // TBat2Device

//******************************************************************************
// Destructor
//******************************************************************************

TBat2Device::~TBat2Device()
// Destructor
{
} // ~TBat2Device

//******************************************************************************
// Set COM
//******************************************************************************

void TBat2Device::SetCom( char *Name)
// Set communincation device name
{
   Disconnect();
   strncpyx( ComName, Name, BD_COM_NAME);
} // SetCom

//******************************************************************************
// Calibrate zero
//******************************************************************************

BOOL TBat2Device::CalibrateZero( dword *Value)
// Calibrate zero, returns <Value>
{
   if( !Command( CMD_CALIBRATE_ZERO)){
      return( FALSE);
   }
   Uart->SetRxWait( RX_CALIBRATION_TIMEOUT, RX_ICH_TIMEOUT);
   if( !ReceiveWord( Value)){
      Uart->SetRxWait( RX_TIMEOUT, RX_ICH_TIMEOUT);
      return( FALSE);
   }
   Uart->SetRxWait( RX_TIMEOUT, RX_ICH_TIMEOUT);
   return( TRUE);
} // Calibrate zero

//******************************************************************************
// Calibrate full scale
//******************************************************************************

BOOL TBat2Device::CalibrateFullScale( dword *Value)
// Calibrate full scale, returns <Value>
{
   if( !Command( CMD_CALIBRATE_FULL)){
      return( FALSE);
   }
   Uart->SetRxWait( RX_CALIBRATION_TIMEOUT, RX_ICH_TIMEOUT);
   if( !ReceiveWord( Value)){
      Uart->SetRxWait( RX_TIMEOUT, RX_ICH_TIMEOUT);
      return( FALSE);
   }
   Uart->SetRxWait( RX_TIMEOUT, RX_ICH_TIMEOUT);
   return( TRUE);
} // CalibrateFullScale

//******************************************************************************
// Set zero
//******************************************************************************

BOOL TBat2Device::SetZero( dword Value)
// Set zero calibration
{
   return( CommandWord( CMD_SET_ZERO, Value));
} // SetZero

//******************************************************************************
// Set full scale
//******************************************************************************

BOOL TBat2Device::SetFullScale( dword Value)
// Set full scale calibration
{
   return( CommandWord( CMD_SET_FULL, Value));
} // SetFullScale

//******************************************************************************
// Measure zero
//******************************************************************************

BOOL TBat2Device::MeasureZero()
// Measure zero level
{
   if( Duration <= 0){
      return( FALSE);
   }
   return( Measure( Duration, &ZeroLsb));
} // MeasureZero

//******************************************************************************
// Measure full scale
//******************************************************************************

BOOL TBat2Device::MeasureFullScale()
// Measure full scale level
{
   if( Duration <= 0){
      return( FALSE);
   }
   return( Measure( Duration, &FullLsb));
} // MeasureFullScale

//******************************************************************************
// Set file
//******************************************************************************

void TBat2Device::SetFile( char *Name)
// Set file <Name>
{
   strncpyx( FileName, Name, BD_PATH);
} // SetFile

//******************************************************************************
// Set parameters
//******************************************************************************

BOOL TBat2Device::SetParameters()
// Send parameters to device
{
   // set parameters
   if( !Command( CMD_SET_DEFAULTS)){
      return( FALSE);
   }
   int EffectiveRate = Rate;
   if( Chop){
      if( Sinc3){
         EffectiveRate *= 3;
      } else {
         EffectiveRate *= 4;
      }
   }
   if( EffectiveRate > 4800){
      EffectiveRate = 4800;
   }
   if( !CommandWord( CMD_SET_RATE, EffectiveRate)){
      return( FALSE);
   }
   if( Chop){
      if( !Command( CMD_SET_CHOP)){
         return( FALSE);
      }
   }
   if( Sinc3){
      if( !Command( CMD_SET_SINC3)){
         return( FALSE);
      }
   }
   if( !CommandByte( CMD_SAMPLES_COUNT, SamplesCount)){
      return( FALSE);
   }
   if( ContinuousMode){
      if( !CommandByte( CMD_INITIAL_DELAY, 0)){
         return( FALSE);
      }
      if( !CommandByte( CMD_SPACE_DELAY, 0)){
         return( FALSE);
      }
   } else {
      if( !CommandByte( CMD_INITIAL_DELAY, BridgeOnDelay)){
         return( FALSE);
      }
      if( !CommandByte( CMD_SPACE_DELAY, Space)){
         return( FALSE);
      }
   }
   return( TRUE);
} // SetParameters

//******************************************************************************
// Run
//******************************************************************************

BOOL TBat2Device::Run()
// Run & save conversions
{
   if( strlen( FileName) == 0){
      return( FALSE);
   }
   f = fopen( FileName, "w");
   if( !f){
      return( FALSE);
   }
   SaveHeader();                       // create file header
   // set parameters :
   if( !SetParameters()){
      return( FALSE);
   }
   // start conversion :
   StopReceive = FALSE;
   SamplesDone = 0;
   if( ContinuousMode){
      if( !Command( CMD_MEASURE_FAST)){
         return( FALSE);
      }
   } else {
      if( !Command( CMD_MEASURE)){
         return( FALSE);
      }
   }
   do {
      Application->ProcessMessages();  // windows message loop
      if( !ReceivePacket()){
         Stop();
         return( FALSE);
      }
      SavePacket();
      SamplesDone += SamplesCount;
   } while( !StopReceive);
   return( TRUE);
} // Run

//******************************************************************************
// Stop
//******************************************************************************

BOOL TBat2Device::Stop()
// Stop conversions & close file
{
   for( int i = 0; i < STOP_TRIALS; i++){
      if( !Command( CMD_STOP)){
         return( FALSE);
      }
   }
   if( f){
      fclose( f);
      f = 0;
   }
   StopReceive = TRUE;
   return( TRUE);
} // Stop

//------------------------------------------------------------------------------

//******************************************************************************
// Check connect
//******************************************************************************

BOOL TBat2Device::CheckConnect()
// Locate and open device
{
   if( Uart->IsOpen){
      return( TRUE);
   }
   TIdentifier Identifier;
   if( !Uart->Locate( ComName, Identifier)){
      Disconnect();
      return( FALSE);
   }
   if( !Uart->Open( Identifier)){
      Disconnect();
      return( FALSE);
   }
   // default parameters :
   TUart::TParameters Parameters;
//   Parameters.BaudRate     = 115200;
//   Parameters.BaudRate     = 57600;
   Parameters.BaudRate     = 38400;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   // common init :
   Uart->SetRxWait( RX_TIMEOUT, RX_ICH_TIMEOUT);
   Uart->Flush();
   return( TRUE);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TBat2Device::Disconnect()
// Close device
{
   Uart->Close();
} // Disconnect

//******************************************************************************
// Command
//******************************************************************************

BOOL TBat2Device::Command( byte Code)
// Send command <Code>
{
char Command[ 10];

   sprintf( Command, "%c", Code);
   return( SendCommand( Command));
} // Command

//******************************************************************************
// Command byte
//******************************************************************************

BOOL TBat2Device::CommandByte( byte Code, byte Value)
// Send command <Code> with byte <Value>
{
char Command[ 10];

   sprintf( Command, "%c%02X", Code, Value);
   return( SendCommand( Command));
} // CommandByte

//******************************************************************************
// Command word
//******************************************************************************

BOOL TBat2Device::CommandWord( byte Code, dword Value)
// Send command <Code> with wrod <Value>
{
char Command[ 10];

   sprintf( Command, "%c%06X", Code, Value);
   return( SendCommand( Command));
} // CommandWord

//******************************************************************************
// Send command
//******************************************************************************

BOOL TBat2Device::SendCommand( char *Command)
// Send command to device
{
   if( !CheckConnect()){
      return( FALSE);
   }
   int Size = strlen( Command);
   Uart->Flush();
   if( Uart->Write( Command, Size) != Size){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( FALSE);
   }
   Sleep( COMMAND_SPACE);
   RwdTx( Command, Size);
   return( TRUE);
} // SendCommand

//******************************************************************************
// Receive word
//******************************************************************************

BOOL TBat2Device::ReceiveWord( dword *Value)
// Read reply from device
{
   if( !CheckConnect()){
      return( FALSE);
   }
   int RetSize;
   RetSize = Uart->Read( Buffer, BD_DWORD_SIZE);
   if( !RetSize){
      RwdReport( "Rx timeout");
      return( FALSE);
   }
   if( RetSize != BD_DWORD_SIZE){
      RwdGarbage( Buffer, RetSize);
      return( FALSE);            // no terminator
   }
   Buffer[ BD_DWORD_SIZE] = '\0';
   if( sscanf( Buffer, "%06X", Value) != 1){
      RwdGarbage( Buffer, RetSize);
      return( FALSE);            // syntax error
   }
   SignumExtension( Value);
   RwdRx( Buffer, BD_DWORD_SIZE);
   return( TRUE);
} // ReceiveWord

//******************************************************************************
// Receive packet
//******************************************************************************

BOOL TBat2Device::ReceivePacket()
// Read reply packet from device
{
   if( !CheckConnect()){
      return( FALSE);
   }
   int PacketSize = AdcPacketSize( SamplesCount);
   int RetSize;
   RetSize = Uart->Read( Buffer, PacketSize);
   if( !RetSize){
      RwdReport( "Rx timeout");
      return( FALSE);
   }
   if( RetSize != PacketSize){
      RwdGarbage( Buffer, RetSize);
      return( FALSE);            // no terminator
   }
   if( Buffer[ 0] != ADC_PACKET_START){
      RwdGarbage( Buffer, RetSize);
      return( FALSE);            // wrong leader
   }
   if( Buffer[ PacketSize - 1] != ADC_PACKET_STOP){
      RwdGarbage( Buffer, RetSize);
      return( FALSE);            // wrong terminator
   }
   RwdRx( Buffer, PacketSize);
   return( TRUE);
} // ReceivePacket

//******************************************************************************
// Save packet
//******************************************************************************

void TBat2Device::SavePacket()
// Parse & save packet
{
byte *Src;
dword Value;

   if( !f){
      return;                           // invalid file
   }
   Src   = (byte *)&Buffer[ 1];         // packet contents start
   // scan Values array :
   for( int i = 0; i < SamplesCount; i++){
      Value = (dword)Src[ ADC_VALUE_SIZE * i]           |
              (dword)Src[ ADC_VALUE_SIZE * i + 1] << 8  |
              (dword)Src[ ADC_VALUE_SIZE * i + 2] << 16;
      SignumExtension( &Value);
      fprintf( f, "%d\n", (int)Value);
   }
} // SavePacket

//******************************************************************************
// Packet statistics
//******************************************************************************

BOOL TBat2Device::PacketStatistics( int64 *Sum)
// Parse & calculate statistics
{
byte *Src;
dword Value;
int   ActiveSamples;

   Src   = (byte *)&Buffer[ 1];         // packet contents start
   *Sum  = 0;
   ActiveSamples = 0;
   // scan Values array :
   for( int i = 0; i < SamplesCount; i++){
      Value = (dword)Src[ ADC_VALUE_SIZE * i]           |
              (dword)Src[ ADC_VALUE_SIZE * i + 1] << 8  |
              (dword)Src[ ADC_VALUE_SIZE * i + 2] << 16;
      SignumExtension( &Value);
      if( SamplesDone + i < Delay){
         continue;                     // skip sample
      }
      ActiveSamples++;                 // samples in sum
      *Sum += (int)Value;              // add to sum
   }
   if( !ActiveSamples){
      return( FALSE);                  // invalid average
   }
   return( TRUE);                      // average OK
} // PacketStatistics

//******************************************************************************
// Save header
//******************************************************************************

void TBat2Device::SaveHeader()
// Save file header
{
   fprintf( f, "%d,%d,%d\n", Rate, Chop, Sinc3);
   fprintf( f, "%d,%d,%d,%d\n", ContinuousMode, SamplesCount, BridgeOnDelay, Space);
   fprintf( f, "%d,%d\n", Duration, Delay);
   fprintf( f, "%lf,%d\n", ZeroKg, ZeroLsb);
   fprintf( f, "%lf,%d\n", FullKg, FullLsb);
   fprintf( f, "%s\n", Comment);
} // SaveHeader

//******************************************************************************
// Measure
//******************************************************************************

BOOL TBat2Device::Measure( int Duration, int *Value)
// Measure <Duration> * SamplesCount burst, returns <Value>
{
   if( Duration * SamplesCount <= Delay){
      return( FALSE);                  // too long delay
   }
   f = 0;                              // prevent file operations
   // set parameters :
   if( !SetParameters()){
      return( FALSE);
   }
   // start conversion :
   if( ContinuousMode){
      if( !Command( CMD_MEASURE_FAST)){
         return( FALSE);
      }
   } else {
      if( !Command( CMD_MEASURE)){
         return( FALSE);
      }
   }
   // measure :
   StopReceive = FALSE;
   int64 PacketSum;
   int64 Sum;
   Sum          = 0;
   SamplesDone  = 0;
   for( int i = 0; i < Duration; i++){
      Application->ProcessMessages();  // windows message loop
      if( StopReceive){
         return( FALSE);               // user break
      }
      if( !ReceivePacket()){
         Stop();
         return( FALSE);
      }
      if( PacketStatistics( &PacketSum)){
         Sum += PacketSum;               // valid packet sum
      }
      SamplesDone += SamplesCount;
   }
   *Value = (dword)(Sum / (Duration * SamplesCount - Delay));
   Stop();
   return( TRUE);
} // Measure

//******************************************************************************
// Signum extension
//******************************************************************************

void TBat2Device::SignumExtension( dword *Value)
// Do signum extension of <Value>
{
   if( !WithSignum){
      return;
   }
   int SValue = (int)*Value;
   SValue -= 0x00800000;               // zero shift
   *Value = (dword)SValue;
} // SignumExtension

