object MainForm: TMainForm
  Left = 400
  Top = 237
  Width = 769
  Height = 617
  Caption = 'BAT2 Recorder'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label10: TLabel
    Left = 8
    Top = 571
    Width = 22
    Height = 13
    Caption = 'File :'
  end
  object LblFileName: TLabel
    Left = 40
    Top = 571
    Width = 6
    Height = 13
    Caption = '?'
  end
  object MainMemo: TMemo
    Left = 8
    Top = 8
    Width = 393
    Height = 561
    Lines.Strings = (
      '')
    TabOrder = 0
  end
  object BtnConnect: TButton
    Left = 408
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 1
    OnClick = BtnConnectClick
  end
  object EdtCom: TEdit
    Left = 488
    Top = 9
    Width = 89
    Height = 21
    TabOrder = 2
    Text = 'COM1'
  end
  object Panel1: TPanel
    Left = 408
    Top = 40
    Width = 345
    Height = 257
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 84
      Top = 8
      Width = 92
      Height = 13
      Caption = 'ADC parameters'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 20
      Top = 72
      Width = 29
      Height = 13
      Caption = 'Rate :'
    end
    object Label5: TLabel
      Left = 140
      Top = 72
      Width = 13
      Height = 13
      Caption = 'Hz'
    end
    object Label2: TLabel
      Left = 12
      Top = 208
      Width = 81
      Height = 13
      Caption = 'Bridge On delay :'
    end
    object Label3: TLabel
      Left = 180
      Top = 208
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label6: TLabel
      Left = 12
      Top = 176
      Width = 76
      Height = 13
      Caption = 'Samples count :'
    end
    object Label7: TLabel
      Left = 180
      Top = 176
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label8: TLabel
      Left = 12
      Top = 232
      Width = 37
      Height = 13
      Caption = 'Space :'
    end
    object Label9: TLabel
      Left = 180
      Top = 232
      Width = 13
      Height = 13
      Caption = 'ms'
    end
    object EdtRate: TEdit
      Left = 64
      Top = 68
      Width = 65
      Height = 21
      TabOrder = 0
      Text = '50'
      OnChange = EdtRateChange
    end
    object RgMode: TRadioGroup
      Left = 16
      Top = 96
      Width = 185
      Height = 57
      Caption = 'Conversion Mode'
      ItemIndex = 1
      Items.Strings = (
        'Continuous'
        'Power save')
      TabOrder = 1
      OnClick = RgModeClick
    end
    object CbChop: TCheckBox
      Left = 24
      Top = 24
      Width = 57
      Height = 17
      Caption = 'Chop'
      TabOrder = 2
      OnClick = CbChopClick
    end
    object CbSinc3: TCheckBox
      Left = 24
      Top = 40
      Width = 57
      Height = 17
      Caption = 'Sinc3'
      TabOrder = 3
      OnClick = CbSinc3Click
    end
    object EdtBridgeOnDelay: TEdit
      Left = 104
      Top = 204
      Width = 65
      Height = 21
      TabOrder = 4
      Text = '0'
      OnChange = EdtBridgeOnDelayChange
    end
    object EdtSamplesCount: TEdit
      Left = 104
      Top = 172
      Width = 65
      Height = 21
      TabOrder = 5
      Text = '10'
      OnChange = EdtSamplesCountChange
    end
    object EdtSpace: TEdit
      Left = 104
      Top = 228
      Width = 65
      Height = 21
      TabOrder = 6
      Text = '0'
      OnChange = EdtSpaceChange
    end
  end
  object CbLogger: TCheckBox
    Left = 656
    Top = 16
    Width = 97
    Height = 17
    Caption = 'Enable logger'
    TabOrder = 4
    OnClick = CbLoggerClick
  end
  object Panel2: TPanel
    Left = 408
    Top = 302
    Width = 345
    Height = 87
    BevelOuter = bvLowered
    TabOrder = 5
    object LblCounter: TLabel
      Left = 91
      Top = 36
      Width = 6
      Height = 13
      Caption = '0'
    end
    object BtnSelectFile: TButton
      Left = 6
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Select File'
      TabOrder = 0
      OnClick = BtnSelectFileClick
    end
    object BtnRun: TButton
      Left = 6
      Top = 31
      Width = 75
      Height = 25
      Caption = 'Run'
      TabOrder = 1
      OnClick = BtnRunClick
    end
    object BtnStop: TButton
      Left = 6
      Top = 57
      Width = 75
      Height = 25
      Caption = 'Stop'
      Enabled = False
      TabOrder = 2
      OnClick = BtnStopClick
    end
  end
  object Panel3: TPanel
    Left = 408
    Top = 392
    Width = 345
    Height = 177
    BevelOuter = bvLowered
    TabOrder = 6
    object Label11: TLabel
      Left = 164
      Top = 13
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label12: TLabel
      Left = 311
      Top = 12
      Width = 20
      Height = 13
      Caption = 'LSB'
    end
    object Label13: TLabel
      Left = 164
      Top = 44
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label14: TLabel
      Left = 311
      Top = 44
      Width = 20
      Height = 13
      Caption = 'LSB'
    end
    object Label15: TLabel
      Left = 24
      Top = 79
      Width = 46
      Height = 13
      Caption = 'Duration :'
    end
    object Label16: TLabel
      Left = 173
      Top = 80
      Width = 83
      Height = 13
      Caption = '* (Samples count)'
    end
    object Label17: TLabel
      Left = 24
      Top = 145
      Width = 50
      Height = 13
      Caption = 'Comment :'
    end
    object Label18: TLabel
      Left = 24
      Top = 108
      Width = 33
      Height = 13
      Caption = 'Delay :'
    end
    object Label19: TLabel
      Left = 173
      Top = 109
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object BtnZero: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Zero'
      TabOrder = 0
      OnClick = BtnZeroClick
    end
    object EdtZeroKg: TEdit
      Left = 88
      Top = 10
      Width = 73
      Height = 21
      TabOrder = 1
      Text = '0'
      OnChange = EdtZeroKgChange
    end
    object EdtZeroLsb: TEdit
      Left = 184
      Top = 10
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '0'
    end
    object BtnFull: TButton
      Left = 8
      Top = 40
      Width = 75
      Height = 25
      Caption = 'Full'
      TabOrder = 3
      OnClick = BtnFullClick
    end
    object EdtFullKg: TEdit
      Left = 88
      Top = 41
      Width = 73
      Height = 21
      TabOrder = 4
      Text = '10'
      OnChange = EdtFullKgChange
    end
    object EdtFullLsb: TEdit
      Left = 184
      Top = 41
      Width = 121
      Height = 21
      TabOrder = 5
      Text = '1000000'
    end
    object EdtDuration: TEdit
      Left = 88
      Top = 76
      Width = 73
      Height = 21
      TabOrder = 6
      Text = '10'
      OnChange = EdtDurationChange
    end
    object EdtComment: TEdit
      Left = 88
      Top = 141
      Width = 249
      Height = 21
      TabOrder = 7
      Text = 'No comment'
      OnChange = EdtCommentChange
    end
    object EdtDelay: TEdit
      Left = 88
      Top = 105
      Width = 73
      Height = 21
      TabOrder = 8
      Text = '5'
      OnChange = EdtDelayChange
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'csv'
    FileName = 'NONAME'
    Filter = 'CSV samples|*.csv'
    Title = 'Select File'
    Left = 584
    Top = 8
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 616
    Top = 8
  end
end
