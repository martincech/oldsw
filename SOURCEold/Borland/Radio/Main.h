//******************************************************************************
//
//   Main.h        Packet diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Memory/ObjectMemory.h"
#include "../Crt/Crt.h"
#include "PktAdapter.h"

#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TComboBox *MainPort;
   TLabel *Label1;
   TButton *MainSetup;
   TMemo *MainMemo;
   TStatusBar *StatusBar1;
   TButton *MainSend;
   TLabel *Label3;
   TEdit *MainCommand;
   TLabel *Label4;
   TEdit *MainData;
        TButton *BtnSendLong;
   void __fastcall Create(TObject *Sender);
   void __fastcall MainSetupClick(TObject *Sender);
   void __fastcall Destroy(TObject *Sender);
   void __fastcall MainPortChange(TObject *Sender);
   void __fastcall MemoResize(TObject *Sender);
   void __fastcall MainSendClick(TObject *Sender);
   void __fastcall BtnSendLongClick(TObject *Sender);
private:	// User declarations
   TObjectMemory *Memory;
   TCrt          *Crt;
   TPktAdapter   *Adapter;

   void __fastcall Restart();
   // Open port
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------

#endif
