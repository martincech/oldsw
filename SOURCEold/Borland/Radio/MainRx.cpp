//******************************************************************************
//
//   MainRx.cpp   RX demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainRx.h"
#include "Com.h"
#include "ComPkt.h"
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt          = new TCrt( Memo);
   Logger       = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   ComInit( "COM1");
} // FormCreate

//******************************************************************************
// Timer
//******************************************************************************

#define DATA_SIZE 64

void __fastcall TMainForm::StatusTimerTick(TObject *Sender)
{
byte  RCommand;
dword RData;

   if( !ComRxPacket( &RCommand, &RData)){
      return;
   }
   ComFlushRxDump();
   Crt->printf( "Command : C %02X D %08X\n", RCommand, RData);
   switch( RCommand){
      case 1 :
         RCommand |= 0x80;
         RData = 0x11223344;
         break;
      case 0x0F :
         // datovy paket :
         StatusTimer->Enabled = false;
         ComTxBlockStart( DATA_SIZE);
         for( int i = 0; i < DATA_SIZE; i++){
            ComTxBlockByte( (byte)i);
         }
         ComTxBlockEnd();
         ComFlushTxDump();
         Crt->printf( "Reply : Data\n");
         StatusTimer->Enabled = true;
         return;
      default :
         RCommand = 0xFF;
         break;
   }
   ComTxPacket( RCommand, RData);
   Crt->printf( "Reply : C %02X D %08X\n", RCommand, RData);
   ComFlushTxDump();
} // StatusTimerTick

