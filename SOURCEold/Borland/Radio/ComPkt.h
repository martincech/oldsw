//*****************************************************************************
//
//   ComPkt.h    COM packet interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __ComPkt_H__
   #define __ComPkt_H__

#ifndef __Com_H__
   #include "Com.h"
#endif

#pragma pack( push, 1)                 // byte alignment
   #include "CPktDef.h"                // definice paketu
#pragma pack( pop)                     // original alignment

extern byte _com_data_crc;             // pro interni pouziti

//-----------------------------------------------------------------------------
// Kratky prikaz
//-----------------------------------------------------------------------------

TYesNo ComRxPacket( byte *cmd, dword *arg);
// Prijme paket, vrati typ prikazu <cmd> a parametr prikazu <arg>.
// Vrati NO, pokud paket neprisel

TYesNo ComRxData( void *data, word *size);
// Prijme datovy paket, vrati data a velikost
// Vrati NO, pokud paket neprisel

void ComTxPacket( byte cmd, dword arg);
// Vysle paket s kodem prikazu <cmd> a parametrem <arg>

//-----------------------------------------------------------------------------
// Datovy blok
//-----------------------------------------------------------------------------

void ComTxBlockStart( word size);
// Vysle zahlavi paketu pro data o velikosti <size>

//TYesNo ComTxBlockByte( byte b);
#define ComTxBlockByte( b)       ComTxChar( b);_com_data_crc += b
// Vysle byte <b> dat

void ComTxBlockEnd( void);
// Uzavre datovy blok

#endif
