//******************************************************************************
//
//   PktAdapter.cpp   Packet adapter
//   Version 0.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "PktAdapter.h"
#include <typeinfo.h>
#include <string.h>

#include "../Serial/Uart/ComUart.h"

// Configuration names :

#define NAME          "Adapter"
#define ADAPTER_NAME  "PktAdapter"
#define TYPE          "Type"
#define PORT          "Port"

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TPktAdapter::TPktAdapter()
// Constructor
{
//   FPort       = new TComUart;
   FPort       = 0;
   FLogger     = 0;

   FRxTimeout               = 3000;
   FRxIntercharacterTimeout = 2;
   FRxPacketTimeout         = 1500;
   FPortName                = "";
   // default parameters :
   FParameters.BaudRate     = 9600;
   FParameters.DataBits     = 8;
   FParameters.StopBits     = 10;
   FParameters.Parity       = TUart::NO_PARITY;
   FParameters.Handshake    = TUart::NO_HANDSHAKE;
} // TPktAdapter

//******************************************************************************
// Destructor
//******************************************************************************

TPktAdapter::~TPktAdapter()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TPktAdapter

//******************************************************************************
// Connect
//******************************************************************************

bool TPktAdapter::Connect( TName Port)
// Connect via <Port>
{
   // disconnect first :
   Disconnect();
   // remember name only :
   FPortName = Port;
   CheckConnect();        // try open, don't check success
   return( true);
} // Connect

//******************************************************************************
// Send
//******************************************************************************

bool TPktAdapter::Send( unsigned Command, unsigned Data)
// Send message
{
TComShortPacket Packet;

   if( !CheckConnect()){
      return( false);
   }
   Packet.Start    = CPKT_SHORT_START;
   Packet.Cmd      = (byte)Command;
   Packet.Data     = (dword)Data;
   Packet.Crc      = CalcCrc( &Packet.Cmd, 5);
   Packet.End      = CPKT_SHORT_END;
   FPort->Flush();
   if( FPort->Write( &Packet, sizeof( Packet)) != sizeof( Packet)){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   RwdTx( &Packet, sizeof( Packet));
   return( true);
} // Send

//******************************************************************************
// Send
//******************************************************************************

bool TPktAdapter::SendData( void *Data, int Length)
// Send data message
{
TComDataPacketStart *Start;
TComDataPacketEnd   *End;
byte *DstData;

   if( !CheckConnect()){
      return( false);
   }
   int Size = sizeof( TComDataPacketStart) + sizeof( TComDataPacketEnd) + Length;
   byte *Packet = new byte[ Size];
   Start   = (TComDataPacketStart *)Packet;
   DstData = (byte *)((TComDataPacketStart *)Packet + 1);
   End     = (TComDataPacketEnd *)&DstData[ Length];

   Start->Start  = CPKT_DATA_START;
   Start->Size1  = (word)Length;
   Start->Size2  = (word)Length;
   Start->Header = CPKT_DATA_START;

   memcpy( DstData, Data, Length);

   End->Crc      = CalcCrc( DstData, Length);
   End->End      = CPKT_SHORT_END;

   FPort->Flush();
   if( FPort->Write( Packet, Size) != Size){
      delete[] Packet;
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   RwdTx( Packet, Size);
   delete[] Packet;
   return( true);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TPktAdapter::Receive( unsigned &Command, unsigned &Data)
// Receive message
{
int Size, ReqSize;

   if( !CheckConnect()){
      return( false);
   }
   TComShortPacket *Packet = (TComShortPacket *)Buffer;
   FPort->SetRxWait( FRxTimeout, 0);     // wait for first char
   if( FPort->Read( Packet, 1) != 1){
      return( false);
   }
   if( Packet->Start != CPKT_SHORT_START &&
       Packet->Start != CPKT_DATA_START){
      RwdGarbage( &Packet, 1);
      return( false);
   }
   FPort->SetRxWait( FRxPacketTimeout, FRxIntercharacterTimeout);
   if( Packet->Start == CPKT_SHORT_START){
      // short packet
      ReqSize = sizeof( *Packet) - 1;
      Size = FPort->Read( &Packet->Cmd, ReqSize);
      if(  Size != ReqSize){
         RwdGarbage( Packet, Size + 1);
         return( false);
      }
      if( Packet->End != CPKT_SHORT_END){
         RwdGarbage( Packet, Size);
         return( false);
      }
      if( Packet->Crc != CalcCrc( &Packet->Cmd, 5)){
         RwdGarbage( Packet, Size);
         return( false);
      }
      Command  = Packet->Cmd;
      Data     = Packet->Data;
      RwdRx( Packet, sizeof( *Packet));
      return( true);
   }
   // long packet
   TComDataPacketStart *Start = (TComDataPacketStart *)Buffer;
   ReqSize = sizeof( *Start) - 1;         // first char done
   Size = FPort->Read( &Start->Size1, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, Size + 1);
      return( false);
   }
   if( Start->Header != CPKT_DATA_START){
      RwdGarbage( Start, Size);
      return( 0);
   }
   if( Start->Size1 != Start->Size2){
      RwdGarbage( Start, Size);
      return( false);
   }
   ReqSize = (int)Start->Size1;
   byte *DataBuffer = &Buffer[ sizeof(*Start)];
   Size =  FPort->Read( DataBuffer, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, Size + sizeof( *Start));
      return( false);
   }
   TComDataPacketEnd *End = (TComDataPacketEnd *)&Buffer[ sizeof( *Start) + Size];
   ReqSize = sizeof( *End);
   Size = FPort->Read( End, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( false);
   }
   if( End->End != CPKT_DATA_END){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( 0);
   }
   if( End->Crc != CalcCrc( DataBuffer, Start->Size1)){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( false);
   }
   Command = DATA_MESSAGE;
   Data    = 0;
   RwdRx( Start, sizeof( *Start) + Start->Size1 + sizeof( *End));
   return( true);
} // Receive

//******************************************************************************
// Get long message
//******************************************************************************

void TPktAdapter::GetData( unsigned &Length, void *Data)
// Get data of long message
{
   TComDataPacketStart *Start = (TComDataPacketStart *)Buffer;
   unsigned Size = Start->Size1;
   Length   = Size;
   if( Data){
      memcpy( Data, &Buffer[ sizeof(*Start)], Size);
   }
} // GetData

#ifdef __PERSISTENT__
//******************************************************************************
// Persistency
//******************************************************************************

bool TPktAdapter::Load( TObjectMemory *Memory)
// Load setup from <Memory>
{
   if( !Memory->Locate( NAME)){
      return( false);  // unable to find
   }
   TString Type = Memory->LoadString( TYPE);
   TString Port = Memory->LoadString( PORT);
   if( !strequ( Type.c_str(), ADAPTER_NAME)){
      return( false);
   }
   if( Port == ""){
      return( false);
   }
   if( !Connect( Port)){
      return( false);
   }
   if( FPort){
      FPort->Load( Memory);
      FPort->GetParameters( FParameters);
   }
   return( true);
} // Load

void TPktAdapter::Save( TObjectMemory *Memory)
// Save setup to <Memory>
{
   Memory->Create( NAME);
   Memory->SaveString( TYPE, ADAPTER_NAME);
   Memory->SaveString( PORT, FPortName);
   if( FPort){
      FPort->Save( Memory);
   }
} // Save
#endif

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Identifier
//******************************************************************************

TIdentifier TPktAdapter::GetIdentifier()
// GetIdentifier
{
   if( !FPort){
      return( INVALID_IDENTIFIER);
   }
   return( FPort->Identifier);
} // GetIdentifier

//******************************************************************************
// Property Name
//******************************************************************************

TName TPktAdapter::GetName()
// Get device name
{
   if( !FPort){
      return("No adapter");
   }
   return( FPort->Name);
} // GetName

//******************************************************************************
// Property IsOpen
//******************************************************************************

bool  TPktAdapter::GetIsOpen()
// Check if device is opened
{
   if( !FPort){
      return( false);
   }
   return( FPort->IsOpen);
} // GetIsOpen

//******************************************************************************
// Check Connection
//******************************************************************************

bool TPktAdapter::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( true);
   }
   if( FPort){
      return( false);            // disconnected
   }
   TIdentifier Identifier;
   TUart    *Uart;
   // COM setup :
   TComUart *Com = new TComUart;
   if( !Com->Locate( FPortName, Identifier)){
      delete Com;
      return( false);
   }
   Uart = Com;
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   Uart->SetParameters( FParameters);
   // common init :
   Uart->SetRxNowait();
   Uart->Flush();
   FPort = Uart;
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TPktAdapter::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

//******************************************************************************
// CRC
//******************************************************************************

byte TPktAdapter::CalcCrc( byte *Data, int Size)
// Calculate CRC
{
byte Crc = 0;
int  i;

   for( i = 0; i < Size; i++){
      Crc += *Data;
      Data++;
   }
   return( ~Crc);
} // CalcCrc

