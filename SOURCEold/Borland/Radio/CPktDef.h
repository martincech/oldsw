//*****************************************************************************
//
//    CPktDef.h - Simple COM Packet interface data structure
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __ComDef_H__
   #define __ComDef_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

// Naraznikove znaky :

#define CPKT_SHORT_START   0x99
#define CPKT_SHORT_END     0x88
#define CPKT_DATA_START    0x77
#define CPKT_DATA_END      0x66

// maximalni delka datoveho paketu :
#define CPKT_PACKET_SIZE( MAX_DATA)  ((MAX_DATA) + sizeof(TComDataPacketStart)+sizeof(TComDataPacketEnd))

// Struktura kratkeho paketu :

typedef struct {
   byte  Start;              // CPKT_SHORT_START
   byte  Cmd;                // kod typu dat
   dword Data;               // parametr, kodovani lo-endian
   byte  Crc;                // zabezpeceni (soucet vsech bytu cmd a arg)
   byte  End;                // CPKT_SHORT_END
} TComShortPacket;

// Struktura dlouheho paketu :

// Zahlavi
typedef struct {
   byte  Start;              // CPKT_DATA_START
   word  Size1;              // velikost dat (lo-endian)
   word  Size2;              // velikost dat (opakovane)
   byte  Header;             // CPKT_DATA_START
} TComDataPacketStart;
// Nasleduji data o velikosti size
// Zakonceni
typedef struct {
   byte  Crc;                // zabezpeceni (soucet vsech bytu dat)
   byte  End;                // CPKT_DATA_END
} TComDataPacketEnd;

#endif
