//******************************************************************************
//
//   Main.cpp      Packet diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainTx.h"
#include "Com.h"
#include "ComPkt.h"
#include <stdio.h>
#include <time.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define DATA_SIZE         64     // send long

#define PACKET_MAX_DATA  504     // dlouhy paket
#define DATA_REPEAT       66     // opakovani dlouheho paketu

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
   ComInit( "COM1");
} // Create

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Crt;
   delete Logger;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::MainSendClick(TObject *Sender)
{
   unsigned Command;
   unsigned Data;
   byte     RxCommand;
   dword    RxData;
   word     Size;
   byte     RData[ PACKET_MAX_DATA];
   sscanf( MainCommand->Text.c_str(), "%X", &Command);
   sscanf( MainData->Text.c_str(),    "%X", &Data);

   ComTxPacket( (byte)Command, (dword)Data);
   ComFlushTxDump();

   switch( Command){
      case 0x0F :
         // dlouhy paket
         if( !ComRxData( RData, &Size)){
            Crt->printf( "Data reply error\n");
            return;
         }
         ComFlushRxDump();
         Crt->printf( "Reply size %d (0x%02X)\n", Size, Size);
         Crt->printf( "Data reply OK\n");
         return;
      case 0xFF :
         // opakovany dlouhy paket
         for( int i = 0; i < DATA_REPEAT; i++){
            if( !ComRxData( RData, &Size)){
               ComFlushRxDump();
               Crt->printf( "Data reply error\n");
               return;
            }
            ComFlushRxDump();
         }
         Crt->printf( "Burst receive OK\n");
         return;

      default :
         // kratky paket
         if( !ComRxPacket( &RxCommand, &RxData)){
            Crt->printf( "Reply error\n");
            return;
         }
         ComFlushRxDump();
         Crt->printf( "Reply : C %02X D %08X\n", RxCommand, RxData);
         return;
   }
} // MainSendClick

//******************************************************************************
// Send Long
//******************************************************************************

void __fastcall TMainForm::BtnSendLongClick(TObject *Sender)
{
   byte Data[ DATA_SIZE];
   for( int i = 0; i < DATA_SIZE; i++){
      Data[ i] = i;
   }
   // send
   ComTxBlockStart( PACKET_MAX_DATA);
   for( int i = 0; i < PACKET_MAX_DATA; i++){
      ComTxBlockByte( (byte)i);
   }
   ComTxBlockEnd();
   Crt->printf( "Data send done\n");
} // BtnSendLongClick

