object MainForm: TMainForm
  Left = 289
  Top = 174
  AutoScroll = False
  Caption = 'Packet diagnostics'
  ClientHeight = 547
  ClientWidth = 748
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnDestroy = Destroy
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 534
    Top = 16
    Width = 25
    Height = 13
    Caption = 'Port :'
  end
  object Label3: TLabel
    Left = 3
    Top = 16
    Width = 53
    Height = 13
    Caption = 'Command :'
  end
  object Label4: TLabel
    Left = 115
    Top = 16
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object MainPort: TComboBox
    Left = 566
    Top = 13
    Width = 98
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    OnChange = MainPortChange
    Items.Strings = (
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10')
  end
  object MainSetup: TButton
    Left = 670
    Top = 12
    Width = 76
    Height = 21
    Caption = 'Setup'
    TabOrder = 4
    OnClick = MainSetupClick
  end
  object MainMemo: TMemo
    Left = 0
    Top = 80
    Width = 625
    Height = 448
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 5
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 528
    Width = 748
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object MainSend: TButton
    Left = 272
    Top = 14
    Width = 76
    Height = 21
    Caption = 'Send'
    TabOrder = 2
    OnClick = MainSendClick
  end
  object MainCommand: TEdit
    Left = 67
    Top = 14
    Width = 33
    Height = 21
    TabOrder = 0
    Text = '1'
  end
  object MainData: TEdit
    Left = 179
    Top = 14
    Width = 73
    Height = 21
    TabOrder = 1
    Text = '0'
  end
  object BtnSendLong: TButton
    Left = 272
    Top = 46
    Width = 76
    Height = 21
    Caption = 'Send Long'
    TabOrder = 7
    OnClick = BtnSendLongClick
  end
end
