//******************************************************************************
//
//   Main.cpp      Packet diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "../Serial/UartSetup.h"
#include "../Serial/CrtLogger.h"
#include "TmcCmd.h"
#include <stdio.h>
#include <time.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define PACKET_MAX_DATA 504

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TPktAdapter;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt             = new TCrt( MainMemo);
   Adapter->Logger = new TCrtLogger( Crt);
   Memory          = new TObjectMemory;
   if( Memory->IsEmpty){
      // first start, set defaults
      MainPort->ItemIndex = 0;
      MainPortChange( 0);      // uloz a restartuj
      return;
   }
   if( !Adapter->Load( Memory)){
      MainPort->ItemIndex = 0;
      MainPortChange( 0);      // uloz a restartuj
      return;
   }
   MainPort->ItemIndex = MainPort->Items->IndexOf( Adapter->Name);
} // Create

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TMainForm::Restart()
// Open port
{
   Crt->Clear();
   TString     PortName = MainPort->Text;
   if( !Adapter->Connect( PortName)){
      Crt->printf( "Unable open %s adapter\n", PortName.c_str());
      return;
   }
   Adapter->Save( Memory);
   Crt->printf( "Open %s\n", PortName.c_str());
} // Restart

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::MainSetupClick(TObject *Sender)
{
   TUartSetupDialog *Dialog = new TUartSetupDialog( this);
   TUart::TParameters Parameters;
   Parameters = *Adapter->Parameters;
   if( !Dialog->Execute( Parameters, MainPort->Text + " Setup")){
      return;
   }
   *Adapter->Parameters = Parameters;
   Restart();
} // MainSetupClick

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Crt;
   delete Memory;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainPortChange(TObject *Sender)
{
   Restart();
} // MainPortChange

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::MainSendClick(TObject *Sender)
{
   unsigned Command;
   unsigned Data;
   sscanf( MainCommand->Text.c_str(), "%X", &Command);
   sscanf( MainData->Text.c_str(),    "%X", &Data);
   if( !Adapter->Send( Command, Data)){
      Crt->printf( "Unable send\n");
      return;
   }
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   if( !Adapter->Receive( RCommand, RData)){
      Crt->printf( "Reply error\n");
      return;
   }
   switch( RCommand){
      case TPktAdapter::DATA_MESSAGE :
         Adapter->GetData( RLength, RBuffer);
         Crt->printf( "Data  : L %02X\n", RLength);
         break;
/*
      case CMD_ERROR :
         Crt->printf( "Error : C %02X D %08X\n", RCommand, RData);
         break;
*/
      default :
         Crt->printf( "Reply : C %02X D %08X\n", RCommand, RData);
         break;
   }
} // MainSendClick


//******************************************************************************
// Send Long
//******************************************************************************

#define DATA_SIZE 64

void __fastcall TMainForm::BtnSendLongClick(TObject *Sender)
{
   byte Data[ DATA_SIZE];
   for( int i = 0; i < DATA_SIZE; i++){
      Data[ i] = i;
   }
   if( !Adapter->SendData( Data, DATA_SIZE)){
      Crt->printf( "Unable send\n");
      return;
   }
   Crt->printf( "Send done\n");
} // BtnSendLongClick

