//******************************************************************************
//
//   MainRx.h     RX demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainRxH
#define MainRxH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "..\Serial\CrtLogger.h"
#include "..\Crt\Crt.h"

typedef enum {
   CONTEXT_UNCONNECTED,
   CONTEXT_CONNECTED,
   CONTEXT_RESET,
   CONTEXT_READY,
   CONTEXT_RING,
   CONTEXT_DATA,
} TMyContext;

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *Memo;
        TLabel *Status;
        TTimer *StatusTimer;
        TEdit *PortName;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall StatusTimerTick(TObject *Sender);
public:	// User declarations
   TCrt       *Crt;
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
