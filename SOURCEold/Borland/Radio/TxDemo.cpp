//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("TxDemo.res");
USEFORM("MainTx.cpp", MainForm);
USEUNIT("Com.cpp");
USEUNIT("ComPkt.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Packet Demo";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
