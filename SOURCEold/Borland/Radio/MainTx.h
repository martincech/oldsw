//******************************************************************************
//
//   Main.h        TX diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainTxH
#define MainTxH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Crt/Crt.h"
#include "../Serial/CrtLogger.h"

#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TLabel *Label1;
   TMemo *MainMemo;
   TStatusBar *StatusBar1;
   TButton *MainSend;
   TLabel *Label3;
   TEdit *MainCommand;
   TLabel *Label4;
   TEdit *MainData;
        TButton *BtnSendLong;
        TEdit *MainPort;
   void __fastcall Create(TObject *Sender);
   void __fastcall Destroy(TObject *Sender);
   void __fastcall MemoResize(TObject *Sender);
   void __fastcall MainSendClick(TObject *Sender);
   void __fastcall BtnSendLongClick(TObject *Sender);
public :	// User declarations
   TCrt          *Crt;
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------

#endif
