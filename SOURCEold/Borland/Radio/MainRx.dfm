object MainForm: TMainForm
  Left = 250
  Top = 169
  Width = 804
  Height = 627
  Caption = 'RX Demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Status: TLabel
    Left = 16
    Top = 576
    Width = 45
    Height = 13
    Caption = 'Status : ?'
  end
  object Memo: TMemo
    Left = 16
    Top = 112
    Width = 641
    Height = 457
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    TabOrder = 0
  end
  object PortName: TEdit
    Left = 680
    Top = 544
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 1
    Text = 'COM1'
  end
  object StatusTimer: TTimer
    Interval = 200
    OnTimer = StatusTimerTick
    Left = 712
    Top = 144
  end
end
