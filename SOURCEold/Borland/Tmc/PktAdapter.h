//******************************************************************************
//
//   PktAdapter.h     Packet adapter
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef PktAdapterH
   #define PktAdapterH

#ifndef UartH
   #include "../Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Serial/Logger.h"
#endif

#pragma pack( push, 1)                    // byte alignment
   #include "CPktDef.h"                   // protocol definition
#pragma pack( pop)                        // original alignment

//******************************************************************************
// TPktAdapter
//******************************************************************************

class TPktAdapter {
public :
   typedef enum {
      DATA_MESSAGE  = 0x8000,           // special command type - data message
   } TPacketType;

   TPktAdapter();
   // Constructor
   ~TPktAdapter();
   // Destructor
   bool Connect( TName Port);
   // Connect via <Port>
   bool Send( unsigned Command, unsigned Data);
   // Send message
   bool Receive( unsigned &Command, unsigned &Data);
   // Receive message
   void GetData( unsigned &Length, void *Data);
   // Get data of long message

#ifdef __PERSISTENT__
   bool Load( TObjectMemory *Memory);
   // Load setup from <Memory>
   void Save( TObjectMemory *Memory);
   // Save setup to <Memory>
#endif
   __property TIdentifier Identifier           = {read=GetIdentifier};
   __property TName       Name                 = {read=GetName};
   __property bool        IsOpen               = {read=GetIsOpen};

   __property TUart   *Port                    = {read=FPort};
   __property int      RxTimeout               = {read=FRxTimeout, write=FRxTimeout};
   __property int      RxIntercharacterTimeout = {read=FRxIntercharacterTimeout, write=FRxIntercharacterTimeout};
   __property int      RxPacketTimeout         = {read=FRxPacketTimeout, write=FRxPacketTimeout};

   __property TString  PortName     = {read=FPortName};
   __property TLogger *Logger       = {read=FLogger, write=FLogger};
   __property TUart::TParameters *Parameters   = {read=GetParameters};
//------------------------------------------------------------------------------

protected :
   TUart   *FPort;                     // connection port
   TString  FPortName;
   TLogger *FLogger;                   // raw data logger
   int      FRxTimeout;                // Rx timeout
   int      FRxIntercharacterTimeout;  // Rx intercharacter timeout
   int      FRxPacketTimeout;          // Rx packet timeout 
   byte     Buffer[ CPKT_PACKET_SIZE(1024)]; // packet buffer
   TUart::TParameters FParameters;     // Communication parameters

   TIdentifier GetIdentifier();
   // GetIdentifier
   TName GetName();
   // Get device name
   bool  GetIsOpen();
   // Check if device is opened
   void FlushRxChars();
   // Flush chars up to intercharacter timeout

   bool CheckConnect();
   // Check if adapter is ready
   void Disconnect();
   // Disconnect port
   byte CalcCrc( byte *Data, int Size);
   // Calculate CRC
   TUart::TParameters *GetParameters(){ return( &FParameters);}
};

#endif
