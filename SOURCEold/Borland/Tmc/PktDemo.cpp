//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("PktDemo.res");
USEUNIT("PktAdapter.cpp");
USEFORM("Main.cpp", MainForm);
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\Modem\Modem.cpp");
USEUNIT("..\Library\Memory\ObjectMemory.cpp");
USEFORM("..\Library\Serial\UartSetup.cpp", UartSetupDialog);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Packet Demo";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->CreateForm(__classid(TUartSetupDialog), &UartSetupDialog);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
