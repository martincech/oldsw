//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("TmcDemo.res");
USEFORM("MainTmc.cpp", MainForm);
USEUNIT("PktAdapter.cpp");
USEUNIT("Tmc.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\Modem\Modem.cpp");
USEUNIT("..\Library\Memory\ObjectMemory.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
