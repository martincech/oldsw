//******************************************************************************
//
//   MainTapi.cpp  TAPI diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainTapi.h"
#include "../Serial/UartSetup.h"
#include "../Serial/CrtLogger.h"
#include "TmcCmd.h"
#include <stdio.h>
#include <time.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define SetStatus( s) StatusBar->Panels->Items[ 0]->Text = s
#define SetRate( s)   StatusBar->Panels->Items[ 1]->Text = s
#define SetPort( s)   StatusBar->Panels->Items[ 2]->Text = s

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TPktAdapter;
   Modem   = new TModem;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt             = new TCrt( MainMemo);
   Adapter->Logger = new TCrtLogger( Crt);
   Memory          = new TObjectMemory;
   MainModem->Items->AddStrings( Modem->DeviceList);  // add modem list
   delete Modem;
   if( Memory->IsEmpty){
      // first start, set defaults
      MainModem->ItemIndex = 0;
      MainModemChange( 0);      // uloz a restartuj
      return;
   }
   if( !Adapter->Load( Memory)){
      MainModem->ItemIndex = 0;
      MainModemChange( 0);      // uloz a restartuj
      return;
   }
   MainModem->ItemIndex = MainModem->Items->IndexOf( Adapter->Name);
} // Create

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TMainForm::Restart()
// Open port
{
   Crt->Clear();
   TString PortName = MainModem->Text;
   if( !Adapter->Connect( PortName)){
      Crt->printf( "Unable open %s adapter\n", PortName.c_str());
      return;
   }
   Adapter->Save( Memory);
   Modem = dynamic_cast<TModem *>( Adapter->Port);
   Crt->printf( "Open %s\n", PortName.c_str());
   SetPort( PortName);
   SetStatus( "Idle");
} // Restart

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::MainSetupClick(TObject *Sender)
{
   Restart();
} // MainSetupClick

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Crt;
   delete Memory;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainModemChange(TObject *Sender)
{
   Restart();
} // MainPortChange

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::MainSendClick(TObject *Sender)
{
   unsigned Command;
   unsigned Data;
   sscanf( MainCommand->Text.c_str(), "%X", &Command);
   sscanf( MainData->Text.c_str(),    "%X", &Data);
   if( !Adapter->Send( Command, Data)){
      Crt->printf( "Unable send\n");
      return;
   }
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   if( !Adapter->Receive( RCommand, RData)){
      Crt->printf( "Reply error\n");
      return;
   }
   switch( RCommand){
      case TPktAdapter::DATA_MESSAGE :
         Adapter->GetData( RLength, RBuffer);
         Crt->printf( "Data  : L %02X\n", RLength);
         break;
/*
      case CMD_ERROR :
         Crt->printf( "Error : C %02X D %08X\n", RCommand, RData);
         break;
*/
      default :
         Crt->printf( "Reply : C %02X D %08X\n", RCommand, RData);
         break;
   }
} // MainSendClick

//******************************************************************************
// Vytaceni
//******************************************************************************

void __fastcall TMainForm::BtnDialClick(TObject *Sender)
{
char Buffer[ 120];

   if( EditNumber->Text.Length() == 0){
      Crt->printf( "Invalid phone number\n");
      return;
   }
   Crt->printf( "Dialing...\n");
   Modem = dynamic_cast<TModem *>( Adapter->Port);
   if( !Modem){
      Crt->printf( "No modem\n");
      return;
   }
   if( !Modem->Dial( EditNumber->Text)){
      Crt->printf( "Error connect %s\n", EditNumber->Text.c_str());
      return;
   }
   SetRate( (TString)Modem->ConnectionRate);
   SetStatus( "Connected");
   Crt->printf( "Connected\n");
} // BtnDialClick

//******************************************************************************
// Zaveseni
//******************************************************************************

void __fastcall TMainForm::BtnHangupClick(TObject *Sender)
{
   Modem = dynamic_cast<TModem *>( Adapter->Port);
   if( !Modem){
      Crt->printf( "No modem\n");
      return;
   }
   if( !Modem->HangUp()){
      Crt->printf( "Error : Hang Up\n");
      return;
   }
   Crt->printf( "Hang Up OK\n");
   SetStatus( "Idle");
}  // BtnHangupClick

//******************************************************************************
// Verze
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   if( !Adapter->Send( TMCCMD_VERSION, 0)){
      Crt->printf( "Unable send\n");
      return;
   }
   unsigned RCommand;
   unsigned RData;
   if( !Adapter->Receive( RCommand, RData)){
      Crt->printf( "Reply error\n");
      return;
   }
   if( RCommand != (unsigned)(TMCCMD_VERSION | TMCCMD_REPLY)){
      Crt->printf( "Reply CMD error\n");
      return;
   }
   Crt->printf( "Version %04X\n", RData);
} // Version

//******************************************************************************
// Cteni pameti
//******************************************************************************

#define EEP_SIZE        32768

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   dword    Address = 0;
   while( 1){
      if( !Adapter->Send( TMCCMD_READ, Address)){
         Crt->printf( "Unable send\n");
         return;
      }
      if( !Adapter->Receive( RCommand, RData)){
         Crt->printf( "Error : reply\n");
         return;
      }
      if( RCommand != TPktAdapter::DATA_MESSAGE){
         Crt->printf( "Error : short packet\n");
         return;
      }
      Adapter->GetData( RLength, RBuffer);
      if( RLength != PACKET_MAX_DATA){
         Crt->printf( "Error : packet size %d\n", RLength);
         return;
      }
      Address += PACKET_MAX_DATA;
      if( Address >= EEP_SIZE){
         Crt->printf( "Done : OK\n");
         return;
      }
   }
} // BtnReadClick

//******************************************************************************
// Read All
//******************************************************************************

void __fastcall TMainForm::BtnReadAllClick(TObject *Sender)
{
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   dword    Address = 0;
   if( !Adapter->Send( TMCCMD_READ_ALL, 0)){
      Crt->printf( "Unable send\n");
      return;
   }
   while( 1){
      if( !Adapter->Receive( RCommand, RData)){
         Crt->printf( "Error : reply\n");
         return;
      }
      if( RCommand != TPktAdapter::DATA_MESSAGE){
         Crt->printf( "Error : short packet\n");
         return;
      }
      Adapter->GetData( RLength, RBuffer);
      if( RLength != PACKET_MAX_DATA){
         Crt->printf( "Error : packet size %d\n", RLength);
         return;
      }
      Address += PACKET_MAX_DATA;
      if( Address >= EEP_SIZE){
         Crt->printf( "Done : OK\n");
         return;
      }
   }
} // BtnReadAllClick

