object MainForm: TMainForm
  Left = 287
  Top = 194
  AutoScroll = False
  Caption = 'TMC diagnostics'
  ClientHeight = 547
  ClientWidth = 748
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnDestroy = Destroy
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 353
    Top = 17
    Width = 41
    Height = 13
    Anchors = [akTop, akRight]
    Caption = 'Modem :'
  end
  object MainModem: TComboBox
    Left = 398
    Top = 13
    Width = 267
    Height = 21
    Style = csDropDownList
    Anchors = [akTop, akRight]
    ItemHeight = 13
    TabOrder = 0
    OnChange = MainModemChange
  end
  object MainSetup: TButton
    Left = 670
    Top = 12
    Width = 76
    Height = 21
    Anchors = [akTop, akRight]
    Caption = 'Setup'
    TabOrder = 1
    OnClick = MainSetupClick
  end
  object MainMemo: TMemo
    Left = 0
    Top = 80
    Width = 625
    Height = 448
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 2
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 528
    Width = 748
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object BtnDial: TButton
    Left = 646
    Top = 85
    Width = 82
    Height = 18
    Anchors = [akTop, akRight]
    Caption = 'Dial'
    TabOrder = 4
    OnClick = BtnDialClick
  end
  object EditNumber: TEdit
    Left = 640
    Top = 120
    Width = 89
    Height = 21
    Anchors = [akTop, akRight]
    TabOrder = 5
    Text = '776505366'
  end
  object BtnHangup: TButton
    Left = 649
    Top = 159
    Width = 79
    Height = 17
    Anchors = [akTop, akRight]
    Caption = 'Hangup'
    TabOrder = 6
    OnClick = BtnHangupClick
  end
  object BtnVersion: TButton
    Left = 648
    Top = 391
    Width = 90
    Height = 18
    Anchors = [akTop, akRight]
    Caption = 'Version'
    TabOrder = 7
    OnClick = BtnVersionClick
  end
  object BtnRead: TButton
    Left = 648
    Top = 419
    Width = 90
    Height = 18
    Anchors = [akTop, akRight]
    Caption = 'Read'
    TabOrder = 8
    OnClick = BtnReadClick
  end
  object BtnReadAll: TButton
    Left = 648
    Top = 445
    Width = 90
    Height = 18
    Anchors = [akTop, akRight]
    Caption = 'Read All'
    TabOrder = 9
    OnClick = BtnReadAllClick
  end
end
