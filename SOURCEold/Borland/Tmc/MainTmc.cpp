//******************************************************************************
//
//   MainTmc.cpp  TMC diagnostics
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainTmc.h"
#include "../Serial/CrtLogger.h"
#include <stdio.h>
#include <time.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define SetStatus( s) StatusBar->Panels->Items[ 0]->Text = s
#define SetRate( s)   StatusBar->Panels->Items[ 1]->Text = s
#define SetPort( s)   StatusBar->Panels->Items[ 2]->Text = s

byte EepBuffer[ EEP_SIZE];

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Tmc = new TTmc;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt = new TCrt( MainMemo);
   Tmc->Adapter->Logger = new TCrtLogger( Crt);
   if( !Tmc->DeviceList->Count){
      Application->MessageBox( "No modem devices found", "Error", MB_OK);
      Close();                                      // terminate application
   }
   MainModem->Items->AddStrings( Tmc->DeviceList);  // add modem list
   MainModem->ItemIndex = 1;
   MainModemChange( 0);                             // uloz a restartuj
} // Create

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TMainForm::Restart()
// Open port
{
   Crt->Clear();
   TString PortName = MainModem->Text;
   if( !Tmc->Connect( PortName)){
      Crt->printf( "Unable open '%s' adapter\n", PortName.c_str());
      return;
   }
   // nastaveni timeoutu :
   Tmc->Adapter->RxTimeout       = 3000;     // cekani na prvni znak odpovedi [ms]
   Tmc->Adapter->RxPacketTimeout = 1500;     // cekani na kompletaci paketu   [ms]

   Crt->printf( "Open %s\n", PortName.c_str());
   SetPort( PortName);
   SetStatus( "Idle");
} // Restart

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::MainSetupClick(TObject *Sender)
{
   Restart();
} // MainSetupClick

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Crt;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainModemChange(TObject *Sender)
{
   Restart();
} // MainPortChange

//******************************************************************************
// Vytaceni
//******************************************************************************

void __fastcall TMainForm::BtnDialClick(TObject *Sender)
{
char Buffer[ 120];

   if( EditNumber->Text.Length() == 0){
      Crt->printf( "Invalid phone number\n");
      return;
   }
   Screen->Cursor = crHourGlass;       // presypaci hodiny
   Crt->printf( "Dialing...\n");
   if( !Tmc->Dial( EditNumber->Text)){
      Screen->Cursor = crDefault;      // zrus hodiny
      Crt->printf( "Error connect %s\n", EditNumber->Text.c_str());
      return;
   }
   Screen->Cursor = crDefault;         // zrus hodiny
   SetStatus( "Connected");
   Crt->printf( "Connected\n");
} // BtnDialClick

//******************************************************************************
// Zaveseni
//******************************************************************************

void __fastcall TMainForm::BtnHangupClick(TObject *Sender)
{
   if( !Tmc->HangUp()){
      SetStatus( "Idle");
      Crt->printf( "Error : Hang Up\n");
      return;
   }
   SetStatus( "Idle");
   Crt->printf( "Hang Up OK\n");
}  // BtnHangupClick

//******************************************************************************
// Verze
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   int Version;
   if( !Tmc->GetVersion( Version)){
      Crt->printf( "Error : get version\n");
      return;
   }
   Crt->printf( "Version : %04X\n", Version);
} // Version

//******************************************************************************
// Cteni pameti
//******************************************************************************

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   Screen->Cursor = crHourGlass;       // presypaci hodiny
   dword Address = 0;
   if( !Tmc->ReadData( Address, EepBuffer, EEP_SIZE)){
      Screen->Cursor = crDefault;      // zrus hodiny
      Crt->printf( "Error : read\n");
      return;
   }
   Screen->Cursor = crDefault;         // zrus hodiny
   Crt->printf( "Done...\n");
} // BtnReadClick

//******************************************************************************
// Read All
//******************************************************************************

void __fastcall TMainForm::BtnReadAllClick(TObject *Sender)
{
   Screen->Cursor = crHourGlass;       // presypaci hodiny
   if( !Tmc->ReadAllData( EepBuffer)){
      Screen->Cursor = crDefault;      // zrus hodiny
      Crt->printf( "Error : read\n");
      return;
   }
   Screen->Cursor = crDefault;         // zrus hodiny
   Crt->printf( "Done...\n");
} // BtnReadAllClick

