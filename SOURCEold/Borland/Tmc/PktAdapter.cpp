//******************************************************************************
//
//   PktAdapter.cpp   Packet adapter
//   Version 0.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "PktAdapter.h"
#include <typeinfo.h>
#include <string.h>

#include "../Serial/Uart/ComUart.h"
#include "../Serial/Modem/Modem.h"


#define COM_PREFIX  "COM"         // COM device prefix

// Configuration names :

#define NAME          "Adapter"
#define ADAPTER_NAME  "PktAdapter"
#define TYPE          "Type"
#define PORT          "Port"

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TPktAdapter::TPktAdapter()
// Constructor
{
//   FPort       = new TComUart;
   FPort       = 0;
   FLogger     = 0;

   FRxTimeout               = 3000;
   FRxIntercharacterTimeout = 2;
   FRxPacketTimeout         = 1500;
   FPortName                = "";
   // default parameters :
   FParameters.BaudRate     = 9600;
   FParameters.DataBits     = 8;
   FParameters.StopBits     = 10;
   FParameters.Parity       = TUart::NO_PARITY;
   FParameters.Handshake    = TUart::NO_HANDSHAKE;
} // TPktAdapter

//******************************************************************************
// Destructor
//******************************************************************************

TPktAdapter::~TPktAdapter()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TPktAdapter

//******************************************************************************
// Connect
//******************************************************************************

bool TPktAdapter::Connect( TName Port)
// Connect via <Port>
{
   // disconnect first :
   Disconnect();
   // remember name only :
   FPortName = Port;
   CheckConnect();        // try open, don't check success
   return( true);
} // Connect

//******************************************************************************
// Send
//******************************************************************************

bool TPktAdapter::Send( unsigned Command, unsigned Data)
// Send message
{
TComShortPacket Packet;

   if( !CheckConnect()){
      return( false);
   }
   Packet.Start    = CPKT_SHORT_START;
   Packet.Cmd      = (byte)Command;
   Packet.Data     = (dword)Data;
   Packet.Crc      = CalcCrc( &Packet.Cmd, 5);
   Packet.End      = CPKT_SHORT_END;
   if( FPort->Write( &Packet, sizeof( Packet)) != sizeof( Packet)){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   RwdTx( &Packet, sizeof( Packet));
   return( true);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TPktAdapter::Receive( unsigned &Command, unsigned &Data)
// Receive message
{
int Size, ReqSize;

   if( !CheckConnect()){
      return( false);
   }
   TComShortPacket *Packet = (TComShortPacket *)Buffer;
   FPort->SetRxWait( FRxTimeout, 0);     // wait for first char
   if( FPort->Read( Packet, 1) != 1){
      return( false);
   }
   if( Packet->Start != CPKT_SHORT_START &&
       Packet->Start != CPKT_DATA_START){
      RwdGarbage( &Packet, 1);
      FlushRxChars();
      return( false);
   }
   FPort->SetRxWait( FRxPacketTimeout, FRxIntercharacterTimeout);
   if( Packet->Start == CPKT_SHORT_START){
      // short packet
      ReqSize = sizeof( *Packet) - 1;
      Size = FPort->Read( &Packet->Cmd, ReqSize);
      if(  Size != ReqSize){
         RwdGarbage( Packet, Size + 1);
         return( false);
      }
      if( Packet->End != CPKT_SHORT_END){
         RwdGarbage( Packet, Size);
         return( false);
      }
      if( Packet->Crc != CalcCrc( &Packet->Cmd, 5)){
         RwdGarbage( Packet, Size);
         return( false);
      }
      Command  = Packet->Cmd;
      Data     = Packet->Data;
      RwdRx( Packet, sizeof( *Packet));
      return( true);
   }
   // long packet
   TComDataPacketStart *Start = (TComDataPacketStart *)Buffer;
   ReqSize = sizeof( *Start) - 1;         // first char done
   Size = FPort->Read( &Start->Size1, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, Size + 1);
      return( false);
   }
   if( Start->Header != CPKT_DATA_START){
      RwdGarbage( Start, Size);
      return( 0);
   }
   if( Start->Size1 != Start->Size2){
      RwdGarbage( Start, Size);
      return( false);
   }
   ReqSize = (int)Start->Size1;
   byte *DataBuffer = &Buffer[ sizeof(*Start)];
   Size =  FPort->Read( DataBuffer, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, Size + sizeof( *Start));
      return( false);
   }
   TComDataPacketEnd *End = (TComDataPacketEnd *)&Buffer[ sizeof( *Start) + Size];
   ReqSize = sizeof( *End);
   Size = FPort->Read( End, ReqSize);
   if( Size != ReqSize){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( false);
   }
   if( End->End != CPKT_DATA_END){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( 0);
   }
   if( End->Crc != CalcCrc( DataBuffer, Start->Size1)){
      RwdGarbage( Start, sizeof( *Start) + Start->Size1 + Size);
      return( false);
   }
   Command = DATA_MESSAGE;
   Data    = 0;
   RwdRx( Start, sizeof( *Start) + Start->Size1 + sizeof( *End));
   return( true);
} // Receive

//******************************************************************************
// Get long message
//******************************************************************************

void TPktAdapter::GetData( unsigned &Length, void *Data)
// Get data of long message
{
   TComDataPacketStart *Start = (TComDataPacketStart *)Buffer;
   unsigned Size = Start->Size1;
   Length   = Size;
   if( Data){
      memcpy( Data, &Buffer[ sizeof(*Start)], Size);
   }
} // GetData

#ifdef __PERSISTENT__
//******************************************************************************
// Persistency
//******************************************************************************

bool TPktAdapter::Load( TObjectMemory *Memory)
// Load setup from <Memory>
{
   if( !Memory->Locate( NAME)){
      return( false);  // unable to find
   }
   TString Type = Memory->LoadString( TYPE);
   TString Port = Memory->LoadString( PORT);
   if( !strequ( Type.c_str(), ADAPTER_NAME)){
      return( false);
   }
   if( Port == ""){
      return( false);
   }
   if( !Connect( Port)){
      return( false);
   }
   if( FPort){
      FPort->Load( Memory);
      FPort->GetParameters( FParameters);
   }
   return( true);
} // Load

void TPktAdapter::Save( TObjectMemory *Memory)
// Save setup to <Memory>
{
   Memory->Create( NAME);
   Memory->SaveString( TYPE, ADAPTER_NAME);
   Memory->SaveString( PORT, FPortName);
   if( FPort){
      FPort->Save( Memory);
   }
} // Save
#endif

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Identifier
//******************************************************************************

TIdentifier TPktAdapter::GetIdentifier()
// GetIdentifier
{
   if( !FPort){
      return( INVALID_IDENTIFIER);
   }
   return( FPort->Identifier);
} // GetIdentifier

//******************************************************************************
// Property Name
//******************************************************************************

TName TPktAdapter::GetName()
// Get device name
{
   if( !FPort){
      return("No adapter");
   }
   return( FPort->Name);
} // GetName

//******************************************************************************
// Property IsOpen
//******************************************************************************

bool  TPktAdapter::GetIsOpen()
// Check if device is opened
{
   if( !FPort){
      return( false);
   }
   return( FPort->IsOpen);
} // GetIsOpen

//******************************************************************************
// Flush
//******************************************************************************

#define FLUSH_DATA_SIZE 1024

void TPktAdapter::FlushRxChars()
// Flush chars up to intercharacter timeout
{
byte Data[ FLUSH_DATA_SIZE];

   FPort->SetRxWait( FRxIntercharacterTimeout, FRxIntercharacterTimeout);
   for( int i = 0; i < FLUSH_DATA_SIZE; i++){
      if( FPort->Read( &Data[ i], 1) == 0){
         // timeout
         if( i > 0){
            RwdGarbage( Data, i);
         }
         return;
      }
   }
   RwdGarbage( Data, FLUSH_DATA_SIZE);
} // FlushRxChars

//******************************************************************************
// Check Connection
//******************************************************************************

bool TPktAdapter::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( true);
   }
   if( FPort){
      return( false);            // disconnected
   }
   TIdentifier Identifier;
   TUart    *Uart;
   if( strnequ( FPortName.c_str(), COM_PREFIX, strlen( COM_PREFIX))){
      // COM setup :
      TComUart *Com = new TComUart;
      if( !Com->Locate( FPortName, Identifier)){
         delete Com;
         return( false);
      }
      Uart = Com;
   } else {
      // Modem setup
      TModem *Modem = new TModem;
      if( !Modem->Locate( FPortName, Identifier)){
         delete Modem;
         return( false);
      }
      Modem->MinRate = FParameters.BaudRate;
      Modem->MaxRate = FParameters.BaudRate;
      Uart = Modem;
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   Uart->SetParameters( FParameters);
   // common init :
   Uart->SetRxNowait();
   Uart->Flush();
   FPort = Uart;
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TPktAdapter::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

//******************************************************************************
// CRC
//******************************************************************************

byte TPktAdapter::CalcCrc( byte *Data, int Size)
// Calculate CRC
{
byte Crc = 0;
int  i;

   for( i = 0; i < Size; i++){
      Crc += *Data;
      Data++;
   }
   return( ~Crc);
} // CalcCrc

