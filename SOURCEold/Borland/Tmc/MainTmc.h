//******************************************************************************
//
//   MainTmc.h    TMC diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainTmcH
#define MainTmcH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Crt/Crt.h"
#include "Tmc.h"

#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TComboBox *MainModem;
   TLabel *Label1;
   TButton *MainSetup;
   TMemo *MainMemo;
   TStatusBar *StatusBar;
   TButton *BtnDial;
   TEdit *EditNumber;
   TButton *BtnHangup;
   TButton *BtnVersion;
   TButton *BtnRead;
   TButton *BtnReadAll;
   void __fastcall Create(TObject *Sender);
   void __fastcall MainSetupClick(TObject *Sender);
   void __fastcall Destroy(TObject *Sender);
   void __fastcall MainModemChange(TObject *Sender);
   void __fastcall MemoResize(TObject *Sender);
   void __fastcall BtnDialClick(TObject *Sender);
   void __fastcall BtnHangupClick(TObject *Sender);
   void __fastcall BtnVersionClick(TObject *Sender);
   void __fastcall BtnReadClick(TObject *Sender);
   void __fastcall BtnReadAllClick(TObject *Sender);
private:	// User declarations
   TCrt          *Crt;
   TTmc          *Tmc;

   void __fastcall Restart();
   // Open port
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------

#endif
