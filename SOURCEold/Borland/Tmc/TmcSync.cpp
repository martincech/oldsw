//******************************************************************************
//
//   Tmc.cpp       TMC project communication
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Tmc.h"
#include "../Serial/Modem/Modem.h"
#include "TmcCmd.h"

#pragma package(smart_init)

#define TRIALS   5              // number of trials

//******************************************************************************
// Constructor
//******************************************************************************

TTmc::TTmc()
// Constructor
{
   FAdapter    = new TPktAdapter;
   FPortName   = "";
   FDeviceList = 0;
} // TTmc

//******************************************************************************
// Destructor
//******************************************************************************

TTmc::~TTmc()
// Destructor
{
   if( FAdapter){
      delete FAdapter;
   }
} // ~TTmc

//******************************************************************************
// Connect
//******************************************************************************

bool TTmc::Connect( TString Port)
// Connect via Port/device
{
   if( !FAdapter->Connect( Port)){
      return( false);
   }
   FPortName = Port;
   return( true);
} // Connect

//******************************************************************************
// Dial
//******************************************************************************

bool TTmc::Dial( TString Number)
// Dial destination modem
{
   TModem *Modem = dynamic_cast<TModem *>( FAdapter->Port);
   if( !Modem){
      return( false);
   }
   if( !Modem->Dial( Number)){
      return( false);
   }
   return( true);
} // Dial

//******************************************************************************
// Hang up
//******************************************************************************

bool TTmc::HangUp()
// Hang up call
{
   TModem *Modem = dynamic_cast<TModem *>( FAdapter->Port);
   if( !Modem){
      return( false);
   }
   if( !Modem->HangUp()){
      return( false);
   }
   return( true);
} // HangUp

//******************************************************************************
// Get Version
//******************************************************************************

bool TTmc::GetVersion( int &Version)
// Read version
{
   if( !FAdapter->Port || !FAdapter->Port->IsOpen){
      return( false);
   }
   unsigned RCommand, RData;
   for( int i = 0; i < TRIALS; i++){
      if( !FAdapter->Send( TMCCMD_VERSION, 0)){
         return( false);
      }
      if( !FAdapter->Receive( RCommand, RData)){
         continue;
      }
      if( RCommand != (unsigned)(TMCCMD_VERSION | TMCCMD_REPLY)){
         continue;
      }
      Version = RData;
      return( true);
   }
   return( false);
} // GetVersion

//******************************************************************************
// Read data
//******************************************************************************

bool TTmc::ReadData( dword Address, void *Data, int Size)
// Read EEPROM data
{
   if( !FAdapter->Port || !FAdapter->Port->IsOpen){
      return( false);
   }
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   int      DstIndex  = 0;             // destination data index
   bool     PacketOk;
   byte    *BData = (byte *)Data;      // byte typecast
   do {
      PacketOk = false;
      for( int i = 0; i < TRIALS; i++){
         Application->ProcessMessages();
         if( !FAdapter->Send( TMCCMD_READ, Address)){
            return( false);            // send err
         }
         if( !FAdapter->Receive( RCommand, RData)){
            continue;                  // receive timeout
         }
         if( RCommand != TPktAdapter::DATA_MESSAGE){
            continue;                  // wrong packet
         }
         FAdapter->GetData( RLength, RBuffer);
         if( RLength != PACKET_MAX_DATA){
            continue;                  // wrong packet size
         }
         PacketOk = true;              // done
         break;
      }
      if( !PacketOk){
         return( false);
      }
      if( Size < PACKET_MAX_DATA){
         memcpy( &BData[ DstIndex], RBuffer, Size);
      } else {
         memcpy( &BData[ DstIndex], RBuffer, PACKET_MAX_DATA);
      }
      Size     -= PACKET_MAX_DATA;
      Address  += PACKET_MAX_DATA;
      DstIndex += PACKET_MAX_DATA;
   } while( Size > 0);
   return( true);
} // ReadData

//******************************************************************************
// Read all data
//******************************************************************************

bool TTmc::ReadAllData( void *Data)
// Read all EEPROM
{
   if( !FAdapter->Port || !FAdapter->Port->IsOpen){
      return( false);
   }
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   dword    Address = 0;
   if( !FAdapter->Send( TMCCMD_READ_ALL, 0)){
      return( false);
   }
   while( 1){
      Application->ProcessMessages();
      if( !FAdapter->Receive( RCommand, RData)){
         return( false);
      }
      if( RCommand != TPktAdapter::DATA_MESSAGE){
         return( false);
      }
      FAdapter->GetData( RLength, RBuffer);
      if( RLength != PACKET_MAX_DATA){
         return( false);
      }
      Address += PACKET_MAX_DATA;
      if( Address >= EEP_SIZE){
         return( true);
      }
   }
} // ReadAllData

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Device List
//******************************************************************************

TStringList *TTmc::GetDeviceList()
// Get list of modems
{
   if( FDeviceList){
      return( FDeviceList);
   }
   // get device list from modem
   TModem *Modem = dynamic_cast<TModem *>( FAdapter->Port);
   if( !Modem){
      // create dummy modem
      Modem = new TModem;
      FDeviceList = Modem->DeviceList;
      delete Modem;
   } else {
      FDeviceList = Modem->DeviceList;
   }
   return( FDeviceList);
} // GetDeviceList

