//******************************************************************************
//
//   Main.cpp      Packet diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "../Serial/UartSetup.h"
#include "../Serial/CrtLogger.h"
#include "TmcCmd.h"
#include <stdio.h>
#include <time.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define PACKET_MAX_DATA 504

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Adapter = new TPktAdapter;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt             = new TCrt( MainMemo);
   Adapter->Logger = new TCrtLogger( Crt);
   Memory          = new TObjectMemory;
   if( Memory->IsEmpty){
      // first start, set defaults
      MainPort->ItemIndex = 0;
      MainPortChange( 0);      // uloz a restartuj
      return;
   }
   if( !Adapter->Load( Memory)){
      MainPort->ItemIndex = 0;
      MainPortChange( 0);      // uloz a restartuj
      return;
   }
   MainPort->ItemIndex = MainPort->Items->IndexOf( Adapter->Name);
} // Create

//******************************************************************************
// Restart
//******************************************************************************

void __fastcall TMainForm::Restart()
// Open port
{
   Crt->Clear();
   TString     PortName = MainPort->Text;
   if( !Adapter->Connect( PortName)){
      Crt->printf( "Unable open %s adapter\n", PortName.c_str());
      return;
   }
   Adapter->Save( Memory);
   Crt->printf( "Open %s\n", PortName.c_str());
} // Restart

//******************************************************************************
// Setup
//******************************************************************************

void __fastcall TMainForm::MainSetupClick(TObject *Sender)
{
   TUartSetupDialog *Dialog = new TUartSetupDialog( this);
   TUart::TParameters Parameters;
   Parameters = *Adapter->Parameters;
   if( !Dialog->Execute( Parameters, MainPort->Text + " Setup")){
      return;
   }
   *Adapter->Parameters = Parameters;
   Restart();
} // MainSetupClick

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Crt;
   delete Memory;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainPortChange(TObject *Sender)
{
   Restart();
} // MainPortChange

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::MainSendClick(TObject *Sender)
{
   unsigned Command;
   unsigned Data;
   sscanf( MainCommand->Text.c_str(), "%X", &Command);
   sscanf( MainData->Text.c_str(),    "%X", &Data);
   if( !Adapter->Send( Command, Data)){
      Crt->printf( "Unable send\n");
      return;
   }
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   if( !Adapter->Receive( RCommand, RData)){
      Crt->printf( "Reply error\n");
      return;
   }
   switch( RCommand){
      case TPktAdapter::DATA_MESSAGE :
         Adapter->GetData( RLength, RBuffer);
         Crt->printf( "Data  : L %02X\n", RLength);
         break;
/*
      case CMD_ERROR :
         Crt->printf( "Error : C %02X D %08X\n", RCommand, RData);
         break;
*/
      default :
         Crt->printf( "Reply : C %02X D %08X\n", RCommand, RData);
         break;
   }
} // MainSendClick

//******************************************************************************
// Vytaceni
//******************************************************************************

void __fastcall TMainForm::BtnDialClick(TObject *Sender)
{
char Buffer[ 120];

   if( EditNumber->Text.Length() == 0){
      Crt->printf( "Invalid phone number\n");
      return;
   }
   Crt->printf( "Dialing...\n");
   sprintf( Buffer, "ATD%s\r", EditNumber->Text.c_str());
   Adapter->Port->Flush();
   Adapter->Port->Write( Buffer, strlen( Buffer));
   Adapter->Port->SetRxNowait();     // wait for first char
   time_t Now;
   int    Size;
   Now  = time( 0);
   Now += 60;
   do {
      if( Adapter->Port->Read( Buffer, 1)){
         Adapter->Port->SetRxWait(1000, 0);
         Size = Adapter->Port->Read( &Buffer[ 1], 10);
         if( Size){
            Buffer[ Size] = 0;
            Crt->printf( "%s\n", Buffer);
         }
         break;
      }
   } while( time( 0) < Now);
   Crt->printf( "Done...\n");
} // BtnDialClick

//******************************************************************************
// Zaveseni
//******************************************************************************

void __fastcall TMainForm::BtnHangupClick(TObject *Sender)
{
char Buffer[ 10];

   Adapter->Port->SetRxWait( 300, 0);
   Sleep( 1000);

   Adapter->Port->Write( "+++", 3);            // escape
   Crt->printf( "+++\n");
   Sleep( 1000);

   int Size = Adapter->Port->Read( Buffer, 6);
   if( !Size){
      Crt->printf( "Error : Enter ESC mode\n");
      return;
   }
   Buffer[ Size] = 0;
   Crt->printf( "%s", Buffer);

   Sleep( 500);
   Adapter->Port->Flush();
   Adapter->Port->Write( "ATH\r", 4);          // zaves
   Crt->printf( "ATH\n");

   Size = Adapter->Port->Read( Buffer, 6);
   if( !Size){
      Crt->printf( "Error : Hangup\n");
      return;
   }
   Buffer[ Size] = 0;
   Crt->printf( "%s", Buffer);

   Sleep( 4000);
   Adapter->Port->Flush();
   // kontrola odezvy :
   Adapter->Port->Write( "AT\r", 3);
   Crt->printf( "AT\n");
   Size = Adapter->Port->Read( Buffer, 6);
   if( !Size){
      Crt->printf( "Error : no response\n");
      return;
   }
   Buffer[ Size] = 0;
   Crt->printf( "Hangup : %s", Buffer);
}  // BtnHangupClick

//******************************************************************************
// Verze
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   if( !Adapter->Send( TMCCMD_VERSION, 0)){
      Crt->printf( "Unable send\n");
      return;
   }
   unsigned RCommand;
   unsigned RData;
   if( !Adapter->Receive( RCommand, RData)){
      Crt->printf( "Reply error\n");
      return;
   }
   if( RCommand != (unsigned)(TMCCMD_VERSION | TMCCMD_REPLY)){
      Crt->printf( "Reply CMD error\n");
      return;
   }
   Crt->printf( "Version %04X\n", RData);
} // Version

//******************************************************************************
// Cteni pameti
//******************************************************************************

#define EEP_SIZE        32768

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   dword    Address = 0;
   while( 1){
      if( !Adapter->Send( TMCCMD_READ, Address)){
         Crt->printf( "Unable send\n");
         return;
      }
      if( !Adapter->Receive( RCommand, RData)){
         Crt->printf( "Error : reply\n");
         return;
      }
      if( RCommand != TPktAdapter::DATA_MESSAGE){
         Crt->printf( "Error : short packet\n");
         return;
      }
      Adapter->GetData( RLength, RBuffer);
      if( RLength != PACKET_MAX_DATA){
         Crt->printf( "Error : packet size %d\n", RLength);
         return;
      }
      Address += PACKET_MAX_DATA;
      if( Address >= EEP_SIZE){
         Crt->printf( "Done : OK\n");
         return;
      }
   }
} // BtnReadClick

//******************************************************************************
// Read All
//******************************************************************************

void __fastcall TMainForm::BtnReadAllClick(TObject *Sender)
{
   unsigned RCommand;
   unsigned RData;
   unsigned RLength;
   byte     RBuffer[ PACKET_MAX_DATA];
   dword    Address = 0;
   if( !Adapter->Send( TMCCMD_READ_ALL, 0)){
      Crt->printf( "Unable send\n");
      return;
   }
   while( 1){
      if( !Adapter->Receive( RCommand, RData)){
         Crt->printf( "Error : reply\n");
         return;
      }
      if( RCommand != TPktAdapter::DATA_MESSAGE){
         Crt->printf( "Error : short packet\n");
         return;
      }
      Adapter->GetData( RLength, RBuffer);
      if( RLength != PACKET_MAX_DATA){
         Crt->printf( "Error : packet size %d\n", RLength);
         return;
      }
      Address += PACKET_MAX_DATA;
      if( Address >= EEP_SIZE){
         Crt->printf( "Done : OK\n");
         return;
      }
   }
} // BtnReadAllClick

