//******************************************************************************
//
//   Tmc.h         TMC project communication
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef TmcH
   #define TmcH

#ifndef PktAdapterH
   #include "PktAdapter.h"
#endif

#define EEP_SIZE        32768        // EEPROM size
#define PACKET_MAX_DATA   504        // packet data size

//******************************************************************************
// TTmc
//******************************************************************************

class TTmc {
public :
   TTmc();
   // Constructor
   ~TTmc();
   // Destructor
   bool Connect( TString Port);
   // Connect via Port/device
   bool Dial( TString Number);
   // Dial destination modem
   bool HangUp();
   // Hang up call
   bool GetVersion( int &Version);
   // Read version
   bool ReadData( dword Address, void *Data, int Size);
   // Read EEPROM data
   bool ReadAllData( void *Data);
   // Read all EEPROM

   __property TStringList *DeviceList = {read=GetDeviceList}; // Modem list
   __property TPktAdapter *Adapter    = {read=FAdapter};
//------------------------------------------------------------------------------
protected :
   TPktAdapter *FAdapter;
   TString      FPortName;
   TStringList *FDeviceList;

   TStringList *GetDeviceList();
   // Get list of modems
}; // TTmc

//------------------------------------------------------------------------------
#endif
