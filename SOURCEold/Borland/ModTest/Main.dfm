object MainForm: TMainForm
  Left = 120
  Top = 473
  BorderStyle = bsSingle
  Caption = 'Module tester'
  ClientHeight = 158
  ClientWidth = 253
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object StatusLabel: TLabel
    Left = 112
    Top = 110
    Width = 68
    Height = 13
    Caption = 'StatusLabel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
  end
  object Label1: TLabel
    Left = 24
    Top = 51
    Width = 38
    Height = 13
    Caption = 'Repeat:'
  end
  object RepeatLabel: TLabel
    Left = 136
    Top = 51
    Width = 6
    Height = 13
    Caption = '0'
  end
  object Button1: TButton
    Left = 16
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Test'
    TabOrder = 0
    OnClick = Button1Click
  end
  object TmcRadioButton: TRadioButton
    Left = 24
    Top = 16
    Width = 57
    Height = 17
    Caption = 'TMC'
    Checked = True
    TabOrder = 1
    TabStop = True
  end
  object Mvz3RadioButton: TRadioButton
    Left = 96
    Top = 16
    Width = 57
    Height = 17
    Caption = 'Mvz 3'
    TabOrder = 2
  end
  object Bat2RadioButton: TRadioButton
    Left = 172
    Top = 16
    Width = 57
    Height = 17
    Caption = 'Bat 2'
    TabOrder = 3
  end
  object RepeatEdit: TEdit
    Left = 72
    Top = 48
    Width = 49
    Height = 21
    TabOrder = 4
    Text = '1'
  end
end
