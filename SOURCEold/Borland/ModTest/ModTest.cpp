//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("ModTest.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("..\FTDI2\UsbSpi.cpp");
USEUNIT("..\FTDI2\Eeprom.cpp");
USEUNIT("..\FTDI2\AtFlash.cpp");
USEUNIT("..\FTDI2\At45dbxx.cpp");
USELIB("..\FTDI2\FTD2XX.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
