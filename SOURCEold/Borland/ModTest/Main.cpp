//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include <stdlib.h>
#include <stdio.h>
#include <time.h>



#include "Main.h"
#include "Eeprom.h"
#include "AtFlash.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;




// M95256 :
#define TMC_EEPROM_SIZE       32768
#define TMC_EEPROM_PAGE_SIZE     64

// M95640 :
#define MVZ3_EEPROM_SIZE        8192
#define MVZ3_EEPROM_PAGE_SIZE     32


//---------------------------------------------------------------------------
// Zobrazeni zprav
//---------------------------------------------------------------------------

void TMainForm::MessageClear() {
  StatusLabel->Visible = false;
  Refresh();
  Application->ProcessMessages();
}

void TMainForm::MessageNote(String Message) {
  StatusLabel->Caption = Message;
  StatusLabel->Font->Color = clBlack;
  StatusLabel->Font->Style = TFontStyles();
  StatusLabel->Visible = true;
  Refresh();
  Application->ProcessMessages();
}

void TMainForm::MessageError(String Message) {
  StatusLabel->Caption = Message;
  StatusLabel->Font->Color = clRed;
  StatusLabel->Font->Style = TFontStyles() << fsBold;
  StatusLabel->Visible = true;
  Refresh();
  Application->ProcessMessages();
}

void TMainForm::MessageOk(String Message) {
  StatusLabel->Caption = Message;
  StatusLabel->Font->Color = clGreen;
  StatusLabel->Font->Style = TFontStyles() << fsBold;
  StatusLabel->Visible = true;
  Refresh();
  Application->ProcessMessages();
}

//---------------------------------------------------------------------------
// Test Eeprom
//---------------------------------------------------------------------------

void TMainForm::CheckEeprom(int TotalSize, int PageSize, int Repeat) {
  unsigned char *Array     = new byte[TotalSize];
  unsigned char *ReadArray = new byte[TotalSize];
  TEeprom *Eeprom = new TEeprom(PageSize);

  MessageClear();
  Refresh();

  try {
    Screen->Cursor = crHourGlass;
    for (int i = 0; i < Repeat; i++) {
      RepeatLabel->Caption = String(i + 1);
      Refresh();
      // Vytvorim data
      randomize();
      for (int i = 0; i < TotalSize; i++) {
        Array[i] = rand() % 0xFF;
      }
      if (!Eeprom->Connected()) {
        MessageError("Not connected");
        throw 1;
      }
      MessageNote("Write...");
      if (!Eeprom->Write(0, Array, TotalSize)) {
        MessageError("Write error");
        throw 1;
      }
      MessageNote("Read...");
      if (!Eeprom->Read(0, ReadArray, TotalSize)) {
        MessageError("Read error");
        throw 1;
      }
      MessageNote("Verify...");
      for (int i = 0; i < TotalSize; i++) {
        if (Array[i] != ReadArray[i]) {
          MessageError("Verification error: " + String(i));
          throw 1;
        }
      }
    }//for
    MessageOk("OK");
  } catch(...) {}
  delete Eeprom;
  delete[] Array;
  delete[] ReadArray;
  Screen->Cursor = crDefault;
}

//---------------------------------------------------------------------------
// Test flash
//---------------------------------------------------------------------------

void TMainForm::CheckFlash(int TotalSize, int Repeat) {
  unsigned char *Array     = new byte[TotalSize];
  unsigned char *ReadArray = new byte[TotalSize];
  TAtFlash *Flash = new TAtFlash();

  MessageClear();
  Refresh();

  try {
    Screen->Cursor = crHourGlass;
    for (int i = 0; i < Repeat; i++) {
      RepeatLabel->Caption = String(i + 1);
      Refresh();
      // Vytvorim data
      randomize();
      for (int i = 0; i < TotalSize; i++) {
        Array[i] = rand() % 0xFF;
      }
      if (!Flash->Connected()) {
        MessageError("Not connected");
        throw 1;
      }
      MessageNote("Write...");
      if (!Flash->Write(0, Array, TotalSize)) {
        MessageError("Write error");
        throw 1;
      }
      if (!Flash->Flush()) {
        MessageError("Flush error");
        throw 1;
      }
      MessageNote("Read...");
      if (!Flash->Read(0, ReadArray, TotalSize)) {
        MessageError("Read error");
        throw 1;
      }
      MessageNote("Verify...");
      for (int i = 0; i < TotalSize; i++) {
        if (Array[i] != ReadArray[i]) {
          MessageError("Verification error: " + String(i));
          throw 1;
        }
      }
    }//for
    MessageOk("OK");
  } catch(...) {}
  delete Flash;
  delete[] Array;
  delete[] ReadArray;
  Screen->Cursor = crDefault;
}

//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::Button1Click(TObject *Sender)
{
  int Repeat;
  try {
    Repeat = RepeatEdit->Text.ToInt();
    if (Repeat <= 0) {
      return;
    }
  } catch(...) {
    return;
  }

  if (TmcRadioButton->Checked) {
    CheckEeprom(TMC_EEPROM_SIZE, TMC_EEPROM_PAGE_SIZE, Repeat);
  } else if (Mvz3RadioButton->Checked) {
    CheckEeprom(MVZ3_EEPROM_SIZE, MVZ3_EEPROM_PAGE_SIZE, Repeat);
  } else if (Bat2RadioButton->Checked) {
    CheckFlash(FLASH_SIZE, Repeat);
  }
}
//---------------------------------------------------------------------------
