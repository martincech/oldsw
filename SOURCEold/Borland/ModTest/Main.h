//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TLabel *StatusLabel;
        TRadioButton *TmcRadioButton;
        TRadioButton *Mvz3RadioButton;
        TRadioButton *Bat2RadioButton;
        TLabel *Label1;
        TEdit *RepeatEdit;
        TLabel *RepeatLabel;
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);

        void MessageClear();
        void MessageNote(String Message);
        void MessageError(String Message);
        void MessageOk(String Message);

        void CheckEeprom(int TotalSize, int PageSize, int Repeat);
        void CheckFlash(int TotalSize, int Repeat);

};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
