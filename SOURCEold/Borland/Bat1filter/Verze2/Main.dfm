object MainForm: TMainForm
  Left = 200
  Top = 111
  Width = 931
  Height = 803
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 640
    Top = 72
    Width = 60
    Height = 13
    Caption = 'Stable range'
  end
  object Graph: TDBChart
    Left = 0
    Top = 153
    Width = 923
    Height = 623
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.DateTimeFormat = 'h:mm'
    Legend.Visible = False
    View3D = False
    Align = alClient
    TabOrder = 0
    object WeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HMOTNOST'
    end
    object LowPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'LO_PASS'
    end
    object HighPassSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 16744703
      ShowInLegend = False
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HI_PASS'
    end
    object StableSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clLime
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object StableWeightSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clWhite
      Dark3D = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 923
    Height = 153
    Align = alTop
    TabOrder = 1
    object Label3: TLabel
      Left = 640
      Top = 16
      Width = 48
      Height = 13
      Caption = 'Averaging'
    end
    object Label1: TLabel
      Left = 848
      Top = 16
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label2: TLabel
      Left = 80
      Top = 8
      Width = 34
      Height = 13
      Caption = 'Display'
    end
    object Label4: TLabel
      Left = 24
      Top = 32
      Width = 28
      Height = 13
      Caption = 'Width'
    end
    object Label5: TLabel
      Left = 24
      Top = 56
      Width = 28
      Height = 13
      Caption = 'Offset'
    end
    object Label8: TLabel
      Left = 640
      Top = 72
      Width = 60
      Height = 13
      Caption = 'Stable range'
    end
    object Label9: TLabel
      Left = 848
      Top = 64
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label10: TLabel
      Left = 640
      Top = 96
      Width = 30
      Height = 13
      Caption = 'Stable'
    end
    object Label11: TLabel
      Left = 848
      Top = 88
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label12: TLabel
      Left = 216
      Top = 32
      Width = 29
      Height = 13
      Caption = 'Data :'
    end
    object LblFileName: TLabel
      Left = 264
      Top = 32
      Width = 41
      Height = 13
      Caption = 'demo.db'
    end
    object Label6: TLabel
      Left = 640
      Top = 48
      Width = 41
      Height = 13
      Caption = 'Treshold'
    end
    object Label16: TLabel
      Left = 848
      Top = 48
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label15: TLabel
      Left = 24
      Top = 104
      Width = 22
      Height = 13
      Caption = 'Tara'
    end
    object EdtAveraging: TEdit
      Left = 720
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '20'
    end
    object BtnRedraw: TButton
      Left = 432
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Redraw'
      TabOrder = 1
      OnClick = BtnRedrawClick
    end
    object EdtWidth: TEdit
      Left = 64
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '2000'
    end
    object EdtOffset: TEdit
      Left = 64
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 3
      Text = '0'
    end
    object BtnBackward: TButton
      Left = 432
      Top = 56
      Width = 75
      Height = 25
      Caption = '<<<'
      TabOrder = 4
      OnClick = BtnBackwardClick
    end
    object BtnForward: TButton
      Left = 512
      Top = 56
      Width = 75
      Height = 25
      Caption = '>>>'
      TabOrder = 5
      OnClick = BtnForwardClick
    end
    object BtnFirst: TButton
      Left = 352
      Top = 56
      Width = 75
      Height = 25
      Caption = '|<<<'
      TabOrder = 6
      OnClick = BtnFirstClick
    end
    object EdtStableRange: TEdit
      Left = 720
      Top = 64
      Width = 121
      Height = 21
      TabOrder = 7
      Text = '0,01'
    end
    object EdtStable: TEdit
      Left = 720
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 8
      Text = '10'
    end
    object BtnFile: TButton
      Left = 224
      Top = 56
      Width = 75
      Height = 25
      Caption = 'File'
      TabOrder = 9
      OnClick = BtnFileClick
    end
    object EdtTreshold: TEdit
      Left = 720
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 10
      Text = '0,1'
    end
    object EdtTara: TEdit
      Left = 64
      Top = 98
      Width = 121
      Height = 21
      TabOrder = 11
      Text = '0'
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'db'
    FileName = 'demo'
    Filter = 'Database|*.db'
    Left = 416
    Top = 32
  end
end
