//*****************************************************************************
//
//    WeightDef.h  -  Weight definitions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __WeightDef_H__
   #define __WeightDef_H__

#define WEIGHT_MIN           (TWeight)0x80000000
#define WEIGHT_MAX           (TWeight)0x7FFFFFFF

typedef int32 TWeight;       // Physical representation of the weight
typedef int32 TRawWeight;    // ADC representation of the weight
typedef int32 TSumWeight;    // weight averaging sum
typedef int64 TMulWeight;    // weight multiplication

#endif
