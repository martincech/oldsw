//******************************************************************************
//
//   Main.cpp      Bat2 filter demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "Data.h"
#include "Filter.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define TOTAL_SAMPLES 2000

#define LAST_STABLE 1        // last stable hold

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Normal
//******************************************************************************

#define STEP_UP_VALUE    100
#define STEP_DOWN_VALUE -100
#define STABLE_VALUE     120

void __fastcall TMainForm::BtnRedrawClick(TObject *Sender)
{
   FilterStop();
   TRawWeight Tara;
   try {
     Tara = (TRawWeight)(EdtTara->Text.ToDouble() * 1000);
   } catch( ...){
     Application->MessageBox( "Invalid Tara format", "Error", MB_OK);
     return;
   }

   // read filter data :
   FilterRecord.AveragingWindow = EdtAveraging->Text.ToInt();
   if( FilterRecord.AveragingWindow > FILTER_MAX_AVERAGING){
      // value out of limit - set limit
      FilterRecord.AveragingWindow = FILTER_MAX_AVERAGING;
      EdtAveraging->Text = AnsiString( FilterRecord.AveragingWindow);
   }
   if( FilterRecord.AveragingWindow < 1){
      // value out of limit - set limit
      FilterRecord.AveragingWindow = 1;
      EdtAveraging->Text = AnsiString( FilterRecord.AveragingWindow);
   }

   TRawWeight Treshold;
   try {
     Treshold = (TRawWeight)(EdtTreshold->Text.ToDouble() * 1000);
   } catch( ...){
     Application->MessageBox( "Invalid Treshold format", "Error", MB_OK);
     return;
   }
   FilterRecord.TresholdWeight = Treshold;

   TRawWeight StableRange;
   try {
     StableRange = (word)(EdtStableRange->Text.ToDouble() * 10);
   } catch( ...){
     Application->MessageBox( "Invalid Stable Range format", "Error", MB_OK);
     return;
   }
   FilterRecord.StableRange = StableRange;

   TRawWeight MaxWeight;
   try {
     MaxWeight = (TRawWeight)(EdtMaxWeight->Text.ToDouble() * 1000);
   } catch( ...){
     Application->MessageBox( "Invalid Max.Weight format", "Error", MB_OK);
     return;
   }
   FilterRecord.MaxWeight = MaxWeight;

   FilterRecord.StableWindow = EdtStable->Text.ToInt();
   if( FilterRecord.StableWindow < 1){
      FilterRecord.StableWindow = 1;
      EdtStable->Text = AnsiString( FilterRecord.StableWindow);
   }
   // Clear old graph :
   WeightSeries->Clear();
   LowPassSeries->Clear();
   HighPassSeries->Clear();
   StableSeries->Clear();
   StableWeightSeries->Clear();
   StableRangeUpSeries->Clear();
   StableRangeDownSeries->Clear();

   // Read display window :
   int WindowWidth = EdtWidth->Text.ToInt();
   int WindowOffset = EdtOffset->Text.ToInt();
   // scroll at offset :
   BatData->Begin();
   for( int i = 0; i < WindowOffset; i++){
      if( !BatData->Next()){
         return;      // end of database
      }
   }
   FilterRecord.ZeroWeight = (TRawWeight)(BatData->Weight * 1000 - Tara); // first = initial value
   // initialize filter :
   FilterStart();
   // draw graph :
   int StartIndex = WindowOffset;
   TRawWeight StableWeight = 0;
   while(1){
      FilterNextSample( (TRawWeight)(BatData->Weight * 1000 - Tara));    // process filter
      WeightSeries->AddXY( StartIndex,
                           FilterRecord.RawWeight,
                            "", clTeeColor);
      LowPassSeries->AddXY( StartIndex,
                           FilterRecord.LowPass,
                            "", clTeeColor);
      HighPassSeries->AddXY( StartIndex,
                           FilterRecord.HighPass,
                            "", clTeeColor);
      StableSeries->AddXY( StartIndex,
                           FilterRecord.Ready ? STABLE_VALUE : 0,
                            "", clTeeColor);
      StableWeightSeries->AddXY( StartIndex,
                           StableWeight,
                            "", clTeeColor);
      StableRangeUpSeries->AddXY( StartIndex,
                           FilterRecord.LowPass + FilterRecord.StableWeight,
                            "", clTeeColor);
      StableRangeDownSeries->AddXY( StartIndex,
                           FilterRecord.LowPass - FilterRecord.StableWeight,
                            "", clTeeColor);
      FilterRead( &StableWeight);    // remember value
      StartIndex++;
      if( StartIndex >= (WindowWidth + WindowOffset)){
         return;
      }
      if( !BatData->Next()){
         return;
      }
   }
} // BtnRedrawClick

//******************************************************************************
// Backward
//******************************************************************************

void __fastcall TMainForm::BtnBackwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset -= WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnBackwardClick

//******************************************************************************
// Forward
//******************************************************************************

void __fastcall TMainForm::BtnForwardClick(TObject *Sender)
{
   int WindowOffset = EdtOffset->Text.ToInt();
   int WindowWidth  = EdtWidth->Text.ToInt();
   WindowOffset += WindowWidth;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnForwardClick

//******************************************************************************
// First
//******************************************************************************

void __fastcall TMainForm::BtnFirstClick(TObject *Sender)
{
   int WindowOffset = 0;
   EdtOffset->Text = AnsiString( WindowOffset);
   BtnRedrawClick( Sender);
} // BtnFirstClick

//******************************************************************************
// File
//******************************************************************************

void __fastcall TMainForm::BtnFileClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   LblFileName->Caption = FileOpenDialog->FileName;
   BatData->SetFile( FileOpenDialog->FileName);
} // BtnFileClick

