//******************************************************************************
//
//   Filter.c     Filtering
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
   #include "Filter.h"
#endif
#ifdef __GNUC__
   // GNU ARM only
   #include "../Inc/Filter.h"
#endif

TFilterRecord        FilterRecord;
static TSamplesCount StableCounter;

static TRawWeight Fifo[ FILTER_MAX_AVERAGING];
static byte       FifoPointer;
static TSumWeight FifoSum;

// Local functions :

void FifoInitialize( TRawWeight Fill);
// Initialize FIFO data

TRawWeight FifoAverage( void);
// Calculate average of the contents

void FifoPut( TRawWeight Weight);
// Put to FIFO

//******************************************************************************
// Start
//******************************************************************************

void FilterStart( void)
// Initialize & start filtering
{
   FifoInitialize( FilterRecord.ZeroWeight);
   FilterRecord.Status = FILTER_WAIT_EMPTY;  // wait for discharge
   FilterRecord.Ready  = NO;
   StableCounter = 0;
} // FilterStart

//******************************************************************************
// Stop
//******************************************************************************

void FilterStop( void)
// Stop filtering
{
   FilterRecord.Status = FILTER_STOP;  // clear ready also
   FilterRecord.Ready  = NO;
} // FilterStop

//******************************************************************************
// Sample
//******************************************************************************

void FilterNextSample( TRawWeight Sample)
// Process next sample
{
TRawWeight TmpHighPass;

   FilterRecord.RawWeight = Sample;              // read sample
   FifoPut( Sample);                             // save sample
   // filtering :
   FilterRecord.LowPass   = FifoAverage();
   FilterRecord.HighPass  = FilterRecord.RawWeight - FilterRecord.LowPass;
   // check for empty weight :
   if( FilterRecord.LowPass < FilterRecord.EmptyWeight){
      FilterRecord.Status = FILTER_WAIT_STEP;    // ready for step
      return;
   }
   // check for hysteresis :
   if( FilterRecord.LowPass < FilterRecord.TriggerWeight){
      StableCounter = 0;                         // start stability window
      return;                                    // dead zone
   }
   // FilterRecord.LowPass > Trigger : trigger/step active
   if( FilterRecord.Status == FILTER_WAIT_EMPTY){
      return;                                    // wait for discharge
   }
   if( FilterRecord.Status == FILTER_WAIT_STEP){
      // rising edge of WAIT_STABLE
      StableCounter = 0;                         // start stability window
      FilterRecord.Status = FILTER_WAIT_STABLE;
      FilterRecord.Ready  = NO;
   }
   // check for stability range :
   TmpHighPass = FilterRecord.HighPass;
   if( TmpHighPass < 0){
      TmpHighPass = -TmpHighPass;   // absolute value
   }
   if( TmpHighPass < FilterRecord.StableRange){
      if( StableCounter < FilterRecord.StableWindow){
         StableCounter++;
      } // else saturation
   } else {
      StableCounter = 0;
   }
   // check for stability duration :
   if( StableCounter < FilterRecord.StableWindow){
      return;                          // still waiting
   }
   // stable value
   FilterRecord.Ready  = YES;
   FilterRecord.Weight = FilterRecord.LowPass;      // set on averaged value
} // FilterNextSample

//******************************************************************************
// Read
//******************************************************************************

TYesNo FilterRead( TRawWeight *Weight)
// Read filtered value
{
   if( !FilterRecord.Ready){
      return( NO);
   }
   FilterRecord.Status = FILTER_WAIT_EMPTY;      // read done, wait for discharge
   FilterRecord.Ready  = NO;                     // handshake
   *Weight = FilterRecord.Weight;
   return( YES);
} // FilterRead

//******************************************************************************
// Fifo initialize
//******************************************************************************

void FifoInitialize( TRawWeight Fill)
// Initialize FIFO data
{
TSamplesCount i;

   FifoSum = 0;
   for( i = 0; i < FilterRecord.AveragingWindow; i++){
      Fifo[ i] = Fill;
      FifoSum += Fill;
   }
   FifoPointer = 0;
} // FifoInitialize

//******************************************************************************
// Fifo average
//******************************************************************************

TRawWeight FifoAverage( void)
// Calculate average of the contents
{
   return( FifoSum / FilterRecord.AveragingWindow);
} // FifoAverage

//******************************************************************************
// Fifo put
//******************************************************************************

void FifoPut( TRawWeight Weight)
// Put to FIFO
{
   FifoSum -= Fifo[ FifoPointer];           // remove old value
   Fifo[ FifoPointer++] = Weight;           // save new value
   FifoSum += Weight;                       // add new value
   if( FifoPointer >= FilterRecord.AveragingWindow){
      FifoPointer = 0;
   }
} // FifoPut

