//******************************************************************************
//
//   Data.cpp     Databaze
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Data.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TBatData *BatData;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TBatData::TBatData(TComponent* Owner)
        : TDataModule(Owner)
{
} // TKurData

//******************************************************************************
// Begin
//******************************************************************************

void TBatData::Begin()
// pred prvni zaznam
{
   DataTable->First();
} // Begin

//******************************************************************************
// Next
//******************************************************************************

bool TBatData::Next()
// na dalsi zaznam
{
   if( DataTable->Eof){
      return( false);
   }
   DataTable->Next();
   return( true);
} // Next

//******************************************************************************
// End of table
//******************************************************************************

bool TBatData::Eof()
// konec databaze
{
   return( DataTable->Eof);
} // Eof

//******************************************************************************
// Hmotnost
//******************************************************************************

double TBatData::GetWeight()
{
   return( DataTable->FieldValues[ "WEIGHT"]);
} // GetWeight

//******************************************************************************
// Set file
//******************************************************************************

void TBatData::SetFile( AnsiString Filename)
// Set new database file
{
   DataTable->Active = false;
   DataTable->TableName = Filename;
   DataTable->Active = true;
} // SetFile
