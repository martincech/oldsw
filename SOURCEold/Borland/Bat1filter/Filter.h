//******************************************************************************
//
//   Filter.h     Filtering
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Filter_H__
#define __Filter_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"    // project directory
#endif

// Filter status :
#ifdef FILTER_RELATIVE
   typedef enum {
      FILTER_STOP,              // weighing disabled
      FILTER_WAIT,              // wait for step
      FILTER_STABLE,            // weighing done
      _FILTER_LAST
   } TFilterStatus;
#else
   typedef enum {
      FILTER_STOP,              // weighing disabled
      FILTER_START,             // wait for first sample
      FILTER_SETUP,             // wait for filter setup
      FILTER_WAIT_STEP,         // wait for step
      FILTER_WAIT_STABLE,       // wait for stable value
      FILTER_WAIT_EMPTY,        // wait for discharge
      _FILTER_COUNT
   } TFilterStatus;
#endif

// Data record :
typedef struct {
   byte          Status;           //  filter status
   byte          Ready;            //  weighing done
   TSamplesCount AveragingWindow;  //* moving average window
   TSamplesCount StableWindow;     //* stability window
   TRawWeight    LowPass;          //  low pass filter value
   TRawWeight    HighPass;         //  high pass filter value
   TRawWeight    RawWeight;        //  filter input
   TRawWeight    ZeroWeight;       //* zero weight - fifo initials
   word          StableRange;      //* relative stabilisation range [0.1%]
   TRawWeight    MaxWeight;        //* overload value
#ifdef FILTER_RELATIVE
   TRawWeight    LastStableWeight; //  last stabilised value
#else
   TRawWeight    TresholdWeight;   //* treshold weight - charge/discharge resolution
   TRawWeight    StableWeight;     //  absolute stabilisation range [kg]
#endif
   TRawWeight    Weight;           //  stable value
} TFilterRecord;

// Global data :
extern TFilterRecord FilterRecord;

//******************************************************************************
// Functions
//******************************************************************************

void FilterStop( void);
// Stop filtering

void FilterStart( void);
// Initialize & start filtering

TYesNo FilterRead( TRawWeight *Weight);
// Read filtered value

void FilterNextSample( TRawWeight Sample);
// Process next sample

void FilterRestart( void);
// Restart filtering

#endif
