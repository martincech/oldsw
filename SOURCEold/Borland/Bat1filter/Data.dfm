object BatData: TBatData
  OldCreateOrder = False
  Left = 312
  Top = 170
  Height = 672
  Width = 644
  object DataSource: TDataSource
    DataSet = DataTable
    Left = 56
    Top = 112
  end
  object DataTable: TTable
    Active = True
    DatabaseName = 'BAT1FILTER'
    FieldDefs = <
      item
        Name = 'DAY_NUMBER'
        DataType = ftSmallint
      end
      item
        Name = 'TIME_HOUR'
        DataType = ftSmallint
      end
      item
        Name = 'WEIGHT'
        DataType = ftFloat
      end
      item
        Name = 'SAVED'
        DataType = ftBoolean
      end
      item
        Name = 'STABLE'
        DataType = ftBoolean
      end>
    StoreDefs = True
    TableName = 'Online.db'
    Left = 64
    Top = 32
  end
end
