//******************************************************************************
//
//   AgMain.cpp   Agilent power interface demo
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "AgMain.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt       = new TCrt( MainMemo);
   Logger    = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Power = new TAgPower;
   Power->Logger = Logger;
} // FormCreate

void __fastcall TMainForm::BtnConnectClick(TObject *Sender)
{
   Power->SetCom( EdtCom->Text.c_str());
   if( !Power->Control( TRUE)){
      Crt->printf( "Unable set remote\n");
      return;
   }
   if( !Power->Display( "CONNECTED")){
      Crt->printf( "Unable display text\n");
      return;
   }
   Sleep( 1000);
   if( !Power->ClearDisplay()){
      Crt->printf( "Unable clear display\n");
      return;
   }
   if( !Power->EnableOutput( FALSE)){
      Crt->printf( "Unable enable/disable output\n");
      return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnVoltageClick(TObject *Sender)
{
   double Voltage;
   try {
      Voltage = EdtVoltage->Text.ToDouble();
   } catch(...){
      Crt->printf( "Invalid voltage format\n");
      return;
   }
   if( !Power->SetVoltage( Voltage)){
      Crt->printf( "Unable set voltage\n");
      return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnCurrentClick(TObject *Sender)
{
   double Current;
   try {
      Current = EdtCurrent->Text.ToDouble();
   } catch(...){
      Crt->printf( "Invalid current format\n");
      return;
   }
   if( !Power->SetCurrent( Current)){
      Crt->printf( "Unable set voltage\n");
      return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::CbEnableClick(TObject *Sender)
{
   if( !Power->EnableOutput( CbEnable->Checked)){
      Crt->printf( "Unable enable/disable output\n");
      return;
   }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   double Voltage, Current;
   if( !Power->Get( &Voltage, &Current)){
      Crt->printf( "Unable get data\n");
      return;
   }
   AnsiString Text;
   Text.printf( "%.5f V", Voltage);
   LblVoltage->Caption = Text;
   Text.printf( "%.5f A", Current);
   LblCurrent->Caption = Text;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::BtnErrorClick(TObject *Sender)
{
   char Message[ AG_MAX_BUFFER + 1];
   if( !Power->GetError( Message)){
      Crt->printf( "Unable get error status\n");
      return;
   }
   LblStatus->Caption = "Status : " + AnsiString( Message);
}
//---------------------------------------------------------------------------

