//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("AgDemo.res");
USEFORM("AgMain.cpp", MainForm);
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("AgPower.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
