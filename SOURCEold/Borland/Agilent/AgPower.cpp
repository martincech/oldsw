//******************************************************************************
//
//   AgPower.cpp   Agilent power supply
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "AgPower.h"

#define RX_TIMEOUT      300                   // total Rx timeout
#define RX_ICH_TIMEOUT  0                     // Rx intercharacter timeout

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                             \
   if( Logger){                                            \
      Logger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                             \
   if( Logger){                                            \
      Logger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                        \
   if( Logger){                                            \
      Logger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                   \
   if( Logger){                                            \
      Logger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TAgPower::TAgPower()
// Constructor
{
   Uart   = new TComUart;
   Logger = 0;
} // TAgPower

//******************************************************************************
// Destructor
//******************************************************************************

TAgPower::~TAgPower()
// Destructor
{
   if( Uart){
      delete Uart;
   }
} // ~TAgPower

//******************************************************************************
// Set COM
//******************************************************************************

void TAgPower::SetCom( char *Name)
// Set communinaction device name
{
   Disconnect();
   strncpyx( ComName, Name, AG_COM_NAME);
} // SetCom

//******************************************************************************
// Remote
//******************************************************************************

BOOL TAgPower::Control( BOOL Remote)
// Control by <Remote> / local
{
   if( Remote){
      sprintf( Buffer, "SYST:REM");
   } else {
      sprintf( Buffer, "SYST:LOC");
   }
   return( SendCommand( Buffer));
} // Remote

//******************************************************************************
// Display
//******************************************************************************

BOOL TAgPower::Display( char *Text)
// Display text
{
   sprintf( Buffer, "DISP:TEXT '%s'", Text);
   return( SendCommand( Buffer));
} // Display

//******************************************************************************
// Clear display
//******************************************************************************

BOOL TAgPower::ClearDisplay()
// Clear display
{
   sprintf( Buffer, "DISP:TEXT:CLE");
   return( SendCommand( Buffer));
} // ClearDisplay

//******************************************************************************
// Set voltage
//******************************************************************************

BOOL TAgPower::SetVoltage( double Voltage)
// Set voltage
{
   sprintf( Buffer, "VOLT %.5f", Voltage);
   return( SendCommand( Buffer));
} // SetVoltage

//******************************************************************************
// Set current
//******************************************************************************

BOOL TAgPower::SetCurrent( double Current)
// Set current
{
   sprintf( Buffer, "CURR %.5f", Current);
   return( SendCommand( Buffer));
} // SetCurrent

//******************************************************************************
// Enable output
//******************************************************************************

BOOL TAgPower::EnableOutput( BOOL Enable)
// Enable output power
{
   if( Enable){
      sprintf( Buffer, "OUTP ON");
   } else {
      sprintf( Buffer, "OUTP OFF");
   }
   return( SendCommand( Buffer));
} // EnableOutput

//******************************************************************************
// Get voltage & current
//******************************************************************************

BOOL TAgPower::Get( double *Voltage, double *Current)
// Get settings
{
   sprintf( Buffer, "APPL?");
   if( !SendCommand( Buffer)){
      return( FALSE);
   }
   char Reply[ AG_MAX_BUFFER + 1];
   if( !ReceiveReply( Reply)){
      return( FALSE);
   }
   if( sscanf( Reply, "\"%lf,%lf\"", Voltage, Current) != 2){
      return( FALSE);
   }
   return( TRUE);
} // Get

//******************************************************************************
// Get error
//******************************************************************************

BOOL TAgPower::GetError( char *Message)
// Get error message
{
   sprintf( Buffer, "SYST:ERR?");
   if( !SendCommand( Buffer)){
      return( FALSE);
   }
   char Reply[ AG_MAX_BUFFER + 1];
   if( !ReceiveReply( Reply)){
      return( FALSE);
   }
   strcpy( Message, Reply);
   return( TRUE);
} // GetError

//------------------------------------------------------------------------------

//******************************************************************************
// Check connect
//******************************************************************************

BOOL TAgPower::CheckConnect()
// Locate and open device
{
   if( Uart->IsOpen){
      return( TRUE);
   }
   TIdentifier Identifier;
   if( !Uart->Locate( ComName, Identifier)){
      Disconnect();
      return( FALSE);
   }
   if( !Uart->Open( Identifier)){
      Disconnect();
      return( FALSE);
   }
   // default parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate     = 9600;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   // common init :
   Uart->SetRxWait( RX_TIMEOUT, RX_ICH_TIMEOUT);
   Uart->Flush();
   return( TRUE);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TAgPower::Disconnect()
// Close device
{
   if( Uart){
      Uart->Close();
   }
} // Disconnect

//******************************************************************************
// Send command
//******************************************************************************

BOOL TAgPower::SendCommand( char *Command)
// Send command to device
{
   if( !CheckConnect()){
      return( FALSE);
   }
   int Size = strlen( Command);
   strcpy( Buffer, Command);
   Buffer[ Size++] = '\n';             // line terminator
   Buffer[ Size]   = '\0'; 
   Uart->Flush();
   if( Uart->Write( Buffer, Size) != Size){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( FALSE);
   }
   RwdTx( Command, Size - 1);
   return( TRUE);
} // SendCommand

//******************************************************************************
// Read reply
//******************************************************************************

BOOL TAgPower::ReceiveReply( char *Reply)
// Read reply from device
{
   if( !CheckConnect()){
      return( FALSE);
   }
   int RetSize;
   RetSize = Uart->Read( Buffer, AG_MAX_BUFFER);
   if( !RetSize){
      RwdReport( "Rx timeout");
      return( FALSE);
   }
   char *p = strchr( Buffer, '\n');
   if( !p){
      RwdGarbage( Buffer, RetSize);
      return( FALSE);            // no terminator
   }
   *p = '\0';                    // replace terminator
   int Size = strlen( Buffer);
   RwdRx( Buffer, Size);
   strcpy( Reply, Buffer);
   return( TRUE);
} // ReceiveReply
