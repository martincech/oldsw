//******************************************************************************
//
//   AgPower.h    Agilent power supply
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef AgPowerH
#define AgPowerH

#ifndef ComUartH
   #include "../Library/Serial/Uart/ComUart.h"
#endif

#ifndef LoggerH
   #include "../Library/Serial/Logger.h"
#endif

#define AG_COM_NAME   8
#define AG_MAX_BUFFER 80

#define AG_SUPPLY_OK     "+0,\"No error\""        // supply OK error message

//******************************************************************************
// TAgPower
//******************************************************************************

class TAgPower {
public :
   TAgPower();
   // Constructor

   ~TAgPower();
   // Destructor

   void SetCom( char *Name);
   // Set communincation device name

   BOOL Control( BOOL Remote);
   // Control by <Remote> / local

   BOOL Display( char *Text);
   // Display text

   BOOL ClearDisplay();
   // Clear display

   BOOL SetVoltage( double Voltage);
   // Set voltage

   BOOL SetCurrent( double Current);
   // Set current

   BOOL EnableOutput( BOOL Enable);
   // Enable output power

   BOOL Get( double *Voltage, double *Current);
   // Get settings

   BOOL GetError( char *Message);
   // Get error message

   TLogger  *Logger;                       // raw data logger
//------------------------------------------------------------------------------
protected :
   TComUart *Uart;                         // COM interface
   char      ComName[ AG_COM_NAME + 1];    // COM port name
   char      Buffer[ AG_MAX_BUFFER + 1];   // COM data buffer

   BOOL CheckConnect();
   // Locate and open device

   void Disconnect();
   // Close device

   BOOL SendCommand( char *Command);
   // Send command to device

   BOOL ReceiveReply( char *Reply);
   // Read reply from device
}; // TAgPower

#endif
