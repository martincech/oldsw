object MainForm: TMainForm
  Left = 400
  Top = 514
  Width = 687
  Height = 445
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LblVoltage: TLabel
    Left = 544
    Top = 232
    Width = 16
    Height = 13
    Caption = '? V'
  end
  object LblCurrent: TLabel
    Left = 544
    Top = 256
    Width = 16
    Height = 13
    Caption = '? A'
  end
  object LblStatus: TLabel
    Left = 32
    Top = 384
    Width = 45
    Height = 13
    Caption = 'Status : ?'
  end
  object MainMemo: TMemo
    Left = 32
    Top = 24
    Width = 369
    Height = 337
    TabOrder = 0
  end
  object EdtCom: TEdit
    Left = 464
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'COM1'
  end
  object BtnConnect: TButton
    Left = 464
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 2
    OnClick = BtnConnectClick
  end
  object EdtVoltage: TEdit
    Left = 504
    Top = 104
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '1,000'
  end
  object BtnVoltage: TButton
    Left = 416
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Voltage'
    TabOrder = 4
    OnClick = BtnVoltageClick
  end
  object BtnCurrent: TButton
    Left = 416
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Current'
    TabOrder = 5
    OnClick = BtnCurrentClick
  end
  object EdtCurrent: TEdit
    Left = 504
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '0,100'
  end
  object CbEnable: TCheckBox
    Left = 424
    Top = 192
    Width = 97
    Height = 17
    Caption = 'Enable power'
    TabOrder = 7
    OnClick = CbEnableClick
  end
  object BtnRead: TButton
    Left = 424
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 8
    OnClick = BtnReadClick
  end
  object BtnError: TButton
    Left = 424
    Top = 312
    Width = 75
    Height = 25
    Caption = 'Get Error'
    TabOrder = 9
    OnClick = BtnErrorClick
  end
end
