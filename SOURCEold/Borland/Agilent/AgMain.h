//******************************************************************************
//
//   AgMain.h     Agilent power interface demo
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef AgMainH
#define AgMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------

#include "AgPower.h"
#include "../Library/Crt/Crt.h"
#include "../Library/Serial/CrtLogger.h"

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TMemo *MainMemo;
   TEdit *EdtCom;
   TButton *BtnConnect;
   TEdit *EdtVoltage;
   TButton *BtnVoltage;
   TButton *BtnCurrent;
   TEdit *EdtCurrent;
   TCheckBox *CbEnable;
   TButton *BtnRead;
   TLabel *LblVoltage;
   TLabel *LblCurrent;
   TButton *BtnError;
   TLabel *LblStatus;
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall BtnConnectClick(TObject *Sender);
   void __fastcall BtnVoltageClick(TObject *Sender);
   void __fastcall BtnCurrentClick(TObject *Sender);
   void __fastcall CbEnableClick(TObject *Sender);
   void __fastcall BtnReadClick(TObject *Sender);
   void __fastcall BtnErrorClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   TAgPower   *Power;
   TCrt       *Crt;
   TCrtLogger *Logger;
}; // TMainForm

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
