//******************************************************************************
//
//   ReadProgress.h   Read progress frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef ReadProgressH
#define ReadProgressH

//----------------------------------------------------------------------------
#include <vcl\System.hpp>
#include <vcl\Windows.hpp>
#include <vcl\SysUtils.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\ExtCtrls.hpp>
#include <ComCtrls.hpp>
//----------------------------------------------------------------------------

class TReadProgressForm : public TForm
{
__published:
        TButton *BtnCancel;
	TBevel *Bevel1;
        TProgressBar *ProgressBar;
        TLabel *LblProgress;
        TLabel *Label1;
        TLabel *LblTime;
        void __fastcall Cancel(TObject *Sender);
private:
   bool DoCancel;   // predcasne ukonceni
public:
   virtual __fastcall TReadProgressForm(TComponent* AOwner);
   bool __fastcall Execute( TForm *Parent);
};
//----------------------------------------------------------------------------
extern PACKAGE TReadProgressForm *ReadProgressForm;
//----------------------------------------------------------------------------
#endif    
