//******************************************************************************
//
//   MReader.h         Memory module reader
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef MReaderH
#define MReaderH

#ifndef SerialH
   #include "../Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Serial/Logger.h"
#endif

#pragma pack( push, 1)                  // byte alignment
   #include "Ctecka.h"                  // protocol definition
#pragma pack( pop)                      // original alignment

//******************************************************************************
// TMReader
//******************************************************************************

class TMReader {
public :
   TMReader();
   ~TMReader();

   bool Connect( TString Port);
   // Connect via <Port>

   bool GetVersion( int &Version);
   // Get reader's version number
   bool ModuleInserted();
   // Check if module is inserted
   bool ReadByte( int Address, byte &Data);
   // Read single byte
   bool WriteByte( int Address, byte Data);
   // Write single byte
   bool ReadData( int Address, int Size, void *Buffer);
   // Read data packet

   __property TUart   *Port     = {read=FPort};
   __property TString  PortName = {read=FPortName};
   __property TLogger *Logger   = {read=FLogger, write=FLogger};

//---------------------------------------------------------------------------
protected :
   TUart   *FPort;                    // connection port
   TString  FPortName;                // connection port name
   TLogger *FLogger;                  // raw data logger
   int      FRxTimeout;               // Rx timeout
   int      FRxIntercharacterTimeout; // Rx intercharacter timeout
   TString  FUsbDevice;               // USB device name
   TCmdUnion   Command;               // command buffer
   TReplyUnion Reply;                 // reply buffer

   bool Send( int SendSize, int ReplySize);
   // Send <Command>, get <Reply>
   bool CheckConnect();
   // Check if adapter is ready
   void Disconnect();
   // Disconnect port
}; // TMReader

//---------------------------------------------------------------------------
#endif
