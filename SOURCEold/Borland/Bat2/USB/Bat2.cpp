//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat2.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("MReader.cpp");
USEUNIT("MFlash.cpp");
USEFORM("ReadProgress.cpp", ReadProgressForm);
USEUNIT("Workplace.cpp");
USEUNIT("KConvert.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\Memory\ObjectMemory.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USELIB("..\Library\Serial\USB\FTDI.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->CreateForm(__classid(TReadProgressForm), &ReadProgressForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
