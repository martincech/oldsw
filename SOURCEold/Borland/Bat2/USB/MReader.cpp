//******************************************************************************
//
//   MReader.cpp       Memory module reader
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MReader.h"
#include <typeinfo.h>
#include <string.h>
#include "../Serial/Uart/ComUart.h"
#include "../Serial/Usb/UsbUart.h" // pozdeji
#include "KConvert.h"

#pragma package(smart_init)

#define USB_NAME        "USB"                  // USB adapter identification
#define USB_DEVICE_NAME "Veit USB Reader"      // USB adapter name

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Constructor
//******************************************************************************

TMReader::TMReader()
// Constructor
{
   FPort   = new TComUart;
   FLogger = 0;

   FRxTimeout               = 300;
   FRxIntercharacterTimeout = 100;
   FUsbDevice               = USB_DEVICE_NAME;
} // TMReader

//******************************************************************************
// Destructor
//******************************************************************************

TMReader::~TMReader()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TMReader

//******************************************************************************
// Connect
//******************************************************************************

bool TMReader::Connect( TString Port)
// Connect via <Port>
{
   // disconnect first :
   Disconnect();
   // remember name only :
   FPortName = Port;
   CheckConnect();        // try open, don't check success
   return( true);
} // Connect

//******************************************************************************
// Version
//******************************************************************************

bool TMReader::GetVersion( int &Version)
// Get reader's version number
{
   Command.Cmd = CMD_VERSION;
   if( !Send( sizeof( Command.Cmd), sizeof( TReplyVersion))){
      return( false);
   }
   Version = GetWord( Reply.Version.Version);
   return( true);
} // GetVersion

//******************************************************************************
// Inserted
//******************************************************************************

bool TMReader::ModuleInserted()
// Check if module is inserted
{
   Command.Cmd = CMD_INSERT;
   if( !Send( sizeof( Command.Cmd), sizeof( Reply.Ack))){
      return( false);
   }
   if( Reply.Ack == CMD_ACK){
      return( true);
   }
   return( false);
} // ModuleInserted

//******************************************************************************
// Read byte
//******************************************************************************

bool TMReader::ReadByte( int Address, byte &Data)
// Read single byte
{
   Command.Read.Cmd     = CMD_READ;
   Command.Read.Address = PutDword( Address);
   if( !Send( sizeof( TCmdRead), sizeof( TReplyRead))){
      return( false);
   }
   Data = Reply.Read.Data;
   return( true);
} // ReadByte

//******************************************************************************
// Write byte
//******************************************************************************

bool TMReader::WriteByte( int Address, byte Data)
// Write single byte
{
   Command.Write.Cmd     = CMD_WRITE;
   Command.Write.Address = PutDword( Address);
   Command.Write.Data    = Data;
   if( !Send( sizeof( TCmdWrite), sizeof( Reply.Ack))){
      return( false);
   }
   if( Reply.Ack == CMD_ACK){
      return( true);
   }
   return( false);
} // WriteByte

//******************************************************************************
// Read data
//******************************************************************************

bool TMReader::ReadData( int Address, int Size, void *Buffer)
// Read data packet
{
   Command.BlockRead.Cmd     = CMD_BLOCK_READ;
   Command.BlockRead.Address = PutDword( Address);
   Command.BlockRead.Length  = PutWord( Size);
   if( !Send( sizeof( TCmdBlockRead), Size + 1)){
      return( false);
   }
   if( Reply.BlockRead.Ack != CMD_ACK){
      return( false);
   }
   memcpy( Buffer, &Reply.BlockRead.Data, Size);
   return( true);
} // ReadData

//******************************************************************************
// Send
//******************************************************************************

bool TMReader::Send( int SendSize, int ReplySize)
// Send <Command>, get <Reply>
{
   if( !CheckConnect()){
      return( false);                  // no adapter
   }
   FPort->Flush();
   if( FPort->Write( &Command, SendSize) != SendSize){
      return( false);
   }
   RwdTx( &Command, SendSize);
   FPort->SetRxWait( FRxTimeout, 0);   // wait for first char
   if( FPort->Read( &Reply.Ack, 1) != 1){
      return( false);
   }
   int Remainder = ReplySize - 1;
   if( Remainder <= 0){
      // jednobytova odpoved
      RwdRx( &Reply.Ack, 1);
      return( true);
   }
   FPort->SetRxWait( FRxTimeout, FRxIntercharacterTimeout);
   int Size;
   Size = FPort->Read( &Reply.Buffer[ 1], Remainder);
   if( Size != Remainder){
      // kratka data
      RwdGarbage( &Reply, Size + 1);
      return( false);
   }
   // uspesny prijem
   RwdRx( &Reply, ReplySize);
   return( true);
} // Send

//******************************************************************************
// Check Connection
//******************************************************************************

bool TMReader::CheckConnect()
// Check if adapter is ready
{
   if( FPort && FPort->IsOpen){
      return( true);
   }
   if( FPort){
      IERROR;                          // always opened !
   }
   TIdentifier Identifier;
   TUart    *Uart;
   TUsbUart *Usb = 0;
   if( FPortName == USB_NAME){
      // USB setup
      Usb = new TUsbUart;
      if( !Usb->Locate( FUsbDevice, Identifier)){
         delete Usb;
         return( false);
      }
      Uart = Usb;
   } else {
      // COM setup :
      TComUart *Com = new TComUart;
      if( !Com->Locate( PortName, Identifier)){
         delete Com;
         return( false);
      }
      Uart = Com;
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   if( Usb){
      Usb->TxLatency = 2;              // latence vystupniho bufferu
   } else {
      // parametry serioveho portu :
      TUart::TParameters Parameters;
      Parameters.BaudRate  = 19200;
      Parameters.DataBits  = 8;
      Parameters.StopBits  = 10;
      Parameters.Parity    = TUart::EVEN_PARITY;
      Parameters.Handshake = TUart::NO_HANDSHAKE;
      Uart->SetParameters( Parameters);
   }
   // common init :
   Uart->SetRxNowait();
   Uart->Flush();
   FPort = Uart;
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TMReader::Disconnect()
// Disconnect port
{
   if( !FPort){
      return;
   }
   delete FPort;
   FPort = 0;
} // Disconnect

