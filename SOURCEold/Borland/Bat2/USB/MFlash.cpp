//******************************************************************************
//
//   MFlash.cpp   Memory module Flash decoder
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MFlash.h"
#include <stdio.h>


#define MTRIALS        3                // pocet pokusu o komunikaci
#define MREAD_PACKET CTECKA_MAX_PACKET  // velikost paketu pri cteni

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
//  Konstruktor
//******************************************************************************

TMFlash::TMFlash()
// Konstruktor
{
   FAdapter = 0;
   FRawData = new byte[ FLASH_SIZE];       // kapacita flash
   FConfigSection = (TConfigSection *)  &FRawData[ FL_CONFIG_BASE];
   FArchiv        = (TArchivDailyInfo *)&FRawData[ FL_ARCHIV_BASE];
} // TMFlash

//******************************************************************************
// Destruktor
//******************************************************************************

TMFlash::~TMFlash()
// Destruktor
{
   if( FRawData){
      delete[] FRawData;
   }
} // ~TMFlash

//******************************************************************************
// Adapter
//******************************************************************************

bool TMFlash::CheckAdapter()
// Kontroluje adapter
{
   if( !Adapter){
      return( false);       // nedefinovan adapter
   }
   for( int i = 0; i < MTRIALS; i++){
      if( Adapter->GetVersion( FAdapterVersion)){
         return( true);
      }
   }
   FAdapterVersion = 0;
   return( false);
} // CheckAdapter

//******************************************************************************
// Modul
//******************************************************************************

bool TMFlash::CheckModule()
// Otestuje pritomnost modulu
{
   if( !Adapter){
      return( false);                  // nedefinovan adapter
   }
   if( !CheckAdapter()){
      return( false);                  // neni pripojen adapter
   }
   for( int i = 0; i < MTRIALS; i++){
      if( Adapter->ModuleInserted()){
         FModuleInserted = true;
         return( true);
      }
   }
   FModuleInserted = false;
   return( false);
} // CheckModule

//******************************************************************************
// Cteni konfigurace
//******************************************************************************

bool TMFlash::ReadConfigSection()
// Nacte konfiguracni sekci modulu
{
   if( !Adapter){
      return( false);       // nedefinovan adapter
   }
   if( !FAdapterVersion || !FModuleInserted){
      return( false);       // neni adapter nebo modul
   }
   int ReadOffset = FL_CONFIG_BASE;
   int TotalSize  = FL_CONFIG_SIZE;
   do {
      int Size = MREAD_PACKET;            // velikost paketu
      if( TotalSize < MREAD_PACKET){
         Size = TotalSize;                // posledni paket muze byt kratsi
      }
      // cteni paketu :
      for( int i = 0; i < MTRIALS; i++){
         if( Adapter->ReadData( ReadOffset, Size, &FRawData[ ReadOffset])){
            ReadOffset += Size;
            break;;
         }
      }
      // zbyvajici velikost :
      TotalSize -= Size;
   } while( TotalSize > 0);
   return( true);
} // ReadConfigSection

//******************************************************************************
// Cteni start
//******************************************************************************

bool TMFlash::ReadData()
// Zahaji cteni dat, vraci false pri chybe
{
   if( !Adapter){
      return( false);       // nedefinovan adapter
   }
   if( !FAdapterVersion || !FModuleInserted){
      return( false);       // neni adapter nebo modul
   }
   FReadOffset = 0;         // od zacatku Flash
   return( true);
} // ReadData

//******************************************************************************
// Cteni paketu
//******************************************************************************

bool TMFlash::ReadPacket( bool &Done)
// Precte data, vraci false pri chybe. Nastavi <Done> je-li nacteno vse
{
   Done = false;
   int Size = MREAD_PACKET;            // velikost paketu
   int Remainder = FLASH_SIZE - FReadOffset;
   if( Remainder < MREAD_PACKET){
      Done = true;                     // posledni cteni
      Size = Remainder;                // posledni paket muze byt kratsi
   }
   for( int i = 0; i < MTRIALS; i++){
      if( Adapter->ReadData( FReadOffset, Size, &FRawData[ FReadOffset])){
         FReadOffset += Size;
         return( true);
      }
   }
   return( false);
} // ReadPacket

//******************************************************************************
// Konec cteni
//******************************************************************************

void TMFlash::ReadFinalize()
// Uzavre proceduru cteni dat
{
   // pro jistotu nastavime ofsety :
   FConfigSection = (TConfigSection *)  &FRawData[ FL_CONFIG_BASE];
   FArchiv        = (TArchivDailyInfo *)&FRawData[ FL_ARCHIV_BASE];
} // ReadFinalize

//******************************************************************************
//  Archiv
//******************************************************************************

void TMFlash::ArchivFirst()
// Nastavi archiv pred prvni zaznam
{
   // hledani markeru :
   for( int i = 0; i < FL_ARCHIV_DAYS; i++){
      if( *(byte *)&FArchiv[ i] == FL_ARCHIV_EMPTY){
         if( i == 0){
            FCurArchiv = -1;           // prazdny archiv
         } else {
            FCurArchiv  = 0;           // od zacatku
         }
         FLastArchiv = i;              // po znacku
         return;
      }
      if( *(byte *)&FArchiv[ i] == FL_ARCHIV_FULL){
         if( i == FL_ARCHIV_DAYS - 1){
            FCurArchiv = 0;            // znacka na konci
         } else {
            FCurArchiv = i + 1;        // zacatek za znackou
         }
         FLastArchiv = i;              // po znacku
         return;
      }
   }
   FCurArchiv = -1;                    // nenalezena znacka
} // ArchivFirst

TArchiv *TMFlash::ArchivNext()
// Nasledujici zaznam (0=posledni)
{
   if( FCurArchiv < 0){
      return( 0);                      // konec
   }
   int Index = FCurArchiv;             // pamatuj aktualni pozici
   // vypocti nasledujici polozku :
   FCurArchiv++;
   if( FCurArchiv >= FL_ARCHIV_DAYS){
      // jsme na konci
      FCurArchiv =  0;                 // pokracuje od zacatku
   }
   if( FCurArchiv == FLastArchiv){
      FCurArchiv = -1;                 // jsme na znacce, konec
   }
   FArchivDay = &FArchiv[ Index];
   return( &FArchivDay->Archiv);
} // ArchivNext

//******************************************************************************
//  Zaznam vzorku
//******************************************************************************

void TMFlash::LoggerFirstSample()
// Nastavi logger pred prvni vzorek
{
   // hledani markeru :
   for( int i = 0; i < FL_LOGGER_SAMPLES; i++){
      if( *(byte *)&FArchivDay->Samples[ i] == LG_SAMPLE_EMPTY){
         if( i == 0){
            FCurSample = -1;           // prazdny archiv
         } else {
            FCurSample  = 0;           // od zacatku
         }
         FLastSample = i;              // po znacku
         return;
      }
      if( *(byte *)&FArchivDay->Samples[ i] == LG_SAMPLE_FULL){
         if( i == FL_LOGGER_SAMPLES - 1){
            FCurSample = 0;            // znacka na konci
         } else {
            FCurSample = i + 1;        // zacatek za znackou
         }
         FLastSample = i;              // po znacku
         return;
      }
   }
   FCurSample = -1;                    // nenalezena znacka
} // LoggerFirstSample

TLoggerSample *TMFlash::LoggerNextSample()
// Nastavi logger na dalsi vzorek (0=posledni)
{
   if( FCurSample < 0){
      return( 0);                      // konec
   }
   int Index = FCurSample;             // pamatuj aktualni pozici
   // vypocti nasledujici polozku :
   FCurSample++;
   if( FCurSample >= FL_LOGGER_SAMPLES){
      // jsme na konci
      FCurSample =  0;                 // pokracuje od zacatku
   }
   if( FCurSample == FLastSample){
      FCurSample = -1;                 // jsme na znacce, konec
   }
   return( &FArchivDay->Samples[ Index]);
} // LoggerNextSample

//******************************************************************************
//  Nacteni dat
//******************************************************************************

bool TMFlash::Load( AnsiString File)
// Nacte data ze souboru
{
   if( !FileExists( File)){
      return( false);
   }
   FILE *f;
   f = fopen( File.c_str(), "rb");
   if( !f){
      return( false);
   }
   fread( FRawData, FLASH_SIZE, 1, f);
   fclose( f);
   ReadFinalize();       // upravy surovych dat
   return( true);
} // Load

//******************************************************************************
// Ulozeni dat
//******************************************************************************

void TMFlash::Save( AnsiString File)
// Ulozi data do souboru
{
   FILE *f;
   f = fopen( File.c_str(), "wb");
   fwrite( FRawData, FLASH_SIZE, 1, f);
   fclose( f);
} // Save

//******************************************************************************
// Prubeh cteni
//******************************************************************************

int TMFlash::GetReadProgress()
// Vraci pocet promile nactenych dat
{
   return( (FReadOffset * 1000) / FLASH_SIZE);
} // GetReadProgress

