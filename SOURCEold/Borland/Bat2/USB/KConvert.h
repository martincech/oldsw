//******************************************************************************
//
//   KConvert.h   Keil data conversions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef KConvertH
   #define KConvertH

#pragma pack( push, 1)                  // byte alignment
   #include "Bat2.h"                    // flash data definition
#pragma pack( pop)                      // original alignment

//******************************************************************************
//  Konverze z Keil :
//******************************************************************************

word   GetWord(  word X);
int16  GetInt(   int16 X);
dword  GetDword( dword X);
long32 GetLong(  long32 X);
float  GetFloat( float X);
TDateTime  GetDateTime( TCas X);
AnsiString GetString( byte *S, int Length = 0);

//******************************************************************************
//  Konverze do Keil :
//******************************************************************************

#define PutWord( X)  GetWord( X)
#define PutInt( X)   GetInt( X)
#define PutDword( X) GetDword( X)
#define PutLong( X)  GetLong( X)
#define PutFloat( X) GetFloat( X)

TCas PutDateTime( TDateTime X);
void PutString( byte *S, AnsiString X, int Length);

#endif
