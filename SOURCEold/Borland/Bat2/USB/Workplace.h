//******************************************************************************
//
//   Workplace.h  Flash module working platform
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef WorkplaceH
#define WorkplaceH

#include "../Serial/Uart.h"
#include "MReader.h"
#include "MFlash.h"

//******************************************************************************
// TWorkplace
//******************************************************************************

class TWorkplace {
public :
   TWorkplace();
   // Konstruktor
   void Initialize();
   // Uvodni inicializace
   bool SetPort( TString PortName);
   // Nastaveni komunikacniho portu

   __property TMReader *Adapter = {read=FAdapter, write=FAdapter};
   __property TMFlash  *Module  = {read=FModule};

//---------------------------------------------------------------------------
protected :
   TMReader      *FAdapter;
   TMFlash       *FModule;
}; // TWorkplace
//---------------------------------------------------------------------------

extern TWorkplace *Workplace;

#endif
