//******************************************************************************
//
//   Workplace.cpp  Flash module working platform
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Workplace.h"

TWorkplace *Workplace = 0;

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TWorkplace::TWorkplace()
{
   FAdapter = new TMReader;
   FModule  = new TMFlash;
   FModule->Adapter = Adapter;
   Workplace = this;                // globalni pointer
} // TWorkplace

//******************************************************************************
// Inicializace
//******************************************************************************

void TWorkplace::Initialize()
// Uvodni inicializace
{
   // first start, set defaults
   if( !SetPort( "COM1")){
      return;
   }
} // Initialize

//******************************************************************************
// Otevreni portu
//******************************************************************************

bool TWorkplace::SetPort( TString PortName)
// Nastaveni komunikacniho portu
{
   if( !FAdapter->Connect( PortName)){
      return( false);
   }
   return( true);
} // SetPort

