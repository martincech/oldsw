//*****************************************************************************
//
//    Bat2.h - common data definitions
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Bat2_H__
   #define __Bat2_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Stat_H__
   #include "Stat.h"
#endif

#ifndef __Histogr_H__
   #include "Histogr.h"
#endif

#ifdef __C51__
// jen pro Keil
   typedef int  int16;
   typedef long long32;
   #include <stddef.h>        // makro offsetof
   #include "..\inc\Iep.h"    // IEP_SIZE, IEP_PAGE_SIZE
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define VERZE                       0x0101  // verze SW

#define NOSNOST_NASLAPNYCH_VAH      99999
#define DILEK_NASLAPNYCH_VAH        1
#define POCETDESETIN_NASLAPNYCH_VAH 3
#define MIN_NORMOVANA_HMOTNOST      30      // Minimalni pripustna cilova hmotnost, pokud vyjde mensi, koriguju
#define MAX_HMOTNOST_KALIBRACE      200000  // Maximalni hmotnost zadavana pri kalibraci
#define MAX_HMOTNOST_ZOBRAZENI      200000  // Maximalni zobrazitelna hmotnost
#define MIN_HMOTNOST_ZOBRAZENI      -99999  // Minimalni zobrazitelna hmotnost

typedef enum {
  JEDNOTKY_KG,
  JEDNOTKY_LB,
  _JEDNOTKY_POCET
} TJednotky;

extern TYesNo AccuOk;                        // Flag, zda je napajeni v poradku

// ---------------------------------------------------------------------------------------------
// Zobrazeni
// ---------------------------------------------------------------------------------------------

// Zobrazeni - co se ma v nasledujicim kroku zobrazit, ORuje se do promenne Zobrazit
#define ZOBRAZIT_NV_HMOTNOST      0x01           // Zobrazi se naslapna vaha po vypocteni nove aktualni hmotnosti
#define ZOBRAZIT_NV_ULOZENO       0x02           // Zobrazi se naslapna vaha po ulozeni nove hmotnosti
#define ZOBRAZIT_1SEC             0x04           // Zobrazi se periodicke veci 1sec (napr. cas)
#define ZOBRAZIT_HISTOGRAM_KURZOR 0x08           // Zobrazi se kurzor v histogramu spolecne s hmotnosti a poctem
#define ZOBRAZIT_MRIZKU           0x80           // Zobrazi se mrizka (kostra)
#define ZOBRAZIT_VSE              0xFF           // Zobrazi se vse (jsou nastaveny vsechny bity)

extern byte __xdata__ Zobrazit;     // Co mam v nasledujicim kroku prekreslit


// ---------------------------------------------------------------------------------------------
// Datum a cas
// ---------------------------------------------------------------------------------------------

// Uplny datum, vcetne celeho roku a na cele bajty (v pripade vicenasobneho ukladani predelat na bity)
typedef struct {
  byte Min;
  byte Hour;
  byte Day;
  byte Month;
  word Year;
} TCas;  // 6 bajtu

// Globalni datum a cas
extern TCas __xdata__ ActualDateTime;

// ---------------------------------------------------------------------------------------------
// Rustova krivka
// ---------------------------------------------------------------------------------------------

#define KRIVKA_MAX_POCET         30          // Max pocet pole s definici rustove krivky
#define KRIVKA_MAX_DEN           999         // Max cislo dne
#define KRIVKA_MAX_HMOTNOST      0xFFFF      // Max hmotnost (aby se vlezla do 2 bajtu)
#define KRIVKA_HMOTNOST_UKONCENI 0           // Tato hmotnost ukoncuje krivku (nulova hmotnost nemuze nikdy byt)
#define KRIVKA_CISLO_PRVNIHO_DNE 1           // Cislo prvniho dne, od ktereho se zacina krmit

#define NormovanaHmotnostVPoradku(Hmotnost) (Hmotnost >= MIN_NORMOVANA_HMOTNOST && Hmotnost <= KRIVKA_MAX_HMOTNOST)

// Format bodu rustove krivky
typedef struct {
  word Den;                            // 0-KRIVKA_MAX_DEN (999)
  word Hmotnost;                       // 0-KRIVKA_MAX_HMOTNOST (0xFFFF)
} TBodKrivky;  // 4 bajty

// Pole krivky
typedef TBodKrivky TBodyKrivky[KRIVKA_MAX_POCET];  // 120 bajtu

//-----------------------------------------------------------------------------
// Hejna
//-----------------------------------------------------------------------------

#define MAX_DELKA_NAZVU_HEJNA     8    // Maximalni delka
#define CISLO_PRAZDNEHO_HEJNA  0xFF    // Pokud je cislo hejna rovno teto hodnote, hejno neni definovane
#define NEVYPLNENY_CAS_OMEZENI 0xFF    // Pokud je casove omezeni vazeni rovno teto hodnote, tak se casove neomezuje a vazi se stale
#define MAX_HODINA               23    // Maximalni hodnota hodiny pro omezeni (0-23hod)

// Rozdeleni pohlavi
#define POHLAVI_SAMICE 0               // Pohlavi samice
#define POHLAVI_SAMEC  1               // Pohlavi samec

#define NAZEV_HEJNA_RYCHLE_VAZENI "-       "  // Default nazev hejna pri rychlem vazeni - pozor, musi mit MAX_DELKA_NAZVU_HEJNA znaku

// Zahlavi hejna
typedef struct {
  byte Cislo;                          // Cislo hejna
  byte Nazev[MAX_DELKA_NAZVU_HEJNA];   // Nazev hejna (bez ukoncovaci nuly, prazdne znaky se doplni mezerami)
  byte PouzivatKrivky;                 // Zda se maji pouzivat rustove krivky nebo automaticke zjistovani cilove hmotnosti bez pouziti rustovych krivek
                                       // Pokud krivky nepouziva, jsou zadane pocatecni hmotnosti ulozene jako 1. bod ve krivce
  byte RozdelovatPohlavi;              // Flag, zda se ma pouzivat rozliseni na samce a samice
  byte VazitOdHodiny;                  // Umoznuje omezit ukladani az od zadane hodiny vcetne. Pokud je vyplneno NEVYPLNENY_CAS_OMEZENI, neomezuje se
  byte VazitDoHodiny;                  // Umoznuje omezit ukladani pouze do zadane hodiny vcetne. Pokud je vyplneno NEVYPLNENY_CAS_OMEZENI, neomezuje se
} TZahlaviHejna;  // 13 bajtu

// Struktura jednoho hejna
typedef struct {
  TZahlaviHejna Zahlavi;
  TBodyKrivky   RustovaKrivkaSamice;   // Rustova krivka pro samice, pripadne celkova rustova krivka, pokud se rozdeleni na samce a samice nepouziva
  TBodyKrivky   RustovaKrivkaSamci;    // Rustova rivka pro samce, pokud se pouziva
} THejno; // 253 bajtu

#define MAX_POCET_HEJN 10              // Pocet hejn, ktere muze zadat
typedef THejno THejna[MAX_POCET_HEJN]; // Cisla hejn odpovidaji pozici, tj. na pozici 0 je hejno cislo 0, na pozici 1 hejno cislo 1 atd.

// Struktura s provoznimi promennymi pro aktualni den vykrmu - tato je v RAM, jsou zde jiz predem predpocitane veci (meze z rustove krivky atd.) a dalsi
// veci, ktere jsou stale potreba a v prubehu dne se nemeni, napr. pro zobrazeni. Je to jakasi cache dat z EEPROM, do RAM se to nacte vzdy po nejake
// zmene v EEPROM, pri prechodu na dalsi den atd.
// Meze musi byt long, kdyby zadal napr. 65.535kg +- 10%, tak horni mez se nevejde do 2 bajtu
typedef struct {
  TZahlaviHejna Zahlavi;
  word          CisloDneVykrmu;                                   // Cislo dne od pocatku vykrmu
  word          NormovanaHmotnostSamice, NormovanaHmotnostSamci;  // Normovana hmotnost nactena z rustove krivky pro samice a samce. Pokud se pohlavi nerozdeluje, bere se pro samice.
  TRawValue     HorniMezSamice, DolniMezSamice;                   // Dolni a horni mez hmotnosti z rustove krivky samic, pripadne pro obe pohlavi, pokud se rozdeleni na samce a samice nepouziva
  TRawValue     HorniMezSamci, DolniMezSamci;                     // Dolni a horni mez hmotnosti z rustove krivky samcu
} TVykrmHejna;

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

#define MAX_POCET_PLOSIN 10             // Maximalni pocet plosin

// Kalibrace 1 plosiny
typedef struct {
  long32 KalibraceNuly;                 // Kalibrace nuly
  long32 KalibraceRozsahu;              // Kalibrace rozsahu
  long32 Rozsah;                        // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
  byte Dilek;                           // Dilek vah
} TKalibracePlosiny; // 13 bajtu

// Kalibrace
typedef struct {
  byte PouzitaPlosina;                  // Cislo prave pouzite plosiny
  TKalibracePlosiny Plosiny[MAX_POCET_PLOSIN];
} TKalibrace;   // 131 bajtu

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

// Rychle zadani parametru vykrmu rovnou pri startu, bez pouziti nektereho preddefinovaneho hejna.
// Pri pouziti hejna se tyto parametry berou primo z hejna.
typedef struct {
  byte RozdelovatPohlavi;        // Flag, zda se ma pouzivat rozliseni na samce a samice
  word PocatecniHmotnostSamice;  // Zadana normovana hmotnost pro pocatecni den vykrmu pro samice. Pokud se pohlavi nerozdeluje, bere se pro samice.
  word PocatecniHmotnostSamci;   // Zadana normovana hmotnost pro pocatecni den vykrmu pro samce.
} TRychlyVykrm;  // 5 bajtu

// Parametry, ktere zadal pri zahajeni vykrmu. Tyto parametry se v celem prubehu vazeni nemeni.
// Po skonceni vykrmu se zde drzi vsechny parametry posledniho vykrmu az do doby odstartovani dalsiho vykrmu. Parametry odtut muze tedy vyuzivat
// historie i po ukonceni vykrmu.
typedef struct {
  byte      ProbihaKrmeni;  // Flag, ze prave probiha krmeni (AktualniHejno je v rychlem vazeni nulove a nestaci uz k podmince zda se krmi nebo ne)
  byte      PouzivatHejno;  // Flag, zda zvolil krmeni podle urciteho hejna. Pokud ne, zadal zakladni parametry primo a vazi se bez pouziti hejna, za pouziti parametru v TRychlyVykrm.
  byte      AktualniHejno;  // Cislo hejna, podle ktereho se prave krmi. Pokud se krmi zadne hejno, je zde ulozena hodnota CISLO_PRAZDNEHO_HEJNA
  word      PosunutiKrivky; // V kolikatem dnu vykrm zacal minus 1, tj. o kolik se ma krivka posunout. Standardne je to 0.
  TCas      ZahajeniVykrmu; // Datum zahajeni vykrmu
  TRychlyVykrm RychlyVykrm; // Zde jsou parametry vykrmu pri PouzivatHejno=NO
  byte RozsahUniformity;    // Rozsah uniformity v +- %
} TStartVykrmu;  // 17 bajtu

// Parametry GSM
#define GSM_NUMBER_MAX_LENGTH 15        // Maximalni delka telefonniho cisla bez zakoncovaci nuly
#define GSM_NUMBER_MAX_COUNT  5         // Maximalni pocet definovanych telefonnich cisel
typedef struct {
  byte Use;                 // Flag, zda se GSM modul vyuziva (zda je pripojen)
  byte SendMidnight;        // Flag, zda se ma posilat o pulnoci statistika
  byte SendOnRequest;       // Flag, zda se ma odpovidat na zaslane SMS
  byte ExecuteOnRequest;    // Flag, zda se ma provadet cinnost podle zaslanych SMS (zatim nevyuzito)
  byte NumberCount;         // Pocet definovanych telefonnich cisel, na ktere se vysila o pulnoci
  byte Numbers[GSM_NUMBER_MAX_COUNT][GSM_NUMBER_MAX_LENGTH];  // Definovana telefonni cisla
} TGsm;  // 78 bajtu

#define MAX_IDENTIFIKACNI_CISLO 31    // Maximalni hodnota identifikacniho cisla

#define MIN_ROZSAH_HISTOGRAMU 10      // Minimalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu
#define MAX_ROZSAH_HISTOGRAMU 100     // Maximalni hodnota rozsahu histogramu v procentech stredni hodnoty histogramu (+-100% dava rozsah 0 az 2xstred)
#define MIN_ROZSAH_UNIFORMITY 1       // Minimalni hodnota rozsahu uniformity v procentech
#define MAX_ROZSAH_UNIFORMITY 49      // Maximalni hodnota rozsahu uniformity v procentech (+-49% dava 98% celkem)

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                          // VERZE
  byte IdentifikacniCislo;             // identifikace zarizeni

  byte SamiceOkoliNad;                 // Okoli nad prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamiceOkoliPod;                 // Okoli pod prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamciOkoliNad;                  // Okoli nad prumerem pro samce v procentech
  byte SamciOkoliPod;                  // Okoli pod prumerem pro samce v procentech
  byte UstaleniNaslapneVahy;           // Ustaleni hmotnosti po nastupu v procentech prvni zvazene hmotnosti

  TJednotky Jednotky;                  // Zobrazovane jednotky

  byte RozsahHistogramu;               // Rozsah histogramu v +- % stredni hodnoty
  byte RozsahUniformity;               // Rozsah uniformity v +- %

  TStartVykrmu StartVykrmu;

  TGsm Gsm;

  byte KontrolniSuma;                  // Kontrolni soucet
} TKonfigurace;  // 107 bajtu

extern TKonfigurace __xdata__ Konfigurace;   // Buffer konfigurace v externi RAM
extern TKalibrace   __xdata__ Kalibrace;     // Buffer kalibrace v externi RAM

// Masky pro zapis konfigurace do EEPROM :
// Kalibraci ukladam zvlast

#define CFG_ALL                       0xFFFFFFFFL    // Uloz vsechno
#define CFG_VERZE                     0x00000001L
#define CFG_IDENTIFIKACNI_CISLO       0x00000002L
#define CFG_SAMICE_OKOLI_NAD          0x00000004L
#define CFG_SAMICE_OKOLI_POD          0x00000008L
#define CFG_SAMCI_OKOLI_NAD           0x00000010L
#define CFG_SAMCI_OKOLI_POD           0x00000020L
#define CFG_USTALENI_NASLAPNE_VAHY    0x00000040L
#define CFG_START_VYKRMU              0x00000080L
#define CFG_JEDNOTKY                  0x00000100L
#define CFG_ROZSAH_HISTOGRAMU         0x00000200L
#define CFG_ROZSAH_UNIFORMITY         0x00000400L
#define CFG_GSM                       0x00000800L

//-----------------------------------------------------------------------------
// Struktura archivu
//-----------------------------------------------------------------------------

// narazniky :

#define FL_ARCHIV_EMPTY        0xFE
#define FL_ARCHIV_FULL         0xFF

typedef struct {
   word       CisloDneVykrmu;          // Cislo dne od pocatku vykrmu
   TCas       Cas;                     // datum a cas porizeni
   TStatistic StatSamice;              // statistika / statistika samic
   THistogram HistSamice;              // histogram  / histogram  samic
   TStatistic StatSamci;               // statistika / statistika samcu
   THistogram HistSamci;               // histogram  / histogram  samcu
   word       VcerejsiPrumerSamice;    // Prumerna vcerejsi hmotnost samic (pro vypocet daily gain)
   word       VcerejsiPrumerSamci;     // Prumerna vcerejsi hmotnost samcu
   word       NormovanaHmotnostSamice; // Normovana hmotnost samic pro tento den
   word       NormovanaHmotnostSamci;  // Normovana hmotnost samcu pro tento den
   byte       PouzitaRealnaUniformita; // YES/NO, zda se pouzil presny vypocet uniformity, ktery je v promennych RealnUniformitaSamice a RealnUniformitaSamci
   byte       RealnaUniformitaSamice;  // Uniformita samic vypoctena presne z jednotlivych vzorku
   byte       RealnaUniformitaSamci;   // Uniformita samcu vypoctena presne z jednotlivych vzorku
} TArchiv;        // 215 bajtu - pozor, pri prekroceni 255 bajtu predefinovat promennou v SaveContext() a LoadContext()

//-----------------------------------------------------------------------------
// Zaznam vzorku
//-----------------------------------------------------------------------------

// Naraznikove znaky :

#define LG_SAMPLE_EMPTY        0x7F     // nenaplnene FIFO
#define LG_SAMPLE_FULL         0xFF     // naplnene FIFO
#define LG_SAMPLE_VALUE        0x20     // zaznam hodnoty
#define LG_SAMPLE_GENDER       0x10     // bit pohlavi
#define LG_SAMPLE_WEIGHT_MSB   0x01     // 17. bit hmotnosti


#define LG_SAMPLE_MASK_WEIGHT  0x01FFFF  // Hmotnost je ulozena ve spodnich 17bit, MSB byte je flag (zabira 1 spodni bit z flagu)
#define LG_SAMPLE_MASK_HOUR    0x00001F  // Hodina je ulozena ve spodnich 5 bitech

typedef struct {
   byte  Flag;        // naraznik
   word  Value;       // vaha nebo hodina (vaha je na 17 bitu)
} TLoggerSample;      // 3 bajty

// zaznam jednotlivych vazeni :
#define FL_LOGGER_SAMPLES      1801L
#define FL_LOGGER_SAMPLE_SIZE  sizeof( TLoggerSample)                       // 3 bajty

// struktura dne (jen pro pro Builder a vypocet velikosti) :
typedef struct {
   TArchiv       Archiv;                                                    // 215  bytu
   TLoggerSample Samples[ FL_LOGGER_SAMPLES];                               // 5403 bytu
} TArchivDailyInfo;   // 5618 bytu

//-----------------------------------------------------------------------------
// Struktura FLASH
//-----------------------------------------------------------------------------

// rozdeleni pameti na sekce
// U AT45BD161B mam k dispozici 4096 x 512 = 2 097 152 bajtu

// konfiguracni sekce :
#define FL_CONFIG_BASE         0L
#define FL_CONFIG_SIZE         4096L

// archiv statistik & histogramu :
#define FL_ARCHIV_BASE         (FL_CONFIG_BASE + FL_CONFIG_SIZE)
#define FL_ARCHIV_DAY_SIZE     sizeof( TArchivDailyInfo)                    // velikost polozky
#define FL_ARCHIV_DAYS         371L                                         // pocet polozek
#define FL_ARCHIV_SIZE         (FL_ARCHIV_DAY_SIZE * FL_ARCHIV_DAYS)        // celkem bytu (2084278)


// struktura konfiguracni sekce :
typedef struct {
  TKonfigurace Konfigurace;            // Konfigurace
  THejna       Hejna;                  // Definice jednotlivych hejn
  TKalibrace   Kalibrace;              // Kalibrace zkopirovana z interni EEPROM
} TConfigSection; // 2768 bajtu

//-----------------------------------------------------------------------------
// Struktura interni EEPROM
//-----------------------------------------------------------------------------


#define IEP_SPARE (IEP_SIZE - sizeof(TKalibrace))

typedef struct {
  TKalibrace    Kalibrace;             // Kalibrace vah
  byte          Volne[IEP_SPARE];
} TIntEEPROM;


//-----------------------------------------------------------------------------
#endif
