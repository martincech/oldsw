//*****************************************************************************
//
//    Histogr.h   -  Histogram calculations
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Histogr_H__
   #define __Histogr_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

typedef struct {
   THistogramValue Center;                  // Stredni hodnota, kolem ktere se histogram vytvari - je treba zadat pri inicializaci
   THistogramValue Step;                    // Krok histogramu - je treba zadat pri inicializaci
   THistogramCount Slot[ HISTOGRAM_SLOTS];
} THistogram;   // 88 bajtu

void HistogramClear( THistogram __xdata__ *Histogram);
// Mazani histogramu

void HistogramAdd( THistogram __xdata__ *Histogram, THistogramValue Value);
// Prida hodnotu do histogramu

THistogramValue HistogramGetDenominator(THistogram __xdata__ *Histogram, THistogramValue MaxValue);
// Vrati cislo, kterym je treba podelit vsechny sloupce, aby se histogram normoval na maximalni hodnotu <MaxValue>

void HistogramSetStep(THistogram __xdata__ *Histogram, byte Range);
// Vypocte a ulozi do zadaneho histogramu krok podle nastaveneho stredu a rozsahu. Rozsah se zadava v +-%, tj. Range = 10 znamena +-10% okolo stredu.

TYesNo HistogramEmpty(THistogram __xdata__ *Histogram);
// Pokud je histogram prazdny, vrati YES

#endif
