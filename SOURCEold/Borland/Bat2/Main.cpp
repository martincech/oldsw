//******************************************************************************
//
//   Main.cpp  Bat2 Main Frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "../Unisys/uni.h"

#include <stdio.h>
#include "Main.h"
#include "ReadProgress.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;


//******************************************************************************
//  Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   new TWorkplace;
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);        // CRT emulator
   Logger = new TCrtLogger( Crt);       // monitor komunikace
   Workplace->Initialize();             // inicializace portu
   EnableDump( 0);                      // nastav monitor komunikace
   MainPort->ItemIndex = MainPort->Items->IndexOf( Workplace->Adapter->Port->Name);
} // Create

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Crt;
   delete Logger;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Port change
//******************************************************************************

void __fastcall TMainForm::MainPortChange(TObject *Sender)
{
   Crt->Clear();
   TString  PortName = MainPort->Text;
   if( !Workplace->SetPort( PortName)){
      Crt->printf( "Unable open %s adapter\n", PortName.c_str());
      return;
   }
   Crt->printf( "Open %s\n", PortName.c_str());
} // MainPortChange

//******************************************************************************
// Hexa vypis komunikace
//******************************************************************************

void __fastcall TMainForm::EnableDump(TObject *Sender)
{
   if( BtnLogger->Checked){
      Workplace->Adapter->Logger = Logger;
   } else {
      Workplace->Adapter->Logger = 0;
   }
} // EnableDump

//---- ADAPTER -----------------------------------------------------------------

//******************************************************************************
// Insert
//******************************************************************************

void __fastcall TMainForm::Insert(TObject *Sender)
{
   if( !Workplace->Adapter->ModuleInserted()){
      Crt->printf( "Module ERR\n");
      return;
   }
   Crt->printf( "Module OK\n");
} // Insert

//******************************************************************************
// Version
//******************************************************************************

void __fastcall TMainForm::GetVersion(TObject *Sender)
{
int Version;

   if( !Workplace->Adapter->GetVersion( Version)){
      Crt->printf( "ERR\n");
      return;
   }
   Crt->printf( "Version %02d.%02d\n", Version >> 8, Version & 0xFF);
} // GetVersion

//******************************************************************************
// Read
//******************************************************************************

void __fastcall TMainForm::Read(TObject *Sender)
{
int  Address;
byte Data;

   sscanf( MAddress->Text.c_str(), "%X", &Address);
   if( !Workplace->Adapter->ReadByte( Address, Data)){
      Crt->printf( "ERR\n");
      return;
   }
   MData->Text = IntToHex( Data, 2);
} // Read

//******************************************************************************
// Write
//******************************************************************************

void __fastcall TMainForm::Write(TObject *Sender)
{
int  Address;
byte Data;

   sscanf( MAddress->Text.c_str(), "%X", &Address);
   sscanf( MData->Text.c_str(),    "%X", &Data);
   if( !Workplace->Adapter->WriteByte( Address, Data)){
      Crt->printf( "ERR\n");
      return;
   }
   Crt->printf( "OK\n");
} // Write

//******************************************************************************
// Read Block
//******************************************************************************

void __fastcall TMainForm::ReadBlock(TObject *Sender)
{
int  Address;
int  Size;
byte Data[ CTECKA_MAX_PACKET];

   sscanf( MAddress->Text.c_str(), "%X", &Address);
   sscanf( MSize->Text.c_str(),    "%d", &Size);
   if( Size <= 0 || Size > CTECKA_MAX_PACKET){
      Crt->printf( "Wrong Size\n");
      return;
   }
   if( !Workplace->Adapter->ReadData( Address, Size, Data)){
      Crt->printf( "ERR\n");
      return;
   }
   if( BtnAutoIncrement->Checked){
      Address += Size;
      MAddress->Text = IntToHex( (int)Address, 8);
   }
   Crt->printf( "OK\n");
} // ReadBlock

//---- MODUL -------------------------------------------------------------------

//******************************************************************************
// Adapter
//******************************************************************************

void __fastcall TMainForm::CheckAdapter(TObject *Sender)
{
   if( !Workplace->Module->CheckAdapter()){
      Crt->printf( "ERR\n");
      return;
   }
   Crt->printf( "OK\n");
} // CheckAdapter

//******************************************************************************
// Modul
//******************************************************************************

void __fastcall TMainForm::CheckModule(TObject *Sender)
{
   if( !Workplace->Module->CheckModule()){
      Crt->printf( "ERR\n");
      return;
   }
   Crt->printf( "OK\n");
} // CheckModule

//******************************************************************************
// Konfigurace
//******************************************************************************

void __fastcall TMainForm::ReadConfig(TObject *Sender)
{
   if( !Workplace->Module->ReadConfigSection()){
      Crt->printf( "ERR\n");
      return;
   }
   Crt->printf( "Verze %04X\n",  GetWord( Workplace->Module->ConfigSection->Konfigurace.Verze));
   Crt->printf( "Id.Cislo %d\n", Workplace->Module->ConfigSection->Konfigurace.IdentifikacniCislo);
} // ReadConfig

//******************************************************************************
// Pamet
//******************************************************************************

void __fastcall TMainForm::ReadMemory(TObject *Sender)
{
   // predame rizeni do dialogu :
   if( !ReadProgressForm->Execute( this)){
      Crt->printf( "ERR\n");
      return;
   }
   Crt->printf( "OK\n");
} // ReadMemory

//******************************************************************************
// Save
//******************************************************************************

void __fastcall TMainForm::Save(TObject *Sender)
{
   Workplace->Module->Save( "data.bin");
   Crt->printf( "OK\n");
} // Save

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::Load(TObject *Sender)
{
   if( !Workplace->Module->Load( "data.bin")){
      Crt->printf( "ERR\n");
   }
   Crt->printf( "Verze %04X\n",  GetWord( Workplace->Module->ConfigSection->Konfigurace.Verze));
   Crt->printf( "Id.Cislo %d\n", Workplace->Module->ConfigSection->Konfigurace.IdentifikacniCislo);
} // Load

//******************************************************************************
// Pocet prvku
//******************************************************************************

void __fastcall TMainForm::CountItems(TObject *Sender)
// Testovani pruchodu pres databaze
{
   TArchiv *Archiv;
   Workplace->Module->ArchivFirst();
   int Count = 0;
   while( 1){
      Archiv = Workplace->Module->ArchivNext();
      if( !Archiv){
         break;
      }
      Count++;
   }
   Crt->printf( "Archiv count %d\n", Count);
   // pocet vzorku ve dnech :
   TLoggerSample *Sample;
   Workplace->Module->ArchivFirst();
   while( 1){
      Archiv = Workplace->Module->ArchivNext();
      if( !Archiv){
         break;
      }
      // pocet vzorku :
      Workplace->Module->LoggerFirstSample();
      int SamplesCount = 0;
      while( 1){
         Sample = Workplace->Module->LoggerNextSample();
         if( !Sample){
            break;
         }
         SamplesCount++;
      }
      Crt->printf( "Day %d.%d.%d : samples count %d\n", Archiv->Cas.Day,
                                                        Archiv->Cas.Month,
                                                        GetWord(Archiv->Cas.Year),
                                                        SamplesCount);
   }
} // CountItems

//******************************************************************************
// Demo konverze
//******************************************************************************

void __fastcall TMainForm::Conversion(TObject *Sender)
// Testovani konverznich rutin
{
   // string :
   Crt->printf( "Hejno :\n");
   THejno *Hejno = &Workplace->Module->ConfigSection->Hejna[ 0];
   Crt->printf( "  Cislo : %d\n", Hejno->Zahlavi.Cislo);
   Crt->printf( "  Nazev : %s\n", GetString( Hejno->Zahlavi.Nazev, MAX_DELKA_NAZVU_HEJNA).c_str());
   TArchiv *Archiv;
   Workplace->Module->ArchivFirst();
   for( int i = 0; i < 3; i++){
      Archiv = Workplace->Module->ArchivNext();
      if( !Archiv){
         return;
      }
      // datum :
      Crt->printf( "Archiv :\n");
      Crt->printf( "   Cislo dne : %d\n", GetWord( Archiv->CisloDneVykrmu));
      Crt->printf( "   Datum : %s\n",
                   GetDateTime( Archiv->Cas).DateTimeString().c_str());
      // float :
      Crt->printf( "   Stat.Count  %d\n", GetWord(  Archiv->StatSamice.Count));
      Crt->printf( "   Stat.XSuma  %f\n", GetFloat( Archiv->StatSamice.XSuma));
      Crt->printf( "   Stat.X2Suma %f\n", GetFloat( Archiv->StatSamice.X2Suma));
   }
} // Conversion

