object MainForm: TMainForm
  Left = 330
  Top = 270
  Width = 870
  Height = 640
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnDestroy = Destroy
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 726
    Top = 16
    Width = 25
    Height = 13
    Caption = 'Port :'
  end
  object Label2: TLabel
    Left = 528
    Top = 24
    Width = 44
    Height = 13
    Caption = 'Address :'
  end
  object Label3: TLabel
    Left = 528
    Top = 56
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object Label4: TLabel
    Left = 528
    Top = 88
    Width = 26
    Height = 13
    Caption = 'Size :'
  end
  object LblReadProgress: TLabel
    Left = 456
    Top = 8
    Width = 35
    Height = 13
    Caption = '100.0%'
    Visible = False
  end
  object MainMemo: TMemo
    Left = 8
    Top = 131
    Width = 849
    Height = 477
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 0
  end
  object MainPort: TComboBox
    Left = 758
    Top = 13
    Width = 98
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = MainPortChange
    Items.Strings = (
      'USB'
      'COM1'
      'COM2'
      'COM3'
      'COM4'
      'COM5'
      'COM6'
      'COM7'
      'COM8'
      'COM9'
      'COM10')
  end
  object BtnIsInsert: TButton
    Left = 8
    Top = 32
    Width = 75
    Height = 17
    Caption = 'Insert'
    TabOrder = 2
    OnClick = Insert
  end
  object MAddress: TEdit
    Left = 592
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '00000000'
  end
  object MData: TEdit
    Left = 592
    Top = 48
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'AA'
  end
  object BtnGetVersion: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 17
    Caption = 'Get Version'
    TabOrder = 5
    OnClick = GetVersion
  end
  object BtnRead: TButton
    Left = 8
    Top = 56
    Width = 75
    Height = 17
    Caption = 'Read'
    TabOrder = 6
    OnClick = Read
  end
  object BtnWrite: TButton
    Left = 8
    Top = 80
    Width = 75
    Height = 17
    Caption = 'Write'
    TabOrder = 7
    OnClick = Write
  end
  object BtnReadBlock: TButton
    Left = 8
    Top = 104
    Width = 75
    Height = 17
    Caption = 'Read Block'
    TabOrder = 8
    OnClick = ReadBlock
  end
  object MSize: TEdit
    Left = 592
    Top = 80
    Width = 121
    Height = 21
    TabOrder = 9
    Text = '128'
  end
  object BtnAutoIncrement: TCheckBox
    Left = 744
    Top = 80
    Width = 97
    Height = 17
    Caption = 'Auto Increment'
    Checked = True
    State = cbChecked
    TabOrder = 10
  end
  object ReadProgress: TProgressBar
    Left = 296
    Top = 8
    Width = 150
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 11
    Visible = False
  end
  object BtnCheckAdapter: TButton
    Left = 104
    Top = 8
    Width = 73
    Height = 17
    Caption = 'Check Adapter'
    TabOrder = 12
    OnClick = CheckAdapter
  end
  object BtnCheckModule: TButton
    Left = 104
    Top = 32
    Width = 75
    Height = 17
    Caption = 'Check Module'
    TabOrder = 13
    OnClick = CheckModule
  end
  object BtnReadMemory: TButton
    Left = 104
    Top = 80
    Width = 73
    Height = 17
    Caption = 'Read Memory'
    TabOrder = 14
    OnClick = ReadMemory
  end
  object BtnLogger: TCheckBox
    Left = 744
    Top = 104
    Width = 97
    Height = 17
    Caption = 'Logger'
    Checked = True
    State = cbChecked
    TabOrder = 15
    OnClick = EnableDump
  end
  object BtnReadConfig: TButton
    Left = 104
    Top = 56
    Width = 75
    Height = 17
    Caption = 'Read Config'
    TabOrder = 16
    OnClick = ReadConfig
  end
  object BtnSave: TButton
    Left = 224
    Top = 56
    Width = 75
    Height = 17
    Caption = 'Save'
    TabOrder = 17
    OnClick = Save
  end
  object BtnLoad: TButton
    Left = 224
    Top = 80
    Width = 75
    Height = 17
    Caption = 'Load'
    TabOrder = 18
    OnClick = Load
  end
  object BtnCount: TButton
    Left = 344
    Top = 56
    Width = 75
    Height = 17
    Caption = 'Count items'
    TabOrder = 19
    OnClick = CountItems
  end
  object BtnConversion: TButton
    Left = 344
    Top = 80
    Width = 75
    Height = 17
    Caption = 'Conversion'
    TabOrder = 20
    OnClick = Conversion
  end
end
