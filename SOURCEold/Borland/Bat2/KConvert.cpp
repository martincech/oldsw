//******************************************************************************
//
//   KConvert.cpp  Keil data conversions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "KConvert.h"

//******************************************************************************
// Word
//******************************************************************************

word GetWord(  word X)
{
TDataConvertor dc;
byte           tmp;

   dc.w = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 1];
   dc.array[ 1] = tmp;
   return( dc.w);
} // GetWord

//******************************************************************************
// Int16
//******************************************************************************

int16 GetInt(   int16 X)
{
TDataConvertor dc;
byte           tmp;

   dc.w = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 1];
   dc.array[ 1] = tmp;
   return( (int16)dc.w);
} // GetInt

//******************************************************************************
// Dword
//******************************************************************************

dword GetDword( dword X)
{
TDataConvertor dc;
byte           tmp;

   dc.dw = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 3];
   dc.array[ 3] = tmp;
   tmp = dc.array[ 1];
   dc.array[ 1] = dc.array[ 2];
   dc.array[ 2] = tmp;
   return( dc.dw);
} // GetDword

//******************************************************************************
// Long32
//******************************************************************************

long32 GetLong(  long32 X)
{
TDataConvertor dc;
byte           tmp;

   dc.dw = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 3];
   dc.array[ 3] = tmp;
   tmp = dc.array[ 1];
   dc.array[ 1] = dc.array[ 2];
   dc.array[ 2] = tmp;
   return( (long32)dc.dw);
} // GetLong

//******************************************************************************
// Float
//******************************************************************************

float  GetFloat( float X)
{
TDataConvertor dc;
byte           tmp;

   dc.f = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 3];
   dc.array[ 3] = tmp;
   tmp = dc.array[ 1];
   dc.array[ 1] = dc.array[ 2];
   dc.array[ 2] = tmp;
   return( dc.f);
} // GetFloat

//******************************************************************************
// DateTime
//******************************************************************************

TDateTime GetDateTime( TCas X)
{
TDateTime Dt;
   Dt = 0.0;
   try {
      Dt = TDateTime( (word)GetWord(X.Year), (word)X.Month, (word)X.Day) +
           TDateTime( (word)X.Hour, (word)X.Min, (word)0, 0);
   } catch(...){
   }
   return( Dt);
} // GetDateTime

TCas PutDateTime( TDateTime X)
{
TCas Cas;
unsigned short Year, Month, Day, Hour, Min, Sec, Msec;

   X.DecodeDate( &Year, &Month, &Day);
   X.DecodeTime( &Hour, &Min, &Sec, &Msec);
   Cas.Min   = (byte)Min;
   Cas.Hour  = (byte)Hour;
   Cas.Day   = (byte)Day;
   Cas.Month = (byte)Month;
   Cas.Year  = (word)PutWord( Year);
   return( Cas);
} // PutDateTime

//******************************************************************************
// String
//******************************************************************************

AnsiString GetString( byte *S, int Length)
{
AnsiString As;

   if( Length == 0){
      // terminator 0
      As = AnsiString( (char *)S);
   } else {
      // omezeni delkou
      As = AnsiString( (char *)S, (unsigned char)Length);
   }
   As.TrimRight();         // odstraneni pravostrannych mezer
   return( As);
} // GetString

void PutString( byte *S, AnsiString X, int Length)
{
   if( Length == 0){
      IERROR;
   }
   strnset( S, ' ', Length);           // vymezerovani
   if( X.Length() < Length){
      Length = X.Length();             // kratsi retezec
   }
   strncpy( S, X.c_str(), Length);     // jen vyznamne znaky
} // PutString

