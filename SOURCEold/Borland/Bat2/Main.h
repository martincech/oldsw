//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "Workplace.h"
#include "../Crt/Crt.h"
#include "../Serial/CrtLogger.h"
#include <ComCtrls.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TLabel *Label1;
        TComboBox *MainPort;
        TButton *BtnIsInsert;
        TEdit *MAddress;
        TLabel *Label2;
        TEdit *MData;
        TLabel *Label3;
        TButton *BtnGetVersion;
        TButton *BtnRead;
        TButton *BtnWrite;
        TButton *BtnReadBlock;
        TLabel *Label4;
        TEdit *MSize;
        TCheckBox *BtnAutoIncrement;
        TProgressBar *ReadProgress;
        TButton *BtnCheckAdapter;
        TButton *BtnCheckModule;
        TButton *BtnReadMemory;
        TLabel *LblReadProgress;
        TCheckBox *BtnLogger;
        TButton *BtnReadConfig;
        TButton *BtnSave;
        TButton *BtnLoad;
        TButton *BtnCount;
        TButton *BtnConversion;
        void __fastcall Create(TObject *Sender);
        void __fastcall Destroy(TObject *Sender);
        void __fastcall MainPortChange(TObject *Sender);
        void __fastcall MemoResize(TObject *Sender);
        void __fastcall Insert(TObject *Sender);
        void __fastcall GetVersion(TObject *Sender);
        void __fastcall Read(TObject *Sender);
        void __fastcall Write(TObject *Sender);
        void __fastcall ReadBlock(TObject *Sender);
        void __fastcall CheckAdapter(TObject *Sender);
        void __fastcall CheckModule(TObject *Sender);
        void __fastcall ReadMemory(TObject *Sender);
        void __fastcall EnableDump(TObject *Sender);
        void __fastcall ReadConfig(TObject *Sender);
        void __fastcall Save(TObject *Sender);
        void __fastcall Load(TObject *Sender);
        void __fastcall CountItems(TObject *Sender);
        void __fastcall Conversion(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
private:	// User declarations
   TCrt          *Crt;
   TLogger       *Logger;
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
