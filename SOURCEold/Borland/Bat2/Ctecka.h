//*****************************************************************************
//
//    Ctecka.h - Ctecka flash modulu
//    Version 2.0, (c) Petr Veit, VymOs
//
//*****************************************************************************

#ifndef __Ctecka_H__
   #define __Ctecka_H__

// Pozor, soubor by mel slouzit i k prekladu v prostredi C++ Builder
// je treba jen prehazet poradi bytu v int16/word a long32/dword

#ifndef __Uni_H__
   #include "Uni.h"     // zakladni datove typy
#endif

#ifdef __C51__
   // jen pro Keil
   typedef int  int16;
   typedef long long32;
#endif

//-----------------------------------------------------------------------------
// Obecne definice
//-----------------------------------------------------------------------------

#define CTECKA_VERZE        0x0200         // verze SW
#define CTECKA_MAX_PACKET   256            // max. delka paketu dat

// Prikazy, ktere posila PC do ctecky
#define CMD_VERSION     0x10           // Cislo verze
#define CMD_READ        0x20           // Cteni bytu
#define CMD_WRITE       0x30           // Zapis bytu
#define CMD_INSERT      0x40           // Pritomnost modulu
#define CMD_INIT        0x50           // Inicializace
#define CMD_BLOCK_READ  0x60           // Cteni bloku bytu

// Odpovedi
#define CMD_ACK         0x02           // Kladne potvrzeni
#define CMD_NAK         0x03           // Zaporne potvrzeni

//-----------------------------------------------------------------------------
// Prikazy
//-----------------------------------------------------------------------------

// CMD_VERSION :
// jen prikaz

// CMD_READ :
typedef struct {
   byte  Cmd;
   dword Address;
} TCmdRead;

// CMD_WRITE :
typedef struct {
   byte  Cmd;
   dword Address;
   byte  Data;
} TCmdWrite;

// CMD_INSERT :
// jen prikaz

// CMD_INIT :
// jen prikaz

// CMD_BLOCK_READ :
typedef struct {
   byte  Cmd;
   dword Address;
   word  Length;
} TCmdBlockRead;

// union prikazu :

typedef union {
   byte          Cmd;           // jednoduchy prikaz
   byte          Buffer[ 1];    // pristup po bytech
   TCmdRead      Read;          // cteni
   TCmdWrite     Write;         // zapis
   TCmdBlockRead BlockRead;     // blokove cteni
} TCmdUnion;

//-----------------------------------------------------------------------------
// Odpovedi
//-----------------------------------------------------------------------------

// CMD_VERSION :
typedef struct {
   word Version;
} TReplyVersion;

// CMD_READ :
typedef struct {
   byte Data;
} TReplyRead;

// CMD_WRITE :
// jen ACK

// CMD_INSERT :
// jen ACK

// CMD_INIT :
// jen ACK

// CMD_BLOCK_READ :
typedef struct {
   byte Ack;                        // zahlavi dat CMD_ACK
   byte Data[ CTECKA_MAX_PACKET];   // pole promenne delky
} TReplyBlockRead;

// union odpovedi :

typedef union {
   byte Ack;                        // jednoducha odpoved
   byte Buffer[ 1];                 // pristup po bytech
   TReplyVersion   Version;         // cislo verze
   TReplyRead      Read;            // cteni bytu
   TReplyBlockRead BlockRead;       // cteni bloku
} TReplyUnion;

//-----------------------------------------------------------------------------
#endif // __Ctecka_H__
