//******************************************************************************
//
//   MFlash.h     Memory module Flash decoder
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MFlashH
#define MFlashH

#ifndef MReaderH
   #include "MReader.h"
#endif

#pragma pack( push, 1)                  // byte alignment
   #include "Bat2.h"                    // flash data definition
#pragma pack( pop)                      // original alignment

#ifndef KConvertH
   #include "KConvert.h"
#endif

//******************************************************************************
//  TMFlash
//******************************************************************************

class TMFlash {
public :
   TMFlash();
   // Konstruktor
   ~TMFlash();
   // Destruktor

//-- Kontrola pripojeni :
   bool CheckAdapter();
   // Kontroluje adapter
   bool CheckModule();
   // Otestuje pritomnost modulu

//-- Cteni zahlavi modulu :
   bool ReadConfigSection();
   // Nacte konfiguracni sekci modulu

//-- Uplne cteni dat modulu :
   bool ReadData();
   // Zahaji cteni dat, vraci false pri chybe
   bool ReadPacket( bool &Done);
   // Precte data, vraci false pri chybe. Nastavi <Done> je-li nacteno vse
   void ReadFinalize();
   // Uzavre proceduru cteni dat

//-- Pohyb v databazi zaznamu :
   void ArchivFirst();
   // Nastavi archiv pred prvni zaznam
   TArchiv *ArchivNext();
   // Nasledujici zaznam (0=posledni)
   void LoggerFirstSample();
   // Nastavi logger pred prvni vzorek
   TLoggerSample *LoggerNextSample();
   // Nastavi logger na dalsi vzorek (0=posledni)

//-- Hardcopy modulu :
   bool Load( AnsiString File);
   // Nacte data ze souboru
   void Save( AnsiString File);
   // Ulozi data do souboru

//-- Udaje o adapteru :
   __property TMReader         *Adapter         = {read=FAdapter,write=FAdapter};
   __property int               AdapterVersion  = {read=FAdapterVersion};
   __property bool              ModuleInserted  = {read=FModuleInserted};
   __property int               ReadProgress    = {read=GetReadProgress};

//-- Data modulu :
   __property TConfigSection   *ConfigSection   = {read=FConfigSection};
   __property TArchivDailyInfo *Archiv          = {read=FArchiv};

//---------------------------------------------------------------------------
protected :
   TMReader *FAdapter;               // adapter (pri praci se souborem muze byt 0)
   int       FAdapterVersion;        // verze adapteru
   bool      FModuleInserted;        // pripojen modul
   byte     *FRawData;               // surova Flash Data
   int       FReadOffset;            // ofset ctenych dat
   // ukazatele do RawData :
   TConfigSection   *FConfigSection; // konfiguracni sekce
   TArchivDailyInfo *FArchiv;        // pole archivu/statistiky

   // ukazatele do databaze :
   int               FCurArchiv;     // pracovni zaznam archivu
   int               FLastArchiv;    // marker
   TArchivDailyInfo *FArchivDay;     // pracovni polozka dne
   int               FCurSample;     // pracovni zaznam vzorku
   int               FLastSample;    // marker

   int GetReadProgress();
   // Vraci pocet promile nactenych dat
}; // TMFlash

//---------------------------------------------------------------------------
#endif
