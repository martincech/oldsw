//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "c51/MBPkt.h"        // Meze adres

#include "ModbusEditScale.h"
#include "Konstanty.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TModbusEditScaleForm *ModbusEditScaleForm;
//---------------------------------------------------------------------------
__fastcall TModbusEditScaleForm::TModbusEditScaleForm(TComponent* Owner)
        : TForm(Owner)
{
  // Naplnim comboboxy
  AddressComboBox->Clear();
  for (int i = MB_MIN_ADDRESS; i <= MB_MAX_ADDRESS; i++) {
    AddressComboBox->Items->Add(AnsiString(i));
  }
  PortComboBox->Clear();
  for (int i = 1; i <= 20; i++) {
    PortComboBox->Items->Add(LoadStr(STR_RS485_PORT_PREFIX) + AnsiString(i));
  }
}
//---------------------------------------------------------------------------
void __fastcall TModbusEditScaleForm::FormShow(TObject *Sender)
{
  AddressComboBox->SetFocus();        
}
//---------------------------------------------------------------------------
