object OProgramuForm: TOProgramuForm
  Left = 571
  Top = 355
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'About'
  ClientHeight = 242
  ClientWidth = 286
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object NameLabel: TLabel
    Left = 16
    Top = 16
    Width = 64
    Height = 13
    Caption = 'NameLabel'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object VerzeLabel: TLabel
    Left = 105
    Top = 16
    Width = 53
    Height = 13
    Caption = 'VerzeLabel'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 176
    Width = 257
    Height = 9
    Shape = bsBottomLine
  end
  object LogoImage: TImage
    Left = 16
    Top = 50
    Width = 70
    Height = 70
  end
  object Button1: TButton
    Left = 198
    Top = 200
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CompanyMemo: TMemo
    Left = 104
    Top = 48
    Width = 169
    Height = 121
    BorderStyle = bsNone
    Color = clBtnFace
    Lines.Strings = (
      'VEIT Electronics'
      'Karlova 49'
      '614 00  Brno'
      'Czech republic'
      'tel: +420 545 235 252'
      'fax: +420 545 235 256'
      'email: veit@veit.cz'
      'www.veit.cz')
    TabOrder = 1
  end
end
