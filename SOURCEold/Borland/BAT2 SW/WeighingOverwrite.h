//---------------------------------------------------------------------------

#ifndef WeighingOverwriteH
#define WeighingOverwriteH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TWeighingOverwriteForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *ScaleLabel;
        TLabel *DayLabel;
        TLabel *DateLabel;
        TGroupBox *GroupBox1;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *OldFemaleCountLabel;
        TLabel *OldFemaleAverageLabel;
        TLabel *OldFemaleGainLabel;
        TLabel *OldFemaleSigmaLabel;
        TLabel *OldFemaleCvLabel;
        TLabel *OldFemaleUniLabel;
        TLabel *InsertedLabel;
        TLabel *GSMLabel;
        TButton *Button1;
        TButton *Button2;
        TLabel *OldMaleCountLabel;
        TLabel *OldMaleAverageLabel;
        TLabel *OldMaleGainLabel;
        TLabel *OldMaleSigmaLabel;
        TLabel *OldMaleCvLabel;
        TLabel *OldMaleUniLabel;
        TSpeedButton *SpeedButton1;
        TSpeedButton *SpeedButton2;
        TShape *Shape1;
        TShape *Shape2;
        TShape *Shape3;
        TGroupBox *GroupBox2;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label24;
        TLabel *NewFemaleCountLabel;
        TLabel *NewFemaleAverageLabel;
        TLabel *NewFemaleGainLabel;
        TLabel *NewFemaleSigmaLabel;
        TLabel *NewFemaleCvLabel;
        TLabel *NewFemaleUniLabel;
        TLabel *NewMaleCountLabel;
        TLabel *NewMaleAverageLabel;
        TLabel *NewMaleGainLabel;
        TLabel *NewMaleSigmaLabel;
        TLabel *NewMaleCvLabel;
        TLabel *NewMaleUniLabel;
        TSpeedButton *SpeedButton4;
        TShape *Shape4;
        TShape *Shape5;
        TShape *Shape6;
        TSpeedButton *SpeedButton10;
        TSpeedButton *SpeedButton3;
        TSpeedButton *SpeedButton5;
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TWeighingOverwriteForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TWeighingOverwriteForm *WeighingOverwriteForm;
//---------------------------------------------------------------------------
#endif
