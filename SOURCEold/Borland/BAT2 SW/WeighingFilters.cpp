//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "WeighingFilters.h"
#include "DM.h"
#include "WeighingFilter.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TWeighingFiltersForm *WeighingFiltersForm;


//---------------------------------------------------------------------------
// Ulozeni parametru filtru do tabulky
//---------------------------------------------------------------------------

bool TWeighingFiltersForm::CheckFilterInTable(String Filter) {
  // Zkontroluju, zda uz filtr v tabulce neexistuje. Pokud ano, dotaze se na prepis. Vraci true, pokud se novy filtr muze zapsat.
  // Tabulka Data->FiltersTable uz musi byt otevrena
  TLocateOptions LocateOptions;
  LocateOptions.Clear();
  LocateOptions << loCaseInsensitive;
  if (Data->FiltersTable->Locate("NAME", Filter, LocateOptions)) {
    // Existuje
    if (MessageBox(NULL, LoadStr(SMS_FILTER_OVERWRITE).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
      return false;   // Nechce smazat
    }
    Data->FiltersTable->Delete();     // Smazu
  }
  return true;  // Muze se ulozit novy filtr (bud neexistoval, nebo ano, ale smazal jsem ho)
}

void TWeighingFiltersForm::SaveFilter() {
  // Ulozi do jiz potevrene tabulky zadany filtr z okna WeighingFilterForm
  Data->FiltersTable->Edit();
  Data->FiltersTable->FieldByName("NAME")->AsString = WeighingFilterForm->NameEdit->Text;
  if (WeighingFilterForm->ScalesEdit->Text.IsEmpty()) {
    Data->FiltersTable->FieldByName("ID")->Clear();     // Prazdne
  } else {
    Data->FiltersTable->FieldByName("ID")->AsInteger = WeighingFilterForm->ScalesEdit->Text.ToInt();
  }
  if (WeighingFilterForm->FromCheckBox->Checked) {
    Data->FiltersTable->FieldByName("DATE_FROM")->AsDateTime = WeighingFilterForm->FromDateTimePicker->Date;
  } else {
    Data->FiltersTable->FieldByName("DATE_FROM")->Clear();  // Prazdne
  }
  if (WeighingFilterForm->TillCheckBox->Checked) {
    Data->FiltersTable->FieldByName("DATE_TILL")->AsDateTime = WeighingFilterForm->TillDateTimePicker->Date;
  } else {
    Data->FiltersTable->FieldByName("DATE_TILL")->Clear();  // Prazdne
  }
  Data->FiltersTable->FieldByName("NOTE")->AsString = WeighingFilterForm->NoteEdit->Text;
  Data->FiltersTable->Post();
}

//---------------------------------------------------------------------------
// Nacteni vsech filtru do seznamu
//---------------------------------------------------------------------------

void TWeighingFiltersForm::ReadFilters() {
  // Nacte vsechny filtry do seznamu
  ListBox->Clear();
  Data->FiltersTable->Open();
  try {
    for (Data->FiltersTable->First(); !Data->FiltersTable->Eof; Data->FiltersTable->Next()) {
      ListBox->Items->Add(Data->FiltersTable->FieldByName("NAME")->AsString);
    }
  } catch(...) {}
  Data->FiltersTable->Close();
}

//---------------------------------------------------------------------------
__fastcall TWeighingFiltersForm::TWeighingFiltersForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TWeighingFiltersForm::FormShow(TObject *Sender)
{
  ReadFilters();
  ListBox->SetFocus();
  if (WeighingFiltersForm->SelectedFilter != "") {
    // Chce, abych vybral zadany filtr
    int Index = ListBox->Items->IndexOf(SelectedFilter);
    if (Index >= 0) {
      ListBox->ItemIndex = Index;       // Zadany filtr v seznamu je, vyberu ho
    }
  }
}
//---------------------------------------------------------------------------
void __fastcall TWeighingFiltersForm::NewButtonClick(TObject *Sender)
{
  WeighingFilterForm->NameEdit->Text = "";
  WeighingFilterForm->NameEdit->Enabled = true;
  WeighingFilterForm->ScalesEdit->Text = "";
  WeighingFilterForm->FromDateTimePicker->Date = Date();
  WeighingFilterForm->FromCheckBox->Checked    = true;
  WeighingFilterForm->TillDateTimePicker->Date = Date();
  WeighingFilterForm->TillCheckBox->Checked    = true;
  WeighingFilterForm->NoteEdit->Text = "";
  if (WeighingFilterForm->ShowModal() !=mrOk) {
    return;
  }
  // Ulozim do tabulky
  Data->FiltersTable->Open();
  try {
    // Zkontroluju, zda uz filtr neexistuje
    if (!CheckFilterInTable(WeighingFilterForm->NameEdit->Text)) {
      throw 1;  // Uz existuje a nechce smazat
    }
    // Ulozim novy zaznam
    Data->FiltersTable->Append();
    SaveFilter();
  } catch(...) {}
  Data->FiltersTable->Close();
  ReadFilters();
  ListBox->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TWeighingFiltersForm::EditButtonClick(TObject *Sender)
{
  if (ListBox->ItemIndex < 0) {
    return;     // Neni nic vybrane
  }
  Data->FiltersTable->Open();
  try {
    TLocateOptions LocateOptions;
    LocateOptions.Clear();
    LocateOptions << loCaseInsensitive;
    if (!Data->FiltersTable->Locate("NAME", ListBox->Items->Strings[ListBox->ItemIndex], LocateOptions)) {
      throw 1;     // Nenasel jsem
    }
    // Zkopiruju udaje do okna
    WeighingFilterForm->NameEdit->Text = Data->FiltersTable->FieldByName("NAME")->AsString;
    WeighingFilterForm->NameEdit->Enabled = false;  // Jmeno nemuze upravovat
    WeighingFilterForm->ScalesEdit->Text = Data->FiltersTable->FieldByName("ID")->AsString;
    WeighingFilterForm->FromCheckBox->Checked = !Data->FiltersTable->FieldByName("DATE_FROM")->IsNull;
    if (Data->FiltersTable->FieldByName("DATE_FROM")->IsNull) {
      WeighingFilterForm->FromDateTimePicker->Date = Date();
    } else {
      WeighingFilterForm->FromDateTimePicker->Date = Data->FiltersTable->FieldByName("DATE_FROM")->AsDateTime;
    }
    WeighingFilterForm->TillCheckBox->Checked = !Data->FiltersTable->FieldByName("DATE_TILL")->IsNull;
    if (Data->FiltersTable->FieldByName("DATE_TILL")->IsNull) {
      WeighingFilterForm->TillDateTimePicker->Date = Date();
    } else {
      WeighingFilterForm->TillDateTimePicker->Date = Data->FiltersTable->FieldByName("DATE_TILL")->AsDateTime;
    }
    WeighingFilterForm->NoteEdit->Text = Data->FiltersTable->FieldByName("NOTE")->AsString;
    // Zobrazim okno
    if (WeighingFilterForm->ShowModal() != mrOk) {
      throw 1;
    }
    // Ulozim zmeny zpet do tabulky
    SaveFilter();
  } catch(...) {}
  Data->FiltersTable->Close();
  // Nemusim znovu nacitat vsechny filtry do seznamu, seznam se nezmenil
  ListBox->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TWeighingFiltersForm::DeleteButtonClick(TObject *Sender)
{
  if (ListBox->ItemIndex < 0) {
    return;     // Neni nic vybrane
  }
  if (MessageBox(NULL, LoadStr(SMS_FILTER_DELETE).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
    return;     // Nechce smazat
  }
  Data->FiltersTable->Open();
  try {
    TLocateOptions LocateOptions;
    LocateOptions.Clear();
    LocateOptions << loCaseInsensitive;
    if (!Data->FiltersTable->Locate("NAME", ListBox->Items->Strings[ListBox->ItemIndex], LocateOptions)) {
      throw 1;     // Nenasel jsem
    }
    Data->FiltersTable->Delete();
  } catch(...) {}
  Data->FiltersTable->Close();
  ReadFilters();
  ListBox->SetFocus();
}
//---------------------------------------------------------------------------
