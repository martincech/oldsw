//***************************************************************************
//
//   Export.cpp        Export to Excel
//   Version 1.0
//
//***************************************************************************

#ifndef ExportH
#define ExportH

//---------------------------------------------------------------------------

#include <Chart.hpp>
#include "Curve.h"


void ExportReport(TQuery *Query, bool BothGenders);
  // Exportuje report v <Query> do Excelu

void ExportSamples(TQuery *Query);
  // Exportuje vzorky v <Query> do Excelu

void ExportHistogram(TChart *Chart, bool BothGenders);
  // Exportuje histogram v <Chart> do Excelu

void ExportDayActivity(TChart *Chart, bool BothGenders);
  // Exportuje aktivitu v <Chart> do Excelu

void ExportTotalActivity(TChart *Chart, bool BothGenders);
  // Exportuje celkovou aktivitu v <Chart> do Excelu

void ExportGrowthCurve(TChart *Chart, bool BothGenders, TGrowthCurve *TheoreticalCurve);
  // Exportuje rustovou krivku v <Chart> do Excelu

void ExportOnline(TQuery *Query, int Min, int Max, int Average);
  // Exportuje online v <Query> do Excelu


#endif
