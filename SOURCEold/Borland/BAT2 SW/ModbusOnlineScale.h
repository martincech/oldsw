//***************************************************************************
//
//    ModbusOnlineScale.cpp - Scale in an online table
//    Version 1.0
//
//***************************************************************************

#ifndef MODBUSONLINESCALE_H
#define MODBUSONLINESCALE_H

#include "ModbusScale.h"

//---------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------

typedef enum {
  ONLINE_COMMAND_IDLE,
  ONLINE_COMMAND_READ_CONFIG,           // Nacte konfiguraci
  ONLINE_COMMAND_READ_DATETIME,         // Nacte datum a cas
  ONLINE_COMMAND_READ_PAUSE,            // Nacte pauzu vazeni
  ONLINE_COMMAND_READ_CURRENT_FLOCK,    // Nacte hejno, podle ktereho se vazi
  ONLINE_COMMAND_READ_TODAY,            // Nacte statistiku aktualniho dne vazeni
  _ONLINE_COMMAND_COUNT
} TModbusOnlineCommand;

typedef enum {
  ONLINE_EXECUTION_RUNNING,             // Nacitani dat jeste probiha
  ONLINE_EXECUTION_ERROR,               // Chyba komunikace, vaha neodpovida
  ONLINE_EXECUTION_DONE,                // Vsechna data nactena
  _ONLINE_EXECUTION_COUNT,
} TModbusOnlineExecution;

//---------------------------------------------------------------------------
// Vaha
//---------------------------------------------------------------------------

class TModbusOnlineScale : public TModbusScale {

private:
  bool Live;                            // Vaha zije (odpovedela na vsechny prikazy)
  TModbusOnlineCommand Command;         // Aktualni prikaz, ktery se provadi
  int ErrorCounter;                     // Pocitadlo chyb

  bool MoveToNextCommand();
  // Presune se na dalsi prikaz, pri prechodu z poseldniho prikazu zpet na prvni vrati true
  TModbusScaleResult ExecuteCommand();
  // Provede prikaz <Command>, vrati vysledek prikazu

public:
  TModbusOnlineScale();

  bool getLive() { return Live; }

  void Clear();
  // Vynuluje pracovni data

  void InitExecute();
  // Provede inicializaci pred zacatkem vykonavani prikazu

  TModbusOnlineExecution ExecuteNextCommand(int Repetitions);
  // Provede dalsi prikaz, po provedeni posledniho prikazu v rade vrati true

};

#endif // MODBUSONLINESCALE_H

