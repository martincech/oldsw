//----------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "VzorkyReport.h"
#include "DM.h"
//----------------------------------------------------------------------------
#pragma resource "*.dfm"
TVzorkyReportForm *VzorkyReportForm;
//----------------------------------------------------------------------------
__fastcall TVzorkyReportForm::TVzorkyReportForm(TComponent* Owner)
    : TQuickRep(Owner)
{
}
//----------------------------------------------------------------------------
void __fastcall TVzorkyReportForm::QRDBText8Print(TObject *sender,
      AnsiString &Value)
{
  if (Data->TiskQuery->FieldByName("GENDER")->AsBoolean == GENDER_FEMALE) {
    Value = LoadStr(SAMICE);
  } else {
    Value = LoadStr(SAMEC);
  }
}
//---------------------------------------------------------------------------
