//---------------------------------------------------------------------------

#ifndef Rs485ScalesH
#define Rs485ScalesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>

#include "ModbusScaleList.h"

//---------------------------------------------------------------------------
class TModbusScalesForm : public TForm
{
__published:	// IDE-managed Components
        TStringGrid *ScalesStringGrid;
        TButton *NewButton;
        TButton *EditButton;
        TButton *OkButton1;
        TButton *CancelButton;
        TButton *DeleteButton;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall NewButtonClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall EditButtonClick(TObject *Sender);
        void __fastcall OkButton1Click(TObject *Sender);
        void __fastcall ScalesStringGridDblClick(TObject *Sender);
        void __fastcall DeleteButtonClick(TObject *Sender);
        void __fastcall ScalesStringGridKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
        TModbusScaleList *ScaleList;

public:		// User declarations

        void ClearTable();
        void DisplayScaleList();

        __fastcall TModbusScalesForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusScalesForm *ModbusScalesForm;
//---------------------------------------------------------------------------
#endif
