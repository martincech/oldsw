//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RozsahOnline.h"
#include "Konstanty.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TRozsahOnlineForm *RozsahOnlineForm;


static bool KontrolaCisla(TEdit *Edit) {
  try {
    if (Edit->Text.ToInt() == 0) {
      throw 1;      // Nesmi zadat nulu
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    Edit->SetFocus();
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
__fastcall TRozsahOnlineForm::TRozsahOnlineForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TRozsahOnlineForm::OdEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //   Backspace      0   az   9
  //       |          |        |
  // 3.9.2001: Chtel natvrdo carku, i kdyz ve Win byla nastavena tecka a pak to blblo
//  if (Key!=8 && (Key<48 || Key>57) && Key!=DecimalSeparator) Key=0;  // Vyberu jen cisla a carku
  if (Key!=8 && (Key<48 || Key>57)) Key=0;  // Vyberu jen cisla
}
//---------------------------------------------------------------------------
void __fastcall TRozsahOnlineForm::FormShow(TObject *Sender)
{
  OdEdit->SetFocus();      
}
//---------------------------------------------------------------------------
void __fastcall TRozsahOnlineForm::Button1Click(TObject *Sender)
{
  // Kontrola zadanych cisel
  if (!KontrolaCisla(OdEdit)) {
    return;
  }
  if (!KontrolaCisla(DoEdit)) {
    return;
  }
  if (!KontrolaCisla(PrumerEdit)) {
    return;
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
