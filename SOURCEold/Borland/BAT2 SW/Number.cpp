//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Number.h"
#include "DM.h"
#include "NewNumber.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TNumberForm *NumberForm;




//---------------------------------------------------------------------------
// Nacteni seznamu cisel
//---------------------------------------------------------------------------

void TNumberForm::ReadNumbers() {
  // Nacte seznam cisel
  String Str;

  NumbersListBox->Clear();
  try {
    Data->NumbersTable->Open();
    Data->NumbersTable->First();
    while (!Data->NumbersTable->Eof) {
      Str = Data->NumbersTable->FieldByName("NUMBER")->AsString;
      if (Data->NumbersTable->FieldByName("NOTE")->AsString != "") {
        Str += " (" + Data->NumbersTable->FieldByName("NOTE")->AsString + ")"; // Pridam i komentar
      }
      NumbersListBox->Items->Add(Str);
      Data->NumbersTable->Next();
    }
  } catch(...) {}
  Data->NumbersTable->Close();
}

//---------------------------------------------------------------------------
// Dekodovani cisla ze seznamu
//---------------------------------------------------------------------------

String TNumberForm::DecodeNumber() {
  // Vrati cislo vybrane v seznamu
  String Str;
  Str = NumbersListBox->Items->Strings[NumbersListBox->ItemIndex];
  int Index = Str.Pos("(");
  if (Index >= 1) {
    return Str.SubString(1, Index - 2); // Obsahuje komentar
  } else {
    return Str;                         // Bez komentare (jen cislo)
  }
}



//---------------------------------------------------------------------------
__fastcall TNumberForm::TNumberForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TNumberForm::NumberEditKeyPress(TObject *Sender, char &Key)
{
  // Filtruju cislice (musim i backspace)
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57)) {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------
void __fastcall TNumberForm::FormShow(TObject *Sender)
{
  ReadNumbers();
  NumberEdit->Text = "";
  NumberEdit->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TNumberForm::OkButtonClick(TObject *Sender)
{
  if (NumberEdit->Text.IsEmpty()) {
    NumberEdit->SetFocus();
    return;
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TNumberForm::NumbersListBoxClick(TObject *Sender)
{
  if (NumbersListBox->ItemIndex < 0) {
    return;     // Neni nic vybrane
  }
  NumberEdit->Text = DecodeNumber();
}
//---------------------------------------------------------------------------
void __fastcall TNumberForm::NewButtonClick(TObject *Sender)
{
  if (NewNumberForm->ShowModal() != mrOk) {
    return;
  }
  // Zkontroluju, zda uz cislo v tabulce neni
  TLocateOptions LocateOptions;
  LocateOptions.Clear();
  LocateOptions << loCaseInsensitive;
  try {
    Data->NumbersTable->Open();
    if (Data->NumbersTable->Locate("NUMBER", NewNumberForm->NumberEdit->Text, LocateOptions)) {
      if (MessageBox(NULL, LoadStr(SMS_NUMBER_EXISTS).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
        throw 1;   // Nechce prepsat
      }
      Data->NumbersTable->Delete();     // Chce prepsat
    }
    Data->NumbersTable->Append();
    Data->NumbersTable->Edit();
    Data->NumbersTable->FieldByName("NUMBER")->AsString = NewNumberForm->NumberEdit->Text;
    Data->NumbersTable->FieldByName("NOTE")->AsString   = NewNumberForm->NoteEdit->Text;
    Data->NumbersTable->Post();
  } catch(...) {}
  Data->NumbersTable->Close();
  ReadNumbers();        // Nactu vsechan cisla do seznamu
}
//---------------------------------------------------------------------------
void __fastcall TNumberForm::DeleteButtonClick(TObject *Sender)
{
  if (NumbersListBox->ItemIndex < 0) {
    return;     // Neni nic vybrane
  }
  if (MessageBox(NULL, LoadStr(SMS_NUMBER_DELETE).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
    return;
  }
  // Najdu a smazu vybrane cislo
  TLocateOptions LocateOptions;
  LocateOptions.Clear();
  LocateOptions << loCaseInsensitive;
  try {
    Data->NumbersTable->Open();
    if (!Data->NumbersTable->Locate("NUMBER", DecodeNumber(), LocateOptions)) {
      throw 1;   // Nenasel jsem, to by nemelo nastat
    }
    Data->NumbersTable->Delete();     // Smazu zaznam
  } catch(...) {}
  Data->NumbersTable->Close();
  ReadNumbers();        // Nactu vsechan cisla do seznamu
}
//---------------------------------------------------------------------------
void __fastcall TNumberForm::NumbersListBoxDblClick(TObject *Sender)
{
  // Jakoby stisknul OK
  if (NumbersListBox->ItemIndex < 0) {
    return;     // Nevybral zadny modem
  }
  OkButtonClick(NULL);
}
//---------------------------------------------------------------------------

