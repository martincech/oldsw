object NewNumberForm: TNewNumberForm
  Left = 353
  Top = 212
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'New number'
  ClientHeight = 160
  ClientWidth = 201
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 40
    Height = 13
    Caption = 'Number:'
  end
  object Label2: TLabel
    Left = 16
    Top = 64
    Width = 26
    Height = 13
    Caption = 'Note:'
  end
  object NumberEdit: TEdit
    Left = 16
    Top = 32
    Width = 169
    Height = 21
    MaxLength = 15
    TabOrder = 0
    Text = 'NumberEdit'
    OnKeyPress = NumberEditKeyPress
  end
  object NoteEdit: TEdit
    Left = 16
    Top = 80
    Width = 169
    Height = 21
    MaxLength = 20
    TabOrder = 1
    Text = 'NoteEdit'
  end
  object Button1: TButton
    Left = 16
    Top = 120
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 110
    Top = 120
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
