//***************************************************************************
//
//   Print.cpp        Printing functions
//   Version 1.0
//
//***************************************************************************


#include <vcl.h>
#pragma hdrstop

#include "Print.h"
#include <Chart.hpp>
#include <Qrtee.hpp>
#include "ReportReport.h"
#include "DM.h"
#include "VzorkyReport.h"
#include "BargrafReport.h"
#include "Hlavni.h"
#include "GrafReport.h"
#include "OnlineGrafReport.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// Lokalni funkce
//---------------------------------------------------------------------------

static void NastavOsyGrafu(TChart *Chart1, TQRDBChart *Chart2);
// Nastavim osy z Chart1 do Chart2

//---------------------------------------------------------------------------
// Tisk reportu
//---------------------------------------------------------------------------

void PrintReport(bool BothGenders) {
  // Vytiskne report v Data->StatistikaVyberQuery
  // Nastavim query a sloupce (report se vyuziva i v databazi vazeni a tam se prehazuji sloupce)
  ReportReportForm->DataSet                    = Data->StatistikaVyberQuery;
  ReportReportForm->DayQRDBText->DataSet       = Data->StatistikaVyberQuery;
  ReportReportForm->DateQRDBText->DataSet      = Data->StatistikaVyberQuery;
  ReportReportForm->CountQRDBText->DataSet     = Data->StatistikaVyberQuery;
  ReportReportForm->AverageQRDBText->DataSet   = Data->StatistikaVyberQuery;
  ReportReportForm->GainQRDBText->DataSet      = Data->StatistikaVyberQuery;
  ReportReportForm->SigmaQRDBText->DataSet     = Data->StatistikaVyberQuery;
  ReportReportForm->CvQRDBText->DataSet        = Data->StatistikaVyberQuery;
  ReportReportForm->UniQRDBText->DataSet       = Data->StatistikaVyberQuery;
  ReportReportForm->DayQRDBText->DataField     = "DAY_NUMBER";
  ReportReportForm->DateQRDBText->DataField    = "DATE_TIME_DATE";
  ReportReportForm->CountQRDBText->DataField   = "STAT_COUNT";
  ReportReportForm->AverageQRDBText->DataField = "AVERAGE_WEIGHT";
  ReportReportForm->GainQRDBText->DataField    = "GAIN";
  ReportReportForm->SigmaQRDBText->DataField   = "SIGMA";
  ReportReportForm->CvQRDBText->DataField      = "CV";
  ReportReportForm->UniQRDBText->DataField     = "UNI";
  // Jmeno souboru
  ReportReportForm->SouborQRLabel->Caption = Data->Soubor.Jmeno;
  // Pohlavi
  ReportReportForm->PohlaviNazevQRLabel->Enabled = BothGenders;
  ReportReportForm->PohlaviQRLabel->Enabled      = BothGenders;
  if (BothGenders) {
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      ReportReportForm->PohlaviQRLabel->Caption = LoadStr(SAMICE);
    } else {
      ReportReportForm->PohlaviQRLabel->Caption = LoadStr(SAMEC);
    }
  }
  ReportReportForm->Preview();
}

//---------------------------------------------------------------------------
// Tisk vzorku
//---------------------------------------------------------------------------

void PrintSamples() {
  // Vytiskne vzorky v Data->TiskQuery
  VzorkyReportForm->SouborQRLabel->Caption = Data->Soubor.Jmeno;
  VzorkyReportForm->Preview();
}

//---------------------------------------------------------------------------
// Tisk histogramu
//---------------------------------------------------------------------------

void PrintHistogram(TChart *Chart, bool BothGenders) {
  // Vytiskne histogram
  BargrafReportForm->NazevQRLabel->Caption  =  HlavniForm->HistogramTabSheet->Caption;
  BargrafReportForm->SouborQRLabel->Caption  = Data->Soubor.Jmeno;
  BargrafReportForm->DenQRLabel->Caption     = Data->Soubor.Den;
  BargrafReportForm->DatumQRLabel->Caption   = HlavniForm->DatumLabel->Caption;
  // Pohlavi
  BargrafReportForm->PohlaviNazevQRLabel->Enabled = BothGenders;
  BargrafReportForm->PohlaviQRLabel->Enabled      = BothGenders;
  if (BothGenders) {
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      BargrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMICE);
    } else {
      BargrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMEC);
    }
  }
  PrintCopyCharts(Chart->Series[0], BargrafReportForm->HistogramQRChart->Chart->Series[0]);
  BargrafReportForm->HistogramQRChart->Chart->BottomAxis->Title->Caption = LoadStr(HMOTNOST);   // Nazev osy
  BargrafReportForm->HistogramQRChart->Chart->LeftAxis->Title->Caption   = LoadStr(POCET);
  BargrafReportForm->Preview();
}

//---------------------------------------------------------------------------
// Tisk denni aktivity
//---------------------------------------------------------------------------

void PrintDayActivity(TChart *Chart, bool BothGenders) {
  // Vytiskne denni aktivitu
  BargrafReportForm->NazevQRLabel->Caption  =  HlavniForm->AktivitaDenTabSheet->Caption;
  BargrafReportForm->SouborQRLabel->Caption  = Data->Soubor.Jmeno;
  BargrafReportForm->DenQRLabel->Caption     = Data->Soubor.Den;
  BargrafReportForm->DatumQRLabel->Caption   = HlavniForm->DatumLabel->Caption;
  // Pohlavi
  BargrafReportForm->PohlaviNazevQRLabel->Enabled = BothGenders;
  BargrafReportForm->PohlaviQRLabel->Enabled      = BothGenders;
  if (BothGenders) {
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      BargrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMICE);
    } else {
      BargrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMEC);
    }
  }
  PrintCopyCharts(Chart->Series[0], BargrafReportForm->HistogramQRChart->Chart->Series[0]);
  BargrafReportForm->HistogramQRChart->Chart->BottomAxis->Title->Caption = LoadStr(HODINA);   // Nazev osy
  BargrafReportForm->HistogramQRChart->Chart->LeftAxis->Title->Caption   = LoadStr(POCET);
  BargrafReportForm->Preview();
}

//---------------------------------------------------------------------------
// Tisk celkove aktivity
//---------------------------------------------------------------------------

void PrintTotalActivity(TChart *Chart, bool BothGenders) {
  // Vytiskne celkovou aktivitu
  GrafReportForm->NazevQRLabel->Caption  =  HlavniForm->AktivitaCelkemTabSheet->Caption;
  GrafReportForm->SouborQRLabel->Caption  = Data->Soubor.Jmeno;
  // Pohlavi
  GrafReportForm->PohlaviNazevQRLabel->Enabled = BothGenders;
  GrafReportForm->PohlaviQRLabel->Enabled      = BothGenders;
  if (BothGenders) {
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      GrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMICE);
    } else {
      GrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMEC);
    }
  }
  PrintCopyCharts(Chart->Series[INDEX_AKTIVITA_CELKEM], GrafReportForm->HistogramQRChart->Chart->Series[0]);
  GrafReportForm->HistogramQRChart->Chart->Series[1]->Active = false;                   // Druhy prubeh zakazu
  GrafReportForm->HistogramQRChart->Chart->Series[2]->Active = false;                   // Treti prubeh zakazu
  GrafReportForm->HistogramQRChart->Chart->Series[3]->Active = false;                   // Ctvrty prubeh zakazu
  GrafReportForm->HistogramQRChart->Chart->Legend->Visible = false;                     // Schovam legendu
  GrafReportForm->HistogramQRChart->Chart->BottomAxis->Title->Caption = LoadStr(DEN);   // Nazev osy
  GrafReportForm->HistogramQRChart->Chart->LeftAxis->Title->Caption   = LoadStr(POCET);
  GrafReportForm->Preview();
}

//---------------------------------------------------------------------------
// Tisk rustove krivky
//---------------------------------------------------------------------------

void PrintGrowthCurve(TChart *Chart, bool BothGenders) {
  // Vytiskne rustovou krivku
  GrafReportForm->NazevQRLabel->Caption  = HlavniForm->KrivkaTabSheet->Caption;
  GrafReportForm->SouborQRLabel->Caption = Data->Soubor.Jmeno;
  // Pohlavi
  GrafReportForm->PohlaviNazevQRLabel->Enabled = BothGenders;
  GrafReportForm->PohlaviQRLabel->Enabled      = BothGenders;
  if (BothGenders) {
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      GrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMICE);
    } else {
      GrafReportForm->PohlaviQRLabel->Caption = LoadStr(SAMEC);
    }
  }

  // Prubeh rustove krivky zobrazuju vzdy
  PrintCopyCharts(Chart->Series[KRIVKA_REAL], GrafReportForm->HistogramQRChart->Chart->Series[0]);

  // Cilovou hmotnost jako druhy prubeh
  GrafReportForm->HistogramQRChart->Chart->Series[1]->Active = Chart->Series[KRIVKA_TARGET]->Active;
  if (Chart->Series[KRIVKA_TARGET]->Active) {
    // Chce zobrazit prubeh cilove hmotnosti
    PrintCopyCharts(Chart->Series[KRIVKA_TARGET], GrafReportForm->HistogramQRChart->Chart->Series[1]);
  }

  // Teoretickou rustovou krivku jako treti prubeh
  GrafReportForm->HistogramQRChart->Chart->Series[2]->Active = Chart->Series[KRIVKA_THEORY]->Active;
  if (Chart->Series[KRIVKA_THEORY]->Active) {
    // Chce zobrazit prubeh cilove hmotnosti
    PrintCopyCharts(Chart->Series[KRIVKA_THEORY], GrafReportForm->HistogramQRChart->Chart->Series[2]);
  }

  // Statistiku na prave ose jako ctvrty prubeh
  GrafReportForm->HistogramQRChart->Chart->Series[3]->Active = Chart->Series[KRIVKA_STATISTICS]->Active;
  if (Chart->Series[KRIVKA_STATISTICS]->Active) {
    // Chce zobrazit prubeh cilove hmotnosti
    PrintCopyCharts(Chart->Series[KRIVKA_STATISTICS], GrafReportForm->HistogramQRChart->Chart->Series[3]);
  }

  GrafReportForm->HistogramQRChart->Chart->Legend->Visible = true;                      // Zobrazim legendu
  GrafReportForm->HistogramQRChart->Chart->BottomAxis->Title->Caption = LoadStr(DEN);   // Nazev osy
  GrafReportForm->HistogramQRChart->Chart->LeftAxis->Title->Caption   = LoadStr(HMOTNOST);
  GrafReportForm->HistogramQRChart->Chart->LeftAxis->Increment = Chart->LeftAxis->Increment;    // Rustova krivka ma mensi inkrement
  GrafReportForm->HistogramQRChart->Chart->RightAxis->Title->Caption  = Chart->RightAxis->Title->Caption;
  try {
    GrafReportForm->Preview();
  } catch(...) {}
  GrafReportForm->HistogramQRChart->Chart->LeftAxis->Increment = 0;     // Zpet na default
}

//---------------------------------------------------------------------------
// Tisk online vazeni
//---------------------------------------------------------------------------

void PrintOnline(TChart *Chart) {
  // Vytiskne online graf
  OnlineGrafReportForm->NazevQRLabel->Caption  =  HlavniForm->OnlineGrafTabSheet->Caption;
  OnlineGrafReportForm->SouborQRLabel->Caption  = Data->Soubor.Jmeno;
  PrintCopyCharts(Chart->Series[0], OnlineGrafReportForm->QRChart->Chart->Series[0]);
  NastavOsyGrafu(Chart, OnlineGrafReportForm->QRChart->Chart);
  OnlineGrafReportForm->QRChart->Chart->BottomAxis->Title->Caption = HlavniForm->OnlineChart->BottomAxis->Title->Caption;   // Nazev osy
  OnlineGrafReportForm->QRChart->Chart->LeftAxis->Title->Caption   = LoadStr(HMOTNOST);
  OnlineGrafReportForm->Preview();
}

//---------------------------------------------------------------------------
// Kopie grafu
//---------------------------------------------------------------------------

void PrintCopyCharts(TChartSeries *Chart1, TChartSeries *Chart2) {
  // Zkopiruje obsah Chart1 do Chart2 vcetne nazvu
  Chart2->Clear();                   // Vymazu z grafu vsechny predchozi hodnoty
  for (int Index = 0; Index < Chart1->Count(); Index++) {
    Chart2->AddXY(Chart1->XValue[Index],
                  Chart1->YValue[Index],
                  "",   // Popisek
                  clTeeColor);
  }
  // Nazev
  Chart2->Title = Chart1->Title;
}

//---------------------------------------------------------------------------
// Nastaveni osy grafu
//---------------------------------------------------------------------------

static void NastavOsyGrafu(TChart *Chart1, TQRDBChart *Chart2) {
  // Nastavim osy z Chart1 do Chart2
  Chart2->BottomAxis->SetMinMax(Chart1->BottomAxis->Maximum, Chart1->BottomAxis->Minimum);
  Chart2->LeftAxis->SetMinMax(Chart1->LeftAxis->Maximum, Chart1->LeftAxis->Minimum);
}


