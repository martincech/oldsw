�
 TCURVESFORM 0D  TPF0TCurvesForm
CurvesFormLeftTop� BorderIconsbiSystemMenu BorderStylebsSingleCaptionWachstumskurvenClientHeight�ClientWidth Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxCurvesGroupBoxLeftTopPWidth� HeightqCaptionBestehende KurvenTabOrder  TListBoxCurvesListBoxLeftTopWidth� Height
ItemHeightTabOrder OnClickCurvesListBoxClick  TButtonNewCurveButtonLeftTop(Width]HeightCaptionNeuTabOrderOnClickNewCurveButtonClick  TButtonDeleteCurveButtonLeft|TopHWidth]HeightCaptionL�schenTabOrderOnClickDeleteCurveButtonClick  TButtonRenameCurveButtonLeftTopHWidth]HeightCaption
UmbenennenTabOrderOnClickRenameCurveButtonClick  TButtonCopyCurveButtonLeft|Top(Width]HeightCaptionKopierenTabOrderOnClickCopyCurveButtonClick   	TGroupBoxDefinitionGroupBoxLeftTopPWidthHeightQCaption
DefinitionTabOrder TStringGridCurveStringGridLeftTopWidth� HeightColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsUT 
RowHeights   TButtonSaveCurveButtonLeft� Top(Width]HeightCaption	SpeichernTabOrderOnClickSaveCurveButtonClick   TPanelDecriptionPanelLeft Top Width HeightAAlignalTop
BevelOuterbvNoneEnabledTabOrder TLabelLabel1LeftTopWidthHeight1AutoSizeCaption�Hier k�nnen Sie die vom Gefl�gelz�chter zur Verf�gung gestellten Soll-Wachstumskurven definieren. Diese k�nnen dann mit den Ist-Werten verglichen werden.WordWrap	   TButtonOkButtonLeftgTop�WidthKHeightCaptionOKTabOrderOnClickOkButtonClick  TButtonCancelButtonLeft�Top�WidthKHeightCaption	AbbrechenModalResultTabOrder   