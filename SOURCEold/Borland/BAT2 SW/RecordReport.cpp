//----------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "RecordReport.h"
//----------------------------------------------------------------------------
#pragma resource "*.dfm"
TRecordReportForm *RecordReportForm;


//----------------------------------------------------------------------------
// Zviditelneni statistiku samcu
//----------------------------------------------------------------------------

void TRecordReportForm::SetMalesVisible(bool Visible) {
  // Pokud je <Visible> true, zviditelni statistiku samcu
  FemalesQRLabel->Enabled      = Visible;
  MalesQRLabel->Enabled        = Visible;

  MalesCountQRLabel->Enabled   = Visible;
  MalesAverageQRLabel->Enabled = Visible;
  MalesGainQRLabel->Enabled    = Visible;
  MalesSigmaQRLabel->Enabled   = Visible;
  MalesCvQRLabel->Enabled      = Visible;
  MalesUniQRLabel->Enabled     = Visible;
} // SetMalesVisible

//----------------------------------------------------------------------------
__fastcall TRecordReportForm::TRecordReportForm(TComponent* Owner)
    : TQuickRep(Owner)
{
}
//----------------------------------------------------------------------------