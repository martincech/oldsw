�
 TNASTAVENIVAHFORM 0�0  TPF0TNastaveniVahFormNastaveniVahFormLeftQTop� BorderIconsbiSystemMenu BorderStylebsSingleCaption	ProgramarClientHeight�ClientWidthOColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth HeightCaptionAjuste:  TButton	NewButtonLeftTop� WidthyHeightCaptionNuevoTabOrderOnClickNewButtonClick  TButton
SaveButtonLeftTopWidthyHeightCaptionGrabarTabOrderOnClickSaveButtonClick  TButtonUploadButtonLeftTopXWidthyHeightCaptionCargarTabOrderOnClickUploadButtonClick  TListBoxSeznamListBoxLeftTop WidthyHeight� 
ItemHeightSorted	TabOrder OnClickSeznamListBoxClick  TPageControlPageControlLeft� TopWidth�Height�
ActivePageHejnaTabSheetTabOrder 	TTabSheetHejnaTabSheetCaptionLotes
ImageIndex TLabelLabel23LeftTopWidthHeightCaptionLotes:  TListBoxSeznamHejnListBoxLeftTopWidthKHeight� 
ItemHeightSorted	TabOrder OnClickSeznamHejnListBoxClick  TButtonNewFlockButtonLeftTop WidthKHeightCaptionNuevoTabOrderOnClickNewFlockButtonClick  TButtonSaveFlockButtonLeftTop WidthKHeightCaptionGrabarTabOrderOnClickSaveFlockButtonClick  TButtonDeleteFlockButtonLeftTop@WidthKHeightCaptionBorrarTabOrderOnClickDeleteFlockButtonClick  TPanel
HejnaPanelLefthTopWidth� HeightY
BevelOuterbvNoneTabOrder TLabelLabel22LeftTopWidth(HeightCaptionNombre:  TLabelLabel24LeftTophWidth;HeightCaptionPeso desde:  TLabelLabel25LeftxTophWidthHeightCaptionhasta:  TDBEditJmenoDBEditLeftXTopWidth� Height	DataFieldNAME
DataSourceData.NastaveniHejnDataSourceTabOrder   TDBEditDBEdit15LeftPTopeWidthHeight	DataField
WEIGH_FROM
DataSourceData.NastaveniHejnDataSourceTabOrder  TDBEditDBEdit16Left� TopeWidthHeight	DataFieldWEIGH_TO
DataSourceData.NastaveniHejnDataSourceTabOrder  TPageControlKrivkyPageControlLeftTop� Width� Height� 
ActivePageKrivkaSamiceTabSheetTabOrder 	TTabSheetKrivkaSamiceTabSheetCaptionHembras TStringGridKrivkaSamiceStringGridLeft Top Width� Height� AlignalTopColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsB] 
RowHeights   TButtonFemalesLoadButtonLeft� Top� WidthKHeightCaptionCargarTabOrderOnClickFemalesLoadButtonClick   	TTabSheetKrivkaSamciTabSheetCaptionMachos
ImageIndex TStringGridKrivkaSamciStringGridLeft Top Width� Height� ColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsB]   TButtonMalesLoadButtonLeft� Top� WidthKHeightCaptionCargarTabOrderOnClickFemalesLoadButtonClick   	TTabSheetPocatecniHmotnostTabSheetCaptionPeso inicial
ImageIndex TLabelLabel26LeftTopWidth-HeightCaptionHembras:  TDBEditPocatecniHmotnostSamiceDBEditLeft`TopWidthaHeight	DataFieldFEMALE_WEIGHT00
DataSourceData.NastaveniHejnDataSourceTabOrder   TPanelPocatecniHmotnostSamciPanelLeftTop0Width� Height!
BevelOuterbvNoneTabOrder TLabelLabel27LeftTopWidth)HeightCaptionMachos:  TDBEditPocatecniHmotnostSamciDBEditLeftXTop WidthaHeight	DataFieldMALE_WEIGHT00
DataSourceData.NastaveniHejnDataSourceTabOrder      	TCheckBoxPohlaviCheckBoxLeftTop0Width� HeightCaptionUsar machos y hembrasTabOrderOnClickPohlaviCheckBoxClick  	TCheckBoxKrivkyCheckBoxLeftTopHWidth� HeightCaptionUsar curvas de crecimientoTabOrderOnClickKrivkyCheckBoxClick   TButtonButton1Left TopHWidth3HeightCaptionButton1TabOrderVisibleOnClickButton1Click   	TTabSheetStatistikaTabSheetCaptionEstad�sticas TLabelLabel2LeftTopWidthjHeightCaptionRango del histogr�ma:  TLabelLabel3LeftTop0WidthkHeightCaptionRango de uniformidad:  TLabelLabel4Left� Top0WidthHeightCaption�  TLabelLabel7Left� Top0WidthHeightCaption%  TLabelLabel6Left� TopWidthHeightCaption%  TLabelLabel5Left� TopWidthHeightCaption�  TDBEditRozsahUniformityDBEditLeft� Top-Width!Height	DataField	UNI_RANGE
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditRozsahHistogramuDBEditLeft� TopWidth!Height	DataField
HIST_RANGE
DataSourceData.NastaveniVahDataSourceTabOrder    	TTabSheetGsmTabSheetCaptionGSM
ImageIndex TLabelLabel8LeftTop� Width-HeightCaptionN�meros:  TLabelLabel48Left"TopHWidth(HeightCaptionPer�odo:  TLabelLabel49LeftzTopHWidthHeightCaptionDias  TDBCheckBoxDBCheckBox1LeftTopWidthHeightCaptionUsar comunicaci�n GSM	DataFieldGSM_USE
DataSourceData.NastaveniVahDataSourceTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2LeftTop0WidthHeightCaption Enviar estad�sticas a medianoche	DataFieldGSM_SEND_MIDNIGHT
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox3LeftTop`WidthHeightCaptionEnviar datos por solicitud	DataFieldGSM_SEND_REQUEST
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox4LeftTopxWidthHeightCaptionRevisar n�meros	DataFieldGSM_CHECK_NUMBERS
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit3LeftTop� Width� Height	DataFieldGSM_NUMBER00
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit4LeftTop� Width� Height	DataFieldGSM_NUMBER01
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit5LeftTop� Width� Height	DataFieldGSM_NUMBER02
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit6LeftTop� Width� Height	DataFieldGSM_NUMBER03
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit7LeftTopWidth� Height	DataFieldGSM_NUMBER04
DataSourceData.NastaveniVahDataSourceTabOrder	
OnKeyPressDBEdit3KeyPress  TDBEditPeriodaDBEditLeftPTopEWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT
DataSourceData.NastaveniVahDataSourceTabOrder   	TTabSheetRs485TabSheetCaptionRS-485
ImageIndex TLabelLabel51LeftTopWidth2HeightCaption
Velocidad:  TLabelLabel55Left� TopWidthHeightCaptionBd  TLabelLabel53LeftTop� Width6HeightCaption
Respuesta:  TLabelLabel54LeftTopWidth,HeightCaption
Intervalo:  TLabelLabel57Left� TopWidthHeightCaptionms  TLabelLabel56Left� Top� WidthHeightCaptionms  TDBRadioGroupRs485ParityDBRadioGroupLeftTop8Width� HeightICaptionParidadColumns	DataFieldRS485_PARITY
DataSourceData.NastaveniVahDataSourceItems.Strings8-n-18-e-18-o-18-n-2 TabOrderValues.Strings0123   TDBEditRs485ReplyDelayDBEditLeft� Top� Width!Height	DataFieldRS485_REPLY_DELAY
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditRs485SilentIntervalDBEditLeft� TopWidth!Height	DataFieldRS485_SILENT_INTERVAL
DataSourceData.NastaveniVahDataSourceTabOrder  TDBComboBoxRs485SpeedDBComboBoxLeft� TopWidthQHeightStylecsDropDownList	DataFieldRS485_SPEED
DataSourceData.NastaveniVahDataSource
ItemHeightItems.Strings1200240048009600192003840057600115200 TabOrder   TDBRadioGroupDBRadioGroup4LeftTop� Width� HeightICaption	Protocolo	DataFieldRS485_PROTOCOL
DataSourceData.NastaveniVahDataSourceItems.Strings
MODBUS RTUMODBUS ASCII TabOrderValues.Strings0123    	TTabSheetVahyTabSheetCaptionBalanzas
ImageIndex TLabelLabel9LeftTopWidth� HeightCaption%Valor sobre el objetivo para hembras:  TLabelLabel10LeftTopWidthHeightCaption%  TLabelLabel11LeftTop0Width� HeightCaption'Valor debajo del objetivo para hembras:  TLabelLabel12LeftTop0WidthHeightCaption%  TLabelLabel13LeftTopHWidth� HeightCaption$Valor sobre el objetivo para machos:  TLabelLabel14LeftTopHWidthHeightCaption%  TLabelLabel15LeftTop`Width� HeightCaption&Valor debajo del objetivo para machos:  TLabelLabel16LeftTop`WidthHeightCaption%  TLabelLabel17LeftTop� WidthCHeightCaptionEstabilizaci�n:  TLabelLabel18Left� Top� WidthHeightCaption�  TLabelLabel19LeftTop� WidthHeightCaption%  TLabelLabel20LeftTop� WidthwHeightCaptionTiempo de estabilizaci�n:  TLabelLabel21LeftTopxWidthHeightCaptionFiltro:  TDBEditSamiceOkoliNadDBEditLeft� TopWidth!Height	DataFieldF_MARGIN_ABOVE
DataSourceData.NastaveniVahDataSourceTabOrder   TDBEditSamiceOkoliPodDBEditLeft� Top-Width!Height	DataFieldF_MARGIN_UNDER
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditSamciOkoliNadDBEditLeft� TopEWidth!Height	DataFieldM_MARGIN_ABOVE
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditSamciOkoliPodDBEditLeft� Top]Width!Height	DataFieldM_MARGIN_UNDER
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditUstaleniDBEditLeft� Top� Width!Height	DataFieldSTABILIZATION
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditDobaUstaleniDBEditLeft� Top� Width!Height	DataFieldSTABILIZATION_TIME
DataSourceData.NastaveniVahDataSourceTabOrder  TDBRadioGroupDBRadioGroup1LeftTop(WidthyHeight1CaptionUnidadesColumns	DataFieldUNITS
DataSourceData.NastaveniVahDataSourceItems.Stringskglb TabOrder	Values.Strings01   TDBEditFiltrDBEditLeft� TopuWidth!Height	DataFieldFILTER_VALUE
DataSourceData.NastaveniVahDataSourceTabOrder  	TCheckBoxNastaveniGainCheckBoxLeftTop� Width� HeightCaption#Usar el modo de ganancia autom�ticaTabOrderOnClickNastaveniGainCheckBoxClick  TDBRadioGroupDBRadioGroup3LeftTop� Width� HeightICaptionGrabar muestra	DataField	JUMP_MODE
DataSourceData.NastaveniVahDataSourceItems.StringsAlmacenar al entrarAlmacenar al salirAlmacenar al entrar y salir TabOrderValues.Strings012    	TTabSheetCorrectionTabSheetCaptionCurva de correcci�n
ImageIndex TLabelLabel61LeftTop8WidthHeightCaptionD�a 1:  TLabelLabel64LeftTopPWidthHeightCaptionD�a 2:  TLabelLabel62LeftTophWidth6HeightCaptionCorrecci�n:  TLabelLabel63Left~TophWidthHeightCaption+  TLabelLabel65Left� TophWidthHeightCaption%  TDBEditCorrectionCorrectionDBEditLeft� TopeWidth!Height	DataFieldCORRECTION_CORRECTION
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEditCorrectionDay2DBEditLeft� TopMWidth!Height	DataFieldCORRECTION_DAY2
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEditCorrectionDay1DBEditLeft� Top5Width!Height	DataFieldCORRECTION_DAY1
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  	TCheckBoxUseCorrectionCheckBoxLeftTopWidthQHeightCaptionUsar la curva de correcci�nTabOrder    	TTabSheetPodsvitTabSheetCaptionRetroiluminaci�n
ImageIndex TDBRadioGroupDBRadioGroup2LeftTopWidth1Height9CaptionRetroiluminaci�nColumns	DataField	BACKLIGHT
DataSourceData.NastaveniVahDataSourceItems.StringsApagado	Encendido
Autom�tico TabOrder Values.Strings012     TButtonDownloadButtonLeftTopxWidthyHeightCaption	DescargarTabOrderOnClickDownloadButtonClick  TButtonDeleteButtonLeftTop8WidthyHeightCaptionBorrarTabOrderOnClickDeleteButtonClick   