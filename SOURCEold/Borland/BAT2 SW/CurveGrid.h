//***************************************************************************
//
//    CurveGrid.cpp - Growth curve in a StringGrid
//    Version 1.0
//
//***************************************************************************

#ifndef CURVEGRID_H
#define CURVEGRID_H

#include <grids.hpp>
#include <system.hpp>           // AnsiString
#include "Curve.h"


void CurveGridClear(TStringGrid *Grid);
// Vymazu celou tabulku <Grid> (nemazu zahlavi)

void CurveGridShow(TStringGrid *Grid);
// Zobrazi popis v tabulce <Grid>

void CurveGridShow(TStringGrid *Grid, TGrowthCurve *Curve);
// Zobrazi krivku <Curve> v tabulce <Grid>

int CurveGridNumberOfDays(TStringGrid *Grid);
// Vrati pocet zadanych dnu v tabulce <Grid>

bool CurveGridCheckData(TStringGrid *Grid);
// Zkontroluje hodnoty zadane v tabulce <Grid>

bool CurveGridSave(TStringGrid *Grid, TGrowthCurve *Curve);
// Ulozi krivku <Curve> zadanou v tabulce <Grid>


#endif // CURVEGRID_H

