//***************************************************************************
//
//   Print.cpp        Printing functions
//   Version 1.0
//
//***************************************************************************

#ifndef PrintH
#define PrintH
//---------------------------------------------------------------------------

#include <Chart.hpp>


void PrintReport(bool BothGenders);
  // Vytiskne report v Data->StatistikaVyberQuery

void PrintSamples();
  // Vytiskne vzorky v Data->TiskQuery

void PrintHistogram(TChart *Chart, bool BothGenders);
  // Vytiskne histogram

void PrintDayActivity(TChart *Chart, bool BothGenders);
  // Vytiskne denni aktivitu

void PrintTotalActivity(TChart *Chart, bool BothGenders);
  // Vytiskne celkovou aktivitu

void PrintGrowthCurve(TChart *Chart, bool BothGenders);
  // Vytiskne rustovou krivku

void PrintOnline(TChart *Chart);
  // Vytiskne online graf

void PrintCopyCharts(TChartSeries *Chart1, TChartSeries *Chart2);
  // Zkopiruje obsah Chart1 do Chart2 vcetne nazvu


#endif // PrintH

