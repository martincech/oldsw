object FindFileForm: TFindFileForm
  Left = 439
  Top = 254
  Width = 630
  Height = 451
  Caption = 'Find file'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 233
    Height = 424
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox3: TGroupBox
      Left = 16
      Top = 16
      Width = 201
      Height = 129
      Caption = 'Filter'
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 24
        Width = 30
        Height = 13
        Caption = 'Scale:'
      end
      object Label2: TLabel
        Left = 16
        Top = 48
        Width = 22
        Height = 13
        Caption = 'Day:'
      end
      object Label3: TLabel
        Left = 16
        Top = 72
        Width = 26
        Height = 13
        Caption = 'From:'
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 16
        Height = 13
        Caption = 'Till:'
      end
      object ScalesEdit: TEdit
        Left = 96
        Top = 21
        Width = 89
        Height = 21
        MaxLength = 5
        TabOrder = 0
        OnKeyPress = ScalesEditKeyPress
      end
      object DayEdit: TEdit
        Left = 96
        Top = 45
        Width = 89
        Height = 21
        MaxLength = 3
        TabOrder = 1
        OnKeyPress = ScalesEditKeyPress
      end
      object FromDateTimePicker: TDateTimePicker
        Left = 96
        Top = 69
        Width = 89
        Height = 21
        CalAlignment = dtaLeft
        Date = 38650.6882599074
        Time = 38650.6882599074
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 2
      end
      object TillDateTimePicker: TDateTimePicker
        Left = 96
        Top = 93
        Width = 89
        Height = 21
        CalAlignment = dtaLeft
        Date = 38650.6882599074
        Time = 38650.6882599074
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 3
      end
      object FromCheckBox: TCheckBox
        Left = 80
        Top = 72
        Width = 16
        Height = 17
        TabOrder = 4
      end
      object TillCheckBox: TCheckBox
        Left = 80
        Top = 95
        Width = 16
        Height = 17
        TabOrder = 5
      end
    end
    object GroupBox1: TGroupBox
      Left = 16
      Top = 160
      Width = 201
      Height = 249
      Caption = 'Location'
      TabOrder = 1
      object DriveComboBox: TDriveComboBox
        Left = 16
        Top = 24
        Width = 169
        Height = 19
        DirList = DirectoryListBox
        TabOrder = 0
      end
      object DirectoryListBox: TDirectoryListBox
        Left = 16
        Top = 56
        Width = 169
        Height = 145
        ItemHeight = 16
        TabOrder = 1
      end
      object SearchButton: TButton
        Left = 16
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Search'
        TabOrder = 2
        OnClick = SearchButtonClick
      end
      object StopButton: TButton
        Left = 110
        Top = 208
        Width = 75
        Height = 25
        Caption = 'Stop'
        TabOrder = 3
        OnClick = StopButtonClick
      end
    end
  end
  object Panel1: TPanel
    Left = 233
    Top = 0
    Width = 389
    Height = 424
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox2: TGroupBox
      Left = 0
      Top = 16
      Width = 377
      Height = 352
      Align = alClient
      Caption = 'Files found'
      TabOrder = 0
      object FilesListBox: TListBox
        Left = 16
        Top = 25
        Width = 345
        Height = 278
        Align = alClient
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel2: TPanel
        Left = 361
        Top = 25
        Width = 14
        Height = 278
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
      end
      object Panel4: TPanel
        Left = 2
        Top = 25
        Width = 14
        Height = 278
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 2
      end
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 373
        Height = 10
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
      end
      object Panel6: TPanel
        Left = 2
        Top = 303
        Width = 373
        Height = 47
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 4
        object Panel8: TPanel
          Left = 280
          Top = 0
          Width = 93
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object OpenButton: TButton
            Left = 4
            Top = 8
            Width = 75
            Height = 25
            Caption = 'Open file'
            TabOrder = 0
            OnClick = OpenButtonClick
          end
        end
      end
    end
    object Panel7: TPanel
      Left = 0
      Top = 368
      Width = 389
      Height = 56
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Panel11: TPanel
        Left = 301
        Top = 0
        Width = 88
        Height = 56
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object Button1: TButton
          Left = 0
          Top = 16
          Width = 75
          Height = 25
          Cancel = True
          Caption = 'Cancel'
          TabOrder = 0
          OnClick = Button1Click
        end
      end
    end
    object Panel9: TPanel
      Left = 0
      Top = 0
      Width = 389
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
    end
    object Panel10: TPanel
      Left = 377
      Top = 16
      Width = 12
      Height = 352
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
    end
  end
end
