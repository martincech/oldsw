//---------------------------------------------------------------------------

#ifndef SimulaceStartH
#define SimulaceStartH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TSimulaceStartForm : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TEdit *OdEdit;
        TEdit *DoEdit;
        TLabel *Label2;
        TButton *Button1;
        TButton *Button2;
        TGroupBox *GroupBox2;
        TLabel *Label30;
        TLabel *Label32;
        TLabel *Label34;
        TLabel *Label36;
        TLabel *Label43;
        TLabel *Label38;
        TLabel *Label41;
        TLabel *Label39;
        TLabel *Label40;
        TLabel *Label37;
        TLabel *Label35;
        TLabel *Label33;
        TLabel *Label31;
        TEdit *SamiceOkoliNadEdit;
        TEdit *SamiceOkoliPodEdit;
        TEdit *SamciOkoliNadEdit;
        TEdit *SamciOkoliPodEdit;
        TEdit *FiltrEdit;
        TEdit *UstaleniEdit;
        TEdit *DobaUstaleniEdit;
        TGroupBox *GroupBox3;
        TLabel *Label8;
        TLabel *Label10;
        TLabel *Label12;
        TLabel *Label28;
        TLabel *Label14;
        TLabel *Label16;
        TEdit *RozsahHistogramuEdit;
        TEdit *RozsahUniformityEdit;
        TGroupBox *GroupBox4;
        TCheckBox *ObePohlaviCheckBox;
        TLabel *Label5;
        TEdit *CilovaHmotnostSamiceEdit;
        TLabel *Label6;
        TEdit *CilovaHmotnostSamciEdit;
        TRadioButton *GrafRadioButton;
        TRadioButton *DnesRadioButton;
        TRadioButton *VseRadioButton;
        TRadioButton *RozsahRadioButton;
        TRadioGroup *JumpModeRadioGroup;
        void __fastcall OdEditKeyPress(TObject *Sender, char &Key);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall UstaleniEditKeyPress(TObject *Sender, char &Key);
        void __fastcall ObePohlaviCheckBoxClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TSimulaceStartForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSimulaceStartForm *SimulaceStartForm;
//---------------------------------------------------------------------------
#endif
