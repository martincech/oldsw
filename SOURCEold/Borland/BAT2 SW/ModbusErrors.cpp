//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusErrors.h"
#include "Konstanty.h"
#include "Dpi.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TModbusErrorsForm *ModbusErrorsForm;
//---------------------------------------------------------------------------
__fastcall TModbusErrorsForm::TModbusErrorsForm(TComponent* Owner)
        : TForm(Owner)
{
  // Nastavim vysku radku vsech StringGridu podle aktualniho DPI
  DpiSetStringGrid(ScalesStringGrid);
}
//---------------------------------------------------------------------------
void __fastcall TModbusErrorsForm::FormShow(TObject *Sender)
{
  // Popis tabulky
  ScalesStringGrid->Cells[0][0] = LoadStr(STR_RS485_SCALE);
  ScalesStringGrid->Cells[1][0] = LoadStr(STR_RS485_ERRORS);

  OkButton->SetFocus();
}
//---------------------------------------------------------------------------
