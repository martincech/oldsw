//*****************************************************************************
//
//    Hardware.h -  Borland cross definitions
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\..\Library\Unisys\uni.h"             // zakladni datove typy
#include "..\../Library/Unisys\SysDef.h"          // zakladni datove typy

// eliminace XDATA :

#define __xdata__                      // prazdna definice

typedef short           int16;
typedef int             long32;

#define char2hex( ch) ((ch) <= '9' ? (ch) - '0' : (ch) - 'A' + 10)
#define nibble2hex( n)   ((n) > 9 ? 'A' + (n) - 10 : '0' + (n))
#define char2dec( ch) ((ch) - '0')

//-----------------------------------------------------------------------------
// Modbus RS232
//-----------------------------------------------------------------------------

#define COM_DATA_SIZE        520      // velikost bufferu
typedef int com_pointer_t;            // datovy typ ukazatele do bufferu

#define COM_LEADER_CHAR    ':'
#define COM_TRAILER_CHAR   '\n'

//-----------------------------------------------------------------------------
// Modbus
//-----------------------------------------------------------------------------

// podminena kompilace :
#define MB_ENABLE_DISCRETE         1   // povoleni operaci se skupinou DISCRETE
#define MB_ENABLE_COILS            1   // povoleni operaci se skupinou COILS
#define MB_ENABLE_INPUT_REGISTERS  1   // povoleni operaci se skupinou Holding REGISTERS
#define MB_ENABLE_REGISTERS        1   // povoleni operaci se skupinou Holding REGISTERS
#define MB_ENABLE_FIFO             1   // povoleni operaci s FIFO registry
#define MB_ENABLE_EXCEPTION_STATUS 1   // povoleni funkce exception status
#define MB_ENABLE_DIAGNOSTIC       1   // povoleni diagnostickych operaci
#define MB_ENABLE_COM_EVENT        1   // povoleni komunikacnich udalosti
#define MB_ENABLE_SLAVE_ID         1   // povoleni identifikace
#define MB_ENABLE_IDENTIFICATION   1   // povoleni textove identifikace

#endif
    