//***************************************************************************
//
//    ScaleSetup.cpp - Setup of scale
//    Version 1.0
//
//***************************************************************************

#ifndef SCALESETUP_H
#define SCALESETUP_H

#include "FlockList.h"

#include <system.hpp>           // AnsiString
#include <vector.h>

#pragma pack(push, 1)           // byte alignment
  #include "c51/Bat2.h"         // flash data definition
#pragma pack(pop)               // original alignment

//---------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// Setup
//---------------------------------------------------------------------------

class TScaleSetup {

private:

public:
  AnsiString Name;              // Nazev nastaveni

  // TConfig:
  word              IdentificationNumber;           // identifikace zarizeni
  byte              MarginAbove[_GENDER_COUNT];     // Okoli nad prumerem pro samice a samce
  byte              MarginBelow[_GENDER_COUNT];     // Okoli pod prumerem pro samice a samce
  byte              Filter;                         // Filtr prevodniku
  double            StabilizationRange;             // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
  byte              StabilizationTime;              // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
  TAutoMode         AutoMode;                       // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)
  TJumpMode         JumpMode;                       // Typ naskoku/seskoku na vahu, ktery se vyhodnocuje
  TUnits            Units;                          // Zobrazovane jednotky
  byte              HistogramRange;                 // Rozsah histogramu v +- % stredni hodnoty
  byte              UniformityRange;                // Rozsah uniformity v +- %
  TBacklight        Backlight;                      // Nastaveny rezim podsvitu
  TGsm              Gsm;                            // Parametry GSM modulu
  TRs485            Rs485;                          // Parametry linky RS-485
  word              WeightCorrectionDay1;           // Kompenzacni krivka - den 1
  word              WeightCorrectionDay2;           // Kompenzacni krivka - den 2
  double            WeightCorrectionCorrection;     // Kompenzacni krivka - korekce v %

  // TFlocks:
  TFlockList        *FlockList;                     // Seznam hejn


  TScaleSetup(AnsiString Name, TConfig *Config, TFlock *Flock);
  ~TScaleSetup();

  void LoadConfig(TConfig *Config);
  // Prebere config z <Config>
  void SaveConfig(TConfig *Config);
  // Ulozi config do <Config>

  void LoadFlocks(TFlock *Flock);
  // Prebere hejna z <Flock>
  void SaveFlocks(TFlock *Flock);
  // Ulozi hejna do <Flock>

  static bool CheckIndentificationNumber(int Value);
  static bool CheckMargin(int Value);
  static bool CheckFilter(int Value);
  static bool CheckStabilizationRange(double Value);
  static bool CheckStabilizationTime(int Value);
  static bool CheckHistogramRange(int Value);
  static bool CheckUniformityRange(int Value);
  static bool CheckGsmPeriod(int Value);
  static bool CheckGsmNumber(AnsiString Number);
  static bool CheckRs485ReplyDelay(int Value);
  static bool CheckRs485SilentInterval(int Value);
  static bool CheckCorrectionDay(int Value);
  static bool CheckCorrectionCorrection(double Value);

};

#endif // SCALESETUP_H

