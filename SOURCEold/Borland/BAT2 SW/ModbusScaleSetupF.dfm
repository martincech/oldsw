object ModbusScaleSetupForm: TModbusScaleSetupForm
  Left = 219
  Top = 136
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Setup'
  ClientHeight = 555
  ClientWidth = 471
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline ScaleSetupFrame1: TScaleSetupFrame
    Left = 8
    Top = 8
    inherited ScaleSetupPageControl: TPageControl
      ActivePage = ScaleSetupFrame1.FlocksTabSheet
    end
  end
  object OkButton: TButton
    Left = 280
    Top = 512
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 378
    Top = 512
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 2
    OnClick = CancelButtonClick
  end
end
