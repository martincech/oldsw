//---------------------------------------------------------------------------

#ifndef WeighingFilterH
#define WeighingFilterH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TWeighingFilterForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *ScalesEdit;
        TLabel *Label3;
        TDateTimePicker *FromDateTimePicker;
        TLabel *Label2;
        TDateTimePicker *TillDateTimePicker;
        TCheckBox *FromCheckBox;
        TCheckBox *TillCheckBox;
        TButton *Button1;
        TButton *Button2;
        TLabel *Label4;
        TEdit *NameEdit;
        TLabel *Label5;
        TEdit *NoteEdit;
        TBevel *Bevel1;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall ScalesEditKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations

        bool CheckValues();


        __fastcall TWeighingFilterForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TWeighingFilterForm *WeighingFilterForm;
//---------------------------------------------------------------------------
#endif
