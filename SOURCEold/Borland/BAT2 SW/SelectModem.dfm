object SelectModemForm: TSelectModemForm
  Left = 370
  Top = 316
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Modem'
  ClientHeight = 171
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 70
    Height = 13
    Caption = 'Select modem:'
  end
  object ModemListBox: TListBox
    Left = 16
    Top = 32
    Width = 193
    Height = 121
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = ModemListBoxDblClick
  end
  object Button1: TButton
    Left = 224
    Top = 88
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 224
    Top = 128
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
