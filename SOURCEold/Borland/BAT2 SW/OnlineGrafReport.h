//----------------------------------------------------------------------------
#ifndef OnlineGrafReportH
#define OnlineGrafReportH
//----------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\QuickRpt.hpp>
#include <vcl\QRCtrls.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Qrctrls.hpp>
#include <QrTee.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//----------------------------------------------------------------------------
class TOnlineGrafReportForm : public TQuickRep
{
__published:
        TQRBand *TitleBand1;
        TQRLabel *NazevQRLabel;
        TQRSysData *QRSysData1;
        TQRLabel *QRLabel11;
        TQRLabel *SouborQRLabel;
        TQRChart *QRChart;
        TQRDBChart *QRDBChart1;
        TFastLineSeries *Series1;
private:
public:
   __fastcall TOnlineGrafReportForm::TOnlineGrafReportForm(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern TOnlineGrafReportForm *OnlineGrafReportForm;
//----------------------------------------------------------------------------
#endif