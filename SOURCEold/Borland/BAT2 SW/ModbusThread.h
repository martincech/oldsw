//---------------------------------------------------------------------------

#ifndef ModbusThreadH
#define ModbusThreadH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------

#include "ModbusScale.h"

typedef enum {
  THREAD_COMMAND_IDLE,
  THREAD_COMMAND_READ_CONFIG,           // Nacte konfiguraci
  THREAD_COMMAND_READ_FLOCK,            // Nacte hejno
  THREAD_COMMAND_READ_FLOCKS,           // Nacte vsechna hejna
  THREAD_COMMAND_READ_DATETIME,         // Nacte datum a cas
  THREAD_COMMAND_READ_TODAY,            // Nacte statistiku aktualniho dne vazeni
  THREAD_COMMAND_READ_ARCHIVE,          // Nacte kompletni archiv
  THREAD_COMMAND_READ_CALIBRATION,      // Nacte kalibraci
//  THREAD_COMMAND_READ_COUNTERS,         // Nacte diagnosticke citace
  _THREAD_COMMAND_COUNT
} TModbusThreadCommand;

class TModbusThread : public TThread
{
private:
        int ErrorCounter;        
protected:
        void __fastcall Execute();
public:
        TModbusScale            *Scale;         // Vaha, se kterou se pracuje

        TModbusThreadCommand    Command;        // Prikaz, ktery se ma provest na pozadi
        int                     CommandValue;   // Hodnota prikazu (napr. cislo hejna)
        int                     Repetitions;    // Pocet opakovani prikazu pri chybe komunikace
        bool                    Busy;           // Prikaz se prave provadi, stazeni do true zacne spusteni prikazu
        int                     CurrentNumber;  // Cislo aktualniho hejna/dne, ktere se cte
        TModbusScaleResult      Result;         // Vysledek prikazu

        __fastcall TModbusThread(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
