//******************************************************************************
//
//   MBCbPar.c    Modbus callbacks parameters
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MBCBPAR_H
#define MBCBPAR_H

#include "..\..\Intel51\Vym\Inc\MBPdu.h"

//---------------------------------------------------------------------------
// Globalni parametry
//---------------------------------------------------------------------------

// Pripojeni vahy
typedef struct {
  int Port;
  int Address;
} TMbcbConnection;
extern TMbcbConnection MbcbConnection;

// Predani parametru
typedef struct {
  int Address;                                  // Pocatecni adresa
  int Count;                                    // Pocet registru v Data[]
  int Data[MB_MAX_READ_INPUT_REGISTERS_COUNT];
} TMbcbRegisters;
extern TMbcbRegisters MbcbRegisters;

// Chyba posledniho prikazu
extern byte MbcbError;

// Vyjimka posledniho prikazu
extern byte MbcbException;

// Pouzite hejno pro cteni
extern byte MbcbFlockNumber;

// Diagnosticke citace
typedef struct {
  int Total;
  int Error;
  int Exception;
  int Message;
  int Response;
  int Nak;
  int Busy;
  int Overrun;
} TMbcbCounters;
extern TMbcbCounters MbcbCounters;

// Nastaveni Modbusu
typedef struct {
  int Speed;            // Rychlost v baudech
  int Parity;           // Typ parity
  int ReplyDelay;       // Zpozdeni mezi prijmem prikazu a odpovedi v milisekundach, kvantovani s TIMER0_PERIOD
  int SilentInterval;   // Mezera mezi dvema pakety MODBUS v milisekundach
  int Protocol;         // Typ komunikacniho protokolu
} TMbcbModbusSetup;
extern TMbcbModbusSetup MbcbModbusSetup;

#endif // MBCBPAR_H

