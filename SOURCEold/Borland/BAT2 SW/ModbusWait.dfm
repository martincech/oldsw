object ModbusWaitForm: TModbusWaitForm
  Left = 507
  Top = 431
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Wait'
  ClientHeight = 54
  ClientWidth = 383
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object WaitLabel: TLabel
    Left = 16
    Top = 21
    Width = 273
    Height = 13
    AutoSize = False
    Caption = 'WaitLabel'
  end
  object CancelButton: TButton
    Left = 296
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 0
    OnClick = CancelButtonClick
  end
end
