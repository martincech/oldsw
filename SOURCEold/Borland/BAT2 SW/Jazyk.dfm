object JazykForm: TJazykForm
  Left = 335
  Top = 236
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Language'
  ClientHeight = 303
  ClientWidth = 163
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 86
    Height = 13
    Caption = 'Choose language:'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 248
    Width = 129
    Height = 9
    Shape = bsTopLine
  end
  object AnglickyRadioButton: TRadioButton
    Left = 24
    Top = 96
    Width = 100
    Height = 17
    Caption = 'English'
    Checked = True
    TabOrder = 2
    TabStop = True
  end
  object FrancouzskyRadioButton: TRadioButton
    Left = 24
    Top = 144
    Width = 100
    Height = 17
    Caption = 'Fran�ais'
    TabOrder = 4
  end
  object Button1: TButton
    Left = 71
    Top = 264
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 8
  end
  object FinskyRadioButton: TRadioButton
    Left = 24
    Top = 192
    Width = 100
    Height = 17
    Caption = 'Suomi'
    TabOrder = 6
  end
  object TureckyRadioButton: TRadioButton
    Left = 24
    Top = 216
    Width = 100
    Height = 17
    Caption = 'Turkce'
    TabOrder = 7
  end
  object DanskyRadioButton: TRadioButton
    Left = 24
    Top = 48
    Width = 100
    Height = 17
    Caption = 'Dansk'
    TabOrder = 0
  end
  object SpanelskyRadioButton: TRadioButton
    Left = 24
    Top = 120
    Width = 100
    Height = 17
    Caption = 'Espanol'
    TabOrder = 3
  end
  object NemeckyRadioButton: TRadioButton
    Left = 24
    Top = 72
    Width = 100
    Height = 17
    Caption = 'Deutsch'
    TabOrder = 1
  end
  object RuskyRadioButton: TRadioButton
    Left = 24
    Top = 168
    Width = 100
    Height = 17
    Caption = 'Russian'
    TabOrder = 5
  end
end
