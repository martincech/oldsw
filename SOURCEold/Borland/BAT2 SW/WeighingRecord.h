//---------------------------------------------------------------------------

#ifndef WeighingRecordH
#define WeighingRecordH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include "DM.h"

//---------------------------------------------------------------------------

class TWeighingRecordForm : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TButton *Button2;
        TGroupBox *GroupBox1;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *InsertLabel;
        TLabel *EditLabel;
        TLabel *GSMLabel;
        TGroupBox *GroupBox2;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TEdit *ScalesEdit;
        TEdit *DayEdit;
        TDateTimePicker *DateTimePicker;
        TGroupBox *GroupBox3;
        TCheckBox *BothGendersCheckBox;
        TGroupBox *FemalesGroupBox;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TLabel *Label9;
        TEdit *FemalesCountEdit;
        TEdit *FemalesAverageEdit;
        TEdit *FemalesGainEdit;
        TEdit *FemalesSigmaEdit;
        TEdit *FemalesCvEdit;
        TEdit *FemalesUniEdit;
        TGroupBox *MalesGroupBox;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *Label13;
        TLabel *Label14;
        TLabel *Label15;
        TEdit *MalesCountEdit;
        TEdit *MalesAverageEdit;
        TEdit *MalesGainEdit;
        TEdit *MalesSigmaEdit;
        TEdit *MalesCvEdit;
        TEdit *MalesUniEdit;
        TLabel *Label19;
        TEdit *NoteEdit;
        TButton *PrintButton;
        void __fastcall ScalesEditKeyPress(TObject *Sender, char &Key);
        void __fastcall FemalesAverageEditKeyPress(TObject *Sender,
          char &Key);
        void __fastcall BothGendersCheckBoxClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FemalesGainEditKeyPress(TObject *Sender,
          char &Key);
        void __fastcall PrintButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TWeighingRecordForm(TComponent* Owner);
        bool CheckIntEdit(TEdit *Edit);
        bool CheckDoubleEdit(TEdit *Edit);
        bool CheckValues();
};
//---------------------------------------------------------------------------
extern PACKAGE TWeighingRecordForm *WeighingRecordForm;
//---------------------------------------------------------------------------
#endif
