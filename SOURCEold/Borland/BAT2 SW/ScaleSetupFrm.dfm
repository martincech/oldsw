object ScaleSetupFrame: TScaleSetupFrame
  Left = 0
  Top = 0
  Width = 452
  Height = 496
  TabOrder = 0
  object ScaleSetupPageControl: TPageControl
    Left = 8
    Top = 8
    Width = 437
    Height = 481
    ActivePage = CorrectionTabSheet
    TabOrder = 0
    object IdentificationTabSheet: TTabSheet
      Caption = 'Identification'
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 101
        Height = 13
        Caption = 'Identification number:'
      end
      object IdentificationNumberEdit: TEdit
        Left = 16
        Top = 32
        Width = 121
        Height = 21
        MaxLength = 5
        TabOrder = 0
        Text = 'IdentificationNumberEdit'
      end
    end
    object FlocksTabSheet: TTabSheet
      Caption = 'Flocks'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 8
        Top = 8
        Width = 105
        Height = 433
        Caption = 'Flocks'
        TabOrder = 0
        object FlocksListBox: TListBox
          Left = 16
          Top = 24
          Width = 73
          Height = 313
          ItemHeight = 13
          TabOrder = 0
          OnClick = FlocksListBoxClick
        end
        object NewFlockButton: TButton
          Left = 16
          Top = 352
          Width = 75
          Height = 25
          Caption = 'New'
          TabOrder = 1
          OnClick = NewFlockButtonClick
        end
        object DeleteFlockButton: TButton
          Left = 16
          Top = 392
          Width = 75
          Height = 25
          Caption = 'Delete'
          TabOrder = 2
          OnClick = DeleteFlockButtonClick
        end
      end
      object GroupBox2: TGroupBox
        Left = 128
        Top = 8
        Width = 289
        Height = 433
        Caption = 'Flock parameters'
        TabOrder = 1
        object Label22: TLabel
          Left = 16
          Top = 24
          Width = 31
          Height = 13
          Caption = 'Name:'
        end
        object Label24: TLabel
          Left = 16
          Top = 120
          Width = 57
          Height = 13
          Caption = 'Weigh from:'
        end
        object Label25: TLabel
          Left = 152
          Top = 120
          Width = 12
          Height = 13
          Caption = 'till:'
        end
        object FlockGendersCheckBox: TCheckBox
          Left = 16
          Top = 72
          Width = 209
          Height = 17
          Caption = 'Use both genders'
          TabOrder = 1
          OnClick = FlockGendersCheckBoxClick
        end
        object FlockCurvesCheckBox: TCheckBox
          Left = 16
          Top = 96
          Width = 209
          Height = 17
          Caption = 'Use growth curves'
          TabOrder = 2
          OnClick = FlockGendersCheckBoxClick
        end
        object FlockNameEdit: TEdit
          Left = 16
          Top = 40
          Width = 121
          Height = 21
          MaxLength = 8
          TabOrder = 0
          Text = 'FlockNameEdit'
        end
        object FlockWeighFromEdit: TEdit
          Left = 16
          Top = 136
          Width = 121
          Height = 21
          MaxLength = 2
          TabOrder = 3
          Text = 'FlockWeighFromEdit'
        end
        object FlockWeighTillEdit: TEdit
          Left = 152
          Top = 136
          Width = 121
          Height = 21
          MaxLength = 2
          TabOrder = 4
          Text = 'FlockWeighTillEdit'
        end
        object FlockCurvesPageControl: TPageControl
          Left = 16
          Top = 176
          Width = 257
          Height = 201
          ActivePage = FlockCurveFTabSheet
          TabOrder = 5
          object FlockCurveFTabSheet: TTabSheet
            Caption = 'Females'
            object FlockCurveFStringGrid: TStringGrid
              Left = 0
              Top = 0
              Width = 249
              Height = 129
              Align = alTop
              ColCount = 3
              DefaultRowHeight = 16
              RowCount = 31
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
              TabOrder = 0
              ColWidths = (
                25
                81
                107)
              RowHeights = (
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16)
            end
            object FlockCurveFLoadButton: TButton
              Left = 167
              Top = 140
              Width = 75
              Height = 25
              Caption = 'Load'
              TabOrder = 1
              OnClick = FlockCurveFLoadButtonClick
            end
          end
          object FlockCurveMTabSheet: TTabSheet
            Caption = 'Males'
            ImageIndex = 1
            object FlockCurveMLoadButton: TButton
              Left = 167
              Top = 140
              Width = 75
              Height = 25
              Caption = 'Load'
              TabOrder = 0
              OnClick = FlockCurveFLoadButtonClick
            end
            object FlockCurveMStringGrid: TStringGrid
              Left = 0
              Top = 0
              Width = 249
              Height = 129
              Align = alTop
              ColCount = 3
              DefaultRowHeight = 16
              RowCount = 31
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
              TabOrder = 1
              ColWidths = (
                25
                81
                107)
              RowHeights = (
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16)
            end
          end
          object FlockWeightTabSheet: TTabSheet
            Caption = 'Initial weight'
            ImageIndex = 2
            object Label26: TLabel
              Left = 16
              Top = 16
              Width = 42
              Height = 13
              Caption = 'Females:'
            end
            object FlockWeightMPanel: TPanel
              Left = 16
              Top = 64
              Width = 217
              Height = 65
              BevelOuter = bvNone
              TabOrder = 1
              object Label27: TLabel
                Left = 0
                Top = 0
                Width = 31
                Height = 13
                Caption = 'Males:'
              end
              object FlockWeightMEdit: TEdit
                Left = 0
                Top = 16
                Width = 121
                Height = 21
                TabOrder = 0
                Text = 'FlockWeightMEdit'
              end
            end
            object FlockWeightFEdit: TEdit
              Left = 16
              Top = 32
              Width = 121
              Height = 21
              TabOrder = 0
              Text = 'FlockWeightFEdit'
            end
          end
        end
        object SaveFlockButton: TButton
          Left = 198
          Top = 392
          Width = 75
          Height = 25
          Caption = 'Save'
          TabOrder = 6
          OnClick = SaveFlockButtonClick
        end
      end
    end
    object StatisticsTabSheet: TTabSheet
      Caption = 'Statistics'
      ImageIndex = 2
      object Label2: TLabel
        Left = 16
        Top = 16
        Width = 80
        Height = 13
        Caption = 'Histogram range:'
      end
      object Label3: TLabel
        Left = 16
        Top = 64
        Width = 79
        Height = 13
        Caption = 'Uniformity range:'
      end
      object Label5: TLabel
        Left = 16
        Top = 36
        Width = 6
        Height = 13
        Caption = '�'
      end
      object Label6: TLabel
        Left = 152
        Top = 36
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label4: TLabel
        Left = 16
        Top = 84
        Width = 6
        Height = 13
        Caption = '�'
      end
      object Label7: TLabel
        Left = 152
        Top = 84
        Width = 8
        Height = 13
        Caption = '%'
      end
      object StatisticsHistogramRangeEdit: TEdit
        Left = 26
        Top = 32
        Width = 121
        Height = 21
        MaxLength = 3
        TabOrder = 0
        Text = 'StatisticsHistogramRangeEdit'
      end
      object StatisticsUniformityRangeEdit: TEdit
        Left = 26
        Top = 80
        Width = 121
        Height = 21
        MaxLength = 2
        TabOrder = 1
        Text = 'StatisticsUniformityRangeEdit'
      end
    end
    object GsmTabSheet: TTabSheet
      Caption = 'GSM'
      ImageIndex = 3
      object Label8: TLabel
        Left = 40
        Top = 64
        Width = 33
        Height = 13
        Caption = 'Period:'
      end
      object Label9: TLabel
        Left = 168
        Top = 84
        Width = 22
        Height = 13
        Caption = 'days'
      end
      object Label10: TLabel
        Left = 16
        Top = 168
        Width = 45
        Height = 13
        Caption = 'Numbers:'
      end
      object GsmUseCheckBox: TCheckBox
        Left = 16
        Top = 16
        Width = 193
        Height = 17
        Caption = 'Use GSM communication'
        TabOrder = 0
      end
      object GsmMidnightCheckBox: TCheckBox
        Left = 16
        Top = 40
        Width = 201
        Height = 17
        Caption = 'Send statistics at midnight'
        TabOrder = 1
      end
      object GsmPeriodEdit: TEdit
        Left = 40
        Top = 80
        Width = 121
        Height = 21
        MaxLength = 2
        TabOrder = 2
        Text = 'GsmPeriodEdit'
      end
      object GsmRequestCheckBox: TCheckBox
        Left = 16
        Top = 112
        Width = 201
        Height = 17
        Caption = 'Send data upon request'
        TabOrder = 3
      end
      object GsmCheckCheckBox: TCheckBox
        Left = 16
        Top = 136
        Width = 185
        Height = 17
        Caption = 'Check numbers'
        TabOrder = 4
      end
      object GsmNumber1Edit: TEdit
        Left = 16
        Top = 184
        Width = 121
        Height = 21
        MaxLength = 15
        TabOrder = 5
        Text = 'GsmNumber1Edit'
      end
      object GsmNumber2Edit: TEdit
        Left = 16
        Top = 208
        Width = 121
        Height = 21
        MaxLength = 15
        TabOrder = 6
        Text = 'GsmNumber2Edit'
      end
      object GsmNumber3Edit: TEdit
        Left = 16
        Top = 232
        Width = 121
        Height = 21
        MaxLength = 15
        TabOrder = 7
        Text = 'GsmNumber3Edit'
      end
      object GsmNumber4Edit: TEdit
        Left = 16
        Top = 256
        Width = 121
        Height = 21
        MaxLength = 15
        TabOrder = 8
        Text = 'GsmNumber4Edit'
      end
      object GsmNumber5Edit: TEdit
        Left = 16
        Top = 280
        Width = 121
        Height = 21
        MaxLength = 15
        TabOrder = 9
        Text = 'GsmNumber5Edit'
      end
    end
    object Rs485TabSheet: TTabSheet
      Caption = 'RS-485'
      ImageIndex = 4
      object Label11: TLabel
        Left = 16
        Top = 16
        Width = 34
        Height = 13
        Caption = 'Speed:'
      end
      object Label12: TLabel
        Left = 144
        Top = 36
        Width = 13
        Height = 13
        Caption = 'Bd'
      end
      object Label13: TLabel
        Left = 16
        Top = 64
        Width = 29
        Height = 13
        Caption = 'Parity:'
      end
      object Label14: TLabel
        Left = 16
        Top = 160
        Width = 58
        Height = 13
        Caption = 'Reply delay:'
      end
      object Label15: TLabel
        Left = 144
        Top = 180
        Width = 13
        Height = 13
        Caption = 'ms'
      end
      object Label16: TLabel
        Left = 16
        Top = 208
        Width = 66
        Height = 13
        Caption = 'Silent interval:'
      end
      object Label17: TLabel
        Left = 144
        Top = 228
        Width = 13
        Height = 13
        Caption = 'ms'
      end
      object Label18: TLabel
        Left = 16
        Top = 112
        Width = 42
        Height = 13
        Caption = 'Protocol:'
      end
      object Rs485SpeedComboBox: TComboBox
        Left = 16
        Top = 32
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        Items.Strings = (
          '1200'
          '2400'
          '4800'
          '9600'
          '19200'
          '38400'
          '57600'
          '115200')
      end
      object Rs485ParityComboBox: TComboBox
        Left = 16
        Top = 80
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          '8-n-1'
          '8-e-1'
          '8-o-1'
          '8-n-2')
      end
      object Rs485ReplyDelayEdit: TEdit
        Left = 16
        Top = 176
        Width = 121
        Height = 21
        MaxLength = 4
        TabOrder = 3
        Text = 'Rs485ReplyDelayEdit'
      end
      object Rs485SilentIntervalEdit: TEdit
        Left = 16
        Top = 224
        Width = 121
        Height = 21
        MaxLength = 3
        TabOrder = 4
        Text = 'Rs485SilentIntervalEdit'
      end
      object Rs485ProtocolComboBox: TComboBox
        Left = 16
        Top = 128
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        Items.Strings = (
          'MODBUS RTU'
          'MODBUS ASCII')
      end
    end
    object ScaleTabSheet: TTabSheet
      Caption = 'Scale'
      ImageIndex = 5
      object Label19: TLabel
        Left = 16
        Top = 16
        Width = 152
        Height = 13
        Caption = 'Margin above target for females:'
      end
      object Label20: TLabel
        Left = 232
        Top = 16
        Width = 149
        Height = 13
        Caption = 'Margin under target for females:'
      end
      object Label21: TLabel
        Left = 16
        Top = 64
        Width = 143
        Height = 13
        Caption = 'Margin above target for males:'
      end
      object Label23: TLabel
        Left = 232
        Top = 64
        Width = 140
        Height = 13
        Caption = 'Margin under target for males:'
      end
      object Label28: TLabel
        Left = 16
        Top = 112
        Width = 25
        Height = 13
        Caption = 'Filter:'
      end
      object Label29: TLabel
        Left = 16
        Top = 160
        Width = 59
        Height = 13
        Caption = 'Stabilization:'
      end
      object Label30: TLabel
        Left = 16
        Top = 208
        Width = 81
        Height = 13
        Caption = 'Stabilization time:'
      end
      object Label31: TLabel
        Left = 144
        Top = 36
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label32: TLabel
        Left = 360
        Top = 36
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label33: TLabel
        Left = 144
        Top = 84
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label34: TLabel
        Left = 360
        Top = 84
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label35: TLabel
        Left = 16
        Top = 280
        Width = 91
        Height = 13
        Caption = 'Save sample upon:'
      end
      object Label36: TLabel
        Left = 16
        Top = 328
        Width = 27
        Height = 13
        Caption = 'Units:'
      end
      object Label38: TLabel
        Left = 144
        Top = 180
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label39: TLabel
        Left = 6
        Top = 180
        Width = 6
        Height = 13
        Caption = '�'
      end
      object ScaleMarginAboveFEdit: TEdit
        Left = 16
        Top = 32
        Width = 121
        Height = 21
        MaxLength = 2
        TabOrder = 0
        Text = 'ScaleMarginAboveFEdit'
      end
      object ScaleMarginBelowFEdit: TEdit
        Left = 232
        Top = 32
        Width = 121
        Height = 21
        MaxLength = 2
        TabOrder = 1
        Text = 'ScaleMarginBelowFEdit'
      end
      object ScaleMarginAboveMEdit: TEdit
        Left = 16
        Top = 80
        Width = 121
        Height = 21
        MaxLength = 2
        TabOrder = 2
        Text = 'ScaleMarginAboveMEdit'
      end
      object ScaleMarginBelowMEdit: TEdit
        Left = 232
        Top = 80
        Width = 121
        Height = 21
        MaxLength = 2
        TabOrder = 3
        Text = 'ScaleMarginBelowMEdit'
      end
      object ScaleFilterEdit: TEdit
        Left = 16
        Top = 128
        Width = 121
        Height = 21
        MaxLength = 2
        TabOrder = 4
        Text = 'ScaleFilterEdit'
      end
      object ScaleStabilizationEdit: TEdit
        Left = 16
        Top = 176
        Width = 121
        Height = 21
        MaxLength = 4
        TabOrder = 5
        Text = 'ScaleStabilizationEdit'
      end
      object ScaleStabilizationTimeEdit: TEdit
        Left = 16
        Top = 224
        Width = 121
        Height = 21
        MaxLength = 1
        TabOrder = 6
        Text = 'ScaleStabilizationTimeEdit'
      end
      object ScaleGainCheckBox: TCheckBox
        Left = 16
        Top = 256
        Width = 345
        Height = 17
        Caption = 'Use gain in automatic mode'
        TabOrder = 7
      end
      object ScaleSaveUponComboBox: TComboBox
        Left = 16
        Top = 296
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 8
        Items.Strings = (
          'Entering the scale'
          'Leaving the scale'
          'Both')
      end
      object ScaleUnitsComboBox: TComboBox
        Left = 16
        Top = 344
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 9
        Items.Strings = (
          'kg'
          'lb')
      end
    end
    object CorrectionTabSheet: TTabSheet
      Caption = 'Correction'
      ImageIndex = 7
      object Label40: TLabel
        Left = 16
        Top = 48
        Width = 31
        Height = 13
        Caption = 'Day 1:'
      end
      object Label41: TLabel
        Left = 6
        Top = 164
        Width = 6
        Height = 13
        Caption = '+'
      end
      object Label42: TLabel
        Left = 144
        Top = 164
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label43: TLabel
        Left = 16
        Top = 96
        Width = 31
        Height = 13
        Caption = 'Day 2:'
      end
      object Label44: TLabel
        Left = 16
        Top = 144
        Width = 51
        Height = 13
        Caption = 'Correction:'
      end
      object CorrectionDay1Edit: TEdit
        Left = 16
        Top = 64
        Width = 121
        Height = 21
        MaxLength = 3
        TabOrder = 0
        Text = 'CorrectionDay1Edit'
      end
      object CorrectionDay2Edit: TEdit
        Left = 16
        Top = 112
        Width = 121
        Height = 21
        MaxLength = 3
        TabOrder = 1
        Text = 'CorrectionDay2Edit'
      end
      object CorrectionCorrectionEdit: TEdit
        Left = 16
        Top = 160
        Width = 121
        Height = 21
        MaxLength = 4
        TabOrder = 2
        Text = 'CorrectionCorrectionEdit'
      end
      object CorrectionUseCheckBox: TCheckBox
        Left = 16
        Top = 16
        Width = 337
        Height = 17
        Caption = 'Use correction curve'
        TabOrder = 3
      end
    end
    object BacklightTabSheet: TTabSheet
      Caption = 'Backlight'
      ImageIndex = 6
      object Label37: TLabel
        Left = 16
        Top = 16
        Width = 47
        Height = 13
        Caption = 'Backlight:'
      end
      object BacklightComboBox: TComboBox
        Left = 16
        Top = 32
        Width = 121
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        Items.Strings = (
          'Off'
          'On'
          'Auto')
      end
    end
  end
end
