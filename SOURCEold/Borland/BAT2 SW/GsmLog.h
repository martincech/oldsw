//---------------------------------------------------------------------------
// Logovani GSM komunikace
//---------------------------------------------------------------------------

#ifndef GsmLogH
#define GsmLogH
//---------------------------------------------------------------------------

bool GsmLogStart(void);
  // Otevre soubor logu a vymaze jeho obsah. Pri uspesnem startu logovani vrati true.

bool GsmLogWriteString(String Str);
  // Zapise do souboru string <Str>. Pri uspesnem zapisu vrati true.

bool GsmLogWriteLine(String Str);
  // Zapise do souboru radek <Str>. Pri uspesnem zapisu vrati true.

bool GsmLogStop(void);
  // Zavre soubor logu a ukonci logovani.

#endif
