//---------------------------------------------------------------------------

#ifndef ModbusErrorsH
#define ModbusErrorsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
//---------------------------------------------------------------------------
class TModbusErrorsForm : public TForm
{
__published:	// IDE-managed Components
        TStringGrid *ScalesStringGrid;
        TButton *OkButton;
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TModbusErrorsForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusErrorsForm *ModbusErrorsForm;
//---------------------------------------------------------------------------
#endif
