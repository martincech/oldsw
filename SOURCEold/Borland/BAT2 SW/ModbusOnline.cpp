//***************************************************************************
//
//    ModbusOnline.cpp - Online table of scales
//    Version 1.0
//
//***************************************************************************

#include "ModbusOnline.h"


//---------------------------------------------------------------------------
// Konstruktor
//---------------------------------------------------------------------------

TModbusOnline::TModbusOnline() {
  ActiveScale      = NULL;
  ActiveScaleIndex = 0;
}

//---------------------------------------------------------------------------
// Smazani vsech vah
//---------------------------------------------------------------------------

void TModbusOnline::ClearScaleList() {
  // Smaze vsechny vahy ze seznamu
  for (vector<TModbusOnlineScale *>::iterator Iterator = ScaleList.begin(); Iterator != ScaleList.end(); Iterator++) {
    delete (*Iterator);         // Smazu vsechny vahy
  }
  // Smazu seznam
  ScaleList.clear();
}

//---------------------------------------------------------------------------
// Smazani dat vsech vah
//---------------------------------------------------------------------------

void TModbusOnline::ClearScaleData() {
  // Vynuluje data u vsech vah
  for (vector<TModbusOnlineScale *>::iterator Iterator = ScaleList.begin(); Iterator != ScaleList.end(); Iterator++) {
    (*Iterator)->Clear();
  }
}

//---------------------------------------------------------------------------
// Pocet vah v seznamu
//---------------------------------------------------------------------------

int TModbusOnline::ScalesCount() {
  // Vrati pocet vah v seznamu
  return ScaleList.size();
}

//---------------------------------------------------------------------------
// Pridani nove vahy
//---------------------------------------------------------------------------

void TModbusOnline::AddScale(TModbusScale *Scale) {
  // Prida vahu <Scale> do seznamu
  TModbusOnlineScale *OnlineScale = new TModbusOnlineScale();
  OnlineScale->SetAddress(Scale->GetAddress());
  OnlineScale->SetPort(Scale->GetPort());
  OnlineScale->SetName(Scale->GetName());
  ScaleList.push_back(OnlineScale);
}

//---------------------------------------------------------------------------
// Vraceni vahy
//---------------------------------------------------------------------------

TModbusOnlineScale *TModbusOnline::GetScale(int Index) {
  // Vrati vahu s indexem <Index>
  if (ScaleList.size() == 0) {
    return NULL;                // Seznam vah je prazdny
  }
  if (Index >= (int)ScaleList.size()) {
    return NULL;                // Index mimo seznam
  }

  return ScaleList.at(Index);
}

//---------------------------------------------------------------------------
// Nahrani vahy
//---------------------------------------------------------------------------

bool TModbusOnline::LoadScale(int Index) {
  // Nahraje vahu s indexem <Index> jako aktivni

  // Nahraju vahu
  ActiveScale = GetScale(Index);
  if (!ActiveScale) {
    return false;       // Seznam prazdny nebo index mimo seznam
  }
  ActiveScaleIndex = Index;

  // Pripojim se
  if (!ActiveScale->Connect()) {
    return false;
  }

  ActiveScale->InitExecute();

  return true;
}

//---------------------------------------------------------------------------
// Nahrani prvni vahy
//---------------------------------------------------------------------------

bool TModbusOnline::LoadFirstScale() {
  // Nahraje prvni vahu jako aktivni
  return LoadScale(0);
}

//---------------------------------------------------------------------------
// Posun na dalsi vahu
//---------------------------------------------------------------------------

bool TModbusOnline::LoadNextScale() {
  // Nahraje dalsi vahu v seznamu jako aktivni
  ActiveScaleIndex++;
  if (ActiveScaleIndex >= (int)ScaleList.size()) {
    ActiveScaleIndex = 0;               // Zpet na prvni vahu
  }
  return LoadScale(ActiveScaleIndex);
}

//---------------------------------------------------------------------------
// Inicializace
//---------------------------------------------------------------------------

bool TModbusOnline::InitExecute(int NewRepetitions) {
  // Inicializace akce
  if (ScaleList.size() == 0) {
    return false;               // Seznam vah je prazdny
  }

  Repetitions = NewRepetitions;

  return LoadFirstScale();
}

//---------------------------------------------------------------------------
// Akce
//---------------------------------------------------------------------------

bool TModbusOnline::Execute() {
  // Samotna akce, po nacteni vsech dat aktualni vahy vrati true
  if (ScaleList.size() == 0) {
    return false;               // Seznam vah je prazdny
  }
  if (!ActiveScale) {
    return false;               // Neni nahrana zadna vaha
  }

  if (ActiveScale->ExecuteNextCommand(Repetitions) == ONLINE_EXECUTION_RUNNING) {
    // Nacitani dat jeste probiha, pokracuju
    return false;
  }

  // Bud jsem nacetl vsechno, nebo doslo k chybe komunikace => prejdu na dalsi vahu
  LoadNextScale();
  return true;
}

