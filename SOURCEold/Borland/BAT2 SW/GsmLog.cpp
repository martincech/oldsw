//---------------------------------------------------------------------------
// Logovani GSM komunikace
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "GsmLog.h"
#include "DM.h"         // Nazev vahy

//---------------------------------------------------------------------------

#pragma package(smart_init)


#define GSM_LOG_FILE    (Data->Version.ScaleTitle + "Gsm.log")          // Soubor s logem
#define GSM_LOG_CRLF    "\r\n"                                          // CRLF v textovem souboru
#define GSM_LOG_SPACE   "   "                                           // Mezera mezi casem a textem
#define GSM_LOG_HEADER  (Data->Version.ScaleTitle + " GSM log file")    // Text hlavicky na 1. radku
#define GSM_LOG_STOP    "STOP"                                          // Text ukonceni logovani


// Lokalni promenne:

int LogFile;


//---------------------------------------------------------------------------
// Start logovani
//---------------------------------------------------------------------------

bool GsmLogStart(void) {
  // Otevre soubor logu a vymaze jeho obsah. Pri uspesnem startu logovani vrati true.
  LogFile = -1;         // Default zadny soubor
  // Smazu jiz existujici log
  if (FileExists(Data->SpecialFolders->Working + GSM_LOG_FILE)) {
    if (!DeleteFile(Data->SpecialFolders->Working + GSM_LOG_FILE)) {
      return false;     // Nepodarilo se smazat log
    }
  }
  // Vytvorim novy soubor
  LogFile = FileCreate(Data->SpecialFolders->Working + GSM_LOG_FILE);
  if (LogFile < 0) {
    return false;       // Nepodarilo se
  }
  // Zapisu hlavicku
  if (!GsmLogWriteLine(GSM_LOG_HEADER)) {
    return false;
  }
  if (!GsmLogWriteLine("")) {   // Prazdny radek
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Zapis stringu
//---------------------------------------------------------------------------

bool GsmLogWriteString(String Str) {
  // Zapise do souboru string <Str>. Pri uspesnem zapisu vrati true.
  if (LogFile < 0) {
    return false;       // Soubor neni otevren
  }
  if (FileWrite(LogFile, Str.c_str(), Str.Length()) < 0) {
    return false;       // Zapis se nepodaril
  }
  return true;
}

//---------------------------------------------------------------------------
// Zapis radku
//---------------------------------------------------------------------------

bool GsmLogWriteLine(String Str) {
  // Zapise do souboru radek <Str>. Pri uspesnem zapisu vrati true.
  if (Str != "") {
    Str = Now().DateTimeString() + GSM_LOG_SPACE + Str;         // Pridam datum a cas (jen pokud nechce pouze prazdny radek)
  }
  return GsmLogWriteString(Str + GSM_LOG_CRLF);
}

//---------------------------------------------------------------------------
// Stop logovani
//---------------------------------------------------------------------------

bool GsmLogStop(void) {
  // Zavre soubor logu a ukonci logovani.
  if (LogFile < 0) {
    return false;       // Soubor neni otevren
  }
  GsmLogWriteLine("");
  GsmLogWriteLine(GSM_LOG_STOP);
  FileClose(LogFile);
  LogFile = -1;         // Default zadny soubor
  return true;
}

