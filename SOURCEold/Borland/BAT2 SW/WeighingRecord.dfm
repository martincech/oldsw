object WeighingRecordForm: TWeighingRecordForm
  Left = 212
  Top = 414
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Record'
  ClientHeight = 311
  ClientWidth = 595
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 408
    Top = 272
    Width = 75
    Height = 25
    Caption = 'Save'
    Default = True
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 502
    Top = 272
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 16
    Width = 225
    Height = 97
    Caption = 'Record information'
    TabOrder = 0
    object Label16: TLabel
      Left = 16
      Top = 24
      Width = 41
      Height = 13
      Caption = 'Inserted:'
    end
    object Label17: TLabel
      Left = 16
      Top = 48
      Width = 43
      Height = 13
      Caption = 'Last edit:'
    end
    object Label18: TLabel
      Left = 16
      Top = 72
      Width = 65
      Height = 13
      Caption = 'GSM number:'
    end
    object InsertLabel: TLabel
      Left = 112
      Top = 24
      Width = 110
      Height = 13
      AutoSize = False
      Caption = 'InsertLabel'
    end
    object EditLabel: TLabel
      Left = 112
      Top = 48
      Width = 110
      Height = 13
      AutoSize = False
      Caption = 'EditLabel'
    end
    object GSMLabel: TLabel
      Left = 112
      Top = 72
      Width = 110
      Height = 13
      AutoSize = False
      Caption = 'GSMLabel'
    end
  end
  object GroupBox2: TGroupBox
    Left = 16
    Top = 128
    Width = 225
    Height = 129
    Caption = 'Scales'
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 24
      Width = 30
      Height = 13
      Caption = 'Scale:'
    end
    object Label2: TLabel
      Left = 16
      Top = 48
      Width = 22
      Height = 13
      Caption = 'Day:'
    end
    object Label3: TLabel
      Left = 16
      Top = 72
      Width = 26
      Height = 13
      Caption = 'Date:'
    end
    object Label19: TLabel
      Left = 16
      Top = 96
      Width = 26
      Height = 13
      Caption = 'Note:'
    end
    object ScalesEdit: TEdit
      Left = 120
      Top = 21
      Width = 89
      Height = 21
      MaxLength = 5
      TabOrder = 0
      OnKeyPress = ScalesEditKeyPress
    end
    object DayEdit: TEdit
      Left = 120
      Top = 45
      Width = 89
      Height = 21
      MaxLength = 3
      TabOrder = 1
      OnKeyPress = ScalesEditKeyPress
    end
    object DateTimePicker: TDateTimePicker
      Left = 120
      Top = 69
      Width = 89
      Height = 21
      CalAlignment = dtaLeft
      Date = 38650.6882599074
      Time = 38650.6882599074
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 2
    end
    object NoteEdit: TEdit
      Left = 120
      Top = 93
      Width = 89
      Height = 21
      MaxLength = 20
      TabOrder = 3
    end
  end
  object GroupBox3: TGroupBox
    Left = 256
    Top = 16
    Width = 321
    Height = 241
    Caption = 'Statistics'
    TabOrder = 2
    object BothGendersCheckBox: TCheckBox
      Left = 16
      Top = 24
      Width = 145
      Height = 17
      Caption = 'Use both genders'
      TabOrder = 0
      OnClick = BothGendersCheckBoxClick
    end
    object FemalesGroupBox: TGroupBox
      Left = 16
      Top = 48
      Width = 137
      Height = 177
      Caption = 'Females / all'
      TabOrder = 1
      object Label4: TLabel
        Left = 16
        Top = 24
        Width = 31
        Height = 13
        Caption = 'Count:'
      end
      object Label5: TLabel
        Left = 16
        Top = 48
        Width = 43
        Height = 13
        Caption = 'Average:'
      end
      object Label6: TLabel
        Left = 16
        Top = 72
        Width = 25
        Height = 13
        Caption = 'Gain:'
      end
      object Label7: TLabel
        Left = 16
        Top = 96
        Width = 40
        Height = 13
        Caption = 'St. dev.:'
      end
      object Label8: TLabel
        Left = 16
        Top = 120
        Width = 16
        Height = 13
        Caption = 'Cv:'
      end
      object Label9: TLabel
        Left = 16
        Top = 144
        Width = 19
        Height = 13
        Caption = 'Uni:'
      end
      object FemalesCountEdit: TEdit
        Left = 88
        Top = 21
        Width = 33
        Height = 21
        MaxLength = 5
        TabOrder = 0
        OnKeyPress = ScalesEditKeyPress
      end
      object FemalesAverageEdit: TEdit
        Left = 88
        Top = 45
        Width = 33
        Height = 21
        TabOrder = 1
        OnKeyPress = FemalesAverageEditKeyPress
      end
      object FemalesGainEdit: TEdit
        Left = 88
        Top = 69
        Width = 33
        Height = 21
        TabOrder = 2
        OnKeyPress = FemalesGainEditKeyPress
      end
      object FemalesSigmaEdit: TEdit
        Left = 88
        Top = 93
        Width = 33
        Height = 21
        TabOrder = 3
        OnKeyPress = FemalesAverageEditKeyPress
      end
      object FemalesCvEdit: TEdit
        Left = 88
        Top = 117
        Width = 33
        Height = 21
        MaxLength = 3
        TabOrder = 4
        OnKeyPress = ScalesEditKeyPress
      end
      object FemalesUniEdit: TEdit
        Left = 88
        Top = 141
        Width = 33
        Height = 21
        MaxLength = 3
        TabOrder = 5
        OnKeyPress = ScalesEditKeyPress
      end
    end
    object MalesGroupBox: TGroupBox
      Left = 168
      Top = 48
      Width = 137
      Height = 177
      Caption = 'Males'
      TabOrder = 2
      object Label10: TLabel
        Left = 16
        Top = 24
        Width = 31
        Height = 13
        Caption = 'Count:'
      end
      object Label11: TLabel
        Left = 16
        Top = 48
        Width = 43
        Height = 13
        Caption = 'Average:'
      end
      object Label12: TLabel
        Left = 16
        Top = 72
        Width = 25
        Height = 13
        Caption = 'Gain:'
      end
      object Label13: TLabel
        Left = 16
        Top = 96
        Width = 40
        Height = 13
        Caption = 'St. dev.:'
      end
      object Label14: TLabel
        Left = 16
        Top = 120
        Width = 16
        Height = 13
        Caption = 'Cv:'
      end
      object Label15: TLabel
        Left = 16
        Top = 144
        Width = 19
        Height = 13
        Caption = 'Uni:'
      end
      object MalesCountEdit: TEdit
        Left = 88
        Top = 21
        Width = 33
        Height = 21
        MaxLength = 5
        TabOrder = 0
        OnKeyPress = ScalesEditKeyPress
      end
      object MalesAverageEdit: TEdit
        Left = 88
        Top = 45
        Width = 33
        Height = 21
        TabOrder = 1
        OnKeyPress = FemalesAverageEditKeyPress
      end
      object MalesGainEdit: TEdit
        Left = 88
        Top = 69
        Width = 33
        Height = 21
        TabOrder = 2
        OnKeyPress = FemalesGainEditKeyPress
      end
      object MalesSigmaEdit: TEdit
        Left = 88
        Top = 93
        Width = 33
        Height = 21
        TabOrder = 3
        OnKeyPress = FemalesAverageEditKeyPress
      end
      object MalesCvEdit: TEdit
        Left = 88
        Top = 117
        Width = 33
        Height = 21
        MaxLength = 3
        TabOrder = 4
        OnKeyPress = ScalesEditKeyPress
      end
      object MalesUniEdit: TEdit
        Left = 88
        Top = 141
        Width = 33
        Height = 21
        MaxLength = 3
        TabOrder = 5
        OnKeyPress = ScalesEditKeyPress
      end
    end
  end
  object PrintButton: TButton
    Left = 16
    Top = 272
    Width = 75
    Height = 25
    Caption = 'Print'
    TabOrder = 3
    OnClick = PrintButtonClick
  end
end
