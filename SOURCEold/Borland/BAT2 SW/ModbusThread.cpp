//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusThread.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall Unit1::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall TModbusThread::TModbusThread(bool CreateSuspended)
        : TThread(CreateSuspended)
{
  Command       = THREAD_COMMAND_IDLE;        // Nic zatim nedelam
  Repetitions   = 1;
  Scale         = NULL;
  Busy          = false;
  CurrentNumber = 0;
}
//---------------------------------------------------------------------------
void __fastcall TModbusThread::Execute()
{
        //---- Place thread code here ----
  while (!Terminated) {

    if (!Scale) {
      continue;         // Vaha neni definovana
    }

    if (!Busy) {
      continue;         // Zadny prikaz se nema provadet
    }

    // Provedu pozadovany prikaz
    ErrorCounter = 0;
    switch (Command) {
      case THREAD_COMMAND_READ_CONFIG:
        do {
          Result = Scale->ReadConfig();
          if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
            break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
          }
        } while (++ErrorCounter < Repetitions && !Terminated);
        break;

      case THREAD_COMMAND_READ_FLOCK:
        do {
          Result = Scale->ReadFlock(CommandValue);
          if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
            break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
          }
        } while (++ErrorCounter < Repetitions && !Terminated);
        break;

      case THREAD_COMMAND_READ_FLOCKS:
        for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
          if (Terminated) {
            break;
          }
          CurrentNumber = i;
          ErrorCounter = 0;
          do {
            Result = Scale->ReadFlock(i);
            if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
              break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
            }
          } while (++ErrorCounter < Repetitions && !Terminated);
          if (Result != MB_RESULT_OK && Result != MB_RESULT_INVALID_VALUE) {
            // Chyba pri cteni, nastavim chybu a koncim s nacitanim
            Result = MB_RESULT_ERROR;           // U nektereho hejna doslo k chybe pri cteni, vsechna hejna se tedy nenacetla 
            break;
          }
          Result = MB_RESULT_OK;                // Nacteni hejna probehlo v poradku
        }
        break;

      case THREAD_COMMAND_READ_DATETIME:
        do {
          Result = Scale->ReadDateTime();
          if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
            break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
          }
        } while (++ErrorCounter < Repetitions && !Terminated);
        break;

      case THREAD_COMMAND_READ_TODAY:
        do {
          Result = Scale->ReadTodayFromArchive();
          if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
            break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
          }
        } while (++ErrorCounter < Repetitions && !Terminated);
        break;

      case THREAD_COMMAND_READ_ARCHIVE:
        // Zapis prvniho dne
        CurrentNumber = 0;
        do {
          Result = Scale->ReadFirstDayFromArchive();
          if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
            break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
          }
        } while (++ErrorCounter < Repetitions && !Terminated);
        if (Result != MB_RESULT_OK) {
          break;
        }

        while (!Terminated) {
          // Prejdu na dalsi den
          CurrentNumber++;
          ErrorCounter = 0;
          do {
            Result = Scale->ReadNextDayFromArchive();
            if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
              break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
            }
          } while (++ErrorCounter < Repetitions && !Terminated);
          if (Result != MB_RESULT_OK) {
            // Bud chyba komunikace, nebo uz dalsi den v pameti neni.
            if (Result == MB_RESULT_INVALID_VALUE) {
              Result = MB_RESULT_OK;    // Dosel jsem na posledni den - opravim Result na OK
            }
            break;
          }
        }
        break;

      case THREAD_COMMAND_READ_CALIBRATION:
        do {
          Result = Scale->ReadCalibration();
          if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
            break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
          }
        } while (++ErrorCounter < Repetitions && !Terminated);
        break;

/*      case THREAD_COMMAND_READ_COUNTERS:
        do {
          Result = Scale->ReadCounters();
          if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
            break;      // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
          }
        } while (++ErrorCounter < Repetitions && !Terminated);
        break;*/

    }//switch

    Busy = false;

  }//while
}
//---------------------------------------------------------------------------
