object RozsahDnuForm: TRozsahDnuForm
  Left = 492
  Top = 357
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Range'
  ClientHeight = 218
  ClientWidth = 205
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 95
    Top = 112
    Width = 3
    Height = 13
    Caption = '-'
  end
  object Label3: TLabel
    Left = 16
    Top = 16
    Width = 100
    Height = 13
    Caption = 'Select range of days:'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 144
    Width = 169
    Height = 10
    Shape = bsBottomLine
  end
  object OdEdit: TEdit
    Left = 56
    Top = 109
    Width = 33
    Height = 21
    MaxLength = 3
    TabOrder = 0
    OnKeyPress = OdEditKeyPress
  end
  object DoEdit: TEdit
    Left = 104
    Top = 109
    Width = 33
    Height = 21
    MaxLength = 3
    TabOrder = 1
    OnKeyPress = OdEditKeyPress
  end
  object Button1: TButton
    Left = 16
    Top = 176
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Button2: TButton
    Left = 112
    Top = 176
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object AktualniRadioButton: TRadioButton
    Left = 32
    Top = 40
    Width = 113
    Height = 17
    Caption = 'Actual day'
    TabOrder = 4
  end
  object VsechnyRadioButton: TRadioButton
    Left = 32
    Top = 64
    Width = 113
    Height = 17
    Caption = 'All days'
    TabOrder = 5
  end
  object RozsahRadioButton: TRadioButton
    Left = 32
    Top = 88
    Width = 105
    Height = 17
    Caption = 'Selected range:'
    TabOrder = 6
  end
end
