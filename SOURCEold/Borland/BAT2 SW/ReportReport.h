//----------------------------------------------------------------------------
#ifndef ReportReportH
#define ReportReportH
//----------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\QuickRpt.hpp>
#include <vcl\QRCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Qrctrls.hpp>
//----------------------------------------------------------------------------
class TReportReportForm : public TQuickRep
{
__published:
        TQRBand *TitleBand1;
        TQRLabel *QRLabel1;
        TQRSysData *QRSysData1;
        TQRLabel *QRLabel11;
        TQRLabel *SouborQRLabel;
        TQRBand *DetailBand1;
        TQRDBText *DayQRDBText;
        TQRDBText *DateQRDBText;
        TQRDBText *CountQRDBText;
        TQRDBText *AverageQRDBText;
        TQRBand *PageHeaderBand1;
        TQRLabel *QRLabel2;
        TQRLabel *QRLabel3;
        TQRLabel *QRLabel4;
        TQRLabel *QRLabel5;
        TQRBand *PageFooterBand1;
        TQRSysData *QRSysData2;
        TQRLabel *QRLabel6;
        TQRLabel *QRLabel7;
        TQRLabel *QRLabel8;
        TQRLabel *QRLabel9;
        TQRDBText *GainQRDBText;
        TQRDBText *SigmaQRDBText;
        TQRDBText *CvQRDBText;
        TQRDBText *UniQRDBText;
        TQRShape *CaraQRShape;
        TQRLabel *PohlaviNazevQRLabel;
        TQRLabel *PohlaviQRLabel;
        void __fastcall QuickRepBeforePrint(TCustomQuickRep *Sender,
          bool &PrintReport);
        void __fastcall DetailBand1BeforePrint(TQRCustomBand *Sender,
          bool &PrintBand);
private:
        int     Counter;
public:
   __fastcall TReportReportForm::TReportReportForm(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern TReportReportForm *ReportReportForm;
//----------------------------------------------------------------------------
#endif