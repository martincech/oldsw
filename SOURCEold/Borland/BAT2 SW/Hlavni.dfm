object HlavniForm: THlavniForm
  Left = 251
  Top = 269
  Width = 800
  Height = 550
  Caption = 'Bat2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 37
    Width = 784
    Height = 455
    ActivePage = KrivkaTabSheet
    Align = alClient
    TabOrder = 0
    Visible = False
    object ReportTabSheet: TTabSheet
      Caption = 'Report'
      ImageIndex = 10
      object ReportDBGrid: TDBGrid
        Left = 0
        Top = 57
        Width = 784
        Height = 382
        Align = alClient
        DataSource = Data.StatistikaQueryDataSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = ReportDBGridKeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'DAY_NUMBER'
            Title.Alignment = taCenter
            Title.Caption = 'Day'
            Visible = True
          end
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'DATE_TIME_DATE'
            Title.Alignment = taCenter
            Title.Caption = 'Date'
            Width = 74
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'STAT_COUNT'
            Title.Alignment = taCenter
            Title.Caption = 'Count'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AVERAGE_WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = 'Average'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GAIN'
            Title.Alignment = taCenter
            Title.Caption = 'Daily gain'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SIGMA'
            Title.Alignment = taCenter
            Title.Caption = 'St. deviation'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CV'
            Title.Alignment = taCenter
            Title.Caption = 'Cv [%]'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'UNI'
            Title.Alignment = taCenter
            Title.Caption = 'UNI [%]'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMPARE'
            Title.Alignment = taCenter
            Title.Caption = 'Compare'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DIFFERENCE'
            Title.Alignment = taCenter
            Title.Caption = 'Difference'
            Visible = True
          end>
      end
      object ReportPanel: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 57
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Panel4: TPanel
          Left = 384
          Top = 0
          Width = 400
          Height = 57
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object Label58: TLabel
            Left = 96
            Top = 19
            Width = 67
            Height = 13
            Alignment = taRightJustify
            Caption = 'Compare with:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object ReportPorovnatSpeedButton: TSpeedButton
            Left = 371
            Top = 16
            Width = 11
            Height = 21
            Caption = '...'
            Flat = True
            OnClick = Growthcurves1Click
          end
          object ReportPorovnatComboBox: TComboBox
            Left = 176
            Top = 16
            Width = 193
            Height = 21
            Style = csDropDownList
            ItemHeight = 0
            TabOrder = 0
            OnChange = KrivkaPorovnatComboBoxChange
          end
        end
      end
    end
    object VazeniTabSheet: TTabSheet
      Caption = 'Samples'
      ImageIndex = 2
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 439
        Align = alClient
        DataSource = Data.ZaznamyQueryDataSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SAMPLE'
            Title.Alignment = taCenter
            Title.Caption = 'Sample'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIME_HOUR'
            Title.Alignment = taCenter
            Title.Caption = 'Hour'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = 'Weight'
            Visible = True
          end>
      end
    end
    object StatistikaTabSheet: TTabSheet
      Caption = 'Statistics'
      ImageIndex = 1
      object Label3: TLabel
        Left = 16
        Top = 32
        Width = 34
        Height = 13
        Caption = 'Target:'
      end
      object CilovaHmotnostLabel: TLabel
        Left = 104
        Top = 32
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '00,000'
      end
      object Label5: TLabel
        Left = 16
        Top = 48
        Width = 43
        Height = 13
        Caption = 'Average:'
      end
      object PrumerLabel: TLabel
        Left = 104
        Top = 48
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '00,000'
      end
      object Label7: TLabel
        Left = 16
        Top = 88
        Width = 31
        Height = 13
        Caption = 'Count:'
      end
      object PocetLabel: TLabel
        Left = 107
        Top = 88
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = '00000'
      end
      object Label9: TLabel
        Left = 16
        Top = 104
        Width = 62
        Height = 13
        Caption = 'St. deviation:'
      end
      object SigmaLabel: TLabel
        Left = 110
        Top = 104
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = '0,000'
      end
      object Label11: TLabel
        Left = 16
        Top = 136
        Width = 49
        Height = 13
        Caption = 'Uniformity:'
      end
      object UniLabel: TLabel
        Left = 119
        Top = 136
        Width = 18
        Height = 13
        Alignment = taRightJustify
        Caption = '000'
      end
      object Label13: TLabel
        Left = 16
        Top = 120
        Width = 16
        Height = 13
        Caption = 'Cv:'
      end
      object CvLabel: TLabel
        Left = 119
        Top = 120
        Width = 18
        Height = 13
        Alignment = taRightJustify
        Caption = '000'
      end
      object Label15: TLabel
        Left = 16
        Top = 64
        Width = 49
        Height = 13
        Caption = 'Daily gain:'
      end
      object PrirustekLabel: TLabel
        Left = 104
        Top = 64
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '00,000'
      end
      object CilovaHmotnostJednotkyLabel: TLabel
        Left = 144
        Top = 32
        Width = 12
        Height = 13
        Caption = 'kg'
      end
      object PrumerJednotkyLabel: TLabel
        Left = 144
        Top = 48
        Width = 12
        Height = 13
        Caption = 'kg'
      end
      object PrirustekJednotkyLabel: TLabel
        Left = 144
        Top = 64
        Width = 12
        Height = 13
        Caption = 'kg'
      end
      object Label20: TLabel
        Left = 144
        Top = 136
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label21: TLabel
        Left = 144
        Top = 120
        Width = 8
        Height = 13
        Caption = '%'
      end
      object StatistikaPorovnaniPanel: TPanel
        Left = 176
        Top = 8
        Width = 145
        Height = 145
        BevelOuter = bvNone
        TabOrder = 0
        object CilovaHmotnostPorovnaniLabel: TLabel
          Left = 8
          Top = 24
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = '00,000'
        end
        object PrumerPorovnaniLabel: TLabel
          Left = 8
          Top = 40
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = '00,000'
        end
        object PocetPorovnaniLabel: TLabel
          Left = 11
          Top = 80
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = '00000'
        end
        object SigmaPorovnaniLabel: TLabel
          Left = 14
          Top = 96
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = '0,000'
        end
        object UniPorovnaniLabel: TLabel
          Left = 23
          Top = 128
          Width = 18
          Height = 13
          Alignment = taRightJustify
          Caption = '000'
        end
        object CvPorovnaniLabel: TLabel
          Left = 23
          Top = 112
          Width = 18
          Height = 13
          Alignment = taRightJustify
          Caption = '000'
        end
        object PrirustekPorovnaniLabel: TLabel
          Left = 8
          Top = 56
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = '00,000'
        end
        object CilovaHmotnostJednotkyPorovnaniLabel: TLabel
          Left = 48
          Top = 24
          Width = 12
          Height = 13
          Caption = 'kg'
        end
        object PrumerJednotkyPorovnaniLabel: TLabel
          Left = 48
          Top = 40
          Width = 12
          Height = 13
          Caption = 'kg'
        end
        object PrirustekJednotkyPorovnaniLabel: TLabel
          Left = 48
          Top = 56
          Width = 12
          Height = 13
          Caption = 'kg'
        end
        object Label22: TLabel
          Left = 48
          Top = 128
          Width = 8
          Height = 13
          Caption = '%'
        end
        object Label23: TLabel
          Left = 48
          Top = 112
          Width = 8
          Height = 13
          Caption = '%'
        end
        object StatistikaPorovnaniLabel: TLabel
          Left = 8
          Top = 4
          Width = 55
          Height = 13
          Caption = 'Comparison'
        end
      end
    end
    object HistogramTabSheet: TTabSheet
      Caption = 'Histogram'
      ImageIndex = 1
      object HistogramSplitter: TSplitter
        Left = 0
        Top = 268
        Width = 784
        Height = 2
        Cursor = crVSplit
        Align = alBottom
        Beveled = True
      end
      object HistogramChart: TChart
        Left = 0
        Top = 0
        Width = 784
        Height = 268
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 3
        MarginLeft = 1
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.ExactDateTime = False
        BottomAxis.Grid.Visible = False
        BottomAxis.Increment = 0.001
        BottomAxis.MinorTickLength = 1
        BottomAxis.Title.Caption = 'Weight'
        LeftAxis.Grid.Color = 11184810
        LeftAxis.Grid.Style = psSolid
        LeftAxis.Labels = False
        LeftAxis.Title.Caption = 'Count'
        Legend.Visible = False
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Series1: TBarSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = True
          SeriesColor = clRed
          BarWidthPercent = 90
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object HistogramPorovnaniPanel: TPanel
        Left = 0
        Top = 270
        Width = 784
        Height = 169
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object HistogramPorovnaniChart: TChart
          Left = 0
          Top = 20
          Width = 784
          Height = 149
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          MarginBottom = 3
          MarginLeft = 1
          MarginTop = 5
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.ExactDateTime = False
          BottomAxis.Grid.Visible = False
          BottomAxis.Increment = 0.001
          BottomAxis.MinorTickLength = 1
          BottomAxis.Title.Caption = 'Weight'
          LeftAxis.Grid.Color = 11184810
          LeftAxis.Grid.Style = psSolid
          LeftAxis.Labels = False
          LeftAxis.Title.Caption = 'Count'
          Legend.Visible = False
          View3D = False
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object BarSeries3: TBarSeries
            Marks.Arrow.Visible = False
            Marks.ArrowLength = 2
            Marks.Font.Charset = DEFAULT_CHARSET
            Marks.Font.Color = clBlack
            Marks.Font.Height = -9
            Marks.Font.Name = 'Arial'
            Marks.Font.Style = []
            Marks.Frame.Visible = False
            Marks.Style = smsValue
            Marks.Transparent = True
            Marks.Visible = True
            SeriesColor = clRed
            BarWidthPercent = 90
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Bar'
            YValues.Multiplier = 1
            YValues.Order = loNone
          end
        end
        object Panel1: TPanel
          Left = 0
          Top = 0
          Width = 784
          Height = 20
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object HistogramPorovnaniLabel: TLabel
            Left = 8
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Comparison'
          end
          object Panel2: TPanel
            Left = 599
            Top = 0
            Width = 185
            Height = 20
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object HistogramPorovnaniOsaCheckBox: TCheckBox
              Left = 96
              Top = 3
              Width = 73
              Height = 17
              Caption = 'Same axis'
              TabOrder = 0
              OnClick = HistogramPorovnaniOsaCheckBoxClick
            end
          end
        end
      end
    end
    object AktivitaDenTabSheet: TTabSheet
      Caption = 'Day activity'
      ImageIndex = 4
      object AktivitaSplitter: TSplitter
        Left = 0
        Top = 268
        Width = 784
        Height = 2
        Cursor = crVSplit
        Align = alBottom
        Beveled = True
      end
      object AktivitaDenChart: TChart
        Left = 0
        Top = 0
        Width = 784
        Height = 268
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 3
        MarginLeft = 1
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Automatic = False
        BottomAxis.AutomaticMaximum = False
        BottomAxis.AutomaticMinimum = False
        BottomAxis.ExactDateTime = False
        BottomAxis.Grid.Visible = False
        BottomAxis.Increment = 1
        BottomAxis.Maximum = 24
        BottomAxis.MinorTickCount = 1
        BottomAxis.MinorTicks.Visible = False
        BottomAxis.Title.Caption = 'Time [hours]'
        LeftAxis.Grid.Color = 11184810
        LeftAxis.Grid.Style = psSolid
        LeftAxis.Labels = False
        LeftAxis.Title.Caption = 'Count'
        Legend.Visible = False
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BarSeries1: TBarSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = True
          SeriesColor = clRed
          BarWidthPercent = 90
          OffsetPercent = 60
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object AktivitaPorovnaniPanel: TPanel
        Left = 0
        Top = 270
        Width = 784
        Height = 169
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 784
          Height = 20
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object AktivitaPorovnaniLabel: TLabel
            Left = 8
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Comparison'
          end
          object Panel6: TPanel
            Left = 599
            Top = 0
            Width = 185
            Height = 20
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
          end
        end
        object AktivitaDenPorovnaniChart: TChart
          Left = 0
          Top = 20
          Width = 784
          Height = 149
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          MarginBottom = 3
          MarginLeft = 1
          MarginTop = 5
          Title.Text.Strings = (
            'TChart')
          Title.Visible = False
          BottomAxis.Automatic = False
          BottomAxis.AutomaticMaximum = False
          BottomAxis.AutomaticMinimum = False
          BottomAxis.ExactDateTime = False
          BottomAxis.Grid.Visible = False
          BottomAxis.Increment = 1
          BottomAxis.Maximum = 24
          BottomAxis.MinorTickCount = 1
          BottomAxis.MinorTicks.Visible = False
          BottomAxis.Title.Caption = 'Time [hours]'
          LeftAxis.Grid.Color = 11184810
          LeftAxis.Grid.Style = psSolid
          LeftAxis.Labels = False
          LeftAxis.Title.Caption = 'Count'
          Legend.Visible = False
          View3D = False
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object BarSeries4: TBarSeries
            Marks.Arrow.Visible = False
            Marks.ArrowLength = 2
            Marks.Font.Charset = DEFAULT_CHARSET
            Marks.Font.Color = clBlack
            Marks.Font.Height = -9
            Marks.Font.Name = 'Arial'
            Marks.Font.Style = []
            Marks.Frame.Visible = False
            Marks.Style = smsValue
            Marks.Transparent = True
            Marks.Visible = True
            SeriesColor = clRed
            BarWidthPercent = 90
            OffsetPercent = 60
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Bar'
            YValues.Multiplier = 1
            YValues.Order = loNone
          end
        end
      end
    end
    object AktivitaCelkemTabSheet: TTabSheet
      Caption = 'Total activity'
      ImageIndex = 6
      object AktivitaCelkemChart: TChart
        Left = 0
        Top = 0
        Width = 784
        Height = 439
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 3
        MarginLeft = 1
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Grid.Color = 11184810
        BottomAxis.Grid.Style = psSolid
        BottomAxis.MinorTickCount = 1
        BottomAxis.MinorTicks.Visible = False
        BottomAxis.Title.Caption = 'Day'
        LeftAxis.Grid.Color = 11184810
        LeftAxis.Grid.Style = psSolid
        LeftAxis.Title.Caption = 'Count'
        Legend.Alignment = laTop
        Legend.Color = clSilver
        Legend.ColorWidth = 30
        Legend.Frame.Visible = False
        Legend.ShadowSize = 0
        Legend.TextStyle = ltsPlain
        Legend.TopPos = 48
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LineSeries1: TLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = False
          SeriesColor = clRed
          Title = 'Activity'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object LineSeries2: TLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = False
          SeriesColor = clNavy
          Title = 'Comparison'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
    end
    object KrivkaTabSheet: TTabSheet
      Caption = 'Growth curve'
      ImageIndex = 5
      object KrivkaChart: TChart
        Left = 0
        Top = 62
        Width = 776
        Height = 365
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 3
        MarginLeft = 1
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.Grid.Color = 11184810
        BottomAxis.Grid.Style = psSolid
        BottomAxis.MinorTickCount = 1
        BottomAxis.MinorTicks.Visible = False
        BottomAxis.Title.Caption = 'Day'
        LeftAxis.ExactDateTime = False
        LeftAxis.Grid.Color = 11184810
        LeftAxis.Grid.Style = psSolid
        LeftAxis.Increment = 0.1
        LeftAxis.Title.Caption = 'Weight'
        Legend.Alignment = laTop
        Legend.Color = clSilver
        Legend.ColorWidth = 30
        Legend.Frame.Visible = False
        Legend.ShadowSize = 0
        Legend.TextStyle = ltsPlain
        Legend.TopPos = 48
        Legend.Visible = False
        RightAxis.Grid.Visible = False
        RightAxis.Title.Caption = 'Statistics'
        RightAxis.Title.Font.Charset = DEFAULT_CHARSET
        RightAxis.Title.Font.Color = clGreen
        RightAxis.Title.Font.Height = -11
        RightAxis.Title.Font.Name = 'Arial'
        RightAxis.Title.Font.Style = []
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BarSeries2: TLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = False
          SeriesColor = clRed
          Title = 'Real'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series2: TLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = False
          SeriesColor = clNavy
          Title = 'Target'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series3: TLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = False
          SeriesColor = clPurple
          Title = 'Real comp.'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series4: TLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = False
          SeriesColor = 8421440
          Title = 'Target comp.'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series5: TLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = False
          SeriesColor = clOlive
          Title = 'Theory'
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
        object Series6: TLineSeries
          Marks.ArrowLength = 8
          Marks.Visible = False
          SeriesColor = clGreen
          Title = 'Statistics'
          VertAxis = aRightAxis
          Pointer.HorizSize = 2
          Pointer.InflateMargins = True
          Pointer.Pen.Visible = False
          Pointer.Style = psRectangle
          Pointer.VertSize = 2
          Pointer.Visible = True
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object KrivkaPanel: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 62
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object KrivkaRealCheckBox: TCheckBox
          Left = 16
          Top = 18
          Width = 57
          Height = 17
          Caption = 'Real'
          Checked = True
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          State = cbChecked
          TabOrder = 0
          OnClick = KrivkaRealCheckBoxClick
        end
        object KrivkaTargetCheckBox: TCheckBox
          Left = 96
          Top = 18
          Width = 73
          Height = 17
          Caption = 'Target'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 1
          OnClick = KrivkaTargetCheckBoxClick
        end
        object KrivkaPorovnaniPanel: TPanel
          Left = 184
          Top = 0
          Width = 185
          Height = 34
          BevelOuter = bvNone
          TabOrder = 2
          object KrivkaPorovnaniLabel: TLabel
            Left = 0
            Top = 2
            Width = 58
            Height = 13
            Caption = 'Comparison:'
          end
          object KrivkaCRealCheckBox: TCheckBox
            Left = 0
            Top = 18
            Width = 49
            Height = 17
            Caption = 'Real'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 0
            OnClick = KrivkaCRealCheckBoxClick
          end
          object KrivkaCTargetCheckBox: TCheckBox
            Left = 80
            Top = 18
            Width = 65
            Height = 17
            Caption = 'Target'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clTeal
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 1
            OnClick = KrivkaCTargetCheckBoxClick
          end
        end
        object KrivkaTeoriePanel: TPanel
          Left = 376
          Top = 0
          Width = 400
          Height = 62
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          object Label52: TLabel
            Left = 96
            Top = 19
            Width = 67
            Height = 13
            Alignment = taRightJustify
            Caption = 'Compare with:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clOlive
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object KrivkaPorovnatSpeedButton: TSpeedButton
            Left = 371
            Top = 16
            Width = 11
            Height = 21
            Caption = '...'
            Flat = True
            OnClick = Growthcurves1Click
          end
          object Label60: TLabel
            Left = 114
            Top = 43
            Width = 49
            Height = 13
            Alignment = taRightJustify
            Caption = 'Right axis:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object KrivkaPorovnatComboBox: TComboBox
            Left = 176
            Top = 16
            Width = 193
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 0
            OnChange = KrivkaPorovnatComboBoxChange
          end
          object CurveRightAxisComboBox: TComboBox
            Left = 176
            Top = 40
            Width = 193
            Height = 21
            Style = csDropDownList
            ItemHeight = 13
            TabOrder = 1
            OnChange = CurveRightAxisComboBoxChange
            Items.Strings = (
              '<none>'
              'Daily gain'
              'Count'
              'St. deviation'
              'Cv'
              'Uniformity')
          end
        end
      end
    end
    object OnlineTabSheet: TTabSheet
      Caption = 'Online table'
      ImageIndex = 8
      object OnlineDBGrid: TDBGrid
        Left = 0
        Top = 0
        Width = 784
        Height = 439
        Align = alClient
        DataSource = Data.OnlineQueryDataSource
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SAMPLE'
            Title.Alignment = taCenter
            Title.Caption = 'Sample'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TIME_HOUR'
            Title.Alignment = taCenter
            Title.Caption = 'Hour'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WEIGHT'
            Title.Alignment = taCenter
            Title.Caption = 'Weight'
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'STABLE'
            Title.Alignment = taCenter
            Title.Caption = 'Stable'
            Width = 62
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SAVED'
            Title.Alignment = taCenter
            Title.Caption = 'Saved'
            Width = 63
            Visible = True
          end>
      end
    end
    object OnlineGrafTabSheet: TTabSheet
      Caption = 'Online chart'
      ImageIndex = 9
      object OnlineChart: TChart
        Left = 0
        Top = 41
        Width = 776
        Height = 386
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 3
        MarginLeft = 1
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        OnUndoZoom = OnlineChartUndoZoom
        BottomAxis.Grid.Color = 11184810
        BottomAxis.Grid.Style = psSolid
        BottomAxis.LabelsSeparation = 20
        BottomAxis.MinorTickCount = 1
        BottomAxis.MinorTicks.Visible = False
        BottomAxis.StartPosition = 1
        BottomAxis.EndPosition = 99
        BottomAxis.Title.Caption = 'Samples'
        LeftAxis.AxisValuesFormat = '#,##0.000'
        LeftAxis.ExactDateTime = False
        LeftAxis.Grid.Color = 11184810
        LeftAxis.Grid.Style = psSolid
        LeftAxis.Increment = 0.001
        LeftAxis.StartPosition = 1
        LeftAxis.Title.Caption = 'Weight'
        Legend.Alignment = laTop
        Legend.Color = clSilver
        Legend.ColorWidth = 30
        Legend.Frame.Visible = False
        Legend.ShadowSize = 0
        Legend.TextStyle = ltsPlain
        Legend.TopPos = 48
        Legend.Visible = False
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        OnMouseMove = OnlineChartMouseMove
        object LineSeries3: TFastLineSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Visible = False
          SeriesColor = clRed
          Title = 'Online'
          LinePen.Color = clRed
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Y'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
      object OnlineGrafPanel: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label44: TLabel
          Left = 112
          Top = 16
          Width = 51
          Height = 13
          Caption = 'Averaging:'
        end
        object Label45: TLabel
          Left = 240
          Top = 16
          Width = 38
          Height = 13
          Caption = 'samples'
        end
        object Label46: TLabel
          Left = 328
          Top = 16
          Width = 37
          Height = 13
          Caption = 'Weight:'
        end
        object OnlineHmotnostLabel: TLabel
          Left = 384
          Top = 16
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = '0,000'
        end
        object OnlineHmotnostJednotkyLabel: TLabel
          Left = 416
          Top = 16
          Width = 12
          Height = 13
          Caption = 'kg'
        end
        object OnlineZoomInSpeedButton: TSpeedButton
          Left = 8
          Top = 12
          Width = 24
          Height = 22
          Flat = True
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            808888888888888807088888888888807BB0888888888807BB0888888888807B
            B0888880000007BB08888708888807B088888088888880088888088880888808
            8888088880888808888808800000880888880888808888088888088880888808
            8888808888888088888887088888078888888880000088888888}
          OnClick = OnlineZoomInSpeedButtonClick
        end
        object OnlineZoomOutSpeedButton: TSpeedButton
          Left = 40
          Top = 12
          Width = 24
          Height = 22
          Flat = True
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            808888888888888807088888888888807BB0888888888807BB0888888888807B
            B0888880000007BB08888708888807B088888088888880088888088888888808
            8888088888888808888808800000880888880888888888088888088888888808
            8888808888888088888887088888078888888880000088888888}
          OnClick = OnlineZoomOutSpeedButtonClick
        end
        object Bevel4: TBevel
          Left = 88
          Top = 13
          Width = 4
          Height = 18
          Shape = bsLeftLine
        end
        object Bevel6: TBevel
          Left = 301
          Top = 13
          Width = 4
          Height = 18
          Shape = bsLeftLine
        end
        object OnlinePrumerovaniComboBox: TComboBox
          Left = 184
          Top = 13
          Width = 49
          Height = 21
          Style = csDropDownList
          DropDownCount = 20
          ItemHeight = 13
          TabOrder = 0
          OnChange = OnlinePrumerovaniComboBoxChange
          Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9'
            '10'
            '11'
            '12'
            '13'
            '14'
            '15'
            '16'
            '17'
            '18'
            '19'
            '20'
            '21'
            '22'
            '23'
            '24'
            '25'
            '26'
            '27'
            '28'
            '29'
            '30'
            '31'
            '32'
            '33'
            '34'
            '35'
            '36'
            '37'
            '38'
            '39'
            '40')
        end
      end
    end
    object NastaveniTabSheet: TTabSheet
      Caption = 'Setup'
      ImageIndex = 3
      object NastaveniPageControl: TPageControl
        Left = 8
        Top = 8
        Width = 513
        Height = 425
        ActivePage = TabSheetCorrection
        TabOrder = 0
        object NastaveniVazeniTabSheet: TTabSheet
          Caption = 'Weighing'
          ImageIndex = 4
          object Label42: TLabel
            Left = 16
            Top = 88
            Width = 29
            Height = 13
            Caption = 'Flock:'
          end
          object Label59: TLabel
            Left = 16
            Top = 112
            Width = 45
            Height = 13
            Caption = 'Compare:'
          end
          object DBCheckBox7: TDBCheckBox
            Left = 16
            Top = 24
            Width = 273
            Height = 17
            Caption = 'Weighing started'
            DataField = 'START_PROGRESS'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 0
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object NastaveniHejnoEdit: TEdit
            Left = 104
            Top = 83
            Width = 81
            Height = 21
            ReadOnly = True
            TabOrder = 2
          end
          object DBCheckBox8: TDBCheckBox
            Left = 16
            Top = 48
            Width = 273
            Height = 17
            Caption = 'Waiting for start'
            DataField = 'START_WAITING'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 1
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object DBEdit11: TDBEdit
            Left = 104
            Top = 109
            Width = 81
            Height = 21
            DataField = 'COMPARE'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 3
          end
        end
        object NastaveniHejnaTabSheet: TTabSheet
          Caption = 'Flocks'
          ImageIndex = 1
          object Label4: TLabel
            Left = 16
            Top = 8
            Width = 34
            Height = 13
            Caption = 'Flocks:'
          end
          object SeznamHejnListBox: TListBox
            Left = 16
            Top = 24
            Width = 75
            Height = 289
            ItemHeight = 13
            Sorted = True
            TabOrder = 0
            OnClick = SeznamHejnListBoxClick
          end
          object HejnaPanel: TPanel
            Left = 104
            Top = 8
            Width = 241
            Height = 345
            BevelOuter = bvNone
            TabOrder = 1
            object Label6: TLabel
              Left = 8
              Top = 16
              Width = 31
              Height = 13
              Caption = 'Name:'
            end
            object Label24: TLabel
              Left = 8
              Top = 104
              Width = 57
              Height = 13
              Caption = 'Weigh from:'
            end
            object Label25: TLabel
              Left = 136
              Top = 104
              Width = 12
              Height = 13
              Caption = 'till:'
            end
            object JmenoDBEdit: TDBEdit
              Left = 88
              Top = 13
              Width = 129
              Height = 21
              DataField = 'NAME'
              DataSource = Data.HejnaDataSource
              ReadOnly = True
              TabOrder = 0
            end
            object DBEdit15: TDBEdit
              Left = 80
              Top = 101
              Width = 25
              Height = 21
              DataField = 'WEIGH_FROM'
              DataSource = Data.HejnaDataSource
              ReadOnly = True
              TabOrder = 3
            end
            object DBEdit16: TDBEdit
              Left = 160
              Top = 101
              Width = 25
              Height = 21
              DataField = 'WEIGH_TO'
              DataSource = Data.HejnaDataSource
              ReadOnly = True
              TabOrder = 4
            end
            object KrivkyPageControl: TPageControl
              Left = 8
              Top = 136
              Width = 225
              Height = 169
              ActivePage = KrivkaSamiceTabSheet
              TabOrder = 5
              object KrivkaSamiceTabSheet: TTabSheet
                Caption = 'Females'
                object KrivkaSamiceStringGrid: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 217
                  Height = 141
                  Align = alClient
                  ColCount = 3
                  DefaultRowHeight = 16
                  RowCount = 31
                  TabOrder = 0
                  ColWidths = (
                    25
                    66
                    93)
                end
              end
              object KrivkaSamciTabSheet: TTabSheet
                Caption = 'Males'
                ImageIndex = 1
                object KrivkaSamciStringGrid: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 217
                  Height = 141
                  Align = alClient
                  ColCount = 3
                  DefaultRowHeight = 16
                  RowCount = 31
                  TabOrder = 0
                  ColWidths = (
                    25
                    66
                    93)
                end
              end
              object PocatecniHmotnostTabSheet: TTabSheet
                Caption = 'Initial weight'
                ImageIndex = 2
                object Label26: TLabel
                  Left = 16
                  Top = 24
                  Width = 42
                  Height = 13
                  Caption = 'Females:'
                end
                object PocatecniHmotnostSamiceDBEdit: TDBEdit
                  Left = 96
                  Top = 21
                  Width = 97
                  Height = 21
                  DataField = 'FEMALE_WEIGHT00'
                  DataSource = Data.HejnaDataSource
                  ReadOnly = True
                  TabOrder = 0
                end
                object PocatecniHmotnostSamciPanel: TPanel
                  Left = 8
                  Top = 48
                  Width = 201
                  Height = 33
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label27: TLabel
                    Left = 8
                    Top = 3
                    Width = 31
                    Height = 13
                    Caption = 'Males:'
                  end
                  object PocatecniHmotnostSamciDBEdit: TDBEdit
                    Left = 88
                    Top = 0
                    Width = 97
                    Height = 21
                    DataField = 'MALE_WEIGHT00'
                    DataSource = Data.HejnaDataSource
                    ReadOnly = True
                    TabOrder = 0
                  end
                end
              end
            end
            object DBCheckBox5: TDBCheckBox
              Left = 8
              Top = 48
              Width = 209
              Height = 17
              Caption = 'Use both genders'
              DataField = 'USE_GENDER'
              DataSource = Data.HejnaDataSource
              ReadOnly = True
              TabOrder = 1
              ValueChecked = 'True'
              ValueUnchecked = 'False'
            end
            object DBCheckBox6: TDBCheckBox
              Left = 8
              Top = 72
              Width = 209
              Height = 17
              Caption = 'Use growth curves'
              DataField = 'USE_CURVES'
              DataSource = Data.HejnaDataSource
              ReadOnly = True
              TabOrder = 2
              ValueChecked = 'True'
              ValueUnchecked = 'False'
            end
          end
        end
        object NastaveniStatistikaTabSheet: TTabSheet
          Caption = 'Statistics'
          object Label8: TLabel
            Left = 16
            Top = 24
            Width = 80
            Height = 13
            Caption = 'Histogram range:'
          end
          object Label10: TLabel
            Left = 16
            Top = 48
            Width = 79
            Height = 13
            Caption = 'Uniformity range:'
          end
          object Label12: TLabel
            Left = 126
            Top = 48
            Width = 6
            Height = 13
            Caption = '�'
          end
          object Label14: TLabel
            Left = 172
            Top = 48
            Width = 8
            Height = 13
            Caption = '%'
          end
          object Label16: TLabel
            Left = 172
            Top = 24
            Width = 8
            Height = 13
            Caption = '%'
          end
          object Label28: TLabel
            Left = 126
            Top = 24
            Width = 6
            Height = 13
            Caption = '�'
          end
          object RozsahUniformityDBEdit: TDBEdit
            Left = 136
            Top = 45
            Width = 33
            Height = 21
            DataField = 'UNI_RANGE'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 1
          end
          object RozsahHistogramuDBEdit: TDBEdit
            Left = 136
            Top = 21
            Width = 33
            Height = 21
            DataField = 'HIST_RANGE'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 0
          end
        end
        object NastaveniGsmTabSheet: TTabSheet
          Caption = 'GSM'
          ImageIndex = 2
          object Label29: TLabel
            Left = 16
            Top = 160
            Width = 45
            Height = 13
            Caption = 'Numbers:'
          end
          object Label48: TLabel
            Left = 34
            Top = 72
            Width = 33
            Height = 13
            Caption = 'Period:'
          end
          object Label49: TLabel
            Left = 122
            Top = 72
            Width = 22
            Height = 13
            Caption = 'days'
          end
          object DBCheckBox1: TDBCheckBox
            Left = 16
            Top = 24
            Width = 273
            Height = 17
            Caption = 'Use GSM communication'
            DataField = 'GSM_USE'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 0
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object DBCheckBox2: TDBCheckBox
            Left = 16
            Top = 48
            Width = 273
            Height = 17
            Caption = 'Send statistics at midnight'
            DataField = 'GSM_SEND_MIDNIGHT'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 1
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object DBCheckBox3: TDBCheckBox
            Left = 16
            Top = 96
            Width = 273
            Height = 17
            Caption = 'Send data on request'
            DataField = 'GSM_SEND_REQUEST'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 3
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object DBCheckBox4: TDBCheckBox
            Left = 16
            Top = 120
            Width = 273
            Height = 17
            Caption = 'Check numbers'
            DataField = 'GSM_CHECK_NUMBERS'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 4
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object DBEdit3: TDBEdit
            Left = 16
            Top = 176
            Width = 153
            Height = 21
            DataField = 'GSM_NUMBER00'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 5
          end
          object DBEdit4: TDBEdit
            Left = 16
            Top = 200
            Width = 153
            Height = 21
            DataField = 'GSM_NUMBER01'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 6
          end
          object DBEdit5: TDBEdit
            Left = 16
            Top = 224
            Width = 153
            Height = 21
            DataField = 'GSM_NUMBER02'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 7
          end
          object DBEdit6: TDBEdit
            Left = 16
            Top = 248
            Width = 153
            Height = 21
            DataField = 'GSM_NUMBER03'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 8
          end
          object DBEdit7: TDBEdit
            Left = 16
            Top = 272
            Width = 153
            Height = 21
            DataField = 'GSM_NUMBER04'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 9
          end
          object DBEdit8: TDBEdit
            Left = 80
            Top = 69
            Width = 33
            Height = 21
            DataField = 'GSM_PERIOD_MIDNIGHT'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 2
          end
        end
        object NastaveniRs485TabSheet: TTabSheet
          Caption = 'RS-485'
          ImageIndex = 6
          object Label50: TLabel
            Left = 16
            Top = 24
            Width = 41
            Height = 13
            Caption = 'Address:'
          end
          object Label51: TLabel
            Left = 16
            Top = 48
            Width = 34
            Height = 13
            Caption = 'Speed:'
          end
          object Label53: TLabel
            Left = 16
            Top = 216
            Width = 58
            Height = 13
            Caption = 'Reply delay:'
          end
          object Label54: TLabel
            Left = 16
            Top = 240
            Width = 66
            Height = 13
            Caption = 'Silent interval:'
          end
          object Label55: TLabel
            Left = 288
            Top = 48
            Width = 13
            Height = 13
            Caption = 'Bd'
          end
          object Label56: TLabel
            Left = 288
            Top = 216
            Width = 13
            Height = 13
            Caption = 'ms'
          end
          object Label57: TLabel
            Left = 288
            Top = 240
            Width = 13
            Height = 13
            Caption = 'ms'
          end
          object DBEdit9: TDBEdit
            Left = 200
            Top = 21
            Width = 81
            Height = 21
            DataField = 'RS485_ADDRESS'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit10: TDBEdit
            Left = 200
            Top = 45
            Width = 81
            Height = 21
            DataField = 'RS485_SPEED'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 1
          end
          object DBEdit12: TDBEdit
            Left = 200
            Top = 213
            Width = 81
            Height = 21
            DataField = 'RS485_REPLY_DELAY'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 3
          end
          object DBEdit13: TDBEdit
            Left = 200
            Top = 237
            Width = 81
            Height = 21
            DataField = 'RS485_SILENT_INTERVAL'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 4
          end
          object DBRadioGroup4: TDBRadioGroup
            Left = 16
            Top = 80
            Width = 265
            Height = 49
            Caption = 'Parity'
            Columns = 4
            DataField = 'RS485_PARITY'
            DataSource = Data.KonfiguraceDataSource
            Items.Strings = (
              '8-n-1'
              '8-e-1'
              '8-o-1'
              '8-n-2')
            ReadOnly = True
            TabOrder = 2
            Values.Strings = (
              '0'
              '1'
              '2'
              '3')
          end
          object DBRadioGroup5: TDBRadioGroup
            Left = 16
            Top = 144
            Width = 265
            Height = 49
            Caption = 'Protocol'
            Columns = 2
            DataField = 'RS485_PROTOCOL'
            DataSource = Data.KonfiguraceDataSource
            Items.Strings = (
              'MODBUS RTU'
              'MODBUS ASCII')
            ReadOnly = True
            TabOrder = 5
            Values.Strings = (
              '0'
              '1'
              '2'
              '3')
          end
        end
        object NastaveniVahyTabSheet: TTabSheet
          Caption = 'Scale'
          ImageIndex = 3
          object Label30: TLabel
            Left = 16
            Top = 48
            Width = 152
            Height = 13
            Caption = 'Margin above target for females:'
          end
          object Label31: TLabel
            Left = 268
            Top = 48
            Width = 8
            Height = 13
            Caption = '%'
          end
          object Label32: TLabel
            Left = 16
            Top = 72
            Width = 149
            Height = 13
            Caption = 'Margin under target for females:'
          end
          object Label33: TLabel
            Left = 268
            Top = 72
            Width = 8
            Height = 13
            Caption = '%'
          end
          object Label34: TLabel
            Left = 16
            Top = 96
            Width = 143
            Height = 13
            Caption = 'Margin above target for males:'
          end
          object Label35: TLabel
            Left = 268
            Top = 96
            Width = 8
            Height = 13
            Caption = '%'
          end
          object Label36: TLabel
            Left = 16
            Top = 120
            Width = 140
            Height = 13
            Caption = 'Margin under target for males:'
          end
          object Label37: TLabel
            Left = 268
            Top = 120
            Width = 8
            Height = 13
            Caption = '%'
          end
          object Label38: TLabel
            Left = 16
            Top = 168
            Width = 59
            Height = 13
            Caption = 'Stabilization:'
          end
          object Label39: TLabel
            Left = 222
            Top = 168
            Width = 6
            Height = 13
            Caption = '�'
          end
          object Label40: TLabel
            Left = 268
            Top = 168
            Width = 8
            Height = 13
            Caption = '%'
          end
          object Label41: TLabel
            Left = 16
            Top = 192
            Width = 81
            Height = 13
            Caption = 'Stabilization time:'
          end
          object Label43: TLabel
            Left = 16
            Top = 144
            Width = 25
            Height = 13
            Caption = 'Filter:'
          end
          object Label47: TLabel
            Left = 16
            Top = 24
            Width = 101
            Height = 13
            Caption = 'Identification number:'
          end
          object SamiceOkoliNadDBEdit: TDBEdit
            Left = 232
            Top = 45
            Width = 33
            Height = 21
            DataField = 'F_MARGIN_ABOVE'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 1
          end
          object SamiceOkoliPodDBEdit: TDBEdit
            Left = 232
            Top = 69
            Width = 33
            Height = 21
            DataField = 'F_MARGIN_UNDER'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 2
          end
          object SamciOkoliNadDBEdit: TDBEdit
            Left = 232
            Top = 93
            Width = 33
            Height = 21
            DataField = 'M_MARGIN_ABOVE'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 3
          end
          object SamciOkoliPodDBEdit: TDBEdit
            Left = 232
            Top = 117
            Width = 33
            Height = 21
            DataField = 'M_MARGIN_UNDER'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 4
          end
          object UstaleniDBEdit: TDBEdit
            Left = 232
            Top = 165
            Width = 33
            Height = 21
            DataField = 'STABILIZATION'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 6
          end
          object DobaUstaleniDBEdit: TDBEdit
            Left = 232
            Top = 189
            Width = 33
            Height = 21
            DataField = 'STABILIZATION_TIME'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 7
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 16
            Top = 328
            Width = 249
            Height = 49
            Caption = 'Units'
            Columns = 2
            DataField = 'UNITS'
            DataSource = Data.KonfiguraceDataSource
            Items.Strings = (
              'kg'
              'lb')
            ReadOnly = True
            TabOrder = 10
            Values.Strings = (
              '0'
              '1')
          end
          object FiltrDBEdit: TDBEdit
            Left = 232
            Top = 141
            Width = 33
            Height = 21
            DataField = 'FILTER_VALUE'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 5
          end
          object DBEdit2: TDBEdit
            Left = 232
            Top = 21
            Width = 33
            Height = 21
            DataField = 'ID'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 0
          end
          object NastaveniGainCheckBox: TCheckBox
            Left = 16
            Top = 216
            Width = 249
            Height = 17
            Caption = 'Use gain in automatic mode'
            TabOrder = 8
          end
          object DBRadioGroup3: TDBRadioGroup
            Left = 16
            Top = 240
            Width = 249
            Height = 73
            Caption = 'Save sample upon'
            DataField = 'JUMP_MODE'
            DataSource = Data.KonfiguraceDataSource
            Items.Strings = (
              'Entering the scale'
              'Leaving the scale'
              'Both')
            ReadOnly = True
            TabOrder = 9
            Values.Strings = (
              '0'
              '1'
              '2')
          end
        end
        object TabSheetCorrection: TTabSheet
          Caption = 'Correction'
          ImageIndex = 7
          object Label61: TLabel
            Left = 16
            Top = 24
            Width = 31
            Height = 13
            Caption = 'Day 1:'
          end
          object Label62: TLabel
            Left = 16
            Top = 72
            Width = 51
            Height = 13
            Caption = 'Correction:'
          end
          object Label64: TLabel
            Left = 16
            Top = 48
            Width = 31
            Height = 13
            Caption = 'Day 2:'
          end
          object Label63: TLabel
            Left = 126
            Top = 72
            Width = 6
            Height = 13
            Caption = '+'
          end
          object Label65: TLabel
            Left = 172
            Top = 72
            Width = 8
            Height = 13
            Caption = '%'
          end
          object DBEdit14: TDBEdit
            Left = 136
            Top = 21
            Width = 33
            Height = 21
            DataField = 'CORRECTION_DAY1'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 0
          end
          object DBEdit18: TDBEdit
            Left = 136
            Top = 45
            Width = 33
            Height = 21
            DataField = 'CORRECTION_DAY2'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 1
          end
          object DBEdit17: TDBEdit
            Left = 136
            Top = 69
            Width = 33
            Height = 21
            DataField = 'CORRECTION_CORRECTION'
            DataSource = Data.KonfiguraceDataSource
            ReadOnly = True
            TabOrder = 2
          end
        end
        object NastaveniPodsvitTabSheet: TTabSheet
          Caption = 'Backlight'
          ImageIndex = 5
          object DBRadioGroup2: TDBRadioGroup
            Left = 16
            Top = 24
            Width = 201
            Height = 57
            Caption = 'Backlight'
            Columns = 3
            DataField = 'BACKLIGHT'
            DataSource = Data.KonfiguraceDataSource
            Items.Strings = (
              'Off'
              'On'
              'Auto')
            ReadOnly = True
            TabOrder = 0
            Values.Strings = (
              '0'
              '1'
              '2')
          end
        end
      end
    end
    object InformaceTabSheet: TTabSheet
      Caption = 'Information'
      ImageIndex = 7
      object Label17: TLabel
        Left = 16
        Top = 32
        Width = 67
        Height = 13
        Caption = 'Scale version:'
      end
      object VerzeLabel: TLabel
        Left = 120
        Top = 32
        Width = 53
        Height = 13
        Caption = 'VerzeLabel'
      end
      object Label18: TLabel
        Left = 16
        Top = 88
        Width = 26
        Height = 13
        Caption = 'Note:'
      end
      object Label19: TLabel
        Left = 16
        Top = 56
        Width = 63
        Height = 13
        Caption = 'Downloaded:'
      end
      object DBText1: TDBText
        Left = 120
        Top = 56
        Width = 185
        Height = 17
        DataField = 'DATE_TIME'
        DataSource = Data.HlavickaDataSource
      end
      object DBEdit1: TDBEdit
        Left = 120
        Top = 85
        Width = 233
        Height = 21
        DataField = 'NOTE'
        DataSource = Data.HlavickaDataSource
        TabOrder = 0
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 37
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Bevel2: TBevel
      Left = 0
      Top = 30
      Width = 784
      Height = 7
      Align = alBottom
      Shape = bsTopLine
    end
    object Bevel3: TBevel
      Left = 0
      Top = 0
      Width = 784
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object OpenSpeedButton: TSpeedButton
      Left = 8
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        77777777777777777777000000000007777700333333333077770B0333333333
        07770FB03333333330770BFB0333333333070FBFB000000000000BFBFBFBFB07
        77770FBFBFBFBF0777770BFB0000000777777000777777770007777777777777
        7007777777770777070777777777700077777777777777777777}
      ParentShowHint = False
      ShowHint = True
      OnClick = Otevt1Click
    end
    object SaveSpeedButton: TSpeedButton
      Left = 32
      Top = 5
      Width = 24
      Height = 22
      Enabled = False
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777770000000000000770330000007703077033000000770307703300000077
        0307703300000000030770333333333333077033000000003307703077777777
        0307703077777777030770307777777703077030777777770307703077777777
        0007703077777777070770000000000000077777777777777777}
      ParentShowHint = False
      ShowHint = True
      OnClick = Uloit1Click
    end
    object ReadSpeedButton: TSpeedButton
      Left = 96
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888808
        0000888888888888888888888888880800008888888888888888888888888808
        0000888888888888888888888888888811888880000888811118880F77708811
        111180F888870888118880F888870811118880F888870811188880F888870888
        88880F888888708888880FFFFFFFF08888888B0B0B0B08888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Nastzznamy1Click
    end
    object FindSpeedButton: TSpeedButton
      Left = 56
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
        DADA000000000DADADAD0FFFFFFF0ADADA000F88888F0DADA0000F88888F0ADA
        000A0F88870007A000AD0F8800FFF0000ADA0F807FFFF070ADAD0F70E000F080
        7ADA0F0EFEFEF0880DAD0F0F0000F0880ADA000EFEFFF0880DADDA7000000080
        7ADAADA008888870ADADDADA0088800ADADAADADA70007ADADAD}
      ParentShowHint = False
      ShowHint = True
      OnClick = Findfile1Click
    end
    object SetupSpeedButton: TSpeedButton
      Left = 120
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
        DADAADADADADADADADADD00ADADADA00DADAA010ADADA080ADADDA010ADA080A
        DADAADA010A080ADADADDADA01080ADADADAADADA080ADADADADDADA08010ADA
        D0DA000080A010AD030D00000ADA010003300DA00DADA003300DDAD00ADAD033
        30DAAD000DAD003330ADDADADA0033000ADAADADADA0000DADAD}
      ParentShowHint = False
      ShowHint = True
      OnClick = Config1Click
    end
    object DatabaseSpeedButton: TSpeedButton
      Left = 200
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
        DADAADADADADADADADADDADADAD000000000ADADADA0FFFFFFF0DADA0000F00F
        00F0ADAD0FF0FFFFFFF0D0000F00F00F00F0A0FF0FF0FFFFFFF0D0F00F00F00F
        00F0A0FF0FF0FFFFFFF0D0F00F0000000000A0FF0FFFFFFF0DADD0F000000000
        0ADAA0FFFFFFF0ADADADD000000000DADADAADADADADADADADAD}
      ParentShowHint = False
      ShowHint = True
      OnClick = Weighingdatabase1Click
    end
    object AddSpeedButton: TSpeedButton
      Left = 224
      Top = 5
      Width = 24
      Height = 22
      Enabled = False
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888800000888088888880
        FFF0888008888880F0F0000030888880FFF00BBBBB080000F0F00000F0880FF0
        FFF0888008880F000000888088880FFF0888888888880F0F0888888888880FFF
        0888888888880000088888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Addtoweighingdatabase1Click
    end
    object Bevel7: TBevel
      Left = 87
      Top = 7
      Width = 4
      Height = 18
      Shape = bsLeftLine
    end
    object Bevel8: TBevel
      Left = 151
      Top = 7
      Width = 4
      Height = 18
      Shape = bsLeftLine
    end
    object Rs485SpeedButton: TSpeedButton
      Left = 160
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00881111111888
        8888111110111118888888111011188888888888808888888888888880888888
        8000888880888888088888888088888808888888808888880888888000008888
        08888880383088880888888083808888088888803830888808888880AAA00000
        8888888038308888888888800000888888888888808888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = RS4851Click
    end
    object Bevel9: TBevel
      Left = 191
      Top = 7
      Width = 4
      Height = 18
      Shape = bsLeftLine
    end
    object ZahlaviPanel: TPanel
      Left = 272
      Top = 5
      Width = 449
      Height = 25
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object Label1: TLabel
        Left = 24
        Top = 5
        Width = 22
        Height = 13
        Caption = 'Day:'
      end
      object DatumLabel: TLabel
        Left = 166
        Top = 5
        Width = 59
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '00.00.0000'
      end
      object PrvniDenSpeedButton: TSpeedButton
        Left = 123
        Top = 0
        Width = 21
        Height = 22
        Flat = True
        Glyph.Data = {
          AE000000424DAE0000000000000076000000280000000D000000070000000100
          0400000000003800000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808888088880
          8000808880088800800080880008800080008080000800008000808800088000
          800080888008880080008088880888808000}
        OnClick = PrvniDenSpeedButtonClick
      end
      object PredchoziDenSpeedButton: TSpeedButton
        Left = 144
        Top = 0
        Width = 21
        Height = 22
        Flat = True
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
          08808800088080000880880008808880088088880880}
        OnClick = PredchoziDenSpeedButtonClick
      end
      object DalsiDenSpeedButton: TSpeedButton
        Left = 226
        Top = 0
        Width = 21
        Height = 22
        Flat = True
        Glyph.Data = {
          92000000424D9200000000000000760000002800000006000000070000000100
          0400000000001C00000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808888008008
          88008000880080000800800088008008880080888800}
        OnClick = DalsiDenSpeedButtonClick
      end
      object PosledniDenSpeedButton: TSpeedButton
        Left = 247
        Top = 0
        Width = 21
        Height = 22
        Flat = True
        Glyph.Data = {
          AE000000424DAE0000000000000076000000280000000D000000070000000100
          0400000000003800000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808888088880
          8000800888008880800080008800088080008000080000808000800088000880
          800080088800888080008088880888808000}
        OnClick = PosledniDenSpeedButtonClick
      end
      object Bevel5: TBevel
        Left = 0
        Top = 2
        Width = 4
        Height = 18
        Shape = bsLeftLine
      end
      object PohlaviPanel: TPanel
        Left = 288
        Top = 0
        Width = 153
        Height = 22
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 24
          Top = 5
          Width = 38
          Height = 13
          Caption = 'Gender:'
        end
        object SpeedButton1: TSpeedButton
          Left = 88
          Top = 4
          Width = 15
          Height = 16
          Enabled = False
          Flat = True
          Glyph.Data = {
            1E010000424D1E010000000000007600000028000000120000000E0000000100
            040000000000A800000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808888888
            8088880000008888088888888088880000008800000888800000880000008888
            0888888880888800000088880888888880888800000088800088888800088800
            0000870888078870888078000000808888808808888808000000088888880088
            8888800000000888888800888888800000000888888800888888800000008088
            8880880888880800000087088807887088807800000088800088888800088800
            0000}
          NumGlyphs = 2
        end
        object SpeedButton2: TSpeedButton
          Left = 128
          Top = 4
          Width = 15
          Height = 15
          Enabled = False
          Flat = True
          Glyph.Data = {
            FA000000424DFA000000000000007600000028000000160000000B0000000100
            0400000000008400000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888000888888
            8800088888008708880788887088807888008088888088880888880888000888
            8888088088888880880008888888088088888880880008888888088088888880
            8800808888808888088888088800870888008808708880088000888000880808
            880008808000888888888008888888880000888888800008888888000000}
          NumGlyphs = 2
        end
        object Bevel1: TBevel
          Left = 0
          Top = 2
          Width = 4
          Height = 18
          Shape = bsLeftLine
        end
        object PohlaviSamiceRadioButton: TRadioButton
          Left = 72
          Top = 4
          Width = 17
          Height = 17
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = PohlaviSamiceRadioButtonClick
        end
        object PohlaviSamciRadioButton: TRadioButton
          Left = 112
          Top = 4
          Width = 17
          Height = 17
          TabOrder = 1
          OnClick = PohlaviSamciRadioButtonClick
        end
      end
      object DenComboBox: TComboBox
        Left = 56
        Top = 1
        Width = 49
        Height = 21
        Style = csDropDownList
        DropDownCount = 20
        ItemHeight = 13
        TabOrder = 1
        OnChange = DenComboBoxChange
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 680
    Top = 8
    object Soubor1: TMenuItem
      Caption = 'File'
      object Otevt1: TMenuItem
        Caption = 'Open'
        ShortCut = 16463
        OnClick = Otevt1Click
      end
      object Uloit1: TMenuItem
        Caption = 'Save'
        ShortCut = 16467
        OnClick = Uloit1Click
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object Findfile1: TMenuItem
        Caption = 'Find file...'
        OnClick = Findfile1Click
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object Openforcomparsion1: TMenuItem
        Caption = 'Open for comparison'
        OnClick = Openforcomparsion1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Tisk1: TMenuItem
        Caption = 'Print'
        object MenuPrintReport: TMenuItem
          Caption = 'Report'
          OnClick = MenuPrintReportClick
        end
        object MenuPrintSamples: TMenuItem
          Caption = 'Samples'
          OnClick = MenuPrintSamplesClick
        end
        object MenuPrintHistogram: TMenuItem
          Caption = 'Histogram'
          OnClick = MenuPrintHistogramClick
        end
        object MenuPrintDayActivity: TMenuItem
          Caption = 'Day activity'
          OnClick = MenuPrintDayActivityClick
        end
        object MenuPrintTotalActivity: TMenuItem
          Caption = 'Total activity'
          OnClick = MenuPrintTotalActivityClick
        end
        object MenuPrintGrowthCurve: TMenuItem
          Caption = 'Growth curve'
          OnClick = MenuPrintGrowthCurveClick
        end
        object MenuPrintOnline: TMenuItem
          Caption = 'Online chart'
          OnClick = MenuPrintOnlineClick
        end
      end
      object Export1: TMenuItem
        Caption = 'Export to Excel'
        object MenuExportReport: TMenuItem
          Caption = 'Report'
          OnClick = MenuExportReportClick
        end
        object MenuExportSamples: TMenuItem
          Caption = 'Samples'
          OnClick = MenuExportSamplesClick
        end
        object MenuExportHistogram: TMenuItem
          Caption = 'Histogram'
          OnClick = MenuExportHistogramClick
        end
        object MenuExportDayActivity: TMenuItem
          Caption = 'Day activity'
          OnClick = MenuExportDayActivityClick
        end
        object MenuExportTotalActivity: TMenuItem
          Caption = 'Total activity'
          OnClick = MenuExportTotalActivityClick
        end
        object MenuExportGrowthCurve: TMenuItem
          Caption = 'Growth curve'
          OnClick = MenuExportGrowthCurveClick
        end
        object MenuExportOnline: TMenuItem
          Caption = 'Online table'
          OnClick = MenuExportOnlineClick
        end
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object MenuSimulation: TMenuItem
        Caption = 'Simulation...'
        OnClick = MenuSimulationClick
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object Konecprogramu1: TMenuItem
        Caption = 'Quit'
        OnClick = Konecprogramu1Click
      end
    end
    object Modul1: TMenuItem
      Caption = 'Scale'
      object Memorymodule1: TMenuItem
        Caption = 'Memory module'
        object Nastzznamy1: TMenuItem
          Caption = 'Read data...'
          OnClick = Nastzznamy1Click
        end
        object Config1: TMenuItem
          Caption = 'Setup...'
          OnClick = Config1Click
        end
        object Diagnostics1: TMenuItem
          Caption = 'Diagnostics'
          object Readdatatofile1: TMenuItem
            Caption = 'Save module to file...'
            OnClick = Readdatatofile1Click
          end
          object Readdatafromfile1: TMenuItem
            Caption = 'Load module from file...'
            OnClick = Readdatafromfile1Click
          end
        end
      end
      object RS4851: TMenuItem
        Caption = 'RS-485 scales...'
        OnClick = RS4851Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Growthcurves1: TMenuItem
        Caption = 'Growth curves...'
        OnClick = Growthcurves1Click
      end
    end
    object Database1: TMenuItem
      Caption = 'Database'
      object Weighingdatabase1: TMenuItem
        Caption = 'Show weighing database...'
        OnClick = Weighingdatabase1Click
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object Addtoweighingdatabase1: TMenuItem
        Caption = 'Add file to weighing database...'
        Enabled = False
        OnClick = Addtoweighingdatabase1Click
      end
    end
    object Nastaven1: TMenuItem
      Caption = 'Options'
      object Jazyk1: TMenuItem
        Caption = 'Language'
        object Dansk1: TMenuItem
          Caption = 'Dansk'
          OnClick = Dansk1Click
        end
        object Deutsch1: TMenuItem
          Caption = 'Deutsch'
          OnClick = Deutsch1Click
        end
        object Anglicky1: TMenuItem
          Caption = 'English'
          OnClick = Anglicky1Click
        end
        object Espanol1: TMenuItem
          Caption = 'Espanol'
          OnClick = Espanol1Click
        end
        object Francouzsky1: TMenuItem
          Caption = 'Fran�ais'
          OnClick = Francouzsky1Click
        end
        object Russian1: TMenuItem
          Caption = 'Russian'
          OnClick = Russian1Click
        end
        object Finsky1: TMenuItem
          Caption = 'Suomi'
          OnClick = Finsky1Click
        end
        object Turkce1: TMenuItem
          Caption = 'Turkce'
          OnClick = Turkce1Click
        end
      end
    end
    object Npovda1: TMenuItem
      Caption = 'Help'
      object Oprogramu1: TMenuItem
        Caption = 'About...'
        OnClick = Oprogramu1Click
      end
    end
  end
  object OpenDialog: TOpenDialog
    Filter = 'Bat2|*.bt2'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 744
    Top = 32
  end
  object SaveDialog: TSaveDialog
    Filter = 'Bat2|*.bt2'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 714
    Top = 28
  end
  object OpenDialogHardCopy: TOpenDialog
    Filter = 'Bat2 hardcopy|*.bin'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 744
  end
  object SaveDialogHardCopy: TSaveDialog
    Filter = 'Bat2 hardcopy|*.bin'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 714
    Top = 65532
  end
end
