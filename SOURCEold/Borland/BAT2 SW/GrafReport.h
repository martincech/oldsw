//----------------------------------------------------------------------------
#ifndef GrafReportH
#define GrafReportH
//----------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\QuickRpt.hpp>
#include <vcl\QRCtrls.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Qrctrls.hpp>
#include <QrTee.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//----------------------------------------------------------------------------
class TGrafReportForm : public TQuickRep
{
__published:
        TQRBand *TitleBand1;
        TQRLabel *NazevQRLabel;
        TQRSysData *QRSysData1;
        TQRLabel *QRLabel11;
        TQRLabel *SouborQRLabel;
        TQRLabel *PohlaviNazevQRLabel;
        TQRLabel *PohlaviQRLabel;
        TQRChart *HistogramQRChart;
        TQRDBChart *QRDBChart1;
        TLineSeries *Series1;
        TLineSeries *Series2;
        TLineSeries *Series3;
        TLineSeries *Series4;
private:
public:
   __fastcall TGrafReportForm::TGrafReportForm(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern TGrafReportForm *GrafReportForm;
//----------------------------------------------------------------------------
#endif