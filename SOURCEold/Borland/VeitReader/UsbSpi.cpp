//******************************************************************************
//
//   UsbSpi.cpp    USB SPI mode
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "UsbSpi.h"
#include <stdio.h>

//---------------------------------------------------------------------------

#pragma package(smart_init)

// Check API return value :

#define if_fterr( status)  if( (status) != FT_OK)

#define DEFAULT_LATENCY 16                       // default latency is 16ms
#define DEFAULT_RATE    (9600 * 16)              // default sampling rate

// Device pins :
#define SPI_SK    0x01                 // ADBUS0
#define SPI_DO    0x02                 // ADBUS1
#define SPI_DI    0x04                 // ADBUS2
#define SPI_CS    0x08                 // ADBUS3
#define SPI_DIRECTION (SPI_SK | SPI_DO | SPI_CS)  // '0' == input, '1' == output

// Implementation limits :
#define MAX_BUFFER 600


// Raw data logger :
#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

//******************************************************************************
// Locator
//******************************************************************************

bool TUsbSpi::Locate( TName Name, TName AuxName, TIdentifier &Identifier)
// Find device by <Name>, returns <Identifier>
{
   // Count of FTDI devices :
   DWORD      NumDevices;
   if_fterr( FT_ListDevices( &NumDevices, NULL, FT_LIST_NUMBER_ONLY)){
      return( false);                  // unable get
   }
   if( NumDevices == 0){
      return( false);                  // no devices
   }

   // Search by device name
   int   Count    = 0;                 // count of devices found
   char  DeviceName[ 256];             // device name
   DWORD UsbIndex = -1;                // index of last device
   for( DWORD i = 0; i < NumDevices; i++){
      if_fterr( FT_ListDevices( (PVOID)i, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
         continue;
      }
      if( Name != DeviceName){
         if( AuxName == 0){
            continue;
         }
         if( AuxName != DeviceName){
            continue;
         }
      }
      Count++;
      UsbIndex = i;
   }
   if( Count == 0){
      return( false);                  // no device
   }
   if( Count > 1){
      return( false);                  // more devices
   }
   Identifier = UsbIndex;              // one device only
   return( true);
} // Locate

//******************************************************************************
// Access rights
//******************************************************************************

bool TUsbSpi::Access( TIdentifier Identifier)
// Check device access by <Identifier>
{
   FT_HANDLE TmpHandle;
   if_fterr( FT_Open( Identifier, &TmpHandle)){
      return( false);
   }
   FT_Close( TmpHandle);
   return( true);
} // Access

//******************************************************************************
// Constructor
//******************************************************************************

TUsbSpi::TUsbSpi()
// Constructor
{
   FHandle       = INVALID_HANDLE_VALUE;
   FIdentifier   = INVALID_IDENTIFIER;
   FTxLatency    = DEFAULT_LATENCY;
   FLogger       = 0;
} // TUsbSpi

//******************************************************************************
// Destructor
//******************************************************************************

TUsbSpi::~TUsbSpi()
// Destructor
{
   Close();
} // ~TUsbSpi


//******************************************************************************
// Open
//******************************************************************************

bool TUsbSpi::Open( TIdentifier Identifier)
// Open device by <Identifier>
{
   if( Identifier == INVALID_IDENTIFIER){
      IERROR;                          // invalid access
   }
   if( IsOpen){
      Close();                         // close previous
   }
   // Check text data first (inaccessible after open) :
   char  DeviceName[ 256];
   if_fterr( FT_ListDevices( (PVOID)Identifier, DeviceName, FT_LIST_BY_INDEX | FT_OPEN_BY_DESCRIPTION)){
      return( false);
   }
   FUsbName = TString( DeviceName);
   char SerialNumber[ 32];
   if_fterr( FT_ListDevices( (PVOID)Identifier, SerialNumber, FT_LIST_BY_INDEX | FT_OPEN_BY_SERIAL_NUMBER)){
      return( false);
   }
   FSerialNumber = TString( SerialNumber);
   // Now open it :
   if_fterr( FT_Open( Identifier, &FHandle)){
      return( false);
   }
   // USB timeouts :
   FTTIMEOUTS to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   to.ReadIntervalTimeout         =  0;
   to.ReadTotalTimeoutConstant    =  50;            // some USB latency
   to.ReadTotalTimeoutMultiplier  =  1;             // delay per character
   to.WriteTotalTimeoutConstant   =  50;            // some Tx latency
   to.WriteTotalTimeoutMultiplier =  1;             // delay per character
   FT_W32_SetCommTimeouts( FHandle,&to);
   // FT Device latency :
   FT_SetLatencyTimer( FHandle, FTxLatency);
   FIdentifier = Identifier;
   // Reset device :
   if_fterr( FT_SetBitMode( FHandle, 0, 0)){
      return( false);
   }
   // Set MPSSE mode :
   if_fterr( FT_SetBitMode( FHandle, 0xFF, 2)){ // all IO to output, MPSSE code = 2
      return( false);
   }
   return( true);
} // Open

//******************************************************************************
// Close
//******************************************************************************

void TUsbSpi::Close()
// Close device
{
   if( IsOpen){
      FT_W32_CloseHandle( FHandle);
   }
   FHandle     = INVALID_HANDLE_VALUE;
   FIdentifier = INVALID_IDENTIFIER;
} // Close

//******************************************************************************
// Clock Rate
//******************************************************************************

bool TUsbSpi::SetClockRate( int Hz)
// Set clock rate [Hz]
{
   if( !IsOpen){
      return( false);
   }
   if( (Hz > 6000000) || (Hz < 90)){
      return( false);
   }
   FClockRate = Hz;                    // remember
   // update USB Rx timeout :
   int CharacterTime;
   if( Hz < 8000){
      CharacterTime = 8000 / Hz + 1;   // set by slow clock
   } else {
      CharacterTime = 1;               // fast clock - set 1ms
   }
   FTTIMEOUTS to;
   FT_W32_GetCommTimeouts( FHandle, &to);
   to.ReadTotalTimeoutMultiplier  =  CharacterTime; // delay per character
   FT_W32_SetCommTimeouts( FHandle,&to);
   // write to device :
   byte Buffer[ 3];
   word Value = (6000000 / Hz) - 1;    // encode dividier
   Buffer[ 0] = 0x86;                  // set TCK/SK divisor
   Buffer[ 1] = Value & 0xFF;          // Value low
   Buffer[ 2] = Value >> 8;            // Value High
   return( Send( Buffer, sizeof( Buffer)));
} // SetClockRate

//******************************************************************************
// Mode
//******************************************************************************

void TUsbSpi::SetMode( TClockPolarity Polarity, TReadStrobe ReadStrobe, TWriteStrobe WriteStrobe)
// Set SPI mode
{
   FClockHigh = (Polarity == CLOCK_HIGH);
   FModeMask  = 0;
   if( ReadStrobe  == READ_ON_FALL){
      FModeMask |= 0x04;
   }
   if( WriteStrobe == WRITE_ON_FALL){
      FModeMask |= 0x01;
   }
} // SetMode

//******************************************************************************
// Select
//******************************************************************************

bool TUsbSpi::ClrCS()
// Device chipselect L
{
   if( !IsOpen){
      return( false);
   }
   byte Value = FClockHigh ? SPI_SK : 0;    // CS active Low
   byte Buffer[ 3];
   Buffer[ 0] = 0x80;            // set data bits Low Byte
   Buffer[ 1] = Value;           // Value
   Buffer[ 2] = SPI_DIRECTION;   // Direction
   return( Send( Buffer, sizeof( Buffer)));
} // ClrCS

//******************************************************************************
// Deselect
//******************************************************************************

bool TUsbSpi::SetCS()
// Device chipselect H
{
   if( !IsOpen){
      return( false);
   }
   byte Value = (FClockHigh ? SPI_SK : 0) | SPI_CS; // CS inactive High
   byte Buffer[ 3];
   Buffer[ 0] = 0x80;            // set data bits Low Byte
   Buffer[ 1] = Value;           // Value
   Buffer[ 2] = SPI_DIRECTION;   // Direction
   return( Send( Buffer, sizeof( Buffer)));
} // SetCS

//******************************************************************************
// Write
//******************************************************************************

bool TUsbSpi::Write( void *Data, int Length)
// Write <Data> of size <Length>
{
   if( !IsOpen){
      return( false);
   }
   if( Length > MAX_BUFFER){
      IERROR;
   }
   char Buffer[ MAX_BUFFER + 3];
   word TmpLength = Length - 1;        // Encode length
   Buffer[ 0] = FModeMask | 0x10;      // Clock data Out
   Buffer[ 1] = TmpLength & 0xFF;      // LengthL
   Buffer[ 2] = TmpLength >> 8;        // LengthH
   memcpy( &Buffer[3], Data, Length);  // Data
   return( Send( Buffer, Length + 3)); // send write command & data
} // Write

//******************************************************************************
// Read
//******************************************************************************

bool TUsbSpi::Read( void *Data, int Length)
// Read data <Data> of size <Length>
{
   if( !IsOpen){
      return( false);
   }
   char Buffer[ 16];
   word TmpLength = Length - 1;        // Encode length
   Buffer[ 0] = FModeMask | 0x20;      // Clock data In
   Buffer[ 1] = TmpLength & 0xFF;      // LengthL
   Buffer[ 2] = TmpLength >> 8;        // LengthH
   if( !Send( Buffer, 3)){             // send read command
      return( false);                  // unable send
   }
   int RetLength = Receive( Data, Length);
   if( RetLength != Length){
      return( false);
   }
   return( true);
} // Read

//******************************************************************************
//  Write Byte
//******************************************************************************

bool TUsbSpi::WriteByte( byte Data)
// Write single byte
{
   return( Write( &Data, 1));
} // WriteByte

//******************************************************************************
// Read Byte
//******************************************************************************

bool TUsbSpi::ReadByte( byte &Data)
// Read single byte
{

   return( Read( &Data, 1));
} // ReadByte

//******************************************************************************
// Flush
//******************************************************************************

void TUsbSpi::Flush()
// Make input/output queue empty
{
   if( !IsOpen){
      return;
   }
   /* with special thanks to FTDI :
   FT_W32_PurgeComm( FHandle, PURGE_RXCLEAR | PURGE_TXCLEAR);
   */
   // Wait for data :
   for( int i = 0; i < 5; i++){
      FT_Purge( FHandle, FT_PURGE_RX | FT_PURGE_TX);
      DWORD Available;
      if_fterr( FT_GetQueueStatus( FHandle, &Available)){
         return;
      }
      if( Available == 0){
         return;
      }
      Sleep( 50);
   }
   IERROR;        // unable to make empty
} // Flush

//------------------------------------------------------------------------------
// Protected
//------------------------------------------------------------------------------

//******************************************************************************
// Property Name
//******************************************************************************

TName TUsbSpi::GetName()
// Get device name by <Identifier>
{
   if( !IsOpen){
      return( "?");
   }
   return( FUsbName);
} // GetName

//******************************************************************************
// Property Is Open
//******************************************************************************

bool TUsbSpi::GetIsOpen()
// Check if device is opened
{
   return( FIdentifier != INVALID_IDENTIFIER);
} // GetIsOpen

//******************************************************************************
// Tx Latency
//******************************************************************************

void TUsbSpi::SetTxLatency( int Latency)
// Set devices output buffer latency
{
   if( Latency < 1 || Latency > 255){
      FTxLatency = DEFAULT_LATENCY;             // out of range, set default
   } else {
      FTxLatency = Latency;
   }
   // device latency
   if_fterr( FT_SetLatencyTimer( FHandle, FTxLatency)){
      FTxLatency = DEFAULT_LATENCY;             // error, remember default
   }
} // SetTxLatency

//******************************************************************************
// Send
//******************************************************************************

bool TUsbSpi::Send( void *Data, int Length)
// Send <Data> of size <Length>
{
   if( !IsOpen){
      IERROR;
   }
   DWORD w;
   if( !FT_W32_WriteFile( FHandle, Data, Length, &w, NULL)){
      RwdReport( "Unable send");
      Close();               // disconnect device
      return( false);        // timeout
   }
   RwdTx( Data, Length);
   return( w == (DWORD)Length);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

int TUsbSpi::Receive( void *Data, int Length)
// Receive data <Data> of size <Length>, returns Rx length
{
   if( !IsOpen){
      IERROR;
   }
   DWORD r;
   if( !FT_W32_ReadFile( FHandle, Data, Length, &r, NULL)){
      RwdReport( "Rx error");
      return( 0);
   }
   if( r > 0){
      RwdRx( Data, r);
   } else {
      RwdReport( "Rx timeout");
   }
   return( r);
} // Receive

