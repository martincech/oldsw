//******************************************************************************
//
//   MainFlash.h  Flash module demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainModuleH
#define MainModuleH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "AtFlash.h"
#include "../Crt/Crt.h"
#include "../Serial/CrtLogger.h"
#include <ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TLabel *LblAdapter;
        TTimer *Timer;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *LblModule;
        TButton *BtnRead;
        TCheckBox *BtnCheck;
        TButton *BtnWrite;
        TButton *BtnReadAll;
        TEdit *TxtAddress;
        TEdit *TxtSize;
        TButton *BtnWriteFragment;
        TLabel *Label4;
        TLabel *Label3;
        void __fastcall Create(TObject *Sender);
        void __fastcall Destroy(TObject *Sender);
        void __fastcall MemoResize(TObject *Sender);
        void __fastcall TimerTimer(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
        void __fastcall BtnWriteClick(TObject *Sender);
        void __fastcall BtnReadAllClick(TObject *Sender);
        void __fastcall BtnWriteFragmentClick(TObject *Sender);
//---------------------------------------------------------------------------
public:		// User declarations
   TCrt          *Crt;
   TLogger       *Logger;
   TAtFlash      *Flash;
   bool          Connected;
   byte          *Array;          // memory array
   byte          *TmpArray;       // working memory array
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
