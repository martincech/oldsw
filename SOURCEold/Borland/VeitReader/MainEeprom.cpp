//******************************************************************************
//
//   MainEeprom.cpp  EEPROM low level demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include <stdio.h>

#include "MainEeprom.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

///*
// M95256 :
#define EEPROM_SIZE       32768
#define EEPROM_PAGE_SIZE     64
//*/

/*
// M95640 :
#define EEPROM_SIZE        8192
#define EEPROM_PAGE_SIZE     32
*/

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Array    = new byte[ EEPROM_SIZE];
   TmpArray = new byte[ EEPROM_SIZE];
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt       = new TCrt( MainMemo);
   Logger    = new TCrtLogger( Crt);
   Eeprom    = new TEeprom( EEPROM_PAGE_SIZE);
   Eeprom->Spi->Logger = Logger;
   Connected = false;
} // Create

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Eeprom;
   delete Logger;
   delete Crt;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TMainForm::TimerTimer(TObject *Sender)
{
   if( !BtnCheck->Checked){
      Eeprom->Spi->Logger = 0;
   } else {
      Crt->printf( "* Tick\n");
   }
   Connected = Eeprom->Connected();
   if( !BtnCheck->Checked){
      Eeprom->Spi->Logger = Logger;
   }
   LblAdapter->Caption = Eeprom->Spi->Name;      // at least Adapter name
   if( Connected){
      LblModule->Caption = "OK";
   } else {
      LblModule->Caption = "Not present";
   }
} // TimerTimer

//******************************************************************************
// Read
//******************************************************************************

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   byte Buffer[ 512];
   if( !Connected){
      return;
   }
   if( !Eeprom->Read(0, Buffer, sizeof( Buffer))){
      Crt->printf( "Unable read\n");
      return;
   }
} // BtnReadClick

//******************************************************************************
// Write
//******************************************************************************

#define TEST_SIZE EEPROM_PAGE_SIZE

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   byte Buffer[ TEST_SIZE];
   word TEST_PAGE = rand() % EEPROM_SIZE;
   TEST_PAGE &= ~(EEPROM_PAGE_SIZE - 1);
   if( !Connected){
      return;
   }
   for( int i = 0; i < TEST_SIZE; i++){
      Buffer[ i] = rand();
   }
   if( !Eeprom->WritePage( TEST_PAGE, Buffer, TEST_SIZE)){
      Crt->printf( "Unable write page\n");
      return;
   }
   byte Buffer2[ TEST_SIZE];
   if( !Eeprom->Read( TEST_PAGE, Buffer2, TEST_SIZE)){
      Crt->printf( "Unable read EEPROM\n");
      return;
   }
   if( !memequ( Buffer, Buffer2, TEST_SIZE)){
      Crt->printf( "Verification failed\n");
      return;
   }
   Crt->printf( "* Verification OK\n");
} // BtnWriteClick

//******************************************************************************
// Read All
//******************************************************************************

void __fastcall TMainForm::BtnReadAllClick(TObject *Sender)
{
   if( !Connected){
      return;
   }
   Timer->Enabled = false;
//   Eeprom->Spi->Logger = 0;         // disable logger

   Application->ProcessMessages(); // vyber cekajici zpravy
   Screen->Cursor = crHourGlass;   // presypaci hodiny

   if( !Eeprom->Read( 0, Array, EEPROM_SIZE)){
      Crt->printf( "Error read\n");
   } else {
      Crt->printf( "Read success\n");
   }
   Screen->Cursor  = crDefault;    // zrus hodiny
   Eeprom->Spi->Logger = Logger;    // enable logger
   Timer->Enabled = true;
} // BtnReadAllClick

//******************************************************************************
// Write fragment
//******************************************************************************

void __fastcall TMainForm::BtnWriteFragmentClick(TObject *Sender)
{
   unsigned Address;
   sscanf( TxtAddress->Text.c_str(), "%X", &Address);
   int Size;
   sscanf( TxtSize->Text.c_str(),    "%d", &Size);
   // some values :
   for( unsigned i = Address; i < Address + Size; i++){
      TmpArray[ i] = rand();
   }
   if( !Eeprom->Write( Address, &TmpArray[ Address], Size)){
      Crt->printf( "Write error");
      return;
   }
   if( !Eeprom->Read( Address, &Array[ Address], Size)){
      Crt->printf( "Read error");
      return;
   }
   if( !memequ( &Array[ Address], &TmpArray[ Address], Size)){
      Crt->printf( "Verification error");
      return;
   }
   Crt->printf( "Verification OK\n");
} // BtnWriteFragment

