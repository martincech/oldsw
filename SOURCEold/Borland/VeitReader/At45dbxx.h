//******************************************************************************
//
//   At45dbxx.h    Atmel flash AT45DBxx
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef At45dbxxH
#define At45dbxxH

#include "UsbSpi.h"

#define FLASH_PAGE_SIZE       528 // real page size
#define FLASH_PAGES          4096 // pages count

//******************************************************************************
// AT45DBxx
//******************************************************************************

class TAt45dbxx {
public :
   TAt45dbxx();
   // Constructor
   ~TAt45dbxx();
   // Destructor
   bool Connected();
   // Check if the adapter/module are present

   bool WaitForReady();
   // Wait for flash ready
   bool WriteBuffer( word Offset, void *Data, int Length);
   // Write data into flash buffer
   bool SaveBuffer( word Page);
   // Save buffer to flash array
   bool LoadBuffer( word Page);
   // Load buffer from flash array
   bool ReadArray( word Page, word Offset, void *Data, int Length);
   // Read data from flash array

   __property TUsbSpi *Spi = {read=FSpi};
//------------------------------------------------------------------------------
protected :
   TUsbSpi *FSpi;        // SPI via USB

   bool CheckConnect();
   // Try connect adapter
   bool Status( byte &Value);
   // Read flash status
}; // TAt45dbxx


#endif
