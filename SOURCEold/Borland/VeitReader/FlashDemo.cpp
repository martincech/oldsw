//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("FlashDemo.res");
USEFORM("MainFlash.cpp", MainForm);
USEUNIT("UsbSpi.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("At45dbxx.cpp");
USELIB("..\Library\Serial\USB\ftd2xx.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
