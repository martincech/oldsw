//******************************************************************************
//
//   Eeprom.h      EEPROM AT95xxx
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Eeprom.h"

#pragma package(smart_init)

#define USB_DEVICE_NAME "VEIT USB Reader A"   // USB adapter name

#define SPI_CLOCK_RATE  1000000               // clock rate [Hz]

#define EEPROM_TIMEOUT     20                 // write cycle duration
#define MAX_PAGE_SIZE     256                 // write page size limit
#define VIRTUAL_PAGE_SIZE 512                 // virtual page size for read

// Instructions :
#define EEP_WREN    0x06           // Set Write Enable Latch (Enable Write)
#define EEP_WRDI    0x04           // Reset Write Enable/Reset Flag Bit
#define EEP_RDSR    0x05           // Read Status Register
#define EEP_WRSR    0x01           // Write Status Register
#define EEP_READ    0x03           // Read data
#define EEP_WRITE   0x02           // Write data

// Status register format :
#define EEP_MASK_RDY  0x01         // -READY, High if busy
#define EEP_MASK_WEN  0x02         // high if Write Enabled
#define EEP_MASK_BP0  0x04         // protection (1/4 capacity)
#define EEP_MASK_BP1  0x08         // protection (1/2 capacity)
#define EEP_MASK_WPEN 0x80         // Write Protect pin /WP control

#define MkPageAddress( a)     ((a) & ~(FPageSize - 1))
#define MkPageOffset(  a)     ((a) &  (FPageSize - 1))

//******************************************************************************
// Constructor
//******************************************************************************

TEeprom::TEeprom( int PageSize)
// Constructor
{
   FSpi      = new TUsbSpi;
   FPageSize = PageSize;
} // TEeprom

//******************************************************************************
// Destructor
//******************************************************************************

TEeprom::~TEeprom()
// Destructor
{
   if( FSpi){
      delete FSpi;
   }
} // ~TEeprom

//******************************************************************************
// Connected
//******************************************************************************

bool TEeprom::Connected()
// Check if the adapter/module are present
{
   // set write enable :
   if( !Wren()){
      return( false);                  // unable set, probably no adapter
   }
   byte Value;
   // check status register for WEN bit
   if( !Rdsr( Value)){
      return( false);
   }
   if( !(Value & EEP_MASK_WEN)){
      return( false);                  // must be set - probably no module
   }
   // set write disable :
   if( !Wrdi()){
      return( false);                  // unable set, probably no adapter
   }
   // check status register for WEN bit
   if( !Rdsr( Value)){
      return( false);
   }
   if( Value & EEP_MASK_WEN){
      return( false);                  // must be clear - probably no module
   }
   return( true);
} // Connected

//******************************************************************************
// Page Read
//******************************************************************************

bool TEeprom::ReadPage( word Address, void *Data, int Length)
// Read data from EEPROM (limited size)
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   // prepare command :
   byte Buffer[ 6];
   int  Index = 0;
   Buffer[ Index++] = EEP_READ;        // read instruction
   Buffer[ Index++] = Address >> 8;    // high address
   Buffer[ Index++] = Address & 0xFF;  // low address
   if( !FSpi->Write( Buffer, Index)){
      return( false);
   }
   // read data :
   if( !FSpi->Read( Data, Length)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // Read

//******************************************************************************
// Read
//******************************************************************************

bool TEeprom::Read( word Address, void *Data, int Length)
// Read data from EEPROM (any size)
{
   int Size;                           // requested size
   int Index = 0;                      // into Data array
   do {
      Size = Length;                    // remainder of length
      // clip size to virtual page size :
      if( Size > VIRTUAL_PAGE_SIZE){
         Size = VIRTUAL_PAGE_SIZE;
      }
      if( !ReadPage( Address, &((byte *)Data)[ Index], Size)){
         return( false);
      }
      Length  -= Size;                  // remainder of data
      Index   += Size;                  // move data pointer
      // next page :
      Address += Size;                  // next address
   } while( Length);
   return( true);
} // Read

//******************************************************************************
// Page Write
//******************************************************************************

bool TEeprom::WritePage( word Address, void *Data, int Length)
// Write data into EEPROM page (please check the page boundaries)
{
   if( Length > FPageSize){
      IERROR;
   }
   if( !Wren()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   // prepare command :
   byte Buffer[ MAX_PAGE_SIZE + 6];
   int  Index = 0;
   Buffer[ Index++] = EEP_WRITE;       // write instruction
   Buffer[ Index++] = Address >> 8;    // high address
   Buffer[ Index++] = Address & 0xFF;  // low address
   // copy data :
   for( int i = 0; i < Length; i++){
      Buffer[ Index++] = ((byte *)Data)[ i];
   }
   if( !FSpi->Write( Buffer, Index)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( WaitForReady());             // wait for done
} // WritePage

//******************************************************************************
// Write
//******************************************************************************

bool TEeprom::Write( word Address, void *Data, int Length)
// Write data into EEPROM (any size)
{
   if( Length == 0){
      IERROR;
   }
   word Page   = MkPageAddress( Address);
   word Offset = MkPageOffset(  Address);
   int Size    = FPageSize - Offset;   // up to end of page
   int Index   = 0;                    // into Data array
   if( Size > Length){
      Size = Length;                   // write inside page
   }
   do {
      if( !WritePage( Page | Offset, &((byte *)Data)[ Index], Size)){
         return( false);
      }
      Length -= Size;                  // remainder of data
      Index  += Size;                  // move data pointer
      // next page :
      Page   += FPageSize;
      Offset = 0;
      // next fragment :
      Size = Length;
      if( Size > FPageSize){
         Size = FPageSize;             // cut to page size
      }
   } while( Length);
   return( true);
} // Write

//------------------------------------------------------------------------------

//******************************************************************************
// CheckConnect
//******************************************************************************

bool TEeprom::CheckConnect()
// Try connect adapter
{
   if( FSpi->IsOpen){
      return( true);                   // already opened
   }
   // open adapter :
   TIdentifier Identifier;
   if( !FSpi->Locate( USB_DEVICE_NAME, 0, Identifier)){
      return( false);
   }
   if( !FSpi->Open( Identifier)){
      return( false);
   }
   // set SPI parameters :
   FSpi->Flush();
   FSpi->SetMode( TUsbSpi::CLOCK_LOW,
                  TUsbSpi::READ_ON_FALL,
                  TUsbSpi::WRITE_ON_FALL);
   if( !FSpi->SetClockRate( SPI_CLOCK_RATE)){
      return( false);
   }
   // deselect & inactive clock :
   if( !FSpi->SetCS()){
      return( false);
   }
   return( true);
} // CheckConnect

//******************************************************************************
// WREN
//******************************************************************************

bool TEeprom::Wren()
// Write enable command
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   if( !FSpi->WriteByte( EEP_WREN)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // Wren

//******************************************************************************
// WRDI
//******************************************************************************

bool TEeprom::Wrdi()
// Write disable command
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   if( !FSpi->WriteByte( EEP_WRDI)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // Wrdi

//******************************************************************************
// RDSR
//******************************************************************************

bool TEeprom::Rdsr( byte &Value)
// Read status register
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   if( !FSpi->WriteByte( EEP_RDSR)){
      return( false);
   }
   if( !FSpi->ReadByte( Value)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // Rdsr

//******************************************************************************
// WRSR
//******************************************************************************

bool TEeprom::Wrsr( byte Value)
// Write status register
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   if( !FSpi->WriteByte( EEP_WRSR)){
      return( false);
   }
   if( !FSpi->WriteByte( Value)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // Wrsr

//******************************************************************************
// Ready
//******************************************************************************

bool TEeprom::WaitForReady()
// Wait for flash ready
{
   byte Value;
   for( int i = 0; i < EEPROM_TIMEOUT; i++){
      if( !Rdsr( Value)){
         return( false);
      }
      if( !(Value & EEP_MASK_RDY)){
         return( true);
      }
      Sleep( 1);
   }
   return( false);                     // timeout
} // WaitForReady

