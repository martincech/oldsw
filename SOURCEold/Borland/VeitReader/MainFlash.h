//******************************************************************************
//
//   MainFlash.h  Flash low level demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainFlashH
#define MainFlashH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "At45dbxx.h"
#include "../Crt/Crt.h"
#include "../Serial/CrtLogger.h"
#include <ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TLabel *LblAdapter;
        TTimer *Timer;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *LblModule;
        TButton *BtnRead;
        TCheckBox *BtnCheck;
        TButton *BtnWrite;
        void __fastcall Create(TObject *Sender);
        void __fastcall Destroy(TObject *Sender);
        void __fastcall MemoResize(TObject *Sender);
        void __fastcall TimerTimer(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
        void __fastcall BtnWriteClick(TObject *Sender);
//---------------------------------------------------------------------------
public:		// User declarations
   TCrt          *Crt;
   TLogger       *Logger;
   TAt45dbxx     *Flash;
   bool          Connected;
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
