object MainForm: TMainForm
  Left = 422
  Top = 366
  Width = 709
  Height = 584
  Caption = 'TMC communication'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object MainMemo: TMemo
    Left = 8
    Top = 24
    Width = 449
    Height = 497
    TabOrder = 0
  end
  object BtnSend: TButton
    Left = 504
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 1
    OnClick = BtnSendClick
  end
  object BtnRepeatSend: TButton
    Left = 504
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Repeat Send'
    TabOrder = 2
    OnClick = BtnRepeatSendClick
  end
  object CbEnableLogger: TCheckBox
    Left = 504
    Top = 16
    Width = 97
    Height = 17
    Caption = 'Enable Logger'
    Checked = True
    State = cbChecked
    TabOrder = 3
    OnClick = CbEnableLoggerClick
  end
  object BtnLongSend: TButton
    Left = 504
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Long Send'
    TabOrder = 4
    OnClick = BtnLongSendClick
  end
  object BtnLongRepeat: TButton
    Left = 504
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Long Repeat'
    TabOrder = 5
    OnClick = BtnLongRepeatClick
  end
end
