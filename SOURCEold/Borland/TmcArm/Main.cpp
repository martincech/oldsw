//******************************************************************************
//
//   Main.cpp     TMC main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
   Adapter = new TPktAdapter;
}

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
   Adapter->Logger = Logger;
}

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
}

//******************************************************************************
// Enable Logger
//******************************************************************************

void __fastcall TMainForm::CbEnableLoggerClick(TObject *Sender)
{
   if( CbEnableLogger->Checked){
      Adapter->Logger = Logger;
   } else {
      Adapter->Logger = 0;
   }
}

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
byte Buffer[ 128];
int  Size;

   Buffer[ 0] = '?';
   Buffer[ 1] = '?';
   if( !Adapter->Send( Buffer, 2)){
      Crt->printf( "Unable send\n");
      return;
   }
   if( !Adapter->Receive( Buffer, Size)){
      Crt->printf( "Rx error\n");
      return;
   }
   Crt->printf( "Rx OK\n");
}

//******************************************************************************
// Repeat send
//******************************************************************************

void __fastcall TMainForm::BtnRepeatSendClick(TObject *Sender)
{
byte Buffer[ 128];
int  Size;

   for( int i = 0; i < 100; i++){
      Buffer[ 0] = '?';
      Buffer[ 1] = '?';
      if( !Adapter->Send( Buffer, 2)){
         Crt->printf( "Unable send\n");
         return;
      }
      if( !Adapter->Receive( Buffer, Size)){
         Crt->printf( "Rx error\n");
         return;
      }
      Sleep( 5);
   }
   Crt->printf( "Rx OK\n");
}

//******************************************************************************
// Long Send
//******************************************************************************

void __fastcall TMainForm::BtnLongSendClick(TObject *Sender)
{
byte Buffer[ 2000];
int  Size;

   memset( Buffer, '?', sizeof( Buffer));
   if( !Adapter->Send( Buffer, sizeof( Buffer))){
      Crt->printf( "Unable send\n");
      return;
   }
   if( !Adapter->Receive( Buffer, Size)){
      Crt->printf( "Rx error\n");
      return;
   }
   Crt->printf( "Rx OK\n");
}

//******************************************************************************
// Long Repeat
//******************************************************************************

void __fastcall TMainForm::BtnLongRepeatClick(TObject *Sender)
{
byte Buffer[ 2000];
int  Size;

   for( int i = 0; i < 100; i++){
      memset( Buffer, '?', sizeof( Buffer));
      if( !Adapter->Send( Buffer, sizeof( Buffer))){
         Crt->printf( "Unable send\n");
         return;
      }
      if( !Adapter->Receive( Buffer, Size)){
         Crt->printf( "Rx error\n");
         return;
      }
      Sleep( 5);
   }
   Crt->printf( "Rx OK\n");
}

