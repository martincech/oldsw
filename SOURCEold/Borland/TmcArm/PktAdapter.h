//******************************************************************************
//
//   PktAdapter.h     Packet adapter
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#ifndef PktAdapterH
   #define PktAdapterH

#ifndef UartH
   #include "../Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Serial/Logger.h"
#endif

#ifndef __PktDef_H__
   #include "PktDef.h"
#endif

#define PKT_MAX_BUFFER   2048

//******************************************************************************
// TPktAdapter
//******************************************************************************

class TPktAdapter {
public :
   TPktAdapter();
   // Constructor
   ~TPktAdapter();
   // Destructor
   void Close();
   // Close communication port
   bool Send( void *Data, int Size);
   // Send message
   bool Receive( void *Data, int &Size);
   // Receive message

   __property TUart   *Port                    = {read=FPort};
   __property int      RxTimeout               = {read=FRxTimeout, write=FRxTimeout};
   __property int      RxIntercharacterTimeout = {read=FRxIntercharacterTimeout, write=FRxIntercharacterTimeout};
   __property int      RxPacketTimeout         = {read=FRxPacketTimeout, write=FRxPacketTimeout};

   __property TLogger *Logger                  = {read=FLogger, write=FLogger};
   __property TUart::TParameters *Parameters   = {read=GetParameters};
   __property TString  Device                  = {read=FDevice, write=FDevice};
//------------------------------------------------------------------------------

protected :
   TUart   *FPort;                     // connection port
   TLogger *FLogger;                   // raw data logger
   int      FRxTimeout;                // Rx timeout
   int      FRxIntercharacterTimeout;  // Rx intercharacter timeout
   int      FRxPacketTimeout;          // Rx packet timeout
   TUart::TParameters FParameters;     // Communication parameters
   TString  FDevice;                   // USB device name
   byte     Buffer[ PKT_MAX_BUFFER];   // packet buffer

   void FlushRxChars();
   // Flush chars up to intercharacter timeout

   bool CheckConnect();
   // Check if adapter is ready
   void Disconnect();
   // Disconnect port
   word CalcCrc( void *Data, int Size);
   // Calculate CRC
   TUart::TParameters *GetParameters(){ return( &FParameters);}
};

#endif
