//******************************************************************************
//
//   Demo.h       GSM demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef DemoH
#define DemoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "GSM.h"
#include "..\Serial\CrtLogger.h"
#include "..\Crt\Crt.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TButton *BtnConnect;
        TButton *BtnCheck;
        TMemo *Memo;
        TEdit *TxText;
        TEdit *RxText;
        TButton *BtnSend;
        TEdit *PortName;
        TEdit *RxFrom;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *TxTo;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Status;
        TLabel *LblRegistered;
        TLabel *LblOperator;
        TLabel *LblSignalStrength;
        TButton *BtnAutodetect;
        TButton *BtnReceive;
        TButton *BtnDelete;
        TEdit *DeleteAddress;
        TLabel *Label5;
        TButton *BtnCount;
        TButton *BtnSimTest;
   TButton *BtnSend1;
   TButton *BtnSend2;
        void __fastcall BtnConnectClick(TObject *Sender);
        void __fastcall BtnCheckClick(TObject *Sender);
        void __fastcall BtnSendClick(TObject *Sender);
        void __fastcall BtnReceiveClick(TObject *Sender);
        void __fastcall BtnAutodetectClick(TObject *Sender);
        void __fastcall BtnDeleteClick(TObject *Sender);
        void __fastcall BtnCountClick(TObject *Sender);
        void __fastcall BtnSimTestClick(TObject *Sender);
   void __fastcall BtnSend2Click(TObject *Sender);
   void __fastcall BtnSend1Click(TObject *Sender);
public:	// User declarations
   TGsmModem  *Gsm;
   TCrt       *Crt;
   TCrtLogger *Logger;
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
