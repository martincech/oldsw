//******************************************************************************
//
//   Demo.cpp     GSM demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Demo.h"
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Gsm          = new TGsmModem;
   Crt          = new TCrt( Memo);
   Logger       = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Gsm->Logger  = Logger;
   Gsm->BaudRate     = 38400;
   Gsm->CmdTimeout   = 500;
   Gsm->SmsRxTimeout = 2000;
} // TMainForm

//******************************************************************************
// Autodetect
//******************************************************************************

#define MAX_COM_NUMBER 10

void __fastcall TMainForm::BtnAutodetectClick(TObject *Sender)
{
   Gsm->Disconnect(); // disconnect active device (unable to detect connected devices)
   char ComName[ 10];
   TString DeviceModel;
   for( int i = 1; i <= MAX_COM_NUMBER; i++){
      sprintf( ComName, "COM%d", i);        // COMx string
      if( !TGsmModem::Detect( ComName, DeviceModel)){
         continue;
      }
      Crt->printf( "%s : '%s'\n", ComName, DeviceModel.c_str());
   }
   Crt->printf( "\nWarning : Active device DISCONNECTED");
} // BtnAutodetectClick

//******************************************************************************
// Connect
//******************************************************************************

void __fastcall TMainForm::BtnConnectClick(TObject *Sender)
{
   if( !Gsm->Connect( PortName->Text)){
      Status->Caption = "Status : ERR";
      return;
   }
   if( !Gsm->Reset()){
      Status->Caption = "Status : reset ERR";
      return;
   }
   Status->Caption = "Status : OK";
} // BtnConnectClick

//******************************************************************************
// Check
//******************************************************************************

void __fastcall TMainForm::BtnCheckClick(TObject *Sender)
{
   if( !Gsm->Check()){
      Status->Caption = "Status : ERR";
      return;
   }
   // sila signalu :
   int SignalStrength;
   Gsm->SignalStrength( SignalStrength);
   AnsiString SSNumber( SignalStrength);
   LblSignalStrength->Caption = SSNumber;
   // registrace :
   LblOperator->Caption   = "";
   if( Gsm->IsRegistered()){
      LblRegistered->Caption = "Registered";

      AnsiString Name;
      Gsm->Operator( Name);
      LblOperator->Caption = Name;
   } else {
      LblRegistered->Caption = "Unregistered";
      LblOperator->Caption = "?";
   }
   Status->Caption = "Status : OK";
} // BtnCheckClick

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
   Status->Caption = "Status : ?";
   Status->Refresh();
   if( TxText->Text.Length() == 0){
      Status->Caption = "Status : No text";
      return;
   }
   if( TxTo->Text.Length() == 0){
      Status->Caption = "Status : No destination number";
      return;
   }
   Status->Caption = "Status : ?";
   Status->Refresh();
   // hodiny :
   Application->ProcessMessages();          // vyber cekajici zpravy
   Screen->Cursor          = crHourGlass;   // presypaci hodiny
   if( !Gsm->SmsSend( TxTo->Text, TxText->Text.c_str())){
      Screen->Cursor  = crDefault;          // zrus hodiny
      Status->Caption = "Status : ERR";
      return;
   }
   Screen->Cursor  = crDefault;             // zrus hodiny
   Status->Caption = "Status : OK";
} // BtnSendClick

//******************************************************************************
// Receive
//******************************************************************************

void __fastcall TMainForm::BtnReceiveClick(TObject *Sender)
{
TString From, Message;
static  int   Address = 0;

   if( !Gsm->SmsReceive( Address, From, Message)){
      Status->Caption = "Status : Address " + AnsiString( Address) + " ERR";
      return;
   }
   if( From == ""){
      Status->Caption = "Status : Address " + AnsiString( Address) + " EMPTY";
   } else {
      Status->Caption = "Status : Address " + AnsiString( Address) + " OK";
   }
   RxFrom->Text = From;
   RxText->Text = Message;
   Address++;
   if( Address >= Gsm->MemorySize){
      Address = 0;
   }
} // BtnReceiveClick

//******************************************************************************
// Delete
//******************************************************************************

void __fastcall TMainForm::BtnDeleteClick(TObject *Sender)
{
int Address;

   sscanf( DeleteAddress->Text.c_str(), "%d", &Address);
   if( !Gsm->SmsDelete( Address)){
      Status->Caption = "Status : ERR";
   }
   Status->Caption = "Status : Address " + AnsiString( Address) + " OK";
} // BtnDeleteClick

//******************************************************************************
// SMS count
//******************************************************************************

void __fastcall TMainForm::BtnCountClick(TObject *Sender)
{
   int Count;
   Count = Gsm->SmsCount();
   if( Count < 0){
      Status->Caption = "Status : ERR";
      return;
   }
   Crt->printf( "\nSMS count = %d\n", Count);
} // BtnCountClick

//******************************************************************************
// SIM Test
//******************************************************************************

void __fastcall TMainForm::BtnSimTestClick(TObject *Sender)
{
   if( !Gsm->Connect( PortName->Text)){
      Status->Caption = "Status : ERR port busy";
      return;
   }
   if( !Gsm->TestSim()){
      Status->Caption = "Status : ERR";
      return;
   }
   Status->Caption = "Status : OK";
} // HtnSimTestClick

