//******************************************************************************
//
//   GSMraw.c      GSM raw utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

//****** Uprava pro modem GE846-PY

#include <vcl.h>
#pragma hdrstop

#include "GSMraw.h"
#include "COM_util.h"

// Preddefinovane retezce :

// Zakladni operace :
static char Ok[]         = "\r\nOK\r\n";           // ok reply
static char EchoOff[]    = "ATE0\r";               // factory default, echo off
static char ErrorMode[]  = "ATV1+CMEE=1\r";        // Extended error mode
static char TextMode[]   = "AT+CMGF=1\r";          // SMS text mode

static char Registered[] = "AT+CREG?\r";           // Registration
static char RegHeader[]  = "\r\n+CREG: ";          // Header of registration

static char Operator[]   = "AT+COPS?\r";           // Get operator
static char OperHeader[] = "\r\n+COPS: ";          // operator header

static char Rssi[]       = "AT+CSQ\r";             // Get Received Signal Strength
static char RssiHeader[] = "\r\n+CSQ: ";           // Signal strength header

// SMS :
static char PduSend[]    = "AT+CMGS=\"+";          // Send SMS + "+<number."<CR>
static char Prompt[]     = "\r\n> ";               // input prompt
static char SendOk[]     = "\r\n+CMGS: ";          // Send ok (+message reference, pduack)

static char PduRead[]    = "AT+CMGR=";             // Read SMS <index><CR>
static char ReadHeader1[]= "\r\n+CM";              // Header of read message
static char ReadHeader2[]= "R: ";                  // Header of read message
//static char EmptyRead[]  = " ERROR: 321\r\n";      // empty SMS message
static char EmptyRead[]  = " ERROR: 3";            // empty SMS message CMS ERROR 321/CMS ERROR 303
static char ReceivedMsg[]= "\"REC";                // REC READ/REC UNREAD status

static char Delete[]     = "AT+CMGD=";             // Delete SMS <index><CR>

// Memory settings :
static char MemoryPhone[] = "AT+CPMS=MT,MT,MT\r";  // SMS data in the internal memory
static char MemorySim[]   = "AT+CPMS=SM,SM,SM\r";  // SMS data in the SIM memory
static char MemoryHeader[]= "\r\n+CPMS: ";         // Header of memory settings

// datove spojeni
static char Ring[]       = "\r\nRING\r\n";         // incomming call
static char OffHook[]    = "ATA\r";                // zvednuti
static char OnHook[]     = "ATH\r";                // zaveseni
static char CommandEsc[] = "+++";                  // escape sekvence pro prikazovy mod
//static char NoCarrier[]  = "\r\nNO CARRIER\r\n";   // preruseni spojeni
static char Connect[]    = "\r\nCONNECT";          // navazani spojeni

static byte RingIndex = 0;                         // kontextova promenna pro zvoneni


#define CTRL_Z  '\x1A'

#define PDU_FIXED_LENGTH      8        // konstatni delka telegramu bez SMSC
#define TIMESTAMP_LENGTH     14        // delka sekce cas+zona

#define SMS_PROMPT_TIMEOUT  100        // Send prompt timeout      * 0.1s


#define TxString( s) ComFlushChars();ComTxString( s)    // pred vyslanim zrus Rx

//-Hardware.h-------------------------------------------------------------------

#define SMS_MAX_LENGTH       160       // max. delka prijate zpravy
#define GSM_SMS_SEND_TIMEOUT  15       // SMS center reply timeout [s]
#define GSM_CONNECT_TIMEOUT  200       // od zvednuti do navazani spojeni
#define SMS_EXPIRATION       167       // SMS send expiration

#define SysDelay( ms)  Sleep( ms)

//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
// Reset
//------------------------------------------------------------------------------

TYesNo GsmReset( void)
// Inicializace modemu
{
   // Echo off :
   TxString( EchoOff);
   ComRxWait( 1);                      // cekani na odpoved
   if( !ComWaitChar( '\n')){           // pred OK
      return( NO);
   }
   if( !ComWaitChar( '\n')){           // za OK
      return( NO);
   }
   // SMS mode :
   ComTxString( ErrorMode);
   if( !ComRxMatch( Ok, 1)){
      return( NO);
   }
   ComTxString( TextMode);
   if( !ComRxMatch( Ok, 1)){
      return( NO);
   }
   return( YES);
} // GsmReset

//******************************************************************************
// Registrace
//******************************************************************************

TYesNo GsmRegistered( void)
// Vraci YES, je-li modem registrovan v siti
{
byte Value;

   TxString( Registered);
   if( !ComRxMatch( RegHeader, 1)){
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <n>
      return( NO);
   }
   Value = ComRxDec();                 // pole <stat>
   if( Value == 1 || Value == 5){
      return( YES);                    // registered or roaming
   }
   return( NO);
} // GsmRegistered

//******************************************************************************
// Operator
//******************************************************************************

TYesNo GsmOperator( char *Name)
// Vrati retezec s nazvem operatora,
// POZOR delka <Name> musi byt min SMS_MAX_OPERATOR+1
{
   TxString( Operator);
   if( !ComRxMatch( OperHeader, 1)){
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <mode>
      return( NO);
   }
   if( !ComWaitChar( ',')){            // preskoc pole <format>
      return( NO);
   }
   if( !ComRxString( Name, SMS_MAX_OPERATOR)){
      return( NO);
   }
   return( YES);
} // GsmOperator

//******************************************************************************
// Sila signalu
//******************************************************************************

byte GsmSignalStrength( void)
// Vrati relativni silu signalu 0..31 nebo 99 nejde-li zjistit
{
byte Value;

   TxString( Rssi);
   if( !ComRxMatch( RssiHeader, 1)){
      return( 99);
   }
   Value = ComRxDec();                 // relativni sila
   return( Value);
} // GsmSignalStrength

//******************************************************************************
// Registrace
//******************************************************************************

TYesNo SmsMemory( byte Location)
// Nastaveni pameti SMS <Location> viz TSmsMemory
{
byte Value;

   switch( Location){
      case SMS_SIM_MEMORY :
         TxString( MemorySim);
         break;
      case SMS_PHONE_MEMORY :
         TxString( MemoryPhone);
         break;
      default :
         return( NO);
   }
   if( !ComRxMatch( MemoryHeader, 50)){
      return( NO);
   }
   if( !ComWaitChar( '\r')){           // preskoc na konec radku
      return( NO);
   }
   if( !ComWaitChar( '\n')){
      return( NO);
   }
   if( !ComRxMatch( Ok, 1)){           // OK
      return( NO);
   }
   return( YES);
} // GsmMemory

//******************************************************************************
// SMS parameters
//******************************************************************************

static char SendParams1[] = "AT+CSMP=17,";     // SMS send parameters part 1 + <expiration>
static char SendParams2[] = ",0,0\r";          // SMS send parameters part 1

TYesNo SmsSendParameters( void)
// Nastaveni implicitnich parametru pro SMS send
{
   TxString( SendParams1);
   ComTxDec( SMS_EXPIRATION);
   TxString( SendParams2);
   if( !ComRxMatch( Ok, 1)){
      return( NO);
   }
   return( YES);
} // SmsSendParameters

//-----------------------------------------------------------------------------
// Send
//-----------------------------------------------------------------------------

TYesNo SmsSend( char *DstNumber, char *Text, byte MessageLength)
// Odeslani zpravy
{
byte   i;
byte   ch;

   // AT command SMS send :
   TxString( PduSend);
   ComTxString( DstNumber);            // destination number
   ComTxChar( '"');                    // close string with "
   ComTxChar( '\r');
   // wait for prompt :
   if( !ComRxMatch( Prompt, SMS_PROMPT_TIMEOUT)){
      return( NO);
   }
   // send text :
   for( i = 0; i < MessageLength; i++){
      ch = Text[ i];
      // ASCII to GSM alphabet :
      if( ch == '@'){
         ch = '\0';
      }
      ComTxChar( ch);
   }
   ComTxChar( CTRL_Z);                 // message terminator
#ifdef GSM_SMS_USE_GSMCTL
   // Nacteni odpovedi necham na automat
   GsmCtl.Status  = GSM_SMS_SENDING;       // kontext radice
   GsmCtl.Counter = GSM_SMS_SEND_TIMEOUT;  // timeout potvrzeni
#else
   // Automat se nepouziva, pockam na odpoved
   if( !ComRxMatch( SendOk, 10 * GSM_SMS_SEND_TIMEOUT)){        // Timeout se do ComRxMatch() zadava v 0.1sec
      return( NO);
   }
 #endif
   return( YES);
} // SmsSend

//-----------------------------------------------------------------------------
// Receive
//-----------------------------------------------------------------------------

byte SmsRead( byte Index, char *SrcNumber, char *Text)
// Precte prijatou zpravu z pozice <Index>, vrati jeji delku
{
byte MessageLength;
byte i;
byte ch;

   Index++;                            // cislovano od 1
   // AT prikaz Read :
   TxString( PduRead);
   ComTxDec( Index);                   // pozice
   ComTxChar( '\r');
   if( !ComRxMatch( ReadHeader1, 5)){
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   // read first character :
   if( !ComRxChar( &ch)){
      return( SMS_READ_ERROR);
   }
   if( ch == 'G'){
      // CMGR:
      if( !ComRxMatch( ReadHeader2, 1)){
         return( SMS_READ_ERROR);      // neplatna hlavicka odpovedi
      }
   } else if( ch == 'S'){
      // CMS ERROR
      if( ComRxMatch( EmptyRead, 1)){
         return( SMS_EMPTY_MESSAGE);
      }
      return( SMS_READ_ERROR);
   } else {
      // unknown string
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   // test for message type :
   if( !ComRxMatch( ReceivedMsg, 1)){
      return( SMS_READ_ERROR);
   }
   if( !ComWaitChar( ',')){
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // phone number :
   if( !ComRxString( SrcNumber, GSM_MAX_NUMBER)){
      return( SMS_READ_ERROR);         // invalid sequence
   }
   if( SrcNumber[ 0] == '+'){          // remove leading + sign
      ch = strlen( SrcNumber);         // sorry
      for( i = 0; i < ch; i++){
          SrcNumber[ i] = SrcNumber[ i + 1];
      }
   }
   // skip up to end of line :
   if( !ComWaitChar( '\n')){
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // text of the message :
   MessageLength = ComRxDelimiter( Text, SMS_MAX_LENGTH, '\r');
   if( !MessageLength){
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // transform from GSM alphabet :
   for( i = 0; i < MessageLength; i++){
      ch = Text[ i];
      if( ch == '\0'){
         Text[ i] = '@';
      }
   }
   return( MessageLength);
} // SmsRead

//-----------------------------------------------------------------------------
// Delete
//-----------------------------------------------------------------------------

void SmsDelete( byte Index)
// Smaze prijatou zpravu z pozice <Index>
{
   Index++;                            // cislovano od 1
   TxString( Delete);
   ComTxDec( Index);                   // pozice
   ComTxChar( '\r');
} // SmsDelete

//-----------------------------------------------------------------------------
// Zvoneni
//-----------------------------------------------------------------------------

TYesNo GsmIsRinging( void)
// Kontrola prichoziho hovoru
{
char ch;

   while( ComRxChar( &ch)){
      if( ch != Ring[ RingIndex]){
         RingIndex = 0;                // znovu od zacatku retezce
         // zopakuj test pro prvni znak :
         if( ch != Ring[ RingIndex]){
            continue;                  // ani prvni nesouhlasi
         }
      }
      RingIndex++;
      if( RingIndex < sizeof( Ring) - 1){
         continue;                     // cekej dalsi znak
      }
      RingIndex = 0;                   // priprava na dalsi
      return( YES);                    // uspesny prijem
   }
   return( NO);                        // zadny znak
} // GsmIsRinging

//-----------------------------------------------------------------------------
// Navazani spojeni
//-----------------------------------------------------------------------------

TYesNo GsmConnect( void)
// Zvedne sluchatko a navaze spojeni
{
   TxString( OffHook);                 // zvedni
   if( !ComRxMatch( Connect, GSM_CONNECT_TIMEOUT)){
      GsmDisconnect();
      return( NO);                     // nepodarilo se navazat
   }
   ComWaitChar( '\n');                 // preskoc zbytek zpravy Connect
   return( YES);
} // GsmConnect

//-----------------------------------------------------------------------------
// Ukonceni spojeni
//-----------------------------------------------------------------------------

void GsmDisconnect( void)
// Prepne do prikazoveho rezimu a zavesi
{
   SysDelay( 1000);                    // povinne cekani pred ESC
   TxString( CommandEsc);              // ESC
   SysDelay( 1000);                    // povinne cekani po ESC
   if( !ComRxMatch( Ok, 10)){
      return;                          // nebyl v datovem rezimu
   }
   TxString( OnHook);                  // zaves
} // GsmDisconnect

