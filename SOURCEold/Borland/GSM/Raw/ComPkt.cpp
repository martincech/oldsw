//*****************************************************************************
//
//   ComPkt.c    COM packet interface
//   Version 1.0 (c) Vymos
//
//*****************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "ComPkt.h"

// Lokalni data :

#ifdef __COM_BLOCK__
byte _com_data_crc;         // pro interni pouziti
#endif

//--------- Hardware.h---------------------------------------------------------
#define PACKET_RX_TIMEOUT   2          // ceka na prvni znak paketu * 0.1s
//--------- Hardware.h---------------------------------------------------------


//-----------------------------------------------------------------------------
// Prijem
//-----------------------------------------------------------------------------

TYesNo ComRxPacket( byte *cmd, dword *arg)
// Prijme paket, vrati typ prikazu <cmd> a parametr prikazu <arg>.
// Vrati NO, pokud paket neprisel
{
TDataConvertor dc;
byte b;
byte crc;

   if( !ComRxWait( PACKET_RX_TIMEOUT)){
      return( NO);                     // timeout
   }
   ComRxChar( &b);
   if( b != CPKT_SHORT_START){
      //!!! vyhodnoceni konce spojeni
      ComFlushChars();                 // prijato smeti, cekat na dobeh vsech znaku
      return( NO);
   }
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc  = b;
   *cmd = b;
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 3] = b;                   // LSB
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 2] = b;
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 1] = b;
   if( !ComRxChar( &b)){
      return( NO);
   }
   crc += b;
   dc.array[ 0] = b;                   // MSB
   *arg = dc.dw;
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != (byte)~crc){
      ComFlushChars();                 // dobeh vsech znaku
      return( NO);                     // chyba zabezpeceni
   }
   if( !ComRxChar( &b)){
      return( NO);
   }
   if( b != CPKT_SHORT_END){
      ComFlushChars();                 // dobeh vsech znaku
      return( NO);                     // neplatne zakonceni
   }
   return( YES);
} // ComRxPacket

//-----------------------------------------------------------------------------
// Vyslani
//-----------------------------------------------------------------------------

void ComTxPacket( byte cmd, dword arg)
// Vysle paket s kodem prikazu <cmd> a parametrem <arg>
{
TDataConvertor dc;
byte crc;

   dc.dw = arg;
   ComTxChar( CPKT_SHORT_START);
   crc    = cmd;
   crc   += dc.array[ 0];
   crc   += dc.array[ 1];
   crc   += dc.array[ 2];
   crc   += dc.array[ 3];
   ComTxChar( cmd);
   ComTxChar( dc.array[ 3]);           // LSB
   ComTxChar( dc.array[ 2]);
   ComTxChar( dc.array[ 1]);
   ComTxChar( dc.array[ 0]);           // MSB
   ComTxChar( ~crc);
   ComTxChar( CPKT_SHORT_END);
} // ComTxPacket

#ifdef __COM_BLOCK__
//-----------------------------------------------------------------------------
// Datovy blok - start
//-----------------------------------------------------------------------------

void ComTxBlockStart( word size)
// Vysle zahlavi paketu pro data o velikosti <size>
{
   ComTxChar( CPKT_DATA_START);
   ComTxChar( size & 0xFF);            // Size1 LSB
   ComTxChar( size >> 8);              // MSB
   ComTxChar( size & 0xFF);            // Size2 LSB
   ComTxChar( size >> 8);              // MSB
   ComTxChar( CPKT_DATA_START);
   _com_data_crc = 0;
} // ComTxBlockStart


//-----------------------------------------------------------------------------
// Datovy blok - konec
//-----------------------------------------------------------------------------

void ComTxBlockEnd( void)
// Uzavre datovy blok
{
   ComTxChar( ~_com_data_crc);
   ComTxChar( CPKT_DATA_END);
} // ComTxBlockEnd
#endif

