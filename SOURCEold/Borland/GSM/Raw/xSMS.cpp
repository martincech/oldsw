//******************************************************************************
//
//   SMS.cpp      GSM network SMS service
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "SMS.h"

#pragma package(smart_init)
//---------------------------------------------------------------------------

#define MAX_TEXT        512           // max raw message length
#define MAX_NUMBER       16           // max Number count
#define MAX_SMS         160           // max SMS length

#define SMSC_TIMEOUT  10000           // SMS center reply timeout
#define ME_TIMEOUT     1000           // ME timeout
#define ICH_TIMEOUT      50           // intercharacter timeout
#define SIM_MEMORY_SIZE  10           // SIM memory size

// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, (void *)Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, (void *)Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, (void *)Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

static const char At[]        = "AT\r";             // AT
static const char Ok[]        = "\r\nOK\r\n";       // ok reply
static const char Error[]     = "\r\n+CMS ERROR: "; // error reply (+number)
static const char EchoOff[]   = "ATE0\r";           // echo off
static const char EchoOk[]    = "ATE0\r\r\nOK\r\n"; // echo off ok reply
static const char PduMode[]   = "AT+CMGF=0\r";      // SMS PDU mode
static const char Prompt[]    = "\r\n> ";           // input prompt
static const char PduSend[]   = "AT+CMGS=%d\r";     // Send SMS <length>
static const char SendOk[]    = "\r\n+CMGS: ";      // Send ok (+message reference, pduack)
static const char ErrorMode[] = "ATV1+CMEE=1\r";    // Error message level
static const char PduRead[]   = "AT+CMGR=%d\r";     // Read SMS <index>
static const char EmptyRead[] = "\r\n+CMGR: 0,,0\r\n\r\nOK\r\n";// Empty read
static const char ReadHeader[] = "\r\n+CMGR: ";     // Header of nonempty message
static const char Delete[]    = "AT+CMGD=%d\r";     // Delete SMS <index>




#define CTRL_Z  '\x1A'

#define nibble2hex( n)   ((n) > 9 ? 'A' + (n) - 10 : '0' + (n))

//******************************************************************************
// Constructor
//******************************************************************************

TSms::TSms()
// Constructor
{
   FPort                  = new TUart;
   FLogger                = 0;
   FTotalTimeout          = ME_TIMEOUT;
   FIntercharacterTimeout = ICH_TIMEOUT;
   FSmsCenterTimeout      = SMSC_TIMEOUT;
   FMemorySize            = SIM_MEMORY_SIZE;
} //TSms

//******************************************************************************
// Destructor
//******************************************************************************

TSms::~TSms()
// Destructor
{
   if( FPort){
      delete FPort;
   }
} // ~TSsms

//******************************************************************************
// Connect
//******************************************************************************

bool TSms::Connect( TString Port)
// Connect via <Port>
{
   // disconnect old port :
   if( FPort){
      delete FPort;
      FPort = 0;
   }
   // open new port :
   TUart *Uart = new TUart;
   TIdentifier Identifier;
   if( !Uart->Locate( Port, Identifier)){
      delete Uart;
      return( false);
   }
   if( !Uart->Open( Identifier)){
      return( false);
   }
   // set communication parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate  = 9600;
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   Uart->SetRxWait( FTotalTimeout, FIntercharacterTimeout);
   Uart->Flush();
   FPort = Uart;
   return( true);
} // Connect

//******************************************************************************
// Reset
//******************************************************************************

bool TSms::Reset()
// Initialize modem device
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   // Echo off :
   if( !TxString( EchoDefault)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, EchoOk)){
      if( !strequ( RxBuffer, Ok)){
         return( false);
      }
   }
   // PDU mode :
   if( !TxString( PduMode)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // Long error code :
   if( !TxString( ErrorMode)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   return( true);
} // Reset

//******************************************************************************
// Check
//******************************************************************************

bool TSms::Check()
// Check for modem device
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( At)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   return( true);
} // Check

//******************************************************************************
// Send
//******************************************************************************

bool TSms::Send( TString To, char *Format, ...)
// Send SMS to <To> number (printf format)
{
static char Text[ MAX_TEXT];         // text of message

   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   // expand text :
   va_list Arg;
   va_start( Arg, Format);
   vsprintf( Text, Format, Arg);
   int MessageLength = strlen( Text);
   if( MessageLength > MAX_SMS){
      return( false);
   }
   AsciiToGsm( Text);                // convert into GSM aplhabet ! may contain 0 chars
   // prepare Tx buffer :
   int Index = 0;
   memset( TxBuffer, 0, sizeof( TxBuffer));
   // SMS Center number :
   TxBuffer[ Index++] = 0;           // use default
   // Status :
   TxBuffer[ Index++] = 0x11;        // use expiration, send PDU
   // Message reference number :
   TxBuffer[ Index++] = 0;           // assign by ME
   // Destination number :
   char *p = To.c_str();
   int  NumberLength = To.Length();
   if( NumberLength > MAX_NUMBER){
      IERROR;
   }
   TxBuffer[ Index++] = NumberLength;  // number length
   TxBuffer[ Index++] = 0x91;          // international format
   // convert <To> to lo-endian BCD number :
   byte *Number = &TxBuffer[ Index];
   byte Digit;
   for( int i = 0; i < NumberLength; i++){
      if( !isdigit( *p)){
         IERROR;
      }
      Digit = *p - '0';
      if( i & 1){
         // odd position
         Number[ i/2] |= Digit << 4;
      } else {
         // even position
         Number[ i/2] |= Digit;
      }
      p++;
   }
   if( NumberLength & 1){
      // event count
      Number[ NumberLength / 2] |= 0xF0;    // fill MSB with 0xF0
      NumberLength++;
   }
   Index += NumberLength / 2;
   // protocol flags :
   TxBuffer[ Index++] = 0;             // no hi-level protocol
   TxBuffer[ Index++] = 0;             // default 7-bit GSM aplhabet
   TxBuffer[ Index++] = 0xAA;          // expiration 4 days
   // message :
   TxBuffer[ Index++] = MessageLength; // message length
   // transform into 7-bit character stream :
   int  BitOffset = 0;                 // bit offset to output stream
   int  ByteIndex;                     // byte offset to output stream
   int  Shift;                         // bit shift
   word Twin;                          // character pair
   for( int i = 0; i < MessageLength; i++){
      ByteIndex  = BitOffset / 8;
      ByteIndex += Index;              // start of stream
      Shift      = BitOffset % 8;
      Twin       = Text[ i] & 0x7F;
      Twin     <<= Shift;
      TxBuffer[ ByteIndex]     |= (byte)Twin;        // LSB
      TxBuffer[ ByteIndex + 1] |= (byte)(Twin >> 8); // MSB
      BitOffset += 7;
   }
   Index += (MessageLength * 7 - 1) / 8 + 1;
   // SMS send :
   sprintf( Text, PduSend, Index - 1); // length without SMS Center address
   if( !TxString( Text)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, Prompt)){
      return( false);
   }
   // Convert binary into ASCII :
   ByteIndex = 0;
   byte Value;
   for( int i = 0; i < Index; i++){
      Value = hnibble( TxBuffer[ i]);             // MSB first
      Text[ ByteIndex++] = nibble2hex( Value);
      Value = lnibble( TxBuffer[ i]);             // LSB next
      Text[ ByteIndex++] = nibble2hex( Value);
   }
   Text[ ByteIndex++] = CTRL_Z;        // Ctrl-Z terminator
   Text[ ByteIndex++] = '\0';          // zero terminator
   // set SMS Center timeout :
   FPort->SetRxWait( FSmsCenterTimeout, FIntercharacterTimeout);
   if( !TxString( Text)){
      return( false);
   }
   if( !RxString()){
      FPort->SetRxWait( FTotalTimeout, FIntercharacterTimeout);
      return( false);
   }
   FPort->SetRxWait( FTotalTimeout, FIntercharacterTimeout);
   if( !strnequ( RxBuffer, SendOk, strlen( SendOk))){
      return( false);
   }
   return( true);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TSms::Receive( TString &From, TString &Message)
// Read received message. Returns false if no message is waiting
{
static char Text[ MAX_TEXT];

   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   int NewMessage = 0;             // address of new message
   for( int i = 1; i <= FMemorySize; i++){
      sprintf( Text, PduRead, i);  // read from memory address <i>
      if( !TxString( Text)){
         return( false);
      }
      if( !RxString()){
         return( false);
      }
      if( strnequ( RxBuffer, Error, strlen( Error))){
         return( false);               // there is something wrong
      }
      if( !strnequ( RxBuffer, EmptyRead, strlen( EmptyRead))){
         NewMessage = i;
         break;                        // nonempty message
      }
   }
   if( !NewMessage){
      return( false);                  // no new message
   }
   if( !strnequ( RxBuffer, ReadHeader, strlen( ReadHeader))){
      return( false);
   }
   int Index = strlen( ReadHeader);    // skip Reply header
   if( RxBuffer[ Index] != '0' && RxBuffer[ Index] != '1'){
      IERROR;                          // flag other than received message
   }
   Index++;
   Index++;                            // skip comma
   while( RxBuffer[ Index++] != ',');  // skip <alpha> field
   int PduLength = atoi( &RxBuffer[ Index]);
   while( RxBuffer[ Index++] != '\n'); // skip end of line
   // binary data
   byte Value;
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++; // SMSC length
   Index += Value * 2;                 // Skip SMS Center number
   int PduStart  = Index;              // PDU start here
   Index += 2;                         // Skip flag
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   int NumberLength = Value;           // Sender number length
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   if( Value != 0x91 && Value != 0x81 && Value != 0xA1){
      return( false);                  // unknown number format
   }
   // swap number nibbles :
   for( int i = 0; i < NumberLength; i += 2){
      Text[ i]     = RxBuffer[ Index + 1];
      Text[ i + 1] = RxBuffer[ Index];
      Index += 2;
   }
   if( NumberLength & 0x01){
      // odd digits count
      Text[ NumberLength - 1] = RxBuffer[ Index++];
      Index++;                         // skip filler
   }
   Text[ NumberLength] = '\0';         // zero terminator
   From = TString( Text);              // return number
   Index += 2;                         // skip protocol
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   if( Value != 0){
      return( false);                  // unknown alphabet
   }
   Index += 12;                        // skip message time
   Index += 2;                         // skip timezone
   Value = GetHexNumber( &RxBuffer[ Index++]);Index++;
   int MessageLength = Value;
   // transform from 7-bit character stream :
   int  BitOffset = 0;                 // bit offset to output stream
   int  ByteIndex;                     // byte offset to output stream
   int  Shift;                         // bit shift
   word Twin;                          // character pair
   for( int i = 0; i < MessageLength; i++){
      ByteIndex  = BitOffset / 8;
      Shift      = BitOffset % 8;
      Twin       =  GetHexNumber( &RxBuffer[ Index + ByteIndex * 2]) |
                   (GetHexNumber( &RxBuffer[ Index + (ByteIndex + 1) * 2]) << 8);
      Twin     >>= Shift;
      Text[ i]   = (char)(Twin & 0x7F);
      BitOffset += 7;
   }
   Index += ((MessageLength * 7 - 1) / 8 + 1) * 2;
   GsmToAscii( Text, MessageLength);
   Message = TString( Text);
   if( PduLength != (Index - PduStart) / 2){
      return( false);                  // message length mismatch
   }
   // Delete message :
   sprintf( Text, Delete, NewMessage);
   if( !TxString( Text)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);                  // there is something wrong
   }
   return( true);
} // Receive

//------------------------------------------------------------------------------

//******************************************************************************
// Send string
//******************************************************************************

bool TSms::TxString( TString Data)
// Send <Data> string
{
   FPort->Flush();                            // flush previous data
   char *TxData  = Data.c_str();
   int  TxLength = strlen( TxData);
   if( FPort->Write( TxData, TxLength) != TxLength){
      return( false);
   }
   RwdTx( TxData, TxLength);
   return( true);
} // TxString

//******************************************************************************
// Receive string
//******************************************************************************

bool TSms::RxString()
// Receive <Data> string into <RxBuffer>
{
   int Length = FPort->Read( RxBuffer, sizeof( RxBuffer));
   if( Length == 0){
      return( false);            // no response - timeout
   }
   RxBuffer[ Length] = 0;        // terminate string
   RwdRx( RxBuffer, Length);
   return( true);
} // RxString

//******************************************************************************
// To GSM
//******************************************************************************

void TSms::AsciiToGsm( char *Text)
// Convert to GSM alphabet
{
   int Length = strlen( Text);
   for( int i = 0; i < Length; i++){
      if( Text[ i] == '@'){
         Text[ i] = '\0';      // @ == GSM 0
      }
   }
} // AsciiToGsm

//******************************************************************************
// To ASCII
//******************************************************************************

void TSms::GsmToAscii( char *Text, int Length)
// Convert GSM alphabet to text
{
   for( int i = 0; i < Length; i++){
      if( Text[ i] == '\0'){
         Text[ i] = '@';      // GSM 0 == @
      }
   }
   Text[ Length] = '\0';      // zero terminator
} // GsmToAscii

//******************************************************************************
// Hex conversion
//******************************************************************************

byte TSms::GetHexNumber( char *Text)
// Converts two characters into number
{

   byte Value;
   char ch    = Text[ 0];
   if( isdigit( ch)){
      Value = (ch - '0') << 4;
   } else {
      Value = (ch - 'A' + 10) << 4;
   }
   ch = Text[ 1];
   if( isdigit( ch)){
      Value |= (ch - '0');
   } else {
      Value |= (ch - 'A' + 10);
   }
   return( Value);
} // GetHexNumber
