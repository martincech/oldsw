//******************************************************************************
//
//   SMS.h        GSM SMS messaging
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __SMS_H__
   #define __SMS_H__

#include "../Unisys/uni.h"

#define SMS_MAX_OPERATOR   16         // max. delka nazvu operatora
#define GSM_MAX_NUMBER     16         // max. delka telefonniho cisla

// interni data :
extern byte RingIndex;

// navratove kody operace SmsRead :

typedef enum {
   SMS_EMPTY_MESSAGE = 0,
   SMS_FORMAT_ERROR  = 0xFE,
   SMS_READ_ERROR    = 0xFF
} TSmsReadError;

typedef enum {
   SMS_SIM_MEMORY,                     // pamet na SIM karte
   SMS_PHONE_MEMORY                    // pamet v telefonu
} TSmsMemory;

TYesNo GsmReset( void);
// Inicializace modemu

TYesNo GsmRegistered( void);
// Vraci YES, je-li modem registrovan v siti

TYesNo GsmOperator( char *Name);
// Vrati retezec s nazvem operatora,
// POZOR delka <Name> musi byt min SMS_MAX_OPERATOR+1

byte GsmSignalStrength( void);
// Vrati relativni silu signalu 0..31 nebo 99 nejde-li zjistit

TYesNo SmsMemory( byte Location);
// Nastaveni pameti SMS <Location> viz TSmsMemory

TYesNo SmsSendParameters( void);
// Nastaveni implicitnich parametru pro SMS send

TYesNo SmsSend( char *DstNumber, char *Text, byte MessageLength);
// Odeslani zpravy

byte SmsRead( byte Index, char *SrcNumber, char *Text);
// Precte prijatou zpravu z pozice <Index>, vrati jeji delku

void SmsDelete( byte Index);
// Smaze prijatou zpravu z pozice <Index>

TYesNo GsmIsRinging( void);
// Kontrola prichoziho hovoru

#define GsmWaitRinging() RingIndex = 0;
// Priprav prijem zvoneni

TYesNo GsmConnect( void);
// Zvedne sluchatko a navaze spojeni

void GsmDisconnect( void);
// Prepne do prikazoveho rezimu a zavesi


#endif
