//******************************************************************************
//
//   COM_util.c   Serial communication utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "com_util.h"
#include <string.h>

// Pomocna makra :

#define nibble2hex( n)   ((n) > 9 ? 'A' + (n) - 10 : '0' + (n))
#define char2dec( ch) ((ch) - '0')
#define char2hex( ch) ((ch) <= '9' ? (ch) - '0' : (ch) - 'A' + 10)

static word bbin2bcd( byte n)
{
word value;

   value  = 0;
   value |= n % 10;
   n /= 10;
   value |= (n % 10) << 4;
   n /= 10;
   value |= (n % 10) << 8;
   return( value);
} // bbin2bcd

//-----------------------------------------------------------------------------
// Tx Digit
//-----------------------------------------------------------------------------

void ComTxDigit( byte n)
// vyslani cislice 0..9
{
   ComTxChar( n + '0');
} // ComTxDigit

//-----------------------------------------------------------------------------
// Tx string
//-----------------------------------------------------------------------------

void ComTxString( char *Text)
// Vysle retezec
{
byte Length;
byte i;

   Length = strlen( Text);
   for( i = 0; i < Length; i++){
      ComTxChar( *Text);
      Text++;
   }
} // ComTxString

//-----------------------------------------------------------------------------
// Tx hexa
//-----------------------------------------------------------------------------

void ComTxHex( byte Number)
// Vysle hexadecimalni reprezentaci bytu
{
   ComTxChar( nibble2hex( hnibble( Number)));
   ComTxChar( nibble2hex( lnibble( Number)));
} // ComTxHex

//-----------------------------------------------------------------------------
// Tx dekadicky
//-----------------------------------------------------------------------------

void ComTxDec( byte Number)
// Vysle dekadickou reprezentaci bytu
{
word w;

   w = bbin2bcd( Number);
   ComTxDigit( w >> 8);                // stovky
   ComTxDigit( hnibble( w));           // desitky
   ComTxDigit( lnibble( w));           // jednotky
} // ComTxDec

//-----------------------------------------------------------------------------
// Rx dekadicky
//-----------------------------------------------------------------------------

byte ComRxDec( void)
// Cte cislo po prvni nenumericky znak
{
byte Value;
char ch;

   Value = 0;
   while(1){
      if( !ComRxChar( &ch)){
         return( 0);    // timeout
      }
      if( ch < '0' || ch > '9'){
         return( Value);
      }
      Value *= 10;
      Value += char2dec( ch);
   }
} // ComRxDec

//-----------------------------------------------------------------------------
// Rx hexadecimalne
//-----------------------------------------------------------------------------

byte ComRxHex( void)
// Cte dva znaky, jako hexa reprezentaci
{
byte Value;
char ch;

   if( !ComRxChar( &ch)){
      return( 0);                      // timeout
   }
   Value  = char2hex( ch) << 4;        // MSB
   if( !ComRxChar( &ch)){
      return( 0);                      // timeout
   }
   Value |= char2hex( ch);             // LSB
   return( Value);
} // ComRxHex

//-----------------------------------------------------------------------------
// Rx wait char
//-----------------------------------------------------------------------------

TYesNo ComWaitChar( char ch)
// Ceka na znak <ch>
{
char x;

   while(1){
      if( !ComRxChar( &x)){
         return( NO);              // timeout
      }
      if( ch == x){
         return( YES);             // prijat
      }
   }
} // ComWaitChar

//-----------------------------------------------------------------------------
// Rx skip chars
//-----------------------------------------------------------------------------

void ComSkipChars( byte Count)
// Preskoci pocet znaku <Count>
{
char x;

   do {
      if( !ComRxChar( &x)){
         return;                       // timeout
      }
   } while( --Count);
} // ComSkipChars

//-----------------------------------------------------------------------------
// Rx match
//-----------------------------------------------------------------------------

TYesNo ComRxMatch( char *Text, byte Timeout)
// Cte ocekavany retezec, vraci YES souhlasi-li
{
byte Length;
byte i;
char *p;
char ch;

   Length = strlen( Text);
   p      = Text;
   // cekani na prvni znak :
   if( !ComRxWait( Timeout)){
      return( NO);
   }
   // cteni zpravy :
   for( i = 0; i < Length; i++){
      if( !ComRxChar( &ch)){
         return( NO);    // timeout
      }
      if( ch != *p){
         return( NO);
      }
      p++;
   }
   return( YES);
} // ComRxMatch

//******************************************************************************
// Rx string
//******************************************************************************

TYesNo ComRxString( char *Text, byte Length)
// Cte retezec v uvozovkach
{
byte i;
char ch;

   // uvodni uvozovka :
   if( !ComWaitChar( '"')){
      return( NO);
   }
   for( i = 0; i < Length; i++){
      if( !ComRxChar( &ch)){
         return( NO);    // timeout
      }
      if( ch == '"'){
         Text[ i] = '\0';
         return( YES);
      }
      Text[ i] = ch;
   }
   Text[ Length] = '\0';
   return( YES);
} // ComRxString

//******************************************************************************
// Rx delimiter
//******************************************************************************

byte ComRxDelimiter( char *Text, byte Length, char Delimiter)
// Cte retezec az po znak <Delimiter>
{
byte i;
char ch;

   for( i = 0; i < Length; i++){
      if( !ComRxChar( &ch)){
         return( 0);    // timeout
      }
      if( ch == Delimiter){
         Text[ i] = '\0';
         return( i);
      }
      Text[ i] = ch;
   }
   Text[ Length] = '\0';
   return( Length);
} // ComRxDelimiter

