//******************************************************************************
//
//   Test.cpp     GSM test main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Test.h"
#include <stdio.h>

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Gsm       = new TGsm;
   Context   = CONTEXT_STOP;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt          = new TCrt( Memo);
   Logger       = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Gsm->Logger  = Logger;
} // FormCreate

//******************************************************************************
// Timer
//******************************************************************************

#define GSM_PERIOD 1

void __fastcall TMainForm::StatusTimerTimer(TObject *Sender)
{
static int GsmPeriod;

   if( Context == CONTEXT_STOP){
      GsmPeriod = 1;
      return;
   }
   if( --GsmPeriod){
      return;
   }
   GsmPeriod = GSM_PERIOD;
   switch( Context){
      case CONTEXT_UNCONNECTED :
         if( !Gsm->Reset()){
            LblRegistered->Caption = "Unconnected";
            return;
         }
         LblRegistered->Caption = "Reset";
         Context   = CONTEXT_RESET;
         break;
      case CONTEXT_RESET :
         if( !Gsm->IsRegistered()){
            Context = CONTEXT_UNCONNECTED;
            LblRegistered->Caption = "Unregistered";
            break;
         }
         LblRegistered->Caption = "Registered";
         break;

      default :
         break;
   }
}
// StatusTimerTick



void __fastcall TMainForm::BtnGoClick(TObject *Sender)
{
   if( BtnGo->Checked){
      LblRegistered->Caption = "Unconnected";
      Context = CONTEXT_UNCONNECTED;
   } else {
      LblRegistered->Caption = "Stop";
      Context = CONTEXT_STOP;
   }
} // BtnGoClick


