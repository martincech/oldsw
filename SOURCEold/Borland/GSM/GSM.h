//******************************************************************************
//
//   GSM.h        GSM network SMS service
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef GsmH
   #define GsmH

#ifndef ComUartH
   #include "../Serial/Uart/ComUart.h"
#endif

#ifndef LoggerH
   #include "../Serial/Logger.h"
#endif

#define SMS_MAX_REPLY      1024        // internal size of receive buffer

//******************************************************************************
// TSms
//******************************************************************************

class TGsmModem {
public :
   TGsmModem();
   // Constructor
   ~TGsmModem();
   // Destructor
   static bool Detect( TString Port, TString &Model);
   // Detect modem device <Model> at <Port>
   // Warning : disconnect all devices before detect, connected are invisible
   bool Connect( TString Port);
   // Connect via <Port>
   void Disconnect();
   // Disconnect port
   bool Reset();
   // Initialize modem device
   bool TestSim();
   // Initialize and test SIM card
   bool Check();
   // Check for modem device
   bool IsRegistered();
   // Returns status of network registration
   bool Operator( TString &Name);
   // Returns registered operator name
   bool SignalStrength( int &Strength);
   // Returns relative signal strength 10..31 or 99 if unknown
   bool SmsSend( TString To, char *Format, ...);
   // Send SMS to <To> number (printf format)
   bool SmsDataSend( char *Number, byte *Data, int Size);
   // Send SMS <Data> with <Size> to <Number>
   bool SmsReceive( int Address, TString &From, TString &Message);
   // Read received message on position <Address>. Message is empty
   // if <From> = ""
   bool SmsDelete( int Address);
   // Delete received message on <Address>
   int SmsCount();
   // Returns count of received messages or -1 if error

   __property int       BaudRate         = {read=FBaudRate, write=FBaudRate};
   __property int       CmdTimeout       = {read=FCmdTimeout, write=FCmdTimeout};
   __property int       SmsCenterTimeout = {read=FSmsCenterTimeout, write=FSmsCenterTimeout};
   __property int       SmsRxTimeout     = {read=FSmsRxTimeout, write=FSmsRxTimeout};
   __property int       SmsDeleteTimeout = {read=FSmsDeleteTimeout, write=FSmsDeleteTimeout};
   __property int       MemorySize       = {read=FMemorySize};

   __property TLogger  *Logger           = {read=FLogger, write=FLogger};
//------------------------------------------------------------------------------
protected :
   TComUart *FPort;                    // connection port
   TLogger  *FLogger;                  // raw data logger

   int      FBaudRate;                 // baud rate modem <> PC
   int      FCmdTimeout;               // modem reply time [ms]
   int      FSmsCenterTimeout;         // SMS Center reply timeout (SmsSend)
   int      FSmsRxTimeout;             // SMS receive timeout
   int      FSmsDeleteTimeout;         // SMS delete timeout
   int      FMemorySize;               // SMS memory size
   int      FError;                    // Error trace
   char     RxBuffer[ SMS_MAX_REPLY];  // receive buffer
   char     TxBuffer[ SMS_MAX_REPLY];  // receive buffer

   bool TxString( const char *Data, int Length = 0);
   // Send <Data> string. Default with zero terminator (else <Length> chars)
   bool RxString( int Size = 0);
   // Receive string of <Size> into <Buffer>
   void AsciiToGsm( char *Text);
   // Convert to GSM alphabet
   void GsmToAscii( char *Text, int Length);
   // Convert GSM alphabet to text
   byte GetHexNumber( char *Text);
   // Converts two characters into number
   byte GetDecNumber( char *Text);
   // Converts numeric characters into number
}; // TGsmModem

#endif
