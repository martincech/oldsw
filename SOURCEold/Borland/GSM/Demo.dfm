object MainForm: TMainForm
  Left = 275
  Top = 233
  Width = 810
  Height = 675
  Caption = 'GSM demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 688
    Top = 400
    Width = 29
    Height = 13
    Caption = 'From :'
  end
  object Label2: TLabel
    Left = 688
    Top = 288
    Width = 19
    Height = 13
    Caption = 'To :'
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Receive :'
  end
  object Label4: TLabel
    Left = 16
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Send :'
  end
  object Status: TLabel
    Left = 16
    Top = 576
    Width = 45
    Height = 13
    Caption = 'Status : ?'
  end
  object LblRegistered: TLabel
    Left = 688
    Top = 127
    Width = 65
    Height = 13
    Caption = 'Unconnected'
  end
  object LblOperator: TLabel
    Left = 696
    Top = 151
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblSignalStrength: TLabel
    Left = 696
    Top = 175
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label5: TLabel
    Left = 688
    Top = 496
    Width = 44
    Height = 13
    Caption = 'Address :'
  end
  object BtnConnect: TButton
    Left = 688
    Top = 40
    Width = 75
    Height = 17
    Caption = 'Connect'
    TabOrder = 0
    OnClick = BtnConnectClick
  end
  object BtnCheck: TButton
    Left = 688
    Top = 96
    Width = 75
    Height = 17
    Caption = 'Check'
    TabOrder = 1
    OnClick = BtnCheckClick
  end
  object Memo: TMemo
    Left = 16
    Top = 112
    Width = 641
    Height = 457
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    TabOrder = 2
  end
  object TxText: TEdit
    Left = 16
    Top = 24
    Width = 641
    Height = 21
    TabOrder = 3
    Text = 'Ahoj.'
  end
  object RxText: TEdit
    Left = 16
    Top = 72
    Width = 641
    Height = 21
    Enabled = False
    TabOrder = 4
    Text = '?'
  end
  object BtnSend: TButton
    Left = 688
    Top = 216
    Width = 81
    Height = 17
    Caption = 'Send'
    TabOrder = 5
    OnClick = BtnSendClick
  end
  object PortName: TEdit
    Left = 688
    Top = 64
    Width = 73
    Height = 21
    TabOrder = 6
    Text = 'COM9'
  end
  object RxFrom: TEdit
    Left = 688
    Top = 416
    Width = 89
    Height = 21
    Enabled = False
    TabOrder = 7
    Text = '?'
  end
  object TxTo: TEdit
    Left = 688
    Top = 304
    Width = 89
    Height = 21
    TabOrder = 8
    Text = '420'
  end
  object BtnAutodetect: TButton
    Left = 688
    Top = 16
    Width = 75
    Height = 17
    Caption = 'Autodetect'
    TabOrder = 9
    OnClick = BtnAutodetectClick
  end
  object BtnReceive: TButton
    Left = 688
    Top = 376
    Width = 75
    Height = 17
    Caption = 'Receive'
    TabOrder = 10
    OnClick = BtnReceiveClick
  end
  object BtnDelete: TButton
    Left = 688
    Top = 464
    Width = 75
    Height = 17
    Caption = 'Delete'
    TabOrder = 11
    OnClick = BtnDeleteClick
  end
  object DeleteAddress: TEdit
    Left = 688
    Top = 512
    Width = 41
    Height = 21
    TabOrder = 12
    Text = '9'
  end
  object BtnCount: TButton
    Left = 688
    Top = 552
    Width = 75
    Height = 17
    Caption = 'SMS count'
    TabOrder = 13
    OnClick = BtnCountClick
  end
  object BtnSimTest: TButton
    Left = 688
    Top = 576
    Width = 75
    Height = 17
    Caption = 'SIM Test'
    TabOrder = 14
    OnClick = BtnSimTestClick
  end
  object BtnSend1: TButton
    Left = 688
    Top = 240
    Width = 81
    Height = 17
    Caption = 'Send 0-127'
    TabOrder = 15
    OnClick = BtnSend1Click
  end
  object BtnSend2: TButton
    Left = 688
    Top = 264
    Width = 81
    Height = 17
    Caption = 'Send 128-256'
    TabOrder = 16
    OnClick = BtnSend2Click
  end
end
