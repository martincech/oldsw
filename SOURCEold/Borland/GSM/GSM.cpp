//******************************************************************************
//
//   GSM.cpp      GSM network module SAGEM HiLoNC
//   Version 1.0  (c) VymOs
//
//******************************************************************************


#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "GSM.h"

#pragma package(smart_init)

#define MAX_TEXT            256        // max raw message length
#define MAX_NUMBER           16        // max Number count
#define MAX_SMS             160        // max SMS length
#define DETECT_BAUD_RATE  38400        // modem detection baud rate
#define DETECT_TIMEOUT      300        // modem detection timeout
#define DEFAULT_BAUD_RATE 38400        // GSM modem rate

#define SMSC_TIMEOUT      15000        // SMS center reply timeout [ms] (pri 15sec to nekdy blblo na ES75 + Eurotel)
#define SMS_RX_TIMEOUT     1000        // SMS Rx timeout [ms]
#define SMS_DELETE_TIMEOUT 2500        // SMS delete timeout [ms]
#define ME_TIMEOUT         1000        // ME timeout [ms]
// Raw data logger :

#define RwdTx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::TX_DATA, (void *)Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                              \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_DATA, (void *)Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                         \
   if( FLogger){                                            \
      FLogger->Write( TLogger::RX_GARBAGE, (void *)Buffer, Length); \
   }
#define RwdReport( Text)                                    \
   if( FLogger){                                            \
      FLogger->Report( Text"\n");                           \
   }

// AT strings :

// Basic :
static const char At[]         = "AT\r";                 // AT
static const char Ok[]         = "\r\nOK\r\n";           // ok reply
static const char Error[]      = "\r\n+CMS ERROR: ";     // error reply (+number)

static const char EchoDefault[]= "ATE0\r";             // echo off
static const char EchoOk[]     = "ATE0\r\r\nOK\r\n";   // echo off ok reply

static const char ErrorMode[]  = "ATV1+CMEE=1\r";        // Extended error mode
static const char TextMode[]   = "AT+CMGF=1\r";          // SMS text mode
static const char ShortData[]  = "AT+CSDH=0\r";          // SMS receive short data flags
static const char SendParams[] = "AT+CSMP=17,167,0,0\r"; // SMS send default parameters

// Memory settings :
//static const char MemoryMode[]  = "AT+CPMS=\"MT\",\"MT\",\"MT\"\r";  // SMS data in the internal memory
static const char MemoryMode[]  = "AT+CPMS=\"SM\",\"SM\",\"SM\"\r";  // SMS data in the SIM card memory
static const char MemoryHeader[]= "\r\n+CPMS: ";         // Header of memory settings

// Operator & signal data :
static const char Registered[] = "AT+CREG?\r";           // Registration
static const char RegHeader[]  = "\r\n+CREG: ";          // Header of registration

static const char GsmOperator[]= "AT+COPS?\r";           // Get operator
static const char OperHeader[] = "\r\n+COPS: ";          // operator header

static const char Rssi[]       = "AT+CSQ\r";             // Get Received Signal Strength
static const char RssiHeader[] = "\r\n+CSQ: ";           // Signal strength header

// Detect :
static const char EchoOff[]     = "ATE0\r";              // echo off
static const char DeviceModel[] = "AT+GMM\r";            // get model

// SMS :
static const char PduSend[]    = "AT+CMGS=\"+%s\"\r";    // Send SMS + "+<number>"<CR>
static const char Prompt[]     = "\r\n> ";               // input prompt
static const char SendOk[]     = "\r\n+CMGS: ";          // Send ok (+message reference, pduack)

static const char PduRead[]    = "AT+CMGR=%d\r";         // Read SMS <index><CR>
static const char ReadHeader[] = "\r\n+CMGR: ";          // Header of read message
static const char EmptyRead[]  = "\r\n+CMS ERROR: 321";  // Teltonika empty read
static const char RecRead[]    = "\"REC READ\"";         // REC READ status
static const char RecUnread[]  = "\"REC UNREAD\"";       // REC UNREAD status

static const char Delete[]     = "AT+CMGD=%d\r";         // Delete SMS <index><CR>

static const char MemoryCount[]= "AT+CPMS?\r";           // Memory counters

#define CTRL_Z  '\x1A'

#define nibble2hex( n)   ((n) > 9 ? 'A' + (n) - 10 : '0' + (n))
#define char2dec( ch)    ((ch) - '0')

//******************************************************************************
// Constructor
//******************************************************************************

TGsmModem::TGsmModem()
// Constructor
{
   FPort                  = new TComUart;
   FLogger                = 0;
   FBaudRate              = DEFAULT_BAUD_RATE;
   FCmdTimeout            = ME_TIMEOUT;
   FSmsCenterTimeout      = SMSC_TIMEOUT;
   FSmsRxTimeout          = SMS_RX_TIMEOUT;
   FSmsDeleteTimeout      = SMS_DELETE_TIMEOUT;
   FMemorySize            = 0;
} //TGsm

//******************************************************************************
// Destructor
//******************************************************************************

TGsmModem::~TGsmModem()
// Destructor
{
   Disconnect();
} // ~TGsm

//******************************************************************************
// Detect
//******************************************************************************

bool TGsmModem::Detect( TString Port, TString &Model)
// Detect modem device <Model> at <Port>
{
   char       *DeviceName;
   TIdentifier Identifier;
   TComUart   *Uart = new TComUart;
   char        Buffer[ 255];
   // port parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate  = DETECT_BAUD_RATE;
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   if( !Uart->Locate( Port.c_str(), Identifier)){
      delete Uart;
      return( NO);
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( NO);
   }
   if( !Uart->SetParameters( Parameters)){
      delete Uart;
      return( NO);
   }
   Uart->SetRxWait( DETECT_TIMEOUT, 0);
   Uart->Flush();
   // Echo off :
   if( !Uart->Write( const_cast<char*>( EchoOff), strlen( EchoOff))){
      delete Uart;
      return( NO);
   }
   if( !Uart->Read(  Buffer, sizeof( Buffer))){
      delete Uart;
      return( NO);
   }
   // Device model :
   if( !Uart->Write( const_cast<char*>( DeviceModel), strlen( DeviceModel))){
      delete Uart;
      return( NO);
   }
   if( !Uart->Read(  Buffer, sizeof( Buffer))){
      delete Uart;
      return( NO);
   }
   // Parse device model string :
   DeviceName = Buffer;
   while( *DeviceName == '\r' || *DeviceName == '\n'){
      DeviceName++;                 // skip starting \r \n
   }
   char *p;
   p = strchr( DeviceName, '\r');   // stops  with \r
   if( !p){
      delete Uart;
      return( NO);
   }
   *p = '\0';                       // trim before \r
   Model = DeviceName;              // return model name
   delete Uart;
   return( YES);
} // Detect

//******************************************************************************
// Connect
//******************************************************************************

bool TGsmModem::Connect( TString Port)
// Connect via <Port>
{
   // disconnect old port :
   Disconnect();
   // open new port :
   TComUart *Uart = new TComUart;
   TIdentifier Identifier;
   if( !Uart->Locate( Port, Identifier)){
      delete Uart;
      return( false);
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   // set communication parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate  = FBaudRate;
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   Uart->SetRxWait( FCmdTimeout, 0);
   Uart->Flush();
   FPort = Uart;
   return( true);
} // Connect

//******************************************************************************
// Disconnect
//******************************************************************************

void TGsmModem::Disconnect()
// Disconnect port
{
   if( FPort){
      delete FPort;
      FPort = 0;
   }
} // Disconnect

//******************************************************************************
// Reset
//******************************************************************************

bool TGsmModem::Reset()
// Initialize modem device
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   // Echo off :
   if( !TxString( EchoDefault)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, EchoOk)){
      if( !strequ( RxBuffer, Ok)){
         return( false);
      }
   }
   // SMS mode - default is text :
   if( !TxString( TextMode)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // SMS receive - short data :
   if( !TxString( ShortData)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // SMS send - default parameters :
   if( !TxString( SendParams)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // Long error code :
   if( !TxString( ErrorMode)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // direct SMS data to internal memory :
   if( !TxString( MemoryMode)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   int HeaderSize = strlen( MemoryHeader);
   if( !strnequ( RxBuffer, MemoryHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <items> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   // Internal memory size :
   FMemorySize = GetDecNumber( p);
   if( !FMemorySize){
      return( false);
   }
   return( true);
} // Reset

//******************************************************************************
// Test SIM
//******************************************************************************

static const char SimId[] = "AT+CCID\r";       // SIM get card identification number
static const char SimOk[] = "\r\n+CCID: ";     // SIM code header

bool TGsmModem::TestSim()
// Initialize and test SIM card
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   // Echo off :
   if( !TxString( EchoDefault)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strequ( RxBuffer, EchoOk)){
      if( !strequ( RxBuffer, Ok)){
         return( false);
      }
   }
   // Long error code :
   if( !TxString( ErrorMode)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   // Get SIM card ID :
   if( !TxString( SimId)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   if( !strnequ( RxBuffer, SimOk, sizeof( SimOk) - 1)){
      return( false);
   }
   return( true);
} // TestSim

//******************************************************************************
// Check
//******************************************************************************

bool TGsmModem::Check()
// Check for modem device
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( At)){
      return( false);
   }
   if( !RxString( sizeof( Ok) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Ok)){
      return( false);
   }
   return( true);
} // Check

//******************************************************************************
// IsRegistered
//******************************************************************************

bool TGsmModem::IsRegistered()
// Returns status of network registration
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( Registered)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   // reply header :
   int HeaderSize = strlen( RegHeader);
   if( !strnequ( RxBuffer, RegHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <n> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   int Value = GetDecNumber( p);
   if( Value == 1 || Value == 5){
      return( true);                   // registered or roaming
   }
   return( false);
} // IsRegistered

//******************************************************************************
// Operator
//******************************************************************************

bool TGsmModem::Operator( TString &Name)
// Returns registered operator name
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( GsmOperator)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   int HeaderSize = strlen( OperHeader);
   if( !strnequ( RxBuffer, OperHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <mode> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   p = strchr( p, ',');                // skip <format> field
   if( !p){
      return( false);
   }
   p++;                                // skip ','
   Name = AnsiString( p);
   return( true);
} // Operator

//******************************************************************************
// Sila signalu
//******************************************************************************

bool TGsmModem::SignalStrength( int &Strength)
// Returns relative signal strength 10..31 or 99 if unknown
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( !TxString( Rssi)){
      return( false);
   }
   if( !RxString()){
      return( false);
   }
   int HeaderSize = strlen( RssiHeader);
   if( !strnequ( RxBuffer, RssiHeader, HeaderSize)){
      return( false);
   }
   char *p = &RxBuffer[ HeaderSize];
   Strength = GetDecNumber( p);
   return( true);
} // SignalStrength

//******************************************************************************
// Send
//******************************************************************************

bool TGsmModem::SmsSend( TString To, char *Format, ...)
// Send SMS to <To> number (printf format)
{
static char Text[ MAX_TEXT];           // text of message

   // expand text :
   va_list Arg;
   va_start( Arg, Format);
   vsprintf( Text, Format, Arg);
   int MessageLength = strlen( Text);
   if( MessageLength > MAX_SMS){
      return( false);
   }
   AsciiToGsm( Text);                  // convert into GSM aplhabet ! may contain 0 chars
   // SMS send :
   return( SmsDataSend( To.c_str(), Text, MessageLength));
} // SmsSend

//------------------------------------------------------------------------------
// Data Send
//------------------------------------------------------------------------------

bool TGsmModem::SmsDataSend( char *Number, byte *Data, int Size)
// Send SMS <Data> with <Size> to <Number>
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   // append terminator :
   Data[ Size] = CTRL_Z;    // Ctrl-Z terminator
   Size++;
   // SMS send :
   char Tmp[ 80];
   sprintf( Tmp, PduSend, Number); // destination number
   if( !TxString( Tmp)){
      return( false);
   }
   // wait for prompt :
   if( !RxString( sizeof( Prompt) - 1)){
      return( false);
   }
   if( !strequ( RxBuffer, Prompt)){
      return( false);
   }
   // append text :
   if( !TxString( Data, Size)){
      return( false);
   }
   // wait for reply header :
   FPort->SetRxWait( FSmsCenterTimeout, 0);
   if( !RxString( sizeof( SendOk) - 1)){
      FPort->SetRxWait( FCmdTimeout, 0);
      return( false);
   }
   FPort->SetRxWait( FCmdTimeout, 0);
   if( !strequ( RxBuffer, SendOk)){
      return( false);    // header error
   }
   // skip <message reference>,<pdu ack> :
   if( !RxString()){
      return( false);
   }
   return( true);
} // SmsDataSend

//******************************************************************************
// Receive
//******************************************************************************

bool TGsmModem::SmsReceive( int Address, TString &From, TString &Message)
// Read received message on position <Address>. Message is empty
// if <From> = ""
{
   FError = 0;
   if( !FPort || !FPort->IsOpen){
      FError = 1;
      return( false);
   }
   if( Address >= FMemorySize){
      FError = 2;
      IERROR;
   }
   Address++;                          // index started at 1
   char Tmp[ 80];
   sprintf( Tmp, PduRead, Address);    // read from memory address <i>
   if( !TxString( Tmp)){
      FError = 3;
      return( false);
   }
   FPort->SetRxWait( FSmsRxTimeout, 0);
   if( !RxString()){
      FError = 4;
      FPort->SetRxWait( FCmdTimeout, 0);
      return( false);
   }
   FPort->SetRxWait( FCmdTimeout, 0);
   if( strnequ( RxBuffer, EmptyRead, strlen( EmptyRead))){
      // empty message
      From = "";
      Message = "<*EMPTY*>";
      return( true);
   }
   if( strnequ( RxBuffer, Error, strlen( Error))){
      FError = 5;
      return( false);                  // there is something wrong
   }
   if( !strnequ( RxBuffer, ReadHeader, strlen( ReadHeader))){
      FError = 6;
      return( false);
   }
   char *p = RxBuffer;
   p      += strlen( ReadHeader);      // skip Reply header
   // check message type :
   if( !strnequ( p, RecRead,   strlen( RecRead)) &&
       !strnequ( p, RecUnread, strlen( RecUnread))){
      FError = 7;
      return (false);                  // flag other than received message
   }
   p = strchr( p, ',');                // skip comma after message type
   if_null( p){
      FError = 8;
      return( false);
   }
   p++;
   // source number field :
   if( *p != '\"'){                    // remove "
      FError = 9;
      return( false);
   }
   p++;
   char *Number;
   if( *p == '+'){
      Number = p + 1;                  // skip +
   } else {
      Number = p;
   }
   p = strchr( p, '\"');
   if_null( p){
      FError = 10;
      return( false);
   }
   *p = '\0';
   From = TString( Number);            // return source number
   p++;
   // skip up to \r\n :
   p = strchr( p, '\n');
   if_null( p){
      FError = 11;
      return( false);
   }
   p++;
   char *Text = p;                     // start of the message
   // end of message :
   p = strchr( p, '\r');
   if_null( p){
      FError = 12;
      return( false);
   }
   *p = 0;
   int MessageLength = p - Text;
   GsmToAscii( Text, MessageLength);
   Message = TString( Text);
   return( true);
} // SmsReceive

//******************************************************************************
// Delete
//******************************************************************************

bool TGsmModem::SmsDelete( int Address)
// Delete received message on <Address>
{
   if( !FPort || !FPort->IsOpen){
      return( false);
   }
   if( Address >= FMemorySize){
      IERROR;
   }
   Address++;                          // indexed from 1
   char Tmp[ 80];
   sprintf( Tmp, Delete, Address);
   if( !TxString( Tmp)){
      return( false);
   }
   FPort->SetRxWait( FSmsDeleteTimeout, 0);     // Modry modem MC39i funguje spravne az pri 2000ms, pri 1000ms jeste nekdy zablbne
   if( !RxString()){
      FPort->SetRxWait( FCmdTimeout, 0);
      return( false);
   }
   FPort->SetRxWait( FCmdTimeout, 0);
   if( !strequ( RxBuffer, Ok)){
      return( false);                  // there is something wrong
   }
   return( true);
} // SmsDelete

//******************************************************************************
// SMS count
//******************************************************************************

int TGsmModem::SmsCount()
// Returns count of received messages or -1 if error
{
   if( !TxString( MemoryCount)){
      return( -1);
   }
   if( !RxString()){
      return( -1);
   }
   int HeaderSize = strlen( MemoryHeader);
   if( !strnequ( RxBuffer, MemoryHeader, HeaderSize)){
      return( -1);
   }
   char *p = &RxBuffer[ HeaderSize];
   p = strchr( p, ',');                // skip <memory type> field
   if( !p){
      return( -1);
   }
   p++;                                // skip ','
   // SMS count :
   int Count = GetDecNumber( p);
   return( Count);
} // SmsCount

//------------------------------------------------------------------------------

//******************************************************************************
// Send string
//******************************************************************************

bool TGsmModem::TxString( const char *Data, int Length)
// Send <Data> string. Default with zero terminator (else <Length> chars)
{
   FPort->Flush();                            // flush previous data
   int   TxLength;
   if( Length == 0){
      TxLength = strlen( Data);
   } else {
      TxLength = Length;
   }
   if( FPort->Write( const_cast<char*>( Data), TxLength) != TxLength){
      return( false);
   }
   RwdTx( Data, TxLength);
   return( true);
} // TxString

//******************************************************************************
// Receive string
//******************************************************************************

bool TGsmModem::RxString( int Size)
// Receive <Data> string into <RxBuffer>
{
   if( !Size){
      Size = sizeof( RxBuffer);  // read maximum - up to timeout
   }
   int Length = FPort->Read( RxBuffer, Size);
   if( Length == 0){
      return( false);            // no response - timeout
   }
   RxBuffer[ Length] = 0;        // terminate string
   RwdRx( RxBuffer, Length);
   return( true);
} // RxString

//******************************************************************************
// To GSM
//******************************************************************************

void TGsmModem::AsciiToGsm( char *Text)
// Convert to GSM alphabet
{
   int Length = strlen( Text);
   for( int i = 0; i < Length; i++){
      if( Text[ i] == '@'){
         Text[ i] = '\0';      // @ == GSM 0
      }
   }
} // AsciiToGsm

//******************************************************************************
// To ASCII
//******************************************************************************

void TGsmModem::GsmToAscii( char *Text, int Length)
// Convert GSM alphabet to text
{
   for( int i = 0; i < Length; i++){
      if( Text[ i] == '\0'){
         Text[ i] = '@';      // GSM 0 == @
      }
   }
   Text[ Length] = '\0';      // zero terminator
} // GsmToAscii

//******************************************************************************
// Hex conversion
//******************************************************************************

byte TGsmModem::GetHexNumber( char *Text)
// Converts two characters into number
{

   byte Value = 0;
   char ch    = Text[ 0];
   if( isdigit( ch)){
      Value = (ch - '0') << 4;
   } else {
      Value = (ch - 'A' + 10) << 4;
   }
   ch = Text[ 1];
   if( isdigit( ch)){
      Value |= (ch - '0');
   } else {
      Value |= (ch - 'A' + 10);
   }
   return( Value);
} // GetHexNumber

//******************************************************************************
// Decadic conversion
//******************************************************************************

byte TGsmModem::GetDecNumber( char *Text)
// Converts numeric characters into number
{
byte Value;
char ch;

   Value = 0;
   while( 1){
      ch = *Text++;
      if( !ch){
         return( Value);        // end of string
      }
      if( ch < '0' || ch > '9'){
         return( Value);
      }
      Value *= 10;
      Value += char2dec( ch);
   }
} // GetDecNumber

