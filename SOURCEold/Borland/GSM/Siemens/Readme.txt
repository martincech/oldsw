registrace vcetne lokatoru :

AT+CREG=2        povolit lokator

OK
AT+CREG?         zjistit registraci
+CREG: 2,1,"071D","1FB5"

OK
AT+CREG=0        vratit (zakazat nevyzadana data)

OK
------------------------------------------------------
souhrnne indikace :

AT+CIND?
+CIND: 5,99,1,0,1,0,0,0,3

OK
------------------------------------------------------

sila signalu :

AT+CSQ
+CSQ: 27,99

OK

- prvni cislo 0..31 nebo 99 nelze zjistit
------------------------------------------------------

prace se seznamy / vlastni cislo stanice :

- pracovni seznam 

at+cpbs?
+CPBS: "SM",0,250

- velikost seznamu a delky polozek

at+cpbr=?
+CPBR: (1-250),20,14

OK

- zobrazeni obsahu (trva az 15s)

at+cpbr=1,250
OK
- (prazdny seznam)

- prepnuti na vlastni seznam

at+cpbs="ON"
OK

- kontrola prepnuti vlastniho seznamu

at+cpbs?
+CPBS: "ON",0,1

OK
- velikost seznamu a delky polozek

at+cpbr=?
+CPBR: (1),20,14

OK

- prazdny seznam

at+cpbr=1
OK

- navrat na puvodni seznam

at+cpbs="SM"
OK

at+cpbs?
+CPBS: "SM",0,250

OK
------------------------------------------------------

DATOVA komunikace :

MASTER                                  SLAVE

//---- zaveseni masteru :

ATD...\r
                                        \r\nRING\r\n  (po cca 5s)
                                        ATA\r
\r\nCONNECT 9600/RLP\r\n                \r\nCONNECT 9600/RLP\r\n

<data>                                  <data>

+++
                                        +++
\r\nOK\r\n
ATH\r
\r\nOK\r\n                              \r\nNO CARRIER\r\n

(cca 3s)

\r\n+CME ERROR: 100\r\n


//--- zaveseni slave :

ATD...\r
                                        \r\nRING\r\n  (po cca 5s)
                                        ATA\r
\r\nCONNECT 9600/RLP\r\n                \r\nCONNECT 9600/RLP\r\n

<data>                                  <data>

                                        +++
+++
                                        \r\nOK\r\n
                                        ATH\r
                                        \r\nOK\r\n
\r\nNO CARRIER\r\n

(cca 3s)

\r\n+CME ERROR: 100\r\n


\\--- timeout master :
ATD...\r
(po cca 1min}
\r\nNO CARRIER\r\n

------------------------------------------------------
SMS pamet :

pocet zprav/kapacita :

AT+CPMS?\r
+CPMS: "MT",0,50,"MT",0,50,"MT",0,50

OK


seznam zprav :

AT+CMGL=4
+CMGL: 1,0,,24
121235466546065006060

OK

AT+CMGL=4
+CMGL: 1,1,,24
121235466546065006060
+CMGL: 2,0,,26
12123546654606500606022

OK


<index>,<status>,[<alpha>],length
<status> 0 received unread
         1 received read

---------------------------------------------------------
Cteni SMS ASCII :

AT+CMGF=1      // SMS text mode

OK

AT+CMGR=1\r
\r\n+CMGR: "REC READ","+61448869330",,"06/08/01,18:41:06+40"\r\n
MESSAGE\r\n

\r\nOK\r\n

prazdna pozice :

\r\n+CMGR: 0,,0\r\n

<stat> stav zpravy (REC UNREAD, REC READ...)
<oa>   odesilatel
<alpha> odesilatel (je-li v seznamu)
<scts> service center timestamp
--- nepovinna data az po \r\n

vypnuti nepovinnych dat :

AT+CSDH=0

OK


-----------------------------------------------------------

Posilani SMS ASCII :

AT+CMGF=1      // SMS text mode

OK

AT+CMGS="604967296"\r   // cilove cislo
\r\n> ahoj<Ctrl-Z>      // \r\n>[mezera]|[text]0x1A
\r\n+CMGS: 48\r\n       // pridelene referencni cislo

\r\nOK\r\n

** mezinarodni cislo ma na zacatku +

nastaveni parametru :

AT+CSMP=17,167,0,0             // 167 je expirace