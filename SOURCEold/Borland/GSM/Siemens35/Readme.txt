    Testy TC35i
    -----------
    

------------------------------------------------------------    
// Ukladani SMS pro odeslani :

at+cmgw="+420604967296"
> Ahoj from TC35i               // [Ctrl-Z]
+CMGW: 1                        // index, kam ulozil

OK

// Odeslani ulozene SMS :

at+cmss=1                       // odesli zpravu na indexu
+CMSS: 106                      // SMS reference number

OK

// Odeslani lze zopakovat :

at+cmss=1
+CMSS: 107

OK

// Lze odeslat i na jine cislo :

at+cmss=1,"+420775929183"      // index a nove cislo
+CMSS: 108

OK

// prohlizeni pameti prijatych SMS :

at+cmgl="REC UNREAD"
OK

at+cmgl="REC READ"
+CMGL: 2,"REC READ","+420775929183",,"11/07/27,07:35:19+08"
Ahoj from TC35i

OK

// prohlizeni pameti ulozenych SMS :

at+cmgl="STO UNSENT"
OK

at+cmgl="STO SENT"
+CMGL: 1,"STO SENT","+420604967296",,
Ahoj from TC35i

OK

// vsechny zpravy :

at+cmgl="ALL"
+CMGL: 1,"STO SENT","+420604967296",,
Ahoj from TC35i
+CMGL: 2,"REC READ","+420775929183",,"11/07/27,07:35:19+08"
Ahoj from TC35i

OK

// Pamet prijatych i vyslanych SMS pouziva stejny index :

at+cmgl="ALL"
+CMGL: 1,"STO SENT","+420604967296",,
Ahoj from TC35i
+CMGL: 3,"STO UNSENT","+420604967296",,
Message 2
+CMGL: 4,"STO UNSENT","+420604967296",,
Message 3
+CMGL: 2,"REC READ","+420775929183",,"11/07/27,07:35:19+08"
Ahoj from TC35i

OK

// zpravy lze ukladat i do ME :
at+cpms="SM","ME"
+CPMS: 8,20,24,25,8,20

OK

at+cmgw="604967296"
> Stored ME
+CMGW: 1

OK

// nelze je ale rusit pomoci +cmgd, musi se prepnout
// at+cpms="ME", potom je lze zrusit pomoci +cmgd

// pri pokusu o zapis do zaplnene pameti hlasi chybu :

at+cmgw="604967296"
+CMS ERROR: 322

------------------------------------------------------------    
Asynchronni hlaseni pro SMS :

// pripustne hodnoty :
at+cnmi=?
+CNMI: (0-3),(0-3),(0,2,3),(0-2),(1)

OK

// aktualni nastaveni :
at+cnmi?
+CNMI: 0,0,0,0,1            // zadne asynchronni zpravy

// povol asynchronni hlaseni prijmu SMS :

at+cnmi=1,1                 // zahazuj zpravy v data modu, povol prijem SMS
OK

       // odeslana SMS do modemu :
+CMTI: "SM",7             // prijata SMS na index 7
at+cmgr=7                 // precti prijatou zpravu
+CMGR: "REC UNREAD","+420604967296",,"11/07/27,07:57:33+08"
No nazdar

OK

// povoleni asynchronni hlaseni o stavu :
at+cnmi=1,0,0,1
OK
at+cnmi?
+CNMI: 1,0,0,1,1

OK
at+cmgs="604967296"
> Ahou async
+CMGS: 109

OK

//... a nic

// pri vypnute cilove stanici zpravu +cmss resp +cmgs
// prijme, priradi ji referenci a opet nic. Jak se delaji
// dorucenky, je mi zahadou

------------------------------------------------------------    
Vypnuti napajeni :

at^smso
^SMSO: MS OFF

OK
^SHUTDOWN

void GsmPowerOff( void)
// Nastaveni implicitnich parametru pro SMS send
{
   TxString( "AT^SMSO");
   ComRxMatch( "\r\n^SMSO: MS OFF\r\n", 5);
   ComRxMatch( Ok, 5);
   // after about 10s switch GSM module power off
} // GsmPowerOff
