Poznamky ke GPRS

// prime pripojeni :
AT+CGDCONT=1,"PPP","internet","0.0.0.0",0,0\r  // set context 1
AT+CGQMIN=1,...        // minimum quality of service
AT+CGQREQ=1,...        // requested qualit of service
ATD*99***1#\r          // enter data state
CONNECT
<data>
+++
ATH\r                  // hangup


// TCP/IP socket :
at+upsd=0                // get profile
+UPSD: 0,0,0

+UPSD: 0,1,""

+UPSD: 0,2,""

+UPSD: 0,4,"0.0.0.0"

+UPSD: 0,5,"0.0.0.0"

+UPSD: 0,6,0

+UPSD: 0,7,"0.0.0.0"

+UPSD: 0,8,0

+UPSD: 0,9,0

+UPSD: 0,10,0

+UPSD: 0,11,0

+UPSD: 0,12,0

+UPSD: 0,13,0

+UPSD: 0,14,0

+UPSD: 0,15,0

+UPSD: 0,16,0

+UPSD: 0,17,0

+UPSD: 0,18,0

+UPSD: 0,19,0

OK
at+upsd=0,1,"internet"    // set APN
OK
at+upsda=0,1              // save profile
OK
at+upsda=0,3              // activate profile
OK
at+upsnd=0,0              // get own IP
+UPSND: 0,0,"10.23.20.196"         

OK
at+usocr=6                // create socket
+USOCR: 0

OK
at+usoco=0,"74.125.87.104",80        // connect www.google.cz port 80
OK
at+usowr=0,18                        // send binary
@GET / HTTP/1.0



+USOWR: 0,18                         // ok written

OK

+UUSORD: 0,840                       // usolicited read
at+usord=0,18                        // read first characters of message
+USORD: 0,0,""                       // great

OK
at+usord=0,840
+USORD: 0,0,""

OK
at+usord=0,8
+USORD: 0,0,""

OK
at+usord=0,8
+USORD: 0,0,""

OK
at+usowr=0,18                       // try send again
@GET / HTTP/1.0


ERROR                               // great
at+usord=0,18                       // try read
+USORD: 0,0,""

OK
at+usocl=0                          // close socket
OK
at+upsda=0,4                        // deactivate GPRS profile
OK
