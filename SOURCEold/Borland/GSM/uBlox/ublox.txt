---------------------------------------------------------------------------
Nastaveni baudove rychlosti :

at+ipr=9600 
at&W0       // zapis do profilu 0
at&Y0       // nastavit profil 0 jako default (neni treba, deje se automaticky)
AT&v        // zobraz profily
at+cpwroff  // vypnuti napajeni - jinak se nezapise

---------------------------------------------------------------------------
Autobaud :

at+ipr=0
funguje na 'at' i 'AT'

---------------------------------------------------------------------------

Power save mode :

at+upsv=0       // vypnuti

---------------------------------------------------------------------------
Vypnuti hw handshake :

at+ifc=0,0

---------------------------------------------------------------------------

Vypnuti napajeni :

at+cpwroff

---------------------------------------------------------------------------
Cislo verze :

at+gmm
LEON-G100

at+gmr
Urbe100_07.10.00

---------------------------------------------------------------------------
Cislo SIM :

at+ccid

---------------------------------------------------------------------------

Nastaveni ukladani SMS :

at+cpms="SM","SM","SM"
// implicitne MT
at+cpwroff                   // ulozi se do NVRAM

POZOR : pamet SIM karty zacina pozici 301
---------------------------------------------------------------------------

Ulozeni SMS expirace :

at+csmp=17,255,0,0  // nastaveni expirace
at+csas             // ulozeni do profilu
at+cpwroff          // vypnuti napajeni

// po restartu (nenacita se automaticky z profilu)
at+cres             // nacteni z profilu

---------------------------------------------------------------------------
DEMO : ulozeni konfigurace (klonovani modemu) :
---------------------------------------------------------------------------

at+ipr=<baud>
// zmena baudu terminalu
at+ifc=0,0
at+upsv=0
at+cpms="SM","SM","SM"
at&W0         // ulozeni do profilu
at&Y0         // nastaveni profilu jako default
at+cpwroff    // vypnout (ulozit do NVRAM)

---------------------------------------------------------------------------
POZOR 
---------------------------------------------------------------------------
- z inicializace vyhodit AT&FE0 jen ATE0
- uprava SMS read na +CMS ERROR : 321  // = empty
- uprava SMS read, SMS delete index += 300
- pri inicializaci volat SmsSendParameters (nastaveni expirace)
---------------------------------------------------------------------------
