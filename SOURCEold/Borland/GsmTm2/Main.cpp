//******************************************************************************
//
//   Main.cpp     GSM demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include <stdio.h>
#include "Com.h"
#include "Tm2.h"
#include "GsmRaw.h"   // GsmErrno only

#define FlushLog()   ComFlushRxDump();ComFlushTxDump()
#define TM2_BAUD      9600             // working baud

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Gsm = new TGsm;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt          = new TCrt( Memo);
   Logger       = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Gsm->Logger  = Logger;
} // FormCreate

//******************************************************************************
// Connect
//******************************************************************************

void __fastcall TMainForm::BtnConnectClick(TObject *Sender)
{
   if( !Gsm->Connect( PortName->Text)){
      FlushLog();
      Status->Caption = "Status : ERR";
      return;
   }
   if( !Gsm->Reset()){
      FlushLog();
      Status->Caption = "Status : reset ERR";
      return;
   }
   FlushLog();
   Status->Caption = "Status : OK";
} // BtnConnectClick

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
   Status->Caption = "Status : ?";
   Status->Refresh();
   if( TxText->Text.Length() == 0){
      Status->Caption = "Status : No text";
      return;
   }
   if( TxTo->Text.Length() == 0){
      Status->Caption = "Status : No destination number";
      return;
   }
   Status->Caption = "Status : ?";
   Status->Refresh();
   if( !Gsm->SmsSend( TxTo->Text, TxText->Text.c_str())){
      FlushLog();
      Status->Caption = "Status : ERR";
      return;
   }
   Status->Caption = "Status : OK";
   FlushLog();
} // BtnSendClick

//******************************************************************************
// Receive
//******************************************************************************

void __fastcall TMainForm::BtnReceiveClick(TObject *Sender)
{
TString From, Message;

   if( !Gsm->SmsReceive( From, Message)){
      FlushLog();
      Status->Caption = "Status : ERR";
      return;
   }
   RxFrom->Text = From;
   RxText->Text = Message;
   Status->Caption = "Status : OK";
   FlushLog();
} // BtnReceiveClick

//******************************************************************************
// Info
//******************************************************************************

void __fastcall TMainForm::BtnInfoClick(TObject *Sender)
{
   // sila signalu :
   int SignalStrength;
   Gsm->SignalStrength( SignalStrength);
   AnsiString SSNumber( SignalStrength);
   LblSignalStrength->Caption = SSNumber;

   // registrace :
   LblOperator->Caption   = "";
   if( Gsm->IsRegistered()){
      LblRegistered->Caption = "Registered";

      AnsiString Name;
      Gsm->Operator( Name);
      LblOperator->Caption = Name;
   } else {
      LblRegistered->Caption = "Unregistered";
   }
   FlushLog();
} // BtnInfoClick

//******************************************************************************
// Programming
//******************************************************************************

void __fastcall TMainForm::BtnProgrammingClick(TObject *Sender)
{
   if( !TmEchoOff()){
      Status->Caption = "ERR : Echo off";
      return;
   }
   if( !TmSetBaud( TM2_BAUD)){
      Status->Caption = "ERR : Set baud";
      return;
   }
   if( !TmSimMemory()){
      Status->Caption = "ERR : SIM memory";
      return;
   }
   if( !TmHandshakeOff()){
      Status->Caption =  "ERR : Handshake";
      return;
   }
   if( !TmNoPowerSave()){
      Status->Caption = "ERR : Power save";
      return;
   }
   if( !TmSaveProfile()){
      Status->Caption = "ERR : Save profile";
      return;
   }
   if( !TmSetProfile()){
      Status->Caption = "ERR : Set profile";
      return;
   }
   if( !TmPowerOff()){
      Status->Caption = "ERR : Power off";
      return;
   }
   Status->Caption = "Status : OK";
} // BtnProgrammingClick

//******************************************************************************
// Defaults
//******************************************************************************

void __fastcall TMainForm::BtnDefaultsClick(TObject *Sender)
{
   if( !TmEchoOff()){
      Status->Caption = "ERR : Echo off";
      return;
   }
   if( !TmFactoryDefault()){
      Status->Caption = "ERR : Factory default";
      return;
   }
   if( !TmPowerOff()){
      Status->Caption = "ERR : Power off";
      return;
   }
   Status->Caption = "Status : OK";
} // BtnDefaultsClick

//******************************************************************************
// Autobaud
//******************************************************************************

void __fastcall TMainForm::BtnAutobaudClick(TObject *Sender)
{
   if( !TmBaudDetection()){
      Status->Caption = "ERR : No modem";
      return;
   }
   switch( GsmErrno){
      case 0 :
         LblBaud->Caption = "115200";
         break;
      case 1 :
         LblBaud->Caption = "57600";
         break;
      case 2 :
         LblBaud->Caption = "38400";
         break;
      case 3 :
         LblBaud->Caption = "19200";
         break;
      case 4 :
         LblBaud->Caption = "9600";
         break;
   }
   Status->Caption = "Status : OK";
} // BtnAutobaudClick

