//*****************************************************************************
//
//    Com.c - RS232 communication services
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Com.h"
#include "..\Serial\Uart\ComUart.h"
#include "..\Serial\CrtLogger.h"
#include <time.h>

// monitorovani komunikace :

#define RxDump()    if( RxIndex) Logger->Write( TLogger::RX_DATA, RxBuffer, RxIndex);RxIndex=0
#define RxGarbage() if( RxIndex) Logger->Write( TLogger::RX_GARBAGE, RxBuffer, RxIndex);RxIndex=0
#define TxDump()    if( TxIndex) Logger->Write( TLogger::TX_DATA, TxBuffer, TxIndex);TxIndex=0

#define TxPut( ch)  TxBuffer[ TxIndex++] = (ch)
#define RxPut( ch)  RxBuffer[ RxIndex++] = (ch)

#define MAX_TX_BUFFER 65536
#define MAX_RX_BUFFER 65536

static byte TxBuffer[ MAX_TX_BUFFER];
static byte RxBuffer[ MAX_RX_BUFFER];
static int  TxIndex = 0;
static int  RxIndex = 0;


#define COM_RX_TIMEOUT 5

static TComUart *Uart = 0;
static int       RxChar;

//******************************************************************************
// Inicializace
//******************************************************************************

void ComInit( char *Port)
// Inicializace komunikace
{
   RxChar = -1;
   if( Uart){
      return;  // already initialized
   }
   // open new port :
   Uart = new TComUart;
   TIdentifier Identifier;
   if( !Uart->Locate( Port, Identifier)){
      delete Uart;
      return;
   }
   if( !Uart->Open( Identifier)){
      return;
   }
   // set communication parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate  = 9600;
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   Uart->SetRxWait( COM_RX_TIMEOUT, COM_RX_TIMEOUT);
   Uart->Flush();
} // ComInit

//******************************************************************************
// Variable init
//******************************************************************************

void ComVarInit( word Baud)
// Communication initialisation
{
   TUart::TParameters Parameters;
   if( Baud == COM_115200Bd){
      Parameters.BaudRate  = 115200;
   } else {
      Parameters.BaudRate  = Baud;
   }
   Parameters.DataBits  = 8;
   Parameters.StopBits  = 10;
   Parameters.Parity    = TUart::NO_PARITY;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   Uart->SetRxWait( COM_RX_TIMEOUT, COM_RX_TIMEOUT);
   Uart->Flush();
} // ComVarInit

//******************************************************************************
// Tx wait
//******************************************************************************

void ComTxWait( void)
// Cekani na vyslani znaku
{
   Sleep( 200);
} // ComTxWait

//******************************************************************************
// Tx
//******************************************************************************

void ComTxChar( byte Character)
// Vyslani znaku
{
   RxDump();
   Uart->Write( &Character, 1);
   // monitorovani :
   TxPut( Character);
   if( Character == '\r'){
      TxDump();
   }
} // ComTxChar

//******************************************************************************
// Rx
//******************************************************************************

TYesNo ComRxChar( byte *Character)
// Prijem znaku
{
byte ch;

   TxDump();
   if( RxChar >= 0){
      RxPut( RxChar);
      *Character = RxChar;
      RxChar = -1;
      return( YES);
   }
   if( Uart->Read( &ch, 1) == 0){
      RxDump();
      return( NO);
   }
   *Character = ch;
   RxPut( ch);
   return( YES);
} // ComRxChar

//******************************************************************************
// Cekani na znak
//******************************************************************************

TYesNo ComRxWait( byte Timeout)
// Ceka na znak az <Timeout> * 100 milisekund
{
byte ch;

   Uart->SetRxWait( Timeout * 100, 0);
   if( Uart->Read( &ch, 1) == 1){
      Uart->SetRxWait( COM_RX_TIMEOUT, COM_RX_TIMEOUT);
      RxChar = (unsigned)ch;
      return( YES);
   }
   Uart->SetRxWait( COM_RX_TIMEOUT, COM_RX_TIMEOUT);
   RxChar = -1;
   return( NO);
} // ComRxWait

//******************************************************************************
// Flush
//******************************************************************************

void ComFlushChars( void)
// Vynecha vsechny znaky az do meziznakoveho timeoutu
{
byte ch;

   RxDump();
   TxDump();
   while( 1){
      if( Uart->Read( &ch, 1) == 0){
         RxGarbage();
         return;
      }
      RxPut( ch);
   }
} // ComFlushChars

//******************************************************************************
// Flush dump
//******************************************************************************

void ComFlushRxDump()
{
   RxDump();
} // ComFlushRxDump

void ComFlushTxDump()
{
   TxDump();
} // ComFlushRxDump

