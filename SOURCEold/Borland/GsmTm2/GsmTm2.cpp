//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("GsmTm2.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Com.cpp");
USEUNIT("Com_util.cpp");
USEUNIT("GSM.cpp");
USEUNIT("GsmSms.cpp");
USEUNIT("GsmBase.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("Tm2.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
