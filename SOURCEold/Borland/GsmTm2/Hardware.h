//*****************************************************************************
//
//    Hardware.h  - Hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\Unisys\uni.h"

#define __xdata__
#define xdata

//-----------------------------------------------------------------------------
// Modul GSM
//-----------------------------------------------------------------------------

#define SMS_MAX_LENGTH      161        // max. delka prijate zpravy, vc. koncove nuly
#define GSM_MAX_NUMBER      16         // max. delka telefonniho cisla

// Podminena kompilace :

//#define GSM_COM2            1          // komunikace pres COM2
//#define COM_RX_STRING       1          // funkce pro GSM_OPERATOR
#define GSM_OPERATOR        1          // nazev operatora
#define GSM_SIGNAL_STRENGTH 1          // sila signalu
//#define GSM_DATA            1          // datova komunikace
#define GSM_SMS               1          // vysilani, prijem SMS
//#define GSM_SMS_USE_GSMCTL  1          // pri obsluze SMS se bude vyuzivat modul GsmCtl
#define GSM_SMS_MEMORY      1          // prekladat funkci SmsMemory
#define GSM_DEBUG             1          // Pouzije se debug mod

// Expirace SMS zpravy (jen pro SmsSendParameters) :

#define SmsExpirationMins( m)   ((m) / 5)               // min 0  max 720 (0..143)
#define SmsExpirationHours( h)  (((h) - 12) * 2 + 143)  // min 13 max 24  (144..167)
#define SmsExpirationDays( d)   ((d) + 166)             // min 2  max 30  (168..196)
#define SmsExpirationWeeks( w)  ((w) + 192)             // min 5  max 58  (197..255)
#define SmsExpirationMax()      255                     // maximalni expirace

#define SMS_EXPIRATION    SmsExpirationDays(4)          // pozadovana expirace

// Timeouty radice GSM :

#define GSM_ERROR_MODE_TIMEOUT 10      // Prodleva po nastaveni modu chyb [ms] (min. 15ms vcetne COM_RX_TIMEOUT). Hodnota 5 jeste funguje, 10 je tutovka.
#define GSM_SMS_SEND_TIMEOUT  20       // SMS center reply timeout [s]
#define GSM_CONNECT_TIMEOUT   30       // od zvednuti do navazani spojeni  [s]
#define GSM_CHECK_PERIOD      30       // cas mezi dvema kontrolami registrace [s]
#define GSM_POWERED_PERIOD    20       // cas na nabeh napajeni modemu [s]
#define GSM_HANGUP_TIMEOUT    30       // zaveseni pri neaktivnim spojeni [s]

//-----------------------------------------------------------------------------

#define SysDelay( ms) Sleep( ms)

#endif
