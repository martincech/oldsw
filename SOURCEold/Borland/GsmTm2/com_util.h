//******************************************************************************
//
//   COM_util.h   Serial communication utility
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __COM_util_H__
   #define __COM_util_H__

#include "Com.h"


void ComTxDigit( byte n);
// vyslani cislice 0..9

void ComTxString( const char *Text);
// Vysle retezec

void ComTxXString( const char *Text);
// Vysle retezec

void ComTxHex( byte Number);
// Vysle hexadecimalni reprezentaci bytu

void ComTxDec( byte Number);
// Vysle dekadickou reprezentaci bytu

void ComTxWord( word Number);
// Vysle dekadickou reprezentaci word 0..9999

byte ComRxDec( void);
// Cte cislo po prvni nenumericky znak

byte ComRxHex( void);
// Cte dva znaky, jako hexa reprezentaci

TYesNo ComWaitChar( char ch);
// Ceka na znak <ch>

void ComSkipChars( byte Count);
// Preskoci pocet znaku <Count>

TYesNo ComRxMatch( const char *Text, byte Timeout);
// Cte ocekavany retezec, vraci YES souhlasi-li

TYesNo ComRxString( char *Text, byte Length);
// Cte retezec v uvozovkach

byte ComRxDelimiter( char *Text, byte Length, const char Delimiter);
// Cte retezec az po znak <Delimiter>, vraci delku

#endif




