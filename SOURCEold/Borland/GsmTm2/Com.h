//*****************************************************************************
//
//    Com.h - RS232 communication services
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Com_H__
   #define __Com_H__

#include "../Unisys/uni.h"

#define COM_115200Bd  0                // highest baudrate code

void ComInit( char *Port);
// Inicializace komunikace

void ComVarInit( word Baud);
// Communication initialisation

void ComTxWait( void);
// Cekani na vyslani znaku

void ComTxChar( byte c);
// Vyslani znaku. Po timeoutu vraci NO

TYesNo ComRxChar( byte *Char);
// Vraci prijaty znak. Po timeoutu vraci NO

TYesNo ComRxWait( byte Timeout);
// Ceka na znak az <Timeout> * 100 milisekund

void ComFlushChars( void);
// Vynecha vsechny znaky az do meziznakoveho timeoutu



//---

void ComFlushRxDump();
void ComFlushTxDump();

#endif

