//******************************************************************************
//
//   Demo.h       GSM demo main form
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>

#include "GSM.h"
#include "..\Serial\CrtLogger.h"
#include "..\Crt\Crt.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TButton *BtnConnect;
        TMemo *Memo;
        TEdit *TxText;
        TEdit *RxText;
        TButton *BtnSend;
        TButton *BtnReceive;
        TEdit *PortName;
        TEdit *RxFrom;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *TxTo;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Status;
        TLabel *LblRegistered;
        TLabel *LblOperator;
        TLabel *LblSignalStrength;
        TButton *BtnInfo;
        TButton *BtnProgramming;
        TButton *BtnDefaults;
        TButton *BtnAutobaud;
        TLabel *Label5;
        TLabel *LblBaud;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnConnectClick(TObject *Sender);
        void __fastcall BtnSendClick(TObject *Sender);
        void __fastcall BtnReceiveClick(TObject *Sender);
        void __fastcall BtnInfoClick(TObject *Sender);
        void __fastcall BtnProgrammingClick(TObject *Sender);
        void __fastcall BtnDefaultsClick(TObject *Sender);
        void __fastcall BtnAutobaudClick(TObject *Sender);
public:	// User declarations
   TGsm *Gsm;
   TCrt       *Crt;
   TCrtLogger *Logger;
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif

