//******************************************************************************
//
//   GSM.h        GSM services
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __SMS_H__
   #define __SMS_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif

#define SMS_MAX_OPERATOR   16         // max. delka nazvu operatora

#ifdef GSM_DEBUG
   extern byte __xdata__ GsmErrno;
#endif

// navratove kody operace SmsRead :

typedef enum {
   SMS_EMPTY_MESSAGE = 0,
   SMS_FORMAT_ERROR  = 0xFE,
   SMS_READ_ERROR    = 0xFF
} TSmsReadStatus;

// umisteni pameti SMS pro SmsMemory :

typedef enum {
   SMS_SIM_MEMORY,                     // pamet na SIM karte
   SMS_PHONE_MEMORY                    // pamet v telefonu
} TSmsMemory;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

TYesNo GsmReset( void);
// Inicializace modemu

TYesNo GsmRegistered( void);
// Vraci YES, je-li modem registrovan v siti

TYesNo GsmOperator( char xdata *Name);
// Vrati retezec s nazvem operatora,
// POZOR delka <Name> musi byt min SMS_MAX_OPERATOR+1

byte GsmSignalStrength( void);
// Vrati relativni silu signalu 0..31 nebo 99 nejde-li zjistit


//-- SMS ----------------------------------------------------------------------

TYesNo SmsSend( char xdata *DstNumber, char *Text, byte MessageLength);
// Odeslani zpravy

TYesNo SmsSendOk( void);
// Testuje potvrzeni odeslani

byte SmsRead( byte Index, char xdata *SrcNumber, char xdata *Text);
// Precte prijatou zpravu z pozice <Index>, vrati jeji delku

void SmsDelete( byte Index);
// Smaze prijatou zpravu z pozice <Index>

TYesNo SmsMemory( byte Location);
// Nastaveni pameti SMS. <Location> viz TSmsMemory

TYesNo SmsSendParameters( void);
// Nastaveni implicitnich parametru pro SMS send

//--Datove sluzby -------------------------------------------------------------

TYesNo GsmIsRinging( void);
// Kontrola prichoziho hovoru

void GsmOffHook( void);
// Zvedne telefon

TYesNo GsmConnect( void);
// Kontrola navazani spojeni

void GsmDisconnect( void);
// Prepne do prikazoveho rezimu a zavesi

#endif
