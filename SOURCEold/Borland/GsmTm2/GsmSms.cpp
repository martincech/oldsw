//******************************************************************************
//
//   GsmSms.c     GSM SMS messaging (Teltonika TM2)
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "GsmRaw.h"
#include <string.h>

#ifdef GSM_SMS_USE_GSMCTL
   #include "..\inc\GsmCtl.h"   // GSM kontext
#endif

#ifndef GSM_COM2
   #include "Com.h"
#else
   #include "..\inc\Com2sw.h"

   #define ComFlushChars Com2FlushChars
   #define ComRxChar     Com2RxChar
   #define ComRxWait     Com2RxWait
   #define ComTxChar     Com2TxChar
#endif

#include "COM_util.h"

#ifdef GSM_DEBUG
  extern byte __xdata__ GsmErrno;
  #define TRACE( Value) GsmErrno = Value
#else
  #define TRACE( Value)
#endif

#define TO_REPLY          8                             // timeout odpovedi [100ms]
#define TO_SMS_READ       15                            // timeout zahlavi SMS read [100ms]
#define SMS_INDEX_BASE    301                           // SIM memory index

// Preddefinovane retezce :

static const char PduSend[]    = "AT+CMGS=\"+";          // Send SMS + "+<number."<CR>
static const char Prompt[]     = "\r\n> ";               // input prompt
static const char SendOk[]     = "\r\n+CMGS: ";          // Send ok (+message reference, pduack)

static const char PduRead[]    = "AT+CMGR=";             // Read SMS <index><CR>
static const char ReadHeader1[]= "\r\n+CM";              // Read message reply header
static const char ReadHeader2[]= "R: ";                  // Read message header continuation
static const char EmptyRead[]  = " ERROR: 321";          // Read empty message CMS ERROR 321
static const char ReceivedMsg[]= "\"REC";                // REC READ/REC UNREAD status

static const char Delete[]     = "AT+CMGD=";             // Delete SMS <index><CR>

#define CTRL_Z  '\x1A'

#define PDU_FIXED_LENGTH      8        // konstatni delka telegramu bez SMSC
#define TIMESTAMP_LENGTH     14        // delka sekce cas+zona

#define SMS_PROMPT_TIMEOUT   10        // Send prompt timeout      * 0.1s


#define TxString( s) ComFlushChars();ComTxString( s)    // pred vyslanim zrus Rx

//-----------------------------------------------------------------------------
// Send
//-----------------------------------------------------------------------------


TYesNo SmsSend( char xdata *DstNumber, char *Text, byte MessageLength)
// Odeslani zpravy
{
byte   i;
byte   ch;

#ifdef GSM_SMS_USE_GSMCTL
   if( !GsmReady()){
      TRACE(1);
      return( NO);                     // out of context
   }
#endif
   // AT command SMS send :
   TxString( PduSend);
   ComTxXString( DstNumber);           // destination number
   ComTxChar( '"');                    // close string with "
   ComTxChar( '\r');
   // wait for prompt :
   if( !ComRxMatch( Prompt, SMS_PROMPT_TIMEOUT)){
      TRACE(2);
      return( NO);
   }
   // send text :
   for( i = 0; i < MessageLength; i++){
      ch = Text[ i];
      // ASCII to GSM alphabet :
      if( ch == '@'){
         ch = '\0';
      }
      ComTxChar( ch);
   }
   ComTxChar( CTRL_Z);                 // message terminator
#ifdef GSM_SMS_USE_GSMCTL
   // Nacteni odpovedi necham na automat
   GsmCtl.Status  = GSM_SMS_SENDING;       // kontext radice
   GsmCtl.Counter = GSM_SMS_SEND_TIMEOUT;  // timeout potvrzeni
#else
   // Automat se nepouziva, pockam na odpoved
   if( !ComRxMatch( SendOk, 10 * GSM_SMS_SEND_TIMEOUT)){        // Timeout se do ComRxMatch() zadava v 0.1sec
      TRACE(3);
      return( NO);
   }
 #endif
   TRACE(0);
   return( YES);
} // SmsSend


//-----------------------------------------------------------------------------
// Send OK
//-----------------------------------------------------------------------------

#ifdef GSM_SMS_USE_GSMCTL

TYesNo SmsSendOk( void)
// Testuje potvrzeni odeslani
{
   if( !ComRxMatch( SendOk, 0)){
      return( NO);
   }
   return( YES);
} // SmsSendOk

#endif

//-----------------------------------------------------------------------------
// Receive
//-----------------------------------------------------------------------------

byte SmsRead( byte Index, char xdata *SrcNumber, char xdata *Text)
// Precte prijatou zpravu z pozice <Index>, vrati jeji delku
{
byte MessageLength;
byte i;
byte ch;
word idx;

#ifdef GSM_SMS_USE_GSMCTL
   if( !GsmReady()){
      TRACE(1);
      return( NO);                     // out of context
   }
#endif
   idx = (word)Index + SMS_INDEX_BASE;
   // AT prikaz Read :
   TxString( PduRead);
   ComTxWord( idx);                    // pozice
   ComTxChar( '\r');
   // read header prefix :
   if( !ComRxMatch( ReadHeader1, TO_SMS_READ)){
      TRACE(2);
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   // read next character :
   if( !ComRxChar( &ch)){
      TRACE(3);
      return( SMS_READ_ERROR);
   }
   if( ch == 'G'){
      // CMGR:
      if( !ComRxMatch( ReadHeader2, TO_REPLY)){
         TRACE(4);
         return( SMS_READ_ERROR);      // neplatna hlavicka odpovedi
      }
   } else if( ch == 'S'){
      // CMS ERROR
      if( ComRxMatch( EmptyRead, TO_REPLY)){
         TRACE(5);
         return( SMS_EMPTY_MESSAGE);
      }
      TRACE(6);
      return( SMS_READ_ERROR);
   } else {
      // unknown string
      TRACE(7);
      return( SMS_READ_ERROR);         // neplatna hlavicka odpovedi
   }
   // test for message type :
   if( !ComRxMatch( ReceivedMsg, TO_REPLY)){
      TRACE(9);
      return( SMS_READ_ERROR);
   }
   if( !ComWaitChar( ',')){
      TRACE(10);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // phone number :
   if( !ComRxString( SrcNumber, GSM_MAX_NUMBER)){
      TRACE(11);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   if( SrcNumber[ 0] == '+'){          // remove leading + sign
      ch = strlen( SrcNumber);         // sorry
      for( i = 0; i < ch; i++){
          SrcNumber[ i] = SrcNumber[ i + 1];
      }
   }
   // skip up to end of line :
   if( !ComWaitChar( '\n')){
      TRACE(12);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // text of the message :
   MessageLength = ComRxDelimiter( Text, SMS_MAX_LENGTH, '\r');
   if( !MessageLength){
      TRACE(13);
      return( SMS_READ_ERROR);         // invalid sequence
   }
   // transform from GSM alphabet :
   for( i = 0; i < MessageLength; i++){
      ch = Text[ i];
      if( ch == '\0'){
         Text[ i] = '@';
      }
   }
   TRACE(0);
   return( MessageLength);
} // SmsRead



//-----------------------------------------------------------------------------
// Delete
//-----------------------------------------------------------------------------


void SmsDelete( byte Index)
// Smaze prijatou zpravu z pozice <Index>
{
word idx;

   idx = (word)Index + SMS_INDEX_BASE;
   TxString( Delete);
   ComTxWord( idx);                    // pozice
   ComTxChar( '\r');
} // SmsDelete
