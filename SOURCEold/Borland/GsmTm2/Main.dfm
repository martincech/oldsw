object MainForm: TMainForm
  Left = 275
  Top = 233
  Width = 804
  Height = 627
  Caption = 'GSM demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 680
    Top = 496
    Width = 29
    Height = 13
    Caption = 'From :'
  end
  object Label2: TLabel
    Left = 680
    Top = 456
    Width = 19
    Height = 13
    Caption = 'To :'
  end
  object Label3: TLabel
    Left = 16
    Top = 56
    Width = 46
    Height = 13
    Caption = 'Receive :'
  end
  object Label4: TLabel
    Left = 16
    Top = 8
    Width = 31
    Height = 13
    Caption = 'Send :'
  end
  object Status: TLabel
    Left = 16
    Top = 576
    Width = 45
    Height = 13
    Caption = 'Status : ?'
  end
  object LblRegistered: TLabel
    Left = 688
    Top = 64
    Width = 65
    Height = 13
    Caption = 'Unconnected'
  end
  object LblOperator: TLabel
    Left = 696
    Top = 88
    Width = 6
    Height = 13
    Caption = '?'
  end
  object LblSignalStrength: TLabel
    Left = 696
    Top = 112
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label5: TLabel
    Left = 680
    Top = 248
    Width = 31
    Height = 13
    Caption = 'Baud :'
  end
  object LblBaud: TLabel
    Left = 720
    Top = 248
    Width = 6
    Height = 13
    Caption = '?'
  end
  object BtnConnect: TButton
    Left = 688
    Top = 16
    Width = 75
    Height = 17
    Caption = 'Connect'
    TabOrder = 0
    OnClick = BtnConnectClick
  end
  object Memo: TMemo
    Left = 16
    Top = 112
    Width = 641
    Height = 457
    Font.Charset = EASTEUROPE_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    TabOrder = 1
  end
  object TxText: TEdit
    Left = 16
    Top = 24
    Width = 641
    Height = 21
    TabOrder = 2
    Text = 'Ahoj.'
  end
  object RxText: TEdit
    Left = 16
    Top = 72
    Width = 641
    Height = 21
    Enabled = False
    TabOrder = 3
    Text = '?'
  end
  object BtnSend: TButton
    Left = 680
    Top = 408
    Width = 75
    Height = 17
    Caption = 'Send'
    TabOrder = 4
    OnClick = BtnSendClick
  end
  object BtnReceive: TButton
    Left = 680
    Top = 432
    Width = 75
    Height = 17
    Caption = 'Receive'
    TabOrder = 5
    OnClick = BtnReceiveClick
  end
  object PortName: TEdit
    Left = 680
    Top = 544
    Width = 73
    Height = 21
    Enabled = False
    TabOrder = 6
    Text = 'COM1'
  end
  object RxFrom: TEdit
    Left = 680
    Top = 512
    Width = 89
    Height = 21
    Enabled = False
    TabOrder = 7
    Text = '?'
  end
  object TxTo: TEdit
    Left = 680
    Top = 472
    Width = 89
    Height = 21
    TabOrder = 8
    Text = '420604967296'
  end
  object BtnInfo: TButton
    Left = 688
    Top = 40
    Width = 75
    Height = 17
    Caption = 'Info'
    TabOrder = 9
    OnClick = BtnInfoClick
  end
  object BtnProgramming: TButton
    Left = 688
    Top = 176
    Width = 75
    Height = 17
    Caption = 'Programming'
    TabOrder = 10
    OnClick = BtnProgrammingClick
  end
  object BtnDefaults: TButton
    Left = 688
    Top = 200
    Width = 75
    Height = 17
    Caption = 'Defaults'
    TabOrder = 11
    OnClick = BtnDefaultsClick
  end
  object BtnAutobaud: TButton
    Left = 688
    Top = 152
    Width = 75
    Height = 17
    Caption = 'Baud detection'
    TabOrder = 12
    OnClick = BtnAutobaudClick
  end
end
