//*****************************************************************************
//
//    Tm2.c          Teltonika Tm2 utility
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Tm2.h"
#include "Com.h"       // RS232
#include "Com_util.h"  // RS232 utility


#define BAUD_DEFAULT     COM_115200Bd                      // default baud rate
#define TO_REPLY         8                                 // standard reply time [100ms]
#define TO_MEMORY_SWITCH 50                                // memory switch timeout [100ms]
#define TO_SWITCH_OFF    50                                // power off timeout [100ms]
#define TxString( s)     ComFlushChars();ComTxString( s)   // flush before Tx


// AT strings :
static const char Ok[]          = "\r\nOK\r\n";             // ok reply

// debug mode :
#ifdef GSM_DEBUG
   extern byte __xdata__ GsmErrno;
   #define TRACE( n)  GsmErrno = n;
#else
   #define TRACE( n)
#endif

// Local functions :

static TYesNo SendCommand( const char *Cmd);
// Send command with OK reply

//---------------------------------------------------------------------------------------------
//  Baud detection
//---------------------------------------------------------------------------------------------

#define DETECT_TRIALS   3                                 // detection trials
#define BAUD_COUNT     (sizeof( BaudSet) / sizeof( word)) // baud set size

// baud rate set :
static word BaudSet[] = { COM_115200Bd, 57600, 38400, 19200, 9600};


TYesNo TmBaudDetection( void)
// Search & set baudrate
{
int  b;
int  i;
byte ch;

   // try all baudrates :
   for( b = 0; b < BAUD_COUNT; b++){
      ComVarInit( BaudSet[ b]);        // set baud rate
      TRACE( b);
      for( i = 0; i < DETECT_TRIALS; i++){
         ComFlushChars();
         ComTxString( "AT\r");
         if( !ComRxWait( TO_REPLY)){
            continue;
         }
         if( !ComRxChar( &ch)){
            continue;
         }
         if( ch == 'A'){
            // response with echo
            if( ComRxMatch( "T\r\r\nOK\r\n", TO_REPLY)){
               return( YES);
            }
         } else if( ch == '\r'){
            // response without echo
            if( ComRxMatch( "\nOK\r\n", TO_REPLY)){
               return( YES);
            }
         } // else invalid response
      }
   }
   ComVarInit( BAUD_DEFAULT);
   TRACE( 99);
   return( NO);
} // TmBaudDetection

//---------------------------------------------------------------------------------------------
//  Echo off
//---------------------------------------------------------------------------------------------

TYesNo TmEchoOff( void)
// Switch echo off
{
   TxString( "ATE0\r");
   if( !ComRxWait( TO_REPLY)){         // wait for reply
      TRACE( 1);
      return( NO);
   }
   if( !ComWaitChar( '\n')){           // before OK
      TRACE( 2);
      return( NO);
   }
   if( !ComWaitChar( '\n')){           // after OK
      TRACE( 3);
      return( NO);
   }
   TRACE( 0);
   return( YES);
} // TmEchoOff

//---------------------------------------------------------------------------------------------
//  Factory default
//---------------------------------------------------------------------------------------------

TYesNo TmFactoryDefault( void)
// Switch echo off
{
   // set default profile :
   return( SendCommand( "AT&Y2\r"));
} // TmFactoryDefault

//---------------------------------------------------------------------------------------------
//  Set Baud
//---------------------------------------------------------------------------------------------

TYesNo TmSetBaud( word Baud)
// Set modem baud rate
{
   ComFlushChars();
   switch( Baud){
      case COM_115200Bd :
         ComTxString( "AT+IPR=115200\r");
         break;
      case 57600 :
         ComTxString( "AT+IPR=57600\r");
         break;
      case 38400 :
         ComTxString( "AT+IPR=38400\r");
         break;
      case 19200 :
         ComTxString( "AT+IPR=19200\r");
         break;
      case 9600  :
         ComTxString( "AT+IPR=9600\r");
         break;
      default :
         TRACE( 99);
         return( NO);
   }
   ComTxWait();                        // wait for send
   ComVarInit( Baud);                  // set new baud rate
#ifndef __WIN32__
   if( !ComRxMatch( Ok, TO_REPLY)){
      TRACE( 1);
      return( NO);
   }
#else
   Sleep( 1000);
   ComFlushChars();
#endif
   TRACE( 0);
   return( YES);
} // TmSetBaud

//-----------------------------------------------------------------------------
// SIM Memory
//-----------------------------------------------------------------------------

TYesNo TmSimMemory( void)
// Set SMS memory to SIM card
{
   TxString( "AT+CPMS=\"SM\",\"SM\",\"SM\"\r");
   if( !ComRxMatch( "\r\n+CPMS: ", TO_MEMORY_SWITCH)){
      TRACE( 1);
      return( NO);
   }
   if( !ComWaitChar( '\r')){            // end of line
      TRACE( 2);
      return( NO);
   }
   if( !ComWaitChar( '\n')){
      TRACE( 3);
      return( NO);
   }
   if( !ComRxMatch( Ok, TO_REPLY)){    // OK
      TRACE( 4);
      return( NO);
   }
   TRACE( 0);
   return( YES);
} // TmSimMemory

//---------------------------------------------------------------------------------------------
//  Handshake off
//---------------------------------------------------------------------------------------------

TYesNo TmHandshakeOff( void)
// Set no handshake
{
   return( SendCommand( "AT+IFC=0,0\r"));
} // TmHandshakeOff

//---------------------------------------------------------------------------------------------
//  No power save
//---------------------------------------------------------------------------------------------

TYesNo TmNoPowerSave( void)
// Disable power save mode
{
   return( SendCommand( "AT+NPSV=0\r"));
} // TmHandshakeOff

//---------------------------------------------------------------------------------------------
//  Save profile
//---------------------------------------------------------------------------------------------

TYesNo TmSaveProfile( void)
// Save as profile 0
{
   return( SendCommand( "AT&W0\r"));
} // TmSaveProfile

//---------------------------------------------------------------------------------------------
//  Set profile
//---------------------------------------------------------------------------------------------

TYesNo TmSetProfile( void)
// Set active profile 0
{
   return( SendCommand( "AT&Y0\r"));
} // TmSetProfile

//---------------------------------------------------------------------------------------------
//  Send command
//---------------------------------------------------------------------------------------------

TYesNo TmPowerOff( void)
// Switch power off
{
   TxString( "AT+CPWROFF\r");
   if( !ComRxMatch( Ok, TO_SWITCH_OFF)){
      TRACE( 1);
      return( NO);
   }
   TRACE( 0);
   return( YES);
} // TmPowerOff

//---------------------------------------------------------------------------------------------
//  Send command
//---------------------------------------------------------------------------------------------

static TYesNo SendCommand( const char *Cmd)
// Send command with OK reply
{
   TxString( Cmd);
   if( !ComRxMatch( Ok, TO_REPLY)){
      TRACE( 1);
      return( NO);
   }
   TRACE( 0);
   return( YES);
} // SendCommand
