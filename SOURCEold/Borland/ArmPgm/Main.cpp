//******************************************************************************
//
//   Main.cpp     Bat1 programmer main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Main.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";
#define StatusErr()  Status() + "ERROR";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Lpc = new TLpcFlash;
   Hex = new THexFile( 512 * 1024);
   Lpc->BaudRate         = 57600;
   Lpc->CrystalFrequency = 18432000;
   strcpy( Lpc->DeviceName, "VEIT BAT1 Poultry Scale");
   Lpc->ViaUsb = true;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Lpc->ProgressFunction       = this->Progress;
   Lpc->EnableFlashComparation = true;
} // FormCreate

//******************************************************************************
// Progress
//******************************************************************************

void TMainForm::Progress( int Percent)
// Update progress bar
{
   ProgressBar->Position = Percent / 10;
   AnsiString String;
   String.printf( "%d %%", Percent / 10);
   LblPercent->Caption = String;
   Application->ProcessMessages();
   Screen->Cursor      = crHourGlass;// wait cursor
} // Progress

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   ProgressBar->Visible = true;
   LblPercent->Visible  = true;
   LblPhase->Visible    = true;
   Progress( 0);
   this->Enabled        = false;      // disable main window
   Application->ProcessMessages();
   Screen->Cursor       = crHourGlass;// wait cursor
} // StartCom

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   Progress( 0);
   ProgressBar->Visible  = false;
   LblPercent->Visible   = false;
   LblPhase->Visible     = false;
   this->Enabled         = true;       // enable main window
   Screen->Cursor        = crDefault;  // normal cursor
} // StopComm

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   if( !Hex->Load( FileOpenDialog->FileName.c_str())){
      StatusErr();
      return;
   }
   BtnWrite->Enabled = true;
   LblFileName->Caption = FileOpenDialog->FileName;
   LblCodeSize->Caption = AnsiString( Hex->CodeSize);
   StatusOk();
} // BtnLoadClick

//******************************************************************************
// Write
//******************************************************************************

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   if( !Lpc->GetDeviceInfo()){
      StatusErr();
      return;
   }
   LblCpu->Caption     = Lpc->Device;
   LblVersion->Caption = Lpc->Version;
   LblPhase->Caption = "Write :";
   StartComm();
   if( !Lpc->WriteFlash( 0, Hex->Code, Hex->CodeSize)){
      StopComm();
      StatusErr();
      return;
   }
   LblPhase->Caption = "Verify :";
   if( !Lpc->CompareFlash( 0, Hex->Code, Hex->CodeSize)){
      StopComm();
      StatusErr();
      return;
   }
   if( !Lpc->Reset()){
      StopComm();
      StatusErr();
      return;
   }
   StopComm();
   StatusOk();
} // BtnWriteClick

