//******************************************************************************
//
//   Main.h        Main header
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Library/Crt/Crt.h"
#include "../Library/Serial/CrtLogger.h"
#include "LpcFlash.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnInfo;
        TButton *BtnWrite;
        TEdit *EdtAddress;
        TLabel *Label1;
        TEdit *EdtSize;
        TLabel *Label2;
        TButton *BtnEcho;
        TButton *BtnRead;
        TButton *BtnCompare;
        TProgressBar *ProgressBar;
        TLabel *LblPercent;
        TCheckBox *CbEnableLogger;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnInfoClick(TObject *Sender);
        void __fastcall BtnWriteClick(TObject *Sender);
        void __fastcall BtnEchoClick(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
        void __fastcall BtnCompareClick(TObject *Sender);
        void __fastcall CbEnableLoggerClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt       *Crt;
   TCrtLogger *Logger;
   TLpcFlash  *Lpc;
   __fastcall TMainForm(TComponent* Owner);
   void Progress( int Percent);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
