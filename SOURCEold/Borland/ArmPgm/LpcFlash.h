//******************************************************************************
//
//   LpcFlash.h     LPC programming commands
//   Version 1.0    (c) VymOs
//
//******************************************************************************

#ifndef LpcFlashH
   #define LpcFlashH

#ifndef LpcIspH
   #include "LpcIsp.h"
#endif

#define LPC_VERSION_SIZE  8     // boot version text size
#define LPC_DEVICE_SIZE   16    // device type text size

//******************************************************************************
// TLpcFlash
//******************************************************************************

class TLpcFlash : public TLpcIsp {
public :
   TLpcFlash();
   // Constructor
   ~TLpcFlash();
   // Destructor

   bool GetDeviceInfo();
   // Get CPU informations
   bool WriteFlash( int Address, void *Data, int Size);
   // Write <Data> to flash memory
   bool ReadFlash( int Address, void *Data, int Size);
   // Read <Data> from memory <Address>
   bool CompareFlash( int Address, void *Data, int Size);
   // Compare flash memory with <Data>
   bool Reset();
   // Restart device

// properties :
   char Version[ LPC_VERSION_SIZE];
   char Device[ LPC_DEVICE_SIZE];
   bool EnableFlashComparation;                       // compare flash segment after burn
   void ( __closure *ProgressFunction)( int Percent); // read/write progress [0.1%]

//------------------------------------------------------------------------------
protected :
   int FPartId;                        // LPC model number
   int FBootVersion;                   // LPC boot version
   int FTotalSize;                     // total read/write size
   int FDoneSize;                      // read/write size alrady done

   int GetFlashSize();
   // Returns flash size
   int GetLastSector();
   // Returns last sector number
   int GetRamSize();
   // Return RAM size
   int GetSectorSize( int Sector);
   // Returns segment size
   int GetCopySize( int Size);
   // Returns maximal copy size

   bool WriteFlashSector( int Sector, int Address, byte *Data, int Size);
   // Write <Data> to <Sector>
}; // TLpcFlash

#endif
