//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("ArmPgm.res");
USEFORM("Main.cpp", MainForm);
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USEUNIT("LpcIsp.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("LpcFlash.cpp");
USEUNIT("HexFile.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "ARM Programmer";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
