//******************************************************************************
//
//   HexFile.cpp   Hex file decoder
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "HexFile.h"

#pragma package(smart_init)

typedef enum {
   HEX_DATA,            // 0 - data record
   HEX_EOF,             // 1 - end of file
   HEX_SEGMENT,         // 2 - extended segment address record
   HEX_START_SEGMENT,   // 3 - start segment address record
   HEX_ADDRESS,         // 4 - extended linear address record
   HEX_START_ADDRESS    // 5 - start linear address record
} THexRecordType;

//******************************************************************************
// Constructor
//******************************************************************************

THexFile::THexFile( int MaxCodeSize)
// Constructor
{
   Code             = new byte[ MaxCodeSize];
   CodeStartAddress = 0;
   CodeSize         = 0;
   FMaxCodeSize     = MaxCodeSize;
} // THexFile

//******************************************************************************
// Destructor
//******************************************************************************

THexFile::~THexFile()
// Destructor
{
   if( Code){
      delete[] Code;
   }
} // THexFile

//******************************************************************************
// Load
//******************************************************************************

bool THexFile::Load( char *FileName)
// Load HEX file
{
   if( !FileExists( FileName)){
      return( false);
   }
   FILE *f;
   f = fopen( FileName, "r");
   if( !f){
      return( false);
   }
   memset( Code, 0xFF, FMaxCodeSize);          // fill with defaults
   CodeStartAddress = 0;
   // row data :
   byte     Reclen;
   word     LoadOffset;
   byte     RecordType;
   byte     Data[ 256];
   // code data :
   unsigned Address    = 0;
   unsigned MaxAddress = 0;
   unsigned MinAddress = FMaxCodeSize;
   unsigned EndAddress;
   bool Fail = false;
   while( 1){
      if( !ReadLine( f, Reclen, LoadOffset, RecordType, Data)){
         Fail = true;  // premature end of file
         break;
      }
      switch( RecordType){
         case HEX_DATA :
            if( Reclen == 0){
               Fail = true;      // zero length record
               goto terminate;
            }
            memcpy( &Code[ Address + LoadOffset], Data, Reclen);
            // minimum address :
            if( Address + LoadOffset < MinAddress){
               MinAddress = Address + LoadOffset;
            }
            // maximum address :
            EndAddress = Address + LoadOffset + (Reclen - 1);
            if( EndAddress > MaxAddress){
               MaxAddress = EndAddress;
            }
            break;

         case HEX_EOF :
            goto terminate;

         case HEX_SEGMENT :
            Address = GetInteger( Data, Reclen) << 4; 
            break;

         case HEX_START_SEGMENT :
            Address = GetInteger( Data, Reclen) << 8;
            break;

         default :
            // unknown record
            Fail = true;
            goto terminate;
      }
   }
terminate :
   fclose( f);
   CodeMinAddress = MinAddress;
   CodeSize       = MaxAddress + 1;
   return( !Fail);
} // LoadHex

//------------------------------------------------------------------------------

//******************************************************************************
// Read line
//******************************************************************************


bool THexFile::ReadLine( FILE *f, byte &Reclen, word &LoadOffset, byte &RecordType, byte *Data)
// read HEX file row
{
   if( !fgets( Line, HEX_MAX_LINE, f)){
      return( false);                // EOF
   }
   LineLength = strlen( Line);
   if( !LineLength){
      return( false);                // zero length
   }
   LineLength--;                     // line termination
   if( Line[ LineLength] != '\n'){
      return( false);                // line too long
   }
   Line[ LineLength] = '\0';         // remove line terminator
   // dekodovani radku :
   LineIndex    = 0;
   LineChecksum = 0;
   if( Line[ LineIndex++] != ':'){
      return( false);                // invalid line
   }
   if( !ReadByte( Reclen)){
      return( false);
   }
   if( !ReadWord( LoadOffset)){
      return( false);
   }
   if( !ReadByte( RecordType)){
      return( false);
   }
   for( int i = 0; i < Reclen; i++){
      if( !ReadByte( Data[ i])){
         return( false);
      }
   }
   byte Checksum;
   if( !ReadByte( Checksum)){
      return( false);
   }
   if( LineChecksum != 0){
      return( false);
   }
   return( true);
} // ReadLine

//******************************************************************************
// Read nibble
//******************************************************************************

byte THexFile::ReadNibble()
// hex character conversion, returns 0xFF on error
{
   if( LineIndex >= LineLength){
      return( 0xFF);
   }
   char c = Line[ LineIndex++];
   if( c >= '0' && c <= '9'){
      return( c - '0');
   }
   if( c >= 'A' && c <= 'F'){
      return( c - 'A' + 10);
   }
   return( 0xFF);
} // ReadNibble

//******************************************************************************
// Read byte
//******************************************************************************

bool THexFile::ReadByte( byte &Value)
// read byte from line
{
   Value = 0;
   byte x = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= x << 4;
   x = ReadNibble();
   if( x == 0xFF){
      return( false);
   }
   Value |= x;
   LineChecksum += Value;
   return( true);
} // ReadByte

//******************************************************************************
// Read word
//******************************************************************************

bool THexFile::ReadWord( word &Value)
// read word from line
{
   byte HiByte;
   byte LoByte;
   if( !ReadByte( HiByte)){
      return( false);
   }
   if( !ReadByte( LoByte)){
      return( false);
   }
   Value = ((word)HiByte << 8) | LoByte;
   return( true);
} // ReadWord

//******************************************************************************
// Get integer
//******************************************************************************

unsigned THexFile::GetInteger( byte *Data, int Size)
// compose integer from <Data> array
{
   unsigned Value = 0;
   for( int i = 0; i < Size; i++){
      Value <<= 8;
      Value  |= Data[ i];
   }
   return( Value);
} // GetInteger

