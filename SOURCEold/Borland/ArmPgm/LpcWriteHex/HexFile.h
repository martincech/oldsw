//******************************************************************************
//
//   HexFile.h     Hex file decoder
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#ifndef HexFileH
#define HexFileH

#include <stdio.h>
#include "../Library/Unisys/Uni.h"

//******************************************************************************
// THexFile
//******************************************************************************

class THexFile {
public :
   THexFile( int MaxCodeSize);
   // Constructor
   ~THexFile();
   // Destructor
   bool Load( char *FileName);
   // Load HEX file

   unsigned  CodeMinAddress;           // min. address of code
   unsigned  CodeStartAddress;         // restart address
   int       CodeSize;                 // code size
   byte     *Code;                     // code data

//------------------------------------------------------------------------------
protected :
   #define HEX_MAX_LINE 600            // max. HEX file line length
   char Line[ HEX_MAX_LINE + 1];       // line buffer
   int  LineLength;                    // line length
   int  LineIndex;                     // line index
   byte LineChecksum;                  // line checksum
   int  FMaxCodeSize;                  // prealocated code size

   bool ReadLine( FILE *f, byte &Reclen, word &LoadOffset, byte &RecordType, byte *Data);
   // read HEX file row
   byte ReadNibble();
   // hex character conversion, returns 0xFF on error
   bool ReadByte( byte &Value);
   // read byte from line
   bool ReadWord( word &Value);
   // read word from line
   unsigned GetInteger( byte *Data, int Size);
   // compose integer from <Data> array
}; // THexFile
//------------------------------------------------------------------------------

#endif
