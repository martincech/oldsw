//******************************************************************************
//
//   LpcIsp.cpp   LPC ARM ISP commands
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "LpcIsp.h"
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "../Serial/Usb/UsbUart.h"
#include "../Serial/UART/ComUart.h"

#pragma package(smart_init)

// Raw data logger :

#define RwdTx( Buffer, Length)                             \
   if( Logger){                                            \
      Logger->Write( TLogger::TX_DATA, Buffer, Length);    \
   }
#define RwdRx( Buffer, Length)                             \
   if( Logger){                                            \
      Logger->Write( TLogger::RX_DATA, Buffer, Length);    \
   }
#define RwdGarbage( Buffer, Length)                        \
   if( Logger){                                            \
      Logger->Write( TLogger::RX_GARBAGE, Buffer, Length); \
   }
#define RwdReport( Text)                                   \
   if( Logger){                                            \
      Logger->Report( Text"\n");                           \
   }

// constants :
#define RX_COMMAND_TIMEOUT        100
#define RX_READ_TIMEOUT           1000
#define RX_ERASE_TIMEOUT          5000
#define RX_BLANK_TIMEOUT          5000
#define RX_INTERCHARACTER_TIMEOUT 10
#define OK_REPLY                  "OK\r\n"

// UU encoded transfer
#define UU_LINES          20        // lines for CRC
#define UU_LINE_SIZE      45        // bytes per line
#define UU_LINE_CHARS     61        // characters per line

//******************************************************************************
// Constructor
//******************************************************************************

TLpcIsp::TLpcIsp()
// Constructor
{
   Port             = 0;
   Logger           = 0;
   strcpy( DeviceName, "?");
   BaudRate         = 9600;
   StopBits         = 1;
   CrystalFrequency = 18432000;
   ErrorCode        = CMD_SUCCESS;
   FEnableEcho      = true;
   FRxTimeout       = RX_COMMAND_TIMEOUT;
} // TLpcIsp

//******************************************************************************
// Destructor
//******************************************************************************

TLpcIsp::~TLpcIsp()
// Destructor
{
   if( Port){
      delete Port;
   }
} // ~TLpcIsp

//******************************************************************************
// Check parameters
//******************************************************************************

bool TLpcIsp::CheckParameters()
// Check BaudRate & CrystalFrequency
{
   switch( CrystalFrequency){
      case 10000000 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 38400 :
               return( true);
            default :
               return( false);
         }
      case 11059200 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 57600 :
               return( true);
            default :
               return( false);
         }
      case 12288000 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 38400 :
               return( true);
            default :
               return( false);
         }
      case 14745600 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 38400 :
            case 57600 :
            case 115200 :
               return( true);
            default :
               return( false);
         }
      case 15360000 :
         switch( BaudRate){
            case 9600 :
               return( true);
            default :
               return( false);
         }
      case 18432000 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 57600 :
               return( true);
            default :
               return( false);
         }
      case 19660800 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 38400 :
               return( true);
            default :
               return( false);
         }
      case 24576000 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 38400 :
               return( true);
            default :
               return( false);
         }
      case 25000000 :
         switch( BaudRate){
            case 9600 :
            case 19200 :
            case 38400 :
               return( true);
            default :
               return( false);
         }
      default :
         return( false);
   }
} // CheckParameters

//******************************************************************************
// Enter programming
//******************************************************************************

bool TLpcIsp::EnterProgramming()
// Enter programming mode
{
   if( !CheckConnect()){
      return( false);
   }
   Port->DTR = true;                   // reset
   Port->RTS = true;                   // P0.14
   Sleep( 100);
   Port->DTR = false;                  // release reset
   Sleep( 100);
   Port->RTS = false;                  // release P0.14
   return( true);
} // EnterProgramming

//******************************************************************************
// Reset target
//******************************************************************************

bool TLpcIsp::ResetTarget()
// Reset & run program
{
   if( !CheckConnect()){
      return( false);
   }
   Port->DTR = true;                   // reset
   Port->RTS = false;                  // P0.14
   Sleep( 100);
   Port->DTR = false;                  // release reset
   return( true);
} // ResetTarget

//******************************************************************************
// Synchronize
//******************************************************************************

#define SYNCHRONIZED_CMD "Synchronized\r\n"

bool TLpcIsp::Synchronize()
// Synchronize target
{
int  Length;
byte Question = '?';

   if( !CheckConnect()){
      return( false);
   }
   Port->Flush();
   FEnableEcho = true;                // default enabled
   // send question mark :
   if( Port->Write( &Question, 1) != 1){
      RwdReport( "Unable send");
      Disconnect();
      return( false);
   }
   RwdTx( &Question, 1);
   if( !ReceiveFixed( SYNCHRONIZED_CMD)){
      return( false);
   }
   // confirm synchonization :
   strcpy( FTxBuffer, SYNCHRONIZED_CMD);
   if( !Send()){
      return( false);
   }
   if( !ReceiveMatch( OK_REPLY)){
      return( false);
   }
   // send crystal frequency :
   sprintf( FTxBuffer, "%d\r\n", CrystalFrequency / 1000);
   if( !Send()){
      return( false);
   }
   if( !ReceiveMatch( OK_REPLY)){
      return( false);
   }
   return( true);
} // Synchronize

//******************************************************************************
// Unlock
//******************************************************************************

bool TLpcIsp::Unlock()
// Unlock device
{
   sprintf( FTxBuffer, "U 23130\r\n"); 
   return( SimpleCommand());
} // Unlock

//******************************************************************************
// Set Baud Rate
//******************************************************************************

bool TLpcIsp::SetBaudRate( int NewBaudRate, int NewStopBits)
// Set <BaudRate> with 1/2 <StopBits>
{
   sprintf( FTxBuffer, "B %d %d\r\n", NewBaudRate, NewStopBits);
   if( !SimpleCommand()){
      return( false);
   }
   Sleep( 100);
   BaudRate = NewBaudRate;
   StopBits = NewStopBits;
   Disconnect();                       // next time reopen port 
   return( true);
} // SetBaudRate

//******************************************************************************
// Echo
//******************************************************************************

bool TLpcIsp::Echo( bool On)
// Set command echo <On>
{
   sprintf( FTxBuffer, "A %c\r\n", On ? '1' : '0');
   if( !SimpleCommand()){
      return( false);
   }
   FEnableEcho = On;
   return( true);
} // Echo

//******************************************************************************
// WriteToRam
//******************************************************************************

bool TLpcIsp::WriteToRam( int Address, void *Data, int Size)
// Write <Data> to RAM at <Address> with <Size>
{
   if( Size % 4 != 0){
      IERROR;
   }
   if( Address %4 != 0){
      IERROR;
   }
   if( !Size){
      IERROR;
   }
   if( Address < LPC_RAM_ADDRESS){
      IERROR;
   }
   // send command :
   sprintf( FTxBuffer, "W %d %d\r\n", Address, Size);
   if( !SimpleCommand()){
      return( false);
   }
   // UU encoded transfer :
   int   BlockSize;
   byte *BlockData = (byte *)Data;
   do {
      // send CRC protected block :
      BlockSize  = Size;               // remainder
      if( BlockSize > UU_LINES * UU_LINE_SIZE){
         BlockSize = UU_LINES * UU_LINE_SIZE;
      }
      if( !SendBlock( BlockData, BlockSize)){
         return( false);
      }
      Size      -= BlockSize;          // already send
      BlockData += BlockSize;          // move data pointer
   } while( Size);
   return( true);
} // WriteToRam

//******************************************************************************
// ReadMemory
//******************************************************************************

bool TLpcIsp::ReadMemory( int Address, void *Data, int Size)
// Read <Data> from memory <Address> with <Size>
{
   if( Size % 4 != 0){
      IERROR;
   }
   if( Address %4 != 0){
      IERROR;
   }
   if( !Size){
      IERROR;
   }
   // send command :
   FRxTimeout = RX_READ_TIMEOUT;
   sprintf( FTxBuffer, "R %d %d\r\n", Address, Size);
   if( !SimpleCommand()){
      FRxTimeout = RX_COMMAND_TIMEOUT;
      return( false);
   }
   // UU encoded transfer :
   int   BlockSize;
   byte *BlockData = (byte *)Data;
   do {
      // receive CRC protected block :
      BlockSize  = Size;               // remainder
      if( BlockSize > UU_LINES * UU_LINE_SIZE){
         BlockSize = UU_LINES * UU_LINE_SIZE;
      }
      if( !ReceiveBlock( BlockData, BlockSize)){
         FRxTimeout = RX_COMMAND_TIMEOUT;
         return( false);
      }
      Size      -= BlockSize;          // already send
      BlockData += BlockSize;          // move data pointer
   } while( Size);
   FRxTimeout = RX_COMMAND_TIMEOUT;
   return( true);
} // ReadMemory

//******************************************************************************
// Preapare sector
//******************************************************************************

bool TLpcIsp::PrepareSector( int StartSector, int EndSector)
// Prepare sector <StartSector>..<EndSector> for write
{
   sprintf( FTxBuffer, "P %d %d\r\n", StartSector, EndSector);
   return( SimpleCommand());
} // Prepare Sector

//******************************************************************************
// Copy Ram to Flash
//******************************************************************************

bool TLpcIsp::CopyRamToFlash( int FlashAddress, int RamAddress, int Size)
// Copy data from <RamAddress> to <FlashAddress> with <Size>
{
   if( Size != 256 &&
       Size != 512 &&
       Size != 1024 &&
       Size != 4096){
      IERROR;
   }
   if( FlashAddress % 256 != 0){
      IERROR;
   }
   if( RamAddress % 4 != 0){
      IERROR;
   }
   sprintf( FTxBuffer, "C %d %d %d\r\n", FlashAddress, RamAddress, Size);
   return( SimpleCommand());
} // CopyRamToFlash

//******************************************************************************
// Go
//******************************************************************************

bool TLpcIsp::Go( int StartAddress, TStartMode Mode)
// Run program at <StartAddress> with <Mode>
{
   if( StartAddress %4 != 0){
      IERROR;
   }
   sprintf( FTxBuffer, "G %d %c\r\n", StartAddress, char(Mode));
   return( SimpleCommand());
} // Go

//******************************************************************************
// Erase Sector
//******************************************************************************

bool TLpcIsp::EraseSector( int StartSector, int EndSector)
// Erase sectors <StartSector>..<EndSector>
{
   sprintf( FTxBuffer, "E %d %d\r\n", StartSector, EndSector);
   FRxTimeout = RX_ERASE_TIMEOUT;
   if( !SimpleCommand()){
      FRxTimeout = RX_COMMAND_TIMEOUT;
      return( false);
   }
   FRxTimeout = RX_COMMAND_TIMEOUT;
   return( true);
} // EraseSector

//******************************************************************************
// Blank check
//******************************************************************************

bool TLpcIsp::BlankCheck( int StartSector, int EndSector)
// Blank check sectors <StartSector>..<EndSector>
{
   if( StartSector == 0){
      StartSector = 1;          // always fails - vectors remapped to bootsector
   }
   sprintf( FTxBuffer, "I %d %d\r\n", StartSector, EndSector);
   FRxTimeout = RX_BLANK_TIMEOUT;
   if( !SimpleCommand()){
      FRxTimeout = RX_COMMAND_TIMEOUT;
      return( false);
   }
   FRxTimeout = RX_COMMAND_TIMEOUT;
   return( true);
} // BlankCheck

//******************************************************************************
// Part Identification
//******************************************************************************

bool TLpcIsp::ReadPartId( int &PartId)
// Read <PartId> number
{
   sprintf( FTxBuffer, "J\r\n");
   if( !SimpleCommand()){
      return( false);
   }
   // read part number :
   if( !ReceiveRaw( 0)){
      return( false);
   }
   if( !isdigit( FRxBuffer[ 0])){
      return( false);                  // reply not numeric
   }
   PartId = atoi( FRxBuffer);
   return( true);
} // ReadPartId

//******************************************************************************
// Read boot version
//******************************************************************************

bool TLpcIsp::ReadBootVersion( int &Version)
// Read <Version> number
{
   sprintf( FTxBuffer, "K\r\n");
   if( !SimpleCommand()){
      return( false);
   }
   // read part number :
   if( !ReceiveRaw( 0)){
      return( false);
   }
   if( !isdigit( FRxBuffer[ 0])){
      return( false);                  // reply not numeric
   }
   if( !isdigit( FRxBuffer[ 1])){
      return( false);                  // reply not numeric
   }
   Version = ((FRxBuffer[ 1] - '0') << 4) | (FRxBuffer[ 0] - '0');
   return( true);
} // ReadBootVersion

//******************************************************************************
// Compare
//******************************************************************************

bool TLpcIsp::Compare( int FlashAddress, int RamAddress, int Size)
// Compare <FlashAddress> with <RamAddress> with <Size>
{
   if( Size % 4 != 0){
      IERROR;
   }
   if( FlashAddress % 4 != 0){
      IERROR;
   }
   if( RamAddress % 4 != 0){
      IERROR;
   }
   if( FlashAddress < LPC_VECTOR_SIZE){
      // skip vector area
      int Offset = LPC_VECTOR_SIZE - FlashAddress;
      FlashAddress  = LPC_VECTOR_SIZE;
      RamAddress   += Offset;
      Size         -= Offset;
   }
   sprintf( FTxBuffer, "M %d %d %d\r\n", FlashAddress, RamAddress, Size);
   return( SimpleCommand());
} // Compare

//------------------------------------------------------------------------------

//******************************************************************************
// Simple Command
//******************************************************************************

bool TLpcIsp::SimpleCommand()
// Send command and check reply
{
   // send command :
   if( !Send()){
      return( false);
   }
   // receive status (code\r\n)
   if( !Receive( 3)){
      return( false);
   }
   if( !isdigit( FRxBuffer[ 0])){
      return( false);                  // reply not numeric
   }
   ErrorCode = (TReturnCodes)atoi( FRxBuffer);
   return( ErrorCode == CMD_SUCCESS);
} // SimpleCommand

//******************************************************************************
// Send block
//******************************************************************************

bool TLpcIsp::SendBlock( byte *Data, int Size)
// Send UU encoded block with CRC
{
   int Crc = 0;
   // send lines :
   int LineSize;
   do {
      LineSize  = Size;
      if( LineSize > UU_LINE_SIZE){
         LineSize = UU_LINE_SIZE;
      }
      if( !SendLine( Data, LineSize)){
         return( false);
      }
      Crc  += LineCrc( Data, LineSize);
      Size -= LineSize;            // already send
      Data += LineSize;            // move data pointer
   } while( Size);
   // send CRC :
   sprintf( FTxBuffer, "%d\r\n", Crc);
   if( !Send()){
      return( false);
   }
   // check confirmation :
   if( !ReceiveMatch( OK_REPLY)){
      return( false);
   }
   return( true);
} // SendBlock

//******************************************************************************
// Send line
//******************************************************************************

bool TLpcIsp::SendLine( byte *Data, int Size)
// Send UU encoded line
{
   int i = 0;
   int TriplesCount;
   int Remainder;
   TriplesCount  = Size / 3;           // whole triples count
   Remainder     = Size % 3;           // bytes of last partial triple
   // line length :
   FTxBuffer[ i++] = UuEncode( Size);
   // send triples :
   unsigned Triple;
   while( TriplesCount){
      Triple  = *Data++;
      Triple <<= 8;
      Triple |= *Data++;
      Triple <<= 8;
      Triple |= *Data++;
      FTxBuffer[ i++] = UuEncode( (Triple >> 18) & 63);
      FTxBuffer[ i++] = UuEncode( (Triple >> 12) & 63);
      FTxBuffer[ i++] = UuEncode( (Triple >>  6) & 63);
      FTxBuffer[ i++] = UuEncode(  Triple        & 63);
      TriplesCount--;
   }
   // send remainder :
   if( Remainder){
      // Remainder is 1, 2
      Triple = *Data++;
      Triple <<= 8;
      if( Remainder > 1){
         Triple |= *Data++;
      }
      Triple <<= 8;
      // skip third triple
      FTxBuffer[ i++] = UuEncode( (Triple >> 18) & 63);
      FTxBuffer[ i++] = UuEncode( (Triple >> 12) & 63);
      FTxBuffer[ i++] = UuEncode( (Triple >>  6) & 63);
      FTxBuffer[ i++] = UuEncode(  Triple        & 63);
   }
   // end of line :
   FTxBuffer[ i++] = '\r';
   FTxBuffer[ i++] = '\n';
   FTxBuffer[ i++] = '\0';
   if( !Send()){
      return( false);
   }
   // line echo :
   if( FEnableEcho){
      if( !ReceiveFixed( FTxBuffer)){
         return( false);
      }
   } else {
      Sleep( 50);                      // with special thanks to NXP
   }
   return( true);
} // SendLine

//******************************************************************************
// Receive block
//******************************************************************************

bool TLpcIsp::ReceiveBlock( byte *Data, int Size)
// Receive UU encoded block with CRC
{
   int Crc = 0;
   // receive lines :
   int LineSize;
   do {
      LineSize  = Size;
      if( LineSize > UU_LINE_SIZE){
         LineSize = UU_LINE_SIZE;
      }
      if( !ReceiveLine( Data, LineSize)){
         return( false);
      }
      Crc  += LineCrc( Data, LineSize);
      Size -= LineSize;            // already received
      Data += LineSize;            // move data pointer
   } while( Size);
   // receive CRC :
   if( !ReceiveRaw( GetDigits( Crc) + 2)){  // <number> /r/n
      return( false);
   }
   if( !isdigit( FRxBuffer[ 0])){
      return( false);
   }
   if( Crc != atoi( FRxBuffer)){
      return( false);
   }
   // send confirmation :
   strcpy( FTxBuffer, OK_REPLY);
   if( !Send()){
      return( false);
   }
   // confirmation echo :
   if( FEnableEcho){
      if( !ReceiveFixed( OK_REPLY)){
         return( false);
      }
   }
   return( true);
} // ReceiveBlock

//******************************************************************************
// Receive line
//******************************************************************************

bool TLpcIsp::ReceiveLine( byte *Data, int Size)
// Receive UU encoded line
{
   int Count;
   int TriplesCount;
   int Remainder;
   TriplesCount = Size / 3;            // whole triples count
   Remainder    = Size % 3;            // bytes of last partial triple
   Count  = TriplesCount * 4;          // characters count
   if( Remainder){
      Count += 4;                      // add characters for one triple
   }
   Count += 3;                         // <size> + /r/n characters
   if( !ReceiveRaw( Count)){
      return( false);
   }
   if( Size != UuDecode( FRxBuffer[ 0])){
      return( false);                  // length mismatch
   }
   // read whole triples :
   unsigned Triple;
   byte    *p = Data;
   int      i = 1;                     // start of data
   while( TriplesCount){
      Triple  = UuDecode( FRxBuffer[ i++]);
      Triple <<= 6;
      Triple |= UuDecode( FRxBuffer[ i++]);
      Triple <<= 6;
      Triple |= UuDecode( FRxBuffer[ i++]);
      Triple <<= 6;
      Triple |= UuDecode( FRxBuffer[ i++]);
      *p++ = (Triple >> 16) & 0xFF;
      *p++ = (Triple >> 8)  & 0xFF;
      *p++ =  Triple        & 0xFF;
      TriplesCount--;
   }
   // read partial triple :
   if( Remainder){
      // Remainder is 1, 2
      Triple  = UuDecode( FRxBuffer[ i++]);
      Triple <<= 6;
      Triple |= UuDecode( FRxBuffer[ i++]);
      Triple <<= 6;
      Triple |= UuDecode( FRxBuffer[ i++]);
      Triple <<= 6;
      Triple |= UuDecode( FRxBuffer[ i++]);
      *p++ = (Triple >> 16) & 0xFF;
      if( Remainder > 1){
         *p++ = (Triple >> 8) & 0xFF;
      }
      // skip third triple
   }
   return( true);
} // ReceiveLine

//******************************************************************************
// Line CRC
//******************************************************************************

int TLpcIsp::LineCrc( byte *Data, int Size)
// Returns line CRC
{
   int Crc = 0;
   for( int i = 0; i < Size; i++){
      Crc += *Data++;
   }
   return( Crc);
} // LineCrc

//******************************************************************************
// UU Encode
//******************************************************************************

byte TLpcIsp::UuEncode( byte Data)
// Returns UU character encoded <Data>
{
   return( (Data == 0) ? 0x60 : (Data + 0x20));
} // UuEncode

//******************************************************************************
// UU Decode
//******************************************************************************

byte TLpcIsp::UuDecode( byte Data)
// Returns binary UU decoded <Data>
{
   return( (Data == 0x60) ? 0x00 : Data - 0x20);
} // UuDecode

//******************************************************************************
// Get digits
//******************************************************************************

int TLpcIsp::GetDigits( int Number)
// Returns <Number> of digits
{
   int Count = 1;                      // at least one digit
   for( int i = 0; i < 10; i++){
      Number /= 10;
      if( !Number){
         break;
      }
      Count++;
   }
   return( Count);
} // GetDigits

//******************************************************************************
// Send
//******************************************************************************

bool TLpcIsp::Send()
// Send command
{
   if( !CheckConnect()){
      return( false);
   }
   Port->Flush();
   int Length = strlen( FTxBuffer);
   if( Port->Write( FTxBuffer, Length) != Length){
      RwdReport( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      return( false);
   }
   RwdTx( FTxBuffer, Length);
   return( true);
} // Send

//******************************************************************************
// Receive
//******************************************************************************

bool TLpcIsp::Receive( int ReqSize)
// Receive reply
{
   // read echo :
   if( FEnableEcho){
      if( !ReceiveFixed( FTxBuffer)){
         return( false);
      }
   }
   // read reply :
   return( ReceiveRaw( ReqSize));
} // Receive

//******************************************************************************
// Receive match
//******************************************************************************

bool TLpcIsp::ReceiveMatch( char *Reply)
// Receive with echo and compare with <Reply>
{
   // read echo :
   if( FEnableEcho){
      if( !ReceiveFixed( FTxBuffer)){
         return( false);
      }
   }
   return( ReceiveFixed( Reply));
} // ReceiveMatch

//******************************************************************************
// Receive fixed
//******************************************************************************

bool TLpcIsp::ReceiveFixed( char *Reply)
// Receive and compare with <Reply>
{
   int Length = strlen( Reply);
   Port->SetRxWait( FRxTimeout, 0);
   int RxSize = Port->Read( FRxBuffer, Length);
   if( RxSize != Length){
      if( !RxSize){
         RwdReport( "Rx timeout");
      } else {
         RwdGarbage( FRxBuffer, RxSize);
      }
      return( false);
   }
   FRxBuffer[ RxSize] = '\0';
   if( !strequ( Reply, FRxBuffer)){
      RwdGarbage( FRxBuffer, RxSize);
      FlushRxChars();
      return( false);
   }
   RwdRx( FRxBuffer, RxSize);
   return( true);
} // ReceiveFixed

//******************************************************************************
// Receive Raw
//******************************************************************************

bool TLpcIsp::ReceiveRaw( int ReqSize)
// Receive <ReqSize> characters. <ReqSize> == 0 up to timeout
{
   // read requested size :
   int RxSize;    // actual read size
   if( ReqSize){
      Port->SetRxWait( FRxTimeout, 0);
      RxSize = Port->Read( FRxBuffer, ReqSize);
      if( RxSize != ReqSize){
         if( !RxSize){
            RwdReport( "Rx timeout");
         } else {
            RwdGarbage( FRxBuffer, RxSize);
         }
         return( false);
      }
      FRxBuffer[ RxSize] = '\0';
      RwdRx( FRxBuffer, RxSize);
      return( true);
   }
   // read any size
   Port->SetRxWait( FRxTimeout, 0);
   RxSize = Port->Read( FRxBuffer, LPC_BUFFER_SIZE - 1);       // space for zero terminator
   if( !RxSize){
      RwdReport( "Rx timeout");
      return( false);
   }
   RwdRx( FRxBuffer, RxSize);
   FRxBuffer[ RxSize] = '\0';
   return( true);
} // ReceiveRaw

//******************************************************************************
// Flush
//******************************************************************************

#define FLUSH_DATA_SIZE 1024

void TLpcIsp::FlushRxChars()
// Flush chars up to intercharacter timeout
{
byte Data[ FLUSH_DATA_SIZE];

   Port->SetRxWait( RX_INTERCHARACTER_TIMEOUT, RX_INTERCHARACTER_TIMEOUT);
   for( int i = 0; i < FLUSH_DATA_SIZE; i++){
      if( Port->Read( &Data[ i], 1) == 0){
         // timeout
         if( i > 0){
            RwdGarbage( Data, i);
         }
         return;
      }
   }
   RwdGarbage( Data, FLUSH_DATA_SIZE);
} // FlushRxChars

//******************************************************************************
// Check Connection
//******************************************************************************

bool TLpcIsp::CheckConnect()
// Check if adapter is ready
{
   if( Port && Port->IsOpen){
      return( true);
   }
   if( Port){
      IERROR;                          // always opened !
   }
   TIdentifier Identifier;
   if( ViaUsb){
      // USB setup
      TUsbUart *Usb = new TUsbUart;
      if( !Usb->Locate( DeviceName, Identifier)){
         delete Usb;
         return( false);
      }
      Port = Usb;
   } else {
      // COM setup
      TComUart *Com = new TComUart;
      if( !Com->Locate( DeviceName, Identifier)){
         delete Com;
         return( false);
      }
      Port = Com;
   }
   if( !Port->Open( Identifier)){
      delete Port;
      Port = 0;
      return( false);
   }
   // default parameters :
   TUart::TParameters Parameters;
   Parameters.BaudRate     = BaudRate;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = StopBits * 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   Port->SetParameters( Parameters);
   // common init :
   Port->DTR = false;
   Port->RTS = false;
   Port->SetRxNowait();
   Port->Flush();
   return( true);
} // CheckConnect

//******************************************************************************
// Disconnect
//******************************************************************************

void TLpcIsp::Disconnect()
// Disconnect port
{
   if( !Port){
      return;
   }
   delete Port;
   Port = 0;
} // Disconnect

