//******************************************************************************
//
//   Main.cpp     Bat1 Tester main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Main.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";
#define StatusErr()  Status() + "ERROR";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Lpc = new TLpcFlash;
   Hex = new THexFile( 512 * 1024);
//   Lpc->BaudRate         = 9600;
//   Lpc->BaudRate         = 19200;
   Lpc->BaudRate         = 57600;
   Lpc->CrystalFrequency = 18432000;
   strcpy( Lpc->DeviceName, "VEIT Bat1 Poultry Scale");
   Lpc->ViaUsb = true;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Lpc->Logger  = Logger;
   Lpc->ProgressFunction       = this->Progress;
   Lpc->EnableFlashComparation = true;
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Progress
//******************************************************************************

void TMainForm::Progress( int Percent)
// Update progress bar
{
   ProgressBar->Position = Percent / 10;
   AnsiString String;
   String.printf( "%5.1f %%", (double)Percent / 10);
   LblPercent->Caption = String;
   Application->ProcessMessages();
   Screen->Cursor      = crHourGlass;// wait cursor
} // Progress

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   ProgressBar->Visible = true;
   LblPercent->Visible  = true;
   Progress( 0);
   this->Enabled        = false;      // disable main window
   Application->ProcessMessages();
   Screen->Cursor       = crHourGlass;// wait cursor
} // StartCom

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   Progress( 0);
   ProgressBar->Visible  = false;
   LblPercent->Visible   = false;
   this->Enabled         = true;       // enable main window
   Screen->Cursor        = crDefault;  // normal cursor
} // StopComm

//******************************************************************************
// Enable Logger
//******************************************************************************

void __fastcall TMainForm::CbEnableLoggerClick(TObject *Sender)
{
   if( CbEnableLogger->Checked){
     Lpc->Logger = Logger;
   } else {
     Lpc->Logger = 0;
   }
} // CbEnableLogger

//******************************************************************************
//  Device info
//******************************************************************************

void __fastcall TMainForm::BtnInfoClick(TObject *Sender)
{
   if( !Lpc->GetDeviceInfo()){
      Crt->printf( "Unable get device info\n");
      StatusErr();
      return;
   }
   Crt->printf( "Device  : %s\n", Lpc->Device);
   Crt->printf( "Version : %s\n", Lpc->Version);
   StatusOk();
} // BtnInfoClick

//******************************************************************************
// Echo
//******************************************************************************

void __fastcall TMainForm::BtnEchoClick(TObject *Sender)
{
   if( !Lpc->Echo( false)){
      Crt->printf( "Unable se echo\n");
      StatusErr();
      return;
   }
   StatusOk();
} // BtnEchoClick

//******************************************************************************
// Write
//******************************************************************************

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   StartComm();
   if( !Lpc->WriteFlash( 0, Hex->Code, Hex->CodeSize)){
      StopComm();
      Crt->printf( "Unable write flash\n");
      StatusErr();
      return;
   }
   StopComm();
   StatusOk();
} // BtnWriteClick

//******************************************************************************
// Read
//******************************************************************************


void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
static byte Buffer[ 512 * 1024];

   StartComm();
   if( !Lpc->ReadFlash( 0, Buffer, Hex->CodeSize)){
      StopComm();
      Crt->printf( "Unable read flash\n");
      StatusErr();
      return;
   }
   StopComm();
   // verify
   for( int i = LPC_VECTOR_SIZE; i < Hex->CodeSize; i++){
      if( Buffer[ i] != Hex->Code[ i]){
         Crt->printf( "Comparation error\n");
         StatusErr();
         return;
      }
   }
   StatusOk();
} // BtnReadClick

//******************************************************************************
// Compare
//******************************************************************************

void __fastcall TMainForm::BtnCompareClick(TObject *Sender)
{
   StartComm();
   if( !Lpc->CompareFlash( 0, Hex->Code, Hex->CodeSize)){
      StopComm();
      Crt->printf( "Unable compare flash\n");
      StatusErr();
      return;
   }
   StopComm();
   StatusOk();
} // BtnCompareClick

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   if( !Hex->Load( FileOpenDialog->FileName.c_str())){
      Crt->printf( "Unable load file\n");
      StatusErr();
      return;
   }
   StatusOk();
} // BtnLoadClick

//******************************************************************************
// Reset
//******************************************************************************

void __fastcall TMainForm::BtnResetClick(TObject *Sender)
{
   if( !Lpc->Reset()){
      Crt->printf( "Unable send reset\n");
      StatusErr();
      return;
   }
   StatusOk();
} // BtnResetClick

