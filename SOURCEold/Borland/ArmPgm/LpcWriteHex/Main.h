//******************************************************************************
//
//   Main.h        Main header
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Library/Crt/Crt.h"
#include "../Library/Serial/CrtLogger.h"
#include "LpcFlash.h"
#include "HexFile.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnInfo;
        TButton *BtnWrite;
        TButton *BtnEcho;
        TButton *BtnRead;
        TButton *BtnCompare;
        TProgressBar *ProgressBar;
        TLabel *LblPercent;
        TCheckBox *CbEnableLogger;
        TButton *BtnLoad;
        TOpenDialog *FileOpenDialog;
        TButton *BtnReset;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnInfoClick(TObject *Sender);
        void __fastcall BtnWriteClick(TObject *Sender);
        void __fastcall BtnEchoClick(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
        void __fastcall BtnCompareClick(TObject *Sender);
        void __fastcall CbEnableLoggerClick(TObject *Sender);
        void __fastcall BtnLoadClick(TObject *Sender);
        void __fastcall BtnResetClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt       *Crt;
   TCrtLogger *Logger;
   TLpcFlash  *Lpc;
   THexFile   *Hex;

   __fastcall TMainForm(TComponent* Owner);
   void Progress( int Percent);
   void StartComm();
   void StopComm();
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
