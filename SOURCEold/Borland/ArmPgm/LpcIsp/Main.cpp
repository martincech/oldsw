//******************************************************************************
//
//   Main.cpp     Bat1 Tester main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Main.h"
#include "../Library/Serial/CrtLogger.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Isp = new TLpcIsp;
   Isp->BaudRate         = 9600;
   Isp->CrystalFrequency = 18432000;
   strcpy( Isp->DeviceName, "VEIT Bat1 Poultry Scale");
   Isp->ViaUsb = true;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt = new TCrt( MainMemo);
   TCrtLogger *Logger = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Isp->Logger = Logger;
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
//  Device info
//******************************************************************************

void __fastcall TMainForm::BtnInfoClick(TObject *Sender)
{
   if( !Isp->EnterProgramming()){
      Crt->printf( "Unable enter programming\n");
      return;
   }
   if( !Isp->Synchronize()){
      Crt->printf( "Unable synchronize\n");
      return;
   }
   int PartId;
   if( !Isp->ReadPartId( PartId)){
      Crt->printf( "Unable read part id\n");
      return;
   }
   Crt->printf( "Part ID : %08X\n", PartId);
   int Version;
   if( !Isp->ReadBootVersion( Version)){
      Crt->printf( "Unable read version\n");
      return;
   }
   Crt->printf( "Version : %1X.%1X\n", Version >> 4, Version & 0x0F);
   StatusOk();
} // BtnInfoClick

//******************************************************************************
// Write
//******************************************************************************

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   int Address = 0x40000200 + EdtAddress->Text.ToInt();
   int Size = EdtSize->Text.ToInt();
   byte Buffer[ LPC_MAX_COPY_SIZE];
   for( int i = 0; i < Size; i++){
      Buffer[ i] = (byte)i;
   }
   if( !Isp->WriteToRam( Address, Size, Buffer)){
      Crt->printf( "Unable write to RAM\n");
      return;
   }
   StatusOk();
} // BtnWriteClick

//******************************************************************************
// Echo
//******************************************************************************

void __fastcall TMainForm::BtnEchoClick(TObject *Sender)
{
   if( !Isp->Echo( false)){
      Crt->printf( "Unable se echo\n");
      return;
   }
   StatusOk();
} // BtnEchoClick

//******************************************************************************
// Read
//******************************************************************************

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   int Address = 0x40000200 + EdtAddress->Text.ToInt();
   int Size = EdtSize->Text.ToInt();
   byte Buffer[ LPC_MAX_COPY_SIZE];
   if( !Isp->ReadMemory( Address, Size, Buffer)){
      Crt->printf( "Unable read RAM\n");
      return;
   }
   // verify
   for( int i = 0; i < Size; i++){
      if( Buffer[ i] != (byte)i){
         Crt->printf( "Comparation error\n");
         return;
      }
   }
   StatusOk();
} // BtnReadClick

