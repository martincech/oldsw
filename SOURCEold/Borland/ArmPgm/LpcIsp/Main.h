//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Crt/Crt.h"
#include "LpcIsp.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnInfo;
        TButton *BtnWrite;
        TEdit *EdtAddress;
        TLabel *Label1;
        TEdit *EdtSize;
        TLabel *Label2;
        TButton *BtnEcho;
        TButton *BtnRead;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnInfoClick(TObject *Sender);
        void __fastcall BtnWriteClick(TObject *Sender);
        void __fastcall BtnEchoClick(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt    *Crt;
   TLpcIsp *Isp;
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
