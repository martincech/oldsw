//******************************************************************************
//
//   LpcIsp.h     NXP ARM ISP commands
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef LpcIspH
   #define LpcIspH

#ifndef UartH
   #include "../Serial/Uart.h"
#endif

#ifndef LoggerH
   #include "../Serial/Logger.h"
#endif

#define LPC_MAX_COPY_SIZE  4096       // RAM to flash max. copy size
#define LPC_RAM_ADDRESS    0x40000200 // free RAM start

#define DEVICE_NAME_SIZE   63         // COM/USB device name
#define LPC_BUFFER_SIZE    128        // Rx/Tx buffer size

//******************************************************************************
// TLpcIsp
//******************************************************************************

class TLpcIsp {
public :
   // ISP return codes
   typedef enum {
      CMD_SUCCESS,
      INVALID_COMMAND,
      SRC_ADDR_ERROR,
      DST_ADDR_ERROR,
      SRC_ADDR_NOT_MAPPED,
      DST_ADDR_NOT_MAPPED,
      COUNT_ERROR,
      INVALID_SECTOR,
      SECTOR_NOT_BLANK,
      SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION,
      COMPARE_ERROR,
      BUSY,
      PARAM_ERROR,
      ADDR_ERROR,
      ADDR_NOT_MAPPED,
      CMD_LOCKED,
      INVALID_CODE,
      INVALID_BAUD_RATE,
      INVALID_STOP_BIT,
      CODE_READ_PROTECTION_ENABLED
   } TReturnCodes;

   // start command mode
   typedef enum {
      START_MODE_ARM = 'A',
      START_MODE_THUMB = 'T'
   } TStartMode;

   // part identification number
   typedef enum {
      LPC2131 = 0x2FF01,
      LPC2132 = 0x2FF11,
      LPC2134 = 0x2FF12,
      LPC2136 = 0x2FF23,
      LPC2138 = 0x2FF25,
   } TPartIdentification;

//------------------------------------------------------------------------------
   TLpcIsp();
   // Constructor
   ~TLpcIsp();
   // Destructor

   bool CheckParameters();
   // Check BaudRate & CrystalFrequency
   int GetCopySize( int Size);
   // Align copy size
   bool EnterProgramming();
   // Enter programming mode
   bool ResetTarget();
   // Reset & run program
   bool Synchronize();
   // Synchronize target

   bool Unlock();
   // Unlock device
   bool SetBaudRate( int NewBaudRate, int NewStopBits);
   // Set <BaudRate> with 1/2 <StopBits>
   bool Echo( bool On);
   // Set command echo <On>
   bool WriteToRam( int Address, int Size, void *Data);
   // Write <Data> to RAM at <Address> with <Size>
   bool ReadMemory( int Address, int Size, void *Data);
   // Read <Data> from memory <Address> with <Size>
   bool PrepareSector( int StartSector, int EndSector);
   // Prepare sector <StartSector>..<EndSector> for write
   bool CopyRamToFlash( int FlashAddress, int RamAddress, int Size);
   // Copy data from <RamAddress> to <FlashAddress> with <Size>
   bool Go( int StartAddress, TStartMode Mode);
   // Run program at <StartAddress> with <Mode>
   bool EraseSector( int StartSector, int EndSector);
   // Erase sectors <StartSector>..<EndSector>
   bool BlankCheck( int StartSector, int EndSector);
   // Blank check sectors <StartSector>..<EndSector>
   bool ReadPartId( int &PartId);
   // Read <PartId> number
   bool ReadBootVersion( int &Version);
   // Read <Version> number
   bool Compare( int Address1, int Address2, int Size);
   // Compare <Address1> with <Address2> with <Size>

// properties :
   TUart       *Port;                              // connection port
   TLogger     *Logger;                            // raw data logger
   char         DeviceName[ DEVICE_NAME_SIZE + 1]; // COM/USB device name
   bool         ViaUsb;                            // device via USB
   int          BaudRate;                          // communication speed [Bd]
   int          StopBits;                          // stop bits [1/2]
   int          CrystalFrequency;                  // device crystal frequency [Hz]
   TReturnCodes ErrorCode;                         // operation result
//------------------------------------------------------------------------------

protected :
   bool FEnableEcho;                   // enable command echo
   byte FTxBuffer[ LPC_BUFFER_SIZE];   // Tx buffer
   byte FRxBuffer[ LPC_BUFFER_SIZE];   // Rx buffer

   bool SimpleCommand();
   // Send command and check reply
   bool SendBlock( byte *Data, int Size);
   // Send UU encoded block with CRC
   bool SendLine( byte *Data, int Size);
   // Send UU encoded line
   bool ReceiveBlock( byte *Data, int Size);
   // Receive UU encoded block with CRC
   bool ReceiveLine( byte *Data, int Size);
   // Receive UU encoded line
   int LineCrc( byte *Data, int Size);
   // Returns line CRC
   byte UuEncode( byte Data);
   // Returns UU character encoded <Data>
   byte UuDecode( byte Data);
   // Returns binary UU decoded <Data>
   int GetDigits( int Number);
   // Returns <Number> of digits

   bool Send();
   // Send command
   bool Receive( int ReqSize);
   // Receive reply with echo
   bool ReceiveMatch( char *Reply);
   // Receive with echo and compare with <Reply>

   bool ReceiveFixed( char *Reply);
   // Receive and compare with <Reply>
   bool ReceiveRaw( int ReqSize);
   // Receive <ReqSize> characters. <ReqSize> == 0 up to timeout

   void FlushRxChars();
   // Flush chars up to intercharacter timeout
   bool CheckConnect();
   // Check if adapter is ready
   void Disconnect();
   // Disconnect adapter
}; // TLpcIsp

#endif
