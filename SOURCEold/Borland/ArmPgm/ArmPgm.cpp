//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("ArmPgm.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USEUNIT("LpcIsp.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("LpcFlash.cpp");
USEUNIT("HexFile.cpp");
USELIB("..\Library\Serial\USB\ftd2xx.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 Programmer";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
