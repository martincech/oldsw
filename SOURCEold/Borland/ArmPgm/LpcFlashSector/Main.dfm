object MainForm: TMainForm
  Left = 559
  Top = 304
  Width = 670
  Height = 646
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'ARM Programmer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 520
    Top = 424
    Width = 44
    Height = 13
    Caption = 'Address :'
  end
  object Label2: TLabel
    Left = 520
    Top = 480
    Width = 26
    Height = 13
    Caption = 'Size :'
  end
  object LblPercent: TLabel
    Left = 568
    Top = 344
    Width = 38
    Height = 13
    Caption = '100.0 %'
  end
  object MainMemo: TMemo
    Left = 0
    Top = 48
    Width = 497
    Height = 537
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 600
    Width = 662
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnInfo: TButton
    Left = 520
    Top = 48
    Width = 75
    Height = 25
    Caption = 'Info'
    TabOrder = 2
    OnClick = BtnInfoClick
  end
  object BtnWrite: TButton
    Left = 520
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Write'
    TabOrder = 3
    OnClick = BtnWriteClick
  end
  object EdtAddress: TEdit
    Left = 520
    Top = 448
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '0'
  end
  object EdtSize: TEdit
    Left = 520
    Top = 496
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '4'
  end
  object BtnEcho: TButton
    Left = 520
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Echo off'
    TabOrder = 6
    OnClick = BtnEchoClick
  end
  object BtnRead: TButton
    Left = 520
    Top = 176
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 7
    OnClick = BtnReadClick
  end
  object BtnCompare: TButton
    Left = 520
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Compare'
    TabOrder = 8
    OnClick = BtnCompareClick
  end
  object ProgressBar: TProgressBar
    Left = 504
    Top = 320
    Width = 150
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 9
  end
  object CbEnableLogger: TCheckBox
    Left = 24
    Top = 16
    Width = 97
    Height = 17
    Caption = 'Enable Logger'
    Checked = True
    State = cbChecked
    TabOrder = 10
    OnClick = CbEnableLoggerClick
  end
end
