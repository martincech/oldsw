//******************************************************************************
//
//   Main.cpp     Bat1 Tester main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "Main.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Lpc = new TLpcFlash;
   Lpc->BaudRate         = 9600;
   Lpc->CrystalFrequency = 18432000;
   strcpy( Lpc->DeviceName, "VEIT Bat1 Poultry Scale");
   Lpc->ViaUsb = true;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
   Logger->Mode = TCrtLogger::MIXED;
   Lpc->Logger  = Logger;
   Lpc->ProgressFunction       = this->Progress;
   Lpc->EnableFlashComparation = true;
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Progress
//******************************************************************************

void TMainForm::Progress( int Percent)
// Update progress bar
{
   ProgressBar->Position = Percent / 10;
   AnsiString String;
   String.printf( "%5.1f %%", (double)Percent / 10);
   LblPercent->Caption = String;
   ProgressBar->Refresh();
   LblPercent->Refresh();
} // Progress

//******************************************************************************
// Enable Logger
//******************************************************************************

void __fastcall TMainForm::CbEnableLoggerClick(TObject *Sender)
{
   if( CbEnableLogger->Checked){
     Lpc->Logger = Logger;
   } else {
     Lpc->Logger = 0;
   }
} // CbEnableLogger

//******************************************************************************
//  Device info
//******************************************************************************

void __fastcall TMainForm::BtnInfoClick(TObject *Sender)
{
   if( !Lpc->GetDeviceInfo()){
      Crt->printf( "Unable get device info\n");
      return;
   }
   Crt->printf( "Device  : %s\n", Lpc->Device);
   Crt->printf( "Version : %s\n", Lpc->Version);
   StatusOk();
} // BtnInfoClick

//******************************************************************************
// Write
//******************************************************************************

#define CODE_SIZE  5000
byte Buffer[ 512 * 1024];

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   memset( Buffer, 0xFF, sizeof( Buffer));
   for( int i = 0; i < CODE_SIZE; i++){
      Buffer[ i] = (byte)i;
   }
   if( !Lpc->WriteFlash( 0, Buffer, CODE_SIZE)){
      Crt->printf( "Unable write flash\n");
      return;
   }
   StatusOk();
} // BtnWriteClick

//******************************************************************************
// Echo
//******************************************************************************

void __fastcall TMainForm::BtnEchoClick(TObject *Sender)
{
   if( !Lpc->Echo( false)){
      Crt->printf( "Unable se echo\n");
      return;
   }
   StatusOk();
} // BtnEchoClick

//******************************************************************************
// Read
//******************************************************************************

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   if( !Lpc->ReadFlash( 0, Buffer, CODE_SIZE)){
      Crt->printf( "Unable read flash\n");
      return;
   }
   // verify
   for( int i = LPC_VECTOR_SIZE; i < CODE_SIZE; i++){
      if( Buffer[ i] != (byte)i){
         Crt->printf( "Comparation error\n");
         return;
      }
   }
   StatusOk();
} // BtnReadClick

//******************************************************************************
// Compare
//******************************************************************************

void __fastcall TMainForm::BtnCompareClick(TObject *Sender)
{
   for( int i = 0; i < CODE_SIZE; i++){
      Buffer[ i] = (byte)i;
   }
   if( !Lpc->CompareFlash( LPC_VECTOR_SIZE, &Buffer[ LPC_VECTOR_SIZE], CODE_SIZE - LPC_VECTOR_SIZE)){
      Crt->printf( "Unable compare flash\n");
      return;
   }
   StatusOk();
} // BtnCompareClick

