//******************************************************************************
//
//   LpcFlash.cpp   LPC programming commands
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "LpcFlash.h"
#include <string.h>
#include <stdio.h>

#pragma package(smart_init)

#define LPC_READ_CHUNK  900      // read RAM/flash chunk size

#define ShowProgress( Percent) if( ProgressFunction) ProgressFunction( Percent)

//******************************************************************************
// Constructor
//******************************************************************************

TLpcFlash::TLpcFlash() : TLpcIsp()
// Constructor
{
   strcpy( Version, "?");
   strcpy( Device,  "?");
   EnableFlashComparation = false;
   ProgressFunction       = 0;
} // TLpcFlash

//******************************************************************************
// Destructor
//******************************************************************************

TLpcFlash::~TLpcFlash()
// Destructor
{
} // ~TLpcFlash

//******************************************************************************
// Get Device info
//******************************************************************************

bool TLpcFlash::GetDeviceInfo()
// Get CPU informations
{
   if( !EnterProgramming()){
      return( false);
   }
   if( !Synchronize()){
      return( false);
   }
   if( !ReadPartId( FPartId)){
      return( false);
   }
   switch( FPartId){
      case LPC2131 :
         strcpy( Device, "LPC2131");
         break;
      case LPC2132 :
         strcpy( Device, "LPC2132");
         break;
      case LPC2134 :
         strcpy( Device, "LPC2134");
         break;
      case LPC2136 :
         strcpy( Device, "LPC2136");
         break;
      case LPC2138 :
         strcpy( Device, "LPC2138");
         break;
      default :
         return( false);               // unsupported device
   }
   if( !ReadBootVersion( FBootVersion)){
      return( false);
   }
   sprintf( Version, "%1X.%1X", FBootVersion >> 4, FBootVersion & 0x0F);
   return( true);
} // GetDeviceInfo

//******************************************************************************
// Write flash
//******************************************************************************

bool TLpcFlash::WriteFlash( int Address, void *Data, int Size)
// Write <Data> to flash memory
{
   if( (Address + Size) > GetFlashSize()){
      return( false);
   }
   if( Address != 0){
      IERROR;                          // not implemented yet
   }
   // enable flash write :
   if( !Unlock()){
      return( false);
   }
   // progress data :
   FTotalSize = Size;
   FDoneSize  = 0;
   ShowProgress( 0);
   // write sectors :
   int   Sector     = 0;
   byte *DataSector = (byte *)Data;
   int   SectorSize;
   do {
      SectorSize = Size;
      if( SectorSize > GetSectorSize( Sector)){
         SectorSize = GetSectorSize( Sector);
      }
      if( !WriteFlashSector( Sector, Address, DataSector, SectorSize)){
         return( false);
      }
      Address    += SectorSize;
      DataSector += SectorSize;
      Size       -= SectorSize;
      Sector++;
   } while( Size);
   return( true);
} // WriteFlash

//******************************************************************************
// Read memory
//******************************************************************************

bool TLpcFlash::ReadFlash( int Address, void *Data, int Size)
// Read <Data> from memory <Address>
{
   if( (Address + Size) > GetFlashSize()){
      return( false);
   }
   // progress data :
   FTotalSize = Size;
   FDoneSize  = 0;
   ShowProgress( 0);
   // read chunks :
   byte *DataChunk = (byte *)Data;
   int   ChunkSize;
   do {
      ChunkSize = Size;
      if( ChunkSize > LPC_READ_CHUNK){
         ChunkSize = LPC_READ_CHUNK;
      }
      if( !ReadMemory( Address, DataChunk, ChunkSize)){
         return( false);
      }
      Address   += ChunkSize;
      DataChunk += ChunkSize;
      Size      -= ChunkSize;
      // update progress :
      FDoneSize += ChunkSize;
      ShowProgress( (FDoneSize * 1000) / FTotalSize);
   } while( Size);
   return( true);
} // ReadFlash

//******************************************************************************
// Compare flash
//******************************************************************************

bool TLpcFlash::CompareFlash( int Address, void *Data, int Size)
// Compare flash memory with <Data>
{
static byte Buffer[ LPC_READ_CHUNK];

   if( (Address + Size) > GetFlashSize()){
      return( false);
   }
   // progress data :
   FTotalSize = Size;
   FDoneSize  = 0;
   ShowProgress( 0);
   // read chunks :
   byte *DataChunk = (byte *)Data;
   int   ChunkSize;
   do {
      ChunkSize = Size;
      if( ChunkSize > LPC_READ_CHUNK){
         ChunkSize = LPC_READ_CHUNK;
      }
      if( !ReadMemory( Address, Buffer, ChunkSize)){
         return( false);
      }
      // compare :
      for( int i = 0; i < ChunkSize; i++){
         if( Buffer[ i] != DataChunk[ i]){
            return( false);
         }
      }
      // next chunk :
      Address   += ChunkSize;
      DataChunk += ChunkSize;
      Size      -= ChunkSize;
      // update progress :
      FDoneSize += ChunkSize;
      ShowProgress( (FDoneSize * 1000) / FTotalSize);
   } while( Size);
   return( true);
} // CompareFlash

//******************************************************************************
// Rest
//******************************************************************************

bool TLpcFlash::Reset()
// Restart device
{
   return( ResetTarget());
} // Reset

//------------------------------------------------------------------------------

//******************************************************************************
// Get flash size
//******************************************************************************

int TLpcFlash::GetFlashSize()
// Returns flash size
{
   return( 512 * 1024 - 12 * 1024);    // total size - boot sector size
   // model specific
} // GetFlashSize

//******************************************************************************
// Get RAM size
//******************************************************************************

int TLpcFlash::GetRamSize()
// Return RAM size
{
   return( 32 * 1024);
   // model specific
} // GetRamSize

//******************************************************************************
// Get segment size
//******************************************************************************

int TLpcFlash::GetSectorSize( int Sector)
// Returns segment size
{
   if( Sector < 8){
      return( 4 * 1024);
   }
   if( Sector < 22){
      return( 32 * 1024);
   }
   return( 4 * 1024);
   // model specific
} // GetSectorSize

//******************************************************************************
// Get Copy size
//******************************************************************************

int TLpcFlash::GetCopySize( int Size)
// Returns maximal copy size
{
   if( Size <= 256){
      return( 256);
   }
   if( Size <= 512){
      return( 512);
   }
   if( Size <= 1024){
      return( 1024);
   }
   return( 4096);
   // model specific
   // compare copy size with available RamSize - (LPC_RAM_ADDRESS - LPC_RAM_START)
} // GetCopySize

//******************************************************************************
// Write flash segment
//******************************************************************************

bool TLpcFlash::WriteFlashSector( int Sector, int Address, byte *Data, int Size)
// Write <Data> to <Sector>
{
   // erase sector :
   if( !PrepareSector( Sector, Sector)){
      return( false);
   }
   if( !EraseSector( Sector, Sector)){
      return( false);
   }
   // copy RAM to flash :
   int CopySize;
   do {
      CopySize = GetCopySize( Size);
      if( !WriteToRam( LPC_RAM_ADDRESS, Data, CopySize)){
         return( false);
      }
      if( !PrepareSector( Sector, Sector)){
         return( false);
      }
      if( !CopyRamToFlash( Address, LPC_RAM_ADDRESS, CopySize)){
         return( false);
      }
      if( EnableFlashComparation){
         if( !Compare( Address, LPC_RAM_ADDRESS, CopySize)){
            return( false);
         }
      }
      Address += CopySize;
      Data    += CopySize;
      if( Size > CopySize){
         Size      -= CopySize;
         FDoneSize += CopySize;
      } else {
         // copy size may be greater than remainder
         FDoneSize += Size;            // take true remainder
         Size = 0;                     // terminate loop
      }
      // update progress :
      ShowProgress( (FDoneSize * 1000) / FTotalSize);
   } while( Size);
   return( true);
} // WriteFlashSector

