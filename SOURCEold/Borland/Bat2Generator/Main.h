//******************************************************************************
//
//   Main.h       Bat2 generator main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//---------------------------------------------------------------------------

#include "Workplace.h"

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TPanel *Panel1;
   TButton *BtnLoadData;
   TOpenDialog *FileOpenDialog;
   TChart *Graph;
   TLineSeries *SamplesSeries;
   TPanel *Panel2;
   TEdit *EdtWidth;
   TLabel *Label2;
   TButton *BtnBegin;
   TButton *BtnPrev;
   TButton *BtnNext;
   TButton *BtnEnd;
   TEdit *EdtOffset;
   TLabel *Label3;
   TButton *BtnRedraw;
   TLabel *Label1;
   TLabel *LblDataName;
   TButton *BtnLoadSetup;
   TButton *BtnSaveSetup;
   TLabel *Label4;
   TLabel *LblSetupName;
   TOpenDialog *SetupOpenDialog;
   TSaveDialog *SetupSaveDialog;
   TPanel *Panel3;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *Label7;
   TLabel *LblMin;
   TLabel *LblMax;
   TLabel *Label9;
   TLabel *Label8;
   TEdit *EdtRangeMax;
   TLabel *Label10;
   TEdit *EdtRangeMin;
   TPanel *Panel4;
   TLabel *Label11;
   TLabel *Label12;
   TEdit *EdtSupplyMin;
   TLabel *Label13;
   TLabel *Label14;
   TLabel *Label15;
   TButton *BtnSetMin;
   TLabel *Label16;
   TEdit *EdtSupplyMax;
   TLabel *Label17;
   TButton *BtnSetMax;
   TLabel *Label18;
   TEdit *EdtSupplyMarker;
   TLabel *Label19;
   TButton *BtnSetMarker;
   TCheckBox *CbUseMarker;
   TButton *BtnPlayAll;
   TButton *BtnPlayWindow;
   TCheckBox *CbPlayLoop;
   TButton *BtnStop;
   TLabel *LblNotConnected;
   TLabel *Label20;
   TEdit *EdtSupplyStep;
   TLabel *Label21;
   TButton *BtnConnect;
   TEdit *EdtCom;
   TLabel *Label22;
   TEdit *EdtSupplySpace;
   TLabel *Label23;
   TButton *BtnSetDefaults;
   TLabel *Label24;
   TLabel *LblCount;
   void __fastcall BtnLoadDataClick(TObject *Sender);
   void __fastcall BtnBeginClick(TObject *Sender);
   void __fastcall BtnPrevClick(TObject *Sender);
   void __fastcall BtnNextClick(TObject *Sender);
   void __fastcall BtnEndClick(TObject *Sender);
   void __fastcall FormCreate(TObject *Sender);
   void __fastcall EdtWidthChange(TObject *Sender);
   void __fastcall EdtOffsetChange(TObject *Sender);
   void __fastcall BtnRedrawClick(TObject *Sender);
   void __fastcall BtnLoadSetupClick(TObject *Sender);
   void __fastcall BtnSaveSetupClick(TObject *Sender);
   void __fastcall EdtRangeMaxChange(TObject *Sender);
   void __fastcall EdtRangeMinChange(TObject *Sender);
   void __fastcall EdtSupplyMinChange(TObject *Sender);
   void __fastcall EdtSupplyMaxChange(TObject *Sender);
   void __fastcall EdtSupplyMarkerChange(TObject *Sender);
   void __fastcall BtnSetMinClick(TObject *Sender);
   void __fastcall BtnSetMaxClick(TObject *Sender);
   void __fastcall BtnSetMarkerClick(TObject *Sender);
   void __fastcall CbUseMarkerClick(TObject *Sender);
   void __fastcall CbPlayLoopClick(TObject *Sender);
   void __fastcall BtnPlayAllClick(TObject *Sender);
   void __fastcall BtnPlayWindowClick(TObject *Sender);
   void __fastcall BtnStopClick(TObject *Sender);
   void __fastcall EdtSupplyStepChange(TObject *Sender);
   void __fastcall BtnConnectClick(TObject *Sender);
   void __fastcall EdtSupplySpaceChange(TObject *Sender);
   void __fastcall BtnSetDefaultsClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   void Redraw();
   void UpdateData();
   void EnablePlay( BOOL Enable);

   TWorkplace *Workplace;
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
