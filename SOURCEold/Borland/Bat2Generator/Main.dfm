object MainForm: TMainForm
  Left = 320
  Top = 162
  Width = 1035
  Height = 620
  Caption = 'BAT2 Generator'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 5
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object LblDataName: TLabel
    Left = 48
    Top = 5
    Width = 47
    Height = 13
    Caption = 'NONAME'
  end
  object Label4: TLabel
    Left = 528
    Top = 4
    Width = 34
    Height = 13
    Caption = 'Setup :'
  end
  object LblSetupName: TLabel
    Left = 568
    Top = 5
    Width = 47
    Height = 13
    Caption = 'NONAME'
  end
  object Panel1: TPanel
    Left = 753
    Top = 24
    Width = 270
    Height = 129
    Anchors = [akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 0
    object Label5: TLabel
      Left = 108
      Top = 5
      Width = 27
      Height = 13
      Caption = 'Files'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BtnLoadData: TButton
      Left = 8
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Load Data'
      TabOrder = 0
      OnClick = BtnLoadDataClick
    end
    object BtnLoadSetup: TButton
      Left = 8
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Load Setup'
      TabOrder = 1
      OnClick = BtnLoadSetupClick
    end
    object BtnSaveSetup: TButton
      Left = 88
      Top = 56
      Width = 75
      Height = 25
      Caption = 'Save Setup'
      TabOrder = 2
      OnClick = BtnSaveSetupClick
    end
    object BtnConnect: TButton
      Left = 8
      Top = 88
      Width = 75
      Height = 25
      Caption = 'Connect'
      TabOrder = 3
      OnClick = BtnConnectClick
    end
    object EdtCom: TEdit
      Left = 88
      Top = 89
      Width = 73
      Height = 21
      TabOrder = 4
      Text = 'COM1'
    end
    object BtnSetDefaults: TButton
      Left = 88
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Set Defaults'
      TabOrder = 5
      OnClick = BtnSetDefaultsClick
    end
  end
  object Graph: TChart
    Left = 8
    Top = 24
    Width = 738
    Height = 516
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Maximum = 10
    LeftAxis.Minimum = -10
    View3D = False
    TabOrder = 1
    Anchors = [akLeft, akTop, akRight, akBottom]
    object SamplesSeries: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = 8388863
      ShowInLegend = False
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 549
    Width = 738
    Height = 33
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
    object Label2: TLabel
      Left = 4
      Top = 9
      Width = 34
      Height = 13
      Caption = 'Width :'
    end
    object Label3: TLabel
      Left = 130
      Top = 8
      Width = 34
      Height = 13
      Caption = 'Offset :'
    end
    object EdtWidth: TEdit
      Left = 42
      Top = 5
      Width = 79
      Height = 21
      TabOrder = 0
      Text = '100'
      OnChange = EdtWidthChange
    end
    object BtnBegin: TButton
      Left = 376
      Top = 4
      Width = 75
      Height = 25
      Caption = '|<'
      TabOrder = 1
      OnClick = BtnBeginClick
    end
    object BtnPrev: TButton
      Left = 464
      Top = 4
      Width = 75
      Height = 25
      Caption = '<'
      TabOrder = 2
      OnClick = BtnPrevClick
    end
    object BtnNext: TButton
      Left = 552
      Top = 4
      Width = 75
      Height = 25
      Caption = '>'
      TabOrder = 3
      OnClick = BtnNextClick
    end
    object BtnEnd: TButton
      Left = 640
      Top = 4
      Width = 75
      Height = 25
      Caption = '>|'
      TabOrder = 4
      OnClick = BtnEndClick
    end
    object EdtOffset: TEdit
      Left = 170
      Top = 5
      Width = 87
      Height = 21
      TabOrder = 5
      Text = '0'
      OnChange = EdtOffsetChange
    end
    object BtnRedraw: TButton
      Left = 266
      Top = 4
      Width = 75
      Height = 25
      Caption = 'Redraw'
      TabOrder = 6
      OnClick = BtnRedrawClick
    end
  end
  object Panel3: TPanel
    Left = 753
    Top = 160
    Width = 271
    Height = 129
    Anchors = [akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 3
    object Label6: TLabel
      Left = 80
      Top = 8
      Width = 28
      Height = 13
      Caption = 'Data'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 24
      Width = 23
      Height = 13
      Caption = 'Min :'
    end
    object LblMin: TLabel
      Left = 35
      Top = 25
      Width = 6
      Height = 13
      Caption = '?'
    end
    object LblMax: TLabel
      Left = 35
      Top = 41
      Width = 6
      Height = 13
      Caption = '?'
    end
    object Label9: TLabel
      Left = 8
      Top = 40
      Width = 26
      Height = 13
      Caption = 'Max :'
    end
    object Label8: TLabel
      Left = 8
      Top = 96
      Width = 60
      Height = 13
      Caption = 'Range max :'
    end
    object Label10: TLabel
      Left = 8
      Top = 64
      Width = 57
      Height = 13
      Caption = 'Range min :'
    end
    object Label14: TLabel
      Left = 208
      Top = 64
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label15: TLabel
      Left = 209
      Top = 96
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label24: TLabel
      Left = 104
      Top = 24
      Width = 34
      Height = 13
      Caption = 'Count :'
    end
    object LblCount: TLabel
      Left = 144
      Top = 24
      Width = 6
      Height = 13
      Caption = '?'
    end
    object EdtRangeMax: TEdit
      Left = 80
      Top = 92
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '10'
      OnChange = EdtRangeMaxChange
    end
    object EdtRangeMin: TEdit
      Left = 80
      Top = 60
      Width = 121
      Height = 21
      TabOrder = 1
      Text = '-10'
      OnChange = EdtRangeMinChange
    end
  end
  object Panel4: TPanel
    Left = 753
    Top = 296
    Width = 272
    Height = 285
    Anchors = [akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 4
    object Label11: TLabel
      Left = 88
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Supply'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 8
      Top = 40
      Width = 23
      Height = 13
      Caption = 'Min :'
    end
    object Label13: TLabel
      Left = 179
      Top = 39
      Width = 7
      Height = 13
      Caption = 'V'
    end
    object Label16: TLabel
      Left = 8
      Top = 72
      Width = 26
      Height = 13
      Caption = 'Max :'
    end
    object Label17: TLabel
      Left = 179
      Top = 71
      Width = 7
      Height = 13
      Caption = 'V'
    end
    object Label18: TLabel
      Left = 8
      Top = 104
      Width = 39
      Height = 13
      Caption = 'Marker :'
    end
    object Label19: TLabel
      Left = 179
      Top = 103
      Width = 7
      Height = 13
      Caption = 'V'
    end
    object LblNotConnected: TLabel
      Left = 144
      Top = 9
      Width = 77
      Height = 13
      Caption = 'Not connected !'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object Label20: TLabel
      Left = 8
      Top = 183
      Width = 28
      Height = 13
      Caption = 'Step :'
    end
    object Label21: TLabel
      Left = 179
      Top = 182
      Width = 13
      Height = 13
      Caption = 'ms'
    end
    object Label22: TLabel
      Left = 8
      Top = 151
      Width = 37
      Height = 13
      Caption = 'Space :'
    end
    object Label23: TLabel
      Left = 179
      Top = 150
      Width = 13
      Height = 13
      Caption = 'ms'
    end
    object EdtSupplyMin: TEdit
      Left = 49
      Top = 36
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '1,0000'
      OnChange = EdtSupplyMinChange
    end
    object BtnSetMin: TButton
      Left = 202
      Top = 33
      Width = 63
      Height = 25
      Caption = 'Set'
      TabOrder = 1
      OnClick = BtnSetMinClick
    end
    object EdtSupplyMax: TEdit
      Left = 49
      Top = 68
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '11,0000'
      OnChange = EdtSupplyMaxChange
    end
    object BtnSetMax: TButton
      Left = 202
      Top = 65
      Width = 63
      Height = 25
      Caption = 'Set'
      TabOrder = 3
      OnClick = BtnSetMaxClick
    end
    object EdtSupplyMarker: TEdit
      Left = 49
      Top = 100
      Width = 121
      Height = 21
      TabOrder = 4
      Text = '0,0000'
      OnChange = EdtSupplyMarkerChange
    end
    object BtnSetMarker: TButton
      Left = 202
      Top = 97
      Width = 63
      Height = 25
      Caption = 'Set'
      TabOrder = 5
      OnClick = BtnSetMarkerClick
    end
    object CbUseMarker: TCheckBox
      Left = 8
      Top = 216
      Width = 97
      Height = 17
      Caption = 'Use Marker'
      TabOrder = 6
      OnClick = CbUseMarkerClick
    end
    object BtnPlayAll: TButton
      Left = 8
      Top = 256
      Width = 75
      Height = 25
      Caption = 'Play All'
      TabOrder = 7
      OnClick = BtnPlayAllClick
    end
    object BtnPlayWindow: TButton
      Left = 96
      Top = 256
      Width = 75
      Height = 25
      Caption = 'Play Window'
      TabOrder = 8
      OnClick = BtnPlayWindowClick
    end
    object CbPlayLoop: TCheckBox
      Left = 120
      Top = 216
      Width = 97
      Height = 17
      Caption = 'Play Loop'
      TabOrder = 9
      OnClick = CbPlayLoopClick
    end
    object BtnStop: TButton
      Left = 184
      Top = 256
      Width = 75
      Height = 25
      Caption = 'Stop'
      Enabled = False
      TabOrder = 10
      OnClick = BtnStopClick
    end
    object EdtSupplyStep: TEdit
      Left = 49
      Top = 179
      Width = 121
      Height = 21
      Hint = 'Time delay between samples'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 11
      Text = '100'
      OnChange = EdtSupplyStepChange
    end
    object EdtSupplySpace: TEdit
      Left = 49
      Top = 147
      Width = 121
      Height = 21
      Hint = 'Time delay at start of sampling'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 12
      Text = '1000'
      OnChange = EdtSupplySpaceChange
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'csv'
    Filter = 'CSV data|*.csv'
    Title = 'Open File'
    Left = 944
    Top = 32
  end
  object SetupOpenDialog: TOpenDialog
    DefaultExt = 'stp'
    FileName = 'DEFAULT'
    Filter = 'Setup file|*.stp'
    Title = 'Load Setup'
    Left = 942
    Top = 64
  end
  object SetupSaveDialog: TSaveDialog
    DefaultExt = 'stp'
    FileName = 'DEFAULT'
    Filter = 'Setup file|*.stp'
    Title = 'Save Setup'
    Left = 974
    Top = 64
  end
end
