//******************************************************************************
//
//   Main.cpp     Bat2 generator main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;


//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
   : TForm(Owner)
{
   Workplace = new TWorkplace;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   LblNotConnected->Visible = TRUE;
   UpdateData();
} // FormCreate

//******************************************************************************
// Connect
//******************************************************************************

void __fastcall TMainForm::BtnConnectClick(TObject *Sender)
{
   Screen->Cursor = crHourGlass;
   Workplace->SupplyCom( EdtCom->Text.c_str());  // set COM name
   Workplace->SupplyConnect();                   // try connect
   LblNotConnected->Visible = !Workplace->IsConnected;
   Screen->Cursor = crDefault;
} // BtnConnectClick

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadDataClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;
   if( !Workplace->LoadData( FileOpenDialog->FileName.c_str())){
      Screen->Cursor = crDefault;
      Redraw();
      return;
   }
   Screen->Cursor = crDefault;
   Workplace->Begin();                                      // at first sample
   Workplace->WindowWidth = Workplace->Samples->Count();    // show all samples
   Redraw();
} // BtnLoadClick

//******************************************************************************
// Set defaults
//******************************************************************************

void __fastcall TMainForm::BtnSetDefaultsClick(TObject *Sender)
{
   Workplace->SetDefaults();
   Redraw();
} // BtnSetDefaultsClick

//******************************************************************************
// Load setup
//******************************************************************************

void __fastcall TMainForm::BtnLoadSetupClick(TObject *Sender)
{
   if( !SetupOpenDialog->Execute()){
      return;
   }
   Application->ProcessMessages();
   Screen->Cursor = crHourGlass;
   if( !Workplace->LoadSetup( SetupOpenDialog->FileName.c_str())){
      Screen->Cursor = crDefault;
      Redraw();
      return;
   }
   Screen->Cursor = crDefault;
   Redraw();
} // BtnLoadSetupClick

//******************************************************************************
// Save setup
//******************************************************************************

void __fastcall TMainForm::BtnSaveSetupClick(TObject *Sender)
{
   SetupSaveDialog->FileName = Workplace->SetupFileName;
   if( !SetupSaveDialog->Execute()){
      return;
   }
   if( !Workplace->SaveSetup( SetupSaveDialog->FileName.c_str())){
      Redraw();
      return;
   }
   Redraw();
} // BtnSaveSetupClick

//******************************************************************************
// Redraw
//******************************************************************************

void TMainForm::Redraw()
// Redraw graph
{
   UpdateData();
   TSamplesItem Item;
   SamplesSeries->Clear();
   if( !Workplace->Samples->Seek( Workplace->WindowOffset)){
      return;
   }
   Screen->Cursor = crHourGlass;
   for( int i = 0; i < Workplace->WindowWidth; i++){
      if( !Workplace->Samples->Next( &Item)){
         break;
      }
      SamplesSeries->AddXY( Item.Time,
                            Item.Value,
                            "", clTeeColor);
   }
   Screen->Cursor = crDefault;
   try {
      // disable range checking :
      Graph->LeftAxis->Maximum =  10.0;
      Graph->LeftAxis->Minimum = -10.0;
      // set range :
      Graph->LeftAxis->Maximum = Workplace->Samples->Max();
      Graph->LeftAxis->Minimum = Workplace->Samples->Min();
   } catch( ...){
      Application->MessageBox( "There is something wrong with the fucking graph scales", "Error", MB_OK);
   }
} // Redraw

//******************************************************************************
// Update data window
//******************************************************************************

void TMainForm::UpdateData()
{
   // connected :
   LblNotConnected->Visible = !Workplace->IsConnected;
   // resorce names :
   EdtCom->Text          = Workplace->ComName;
   LblDataName->Caption  = Workplace->DataFileName;
   LblSetupName->Caption = Workplace->SetupFileName;
   // window boundary :
   EdtOffset->Text = AnsiString( Workplace->WindowOffset);
   EdtWidth->Text  = AnsiString( Workplace->WindowWidth);
   // data span :
   LblMin->Caption = AnsiString( Workplace->Samples->Min());
   LblMax->Caption = AnsiString( Workplace->Samples->Max());
   // data range :
   EdtRangeMin->Text = AnsiString( Workplace->RangeMin);
   EdtRangeMax->Text = AnsiString( Workplace->RangeMax);
   // supply data :
   EdtSupplyMin->Text    = AnsiString( Workplace->SupplyMin);
   EdtSupplyMax->Text    = AnsiString( Workplace->SupplyMax);
   EdtSupplyMarker->Text = AnsiString( Workplace->SupplyMarker);
   EdtSupplyStep->Text   = AnsiString( Workplace->SupplyStep);
   EdtSupplySpace->Text  = AnsiString( Workplace->SupplySpace);
   // checkboxes :
   CbUseMarker->Checked  = Workplace->UseMarker;
   CbPlayLoop->Checked   = Workplace->PlayLoop;
} // UpdateData

//******************************************************************************
// Redraw
//******************************************************************************

void __fastcall TMainForm::BtnRedrawClick(TObject *Sender)
{
   Redraw();
} // BtnRedrawClick

//******************************************************************************
// Begin
//******************************************************************************

void __fastcall TMainForm::BtnBeginClick(TObject *Sender)
{
   Workplace->Begin();
   Redraw();
} // BtnBeginClick

//******************************************************************************
// Previous
//******************************************************************************

void __fastcall TMainForm::BtnPrevClick(TObject *Sender)
{
   Workplace->Previous();
   Redraw();
} // BtnPrevClick

//******************************************************************************
// Next
//******************************************************************************

void __fastcall TMainForm::BtnNextClick(TObject *Sender)
{
   Workplace->Next();
   Redraw();
} // BtnNextClick

//******************************************************************************
// End
//******************************************************************************

void __fastcall TMainForm::BtnEndClick(TObject *Sender)
{
   Workplace->End();
   Redraw();
} // BtnEndClick

//******************************************************************************
// Width
//******************************************************************************

void __fastcall TMainForm::EdtWidthChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtWidth->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->WindowWidth = Value;
} // EdtWidthChange

//******************************************************************************
// Offset
//******************************************************************************

void __fastcall TMainForm::EdtOffsetChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtOffset->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->WindowOffset = Value;
} // EdtOffsetChange

//******************************************************************************
// Range
//******************************************************************************

void __fastcall TMainForm::EdtRangeMaxChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtRangeMax->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->RangeMax = Value;
} // EdtRangeMaxChange

void __fastcall TMainForm::EdtRangeMinChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtRangeMin->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->RangeMin = Value;
} // EdtRangeMinChange

//******************************************************************************
// Supply range
//******************************************************************************

void __fastcall TMainForm::EdtSupplyMinChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtSupplyMin->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->SupplyMin = Value;
} // EdtSupplyMinChange

void __fastcall TMainForm::EdtSupplyMaxChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtSupplyMax->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->SupplyMax = Value;
} // EdtSupplyMaxChange


void __fastcall TMainForm::EdtSupplyMarkerChange(TObject *Sender)
{
   double Value;
   try {
      Value = EdtSupplyMarker->Text.ToDouble();
   } catch( ...){
      return;
   }
   Workplace->SupplyMarker = Value;
} // EdtSupplyMarkerChange

void __fastcall TMainForm::EdtSupplySpaceChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtSupplySpace->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->SupplySpace = Value;
} // EdtSupplySpaceChange

void __fastcall TMainForm::EdtSupplyStepChange(TObject *Sender)
{
   int Value;
   try {
      Value = EdtSupplyStep->Text.ToInt();
   } catch( ...){
      return;
   }
   Workplace->SupplyStep = Value;
} // EdtSupplyStepChange

//******************************************************************************
// Set voltage
//******************************************************************************

void __fastcall TMainForm::BtnSetMinClick(TObject *Sender)
{
   Workplace->SupplySet( Workplace->SupplyMin);
} // BtnSetMinClick

void __fastcall TMainForm::BtnSetMaxClick(TObject *Sender)
{
   Workplace->SupplySet( Workplace->SupplyMax);
} // BtnSetMaxClick

void __fastcall TMainForm::BtnSetMarkerClick(TObject *Sender)
{
   Workplace->SupplySet( Workplace->SupplyMarker);
} // BtnSetMarkerClick

//******************************************************************************
// Checkboxes
//******************************************************************************

void __fastcall TMainForm::CbUseMarkerClick(TObject *Sender)
{
   Workplace->UseMarker = CbUseMarker->Checked;
} // CbUseMarkerClick


void __fastcall TMainForm::CbPlayLoopClick(TObject *Sender)
{
   Workplace->PlayLoop  = CbPlayLoop->Checked;
} // CbPlayLoopClick


//******************************************************************************
// Play all
//******************************************************************************

void __fastcall TMainForm::BtnPlayAllClick(TObject *Sender)
{
   EnablePlay( FALSE);
   Screen->Cursor = crHourGlass;
   Workplace->PlayAll();
   Screen->Cursor = crDefault;
   EnablePlay( TRUE);
} // BtnPlayAllClick

//******************************************************************************
// Play window
//******************************************************************************

void __fastcall TMainForm::BtnPlayWindowClick(TObject *Sender)
{
   EnablePlay( FALSE);
   Screen->Cursor = crHourGlass;
   Workplace->PlayWindow();
   Screen->Cursor = crDefault;
   EnablePlay( TRUE);
} // BtnPlayWindowClick

//******************************************************************************
// Stop
//******************************************************************************

void __fastcall TMainForm::BtnStopClick(TObject *Sender)
{
   EnablePlay( TRUE);
   Workplace->Stop();
} // BtnStopClick

//******************************************************************************
// Enable play
//******************************************************************************

void TMainForm::EnablePlay( BOOL Enable)
{
   BtnStop->Enabled       = !Enable;
   BtnPlayAll->Enabled    = Enable;
   BtnPlayWindow->Enabled = Enable;
} // EnablePlay

