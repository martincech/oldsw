//******************************************************************************
//
//   Workplace.h     Bat2 generator workplace
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#ifndef WorkplaceH
#define WorkplaceH

#include "Samples.h"
#include "AgPower.h"

#define PATH_MAX 255

//******************************************************************************
// TWorkplace
//******************************************************************************

class TWorkplace {
public :
   TWorkplace();
   // Constructor

   ~TWorkplace();
   // Destructor

   void SetDefaults();
   // Set default parameters

   BOOL LoadData( char *FileName);
   // Load data from <FileName>

   BOOL LoadSetup( char *FileName);
   // Load setup data

   BOOL SaveSetup( char *FileName);
   // Save setup data

   void Begin();
   // Seek to begin

   void Next();
   // Seek to next

   void Previous();
   // Seek to previous

   void End();
   // Seek to end

   void SupplyCom( char *Com);
   // Set cupply <Com>
 
   BOOL SupplyConnect();
   // Connect power supply

   void SupplySet( double Voltage);
   // Set supply voltage

   void PlayAll();
   // Play all data

   void PlayWindow();
   // Play window data

   void Stop();
   // Stop play

   char      DataFileName[  PATH_MAX + 1];
   char      SetupFileName[ PATH_MAX + 1];
   char      ComName[ 8];

   int       WindowOffset;
   int       WindowWidth;

   double    RangeMin;        // Data range minimum
   double    RangeMax;        // Data range maximum

   double    SupplyMin;
   double    SupplyMax;
   double    SupplyMarker;
   int       SupplySpace;
   int       SupplyStep;

   BOOL      UseMarker;
   BOOL      PlayLoop;

   BOOL      IsConnected;    // connect status

   TSamples *Samples;
//------------------------------------------------------------------------------
protected :
   TAgPower *Supply;
   BOOL      StopPlay;

   BOOL SupplyFlushErrors();
   // Read supply errors

   double SampleToVoltage( double Value);
   // Normalize sample <Value> to voltage

   BOOL PlayRange( int Start, int Count);
   // Play from <Count> samples from <Start> index
}; // TWorkplace

#endif
