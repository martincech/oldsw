//******************************************************************************
//
//   Workplace.cpp   Bat2 generator workplace
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include "../Library/Unisys/Uni.h"
#include "Workplace.h"

#pragma package(smart_init)

#define DEFAULT_WIDTH 100          // default window width
#define DELTA_RANGE   1.0e-6       // minimum samples range

#define IDLE_TIME     500          // wait before start
#define MARKER_TIME   500          // marker duration

#define DEFAULT_CURRENT 0.700      // default current limit
#define NONAME_FILE     "NONAME"
#define DEFAULT_COM     "COM1"

//******************************************************************************
// Constructor
//******************************************************************************

TWorkplace::TWorkplace()
// Constructor
{
   Samples = new TSamples;
   Supply  = new TAgPower;
   SetDefaults();
} // TWorkplace

//******************************************************************************
// Destructor
//******************************************************************************

TWorkplace::~TWorkplace()
// Destructor
{
   if( Samples){
      delete Samples;
   }
   if( Supply){
      delete Supply;
   }
} // TWorkplace

//******************************************************************************
// Defaults
//******************************************************************************

void TWorkplace::SetDefaults()
// Set default parameters
{
   Samples->Clear();

   strcpy( ComName, DEFAULT_COM);
   strcpy( DataFileName,  NONAME_FILE);
   strcpy( SetupFileName, NONAME_FILE);

   WindowOffset = 0;
   WindowWidth  = DEFAULT_WIDTH;

   RangeMin     = 0.0;
   RangeMax     = 10.0;

   SupplyMin    = 1.0;
   SupplyMax    = 11.0;
   SupplyMarker = 0.0;
   SupplyStep   = 100;
   SupplySpace  = 1000;

   UseMarker    = FALSE;
   PlayLoop     = FALSE;

   StopPlay     = FALSE;
   IsConnected  = FALSE;
} // SetDefaults

//******************************************************************************
// Load data
//******************************************************************************

BOOL TWorkplace::LoadData( char *FileName)
// Load data from <FileName>
{
   if( !Samples->Load( FileName)){
      strcpy( DataFileName, NONAME_FILE);
      return( FALSE);
   }
   strncpyx( DataFileName, FileName, PATH_MAX);
   return( TRUE);
} // LoadData

//******************************************************************************
// Load setup
//******************************************************************************

BOOL TWorkplace::LoadSetup( char *FileName)
// Load setup data
{
   FILE *f;
   SetDefaults();
   // open file :
   f = fopen( FileName, "r");
   if( !f){
      return( FALSE);
   }
   strncpyx( SetupFileName, FileName, PATH_MAX);

   fscanf( f, "%s\n",  DataFileName);
   fscanf( f, "%s\n",  ComName);

   fscanf( f, "%d\n",  &WindowOffset);
   fscanf( f, "%d\n",  &WindowWidth);

   fscanf( f, "%lf\n", &RangeMin);
   fscanf( f, "%lf\n", &RangeMax);

   fscanf( f, "%lf\n", &SupplyMin);
   fscanf( f, "%lf\n", &SupplyMax);
   fscanf( f, "%lf\n", &SupplyMarker);
   fscanf( f, "%d\n",  &SupplySpace);
   fscanf( f, "%d\n",  &SupplyStep);

   fscanf( f, "%d\n",  &UseMarker);
   fscanf( f, "%d\n",  &PlayLoop);

   fclose( f);
   if( !LoadData( DataFileName)){
      return( FALSE);
   }
   if( !SupplyConnect()){
      return( FALSE);
   }
   return( TRUE);
} // LoadSetup

//******************************************************************************
// Save setup
//******************************************************************************

BOOL TWorkplace::SaveSetup( char *FileName)
// Save setup data
{
   FILE *f;
   strcpy( SetupFileName, NONAME_FILE);
   // open file :
   f = fopen( FileName, "w");
   if( !f){
      return( FALSE);
   }

   fprintf( f, "%s\n",  DataFileName);
   fprintf( f, "%s\n",  ComName);

   fprintf( f, "%d\n",  WindowOffset);
   fprintf( f, "%d\n",  WindowWidth);

   fprintf( f, "%lf\n", RangeMin);
   fprintf( f, "%lf\n", RangeMax);

   fprintf( f, "%lf\n", SupplyMin);
   fprintf( f, "%lf\n", SupplyMax);
   fprintf( f, "%lf\n", SupplyMarker);
   fprintf( f, "%d\n",  SupplySpace);
   fprintf( f, "%d\n",  SupplyStep);

   fprintf( f, "%d\n",  UseMarker ? 1 : 0);
   fprintf( f, "%d\n",  PlayLoop  ? 1 : 0);

   fclose( f);
   strncpyx( SetupFileName, FileName, PATH_MAX);
   return( TRUE);
} // SaveSetup

//******************************************************************************
// Seek
//******************************************************************************

void TWorkplace::Begin()
// Seek to begin
{
   WindowOffset = 0;
} // Begin

void TWorkplace::Next()
// Seek to next
{
   WindowOffset += WindowWidth;
   if( WindowOffset + WindowWidth > Samples->Count()){
      End();
      return;
   }
} // Next

void TWorkplace::Previous()
// Seek to previous
{
   WindowOffset -= WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
} // Previous

void TWorkplace::End()
// Seek to end
{
   WindowOffset = Samples->Count() - WindowWidth;
   if( WindowOffset < 0){
      WindowOffset = 0;
   }
} // End

//******************************************************************************
// Supply COM
//******************************************************************************

void TWorkplace::SupplyCom( char *Com)
// Set cupply <Com>
{
   strncpyx( ComName, Com, sizeof( ComName) - 1);
} // SupplyCom

//******************************************************************************
// Supply connect
//******************************************************************************

BOOL TWorkplace::SupplyConnect()
// Connect power supply
{
   IsConnected  = FALSE;
   Supply->SetCom( ComName);
   if( !Supply->Control( TRUE)){
      return( FALSE);
   }
   // check for error status :
   Sleep( 100);                        // wait for remote switch status
   if( !SupplyFlushErrors()){
      return( FALSE);
   }
   // probably connected :
   if( !Supply->Display( "CONNECTED")){
      return( FALSE);
   }
   Sleep( 500);
   if( !Supply->ClearDisplay()){
      return( FALSE);
   }
   if( !Supply->EnableOutput( FALSE)){
      return( FALSE);
   }
   if( !Supply->SetCurrent( DEFAULT_CURRENT)){
      return( FALSE);
   }
   IsConnected = TRUE;
   return( TRUE);
} // SupplyConnect

//******************************************************************************
// Supply set
//******************************************************************************

void TWorkplace::SupplySet( double Voltage)
// Set supply voltage
{
   Supply->EnableOutput( TRUE);
   Supply->SetVoltage( Voltage);
} // SupplySet

//******************************************************************************
// Play all
//******************************************************************************

void TWorkplace::PlayAll()
// Play all data
{
   if( !IsConnected){
      return;
   }
   if( !Samples->Count()){
      return;         // no data
   }
   if( fabs( RangeMax - RangeMin) < DELTA_RANGE){
      return;         // invalid range
   }
   StopPlay = FALSE;  // no asynchronous stop
   Supply->SetVoltage( SupplyMin);
   Supply->EnableOutput( TRUE);
   do {
      if( !PlayRange(  0, Samples->Count())){
         break;
      }
   } while( PlayLoop);
   Supply->EnableOutput( FALSE);
   Supply->SetVoltage( SupplyMin);
} // PlayAll

//******************************************************************************
// Play window
//******************************************************************************

void TWorkplace::PlayWindow()
// Play window data
{
   if( !IsConnected){
      return;
   }
   if( !Samples->Count()){
      return;       // no data
   }
   if( !WindowWidth){
      return;       // no window
   }
   if( WindowOffset >= Samples->Count()){
      return;
   }
   if( fabs( RangeMax - RangeMin) < DELTA_RANGE){
      return;         // invalid range
   }
   StopPlay = FALSE;  // no asynchronous stop
   Supply->SetVoltage( SupplyMin);
   Supply->EnableOutput( TRUE);
   do {
      if( !PlayRange( WindowOffset, WindowWidth)){
         break;
      }
   } while( PlayLoop);
   Supply->EnableOutput( FALSE);
   Supply->SetVoltage( SupplyMin);
} // PlayWindow

//******************************************************************************
// Stop play
//******************************************************************************

void TWorkplace::Stop()
// Stop play
{
   StopPlay = TRUE;
} // Stop

//------------------------------------------------------------------------------

//******************************************************************************
// Supply flush errors
//******************************************************************************

BOOL TWorkplace::SupplyFlushErrors()
// Read supply errors
{
   char Message[ AG_MAX_BUFFER + 1];
   forever {
      if( !Supply->GetError( Message)){
         return( FALSE);            // unable get status
      }
      if( strnequ( AG_SUPPLY_OK, Message, strlen( AG_SUPPLY_OK))){
         return( TRUE);
      }
   }
} // SupplyFlushErrors

//******************************************************************************
// Play range
//******************************************************************************

BOOL TWorkplace::PlayRange( int Start, int Count)
// Play from <Count> samples from <Start> index
{
   if( !Samples->Seek( Start)){
      return( FALSE);                  // start out of range
   }
   // wait for start :
   if( SupplySpace){
      Supply->SetVoltage( SupplyMin);  // set min voltage
      Sleep( SupplySpace);             // wait for space
   }
   // show marker
   if( UseMarker){
      Supply->SetVoltage( SupplyMarker);
      Sleep( MARKER_TIME);
   }
   TSamplesItem Item;
   double       Voltage;
   for( int i = 0; i < Count; i++){
      if( !Samples->Next( &Item)){
         break;
      }
      Voltage = SampleToVoltage( Item.Value);
      Supply->SetVoltage( Voltage);
      Sleep( SupplyStep);
      Application->ProcessMessages();  // windows message loop
      if( StopPlay){
          break;                       // asynchronous stop
      }
   }
   if( UseMarker){
      Supply->SetVoltage( SupplyMarker);
      Sleep( MARKER_TIME);
   }
   return( !StopPlay);                // returns true if not stopped
} // PlayRange

//******************************************************************************
// Sample to voltage
//******************************************************************************

double TWorkplace::SampleToVoltage( double Value)
// Normalize sample <Value> to voltage
{
   // sample saturation :
   if( Value < RangeMin){
      Value = RangeMin;
   }
   if( Value > RangeMax){
      Value = RangeMax;
   }
   double Voltage;
   Value   -= RangeMin;
   Voltage  = Value * (SupplyMax - SupplyMin) / (RangeMax - RangeMin);
   Voltage += SupplyMin;
   return( Voltage);
} // SampleToVoltage
