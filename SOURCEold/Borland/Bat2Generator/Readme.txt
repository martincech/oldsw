   BAT2 Generator
   
Vstupni soubor je tvoren cisly v pohyblive radove carce.
Kazde cislo na samostatnem radku. Je pouzita desetinna tecka.

Ovladaci prvky :

Panel File :

Load Data - nacte vybrany soubor vzorku
Set Defaults - smaze vsechna data, nastavi implicitni hodnoty
Load Setup - nacte soubor setup - vsechny ulozene parametry
             a vzorky ze soubor vzorku
Save Setup - aktualni nastaveni programu ulozi do setup souboru
             (vsechny parametry, vc. nazvu souboru vzorku)
Connect - navazani spojeni se zdrojem pres RS232

Panel Data :

Min - minimum nactenych vzorku
Max - maximum nactenych vzorku
Count - pocet vzorku
Range min - hodnota vzorku, ktera se transformuje na napeti Supply/Min
Range max - hodnota vzorku, ktera se transformuje na napeti Supply/Max

Panel Supply :

Pokud se zobrazuje cerveny napis 'Not connected',
spojeni se zdrojem neni navazano. Stisknete File/Connect.

Min - minimalni simulovane napeti (odpovida vzorku s hodnotou Range min)
Max - maximalni simulovane napeti (odpovida vzorku s hodnotou Range max)
Marker - napeti, ktere se objevi po dobu 0.5s pred startem prehravani
         a 0.5s po ukonceni prehravani
Space - doba, po kterou je po startu smycky prehravani udrzovano
        napeti na Min (za timto nasleduje marker a vzorky)
Step - delka jednoho vzorku

Use Marker - je-li zaskrtnuto, generuje se marker na zacatku a konci smycky
Play Loop - je-li zaskrtnuto, data se prehravaji v nekonecne smycce
            neni-li zaskrtnuto data se prehraji jednorazove

Play All - prehravaji se vsechny vzorky od 0..Count
Play Window - prehravaji se jen vzorky zobrazene v grafu
Stop - zastaveni prehravani (na toto tlacitko je treba kliknout,
       trebaze kurzor jsou presypaci hodiny)

Dolni lista pod grafem :

Width - sirka okna grafu v poctu vzorku
Offset - levy okraj grafu (cislo vzorku)

Redraw - prekresli graf podle aktualniho nastaveni
|< - slouzi k posunu okna na prvni vzorek
<  - o okno zpatky
>  - o okno dopredu
>| - na posledni okno

-------------------------------------------------------------------------------
Priklad prehravani :

- je zaskrtnuto Use Marker 
- je zaskrtnuto Play Loop
- je zobrazeno okno o sirce 5 vzorku
Stiskneme Play Window :

1. <napeti Min> <cekani Space> <napeti Marker> <cekani 0.5s>
2. <vzorek 0> <cekani Step>
...
6. <vzorek 4> <cekani Step>
7. <napeti Marker> <cekani 0.5s>
8. zpatky na bod 1. az do stisknuti Stop

Generovani obdelnikoveho prubehu :

Data obsahuji napr . :
0.0000
10.0000

- je zaskrtnuto Play Loop
- Space je 0
- Step je polovina periody napr. 1000 (1s)

Stiskneme Play Window :
1. <napeti Min> <cekani Step> 
2. <napeti Max> <cekani Step>
3. znovu na bod 1. az do stisknuti Stop

tj. generuje obdelnikovy prubeh s periodou 2s
