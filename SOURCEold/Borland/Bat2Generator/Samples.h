//******************************************************************************
//
//   Samples.cpp  Samples database
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef SamplesH
#define SamplesH

#include <stdio.h>

typedef struct {
   int    Time;
   double Value;
} TSamplesItem;

//******************************************************************************
// TSamples
//******************************************************************************

class TSamples {
public :
   TSamples();
   // Constructor

   ~TSamples();
   // Destructor

   BOOL Load( char *Name);
   // Load from file <Name>

   BOOL Seek( int Offset);
   // Seek to <Offset>

   int Count();
   // Returns samples count

   double Min();
   // Returns samples minimum

   double Max();
   // Returns samples maximum

   BOOL Next( TSamplesItem *Item);
   // Returns next sample <Item>

   void Clear();
   // Clear all samples

//------------------------------------------------------------------------------
protected :
   TSamplesItem *List;              // array of items
   int           Size;              // size of array
   int           ItemsCount;        // items count of array
   int           Index;             // actual index

   void FreeList();
   // Free list memory
   BOOL ReadLine( FILE *f, double *Value);
   // Read file line
}; // TSamples

#endif
