//******************************************************************************
//
//   Samples.cpp  Samples database
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Samples.h"
#include <stdlib.h>
#include "../Library/Unisys/Uni.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//#define SA_PAGE_COUNT  1024                                    // items per allocation page
#define SA_PAGE_COUNT  1                                    // items per allocation page
#define PageBytes( Size) ((Size) * sizeof( TSamplesItem))      // bytes per <Size> items
#define SA_PAGE_SIZE     PageBytes(SA_PAGE_COUNT)              // bytes per allocation page

#define LINE_SIZE  160

//******************************************************************************
// Constructor
//******************************************************************************

TSamples::TSamples()
// Constructor
{
   Size       = 0;
   ItemsCount = 0;
   Index      = 0;
   List       = 0;
} // TSamples

//******************************************************************************
// Destructor
//******************************************************************************

TSamples::~TSamples()
// Destructor
{
   FreeList();
} // ~TSamples

//******************************************************************************
// Load
//******************************************************************************

BOOL TSamples::Load( char *Name)
// Load from file <Name>
{
FILE *f;

   // open file :
   f = fopen( Name, "r");
   if( !f){
      return( FALSE);
   }
   // allocate list :
   if( List){
      free( List);
   }
   List  = (TSamplesItem *)calloc( 1, SA_PAGE_SIZE);
   Size  = SA_PAGE_COUNT;
   if( !List){
      return( FALSE);
   }
   // read items
   double Value;
   ItemsCount = 0;
   forever {
      if( !ReadLine( f, &Value)){
         break;
      }
      // check for space :
      if( ItemsCount >= Size){
         Size += SA_PAGE_COUNT;
         List = (TSamplesItem *)realloc( List, PageBytes( Size));
         if( !List){
            fclose( f);
            FreeList();
            return( FALSE);
         }
      }
      List[ ItemsCount].Time  = ItemsCount;
      List[ ItemsCount].Value = Value;
      ItemsCount++;
   }
   fclose( f);
   return( TRUE);   
} // Load

//******************************************************************************
// Seek
//******************************************************************************

BOOL TSamples::Seek( int Offset)
// Seek to <Offset>
{
   if( Offset >= ItemsCount){
      return( FALSE);
   }
   if( Offset < 0){
      return( FALSE);
   }
   Index = Offset;
   return( TRUE);
} // Seek

//******************************************************************************
// Count
//******************************************************************************

int TSamples::Count()
// Returns samples count
{
   return( ItemsCount);
} // Count

//******************************************************************************
// Minimum
//******************************************************************************

double TSamples::Min()
// Returns samples minimum
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   double Min = List[ 0].Value;
   for( int i = 1; i < ItemsCount; i++){
      if( List[ i].Value < Min){
         Min = List[ i].Value;
      }
   }
   return( Min);
} // Min

//******************************************************************************
// Maximum
//******************************************************************************

double TSamples::Max()
// Returns samples maximum
{
   if( ItemsCount <= 0){
      return( 0.0);
   }
   double Max = List[ 0].Value;
   for( int i = 1; i < ItemsCount; i++){
      if( List[ i].Value > Max){
         Max = List[ i].Value;
      }
   }
   return( Max);
} // Max

//******************************************************************************
// Next
//******************************************************************************

BOOL TSamples::Next( TSamplesItem *Item)
// Returns next sample <Item>
{
   if( Index >= ItemsCount){
      return( FALSE);
   }
   *Item = List[ Index];
   Index++;
   return( TRUE);
} // Next

//******************************************************************************
// Clear
//******************************************************************************

void TSamples::Clear()
// Clear all samples
{
   FreeList();
} // Clear

//******************************************************************************
// Free list
//******************************************************************************

void TSamples::FreeList()
// Free list memory
{
   if( List){
      free( List);
   }
   Size       = 0;
   List       = 0;
   ItemsCount = 0;
} // FreeList

//******************************************************************************
// Read line
//******************************************************************************

BOOL TSamples::ReadLine( FILE *f, double *Value)
// Read file line
{
char Buffer[ LINE_SIZE + 1];

   if( !fgets( Buffer, LINE_SIZE, f)){
      return( FALSE);
   }
   sscanf( Buffer, "%lf", Value);
   return( TRUE); 
} // ReadLine

