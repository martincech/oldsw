//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat2Generator.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Samples.cpp");
USEUNIT("Workplace.cpp");
USEUNIT("..\Agilent\AgPower.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
   try
   {
       Application->Initialize();
       Application->Title = "BAT2 Generator";
       Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
   }
   catch (Exception &exception)
   {
       Application->ShowException(&exception);
   }
   return 0;
}
//---------------------------------------------------------------------------
