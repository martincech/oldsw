//******************************************************************************
//
//   MainIsp.h    ISP test main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainIspH
#define MainIspH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Library/Serial/USB/UsbUart.h"

//******************************************************************************
// Main Form
//******************************************************************************

class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TButton *BtnTest;
        TLabel *LblStatus;
        TButton *BtnEnter;
        TButton *BtnEcho;
        void __fastcall BtnTestClick(TObject *Sender);
        void __fastcall BtnEnterClick(TObject *Sender);
        void __fastcall BtnEchoClick(TObject *Sender);
private:	// User declarations
   TUsbUart *Usb;
public:		// User declarations
   __fastcall TMainForm(TComponent* Owner);

   bool Connect();
   // Connect USB device
   void Disconnect();
   // Disconnect USB device
   bool ReceiveFixed( char *Reply);
   // Receive and compare with <Reply>
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
