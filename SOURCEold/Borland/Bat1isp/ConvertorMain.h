//******************************************************************************
//
//   ConvertorMain.h   Hex convertor main module
//   Version 0.0       (c) VymOs
//
//******************************************************************************

#ifndef ConvertorMainH
#define ConvertorMainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "CodeFile.h"
#include "../ArmPgm/HexFile.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TConvertorMainForm : public TForm
{
__published:	// IDE-managed Components
        TButton *LoadHex;
        TButton *Save;
        TEdit *EditDescription;
        TEdit *EditVersion;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *LblStartAddress;
        TLabel *Label4;
        TLabel *LblLength;
        TOpenDialog *FileOpenDialog;
        TSaveDialog *FileSaveDialog;
        TLabel *Label5;
        TLabel *LblStatus;
        void __fastcall LoadHexClick(TObject *Sender);
        void __fastcall SaveClick(TObject *Sender);
private:	// User declarations
   TCodeFile *CodeFile;
   THexFile  *HexFile;
public:		// User declarations
   __fastcall TConvertorMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TConvertorMainForm *ConvertorMainForm;
//---------------------------------------------------------------------------
#endif
