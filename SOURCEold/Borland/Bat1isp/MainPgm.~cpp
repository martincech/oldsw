//******************************************************************************
//
//   MainPgm.cpp  Bat1 programmer main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainPgm.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

// USB device name :
#define USB_DEVICE_NAME   "VEIT BAT1 Poultry Scale"
#define USB_DEVICE_NAME1  "FlexScale"

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";
#define StatusErr()  Status() + "ERROR";

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Lpc      = new TLpcFlash;
   CodeFile = new TCodeFile;
   Lpc->BaudRate         = 57600;
   Lpc->CrystalFrequency = 18432000;
   strcpy( Lpc->DeviceName, USB_DEVICE_NAME);
   Lpc->ViaUsb = true;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Lpc->ProgressFunction       = this->Progress;
   Lpc->EnableFlashComparation = true;
} // FormCreate

//******************************************************************************
// Progress
//******************************************************************************

void TMainForm::Progress( int Percent)
// Update progress bar
{
   ProgressBar->Position = Percent / 10;
   AnsiString String;
   String.printf( "%d %%", Percent / 10);
   LblPercent->Caption = String;
   Application->ProcessMessages();
   Screen->Cursor      = crHourGlass;// wait cursor
} // Progress

//******************************************************************************
// Communication start
//******************************************************************************

void TMainForm::StartComm()
// Communication start
{
   // show progress bar :
   ProgressBar->Visible = true;
   LblPercent->Visible  = true;
   LblPhase->Visible    = true;
   Progress( 0);
   this->Enabled        = false;      // disable main window
   Application->ProcessMessages();
   Screen->Cursor       = crHourGlass;// wait cursor
} // StartCom

//******************************************************************************
// Communication stop
//******************************************************************************

void TMainForm::StopComm()
// Communication stop
{
   Progress( 0);
   ProgressBar->Visible  = false;
   LblPercent->Visible   = false;
   LblPhase->Visible     = false;
   this->Enabled         = true;       // enable main window
   Screen->Cursor        = crDefault;  // normal cursor
} // StopComm

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TMainForm::BtnLoadClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   if( !FileExists( FileOpenDialog->FileName)){
      Status() + "ERROR File doesn't exists";
      return;
   }
   if( !CodeFile->Load( FileOpenDialog->FileName.c_str())){
      Status() + "ERROR Corrupted file";
      return;
   }
   BtnProgramming->Enabled = true;
   // show statistics :
   LblDescription->Caption = CodeFile->Signature.Description;
   AnsiString String;
   String.printf( "%X.%02X.%1X", (CodeFile->Signature.Version >> 16) & 0xFF,
                                 (CodeFile->Signature.Version >> 8)  & 0xFF,
                                  CodeFile->Signature.Version        & 0xFF);
   LblVersion->Caption  = String;
   LblCodeSize->Caption = AnsiString( CodeFile->Signature.Size);
   LblFileName->Caption = FileOpenDialog->FileName;
   StatusOk();
} // BtnLoadClick

//******************************************************************************
// Write
//******************************************************************************

void __fastcall TMainForm::BtnProgrammingClick(TObject *Sender)
{
   LblPhase->Caption = "Check device :";
   Status() + "?";
   StartComm();
   // check for default USB name :
   strcpy( Lpc->DeviceName, USB_DEVICE_NAME);
   if( !Lpc->GetDeviceInfo()){
      strcpy( Lpc->DeviceName, USB_DEVICE_NAME1);
      if( !Lpc->GetDeviceInfo()){
         StopComm();
         StatusErr();
         return;
      }
   }
   LblPhase->Caption = "Write :";
   if( !Lpc->WriteFlash( 0, CodeFile->Code, CodeFile->Signature.Size)){
      StopComm();
      StatusErr();
      return;
   }
   LblPhase->Caption = "Verify :";
   if( !Lpc->CompareFlash( 0, CodeFile->Code, CodeFile->Signature.Size)){
      StopComm();
      StatusErr();
      return;
   }
   if( !Lpc->Reset()){
      StopComm();
      StatusErr();
      return;
   }
   StopComm();
   StatusOk();
} // BtnWriteClick

