//******************************************************************************
//
//   CodeFile.cpp  Processor code file
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "CodeFile.h"
#include <stdio.h>
#include <stddef.h>

#pragma package(smart_init)

//******************************************************************************
// Constructor
//******************************************************************************

TCodeFile::TCodeFile()
// Constructor
{
   memset( &Signature, 0, sizeof( Signature));
   Code = new byte[ CODEFILE_MAX_SIZE];
} // TCodeFile

//******************************************************************************
// Destructor
//******************************************************************************

TCodeFile::~TCodeFile()
// Destructor
{
   if( Code){
      delete[] Code;
   }
} // TCodeFile


//******************************************************************************
// Load
//******************************************************************************

bool TCodeFile::Load( char *FileName)
// Load file
{
   FILE *f;
   f = fopen( FileName, "rb");
   if( !f){
      return( false);
   }
   // read and check signature :
   fread( &Signature, sizeof( Signature), 1, f);
   if( CalcCheckSum() != Signature.CheckSum){
      fclose( f);
      return( false);
   }
   // read and check code :
   memset( Code, 0xFF, CODEFILE_MAX_SIZE);       // default code
   fread( Code, Signature.Size, 1, f);           // read code
   Decrypt();                                    // decrypt code file
   if( CalcCrc() != Signature.Crc){
      fclose( f);
      return( false);
   }
   fclose( f);
   return( true);
} // Load

//******************************************************************************
// Save
//******************************************************************************

void TCodeFile::Save( char *FileName)
// Save file
{
   Signature.Crc      = CalcCrc();              // code CRC
   Signature.CheckSum = CalcCheckSum();         // signature CRC
   Encrypt();                                   // encrypt code file
   FILE *f;
   f = fopen( FileName, "wb");
   fwrite( &Signature, sizeof( Signature), 1, f);
   fwrite( Code, Signature.Size, 1, f);
   fclose( f);
} // Save

//------------------------------------------------------------------------------

//******************************************************************************
// Signature CRC
//******************************************************************************

#define SIGNATURE_DATA_SIZE    (int)offsetof( TSignature, CheckSum)

byte TCodeFile::CalcCheckSum()
// Signature CRC
{
   byte *p = (byte *)&Signature;
   byte CheckSum = 0;
   for( int i = 0; i < SIGNATURE_DATA_SIZE; i++){
      CheckSum += p[ i];
   }
   return( CheckSum);
} // CalcCheckSum

//******************************************************************************
// Code CRC
//******************************************************************************

word TCodeFile::CalcCrc()
// Code CRC
{
   word Crc = 0;
   for( unsigned i = 0; i < Signature.Size; i++){
      Crc += Code[ i];
   }
   return( Crc);
} // CalcCrc

//******************************************************************************
// Encrypt
//******************************************************************************

#define ENCRYPT_KEY  0xA5

void TCodeFile::Encrypt()
// Encrypt code
{
   for( unsigned i = 0; i < Signature.Size; i++){
      Code[ i] ^= ENCRYPT_KEY;
   }
} // Encrypt

//******************************************************************************
// Decrypt
//******************************************************************************

void TCodeFile::Decrypt()
// Decrypt code
{
   for( unsigned i = 0; i < Signature.Size; i++){
      Code[ i] ^= ENCRYPT_KEY;
   }
} // Decrypt

