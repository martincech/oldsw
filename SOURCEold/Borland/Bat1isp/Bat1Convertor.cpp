//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Convertor.res");
USEFORM("ConvertorMain.cpp", ConvertorMainForm);
USEUNIT("CodeFile.cpp");
USEUNIT("..\ArmPgm\HexFile.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 Convertor";
                 Application->CreateForm(__classid(TConvertorMainForm), &ConvertorMainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
