//******************************************************************************
//
//   Main.h        Main header
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainPgmH
#define MainPgmH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

#include "../Library/Crt/Crt.h"
#include "LpcFlash.h"
#include "CodeFile.h"

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar;
        TButton *BtnProgramming;
        TProgressBar *ProgressBar;
        TLabel *LblPercent;
        TButton *BtnLoad;
        TOpenDialog *FileOpenDialog;
        TLabel *LblPhase;
        TLabel *Label2;
        TLabel *LblVersion;
        TLabel *Label3;
        TLabel *LblCodeSize;
        TLabel *Label4;
        TLabel *LblFileName;
        TLabel *Label5;
        TLabel *LblDescription;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnProgrammingClick(TObject *Sender);
        void __fastcall BtnLoadClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TLpcFlash  *Lpc;
   TCodeFile  *CodeFile;

   __fastcall TMainForm(TComponent* Owner);
   void Progress( int Percent);
   void StartComm();
   void StopComm();
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
