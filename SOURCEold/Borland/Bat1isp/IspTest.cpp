//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("IspTest.res");
USEFORM("MainIsp.cpp", MainForm);
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USELIB("..\Library\Serial\USB\FTDI.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
