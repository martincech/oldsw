//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Pgm.res");
USEFORM("MainPgm.cpp", MainForm);
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USEUNIT("..\ArmPgm\LpcIsp.cpp");
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\ArmPgm\LpcFlash.cpp");
USEUNIT("CodeFile.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 Programmer";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
