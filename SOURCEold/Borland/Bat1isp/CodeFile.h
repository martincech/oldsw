//******************************************************************************
//
//   CodeFile.h    Processor code file
//   Version 0.0   (c) VymOs
//
//******************************************************************************

#ifndef CodeFileH
#define CodeFileH

#include "../Library/Unisys/Uni.h"

#define CODEFILE_MAX_SIZE         (512 * 1024)      // max. code size
#define CODEFILE_MAX_DESCRIPTION  63                // max. description

//******************************************************************************
// TCodeFile
//******************************************************************************

class TCodeFile {
public :
   // file signature :
   typedef struct {
      char     Description[ CODEFILE_MAX_DESCRIPTION + 1]; // code description
      unsigned Version;                                // version number 0x010000 = 1.00.0
      unsigned MinAddress;                             // min. address
      unsigned StartAddress;                           // start address
      unsigned Size;                                   // code size
      word     Crc;                                    // code CRC
      byte     CheckSum;                               // signature CRC
   } TSignature;

   TCodeFile();
   // Constructor
   ~TCodeFile();
   // Destructor
   bool Load( char *FileName);
   // Load file
   void Save( char *FileName);
   // Save file

// properties
   TSignature Signature;
   byte      *Code;                     // code data
//------------------------------------------------------------------------------
protected :
   byte CalcCheckSum();
   // Signature CRC
   word CalcCrc();
   // Code CRC
   void Encrypt();
   // Encrypt code
   void Decrypt();
   // Decrypt code
}; // TCodeFile
//------------------------------------------------------------------------------

#endif

