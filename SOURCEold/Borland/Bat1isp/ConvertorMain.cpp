//******************************************************************************
//
//   ConvertorMain.cpp  Hex convertor main module
//   Version 0.0       (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "ConvertorMain.h"
#pragma package(smart_init)
#pragma resource "*.dfm"

TConvertorMainForm *ConvertorMainForm;

//******************************************************************************
// Konstruktor
//******************************************************************************

__fastcall TConvertorMainForm::TConvertorMainForm(TComponent* Owner)
        : TForm(Owner)
{
   CodeFile = new TCodeFile;
   HexFile  = new THexFile( CODEFILE_MAX_SIZE);
} // TConvertorMainForm

//******************************************************************************
// Load
//******************************************************************************

void __fastcall TConvertorMainForm::LoadHexClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   if( !FileExists( FileOpenDialog->FileName)){
      LblStatus->Caption = "ERR : File doesn't exists";
      return;
   }
   if( !HexFile->Load( FileOpenDialog->FileName.c_str())){
      LblStatus->Caption = "ERR : Unable open/decode file";
      return;
   }
   // copy to code file :
   memset( CodeFile->Code, 0xFF, CODEFILE_MAX_SIZE);
   memcpy( CodeFile->Code, HexFile->Code, HexFile->CodeSize);
   CodeFile->Signature.MinAddress   = HexFile->CodeMinAddress;
   CodeFile->Signature.StartAddress = HexFile->CodeStartAddress;
   CodeFile->Signature.Size         = HexFile->CodeSize;
   // show statistics :
   LblStartAddress->Caption = "0x" + IntToHex( (int)CodeFile->Signature.MinAddress, 8);
   LblLength->Caption       = AnsiString( CodeFile->Signature.Size);
   LblStatus->Caption       = "Load OK";
} // LoadHexClick

//******************************************************************************
// Save
//******************************************************************************

void __fastcall TConvertorMainForm::SaveClick(TObject *Sender)
{
   // check description :
   EditDescription->Text.Trim();
   if( EditDescription->Text.Length() > CODEFILE_MAX_DESCRIPTION){
      LblStatus->Caption       = "ERR : Description too long";
      return;
   }
   if( EditDescription->Text.Length() == 0){
      LblStatus->Caption       = "ERR : Empty description";
      return;
   }
   // check version number :
   char Buf[ 20];
   strcpy( Buf, EditVersion->Text.c_str());
   int Version, SubVersion, Build;
   sscanf( Buf, "%d.%d.%d", &Version, &SubVersion, &Build);
   Version    &= 0xFF;
   SubVersion &= 0xFF;
   Build      &= 0xFF;
   if( !Version && !SubVersion && !Build){
      LblStatus->Caption       = "ERR : Invalid version number";
      return;
   }
   // save file :
   FileSaveDialog->FileName = ChangeFileExt( FileOpenDialog->FileName, ".bfw" );
   if( !FileSaveDialog->Execute()){
      return;
   }
   if( FileExists( FileSaveDialog->FileName)){
      if( Application->MessageBox( "File already exists,\nIt will be owerwritten",
          "Confirmation", MB_OKCANCEL) != IDOK){
          LblStatus->Caption       = "File save canceled";
          return;
      }
   }
   strcpy( CodeFile->Signature.Description, EditDescription->Text.c_str());
   CodeFile->Signature.Version = (Version  << 16) | (SubVersion << 8) | Build;
   CodeFile->Save( FileSaveDialog->FileName.c_str());
   LblStatus->Caption       = "Save OK";
} // SaveClick

