//******************************************************************************
//
//   MainIsp.cpp   ISP test main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainIsp.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TMainForm *MainForm;

#define BAT1_DEVICE_NAME   "VEIT BAT1 Poultry Scale"

#define SYNCHRONIZED_CMD   "Synchronized\r\n"
#define CRYSTAL_FREQUENCY  "18432\r\n"
#define ECHO_DATA          "DEMO"
#define RX_TIMEOUT         2000

#define Status( msg)     LblStatus->Caption = (AnsiString)"Status : " + msg
#define StatusOk()       Status( "OK")

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Usb = 0;
} // TMainForm

//******************************************************************************
// Test
//******************************************************************************

void __fastcall TMainForm::BtnTestClick(TObject *Sender)
{
   // open USB device :
   if( !Connect()){
      return;
   }
   // common init :
   Usb->SetRxNowait();
   Usb->Flush();
   // synchronization test :
   Screen->Cursor = crHourGlass;       // wait cursor
   int  Length;
   byte Question = '?';
   // send question mark :
   if( Usb->Write( &Question, 1) != 1){
      Status( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      Screen->Cursor = crDefault;      // normal cursor
      return;
   }
   if( !ReceiveFixed( SYNCHRONIZED_CMD)){
      StatusOk();
      Screen->Cursor = crDefault;      // normal cursor
      return;
   }
   Usb->Flush();
   Length = strlen( SYNCHRONIZED_CMD);
   if( Usb->Write( SYNCHRONIZED_CMD, Length) != Length){
      Status( "Unable send confirmation");
      Disconnect();                    // something is wrong, try reopen
      Screen->Cursor = crDefault;      // normal cursor
      return;
   }
   Length = strlen( CRYSTAL_FREQUENCY);
   if( Usb->Write( CRYSTAL_FREQUENCY, Length) != Length){
      Status( "Unable send frequency");
      Disconnect();                    // something is wrong, try reopen
      Screen->Cursor = crDefault;      // normal cursor
      return;
   }
   Status( "ISP mode !");
   Screen->Cursor = crDefault;         // normal cursor
} // BtnTestClick

//******************************************************************************
// Enter ISP
//******************************************************************************

void __fastcall TMainForm::BtnEnterClick(TObject *Sender)
{
   if( !Connect()){
      return;
   }
   Usb->DTR = true;                   // reset
   Usb->RTS = true;                   // P0.14
   Sleep( 100);
   Usb->DTR = false;                  // release reset
   Sleep( 100);
   Usb->RTS = false;                  // release P0.14
} // BtnEnterClick

//******************************************************************************
// Echo
//******************************************************************************

void __fastcall TMainForm::BtnEchoClick(TObject *Sender)
{
   // open USB device :
   if( !Connect()){
      return;
   }
   // common init :
   Usb->SetRxNowait();
   Usb->Flush();
   // synchronization test :
   Screen->Cursor = crHourGlass;       // wait cursor
   int  Length;
   Length = strlen( ECHO_DATA);
   // send question mark :
   if( Usb->Write( ECHO_DATA, Length) != Length){
      Status( "Unable send");
      Disconnect();                    // something is wrong, try reopen
      Screen->Cursor = crDefault;      // normal cursor
      return;
   }
   if( !ReceiveFixed( ECHO_DATA)){
      StatusOk();
      Screen->Cursor = crDefault;      // normal cursor
      return;
   }
   Status( "ECHO received !");
   Screen->Cursor = crDefault;         // normal cursor
} // BtmEchoClick

//******************************************************************************
// Connect
//******************************************************************************

bool TMainForm::Connect()
// Connect USB device
{
   if( Usb){
      return( true);
   }
   Usb = new TUsbUart;
   TIdentifier Identifier;
   if( !Usb->Locate( BAT1_DEVICE_NAME, Identifier)){
      Status( "Unable locate USB device");
      delete Usb;
      return( false);
   }
   if( !Usb->Open( Identifier)){
      Status( "Unable open USB device");
      delete Usb;
      return( false);
   }
   TUart::TParameters Parameters;
   Parameters.BaudRate     = 9600;
   Parameters.DataBits     = 8;
   Parameters.StopBits     = 10;
   Parameters.Parity       = TUart::NO_PARITY;
   Parameters.Handshake    = TUart::NO_HANDSHAKE;
   Usb->SetParameters( Parameters);
   return( YES);
} // Connect

//******************************************************************************
// Disconnect
//******************************************************************************

void TMainForm::Disconnect()
// Disconnect USB device
{
   if( Usb){
      delete Usb;
      Usb = 0;
   }
} // Disconnect

//******************************************************************************
// Receive
//******************************************************************************

bool TMainForm::ReceiveFixed( char *Reply)
// Receive and compare with <Reply>
{
   byte RxBuffer[ 128];
   int Length = strlen( Reply);
   Usb->SetRxWait( RX_TIMEOUT, 0);
   int RxSize = Usb->Read( RxBuffer, Length);
   if( RxSize != Length){
      return( false);
   }
   RxBuffer[ RxSize] = '\0';
   if( !strequ( Reply, RxBuffer)){
      return( false);
   }
   return( true);
} // ReceiveFixed

