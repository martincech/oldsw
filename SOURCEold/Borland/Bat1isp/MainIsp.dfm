object MainForm: TMainForm
  Left = 635
  Top = 303
  Width = 343
  Height = 190
  Caption = 'ISP test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LblStatus: TLabel
    Left = 24
    Top = 96
    Width = 45
    Height = 13
    Caption = 'Status : ?'
  end
  object BtnTest: TButton
    Left = 24
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Test'
    TabOrder = 0
    OnClick = BtnTestClick
  end
  object BtnEnter: TButton
    Left = 240
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Enter ISP'
    TabOrder = 1
    OnClick = BtnEnterClick
  end
  object BtnEcho: TButton
    Left = 24
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Test Echo'
    TabOrder = 2
    OnClick = BtnEchoClick
  end
end
