object PropertiesForm: TPropertiesForm
  Left = 376
  Top = 253
  ActiveControl = PropertiesCancel
  BorderStyle = bsDialog
  Caption = 'Tester Properties'
  ClientHeight = 276
  ClientWidth = 427
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 152
    Top = 96
    Width = 57
    Height = 13
    Caption = 'RxTimeout :'
  end
  object Label5: TLabel
    Left = 160
    Top = 144
    Width = 57
    Height = 13
    Caption = 'RxTimeout :'
  end
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 409
    Height = 193
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 32
      Width = 25
      Height = 13
      Caption = 'Port :'
    end
    object Label2: TLabel
      Left = 144
      Top = 96
      Width = 57
      Height = 13
      Caption = 'RxTimeout :'
    end
    object Label3: TLabel
      Left = 312
      Top = 96
      Width = 13
      Height = 13
      Caption = 'ms'
    end
    object Label6: TLabel
      Left = 144
      Top = 128
      Width = 52
      Height = 13
      Caption = 'Data Size :'
    end
    object Label7: TLabel
      Left = 312
      Top = 128
      Width = 25
      Height = 13
      Caption = 'bytes'
    end
    object Label8: TLabel
      Left = 144
      Top = 160
      Width = 65
      Height = 13
      Caption = 'USB Device :'
    end
    object PropertiesPort: TComboBox
      Left = 72
      Top = 24
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'USB'
        'COM1'
        'COM2'
        'COM3'
        'COM4'
        'COM5'
        'COM6'
        'COM7'
        'COM8'
        'COM9'
        'COM10')
    end
    object PropertiesDisplay: TRadioGroup
      Left = 24
      Top = 88
      Width = 97
      Height = 89
      Caption = 'Display Mode'
      Items.Strings = (
        'Binary'
        'Text'
        'Both')
      TabOrder = 1
    end
    object PropertiesPortSettings: TButton
      Left = 264
      Top = 24
      Width = 105
      Height = 25
      Caption = 'Settings...'
      TabOrder = 2
      OnClick = PortSettingsClick
    end
    object PropertiesRxTimeout: TEdit
      Left = 216
      Top = 88
      Width = 89
      Height = 21
      TabOrder = 3
    end
    object PropertiesUsbDevice: TEdit
      Left = 216
      Top = 152
      Width = 177
      Height = 21
      TabOrder = 4
    end
  end
  object PropertiesOK: TBitBtn
    Left = 8
    Top = 240
    Width = 89
    Height = 25
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 1
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object PropertiesCancel: TBitBtn
    Left = 328
    Top = 240
    Width = 89
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    Default = True
    ModalResult = 2
    TabOrder = 2
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object PropertiesDataSize: TEdit
    Left = 224
    Top = 128
    Width = 89
    Height = 21
    TabOrder = 3
  end
end
