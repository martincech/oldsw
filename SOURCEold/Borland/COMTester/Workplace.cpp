//******************************************************************************
//
//   Workplace.c  Tester working platform
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "../Unisys/uni.h"

#include "Workplace.h"
#include "../UartSetup.h"

#include "../UART/ComUart.h"
#include "../USB/UsbUart.h"

#define USB_NAME   "USB"

#pragma package(smart_init)

TWorkplace *Workplace;

//******************************************************************************
// Konstruktor
//******************************************************************************

TWorkplace::TWorkplace()
{
   // Alokovani pracovnich promennych :
   FUart          = new TComUart;
   FPortName      = "COM1";
   FUsbDeviceName = "USB adapter";

   FMode        = TCrtLogger::MIXED;

   FParameters.BaudRate  = 9600;
   FParameters.DataBits  = 8;
   FParameters.StopBits  = 10;
   FParameters.Parity    = TUart::NO_PARITY;
   FParameters.Handshake = TUart::NO_HANDSHAKE;

   FReceiveTimeout = 0;     // bez cekani
   FDataSize       = 1024;  // default buffer

   Workplace = this;
} // TWorkplace

//******************************************************************************
// Inicializace
//******************************************************************************

bool TWorkplace::Initialize()
// Podle nastavenych parametru provede inicializaci
{
   FMemory = new TObjectMemory;
   if( FMemory->IsEmpty){
      Connect();
      Save();
      return( false);
   }
   Load();
   return( true);
} // Initialize

//******************************************************************************
// Ukonceni
//******************************************************************************

void TWorkplace::Shutdown()
// Ukonceni a ulozeni do souboru
{
   Save();
   FMemory->Save();
} // Shutdown

//******************************************************************************
// Zmena portu adapteru
//******************************************************************************

bool TWorkplace::Connect()
// Pripoji port adapteru
{
   if( FUart){
      delete FUart;
      FUart = 0;
   }
   TIdentifier Identifier;
   TUart *Uart;
   if( FPortName == USB_NAME){
      // USB setup
      TUsbUart *Usb = new TUsbUart;
      if( !Usb->Locate( FUsbDeviceName, Identifier)){
         delete Usb;
         return( false);
      }
      Uart = Usb;
   } else {
      // COM setup :
      TComUart *Com = new TComUart;
      if( !Com->Locate( FPortName, Identifier)){
         delete Com;
         return( false);
      }
      Uart = Com;
   }
   if( !Uart->Open( Identifier)){
      delete Uart;
      return( false);
   }
   Uart->SetParameters( FParameters);
   // common init :
   Uart->SetRxNowait();
   Uart->Flush();
   FUart = Uart;
   FUart->Save( Memory);
   return( true);
} // Connect

//******************************************************************************
// Persistence
//******************************************************************************

// Persistent names :
#define NAME            "Workplace"        // section name
#define PORT_NAME       "PortName"
#define USB_DEVICE_NAME "UsbDeviceName"
#define MODE            "Mode"

void TWorkplace::Save()
// persistent save
{
   Memory->Create( NAME);
   Memory->SaveString(  PORT_NAME, FPortName);
   Memory->SaveString(  USB_DEVICE_NAME, FUsbDeviceName);
   Memory->SaveInteger( MODE, FMode);
   if( FUart){
      FUart->Save( FMemory);
   }
} // Save

bool TWorkplace::Load()
// persistent load
{
   if( !Memory->Locate( NAME)){
      return( false);  // unable to find
   }
   FPortName      = Memory->LoadString(  PORT_NAME);
   FUsbDeviceName = Memory->LoadString(  USB_DEVICE_NAME);
   FMode          = (TCrtLogger::TDisplayMode)Memory->LoadInteger( MODE);
   if( FUart){
      FUart->Load( FMemory);
   }
   return( true);
} // Load

