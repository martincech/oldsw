//******************************************************************************
//
//   Main.cpp     Tester Main Frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "../Unisys/uni.h"

#include "MainLoader.h"
#include "../Serial/USB/UsbUart.h"

#include <stdio.h>
#include "Properties.h"

//---------------------------------------------------------------------------

#define TX_SPACE  10         // Tx character spacing [ms]
//#define SEND_FAST 1        // Tx without spacing

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define SetStatus( s) StatusBar->Panels->Items[ 0]->Text = s
#define SetDevice( s) StatusBar->Panels->Items[ 1]->Text = s
#define SetUsb( s)    StatusBar->Panels->Items[ 2]->Text = s

//******************************************************************************
//  Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Workplace = new TWorkplace();
   RxEnable  = true;
} // TMainForm

//******************************************************************************
//  OnCreate
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt          = new TCrt( MainMemo);
   Logger       = new TCrtLogger( Crt);
   // Cteni parametru z registru :
   if( !Workplace->Initialize()){
      return;  // nelze nacist konfiguraci
   }
   Load( Workplace->Memory);      // nacteni parametru okna
   SetStatus( "Idle");
} // FormCreate

//******************************************************************************
//  OnClose
//******************************************************************************

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
   // Zapis :
   Save( Workplace->Memory);     // parametry okna
   Workplace->Shutdown();
} // FormClose

//******************************************************************************
//  OnResize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
//  Exit
//******************************************************************************

void __fastcall TMainForm::FileExitExecute(TObject *Sender)
{
  Close();
} // FileExitExecute

//******************************************************************************
//  New
//******************************************************************************

void __fastcall TMainForm::FileNewExecute(TObject *Sender)
{
   Crt->Clear();
} // FileNewExecute

//******************************************************************************
//  Properties
//******************************************************************************

void __fastcall TMainForm::FilePropertiesExecute(TObject *Sender)
{
   if( !PropertiesForm->Execute()){
      return;
   }
   Restart();  // Restart s novym nastavenim
} // FilePropertiesExecute

//******************************************************************************
//  OnShow
//******************************************************************************

void __fastcall TMainForm::FormShow(TObject *Sender)
{
   Restart();
} // FormShow

//---------------------------------------------------------------------------

//******************************************************************************
// Restart
//******************************************************************************

void TMainForm::Restart()
 // Start/Restart s novym nastavenim
{
char buff[ 100];

   if( !Workplace->Connect()){
      SetDevice( "No device");
      return;
   }
   SetDevice( Workplace->PortName + ":" + (TString)Workplace->Parameters->BaudRate);
   TUsbUart *Usb = dynamic_cast<TUsbUart *>(Workplace->Uart);
   if( Usb){
      SetUsb( Usb->Name);
   } else {
      SetUsb( "");
   }
   Logger->Mode = Workplace->Mode;
} // Restart

//******************************************************************************
// Aktualizace Tx combo
//******************************************************************************

void TMainForm::UpdateTxList()
// Aktualizuje seznam Tx dat
{
   // porovnani s ulozenym textem :
   int Count = TxData->Items->Count;
   if( !Count){
      // prazdny seznam
      TxData->Items->Append( TxData->Text);
      return;
   }
   for( int i = 0; i < Count; i++){
      if( TxData->Text == TxData->Items->Strings[ i]){
         return;       // je jiz v seznamu
      }
   }
   TxData->Items->Append( TxData->Text);  // neni v seznamu
} // UpdateTxList

//******************************************************************************
// Ulozeni/nacteni konfigurace
//******************************************************************************

#define FORM_PROPERTIES "MainForm"
#define MAXIMIZED       "Maximized"

#define PLoadInteger( name)   name = Memory->LoadInteger(#name);

void TMainForm::Load( TObjectMemory *Memory)
// Load setup from <Memory>
{
   if( !Memory->Locate( FORM_PROPERTIES)){
      Save( Memory);
      return;
   }
   PLoadInteger( Top);
   PLoadInteger( Left);
   PLoadInteger( Height);
   PLoadInteger( Width);
   if( Memory->LoadBool( MAXIMIZED)){
      WindowState = wsMaximized;
   } else {
      WindowState = wsNormal;
   }
} // Load

#define PSaveInteger( name)  Memory->SaveInteger(#name, name)

void TMainForm::Save( TObjectMemory *Memory)
// Save setup to <Memory>
{
   // zapis parametru do registru :
   Memory->Create(  FORM_PROPERTIES);
   PSaveInteger( Top);
   PSaveInteger( Left);
   PSaveInteger( Height);
   PSaveInteger( Width);
   Memory->SaveBool( MAXIMIZED, WindowState == wsMaximized);
} // Save

//******************************************************************************
// Open
//******************************************************************************

void __fastcall TMainForm::BtnOpenClick(TObject *Sender)
{
   if( !FileOpenDialog->Execute()){
      return;
   }
   LblSendFilename->Caption = FileOpenDialog->FileName;
} // BtnOpenClick

//******************************************************************************
// Save
//******************************************************************************

void __fastcall TMainForm::BtnSaveClick(TObject *Sender)
{
   if( !FileSaveDialog->Execute()){
      return;
   }
   LblReceiveFilename->Caption = FileSaveDialog->FileName;
} // BtnSaveClick

//******************************************************************************
// Send
//******************************************************************************

#define BUFFER_SIZE   1024
byte Buffer[ BUFFER_SIZE];

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
FILE *f;
   if( !FileExists( FileOpenDialog->FileName)){
      Crt->printf( "No file selected\n");
      return;
   }
   if( !Workplace->Uart){
      Crt->printf( "No open UART\n");
      return;
   }
   // read & send :
   f = fopen( FileOpenDialog->FileName.c_str(), "rb");
   if( !f){
      Crt->printf( "Unable open file\n");
      return;
   }
   int Size, TxSize;
   forever {
      Size = fread( Buffer, 1, BUFFER_SIZE, f);
      if( Size <= 0){
         Crt->printf( "Data send DONE\n");
         break;
      }
#ifndef __SEND_FAST__
      int TxSize;
      for( int i = 0; i < Size; i++){
         TxSize = Workplace->Uart->Write( &Buffer[ i], 1);
         if( TxSize != 1){
            Crt->printf( "Unable Send data\n");
            fclose( f);
            return;
         }
         Sleep( TX_SPACE);
      }
#else
      int TxSize = Workplace->Uart->Write( Buffer, Size);
      if( TxSize != Size){
         Crt->printf( "Unable Send data\n");
         break;
      }
#endif
   }
   fclose( f);
} // BtnSendClick

//******************************************************************************
// Receive
//******************************************************************************

void __fastcall TMainForm::BtnReceiveClick(TObject *Sender)
{
   RxEnable  = false;
   //!!! uart parameters
   //!!! receive
   //!!! restore uart nowait
   RxEnable  = true;
} // BtnReceiveClick

//******************************************************************************
// Transmit
//******************************************************************************

void __fastcall TMainForm::BtnTransmitClick(TObject *Sender)
{
   if( !Workplace->Uart){
      return;
   }
   if( TxData->Text.IsEmpty()){
      StatusBar->Panels->Items[ 0]->Text = "Empty !";
      return;
   }
   StatusBar->Panels->Items[ 0]->Text = "Tx Start";
   StatusBar->Refresh();
   UpdateTxList();             // aktualizace combo boxu
   // dekodovani radku :
   char     Line[ 1000];
   int      Length = 0;
   unsigned Hex;
   strcpy( Line, TxData->Text.c_str());
   if( SendHexa->Checked){
      // vysilani v hexa modu
      char *p = Line;
      byte     Buffer[ 300];
      while( 1){
         if( sscanf( p, "%X", &Hex) != 1){
            break;
         }
         Buffer[ Length++] = (byte)Hex;
         p = strchr( p, ' ');             // dalsi pole
         if_null( p){
            break;   // konec retezce
         }
         p++;        // dalsi za mezerou
      }
      int size = Workplace->Uart->Write( Buffer, Length);
      if( size != Length){
         StatusBar->Panels->Items[ 0]->Text = "Tx Error";
         StatusBar->Refresh();
         return;
      }
   } else {
      // vysilani v ASCII modu
      Length = strlen( Line);
      for( int i = 0; i < Length; i++){
         if( Line[ i] == '\\'){
            // escape sekvence
            switch( Line[ i + 1]){
               case '\\' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1); // vc. koncove nuly
                  Length--;
                  break;  // jen opacne lomitko
               case 'r' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\r';
                  Length--;
                  break;
               case 'n' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\n';
                  Length--;
                  break;
               case 't' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\t';
                  Length--;
                  break;
               case 'f' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\f';
                  Length--;
                  break;
               case '0' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\0';
                  Length--;
                  break;
               case 'x' :
                  if( sscanf( &Line[ i + 2], "%02X", &Hex) != 1){
                     break;
                  }
                  memmove( &Line[ i + 1], &Line[ i + 4], Length - i - 3);
                  Line[ i] = Hex;
                  Length -= 3;
                  break;
               default :
                  break;
            } // switch
         }
      }
      int Size = Workplace->Uart->Write( Line, Length);
      if( Size != Length){
         StatusBar->Panels->Items[ 0]->Text = "Tx Error";
         StatusBar->Refresh();
         return;
      }
   }
   StatusBar->Panels->Items[ 0]->Text = "Tx Stop";
   StatusBar->Refresh();
} // BtnTransmitClick

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TMainForm::StatusTimerTimer(TObject *Sender)
{
   if( !RxEnable){
      return;
   }
   if( !Workplace->Uart){
      return;
   }
   // cteni dat :
   byte     Buffer[ 100];
   int size = Workplace->Uart->Read( Buffer, sizeof( Buffer));
   // zobrazeni :
   if( size > 0){
      Logger->Write( TLogger::RX_DATA, Buffer, size);
   }
} // StatusTimerTimer

