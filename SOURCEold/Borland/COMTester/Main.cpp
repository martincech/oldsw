//******************************************************************************
//
//   Main.cpp     Tester Main Frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "../Unisys/uni.h"

#include "Main.h"
#include "../Serial/USB/UsbUart.h"

#include <stdio.h>
#include "Properties.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define SetStatus( s) StatusBar->Panels->Items[ 0]->Text = s
#define SetDevice( s) StatusBar->Panels->Items[ 1]->Text = s
#define SetUsb( s)    StatusBar->Panels->Items[ 2]->Text = s

//******************************************************************************
//  Konstruktor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   Workplace = new TWorkplace();
} // TMainForm

//******************************************************************************
//  OnCreate
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt          = new TCrt( MainMemo);
   Logger       = new TCrtLogger( Crt);
   // Cteni parametru z registru :
   if( !Workplace->Initialize()){
      return;  // nelze nacist konfiguraci
   }
   Load( Workplace->Memory);      // nacteni parametru okna
   SetStatus( "Idle");
} // FormCreate

//******************************************************************************
//  OnClose
//******************************************************************************

void __fastcall TMainForm::FormClose(TObject *Sender, TCloseAction &Action)
{
   // Zapis :
   Save( Workplace->Memory);     // parametry okna
   Workplace->Shutdown();
} // FormClose

//******************************************************************************
//  OnResize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
//  OnTimer
//******************************************************************************

void __fastcall TMainForm::StatusTimerTimer(TObject *Sender)
{

   if( !Workplace->Uart){
      return;
   }
   DSR->Down = Workplace->Uart->DSR;
   CTS->Down = Workplace->Uart->CTS;
   DCD->Down = Workplace->Uart->DCD;
   RI->Down  = Workplace->Uart->RI;

   RTS->Down = Workplace->Uart->RTS;
   DTR->Down = Workplace->Uart->DTR;
   TXD->Down = Workplace->Uart->TxD;

   // cteni dat :
   byte     Buffer[ 100];
   int size = Workplace->Uart->Read( Buffer, sizeof( Buffer));
   // zobrazeni :
   if( size > 0){
      Logger->Write( TLogger::RX_DATA, Buffer, size);
   }
} // StatusTimerTimer

//******************************************************************************
//  Exit
//******************************************************************************

void __fastcall TMainForm::FileExitExecute(TObject *Sender)
{
  Close();
} // FileExitExecute

//******************************************************************************
//  New
//******************************************************************************

void __fastcall TMainForm::FileNewExecute(TObject *Sender)
{
   Crt->Clear();
} // FileNewExecute

//******************************************************************************
//  Properties
//******************************************************************************

void __fastcall TMainForm::FilePropertiesExecute(TObject *Sender)
{
   if( !PropertiesForm->Execute()){
      return;
   }
   Restart();  // Restart s novym nastavenim
} // FilePropertiesExecute

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::BtnSendClick(TObject *Sender)
{
   if( !Workplace->Uart){
      return;
   }
   if( TxData->Text.IsEmpty()){
      StatusBar->Panels->Items[ 0]->Text = "Empty !";
      return;
   }
   StatusBar->Panels->Items[ 0]->Text = "Tx Start";
   StatusBar->Refresh();
   UpdateTxList();             // aktualizace combo boxu
   // dekodovani radku :
   char     Line[ 1000];
   int      Length = 0;
   unsigned Hex;
   strcpy( Line, TxData->Text.c_str());
   if( SendHexa->Checked){
      // vysilani v hexa modu
      char *p = Line;
      byte     Buffer[ 300];
      while( 1){
         if( sscanf( p, "%X", &Hex) != 1){
            break;
         }
         Buffer[ Length++] = (byte)Hex;
         p = strchr( p, ' ');             // dalsi pole
         if_null( p){
            break;   // konec retezce
         }
         p++;        // dalsi za mezerou
      }
      int size = Workplace->Uart->Write( Buffer, Length);
      if( size != Length){
         StatusBar->Panels->Items[ 0]->Text = "Tx Error";
         StatusBar->Refresh();
         return;
      }
   } else {
      // vysilani v ASCII modu
      Length = strlen( Line);
      for( int i = 0; i < Length; i++){
         if( Line[ i] == '\\'){
            // escape sekvence
            switch( Line[ i + 1]){
               case '\\' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1); // vc. koncove nuly
                  Length--;
                  break;  // jen opacne lomitko
               case 'r' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\r';
                  Length--;
                  break;
               case 'n' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\n';
                  Length--;
                  break;
               case 't' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\t';
                  Length--;
                  break;
               case 'f' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\f';
                  Length--;
                  break;
               case '0' :
                  memmove( &Line[ i + 1], &Line[ i + 2], Length - i - 1);
                  Line[ i] = '\0';
                  Length--;
                  break;
               case 'x' :
                  if( sscanf( &Line[ i + 2], "%02X", &Hex) != 1){
                     break;
                  }
                  memmove( &Line[ i + 1], &Line[ i + 4], Length - i - 3);
                  Line[ i] = Hex;
                  Length -= 3;
                  break;
               default :
                  break;
            } // switch
         }
      }
      int Size = Workplace->Uart->Write( Line, Length);
      if( Size != Length){
         StatusBar->Panels->Items[ 0]->Text = "Tx Error";
         StatusBar->Refresh();
         return;
      }
   }
   StatusBar->Panels->Items[ 0]->Text = "Tx Stop";
   StatusBar->Refresh();
} // BtnSendClick

//******************************************************************************
//  OnDTR
//******************************************************************************

void __fastcall TMainForm::DTRClick(TObject *Sender)
{
   if( !Workplace->Uart){
      return;
   }
   Workplace->Uart->DTR = !Workplace->Uart->DTR;
} // DTRClick

//******************************************************************************
//  OnRTS
//******************************************************************************

void __fastcall TMainForm::RTSClick(TObject *Sender)
{
   if( !Workplace->Uart){
      return;
   }
   Workplace->Uart->RTS = !Workplace->Uart->RTS;
} // RTSClick

//******************************************************************************
// OnTxD
//******************************************************************************

void __fastcall TMainForm::TXDClick(TObject *Sender)
{
   if( !Workplace->Uart){
      return;
   }
   Workplace->Uart->TxD = !Workplace->Uart->TxD;
}

//******************************************************************************
//  OnShow
//******************************************************************************

void __fastcall TMainForm::FormShow(TObject *Sender)
{
   Restart();
} // FormShow

//---------------------------------------------------------------------------

//******************************************************************************
// Restart
//******************************************************************************

void TMainForm::Restart()
 // Start/Restart s novym nastavenim
{
char buff[ 100];

   if( !Workplace->Connect()){
      SetDevice( "No device");
      return;
   }
   SetDevice( Workplace->PortName + ":" + (TString)Workplace->Parameters->BaudRate);
   TUsbUart *Usb = dynamic_cast<TUsbUart *>(Workplace->Uart);
   if( Usb){
      SetUsb( Usb->Name);
   } else {
      SetUsb( "");
   }
   Logger->Mode = Workplace->Mode;
} // Restart

//******************************************************************************
// Aktualizace Tx combo
//******************************************************************************

void TMainForm::UpdateTxList()
// Aktualizuje seznam Tx dat
{
   // porovnani s ulozenym textem :
   int Count = TxData->Items->Count;
   if( !Count){
      // prazdny seznam
      TxData->Items->Append( TxData->Text);
      return;
   }
   for( int i = 0; i < Count; i++){
      if( TxData->Text == TxData->Items->Strings[ i]){
         return;       // je jiz v seznamu
      }
   }
   TxData->Items->Append( TxData->Text);  // neni v seznamu
} // UpdateTxList

//******************************************************************************
// Ulozeni/nacteni konfigurace
//******************************************************************************

#define FORM_PROPERTIES "MainForm"
#define MAXIMIZED       "Maximized"

#define PLoadInteger( name)   name = Memory->LoadInteger(#name);

void TMainForm::Load( TObjectMemory *Memory)
// Load setup from <Memory>
{
   if( !Memory->Locate( FORM_PROPERTIES)){
      Save( Memory);
      return;
   }
   PLoadInteger( Top);
   PLoadInteger( Left);
   PLoadInteger( Height);
   PLoadInteger( Width);
   if( Memory->LoadBool( MAXIMIZED)){
      WindowState = wsMaximized;
   } else {
      WindowState = wsNormal;
   }
} // Load

#define PSaveInteger( name)  Memory->SaveInteger(#name, name)

void TMainForm::Save( TObjectMemory *Memory)
// Save setup to <Memory>
{
   // zapis parametru do registru :
   Memory->Create(  FORM_PROPERTIES);
   PSaveInteger( Top);
   PSaveInteger( Left);
   PSaveInteger( Height);
   PSaveInteger( Width);
   Memory->SaveBool( MAXIMIZED, WindowState == wsMaximized);
} // Save

