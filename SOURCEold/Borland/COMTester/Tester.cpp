//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Tester.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Workplace.cpp");
USEFORM("Properties.cpp", PropertiesForm);
USEUNIT("..\Library\Serial\UART\ComUart.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USELIB("..\Library\Serial\USB\FTDI.lib");
USEUNIT("..\Library\Memory\ObjectMemory.cpp");
USEFORM("..\Library\Serial\UartSetup.cpp", UartSetupDialog);
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->CreateForm(__classid(TPropertiesForm), &PropertiesForm);
                 Application->CreateForm(__classid(TUartSetupDialog), &UartSetupDialog);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
