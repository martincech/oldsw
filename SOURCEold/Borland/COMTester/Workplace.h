//******************************************************************************
//
//   Workplace.h  Tester working platform
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef WorkplaceH
#define WorkplaceH

#include "../Uart.h"
#include "../CrtLogger.h"

//******************************************************************************
// Workplace
//******************************************************************************

class TWorkplace
{
public:
   TWorkplace();
   // Constructor
   bool Initialize();
   // Inititalize by file
   void Shutdown();
   // Save to file
   bool Connect();
   // Connect port

  __property TUart              *Uart           = {read=FUart, write=FUart};
  __property TString             PortName       = {read=FPortName, write=FPortName};
  __property TString             UsbDeviceName  = {read=FUsbDeviceName, write=FUsbDeviceName};
  __property TUart::TParameters *Parameters     = {read=GetParameters};
  __property int                 ReceiveTimeout = {read=FReceiveTimeout, write=FReceiveTimeout};
  __property int                 DataSize       = {read=FDataSize, write=FDataSize};
  __property TCrtLogger::TDisplayMode Mode      = {read=FMode, write=FMode};
  __property TObjectMemory      *Memory         = {read=FMemory, write=FMemory};

//------------------------------------------------------------------------------
protected :
   TUart                    *FUart;                // Communication port
   TString                   FPortName;            // Com Name
   TString                   FUsbDeviceName;       // USB device name
   TUart::TParameters        FParameters;          // Com parameters
   int                       FReceiveTimeout;
   int                       FDataSize;
   TCrtLogger::TDisplayMode  FMode;                // Dump display mode
   TObjectMemory            *FMemory;              // Configuration file
   TUart::TParameters *GetParameters(){return( &FParameters);};
   void Save();
   // persistent save
   bool Load();
   // persistent load
}; // TWorkplace

extern PACKAGE TWorkplace *Workplace;

#endif
