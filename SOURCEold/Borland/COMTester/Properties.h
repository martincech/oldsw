//******************************************************************************
//
//   TesterProperties.cpp   Tester Configuration Dialog
//   Version 1.0  (c) VymOs
//
//******************************************************************************
#ifndef PropertiesH
#define PropertiesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <IniFiles.hpp>

#include "../Uart.h"
#include "../UartSetup.h"
#include "Workplace.h"

//---------------------------------------------------------------------------
class TPropertiesForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TComboBox *PropertiesPort;
        TLabel *Label1;
        TRadioGroup *PropertiesDisplay;
        TButton *PropertiesPortSettings;
        TBitBtn *PropertiesOK;
        TBitBtn *PropertiesCancel;
        TEdit *PropertiesRxTimeout;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TEdit *PropertiesDataSize;
        TLabel *Label6;
        TLabel *Label7;
        TEdit *PropertiesUsbDevice;
        TLabel *Label8;
        void __fastcall PortSettingsClick(TObject *Sender);
private:	// User declarations
   // Pracovni data :
   int                ComNumber;

public:		// User declarations
   __fastcall TPropertiesForm(TComponent* Owner);
   // Konstruktor
   bool __fastcall Execute();
   // Zobrazeni konfiguracniho dialogu
};
//---------------------------------------------------------------------------
extern PACKAGE TPropertiesForm *PropertiesForm;
//---------------------------------------------------------------------------
#endif
