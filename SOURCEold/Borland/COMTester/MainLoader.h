//******************************************************************************
//
//   Main.H       Tester Main Frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef MainLoaderH
#define MainLoaderH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <IniFiles.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------

#include "Properties.h"
#include "Workplace.h"
#include <Buttons.hpp>
#include <Dialogs.hpp>

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TMainMenu *MainMenu;
   TMenuItem *File1;
   TActionList *ActionList1;
   TImageList *ImageList1;
   TAction *FileExit;
        TMenuItem *MenuFileNew;
        TMenuItem *MenuFileExit;
   TMenuItem *N1;
   TStatusBar *StatusBar;
   TToolBar *ToolBar1;
   TToolButton *ToolButton2;
   TAction *FileProperties;
   TToolButton *ToolButton4;
        TMenuItem *MenuFileProperties;
        TMemo *MainMemo;
   TAction *FileNew;
   TToolButton *ToolButton5;
        TOpenDialog *FileOpenDialog;
        TSaveDialog *FileSaveDialog;
        TButton *BtnOpen;
        TButton *BtnSave;
        TButton *BtnSend;
        TButton *BtnReceive;
        TLabel *Label2;
        TLabel *Label1;
        TLabel *LblSendFilename;
        TLabel *LblReceiveFilename;
        TLabel *Label3;
        TComboBox *TxData;
        TButton *BtnTransmit;
        TCheckBox *SendHexa;
        TTimer *StatusTimer;

   void __fastcall FormCreate(TObject *Sender);
   void __fastcall FormShow(TObject *Sender);
   void __fastcall FormResize(TObject *Sender);
   void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
   void __fastcall FileExitExecute(TObject *Sender);
   void __fastcall FilePropertiesExecute(TObject *Sender);
   void __fastcall FileNewExecute(TObject *Sender);
        void __fastcall BtnOpenClick(TObject *Sender);
        void __fastcall BtnSaveClick(TObject *Sender);
        void __fastcall BtnReceiveClick(TObject *Sender);
        void __fastcall BtnSendClick(TObject *Sender);
        void __fastcall BtnTransmitClick(TObject *Sender);
        void __fastcall StatusTimerTimer(TObject *Sender);

private:	// User declarations
   TCrt       *Crt;
   TCrtLogger *Logger;
   TWorkplace *Workplace;
   bool       RxEnable;

   void Restart();
   // Start/Restart s novym nastavenim
   void UpdateTxList();
   // Aktualizuje seznam Tx dat
public:		// User declarations
    __fastcall TMainForm(TComponent* Owner);
   // Konstruktor
   void Load( TObjectMemory *Memory);
   // Load setup from <Memory>
   void Save( TObjectMemory *Memory);
   // Save setup to <Memory>
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
