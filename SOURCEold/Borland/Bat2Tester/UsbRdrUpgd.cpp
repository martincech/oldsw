//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("UsbRdrUpgd.res");
USEFORM("MainUsb.cpp", MainForm);
USEUNIT("UsbTester.cpp");
USELIB("..\..\Library\Serial\USB\FTDI.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "USB Reader Upgrade";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
