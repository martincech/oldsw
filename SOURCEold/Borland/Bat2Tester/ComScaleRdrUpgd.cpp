//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("ComScaleRdrUpgd.res");
USEFORM("MainComScale.cpp", MainForm);
USEUNIT("UsbTesterComScale.cpp");
USELIB("..\Library\Serial\USB\FTDI.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "USB Reader Upgrade";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
       Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
