//******************************************************************************
//
//   Data.cpp     Databaze
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef DataH
#define DataH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------

#define PREFILTER_MAX_SAMPLES   10

class TBatData : public TDataModule
{
__published:	// IDE-managed Components
        TDataSource *DataSource;
        TTable *DataTable;
private:	// User declarations
public:		// User declarations
   __fastcall TBatData(TComponent* Owner);
   // konstruktor
   void Begin();
   // pred prvni zaznam
   bool Next();
   // na dalsi zaznam
   bool Eof();
   // konec databaze
   void SetFile( AnsiString Filename);
   // Set new database file
   void SetPrefilter(int Count);
   // Nastavi predfiltraci
   __property double Weight = {read=GetWeight};
protected :
   double GetWeight();
};
//---------------------------------------------------------------------------
extern PACKAGE TBatData *BatData;
//---------------------------------------------------------------------------
#endif
