object MainForm: TMainForm
  Left = 477
  Top = 251
  Width = 289
  Height = 311
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 Grafik'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImage
    Left = 16
    Top = 16
    Width = 240
    Height = 160
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 265
    Width = 281
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnUpload: TButton
    Left = 16
    Top = 232
    Width = 75
    Height = 25
    Caption = 'Upload'
    TabOrder = 1
    OnClick = BtnUploadClick
  end
  object BtnOpen: TButton
    Left = 16
    Top = 192
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 2
    OnClick = BtnOpenClick
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'bmp'
    FileName = '*.bmp'
    Filter = 'Bat1 Logo|*.bmp'
    Left = 72
    Top = 8
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 120
    Top = 8
  end
end
