//******************************************************************************
//
//   BatDevice.cpp    Bat device basics
//   Version 1.0      (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "BatDevice.h"
#include <time.h>
#include <stdio.h>

#pragma package(smart_init)

#define TRIALS 3         // number of trials

//******************************************************************************
// Constructor
//******************************************************************************

TBatDevice::TBatDevice()
// Constructor
{
   FAdapter   = 0;
   FWriteVerification = false;
} // TBatDevice

//******************************************************************************
// Destructor
//******************************************************************************

TBatDevice::~TBatDevice()
// Destructor
{
} // ~TBatDevice

//******************************************************************************
// Check
//******************************************************************************

bool TBatDevice::Check()
// Check for device
{
   if( !FAdapter){
      return( false);
   }
   int Command = USB_CMD_VERSION;
   int Data    = 0;
   int RCommand, RData;

   if( !Adapter->Send( Command, Data)){
      return( false);
   }
   if( !Adapter->Receive( RCommand, RData)){
      return( false);
   }
   if( !(RCommand & USB_CMD_REPLY)){
      return( false);
   }
   if( (RCommand & ~USB_CMD_REPLY) != Command){
      return( false);
   }
   return( true);
} // Check

//******************************************************************************
// Version
//******************************************************************************

bool TBatDevice::GetVersion( int &Version)
// Get bat version
{
   int Cmd  = USB_CMD_VERSION;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   Version = Data;
   return( true);
} // GetVersion

//******************************************************************************
// Flush
//******************************************************************************

bool TBatDevice::Flush()
// Flush graphics
{
   int Cmd  = USB_CMD_FLUSH;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // Flush

//******************************************************************************
// Flush
//******************************************************************************

bool TBatDevice::Clear()
// Flush graphics
{
   int Cmd  = USB_CMD_CLEAR;
   int Data = 0;
   if( !SendCommand( Cmd, Data)){
      return( false);
   }
   return( true);
} // Clear

//******************************************************************************
// Write Memory
//******************************************************************************

bool TBatDevice::WriteMemory( int Address, void *Buffer, int Size)
// Write EEPROM data
{
   if( !FAdapter){
      return( false);
   }
   // Split into packets :
   int Length = Size > USB_WRITE_SIZE ? USB_WRITE_SIZE : Size;
   int TotalLength = Size;
   int RLength;
   int Offset      = 0;
   do {
      if( !WriteFragment( Address + Offset, &((char *)Buffer)[ Offset], Length)){
         return( false);
      }
      // next packet :
      Offset      += Length;
      TotalLength -= Length;
      if( TotalLength > USB_WRITE_SIZE){
         Length = USB_WRITE_SIZE;
      } else {
         Length = TotalLength;
      }
   } while( TotalLength != 0);
   // Check for address :
   int Command = USB_CMD_GET_ADDRESS;
   int Data    = 0;
   if( !SendCommand( Command, Data)){
      return( false);
   }
   if( Data != (Address + Size)){
      return( false);
   }
   return( true);
} // WriteMemory

//------------------------------------------------------------------------------

//******************************************************************************
// Send command
//******************************************************************************

bool TBatDevice::SendCommand( int &Command, int &Data)
// Send command with error recovery
{
   if( !FAdapter){
      return( false);
   }
   int RCommand, RData;
   for( int i = 0; i < TRIALS; i++){
      if( !Adapter->Send( Command, Data)){
         continue;
      }
      if( !Adapter->Receive( RCommand, RData)){
         continue;
      }
      if( RCommand != TPktAdapter::DATA_MESSAGE &&
         (RCommand & ~USB_CMD_REPLY) != Command){
         return( false);
      }
      Data    = RData;
      Command = RCommand;
      return( true);
   }
   return( false);
} // SendCommand

//******************************************************************************
// Send command
//******************************************************************************

bool TBatDevice::WriteFragment( int Address, void *Buffer, int Size)
// Write fragment of data with recovery
{
   int Command,  Data;
   int RCommand, RData;
   TUsbCmdUnion  CmdUnion;
   for( int i = 0; i < TRIALS; i++){
      // send address :
      Command = USB_CMD_ADDRESS;
      Data    = Address;
      if( !Adapter->Send( Command, Data)){
         continue;
      }
      if( !Adapter->Receive( RCommand, RData)){
         continue;
      }
      if( !(RCommand & USB_CMD_REPLY)){
         continue;
      }
      if( (RCommand & ~USB_CMD_REPLY) != Command){
         continue;
      }
      // send data :
      CmdUnion.Write.Cmd = USB_CMD_WRITE;
      memcpy( CmdUnion.Write.Data, Buffer, Size);
      if( !Adapter->SendData( &CmdUnion, Size + USB_CMD_SIZE)){
         continue;
      }
      if( !Adapter->Receive( RCommand, RData)){
         continue;
      }
      if( !(RCommand & USB_CMD_REPLY)){
         continue;
      }
      if( (RCommand & ~USB_CMD_REPLY) != USB_CMD_WRITE){
         continue;
      }
      // check for returned address :
      if( RData != Address + Size){
         continue;
      }
      return( true);
   }
   return( false);
} // WriteFragment

//******************************************************************************
// Property IsAlive
//******************************************************************************

bool TBatDevice::GetAlive()
// Property IsAlive - adapter OK
{
   if( !FAdapter){
      return( false);
   }
   if( !FAdapter->IsOpen){
      return( false);
   }
   return( true);
} // GetAlive

