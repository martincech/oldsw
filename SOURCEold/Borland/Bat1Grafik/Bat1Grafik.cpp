//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat1Grafik.res");
USEFORM("MainGrafik.cpp", MainForm);
USEUNIT("PktAdapter.cpp");
USEUNIT("..\Library\Serial\USB\UsbUart.cpp");
USELIB("..\FTDI-USB\FTDI.lib");
USEUNIT("BatDevice.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Bat1 Grafik";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
