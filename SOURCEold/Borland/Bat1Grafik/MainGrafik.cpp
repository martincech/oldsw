//******************************************************************************
//
//   Main.cpp     Bat1Grafik main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainGrafik.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//-----------------------------------------------------------------------------
// Logo definition
//-----------------------------------------------------------------------------

#define LOGO_WIDTH  240
#define LOGO_HEIGHT 160
#define LOGO_PLANES 2
#define LOGO_ROWS   (LOGO_HEIGHT / 8)
#define LOGO_SIZE   LOGO_WIDTH * LOGO_ROWS

typedef byte TLogo[ LOGO_PLANES][ LOGO_ROWS][ LOGO_WIDTH];

TLogo Logo;          // Global logo buffer

//-----------------------------------------------------------------------------

// Color planes :
//            White   Light gray   Dark Gray Black
// Plane 0    0       1            0         1
// Plane 1    0       0            1         1

// Color transformation :
#define COLOR_BLACK      0x00000000
#define COLOR_MIDDLEGRAY 0x007F7F7F
#define COLOR_WHITE      0x00FFFFFF

//-----------------------------------------------------------------------------

AnsiString FileName = "";
int        FileDate = -1;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   MyBitmap = 0;
   Adapter  = new TPktAdapter;
   Device   = new TBatDevice;
   Device->Adapter = Adapter;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Adapter->Logger = 0;
} // FormCreate

//******************************************************************************
// Open
//******************************************************************************

void __fastcall TMainForm::BtnOpenClick(TObject *Sender)
{
   // select file :
   if( !FileOpenDialog->Execute()){
      return;
   }
   Image->Picture->LoadFromFile( FileOpenDialog->FileName);
   FileName = FileOpenDialog->FileName;
   FileDate = -1;           // force upload
} // BtnOpenClick

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TMainForm::TimerTimer(TObject *Sender)
{
   if( !Device->Check()){
      Status() + "No device";
      return;
   }
   Status() + "Device OK";
   int FDate = GetFileDate();
   if( FDate == -1){
      return;          // invalid file
   }
   if( FDate == FileDate){
      return;          // already uploaded
   }
   Upload();           // update
   FileDate = FDate;   // don't load next time
} // TimerTimer

//******************************************************************************
// Upload
//******************************************************************************

void __fastcall TMainForm::BtnUploadClick(TObject *Sender)
{
   Upload();
} // BtnUploadClick

//******************************************************************************
// Get File Date
//******************************************************************************

int TMainForm::GetFileDate()
{
int Handle;
int FDate;

   if( FileName == ""){
      return( -1);
   }
   Handle = FileOpen( FileName, fmOpenRead);
   if( Handle == -1){
      return( -1);
   }
   FDate = FileGetDate( Handle);
   FileClose( Handle);
   return( FDate);
} // GetFileDate

//******************************************************************************
// Upload
//******************************************************************************

void TMainForm::Upload()
{
   Timer->Enabled = false;
   Status() + "Upload...";
   StatusBar->Refresh();
   // prepare data :
   memset( Logo, 0, sizeof( Logo));
   for( int x = 0; x < LOGO_WIDTH; x++){
      for( int y = 0; y < LOGO_HEIGHT; y ++){
         TColor Color = Image->Picture->Bitmap->Canvas->Pixels[ x][ y];
         int Yaddr = y / 8;
         int Ymask = 1 << (y % 8);
         if( Color == COLOR_WHITE){
            continue;
         }
         // plane 0
         if( Color == COLOR_BLACK || Color >= COLOR_MIDDLEGRAY){ // light gray
            Logo[ 0][ Yaddr][x] |= Ymask;
         }
         // plane 1
         if( Color == COLOR_BLACK || Color <  COLOR_MIDDLEGRAY){ // dark gray
            Logo[ 1][ Yaddr][x] |= Ymask;
         }
      }
   }
   // clear display :
   if( !Device->Clear()){
      Status() + "Unable clear";
      Timer->Enabled = true;
      return;
   }
   // write data :
   Screen->Cursor = crHourGlass;
   if( !Device->WriteMemory( 0, &Logo, sizeof( Logo))){
      Screen->Cursor = crDefault;
      Status() + "Unable write";
      Timer->Enabled = true;
      return;
   }
   Screen->Cursor = crDefault;
   // flush display :
   if( !Device->Flush()){
      Status() + "Unable flush";
      Timer->Enabled = true;
      return;
   }
   Status() + "Upload OK";
   Timer->Enabled = true;
} // Upload

