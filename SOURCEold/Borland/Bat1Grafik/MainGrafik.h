//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainGrafikH
#define MainGrafikH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#include "BatDevice.h"
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TStatusBar *StatusBar;
        TButton *BtnUpload;
        TOpenDialog *FileOpenDialog;
        TButton *BtnOpen;
        TImage *Image;
        TTimer *Timer;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall BtnUploadClick(TObject *Sender);
        void __fastcall BtnOpenClick(TObject *Sender);
        void __fastcall TimerTimer(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TPktAdapter   *Adapter;
   TBatDevice    *Device;
   Graphics::TBitmap  *MyBitmap;
   __fastcall TMainForm(TComponent* Owner);
   int GetFileDate();
   void Upload();
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
