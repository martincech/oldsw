//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#include "BatDevice.h"
#include "../Crt/Crt.h"
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnSend;
        TButton *BtnSendData;
        TButton *BtnVersion;
        TButton *BtnRead;
        TButton *BtnWrite;
        TCheckBox *CbEnableVerify;
        TButton *BtnStatus;
        TButton *BtnPowerOff;
        TButton *BtnDownload;
        TButton *BtnUpload;
        TOpenDialog *FileOpenDialog;
        TSaveDialog *FileSaveDialog;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnSendClick(TObject *Sender);
        void __fastcall BtnSendDataClick(TObject *Sender);
        void __fastcall BtnVersionClick(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
        void __fastcall BtnWriteClick(TObject *Sender);
        void __fastcall BtnStatusClick(TObject *Sender);
        void __fastcall BtnPowerOffClick(TObject *Sender);
        void __fastcall BtnDownloadClick(TObject *Sender);
        void __fastcall BtnUploadClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt          *Crt;
   TPktAdapter   *Adapter;
   TBatDevice    *Device;
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
