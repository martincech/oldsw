//******************************************************************************
//
//   Main.cpp     Bat1Reader main
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

#include "MainGrafik.h"
#include "../Serial/CrtLogger.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define Status()     StatusBar->Panels->Items[ 0]->Text = (AnsiString)"Status : "
#define StatusOk()   Status() + "OK";

//-----------------------------------------------------------------------------
// Logo definition
//-----------------------------------------------------------------------------

#define LOGO_WIDTH  240
#define LOGO_HEIGHT 160
#define LOGO_PLANES 2
#define LOGO_ROWS   (LOGO_HEIGHT / 8)
#define LOGO_SIZE   LOGO_WIDTH * LOGO_ROWS

typedef byte TLogo[ LOGO_PLANES][ LOGO_ROWS][ LOGO_WIDTH];

TLogo Logo;          // Global logo buffer

// Color planes :
//            White   Light gray   Dark Gray Black
// Plane 0    0       1            0         1
// Plane 1    0       0            1         1

// Color transformation :
#define COLOR_BLACK     clBlack
#define COLOR_LIGHTGRAY clYellow
#define COLOR_DARKGRAY  clRed
#define COLOR_WHITE     clWhite

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
   MyBitmap = 0;
   Adapter  = new TPktAdapter;
   Device   = new TBatDevice;
   Device->Adapter = Adapter;
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
   Adapter->Logger = Logger;
} // FormCreate

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::FormResize(TObject *Sender)
{
   Crt->Resize( this->Canvas);
} // FormResize

//******************************************************************************
// Version
//******************************************************************************

void __fastcall TMainForm::BtnVersionClick(TObject *Sender)
{
   int Version;
   if( !Device->GetVersion( Version)){
      Status() + "Unable get version";
      return;
   }
   Crt->printf( "Version %x.%02x\n", Version >> 8, Version & 0xFF);
   StatusOk();
} // BtnVersionClick

//******************************************************************************
// Open
//******************************************************************************

void __fastcall TMainForm::BtnOpenClick(TObject *Sender)
{
   // select file :
   if( !FileOpenDialog->Execute()){
      return;
   }
   Image->Picture->LoadFromFile( FileOpenDialog->FileName);
} // BtnOpenClick

//******************************************************************************
// Upload
//******************************************************************************

void __fastcall TMainForm::BtnUploadClick(TObject *Sender)
{
   // prepare data :
   memset( Logo, 0, sizeof( Logo));
   for( int x = 0; x < LOGO_WIDTH; x++){
      for( int y = 0; y < LOGO_HEIGHT; y ++){
         TColor Color = Image->Picture->Bitmap->Canvas->Pixels[ x][ y];
         int Yaddr = y / 8;
         int Ymask = 1 << (y % 8);
         // plane 0
         if( Color == COLOR_BLACK || Color == COLOR_LIGHTGRAY){
            Logo[ 0][ Yaddr][x] |= Ymask;
         }
         // plane 1
         if( Color == COLOR_BLACK || Color == COLOR_DARKGRAY){
            Logo[ 1][ Yaddr][x] |= Ymask;
         }
      }
   }
   // clear display :
   if( !Device->Clear()){
      Status() + "Unable clear";
      return;
   }
   // write data :
   Screen->Cursor = crHourGlass;
   if( !Device->WriteMemory( 0, &Logo, sizeof( Logo))){
      Screen->Cursor = crDefault;
      Status() + "Unable write";
      return;
   }
   Screen->Cursor = crDefault;
   // flush display :
   if( !Device->Flush()){
      Status() + "Unable flush";
      return;
   }
   Crt->printf( "Upload OK\n");
   StatusOk();
} // BtnUploadClick

//******************************************************************************
//  Enable Logger
//******************************************************************************

void __fastcall TMainForm::BtnEnableLoggerClick(TObject *Sender)
{
   if( BtnEnableLogger->Checked){
      Adapter->Logger = Logger;
   } else {
      Adapter->Logger = 0;
   }
} // BtnEnableLoggerClick

