object MainForm: TMainForm
  Left = 212
  Top = 169
  Width = 670
  Height = 646
  Anchors = [akLeft, akTop, akRight, akBottom]
  Caption = 'Bat1 Logo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Image: TImage
    Left = 376
    Top = 88
    Width = 240
    Height = 160
  end
  object MainMemo: TMemo
    Left = 0
    Top = 88
    Width = 361
    Height = 497
    TabOrder = 0
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 600
    Width = 662
    Height = 19
    Panels = <
      item
        Width = 300
      end>
    SimplePanel = False
  end
  object BtnVersion: TButton
    Left = 376
    Top = 352
    Width = 75
    Height = 17
    Caption = 'Version'
    TabOrder = 2
    OnClick = BtnVersionClick
  end
  object BtnUpload: TButton
    Left = 512
    Top = 416
    Width = 75
    Height = 25
    Caption = 'Upload'
    TabOrder = 3
    OnClick = BtnUploadClick
  end
  object BtnOpen: TButton
    Left = 512
    Top = 352
    Width = 75
    Height = 25
    Caption = 'Open'
    TabOrder = 4
    OnClick = BtnOpenClick
  end
  object BtnEnableLogger: TCheckBox
    Left = 8
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Enable Logger'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = BtnEnableLoggerClick
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'bmp'
    FileName = '*.bmp'
    Filter = 'Bat1 Logo|*.bmp'
    Left = 432
    Top = 8
  end
end
