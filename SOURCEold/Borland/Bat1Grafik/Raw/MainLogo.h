//******************************************************************************
//
//   Main.h        Main header
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainLogoH
#define MainLogoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#include "BatDevice.h"
#include "../Crt/Crt.h"
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *MainMemo;
        TStatusBar *StatusBar;
        TButton *BtnVersion;
        TButton *BtnStatus;
        TButton *BtnPowerOff;
        TButton *BtnDownload;
        TButton *BtnUpload;
        TOpenDialog *FileOpenDialog;
        TSaveDialog *FileSaveDialog;
        TButton *BtnOpen;
        TImage *Image;
        TCheckBox *BtnEnableLogger;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall BtnVersionClick(TObject *Sender);
        void __fastcall BtnStatusClick(TObject *Sender);
        void __fastcall BtnPowerOffClick(TObject *Sender);
        void __fastcall BtnDownloadClick(TObject *Sender);
        void __fastcall BtnUploadClick(TObject *Sender);
        void __fastcall BtnOpenClick(TObject *Sender);
        void __fastcall BtnEnableLoggerClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
   TCrt          *Crt;
   TPktAdapter   *Adapter;
   TBatDevice    *Device;
   Graphics::TBitmap  *MyBitmap;
   __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
