//*****************************************************************************
//
//    Megavi.h      MEGAVI protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Megavi_H__
   #define __Megavi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void MegaviInit( void);
// Communication initialisation

void MegaviTxChar( word Char);
// Transmit <Char> word

void MegaviTxDrain( void);
// Waits for end of transmission

TYesNo MegaviRxChar( word *Char);
// Returns YES and received byte on <Char>,
// or NO on timeout

TYesNo MegaviRxWait( word Timeout);
// Waits for receive up to <Timeout> miliseconds

#endif
