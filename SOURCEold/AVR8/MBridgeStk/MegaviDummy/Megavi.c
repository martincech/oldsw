//*****************************************************************************
//
//    Megavi.c      MEGAVI protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Megavi.h"
#include "Delays.h"

// Ports :
#ifndef MEGAVI_COM1
   #define UBRRH UBRR0H
   #define UBRRL UBRR0L
   #define UCSRA UCSR0A
   #define UCSRB UCSR0B
   #define UCSRC UCSR0C
   #define UDR   UDR0
#else // MEGAVI_COM1
   #define UBRRH UBRR1H
   #define UBRRL UBRR1L
   #define UCSRA UCSR1A
   #define UCSRB UCSR1B
   #define UCSRC UCSR1C
   #define UDR   UDR1
#endif

#define BAUD_DIVISOR ((FXTAL / 16 / MEGAVI_BAUD) - 1)

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void MegaviInit( void)
// Communication initialisation
{
   MegaviTxDisable();                       // RS485 Rx direction
   MegaviTxInit();                          // RS485 direction init
   UBRRH = (byte)(BAUD_DIVISOR >> 8);       // set baud rate divisor
   UBRRL = (byte) BAUD_DIVISOR;
   UCSRB = (1 << RXEN0) | (1 << TXEN0) | (1 << UCSZ02);    // enable Rx, Tx, 9bits
   UCSRC = (3 << UCSZ00);                                  // 9 bits, 1 stopbit
} // MegaviInit

//-----------------------------------------------------------------------------
// Tx character
//-----------------------------------------------------------------------------

void MegaviTxChar( word Char)
// Transmit <Char> word
{
   while( !(UCSRA & (1 << UDRE0)));
   MegaviTxEnable();                     // enable Tx
   UCSRB &= ~(1 << TXB80);               // clear bit 9
   if( Char & 0x100){
      UCSRB |= (1 << TXB80);             // set bit 9
   }
   UCSRA |= (1 << TXC0);                 // clear Tx complete bit
   UDR    = Char;
   MegaviTxDrain();                      // wait for Tx empty
   MegaviTxDisable();                    // disable Tx
} // MegaviTxChar

//-----------------------------------------------------------------------------
// Tx empty
//-----------------------------------------------------------------------------

void MegaviTxDrain( void)
// Waits for end of transmission
{
   while( !(UCSRA & (1 << TXC0)));     // wait for Tx complete set
} // MegaviTxDrain

//-----------------------------------------------------------------------------
// Rx character
//-----------------------------------------------------------------------------

TYesNo MegaviRxChar( word *Char)
// Returns YES and received byte on <Char>,
// or NO on timeout
{
word Timeout;
byte Data;
byte Status;
byte Data9;

   Timeout = WDelayCount( MEGAVI_RX_TIMEOUT, 3);
   do {
      if( UCSRA & (1 << RXC0)){
         Status = UCSRA;
         Data9  = UCSRB;
         Data   = UDR;
         if( Status & ((1 << FE0) | (1 << DOR0) | (1 << UPE0))){
            *Char = 0xFFFF;
            return( YES);
         }
         if( Data9 & RXB80){
            *Char = (word)Data | 0x100;
            return( YES);
         }
         *Char = Data;
         return( YES);
      }
   } while( --Timeout);
   return( NO);
} // MegaviRxChar

//-----------------------------------------------------------------------------
// Wait for Rx
//-----------------------------------------------------------------------------

TYesNo MegaviRxWait( word Timeout)
// Waits for receive up to <Timeout> miliseconds
{
word i;

   i = 1;
   Timeout++;
   do{
      do {
         if( UCSRA & (1 << RXC0)){
            return( YES);
         }
      } while( --i);
      i = WDelayCount( 1000, 3);
   } while( --Timeout);
   return( NO);
} // MegaviRxWait
