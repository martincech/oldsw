//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "Uart1.h"           // USART1 - stdio
#include "CStdio.h"          // Console output
#include "Megavi.h"          // MEGAVI protocol

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
word ch;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart1Init();
   StdioInit();
   MegaviInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   cprintf( "Start\n");
   MegaviTxChar( 'M');
   MegaviTxChar( 'E');
   MegaviTxChar( 'G');
   // main loop
   forever {
      if( CalButtonPressed()){
         MegaviTxChar( 'X' | 0x100);
      }
      if( !MegaviRxChar( &ch)){
         continue;
      }
      MegaviTxChar( ch);
   }
   return( 0);
} // main



