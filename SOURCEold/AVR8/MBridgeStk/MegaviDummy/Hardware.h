//*****************************************************************************
//
//    Hardware.h  - Bat2 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED   0

#define StatusLedInit()  DDRC  |=  (1 << StatusLED)

#define StatusLedOn()    PORTC &= ~(1 << StatusLED) 
#define StatusLedOff()   PORTC |=  (1 << StatusLED)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           9600       // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

//------------------------------------------------------------------------------
// UART1
//------------------------------------------------------------------------------

#define UART1_BAUD           9600       // baud rate
#define UART1_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM1

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   0

#define CalButtonInit()     DDRA  &= ~(1 << CalButton)

#define CalButtonPressed()  (!(PINA & (1 << CalButton)))       // active L


//------------------------------------------------------------------------------
// MEGAVI
//------------------------------------------------------------------------------

#define MEGAVI_COM0

#define MEGAVI_BAUD          19200      // baud rate
#define MEGAVI_RX_TIMEOUT    50000      // intercharacter timeout [us]

// RS485 Tx control :
#define MegaviTxInit()
#define MegaviTxEnable()
#define MegaviTxDisable()

//-----------------------------------------------------------------------------

#endif
