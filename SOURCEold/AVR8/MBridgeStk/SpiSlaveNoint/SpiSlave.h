//*****************************************************************************
//
//   Spi.h       SPI interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __Spi_H__
   #define __Spi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void SpiInit( void);
// Initialize bus

TYesNo SpiRxReady( void);
// Returns YES, on Rx data ready

byte SpiRxSize( void);
// Returns received DATA bytes count

void *SpiRxData( void);
// Returns Rx data buffer address

void *SpiTxData( void);
// Returns Tx data buffer address

void SpiTx( byte Size);
// Send <Size>


#endif
