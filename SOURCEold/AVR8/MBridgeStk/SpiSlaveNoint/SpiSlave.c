//*****************************************************************************
//
//   SpiSlave.c  Hardware controller SPI slave
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "SpiSlave.h"

#define SPI_FILLER_DATA    0xFF          // Tx filler data

// Clock divisor selection :
#if   (FXTAL / SPI_CLOCK) == 2
   #define SPI_CLOCK_DIVISOR  0
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 4
   #define SPI_CLOCK_DIVISOR  0
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 8
   #define SPI_CLOCK_DIVISOR  1
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 16
   #define SPI_CLOCK_DIVISOR  1
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 32
   #define SPI_CLOCK_DIVISOR  2
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 64
   #define SPI_CLOCK_DIVISOR  2
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 128
   #define SPI_CLOCK_DIVISOR  3
   #define SPI_CLOCK_X2       0
#else
   #error "Invalid SPI_CLOCK value"
#endif

static byte _RxData[ SPI_RX_BUFFER_SIZE];
static byte _TxData[ SPI_TX_BUFFER_SIZE];
static byte _RxDataSize;
static byte _TxDataSize;
static byte _TxIndex;

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void SpiInit( void)
// Initialize bus
{
   SpiPortInit();
   SPCR  = (1 << SPE) | (SPI_CPOL << CPOL) | (SPI_CPHA << CPHA) | 
           (SPI_CLOCK_DIVISOR << SPR0);
   SPSR |= (SPI_CLOCK_X2 << SPI2X);
   SPDR  = SPI_FILLER_DATA;
   _TxDataSize = 0;
   _RxDataSize = 0;
   _TxIndex    = 0;
} // SpiInit

//-----------------------------------------------------------------------------
// Ready
//-----------------------------------------------------------------------------

TYesNo SpiRxReady( void)
// Returns YES, on Rx data ready
{
byte *p;
byte *q;

   if( !(SPSR & (1 << SPIF))){
      return( NO);
   }
   p           = _RxData;
   q           = _TxData;
   _RxDataSize = 0;
   do {
      if( !(SPSR & (1 << SPIF))){
         continue;
      }
      *p = SPDR;
      if( _TxIndex < _TxDataSize){
         SPDR = *q;
         q++;
         _TxIndex++;
      } else {
         SPDR = SPI_FILLER_DATA;
      }
      p++;
      _RxDataSize++;
   } while( !SpiSsGet());
   // reset transmitter :
   SPDR     = SPI_FILLER_DATA;
   _TxIndex = 0;
   return( YES);
} // SpiRxReady

//-----------------------------------------------------------------------------
// Size
//-----------------------------------------------------------------------------

byte SpiRxSize( void)
// Returns received DATA bytes count
{
   return( _RxDataSize);
} // SpiRxSize

//-----------------------------------------------------------------------------
// Rx Data
//-----------------------------------------------------------------------------

void *SpiRxData( void)
// Returns Rx data buffer address
{
   return( _RxData);
} // SpiRxData

//-----------------------------------------------------------------------------
// Tx Data
//-----------------------------------------------------------------------------

void *SpiTxData( void)
// Returns Tx data buffer address
{
   return( _TxData);
} // SpiTxData

//-----------------------------------------------------------------------------
// Tx
//-----------------------------------------------------------------------------

void SpiTx( byte Size)
// Send <Size>
{
   _TxDataSize = Size;
} // SpiTx
