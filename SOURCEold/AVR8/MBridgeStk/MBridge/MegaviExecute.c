//*****************************************************************************
//
//   MegaviExecute.c  MBridge MEGAVI executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#include "MegaviExecute.h"
#include "System.h"          // Operating system
#include "MBridgeDef.h"      // MBridge application
#include "Megavi.h"          // MEGAVI protocol
#include "Bat2Execute.h"     // Bat2 executive
#include "Sms.h"             // SMS memory
#include <string.h>

static void ProcessCommand( TMegaviCommand *Command, byte Size);
// Process <Command>

static void ReplyVersion( void);
// Reply version

static void ReplyMaleDataGet( void);
// Reply get male data

static void ReplyFemaleDataGet( void);
// Reply get female data

static void ReplySmsSend( TBat2Sms *Sms);
// Reply SMS send

static void ReplySmsStatus( byte Slot);
// Reply SMS status for <Slot>

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void MexInit( void)
// Module init
{
   MegaviInit();
   MegaviRxStart();
} // MexInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void MexExecute( void)
// Module executive
{
byte Size;
byte *Data;

   if( !MegaviRxReady()){
      return;
   }
   StatusLedsOff();
   // check for identification field :
   if( !MBridgeAddressMatch( MegaviRxId())){
      return;                          // foreign packet
   }
   StatusLedROn();
   Data = MegaviData();
   Size = MegaviRxSize();
   ProcessCommand( (TMegaviCommand *)Data, Size);
} // MexExecute

//*****************************************************************************

//-----------------------------------------------------------------------------
// Process command
//-----------------------------------------------------------------------------

static void ProcessCommand( TMegaviCommand *Command, byte Size)
// Process <Command>
{
   switch( Command->Command){
      case BAT2_MEGAVI_COMMAND_VERSION :
         if( Size != sizeof( TBat2MegaviCommand)){
            return;
         }
         ReplyVersion();
         return;

      case BAT2_MEGAVI_COMMAND_MALE_DATA_GET :
         if( Size != sizeof( TBat2MegaviCommand)){
            return;
         }
         ReplyMaleDataGet();
         return;

      case BAT2_MEGAVI_COMMAND_FEMALE_DATA_GET :
         if( Size != sizeof( TBat2MegaviCommand)){
            return;
         }
         ReplyFemaleDataGet();
         return;

      case BAT2_MEGAVI_COMMAND_SMS_SEND :
         if( Size != sizeof( TBat2MegaviCommandSmsSend)){
            return;
         }
         ReplySmsSend( &Command->SmsSend.Sms);
         return;

      case BAT2_MEGAVI_COMMAND_SMS_STATUS_GET :
         if( Size != sizeof( TBat2MegaviCommandSmsStatusGet)){
            return;
         }
         ReplySmsStatus( Command->SmsStatusGet.Slot);
         return;

      default :
         return;
   }
} // ProcessCommand


//-----------------------------------------------------------------------------
// Version
//-----------------------------------------------------------------------------

static void ReplyVersion( void)
// Reply version
{
TBat2MegaviReplyVersion *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplyVersion *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_VERSION;
   Reply->Minor = MBRIDGE_VERSION_MINOR;
   Reply->Major = MBRIDGE_VERSION_MAJOR;
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplyVersion));
} // ReplyVersion

//-----------------------------------------------------------------------------
// Male data get
//-----------------------------------------------------------------------------

static void ReplyMaleDataGet( void)
// Reply get male data
{
TBat2MegaviReplyData *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplyData *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_MALE_DATA;
   memcpy( &Reply->Data, BatxMaleData(), sizeof( TBat2Data));
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplyData));
} // ReplyMaleDataGet

//-----------------------------------------------------------------------------
// Female data get
//-----------------------------------------------------------------------------

static void ReplyFemaleDataGet( void)
// Reply get female data
{
TBat2MegaviReplyData *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplyData *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_FEMALE_DATA;
   memcpy( &Reply->Data, BatxFemaleData(), sizeof( TBat2Data));
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplyData));
} // ReplyDataGet

//-----------------------------------------------------------------------------
// SMS send
//-----------------------------------------------------------------------------

static void ReplySmsSend( TBat2Sms *Sms)
// Reply SMS send
{
TBat2MegaviReplySmsSend *Reply;
byte                     Slot;

   Slot  = SmsInsert( Sms);
   // get reply buffer :
   Reply = (TBat2MegaviReplySmsSend *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_SMS_SEND;
   Reply->Slot  = Slot;
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplySmsSend));
} // ReplySmsSend

//-----------------------------------------------------------------------------
// SMS status
//-----------------------------------------------------------------------------

static void ReplySmsStatus( byte Slot)
// Reply SMS status for <Slot>
{
TBat2MegaviReplySmsStatus *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplySmsStatus *)MegaviData();
   // prepare reply data :
   Reply->Reply  = BAT2_MEGAVI_REPLY_SMS_STATUS;
   Reply->Status = SmsStatus( Slot);
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplySmsStatus));
} // ReplySmsStatus
