//*****************************************************************************
//
//   Sms.c         SMS memory services
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Sms.h"
#include "System.h"          // Operating system
#include "MBridgeDef.h"      // MBridge application
#include <string.h>

// Sms memory :
typedef struct {
   byte     Status;
   TBat2Sms Sms;
} TSms;

static TSms _Sms[ MBRIDGE_SMS_SLOT_COUNT];
static byte _WriteSlot;
static byte _ReadSlot;

// FIFO macros :
#define SmsFifoInit()                 _ReadSlot = 0; _WriteSlot = 0
#define SmsFifoEmpty()               (_ReadSlot == _WriteSlot)
#define SmsFifoFull()                ((byte)(_ReadSlot + MBRIDGE_SMS_SLOT_COUNT) == _WriteSlot)
#define SmsFifoCount()               (_WriteSlot - _ReadSlot)
#define SmsFifoWriteSlot()           (_WriteSlot++ % MBRIDGE_SMS_SLOT_COUNT)   // get position & move
#define SmsFifoReadSlot()            (_ReadSlot    % MBRIDGE_SMS_SLOT_COUNT)   // don't move position
#define SmsFifoRemove()              (_ReadSlot++)                             // move position only

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SmsInit( void)
// Module init
{
byte  i;
TSms *Sms;

   for( i = 0; i < MBRIDGE_SMS_SLOT_COUNT; i++){
      Sms = &_Sms[ i];
      memset( Sms, 0, sizeof( TSms));
      Sms->Status = BAT2_MEGAVI_SMS_STATUS_EMPTY;
   }
   SmsFifoInit();
} // SmsInit

//-----------------------------------------------------------------------------
// Insert
//-----------------------------------------------------------------------------

byte SmsInsert( TBat2Sms *Sms)
// Insert <Sms> into processing, returns Slot
{
TSms *ActiveSms;
byte  Slot;

   // check for slot status :
   if( SmsFifoFull()){
      return( BAT2_MEGAVI_SMS_SLOT_ERROR);
   }
   // get empty slot :
   Slot      = SmsFifoWriteSlot();
   ActiveSms = &_Sms[ Slot];
   // save message :
   ActiveSms->Status = BAT2_MEGAVI_SMS_STATUS_PENDING;
   memcpy( &ActiveSms->Sms, Sms, sizeof( TBat2Sms));
   return( Slot);
} // SmsInsert

//-----------------------------------------------------------------------------
// Status
//-----------------------------------------------------------------------------

byte SmsStatus( byte Slot)
// Returns SMS <Slot> status
{
   if( Slot > MBRIDGE_SMS_SLOT_COUNT - 1){
      return( BAT2_MEGAVI_SMS_STATUS_UNDEFINED);
   }
   return( _Sms[ Slot].Status);
} // SmsStatus

//-----------------------------------------------------------------------------
// Get phone
//-----------------------------------------------------------------------------

byte SmsPhoneGet( char *Phone)
// Get <Phone> for send, returns Slot
{
byte Slot;

   if( SmsFifoEmpty()){
      return( BAT2_MEGAVI_SMS_SLOT_ERROR);
   }
   Slot = SmsFifoReadSlot();
   memcpy( Phone, _Sms[ Slot].Sms.Phone, BAT2_SMS_PHONE_NUMBER_SIZE);
   return( Slot);
} // SmsPhoneGet

//-----------------------------------------------------------------------------
// Get text
//-----------------------------------------------------------------------------

byte SmsTextGet( char *Text)
// Get <Text> for send, returns Slot
{
byte Slot;

   if( SmsFifoEmpty()){
      return( BAT2_MEGAVI_SMS_SLOT_ERROR);
   }
   Slot = SmsFifoReadSlot();
   memcpy( Text, _Sms[ Slot].Sms.Text, BAT2_SMS_SIZE);
   return( Slot);
} // SmsGet

//-----------------------------------------------------------------------------
// Done
//-----------------------------------------------------------------------------

void SmsDone( byte Slot, byte Status)
// Set SMS <Status> at <Slot>, remove SMS from queue
{
byte ActiveSlot;

   ActiveSlot = SmsFifoReadSlot();
   if( ActiveSlot != Slot){
      return;                          // slot already inactive
   }
   _Sms[ Slot].Status = Status;
   SmsFifoRemove();
} // SmsSatusSet

//-----------------------------------------------------------------------------
// Count
//-----------------------------------------------------------------------------

byte SmsCount( void)
// Returns SMS count ready for send
{
   return( SmsFifoCount());
} // SmsCount
