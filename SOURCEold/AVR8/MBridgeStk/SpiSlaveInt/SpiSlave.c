//*****************************************************************************
//
//   SpiSlave.c  Hardware controller SPI slave
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "SpiSlave.h"

#define SPI_FILLER_DATA    0xFF          // Tx filler data

#define SpiInterruptEnable()  SPCR |=  (1 << SPIE)
#define SpiInterruptDisable() SPCR &= ~(1 << SPIE)

// Local variables :
static byte _RxData[ SPI_RX_BUFFER_SIZE];
static byte _TxData[ SPI_TX_BUFFER_SIZE];

static byte _RxCount;
static byte _TxCount;
static byte _TxDataSize;
static byte *_TxPointer;
static byte *_RxPointer;

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void SpiInit( void)
// Initialize bus
{
   SpiPortInit();
   SPCR  = (1 << SPE) | (SPI_CPOL << CPOL) | (SPI_CPHA << CPHA);
   _TxDataSize = 0;
} // SpiInit

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void SpiRxStart( void)
// Start Rx
{
byte Value;

   Value      = SPDR;                  // flush data
   SPDR       = SPI_FILLER_DATA;       // Tx filler
   _TxCount   = 0;
   _RxCount   = 0;
   _RxPointer = _RxData;
   _TxPointer = _TxData;
   SpiInterruptEnable();
} // SpiRxStart

//-----------------------------------------------------------------------------
// Ready
//-----------------------------------------------------------------------------

TYesNo SpiRxReady( void)
// Returns YES, on Rx data ready
{
   if( !_RxCount){
      return( NO);                     // no data received
   }
   if( !SpiSsGet()){
      return( NO);                     // chipselect still active
   }
   SpiInterruptDisable();              // stop transmission
   return( YES);
} // SpiRxReady

//-----------------------------------------------------------------------------
// Size
//-----------------------------------------------------------------------------

byte SpiRxSize( void)
// Returns received DATA bytes count
{
   return( _RxCount);
} // SpiRxSize

//-----------------------------------------------------------------------------
// Rx Data
//-----------------------------------------------------------------------------

void *SpiRxData( void)
// Returns Rx data buffer address
{
   return( _RxData);
} // SpiRxData

//-----------------------------------------------------------------------------
// Tx Data
//-----------------------------------------------------------------------------

void *SpiTxData( void)
// Returns Tx data buffer address
{
   return( _TxData);
} // SpiTxData

//-----------------------------------------------------------------------------
// Tx
//-----------------------------------------------------------------------------

void SpiTx( byte Size)
// Send <Size>
{
   _TxDataSize = Size;
} // SpiTx

//-----------------------------------------------------------------------------
// SPI Interrupt handler
//-----------------------------------------------------------------------------

ISR( SPI_STC_vect)
{
byte Value;

   // write Tx data first :
   if( _TxCount < _TxDataSize){
      SPDR = *_TxPointer;
      _TxCount++;
      _TxPointer++;
   } else {
      SPDR = SPI_FILLER_DATA;
   }
   // read Rx data :
   Value = SPDR;
   if( _RxCount < SPI_RX_BUFFER_SIZE){
      *_RxPointer = Value;
      _RxCount++;
      _RxPointer++;
   } // else data buffer overrun
} // SpiInterrupt
