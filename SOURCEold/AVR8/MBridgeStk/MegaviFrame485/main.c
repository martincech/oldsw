//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "Uart0.h"           // USART0 - stdio
#include "CStdio.h"          // Console output
#include "Megavi.h"          // MEGAVI protocol


#define TEST_DATA_SIZE   10

static void PrepareTxData( void);
// Prepare data buffer

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Size;
byte *Data;
byte Id;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   StdioInit();
   MegaviInit();
   MegaviRxStart();
   // flash on start :
   StatusLedOn();
   StatusLed1On();
   StatusLed2On();
   StatusLed3On();
   SysDelay( 500);
   StatusLedOff();
   StatusLed1Off();
   StatusLed2Off();
   StatusLed3Off();
   SysDelay( 500);
   cprintf( "Start\n");
   EnableInts();
  // main loop
   forever {
      if( CalButtonPressed()){
         /*
         StatusLedOn();
         PrepareTxData();
         MegaviTx( TEST_DATA_SIZE);
         while( !MegaviTxDone());
         SysDelay( 500);
         */
         StatusLedOff();
         StatusLed1Off();
         StatusLed2Off();
         StatusLed3Off();
      }
      if( !MegaviRxReady()){
         continue;
      }
      StatusLedOn();
      Data = MegaviData();
      Size = MegaviRxSize();
      Id   = MegaviRxId();
      //!!! dump data
      MegaviTx( Id, Size);
      while( !MegaviTxDone());
      StatusLedOff();
   }
   return( 0);
} // main

//-----------------------------------------------------------------------------
// Prepare data
//-----------------------------------------------------------------------------

static void PrepareTxData( void)
// Prepare data buffer
{
word i;
byte *Data;

   Data = MegaviData();
   for( i = 0; i < TEST_DATA_SIZE; i++){
      *Data = i;
      Data++;
   }
} // PrepareTxData
