//*****************************************************************************
//
//   Bat2Execute.c    MBridge Bat2 executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#include "Bat2Execute.h"
#include "System.h"          // Operating system
#include "MBridgeDef.h"      // MBridge application
#include "Bat2Com.h"         // Bat2 communication
#include "Sms.h"             // SMS memory
#include <string.h>

static TBat2Data _Data;

// Local functions :

static void ProcessCommand( TBridgeCommand *Command, byte Size);
// Process <Command>

static void ReplySimple( byte ReplyCode);
// Simple reply

static void ReplySmsGet( void);
// Reply with SMS

static void ReplySmsCount( void);
// Reply with SMS count

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void BatxInit( void)
// Module init
{
byte *p;
byte Size;
byte i;

   //!!!>>>
   p    = (byte *)&_Data;
   Size = sizeof( TBat2Data);
   i    = 0;
   do {
      *p = i;
      p++;
      i++;
   } while( --Size);
   //!!!<<<
   BatcInit();
   BatcRxStart();
} // BatxInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void BatxExecute( void)
// Module executive
{
byte Size;
byte *Data;

   if( !BatcRxReady()){
      return;
   }
   StatusLedOn();
   Data = BatcData();
   Size = BatcRxSize();
   ProcessCommand( (TBridgeCommand *)Data, Size);
   StatusLedOff();
} // BatxExecute

//-----------------------------------------------------------------------------
// Data
//-----------------------------------------------------------------------------

TBat2Data *BatxData( void)
// Returns Bat2 actual data
{
   return( &_Data);
} // BatxData

//*****************************************************************************

//-----------------------------------------------------------------------------
// Process command
//-----------------------------------------------------------------------------

static void ProcessCommand( TBridgeCommand *Command, byte Size)
// Process <Command>
{
   switch( Command->Command){
      case BRIDGE_COMMAND_DATA_SET :
         if( Size != sizeof( TBridgeCommandDataSet)){
            return;
         }
         memcpy( &_Data, &Command->DataSet.Data, sizeof( TBat2Data));
         ReplySimple( BRIDGE_REPLY_DATA_SET);
         return;

      case BRIDGE_COMMAND_SMS_COUNT :
         if( Size != sizeof( TBridgeCommandSmsCount)){
            return;
         }
         ReplySmsCount();
         return;

      case BRIDGE_COMMAND_SMS_GET :
         if( Size != sizeof( TBridgeCommandSmsGet)){
            return;
         }
         ReplySmsGet();
         return;

      case BRIDGE_COMMAND_SMS_STATUS :
         if( Size != sizeof( TBridgeCommandSmsStatus)){
            return;
         }
         SmsDone( Command->SmsStatus.Slot, Command->SmsStatus.Status);
         ReplySimple( BRIDGE_REPLY_SMS_STATUS);
         return;

      default :
         return;
   }
} // ProcessCommand

//-----------------------------------------------------------------------------
// Simple reply
//-----------------------------------------------------------------------------

static void ReplySimple( byte ReplyCode)
// Simple reply
{
TBridgeReplySimple *Reply;

   // get reply buffer :
   Reply = (TBridgeReplySimple *)BatcData();
   // prepare reply data :
   Reply->Reply = ReplyCode;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySimple));
} // ReplySimple

//-----------------------------------------------------------------------------
// SMS get reply
//-----------------------------------------------------------------------------

static void ReplySmsGet( void)
// Reply with SMS
{
TBridgeReplySmsGet *Reply;
byte                Slot;

   // get reply buffer :
   Reply = (TBridgeReplySmsGet *)BatcData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_GET;
   Slot         = SmsGet( &Reply->Sms);
   if( Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      memset( &Reply->Sms, 0, sizeof( TBat2Sms));  // SMS memory empty
   }
   Reply->Slot  = Slot;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsGet));
} // ReplySmsGet

//-----------------------------------------------------------------------------
// SMS count reply
//-----------------------------------------------------------------------------

static void ReplySmsCount( void)
// Reply with SMS count
{
TBridgeReplySmsCount *Reply;

   // get reply buffer :
   Reply = (TBridgeReplySmsCount *)BatcData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_COUNT;
   Reply->Count = SmsCount();
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsCount));
} // ReplySmsCount
