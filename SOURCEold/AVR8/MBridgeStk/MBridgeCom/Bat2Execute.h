//*****************************************************************************
//
//   Bat2Execute.h    MBridge Bat2 executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bat2Execute_H__
   #define __Bat2Execute_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Bat2Def_H__
   #include "Bat2Def.h"
#endif

void BatxInit( void);
// Module init

void BatxExecute( void);
// Module executive

TBat2Data *BatxData( void);
// Returns Bat2 actual data

#endif
