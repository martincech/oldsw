//*****************************************************************************
//
//    Hardware.h   MBridge hardware descriptions (STK500)
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED    0

#define StatusLedInit()  DDRC  |=  (1 << StatusLED)

#define StatusLedOn()    PORTC &= ~(1 << StatusLED)
#define StatusLedOff()   PORTC |=  (1 << StatusLED)

//------------------------------------------------------------------------------
// MEGAVI
//------------------------------------------------------------------------------

#define MEGAVI_COM0

#define MEGAVI_BAUD          19200      // baud rate
#define MEGAVI_BUFFER_SIZE   250        // maximum packet size
#define MEGAVI_TX_SPACE      2          // wait before Tx [ms]

// RS485 Tx control :
#define MegaviTxInit()
#define MegaviTxEnable()
#define MegaviTxDisable()

//------------------------------------------------------------------------------
// Bat2 communication
//------------------------------------------------------------------------------

#define BATC_COM1

#define BATC_BAUD            9600      // baud rate
#define BATC_BUFFER_SIZE     250        // maximum packet size

//-----------------------------------------------------------------------------

#endif
