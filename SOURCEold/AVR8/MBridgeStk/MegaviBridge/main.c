//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "Uart1.h"           // USART1 - stdio
#include "CStdio.h"          // Console output
#include "MegaviExecute.h"   // MEGAVI executive

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart1Init();
   StdioInit();
   MexInit();
   // flash on start :
   StatusLedOn();
   StatusLed1On();
   StatusLed2On();
   StatusLed3On();
   SysDelay( 500);
   StatusLedOff();
   StatusLed1Off();
   StatusLed2Off();
   StatusLed3Off();
   SysDelay( 500);
   cprintf( "Start\n");
   EnableInts();
   // main loop
   forever {
      if( CalButtonPressed()){
         /*
         StatusLedOn();
         PrepareTxData();
         MegaviTx( TEST_DATA_SIZE);
         while( !MegaviTxDone());
         SysDelay( 500);
         */
         StatusLedOff();
         StatusLed1Off();
         StatusLed2Off();
         StatusLed3Off();
      }
      MexExecute();
   }
   return( 0);
} // main

