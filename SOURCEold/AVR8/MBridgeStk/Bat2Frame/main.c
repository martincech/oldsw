//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "Uart1.h"           // USART1 - stdio
#include "CStdio.h"          // Console output
#include "Bat2Com.h"         // Bat2 communication

#define TEST_DATA_SIZE  10

static void PrepareTxData( void);
// Prepare data buffer

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte *Data;
byte  Size;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart1Init();
   StdioInit();
   // initialize executives :
   BatcInit();
   // flash on start :
   StatusLedOn();
   StatusLed1On();
   StatusLed2On();
   StatusLed3On();
   SysDelay( 500);
   StatusLedOff();
   StatusLed1Off();
   StatusLed2Off();
   StatusLed3Off();
   SysDelay( 500);
   cprintf( "Start\n");
   EnableInts();
   // main loop
   forever {
      if( CalButtonPressed()){
         StatusLedOn();
         PrepareTxData();
         BatcTx( TEST_DATA_SIZE);
         while( !BatcTxDone());
         SysDelay( 500);
         StatusLedOff();
         StatusLed1Off();
         StatusLed2Off();
         StatusLed3Off();
      }
      if( !BatcRxReady()){
         continue;
      }
      StatusLedOn();
      Data = BatcData();
      Size = BatcRxSize();
      //!!! dump data
      BatcTx( Size);
      while( !BatcTxDone());
      StatusLedOff();
   }
   return( 0);
} // main


//-----------------------------------------------------------------------------
// Prepare data
//-----------------------------------------------------------------------------

static void PrepareTxData( void)
// Prepare data buffer
{
word i;
byte *Data;

   Data = BatcData();
   for( i = 0; i < TEST_DATA_SIZE; i++){
      *Data = i;
      Data++;
   }
} // PrepareTxData
