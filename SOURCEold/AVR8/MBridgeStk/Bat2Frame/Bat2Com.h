//*****************************************************************************
//
//    Bat2Com.h     Bat2 bridge protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Bat2Com_H__
   #define __Bat2Com_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __BridgeDef_H__
   #include "BridgeDef.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void BatcInit( void);
// Communication initialisation

void BatcRxStart( void);
// Start reception

TYesNo BatcRxReady( void);
// Returns YES, on Rx data ready

byte BatcRxSize( void);
// Returns received DATA bytes count

void *BatcData( void);
// Returns data buffer address

void BatcTx( byte Size);
// Start transmition with <Id> and <Size>

TYesNo BatcTxDone( void);
// Wait for transmission done

#endif
