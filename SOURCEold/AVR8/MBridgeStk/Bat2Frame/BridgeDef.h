//*****************************************************************************
//
//   BridgeDef.h   Bat2 Bridge protocol
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bridge_H__
   #define __Bridge_H__

#ifndef __Bat2Def_H__
   #include "Bat2Def.h"
#endif

#ifndef __Bat2SmsDef_H__
   #include "Bat2SmsDef.h"
#endif

//-----------------------------------------------------------------------------
// Protocol
//-----------------------------------------------------------------------------

// <Leader><Size1><Size2><DATA><CRC>
//
// CRC is bit inversion of Sum <Size1>,<Size2>,<DATA>

#define BRIDGE_LEADER_CHAR    0x55

#define BRIDGE_LEADER_OFFSET  0
#define BRIDGE_SIZE1_OFFSET   1
#define BRIDGE_SIZE2_OFFSET   2
#define BRIDGE_DATA_OFFSET    3
#define BRIDGE_CRC_OFFSET     3

#define BRIDGE_FRAME_SIZE     4

//-----------------------------------------------------------------------------
// Commands
//-----------------------------------------------------------------------------

typedef enum {
   BRIDGE_COMMAND_UNDEFINED,
   BRIDGE_COMMAND_DATA_SET,
   BRIDGE_COMMAND_SMS_COUNT,
   BRIDGE_COMMAND_SMS_GET,
   BRIDGE_COMMAND_SMS_STATUS,
   _BRIDGE_COMMAND_LAST
} EBridgeCommand;

//-----------------------------------------------------------------------------
// Simple command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;
} TBridgeCommandSimple;

typedef TBridgeCommandSimple TBridgeCommandSmsCount;
typedef TBridgeCommandSimple TBridgeCommandSmsGet;

//-----------------------------------------------------------------------------
// Send data
//-----------------------------------------------------------------------------

typedef struct {
   byte      Command;
   TBat2Data Data;
} TBridgeCommandDataSet;

//-----------------------------------------------------------------------------
// Send SMS status
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;
   byte Status;
} TBridgeCommandSmsStatus;

//-----------------------------------------------------------------------------
// Command union
//-----------------------------------------------------------------------------

typedef union {
   byte                    Command;
   TBridgeCommandDataSet   DataSet;
   TBridgeCommandSmsCount  SmsCount;
   TBridgeCommandSmsGet    SmsGet;
   TBridgeCommandSmsStatus SmsStatus;
} TBridgeCommand;

//-----------------------------------------------------------------------------
// Replies
//-----------------------------------------------------------------------------

#define BRIDGE_REPLY_BASE  0x80

typedef enum {
   BRIDGE_REPLY_UNDEFINED = BRIDGE_REPLY_BASE,
   BRIDGE_REPLY_DATA_SET,
   BRIDGE_REPLY_SMS_COUNT,
   BRIDGE_REPLY_SMS_GET,
   BRIDGE_REPLY_SMS_STATUS,
   _BRIDGE_REPLY_LAST
} EBridgeReply;

//-----------------------------------------------------------------------------
// Simple reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;
} TBridgeReplySimple;

typedef TBridgeReplySimple TBridgeReplyDataSet;
typedef TBridgeReplySimple TBridgeReplySmsStatus;

//-----------------------------------------------------------------------------
// Reply SMS count
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;
   byte Count;
} TBridgeReplySmsCount;

//-----------------------------------------------------------------------------
// Reply SMS get
//-----------------------------------------------------------------------------

typedef struct {
   byte     Reply;
   TBat2Sms Sms;
} TBridgeReplySmsGet;

//-----------------------------------------------------------------------------
// Reply union
//-----------------------------------------------------------------------------

typedef union {
   byte                  Reply;
   TBridgeReplyDataSet   DataSet;
   TBridgeReplySmsCount  SmsCount;
   TBridgeReplySmsGet    SmsGet;
   TBridgeReplySmsStatus SmsStatus;
} TBridgeReply;

#endif
