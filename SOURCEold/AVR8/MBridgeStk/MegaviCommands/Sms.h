//*****************************************************************************
//
//   Sms.c         SMS memory services
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Sms_H__
   #define __Sms_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Bat2SmsDef_H__
   #include "Bat2SmsDef.h"
#endif

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SmsInit( void);
// Module init

//-----------------------------------------------------------------------------
// Writer
//-----------------------------------------------------------------------------

byte SmsInsert( TBat2Sms *Sms);
// Insert <Sms> into processing, returns Slot

byte SmsStatus( byte Slot);
// Returns SMS <Slot> status

//-----------------------------------------------------------------------------
// Reader
//-----------------------------------------------------------------------------

TYesNo SmsGet( TBat2Sms *Sms);
// Get <Sms> for send

void SmsDone( byte Status);
// Set SMS <Status>, remove SMS from queue

byte SmsCount( void);
// Returns SMS count ready for send

#endif
