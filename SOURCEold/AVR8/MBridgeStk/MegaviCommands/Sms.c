//*****************************************************************************
//
//   Sms.c         SMS memory services
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Sms.h"
#include "System.h"          // Operating system
#include "MBridgeDef.h"      // MBridge application
#include <string.h>

// Sms memory :
typedef struct {
   byte     Status;
   TBat2Sms Sms;
} TSms;

static TSms _Sms[ MBRIDGE_SMS_SLOT_COUNT];
static byte _WriteSlot;
static byte _ReadSlot;

// FIFO macros :
#define SmsFifoInit()                 _ReadSlot = 0; _WriteSlot = 0
#define SmsFifoEmpty()               (_ReadSlot == _WriteSlot)
#define SmsFifoFull()                ((byte)(_ReadSlot + MBRIDGE_SMS_SLOT_COUNT) == _WriteSlot)
#define SmsFifoCount()               (_WriteSlot - _ReadSlot)
#define SmsFifoWriteSlot()           (_WriteSlot++ % MBRIDGE_SMS_SLOT_COUNT)   // get position & move
#define SmsFifoReadSlot()            (_ReadSlot    % MBRIDGE_SMS_SLOT_COUNT)   // don't move position
#define SmsFifoRemove()              (_ReadSlot++)                             // move position only

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SmsInit( void)
// Module init
{
byte  i;
TSms *Sms;

   for( i = 0; i < MBRIDGE_SMS_SLOT_COUNT; i++){
      Sms = &_Sms[ i];
      memset( Sms, 0, sizeof( TSms));
      Sms->Status = BAT2_MEGAVI_SMS_STATUS_EMPTY;
   }
   SmsFifoInit();
} // SmsInit

//-----------------------------------------------------------------------------
// Insert
//-----------------------------------------------------------------------------

byte SmsInsert( TBat2Sms *Sms)
// Insert <Sms> into processing, returns Slot
{
TSms *ActiveSms;
byte  Slot;

   // check for slot status :
   if( SmsFifoFull()){
      return( BAT2_MEGAVI_SMS_SLOT_ERROR);
   }
   // get empty slot :
   Slot      = SmsFifoWriteSlot();
   ActiveSms = &_Sms[ Slot];
   // save message :
   ActiveSms->Status = BAT2_MEGAVI_SMS_STATUS_PENDING;
   memcpy( &ActiveSms->Sms, Sms, sizeof( TBat2Sms));
   return( Slot);
} // SmsInsert

//-----------------------------------------------------------------------------
// Status
//-----------------------------------------------------------------------------

byte SmsStatus( byte Slot)
// Returns SMS <Slot> status
{
   if( Slot > MBRIDGE_SMS_SLOT_COUNT - 1){
      return( BAT2_MEGAVI_SMS_STATUS_UNDEFINED);
   }
   return( _Sms[ Slot].Status);
} // SmsStatus

//-----------------------------------------------------------------------------
// Get
//-----------------------------------------------------------------------------

TYesNo SmsGet( TBat2Sms *Sms)
// Get <Sms> for send
{
byte Slot;

   if( SmsFifoEmpty()){
      return( NO);
   }
   Slot = SmsFifoReadSlot();
   memcpy( Sms, &_Sms[ Slot], sizeof( TBat2Sms));
   return( YES);
} // SmsGet

//-----------------------------------------------------------------------------
// Done
//-----------------------------------------------------------------------------

void SmsDone( byte Status)
// Set SMS <Status>, remove SMS from queue
{
byte Slot;

   Slot = SmsFifoReadSlot();
   _Sms[ Slot].Status = Status;
   SmsFifoRemove();
} // SmsSatusSet

//-----------------------------------------------------------------------------
// Count
//-----------------------------------------------------------------------------

byte SmsCount( void)
// Returns SMS count ready for send
{
   return( SmsFifoCount());
} // SmsCount
