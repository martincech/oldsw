//*****************************************************************************
//
//   Bat2Execute.c    MBridge Bat2 executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#include "Bat2Execute.h"
#include "System.h"          // Operating system
#include "MBridgeDef.h"      // MBridge application

static TBat2Data _Data;

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void BatxInit( void)
// Module init
{
byte *p;
byte Size;
byte i;

   //!!!>>>
   p    = (byte *)&_Data;
   Size = sizeof( TBat2Data);
   i    = 0;
   do {
      *p = i;
      p++;
      i++;
   } while( --Size);
   //!!!<<<
} // BatxInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void BatxExecute( void)
// Module executive
{
} // BatxExecute

//-----------------------------------------------------------------------------
// Data
//-----------------------------------------------------------------------------

TBat2Data *BatxData( void)
// Returns Bat2 actual data
{
   return( &_Data);
} // BatxData

//*****************************************************************************
