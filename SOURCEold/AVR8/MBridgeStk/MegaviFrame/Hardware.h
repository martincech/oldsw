//*****************************************************************************
//
//    Hardware.h  - Bat2 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED    0
#define StatusLED1   1
#define StatusLED2   2
#define StatusLED3   3

#define StatusLedInit()  DDRC  |=  (1 << StatusLED) | (1 << StatusLED1) | (1 << StatusLED2) | (1 << StatusLED3)

#define StatusLedOn()    PORTC &= ~(1 << StatusLED)
#define StatusLedOff()   PORTC |=  (1 << StatusLED)

#define StatusLed1On()   PORTC &= ~(1 << StatusLED1)
#define StatusLed1Off()  PORTC |=  (1 << StatusLED1)

#define StatusLed2On()   PORTC &= ~(1 << StatusLED2)
#define StatusLed2Off()  PORTC |=  (1 << StatusLED2)

#define StatusLed3On()   PORTC &= ~(1 << StatusLED3)
#define StatusLed3Off()  PORTC |=  (1 << StatusLED3)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           9600       // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

//------------------------------------------------------------------------------
// UART1
//------------------------------------------------------------------------------

#define UART1_BAUD           9600       // baud rate
#define UART1_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM1

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   0

#define CalButtonInit()     DDRA  &= ~(1 << CalButton)

#define CalButtonPressed()  (!(PINA & (1 << CalButton)))       // active L


//------------------------------------------------------------------------------
// MEGAVI
//------------------------------------------------------------------------------

#define MEGAVI_COM0

#define MEGAVI_BAUD          19200      // baud rate
#define MEGAVI_RX_TIMEOUT    50000      // intercharacter timeout [us]

#define MEGAVI_BUFFER_SIZE   250        // maximum packet size
#define MEGAVI_TX_SPACE      1          // wait before Tx [ms]

// RS485 Tx control :
#define MegaviTxInit()
#define MegaviTxEnable()
#define MegaviTxDisable()

//-----------------------------------------------------------------------------

#endif
