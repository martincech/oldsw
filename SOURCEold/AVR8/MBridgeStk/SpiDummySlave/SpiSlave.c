//*****************************************************************************
//
//   SpiSlave.c  Hardware controller SPI slave
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "SpiSlave.h"

#if   (FXTAL / SPI_CLOCK) == 2
   #define SPI_CLOCK_DIVISOR  0
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 4
   #define SPI_CLOCK_DIVISOR  0
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 8
   #define SPI_CLOCK_DIVISOR  1
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 16
   #define SPI_CLOCK_DIVISOR  1
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 32
   #define SPI_CLOCK_DIVISOR  2
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 64
   #define SPI_CLOCK_DIVISOR  2
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 128
   #define SPI_CLOCK_DIVISOR  3
   #define SPI_CLOCK_X2       0
#else
   #error "Invalid SPI_CLOCK value"
#endif

static byte _Data[ SPI_BUFFER_SIZE];
static byte _DataSize;

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void SpiInit( void)
// Initialize bus
{
   SpiPortInit();
   SPCR  = (1 << SPE) | (SPI_CPOL << CPOL) | (SPI_CPHA << CPHA) | 
           (SPI_CLOCK_DIVISOR << SPR0);
   SPSR |= (SPI_CLOCK_X2 << SPI2X);
} // SpiInit

//-----------------------------------------------------------------------------
// Ready
//-----------------------------------------------------------------------------

TYesNo SpiRxReady( void)
// Returns YES, on Rx data ready
{
   if( !(SPSR & (1 << SPIF))){
      return( NO);
   }
   _Data[ 0] = SPDR;
   _DataSize = 1;
   return( YES);
} // SpiRxReady

//-----------------------------------------------------------------------------
// Size
//-----------------------------------------------------------------------------

byte SpiRxSize( void)
// Returns received DATA bytes count
{
   return( _DataSize);
} // SpiRxSize

//-----------------------------------------------------------------------------
// Data
//-----------------------------------------------------------------------------

void *SpiData( void)
// Returns data buffer address
{
   return( _Data);
} // SpiData

//-----------------------------------------------------------------------------
// Tx
//-----------------------------------------------------------------------------

void SpiTx( byte Value)
// Send <Value>
{
   SPDR = Value;
} // SpiTx
