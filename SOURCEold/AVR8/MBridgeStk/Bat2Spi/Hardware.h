//*****************************************************************************
//
//    Hardware.h   MBridge hardware descriptions (STK500)
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED    0

#define StatusLedInit()  DDRC  |=  (1 << StatusLED)

#define StatusLedOn()    PORTC &= ~(1 << StatusLED)
#define StatusLedOff()   PORTC |=  (1 << StatusLED)

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   0

#define CalButtonInit()     DDRA  &= ~(1 << CalButton)

#define CalButtonPressed()  (!(PINA & (1 << CalButton)))       // active L

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

// PD0 - RXD, PD1 - TXD

#define UART0_BAUD           9600       // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM0

//------------------------------------------------------------------------------
// UART1
//------------------------------------------------------------------------------

// PD2 - RXD, PD3 - TXD

#define UART1_BAUD           9600       // baud rate
#define UART1_RX_TIMEOUT     10000      // intercharacter timeout [us]

//------------------------------------------------------------------------------
// MEGAVI
//------------------------------------------------------------------------------

// PD0 - RXD, PD1 - TXD

#define MEGAVI_COM0

#define MEGAVI_BAUD          19200      // baud rate
#define MEGAVI_BUFFER_SIZE   250        // maximum packet size
#define MEGAVI_TX_SPACE      2          // wait before Tx [ms]

// RS485 Tx control :
#define MegaviTxInit()
#define MegaviTxEnable()
#define MegaviTxDisable()

//------------------------------------------------------------------------------
// Bat2 communication
//------------------------------------------------------------------------------

// PD2 - RXD, PD3 - TXD

#define BATC_COM1

#define BATC_BAUD            9600      // baud rate
#define BATC_BUFFER_SIZE     250        // maximum packet size

//------------------------------------------------------------------------------
// SPI Slave
//------------------------------------------------------------------------------

// PB4 - SS, PB7 - SCK, PB5 - MOSI, PB6 - MISO

#define SPI_SS   4
#define SPI_MOSI 5
#define SPI_MISO 6
#define SPI_SCK  7


#define SpiPortInit()    DDRB &= ~((1 << SPI_SS) | (1 << SPI_SCK) | (1 << SPI_MOSI));\
                         DDRB |=   (1 << SPI_MISO)


#define SpiSsGet()       (PINB & (1 << SPI_SS))

#define SPI_CLOCK        2000000L         // SPI clock [Hz]
#define SPI_CPOL         1                // idle clock polarity
#define SPI_CPHA         1                // clock phase leading/trailing edge

#define SPI_RX_BUFFER_SIZE  128
#define SPI_TX_BUFFER_SIZE  128

//-----------------------------------------------------------------------------

#endif
