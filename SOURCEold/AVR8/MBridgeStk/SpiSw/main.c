//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "CStdio.h"
#include "Uart0.h"
#include "Spi.h"

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Char;
byte Value;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   StdioInit();
   SpiInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   // send data on start :
   cprintf( " Start\n");
   // main loop
   forever {
      if( CalButtonPressed()){
         StatusLedOn();
         SysDelay( 50);
         StatusLedOff();
         cprintf( "Button\n");
      }
      if( !Uart0RxChar( &Char)){
         continue;                     // timeout
      }
      switch( Char){
         case 's' :
            SpiAttach();
            break;

         case 'e' :
            SpiRelease();
            break;

         case 'r' :
            Value = SpiByteRead();
            cprintf( "Rx : %02X\n", Value);
            break;

         case 'w' :
            Value = 0xD7;
            SpiByteWrite( Value);
            cprintf( "Tx : %02X\n", Value);
            break;

         case 't' :
            Value = 0xD7;
            Value = SpiByteTransceive( Value);
            cprintf( "Rx : %02X\n", Value);
            break;

         case 'm' :
            SpiAttach();
            SpiByteWrite( 0xD7);
            Value = SpiByteRead();
            cprintf( "Status : %02X\n", Value);
            SpiRelease();
            break;
            

         default :
            break;
      }
      Uart0TxChar( Char);              // echo character
   }
   return( 0);
} // main
