//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "Uart0.h"
#include "Uart1.h"

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Char;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   Uart1Init();
   // flash on start :
   StatusLedOn();
   StatusLed1On();
   StatusLed2On();
   StatusLed3On();
   SysDelay( 500);
   StatusLedOff();
   StatusLed1Off();
   StatusLed2Off();
   StatusLed3Off();
   SysDelay( 500);
   // send data on start :
   Uart1TxChar( 'A');
   Uart1TxChar( 'B');
   Uart1TxChar( 'C');
   // main loop
   forever {
      if( CalButtonPressed()){
         StatusLedOn();
         SysDelay( 50);
         StatusLedOff();
         Uart0TxChar( 'X');
         Uart1TxChar( 'X');
         Uart1FlushChars();
      }
      if( !Uart1RxChar( &Char)){
         continue;                     // timeout
      }
      Uart0TxChar( Char);
      Uart1TxChar( Char);              // echo character
      Uart1FlushChars();
   }
   return( 0);
} // main
