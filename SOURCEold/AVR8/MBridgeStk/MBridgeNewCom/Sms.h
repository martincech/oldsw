//*****************************************************************************
//
//   Sms.c         SMS memory services
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Sms_H__
   #define __Sms_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Bat2SmsDef_H__
   #include "Bat2SmsDef.h"
#endif

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SmsInit( void);
// Module init

//-----------------------------------------------------------------------------
// Writer
//-----------------------------------------------------------------------------

byte SmsInsert( TBat2Sms *Sms);
// Insert <Sms> into processing, returns Slot

byte SmsStatus( byte Slot);
// Returns SMS <Slot> status

//-----------------------------------------------------------------------------
// Reader
//-----------------------------------------------------------------------------

byte SmsPhoneGet( char *Phone);
// Get <Phone> for send, returns Slot

byte SmsTextGet( char *Text);
// Get <Text> for send, returns Slot

void SmsDone( byte Slot, byte Status);
// Set SMS <Status> at <Slot>, remove SMS from queue

byte SmsCount( void);
// Returns SMS count ready for send

#endif
