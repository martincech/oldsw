//*****************************************************************************
//
//   MegaviExecute.h  MBridge MEGAVI executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#ifndef __MegaviExecute_H__
   #define __MegaviExecute_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void MexInit( void);
// Module init

void MexExecute( void);
// Module executive

#endif
