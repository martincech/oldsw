//*****************************************************************************
//
//   Bat2Execute.c    MBridge Bat2 executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#include "Bat2Execute.h"
#include "System.h"          // Operating system
#include "Endian.h"          // Endian swap
#include "MBridgeDef.h"      // MBridge application
#include "Bat2Com.h"         // Bat2 communication
#include "Sms.h"             // SMS memory
#include <string.h>

static TBat2Data _MaleData;
static TBat2Data _FemaleData;

// Local functions :

static void ProcessCommand( TBridgeCommand *Command, byte Size);
// Process <Command>

static void ReplySimple( byte ReplyCode);
// Simple reply

static void ReplyMaleDataSet( TBat2Data *Data);
// Save male data and reply

static void ReplyFemaleDataSet( TBat2Data *Data);
// Save female data and reply

static void ReplySmsPhoneGet( void);
// Reply with SMS phone number

static void ReplySmsTextGet( void);
// Reply with SMS text

static void ReplySmsCount( void);
// Reply with SMS count

static void DataEndianSwap( TBat2Data *Data);
// Swap <Data> endian

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void BatxInit( void)
// Module init
{
byte *p;
byte Size;
byte i;

   //!!!>>>
   p    = (byte *)&_MaleData;
   Size = sizeof( TBat2Data);
   i    = 0;
   do {
      *p = i;
      p++;
      i++;
   } while( --Size);
   //!!!<<<
   p    = (byte *)&_FemaleData;
   Size = sizeof( TBat2Data);
   i    = 0;
   do {
      *p = i | 0x80;
      p++;
      i++;
   } while( --Size);
   //!!!<<<
   BatcInit();
   BatcRxStart();
} // BatxInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void BatxExecute( void)
// Module executive
{
byte Size;
byte *Data;

   if( !BatcRxReady()){
      return;
   }
   StatusLedOn();
   Data = BatcData();
   Size = BatcRxSize();
   ProcessCommand( (TBridgeCommand *)Data, Size);
   StatusLedOff();
} // BatxExecute

//-----------------------------------------------------------------------------
// Male data
//-----------------------------------------------------------------------------

TBat2Data *BatxMaleData( void)
// Returns Bat2 actual male data
{
   return( &_MaleData);
} // BatxMaleData

//-----------------------------------------------------------------------------
// Female data
//-----------------------------------------------------------------------------

TBat2Data *BatxFemaleData( void)
// Returns Bat2 actual female data
{
   return( &_FemaleData);
} // BatxFemaleData

//*****************************************************************************

//-----------------------------------------------------------------------------
// Process command
//-----------------------------------------------------------------------------

static void ProcessCommand( TBridgeCommand *Command, byte Size)
// Process <Command>
{
   switch( Command->Command){
      case BRIDGE_COMMAND_MALE_DATA_SET :
         if( Size != sizeof( TBridgeCommandDataSet)){
            return;
         }
         ReplyMaleDataSet( &Command->DataSet.Data);
         return;

      case BRIDGE_COMMAND_FEMALE_DATA_SET :
         if( Size != sizeof( TBridgeCommandDataSet)){
            return;
         }
         ReplyFemaleDataSet( &Command->DataSet.Data);
         return;

      case BRIDGE_COMMAND_SMS_COUNT :
         if( Size != sizeof( TBridgeCommandSmsCount)){
            return;
         }
         ReplySmsCount();
         return;

      case BRIDGE_COMMAND_SMS_PHONE_GET :
         if( Size != sizeof( TBridgeCommandSmsPhoneGet)){
            return;
         }
         ReplySmsPhoneGet();
         return;

      case BRIDGE_COMMAND_SMS_TEXT_GET :
         if( Size != sizeof( TBridgeCommandSmsTextGet)){
            return;
         }
         ReplySmsTextGet();
         return;

      case BRIDGE_COMMAND_SMS_STATUS :
         if( Size != sizeof( TBridgeCommandSmsStatus)){
            return;
         }
         SmsDone( Command->SmsStatus.Slot, Command->SmsStatus.Status);
         ReplySimple( BRIDGE_REPLY_SMS_STATUS);
         return;

      default :
         return;
   }
} // ProcessCommand

//-----------------------------------------------------------------------------
// Simple reply
//-----------------------------------------------------------------------------

static void ReplySimple( byte ReplyCode)
// Simple reply
{
TBridgeReplySimple *Reply;

   // get reply buffer :
   Reply = (TBridgeReplySimple *)BatcData();
   // prepare reply data :
   Reply->Reply = ReplyCode;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySimple));
} // ReplySimple

//-----------------------------------------------------------------------------
// Male data set
//-----------------------------------------------------------------------------

static void ReplyMaleDataSet( TBat2Data *Data)
// Save male data and reply
{
   memcpy( &_MaleData, Data, sizeof( TBat2Data));
   DataEndianSwap( &_MaleData);
   ReplySimple( BRIDGE_REPLY_MALE_DATA_SET);
} // ReplyMaleDataSet

//-----------------------------------------------------------------------------
// Data set
//-----------------------------------------------------------------------------

static void ReplyFemaleDataSet( TBat2Data *Data)
// Save female data and reply
{
   memcpy( &_FemaleData, Data, sizeof( TBat2Data));
   DataEndianSwap( &_FemaleData);
   ReplySimple( BRIDGE_REPLY_FEMALE_DATA_SET);
} // ReplyDataSet

//-----------------------------------------------------------------------------
// SMS get phone number
//-----------------------------------------------------------------------------

static void ReplySmsPhoneGet( void)
// Reply with SMS phone number
{
TBridgeReplySmsPhoneGet *Reply;
byte                     Slot;

   // get reply buffer :
   Reply = (TBridgeReplySmsPhoneGet *)BatcData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_PHONE_GET;
   Slot         = SmsPhoneGet( Reply->Phone);
   if( Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      memset( &Reply->Phone, 0, BAT2_SMS_PHONE_NUMBER_SIZE);  // SMS memory empty
   }
   Reply->Slot  = Slot;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsPhoneGet));
} // ReplySmsPhoneGet

//-----------------------------------------------------------------------------
// SMS get text
//-----------------------------------------------------------------------------

static void ReplySmsTextGet( void)
// Reply with SMS text
{
TBridgeReplySmsTextGet *Reply;
byte                    Slot;

   // get reply buffer :
   Reply = (TBridgeReplySmsTextGet *)BatcData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_TEXT_GET;
   Slot         = SmsTextGet( Reply->Text);
   if( Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      memset( &Reply->Text, 0, BAT2_SMS_SIZE);  // SMS memory empty
   }
   Reply->Slot  = Slot;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsTextGet));
} // ReplySmsTextGet

//-----------------------------------------------------------------------------
// SMS count reply
//-----------------------------------------------------------------------------

static void ReplySmsCount( void)
// Reply with SMS count
{
TBridgeReplySmsCount *Reply;

   // get reply buffer :
   Reply = (TBridgeReplySmsCount *)BatcData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_COUNT;
   Reply->Count = SmsCount();
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsCount));
} // ReplySmsCount

//-----------------------------------------------------------------------------
// Endian swap
//-----------------------------------------------------------------------------

static void DataEndianSwap( TBat2Data *Data)
// Swap <Data> endian
{
   //!!! TODO
} // DataEndianSwap
