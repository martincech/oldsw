//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "MegaviExecute.h"   // MEGAVI executive
#include "Bat2Execute.h"     // Bat2 executive

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
   // initialize devices :
   StatusLedInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // initialize executives :
   MexInit();
   BatxInit();
   EnableInts();
   // main loop
   forever {
      BatxExecute();
      MexExecute();
   }
   return( 0);
} // main
