//*****************************************************************************
//
//   MBridgeDef.h  MBridge project definitions
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __MBridge_H__
   #define __MBridge_H__

#ifndef __MegaviUnion_H__
   #include "MegaviUnion.h"
#endif

//-----------------------------------------------------------------------------
// Version
//-----------------------------------------------------------------------------

#define MBRIDGE_VERSION_MINOR  0x00
#define MBRIDGE_VERSION_MAJOR  0x01

#define MBRIDGE_VERSION        ((MBRIDGE_VERSION_MAJOR << 8) | (MBRIDGE_VERSION_MINOR))

//-----------------------------------------------------------------------------
// Constants
//-----------------------------------------------------------------------------

#define MBRIDGE_MEGAVI_ADDRESS 0x0A

#define MBridgeAddressMatch( a)  ((a) == MBRIDGE_MEGAVI_ADDRESS)

#define MBRIDGE_SMS_SLOT_COUNT 10

#endif
