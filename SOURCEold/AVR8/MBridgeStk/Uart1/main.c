//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "Uart1.h"

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Char;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart1Init();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   Uart1TxChar( 'A');
   Uart1TxChar( 'B');
   Uart1TxChar( 'C');
   // main loop
   forever {
      if( CalButtonPressed()){
         StatusLedOn();
         SysDelay( 50);
         StatusLedOff();
         Uart1TxChar( 'X');            // button pressed
      }
      if( !Uart1RxChar( &Char)){
         continue;                     // timeout
      }
      Uart1TxChar( Char);              // echo character
   }
   return( 0);
} // main
