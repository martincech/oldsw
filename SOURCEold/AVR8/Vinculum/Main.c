//*****************************************************************************
//
//    Main.c         Vinculum main
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "../inc/System.h"  // Operating System
#include "Vnc.h"            // Vinculum
#define __COM0__
#include "../inc/Com.h"     // UART


//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------

int main( void)
{
byte ch;

   StatusLedInit();
   StatusLedOff();
   ComInit();
   VncInit();
   // flash :
   StatusLed0On();
   SysDelay( 500);
   StatusLed0Off();
   ComTxChar( 'S');
   ComTxChar( 'T');
   forever {
      if( VncReceiveData( &ch)){
         ComTxChar( ch);
      }
      if( !ComRxChar( &ch)){
	      continue;
      }
      if( ch == '?'){
         if( !VncReadStatus( &ch)){
            ComTxChar( '?');
         }
         ComTxChar( ch);
         continue;
      }
      if( !VncSendData( ch)){
         ComTxChar( '*');
      }
   }
} // main
