//*****************************************************************************
//
//    Main.c         QHPanel main
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "../inc/System.h"  // Operating System
#define __COM0__
#include "../inc/Com.h"     // UART


//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------

int main( void)
{
byte ch;

   StatusLedInit();
   StatusLedOff();
   ComInit();
   // flash :
   StatusLed0On();
   SysDelay( 500);
   StatusLed0Off();
   forever {
      if( ComRxChar( &ch)){
	      ComTxChar( ch);
      }
/*
      StatusLed1On();
      SysDelay( 500);
      StatusLed1Off();
      SysDelay( 500);
      ComTxChar( 'A');
      ComTxChar( 'B');
      ComTxChar( 'C');
*/
   }
} // main
