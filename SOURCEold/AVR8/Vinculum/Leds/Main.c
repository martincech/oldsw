//*****************************************************************************
//
//    Main.c         QHPanel main
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "../inc/System.h"  // Operating System


//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------

int main( void)
{
   StatusLedInit();
   StatusLedOff();
   forever {
      StatusLed0On();
      SysDelay( 500);
      StatusLed1On();
      SysDelay( 500);
      StatusLed2On();
      SysDelay( 500);      
      StatusLed3On();
      SysDelay( 500);      
      StatusLedOff();
      SysDelay( 500);      
   }
} // main
