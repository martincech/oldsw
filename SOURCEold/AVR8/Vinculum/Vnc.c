//*****************************************************************************
//
//    Vnc.c          Vinculum interface
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "Vnc.h"

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void VncInit( void)
// Initialize
{
   VncSetSdi();
   VncClrCs();
   VncSetSclk();
   VncPortInit();
} // VncInit

//-----------------------------------------------------------------------------
// Send command
//-----------------------------------------------------------------------------

TYesNo VncSendData( byte Data)
// Send <Data>
{
byte i;
byte Status;

   VncClrSclk();                       // inactive edge
   VncSetCs();                         // chipselect
   VncSetSdi();                        // START
   VncSetSclk();                       // strobe

   VncClrSclk();                       // inactive edge
   VncClrSdi();                        // R/W bit
   VncSetSclk();                       // strobe

   VncClrSclk();                       // inactive edge
   VncClrSdi();                        // ADDR bit
   VncSetSclk();                       // strobe

   // write data :
   i = 8;
   do {
      VncClrSclk();                    // inactive edge
      VncClrSdi();
      if( Data & 0x80){
         VncSetSdi();
      }
      VncSetSclk();                    // next data
      Data <<= 1;
   } while( --i);

   // read status bit :
   VncClrSclk();                       // inactive edge
   Status = VncGetSdo();               // STATUS bit
   VncSetSclk();                       // strobe

   // deselect :
   VncClrSclk();                       // inactive edge
   VncClrCs();                         // chipselect
   VncSetSdi();                        // inactive level
   VncSetSclk();                       // strobe

   return( !Status);   
} // VncSendCommand

//-----------------------------------------------------------------------------
// Receive data
//-----------------------------------------------------------------------------

TYesNo VncReceiveData( byte *Data)
// Read <Data>
{
byte i;
byte Value;
byte Status;

   VncClrSclk();                       // inactive edge
   VncSetCs();                         // chipselect
   VncSetSdi();                        // START
   VncSetSclk();                       // strobe

   VncClrSclk();                       // inactive edge
   VncSetSdi();                        // R/W bit
   VncSetSclk();                       // strobe

   VncClrSclk();                       // inactive edge
   VncClrSdi();                        // ADDR bit
   VncSetSclk();                       // strobe

   // data already set, read it :
   i     = 8;
   Value = 0;
   do {
      Value <<= 1;
      VncClrSclk();                    // inactive edge
      if( VncGetSdo()){
         Value++;                      // add LSB
      }
      VncSetSclk();                    // next data
   } while( --i);

   // read status bit :
   VncClrSclk();                       // inactive edge
   Status = VncGetSdo();               // STATUS bit
   VncSetSclk();                       // strobe

   // deselect :
   VncClrSclk();                       // inactive edge
   VncClrCs();                         // chipselect
   VncSetSdi();                        // inactive level
   VncSetSclk();                       // strobe

   *Data = Value;
   return( !Status);   
} // VncReceiveData


#ifdef VNC_READ_STATUS
//-----------------------------------------------------------------------------
// Read status
//-----------------------------------------------------------------------------

TYesNo VncReadStatus( byte *Data)
// Read <Status>
{
byte i;
byte Value;
byte Status;

   VncClrSclk();                       // inactive edge
   VncSetCs();                         // chipselect
   VncSetSdi();                        // START
   VncSetSclk();                       // strobe

   VncClrSclk();                       // inactive edge
   VncSetSdi();                        // R/W bit
   VncSetSclk();                       // strobe

   VncClrSclk();                       // inactive edge
   VncSetSdi();                        // ADDR bit
   VncSetSclk();                       // strobe

   // data already set, read it :
   i     = 8;
   Value = 0;
   do {
      Value <<= 1;
      VncClrSclk();                    // inactive edge
      if( VncGetSdo()){
         Value++;                      // add LSB
      }
      VncSetSclk();                    // next data
   } while( --i);

   // read status bit :
   VncClrSclk();                       // inactive edge
   Status = VncGetSdo();               // STATUS bit
   VncSetSclk();                       // strobe

   // deselect :
   VncClrSclk();                       // inactive edge
   VncClrCs();                         // chipselect
   VncSetSdi();                        // inactive level
   VncSetSclk();                       // strobe

   *Data = Value;
   return( !Status);   
} // VncReadStatus

#endif // VNC_READ_STATUS

