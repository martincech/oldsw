//*****************************************************************************
//
//    Vnc.h          Vinculum interface
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#ifndef __Vnc_H__
   #define __Vnc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// status mask :
#define VNC_STATUS_RXF    0x01    // receive buffer full (active L)
#define VNC_STATUS_TXE    0x02    // transmit buffer empty (active L)
#define VNC_STATUS_RXF_IE 0x10    // receive  buffer full interrupt enabled
#define VNC_STATUS_TXE_IE 0x20    // transmit buffer empty interrupt enabled


void VncInit( void);
// Initialize

TYesNo VncSendData( byte Data);
// Send <Data>

TYesNo VncReceiveData( byte *Data);
// Read <Data>

TYesNo VncReadStatus( byte *Data);
// Read <Status>

#endif
