//*****************************************************************************
//
//    Hardware.h    Vinculum hardware descriptions (BASTL)
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../inc/cpu.h"
#include "../inc/uni.h"


//-----------------------------------------------------------------------------
// Parametry CPU
//-----------------------------------------------------------------------------

// Krystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Stavove LED
//-----------------------------------------------------------------------------

#define StatusLED0  0
#define StatusLED1  1
#define StatusLED2  2
#define StatusLED3  3

#define StatusLedInit()   DDRC  |=  (1 << StatusLED0) | (1 << StatusLED1) | (1 << StatusLED2) | (1 << StatusLED3)
#define StatusLedOff()    PORTC |=  (1 << StatusLED0) | (1 << StatusLED1) | (1 << StatusLED2) | (1 << StatusLED3)

#define StatusLed0On()    PORTC &= ~(1 << StatusLED0) 
#define StatusLed0Off()   PORTC |=  (1 << StatusLED0) 
#define StatusLed1On()    PORTC &= ~(1 << StatusLED1) 
#define StatusLed1Off()   PORTC |=  (1 << StatusLED1) 
#define StatusLed2On()    PORTC &= ~(1 << StatusLED2) 
#define StatusLed2Off()   PORTC |=  (1 << StatusLED2) 
#define StatusLed3On()    PORTC &= ~(1 << StatusLED3)
#define StatusLed3Off()   PORTC |=  (1 << StatusLED3)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           9600       // Baud rate
#define UART0_RX_TIMEOUT     10000      // Intercharacter timeout [us]

//------------------------------------------------------------------------------
// Vinculum
//------------------------------------------------------------------------------

#define VncSCLK 2
#define VncSDI  3
#define VncSDO  4
#define VncCS   5

#define VncPortInit()    DDRD |= (1 << VncSCLK) | (1 << VncSDI) | (1 << VncCS)

#define VncSetSclk()     PORTD |=  (1 << VncSCLK)
#define VncClrSclk()     PORTD &= ~(1 << VncSCLK)
#define VncSetSdi()      PORTD |=  (1 << VncSDI)
#define VncClrSdi()      PORTD &= ~(1 << VncSDI)
#define VncGetSdo()     (PIND  &   (1 << VncSDO))
#define VncSetCs()       PORTD |=  (1 << VncCS)
#define VncClrCs()       PORTD &= ~(1 << VncCS)

#define VNC_READ_STATUS  1             // conditional compilation VncReadStatus()

//-----------------------------------------------------------------------------

#endif
