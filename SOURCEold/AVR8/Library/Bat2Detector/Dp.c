//*****************************************************************************
//
//   Dp.c     Detector protocol
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "Uart0.h"
#include "../Library/Bat2Detector/Dp.h"
#include "../Library/Bat2Detector/DpDef.h"
#include "../Library/Bat2Detector/DpCom.h"

#include "../Library/Bat2Detector/Detector.h"

// Buffers
static TDpQuery QueryData;
static TDpReply ReplyData;
static TDpDataReply AsynchData;

static TYesNo Send(void *Data, word DataSize);
// Common send routine

void DpInit()
// Init
{
   DpComInit();
}

void DpExecute()
// Execute Detector protocol
{
   byte DataSize;
   byte ReplyLength;
   
   switch(DpComStatus()) {
	  case DP_COM_TIMEOUT:
	     DataSize = DpComDataSizeGet();
		 
		 if(DataSize == 0) { // no data received
			DpComReceive((byte *)&QueryData, sizeof(QueryData)); // listen again
            break;
		 }
			
		 ReplyLength = 0;
			
	     switch(QueryData.Command) {
	        case ADC_PARAMETERS:
		       if(DataSize != sizeof(TAdcParameters) + 1) {
				  break;  
			   }

               ReplyLength = 1;

			   if(!DetectorValidAdc(&QueryData.Parameters.Adc)) {
			      ReplyData.Parameters.Status = STATUS_ERROR;
				  break;
			   }	
			   			   
			   memcpy(&DetectorParameters.Adc, &QueryData.Parameters.Adc, sizeof(TAdcParameters)); // store parameters

			   ReplyData.Parameters.Status = STATUS_OK;

		       break;
			   
	        case DETECTION:
		       if(DataSize != sizeof(TDetectionParameters) + 1) {
				  break;  
			   }
			   
			   ReplyLength = 1;

			   if(!DetectorValidDetection(&QueryData.Parameters.Detection)) {
			      ReplyData.Parameters.Status = STATUS_ERROR;
				  break;
			   }
			   				   
			   memcpy(&DetectorParameters.Detection, &QueryData.Parameters.Detection, sizeof(TDetectionParameters)); // store parameters
			   ReplyData.Parameters.Status = STATUS_OK;

		       break;
			   
	        case CALIBRATION:
		       if(DataSize != sizeof(TCalibrationQuery)) {
				  break;  
			   }
   
			   memcpy(&DetectorParameters.Calibration, &QueryData.Calibration.Parameters, sizeof(TCalibrationParameters)); // store parameters
			   
			   if(!DetectorCalibrate()) {
                  ReplyData.Calibration.Status = STATUS_ERROR;
			   } else {
                  ReplyData.Calibration.Status = STATUS_OK;
			   }

               memcpy(&ReplyData.Calibration.CalibrationData, &DetectorParameters.CalibrationData, sizeof(TCalibrationData)); // store parameters

               ReplyLength = 1 + sizeof(TCalibrationData);

		       break;
			   
	        case CALIBRATION_DATA:
		       if(DataSize != sizeof(TCalibrationDataQuery)) {
				  break;  
			   }
   
			   memcpy(&ReplyData.CalibrationData.CalibrationData, &DetectorParameters.CalibrationData, sizeof(TCalibrationData)); // store parameters
               ReplyLength = sizeof(TCalibrationData);
			   
		       break;
			   			   
	        case DETECTOR_MODE:
		       if(DataSize != sizeof(TModeParameters) + 1) {
				  break;  
			   }

               ReplyLength = 1;
			   
			   if(!DetectorValidMode(&QueryData.Parameters.Mode)) {
			      ReplyData.Parameters.Status = STATUS_ERROR;
				  break;
			   }

			   memcpy(&DetectorParameters.Mode, &QueryData.Parameters.Mode, sizeof(TModeParameters)); // store parameters
			   ReplyData.Parameters.Status = STATUS_OK;

		       break;
			   
			/*
			case ..................
			*/ 
			
			case START:
		       if(DataSize != sizeof(TSimpleCommandQuery)) {
				  break;  
			   }
			   
			   ReplyLength = 1;
			   
			   if(!DetectorStart()) { // Try to start detector
			      ReplyData.SimpleCommand.Status = STATUS_ERROR;
				  break;
			   }
			   
			   ReplyData.SimpleCommand.Status = STATUS_OK;

			   break;
			   
			case STOP:
		       if(DataSize != sizeof(TSimpleCommandQuery)) {
				  break;  
			   }
			   
			   if(DetectorStop()) {
				  ReplyData.SimpleCommand.Status = STATUS_OK;
			   } else {
			      ReplyData.SimpleCommand.Status = STATUS_ERROR;
			   }
			   
			   ReplyLength = 1;
			   break;
			   
			case FIRMWARE_START:
		       if(DataSize != sizeof(TSimpleCommandQuery)) {
				  break;  
			   }
			   
			   ReplyLength = 1;
			   
			   if(!DetectorFirmwareStart()) {
			      ReplyData.SimpleCommand.Status = STATUS_ERROR;
				  break;
			   }	
			   		   
			   ReplyData.SimpleCommand.Status = STATUS_OK;

			   break;
			   
			case FIRMWARE_STOP:
		       if(DataSize != sizeof(TSimpleCommandQuery)) {
				  break;  
			   }
			   
			   ReplyLength = 1;
			   
			   if(!DetectorFirmwareStop()) {
			      ReplyData.SimpleCommand.Status = STATUS_ERROR;
				  break;
			   }	
			   		   
			   ReplyData.SimpleCommand.Status = STATUS_OK;

			   break;
			   
			case FIRMWARE_CHUNK:
		       if(DataSize != sizeof(TFirmwareChunkQuery)) {
				  break;  
			   }
			   
			   ReplyLength = 1;
			   
			   if(!DetectorFirmwareChunk(QueryData.FirmwareChunk.Chunk, QueryData.FirmwareChunk.ChunkNum)) {
			      ReplyData.FirmwareChunk.Status = STATUS_ERROR;
				  break;
			   }	
			   		   
			   ReplyData.FirmwareChunk.Status = STATUS_OK;

			   break;
			   			   
		    default:
		       break;
	     }

		 // Send reply
		 if(ReplyLength) {
			ReplyData.Command = QueryData.Command; // Copy command
			ReplyLength++;
			Send(&ReplyData, ReplyLength);
		 }
			
		 // Listen again
		 DpComReceive((byte *)&QueryData, sizeof(QueryData));
		 break;
			
	  default:
	     // Listen again
		 DpComReceive((byte *)&QueryData, sizeof(QueryData));
		 break;
   }
}

void DpSendAsynch(word DataLength)
// Send data without master's query
{
   AsynchData.Command = DATA;
   DataLength++;

   Send(&AsynchData, DataLength);
   
   /*
      dod�lat potvrzen� od mastera
   */
}

void *DpGetAsynchBuffer()
// Data buffer
{
   return (void *) &AsynchData.Data;
}

void DpStop()
// Stop
{
   DpComStop();
}

static TYesNo Send(void *Data, word DataSize)
// Common send routine
{
   TYesNo Done = NO;
   
   DpComSend(Data, DataSize);
	   
   do {
      switch(DpComStatus()) {
         case DP_COM_SEND_DONE:
            Done = YES;
		    break;
		 
         default:
            Done = NO;
            break;
      }	   
   } while(!Done);
   
   return YES;
}