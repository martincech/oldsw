//*****************************************************************************
//
//   DetectorValidParam.c     Parameter's validation
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "Uni.h"
#include "../../AVR8/Library/Bat2Detector/DetectorDef.h"

TYesNo DetectorValidAdc(TAdcParameters *Parameters) {
   if(Parameters->Rate < ADC_RATE_MIN || Parameters->Rate > ADC_RATE_MAX) {
      return NO;
   }
   
   if(Parameters->Accuracy < ADC_ACCURACY_MIN || Parameters->Accuracy > ADC_ACCURACY_MAX) {
      return NO;
   }
   
   if(Parameters->Prefilter < ADC_PREFILTER_MIN || Parameters->Prefilter > ADC_PREFILTER_MAX) {
      return NO;
   }

   switch(Parameters->Filter) {
      case PS081_FILTER_NONE:
      case PS081_FILTER_SINC3:
      case PS081_FILTER_SINC5:
	     break;
		 
	  default:
	     return NO;
   }

   return YES;
}

TYesNo DetectorValidMode(TModeParameters *Parameters) {
   if(Parameters->BurstSize < 1 || Parameters->BurstSize > DATA_BURST_SIZE_MAX) {
      return NO;
   }

   if(Parameters->Mode >= _DETECTOR_MODE_LAST) {
      return NO;
   }
   
   return YES;
}

TYesNo DetectorValidDetection(TDetectionParameters *Parameters) {
   if(Parameters->AveragingWindow < DETECTION_AVERAGING_WINDOW_MIN || Parameters->AveragingWindow > DETECTION_AVERAGING_WINDOW_MAX) {
      return NO;
   }

   if(Parameters->StableWindow < DETECTION_STABLE_WINDOW_MIN || Parameters->StableWindow > DETECTION_STABLE_WINDOW_MAX) {
      return NO;
   }
   
   return YES;
}