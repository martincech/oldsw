//*****************************************************************************
//
//   Detector.c     Detector
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "../../AVR8/Library/Bat2Detector/Detector.h"
#include "../../AVR8/Library/Bat2Detector/Adc/Ps081/Ps081.h"
#include "System.h"

// Calibration defaults
TDetectorParameters DetectorParameters = {
   .CalibrationData = {
      .Zero = 0,
	  .FullRange = 50000,
	  .RawZeroLow = 16658740,
	  .RawFullRangeLow = 587720,
	  .RawZeroHigh = 16658735,
	  .RawFullRangeHigh = 587750
   }
};

// Buffer
TData *Data;
   volatile TDataSample *BufferAddress;
   volatile byte SampleSize;
   volatile TDataSample *Sample;
   
   
static void CalibrateSample(TDataSample *Sample);  
static TYesNo CalibrationCancel();

typedef enum {
   CALIBRATION_NOT,
   CALIBRATION_CONFIRM_ZERO,
   CALIBRATION_ZERO,
   CALIBRATION_CONFIRM_FULL_RANGE,
   CALIBRATION_FULL_RANGE,
   CALIBRATION_DONE
} ECalibrationState;

volatile ECalibrationState CalibrationState = CALIBRATION_NOT;

void DetectorNewAdcValue(TDataSample *Sample)
// New ADC value arrives here
{  

   if(CalibrationState != CALIBRATION_NOT) {
	   CalibrateSample(Sample);
	   return;
   }
   switch(DetectorParameters.Mode.Mode) {
      case DETECTOR_MODE_PS081: // Sample to be send to terminal directly
	     BufferAddress = &Data->DataPs081[Data->DataCount];
		 SampleSize = sizeof(TPs081Weight);
         break;
		 
      case DETECTOR_MODE_ALL: // Sample to be send to terminal directly
	     BufferAddress = &Data->DataAll[Data->DataCount];
		 SampleSize = sizeof(TWeight);
         break;
		 
      case DETECTOR_MODE_STABLE: // Stable samples to be send to terminal directly
	     Data->DataStable[Data->DataCount].Time = 0;
	     BufferAddress = &Data->DataStable[Data->DataCount];
		 SampleSize = sizeof(TStableWeight);
         break;
		 
      case DETECTOR_MODE_DEFAULT: // Stable sample to be send to Acceptance module
         //AcceptNewSample(StableSample);
         return;
      
      default:
         return;
   }
   
   memcpy(BufferAddress, Sample, SampleSize);
   
   Data->DataCount++;
			
   if(Data->DataCount == DetectorParameters.Mode.BurstSize) { // Time to send buffer
      // ADC samples are read in ISR, so don't waste time, otherwise next samples will be lost
      DpSendAsynch(Data->DataCount * SampleSize + sizeof(Data->DataCount));
      Data->DataCount = 0;
   }
}

TYesNo DetectorStart()
// Start detector
{
   // Init data buffer
   Data = DpGetAsynchBuffer();
   Data->DataCount = 0;
   
   // Try to start ADC
#ifdef PICOSTRAIN
   if(!AdcStart(&DetectorParameters.Adc, &DetectorParameters.Detection, &DetectorParameters.CalibrationData)) {
#else
   #error "Unsupported ADC"
#endif
      return NO;  
   }
   
   return YES;
}

TYesNo DetectorStop()
// Stop detector
{
   if(!AdcStop()) { // Try to stop ADC
	   return NO;
   }
   
   return YES;
}


volatile TPs081ConfRegValue CCC;
volatile TYesNo check;

char DataBeg[] = {0x28, 0x83, 0x2C, 0x2F, 0x40, 0x87, 0x01, 0x0F};


TYesNo DetectorFirmwareStart()
// Start detector
{
   byte i;
   
   if(!DetectorStop()) {
      return NO;
   }

   Ps081Init();      // Init Ps081
   Ps081ComEnable();
   SysDelay(5);

   Ps081EepromBgapOn();
   SysDelay(5);
   Ps081EepromEnableOn();
   SysDelay(5);

   for(i = 0 ; i < PS081_EEPROM_BLOCK_COUNT ; i++) {
      Ps081EepromBlockErase(i);
	  SysDelay(50);
   }

   return YES;
}
volatile byte i;
volatile byte ChunkNumV[100];
volatile byte ptr = 0;
TYesNo DetectorFirmwareChunk(byte *Chunk, byte ChunkNum)
// Start detector
{
   
ChunkNumV[ptr] = ChunkNum;
ptr++;
if(ptr >= 100) {
   ptr = 0;
}


   Ps081EepromWrite(ChunkNum * PS081_FIRMWARE_CHUNK_SIZE, Chunk, PS081_FIRMWARE_CHUNK_SIZE);

   for(i = 0 ; i < PS081_FIRMWARE_CHUNK_SIZE ; Chunk += PS081_COMPARE_DATA_LENGTH, i += PS081_COMPARE_DATA_LENGTH) {
	   if(!Ps081EepromRead(ChunkNum * PS081_FIRMWARE_CHUNK_SIZE + i, Chunk)) {
		   i++;
		   i--;
	     return NO;
	   }
   }

   return YES;
}

TYesNo DetectorFirmwareStop()
// Start detector
{
   byte i;
   
   Ps081EepromBgapOff();
   Ps081EepromEnableOff();
   
   Ps081PowerReset(); // Reset
   return YES;
}

TYesNo DetectorCalibrate() {
	CalibrationState = CALIBRATION_CONFIRM_ZERO;
	
	DetectorParameters.Mode.Mode = DETECTOR_MODE_PS081;
	
	#ifdef PICOSTRAIN
   if(!AdcStart(&DetectorParameters.Adc, &DetectorParameters.Detection, &DetectorParameters.CalibrationData)) {
#else
   #error "Unsupported ADC"
#endif
      return NO;  
   }
   
   CalibrationLedOff();
   CalibrationLedInit();
   CalibrationButtonInit();
   
   SysDelay(2000);

   DetectorParameters.CalibrationData.Zero = DetectorParameters.Calibration.Zero;
   DetectorParameters.CalibrationData.FullRange = DetectorParameters.Calibration.FullRange;

   // User is putting zero weight
   CalibrationLedOn();
   while(!CalibrationButtonPushed());
   SysDelay(100);
   while(CalibrationButtonPushed()) {
      if(CalibrationCancel()) {
	     CalibrationLedOff();
         AdcStop();
         return NO;
      }
   }
   CalibrationLedOff();
   CalibrationState = CALIBRATION_ZERO;
   
   // Calibrating zero weight
   while(CalibrationState != CALIBRATION_CONFIRM_FULL_RANGE) {
      if(CalibrationCancel()) {
         AdcStop();
         return NO;
      }
   }
   CalibrationLedOn();
   
   // User is putting full weight
   while(!CalibrationButtonPushed());
   SysDelay(100);
   while(CalibrationButtonPushed()) {
      if(CalibrationCancel()) {
		 CalibrationLedOff();
         AdcStop();
         return NO;
      }
   }
   CalibrationLedOff();
   CalibrationState = CALIBRATION_FULL_RANGE;
   
   // Calibrating
   while(CalibrationState != CALIBRATION_DONE) {
      if(CalibrationCancel()) {
         AdcStop();
         return NO;
      }
   }
   CalibrationLedOn();
   
   // User ends calibration
   while(!CalibrationButtonPushed());
   SysDelay(100);
   while(CalibrationButtonPushed());
   CalibrationLedOff();
   CalibrationState = CALIBRATION_NOT;
   
   if(!AdcStop()) {
	   return NO;
   }
   
   return YES;
}

static TYesNo CalibrationCancel() {
   byte Timer = 0;
   
   if(!CalibrationButtonPushed()) {
	   return NO;
   }
   
   while(CalibrationButtonPushed()) {
	  SysDelay(100);
	  
      Timer++;
		  
      if(Timer > 20) {
         return YES;
      }
   }
}

static void CalibrateSample(TDataSample *Sample) {
	switch(CalibrationState) {
      case CALIBRATION_ZERO:
         DetectorParameters.CalibrationData.RawZeroHigh = Sample->Ps081.Raw; // ukl�d�m aktu�ln� hodnotu - cht�lo by to filtrovat
         CalibrationState = CALIBRATION_CONFIRM_FULL_RANGE;
		 break;
		
      case CALIBRATION_FULL_RANGE:
         DetectorParameters.CalibrationData.RawFullRangeHigh = Sample->Ps081.Raw; // ukl�d�m aktu�ln� hodnotu - cht�lo by to filtrovat
         CalibrationState = CALIBRATION_DONE;
		 break;

      default:
		 break;
	}
}