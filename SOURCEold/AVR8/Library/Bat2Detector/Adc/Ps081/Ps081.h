//*****************************************************************************
//
//   Ps081.h     PS081
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Ps081_h__
   #define __Ps081_h__
   
#include "Uni.h"
   
   
#define PS081_STRETCH_CYTIME_BASE_US 100
#define PS081_CONTINUOUS_CYTIME_BASE_US 2
   
#define PS081_BITS      24

#define PS081_EEPROM_LOW_ADDRESS    0
#define PS081_EEPROM_SIZE           2048

#define PS081_EEPROM_BLOCK_COUNT    4
#define PS081_EEPROM_BLOCK_SIZE     PS081_EEPROM_SIZE / PS081_EEPROM_BLOCK_COUNT

#define PS081_USER_EEPROM_LOW_ADDRESS    0
#define PS081_USER_EEPROM_SIZE           48

#define PS081_USER_EEPROM_OFFSET          2000

#define PS081_CONFREG_BASE 48
#define PS081_COMPARE_DATA_LENGTH 8

// �asov�n� z�pis� do EEPROM je v datasheetu pops�no divn�
#define EEPROM_BLOCK_WRITE_MS  8000  // ???????????????????????????

//------------------------------------------------------------------------------
// Config registers
//------------------------------------------------------------------------------

typedef enum {
   PS081_CONFREG00 = 0,
   PS081_CONFREG01,
   PS081_CONFREG02,
   PS081_CONFREG03,
   PS081_CONFREG04,
   PS081_CONFREG05,
   PS081_CONFREG06,
   PS081_CONFREG07,
   PS081_CONFREG08,
   PS081_CONFREG09,
   PS081_CONFREG10,
   PS081_CONFREG11,
   PS081_CONFREG12,
   PS081_CONFREG13,
   PS081_CONFREG14,
   PS081_CONFREG15,
   PS081_CONFREG16,
   PS081_CONFREG17,
   PS081_CONFREG18,
   _PS081_CONFREG_COUNT
} EPs081ConfReg;



typedef enum {
   PS081_SEL_COMPR_10K = 0,
   PS081_SEL_COMPR_10K_,
   PS081_SEL_COMPR_7K,
   PS081_SEL_COMPR_4_1K
} EPs081Sel_compr;

typedef enum {
   PS081_DIS_OSC_STARTUP_OFF = 0,
   PS081_DIS_OSC_STARTUP_ON
} EPs081Dis_osc_startup;

// CF0
typedef struct {
   byte dis_haltpp_ps : 1;
   byte cpu_speed  : 2;
   EPs081Dis_osc_startup dis_osc_startup : 1;
   byte osz10khz_fsos :6;
   byte io_a : 4;
   EPs081Sel_compr sel_compr : 2;
   byte tdc_conv_cnt : 8;
} __packed TPs081ConfRegValue0;

// CF1
typedef struct {
   byte epr_usr_cfg : 1;
   byte epr_pwr_prg : 1;
   byte epr_pwr_cfg : 1;
   byte low_batt  : 3;
   byte mod_rspan : 1;
   byte mult_en_pp : 1;
   byte rspan_by_temp : 1;
   byte en_avcal : 1;
   byte mult_en_ub : 1;
   byte lcd_dis_chargem : 1;
   byte lcd_freq : 3;
   byte lcd_duty : 2;
   byte tdc_sleepmode : 1;
   byte messb2 : 1;
   byte abgl_hr : 4;
   byte speed_talu : 1;
} __packed TPs081ConfRegValue1;

typedef enum {
   PS081_PPTEMP_DISABLE = 0,
   PS081_PPTEMP_ENABLE
} EPs081Pptemp;

typedef enum {
   PS081_SINGLE_CONVERSION_CONTINUOUS = 0,
   PS081_SINGLE_CONVERSION_SINGLE
} EPs081Single_Conversion;

// CF2
typedef struct {
   byte auto10k : 1;
   byte _dummy  : 1;
   EPs081Single_Conversion single_conversion :1;
   EPs081Pptemp pptemp : 1;
   word cytime : 10;
   word avrate : 10;
} __packed TPs081ConfRegValue2;

typedef enum {
   PS081_EN_WHEATSTONE_DISABLE = 0,
   PS081_EN_WHEATSTONE_ENABLE
} EPs081En_wheatstone;

typedef enum {
   PS081_STOP_OSZ_INACTIVE = 0,
   PS081_STOP_OSZ_ACTIVE
} EPs081Stop_osz;

typedef enum {
   PS081_SEL_START_OSZ_OFF = 0,
   PS081_SEL_START_OSZ_ON,
   PS081_SEL_START_OSZ_100U,
   PS081_SEL_START_OSZ_200U,
   PS081_SEL_START_OSZ_300U,
   PS081_SEL_START_OSZ_400U
} EPs081Sel_start_osz;

typedef enum {
   PS081_STRETCH_OFF = 0,
   PS081_STRETCH_1R,
   PS081_STRETCH_2R_200U,
   PS081_STRETCH_2R_300U
} EPs081Stretch;

typedef enum {
   PS081_BRIDGE_1H = 0,
   PS081_BRIDGE_2H,
   _DUMMY_,
   PS081_BRIDGE_4H
} EPs081Bridge;

// CF3
typedef struct {
   EPs081Bridge bridge : 2;
   byte mfake  : 2;
   byte ps081adjust : 6;
   byte _dummy0 : 2;
   EPs081Stretch stretch : 2;
   byte _dummy1 : 3;
   EPs081Sel_start_osz sel_start_osz : 3;
   EPs081Stop_osz stop_osz : 1;
   EPs081En_wheatstone en_wheatstone : 1;
   byte _dummy2 : 2;
} __packed TPs081ConfRegValue3;

// CF4
typedef struct {
   dword Mult_Hb1;
} __packed TPs081ConfRegValue4;

// CF5
typedef struct {
   dword Mult_Hb2;
} __packed TPs081ConfRegValue5;

// CF6
typedef struct {
   dword Mult_Hb3;
} __packed TPs081ConfRegValue6;

// CF7
typedef struct {
   dword Mult_Hb4;
} __packed TPs081ConfRegValue7;

// CF8
typedef struct {
   dword Mult_TkG;
} __packed TPs081ConfRegValue8;

// CF9
typedef struct {
   dword Mult_TkO;
} __packed TPs081ConfRegValue9;

// CF10
typedef struct {
   byte Mult_PP : 8;
   byte Mult_Ub : 8;
   byte calcor : 8;
} __packed TPs081ConfRegValue10;

typedef enum {
   PS081_CON_COMP_OFF = 0,
   PS081_CON_COMP_OFF_MEASUREMENTS,
   PS081_CON_COMP_OFF_LOADING_MEASUREMENTS,
   PS081_CON_COMP_ON
} EPs081Con_comp;

typedef enum {
   PS081_SEL_COMPIN_EXTERNAL = 0,
   PS081_SEL_COMPIN_INTERNAL
} EPs081Sel_compin;

// CF11
typedef struct {
   EPs081Con_comp con_comp : 1;
   EPs081Sel_compin sel_compin : 1;
   dword dummy : 22;
} __packed TPs081ConfRegValue11;

// Union
typedef union {
   TPs081ConfRegValue0 CF0;
   TPs081ConfRegValue1 CF1;
   TPs081ConfRegValue2 CF2;
   TPs081ConfRegValue3 CF3;
   TPs081ConfRegValue4 CF4;
   TPs081ConfRegValue5 CF5;
   TPs081ConfRegValue6 CF6;
   TPs081ConfRegValue7 CF7;
   TPs081ConfRegValue8 CF8;
   TPs081ConfRegValue9 CF9;
   TPs081ConfRegValue10 CF10;
   TPs081ConfRegValue11 CF11;
   dword dw;
} TPs081ConfRegValue;


void Ps081Init(void);
void Ps081ComEnable(void);
void Ps081ComDisable(void);

void Ps081InitReset(void);
void Ps081StartNewCycle(void);
void Ps081PowerReset(void);
void Ps081WatchdogOn(void);
void Ps081WatchdogOff(void);
void Ps081EepromEnableOn(void);

TYesNo Ps081ReadRam(byte Address, dword *Value);
TYesNo Ps081WriteRam(byte Address, dword Value);
TYesNo Ps081ReadConfReg(EPs081ConfReg CF, TPs081ConfRegValue *Value);
TYesNo Ps081WriteConfReg(EPs081ConfReg CF, TPs081ConfRegValue Value);

TYesNo Ps081EepromWrite(word Address, byte *Data, word DataLength);
TYesNo Ps081EepromRead(word Address8, byte* CompareData);
TYesNo Ps081EepromBlockErase(byte Block);
#endif