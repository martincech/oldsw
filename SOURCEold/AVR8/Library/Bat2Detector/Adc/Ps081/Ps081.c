//*****************************************************************************
//
//   Ps081.c     PS081
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "Spi.h"
#include "Hardware.h"
#include "System.h"
#include "Uni.h"
#include "../Library/Bat2Detector/Adc/Ps081/Ps081.h"

// Commands
#define RAM_WRITE       0x00
#define RAM_READ        0x40
#define INIT_RESET      0xC0
#define POWER_RESET     0xF0
#define START_NEW_CYCLE 0xCC
#define WATCHDOG_ON     0x9E
#define WATCHDOG_OFF    0x9F

// EEPROM commands
#define EEPROM_BGAP_OFF     0x86
#define EEPROM_BGAP_ON      0x87
#define EEPROM_ENABLE_OFF   0x90
#define EEPROM_ENABLE_ON    0x91
#define EEPROM_READ         0xA0
#define EEPROM_WRITE        0xA1
#define EEPROM_BWRITE       0xA3
#define EEPROM_BERASE       0xA4
#define USER_EEPROM_READ    0xA5

// �asov�n� z�pis� do EEPROM je v datasheetu pops�no divn�
#define EEPROM_WRITE_WORD_DELAY_MS  5  // ???????????????????????????

static void CommandStart();
// Common command start

static void CommandStop();
// Common command stop

static void Ps081SingleByteCommand(byte Command);
// Single byte command

void Ps081Init(void)
// Init
{
   SpiInit();
   //SpiSelect(); // Bring PS081 out of reset

   Ps081SpiDisable();
   Ps081SpiEnableInit();
}

void Ps081ComEnable(void)
// Enables communication with PS081
/*
   Must be called in order to establish communication with PS081
*/
{
   SpiSelect(); // make sure RST_CSN = 0
   SysDelay(1); // wait 1 ms
   Ps081SpiEnable(); // SPI_ENA = 1
   SysDelay(1); // wait 1 ms
   SpiRelease(); // RST_CSN = 1
}

void Ps081ComDisable(void)
// Disables communication with PS081, resets PS081 !
{
   SpiSelect(); // RST_CSN = 0
   SysDelay(1); // wait 1 ms
   Ps081SpiDisable(); // SPI_ENA = 0
}

void Ps081PowerReset(void) {
   Ps081SingleByteCommand(POWER_RESET);
}

void Ps081InitReset(void) {
   Ps081SingleByteCommand(INIT_RESET);
}

void Ps081WatchdogOn(void) {
   Ps081SingleByteCommand(WATCHDOG_ON);
}

void Ps081WatchdogOff(void) {
   Ps081SingleByteCommand(WATCHDOG_OFF);
}

void Ps081EepromBgapOn(void) {
   Ps081SingleByteCommand(EEPROM_BGAP_ON);
}

void Ps081EepromBgapOff(void) {
   Ps081SingleByteCommand(EEPROM_BGAP_OFF);
}

void Ps081EepromEnableOn(void) {
   Ps081SingleByteCommand(EEPROM_ENABLE_ON);
}

void Ps081EepromEnableOff(void) {
   Ps081SingleByteCommand(EEPROM_ENABLE_OFF);
}

void Ps081StartNewCycle(void) {
   Ps081SingleByteCommand(START_NEW_CYCLE);
}

TYesNo Ps081EepromRead(word Address8, byte* CompareData)
// EEPROM read comparation
{
   byte i;
   byte Ch;
   
   // Check address
   if(Address8 >= PS081_EEPROM_LOW_ADDRESS + PS081_EEPROM_SIZE) {
	   return NO;
   }
   
   // It should be aligned to 8 boundary
   if(Address8 & (PS081_COMPARE_DATA_LENGTH - 1)) {
	   return NO;
   }
   
   // EEPROM je adresovan� po slovech (16 bit)! Z datasheetu mysl�m ka�d� pochop� opak (adresovan� po bytech)
   Address8 /= 2;

   CommandStart();

   SpiByteWrite(EEPROM_READ);
   SpiByteWrite(Address8 >> 8);
   SpiByteWrite(Address8);
   
   i = PS081_COMPARE_DATA_LENGTH / 2;
   
   while(i--)   {
	  SpiByteWrite(*(CompareData + 1)); // LSB first
      SpiByteWrite(*CompareData);
	  CompareData += 2;
   }

   Ch = SpiByteRead();
   
   CommandStop();

   if(Ch != 0xFF) { // Data match?
	   return NO;
   }
   
   return YES;
}

static TYesNo Ps081EepromWriteWord(word Address, word Data)
// EEPROM block write
{

   CommandStart();
   
   // EEPROM je adresovan� po slovech (16 bit)! Z datasheetu mysl�m ka�d� pochop� opak (adresovan� po bytech)
   Address /= 2;

   // Oproti datasheetu je nutn� pos�lat data v opa�n�m po�ad� - nejd��v LSB byte !
   SpiByteWrite(EEPROM_WRITE);
   SpiByteWrite(Address >> 8);                   // address high
   SpiByteWrite(Address);                        // address low
   SpiByteWrite(Data);                           // data low
   SpiByteWrite(Data >> 8);                      // data high
   SpiByteWrite(0x1E);                           // dummy - start operation
   SysDelay(EEPROM_WRITE_WORD_DELAY_MS);
   SpiByteWrite(0x1E);                           // dummy - stop operation

   CommandStop();

   return YES;
}

TYesNo Ps081EepromWrite(word Address, byte *Data, word DataLength)
// EEPROM block write
{
   word i;
   // Check address
   if(Address + DataLength >= PS081_EEPROM_SIZE) {
	   return NO;
   }
   
   if(DataLength % 2 != 0) {
	   return NO;
   }

   while(DataLength)   {
      Ps081EepromWriteWord(Address, ((*Data) << 8) | *(Data+1));
	  
	  Data += 2;
	  Address += 2;
	  DataLength -= 2;
   }

   return YES;
}

TYesNo Ps081EepromBlockErase(byte Block)
// EEPROM block erase
{
   word i;
   // Check address
   if(Block >= PS081_EEPROM_BLOCK_COUNT) {
	   return NO;
   }

   CommandStart();

   SpiByteWrite(EEPROM_BERASE);
   SpiByteWrite(Block);          // address high
   SpiByteWrite(0);              // address low
   SpiByteWrite(0);              // data low
   SpiByteWrite(0);              // data high
   SpiByteWrite(0x1E);           // dummy - start operation
   SysDelay(EEPROM_WRITE_WORD_DELAY_MS);
   SpiByteWrite(0x1E);           // dummy - stop operation

   CommandStop();

   return YES;
}

TYesNo Ps081UserEepromRead(byte Address, word *Value)
// Read user EEPROM - NETESTOVANO !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
{
   byte Ch;
   TDataConvertor *Data = Value;
   
   Data->w = 0;
   
   // Check address
   if(Address >= PS081_USER_EEPROM_LOW_ADDRESS + PS081_USER_EEPROM_SIZE - 1) {
	   return NO;
   }

   CommandStart();

   SpiByteWrite(USER_EEPROM_READ);
   SpiByteWrite(0x00);
   SpiByteWrite(Address);

   Data->array[1] = SpiByteRead();
   Data->array[0] = SpiByteRead();

   CommandStop();

   return YES;
}

TYesNo Ps081ReadRam(byte Address, dword *Value)
// Read RAM
{
   byte Ch;
   TDataConvertor *Data = Value;
   
   Data->dw = 0;

   CommandStart();

   SpiByteWrite(RAM_READ);
   SpiByteWrite(Address);

   Data->array[2] = SpiByteRead();
   Data->array[1] = SpiByteRead();
   Data->array[0] = SpiByteRead();
   
   CommandStop();

   return YES;
}

TYesNo Ps081WriteRam(byte Address, dword Value)
// Write RAM
{
   TDataConvertor *Data = &Value;
   
   CommandStart();

   SpiByteWrite(RAM_WRITE);
   SpiByteWrite(Address);

   SpiByteWrite(Data->array[2]);
   SpiByteWrite(Data->array[1]);
   SpiByteWrite(Data->array[0]);

   CommandStop();
   
   return YES;
}

TYesNo Ps081ReadConfReg(EPs081ConfReg CF, TPs081ConfRegValue *Value)
// Read Config register
{
   return Ps081ReadRam(CF + PS081_CONFREG_BASE, &Value->dw);
}

TYesNo Ps081WriteConfReg(EPs081ConfReg CF, TPs081ConfRegValue Value)
// Write Config register
{
   return Ps081WriteRam(CF + PS081_CONFREG_BASE, Value.dw);
}



static void CommandStart() {
   SpiRelease();
   SysUDelay(1);
   SpiSelect();
}

static void CommandStop() {
   //SpiRelease();
}

static void Ps081SingleByteCommand(byte Command) {
   CommandStart();
   SpiByteWrite(Command);
   CommandStop();
}