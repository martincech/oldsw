//*****************************************************************************
//
//   Adc.h     ADC template
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Adc_H__
   #define __Adc_H__

#include "Uni.h"
#include "../Library/Bat2Detector/DetectorDef.h"

#ifdef PICOSTRAIN
TYesNo AdcStart(TAdcParameters *AdcParam, TDetectionParameters *DetectionParam, TCalibrationData *CalibrationData);
#else 
   #error "Unsupported ADC"
#endif
// ADC start

TYesNo AdcStop();
// ADC stop

#endif