//*****************************************************************************
//
//   AdcPs081Def.h     PS081 defines
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __AdcPs081Def_H__
   #define __AdcPs081Def_H__

#include "Uni.h"

#define ADC_ACCURACY_MIN    0
#define ADC_ACCURACY_MAX    100

// Filters
typedef enum {
   PS081_FILTER_NONE = 0,
   PS081_FILTER_SINC3 = 3,
   PS081_FILTER_SINC5 = 5
} EPs081Filter;

#define ADC_PREFILTER_MIN    1
#define ADC_PREFILTER_MAX    17

#define ADC_RATE_MIN    1
#define ADC_RATE_MAX    50

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

// Adc parameters
typedef struct {
   byte Accuracy;
   EPs081Filter Filter;
   byte Rate;
   byte Prefilter;
} __packed TAdcParametersPs081;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

#endif