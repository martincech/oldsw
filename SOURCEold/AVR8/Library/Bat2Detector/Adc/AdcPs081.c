//*****************************************************************************
//
//   AdcPs081.c     PS081 ADC
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "Uni.h"
#include "Hardware.h"

#include "../Library/Bat2Detector/Adc/Adc.h"
#include "../Library/Bat2Detector/Adc/Ps081/Ps081.h"
#include "../Library/Bat2Detector/Detector.h"

// kv�li TWeight
#include "../Library/Bat2Detector/DpDef.h"

// Ps081 RAM addresses
#define ResultAddress 		        0

#define ControlRegisterAddr 		1
// Control register flags
#define WAKE_UP               	    0
#define ALL              	        1
#define STABLE_VALUE           	    2
#define VALID_DATA           	    3

#define	RawValueAddress	            2
#define LowPassInputAddress         3
#define LowPassOutputAddress        4
#define HighPassAbsAddress          5

#define PrefilterInputAddress		6
#define PrefilterAddress			7
#define PrefilterCounterAddress	    8

#define FilterValueAAddress				11
#define FilterValueBAddress				12
#define FilterValueCAddress				13
#define FilterValueDAddress				14

#define FactorAddress		           32
#define PreLowPassPeriodAddress		   33
#define PreLowPassCounterAddress       34
#define AveragingWindowAddress		   35
#define AveragingWindowNativeAddress   36
#define RangeAddress	               37
#define StableWindowAddress		       38
#define StableWindowCounterAddress	   39




#define RawZeroAddr			        40	
#define RawFullRangeAddr		    41
#define ZeroAddr			        42	
#define FullRangeAddr		        43

#define FilterTypeAddress			44


#define RawZeroUnusedAddress			45
#define RawFullRangeUnusedAddress		46

#define AccuracyDataAUnusedAddress		 9
#define AccuracyDataBUnusedAddress		 47




#define	StableWindowCounterAddress	40




#define PS081_DEBUG


static TWeight Ps081ToWeight(dword Value);
// Convert PS081 format to TWeight

#define STRETCH_CYTIME_MIN 5   // nem� cenu pou��vat ni��� hodnotu, proto�e by se za tu dobu  stejn� cyklus nestihl prov�st
#define AVRATE_MIN 2
volatile word Cytime;
volatile word Avrate;
volatile word MaxAvrateAtTheRate;
volatile TPs081ConfRegValue ConfReg;
volatile TPs081ConfRegValue ConfReg2;
volatile TPs081ConfRegValue ConfReg3;

volatile TPs081ConfRegValue AccuracyDataA;
volatile TPs081ConfRegValue AccuracyDataAUnused;
volatile TPs081ConfRegValue *AccuracyLowDataA;
volatile TPs081ConfRegValue *AccuracyHighDataA;
   
volatile TPs081ConfRegValue AccuracyDataB;
volatile TPs081ConfRegValue AccuracyDataBUnused;
volatile TPs081ConfRegValue *AccuracyLowDataB;
volatile TPs081ConfRegValue *AccuracyHighDataB;
   
   
static void ComputeStretchModeParameters(byte Rate, byte Accuracy, EPs081Bridge bridge, EPs081Pptemp pptemp, byte mfake, TPs081ConfRegValue* ConfReg2, TPs081ConfRegValue* ConfReg3) {
   // Recalculate Accuracy and Rate here to obtain PS081's Avrate and CycleTime
   // Calculate max reachable avrate
   MaxAvrateAtTheRate = 1000000 / ((dword) Rate * STRETCH_CYTIME_MIN * PS081_STRETCH_CYTIME_BASE_US * (2 * (bridge + 1)))  - (pptemp + mfake + 1);
   
   // Multiply it by accuracy factor
   Avrate = (dword)MaxAvrateAtTheRate * Accuracy / 100;
   
   if(Avrate < AVRATE_MIN) {
      Avrate = AVRATE_MIN;
   }
   
   if(Avrate % 2) { // don't use even avrates - datasheet
      Avrate -= 1;
   }
   
   // Calculate cycle time to achieve desired rate
   Cytime = 1000000 / ((dword)(Avrate * 2 * (bridge + 1) + pptemp + mfake + 1) * PS081_STRETCH_CYTIME_BASE_US * Rate);

   ConfReg2->CF2.avrate = Avrate;
   ConfReg2->CF2.cytime = Cytime;
   ConfReg2->CF2.pptemp = pptemp;	

   if(Avrate <= 16) { // CF3.ps081adjust should be 2 * avrate (datasheet)
	  ConfReg3->CF3.ps081adjust = Avrate * 2;
   } else {
	   ConfReg3->CF3.ps081adjust = 33;
   }

   // Nem�nit, jinak se rozhod� p�epo�et Rate, Accuracy na cytime, avrate
   ConfReg3->CF3.sel_start_osz = PS081_SEL_START_OSZ_100U;
   ConfReg3->CF3.stretch = PS081_STRETCH_2R_200U;
}

static ComputeContinuousModeParameters(byte Rate, EPs081Bridge bridge, EPs081Pptemp pptemp, byte mfake, TPs081ConfRegValue* ConfReg2, TPs081ConfRegValue* ConfReg3) {
   Cytime = 65;
   Avrate = (1000000 - (dword) Rate * Cytime * PS081_CONTINUOUS_CYTIME_BASE_US * (6 + mfake * 2 + pptemp)) / ((dword) Rate * Cytime * PS081_CONTINUOUS_CYTIME_BASE_US * 2 * (bridge + 1));

   ConfReg2->CF2.avrate = Avrate;
   ConfReg2->CF2.cytime = Cytime;
   ConfReg2->CF2.pptemp = pptemp;	

   if(Avrate <= 16) { // CF3.ps081adjust should be 2 * avrate (datasheet)
	  ConfReg3->CF3.ps081adjust = Avrate * 2;
   } else {
	   ConfReg3->CF3.ps081adjust = 33;
   }

   ConfReg3->CF3.sel_start_osz = PS081_SEL_START_OSZ_ON;
   ConfReg3->CF3.stretch = PS081_STRETCH_OFF;
}	
   
   
   
static TYesNo SetAdcParameters(TAdcParametersPs081 *AdcParam, TDetectionParameters *DetectionParam, TCalibrationData *CalibrationData) {
   EPs081Bridge bridge = PS081_BRIDGE_1H;
   EPs081Pptemp pptemp = PS081_PPTEMP_ENABLE;
   byte mfake = 1;
   dword *RawZero;
   dword *RawZeroUnused;
   dword *RawFullRange;
   dword *RawFullRangeUnused;
   
   
   if(AdcParam->Accuracy != 100) {
	  AccuracyLowDataA = &AccuracyDataAUnused;
	  AccuracyHighDataA = &AccuracyDataA;
	  
	  AccuracyLowDataB = &AccuracyDataBUnused;
	  AccuracyHighDataB = &AccuracyDataB;

	  RawZeroUnused = &CalibrationData->RawZeroLow;
	  RawFullRangeUnused = &CalibrationData->RawFullRangeLow;

	  RawZero = &CalibrationData->RawZeroHigh;
	  RawFullRange = &CalibrationData->RawFullRangeHigh;
   } else {
	  AccuracyLowDataA = &AccuracyDataA;
	  AccuracyHighDataA = &AccuracyDataAUnused;
	  
	  AccuracyLowDataB = &AccuracyDataB;
	  AccuracyHighDataB = &AccuracyDataBUnused;
	  
      RawZero = &CalibrationData->RawZeroLow;
	  RawFullRange = &CalibrationData->RawFullRangeLow;

	  RawZeroUnused = &CalibrationData->RawZeroHigh;
	  RawFullRangeUnused = &CalibrationData->RawFullRangeHigh;
   }

   // Common
   Ps081ReadConfReg(PS081_CONFREG02, AccuracyHighDataA);
   memcpy(AccuracyLowDataA, AccuracyHighDataA, sizeof(*AccuracyHighDataA));
   AccuracyDataAUnused.CF2.single_conversion = AccuracyDataA.CF2.single_conversion = PS081_SINGLE_CONVERSION_CONTINUOUS;

   Ps081ReadConfReg(PS081_CONFREG03, AccuracyHighDataB);
   memcpy(AccuracyLowDataB, AccuracyHighDataB, sizeof(*AccuracyHighDataB));
   
   
   AccuracyHighDataB->CF3.en_wheatstone = AccuracyLowDataB->CF3.en_wheatstone = PS081_EN_WHEATSTONE_DISABLE;
   AccuracyHighDataB->CF3.stop_osz = AccuracyLowDataB->CF3.stop_osz = PS081_STOP_OSZ_INACTIVE;
   AccuracyHighDataB->CF3.mfake = AccuracyLowDataB->CF3.mfake = mfake;
   AccuracyHighDataB->CF3.bridge = AccuracyLowDataB->CF3.bridge = bridge;



   ComputeStretchModeParameters(AdcParam->Rate, AdcParam->Accuracy, bridge, pptemp, mfake, AccuracyLowDataA, AccuracyLowDataB);
AccuracyLowDataB->CF3.sel_start_osz = PS081_SEL_START_OSZ_100U;
AccuracyLowDataB->CF3.stretch = PS081_STRETCH_2R_200U;
   ComputeStretchModeParameters(AdcParam->Rate, 100, bridge, pptemp, mfake, AccuracyHighDataA, AccuracyHighDataB);
AccuracyHighDataB->CF3.sel_start_osz = PS081_SEL_START_OSZ_400U;
AccuracyHighDataB->CF3.stretch = PS081_STRETCH_2R_300U;
   ComputeContinuousModeParameters(AdcParam->Rate, bridge, pptemp, mfake, AccuracyHighDataA, AccuracyHighDataB);

   
   
   
   // Write
   Ps081WriteConfReg(PS081_CONFREG02, AccuracyDataA);	
   Ps081WriteRam(AccuracyDataAUnusedAddress, AccuracyDataAUnused.dw);	
   Ps081WriteConfReg(PS081_CONFREG03, AccuracyDataB);	
   Ps081WriteRam(AccuracyDataBUnusedAddress, AccuracyDataBUnused.dw);	
   
   Ps081WriteRam(RawZeroUnusedAddress, *RawZeroUnused);
   Ps081WriteRam(RawFullRangeUnusedAddress, *RawFullRangeUnused);
   Ps081WriteRam(RawZeroAddr, *RawZero);
   Ps081WriteRam(RawFullRangeAddr, *RawFullRange);
   
   
   return YES;
}

#define DENY_SAMPLES    10

static TYesNo DebugInitParamRead = NO;
static byte SamplesDenyCount = 0;


struct {
   dword Prefilter;
   dword AveragingWindow;
   dword LowPassPeriod;
   dword AveragingWindowNative;
   dword Factor;
   dword RawZero;
   dword RawFullRange;
   dword Zero;
   dword FullRange;
   dword FilterType;
   dword FilterValueA;
   dword FilterValueB;
   dword FilterValueC;
   dword FilterValueD;
   dword PrefilterCounter;
} DebugStruct;
volatile dword Value;
ISR(Ps081DataReadyIntVector)
// New ADC value ISR
{
   TDataSample Sample;
   /*if(!DebugInitParamRead) {
      SysDelay(1000);
	  Ps081ReadRam(PrefilterAddress, &DebugStruct.Prefilter);
      Ps081ReadRam(AveragingWindowAddress, &DebugStruct.AveragingWindow);
	  Ps081ReadRam(PreLowPassPeriodAddress, &DebugStruct.LowPassPeriod);
	  Ps081ReadRam(AveragingWindowNativeAddress, &DebugStruct.AveragingWindowNative);
	  Ps081ReadRam(FactorAddress, &DebugStruct.Factor);
      Ps081ReadRam(RawZeroAddr, &DebugStruct.RawZero);
      Ps081ReadRam(RawFullRangeAddr, &DebugStruct.RawFullRange);
      Ps081ReadRam(ZeroAddr, &DebugStruct.Zero);
      Ps081ReadRam(FullRangeAddr, &DebugStruct.FullRange);
      Ps081ReadRam(FilterTypeAddress, &DebugStruct.FilterType);
	  Ps081ReadRam(FilterValueAAddress, &DebugStruct.FilterValueA);
	  Ps081ReadRam(FilterValueBAddress, &DebugStruct.FilterValueB);
	  Ps081ReadRam(FilterValueCAddress, &DebugStruct.FilterValueC);
	  Ps081ReadRam(FilterValueDAddress, &DebugStruct.FilterValueD);
	  Ps081ReadRam(PrefilterCounterAddress, &DebugStruct.PrefilterCounter);
	  DebugInitParamRead = YES;
   }*/
   
   // read control register first
   Ps081ReadRam(ControlRegisterAddr, &Value);

   if((Value & (1 << VALID_DATA))) {
	  if(SamplesDenyCount) {
	     SamplesDenyCount--;
		 return;
	  }
	  
      switch(DetectorParameters.Mode.Mode) {
	     case DETECTOR_MODE_PS081:
		    Sample.Ps081.Stable = (Value & (1 << STABLE_VALUE)) ? YES : NO;
			
		    Ps081ReadRam(ResultAddress, &Value);  // Read result
		    Sample.Ps081.Weight = Ps081ToWeight(Value);
			
			Ps081ReadRam(RawValueAddress, &Value);
	        Sample.Ps081.Raw = Value;

			//Ps081ReadRam(LowPassInputAddress, &Value);
	        //Sample.Ps081.LowPassInput = Ps081ToWeight(Value);
			
			//Ps081ReadRam(LowPassOutputAddress, &Value);
	        //Sample.Ps081.LowPassOutput = Ps081ToWeight(Value);

			//Ps081ReadRam(PreAvgCounterAddress, &Value);
	        //Sample.Ps081.PreAvgCounter = Value;
			
            //Ps081ReadRam(HighPassAbsAddress, &Value);
	        //Sample.Ps081.HighPassAbs = Ps081ToWeight(Value);
			
			//Ps081ReadRam(StableWindowCounterAddress, &Value);
	        //Sample.Ps081.StableWindowCounter = Value;
			
			//Ps081ReadRam(PrefilterInputAddress, &Value);
			//Ps081ReadRam(PrefilterCounterAddress, &Value);
			break;
			
		 default:
		    Ps081ReadRam(ResultAddress, &Value);  // Read result
		    Sample.Weight = Ps081ToWeight(Value);
		    break;
	  }

	  DetectorNewAdcValue(&Sample); // Convert and pass result
   }   
}



TYesNo AdcStart(TAdcParametersPs081 *AdcParam, TDetectionParameters *DetectionParam, TCalibrationData *CalibrationData)
// Start Adc
{
   byte AllFlag;
// After writing to the RAM via SPI it is necessary to wait for 10 ?s.
   

   
   Ps081Init();      // Init Ps081
   Ps081ComEnable();

   Ps081PowerReset(); // Reset
   SysDelay(10);
   
   SetAdcParameters(AdcParam, DetectionParam, CalibrationData);

   Ps081ReadConfReg(PS081_CONFREG00, &ConfReg);
   ConfReg.CF0.tdc_conv_cnt = 59;
   ConfReg.CF0.dis_osc_startup = PS081_DIS_OSC_STARTUP_OFF;
   ConfReg.CF0.sel_compr = PS081_SEL_COMPR_7K;
   Ps081WriteConfReg(PS081_CONFREG00, ConfReg);

   Ps081ReadConfReg(PS081_CONFREG01, &ConfReg);
   ConfReg.CF1.messb2 = 1;
   ConfReg.CF1.tdc_sleepmode = 1;
   ConfReg.CF1.mult_en_ub = 1;
   ConfReg.CF1.rspan_by_temp = 0;	
   ConfReg.CF1.mult_en_pp = 1;
   ConfReg.CF1.mod_rspan = 1;
   Ps081WriteConfReg(PS081_CONFREG01, ConfReg);
   

   
   ConfReg.CF4.Mult_Hb1 = 0x7A0000;		
   Ps081WriteConfReg(PS081_CONFREG04, ConfReg);
   
   ConfReg.CF5.Mult_Hb2 = 0x7A0000;		
   Ps081WriteConfReg(PS081_CONFREG05, ConfReg);
   
   ConfReg.CF6.Mult_Hb3 = 0x800000;		
   Ps081WriteConfReg(PS081_CONFREG06, ConfReg);
   
   ConfReg.CF7.Mult_Hb4 = 0x800000;		
   Ps081WriteConfReg(PS081_CONFREG07, ConfReg);
   
   ConfReg.CF8.Mult_TkG = 1048576;		
   Ps081WriteConfReg(PS081_CONFREG08, ConfReg);
   
   ConfReg.CF9.Mult_TkO = 0;		
   Ps081WriteConfReg(PS081_CONFREG09, ConfReg);
   
   Ps081ReadConfReg(PS081_CONFREG10, &ConfReg);
   ConfReg.CF10.Mult_PP = 164;		
   ConfReg.CF10.Mult_Ub = 251;		
   Ps081WriteConfReg(PS081_CONFREG10, ConfReg);
   
   Ps081ReadConfReg(PS081_CONFREG11, &ConfReg);
   ConfReg.CF11.con_comp = PS081_CON_COMP_OFF_MEASUREMENTS;		
   ConfReg.CF11.sel_compin = PS081_SEL_COMPIN_EXTERNAL;		
   Ps081WriteConfReg(PS081_CONFREG11, ConfReg);

   Ps081WriteRam(FilterTypeAddress, AdcParam->Filter);
   Ps081WriteRam(PrefilterAddress, AdcParam->Prefilter);

   // Detection parameters
   Ps081WriteRam(AveragingWindowAddress, DetectionParam->AveragingWindow);
   Ps081WriteRam(RangeAddress, DetectionParam->AbsoluteRange);
   Ps081WriteRam(StableWindowAddress, DetectionParam->StableWindow);

   // Calibration data

   Ps081WriteRam(ZeroAddr, CalibrationData->Zero);
   Ps081WriteRam(FullRangeAddr, CalibrationData->FullRange);

   /*
      Ud�lat switch modu podle p�edan�ch parametr�??
      P�ed�ny pouze kalibra�n� a ADC parametry = MODE_ALL, p�ed�ny i detek�n� parametry = 

MODE_STABLE
   */
   
   switch(DetectorParameters.Mode.Mode) {
      case DETECTOR_MODE_ALL:
	  case DETECTOR_MODE_PS081:
	     // ADC will send every sample
         AllFlag = 1;
		 SamplesDenyCount = DENY_SAMPLES;
		 break;

	  default:
	     // ADC will send only stable samples
	     AllFlag = 0;
	     break;
   }

   // Start ADC
   Ps081WriteRam(ControlRegisterAddr, (1 << WAKE_UP) | (AllFlag << ALL));
   Ps081InitReset();

   // Enable interrupt
   Ps081DataReadyIntInit();
   Ps081DataReadyIntEnable();
   
   return YES;
}

TYesNo AdcStop()
// Stop ADC
{
   Ps081ReadConfReg(PS081_CONFREG02, &ConfReg2);
   Ps081ReadConfReg(PS081_CONFREG03, &ConfReg3);
   Ps081ComDisable();
   Ps081DataReadyIntDisable();
   
   DebugInitParamRead = NO;
   
   return YES;
}

static TWeight Ps081ToWeight(dword Value)
// Ps081 -> Weight
{
   if(Value & ((dword)1 << (PS081_BITS - 1))) {
      Value |= 0xFFFFFFFF & (~(((dword)1 << PS081_BITS) - 1)); // Add leading 1s to negative number
   }
   
   return Value;
}