//*****************************************************************************
//
//   DpDef.h     Detector protocol
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __DpDef_H__
   #define __DpDef_H__
   
#include "Uni.h"
#include "../../AVR8/Library/Bat2Detector/DetectorDef.h"

// Detector Command
typedef enum {
   ADC_PARAMETERS,
   CALIBRATION,
   CALIBRATION_DATA,
   DETECTION,
   DATA,
   DETECTOR_MODE,
   START,
   STOP,
   FIRMWARE_START,
   FIRMWARE_CHUNK,
   FIRMWARE_STOP,
   __INVALID_CMD
} EDetectorCommand;

// Detector reply status
typedef enum {
   STATUS_OK,
   STATUS_ERROR
} EReplyStatus;


#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

//------------------------------------------------------------------------------
// Parameter command
//------------------------------------------------------------------------------

typedef struct  {
   EDetectorCommand Command;
   union {
      TAdcParameters Adc;
      TModeParameters Mode;
	  TDetectionParameters Detection;
	  TCalibrationParameters Calibration;
	  TCalibrationData CalibrationData;
   };
} __packed TParametersQuery;


typedef struct {
   EDetectorCommand Command;
   EReplyStatus Status;
} __packed TParametersReply;

//------------------------------------------------------------------------------
// Parameter command
//------------------------------------------------------------------------------

typedef struct  {
   EDetectorCommand Command;
   TCalibrationParameters Parameters;
} __packed TCalibrationQuery;


typedef struct {
   EDetectorCommand Command;
   EReplyStatus Status;
   TCalibrationData CalibrationData;
} __packed TCalibrationReply;

//------------------------------------------------------------------------------
// Calibration data read command
//------------------------------------------------------------------------------

typedef struct  {
   EDetectorCommand Command;
} __packed TCalibrationDataQuery;


typedef struct {
   EDetectorCommand Command;
   TCalibrationData CalibrationData;
} __packed TCalibrationDataReply;

//------------------------------------------------------------------------------
// Firmware chunk command
//------------------------------------------------------------------------------

typedef struct {
   EDetectorCommand Command;
   byte ChunkNum;
   byte Chunk[PS081_FIRMWARE_CHUNK_SIZE];
} __packed TFirmwareChunkQuery;

typedef struct {
   EDetectorCommand Command;
   EReplyStatus Status;
} __packed TFirmwareChunkReply;

//------------------------------------------------------------------------------
// Simple Command
//------------------------------------------------------------------------------

typedef struct {
   EDetectorCommand Command;
} __packed TSimpleCommandQuery;

typedef struct {
   EDetectorCommand Command;
   EReplyStatus Status;
} __packed TSimpleCommandReply;


#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

//------------------------------------------------------------------------------
// Query union
//------------------------------------------------------------------------------

typedef union {
   EDetectorCommand Command;
   TSimpleCommandQuery SimpleCommand;
   TParametersQuery Parameters;
   TFirmwareChunkQuery FirmwareChunk;
   TCalibrationDataQuery CalibrationData;
   TCalibrationQuery Calibration;
} TDpQuery;

//------------------------------------------------------------------------------
// Reply union
//------------------------------------------------------------------------------

typedef union {
   EDetectorCommand Command;
   TParametersReply Parameters;
   TSimpleCommandReply SimpleCommand;
   TFirmwareChunkReply FirmwareChunk;
   TCalibrationDataReply CalibrationData;
   TCalibrationReply Calibration;
} TDpReply;

//------------------------------------------------------------------------------
// Data union
//------------------------------------------------------------------------------

typedef struct {
   EDetectorCommand Command;
   TData Data;
} TDpDataReply;

#endif