#include "../Library/Bat2Detector/DpCom.h"
#include "Uart0.h"

#define REPLY_TIMEOUT   100

// Status
static EDpComStatus Status = DP_COM_IDLE;

// Received data size
static word DataSize;


void DpComInit()
// Init
{
   Uart0Init();
}

void DpComReceive(byte *Buffer, byte DataLength)
// Init receive
{
   DataSize = 0;
   Status = DP_COM_TIMEOUT;

   if(!Uart0RxWait(REPLY_TIMEOUT)) {
	   return;
   }

   while(Uart0RxChar(Buffer) && DataLength > DataSize) {
      Buffer++;
	  DataSize++;
   }
}

word DpComDataSizeGet()
// Get received size
{
   return DataSize;
}

EDpComStatus DpComStatus()
// Status
{
	return Status;
}

void DpComSend(byte *Buffer, byte DataLength)
// Send data
{
   while(DataLength--) {
	   Uart0TxChar(*Buffer);
	   Buffer++;
   }
   
   Status = DP_COM_SEND_DONE;
}

void DpComStop()
// Stop all
{
   // stop receive and send activity here
   Status = DP_COM_IDLE;
}