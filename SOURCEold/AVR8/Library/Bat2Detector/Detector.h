//*****************************************************************************
//
//   Detector.h     Detector
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "Uni.h"
#include "../../AVR8/Library/Bat2Detector/DetectorDef.h"

// Detector parameters
typedef struct {
   TAdcParameters Adc;
   TDetectionParameters Detection;
   TModeParameters Mode;
   TCalibrationParameters Calibration;
   TCalibrationData CalibrationData;
} TDetectorParameters;

extern TDetectorParameters DetectorParameters;

void DetectorNewAdcValue(TDataSample *Sample);
// New ADC value

TYesNo DetectorStart();
// Stop detector

TYesNo DetectorStop();
// Start detector

TYesNo DetectorFirmwareStart();

TYesNo DetectorFirmwareChunk(byte *Chunk, byte ChunkNum);

TYesNo DetectorFirmwareStop();

TYesNo DetectorCalibrate();