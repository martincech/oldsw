//*****************************************************************************
//
//   Dp.h     Detector protocol
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Dp_H__
   #define __Dp_H__
   
#include "Uni.h"

void DpInit();
// Init

void DpExecute();
// Execute Detector protocol

void DpSendAsynch(word DataLength);
// Send data without master's query

void *DpGetAsynchBuffer();
// Data buffer

void DpStop();
// Stop

#endif