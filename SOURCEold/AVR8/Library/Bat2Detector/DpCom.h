#ifndef __DpCom_H__
   #define __DpCom_H__
   
#include "Uni.h"

// Com status
typedef enum {
   DP_COM_TIMEOUT,
   DP_COM_IDLE,   
   DP_COM_SEND_DONE   
} EDpComStatus;

void DpComInit();
// Com Init

void DpComReceive(byte *Buffer, byte DataLength);
// Init receive

word DpComDataSizeGet();
// Get received size

void DpComSend(byte *Buffer, byte DataLength);
// Send data

EDpComStatus DpComStatus();
// Status

void DpComStop();
// Stop all

#endif