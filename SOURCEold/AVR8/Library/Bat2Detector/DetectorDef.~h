//*****************************************************************************
//
//   DetectorDef.h     DetectorDef
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************
#ifndef __DetectorDef_H__
   #define __DetectorDef_H__
   
#include "Uni.h"
#include "Hardware.h"
#include "../../AVR8/Library/Bat2Detector/Adc/AdcPs081Def.h"

// Weight
typedef sdword TWeight;

// Time
typedef dword TTimestamp;

#ifdef __WIN32__
   #pragma pack( push, 1)                   // byte alignment
#endif

// Stable weight sample
typedef struct {
   TWeight Weight;
   TTimestamp Time;
} __packed TStableWeight;

// PS081 debug weight sample
typedef struct {
   TWeight Weight;
   dword Raw;
   TYesNo Stable;
   TWeight LowPassInput;
   TWeight LowPassOutput;
   TWeight HighPassAbs;
   byte StableWindowCounter;
    byte PreAvgCounter;
} __packed TPs081Weight;

// Detector modes
typedef enum {
   DETECTOR_MODE_DEFAULT,    // Send only accepted samples
   DETECTOR_MODE_ALL,        // Send all samples
   DETECTOR_MODE_STABLE,     // Send only stable samples
   DETECTOR_MODE_PS081      // PS081 debug mode
} EDetectorMode;

// Mode parameters
typedef struct {
   EDetectorMode Mode;
   word BurstSize;
   byte Averaging;   // All mode only
} __packed TModeParameters;

// Calibration data
typedef struct {
   TWeight Zero;
   dword   RawZero;
   TWeight FullRange;
   dword   RawFullRange;
} __packed TCalibrationData;

// Calibration parameters
typedef struct {
   TWeight Zero;
   TWeight FullRange;
   word Delay;
   word Duration;
} __packed TCalibrationParameters;

// Detection parameters
typedef struct {
   byte AveragingWindow;
   TWeight AbsoluteRange;
   byte StableWindow;
} __packed TDetectionParameters;

// Data buffer
#define DATA_ALL_BURST_SIZE_MAX  15

typedef union {
   TStableWeight StableWeight;
   TWeight Weight;
   TPs081Weight Ps081;
} TDataSample;

typedef struct {
   word DataCount;
   union {
      TStableWeight DataStable[DATA_ALL_BURST_SIZE_MAX];
      TWeight DataAll[DATA_ALL_BURST_SIZE_MAX];
	  TPs081Weight DataPs081[DATA_ALL_BURST_SIZE_MAX];
   };
} TData;

#ifdef __WIN32__
   #pragma pack( pop)                       // original alignment
#endif

// Adc
#ifdef PICOSTRAIN
   typedef TAdcParametersPs081 TAdcParameters;
#else
   #error "ADC not supported"
#endif

#endif