//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Uart0.h"
#include <stdio.h>

int UartPutchar( char c, FILE *stream);
FILE UartStream = FDEV_SETUP_STREAM( UartPutchar, NULL, _FDEV_SETUP_WRITE);

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   stdout = &UartStream;
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   Uart0TxChar( 'A');
   Uart0TxChar( 'B');
   Uart0TxChar( 'C');
   // main loop
   forever {
      if( CalButtonPressed()){
         printf( "Button\n");            // button pressed
      }
   }
   return( 0);
} // main


//-----------------------------------------------------------------------------
// Uart putchar
//-----------------------------------------------------------------------------

int UartPutchar( char c, FILE *stream)
{
   if( c == '\n'){
      Uart0TxChar( '\r');              // translate LF to CR+LF
   }
   Uart0TxChar( c);
   return( 0);
} // UartPutchar
