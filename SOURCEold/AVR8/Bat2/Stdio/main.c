//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   StdioInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   Uart0TxChar( 'A');
   Uart0TxChar( 'B');
   Uart0TxChar( 'C');
   // main loop
   forever {
      if( CalButtonPressed()){
         cprintf( "Button\n");         // button pressed
      }
   }
   return( 0);
} // main



