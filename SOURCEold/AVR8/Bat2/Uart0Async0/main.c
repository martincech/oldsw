//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Uart0Async.h"

#define BUFFER_SIZE 128

byte Buffer[ BUFFER_SIZE];
word Size;

static volatile TYesNo TxDone;
static volatile TYesNo RxDone;

static volatile byte RxStatus;
static volatile word RxSize;

static void TxChar( byte ch);
// send single character

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);

   SysStartTimer();
   EnableInts();
   // send data on start :
   TxDone = NO;
   Size = 0;
   Buffer[ Size++] = 'A';
   Buffer[ Size++] = 'B';
   Buffer[ Size++] = 'C';
   Uart0Tx( Buffer, Size);
   while( !TxDone);
   // main loop
   forever {
/*
      if( CalButtonPressed()){
         Size = 0;
         Buffer[ Size++] = 'X';
         Uart0Tx( Buffer, Size);         // button pressed
      }
*/
      RxStatus = UART_RX_RUNNING;
      Uart0Rx( Buffer, 3);
      while( RxStatus ==  UART_RX_RUNNING);
      switch( RxStatus){
         case UART_RX_DONE :
            // data received
            TxDone = NO;
            Uart0Tx( Buffer, RxSize);    // echo data
            while( !TxDone);
            break;

         case UART_RX_REPLY_TIMEOUT :
            TxChar( 'r');
            break;
         case UART_RX_INTERCHARACTER_TIMEOUT :
            TxChar( 'i');
            if( RxSize > 0){
               TxDone = NO;
               Uart0Tx( Buffer, RxSize); // echo data
               while( !TxDone);
            }
            break;
         case UART_RX_ERROR :
            TxChar( 'e');
            break;
         default :
            TxChar( '?');
            break;
      }
   }
   return( 0);
} // main

//-----------------------------------------------------------------------------
// Tx done
//-----------------------------------------------------------------------------

void Uart0TxDone( void)
// Transmit done callback
{
   TxDone = YES;
} // Uart0TxDone

//-----------------------------------------------------------------------------
// Rx done
//-----------------------------------------------------------------------------

void Uart0RxDone( byte Status, word Count)
// Receive callback, terminate with <Status>, <Count> character received
{
   RxStatus = Status;
   RxSize   = Count;
} // Uart0RxDone

//-----------------------------------------------------------------------------
// Tx char
//-----------------------------------------------------------------------------

static void TxChar( byte ch)
// send single character
{
   TxDone     = NO;
   Uart0Tx( &ch, 1);               // echo character
   while( !TxDone);
} // TxChar

//------------------------------------------------------------------------------
// Timer start
//------------------------------------------------------------------------------

#define TIMER_COUNT  (125 - 1)     // 1000Hz/1ms tick (8MHz / 64 / 125)


void SysStartTimer( void)
// Start system timer
{
   OCR0A   = TIMER_COUNT;               // max. timer value
   TCCR0A  = (2 << WGM00);              // WGM 2 (reset mode), COM 0 (no output)
   TCCR0B  = (3 << CS00);               // prescaler clk/64
   TIMSK0 |= (1 << OCIE0A);             // enable compare interrupt
} // SysStartTimer

//------------------------------------------------------------------------------
// Timer handler
//------------------------------------------------------------------------------

ISR( TIMER0_COMPA_vect)
{
   Uart0Timer();
} // TimerHandler
