//*****************************************************************************
//
//    Uart0.h - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Uart0_H__
   #define __Uart0_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Rx status :
typedef enum {
   UART_RX_RUNNING,                   // receive in progress
   UART_RX_DONE,                      // recived <Size> characters
   UART_RX_REPLY_TIMEOUT,             // no reply
   UART_RX_INTERCHARACTER_TIMEOUT,    // intercharacter timeout break
   UART_RX_ERROR,                     // framing error, wrong parity or data overrun
}  TUartRxStatus;

// Rem : Status < UART_RX_DONE = received characters count

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void Uart0Init( void);
// Communication initialisation

void Uart0Tx( void *Buffer, word Size);
// Transmit <Buffer> with <Size>

void Uart0TxDone( void);
// Transmit done callback

void Uart0Rx( void *Buffer, word Size);
// Receive <Buffer> with <Size>

void Uart0RxDone( byte Status, word Count);
// Receive callback, terminate with <Status>, <Count> character received

void Uart0FlushChars( void);
// Skip all Rx chars

void Uart0Timer( void);
// Timer callback

#endif
