//*****************************************************************************
//
//    Uart0Async.c  RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "../../Inc/Uart0Async.h"
#include "../../Inc/System.h"

//!!!------>>>

// parameters :
#ifdef UART0_DOUBLE
   #define BAUD_DIVISOR ((FXTAL / 8  / UART0_BAUD) - 1)
   #define UART_DOUBLE
#else
   #define BAUD_DIVISOR ((FXTAL / 16 / UART0_BAUD) - 1)
#endif

#ifdef UART0_PARITY_EVEN
   #define UART_PARITY_EVEN
#endif   
   
#ifdef UART0_PARITY_ODD
   #define UART_PARITY_ODD
#endif   

#define UART_RX_TIMEOUT UART0_RX_TIMEOUT

#define UBRRH UBRR0H
#define UBRRL UBRR0L
#define UCSRA UCSR0A
#define UCSRB UCSR0B
#define UCSRC UCSR0C
#define UDR   UDR0

//!!!-----<<<

#define EnableRxInt()       UCSRB |=  (1 << RXCIE0)
#define DisableRxInt()      UCSRB &= ~(1 << RXCIE0)

#define EnableTxInt()       UCSRB |=  (1 << UDRIE0)
#define DisableTxInt()      UCSRB &= ~(1 << UDRIE0)

#define EnableTxDoneInt()   UCSRB |=  (1 << TXCIE0)
#define DisableTxDoneInt()  UCSRB &= ~(1 << TXCIE0)
#define ClearTxDone()       UCSRA &= ~(1 << TXC0)

#define RxError()          (UCSRA & ((1 << FE0) | (1 << DOR0) | (1 << UPE0)))

// Rx buffer data :
static byte *RxBuffer;
static word RxSize;
static word RxIndex;
static byte RxStatus;
static word RxTimer;

// Tx buffer data :
static byte *TxBuffer;
static word TxSize;
static word TxIndex;

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void Uart0Init( void)
// Communication initialisation
{
   UBRRH = (byte)(BAUD_DIVISOR >> 8);  // set baud rate divisor
   UBRRL = (byte) BAUD_DIVISOR;
   UCSRB = (1 << RXEN0) | (1 << TXEN0);// enable Rx, Tx
   #if defined( UART_PARITY_EVEN)
      UCSRC = (3 << UCSZ00) | (2 << UPM0);  // 8 bits, 1 stopbit, even
   #elif defined( UART_PARITY_ODD)
      UCSRC = (3 << UCSZ00) | (3 << UPM0);  // 8 bits, 1 stopbit, odd
   #else // no parity
      UCSRC = (3 << UCSZ00);                // 8 bits, 1 stopbit
   #endif   
   #ifdef UART_DOUBLE
      UCSRA |= (1 << U2X0);                 // double baudrate
   #endif
   // disable interrupts :
   DisableRxInt();
   DisableTxInt();
   DisableTxDoneInt();
   RxTimer = 0;                             // disable timer
} // UartInit

//-----------------------------------------------------------------------------
// Tx buffer
//-----------------------------------------------------------------------------

void Uart0Tx( void *Buffer, word Size)
// Transmit <Buffer> with <Size>
{
   // remember Tx data :
   TxBuffer = (byte *)Buffer;
   TxSize   = Size;
   TxIndex  = 0;
   // enable transmitter :
   Uart0TxEnable();                    // RS485 direction to Tx
   EnableTxInt();                      // enable Tx data interrupt
} // UartTxChar

//-----------------------------------------------------------------------------
// Rx buffer
//-----------------------------------------------------------------------------

void Uart0Rx( void *Buffer, word Size)
// Receive <Buffer> with <Size>
{
   // remember Rx data :
   RxBuffer = (byte *)Buffer;
   RxSize   = Size;
   RxIndex  = 0;
   RxStatus = UART_RX_REPLY_TIMEOUT;   // wait for first character
   SysDisableTimer();                  // disable timer interrupt
   RxTimer  = UART0_REPLY_TIMEOUT;     // up to reply timeout
   SysEnableTimer();                   // enable timer interrupt
   // enable receiver :
   EnableRxInt();                      // enable Rx data interrupt
} // Uart0Rx

//-----------------------------------------------------------------------------
// Timer
//-----------------------------------------------------------------------------

void Uart0Timer( void)
// Timer callback
{
   if( RxTimer && !--RxTimer){
      DisableRxInt();
      Uart0RxDone( RxStatus, RxIndex);
   }
} // Uart0Timer

//-----------------------------------------------------------------------------
// Flush Rx
//-----------------------------------------------------------------------------

void Uart0FlushChars( void)
// Skip all Rx chars
{
byte c;

   while( UCSRA & (1 << RXC0)){
      c  = UDR;                        // get character
   }
} // UartFlushChars


//-----------------------------------------------------------------------------
// Tx data interrupt
//-----------------------------------------------------------------------------

ISR( USART0_UDRE_vect)
{
   UDR = TxBuffer[ TxIndex++];
   if( TxIndex == TxSize){
      DisableTxInt();                  // stop Tx data interrupt
      ClearTxDone();                   // clear Tx done flag
      EnableTxDoneInt();               // wait for shift register empty
   }
} // USART0_UDRE_vect

//-----------------------------------------------------------------------------
// Tx done interrupt
//-----------------------------------------------------------------------------

ISR( USART0_TX_vect)
{
   Uart0TxDisable();                   // RS485 direction to Rx
   DisableTxDoneInt();                 // stop Tx done interrupt
   Uart0TxDone();                      // Tx done callback
} // USART0_TX_vect


//-----------------------------------------------------------------------------
// Rx done interrupt
//-----------------------------------------------------------------------------

ISR( USART0_RX_vect)
{
byte Data;
byte Error;

   Error = RxError();                            // must be read before UDR
   Data  = UDR;                                  // read data register
   if( Error){
      RxTimer = 0;                               // disable timeout
      DisableRxInt();                            // disable Rx data interrupt
      Uart0RxDone( UART_RX_ERROR, RxIndex);
      return;
   }
   RxStatus = UART_RX_INTERCHARACTER_TIMEOUT;    // wait for next character
   RxTimer  = UART0_INTERCHARACTER_TIMEOUT;      // up to intercharacter timeout
   RxBuffer[ RxIndex++] = Data;
   if( RxIndex == RxSize){
      RxTimer = 0;                               // disable timeout
      DisableRxInt();                            // disable Rx data interrupt
      Uart0RxDone( UART_RX_DONE, RxIndex);       // Rx done callback
   }
} // USART0_RX_vect
