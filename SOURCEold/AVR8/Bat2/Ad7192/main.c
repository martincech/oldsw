//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"
#include "../Inc/Ad7192.h"

#define CONVERSION_COUNT   10
#define CONVERSION_TIMEOUT 100        // timeout [ms]

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
dword Value;
byte  ch;
int   i;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   AdcInit();
   StdioInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   Uart0TxChar( 'A');
   Uart0TxChar( 'B');
   Uart0TxChar( 'C');
   Uart0TxChar( '\r');
   Uart0TxChar( '\n');
   AdcCheck();
   // main loop
   forever {
      if( !Uart0RxWait( 0)){
         continue;
      }
      if( !Uart0RxChar( &ch)){
         continue;
      }
      switch( ch){
         case 'm' :
            for( i = 0; i < CONVERSION_COUNT; i++){
               if( !AdcWaitReady( CONVERSION_TIMEOUT)){
                  cprintf( "T.O.\n");
                  continue;
               }
               Value = AdcReadValue();
               cprintf( "%06lX\n", Value);
            }
            break;

         case 'c' :
            AdcCheck();
            break;

         case 'i' :
            AdcInit();
            break;

         case 'x' :
            AdcIdle();
            break;

         case 'p' :
            AdcPowerDown();
            break;

         case 's' :
            AdcSetMode();
            break;

         case 'z' :
            Value = AdcZero();
            cprintf( "%06lX\n", Value);
            break;

         case 'f' :
            Value = AdcFullScale();
            cprintf( "%06lX\n", Value);
            break;

         default :
            break;
      } // switch
   } // forever
   return( 0);
} // main
