//*****************************************************************************
//
//    AdcTesterDef.h  Bat2 ADC tester definitions
//    Version 1.0     (c) VymOs
//
//*****************************************************************************

#ifndef __AdcTesterDef_H__
#define __AdcTesterDef_H__

#define ADC_PACKET_START 0xAA
#define ADC_PACKET_STOP  0xBB

typedef enum {
   CMD_INITIAL_DELAY  = 'd',     // + byte
   CMD_SAMPLES_COUNT  = 'n',     // + byte
   CMD_SPACE_DELAY    = 's',     // + byte
   CMD_SET_CHOP       = 'h',
   CMD_SET_SINC3      = '3',
   CMD_SET_RATE       = 'r',     // + word
   CMD_SET_DEFAULTS   = 'e',
   CMD_MEASURE        = 'm',
   CMD_CALIBRATE_ZERO = 'o',
   CMD_CALIBRATE_FULL = 'f',
   CMD_SET_ZERO       = 'O',     // + word
   CMD_SET_FULL       = 'F',     // + word
} TAdcCommands;

//typedef enum {
//} TAdcReply;

#endif
