//*****************************************************************************
//
//    Hardware.h  - Bat2 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED   7

#define StatusLedInit()  DDRD  |=  (1 << StatusLED)

#define StatusLedOn()    PORTD |=  (1 << StatusLED) 
#define StatusLedOff()   PORTD &= ~(1 << StatusLED)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           38400L     // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]
#define UART0_DOUBLE         1          // double baudrate

#define PUTCHAR_COM0


//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   2

#define CalButtonInit()     DDRD  &= ~(1 << CalButton);\
                            PORTD |=  (1 << CalButton)         // enable pullup

#define CalButtonPressed()  (!(PIND & (1 << CalButton)))       // active L


//------------------------------------------------------------------------------
// AD7192
//------------------------------------------------------------------------------

#define AdcCS       6
#define AdcSCLK     5
#define AdcDIN      4
#define AdcDOUT_RDY 3

#define AdcPortInit()    DDRD  |=  (1 << AdcDIN) | (1 << AdcSCLK) | (1 << AdcCS);\
                         DDRD  &= ~(1 << AdcDOUT_RDY)

#define AdcSetCS()       PORTD |=  (1 << AdcCS)
#define AdcClrCS()       PORTD &= ~(1 << AdcCS)
#define AdcSetSCLK()     PORTD |=  (1 << AdcSCLK)
#define AdcClrSCLK()     PORTD &= ~(1 << AdcSCLK)
#define AdcSetDIN()      PORTD |=  (1 << AdcDIN)
#define AdcClrDIN()      PORTD &= ~(1 << AdcDIN)


#define AdcGetDOUT()       (PIND & (1 << AdcDOUT_RDY))
#define AdcGetRDY()      (!(PIND & (1 << AdcDOUT_RDY)))

// Parameters :
#define ADC_CONVERSION_RATE   600         // conversion rate [Hz]
//#define ADC_SINC3             1           // enable SINC3 filter
//#define ADC_CHOP              1           // enable CHOP mode

//-----------------------------------------------------------------------------

#endif
