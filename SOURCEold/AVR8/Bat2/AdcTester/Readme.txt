Komunikace s testerem
----------------------

Nastaveni UART 38400 Bd bez parity
Pri prijmu je meziznakovy timeout 1000ms

Prikazy pro ADC tester
-----------------------

(X je hexa cislice)

dXX       - pocatecni zpozdeni (od zapnuti mustku 
            do prvniho vzorku) 0..255 ms

nXX       - pocet vzorku

sXX       - zpozdeni po odeslani vzorku do PC 
            (do zapnuti mustku) 0..255 ms

m         - spustit mereni, ukonci se pismenem x

c         - vypis registru A/D prevodniku a parametru

i         - prevodnik do Idle mode

p         - prevodnik do Power down

z         - kalibrace nuly

f         - kalibrace rozsahu

ZXXXXXX   - zapsani hodnoty do Offset registru

FXXXXXX   - zapsani hodnoty do Full Scale registru


Odpovedi testeru :
------------------

po prikazu 'm' posila

START<CR><LF>        // start bloku hodnot
XXXXXX<CR><LF>       // 1. mereny vzorek
...
XXXXXX<CR><LF>       // n. mereny vzorek

po prikazu 'z' posila

XXXXXX<CR><LF>       // hodnota Offset registru

po prikazu 'f' posila

XXXXXX<CR><LF>       // hodnota Full Scale registru
