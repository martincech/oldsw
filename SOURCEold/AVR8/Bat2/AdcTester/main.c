//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"
#include "../Inc/Ad7192.h"
#include "../Inc/Bcd.h"

#define CONVERSION_TIMEOUT 100        // timeout [ms]
#define MAX_SAMPLES_COUNT  256        // max. samples per burst
#define ICH_DELAY          1000       // intercharacter delay

byte StartDelay;         // from start to measure [ms]
byte SamplesCount;       // samples count
byte SpaceDelay;         // space delay [ms]

// Local functions :

static byte ReadByte( void);
// read byte from uart

static dword ReadWord( void);
// read word from uart

static void Measure( void);
// run measuring loop

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
dword Value;
byte  ch;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   AdcInit();
   StdioInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   AdcCheck();
   // default data :
   StartDelay   = 0;
   SamplesCount = 10;
   SpaceDelay   = 0;
   // main loop
   forever {
      if( !Uart0RxChar( &ch)){
         continue;
      }
      switch( ch){
         case 'd' :
            // initial delay
            StartDelay = ReadByte();
            break;

         case 'n' :
            // samples count
            SamplesCount = ReadByte();
            break;

         case 's' :
            // space delay
            SpaceDelay = ReadByte();
            break;

         case 'm' :
            // measure burst
            Measure();
            break;

         case 'c' :
            // print configuration
            AdcCheck();
            cprintf( "DLY : %dms\n", StartDelay);
            cprintf( "CNT : %d\n",   SamplesCount);
            cprintf( "SPC : %dms\n", SpaceDelay);
            break;

         case 'i' :
            // set idle mode
            AdcIdle();
            break;

         case 'p' :
            // set power down mode
            AdcPowerDown();
            break;

         case 'z' :
            // calibrate for zero
            Value = AdcZero();
            cprintf( "%06lX\n", Value);
            break;

         case 'f' :
            // calibrate for full scale
            Value = AdcFullScale();
            cprintf( "%06lX\n", Value);
            break;

         case 'Z' :
            // set zero calibration
            Value = ReadWord();
            AdcSetZero( Value);
            break;

         case 'F' :
            // set full scale calibration
            Value = ReadWord();
            AdcSetFullScale( Value);
            break;

         default :
            break;
      } // switch
   } // forever
   return( 0);
} // main

//-----------------------------------------------------------------------------
// Read byte
//-----------------------------------------------------------------------------

static byte ReadByte( void)
// read byte from uart
{
byte Value;
byte ch;

   if( !Uart0RxWait( ICH_DELAY)){
      return( 0);
   }
   if( !Uart0RxChar( &ch)){
      return( 0);
   }
   Value  = char2hex( ch) << 4;
   if( !Uart0RxWait( ICH_DELAY)){
      return( 0);
   }
   if( !Uart0RxChar( &ch)){
      return( 0);
   }
   Value |= char2hex( ch);
   return( Value);
} // ReadByte

//-----------------------------------------------------------------------------
// Read word
//-----------------------------------------------------------------------------

static dword ReadWord( void)
// read word from uart
{
dword Value;

   Value  = (dword)ReadByte() << 16;
   Value |= (dword)ReadByte() <<  8;
   Value |= (dword)ReadByte();
   return( Value);
} // ReadWord

//-----------------------------------------------------------------------------
// Measure
//-----------------------------------------------------------------------------

static dword Values[ MAX_SAMPLES_COUNT];

static void Measure( void)
// run measuring loop
{
byte i;
byte ch;

   forever {
      if( Uart0RxWait( 0)){
         if( Uart0RxChar( &ch)){
            // wait for termination character
            if( ch == 'x'){
               AdcPowerDown();         // stop conversion
               return;
            }
         }
      }
      StatusLedOn();                   // show measuring period
      AdcStart();                      // start conversion
      // wait for sampling
      if( StartDelay != 0){
         SysDelay( StartDelay);
      }
      // run sampling
      for( i = 0; i < SamplesCount; i++){
         if( !AdcWaitReady( CONVERSION_TIMEOUT)){
            cprintf( "T.O.\n");
            continue;
         }
         Values[ i] = AdcReadValue();
      }
      AdcPowerDown();                  // stop conversion
      StatusLedOff();                  // stop measuring period
      // print result :
      cprintf( "START\n");
      for( i = 0; i < SamplesCount; i++){
         cprintf( "%06lX\n", Values[ i]);
      }
      if( SpaceDelay != 0){
         SysDelay( SpaceDelay);
      }
   } // forever
} // Measure
