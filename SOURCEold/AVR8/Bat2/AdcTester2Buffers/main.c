//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Stdio.h"
#include "../Inc/Bcd.h"

#include "../Inc/Uart0Async.h"
#include "../Inc/Ad7192b.h"

#include "AdcTesterDef.h"

//-----------------------------------------------------------------------------
// ADC data
//-----------------------------------------------------------------------------

static dword Values0[ ADC_CONVERSION_COUNT];
static dword Values1[ ADC_CONVERSION_COUNT];
static volatile byte ActiveBuffer;

static volatile byte ConversionDone;

static byte StartDelay;         // from start to measure [ms]
static byte SamplesCount;       // samples count
static byte SpaceDelay;         // space delay [ms]

//-----------------------------------------------------------------------------
// UART data
//-----------------------------------------------------------------------------

#define BUFFER_SIZE    AdcPacketSize( ADC_CONVERSION_COUNT)

byte Buffer[ BUFFER_SIZE];
word Size;

static volatile TYesNo TxDone;
static volatile TYesNo RxDone;

static volatile byte RxStatus;
static volatile word RxSize;


//-----------------------------------------------------------------------------
// Commands 
//-----------------------------------------------------------------------------

#define REPLY_TIMEOUT          1000       // reply timeout [ms] 
#define BREAK_TIMEOUT          10         // measure break timeout [ms]
#define FAST_TIMEOUT           1          // measure break timeout [ms]

void Uart0TxChar( byte ch);
// send single character

static TYesNo RxChar( byte *ch);
// wait for character

static byte ReadByte( void);
// read byte from uart

static dword ReadWord( void);
// read word from uart

static void Measure( void);
// run measuring loop

static void MeasureFast( void);
// run fast measuring loop

static void PreparePacket( void);
// copy conversions to packet

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
dword Value;
byte  ch;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   AdcInit();
   StdioInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // timer run :
   SysStartTimer();
   EnableInts();
   // send data on start :
   cprintf( "START\n");
   // default data :
   StartDelay   = 0;
   SamplesCount = 10;
   SpaceDelay   = 0;
   // main loop
   forever {
      Uart0ReplyTimeout( REPLY_TIMEOUT);
      if( !RxChar( &ch)){
         continue;
      }
      switch( ch){
         case CMD_INITIAL_DELAY :
            // initial delay
            StartDelay = ReadByte();
            break;

         case CMD_SAMPLES_COUNT :
            // samples count
            SamplesCount = ReadByte();
            break;

         case CMD_SPACE_DELAY :
            // space delay
            SpaceDelay = ReadByte();
            break;

         case CMD_SET_CHOP :
            AdcChop( YES);
            break;

         case CMD_SET_SINC3 :
            AdcSinc3( YES);
            break;

         case CMD_SET_RATE :
            Value = ReadWord();
            AdcDataRate( (word)Value);
            break;

         case CMD_SET_DEFAULTS :
            AdcChop( NO);
            AdcSinc3( NO);
            AdcDataRate( ADC_CONVERSION_RATE);
            break;

         case CMD_MEASURE :
            Measure();
            break;

         case CMD_MEASURE_FAST :
            MeasureFast();
            break;

         case CMD_CALIBRATE_ZERO :
            Value = AdcZero();
            cprintf( "%06lX\n", Value);
            break;

         case CMD_CALIBRATE_FULL :
            Value = AdcFullScale();
            cprintf( "%06lX\n", Value);
            break;

         case CMD_SET_ZERO :
            // set zero calibration
            Value = ReadWord();
            AdcSetZero( Value);
            break;

         case CMD_SET_FULL :
            // set full scale calibration
            Value = ReadWord();
            AdcSetFullScale( Value);
            break;

         default :
            break;
      } // switch
   } // forever
   return( 0);
} // main


//-----------------------------------------------------------------------------
// Conversion Done
//-----------------------------------------------------------------------------

void AdcConversionDone( void)
// Conversion done callback
{
   ConversionDone = YES;
} // AdcConversionDone

//-----------------------------------------------------------------------------
// Tx done
//-----------------------------------------------------------------------------

void Uart0TxDone( void)
// Transmit done callback
{
   TxDone = YES;
} // Uart0TxDone

//-----------------------------------------------------------------------------
// Rx done
//-----------------------------------------------------------------------------

void Uart0RxDone( byte Status, word Count)
// Receive callback, terminate with <Status>, <Count> character received
{
   RxStatus = Status;
   RxSize   = Count;
} // Uart0RxDone

//-----------------------------------------------------------------------------
// Tx char
//-----------------------------------------------------------------------------

void Uart0TxChar( byte ch)
// send single character
{
   TxDone     = NO;
   Uart0Tx( &ch, 1);               // echo character
   while( !TxDone);
} // Uart0TxChar

//-----------------------------------------------------------------------------
// Rx char
//-----------------------------------------------------------------------------

TYesNo RxChar( byte *ch)
// wait for character
{
   RxStatus = UART_RX_RUNNING;
   Uart0Rx( Buffer, 1);
   while( RxStatus == UART_RX_RUNNING);
   switch( RxStatus){
      case UART_RX_DONE :
         // data received
         *ch = Buffer[ 0];
         return( YES);

      case UART_RX_REPLY_TIMEOUT :
         return( NO);

      case UART_RX_INTERCHARACTER_TIMEOUT :
         return( NO);
      case UART_RX_ERROR :
         return( NO);
      default :
         Uart0TxChar( '?');
         return( NO);
   }
} // RxChar

//-----------------------------------------------------------------------------
// Read byte
//-----------------------------------------------------------------------------

static byte ReadByte( void)
// read byte from uart
{
byte Value;
byte ch;

   if( !RxChar( &ch)){
      return( 0);
   }
   Value  = char2hex( ch) << 4;
   if( !RxChar( &ch)){
      return( 0);
   }
   Value |= char2hex( ch);
   return( Value);
} // ReadByte

//-----------------------------------------------------------------------------
// Read word
//-----------------------------------------------------------------------------

static dword ReadWord( void)
// read word from uart
{
dword Value;

   Value  = (dword)ReadByte() << 16;
   Value |= (dword)ReadByte() <<  8;
   Value |= (dword)ReadByte();
   return( Value);
} // ReadWord

//-----------------------------------------------------------------------------
// Measure
//-----------------------------------------------------------------------------

static void Measure( void)
// run measuring loop
{
byte ch;

   Uart0ReplyTimeout( BREAK_TIMEOUT);
   TxDone         = YES;               // transmitter ready for packet
   AdcSetDelay( StartDelay);           // wait for sampling
   AdcSetSamples( SamplesCount);       // set samples count
   forever {
      // look for termination character :
      if( Uart0RxChar( &ch)){
         if( ch == CMD_STOP){
            AdcPowerDown();         // stop conversion
            return;
         }
      }
      StatusLedOn();                   // show measuring period
      // run sampling :
      ConversionDone = NO;             // wait for conversion
      ActiveBuffer   = 0;              // first buffer active
      AdcSetBuffer( Values0);          // conversions buffer
      AdcStart();                      // start conversion
      while( !ConversionDone);         // wait for conversion
      AdcStop();                       // stop conversion
      AdcPowerDown();                  // switch power off
      ActiveBuffer   = 1;              // mark first buffer as ready
      StatusLedOff();                  // stop measuring period
      // send result :
      while( !TxDone);                 // wait for packet sent
      TxDone = NO;                     // prepare wait for next packet
      PreparePacket();
      Uart0Tx( Buffer, AdcPacketSize( SamplesCount));
      // wait between conversions :
      if( SpaceDelay != 0){
         SysDelay( SpaceDelay);
      }
   } // forever
} // Measure

//------------------------------------------------------------------------------
// Measure fast
//------------------------------------------------------------------------------

static void MeasureFast( void)
// run fast measuring loop
{
byte ch;

   Uart0ReplyTimeout( FAST_TIMEOUT);
   TxDone         = YES;               // transmitter ready for packet
   ActiveBuffer   = 0;                 // first buffer active
   AdcSetDelay( StartDelay);           // wait for sampling
   AdcSetSamples( SamplesCount);       // set samples count
   AdcSetBuffer( Values0);             // first conversions buffer
   ConversionDone = NO;                // wait for conversion
   AdcStart();                         // start conversions
   forever {
      StatusLedOn();                   // show measuring period
      // wait for conversion :
      do {
         // look for termination character :
         if( Uart0RxChar( &ch)){
            if( ch == CMD_STOP){
               AdcStop();                 // stop conversion
               AdcPowerDown();            // ADC off
               return;
            }
         }
      } while( !ConversionDone);       // wait for conversion
      ConversionDone = NO;             // confirm conversion
      // swap buffers :
      if( !ActiveBuffer){
         AdcSetBuffer( Values1);       // second conversions buffer
         ActiveBuffer = 1;
      } else {
         AdcSetBuffer( Values0);       // first conversions buffer
         ActiveBuffer = 0;
      }
      StatusLedOff();                  // stop measuring period
      // send result :
      while( !TxDone);                 // wait for packet sent
      TxDone = NO;                     // prepare wait for next packet
      PreparePacket();                 // prepare packet
      Uart0Tx( Buffer, AdcPacketSize( SamplesCount));
   } // forever
} // MeasureFast

//------------------------------------------------------------------------------
// Prepare packet
//------------------------------------------------------------------------------

static void PreparePacket( void)
// copy conversions to packet
{
int  i, j;
int  Count;
byte *Src;

   if( !ActiveBuffer){
      // now is first buffer active
      Src   = (byte *)Values1;         // send second buffer
   } else {
      // now is second buffer active
      Src   = (byte *)Values0;         // send first buffer
   }
   j     = 0;
   Count = SamplesCount * sizeof( dword);
   // scan Values array :
   Buffer[ j++] = ADC_PACKET_START;
   for( i = 0; i < Count; i++){
      if( (i & ADC_VALUE_SIZE) == ADC_VALUE_SIZE){
         continue;                     // skip every 4th byte
      }
      Buffer[ j++] = Src[ i];
   }
   Buffer[ j++] = ADC_PACKET_STOP;
} // PreparePacket

//------------------------------------------------------------------------------
// Timer start
//------------------------------------------------------------------------------

#define TIMER_COUNT  (125 - 1)     // 1000Hz/1ms tick (8MHz / 64 / 125)


void SysStartTimer( void)
// Start system timer
{
   OCR0A   = TIMER_COUNT;               // max. timer value
   TCCR0A  = (2 << WGM00);              // WGM 2 (reset mode), COM 0 (no output)
   TCCR0B  = (3 << CS00);               // prescaler clk/64
   TIMSK0 |= (1 << OCIE0A);             // enable compare interrupt
} // SysStartTimer

//------------------------------------------------------------------------------
// Timer handler
//------------------------------------------------------------------------------

ISR( TIMER0_COMPA_vect)
{
   Uart0Timer();
} // TimerHandler
