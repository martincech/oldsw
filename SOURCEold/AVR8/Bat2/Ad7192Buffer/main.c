//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"
#include "../Inc/Ad7192b.h"

#define CONVERSION_COUNT   10
#define CONVERSION_TIMEOUT 500        // timeout [ms]

dword Values[ CONVERSION_COUNT];

static volatile byte ConversionDone;

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
dword Value;
byte  ch;
int   i;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   AdcInit();
   StdioInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   Uart0TxChar( 'A');
   Uart0TxChar( 'B');
   Uart0TxChar( 'C');
   Uart0TxChar( '\r');
   Uart0TxChar( '\n');
   EnableInts();
   // main loop
   forever {
      if( !Uart0RxWait( 0)){
         continue;
      }
      if( !Uart0RxChar( &ch)){
         continue;
      }
      switch( ch){
         case 'm' :
            ConversionDone = NO;
            AdcSetDelay( 0);
            AdcSetSamples( CONVERSION_COUNT);
            AdcSetBuffer( Values);
            while( !ConversionDone);
            for( i = 0; i < CONVERSION_COUNT; i++){
               cprintf( "%06lX\n", Values[ i]);
            }
            break;

         case 'r' :
            AdcReset();
            break;

         case 'i' :
            AdcInit();
            break;

         case 'x' :
            AdcIdle();
            break;

         case 'd' :
            AdcPowerDown();
            break;

         case 's' :
            AdcStart();
            break;

         case 'p' :
            AdcStop();
            break;

         case 'z' :
            Value = AdcZero();
            cprintf( "%06lX\n", Value);
            break;

         case 'f' :
            Value = AdcFullScale();
            cprintf( "%06lX\n", Value);
            break;

         case 'h' :
            AdcChop( YES);
            break;

         case '3' :
            AdcSinc3( YES);
            break;

         case '0' :
            AdcDataRate( 4);      // slowest data rate
            break;

         case '9' :
            AdcDataRate( 5000);   // fastest
            break;

         case '5' :
            AdcChop( NO);
            AdcSinc3( NO);
            AdcDataRate( ADC_CONVERSION_RATE);
            break;

         default :
            break;
      } // switch
      Uart0TxChar( ch);
   } // forever
   return( 0);
} // main


//-----------------------------------------------------------------------------
// Conversion Done
//-----------------------------------------------------------------------------

void AdcConversionDone( void)
// Conversion done callback
{
   ConversionDone = YES;
} // AdcConversionDone

