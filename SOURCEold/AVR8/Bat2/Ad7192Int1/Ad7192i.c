//*****************************************************************************
//
//    Ad7192i.c    A/D convertor AD7192
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "../../Inc/Ad7192i.h"
#include "../../Inc/Delays.h"
#include "../../Inc/System.h"
#include "../../Inc/Stdio.h"

#define MCLK 4920000L                            // internal clock [Hz]
#define RS0  3                                   // Register Select bit 0 shift

// Communications register :
#define CR_WEN              (1 << 7)             // Write enable
#define CR_RW               (1 << 6)             // Read/~Write
#define CR_SET_RS( r)       ((r) << 3)           // Register select
#define CR_CREAD            (1 << 2)             // Continuous read

#define CR_READ             CR_RW                // Read data from register
#define CR_WRITE            0x00                 // Write data to register

// Register address :
#define CR_COMMUNICATIONS_REGISTER  CR_SET_RS(0) // Communications register 8bit
#define CR_STATUS_REGISTER          CR_SET_RS(0) // Status register 8bit
#define CR_MODE_REGISTER            CR_SET_RS(1) // Mode register 24bit
#define CR_CONFIGURATION_REGISTER   CR_SET_RS(2) // Configuration register 24bit
#define CR_DATA_REGISTER            CR_SET_RS(3) // Data register 24/32bit
#define CR_ID_REGISTER              CR_SET_RS(4) // ID register 8bit
#define CR_GPOCON_REGISTER          CR_SET_RS(5) // GPOCON register 8bit
#define CR_OFFSET_REGISTER          CR_SET_RS(6) // Offset register 24bit
#define CR_FULL_SCALE_REGISTER      CR_SET_RS(7) // Offset register 24bit


// Status register :
#define SR_RDY              (1 << 7)             // ~Ready
#define SR_ERR              (1 << 6)             // ADC error
#define SR_NOREF            (1 << 5)             // No external reference
#define SR_PARITY           (1 << 4)             // Parity bit
#define SR_GET_CHD(r)       ((r) & 0x07)         // Channel for data 0


// Mode register :
#define MR_SET_MD( n)       (((dword)(n)) << 21) // set mode select bits
#define MR_GET_MD( r)       (((r) >> 21) & 0x07L)// get mode select bits
#define MR_DAT_STA          (1L << 20)           // status after data transmission
#define MR_SET_CLK( n)      (((dword)(n)) << 18) // set clock source
#define MR_GET_CLK( r)      (((r) >> 18) & 0x03L)// get clock source
#define MR_SINC3            (1L << 15)           // sinc3 filter/~sinc4 filter
#define MR_ENPAR            (1L << 13)           // parity bit enable
#define MR_CLK_DIV          (1L << 12)           // clock divide by 2
#define MR_SINGLE           (1L << 11)           // single cycle conversion
#define MR_REJ60            (1L << 10)           // 60Hz filter notch (for 50Hz only)
#define MR_SET_FS( n)       (((dword)(n)) <<  0) // set filter output data rate
#define MR_GET_FS( r)       ((r) & 0x3FFL)       // get filter output data rate

#define MR_EXTERNAL_CRYSTAL     MR_SET_CLK( 0)   // set external crystal clock
#define MR_EXTERNAL_CLOCK       MR_SET_CLK( 1)   // set external clock
#define MR_INTERNAL_CLOCK       MR_SET_CLK( 2)   // set internal clock
#define MR_INTERNAL_CLOCK_OUT   MR_SET_CLK( 3)   // set internal clock with output at MCLK2

#define MR_CONTINUOUS_MODE      MR_SET_MD( 0)    // continuous conversion
#define MR_SINGLE_MODE          MR_SET_MD( 1)    // single conversion mode
#define MR_IDLE                 MR_SET_MD( 2)    // idle mode - no conversions
#define MR_POWER_DOWN           MR_SET_MD( 3)    // power down mode
#define MR_INT_ZERO_CALIBRATION MR_SET_MD( 4)    // internal zero calibration
#define MR_INT_FULL_CALIBRATION MR_SET_MD( 5)    // internal full scale calibration
#define MR_SYS_ZERO_CALIBRATION MR_SET_MD( 6)    // system zero calibration
#define MR_SYS_FULL_CALIBRATION MR_SET_MD( 7)    // system full scale calibration

#define MR_SET_DATA_RATE( f)    MR_SET_FS( MCLK / (1024L * (f)))  // set conversion data rate [Hz]

#define MR_MIN_DATA_RATE        MR_SET_FS( 1023) // FS for 4.7 Hz
#define MR_MAX_DATA_RATE        MR_SET_FS( 1)    // FS for 4.8 kHz
#define MR_CAL_DATA_RATE        MR_SET_FS( 1008) // FS for 4.7 Hz (divisible by 16)


// Configuration register :
#define CR_CHOP          (1L << 23)              // Chop enable bit
#define CR_REFSEL        (1L << 20)              // 0 - REFIN1, 1 - REFIN2
#define CR_SET_CH( n)    (((dword)(n)) << 8)     // set channel select bits
#define CR_GET_CH( r)    (((r) >> 8) & 0xFFL)    // get channel
#define CR_BURN          (1L <<  7)              // set burnout current
#define CR_REFDET        (1L <<  6)              // enable reference detection
#define CR_BUF           (1L <<  4)              // enable buffer amplifier
#define CR_UB            (1L <<  3)              // Unipolar /~Bipolar
#define CR_SET_G( n)     (((dword)(n)) << 0)     // set gain
#define CR_GET_G( r)     (((r) & 0x07L)          // get gain

#define CR_GAIN_1        CR_SET_G(0)             // Set gain 1
#define CR_GAIN_8        CR_SET_G(3)             // Set gain 8
#define CR_GAIN_16       CR_SET_G(4)             // Set gain 16
#define CR_GAIN_32       CR_SET_G(5)             // Set gain 32
#define CR_GAIN_64       CR_SET_G(6)             // Set gain 64
#define CR_GAIN_128      CR_SET_G(7)             // Set gain 128

#define CR_CH_12         0x01                    // Select differential AIN1+2
#define CR_CH_34         0x02                    // Select differential AIN3+4
#define CR_CH_TEMP       0x04                    // Select temperature channel
#define CR_CH_22         0x08                    // Select differential AIN2+2
#define CR_CH_1          0x10                    // Select single AIN1+AINCOM
#define CR_CH_2          0x20                    // Select single AIN2+AINCOM
#define CR_CH_3          0x40                    // Select single AIN3+AINCOM
#define CR_CH_4          0x80                    // Select single AIN4+AINCOM

// GPOCON register :
#define GR_BPDSW         (1 << 6)                // bridge power down switch 1-on / 0-off
#define GR_GP32EN        (1 << 5)                // enable P3+P2
#define GR_GP10EN        (1 << 4)                // enable P1+P0
#define GR_P3DAT         (1 << 3)                // digital output P3
#define GR_P2DAT         (1 << 2)                // digital output P2
#define GR_P1DAT         (1 << 1)                // digital output P1
#define GR_P0DAT         (1 << 0)                // digital output P0


#define ADC_RESET_PULSES 40                      // clock count for reset

//-----------------------------------------------------------------------------
// Implementation constants
//-----------------------------------------------------------------------------

// mode register conversion control :
#define MR_DEFAULT_MODE         (MR_CONTINUOUS_MODE | MR_INTERNAL_CLOCK)
#define MR_IDLE_MODE            (MR_IDLE)
#define MR_POWER_DOWN_MODE      (MR_POWER_DOWN)
// mode register calibration :
#define MR_CALIBRATE_ZERO       (MR_INT_ZERO_CALIBRATION | MR_INTERNAL_CLOCK | MR_CAL_DATA_RATE)
#define MR_CALIBRATE_FULL_SCALE (MR_INT_FULL_CALIBRATION | MR_INTERNAL_CLOCK | MR_CAL_DATA_RATE |\
                                 MR_CLK_DIV)

// configuration register defaults :
#define CR_DEFAULT_CONFIG  (CR_SET_CH( CR_CH_12) | \
                            CR_BUF   | \
                            CR_GAIN_128)

// GPOCON register control :
#define GR_DEFAULT_GPOCON (GR_BPDSW)          // bridge on
#define GR_IDLE_GPOCON     0x00               // bridge off

// Parametric configuration values :
static word   FsValue;
static TYesNo Chop;
static TYesNo Sinc3;

//-----------------------------------------------------------------------------
// External interrupt
//-----------------------------------------------------------------------------

#include "../../Inc/Eint.h"

//#define TRACE_INTERRUPT        // make interrupt visible

#define ADC_EINT_SENSE   EINT_SENSE_FALLING_EDGE

#define AdcClearInt()    EintClearFlag( ADC_EINT)
#define AdcEnableInt()   EintEnable( ADC_EINT)
#define AdcDisableInt()  EintDisable( ADC_EINT)

#if ADC_EINT == 0
   #define ADC_INT_vect  INT0_vect
#elif ADC_EINT == 1
   #define ADC_INT_vect  INT1_vect
#elif ADC_EINT == 2
   #define ADC_INT_vect  INT2_vect
#else
   #error "Missing ADC_EINT definition"
#endif

// internal variables :
static volatile dword  AdcValue;
static volatile TYesNo AdcReadyFlag;

//-----------------------------------------------------------------------------
// Local functions :
//-----------------------------------------------------------------------------

static byte ReadByte( void);
// Read data byte from ADC

static void WriteByte( byte Value);
// Write data byte to ADC

static dword ReadWord( void);
// Read 24bit data from ADC

static void WriteWord( dword Value);
// Write 24bit to to ADC
 
//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void AdcInit( void)
// Initialisation
{
   AdcPortInit();
   // Defaults :
   AdcSetDIN();
   AdcSetSCLK();   
   AdcClrCS();                          // select
   AdcReset();                          // reset communication
   AdcIdle();                           // stop conversion
   // prepare default values :
   FsValue = MR_SET_DATA_RATE( ADC_CONVERSION_RATE);
   Chop    = NO;
   Sinc3   = NO;
   // prepare interrupt :
   EintDisable( ADC_EINT);              // disable ADC external interrupt
   EintSense( ADC_EINT, ADC_EINT_SENSE);// set ADC interrupt sense
} // AdcInit

//-----------------------------------------------------------------------------
// Reset
//-----------------------------------------------------------------------------

void AdcReset( void)
// Reset communication
{
byte i;

   AdcDisableInt();
   AdcSetDIN();
   for( i = 0; i < ADC_RESET_PULSES + 2; i++){
      AdcClrSCLK();
      _NOP();
      AdcSetSCLK();
   }
   SysDelay( 1);                       // wait at least 500us
} // AdcReset

//-----------------------------------------------------------------------------
// Data rate
//-----------------------------------------------------------------------------

void AdcDataRate( word Hertz)
// Set conversion data rate
{
   FsValue = (word)MR_SET_DATA_RATE( Hertz);
   // minimum divisor :
   if( FsValue < MR_MAX_DATA_RATE){
      FsValue = MR_MAX_DATA_RATE;
   }
   // maximum divisor :
   if( FsValue > MR_MIN_DATA_RATE){
      FsValue = MR_MIN_DATA_RATE;
   }
} // AdcDataRate

//-----------------------------------------------------------------------------
// Enable SINC3
//-----------------------------------------------------------------------------

void AdcSinc3( TYesNo Enable)
// <Enable> SINC3 filtering
{
   Sinc3 = Enable;
} // AdcSinc3

//-----------------------------------------------------------------------------
// Enable CHOP
//-----------------------------------------------------------------------------

void AdcChop( TYesNo Enable)
// <Enable> CHOP filtering
{
   Chop = Enable;
} // AdcChop

//-----------------------------------------------------------------------------
// Idle
//-----------------------------------------------------------------------------

void AdcIdle( void)
// Set idle mode
{
   AdcDisableInt();
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);          // comm register
   WriteWord( MR_IDLE_MODE);                         // mode value
   // Bridge power off :
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);        // comm register
   WriteByte( GR_IDLE_GPOCON);                       // value
} // AdcIdle

//-----------------------------------------------------------------------------
// Power down
//-----------------------------------------------------------------------------

void AdcPowerDown( void)
// Set power down mode
{
   AdcDisableInt();
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);          // comm register
   WriteWord( MR_POWER_DOWN_MODE);                   // mode value
   // Bridge power off :
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);        // comm register
   WriteByte( GR_IDLE_GPOCON);                       // value
} // AdcPoweDown

//-----------------------------------------------------------------------------
// Start
//-----------------------------------------------------------------------------

void AdcStart( void)
// Start continuous conversion
{
   AdcIdle();                                        // stop conversion
   // Configuration register :  
   WriteByte( CR_CONFIGURATION_REGISTER | CR_WRITE); // comm register
   if( Chop){
      WriteWord( CR_DEFAULT_CONFIG | CR_CHOP);       //  value
   } else {
      WriteWord( CR_DEFAULT_CONFIG);                 //  value
   }
   // GPOCON register :  
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);        // comm register
   WriteByte( GR_DEFAULT_GPOCON);                    // bridge on
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);          // comm register
   if( Sinc3){
      WriteWord( FsValue | MR_DEFAULT_MODE | MR_SINC3);    // mode value
   } else {
      WriteWord( FsValue | MR_DEFAULT_MODE);               // mode value
   }
   // Continuous read :
   WriteByte( CR_DATA_REGISTER | CR_READ | CR_CREAD);      // comm register
   AdcClrDIN();                                            // DIN = H does reset
   // Enable interrupt :
   AdcReadyFlag = NO;
   AdcClearInt();                                    // clear old interupt flag
   AdcEnableInt();                                   // enable external interrupt
} // AdcStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void AdcStop( void)
// Stop continous conversion
{
   AdcDisableInt();
   while( !AdcGetRDY());                            // wait for conversion
   WriteByte( CR_DATA_REGISTER | CR_READ);          // stop continuous read
   AdcIdle();                                       // stop conversion
} // AdcStop

//-----------------------------------------------------------------------------
// Zero calibration
//-----------------------------------------------------------------------------

dword AdcZero( void)
// Process zero calibration, returns offset register
{
dword Value;

   AdcIdle();
   // Configuration register :  
   WriteByte( CR_CONFIGURATION_REGISTER | CR_WRITE); // comm register
   if( Chop){
      WriteWord( CR_DEFAULT_CONFIG | CR_CHOP);       //  value
   } else {
      WriteWord( CR_DEFAULT_CONFIG);                 //  value
   }
   // GPOCON register :  
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);        // comm register
   WriteByte( GR_DEFAULT_GPOCON);                    //  value
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);          // comm register
   if( Sinc3){
      WriteWord( MR_CALIBRATE_ZERO | MR_SINC3);      // mode value
   } else {
      WriteWord( MR_CALIBRATE_ZERO);                 // mode value
   }
   // wait for calibration :
   while( !AdcGetRDY());
   // Read offset register :
   WriteByte( CR_OFFSET_REGISTER | CR_READ); // comm register
   Value = ReadWord();
   return( Value);
} // AdcZero

//-----------------------------------------------------------------------------
// Full scale calibration
//-----------------------------------------------------------------------------

dword AdcFullScale( void)
// Process full-scale calibration, returns full scale register
{
dword Value;

   AdcIdle();
   // Configuration register :  
   WriteByte( CR_CONFIGURATION_REGISTER | CR_WRITE); // comm register
   if( Chop){
      WriteWord( CR_DEFAULT_CONFIG | CR_CHOP);       //  value
   } else {
      WriteWord( CR_DEFAULT_CONFIG);                 //  value
   }
   // GPOCON register :  
   WriteByte( CR_GPOCON_REGISTER | CR_WRITE);        // comm register
   WriteByte( GR_DEFAULT_GPOCON);                    //  value
   // Mode register :  
   WriteByte( CR_MODE_REGISTER | CR_WRITE);          // comm register
   if( Sinc3){
      WriteWord( MR_CALIBRATE_FULL_SCALE | MR_SINC3);// mode value
   } else {
      WriteWord( MR_CALIBRATE_FULL_SCALE);           // mode value
   }
   // wait for calibration :
   while( !AdcGetRDY());
   // Read full scale register :
   WriteByte( CR_FULL_SCALE_REGISTER | CR_READ);     // comm register
   Value = ReadWord();
   return( Value);
} // AdcFullScale

//-----------------------------------------------------------------------------
// Set zero
//-----------------------------------------------------------------------------

void AdcSetZero( dword Value)
// Set offset register to <Value>
{
   AdcIdle();                                    // stop conversion
   SysDelay( 1);                                 // some wait
   // Write offset register :
   WriteByte( CR_OFFSET_REGISTER | CR_WRITE);    // comm register
   WriteWord( Value);
} // AdcSetZero

//-----------------------------------------------------------------------------
// Set full scale
//-----------------------------------------------------------------------------

void AdcSetFullScale( dword Value)
// Set full scale register to <Value>
{
   AdcIdle();                                    // stop conversion
   SysDelay( 1);                                 // some wait
   // Write offset register :
   WriteByte( CR_FULL_SCALE_REGISTER | CR_WRITE);// comm register
   WriteWord( Value);
} // AdcSetFullScale

//-----------------------------------------------------------------------------
// Data ready
//-----------------------------------------------------------------------------

void AdcCheck( void)
// check config data
{
dword Value;
byte  BValue;

   WriteByte( CR_STATUS_REGISTER | CR_READ);        // comm register
   BValue = ReadByte();
   cprintf( "STA : %02X (%02X)\n", BValue, 0);

   WriteByte( CR_CONFIGURATION_REGISTER | CR_READ); // comm register
   Value = ReadWord();
   cprintf( "CFG : %06lX (%06lX)\n", Value, CR_DEFAULT_CONFIG);

   WriteByte( CR_MODE_REGISTER | CR_READ); // comm register
   Value = ReadWord();
   cprintf( "MOD : %06lX (%06lX)\n", Value, MR_DEFAULT_MODE);

   WriteByte( CR_GPOCON_REGISTER | CR_READ); // comm register
   BValue = ReadByte();
   cprintf( "GPO : %02X (%02X)\n", BValue, GR_DEFAULT_GPOCON);
} // AdcCheck

//-----------------------------------------------------------------------------
// Data ready
//-----------------------------------------------------------------------------

TYesNo AdcIsReady( void)
// Conversin terminated - data ready
{

   return( AdcReadyFlag);
} // AdcIsReady

//-----------------------------------------------------------------------------
// Wait for data ready
//-----------------------------------------------------------------------------

TYesNo AdcWaitReady( word Timeout)
// Wait for data ready
{
word i;

   i = 1;
   Timeout++;
   do{
      do {
         if( AdcReadyFlag){
            return( YES);
         }
      } while( --i);
      i = WDelayCount( 1000, 4);
   } while( --Timeout);
   return( NO);
} // AdcWaitReady

//-----------------------------------------------------------------------------
// Read value
//-----------------------------------------------------------------------------

dword AdcReadValue( void)
// Read conversion data
{
dword Value;

   AdcDisableInt();
   Value        = AdcValue;
   AdcReadyFlag = NO;
   AdcEnableInt();
   return( Value);
} // AdcReadValue

//-----------------------------------------------------------------------------
// Read byte
//-----------------------------------------------------------------------------

static byte ReadByte( void)
// Read data byte from ADC
{
byte Value;
byte i;

   Value  = 0;
   i = 8;
   do {
      Value <<= 1;
      AdcClrSCLK();
      _NOP();
      if( AdcGetDOUT()){
         Value |= 1;
      }
      AdcSetSCLK();   
   } while( --i);
   return( Value);
} // ReadByte

//-----------------------------------------------------------------------------
// Write byte
//-----------------------------------------------------------------------------

static void WriteByte( byte Value)
// Write data byte to ADC
{
byte i;

   i = 8;
   do {
      AdcClrSCLK();
      AdcClrDIN();
      if( Value & 0x80){
         AdcSetDIN();
      }
      _NOP();
      AdcSetSCLK();
      Value <<= 1;
   } while( --i);
   AdcSetDIN();
} // WriteByte

//-----------------------------------------------------------------------------
// Read word
//-----------------------------------------------------------------------------

static dword ReadWord( void)
// Read 24bit data from ADC
{
dword Value;
byte  i;

   Value  = 0;
   i = 24;
   do {
      Value <<= 1;
      AdcClrSCLK();
      _NOP();
      if( AdcGetDOUT()){
         Value |= 1;
      }
      AdcSetSCLK();   
   } while( --i);
   return( Value);
} // ReadWord

//-----------------------------------------------------------------------------
// Write word
//-----------------------------------------------------------------------------

static void WriteWord( dword Value)
// Write 24bit to to ADC
{
byte i;

   i = 24;
   Value <<= 8;                        // bit 23 to MSB
   do {
      AdcClrSCLK();
      AdcClrDIN();
      if( Value & 0x80000000L){
         AdcSetDIN();
      }
      _NOP();
      AdcSetSCLK();
      Value <<= 1;
   } while( --i);
   AdcSetDIN();
} // WriteWord

//-----------------------------------------------------------------------------
// Interrupt handler
//-----------------------------------------------------------------------------

ISR( ADC_INT_vect)
{
#ifdef TRACE_INTERRUPT
   static TYesNo Toggle = YES;
   if( Toggle){
      StatusLedOn();
   } else {
      StatusLedOff();
   }
   Toggle = !Toggle;
#endif // TRACE_INTERRUPT

   AdcValue     = ReadWord();
   AdcReadyFlag = YES;
   AdcClearInt();                      // clear flag set by data read edges
} // ADC_INT_vect
