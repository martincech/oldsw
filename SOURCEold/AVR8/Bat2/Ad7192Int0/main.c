//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "../Inc/System.h"
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"
#include "../Inc/Ad7192i.h"

#define CONVERSION_COUNT   10
#define CONVERSION_TIMEOUT 500        // timeout [ms]

dword Values[ CONVERSION_COUNT];

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
dword Value;
byte  ch;
int   i;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();
   AdcInit();
   StdioInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   Uart0TxChar( 'A');
   Uart0TxChar( 'B');
   Uart0TxChar( 'C');
   Uart0TxChar( '\r');
   Uart0TxChar( '\n');
   AdcCheck();
   EnableInts();
   // main loop
   forever {
      if( !Uart0RxWait( 0)){
         continue;
      }
      if( !Uart0RxChar( &ch)){
         continue;
      }
      switch( ch){
         case 'm' :
            for( i = 0; i < CONVERSION_COUNT; i++){
               if( !AdcWaitReady( CONVERSION_TIMEOUT)){
                  cprintf( "T.O.\n");
                  continue;
               }
               Values[ i] = AdcReadValue();
            }
            for( i = 0; i < CONVERSION_COUNT; i++){
               cprintf( "%06lX\n", Values[ i]);
            }
            break;

         case 'c' :
            AdcCheck();
            break;

         case 'i' :
            AdcInit();
            break;

         case 'x' :
            AdcIdle();
            break;

         case 'p' :
            AdcPowerDown();
            break;

         case 's' :
            AdcStart();
            break;

         case 'z' :
            Value = AdcZero();
            cprintf( "%06lX\n", Value);
            break;

         case 'f' :
            Value = AdcFullScale();
            cprintf( "%06lX\n", Value);
            break;

         case 'h' :
            AdcChop( YES);
            break;

         case '3' :
            AdcSinc3( YES);
            break;

         case '0' :
            AdcDataRate( 4);      // slowest data rate
            break;

         case '9' :
            AdcDataRate( 5000);   // fastest
            break;

         case 'd' :
            AdcChop( NO);
            AdcSinc3( NO);
            AdcDataRate( ADC_CONVERSION_RATE);
            break;

         default :
            break;
      } // switch
      Uart0TxChar( ch);
   } // forever
   return( 0);
} // main
