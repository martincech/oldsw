//*****************************************************************************
//
//    Hardware.h  - Bat2 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED   7

#define StatusLedInit()  DDRD  |=  (1 << StatusLED)

#define StatusLedOn()    PORTD |=  (1 << StatusLED) 
#define StatusLedOff()   PORTD &= ~(1 << StatusLED)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           57600              // baud rate
#define UART0_DOUBLE         1                  // double baudrate

/*
//#define UART0_REPLY_TIMEOUT          1000       // reply timeout [ms] 
#define UART0_REPLY_TIMEOUT          0          // reply timeout [ms] 
#define UART0_INTERCHARACTER_TIMEOUT 10         // intercharacter timeout [ms]
*/
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

// RS485 direction :
#define Uart0TxEnable()      StatusLedOn()
#define Uart0TxDisable()     StatusLedOff()

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   2

#define CalButtonInit()     DDRD  &= ~(1 << CalButton);\
                            PORTD |=  (1 << CalButton)         // enable pullup

#define CalButtonPressed()  (!(PIND & (1 << CalButton)))       // active L


//------------------------------------------------------------------------------
// AD7192
//------------------------------------------------------------------------------

#define AdcCS       6
#define AdcSCLK     5
#define AdcDIN      4
#define AdcDOUT_RDY 3

#define ADC_EINT    1                  // DOUT_RDY connected on INT1

#define AdcPortInit()    DDRD  |=  (1 << AdcDIN) | (1 << AdcSCLK) | (1 << AdcCS);\
                         DDRD  &= ~(1 << AdcDOUT_RDY)

#define AdcSetCS()       PORTD |=  (1 << AdcCS)
#define AdcClrCS()       PORTD &= ~(1 << AdcCS)
#define AdcSetSCLK()     PORTD |=  (1 << AdcSCLK)
#define AdcClrSCLK()     PORTD &= ~(1 << AdcSCLK)
#define AdcSetDIN()      PORTD |=  (1 << AdcDIN)
#define AdcClrDIN()      PORTD &= ~(1 << AdcDIN)


#define AdcGetDOUT()       (PIND & (1 << AdcDOUT_RDY))
#define AdcGetRDY()      (!(PIND & (1 << AdcDOUT_RDY)))


// Parameters :
#define ADC_CONVERSION_RATE   600      // default conversion rate [Hz]

//-----------------------------------------------------------------------------

#endif
