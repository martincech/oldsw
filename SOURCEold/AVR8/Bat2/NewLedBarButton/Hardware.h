//*****************************************************************************
//
//    Hardware.h  - Bat2 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define STATUS_LEDR   7   // PD7 - red
#define STATUS_LEDG   1   // PC1 - green
#define STATUS_LEDB   0   // PC0 - blue


#define StatusLedInit()   StatusLedsOff();            \
                          DDRD |= (1 << STATUS_LEDR); \
                          DDRC |= (1 << STATUS_LEDG); \
                          DDRC |= (1 << STATUS_LEDB)

#define StatusLedsOff()   StatusLedROff(); StatusLedGOff(); StatusLedBOff();

#define StatusLedROn()    PORTD &= ~(1 << STATUS_LEDR) 
#define StatusLedROff()   PORTD |=  (1 << STATUS_LEDR)

#define StatusLedGOn()    PORTC &= ~(1 << STATUS_LEDG) 
#define StatusLedGOff()   PORTC |=  (1 << STATUS_LEDG)

#define StatusLedBOn()    PORTC &= ~(1 << STATUS_LEDB) 
#define StatusLedBOff()   PORTC |=  (1 << STATUS_LEDB)

//-----------------------------------------------------------------------------
// Bargraph
//-----------------------------------------------------------------------------

#define BAR_LED0    1    // PA1
#define BAR_LED1    2
#define BAR_LED2    3
#define BAR_LED3    4
#define BAR_LED4    5    // PA5

#define BarInit()   BarOff(); \
                    DDRA  |= (1 << BAR_LED0) | (1 << BAR_LED1) |\
                             (1 << BAR_LED2) | (1 << BAR_LED3) | (1 << BAR_LED4)

#define BarOff()    PORTA |= (1 << BAR_LED0) | (1 << BAR_LED1) |\
                             (1 << BAR_LED2) | (1 << BAR_LED3) | (1 << BAR_LED4)

#define Bar0On()    PORTA &= ~(1 << BAR_LED0)
#define Bar0Off()   PORTA |=  (1 << BAR_LED0)
#define Bar1On()    PORTA &= ~(1 << BAR_LED1)
#define Bar1Off()   PORTA |=  (1 << BAR_LED1)
#define Bar2On()    PORTA &= ~(1 << BAR_LED2)
#define Bar2Off()   PORTA |=  (1 << BAR_LED2)
#define Bar3On()    PORTA &= ~(1 << BAR_LED3)
#define Bar3Off()   PORTA |=  (1 << BAR_LED3)
#define Bar4On()    PORTA &= ~(1 << BAR_LED4)
#define Bar4Off()   PORTA |=  (1 << BAR_LED4)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

//#define UART0_BAUD           115200             // baud rate
//#define UART0_BAUD           57600              // baud rate
#define UART0_BAUD           38400              // baud rate
#define UART0_DOUBLE         1                  // double baudrate

#define UART0_INTERCHARACTER_TIMEOUT 10         // intercharacter timeout [ms]

// RS485 direction :
#define Uart0TxEnable()      StatusLedOn()
#define Uart0TxDisable()     StatusLedOff()

//-----------------------------------------------------------------------------
// Buttons
//-----------------------------------------------------------------------------

#define BUTTON_RED    7     // PB7 - red button
#define BUTTON_BLACK  2     // PD2 - black button

#define ButtonInit()        DDRB  &= ~(1 << BUTTON_RED);\
                            PORTB |=  (1 << BUTTON_RED);         /* enable pullup */ \
                            DDRD  &= ~(1 << BUTTON_BLACK);\
                            PORTD |=  (1 << BUTTON_BLACK);       /* enable pullup */

#define ButtonRedPressed()    (!(PINB & (1 << BUTTON_RED)))      // active L
#define ButtonBlackPressed()  (!(PIND & (1 << BUTTON_BLACK)))    // active L

//------------------------------------------------------------------------------
// AD7192
//------------------------------------------------------------------------------

#define AdcCS       6
#define AdcSCLK     5
#define AdcDIN      4
#define AdcDOUT_RDY 3

#define ADC_EINT    1                  // DOUT_RDY connected on INT1

#define AdcPortInit()    DDRD  |=  (1 << AdcDIN) | (1 << AdcSCLK) | (1 << AdcCS);\
                         DDRD  &= ~(1 << AdcDOUT_RDY)

#define AdcSetCS()       PORTD |=  (1 << AdcCS)
#define AdcClrCS()       PORTD &= ~(1 << AdcCS)
#define AdcSetSCLK()     PORTD |=  (1 << AdcSCLK)
#define AdcClrSCLK()     PORTD &= ~(1 << AdcSCLK)
#define AdcSetDIN()      PORTD |=  (1 << AdcDIN)
#define AdcClrDIN()      PORTD &= ~(1 << AdcDIN)


#define AdcGetDOUT()       (PIND & (1 << AdcDOUT_RDY))
#define AdcGetRDY()      (!(PIND & (1 << AdcDOUT_RDY)))

// Parameters :
//#define ADC_CONVERSION_RATE   600      // default conversion rate [Hz]
#define ADC_CONVERSION_RATE   10      // default conversion rate [Hz]

//-----------------------------------------------------------------------------

#endif
