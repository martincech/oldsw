//*****************************************************************************
//
//    AdcTesterDef.h  Bat2 ADC tester definitions
//    Version 1.0     (c) VymOs
//
//*****************************************************************************

#ifndef __AdcTesterDef_H__
#define __AdcTesterDef_H__

#define ADC_PACKET_START 0xAA
#define ADC_PACKET_STOP  0xBB

#define ADC_CONVERSION_COUNT   256        // maximum conversion count
#define ADC_VALUE_SIZE         3          // single conversion size
#define ADC_FRAME_SIZE         2          // additional frame characters

#define AdcSamplesSize( Samples) (Samples * ADC_VALUE_SIZE)
#define AdcPacketSize( Samples)   (AdcSamplesSize( Samples) + ADC_FRAME_SIZE)

typedef enum {
   CMD_INITIAL_DELAY  = 'd',     // + byte
   CMD_SAMPLES_COUNT  = 'n',     // + byte
   CMD_SPACE_DELAY    = 's',     // + byte
   CMD_SET_CHOP       = 'h',
   CMD_SET_SINC3      = '3',
   CMD_SET_RATE       = 'r',     // + word
   CMD_SET_DEFAULTS   = 'e',
   CMD_MEASURE        = 'm',
   CMD_MEASURE_FAST   = 'M',
   CMD_CALIBRATE_ZERO = 'o',
   CMD_CALIBRATE_FULL = 'f',
   CMD_SET_ZERO       = 'O',     // + word
   CMD_SET_FULL       = 'F',     // + word
   CMD_STOP           = 'x',
} TAdcCommands;

//typedef enum {
//} TAdcReply;

#endif
