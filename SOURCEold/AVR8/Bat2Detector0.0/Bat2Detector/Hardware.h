//*****************************************************************************
//
//    Hardware.h  - Bat2 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "cpu.h"
#include "uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           38400       // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM0

//------------------------------------------------------------------------------
// SPI
//------------------------------------------------------------------------------

#define SPI_CLOCK			(FXTAL / 16)
#define SPI_CPOL			0
#define SPI_CPHA			1
#define SPI_CLOCK_DIVISOR	0
#define SPI_CLOCK_X2		1


#define SpiSSPin	4	
#define SpiSckPin	7	
#define SpiMosiPin	5	
#define SpiMisoPin	6	

/*	MISO - IN
	others - OUT
*/
#define SpiPortInit()		DDRB &= ~(1 << SpiMisoPin);\
							DDRB |= (1 << SpiSSPin) | (1 << SpiSckPin) | (1 << SpiMosiPin)

#define SpiSelect()    PORTB &= ~(1 << SpiSSPin)
#define SpiRelease()  PORTB |=  (1 << SpiSSPin)


#define Ps081SpiEnableInit()  DDRB |= (1 << 3)
#define Ps081SpiEnable()      PORTB |=  (1 << 3)
#define Ps081SpiDisable()     PORTB &= ~(1 << 3)

#define CalibrationButton  0

#define CalibrationButtonInit()   DDRB &= ~(1 << CalibrationButton); \
                                  PORTB |= (1 << CalibrationButton)
#define CalibrationButtonPushed() (!(PINB &  (1 << CalibrationButton)))

#define CalibrationLed  1

#define CalibrationLedInit()   DDRB |= (1 << CalibrationLed)
#define CalibrationLedOn()   PORTB |= (1 << CalibrationLed)
#define CalibrationLedOff()   PORTB &= ~(1 << CalibrationLed)
#define CalibrationLedToggle()   PORTB ^= (1 << CalibrationLed)

// Set falling edge generates interrupt request
#define PinIntSetFallingEdge(IntPin)   EICRA &= ~(3 << (2 * IntPin));\
                                    EICRA |=  (2 << (2 * IntPin))
									
#define PinIntSetRisingEdge(IntPin)   EICRA |=  (3 << (2 * IntPin))
									
#define PinIntEnable(IntPin)     EIMSK |=  (1 << IntPin);
#define PinIntDisable(IntPin)     EIMSK &= ~(1 << IntPin);

#define Ps081DataReadyIntPin        2   
#define Ps081DataReadyIntVector     INT2_vect
#define Ps081DataReadyIntInit()     PinIntSetRisingEdge(Ps081DataReadyIntPin)  
#define Ps081DataReadyIntEnable()   PinIntEnable(Ps081DataReadyIntPin)    
#define Ps081DataReadyIntDisable()  PinIntDisable(Ps081DataReadyIntPin)    

#define PICOSTRAIN

#endif
