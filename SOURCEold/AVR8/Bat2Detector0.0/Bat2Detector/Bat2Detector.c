//*****************************************************************************
//
//   Bat2Detector.c     Bat2Detector
//   Version 1.0 (c) Veit Electronics
//
//*****************************************************************************

#include "Cpu.h"
#include "../Library/Bat2Detector/Dp.h"

int main(void)
{
   DpInit();
   
   // to be able to read ADC samples...
   EnableInts();
   
   while(1) {
      DpExecute();
   }      
}