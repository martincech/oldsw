Bat2Detector
------------

Jedn� se o n�st�el komunikace mezi Detektorem a termin�lem. Sou��st� projektu je i modul AD p�evodn�ku. Ten �te z Picostrainu nov� vzorky a p�ed�v� je d�le ke zpracov�n�. Vzorky jsou �teny v p�eru�en�.


Moduly:

Dp.h - Detector protocol (d�le DP). P�ij�m� po�adavky od termin�lu a realizuje je. V�t�ina podstatn�ho se d�je v DpExecute() (vol�no v main()). Funkce DpSendAsynch() slou�� k odesl�n� dat z detektoru bez p�edchoz�ho po�adavku od termin�lu.

DpDef.h - definice struktur p�en�en�ch v DP.

DpCom.h - komunika�n� vrstva DP (Uart).

Detector.h - �st�edn� modul.
   1) DP sem p�ed�v� p�ijat� parametry (ulo�eny v DetectorParameters struktu�e) a po�adavky na spou�t�n� a ukon�en� m��en� (DetectorStart(), DetectorStop()).
   2) ADC sem p�ed�v� nam��en� vzorky. Podle modu, ve kter�m se detektor nach�z�, jsou vzorky bu� ukl�d�ny (a po napln�n� bufferu odesl�ny; v p��pad� kontinu�ln�ho m��en�, MODE_ALL) nebo pos�l�ny akcepta�n�mu modulu (v p��pad� provozn�ho modu, MODE_DEFAULT).

DetectorDef.h - definice struktur detektoru.

Adc.h - AD p�evodn�k

PS081.h - Picostrain