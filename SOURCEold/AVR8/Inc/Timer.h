//*****************************************************************************
//
//    Timer.h      Timer calculations
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Timer_H__
   #define __Timer_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

// Timer (word) operations :
#define TimerAfter(    Actual, Requested)   ((short)(Requested) - (short)(Actual)    <  0)
#define TimerBefore(   Actual, Requested)   ((short)(Actual)    - (short)(Requested) <  0)
#define TimerAfterEq(  Actual, Requested)   ((short)(Actual)    - (short)(Requested) >= 0)
#define TimerBeforeEq( Actual, Requested)   ((short)(Requested) - (short)(Actual)    >= 0)


// Time (dword) operations :
#define TimeAfter(     Actual, Requested)   ((long)(Requested)  - (long)(Actual)     <  0)
#define TimeBefore(    Actual, Requested)   ((long)(Actual)     - (long)(Requested)  <  0)
#define TimeAfterEq(   Actual, Requested)   ((long)(Actual)     - (long)(Requested)  >= 0)
#define TimeBeforeEq(  Actual, Requested)   ((long)(Requested)  - (long)(Actual)     >= 0)

#endif
