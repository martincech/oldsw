//*****************************************************************************
//
//    conio.h - simple display services
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __conio_H__
   #define __conio_H__
   
   #ifndef __Hardware_H__
      #include "Hardware.h"
   #endif

#define cputs( s)   mputs( PSTR( s))
#define cputch( c)  putchar( c)

#ifndef __PUTCHAR__
   #define __PUTCHAR__
   
int putchar( int c);
// standardni zapis znaku

#endif
   
void mputs( PGM_P String);
// Put string

void cprinthex( word Value, byte Decimals);
// Print hexadecimal <Value>, place dot on <Decimals> position

void cprintdec( word Value, byte Decimals);
// Print decimal <Value>, place dot on <Decimals> position

void cint32( dword Value);
// Print decimal <Value> (up to 9999 9999)

void cprinttime( dword Value);
// Print time as hh:mm:ss
   
#endif
