//******************************************************************************
//
//    Debug.h      Debugger utility
//    Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Debug_H__
   #define __Debug_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef __DEBUG__
   #ifndef DEBUG_COM1
      #define __COM0__
   #else
      #define __COM1__
   #endif
   #include "../inc/Com.h"

#ifdef __TINY_DEBUG__
   #include "../inc/conio.h"           // simple console
   
   #define TRACE_INIT()     ComInit();
   #define TRACE( msg)      cputs( msg); cputch( '\n');
   #define TRACED( msg, n)  cputs( msg); cputch(' '); cprintdec( (n), 0); cputch( '\n');
   #define TRACEX( msg, n)  cputs( msg); cputch(' '); cprinthex( (n), 0); cputch( '\n');
   #define TRACEXX( msg, n) cputs( msg); cputch(' '); cprinthex( (n) >> 16, 0); \
                            cprinthex( (n) & 0xFFFF, 0); cputch( '\n');
#else // use printf
   #include "../Inc/Stdio.h"           // cprintf

   #define TRACE_INIT()     ComInit();StdioInit();
   #define TRACE( msg)      cprintf( "%s\n",     msg);
   #define TRACED( msg, n)  cprintf( "%s %d\n",  msg, n);
   #define TRACEX( msg, n)  cprintf( "%s %X\n",  msg, n);
   #define TRACEXX( msg, n) cprintf( "%s %lX\n", msg, n);
#endif

#else // nodebug
   #define TRACE_INIT()
   #define TRACE( msg) 
   #define TRACED( msg, n)
   #define TRACEX( msg, n)
   #define TRACEXX( msg, n)
#endif

#endif
