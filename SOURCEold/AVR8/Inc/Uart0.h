//*****************************************************************************
//
//    Uart0.h - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Uart0_H__
   #define __Uart0_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void Uart0Init( void);
// Communication initialisation

TYesNo Uart0TxBusy( void);
// Returns YES if transmitter is busy

void Uart0TxChar( byte Char);
// Transmit <Char> byte

void Uart0TxDrain( void);
// Waits for end of transmission

TYesNo Uart0RxChar( byte *Char);
// Returns YES and received byte on <Char>,
// or NO on timeout

TYesNo Uart0RxWait( word Timeout);
// Waits for receive up to <Timeout> miliseconds

void Uart0FlushChars( void);
// Skips all Rx chars up to intercharacter timeout

#endif
