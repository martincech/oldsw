//*****************************************************************************
//
//    Iadc.h -  internal A/D converter
//    Version 1.0 (c) Vymos
//
//*****************************************************************************

#ifndef __Iadc_H__
   #define __Iadc_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void IadcInit( void);
// Inicializace prevodniku

void IadcStop( void);
// Zastaveni prevodniku

word IadcMeasure( byte Channel);
// Vrati zmerenou hodnotu na vstupu <Channel>

#endif
