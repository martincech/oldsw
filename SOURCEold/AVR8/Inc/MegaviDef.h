//*****************************************************************************
//
//   MegaviDef.h  MEGAVI protocol definitions
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MegaviDef_H__
   #define __MegaviDef_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

// <Leader> <Id> <Data> <Crc> <Trailer>

#define MEGAVI_LEADER_CHAR  0x55
#define MEGAVI_TRAILER_CHAR 0xAA

#define MEGAVI_LEADER_OFFSET  0
#define MEGAVI_ID_OFFSET      1
#define MEGAVI_DATA_OFFSET    2
#define MEGAVI_CRC_OFFSET     2
#define MEGAVI_TRAILER_OFFSET 3
#define MEGAVI_FRAME_SIZE     4


#define MEGAVI_CONTROL_PARITY   MARK_PARITY     // bit 9 always 1
#define MEGAVI_NORMAL_PARITY    SPACE_PARITY    // bit 9 always 0


#endif

