//*****************************************************************************
//
//    Com.h - RS232 template header
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Com_H__
   #define __Com_H__

#if defined( __COM0__)

   #ifndef __Uart0_H__
      #include "Uart0.h"
   #endif
   
   #define ComInit()       Uart0Init()
   #define ComTxBusy()     Uart0TxBusy()
   #define ComTxChar(c)    Uart0TxChar(c)
   #define ComTxDrain()    Uart0TxDrain()   
   #define ComRxChar(c)    Uart0RxChar(c)
   #define ComRxWait(t)    Uart0RxWait(t)
   #define ComFlushChars() Uart0FlushChars()
   
#elif defined( __COM1__)

   #ifndef __Uart1_H__
      #include "Uart1.h"
   #endif

   #define ComInit()       Uart1Init()
   #define ComTxBusy()     Uart1TxBusy()
   #define ComTxChar(c)    Uart1TxChar(c)
   #define ComTxDrain()    Uart1TxDrain()   
   #define ComRxChar(c)    Uart1RxChar(c)
   #define ComRxWait(t)    Uart1RxWait(t)
   #define ComFlushChars() Uart1FlushChars()

#else
   #error "Unknown COM number"
#endif

#endif
