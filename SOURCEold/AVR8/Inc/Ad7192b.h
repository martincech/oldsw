//*****************************************************************************
//
//    Ad7192b.h    A/D convertor AD7192 services 
//    Version 1.0  (c) Vymos
//
//*****************************************************************************

#ifndef __Ad7192_H__
   #define __Ad7192_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void AdcInit( void);
// Inicializace prevodniku

void AdcReset( void);
// Reset communication

void AdcDataRate( word Hertz);
// Set <Hertz> conversion rate

void AdcSinc3( TYesNo Enable);
// <Enable> SINC3 filtering

void AdcChop( TYesNo Enable);
// <Enable> CHOP filtering

void AdcIdle( void);
// Set idle mode

void AdcPowerDown( void);
// Set power down mode

void AdcStart( void);
// Start continuous conversion

void AdcStop( void);
// Stop continous conversion

dword AdcZero( void);
// Process zero calibration, returns offset register

dword AdcFullScale( void);
// Process full-scale calibration, returns full scale register

void AdcSetZero( dword Value);
// Set offset register to <Value>

void AdcSetFullScale( dword Value);
// Set full scale register to <Value>

void AdcSetDelay( byte Samples);
// Wait <Samples> before measuring

void AdcSetSamples( byte Count);
// Set samples <Count>

void AdcSetBuffer( dword *Buffer);
// Set conversion buffer

void AdcConversionDone( void);
// Conversion done callback

#endif
