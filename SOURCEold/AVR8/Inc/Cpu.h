//*****************************************************************************
//
//    Cpu.h       AVR basic macros
//    Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __Cpu_H__
   #define __Cpu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#include <avr/interrupt.h>
#include <avr/wdt.h>

// aktivace/refresh WatchDogu :

#define WatchDogInit()    wdt_enable(WDTO_2S);
#define WatchDog()        wdt_reset()

// Globalni preruseni :

#define DisableInts() cli()
#define EnableInts()  sei()

// operace NOP :

#define _NOP() __asm__ __volatile__ ("nop")

// deleni hodinoveho kmitoctu :
#define CpuClockDivide( n)  XDIV = (129 - (n)) | (1 << XDIVEN)     // divide clock by n
#define CpuClockNormal()    XDIV = 0                               // disable clock division

#endif
