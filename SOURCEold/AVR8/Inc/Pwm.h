//*****************************************************************************
//
//    Ad7192b.h    A/D convertor AD7192 services 
//    Version 1.0  (c) Vymos
//
//*****************************************************************************

#ifndef __Pwm_H__
   #define __Pwm_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

void PwmInit( void);
// Initialization


void PwmSet( byte Percent);
// PWM control

#endif
