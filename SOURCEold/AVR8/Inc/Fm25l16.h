//*****************************************************************************
//
//    Fm25l16.h    FM25L16 FRAM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Fm25l16_H__
   #define __Fm25l16_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Spi_H__
   #include "Spi.h"
#endif   

void FramInit( void);
// Initialisation (warning - without SPI)

TYesNo FramIsPresent( void);
// Returns YES if memory is present

byte FramReadByte( word Address);
// Returns single byte from <Address>

void FramWriteByte( word Address, byte Value);
// Write <Value> at <Address>

//------ Block read -----------------------------

void FramBlockReadStart( word Address);
// Block read start

#define FramBlockReadData() SpiReadByte()
// Read byte from block

#define FramBlockReadStop() SpiRelease()
// Stop read block

//------ Block write ----------------------------

void FramPageWriteStart( word Address);
// Start page write at <Address>

#define FramPageWriteData( Value) SpiWriteByte( Value)
// Write byte to page

#define FramPageWritePerform() SpiRelease()
// Write page


#endif
