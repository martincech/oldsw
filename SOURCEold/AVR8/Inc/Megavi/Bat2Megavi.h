//*****************************************************************************
//
//   Bat2Megavi.h  Bat2 to MEGAVI protocol definitions
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Bat2Megavi_H__
   #define __Bat2Megavi_H__

#ifndef __Bat2Dat_H__
   #include "Bat2Dat.h"
#endif

#ifndef __Bat2Sms_H__
   #include "Bat2Sms.h"
#endif

//-----------------------------------------------------------------------------
// Commands
//-----------------------------------------------------------------------------

typedef enum {
   BAT2_MEGAVI_COMMAND_UNDEFINED,           // undefined/unused command
   BAT2_MEGAVI_COMMAND_VERSION,             // get version info
   BAT2_MEGAVI_COMMAND_MALE_DATA_GET,       // get statistics & weighings for male gender
   BAT2_MEGAVI_COMMAND_FEMALE_DATA_GET,     // get statistics & weighings for female gender
   BAT2_MEGAVI_COMMAND_SMS_SEND,            // send SMS
   BAT2_MEGAVI_COMMAND_SMS_STATUS_GET,      // get SMS status
   _BAT2_MEGAVI_COMMAND_LAST                // enum delimiter
} EBat2MegaviCommand;

//-----------------------------------------------------------------------------
// SMS request code
//-----------------------------------------------------------------------------

#define BAT2_MEGAVI_SMS_REQUEST_INVALID    '\0'                 // no SMS request
#define BAT2_MEGAVI_SMS_REQUEST_SHORT      ' '                  // SMS request without data
#define BAT2_MEGAVI_SMS_REQUEST_MIN        ((char)(' ' + 1))    // SMS request character min
#define BAT2_MEGAVI_SMS_REQUEST_MAX        '~'                  // SMS request character max

//-----------------------------------------------------------------------------
// SMS target code
//-----------------------------------------------------------------------------

#define BAT2_MEGAVI_SMS_TARGET_BROADCAST   '\0'                 // broadcast SMS to all phone numbers
#define BAT2_MEGAVI_SMS_TARGET_MIN         ' '                  // single SMS reply character min
#define BAT2_MEGAVI_SMS_TARGET_MAX         '~'                  // single SMS reply character max

//-----------------------------------------------------------------------------
// Short command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;                       // command code BAT2_MEGAVI_COMMAND_VERSION, BAT2_MEGAVI_COMMAND_MALE_DATA_GET, BAT2_MEGAVI_COMMAND_FEMALE_DATA_GET
} TBat2MegaviCommand;

//-----------------------------------------------------------------------------
// SMS send command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;                       // command code BAT2_MEGAVI_COMMAND_SMS_SEND
   byte Target;                        // SMS destination code
   char Text[ BAT2_SMS_SIZE];          // SMS text
} TBat2MegaviCommandSmsSend;

//-----------------------------------------------------------------------------
// SMS status command
//-----------------------------------------------------------------------------

typedef struct {
   byte Command;                       // command code BAT2_MEGAVI_SMS_STATUS_GET
   byte Slot;                          // requested SMS slot
} TBat2MegaviCommandSmsStatusGet;

//*****************************************************************************

//-----------------------------------------------------------------------------
// Replies
//-----------------------------------------------------------------------------

#define BAT2_MEGAVI_REPLY_BASE  0x80   // reply code base

typedef enum {
   BAT2_MEGAVI_REPLY_UNDEFINED = BAT2_MEGAVI_REPLY_BASE, // undefined/unused reply
   BAT2_MEGAVI_REPLY_VERSION,          // version reply
   BAT2_MEGAVI_REPLY_MALE_DATA,        // statistics & weighings data reply for male gender
   BAT2_MEGAVI_REPLY_FEMALE_DATA,      // statistics & weighings data reply for female gender
   BAT2_MEGAVI_REPLY_SMS_SEND,         // SMS send reply
   BAT2_MEGAVI_REPLY_SMS_STATUS,       // SMS status reply
   _BAT2_MEGAVI_REPLY_LAST             // enum delimiter
} EBat2MegaviReply;

//-----------------------------------------------------------------------------
// Version reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;                         // reply code BAT2_MEGAVI_REPLY_VERSION
   byte Minor;                         // minor version number
   byte Major;                         // major version number
} TBat2MegaviReplyVersion;

//-----------------------------------------------------------------------------
// Data reply
//-----------------------------------------------------------------------------

typedef struct {
   byte      Reply;                    // reply code BAT2_MEGAVI_REPLY_MALE_DATA, BAT2_MEGAVI_REPLY_FEMALE_DATA
   byte      SmsRequest;               // SMS request code
   TBat2Data Data;                     // statistics & weighings data
} TBat2MegaviReplyData;

//-----------------------------------------------------------------------------
// SMS send reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;                         // reply code BAT2_MEGAVI_REPLY_SMS_SEND
   byte Slot;                          // SMS slot code
} TBat2MegaviReplySmsSend;

#define BAT2_MEGAVI_SMS_PHONEBOOK_ERROR 0xFD     // phonebook error unable send broadcast
#define BAT2_MEGAVI_SMS_TARGET_ERROR    0xFE     // unknown target phone number
#define BAT2_MEGAVI_SMS_SLOT_ERROR      0xFF     // unable get SMS slot

//-----------------------------------------------------------------------------
// SMS status reply
//-----------------------------------------------------------------------------

typedef struct {
   byte Reply;                         // reply code
   byte Status;                        // SMS status code
} TBat2MegaviReplySmsStatus;

// SMS status codes :
typedef enum {
   BAT2_MEGAVI_SMS_STATUS_UNDEFINED,   // unknown status
   BAT2_MEGAVI_SMS_STATUS_EMPTY,       // SMS slot empty
   BAT2_MEGAVI_SMS_STATUS_PENDING,     // SMS pending for send
   BAT2_MEGAVI_SMS_STATUS_SENDED,      // SMS already sended
   BAT2_MEGAVI_SMS_STATUS_ERROR = 0xFF // unable deliver SMS
} EBat2MegaviSmsStatus;


#endif
