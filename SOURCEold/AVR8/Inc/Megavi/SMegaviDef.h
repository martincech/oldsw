//*****************************************************************************
//
//   SMegaviDef.h  MEGAVI simulator protocol
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __SMegaviDef_H__
   #define __SMegaviDef_H__

#ifndef __Uni_H__
   #include "Uni.h"
#endif

//-----------------------------------------------------------------------------
// Protocol
//-----------------------------------------------------------------------------

typedef struct {
   byte Leader;
   byte Size1;             // payload data & id size
   byte Size2;             // payload data & id size 
   byte Id;                // MEGAVI Id field
   byte Data[];            // MEGAVI payload data
// byte Crc;   
} TSMegaviFrame;

// CRC is bit inversion of Sum <Size1>,<Size2>,<Id>,<DATA>

#define SMEGAVI_LEADER_CHAR    0x55

#define SMEGAVI_LEADER_OFFSET  0
#define SMEGAVI_SIZE1_OFFSET   1
#define SMEGAVI_SIZE2_OFFSET   2
#define SMEGAVI_ID_OFFSET      3
#define SMEGAVI_DATA_OFFSET    4
#define SMEGAVI_CRC_OFFSET     4

#define SMEGAVI_FRAME_SIZE     5

#endif
