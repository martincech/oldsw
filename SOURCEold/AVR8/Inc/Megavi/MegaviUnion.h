//*****************************************************************************
//
//   MegaviUnion.h MEGAVI unions
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __MegaviUnion_H__
   #define __MegaviUnion_H__

#ifndef __Bat2Megavi_H__
   #include "Bat2Megavi.h"
#endif

//-----------------------------------------------------------------------------
// Command
//-----------------------------------------------------------------------------

typedef union {
   byte Command;                                 // command code only
   TBat2MegaviCommand             Version;       // get version command
   TBat2MegaviCommand             DataGet;       // get data command
   TBat2MegaviCommandSmsSend      SmsSend;       // send SMS
   TBat2MegaviCommandSmsStatusGet SmsStatusGet;  // get SMS status
} TMegaviCommand;

//-----------------------------------------------------------------------------
// Reply
//-----------------------------------------------------------------------------

typedef union {
   byte                      Reply;              // reply code only
   TBat2MegaviReplyVersion   Version;            // version reply
   TBat2MegaviReplyData      Data;               // get data reply
   TBat2MegaviReplySmsSend   SmsSend;            // send SMS reply
   TBat2MegaviReplySmsStatus SmsStatus;          // get SMS status reply
} TMegaviReply;

#endif
