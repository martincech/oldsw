//*****************************************************************************
//
//    Iic.h  -  I2C bus services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Iic_H__
   #define __Iic_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


void IicInit( void);
// Provede inicializaci.

void IicStart( void);
// Vygeneruje na sbernici startovaci sekvenci. Lze volat
// z libovolne pozice, zajisti klidovy stav sbernice, prodlevu
// a nasledujici start

void IicStop( void);
// Vygeneruje na sbernici Ukoncovaci sekvenci. Lze volat
// z libovolne pozice, zajisti klidovy stav sbernice bez
// koncoveho cekani (je soucasti IicStart)

TYesNo IicSend( byte value);
// Vysle byte <value> po sbernici, precte odpoved ACK.
// Vraci YES pri ACK a NO pri NAK

byte IicReceive( TYesNo ack);
// Prijme byte se sbernice, vrati jej jako navratovou hodnotu.
// Odesle potvrzeni <ack>, YES je ACK NO je NAK

#endif

