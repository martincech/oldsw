//*****************************************************************************
//
//    Uart1.h - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Uart1_H__
   #define __Uart1_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void Uart1Init( void);
// Communication initialisation

TYesNo Uart1TxBusy( void);
// Returns YES if transmitter is busy

void Uart1TxChar( byte Char);
// Transmit <Char> byte

void Uart1TxDrain( void);
// Waits for end of transmission

TYesNo Uart1RxChar( byte *Char);
// Returns YES and received byte on <Char>,
// or NO on timeout

TYesNo Uart1RxWait( word Timeout);
// Waits for receive up to <Timeout> miliseconds

void Uart1FlushChars( void);
// Skips all Rx chars up to intercharacter timeout

#endif
