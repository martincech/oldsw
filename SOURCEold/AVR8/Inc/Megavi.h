//*****************************************************************************
//
//    Megavi.h      MEGAVI protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Megavi_H__
   #define __Megavi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MegaviDef_H__
   #include "MegaviDef.h"
#endif

// Parameter <Size> is without Leader/ID/CRC/Trailer characters

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void MegaviInit( void);
// Communication initialisation

void MegaviRxStart( void);
// Start reception

void MegaviStop( void);
// Stop processing

TYesNo MegaviRxReady( void);
// Returns YES, on Rx data ready

byte MegaviRxSize( void);
// Returns received DATA bytes count

byte MegaviRxId( void);
// Returns received ID field

void *MegaviData( void);
// Returns data buffer address

void MegaviTx( byte Id, byte Size);
// Start transmition with <Id> and <Size>

TYesNo MegaviTxDone( void);
// Wait for transmission done

#endif
