//*****************************************************************************
//
//    Endian.h     Endian conversions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Endian_H__
   #define __Endian_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif


word EndianWordSwap( word Value);
// Swap word <Value>

dword EndianDwordSwap( dword Value);
// Swap dword <Value>

#endif
