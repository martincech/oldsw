//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "CStdio.h"
#include "Uart1.h"
#include "Spi.h"

static byte _Buffer[ 128];


void Transceiver( byte Size);
// Send SPI & receive

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Char;

   // initialize devices :
   StatusLedInit();
   CalButtonInit();
   Uart1Init();
   StdioInit();
   SpiInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   // send data on start :
   cprintf( " Start\n");
   // main loop
   forever {
      if( CalButtonPressed()){
         StatusLedOn();
         SysDelay( 50);
         StatusLedOff();
         cprintf( "Button\n");
      }
      if( !Uart1RxChar( &Char)){
         continue;                     // timeout
      }
      switch( Char){
         case 'b' :
            Transceiver( 1);
            break;

         case 'c' :
            Transceiver( 2);
            break;

         case 'd' :
            Transceiver( 8);
            break;
           
         case 'e' :
            Transceiver( 128);
            break;

         default :
            Uart1TxChar( Char);              // echo character
            break;
      }
   }
   return( 0);
} // main


//-----------------------------------------------------------------------------
// Transceiver
//-----------------------------------------------------------------------------

void Transceiver( byte Size)
// Send SPI & receive
{
byte i;
byte *p;

   // Tx/Rx data :
   SpiAttach();
   p = _Buffer;
   if( Size == 1){
      *p = SpiByteTransceive( 0xAB);
   } else {
      for( i = 0; i < Size; i++){
         *p = SpiByteTransceive( i);
         p++;
      }
   }
   SpiRelease();
   // print dump :
   cprintf( "bytes : %d\n", Size);
   p = _Buffer;
   do {
      Uart1TxChar( *p);
      p++;
   } while( --Size);
   cprintf( "\nEND\n");
} // Transceiver
