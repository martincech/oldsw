//*****************************************************************************
//
//    Hardware.h   MBridge hardware descriptions STK500
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED    0
#define StatusLED1   1
#define StatusLED2   2
#define StatusLED3   3

#define StatusLedInit()  DDRC  |=  (1 << StatusLED) | (1 << StatusLED1) | (1 << StatusLED2) | (1 << StatusLED3);\
                         PORTC |=  (1 << StatusLED) | (1 << StatusLED1) | (1 << StatusLED2) | (1 << StatusLED3)
                         
#define StatusLedOn()    PORTC &= ~(1 << StatusLED)
#define StatusLedOff()   PORTC |=  (1 << StatusLED)

#define StatusLed1On()   PORTC &= ~(1 << StatusLED1)
#define StatusLed1Off()  PORTC |=  (1 << StatusLED1)

#define StatusLed2On()   PORTC &= ~(1 << StatusLED2)
#define StatusLed2Off()  PORTC |=  (1 << StatusLED2)

#define StatusLed3On()   PORTC &= ~(1 << StatusLED3)
#define StatusLed3Off()  PORTC |=  (1 << StatusLED3)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

// PD0 - RX, PD1 - TX

#define UART0_BAUD           9600       // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

//------------------------------------------------------------------------------
// UART1
//------------------------------------------------------------------------------

// PD2 - RX, PD3 - TX

#define UART1_BAUD           9600       // baud rate
#define UART1_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM1

//------------------------------------------------------------------------------
// SPI
//------------------------------------------------------------------------------

// PB4 - SS, PB5 - MOSI, PB6 - MISO, PB7 - SCK

#define SPI_SS   4
#define SPI_MOSI 5
#define SPI_MISO 6
#define SPI_SCK  7


#define SpiPortInit()    DDRB |=  (1 << SPI_SS) | (1 << SPI_SCK) | (1 << SPI_MOSI);\
                         DDRB &= ~(1 << SPI_MISO)

#define SpiAttach()      PORTB &= ~(1 << SPI_SS)
#define SpiRelease()     PORTB |=  (1 << SPI_SS)

#define SPI_TRANSCEIVE   1

#define SPI_CLOCK        125000L          // SPI clock [Hz]
#define SPI_CPOL         0                // idle clock polarity
#define SPI_CPHA         0                // clock phase leading/trailing edge

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   0

#define CalButtonInit()     DDRA  &= ~(1 << CalButton)

#define CalButtonPressed()  (!(PINA & (1 << CalButton)))       // active L


//-----------------------------------------------------------------------------

#endif
