###############################################################################
# Makefile for the project Fram
###############################################################################

## General Flags
PROJECT = Fram
MCU = atmega16
TARGET = Fram.elf
CC = avr-gcc

CPP = avr-g++

## Options common to compile, link and assembly rules
COMMON = -mmcu=$(MCU)

## Compile options common for all C compilation units.
CFLAGS = $(COMMON)
CFLAGS += -Wall -gdwarf-2 -std=gnu99 -DF_CPU=8000000UL -Os -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums -Wstrict-prototypes
CFLAGS += -MD -MP -MT $(*F).o -MF dep/$(@F).d 

## Assembly specific flags
ASMFLAGS = $(COMMON)
ASMFLAGS += $(CFLAGS)
ASMFLAGS += -x assembler-with-cpp -Wa,-gdwarf2

## Linker flags
LDFLAGS = $(COMMON)
LDFLAGS +=  -Wl,-Map=Fram.map


## Intel Hex file production flags
HEX_FLASH_FLAGS = -R .eeprom -R .fuse -R .lock -R .signature

HEX_EEPROM_FLAGS = -j .eeprom
HEX_EEPROM_FLAGS += --set-section-flags=.eeprom="alloc,load"
HEX_EEPROM_FLAGS += --change-section-lma .eeprom=0 --no-change-warnings


## Include Directories
INCLUDES = -I"C:\SOURCE\AVR8\Fram\." 

## Libraries
LIBS = -lprintf_min 

## Objects that must be built in order to link
OBJECTS = Main.o Cpu.o Uart0.o Stdio.o SpiSw.o Fm25v10.o 

## Objects explicitly added by the user
LINKONLYOBJECTS = 

## Build
all: $(TARGET) Fram.hex Fram.eep Fram.lss size

## Compile
Main.o: ../Main.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

Cpu.o: ../../Moduly/Cpu/Cpu.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

Uart0.o: ../../Moduly/CpuOld/Uart0.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

Stdio.o: ../../Moduly/LIB/Stdio.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

SpiSw.o: ../../Moduly/Cpu/SpiSw.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

Fm25v10.o: ../../Moduly/Fram/Fm25v10.c
	$(CC) $(INCLUDES) $(CFLAGS) -c  $<

##Link
$(TARGET): $(OBJECTS)
	 $(CC) $(LDFLAGS) $(OBJECTS) $(LINKONLYOBJECTS) $(LIBDIRS) $(LIBS) -o $(TARGET)

%.hex: $(TARGET)
	avr-objcopy -O ihex $(HEX_FLASH_FLAGS)  $< $@

%.eep: $(TARGET)
	-avr-objcopy $(HEX_EEPROM_FLAGS) -O ihex $< $@ || exit 0

%.lss: $(TARGET)
	avr-objdump -h -S $< > $@

size: ${TARGET}
	@echo
	@avr-size -C --mcu=${MCU} ${TARGET}

## Clean target
.PHONY: clean
clean:
	-rm -rf $(OBJECTS) Fram.elf dep/* Fram.hex Fram.eep Fram.lss Fram.map


## Other dependencies
-include $(shell mkdir dep 2>/dev/null) $(wildcard dep/*)

