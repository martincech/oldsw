//*****************************************************************************
//
//    Main.c         FRAM main
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "../Inc/System.h"  // Operating System
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"
#include "../Inc/Fm25v10.h"

#include "../Inc/Debug.h"
#include <stdlib.h>

#define SEED_INITIAL     1

//#define DEBUG_SHOW_WRITE 1   // show data written
//#define DEBUG_SHOW_DONE  1   // show write done

word Seed;

static void TestWrite( void);
// Write test pattern

static void TestFastWrite( void);
// Write test pattern

static void TestRead( void);
// Read test pattern

static void TestFastRead( void);
// Read test pattern

//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------

int main( void)
{
byte ch;

   StatusLedInit();
   StatusLedOff();
   ButtonInit();

   SpiInit();
   FramInit();

   TRACE_INIT();

   // flash :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   // main loop :
   TRACE( "START");
   Seed    = SEED_INITIAL;
   forever {
      if( !Uart0RxChar( &ch)){
         continue;
      }
      Uart0TxChar( ch);
      switch( ch){
         case 'p' :
            if( !FramIsPresent()){
               TRACE( "Not present\n");
               break;
            }
            cprintf( "Present OK\n");
            break;

         case 'w' :
            TestWrite();
            break;

         case 'r' :
            TestRead();
            break;

         case 's' :
            Seed++;
            cprintf( "Seed : %d\n", Seed);
            break;

         case 'c' :
            Seed = SEED_INITIAL;
            cprintf( "Seed : %d\n", Seed);
            break;

         case 'f' :
            TestFastWrite();
            break;

         case 'g' :
            TestFastRead();
            break;

         case 'x' :
            FramSleep();
            break;

         case 'y' :
            FramWakeup();
            break;

         default :
            break;

      }

   }
} // main

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------

static void TestWrite( void)
// Write test pattern
{
long i;
byte Rand;

   cprintf( "Start...\n");
   forever {
      srand( Seed);
      for( i = 0; i < FRAM_SIZE; i++){
          Rand = rand();
          FramWriteByte( i, Rand);
#ifdef DEBUG_SHOW_WRITE
          if( i < 10){
             cprintf( "%02X\n", Rand);
          }
#endif
      }
#ifdef DEBUG_SHOW_DONE
      Uart0TxChar( '.');
#endif
      if( !FramIsPresent()){
         cprintf( "\nDown\n");
         break;
      }
      Seed++;
      cprintf( "Seed : %d\n", Seed);
   }
} // TestWrite

//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------

static void TestRead( void)
// Read test pattern
{
long i;
byte Value;
byte Rand;
long Address;

   cprintf( "Start...\n");
   srand( Seed);
   for( i = 0; i < FRAM_SIZE; i++){
      Value = FramReadByte( i);
      Rand  = rand();
      if( Value != Rand){
         cprintf( "*%06lX : %02X (%02X)\n", i, Value, Rand);
         Address = i;              // first invalid address
         srand( Seed - 1);         // try previous pattern
         for( i = 0; i < FRAM_SIZE; i++){
            Rand  = rand();
            if( i < Address){
               continue;           // already compared
            }
            Value = FramReadByte( i);
            if( Value != Rand){
               cprintf( "@%06lX : %02X (%02X)\n", i, Value, Rand);
               if( ComRxWait(0)){
                  break;                // stop on first receive
               }
            }
         } // for
         cprintf( "*Done\n");
         return;
      } // if
   }
   cprintf( "Done\n");
} // TestRead

//------------------------------------------------------------------------------
// Fast Write
//------------------------------------------------------------------------------

static void TestFastWrite( void)
// Write test pattern
{
long i;

   cprintf( "Start...\n");
   forever {
      srand( Seed);
      FramPageWriteStart( 0);
      for( i = 0; i < FRAM_SIZE; i++){
          FramPageWriteData( rand());
      }
      FramPageWritePerform();
#ifdef DEBUG_SHOW_DONE
      Uart0TxChar( '.');
#endif
      if( !FramIsPresent()){
         cprintf( "\nDown\n");
         break;
      }
      Seed++;
      cprintf( "Seed : %d\n", Seed);
   }
} // TestFastWrite

//------------------------------------------------------------------------------
// Fast Read
//------------------------------------------------------------------------------

static void TestFastRead( void)
// Read test pattern
{
long i;
byte Value;
byte Rand;

   cprintf( "Start...\n");
   srand( Seed);
   FramBlockReadStart( 0);
   for( i = 0; i < FRAM_SIZE; i++){
      Value = FramBlockReadData();
      Rand  = rand();
      if( Value != Rand){
         cprintf( "@%06lX : %02X (%02X)\n", i, Value, Rand);
         if( ComRxWait(0)){
            break;                // stop on first receive
         }
      }
   }
   FramBlockReadStop();
   cprintf( "Done\n");
} // TestFastRead
