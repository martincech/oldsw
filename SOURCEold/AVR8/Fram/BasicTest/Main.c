//*****************************************************************************
//
//    Main.c         FRAM main
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "../Inc/System.h"  // Operating System
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"
#include "../Inc/Fm25l16.h"

#include "../Inc/Debug.h"
#include <stdlib.h>

#define SEED_INITIAL     1

word Seed;

static void TestWrite( void);
// Write test pattern

static void TestFastWrite( void);
// Write test pattern

static void TestRead( void);
// Read test pattern

static void TestFastRead( void);
// Read test pattern

//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------

int main( void)
{
byte ch;
word Address;
byte Value;

   StatusLedInit();
   StatusLedOff();
   ButtonInit();

   SpiInit();
   FramInit();

   TRACE_INIT();

   // flash :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   // main loop :
   TRACE( "START");
   Address = 0;
   Seed    = SEED_INITIAL;
   forever {
      if( !Uart0RxChar( &ch)){
         continue;
      }
      Uart0TxChar( ch);
      switch( ch){
         case 'p' :
            if( !FramIsPresent()){
               TRACE( "Not present\n");
               break;
            }
            cprintf( "Present OK\n");
            break;

         case 'w' :
            FramWriteByte( Address, 0x5A);
            cprintf( "Write OK\n");
            break;

         case 'r' :
            Value = FramReadByte( Address);
            cprintf( "Read : %02X\n", Value);
            break;

         case 'a' :
            Address++;
            break;

         case 'n' :
            Address = 0;
            break;

         case 'W' :
            TestWrite();
            break;

         case 'R' :
            TestRead();
            break;

         case 'S' :
            Seed++;
            cprintf( "Seed : %d\n", Seed);
            break;

         case 'C' :
            Seed = SEED_INITIAL;
            cprintf( "Seed : %d\n", Seed);
            break;

         case 'F' :
            TestFastWrite();
            break;

         case 'G' :
            TestFastRead();
            break;

         default :
            break;

      }

   }
} // main

//------------------------------------------------------------------------------
// Write
//------------------------------------------------------------------------------

static void TestWrite( void)
// Write test pattern
{
int  i;
byte Rand;

   cprintf( "Start...\n");
   srand( Seed);
   for( i = 0; i < FRAM_SIZE; i++){
       Rand = rand();
       FramWriteByte( i, Rand);
#ifdef __DEBUG__
       if( i < 10){
          cprintf( "%02X\n", Rand);
       }
#endif
   }
   cprintf( "Done\n");
} // TestWrite

//------------------------------------------------------------------------------
// Read
//------------------------------------------------------------------------------

static void TestRead( void)
// Read test pattern
{
int  i;
byte Value;
byte Rand;

   cprintf( "Start...\n");
   srand( Seed);
   for( i = 0; i < FRAM_SIZE; i++){
      Value = FramReadByte( i);
      Rand  = rand();
      if( Value != Rand){
         cprintf( "@%04X : %02X (%02X)\n", i, Value, Rand);
         if( ComRxWait(0)){
            break;                // stop on first receive
         }
      }
   }
   cprintf( "Done\n");
} // TestRead

//------------------------------------------------------------------------------
// Fast Write
//------------------------------------------------------------------------------

static void TestFastWrite( void)
// Write test pattern
{
int i;

   cprintf( "Start...\n");
   srand( Seed);
   FramPageWriteStart( 0);
   for( i = 0; i < FRAM_SIZE; i++){
       FramPageWriteData( rand());
   }
   FramPageWritePerform();
   cprintf( "Done\n");
} // TestFastWrite

//------------------------------------------------------------------------------
// Fast Read
//------------------------------------------------------------------------------

static void TestFastRead( void)
// Read test pattern
{
int  i;
byte Value;
byte Rand;

   cprintf( "Start...\n");
   srand( Seed);
   FramBlockReadStart( 0);
   for( i = 0; i < FRAM_SIZE; i++){
      Value = FramBlockReadData();
      Rand  = rand();
      if( Value != Rand){
         cprintf( "@%04X : %02X (%02X)\n", i, Value, Rand);
         if( ComRxWait(0)){
            break;                // stop on first receive
         }
      }
   }
   FramBlockReadStop();
   cprintf( "Done\n");
} // TestFastRead
