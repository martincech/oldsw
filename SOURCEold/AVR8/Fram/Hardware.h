//*****************************************************************************
//
//    Hardware.h   Fram STK500 hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../inc/cpu.h"
#include "../inc/uni.h"


#define __DEBUG__    1

//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED  0

#define StatusLedInit()   DDRC  |=  (1 << StatusLED)

#define StatusLedOn()    PORTC &= ~(1 << StatusLED) 
#define StatusLedOff()   PORTC |=  (1 << StatusLED) 

//-----------------------------------------------------------------------------
// Button
//-----------------------------------------------------------------------------

#define ButtonBT0   0

#define ButtonInit()     DDRA &= ~(1 << ButtonBT0)

#define ButtonPressed()  !(PINA & (1 << ButtonBT0))

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           9600       // Baud rate
#define UART0_RX_TIMEOUT     10000      // Intercharacter timeout [us]


//-----------------------------------------------------------------------------
// SPI bus
//-----------------------------------------------------------------------------

#define SpiSS            7              // PD7 signal SS/
#define SpiMOSI          6              // PD6 signal MOSI
#define SpiMISO          5              // PD5 signal MISO
#define SpiSCK           4              // PD4 signal SCK

#define SpiPortInit()    DDRD  |=  (1 << SpiSS) | (1 << SpiMOSI) | (1 << SpiSCK);\
                         DDRD  &= ~(1 << SpiMISO)

#define SpiSetCS()       PORTD |=  (1 << SpiSS)
#define SpiClrCS()       PORTD &= ~(1 << SpiSS)
#define SpiSetSCK()      PORTD |=  (1 << SpiSCK);
#define SpiClrSCK()      PORTD &= ~(1 << SpiSCK);
#define SpiSetSI()       PORTD |=  (1 << SpiMOSI)
#define SpiClrSI()       PORTD &= ~(1 << SpiMOSI)
#define SpiGetSO()      (PIND  &   (1 << SpiMISO))

// chipselect macros
#define SPI_SIMPLE_CS    1

#define SpiAttach()      SpiClrCS()
#define SpiRelease()     SpiSetCS()

//-----------------------------------------------------------------------------
// FRAM FM25V10
//-----------------------------------------------------------------------------

#define FRAM_SIZE         (128 * 1024L)   // memory total capacity

// conditional compilation :
#define FRAM_PRESENT      1             // check for presence
#define FRAM_READ_BYTE    1             // read single byte
#define FRAM_WRITE_BYTE   1             // write single byte

//-----------------------------------------------------------------------------

#endif
