//*****************************************************************************
//
//    Hardware.h   Bat1 tester hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../inc/cpu.h"
#include "../inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED  0

#define StatusLedInit()   DDRC  |=  (1 << StatusLED)

#define StatusLedOn()    PORTC &= ~(1 << StatusLED) 
#define StatusLedOff()   PORTC |=  (1 << StatusLED) 

//-----------------------------------------------------------------------------
// Button
//-----------------------------------------------------------------------------

#define ButtonBT0   0

#define ButtonInit()     DDRA &= ~(1 << ButtonBT0)

#define ButtonPressed()  !(PINA & (1 << ButtonBT0))

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           9600       // Baud rate
#define UART0_RX_TIMEOUT     10000      // Intercharacter timeout [us]


//-----------------------------------------------------------------------------

#endif
