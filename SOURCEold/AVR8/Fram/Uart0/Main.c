//*****************************************************************************
//
//    Main.c         FRAM main
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "../Inc/System.h"  // Operating System
#include "../Inc/Uart0.h"
#include "../Inc/Stdio.h"


//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------

int main( void)
{
byte ch;

   StatusLedInit();
   StatusLedOff();
   ButtonInit();

   Uart0Init();                        // inicializace UART
   StdioInit();                        // inicializace funkce cprintf

   // flash :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   // main loop :
   forever {
      if( Uart0RxChar( &ch)){
	      Uart0TxChar( ch);
      }
      if( ButtonPressed()){
         SysDelay( 50);
         if( ButtonPressed()){
            cprintf( "Button\n");
         }
      }
/*
      StatusLedOn();
      SysDelay( 500);
      StatusLedOff();
      SysDelay( 500);
      ComTxChar( 'A');
      ComTxChar( 'B');
      ComTxChar( 'C');
*/
   }
} // main
