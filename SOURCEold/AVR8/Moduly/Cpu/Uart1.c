//*****************************************************************************
//
//    Uart1.c - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Uart1.h"

// parameters :
#define BAUD_DIVISOR ((FXTAL / 16 / UART1_BAUD) - 1)

#ifdef UART1_PARITY_EVEN
   #define UART_PARITY_EVEN
#endif   
   
#ifdef UART1_PARITY_ODD
   #define UART_PARITY_ODD
#endif   
   
#define UART_RX_TIMEOUT UART1_RX_TIMEOUT

#ifdef UART1_RS485
   #define UART_RS485
#endif

// Functions :
#define UartInit       Uart1Init
#define UartTxBusy     Uart1TxBusy
#define UartTxChar     Uart1TxChar
#define UartTxDrain    Uart1TxDrain
#define UartRxChar     Uart1RxChar
#define UartRxWait     Uart1RxWait
#define UartFlushChars Uart1FlushChars
#define UartTxInit     Uart1TxInit
#define UartTxEnable   Uart1TxEnable
#define UartTxDisable  Uart1TxDisable

// Ports :
#define UBRRH UBRR1H
#define UBRRL UBRR1L
#define UCSRA UCSR1A
#define UCSRB UCSR1B
#define UCSRC UCSR1C
#define UDR   UDR1

// Common template :
#include "UartTpl.c"
