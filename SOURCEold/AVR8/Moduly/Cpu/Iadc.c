//******************************************************************************
//
//    Iadc.c -  internal A/D converter
//    Version 1.0 (c) Vymos
//
//******************************************************************************

#include "Iadc.h"

//------------------------------------------------------------------------------
// Inicializace
//------------------------------------------------------------------------------

void IadcInit( void)
// Inicializace prevodniku
{
   IadcPortInit();
   ADMUX   = ADC_REFERENCE | ADC_INPUT;     // select reference & input
   ADCSRA  = (1 << ADEN) | ADC_PRESCALER;   // A/D converter power on & set clock
} // IadcInit

//------------------------------------------------------------------------------
// Zastaveni
//------------------------------------------------------------------------------

void IadcStop( void)
// Zastaveni prevodniku
{
   ADCSRA &= ~(1 << ADEN);             // A/D converter power off
} // IadcStop

#ifndef ADC_AVERAGING  // single conversion
//------------------------------------------------------------------------------
// Mereni
//------------------------------------------------------------------------------

word IadcMeasure( byte Channel)
// Vrati zmerenou hodnotu na vstupu <Channel>
{
   ADMUX = ADC_REFERENCE | (Channel << MUX0); // select input
   ADCSRA |= (1 << ADSC);              // start conversion
   while( !(ADCSRA & (1 << ADIF)));    // wait for conversion done
   ADCSRA |= (1 << ADIF);              // clear ready flag
   return( ADCW);                      // read conversion result
} // IadcMeasure

#else // conversion with averaging
//------------------------------------------------------------------------------
// Mereni
//------------------------------------------------------------------------------

word IadcMeasure( byte Channel)
// Vrati zmerenou hodnotu na vstupu <Channel>
{
byte i;
word Value;

   ADMUX = ADC_REFERENCE | (Channel << MUX0); // select input
   i     = ADC_AVERAGE_COUNT;
   Value = 0;
   do {   
      ADCSRA |= (1 << ADSC);              // start conversion
      while( !(ADCSRA & (1 << ADIF)));    // wait for conversion done
      ADCSRA |= (1 << ADIF);              // clear ready flag
      Value  += ADCW;                     // read conversion result
   } while( --i);
   return( Value >> ADC_AVERAGE_SHIFT);
} // IadcMeasure
#endif
