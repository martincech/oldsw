//*****************************************************************************
//
//    Pwm.c          Pulse Width Modulation
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "Pwm.h"

// 9bit fast PWM : clear on compare match, WGM = 6, clock = CLOCK/1
#define TCCR1A_DISABLE ((1 << WGM11) | (0 << WGM10))  
#define TCCR1A_ENABLE  (TCCR1A_DISABLE | (2 << COM1A0))
#define TCCR1B_DEFAULT ((0 << WGM13)  | (1 << WGM12) | (1 << CS10))

#define OCR1A_VALUE( Percent)    ((word)(((word)512 * (Percent)) / 100))

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void PwmInit( void)
// Initialization
{
   // prepare ports :
   PwmPortInit();
   // init PWM :
   TCCR1A = TCCR1A_DISABLE;
   TCCR1B = TCCR1B_DEFAULT;
   PwmSet( 0);
} // PwmInit

//-----------------------------------------------------------------------------
// PWM
//-----------------------------------------------------------------------------

void PwmSet( byte Percent)
// PWM control
{
   if( Percent >= 100){
      TCCR1A = TCCR1A_DISABLE;         // PWM off
      PwmOn();                         // permanently on
      return;
   }
   if( Percent == 0){
      TCCR1A = TCCR1A_DISABLE;         // PWM off
      PwmOff();                        // permanently off
      return;
   }
   OCR1A  = OCR1A_VALUE( Percent);
   TCCR1A = TCCR1A_ENABLE;
} // PwmSet
