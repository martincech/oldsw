//*****************************************************************************
//
//   SpiHw.c     Hardware controller SPI interface
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include "Spi.h"

#if   (FXTAL / SPI_CLOCK) == 2
   #define SPI_CLOCK_DIVISOR  0
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 4
   #define SPI_CLOCK_DIVISOR  0
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 8
   #define SPI_CLOCK_DIVISOR  1
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 16
   #define SPI_CLOCK_DIVISOR  1
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 32
   #define SPI_CLOCK_DIVISOR  2
   #define SPI_CLOCK_X2       1
#elif (FXTAL / SPI_CLOCK) == 64
   #define SPI_CLOCK_DIVISOR  2
   #define SPI_CLOCK_X2       0
#elif (FXTAL / SPI_CLOCK) == 128
   #define SPI_CLOCK_DIVISOR  3
   #define SPI_CLOCK_X2       0
#else
   #error "Invalid SPI_CLOCK value"
#endif

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void SpiInit( void)
// Initialize bus
{
   SpiRelease();                       
   SpiPortInit();
   SPCR  = (1 << SPE) | (1 << MSTR) | (SPI_CPOL << CPOL) | (SPI_CPHA << CPHA) | 
           (SPI_CLOCK_DIVISOR << SPR0);
   SPSR |= (SPI_CLOCK_X2 << SPI2X);
} // SpiInit

//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte SpiByteRead( void)
// Read byte from SPI
{
   return( SpiByteTransceive( 0xFF));  // dummy Tx
} // SpiReadByte

//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void SpiByteWrite( byte Value)
// Write byte to SPI
{
   SpiByteTransceive( Value);
} // SpiWriteByte

//-----------------------------------------------------------------------------
// Write/Read
//-----------------------------------------------------------------------------

byte SpiByteTransceive( byte Value)
// Write/read SPI. Returns value read
{
   SPDR = Value;
   while( !(SPSR & (1 << SPIF)));
   return( SPDR);
} // SpiByteTransceive
