//*****************************************************************************
//
//    Fm25l16.c    FM25L16 FRAM services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Fm25l16.h"
#include "Debug.h"

// Commands :
#define FRAM_WREN    0x06           // Set Write Enable Latch (Enable Write)
#define FRAM_WRDI    0x04           // Reset Write Enable/Reset Flag Bit
#define FRAM_RDSR    0x05           // Read Status Register
#define FRAM_WRSR    0x01           // Write Status Register
#define FRAM_READ    0x03           // Read data
#define FRAM_WRITE   0x02           // Write data

// Status register format :
#define FRAM_MASK_WEL  0x02         // device is Write Enabled
#define FRAM_MASK_BP0  0x04         // protection bit 0 (1/4 capacity)
#define FRAM_MASK_BP1  0x08         // protection bit 1 (1/2 capacity)
#define FRAM_MASK_WPEN 0x80         // Write Protect /WP pin control

// Local functions

static void WrenCmd( void);
// Send WREN instruction

static byte RdsrCmd( void);
// Returns status register

static void WrsrCmd( byte Value);
// Write status register

static void WrdiCmd( void);
// Set write disable


//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void FramInit()
// Initialisation (warning - without SPI)
{
   WrenCmd();                          // set write enable latch
   WrsrCmd( 0);                        // clear write protection
} // FramInit

#ifdef FRAM_PRESENT
//-----------------------------------------------------------------------------
// Check for presence
//-----------------------------------------------------------------------------

TYesNo FramIsPresent( void)
// Returns YES if memory is present
{
   WrenCmd();                          // write enable - set WEN bit in the status register
   if( !(RdsrCmd() & FRAM_MASK_WEL)){
      return( NO);                     // not present
   }
   WrdiCmd();                          // write disable - clear WEN bit
   if(   RdsrCmd() & FRAM_MASK_WEL){
      return( NO);                     // not present
   }
   return( YES);
} // FramIsPresent
#endif // FRAM_PRESENT

#ifdef FRAM_READ_BYTE
//-----------------------------------------------------------------------------
// Read
//-----------------------------------------------------------------------------

byte FramReadByte( word Address)
// Returns single byte from <Address>
{
byte Value;

   FramBlockReadStart( Address);       // block start
   Value = FramBlockReadData();        // write data byte
   FramBlockReadStop();                // stop block read
   return( Value);
} // FramReadByte

#endif // FRAM_READ_BYTE

//-----------------------------------------------------------------------------
// Page read
//-----------------------------------------------------------------------------

void FramBlockReadStart( word Address)
// Block read start
{
   SpiAttach();                        // select CS
   SpiWriteByte( FRAM_READ);           // READ instruction
   SpiWriteByte( Address >> 8);        // high address
   SpiWriteByte( Address);             // low address
} // FramBlockReadStart

#ifdef FRAM_WRITE_BYTE
//-----------------------------------------------------------------------------
// Write
//-----------------------------------------------------------------------------

void FramWriteByte( word Address, byte Value)
// Write <Value> at <Address>
{
   FramPageWriteStart( Address);       // start page write
   FramPageWriteData( Value);          // write data byte
   FramPageWritePerform();             // save data
} // FramWriteByte

#endif // FRAM_WRITE_BYTE

//-----------------------------------------------------------------------------
// Zapis Stranky do pameti
//-----------------------------------------------------------------------------

void FramPageWriteStart( word Address)
// Start page write at <Address>
{
   WrenCmd();                          // Set write enable latch
   SpiAttach();                        // select CS
   SpiWriteByte( FRAM_WRITE);          // WRITE instruction
   SpiWriteByte( Address >> 8);        // high address
   SpiWriteByte( Address);             // low address
} // FramPageWriteStart

//-----------------------------------------------------------------------------
// Inctruction WREN
//-----------------------------------------------------------------------------

static void WrenCmd()
// Send WREN instruction
{
   SpiAttach();                        // select CS
   SpiWriteByte( FRAM_WREN);
   SpiRelease();                       // deselect CS
} // WrenCmd


//-----------------------------------------------------------------------------
// Inctruction RDSR
//-----------------------------------------------------------------------------

static byte RdsrCmd()
// Returns status register
{
byte Value;

   SpiAttach();                        // select CS
   SpiWriteByte( FRAM_RDSR);
   Value  = SpiReadByte();
   SpiRelease();                       // deselect CS
   return( Value);
}  // RdsrCmd

//-----------------------------------------------------------------------------
// Inctruction WRSR
//-----------------------------------------------------------------------------

static void WrsrCmd( byte Value)
// Write status register
{
   SpiAttach();                        // select CS
   SpiWriteByte( FRAM_WRSR);
   SpiWriteByte( Value);
   SpiRelease();                       // deselect CS
}  // WrsrCmd

//-----------------------------------------------------------------------------
// Inctruction WRDI
//-----------------------------------------------------------------------------

static void WrdiCmd( void)
// Set write disable
{
  SpiAttach();                         // select CS
  SpiWriteByte( FRAM_WRDI);
  SpiRelease();                        // deselect CS
} // WrdiCmd
