//*****************************************************************************
//
//    Megavi.c      MEGAVI protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Megavi.h"
#include "System.h"          // Operating system

// UART Ports :
#ifndef MEGAVI_COM1
   // MEGAVI_COM0
   #define UBRRH UBRR0H
   #define UBRRL UBRR0L
   #define UCSRA UCSR0A
   #define UCSRB UCSR0B
   #define UCSRC UCSR0C
   #define UDR   UDR0

   #define USART_RX_VECTOR       USART0_RX_vect
   #define USART_TX_VECTOR       USART0_UDRE_vect
   #define USART_TX_DONE_VECTOR  USART0_TX_vect
#else // MEGAVI_COM1
   #define UBRRH UBRR1H
   #define UBRRL UBRR1L
   #define UCSRA UCSR1A
   #define UCSRB UCSR1B
   #define UCSRC UCSR1C
   #define UDR   UDR1

   #define USART_RX_VECTOR       USART1_RX_vect
   #define USART_TX_VECTOR       USART1_UDRE_vect
   #define USART_TX_DONE_VECTOR  USART1_TX_vect
#endif

#define BAUD_DIVISOR ((FXTAL / 16 / MEGAVI_BAUD) - 1)

// UART Helper macros :
#define ComEnableRxInt()       UCSRB |=  (1 << RXCIE0)
#define ComDisableRxInt()      UCSRB &= ~(1 << RXCIE0)

#define ComEnableTxInt()       UCSRB |=  (1 << UDRIE0)
#define ComDisableTxInt()      UCSRB &= ~(1 << UDRIE0)

#define ComEnableTxDoneInt()   UCSRB |=  (1 << TXCIE0)
#define ComDisableTxDoneInt()  UCSRB &= ~(1 << TXCIE0)
#define ComClearTxDone()       UCSRA &= ~(1 << TXC0)

#define ComDisableInts()       UCSRB &= ~( (1 << RXCIE0) | (1 << TXCIE0) | (1 << UDRIE0))

#define ComRxError( s)         ((s) & ((1 << FE0) | (1 << DOR0) | (1 << UPE0)))

#define ComDirectionRx()       MegaviTxDisable()
#define ComDirectionTx()       MegaviTxEnable()

//-----------------------------------------------------------------------------

// Device states :
typedef enum {
   MEGAVI_STATUS_STOPPED,                 // stopped
   MEGAVI_STATUS_RX_RUNNING,              // Rx active
   MEGAVI_STATUS_RX_DONE,                 // Rx done, read data
   MEGAVI_STATUS_TX_RUNNING,              // Tx active
   _MEGAVI_STATUS_LAST
} TMegaviStatus;

// parity check :
#define RxControlParity( p)  ((p) & (1 << RXB80))         // bit 9 is set   - MARK
#define RxDataParity( p)     (!RxControlParity( p))       // bit 9 is clear - SPACE

#define TxControlParity()    UCSRB |=  (1 << TXB80)       // set bit 9      - MARK
#define TxDataParity()       UCSRB &= ~(1 << TXB80)       // clear bit 9    - SPACE

#define INDEX_LEADER      0xFF         // index at wait for leader state

// Local variables :
static byte          _Buffer[ MEGAVI_BUFFER_SIZE];
static byte          _DataSize;        // Tx data size
static byte          _Index;           // Rx/Tx buffer index
static byte volatile _Status;          // device status

// Local functions :
static byte CalculateCrc( byte Size);
// Calculate <_Buffer> with <Size> CRC

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void MegaviInit( void)
// Communication initialisation
{
   ComDisableInts();
   MegaviTxDisable();                       // RS485 Rx direction
   MegaviTxInit();                          // RS485 direction init
   UBRRH = (byte)(BAUD_DIVISOR >> 8);       // set baud rate divisor
   UBRRL = (byte) BAUD_DIVISOR;
   UCSRB = (1 << RXEN0) | (1 << TXEN0) | (1 << UCSZ02);    // Enable Rx/Tx, 9bits
   UCSRC = (3 << UCSZ00);                                  // 9 bits, 1 stopbit
   _Status   = MEGAVI_STATUS_STOPPED;  // stop communication
   _DataSize = 0;
} // MegaviInit

//-----------------------------------------------------------------------------
// Rx start
//-----------------------------------------------------------------------------

void MegaviRxStart( void)
// Start reception
{
   ComDisableInts();                   // disable interrupts
   ComDirectionRx();                   // direction Rx
   _Status   = MEGAVI_STATUS_RX_RUNNING;  // start of reception
   _Index    = INDEX_LEADER;           // wait for leader
   ComEnableRxInt();                   // enable Rx interrupts
} // MegaviRxStart

//-----------------------------------------------------------------------------
// Stop
//-----------------------------------------------------------------------------

void MegaviStop( void)
// Stop processing
{
   ComDisableInts();                   // disable interrupts
   ComDirectionRx();                   // direction Rx
   _Status   = MEGAVI_STATUS_STOPPED;  // stop communication
   _DataSize = 0;
} // MegaviStop

//-----------------------------------------------------------------------------
// Rx ready
//-----------------------------------------------------------------------------

TYesNo MegaviRxReady( void)
// Returns YES, on Rx data ready
{
   return( _Status == MEGAVI_STATUS_RX_DONE);
} // MegaviRxReady

//-----------------------------------------------------------------------------
// Rx size
//-----------------------------------------------------------------------------

byte MegaviRxSize( void)
// Returns received DATA bytes count
{
   return( _Index - 2);                // without CRC and ID
} // MegaviRxSize

//-----------------------------------------------------------------------------
// Rx ID
//-----------------------------------------------------------------------------

byte MegaviRxId( void)
// Returns received ID field
{
   return( _Buffer[ MEGAVI_ID_OFFSET - 1]);    // ID field (buffer doesn't contain leader)
} // MegaviRxId

//-----------------------------------------------------------------------------
// Data buffer
//-----------------------------------------------------------------------------

void *MegaviData( void)
// Returns data buffer address
{
   return( &_Buffer[ MEGAVI_DATA_OFFSET - 1]); // skip ID field (buffer doesn't contain leader)
} // MegaviData

//-----------------------------------------------------------------------------
// Tx send
//-----------------------------------------------------------------------------

void MegaviTx( byte Id, byte Size)
// Start transmition with <Id> and <Size>
{
   _Buffer[ MEGAVI_ID_OFFSET - 1]          = Id;
   _Buffer[ Size + MEGAVI_DATA_OFFSET - 1] = CalculateCrc( Size);
   ComDisableInts();                   // disable interrupts
   SysDelay( MEGAVI_TX_SPACE);         // space before send
   ComDirectionTx();                   // direction to Tx
   _Status   = MEGAVI_STATUS_TX_RUNNING;  // start transmission
   _DataSize = Size + 2;               // requested data size with additional ID and CRC
   _Index    = INDEX_LEADER;           // start with leader character
   ComEnableTxInt();
} // MegaviTx


//-----------------------------------------------------------------------------
// Tx done
//-----------------------------------------------------------------------------

TYesNo MegaviTxDone( void)
// Wait for transmission done
{
   return( _Status != MEGAVI_STATUS_TX_RUNNING);
} // MegaviTxDone

//*****************************************************************************

//-----------------------------------------------------------------------------
// CRC
//-----------------------------------------------------------------------------

static byte CalculateCrc( byte Size)
// Calculate <_Buffer> with <Size> CRC
{
byte Crc;
byte *Data;

   Data = &_Buffer[ MEGAVI_DATA_OFFSET - 1];  // skip ID field (buffer doesn't contain leader)
   Crc  = 0xFF;
   do {
      Crc ^= *Data;
      Data++;
   } while( --Size);
   return( Crc);
} // CalculateCrc

//-----------------------------------------------------------------------------
// Rx done interrupt
//-----------------------------------------------------------------------------

ISR( USART_RX_VECTOR)
{
byte Data;
byte Status;
byte Data9;
byte Crc;

   Status = UCSRA;                     // status must be read before UDR
   Data9  = UCSRB;                     // bit9 must be read before UDR
   Data   = UDR;                       // read data register
   // check for framing error :
   if( ComRxError( Status)){
      _Index = INDEX_LEADER;           // wait for leader again
      StatusLedGOff();
      return;
   }
   // check for leader :
   if( RxControlParity( Data9) && (Data == MEGAVI_LEADER_CHAR)){
      _Index = 0;                      // start new data
      StatusLedGOn();
      return;
   }
   if( _Index == INDEX_LEADER){
      StatusLedGOff();
      return;                          // skip characters before leader
   }
   // check for overrun :
   if( _Index >= MEGAVI_BUFFER_SIZE){
      _Index = INDEX_LEADER;           // data overrun, wait for leader again
      StatusLedGOff();
      return;
   }
   // check for trailer :
   if( RxControlParity( Data9) && (Data == MEGAVI_TRAILER_CHAR)){
      if( _Index < MEGAVI_FRAME_SIZE - 2){
         _Index = INDEX_LEADER;        // short frame, wait for leader again
         StatusLedGOff();
         return;
      }
      // valid data
      Crc = CalculateCrc( _Index - 2); // data size without ID and CRC
      if( _Buffer[ _Index - 1] != Crc){
         _Index = INDEX_LEADER;        // wrong CRC, wait for leader again
         StatusLedGOff();
         return;
      }
      // data ok :
      ComDisableRxInt();
      _Status = MEGAVI_STATUS_RX_DONE;
      return;
   }
   // check for data parity :
   if( !RxDataParity( Data9)){
      _Index = INDEX_LEADER;           // parity error, wait for leader again
      StatusLedGOff();
      return;
   }
   _Buffer[ _Index++] = Data;          // save received data
} // USART0_RX_VECTOR

//-----------------------------------------------------------------------------
// Tx data interrupt
//-----------------------------------------------------------------------------

ISR( USART_TX_VECTOR)
{
   // check for leader :
   if( _Index == INDEX_LEADER){
      TxControlParity();               // update bit 9
      UDR    = MEGAVI_LEADER_CHAR;     // send leader
      _Index = 0;                      // first data character
      return;
   }
   // check for trailer :
   if( _Index == _DataSize){
      TxControlParity();               // update bit 9
      UDR    = MEGAVI_TRAILER_CHAR;    // send trailer
      ComDisableTxInt();               // stop Tx data interrupt
      ComClearTxDone();                // clear Tx done flag
      ComEnableTxDoneInt();            // wait for shift register empty
      return;
   }
   // send data :
   TxDataParity();                     // update bit 9
   UDR = _Buffer[ _Index++];           // send data
} // USART_TX_VECTOR

//-----------------------------------------------------------------------------
// Tx done interrupt
//-----------------------------------------------------------------------------

ISR( USART_TX_DONE_VECTOR)
{
   ComDisableTxDoneInt();               // stop Tx done interrupt
   StatusLedsOff();
   MegaviRxStart();                     // start reception
} // USART_TX_DONE_VECTOR
