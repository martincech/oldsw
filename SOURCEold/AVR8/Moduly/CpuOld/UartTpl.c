//*****************************************************************************
//
//    Uart.c - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Delays.h"

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void UartInit( void)
// Communication initialisation
{
   UBRRH = (byte)(BAUD_DIVISOR >> 8);  // set baud rate divisor
   UBRRL = (byte) BAUD_DIVISOR;
   UCSRB = (1 << RXEN) | (1 << TXEN);  // enable Rx, Tx
   #if (defined __AVR_ATmega64__) || (defined __AVR_ATmega128__) || (defined __AVR_ATtiny2313__)
      #if defined( UART_PARITY_EVEN)
         UCSRC = (3 << UCSZ0) | (2 << UPM0); // 8 bits, 1 stopbit, even
      #elif defined( UART_PARITY_ODD)
         UCSRC = (3 << UCSZ0) | (3 << UPM0); // 8 bits, 1 stopbit, odd
      #else // no parity
         UCSRC = (3 << UCSZ0);               // 8 bits, 1 stopbit
      #endif   
   #else   
      #if defined( UART_PARITY_EVEN)
         UCSRC = (1 << URSEL) | (3 << UCSZ0) | (2 << UPM0); // 8 bits, 1 stopbit, even
      #elif defined( UART_PARITY_ODD)
         UCSRC = (1 << URSEL) | (3 << UCSZ0) | (3 << UPM0); // 8 bits, 1 stopbit, odd
      #else // no parity
         UCSRC = (1 << URSEL) | (3 << UCSZ0);               // 8 bits, 1 stopbit
      #endif   
   #endif
} // UartInit

//-----------------------------------------------------------------------------
// Tx Busy
//-----------------------------------------------------------------------------

TYesNo UartTxBusy( void)
// Returns YES if transmitter is busy
{
   return( UCSRA & (1 << UDRE));
} // UartTxBusy

//-----------------------------------------------------------------------------
// Tx character
//-----------------------------------------------------------------------------

void UartTxChar( byte Char)
// Transmit <Char> byte
{
   while( !(UCSRA & (1 << UDRE)));
   UCSRA |= (1 << TXC);              // clear Tx complete bit
   UDR = Char;
} // UartTxChar

//-----------------------------------------------------------------------------
// Tx empty
//-----------------------------------------------------------------------------

void UartTxDrain( void)
// Waits for end of transmission
{
   while( !(UCSRA & (1 << TXC)));    // wait for Tx complete set
} // UartTxDrain

//-----------------------------------------------------------------------------
// Rx character
//-----------------------------------------------------------------------------

TYesNo UartRxChar( byte *Char)
// Returns YES and received byte on <Char>,
// or NO on timeout
{
word Timeout;

   Timeout = WDelayCount( UART_RX_TIMEOUT, 3);
   do {
      if( UCSRA & (1 << RXC)){
         *Char = UDR;
         return( YES);
      }
   } while( --Timeout);
   return( NO);
} // UartRxChar

//-----------------------------------------------------------------------------
// Wait for Rx
//-----------------------------------------------------------------------------

TYesNo UartRxWait( word Timeout)
// Waits for receive up to <Timeout> miliseconds
{
word i;

   i = 1;
   Timeout++;
   do{
      do {
         if( UCSRA & (1 << RXC)){
            return( YES);
         }
      } while( --i);
      i = WDelayCount( 1000, 3);
   } while( --Timeout);
   return( NO);
} // UartRxWait

//-----------------------------------------------------------------------------
// Flush Rx
//-----------------------------------------------------------------------------

void UartFlushChars( void)
// Skips all Rx chars up to intercharacter timeout
{
word Timeout;
byte c;

   Timeout = WDelayCount( UART_RX_TIMEOUT, 3);
   do {
      if( UCSRA & (1 << RXC)){
         c  = UDR;                                      // get character
         Timeout = WDelayCount( UART_RX_TIMEOUT, 3);   // timeout again
         continue;
      }
   } while( --Timeout);
} // UartFlushChars
