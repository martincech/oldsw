//*****************************************************************************
//
//    Uart0.c - RS232 communication services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Uart0.h"

// parameters :
#define BAUD_DIVISOR ((FXTAL / 16 / UART0_BAUD) - 1)

#ifdef UART0_PARITY_EVEN
   #define UART_PARITY_EVEN
#endif   
   
#ifdef UART0_PARITY_ODD
   #define UART_PARITY_ODD
#endif   
   
#define UART_RX_TIMEOUT UART0_RX_TIMEOUT

// Functions :
#define UartInit       Uart0Init
#define UartTxBusy     Uart0TxBusy
#define UartTxChar     Uart0TxChar
#define UartTxDrain    Uart0TxDrain
#define UartRxChar     Uart0RxChar
#define UartRxWait     Uart0RxWait
#define UartFlushChars Uart0FlushChars

/*
// Ports :
#define UBRRH UBRR0H
#define UBRRL UBRR0L
#define UCSRA UCSR0A
#define UCSRB UCSR0B
#define UCSRC UCSR0C
#define UDR   UDR0
*/

// Common template :
#include "UartTpl.c"
