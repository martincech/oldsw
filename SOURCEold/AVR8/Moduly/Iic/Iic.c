//*****************************************************************************
//
//    Iic.c  -  I2C bus services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Iic.h"
#include "System.h"


#ifndef IIC_WAIT
   #define IIC_WAIT  5                 // prodleva us
#endif

static void IicWait( void);
// prodleva min 5us

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void IicInit( void)
// Provede inicializaci.
{
   IicPortInit();                      // SCL, SDA vystup na L
   IicSetSDA();                        // SDA direction in = HiZ
   IicSetSCL();                        // SCL direction in = HiZ
} // IicInit

//-----------------------------------------------------------------------------
// Start Bit
//-----------------------------------------------------------------------------

void IicStart( void)
// Vygeneruje na sbernici startovaci sekvenci. Lze volat
// z libovolne pozice, zajisti klidovy stav sbernice, prodlevu
// a nasledujici start
{
   IicSetSDA();                        // klidovy stav sbernice
   IicSetSCL();
   IicWait();                          // setup pred startbitem
   IicClrSDA();                        // start
   IicWait();                          // setup hodin
   IicClrSCL();                        // atakovana sbernice SCL=0, SDA=0
   IicWait();                          // dokonceni periody hodin
} // IicStart

//-----------------------------------------------------------------------------
// Stop Bit
//-----------------------------------------------------------------------------

void IicStop( void)
// Vygeneruje na sbernici Ukoncovaci sekvenci. Lze volat
// z libovolne pozice, zajisti klidovy stav sbernice bez
// koncoveho cekani (je soucasti IicStart)
{
   IicClrSDA();
   IicSetSCL();
   IicWait();                          // setup pred stopbitem
   IicSetSDA();                        // stopbit, klidovy stav sbernice SCL=1 SDA=1
} // IicStop

//-----------------------------------------------------------------------------
// Vyslani bytu
//-----------------------------------------------------------------------------

TYesNo IicSend( byte value)
// Vysle byte <value> po sbernici, precte odpoved ACK.
// Vraci YES pri ACK a NO pri NAK
{
byte i;
byte ack;

   i = 8;
   do {
      if( value & 0x80){
         IicSetSDA();
      } else {
         IicClrSDA();
      }
      SysUDelay( 1);                   // data setup
      IicSetSCL();
      IicWait();
      IicClrSCL();
      IicWait();
      value <<= 1;
   } while( --i);
   // prijem ACK/NAK :
   IicSetSDA();                        // data do vysoke urovne
   IicSetSCL();
   IicWait();
   ack = !IicGetSDA();                 // pozitivni potvrzeni je 0
   IicClrSCL();
   IicWait();                          // dokonceni periody hodin
   return( ack);
} // IicSend

//-----------------------------------------------------------------------------
// Cteni bytu
//-----------------------------------------------------------------------------

byte IicReceive( TYesNo ack)
// Prijme byte se sbernice, vrati jej jako navratovou hodnotu.
// Odesle potvrzeni <ack>, YES je ACK NO je NAK
{
byte value;
byte i;

   value  = 0;
   IicSetSDA();                        // data do vysoke urovne
   i = 8;
   do {
      value <<= 1;
      IicSetSCL();
      IicWait();
      if( IicGetSDA()){
         value++;
      }
      IicClrSCL();
      IicWait();
   } while( --i);
   // vyslani ACK/NAK :
   if( ack){
      IicClrSDA();                     // pozitivni potvrzeni je 0
   } // else SetSDA() uz mame
   IicSetSCL();
   IicWait();
   IicClrSCL();
   IicWait();                          // dokonceni periody hodin
   return( value);
} // IicReceive

//-----------------------------------------------------------------------------
// Cekani
//-----------------------------------------------------------------------------

static void IicWait( void)
// prodleva min 5us (vcetne volani)
{
   SysUDelay( IIC_WAIT);
} // IicWait
