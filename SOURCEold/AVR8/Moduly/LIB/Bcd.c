//*****************************************************************************
//
//    Bcd.c - BCD utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Bcd.h"
#include <stdlib.h>

//-----------------------------------------------------------------------------
// Binarni->BCD
//-----------------------------------------------------------------------------

byte bbin2bcd( byte x)
// prevede 0..99 do bcd
{
div_t Tmp;
byte  Bcd;

   Bcd  = 0;
   Tmp  = div( x, 10);                  // div/mod 10
   Bcd |= Tmp.quot << 4;                // decimals
   Bcd |= Tmp.rem;                      // units
   return( Bcd);
} // bbin2bcd

#ifndef BCD_DISABLE_WORD

word wbin2bcd( word x)
// prevede 0..9999 do bcd
{
div_t Tmp;
word  Bcd;

   Bcd  = 0;

   Tmp  = div( x, 10);                 // units
   x    = Tmp.quot;                    // div 10
   Bcd |= Tmp.rem;                     // mod 10

   Tmp  = div( x, 10);                 // decimals
   x    = Tmp.quot;                    // div 10
   Bcd |= Tmp.rem << 4;                // mod 10
   
   Tmp  = div( x, 10);                 // hundreds
   x    = Tmp.quot;                    // div 10
   Bcd |= Tmp.rem << 8;                // mod 10

   Tmp  = div( x, 10);                 // thousands
   x    = Tmp.quot;                    // div 10
   Bcd |= Tmp.rem << 12;               // mod 10
   
   return( Bcd);
} // wbin2bcd

#endif // BCD_DISABLE_WORD

//-----------------------------------------------------------------------------
// BCD->Binarni
//-----------------------------------------------------------------------------

byte bbcd2bin( byte x)
// prevede 0..0x99 do bin
{
   return( hnibble( x) * 10 + lnibble( x));
} // bbcd2bin

#ifndef BCD_DISABLE_WORD

word wbcd2bin( word x)
// prevede 0..0x9999 do bin
{
   return( (word)bbcd2bin( x >> 8) * 100 + bbcd2bin( (byte)x));
} // wbcd2bin

#endif // BCD_DISABLE_WORD

#ifndef BCD_DISABLE_NIBBLE
//-----------------------------------------------------------------------------
// Prevod na znaky
//-----------------------------------------------------------------------------

byte nibble2hex( byte x)
// prevede dolni nibble na '0'..'F'
{
   x &= 0x0F;
   if( (x) < 10){
      return( x + '0');
   } else {
      return( x - 10 + 'A');
   }
} // nibble2hex

//-----------------------------------------------------------------------------
// Prevod na cislo
//-----------------------------------------------------------------------------

byte char2hex( byte ch)
// prevede ASCII '0'..'9' a 'A'..'F' na cislo
{
   return( (ch) <= '9' ? (ch) - '0' : (ch) - 'A' + 10);
} // char2hex

#endif // BCD_DISABLE_NIBBLE
