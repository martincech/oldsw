//*****************************************************************************
//
//    conio.h - simple display services
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#include "conio.h"
#include "Bcd.h"
#include <stdlib.h>

//------------------------------------------------------------------------------
// String
//------------------------------------------------------------------------------
   
void mputs( PGM_P String)
// Put string
{
char ch;

   ch = pgm_read_byte( String);
   while( ch){
      putchar( ch);
      String++;
      ch = pgm_read_byte( String);
   }
} // mputs

//------------------------------------------------------------------------------
// Hexadecimal number
//------------------------------------------------------------------------------

void cprinthex( word Value, byte Decimals)
// Print hexadecimal <Value>, place dot on <Decimals> position
{
byte Tmp;

   if( Decimals == 4){
      putchar( '0');
      putchar( '.');                        // 0.9999
   }
   Tmp = Value >> 8;                        // upper byte
   putchar( nibble2hex( hnibble( Tmp)));    // upper nibble
   if( Decimals == 3){
      putchar( '.');                        // 9.999
   }
   putchar( nibble2hex( lnibble( Tmp)));    // lower nibble
   if( Decimals == 2){
      putchar( '.');                        // 99.99
   }
   Tmp = Value & 0xFF;                      // lower byte
   putchar( nibble2hex( hnibble( Tmp)));    // upper nibble
   if( Decimals == 1){
      putchar( '.');                        // 999.9
   }
   putchar( nibble2hex( lnibble( Tmp)));    // lower nibble
} // cprinthex

//------------------------------------------------------------------------------
// Decimal number
//------------------------------------------------------------------------------

void cprintdec( word Value, byte Decimals)
// Print decimal <Value>, place dot on <Decimals> position
{
   Value = wbin2bcd( Value);   
   cprinthex( Value, Decimals);
} // cprintdec

//------------------------------------------------------------------------------
// Long number
//------------------------------------------------------------------------------

void cint32( dword Value)
// Print decimal <Value> (up to 9999 9999)
{
ldiv_t Tmp;

   Tmp = ldiv( Value, 10000);
   Tmp.quot = wbin2bcd( Tmp.quot);
   cprinthex( Tmp.quot, 0);   
   Tmp.rem  = wbin2bcd( Tmp.rem);
   cprinthex( Tmp.rem, 0);
} // cprintdec

#ifdef __CPRINTTIME__
//------------------------------------------------------------------------------
// Time
//------------------------------------------------------------------------------

void cprinttime( dword Value)
// Print time as hh:mm:ss
{
ldiv_t Tmp;
byte   hh, mm, ss;

   Tmp   = ldiv( Value, 60);           // seconds
   Value = Tmp.quot;                   // div 60
   ss    = Tmp.rem;                    // mod 60

   Tmp   = ldiv( Value, 60);           // minuts
   Value = Tmp.quot;                   // div 60
   mm    = Tmp.rem;                    // mod 60

   Tmp   = ldiv( Value, 24);           // hours
   Value = Tmp.quot;                   // div 60
   hh    = Tmp.rem;                    // mod 60

   hh = bbin2bcd( hh);                 // hours
   putchar( nibble2dec( hnibble( hh)));
   putchar( nibble2dec( lnibble( hh)));
   putchar( ':');
   mm = bbin2bcd( mm);                 // minuts
   putchar( nibble2dec( hnibble( mm)));
   putchar( nibble2dec( lnibble( mm)));
   putchar( ':');
   ss = bbin2bcd( ss);               // seconds
   putchar( nibble2dec( hnibble( ss)));
   putchar( nibble2dec( lnibble( ss)));
} // cprinttime
#endif
