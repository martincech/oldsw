//*****************************************************************************
//
//    putchar.c      putchar via COM
//    Version 1.00   (c) VymOs
//
//*****************************************************************************

#include "conio.h"

#ifndef PUTCHAR_COM1
   #define __COM0__
#else
   #define __COM1__
#endif

#include "../inc/Com.h"

// ---------------------------------------------------------------------------------------------
// putchar
// ---------------------------------------------------------------------------------------------

int putchar( int c)
// standardni zapis znaku
{
   ComTxChar( c);
   return( c);
} // putchar
