//*****************************************************************************
//
//    Endian.c     Endian conversions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "Endian.h"


//-----------------------------------------------------------------------------
// Swap word
//-----------------------------------------------------------------------------

word EndianWordSwap( word Value)
// Swap word <Value>
{
word Result;

   Result = ((Value & 0xFF) << 8) | 
             (Value         >> 8);
   return( Result);
} // EndianWordSwap

//-----------------------------------------------------------------------------
// Swap dword
//-----------------------------------------------------------------------------

dword EndianDwordSwap( dword Value)
// Swap dword <Value>
{
dword Result;

   Result = ((Value & 0x000000FF) << 24) | ((Value & 0x0000FF00) << 8) | 
            ((Value & 0x00FF0000) >> 8)  |  (Value >> 24);
   return( Result);
} // EndianDwordSwap
