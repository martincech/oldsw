//******************************************************************************
//
//   DtRaw.c      Data/Time raw utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "Dt.h"

// helper macros :
#define YearDay( m)       pgm_read_word( &_YDay[ (m) - 1]) // days from begin of year
#define IsLeapYear( y)    (!((y) & 0x03))                  // leap year

// date internal representation :
typedef word TDtDate;                            // days from 1.1.2000

// month days from start of year :
static const word _YDay[12] PROGMEM = {
  0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
};

// Local functions :

static TDtDate EncodeDate( byte Year, byte Month, byte Day);
// Encode <Local> to internal date representation

static TTimestamp ComposeDateTime( TDtDate Date, TLocalTime *Time);
// Compose  <Date> with <Time>

//------------------------------------------------------------------------------
// Compose
//------------------------------------------------------------------------------

TTimestamp DtCompose( TLocalTime *Local)
// Compose date/time <Local> to internal representation
{
TDtDate Date;

   Date = EncodeDate( Local->Year, Local->Month, Local->Day);
   return( ComposeDateTime( Date, Local));
} // DtCompose

//******************************************************************************

//------------------------------------------------------------------------------
// Encode date
//------------------------------------------------------------------------------

static TDtDate EncodeDate( byte Year, byte Month, byte Day)
// Encode <Local> to internal date representation
{
int Leaps;                  // count of leap days
int Tmp;

   Leaps = (Year + 4) / 4;             // 2000 (0) is first leap
   if( IsLeapYear( Year) && Month < DT_MARCH){
      Leaps--;                         // this year is leap, but low date
   }
   Tmp = Leaps + --Day;                // day of month startig at 0
   return( Tmp + (TDtDate)Year * 365 +  YearDay( Month));
} // EncodeDate

//******************************************************************************
// Compose date & time
//******************************************************************************

static TTimestamp ComposeDateTime( TDtDate Date, TLocalTime *Time)
// Compose  <Date> with <Time>
{
TTimestamp Timestamp;

   Timestamp  = (TTimestamp)Time->Sec  * DT_SEC;
   Timestamp += (TTimestamp)Time->Min  * DT_MIN;
   Timestamp += (TTimestamp)Time->Hour * DT_HOUR;
   Timestamp += (TTimestamp)Date * DT_DAY;
   return( Timestamp);
} // ComposeDateTime
