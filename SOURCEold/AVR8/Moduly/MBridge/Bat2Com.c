//*****************************************************************************
//
//    Bat2Com.c     Bat2 bridge protocol services via SPI
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Bat2Com.h"
#include "SpiSlave.h"        // SPI slave services
#include <string.h>

// errors monitor :
#define StatusLed1On()
#define StatusLed2On()

// Local functions :

static void BatcTxClear( void);
// Clear Tx buffer

static byte CalculateCrc( void *Buffer, byte Size);
// Calculate <Buffer> with data <Size> CRC

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void BatcInit( void)
// Communication initialisation
{
   SpiInit();
   BatcTxClear();
} // BatcInit

//-----------------------------------------------------------------------------
// Rx start
//-----------------------------------------------------------------------------

void BatcRxStart( void)
// Start reception
{
   SpiRxStart();
} // BatcRxStart

//-----------------------------------------------------------------------------
// Rx ready
//-----------------------------------------------------------------------------

TYesNo BatcRxReady( void)
// Returns YES, on Rx data ready
{
byte *RxData;
byte  DataSize;
byte  FrameSize;

   if( !SpiRxReady()){
      return( NO);
   }
   RxData    = SpiRxData();
   FrameSize = SpiRxSize();
   if( FrameSize < BRIDGE_FRAME_SIZE){
      BatcRxStart();
      StatusLed1On();
      return( NO);
   }
   // check for query data :
   if( RxData[ BRIDGE_LEADER_OFFSET] == BRIDGE_QUERY_CHAR){
      BatcRxStart();
      return( NO);                     // no incoming data, query only
   }
   // check for data framing :
   if( RxData[ BRIDGE_LEADER_OFFSET] != BRIDGE_LEADER_CHAR){
      BatcRxStart();
      StatusLed2On();
      return( NO);
   }
   DataSize = RxData[ BRIDGE_SIZE1_OFFSET];
   if( DataSize != RxData[ BRIDGE_SIZE2_OFFSET]){
      BatcRxStart();
      StatusLed1On();
      return( NO);
   }
   if( DataSize != FrameSize - BRIDGE_FRAME_SIZE){
      BatcRxStart();
      StatusLed1On();
      return( NO);
   }   
   if( !CalculateCrc( RxData, DataSize)){
      BatcRxStart();
      StatusLed1On();
      return( NO);
   }   
   BatcTxClear();                      // prepare invalid reply
   return( YES);
} // BatcRxReady

//-----------------------------------------------------------------------------
// Rx size
//-----------------------------------------------------------------------------

byte BatcRxSize( void)
// Returns received DATA bytes count
{
   return( SpiRxSize() - BRIDGE_FRAME_SIZE);
} // BatcRxSize

//-----------------------------------------------------------------------------
// Rx Data buffer
//-----------------------------------------------------------------------------

void *BatcRxData( void)
// Returns Rx data buffer address
{
byte *RxData;

   RxData  = SpiRxData();
   RxData += BRIDGE_DATA_OFFSET;
   return( RxData);
} // BatcRxData

//-----------------------------------------------------------------------------
// Tx Data buffer
//-----------------------------------------------------------------------------

void *BatcTxData( void)
// Returns Tx data buffer address
{
byte *TxData;

   TxData  = SpiTxData();
   TxData += BRIDGE_DATA_OFFSET;
   return( TxData);
} // BatcTxData

//-----------------------------------------------------------------------------
// Tx send
//-----------------------------------------------------------------------------

void BatcTx( byte Size)
// Start transmition with <Size>
{
byte *TxData;

   TxData  = SpiTxData();
   TxData[ BRIDGE_LEADER_OFFSET]         = BRIDGE_LEADER_CHAR;
   TxData[ BRIDGE_SIZE1_OFFSET]          = Size;
   TxData[ BRIDGE_SIZE2_OFFSET]          = Size;
   TxData[ Size + BRIDGE_FRAME_SIZE - 1] = CalculateCrc( TxData, Size);
   SpiTx( Size + BRIDGE_FRAME_SIZE);
   SpiRxStart();                       // enable Rx/Tx
} // BatcTx


//*****************************************************************************

//-----------------------------------------------------------------------------
// Clear Tx buffer
//-----------------------------------------------------------------------------

static void BatcTxClear( void)
// Clear Tx buffer
{
byte *TxData;

   TxData = SpiTxData();
   memset( TxData, 0xFF, SPI_TX_BUFFER_SIZE);
} // BatcTxClear

//-----------------------------------------------------------------------------
// CRC
//-----------------------------------------------------------------------------

static byte CalculateCrc( void *Buffer, byte Size)
// Calculate <Buffer> with data <Size> CRC
{
byte Crc;
byte *Data;

   Data  = Buffer + BRIDGE_SIZE1_OFFSET;
   Size += BRIDGE_DATA_OFFSET - 1;     // inclusive Size1,2
   Crc   = 0;
   do {
      Crc += *Data;
      Data++;
   } while( --Size);
   return( ~Crc);
} // CalculateCrc
