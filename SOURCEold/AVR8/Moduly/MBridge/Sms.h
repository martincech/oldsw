//*****************************************************************************
//
//   Sms.c         SMS memory services
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#ifndef __Sms_H__
   #define __Sms_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __Bat2Sms_H__
   #include "Bat2Sms.h"
#endif

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SmsInit( void);
// Module init

void SmsTimer( void);
// Module 1s timer

//-----------------------------------------------------------------------------
// Writer
//-----------------------------------------------------------------------------

byte SmsInsert( char Target, char *Text);
// Insert SMS <Text> by <Target> into processing, returns Slot

byte SmsStatus( byte Slot);
// Returns SMS <Slot> status

//-----------------------------------------------------------------------------
// Reader
//-----------------------------------------------------------------------------

byte SmsCount( void);
// Returns SMS count ready for send

byte SmsPhoneGet( char *Phone);
// Get <Phone> for send, returns Slot

byte SmsTextGet( char *Text);
// Get <Text> for send, returns Slot

void SmsStatusSet( byte Slot, byte Status);
// Set SMS <Status> at <Slot>

//-----------------------------------------------------------------------------
// Received SMS
//-----------------------------------------------------------------------------

TYesNo SmsPhoneSet( char *Phone);
// Set received SMS <Phone>

TYesNo SmsTextSet( char *Text);
// Set received SMS <Text>

byte SmsReceivedStatus( void);
// Returns received SMS status

#endif
