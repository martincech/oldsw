//*****************************************************************************
//
//   Bat2Execute.c    MBridge Bat2 executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#include "Bat2Execute.h"
#include "System.h"          // Operating system
#include "Endian.h"          // Endian swap
#include "MBridgeDef.h"      // MBridge application
#include "Bat2Com.h"         // Bat2 communication
#include "Sms.h"             // SMS memory
#include <string.h>

TBat2Configuration Config;   // global configuration

static TBat2Data _MaleData;
static TBat2Data _FemaleData;

// Local functions :

static void ProcessCommand( TBridgeCommand *Command, byte Size);
// Process <Command>

static void ReplySimple( byte ReplyCode);
// Simple reply

static void ReplyConfigurationSet( TBat2Configuration *Configuration);
// Save <Configuration> and reply

static void ReplyMaleDataSet( TBat2Data *Data);
// Save male data and reply

static void ReplyFemaleDataSet( TBat2Data *Data);
// Save female data and reply

static void ReplySmsPhoneGet( void);
// Reply with SMS phone number

static void ReplySmsTextGet( void);
// Reply with SMS text

static void ReplySmsCountGet( void);
// Reply with SMS count

static void ReplySmsPhoneSet( char *Phone);
// Received SMS phone number

static void ReplySmsTextSet( char *Text);
// Received SMS text

static void DataEndianSwap( TBat2Data *Data);
// Swap <Data> endian

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void BatxInit( void)
// Module init
{
   memset( &_MaleData,   0, sizeof( TBat2Data));
   memset( &_FemaleData, 0, sizeof( TBat2Data));
   memset( &Config,      0, sizeof( TBat2Configuration));
#ifdef __CONFIG_DEFAULTS__
   // configuration defaults :
   Config.BridgeAddress    = MBRIDGE_MEGAVI_ADDRESS;
   Config.ReplyTimeout     = 10;   // [min]
   Config.BroadcastTimeout = 60;   // [min]
   Config.MegaviTimeout    = 60;   // [s]
   Config.PhoneBookSize    = 2;
   memset( Config.Phone, 0, BAT2_PHONEBOOK_SIZE * BAT2_SMS_PHONE_NUMBER_SIZE);
   strcpy( Config.Phone[ 0], "420604967296");
   strcpy( Config.Phone[ 1], "420604967296");
#endif
   BatcInit();
   BatcRxStart();
} // BatxInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void BatxExecute( void)
// Module executive
{
byte Size;
byte *Data;

   if( !BatcRxReady()){
      return;
   }
   Data = BatcRxData();
   Size = BatcRxSize();
   ProcessCommand( (TBridgeCommand *)Data, Size);
} // BatxExecute

//-----------------------------------------------------------------------------
// Configuration OK
//-----------------------------------------------------------------------------

TYesNo BatxConfigurationValid( void)
// Return YES on valid configuration
{
   return( MBridgeAddressValid( Config.BridgeAddress));
} // BatxConfigurationValid

//-----------------------------------------------------------------------------
// Male data
//-----------------------------------------------------------------------------

TBat2Data *BatxMaleData( void)
// Returns Bat2 actual male data
{
   return( &_MaleData);
} // BatxMaleData

//-----------------------------------------------------------------------------
// Female data
//-----------------------------------------------------------------------------

TBat2Data *BatxFemaleData( void)
// Returns Bat2 actual female data
{
   return( &_FemaleData);
} // BatxFemaleData

//*****************************************************************************

//-----------------------------------------------------------------------------
// Process command
//-----------------------------------------------------------------------------

static void ProcessCommand( TBridgeCommand *Command, byte Size)
// Process <Command>
{
   // check for valid configuration
   if( !BatxConfigurationValid() && (Command->Command != BRIDGE_COMMAND_CONFIGURATION_SET)){
      ReplySimple( BRIDGE_REPLY_ERROR);          // accept configuration command only
      return;
   }
   switch( Command->Command){
      case BRIDGE_COMMAND_CONFIGURATION_SET :
         if( Size != sizeof( TBridgeCommandConfigurationSet)){
            return;
         }
         ReplyConfigurationSet( &Command->ConfigurationSet.Configuration);
         return;

      case BRIDGE_COMMAND_MALE_DATA_SET :
         if( Size != sizeof( TBridgeCommandDataSet)){
            return;
         }
         ReplyMaleDataSet( &Command->DataSet.Data);
         return;

      case BRIDGE_COMMAND_FEMALE_DATA_SET :
         if( Size != sizeof( TBridgeCommandDataSet)){
            return;
         }
         ReplyFemaleDataSet( &Command->DataSet.Data);
         return;

      case BRIDGE_COMMAND_SMS_COUNT_GET :
         if( Size != sizeof( TBridgeCommandSmsCountGet)){
            return;
         }
         ReplySmsCountGet();
         return;

      case BRIDGE_COMMAND_SMS_PHONE_GET :
         if( Size != sizeof( TBridgeCommandSmsPhoneGet)){
            return;
         }
         ReplySmsPhoneGet();
         return;

      case BRIDGE_COMMAND_SMS_TEXT_GET :
         if( Size != sizeof( TBridgeCommandSmsTextGet)){
            return;
         }
         ReplySmsTextGet();
         return;

      case BRIDGE_COMMAND_SMS_STATUS_SET :
         if( Size != sizeof( TBridgeCommandSmsStatusSet)){
            return;
         }
         if( Command->SmsStatusSet.Status == YES){
            SmsStatusSet( Command->SmsStatusSet.Slot, BAT2_MEGAVI_SMS_STATUS_SENDED);
         } else {
            SmsStatusSet( Command->SmsStatusSet.Slot, BAT2_MEGAVI_SMS_STATUS_ERROR);
         }
         ReplySimple( BRIDGE_REPLY_SMS_STATUS_SET);
         return;

      case BRIDGE_COMMAND_SMS_PHONE_SET :
         if( Size != sizeof( TBridgeCommandSmsPhoneSet)){
            return;
         }
         ReplySmsPhoneSet( Command->SmsPhoneSet.Phone);
         return;

      case BRIDGE_COMMAND_SMS_TEXT_SET :
         if( Size != sizeof( TBridgeCommandSmsTextSet)){
            return;
         }
         ReplySmsTextSet( Command->SmsTextSet.Text);
         return;

      default :
         return;
   }
} // ProcessCommand

//-----------------------------------------------------------------------------
// Simple reply
//-----------------------------------------------------------------------------

static void ReplySimple( byte ReplyCode)
// Simple reply
{
TBridgeReplySimple *Reply;

   // get reply buffer :
   Reply = (TBridgeReplySimple *)BatcTxData();
   // prepare reply data :
   Reply->Reply = ReplyCode;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySimple));
} // ReplySimple

//-----------------------------------------------------------------------------
// Configuration set
//-----------------------------------------------------------------------------

static void ReplyConfigurationSet( TBat2Configuration *Configuration)
// Save <Configuration> and reply
{
byte n, i;

   Config = *Configuration;
   // no endian swap, all data byte / char
   ReplySimple( BRIDGE_REPLY_CONFIGURATION_SET);
   StatusLedROff();                    // clear wrong configuration signalisation
   // replace phone number spaces with zero :
   for( n = 0; n < Config.PhoneBookSize; n++){
      for( i = 0; i < BAT2_SMS_PHONE_NUMBER_SIZE; i++){
         if( Config.Phone[ n][ i] == ' '){
            Config.Phone[ n][ i] = '\0';
         }
      }
   }
} // ReplyConfigurationSet

//-----------------------------------------------------------------------------
// Male data set
//-----------------------------------------------------------------------------

static void ReplyMaleDataSet( TBat2Data *Data)
// Save male data and reply
{
   memcpy( &_MaleData, Data, sizeof( TBat2Data));
   DataEndianSwap( &_MaleData);
   ReplySimple( BRIDGE_REPLY_MALE_DATA_SET);
} // ReplyMaleDataSet

//-----------------------------------------------------------------------------
// Data set
//-----------------------------------------------------------------------------

static void ReplyFemaleDataSet( TBat2Data *Data)
// Save female data and reply
{
   memcpy( &_FemaleData, Data, sizeof( TBat2Data));
   DataEndianSwap( &_FemaleData);
   ReplySimple( BRIDGE_REPLY_FEMALE_DATA_SET);
} // ReplyDataSet

//-----------------------------------------------------------------------------
// SMS get phone number
//-----------------------------------------------------------------------------

static void ReplySmsPhoneGet( void)
// Reply with SMS phone number
{
TBridgeReplySmsPhoneGet *Reply;
byte                     Slot;

   // get reply buffer :
   Reply = (TBridgeReplySmsPhoneGet *)BatcTxData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_PHONE_GET;
   Slot         = SmsPhoneGet( Reply->Phone);
   if( Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      memset( Reply->Phone, 0, BAT2_SMS_PHONE_NUMBER_SIZE);  // SMS memory empty
   }
   Reply->Slot  = Slot;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsPhoneGet));
} // ReplySmsPhoneGet

//-----------------------------------------------------------------------------
// SMS get text
//-----------------------------------------------------------------------------

static void ReplySmsTextGet( void)
// Reply with SMS text
{
TBridgeReplySmsTextGet *Reply;
byte                    Slot;

   // get reply buffer :
   Reply = (TBridgeReplySmsTextGet *)BatcTxData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_TEXT_GET;
   Slot         = SmsTextGet( Reply->Text);
   if( Slot == BAT2_MEGAVI_SMS_SLOT_ERROR){
      memset( Reply->Text, 0, BAT2_SMS_SIZE);  // SMS memory empty
   }
   Reply->Slot  = Slot;
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsTextGet));
} // ReplySmsTextGet

//-----------------------------------------------------------------------------
// SMS count reply
//-----------------------------------------------------------------------------

static void ReplySmsCountGet( void)
// Reply with SMS count
{
TBridgeReplySmsCountGet *Reply;

   // get reply buffer :
   Reply = (TBridgeReplySmsCountGet *)BatcTxData();
   // prepare reply data :
   Reply->Reply = BRIDGE_REPLY_SMS_COUNT_GET;
   Reply->Count = SmsCount();
   // send reply data :
   BatcTx( sizeof( TBridgeReplySmsCountGet));
} // ReplySmsCountGet

//-----------------------------------------------------------------------------
// Rx SMS Phone
//-----------------------------------------------------------------------------

static void ReplySmsPhoneSet( char *Phone)
// Received SMS phone number
{
   if( !SmsPhoneSet( Phone)){
      ReplySimple( BRIDGE_REPLY_ERROR);
      return;
   }
   ReplySimple( BRIDGE_REPLY_SMS_PHONE_SET);
} // ReplySmsPhoneSet

//-----------------------------------------------------------------------------
// Rx SMS Text
//-----------------------------------------------------------------------------

static void ReplySmsTextSet( char *Text)
// Received SMS text
{
   if( !SmsTextSet( Text)){
      ReplySimple( BRIDGE_REPLY_ERROR);
      return;
   }
   ReplySimple( BRIDGE_REPLY_SMS_TEXT_SET);
} // ReplySmsTextSet

//-----------------------------------------------------------------------------
// Endian swap
//-----------------------------------------------------------------------------

static void DataEndianSwap( TBat2Data *Data)
// Swap <Data> endian
{
TBat2Weight *w;
byte         Count;

   // convert statistics :
   Data->Statistics.Target     = EndianWordSwap(  Data->Statistics.Target);
   Data->Statistics.Count      = EndianWordSwap(  Data->Statistics.Count);
   Data->Statistics.Average    = EndianWordSwap(  Data->Statistics.Average);
   Data->Statistics.Gain       = EndianDwordSwap( Data->Statistics.Gain);
   Data->Statistics.Sigma      = EndianWordSwap(  Data->Statistics.Sigma);
   Data->Statistics.Uniformity = EndianWordSwap(  Data->Statistics.Uniformity);
   // convert weighings :
   Count = BAT2_WEIGHT_COUNT;
   w     = Data->Weight;
   do {
      *w = EndianWordSwap( *w);
      w++;
   } while( --Count);
} // DataEndianSwap
