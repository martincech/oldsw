//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "MegaviExecute.h"   // MEGAVI executive
#include "Bat2Execute.h"     // Bat2 executive
#include "Sms.h"

static word _Timer;
static byte _Timer1s;

#define TIMER_1S         1000     // tick count for 1s

#define SetTimer()       _Timer = TIMER_1S

void SysExecute( void);
// Operating system executive

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
   // initialize devices :
   StatusLedInit();
   StatusLedsOff();
   // flash on start :
   StatusLedGOn();
   SysDelay( 500);
   StatusLedGOff();
   SysDelay( 500);
   // initialize executives :
   MexInit();
   BatxInit();
   SysStartTimer();
   EnableInts();
   // main loop
   forever {
      BatxExecute();
      MexExecute();
      SysExecute();
   }
   return( 0);
} // main

//------------------------------------------------------------------------------
// System
//------------------------------------------------------------------------------

void SysExecute( void)
// Operating system executive
{
   // check for 1s timer :
   SysDisableTimer();
   if( !_Timer1s){
      SysEnableTimer();
      return;
   }
   _Timer1s = NO;
   SysEnableTimer();
   // execute 1s tasks :
   SmsTimer();
} // SysExecute

//------------------------------------------------------------------------------
// Timer start
//------------------------------------------------------------------------------

#define TIMER_COUNT  (125 - 1)     // 1000Hz/1ms tick (8MHz / 64 / 125)


void SysStartTimer( void)
// Start system timer
{
   OCR0A   = TIMER_COUNT;               // max. timer value
   TCCR0A  = (2 << WGM00);              // WGM 2 (reset mode), COM 0 (no output)
   TCCR0B  = (3 << CS00);               // prescaler clk/64
   TIMSK0 |= (1 << OCIE0A);             // enable compare interrupt
   SetTimer();                          // 1s countdown
} // SysStartTimer

//------------------------------------------------------------------------------
// Timer handler
//------------------------------------------------------------------------------

ISR( TIMER0_COMPA_vect)
{
   if( !_Timer){
      SetTimer();
      return;
   }
   _Timer--;
   if( !_Timer){
      _Timer1s = YES;
      SetTimer();
   }
} // TimerHandler
