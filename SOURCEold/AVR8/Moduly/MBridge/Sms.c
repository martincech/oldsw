//*****************************************************************************
//
//   Sms.c         SMS memory services
//   Version 1.0   (c) Veit Electronics
//
//*****************************************************************************

#include "Sms.h"
#include "System.h"          // Operating system
#include "MBridgeDef.h"      // MBridge application
#include "Bat2Execute.h"     // Config only
#include <string.h>

//-----------------------------------------------------------------------------

// Sms memory :
typedef struct {
   byte Status;                                 // SMS actual state
   byte Target;                                 // SMS destination code
   char Phone[ BAT2_SMS_PHONE_NUMBER_SIZE + 1]; // SMS phone number
   char Text[ BAT2_SMS_SIZE + 1];               // SMS text
} TSms;

static TSms _Sms[ MBRIDGE_SMS_SLOT_COUNT];      // SMS FIFO
static byte _WriteSlot;                         // SMS FIFO write pointer
static byte _ReadSlot;                          // SMS FIFO read pointer
static word _Timeout;                           // SMS deliver timeout
static byte _ActivePhone;                       // active phonebook entry

// FIFO macros :
#define SmsFifoInit()                 _ReadSlot = 0; _WriteSlot = 0
#define SmsFifoEmpty()               (_ReadSlot == _WriteSlot)
#define SmsFifoFull()                ((byte)(_ReadSlot + MBRIDGE_SMS_SLOT_COUNT) == _WriteSlot)
#define SmsFifoCount()               (_WriteSlot - _ReadSlot)
#define SmsFifoWriteSlot()           (_WriteSlot++ % MBRIDGE_SMS_SLOT_COUNT)   // get position & move
#define SmsFifoReadSlot()            (_ReadSlot    % MBRIDGE_SMS_SLOT_COUNT)   // don't move position
#define SmsFifoRemove()              (_ReadSlot++)                             // move position only

#define SmsDeliverStart()             _Timeout = Config.ReplyTimeout     * 60;
#define SmsDeliverBroadcastStart()    _Timeout = Config.BroadcastTimeout * 60;
#define SmsDeliverStop()              _Timeout = 0;

#define SmsDeliverActive()            (_Timeout != 0)
#define SmsDeliverExpired()           ((--_Timeout) == 0)

//-----------------------------------------------------------------------------

// Received SMS :
typedef struct {
   byte Status;                                 // SMS status (BAT2_MEGAVI_SMS_REQUEST_xxx)
   byte Timeout;                                // reply timeout
   char Phone[ BAT2_SMS_PHONE_NUMBER_SIZE + 1]; // SMS phone numbers
   char Text[ BAT2_SMS_SIZE + 1];               // SMS text
} TRxSms;

static TRxSms _RxSms;

#define SmsRequestTimeoutClear()   _RxSms.Timeout = 0
#define SmsRequestTimeoutStart()   _RxSms.Timeout = Config.MegaviTimeout

#define SmsRequestInvalidSet()     _RxSms.Status = BAT2_MEGAVI_SMS_REQUEST_INVALID;
#define SmsRequestShortSet()       _RxSms.Status = BAT2_MEGAVI_SMS_REQUEST_SHORT;   
#define SmsRequestSet( Code)       _RxSms.Status = (Code)
#define SmsRequestDone()           SmsRequestInvalidSet(); SmsRequestTimeoutClear()

#define SmsRequestCode()          (_RxSms.Status)
#define SmsRequestInvalid()       (_RxSms.Status == BAT2_MEGAVI_SMS_REQUEST_INVALID)
#define SmsRequestActive()        (_RxSms.Timeout)
#define SmsRequestExpired()       ((--_RxSms.Timeout) == 0)


// Local functions :

static void SmsDone( byte Status);
// Set SMS <Status> at active slot, remove SMS from queue

#define PhoneClear()                _ActivePhone = 0;
#define PhoneActive()               Config.Phone[ _ActivePhone]

static TYesNo PhoneNext( void);
// Move to next active phone number

static TYesNo PhoneInvalid( void);
// Check for phonebook integrity

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void SmsInit( void)
// Module init
{
byte  i;
TSms *Sms;

   // clear FIFO :
   for( i = 0; i < MBRIDGE_SMS_SLOT_COUNT; i++){
      Sms = &_Sms[ i];
      memset( Sms, 0, sizeof( TSms));
      Sms->Status = BAT2_MEGAVI_SMS_STATUS_EMPTY;
   }
   SmsFifoInit();
   // clear SMS data :
   SmsDeliverStop();
   PhoneClear();
   // clear SMS request data :
   memset( &_RxSms, 0, sizeof( TRxSms));
   SmsRequestInvalidSet();
} // SmsInit

//-----------------------------------------------------------------------------
// Timer
//-----------------------------------------------------------------------------

void SmsTimer( void)
// Module 1s timer
{
   // check for SMS request timeout :
   if( SmsRequestActive()){
      if( SmsRequestExpired()){
         SmsRequestDone();             // set ready for next request
      }
   }
   // check for SMS deliver timeout :
   if( SmsDeliverActive()){
      if( SmsDeliverExpired()){
         SmsDeliverStop();             // stop timeout
         SmsDone( BAT2_MEGAVI_SMS_STATUS_ERROR);
      }
   }
} // SmsTimer

//-----------------------------------------------------------------------------
// Insert
//-----------------------------------------------------------------------------

byte SmsInsert( char Target, char *Text)
// Insert SMS <Text> by <Target> into processing, returns Slot
{
TSms *ActiveSms;
byte  Slot;

   // check for slot status :
   if( SmsFifoFull()){
      return( BAT2_MEGAVI_SMS_SLOT_ERROR);
   }
   if( Target != BAT2_MEGAVI_SMS_TARGET_BROADCAST){
      if( Target != SmsRequestCode()){
         // wrong SMS reply code
         return( BAT2_MEGAVI_SMS_TARGET_ERROR);
      }
      SmsRequestDone();                // confirm request
   } else if( PhoneInvalid()){
      return( BAT2_MEGAVI_SMS_PHONEBOOK_ERROR);
   }
   // get empty slot :
   Slot      = SmsFifoWriteSlot();
   ActiveSms = &_Sms[ Slot];
   // save message :
   ActiveSms->Status = BAT2_MEGAVI_SMS_STATUS_PENDING;
   ActiveSms->Target = Target;
   memcpy( &ActiveSms->Text, Text, BAT2_SMS_SIZE);
   ActiveSms->Text[ BAT2_SMS_SIZE] = '\0';
   memset( &ActiveSms->Phone, 0, BAT2_SMS_PHONE_NUMBER_SIZE + 1);
   if( Target == BAT2_MEGAVI_SMS_TARGET_BROADCAST){
      SmsDeliverBroadcastStart();      // start broadcast timeout
      return( Slot);                   // no phone number for broadcast
   }
   // save reply phone number :
   memcpy( ActiveSms->Phone, _RxSms.Phone, BAT2_SMS_PHONE_NUMBER_SIZE);
   ActiveSms->Phone[ BAT2_SMS_PHONE_NUMBER_SIZE] = '\0';
   SmsDeliverStart();                  // start standard timeout
   return( Slot);
} // SmsInsert

//-----------------------------------------------------------------------------
// Status
//-----------------------------------------------------------------------------

byte SmsStatus( byte Slot)
// Returns SMS <Slot> status
{
   if( Slot > MBRIDGE_SMS_SLOT_COUNT - 1){
      return( BAT2_MEGAVI_SMS_STATUS_UNDEFINED);
   }
   return( _Sms[ Slot].Status);
} // SmsStatus

//-----------------------------------------------------------------------------
// Count
//-----------------------------------------------------------------------------

byte SmsCount( void)
// Returns SMS count ready for send
{
   return( SmsFifoCount());
} // SmsCount

//-----------------------------------------------------------------------------
// Get phone
//-----------------------------------------------------------------------------

byte SmsPhoneGet( char *Phone)
// Get <Phone> for send, returns Slot
{
byte Slot;

   if( SmsFifoEmpty()){
      return( BAT2_MEGAVI_SMS_SLOT_ERROR);
   }
   Slot = SmsFifoReadSlot();
   if( _Sms[ Slot].Target != BAT2_MEGAVI_SMS_TARGET_BROADCAST){
      // single reply
      memcpy( Phone, _Sms[ Slot].Phone, BAT2_SMS_PHONE_NUMBER_SIZE);
      return( Slot);
   }
   // broadcast reply :
   memcpy( Phone, PhoneActive(), BAT2_SMS_PHONE_NUMBER_SIZE);
   return( Slot);
} // SmsPhoneGet

//-----------------------------------------------------------------------------
// Get text
//-----------------------------------------------------------------------------

byte SmsTextGet( char *Text)
// Get <Text> for send, returns Slot
{
byte Slot;

   if( SmsFifoEmpty()){
      return( BAT2_MEGAVI_SMS_SLOT_ERROR);
   }
   Slot = SmsFifoReadSlot();
   memcpy( Text, _Sms[ Slot].Text, BAT2_SMS_SIZE);
   return( Slot);
} // SmsTextGet

//-----------------------------------------------------------------------------
// SMS status set
//-----------------------------------------------------------------------------

void SmsStatusSet( byte Slot, byte Status)
// Set SMS <Status> at <Slot>
{
byte ActiveSlot;

   ActiveSlot = SmsFifoReadSlot();
   if( ActiveSlot != Slot){
      return;                          // slot already inactive
   }
   // check for delivered :
   if( Status != BAT2_MEGAVI_SMS_STATUS_SENDED){
      return;                          // unable deliver, try again
   }
   // succesfully delivered :
   if( _Sms[ Slot].Target != BAT2_MEGAVI_SMS_TARGET_BROADCAST){
      // single reply
      SmsDone( Status);
      return;
   }
   // broadcast reply :
   if( !PhoneNext()){                  // move to next phone
      SmsDone( Status);                // delivered to all phones
      return;
   }
   // activate next phone number
} // SmsStatusSet

//-----------------------------------------------------------------------------
// Rx SMS Phone
//-----------------------------------------------------------------------------

TYesNo SmsPhoneSet( char *Phone)
// Set received SMS <Phone>
{
   // check for pending request :
   if( SmsRequestActive()){
      return( NO);                     // previous request pending
   }
   // clear previous request :
   SmsRequestDone();
   memcpy( _RxSms.Phone, Phone, BAT2_SMS_PHONE_NUMBER_SIZE);
   _RxSms.Phone[ BAT2_SMS_PHONE_NUMBER_SIZE] = '\0';
   return( YES);
} // SmsPhoneSet

//-----------------------------------------------------------------------------
// Rx SMS Text
//-----------------------------------------------------------------------------

TYesNo SmsTextSet( char *Text)
// Set received SMS <Text>
{
char *p;


   // check for pending request :
   if( SmsRequestActive()){
      return( NO);                     // previous request pending
   }
   // clear previous request :
   SmsRequestDone();
   memcpy( _RxSms.Text, Text, BAT2_SMS_SIZE);
   _RxSms.Text[ BAT2_SMS_SIZE] = '\0';
   strupr( _RxSms.Text);
   if( !memequ( _RxSms.Text, MBRIDGE_SMS_COMMAND, MBRIDGE_SMS_COMMAND_SIZE)){
      return( YES);                    // SMS accepted, but unknown command
   }
   // check for additional characters :
   p = &_RxSms.Text[ MBRIDGE_SMS_COMMAND_SIZE];
   while( *p){
      if( *p != ' '){
         SmsRequestSet( *p);           // save command code
         SmsRequestTimeoutStart();     // start request service timeout
         return( YES);
      }
      p++;
   }
   // no additional characters :
   SmsRequestShortSet();               // save command code as whitespace only
   SmsRequestTimeoutStart();           // start request service timeout
   return( YES);
} // SmsTextSet

//-----------------------------------------------------------------------------
// SMS Rx status
//-----------------------------------------------------------------------------

byte SmsReceivedStatus( void)
// Returns received SMS status
{
   return( _RxSms.Status);
} // SmsReceivedStatus

//*****************************************************************************

//-----------------------------------------------------------------------------
// Done
//-----------------------------------------------------------------------------

static void SmsDone( byte Status)
// Set SMS <Status> at active slot, remove SMS from queue
{
byte ActiveSlot;

   ActiveSlot = SmsFifoReadSlot();
   _Sms[ ActiveSlot].Status = Status;
   SmsFifoRemove();                    // remove from FIFO
   PhoneClear();                       // clear active phonebook entry
   if( SmsFifoEmpty()){
      SmsDeliverStop();                // stop deliver timeout
      return;
   }
   // lookup Target for deliver timeout :
   ActiveSlot = SmsFifoReadSlot();
   if( _Sms[ ActiveSlot].Target == BAT2_MEGAVI_SMS_TARGET_BROADCAST){
      SmsDeliverBroadcastStart();      // start broadcast timeout
   } else {
      SmsDeliverStart();               // start standard timeout
   }
} // SmsDone

//-----------------------------------------------------------------------------
// Phone next
//-----------------------------------------------------------------------------

static TYesNo PhoneNext( void)
// Move to next active phone number
{
   if( _ActivePhone >= BAT2_PHONEBOOK_SIZE - 1){      
      return( NO);
   }
   _ActivePhone++;
   if( _ActivePhone >= Config.PhoneBookSize){
      _ActivePhone--;
      return( NO);
   }
   return( YES);
} // PhoneNext

//-----------------------------------------------------------------------------
// Phone invalid
//-----------------------------------------------------------------------------

static TYesNo PhoneInvalid( void)
// Check for phonebook integrity
{
   return( Config.PhoneBookSize == 0);
} // PhoneInvalid
