//*****************************************************************************
//
//   MegaviExecute.c  MBridge MEGAVI executive
//   Version 1.0      (c) Veit Electronics
//
//*****************************************************************************

#include "MegaviExecute.h"
#include "System.h"          // Operating system
#include "MBridgeDef.h"      // MBridge application
#include "Megavi.h"          // MEGAVI protocol
#include "Bat2Execute.h"     // Bat2 executive
#include "Sms.h"             // SMS memory
#include <string.h>

static TYesNo ProcessCommand( TMegaviCommand *Command, byte Size);
// Process <Command>

static void ReplyVersion( void);
// Reply version

static void ReplyMaleDataGet( void);
// Reply get male data

static void ReplyFemaleDataGet( void);
// Reply get female data

static void ReplySmsSend( byte Target, char *Text);
// Reply SMS send

static void ReplySmsStatus( byte Slot);
// Reply SMS status for <Slot>

//-----------------------------------------------------------------------------
// Initialization
//-----------------------------------------------------------------------------

void MexInit( void)
// Module init
{
   MegaviInit();
   MegaviRxStart();
} // MexInit

//-----------------------------------------------------------------------------
// Execute
//-----------------------------------------------------------------------------

void MexExecute( void)
// Module executive
{
byte Size;
byte *Data;

   if( !BatxConfigurationValid()){
      StatusLedROn();                  // signalize error
      MegaviRxStart();                 // wrong configuration, restart reception
      return;
   } // else StatusLedROff() by Bat2 Configuration Set command
   if( !MegaviRxReady()){
      return;                          // no incomming data
   }
   StatusLedsOff();
   // check for identification field :
   if( !MBridgeAddressMatch( MegaviRxId())){
      MegaviRxStart();                 // no reply, restart reception
      return;                          // foreign packet or undefined own address
   }
   StatusLedROn();
   Data = MegaviData();
   Size = MegaviRxSize();
   if( !ProcessCommand( (TMegaviCommand *)Data, Size)){
      MegaviRxStart();                 // no reply, restart reception
      return;                          // wrong command
   }
} // MexExecute

//*****************************************************************************

//-----------------------------------------------------------------------------
// Process command
//-----------------------------------------------------------------------------

static TYesNo ProcessCommand( TMegaviCommand *Command, byte Size)
// Process <Command>
{
   switch( Command->Command){
      case BAT2_MEGAVI_COMMAND_VERSION :
         if( Size != sizeof( TBat2MegaviCommand)){
            return( NO);
         }
         ReplyVersion();
         return( YES);

      case BAT2_MEGAVI_COMMAND_MALE_DATA_GET :
         if( Size != sizeof( TBat2MegaviCommand)){
            return( NO);
         }
         ReplyMaleDataGet();
         return( YES);

      case BAT2_MEGAVI_COMMAND_FEMALE_DATA_GET :
         if( Size != sizeof( TBat2MegaviCommand)){
            return( NO);
         }
         ReplyFemaleDataGet();
         return( YES);

      case BAT2_MEGAVI_COMMAND_SMS_SEND :
         if( Size != sizeof( TBat2MegaviCommandSmsSend)){
            return( NO);
         }
         ReplySmsSend( Command->SmsSend.Target, Command->SmsSend.Text);
         return( YES);

      case BAT2_MEGAVI_COMMAND_SMS_STATUS_GET :
         if( Size != sizeof( TBat2MegaviCommandSmsStatusGet)){
            return( NO);
         }
         ReplySmsStatus( Command->SmsStatusGet.Slot);
         return( YES);

      default :
         return( NO);
   }
} // ProcessCommand


//-----------------------------------------------------------------------------
// Version
//-----------------------------------------------------------------------------

static void ReplyVersion( void)
// Reply version
{
TBat2MegaviReplyVersion *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplyVersion *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_VERSION;
   Reply->Minor = MBRIDGE_VERSION_MINOR;
   Reply->Major = MBRIDGE_VERSION_MAJOR;
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplyVersion));
} // ReplyVersion

//-----------------------------------------------------------------------------
// Male data get
//-----------------------------------------------------------------------------

static void ReplyMaleDataGet( void)
// Reply get male data
{
TBat2MegaviReplyData *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplyData *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_MALE_DATA;
   Reply->SmsRequest = SmsReceivedStatus();      // last received SMS code
   memcpy( &Reply->Data, BatxMaleData(), sizeof( TBat2Data));
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplyData));
} // ReplyMaleDataGet

//-----------------------------------------------------------------------------
// Female data get
//-----------------------------------------------------------------------------

static void ReplyFemaleDataGet( void)
// Reply get female data
{
TBat2MegaviReplyData *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplyData *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_FEMALE_DATA;
   Reply->SmsRequest = SmsReceivedStatus();      // last received SMS code
   memcpy( &Reply->Data, BatxFemaleData(), sizeof( TBat2Data));
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplyData));
} // ReplyDataGet

//-----------------------------------------------------------------------------
// SMS send
//-----------------------------------------------------------------------------

static void ReplySmsSend( byte Target, char *Text)
// Reply SMS send
{
TBat2MegaviReplySmsSend *Reply;
byte                     Slot;

   Slot  = SmsInsert( Target, Text);
   // get reply buffer :
   Reply = (TBat2MegaviReplySmsSend *)MegaviData();
   // prepare reply data :
   Reply->Reply = BAT2_MEGAVI_REPLY_SMS_SEND;
   Reply->Slot  = Slot;
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplySmsSend));
} // ReplySmsSend

//-----------------------------------------------------------------------------
// SMS status
//-----------------------------------------------------------------------------

static void ReplySmsStatus( byte Slot)
// Reply SMS status for <Slot>
{
TBat2MegaviReplySmsStatus *Reply;

   // get reply buffer :
   Reply = (TBat2MegaviReplySmsStatus *)MegaviData();
   // prepare reply data :
   Reply->Reply  = BAT2_MEGAVI_REPLY_SMS_STATUS;
   Reply->Status = SmsStatus( Slot);
   // send reply data :
   MegaviTx( MBRIDGE_MEGAVI_ADDRESS, sizeof( TBat2MegaviReplySmsStatus));
} // ReplySmsStatus
