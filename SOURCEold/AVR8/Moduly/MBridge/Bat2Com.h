//*****************************************************************************
//
//    Bat2Com.h     Bat2 bridge protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Bat2Com_H__
   #define __Bat2Com_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __BridgeDf_H__
   #include "BridgeDf.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void BatcInit( void);
// Communication initialisation

void BatcRxStart( void);
// Start reception

TYesNo BatcRxReady( void);
// Returns YES, on Rx data ready

byte BatcRxSize( void);
// Returns received DATA bytes count

void *BatcRxData( void);
// Returns Rx data buffer address

void *BatcTxData( void);
// Returns Tx data buffer address

void BatcTx( byte Size);
// Start transmition with <Id> and <Size>

#endif
