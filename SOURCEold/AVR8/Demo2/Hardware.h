//*****************************************************************************
//
//    Hardware.h  - Bat2 bastl hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED   7

#define StatusLedInit()  DDRD  |=  (1 << StatusLED)

#define StatusLedOn()    PORTD |=  (1 << StatusLED) 
#define StatusLedOff()   PORTD &= ~(1 << StatusLED)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

#define UART0_BAUD           38400L     // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM0                    // konzola pro printf na UART0

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

#define CalButton   2

#define CalButtonInit()     DDRD  &= ~(1 << CalButton);\
                            PORTD |=  (1 << CalButton)         // enable pullup

#define CalButtonPressed()  (!(PIND & (1 << CalButton)))       // active L


//-----------------------------------------------------------------------------

#endif
