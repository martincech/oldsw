   Zakladni ukoly v C
   -------------------
   
   
1. Vytvorit adresar C:\SOURCE
2. Do adresare C:\SOURCE rozpakovat AVR2.ZIP
3. Mel by pribyt adresar Demo2

4. V AVR Studio otevrit pripraveny projekt :
   C:\SOURCE\AVR8\Demo2\Bat2.aps

5. Program demonstruje komunikaci CPU pres UART
   na strane PC musi byt spusten terminal, bastl
   musi byt pripojen na COM1
   
6. Po prelozeni a nahrani program na terminal
   vypise priklady funkce printf (presneji
   receno je pouzita funkce cprintf, ktera
   posila data na UART). Program ceka na znak
   z terminalu. Po stisknuti klavesy v PC
   CPU znak vrati do terminalu a provede akci.
   
Pozn. komentare uvozene //>>> jsou na vysvetleni nejakeho jevu,
v normalnim zdrojovem textu by nebyly
-----------------------------------------------------------------------   
   
   
   
   Uvod do firemnich standardu
   ----------------------------
   
Adresarova struktura je :

c:\SOURCE\AVR8\<Projekt>
              \Inc
              \Moduly\
              \Moduly\CPU
              \Moduly\LIB			  
              \Moduly\Ad7192

Novy projekt ma vzdy svuj adresar na urovni <Projekt>.
V adresari Inc jsou hlavickove soubory parametrickych
modulu.
V adresari Moduly je podstrom s zdrojovymi texty
parametrickych modulu :
- CPU jsou vnitrni moduly CPU (ADC, UART, SPI, I2C)
- LIB jsou pomocne knihovni funkce
- kazdy externi obvod ma svuj adresar (zde treba Ad7192)

V adresari <Projekt> je soubor Hardware.h, ktery popisuje
schema zapojeni a popr. dalsi parametry pouzitych modulu.

Soubor Hardware.h
------------------

definuje makra na ovladani pinu procesoru.
Pri zmene zapojeni (prepojeni zarizeni na jiny
pin, vlozeni inverze, se zmeni jen definice
makra, zdrojovy text modulu je beze zmeny.
Takto je mozne prejit z bastlu na finalni desku
jen preeditovanim souboru Hardware.h

Definice portu
--------------

Vzhledem k tomu, ze jazyk C neumi v makru zjistit
cislo bitu z masky, pin se definuje postupne jako
-----------------------------------------------------------

(Predpokladame zarizeni pripojene na PB3 = vystup)

#define NAZEV_PINU_VE_SCHEMATU   3      // cislo bitu na prislusnem portu

// vyraz (1 << NAZEV_PINU_VE_SCHEMATU)   znamena jednicku shiftuj o 3 mista doleva : maska pinu na portu je 0x08

// nastaveni bitu na portu je

#define NazevModuluPinSet()      PORTB |=  (1 << NAZEV_PINU_VE_SCHEMATU) // tj. PORTB = PORTB <or> 0x08
#define NazevModuluPinClr()      PORTB &= ~(1 << NAZEV_PINU_VE_SCHEMATU) // tj. PORTB = PORTB <and> <bitova negace>0x08

tyto nepekne vyrazy se prekvapive prelozi do jedne instrukce strojoveho kodu

zbyva nastavit smer portu

#define NazevModuluPortInit()    DDRB  |= (1 << NAZEV_PINU_VE_SCHEMATU)  // 1 v masce = vystupni port

-----------------------------------------------------------
(Predpokladame zarizeni pripojene na PB5 = vstup)

#define NAZEV_PINU_VE_SCHEMATU   5      // cislo bitu na prislusnem portu

// cteni bitu z portu je :

#define NazevModuluPinGet()      (PINB & (1 << NAZEV_PINU_VE_SCHEMATU))   // tj. PINB <or> 0x08, vyraz je nenulovy, je-li pin H

nebo pro vstup aktivni v L :

#define NazevModuluPinGet()     (!(PINB & (1 << NAZEV_PINU_VE_SCHEMATU))) // tj. (logicka inverze)(PINB <or> 0x08), vyraz je nenulovy, je-li pin L


zbyva nastavit smer portu

#define NazevModuluPortInit()    DDRB  &= ~(1 << NAZEV_PINU_VE_SCHEMATU)  // 0 v masce = vstupni port

S makry se ve zdrojovem textu dale pracuje jako s funkcemi (viz Demo1)

Pozn : Protoze dioda LED a tlacitko nema zadnou funkci na uvodni inicializaci,
Je misto NazevModuluPortInit() pouzito makro NazevModuluInit().
Makro NazevModuluPortInit() je u normalnich modulu ukryto
ve funkci NazevModuluInit() 

-----------------------------------------------------------

Nejcasteji pouzivane knihovni funkce :

NazevModuluInit()      - inicializace (napr. StdioInit() pro inicializaci cprintf, AdcInit() pro inicializaci AD prevodniku...

z hlavickoveho souboru System.h, resp. Cpu.h

SysDelay( ms)          - zpozdovaci smycka v [ms] (skoro presne)
SysUDelay( ms)         - zpozdovaci smucka v [us] (jen priblizne)
_NOP()                 - vlozeni instrukce NOP

z hlavickoveho souboru Stdio.h

cprintf( format, seznam promennych podle formatu)  viz printf ale vystup na UART

z hlavickoveho souboru Uart0.h

Uart0RxChar( &ch)       - prijem znaku z UART s timeoutem viz Hardware.h
Uart0TxChar( ch)        - vyslani znaku na UART
Uart0RxWait( ms)        - cekani na prijem znaku [ms] je-li ms = 0 vrati se okamzite, pokud neni znak v bufferu prijimace

hlavickovy soubor Ad7192.h a zdrojovy text Ad7192.c je priklad
realizace obsluhy externiho obvodu. Pro vysvetleni souboru je
treba vyjasnit zakladni strategii programovani modulu :

- zdrojovy text se pise jen jednou
- ale cte se mnohokrat
- zdrojovy text by mel byt citelny i bez datasheetu soucastky
- zdrojovy text by mel byt citelny i nekolik let po napsani
  (nekdy se komentare doplnuji pri nasledujicim cteni souboru)
- pouziti bajecnych triku, ktere usetri 3 instrukce,
  zpusobi nasledujici necitelnost a neudrzovatelnost programu.
  Pri kazdem cteni se clovek zamysli, jakze to vlastne funguje

Na testovani obvodu bude stacit prevzit zakladni ideu :
- napsat funkce na zapis/cteni registru zarizeni
  (nebo pouzit knihovni modul pro I2C, SPI, UART apod.)
- registry je mozne plnit hexa konstantami, ale
  stejne bude vhodne doplnit komentar, co se vlastne nastavuje
- definovat funkce, ktere odpovidaji funkcim cipu, napr.
  AdcInit() - inicializace
  AdcStart() - start prevodu
  AdcReadData() - precteni zmerene hodnoty
  AdcCalibrate() - kalibrace
  AdcStop() - zastaveni prevodu

Na procviceni je v adresari Bat2 projekt pro bastl AD prevodniku.
  