//*****************************************************************************
//
//    main.c  - Bat2 main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "Uart0.h"
#include "Cstdio.h"

// prototypy lokalnich funkci

//>>> prototyp funkce musi byt uveden pred jejim pouzitim
//>>> je to kopie hlavicky funkce ukoncena strednikem
//>>> klicove slovo static znaci, ze funkce je pouzita
//>>> jen v tomto zdrojovem textu

static void PrintfDemo( void);       //>>> procedura bez parametru, nevraci nic
// Priklady formatu printf

static int16 FunctionDemo( int8 x);  //>>> funkce s jednim parametrem, vraci int16
// Demostracni funkce

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte  ch;   //>>> lokalni promenne funkce main
int16 y;

   // inicializace portu a zarizeni :
   StatusLedInit();
   CalButtonInit();
   Uart0Init();                        // inicializace UART
   StdioInit();                        // inicializace funkce cprintf

   // bliknuti LED na uvod :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);

   // vyslani kontrolnich znaku na UART0 :
   Uart0TxChar( 'A');                  // vyslani bytu na UART0
   Uart0TxChar( 'B');
   Uart0TxChar( '\r');                 // CR
   Uart0TxChar( '\n');                 // LF

   // demonstracni priklady pouziti printf :
   PrintfDemo();

   // hlavni smycka :
   forever {
      // po stisknuti tlacitka poslat text :
      if( CalButtonPressed()){
         SysDelay( 50);                // fitrace zakmitu
         if( CalButtonPressed()){
            cprintf( "Button\n");      // opravdu stisknut
         } // else jen zakmit
      } // else neni stisknuto

      // prijmout znak z UART0
      if( !Uart0RxChar( &ch)){
         continue;                     // zadny znak nebyl prijat skok na zacatek cyklu (forever)
      }
      Uart0TxChar( ch);                // echo prijateho znaku - zpatky do terminalu

      //>>> prikaz switch :
      switch( ch){
         //>>> sem skoci po prijeti pismene z :
         case 'z' :
            // zhasni LED
            StatusLedOff();
            break;  // skoci za konec prikazu switch

         //>>> sem skoci po prijeti pismene r :
         case 'r' :
            // rozsvit LED
            StatusLedOn();
            break;

         case 'f' :
            // spocitej funkci pro x = 3
            y = FunctionDemo( 3);
            cprintf( "Funkce pro x = 3  : %d\n", y);

            y = FunctionDemo( -3);
            cprintf( "Funkce pro x = -3 : %d\n", y);

            //>>> lze i takto, ale vysledek nelze zobrazit v debuggeru :
//          cprintf( "Funkce pro x =  3 : %d\n", FunctionDemo( 3)); 
//          cprintf( "Funkce pro x = -3 : %d\n", FunctionDemo( -3)); 

            break;

         //>>> jeden blok muze chytat i vice hodnot :
         case '\r' :  // CR
         case '\n' :  // LF
            break;  // ignoruj <CR> a <LF>

         //>>> vsechny zbyvajici moznosti konci zde :
         default :
            cprintf( "\nz - zhasni LED\n");   // napred odradkuj a potom napis 'z - zhasni LED'
            cprintf( "r - rozsvit LED\n");
            cprintf( "f - funkce\n");
            break;
      } // switch
      //>>> sem odskakuji bloky po provedeni break v bloku case
   }
   return( 0);
} // main

//-----------------------------------------------------------------------------
// Printf demo
//-----------------------------------------------------------------------------

// demonstracni retezec :
static char  s[ 3] = "OK";  // retezec 2 znaky + ukonceni znakem '\0'

//>>> POZOR promenna typu pole by nemela byt deklarovana jako
//>>> lokalni, protoze muze dojit k preteceni zasobniku
//>>> zde je umistena do datoveho segmentu procesoru
//>>> klicove slovo static "zneviditelni" promennou pro ostatni
//>>> zdrojove texty

static void PrintfDemo( void)      //>>> za definici funkce neni strednik
// Priklady formatu printf
{
byte  b;        // 8 bitu bez znamenka
word  w;        // 16 bitu bez znamenka
dword d;        // 32 bitu bez znamenka
int8  i8;       // 8 bitu se znamenkem
int16 i16;      // 16 bitu se znamenkem
int32 i32;      // 32 bitu se znamenkem
char  ch;       // 1 ascii znak (byte)

byte  i;        // pomocna promenna cyklu


//>>> Pozn. v AVR GCC kompilatoru je typ 'int' 16 bitu se znamenkem
//>>> je lepsi pouzivat typy s explicitni delkou, aby bylo zrejme
//>>> jak je promenna dlouha

   // byte :
   b = 0xAB;                           // hexa konstanta
   cprintf( "Byte : %02X\n", b);       // tisk bytu na dve cislice v hexa
   cprintf( "Byte : %u\n",   b);       // tisk bytu dekadicky (bez znamenka)

   // word
   w = 0xABCD;                         // hexa konstanta
   cprintf( "Word : %04X\n", w);       // tisk slova na ctyri cislice hexa
   cprintf( "Word : %u\n",   w);       // tisk slova dekadicky (bez znamenka)

   // dword :
   d = 0xABCD4321UL;                   // hexa konstanta unsigned long
   cprintf( "DWord : %08lX\n", d);     // tisk dvojslova na osm cislic hexa
   cprintf( "DWord : %lu\n",   d);     // tisk dvojslova dekadicky (bez znamenka)

   // int8 :
   i8 = 0xFF;                          // hexa konstanta, ve skutecnosti -1
   cprintf( "Int8 : %02X\n", i8);      // tisk znamenkoveho bytu na dve cislice hexa
   cprintf( "Int8 : %d\n",   i8);      // tisk znamenkoveho bytu dekadicky

   // int16 :
   i16 = 0xFFFE;                       // hexa konstanta, ve skutecnosti -2
   cprintf( "Int16 : %02X\n", i16);    // tisk znamenkoveho slova na ctyri cislice hexa
   cprintf( "Int16 : %d\n",   i16);    // tisk znamenkoveho slova dekadicky

   // int32 :
   i32 = 0xFFFFFFFD;                   // hexa konstanta, ve skutecnosti -3
   cprintf( "Int32 : %02lX\n", i32);   // tisk znamenkoveho slova na ctyri cislice hexa
   cprintf( "Int32 : %ld\n",   i32);   // tisk znamenkoveho slova dekadicky

   // pismeno :
   ch = 'Y';
   cprintf( "Char : %c\n", ch);

   // retezec :
   cprintf( "Str : %s\n", s);

   // vypis pole :
   for( i = 0; i < 3; i++){
      cprintf( "s[%d] : %02X\n", i, s[ i]); // prvni %d vypise index i, %02X vypise znak jako hexa byte
   }
} // PrintfDemo

//-----------------------------------------------------------------------------
// Function demo
//-----------------------------------------------------------------------------

static int16 FunctionDemo( int8 x)
// Demonstracni funkce
{
   return( (int16)x * (int16)x);   // prevede x na 16 bitu se znamenkem, vrati x^2
} // FunctionDemo
