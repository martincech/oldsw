//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "CStdio.h"
#include "Uart1.h"
#include "SpiSlave.h"

//#define TX_SIZE  128
#define TX_SIZE  4

static void DumpData( byte *Data, byte Size);
// Dump data to UART

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
//byte Char;
byte *Data;
byte Size;

   // initialize devices :
   StatusLedInit();
   Uart1Init();
   StdioInit();
   SpiInit();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   // send data on start :
   cprintf( " Start\n");
   // fill Tx Buffer :
   Data = SpiTxData();
   for( Size = 0; Size < TX_SIZE; Size++){
      *Data = Size;
      Data++;
   }
   SpiTx( TX_SIZE);
   SpiRxStart();
   EnableInts();
   // main loop
   forever {
      if( SpiRxReady()){
         Size = SpiRxSize();
         Data = SpiRxData();
         DumpData( Data, Size);
         SpiRxStart();
      }
   }
   return( 0);
} // main


//-----------------------------------------------------------------------------
// Dump
//-----------------------------------------------------------------------------

static void DumpData( byte *Data, byte Size)
// Dump data to UART
{
   cprintf( "Data size : %02d\n", Size);
   do {
      Uart1TxChar( *Data);
      Data++;
   } while( --Size);
   cprintf( "\nEND\n");
} // DumpData
