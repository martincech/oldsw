//*****************************************************************************
//
//    main.c       MBridge main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"
#include "Uart0.h"

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Char;

   // initialize devices :
   StatusLedInit();
   Uart0Init();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   // send data on start :
   Uart0TxChar( 'A');
   Uart0TxChar( 'B');
   Uart0TxChar( 'C');
   // main loop
   forever {
      if( !Uart0RxChar( &Char)){
         continue;                     // timeout
      }
      Uart0TxChar( Char);              // echo character
   }
   return( 0);
} // main
