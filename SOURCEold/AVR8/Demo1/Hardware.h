//*****************************************************************************
//
//    Hardware.h  - Bat2 bastl hardware descriptions
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

//>>> prevence proti vicenasobnemu include
#ifndef __Hardware_H__
   #define __Hardware_H__

//>>> systemove definice (vzdy opsat)
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"
//<<<


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L             // kmitocet krystalu v Hz

//-----------------------------------------------------------------------------
// Device parameters
//-----------------------------------------------------------------------------

//>>> tady by byly pripadne definice souvisejici s deskou
//>>> zapojeni vyvodu, kapacity pameti...

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

//>>> definice vystupniho portu pro ovladani LED
//>>> LED je pripojena na PD7

#define StatusLED   7             //>>> cislo vyvodu portu

#define StatusLedInit()  DDRD  |=  (1 << StatusLED)          //>>> vystupni smer

#define StatusLedOn()    PORTD |=  (1 << StatusLED)          //>>> aktivni H - zapnuto
#define StatusLedOff()   PORTD &= ~(1 << StatusLED)          //>>> shodit do L - vypnuto

//-----------------------------------------------------------------------------
// Calibration button
//-----------------------------------------------------------------------------

//>>> definice vstupniho portu pro kalibracni tlacitko 
//>>> tlacitko je na portu PD2

#define CalButton   2             //>>> cislo bitu

//>>> vstupni smer  + aktivace pullup
#define CalButtonInit()     DDRD  &= ~(1 << CalButton);\
                            PORTD |=  (1 << CalButton)

#define CalButtonPressed()  (!(PIND & (1 << CalButton)))       //>>> aktivni L


//-----------------------------------------------------------------------------

#endif // __Hardware_H__
