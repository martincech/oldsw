//*****************************************************************************
//
//    main.c  - Bat2 bastl main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte i;           // promenna cyklu (max. 255)

   // inicializace portu :
   StatusLedInit();                    // nastaveni smeru portu pro LED
   StatusLedOff();                     // zhasnuti LED
   CalButtonInit();                    // nastaveni smeru portu pro Kalibracni tlacitko

   // 5x blikni LED 
   //>>> Pozn. : (tuto variantu cykly prekladac lepe optimalizuje,
   //>>> pokud neni potreba sledovat i je lepe pouzit tuto konstrukci :
   i = 5;                              // pocet cyklu
   do {
      // i = 5..1
      StatusLedOn();                   // rozsviceni LED
      SysDelay( 200);                  // cekani 200ms
      StatusLedOff();                  // zhasnuti LED
      SysDelay( 200);                  // cekani 200ms
   } while( --i);
/*
//>>> Pozn. : pokud potrebujeme index i = 0..4 (napr. pro indexovani pole),
//>>> muzeme pouzit tuto variantu :
   for( i = 0; i < 5; i++){
      // i = 0..4
      StatusLedOn();                   // rozsviceni LED
      SysDelay( 200);                  // cekani 200ms
      StatusLedOff();                  // zhasnuti LED
      SysDelay( 200);                  // cekani 200ms
   }
*/

   // rozsvecuje LED pri stisknuti tlacitka
   forever {                           // nekonecny cyklus
      if( CalButtonPressed()){
         StatusLedOn();                // rozsviceni LED
      } else {
         StatusLedOff();               // zhasnuti LED
      }
   }
   return( 0);                         // sem to nikdy nedojde - jen odstraneni warningu kompilatoru
} // main
