//*****************************************************************************
//
//    Hardware.h   MBridge hardware descriptions (STK500)
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED   7

#define StatusLedInit()  DDRD  |=  (1 << StatusLED)

#define StatusLedOn()    PORTD |=  (1 << StatusLED) 
#define StatusLedOff()   PORTD &= ~(1 << StatusLED)

#define StatusLed1On()   PORTD |=  (1 << StatusLED)
#define StatusLed1Off()  PORTD &= ~(1 << StatusLED)

#define StatusLed2On()   PORTD |=  (1 << StatusLED)
#define StatusLed2Off()  PORTD &= ~(1 << StatusLED)

#define StatusLed3On()   PORTD |=  (1 << StatusLED)
#define StatusLed3Off()  PORTD &= ~(1 << StatusLED)

//------------------------------------------------------------------------------
// UART0
//------------------------------------------------------------------------------

// PD0 - RXD, PD1 - TXD

#define UART0_BAUD           9600       // baud rate
#define UART0_RX_TIMEOUT     10000      // intercharacter timeout [us]

#define PUTCHAR_COM0

//------------------------------------------------------------------------------
// UART1
//------------------------------------------------------------------------------

// PD2 - RXD, PD3 - TXD

#define UART1_BAUD           9600       // baud rate
#define UART1_RX_TIMEOUT     10000      // intercharacter timeout [us]

//------------------------------------------------------------------------------
// MEGAVI
//------------------------------------------------------------------------------

// PD0 - RXD, PD1 - TXD

#define MEGAVI_COM1

#define MEGAVI_BAUD          19200      // baud rate
#define MEGAVI_BUFFER_SIZE   250        // maximum packet size
#define MEGAVI_TX_SPACE      2          // wait before Tx [ms] (must be less than Bat2 MBRIDGE_DELAY)

// RS485 Tx control :
#define MEGAVI_RS485_DE      4          // RS485 direction control

#define MegaviTxInit()       DDRD  |=  (1 << MEGAVI_RS485_DE)
#define MegaviTxEnable()     PORTD |=  (1 << MEGAVI_RS485_DE)
#define MegaviTxDisable()    PORTD &= ~(1 << MEGAVI_RS485_DE)

//------------------------------------------------------------------------------
// SPI Slave
//------------------------------------------------------------------------------

// PB4 - SS, PB7 - SCK, PB5 - MOSI, PB6 - MISO

#define SPI_SS   4
#define SPI_MOSI 5
#define SPI_MISO 6
#define SPI_SCK  7


#define SpiPortInit()    DDRB &= ~((1 << SPI_SS) | (1 << SPI_SCK) | (1 << SPI_MOSI));\
                         DDRB |=   (1 << SPI_MISO)


#define SpiSsGet()       (PINB & (1 << SPI_SS))

#define SPI_CLOCK        2000000L         // SPI clock [Hz]
#define SPI_CPOL         1                // idle clock polarity
#define SPI_CPHA         1                // clock phase leading/trailing edge

#define SPI_RX_BUFFER_SIZE  200
#define SPI_TX_BUFFER_SIZE  200

//-----------------------------------------------------------------------------

#endif
