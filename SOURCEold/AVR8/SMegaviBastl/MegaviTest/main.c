//*****************************************************************************
//
//    main.c       SMegavi main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "Uart0.h"           // USART1 - stdio
#include "CStdio.h"          // Console output
#include "Megavi.h"          // MEGAVI protocol


#define TEST_DATA_SIZE   10

static void DumpData( byte *Data, byte Size);
// Dump data to UART

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Size;
byte *Data;
byte Id;

   // initialize devices :
   StatusLedInit();
   Uart0Init();
   StdioInit();
   MegaviInit();
   MegaviRxStart();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   cprintf( "Start\n");
   EnableInts();
  // main loop
   forever {
      if( !MegaviRxReady()){
         continue;
      }
      StatusLedOn();
      Data = MegaviData();
      Size = MegaviRxSize();
      Id   = MegaviRxId();
      DumpData( Data, Size);
      MegaviTx( Id, Size);
      while( !MegaviTxDone());
      StatusLedOff();
   }
   return( 0);
} // main

//-----------------------------------------------------------------------------
// Dump
//-----------------------------------------------------------------------------

static void DumpData( byte *Data, byte Size)
// Dump data to UART
{
   cprintf( "Data size : %02d\n", Size);
   do {
      Uart0TxChar( *Data);
      Data++;
   } while( --Size);
   cprintf( "\nEND\n");
} // DumpData
