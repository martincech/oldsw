//*****************************************************************************
//
//    SMegavi.h     SMegavi protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __SMegavi_H__
   #define __SMegavi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __SMegaviDef_H__
   #include "SMegaviDef.h"
#endif

//-----------------------------------------------------------------------------
// Functions
//-----------------------------------------------------------------------------

void SMegaviInit( void);
// Communication initialisation

void SMegaviRxStart( void);
// Start reception

TYesNo SMegaviRxReady( void);
// Returns YES, on Rx data ready

byte SMegaviRxSize( void);
// Returns received DATA bytes count

byte SMegaviRxId( void);
// Returns received ID field

void *SMegaviData( void);
// Returns data buffer address

void SMegaviTx( byte Id, byte Size);
// Start transmition with <Id> and <Size>

TYesNo SMegaviTxDone( void);
// Wait for transmission done

#endif
