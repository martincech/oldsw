//*****************************************************************************
//
//    Hardware.h   MBridge hardware descriptions (STK500)
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "../Inc/cpu.h"
#include "../Inc/uni.h"


//-----------------------------------------------------------------------------
// CPU parameters
//-----------------------------------------------------------------------------

// Crystal 8 MHz
#define FXTAL 8000000L

//-----------------------------------------------------------------------------
// Status LED
//-----------------------------------------------------------------------------

#define StatusLED   7

#define StatusLedInit()  DDRD  |=  (1 << StatusLED)

#define StatusLedOn()    PORTD |=  (1 << StatusLED) 
#define StatusLedOff()   PORTD &= ~(1 << StatusLED)

//------------------------------------------------------------------------------
// MEGAVI
//------------------------------------------------------------------------------

// PD2 - RXD, PD3 - TXD

#define MEGAVI_COM1

#define MEGAVI_BAUD          19200      // baud rate
#define MEGAVI_BUFFER_SIZE   250        // maximum packet size
#define MEGAVI_TX_SPACE      2          // wait before Tx [ms] (must be less than Bat2 MBRIDGE_DELAY)

// RS485 Tx control :
#define MEGAVI_RS485_DE      4          // RS485 direction control

#define MegaviTxInit()       DDRD  |=  (1 << MEGAVI_RS485_DE)
#define MegaviTxEnable()     PORTD |=  (1 << MEGAVI_RS485_DE)
#define MegaviTxDisable()    PORTD &= ~(1 << MEGAVI_RS485_DE)

//------------------------------------------------------------------------------
// SMEGAVI
//------------------------------------------------------------------------------

// PD0 - RXD, PD1 - TXD

#define SMEGAVI_COM0
#define SMEGAVI_BAUD          9600       // baud rate
#define SMEGAVI_BUFFER_SIZE   254        // maximum packet size

//-----------------------------------------------------------------------------

#endif
