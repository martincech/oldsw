//*****************************************************************************
//
//    main.c       SMegavi main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "SMegavi.h"         // SMegavi protocol
#include "Megavi.h"          // MEGAVI protocol
#include <string.h>

#define SMEGAVI_MS          200L
#define SMEGAVI_TIMEOUT    (1000 * SMEGAVI_MS)  // MEGAVI reply delay [ms]

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Size;
byte *Data;
byte *TxData;
byte TxSize;
byte Id;
dword Timeout;

   // initialize devices :
   StatusLedInit();
   MegaviInit();
   SMegaviInit();
   SMegaviRxStart();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   EnableInts();
   // main loop
   forever {
      if( !SMegaviRxReady()){
         continue;
      }
      StatusLedOn();
      Data   = SMegaviData();
      Size   = SMegaviRxSize();
      Id     = SMegaviRxId();
      // send as MEGAVI packet :
      TxData = MegaviData();           // get output buffer
      TxSize = Size;
      memcpy( TxData, Data, TxSize);
      MegaviTx( Id, TxSize);
      while( !MegaviTxDone());         // starts Rx too
      // wait for MEGAVI reply :
      Timeout = SMEGAVI_TIMEOUT;
      do {
         if( !MegaviRxReady()){
            continue;
         }
         StatusLedOn();
         TxData = MegaviData();
         TxSize = MegaviRxSize();
         Id     = MegaviRxId();
         // send Rx MEGAVI packet :
         Size = TxSize;
         Data = SMegaviData();
         memcpy( Data, TxData, TxSize);
         SMegaviTx( Id, Size);
         while( !SMegaviTxDone());
         StatusLedOff();
         break;
      } while( --Timeout);
      MegaviStop();
      SMegaviRxStart();
      StatusLedOff();
   }
   return( 0);
} // main
