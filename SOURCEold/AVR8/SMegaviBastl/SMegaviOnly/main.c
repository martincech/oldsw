//*****************************************************************************
//
//    main.c       SMegavi main
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "System.h"          // Operating system
#include "SMegavi.h"         // SMegavi protocol
//#include "Megavi.h"          // MEGAVI protocol
#include "Uart1.h"           // USART1 - stdio
#include "CStdio.h"          // Console output
#include <string.h>

static void DumpData( byte *Data, byte Size);
// Dump data to UART

//-----------------------------------------------------------------------------
// main
//-----------------------------------------------------------------------------

int main( void)
{
byte Size;
byte *Data;
//byte *TxData;
//byte TxSize;
byte Id;

   // initialize devices :
   StatusLedInit();
   Uart1Init();
   StdioInit();
//   MegaviInit();
   SMegaviInit();
   SMegaviRxStart();
   // flash on start :
   StatusLedOn();
   SysDelay( 500);
   StatusLedOff();
   SysDelay( 500);
   cprintf( "Start");
   EnableInts();
   // main loop
   forever {
      if( !SMegaviRxReady()){
         continue;
      }
      StatusLedOn();
      Data   = SMegaviData();
      Size   = SMegaviRxSize();
      Id     = SMegaviRxId();
      DumpData( Data - 3, Size + 4);
      // send reply :
      SMegaviTx( Id, Size);
      while( !SMegaviTxDone());
      StatusLedOff();
      SMegaviRxStart();
/*
      // send as MEGAVI packet :
      TxData = MegaviData();           // get output buffer
      TxSize = Size - 1;               // data without Id
      Id = Data[ 0];
      memcpy( TxData, &Data[ 1], TxSize);
      MegaviTx( Id, TxSize);
      while( !MegaviTxDone());
      // wait for MEGAVI reply :
      MegaviRxStart();
      StatusLedOff();
      forever {
         if( !MegaviRxReady()){
            continue;
         }
         StatusLedOn();
         TxData = MegaviData();
         TxSize = MegaviRxSize();
         Id     = MegaviRxId();
         // send Rx MEGAVI packet :
         Size = TxSize + 1;
         Data = SMegaviData();
         Data[ 0] = Id;
         memcpy( &Data[ 1], TxData, TxSize);
         SMegaviTx( Size);
         while( !SMegaviTxDone());
         StatusLedOff();
      }
*/
   }
   return( 0);
} // main

//-----------------------------------------------------------------------------
// Dump
//-----------------------------------------------------------------------------

static void DumpData( byte *Data, byte Size)
// Dump data to UART
{
   cprintf( "Data size : %02d\n", Size);
   do {
      Uart1TxChar( *Data);
      Data++;
   } while( --Size);
   cprintf( "\nEND\n");
} // DumpData
