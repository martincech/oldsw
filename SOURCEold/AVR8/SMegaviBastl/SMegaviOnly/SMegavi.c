//*****************************************************************************
//
//    SMegavi.c     SMegavi protocol services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "SMegavi.h"
#include "System.h"          // Operating system

// UART Ports :
#ifndef SMEGAVI_COM1
   // SMEGAVI_COM0
   #define UBRRH UBRR0H
   #define UBRRL UBRR0L
   #define UCSRA UCSR0A
   #define UCSRB UCSR0B
   #define UCSRC UCSR0C
   #define UDR   UDR0

   #define USART_RX_VECTOR       USART0_RX_vect
   #define USART_TX_VECTOR       USART0_UDRE_vect
   #define USART_TX_DONE_VECTOR  USART0_TX_vect
#else // SMEGAVI_COM1
   #define UBRRH UBRR1H
   #define UBRRL UBRR1L
   #define UCSRA UCSR1A
   #define UCSRB UCSR1B
   #define UCSRC UCSR1C
   #define UDR   UDR1

   #define USART_RX_VECTOR       USART1_RX_vect
   #define USART_TX_VECTOR       USART1_UDRE_vect
   #define USART_TX_DONE_VECTOR  USART1_TX_vect
#endif

#define BAUD_DIVISOR ((FXTAL / 16 / SMEGAVI_BAUD) - 1)

// UART Helper macros :
#define ComEnableRxInt()       UCSRB |=  (1 << RXCIE0)
#define ComDisableRxInt()      UCSRB &= ~(1 << RXCIE0)

#define ComEnableTxInt()       UCSRB |=  (1 << UDRIE0)
#define ComDisableTxInt()      UCSRB &= ~(1 << UDRIE0)

#define ComEnableTxDoneInt()   UCSRB |=  (1 << TXCIE0)
#define ComDisableTxDoneInt()  UCSRB &= ~(1 << TXCIE0)
#define ComClearTxDone()       UCSRA &= ~(1 << TXC0)

#define ComDisableInts()       UCSRB &= ~( (1 << RXCIE0) | (1 << TXCIE0) | (1 << UDRIE0))

#define ComRxError( s)         ((s) & ((1 << FE0) | (1 << DOR0) | (1 << UPE0)))

//-----------------------------------------------------------------------------

// Device states :
typedef enum {
   SMEGAVI_STATUS_STOPPED,                 // stopped
   SMEGAVI_STATUS_RX_RUNNING,              // Rx active
   SMEGAVI_STATUS_RX_DONE,                 // Rx done, read data
   SMEGAVI_STATUS_TX_RUNNING,              // Tx active
   _SMEGAVI_STATUS_LAST
} TSMegaviStatus;

#define INDEX_LEADER      0xFF         // index at wait for leader state

// Local variables :
static byte          _Buffer[ SMEGAVI_BUFFER_SIZE];
static byte          _DataSize;        // Rx/Tx data size
static byte          _Index;           // Rx/Tx buffer index
static byte volatile _Status;          // device status

// Local functions :
static byte CalculateCrc( byte Size);
// Calculate <_Buffer> with <Size> CRC

//-----------------------------------------------------------------------------
// Initialisation
//-----------------------------------------------------------------------------

void SMegaviInit( void)
// Communication initialisation
{
   ComDisableInts();
   UBRRH = (byte)(BAUD_DIVISOR >> 8);       // set baud rate divisor
   UBRRL = (byte) BAUD_DIVISOR;
   UCSRB = (1 << RXEN0) | (1 << TXEN0);     // Enable Rx/Tx
   UCSRC = (3 << UCSZ00);                   // 8 bits, 1 stopbit
   _Status   = SMEGAVI_STATUS_STOPPED;      // stop communication
   _DataSize = 0;
} // SMegaviInit

//-----------------------------------------------------------------------------
// Rx start
//-----------------------------------------------------------------------------

void SMegaviRxStart( void)
// Start reception
{
   ComDisableInts();                   // disable interrupts
   _Status   = SMEGAVI_STATUS_RX_RUNNING;  // start of reception
   _Index    = INDEX_LEADER;           // wait for leader
   _DataSize = 0;                      // clear Rx data size
   ComEnableRxInt();                   // enable Rx interrupts
} // SMegaviRxStart

//-----------------------------------------------------------------------------
// Rx ready
//-----------------------------------------------------------------------------

TYesNo SMegaviRxReady( void)
// Returns YES, on Rx data ready
{
   return( _Status == SMEGAVI_STATUS_RX_DONE);
} // SMegaviRxReady

//-----------------------------------------------------------------------------
// Rx size
//-----------------------------------------------------------------------------

byte SMegaviRxSize( void)
// Returns received DATA bytes count
{
   return( _DataSize);
} // SMegaviRxSize

//-----------------------------------------------------------------------------
// Rx Id
//-----------------------------------------------------------------------------

byte SMegaviRxId( void)
// Returns received ID field
{
   return( _Buffer[ SMEGAVI_ID_OFFSET - 1]);
} // SMegaviRxId

//-----------------------------------------------------------------------------
// Data buffer
//-----------------------------------------------------------------------------

void *SMegaviData( void)
// Returns data buffer address
{
   return( &_Buffer[ SMEGAVI_DATA_OFFSET - 1]);   // data offset without leader
} // SMegaviData

//-----------------------------------------------------------------------------
// Tx send
//-----------------------------------------------------------------------------

void SMegaviTx( byte Id, byte Size)
// Start transmition with <Id> and <Size>
{
   ComDisableInts();                                  // disable interrupts
   _Buffer[ SMEGAVI_SIZE1_OFFSET - 1] = Size + 1;     // with Id
   _Buffer[ SMEGAVI_SIZE2_OFFSET - 1] = Size + 1;     // with Id
   _Buffer[ SMEGAVI_ID_OFFSET - 1]    = Id;
   _Buffer[ Size + SMEGAVI_CRC_OFFSET - 1] = CalculateCrc( Size);
   _Status   = SMEGAVI_STATUS_TX_RUNNING;             // start transmission
   _DataSize = Size + SMEGAVI_FRAME_SIZE - 1;         // transmission size without leader
   _Index    = INDEX_LEADER;                          // start with leader character
   ComEnableTxInt();
} // SMegaviTx


//-----------------------------------------------------------------------------
// Tx done
//-----------------------------------------------------------------------------

TYesNo SMegaviTxDone( void)
// Wait for transmission done
{
   return( _Status != SMEGAVI_STATUS_TX_RUNNING);
} // SMegaviTxDone

//*****************************************************************************

//-----------------------------------------------------------------------------
// CRC
//-----------------------------------------------------------------------------

static byte CalculateCrc( byte Size)
// Calculate <_Buffer> with <Size> CRC
{
byte Crc;
byte *Data;

   Data  = &_Buffer[ SMEGAVI_SIZE1_OFFSET - 1]; // Size1 without leader
   Size += 3;                                   // inclusive Size1,2, Id
   Crc   = 0;
   do {
      Crc += *Data;
      Data++;
   } while( --Size);
   return( ~Crc);
} // CalculateCrc

//-----------------------------------------------------------------------------
// Rx done interrupt
//-----------------------------------------------------------------------------

ISR( USART_RX_VECTOR)
{
byte Data;
byte Status;
byte Crc;

   Status = UCSRA;                     // status must be read before UDR
   Data   = UDR;                       // read data register
   // check for framing error :
   if( ComRxError( Status)){
      _Index = INDEX_LEADER;           // wait for leader again
      return;
   }
   // check for leader :
   if( _Index == INDEX_LEADER){
      if( Data == SMEGAVI_LEADER_CHAR){
         _Index = 0;                   // start new data
         return;
      }
      return;                          // skip characters before leader
   }
   // check for size 1 :
   if( _Index == SMEGAVI_SIZE1_OFFSET - 1){
      if( (Data == 0) || (Data >= SMEGAVI_BUFFER_SIZE)){
         _Index = INDEX_LEADER;        // wrong size, wait for leader again
         return;
      }
      _DataSize = Data;
      // insert into buffer
   }
   // check for size 2 :
   if( _Index == SMEGAVI_SIZE2_OFFSET - 1){
      if( Data != _DataSize){
         _Index = INDEX_LEADER;        // different size, wait for leader again
         return;
      }
      // insert into buffer
   }
   // check for buffer overflow :
   if( _Index >= SMEGAVI_BUFFER_SIZE){
      _Index = INDEX_LEADER;           // data overrun, wait for leader again
      return;
   }
   // check for message size :
   if( _Index == _DataSize - 1 + SMEGAVI_CRC_OFFSET - 1){  // with CRC, without leader
      // valid data
      _DataSize--;                        // data size without Id
      Crc = CalculateCrc( _DataSize);     // payload data size
      if( Data != Crc){
         _Index = INDEX_LEADER;           // wrong CRC, wait for leader again
         return;
      }
      // data ok :
      ComDisableRxInt();
      _Status = SMEGAVI_STATUS_RX_DONE;
   }
   _Buffer[ _Index++] = Data;          // save received data
} // USART0_RX_VECTOR

//-----------------------------------------------------------------------------
// Tx data interrupt
//-----------------------------------------------------------------------------

ISR( USART_TX_VECTOR)
{
   // check for leader :
   if( _Index == INDEX_LEADER){
      UDR    = SMEGAVI_LEADER_CHAR;    // send leader
      _Index = 0;                      // first data character
      return;
   }
   // check for trailer :
   if( _Index == _DataSize - 1){
      ComDisableTxInt();               // stop Tx data interrupt
      ComClearTxDone();                // clear Tx done flag
      ComEnableTxDoneInt();            // wait for shift register empty
   }
   // send data :
   UDR = _Buffer[ _Index++];           // send data
} // USART_TX_VECTOR

//-----------------------------------------------------------------------------
// Tx done interrupt
//-----------------------------------------------------------------------------

ISR( USART_TX_DONE_VECTOR)
{
   ComDisableTxDoneInt();               // stop Tx done interrupt
   SMegaviRxStart();                       // start reception
} // USART_TX_DONE_VECTOR
