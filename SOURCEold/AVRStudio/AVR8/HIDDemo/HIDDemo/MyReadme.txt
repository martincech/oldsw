Testy generic HID

1. Echo paketu funguje, prenosova rychlost je priblizne
   3,6 kB/s pri vyslani/prijmu bloku
   
2. Pokud Host vysle vsechny pakety a potom ceka na prijem,
   vrati se jen 16 paketu, potom cosi vynecha a vraci
   posledni pakety
   
3. Pokud Host vysle jeden paket a ceka vicenasobnou odpoved,
   vrati se jen 32 paketu. Rychlost je 32kB/s
   

Testovani enumerace :

1. enumerace probehne pokud je Delay = 120ms a mensi
   (150ms uz neprojde) v kazde smycce scheduleru.
   
2. pokud se zpozdeni generuje kazdou druhou smycku,
   citlivost na Delay se nemeni. Zrejme nastavuje
   timeout na kazdou transakci (ne na celou enumeraci)
   

   