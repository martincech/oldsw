//******************************************************************************
//
//   hid_task.c       HID demo main
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include "config.h"
#include "conf_usb.h"
#include "hid_task.h"
#include "lib_mcu/usb/usb_drv.h"
#include "usb_descriptors.h"
#include "modules/usb/device_chap9/usb_standard_request.h"
#include "usb_specific_request.h"

#include "lib_mem/df/df_mem.h"
#include "lib_mcu/uart/uart_lib.h"

#include <stdio.h>
#include <string.h>
#include "HidDef.h"


//volatile U8 cpt_sof = 0;

static Byte        Buffer[ HIDDEMO_IN_REPORT_SIZE];
static Bool        IsBufferFull = false;      
static THidRequest Request;

// Local functions :
void hid_report_out( void);
void hid_report_in( void);

// stdio input/output :

static int my_putchar( int ch, FILE *f);

FILE StdioStream = FDEV_SETUP_STREAM( (int (*)(char, FILE*))my_putchar, 
                                      (int (*)(FILE*))uart_getchar, 
                                      _FDEV_SETUP_RW);

#define cprintf( Format, ...) printf_P( PSTR( Format), ## __VA_ARGS__)

static int my_putchar( int ch, FILE *f)
{
   if( ch == '\n'){
      uart_putchar( '\r');
   }
   uart_putchar( ch);
   return( ch);
} // my_putchar

//-----------------------------------------------------------------------------
// Initialize
//-----------------------------------------------------------------------------

void hid_task_init( void)
{
U32 nb_sectors;

   Leds_init();
   Joy_init();
   df_mem_init();    // Init the hw/sw ressources required to drive the DF.
   uart_init(); 
   stdout = &StdioStream;
   cprintf( "Start...\n");
   if( df_test_unit_ready() != CTRL_GOOD){
      cprintf( "ERROR : flash not ready\n");
   }
   if( df_read_capacity( &nb_sectors) != CTRL_GOOD){
      cprintf( "ERROR : unable read flash capacity\n");
      nb_sectors = 0;
   }
   cprintf( "Flash size : %ld sectors\n", nb_sectors);
} // hid_task_init

//-----------------------------------------------------------------------------
// Task
//-----------------------------------------------------------------------------

void hid_task( void)
{
   if(!Is_device_enumerated())          // Check USB HID is enumerated
      return;

   hid_report_out();
   hid_report_in();
}

//-----------------------------------------------------------------------------
// Start of frame
//-----------------------------------------------------------------------------

void sof_action()
{
   //cpt_sof++;
} // sof_action

//-----------------------------------------------------------------------------
// Report OUT
//-----------------------------------------------------------------------------

void hid_report_out( void)
{
Byte i;


   Usb_select_endpoint(EP_HID_OUT);
   if( !Is_usb_receive_out()){
      return;                          // no data received
   }
   // read report from HID Generic Demo
   Led0_on();
   for( i = 0; i < HIDDEMO_OUT_REPORT_SIZE; i++){
      Buffer[ i] = Usb_read_byte();
   }
   Usb_ack_receive_out();
   memcpy( &Request, Buffer, sizeof( Request));
   if( Request.Command != HID_FLASH_READ){
      cprintf( "Command %d unknown\n", Request.Command);
      Led0_off();
      return;
   }
   IsBufferFull = true;
   Led0_off();
} // hid_report_out

//-----------------------------------------------------------------------------
// Report IN
//-----------------------------------------------------------------------------

void hid_report_in( void)
{
   if( !IsBufferFull){
      return;                          // no data to send
   }

   Usb_select_endpoint( EP_HID_IN);
   if(!Is_usb_write_enabled()){
      return;                          // not ready to send report
   }
   // Send report
   Led2_on();
//   cprintf( "R @%08X %d\n", Request.Address, Request.Count);
   if( df_read_10( Request.Address, Request.Count) != CTRL_GOOD){
      cprintf( "ERROR : unable send sector\n");
   }
   IsBufferFull = false;            // buffer done
   Led2_off();
} // hid_report_in
