;--------------------------------------------------------
;	File: Untitled.asm
;
;	Author: unknown
;--------------------------------------------------------
<COMMENT>
!!! User must edit hex file otherwise filter won't work !!!
Instructions below
<ENDCOMMENT>

#include	"AlgConfig.h"

CONST	AVG_MAX					17



; Registers
CONST StatusRegister			22
CONST FlagResetPOR			19 ; Power-On-Reset						flg_rstpwr
CONST FlagResetButton		18 ; POR - RST pin							flg_rstssn
CONST FlagResetWD			17 ; POR - Watchdog						flg_wdtalt
CONST FlagResetMeas			16 ; Wake-up - new measurement done		flg_endavg
CONST FlagResetSleep		15 ; Wake-up - regularly from sleep mode	flg_intav0

CONST ConfigRegAddr	48


CONST IoPin4				6 	; 17
CONST IoPin5				7 	; 17
CONST	TdcSleepMode		17 	; 1

CONST sel_start_osz		17  ; 3



; RAM variables
CONST Result						0

CONST ControlRegister			1
CONST	WAKE_UP					0
CONST SEND_ALL					1
CONST STABLE_VALUE			2
CONST VALID_DATA				3
CONST CALIBRATION				4
CONST CALIBRATION_LOW		5
CONST CALIBRATION_HIGH		6
CONST IS_USING_FINE_CONTROL	7


CONST RawValue					2
CONST	LowPassInput				3
CONST	LowPassOutput				4
CONST	LastStable		5

CONST PrefilterInput				6
CONST Prefilter					7
CONST PrefilterCounter			8

CONST FilterValueA				11	; Compiler doesn't like numbers in identifiers names too much
CONST FilterValueB				12
CONST FilterValueC				13
CONST FilterValueD				14

CONST DeltaFromLastStable		15

CONST Factor						32
CONST PreLowPassPeriod			33
CONST PreLowPassCounter		34

CONST AveragingWindow			35


CONST RangeUnused				9

CONST Range						37
CONST	StableWindow				38
CONST	StableWindowCounter		39

CONST RawZero					40	
CONST RawFullRange				41
CONST Zero						42	
CONST FullRange					43

CONST FilterType					44

CONST FILTER_NONE						 0
CONST FILTER_SINC3						 3
CONST FILTER_SINC5						 5



CONST FineSwitchoverRangeAddress					36
CONST CoarseSwitchoverAllowedRangeAddress			47


CONST RawZeroUnused			45
CONST RawFullRangeUnused		46


CONST ConfRegTwoUnused		 54 ; in CONFIGREG !!!
CONST ConfRegThreeUnused		 55 ; in CONFIGREG !!!



CONST MyStatus					65 ; in CONFIGREG !!!
CONST IS_USING_FINE		19 ; lcd_pulsed[3]
CONST CAN_SWITCH_TO_FINE		18 ; lcd_pulsed[2]


;volno 10





	
	
	
ResetCheck:		ramadr		StatusRegister	
					gotoBitS	r, FlagResetPOR, PORCheck
					gotoBitS	r, FlagResetSleep, ResetSleep
					gotoBitS	r, FlagResetMeas, ResetMeas
					
					goto		End ; unknown reset

PORCheck:			gotoBitS	r, FlagResetButton, End
					gotoBitS	r, FlagResetWD, End
					
					ramadr		ControlRegister		; POR reset
					clear		r

					goto		End

;-------------------------------------------------------------------------------------------------------------------------------------
; ------------------------------------------------------ ALGORITHM ---------------------------------------------------------	
;-------------------------------------------------------------------------------------------------------------------------------------
ResetMeas:	
					ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin5				; clear interrupt flag

					;move		y, 0x080080

					;ramadr		Pokus
					;decr		r
					;gotoEQ		Odecist
					;ramadr		ConfigRegAddr + 2
					;add		r, y
					;ramadr		Pokus
					;move		r, 1
					;goto		Konec
Odecist:					
					;ramadr		ConfigRegAddr + 2
					;sub			y, r
					;swap		r, y
					;ramadr		Pokus
					;move		r, 0

					;
					
					;newcyc



					ramadr		ControlRegister
					bitclr		r, VALID_DATA			; data not valid
					
					ramadr		RawValue
					move		r, x						; store raw value

TranslateRaw:
					ramadr		RawZero
					move		z, r	
					
					sub			z, x						; - RawZero
					
					ramadr		Factor
					mult24		z, r						; * Factor
					
					ramadr		Zero
					add		z, r						; + Zero offset


PreFilter:	
					ramadr		PrefilterInput
					add		r, z
	
					ramadr		PrefilterCounter
					decr		r
					skipEQ		1
					goto		End

					ramadr		PrefilterInput
					move		z, r
					clear		r
					
					ramadr		Prefilter
					move		y, r
					
					ramadr		PrefilterCounter
					move		r, y
					
					divmod		z, y


Filter:
					move		x, z
					ramadr		FilterType
					move		z, r
					jsub		FilterRoll
					move		z, x







StoreResult:
					ramadr		Result
					move		r, z						; store result
					


					ramadr		ControlRegister
					gotoBitC	r, CALIBRATION, NormalOperation

					ramadr		ControlRegister
					gotoBitC	r, CALIBRATION_LOW, CheckHigh
					bitclr		r, CALIBRATION_LOW
					ramadr		MyStatus
					gotoBitC	r, IS_USING_FINE, DoInterrupt
					jsub		SwitchAccuracy
					initTDC
					newcyc
					goto		DoInterrupt

CheckHigh:					
					ramadr		ControlRegister
					gotoBitC	r, CALIBRATION_HIGH, DoInterrupt
					bitclr		r, CALIBRATION_HIGH
					ramadr		MyStatus
					gotoBitS	r, IS_USING_FINE, DoInterrupt
					jsub		SwitchAccuracy
					goto		DoInterrupt



NormalOperation:
					ramadr		Result
					move		z, r

				
<COMMENT>
	!!! Merene hodnoty musi byt dostatecne male, aby nedoslo k preteceni pri vypoctu prumeru !!!
<ENDCOMMENT>				

<COMMENT>
rollAvg routines can handle up to 17 samples. To be able to handle more samples average them before they reach rollAvg routine
2 - 17 samples - /1 (no averaging)
18 - 34 samples - /2
35 - 68 samples - /4
<ENDCOMMENT>
PreAvg:	
					ramadr		LowPassInput
					add		r, z
	
					ramadr		PreLowPassCounter
					decr		r
					
					gotoEQ		ComputeLowPassInput
					ramadr		LowPassOutput		; else take old value
					move		x, r
					goto		Detection
					
					; compute new low pass value
ComputeLowPassInput:		
					ramadr		PreLowPassPeriod
					move		y, r
					ramadr		PreLowPassCounter
					move		r, y					; PreAvgCounter = PreAvgPeriod
					
					ramadr		LowPassInput
					move		x, r
					clear		r
					
Divide:				shiftR		y
					skipEQ		2
					shiftR		x						; AvgInput /= 2
					goto		Divide

					ramadr		AveragingWindow
					move		z, r
					jsub		FAKE_ROLL_AVG_ROUTINE			; rollAvg: Replace with EC21
					
					ramadr		LowPassOutput
					move		r, x
		
Detection:			
					ramadr		Result
					move		z, r
					
					sub			x, z		; HighPass = Result - AvgOutput

					ramadr		MyStatus
					gotoBitS	r, IS_USING_FINE, CheckRange
					ramadr		DeltaFromLastStable
					add		r, x
					move		y, r
					abs			y
					ramadr		CoarseSwitchoverAllowedRangeAddress
					compare	y, r
					skipPos	2
					ramadr		MyStatus
					bitset		r, CAN_SWITCH_TO_FINE
					
CheckRange:			
					abs			x		; HighPassAbs = |HighPass|
					ramadr		Range	; check whether HighPassAbs is in range
					compare	x, r
				
					gotoPos	CheckDuration  ; In range, check duration
					
												; Not in range
					ramadr		StableWindowCounter  
					clear		r				; DurationCounter = 0
					
					ramadr		MyStatus
					gotoBitC	r, IS_USING_FINE, NotStableVariations
					
					ramadr		FineSwitchoverRangeAddress
					compare	x, r
					skipPos	3
					
					jsub		SwitchAccuracy
					ramadr		MyStatus
					bitset		r, CAN_SWITCH_TO_FINE
					
					goto		NotStableVariations

CheckDuration:
					ramadr		StableWindow
					move		y, r

					ramadr		StableWindowCounter

					compare	y, r			; Duration - DurationCounter
					
					skipEQ		3
					incr		r
					compare	y, r
					gotoNE		StillNotStable


					ramadr		MyStatus
					gotoBitC	r, IS_USING_FINE, StableInCoarse
					
					ramadr		ControlRegister
					;gotoBitS	r, STABLE_VALUE, StableButDontSend

					bitset		r, STABLE_VALUE
					jsub		SwitchAccuracy
					ramadr		DeltaFromLastStable
					clear		r
					
					ramadr 	LowPassOutput
					move		x, r
					ramadr		LastStable
					move		r, x
					
					goto		DoInterrupt

					
StableInCoarse:
					ramadr		MyStatus
					skipBitC	r, CAN_SWITCH_TO_FINE, 2
					bitclr		r, CAN_SWITCH_TO_FINE
					jsub		SwitchAccuracy
StillNotStable:		
					nop	
									
StableButDontSend:
					ramadr		ControlRegister
					gotoBitC	r, SEND_ALL, End		; should we send all?
					goto 		DoInterrupt
					
NotStableVariations:
					ramadr		ControlRegister
					bitclr		r, STABLE_VALUE

					
					ramadr		ControlRegister
					gotoBitC	r, SEND_ALL, End		; should we send all?
					goto 		DoInterrupt
				
DoInterrupt:		
					ramadr		ControlRegister
					bitset		r, VALID_DATA

					ramadr		ConfigRegAddr + 17
					bitset		r, IoPin5
					goto		End
;-------------------------------------------------------------------------------------------------------------------------------------					
; -------------------------------------------------------- INITIALISATION -------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------------------------				
ResetSleep:		
					ramadr		ControlRegister
					gotoBitC	r, WAKE_UP, End
MeasureStart:	
; Compute factor

					ramadr		MyStatus
					bitclr		r, IS_USING_FINE
					
					ramadr		ControlRegister
					bitclr		r, IS_USING_FINE_CONTROL
					
					jsub		SwitchAccuracy


; Clear variables
					
					ramadr		PrefilterInput
					clear		r
	
					ramadr		Prefilter
					move		x, r
					ramadr		PrefilterCounter
					move		r, x
					
					ramadr		StableWindowCounter
					clear		r

					ramadr		LowPassOutput
					clear		r
					
					ramadr		LowPassInput
					clear		r

; AveragingWindow value must be limited (max 17) - decrease it if neccessary
					ramadr		AveragingWindow
					move		x, r
					ramadr		PreLowPassPeriod
					move		r, 1			; PreAvgPeriod = 1

InitPreAvg:					
					compare	x, AVG_MAX
					skipPos	3			; Filter OK, can leave cycle
					shiftL		r			; PreAvgPeriod +=1
					shiftR		x			; AveragingWindow /= 2
					goto		InitPreAvg	; Check again

					move		y, r
					ramadr		PreLowPassCounter  ; Initiate, PreAvgCounter = PreAvgPeriod
					move		r, y
					
					ramadr		AveragingWindow
					move		r, x			; Store AveragingWindow

<COMMENT>
Compiler doesn't allow initAvg routine to be initialized with non-constant filter length.

By examination of hex file, one can see that initAvg x, CONST compiles to:
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3120); jsub2_0 (3120);

and rollAvg x, CONST
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3105);  jsub2_0 (3105);

where:
some_number = f(CONST) = (CONST << (CONST - 1)) | ((1 << (CONST - 2)) - 1)

After compilation replace two fake calls of FAKE_AVG_ROUTINE with EC and 30 opcodes ( = jsub(3120), for initAvg ) or EC and 21 opcodes ( =  jsub (3105), for rollAvg )

<ENDCOMMENT>
					; Recalculation of AveragingWindow

					move		y, r

AWShift1:								; Calculate W << (W - 1)
					compare	y, 1
					skipEQ		3
					decr		y
					shiftL		x
					goto		AWShift1
					
					move		z, 1
					move		y, r
AWShift2:								; Calculate 1 << (W - 2)
					compare	y, 2
					skipEQ		3
					decr		y
					shiftL		z
					goto		AWShift2
					
					decr		z
					
					or			z, x

				 	ramadr		AveragingWindow
				 	move		r, z
				 	
					ramadr		Zero	; offset
					move		x, r
					jsub		FAKE_INIT_AVG_ROUTINE			; initAvg: Replace with EC30

					ramadr		Zero	; offset
					move		x, r
					ramadr		FilterType
					move		z, r
					jsub		FilterInit

					ramadr		ConfigRegAddr + 1
					bitclr		r, TdcSleepMode	; wake up TDC
					
End:				clrwdt
					stop


<COMMENT>
	pokus
InitAvg
parameters:
x - init value
z - filter length

InitAvg:
					ramadr		FilterRAMStart

InitValues:					
					decr		z
					skipNeg	3
					move		r, x
					incramadr
					goto		InitValues
					
					ramadr		FilterRAMPointer
					clear		r
					
					jsubret
<ENDCOMMENT>


<COMMENT>
FilterInit
parameters:
x - init value
z - filter type
<ENDCOMMENT>
FilterInit:
					compare	z, FILTER_NONE
					skipNE		1
					jsubret
						
					ramadr		FilterValueA
					move		r, x
					ramadr		FilterValueB
					move		r, x

					compare	z, FILTER_SINC3
					skipNE		1
					jsubret

					ramadr		FilterValueC
					move		r, x
					ramadr		FilterValueD
					move		r, x
					jsubret

<COMMENT>
FilterRoll
parameters:
x - new value
z - filter type
<ENDCOMMENT>
FilterRoll:
					compare	z, FILTER_NONE
					skipNE		1
					jsubret
					
		 			move		y, x

					ramadr		FilterValueA
					add		x, r
					swap		y, r
					
					ramadr		FilterValueB
					add		x, r
					swap		y, r
						
					compare	z, FILTER_SINC3	
					skipNE		1
					goto		FilterDivide
						
					ramadr		FilterValueC
					add		x, r
					swap		y, r
						
					ramadr		FilterValueD
					add		x, r
					swap		y, r
						
FilterDivide:
	
					divmod		x, z
					jsubret

<COMMENT>
RollAvg:
Pokus
					ramadr		FilterRAMStart

RollRAM:					
					decr		z
					skipEQ		2
					incramadr
					goto		RollRAM
					
					
					move		r, x
					
					incr		r
					compare	r
<ENDCOMMENT>

SwitchAccuracy:		
					ramadr		ConfigRegAddr + 2
					move		x, r
					ramadr		ConfRegTwoUnused
					swap		x, r
					ramadr		ConfigRegAddr + 2
					move		r, x
				
					ramadr		ConfigRegAddr + 3
					move		x, r
					ramadr		ConfRegThreeUnused
					swap		x, r
					ramadr		ConfigRegAddr + 3
					move		r, x
					



					ramadr		ControlRegister
					bitinv		r, IS_USING_FINE_CONTROL

					ramadr		MyStatus
					bitinv		r, IS_USING_FINE
					skipBitS	r, IS_USING_FINE, 2
					initTDC
					newcyc



  					ramadr		StableWindowCounter
					clear		r
					

					ramadr		MyStatus
					gotoBitC	r, IS_USING_FINE, skipFilterInit
					ramadr		LowPassOutput
					move		x, r
					jsub		FAKE_INIT_AVG_ROUTINE

					ramadr		LowPassOutput
					move		x, r
					ramadr		FilterType
					move		z, r
					jsub		FilterInit
					
					ramadr		PreLowPassPeriod
					move		x, r
					ramadr		PreLowPassCounter
					move		r, x
					
skipFilterInit:

					ramadr		Range
					move		x, r
					ramadr		RangeUnused
					swap		x, r
					ramadr		Range
					move		r, x
					
	
					ramadr		RawZero
					move		x, r
					ramadr		RawZeroUnused
					swap		x, r
					ramadr		RawZero
					move		r, x

					ramadr		RawFullRange
					move		y, r
					ramadr		RawFullRangeUnused
					swap		y, r
					ramadr		RawFullRange
					move		r, y

					sub			x, r

					ramadr		FullRange	
					move		y, r
					
					div24		y, x						; (FullRange << 24) / (RawFullRange - RawZero)
					
					ramadr		Factor
					move		r, y

					jsubret

					
FAKE_INIT_AVG_ROUTINE: ; Call to this routine will be replaced by ROM built-in InitAvg
					jsubret
FAKE_ROLL_AVG_ROUTINE: ; Call to this routine will be replaced by ROM built-in RollAvg
					jsubret

; --- EOF ---