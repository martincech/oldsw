;--------------------------------------------------------
;	File: _ResetCheckDefault.h
;
;	Author: Veit Electronics
;	
;	Description: Place this include file just on program entry.
;--------------------------------------------------------

ResetCheck:		ramadr		StatusRegister	
					gotoBitS	r, FlagResetPOR, PORCheck
					gotoBitS	r, FlagResetSleep, ResetSleep
					gotoBitS	r, FlagResetMeas, ResetMeas
					
					goto		End ; unknown reset

PORCheck:			gotoBitS	r, FlagResetButton, End
					gotoBitS	r, FlagResetWD, End
					
					ramadr		ControlRegister		; POR reset
					clear		r

					goto		End