<COMMENT>
PrefilterInit
<ENDCOMMENT>
PrefilterInit:
					ramadr		PrefilterInput
					clear		r
	
					ramadr		Prefilter
					move		x, r
					ramadr		PrefilterCounter
					move		r, x
					jsubret

ComputeFactors:
					ramadr		RawZero
					move		x, r
					ramadr		RawFullRange
					move		y, r
					ramadr		FullRange
					move		z, r
					ramadr		Factor
					jsub		ComputeFactor

					ramadr		RawZeroUnused
					move		x, r
					ramadr		RawFullRangeUnused
					move		y, r
					ramadr		FullRange
					move		z, r
					ramadr		FactorUnused
					jsub		ComputeFactor
					
					jsubret


FilterDo:
					move		x, z
					ramadr		FilterType
					move		z, r
					jsub		FilterRoll
					jsubret


<COMMENT>
FilterInit
parameters:
x - init value
z - filter type
<ENDCOMMENT>
FilterInit:
					ramadr		FilterValueA
					move		r, x
					ramadr		FilterValueB
					move		r, x
					ramadr		FilterValueC
					move		r, x
					ramadr		FilterValueD
					move		r, x
					jsubret

<COMMENT>
FilterRoll
parameters:
x - new value
z - filter type
<ENDCOMMENT>
FilterRoll:
					compare	z, FILTER_NONE
					skipNE		1
					jsubret
					
		 			move		y, x

					ramadr		FilterValueA
					add		x, r
					swap		y, r
					
					ramadr		FilterValueB
					add		x, r
					swap		y, r
						
					compare	z, FILTER_SINC3	
					skipNE		1
					goto		FilterDivide
						
					ramadr		FilterValueC
					add		x, r
					swap		y, r
						
					ramadr		FilterValueD
					add		x, r
					swap		y, r
						
FilterDivide:
					divmod		x, z
					jsubret

SwitchAccuracyHw:
					ramadr		ControlRegister
					skipBitS	r, DO_SWITCH, 1
					jsubret

					ramadr		ConfigRegAddr + 2
					move		x, r
					ramadr		ConfRegTwoUnused
					swap		x, r
					ramadr		ConfigRegAddr + 2
					move		r, x

					ramadr		ConfigRegAddr + 3
					move		x, r
					ramadr		ConfRegThreeUnused
					swap		x, r
					ramadr		ConfigRegAddr + 3
					move		r, x
					
					ramadr		MyStatus
					bitset		r, SWITCHED
					
					jsubret
					
SwitchAccuracyPost:		
					ramadr		MyStatus
					skipBitS	r, SWITCHED, 1
					jsubret

					bitinv		r, IS_USING_FINE

					ramadr		StableWindowCounter
					clear		r

					ramadr		Factor
					move		x, r
					ramadr		FactorUnused
					swap		x, r
					ramadr		Factor
					move		r, x
					
					ramadr		RawZero
					move		x, r
					ramadr		RawZeroUnused
					swap		x, r
					ramadr		RawZero
					move		r, x

					ramadr		StableWindow
					move		x, r
					ramadr		StableWindowUnused
					swap		x, r
					ramadr		StableWindow
					move		r, x

					ramadr		AveragingWindow
					move		x, r
					ramadr		AveragingWindowUnused
					swap		x, r
					ramadr		AveragingWindow
					move		r, x

					ramadr		Range
					move		x, r
					ramadr		RangeUnused
					swap		x, r
					ramadr		Range
					move		r, x

					jsubret


; r = AveragingWindow address


<COMMENT>
Compiler doesn't allow initAvg routine to be initialized with non-constant filter length.

By examination of hex file, one can see that initAvg x, CONST compiles to:
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3120); jsub2_0 (3120);

and rollAvg x, CONST
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3105);  jsub2_0 (3105);

where:
some_number = f(CONST) = (CONST << (CONST - 1)) | ((1 << (CONST - 2)) - 1)

After compilation replace two fake calls of FAKE_AVG_ROUTINE with EC and 30 opcodes ( = jsub(3120), for initAvg ) or EC and 21 opcodes ( =  jsub (3105), for rollAvg )

<ENDCOMMENT>
					; Recalculation of AveragingWindow
ComputeNative:
					move		x, r
					move		y, r
; Calculate W << (W - 1)
AWShift1:								
					compare	y, 1
					skipEQ		3
					decr		y
					shiftL		x
					goto		AWShift1
					
					move		y, r
					move		r, 1
; Calculate 1 << (W - 2)					
AWShift2:								
					compare	y, 2
					skipEQ		3
					decr		y
					shiftL		r
					goto		AWShift2
					
					decr		r
					
					or			r, x
					jsubret

CONST	MULTIPLIER	3

; x = RawZero
; y = RawFullRange
; z = FullRange
; r = factor address

ComputeFactor:
					sub			x, y  ; RawFullRange - RawZero
					
					shiftL		x, MULTIPLIER
					
					div24		z, x						; (FullRange << 24) / (RawFullRange - RawZero)
					
					move		r, z
					jsubret

TranslateRaw:
					ramadr		RawZero
					move		z, r	
					
					sub			z, x						; - RawZero
					
					shiftL		z, MULTIPLIER
					
					ramadr		Factor
					mult24		z, r						; * Factor
					
					ramadr		Zero
					add		z, r						; + Zero offset
					
					jsubret


RollAvgxInit:
					jsubret
					

RollAvgx:

					move		y, x
					
					ramadr		AveragingWindowCurrent		
					move		z, r
					ramadr		ROLL_AVG_NEWEST_SAMPLE
					compare	z, AVG_MAX
					gotoPos	RollAvgLessThanAvgMax
					

					
					

RollAvgEqualAvgMax:
					nop
RollAvgxIterAvgMax:

					decr		z
					gotoEQ		Div
					swap		x, r
					add		y, x
					incramadr
					goto		RollAvgxIterAvgMax


RollAvgLessThanAvgMax:	
					nop		
RollAvgxIterLess:
					decr		z
					gotoEQ		RollAvgxIterDone
					swap		x, r
					add		y, x
					incramadr
					goto		RollAvgxIterLess
RollAvgxIterDone:
					move		r, x
					

Div:					
					ramadr		AveragingWindowCurrent
					move		z, r
					divmod		y, z
					move		x, y
			

					ramadr		AveragingWindow
					move		z, r
					ramadr		AveragingWindowCurrent
					compare	z, r	; AveragingWindowCurrent - AveragingWindow
					gotoEQ		return
					skipPos	2
					incr		r
					skip		1
					decr		r
					
return:					
					jsubret


; Call to this routine will be replaced by ROM built-in InitAvg					
FAKE_INIT_AVG_ROUTINE: 
					jsubret
; Call to this routine will be replaced by ROM built-in RollAvg
FAKE_ROLL_AVG_ROUTINE: 
					jsubret