;--------------------------------------------------------
;	File: Untitled.asm
;
;	Author: unknown
;--------------------------------------------------------
<COMMENT>
!!! User must manually edit hex file otherwise filter won't work !!!
Instructions below
<ENDCOMMENT>

#include	"AlgConfig.h"


;CONST		initAvg_Addr		3120

; User RAM
<COMMENT>
CONST RAM_START_SEGMENT0		0
CONST RAM_LENGTH_SEGMENT0		16

CONST RAM_START_SEGMENT1		32
CONST RAM_LENGTH_SEGMENT1		16

; Used by ROM
CONST RAM_START_SEGMENT2		112
CONST RAM_LENGTH_SEGMENT2		16
<ENDCOMMENT>

CONST	AVG_MAX					17

; RAM variables
CONST ControlRegister			1
CONST	WAKE_UP					0
CONST SEND_ALL					1
CONST STABLE_VALUE			2
CONST VALID_DATA				3

CONST Result				2
CONST Factor				3
CONST RawValue			4
CONST	HighPassAbs		5


CONST RawZero			34	
CONST RawFullRange		38
CONST Zero				36	
CONST FullRange			37


CONST	Duration			39
CONST	DurationCounter	40

CONST Range				41

CONST AveragingWindow				46

CONST	AvgOutput		42	; Last output of low pass filter
CONST PreAvgPeriod		43
CONST PreAvgCounter	44
CONST	AvgInput		45

CONST	AveragingWindowNative		47

; Registers
CONST StatusRegister			22
CONST FlagResetPOR			19 ; Power-On-Reset						flg_rstpwr
CONST FlagResetButton		18 ; POR - RST pin							flg_rstssn
CONST FlagResetWD			17 ; POR - Watchdog						flg_wdtalt
CONST FlagResetMeas			16 ; Wake-up - new measurement done		flg_endavg
CONST FlagResetSleep		15 ; Wake-up - regularly from sleep mode	flg_intav0

CONST ConfigRegAddr	48


CONST IoPin4				6 	; 17
CONST IoPin5				7 	; 17
CONST	TdcSleepMode		17 	; 1

	
ResetCheck:		ramadr		StatusRegister	
					gotoBitS	r, FlagResetPOR, PORCheck
					gotoBitS	r, FlagResetSleep, ResetSleep
					gotoBitS	r, FlagResetMeas, ResetMeas
					
					goto		End ; unknown reset

PORCheck:			gotoBitS	r, FlagResetButton, End
					gotoBitS	r, FlagResetWD, End
					
					ramadr		ControlRegister		; POR reset
					clear		r

					goto		End

;-------------------------------------------------------------------------------------------------------------------------------------
; ------------------------------------------------------ ALGORITHM ---------------------------------------------------------	
;-------------------------------------------------------------------------------------------------------------------------------------
ResetMeas:	
					ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin5				; clear interrupt flag
					
					ramadr		ControlRegister
					bitclr		r, VALID_DATA			; data not valid
					
					ramadr		RawValue
					move		r, x						; store raw value

TranslateRaw:
					ramadr		RawZero
					move		z, r	
					
					sub			z, x						; - RawZero
					
					ramadr		Factor
					mult24		z, r						; * Factor
					
					ramadr		Zero
					add		z, r						; + Zero offset

					ramadr		Result
					move		r, z						; store result
					
					move		x, z	
<COMMENT>
	!!! Merene hodnoty musi byt dostatecne male, aby nedoslo k preteceni pri vypoctu preavg prumeru !!!
<ENDCOMMENT>			

<COMMENT>
rollAvg routines can handle up to 17 samples. To be able to handle more samples average them before they reach rollAvg routine
2 - 17 samples - /1 (no averaging)
18 - 34 samples - /2
35 - 68 samples - /4
<ENDCOMMENT>
PreAvg:	
					ramadr		AvgInput
					add		r, x
	
					ramadr		PreAvgCounter
					shiftR		r
					
;					gotoEQ		ComputeAvgInput
					ramadr		AvgOutput		; else take old value
					move		x, r
					goto		Detection
; compute new low pass value					
ComputeAvgInput:		
					ramadr		PreAvgPeriod
					move		y, r
					ramadr		PreAvgCounter
					move		r, y
					ramadr		AvgInput
					move		x, r
					
Divide:				shiftR		y
					skipEQ		2
					shiftR		x
					goto		Divide

					ramadr		AveragingWindowNative
					move		z, r
					jsub		FAKE_AVG_ROUTINE			; rollAvg: Replace with EC21
					
					ramadr		AvgOutput
					move		r, x
					
					ramadr		AvgInput
					clear		r
					
					ramadr		PreAvgCounter
					clear		r
					
<COMMENT>
ComputeAvgInput:		
					ramadr		PreAvgPeriod
					move		y, r
					ramadr		PreAvgCounter
					move		r, y					; PreAvgCounter = PreAvgPeriod
					
					ramadr		AvgInput
					move		x, r
					clear		r
					
Divide:				shiftR		y
					skipEQ		2
					shiftR		x						; AvgInput /= 2
					goto		Divide

					ramadr		AveragingWindowNative
					move		z, r
					jsub		FAKE_AVG_ROUTINE			; rollAvg: Replace with EC21
					
					ramadr		AvgOutput
					move		r, x
<ENDCOMMENT>
Detection:				
					ramadr		Result
					move		z, r
					
					sub			x, z		; HighPass = Value - LowPassOutput
					abs			x		; HighPassAbs = |HighPass|
					
					ramadr		HighPassAbs
					move		r, x		; store HighPass
					
					ramadr		Range	; check whether HighPassAbs is in range
					compare	x, r
				
					gotoPos	CheckDuration  ; In range, check duration
					
												; Not in range
					ramadr		DurationCounter  
					clear		r				; DurationCounter = 0
					ramadr		ControlRegister
					bitclr		r, STABLE_VALUE
					goto		NotStable

CheckDuration:
					ramadr		Duration
					move		y, r

					ramadr		DurationCounter

					compare	y, r			; Duration - DurationCounter
					
					skipEQ		3
					incr		r
					compare	y, r
					gotoNE		NotStable
					
					ramadr		ControlRegister
					skipBitS	r, STABLE_VALUE, 2	; do interrupt only if this is new stable value
					bitset		r, STABLE_VALUE
					goto		DoInterrupt
					
NotStable:
					ramadr		ControlRegister
					gotoBitC	r, SEND_ALL, End		; should we send all?
				
DoInterrupt:		
					bitset		r, VALID_DATA

					ramadr		ConfigRegAddr + 17
					bitset		r, IoPin5					; do interrupt
					goto		End
;-------------------------------------------------------------------------------------------------------------------------------------					
; -------------------------------------------------------- INITIALISATION -------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------------------------				
ResetSleep:		
					ramadr		ControlRegister
					gotoBitC	r, WAKE_UP, End
MeasureStart:	
; Compute factor
					ramadr		RawZero
					move		x, r
					
					ramadr		RawFullRange
					
					sub			x, r

					ramadr		FullRange	
					move		y, r
					
					div24		y, x						; (FullRange << 24) / (RawFullRange - RawZero)
					
					ramadr		Factor
					move		r, y

; Clear variables
					ramadr		DurationCounter
					clear		r

					ramadr		AvgOutput
					clear		r
					
					ramadr		AvgInput
					clear		r

; AveragingWindow value must be limited (max 17) - decrease it if neccessary
					ramadr		AveragingWindow
					move		x, r
					ramadr		PreAvgPeriod
					move		r, 1			; PreAvgPeriod = 1

InitPreAvg:					
					compare	x, AVG_MAX
					skipPos	3			; Filter OK, can leave cycle
					shiftL		r			; PreAvgPeriod *= 2
					shiftR		x			; AveragingWindow /= 2
					goto		InitPreAvg	; Check again

					move		y, r
					ramadr		PreAvgCounter  ; Initiate, PreAvgCounter = PreAvgPeriod
					move		r, y
					
					ramadr		AveragingWindow
					move		r, x			; Store AveragingWindow

<COMMENT>
Compiler doesn't allow initAvg routine to be initialized with non-constant second parameter.

By examination of hex file, one can see that initAvg x, CONST compiles to:
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3120); jsub2_0 (3120);

and rollAvg x, CONST
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3105);  jsub2_0 (3105);

where:
some_number = f(CONST) = (CONST << (CONST - 1)) | ((1 << (CONST - 2)) - 1)

After compilation replace manually two fake calls of FAKE_AVG_ROUTINE with EC and 30 opcodes ( = jsub(3120), for initAvg ) or EC and 21 opcodes ( =  jsub (3105), for rollAvg )

<ENDCOMMENT>
					; Recalculation of AveragingWindow

					move		y, r

AWShift1:								; Calculate W << (W - 1)
					compare	y, 1
					skipEQ		3
					decr		y
					shiftL		x
					goto		AWShift1
					
					move		z, 1
					move		y, r
AWShift2:								; Calculate 1 << (W - 2)
					compare	y, 2
					skipEQ		3
					decr		y
					shiftL		z
					goto		AWShift2
					
					decr		z
					
					or			z, x

				 	ramadr		AveragingWindowNative
				 	move		r, z
				 	
					ramadr		RawZero	; offset
					move		x, r
					jsub		FAKE_AVG_ROUTINE			; initAvg: Replace with EC30


					ramadr		ConfigRegAddr + 1
					bitclr		r, TdcSleepMode	; wake up TDC
			
End:				clrwdt
					stop
					
FAKE_AVG_ROUTINE:
					jsubret
; --- EOF ---