;--------------------------------------------------------
;	File: _Bat2Definitions.h
;
;	Author: Veit Electronics
;	
;	Description: 
;--------------------------------------------------------

CONST	AVG_MAX					17



; Registers
CONST StatusRegister			22
CONST FlagResetPOR			19 ; Power-On-Reset						flg_rstpwr
CONST FlagResetButton		18 ; POR - RST pin							flg_rstssn
CONST FlagResetWD			17 ; POR - Watchdog						flg_wdtalt
CONST FlagResetMeas			16 ; Wake-up - new measurement done		flg_endavg
CONST FlagResetSleep		15 ; Wake-up - regularly from sleep mode	flg_intav0

CONST ConfigRegAddr	48


CONST IoPin4				6 	; 17
CONST IoPin5				7 	; 17
CONST	TdcSleepMode		17 	; 1

CONST sel_start_osz		17  ; 3



CONST ROLL_AVG_NEWEST_SAMPLE  112
CONST	ROLL_AVG_LAST_SAMPLE	127

; RAM variables
CONST Weight						0

CONST ControlRegister			1
CONST	WAKE_UP					0
CONST SEND_ALL					1
CONST STABLE_VALUE			2
CONST VALID_DATA				3
CONST CALIBRATION				4
CONST CALIBRATION_LOW		5
CONST CALIBRATION_HIGH		6
CONST IS_USING_FINE_CONTROL	7
CONST DO_SWITCH				8

CONST RawValue					2

CONST	LowPassOutput			4
CONST	LastStableValue					5

CONST PrefilterInput				6
CONST Prefilter					7
CONST PrefilterCounter			8

CONST FilterValueA				11	; Compiler doesn't like numbers in identifiers names too much
CONST FilterValueB				12
CONST FilterValueC				13
CONST FilterValueD				14

CONST	PreLowPassInput				15
CONST PreLowPassAveragingPeriod			33
CONST PreLowPassAveragingCounter		43

CONST AveragingWindow			10
CONST AveragingWindowUnused 35

CONST Range						37
CONST RangeUnused	9




CONST	StableWindow				38
CONST	StableWindowCounter		39
CONST	StableWindowUnused		34

CONST Zero						42	

CONST RawZero					40	
CONST RawZeroUnused			45

CONST Factor						32
CONST FactorUnused				41
CONST RawFullRange				41		; can be overwritten after initialization


CONST	AveragingWindowCurrent	46
CONST RawFullRangeUnused		46		; can be overwritten after initialization


CONST FullRange					43		; can be overwritten after initialization


CONST FilterType					44

CONST FILTER_NONE						 0
CONST FILTER_SINC3						 3
CONST FILTER_SINC5						 5



CONST FineSwitchoverRange					36
CONST CoarseSwitchoverRange			47



CONST 	Switcher				36
CONST Deny			47
CONST ConfRegTwoUnused		 54 ; in CONFIGREG !!!
CONST ConfRegThreeUnused		 55 ; in CONFIGREG !!!



CONST MyStatus					65 ; in CONFIGREG !!!
CONST IS_USING_FINE		19 ; lcd_pulsed[3]
CONST CAN_SWITCH_TO_FINE		18 ; lcd_pulsed[2]
CONST SWITCHED		15 ; lcd_pulsed[1]
CONST STABLE_NOTIFIED		16 ; lcd_pulsed[0]

CONST	FilterOutput		3