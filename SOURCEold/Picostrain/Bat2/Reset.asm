;--------------------------------------------------------
;	File: Untitled.asm
;
;	Author: unknown
;--------------------------------------------------------

#include	"AlgConfig.h"

; RAM variables
CONST CounterResetButton		1
CONST CounterResetWatchdog	2
CONST CounterResetSleep			3
CONST CounterResetMeas			4
CONST CounterResetTotal			5
CONST CounterResetUnknown		6
CONST CounterResetPOR			7


; Registers
CONST StatusRegister			22
CONST FlagResetPOR			19 ; Power-On-Reset
CONST FlagResetButton		18 ; POR - RST pin
CONST FlagResetWD			17 ; POR - Watchdog
CONST FlagResetMeas			16 ; Wake-up - new measurement done
CONST FlagResetSleep		15 ; Wake-up - regularly from sleep mode

CONST ConfigReg17	65

CONST IoPin4			6

ResetCheck:		ramadr		ConfigReg17   ; program entry
					bitset		r, IoPin4
					
					ramadr		CounterResetTotal
					incr		r
					
					ramadr		StatusRegister	
					gotoBitS	r, FlagResetMeas, ResetMeas
					gotoBitS	r, FlagResetSleep, ResetSleep
					gotoBitS	r, FlagResetPOR, PORCheck
					goto		ResetUnknown

PORCheck:			gotoBitS	r, FlagResetButton, ResetButton
					gotoBitS	r, FlagResetWD, ResetWatchdog
					goto		ResetPOR	

ResetPOR:			ramadr		CounterResetButton
					clear		r
					ramadr		CounterResetWatchdog
					clear		r
					ramadr		CounterResetMeas
					clear		r
					ramadr		CounterResetSleep
					clear		r
					ramadr		CounterResetTotal
					clear		r
					ramadr		CounterResetUnknown
					clear		r
					
					ramadr		CounterResetPOR
					incr		r
					
					goto		End
					
ResetButton:		ramadr		CounterResetButton
					incr		r
					goto		End

ResetWatchdog:	ramadr		CounterResetWatchdog
					incr		r
					goto		End

ResetMeas:		ramadr		CounterResetMeas
					incr		r
					goto		End
					
ResetSleep:		ramadr		CounterResetSleep
					incr		r
					goto		End		
							
ResetUnknown:	ramadr		CounterResetUnknown
					incr		r
					goto		End		
					
End:				move		x, 0x9FFF
Dec:				decr		x
					gotoNE		Dec
					
					ramadr		ConfigReg17
					bitclr		r, IoPin4
					
					clrwdt
					stop
; --- EOF ---