;--------------------------------------------------------
;	File: Untitled.asm
;
;	Author: unknown
;--------------------------------------------------------

#include	"SpiConfig.h"

CONST ConfigRegAddr	48


CONST IoPin4				6 	; 17
CONST IoPin5				7 	; 17
CONST	TdcSleepMode		17 	; 1

					ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin5	
					
					move		y, 0x55
					ssnPulse
					sendSPI	y, 8

					ramadr		ConfigRegAddr + 17
					bitset		r, IoPin5	
					
					clrwdt
					stop
					
; --- EOF ---