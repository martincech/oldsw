;--------------------------------------------------------
;	File: First.asm
;
;	Author: Veit Electronics
;--------------------------------------------------------

#include "FirstConfig.h"
;--------------- RAM addresses in which values are stored-------------------------------------------
CONST	CalLow				1
CONST	CalHigh			7

;--------------- constants for RAM addresses used by the TDC---------------------------------------------------------

CONST ConfigReg17	65

CONST IoPin4			6
	
reset:				ramadr		ConfigReg17   ; program entry
					bitset		r, IoPin4
					
					ramadr 	CalHigh			; our preprocessing
					move		r, 0xAFFF
decrem:			decr 		r
					gotoNE		decrem
			
end:  				

					ramadr		ConfigReg17
					bitclr		r, IoPin4
					clrwdt
					stop						; program end
; --- EOF ---