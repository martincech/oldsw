;--------------------------------------------------------
;	File: Untitled.asm
;
;	Author: unknown
;--------------------------------------------------------
<COMMENT>
!!! User must edit hex file otherwise filter won't work !!!
Instructions below
<ENDCOMMENT>

#include	"_ConfigDefault.h"

#include	"_Bat2Definitions.h"

#include	"_RunResetCheckDefault.h"

;-------------------------------------------------------------------------------------------------------------------------------------
; ------------------------------------------------------ ALGORITHM ---------------------------------------------------------	
;-------------------------------------------------------------------------------------------------------------------------------------
ResetMeas:	
					ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin5				; clear interrupt flag
; check if value < 493 447

					ramadr		ControlRegister
					bitclr		r, VALID_DATA			; data not valid
					
					; IS_USING_FINE_CONTROL = IS_USING_FINE
					ramadr		ControlRegister
					bitclr		r, IS_USING_FINE_CONTROL
					ramadr		MyStatus
					skipBitC	r, IS_USING_FINE, 2
					ramadr		ControlRegister
					bitset		r, IS_USING_FINE_CONTROL


					ramadr		MyStatus

					gotoBitC	r, SWITCHED, DontSkip
					bitclr		r, SWITCHED
					gotoBitC	r, IS_USING_FINE, End


DontSkip:
					ramadr		RawValue
					move		r, x						; store raw value
					jsub		TranslateRaw
					ramadr		Weight
					move		r, z
					
PreFilter:	
					ramadr		PrefilterInput
					add		r, z
	
					ramadr		PrefilterCounter
					decr		r
					skipEQ		1
					goto		NotStable

					ramadr		PrefilterInput
					move		z, r
					clear		r
					
					ramadr		Prefilter
					move		y, r
					
					ramadr		PrefilterCounter
					move		r, y
					
					divmod		z, y


Filter:				
					jsub		FilterDo
					ramadr		FilterOutput
					move		r, x

CheckCalibration:
  					ramadr		ControlRegister
					gotoBitC	r, CALIBRATION, NormalOperation

					gotoBitC	r, CALIBRATION_LOW, CheckHigh
					ramadr		MyStatus
					skipBitC	r, IS_USING_FINE, 1
					jsub		SwitchAccuracyHw
					goto		DoInterrupt

CheckHigh:					
					gotoBitC	r, CALIBRATION_HIGH, DoInterrupt
					ramadr		MyStatus
					skipBitS	r, IS_USING_FINE, 1
					jsub		SwitchAccuracyHw
					goto		DoInterrupt



NormalOperation:
					nop
;-------------------------------------------------------- Low Pass ----------------------------------------------------------		
				
<COMMENT>
	!!! Merene hodnoty musi byt dostatecne male, aby nedoslo k preteceni pri vypoctu prumeru !!!
<ENDCOMMENT>				

<COMMENT>
rollAvg routines can handle up to 17 samples. To be able to handle more samples average them before they reach rollAvg routine
2 - 17 samples - /1 (no averaging)
18 - 34 samples - /2
35 - 68 samples - /4
<ENDCOMMENT>
LowPass:	
					; PreLowPassInput += FilterOutput
					ramadr		FilterOutput
					move		z, r
					ramadr		PreLowPassInput
					add		r, z
	
					ramadr		PreLowPassAveragingCounter
					decr		r

					gotoNE		CheckRange
					
					; compute new low pass value
ComputeLowPassInput:		
					ramadr		PreLowPassAveragingPeriod
					move		y, r
					ramadr		PreLowPassAveragingCounter
					move		r, y					; PreAvgCounter = PreAvgPeriod
					
					ramadr		PreLowPassInput
					move		x, r
					clear		r
					
Divide:				shiftR		y
					skipEQ		2
					shiftR		x						; PreLowPassInput /= 2
					goto		Divide

ComputeLowPass:
					jsub		RollAvgx
					ramadr		LowPassOutput
					move		r, x
					
;-------------------------------------------------------- Range ----------------------------------------------------------						
CheckRange:		
					ramadr		MyStatus
					gotoBitS	r, IS_USING_FINE, ComputeHighPass
					
CheckCoarseUnlock:		
				
					ramadr		LowPassOutput
					move		x, r
					ramadr		LastStableValue
					sub			x, r
					abs			x
					ramadr		CoarseSwitchoverRange
					compare	x, r
					skipPos	3
				; if(abs(LowPassOutput - LastStableValue) > CoarseSwitchoverAllowedRange)	{		
					; CAN_SWITCH_TO_FINE = true
					ramadr		MyStatus
					bitset		r, CAN_SWITCH_TO_FINE
					bitclr		r, STABLE_NOTIFIED
				; }	
					
ComputeHighPass:					
				; HighPass = abs(FilterOutput - LowPassOutput);
					ramadr		FilterOutput
					move		z, r
					ramadr		LowPassOutput
					move		x, r
					sub			x, z	
					abs			x

					ramadr		Range
					compare	x, r
				
					gotoPos	CheckStableWindow
					
				; if(HighPass > Range) {
					ramadr		ControlRegister
					bitclr		r, STABLE_VALUE
					
					ramadr		StableWindowCounter  
					clear		r

					ramadr		MyStatus
					bitclr		r, STABLE_NOTIFIED
					gotoBitC	r, IS_USING_FINE, NotStableEnd

					ramadr		FineSwitchoverRange
					compare	x, r
					skipPos	1
					jsub		SwitchAccuracyHw


NotStableEnd:
					goto		NotStable
				; } else 

;-------------------------------------------------------- Window ----------------------------------------------------------		

CheckStableWindow:
				
				; if(StableWindow != StableWindowCounter) {
					ramadr		StableWindow
					move		y, r
					ramadr		StableWindowCounter
					compare	y, r
					
					skipEQ		3
					; StableWindowCounter ++
					incr		r
					compare	y, r
					gotoNE		NotStable
				; }
					
				; if(StableWindow == StableWindowCounter) {
					ramadr		MyStatus
					skipBitS	r, IS_USING_FINE, 3
					skipBitC	r, CAN_SWITCH_TO_FINE, 1
					; if(!IS_USING_FINE && CAN_SWITCH_TO_FINE) {
						jsub		SwitchAccuracyHw
					; }
					goto		NotStable

					jsub		SwitchAccuracyHw

					; CAN_SWITCH_TO_FINE = 0
					ramadr		MyStatus
					gotoBitS	r, STABLE_NOTIFIED, NotStable
					bitclr		r, CAN_SWITCH_TO_FINE
					bitset		r, STABLE_NOTIFIED
					
					; STABLE_VALUE = 1
					ramadr		ControlRegister
					bitset		r, STABLE_VALUE

					; LastStableValue = LowPassOutput
					ramadr 	LowPassOutput
					move		x, r
					ramadr		LastStableValue
					move		r, x
					
					goto		DoInterrupt
				; }
					
NotStable:		
					ramadr		ControlRegister
					gotoBitC	r, SEND_ALL, MeasurementEnd		; should we send all?
DoInterrupt:		
					ramadr		ControlRegister
					bitset		r, VALID_DATA

					ramadr		ConfigRegAddr + 17
					bitset		r, IoPin5

MeasurementEnd:
					jsub		SwitchAccuracyPost
					goto 		End
					
;-------------------------------------------------------------------------------------------------------------------------------------					
; -------------------------------------------------------- INITIALISATION -------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------------------------				
ResetSleep:		
					ramadr		ControlRegister
					gotoBitC	r, WAKE_UP, End
MeasureStart:	
;-------------------------------------------------------- Factors Init ----------------------------------------------------------		
					jsub		ComputeFactors ; Call this soon!

					;jsub		SwitchAccuracyHw
					;jsub		SwitchAccuracyPost

					ramadr		MyStatus
					bitset		r, IS_USING_FINE
					bitclr		r, SWITCHED
					
					ramadr		StableWindowCounter
					clear		r

					ramadr		AveragingWindowCurrent
					move		r, 1

;-------------------------------------------------------- Prefilter Init ----------------------------------------------------------							
					jsub		PrefilterInit

;---------------------------------------------------------- Filter Init ------------------------------------------------------------		
					ramadr		Zero
					move		x, r
					jsub		FilterInit
					
;------------------------------------------------------ Low Pass Init ---------------------------------------------------------				
					ramadr		AveragingWindow
					move		x, r
					ramadr		AveragingWindowUnused
					move		y, r
					ramadr		PreLowPassAveragingPeriod
					move		r, 1
					
InitPreAvg:
					compare	x, AVG_MAX
					gotoPos	InitPreAvgEnd	; Filter OK, can leave cycle
					shiftL		r				; PreAvgPeriod *= 2
					shiftR		x				; AveragingWindow /= 2
					shiftR		y				; AveragingWindow /= 2
					goto		InitPreAvg		; Check again
InitPreAvgEnd:					
					compare	y, 0
					skipNE		1
					incr		y
					
					ramadr		AveragingWindowUnused
					move		r, y
					
					ramadr		AveragingWindow
					move		r, x
					
					; PreLowPassAveragingCounter = PreLowPassAveragingPeriod
					ramadr		PreLowPassAveragingPeriod
					move		y, r
					ramadr		PreLowPassAveragingCounter  ; Initiate, PreAvgCounter = PreAvgPeriod
					move		r, y
					
					; PreLowPassInput = 0
					ramadr		PreLowPassInput
					clear		r

					; AveragingWindowFine = ComputeNative(AveragingWindowFine)
					;ramadr		AveragingWindowFine
					;jsub		ComputeNative
					
					ramadr		Zero
					move		x, r
					jsub		RollAvgxInit
					
					; LowPassOutput = RawZero
					ramadr		Zero
					move		x, r
					ramadr		LowPassOutput
					move		r, x
					ramadr		FilterOutput
					move		r, x
					
					; wake up TDC
					ramadr		ConfigRegAddr + 1
					bitclr		r, TdcSleepMode	
					
End:				clrwdt
					stop

#include "_Bat2Subroutines.h"

; --- EOF ---