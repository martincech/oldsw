;--------------------------------------------------------
;	File: Untitled.asm
;
;	Author: unknown
;--------------------------------------------------------
<COMMENT>
!!! User must manually edit hex file otherwise filter won't work !!!
Instructions below
<ENDCOMMENT>

#include	"AlgConfig.h"


;CONST		initAvg_Addr		3120

; User RAM
<COMMENT>
CONST RAM_START_SEGMENT0		0
CONST RAM_LENGTH_SEGMENT0		16

CONST RAM_START_SEGMENT1		32
CONST RAM_LENGTH_SEGMENT1		16

; Used by ROM
CONST RAM_START_SEGMENT2		112
CONST RAM_LENGTH_SEGMENT2		16
<ENDCOMMENT>

CONST	FILTER_MAX					17

CONST	FILTER_INIT_WAIT				0
CONST	FILTER_STABLE_WAIT		1
CONST	FILTER_STABLE				2

; RAM variables
CONST ControlRegister			1
CONST	WAKE_UP					0
CONST DIAGNOSTICS				1
CONST STABLE_VALUE			2
CONST VALID_DATA				3

CONST Result				2
CONST Factor				3
CONST RawValue			4
CONST	HighPassAbs		5


CONST RawZero			34	
CONST RawFullRange		5
CONST Zero				36	
CONST FullRange			37


CONST	Duration			39
CONST	DurationCounter	40

CONST Range				41

CONST AveragingWindow				46

CONST	LowPassOutput		42	; Last output of low pass filter
CONST LowPassPeriod		43
CONST LowPassCounter	44
CONST	LowPassInput		45

CONST	AveragingWindowNative		47

; Registers
CONST StatusRegister			22
CONST FlagResetPOR			19 ; Power-On-Reset						flg_rstpwr
CONST FlagResetButton		18 ; POR - RST pin							flg_rstssn
CONST FlagResetWD			17 ; POR - Watchdog						flg_wdtalt
CONST FlagResetMeas			16 ; Wake-up - new measurement done		flg_endavg
CONST FlagResetSleep		15 ; Wake-up - regularly from sleep mode	flg_intav0

CONST ConfigRegAddr	48


CONST IoPin4				6 	; 17
CONST IoPin5				7 	; 17
CONST	TdcSleepMode		17 	; 1

	
ResetCheck:		ramadr		ConfigRegAddr + 17   ; program entry
					bitset		r, IoPin4
					
				
					ramadr		StatusRegister	
					gotoBitS	r, FlagResetPOR, PORCheck
					gotoBitS	r, FlagResetSleep, ResetSleep
					gotoBitS	r, FlagResetMeas, ResetMeas
					
					goto		ResetUnknown ; active measurement

PORCheck:			gotoBitS	r, FlagResetButton, ResetButton
					gotoBitS	r, FlagResetWD, ResetWatchdog
					
					goto		ResetPOR
				
	<COMMENT>								
					ramadr		StatusRegister	
					gotoBitS	r, FlagResetMeas, ResetMeas
					gotoBitS	r, FlagResetSleep, ResetSleep
					gotoBitS	r, FlagResetPOR, PORCheck
					goto		ResetUnknown

PORCheck:			gotoBitS	r, FlagResetButton, ResetButton
					gotoBitS	r, FlagResetWD, ResetWatchdog
					goto		ResetPOR	
	<ENDCOMMENT>	
ResetPOR:			

					ramadr		ControlRegister
					clear		r

					goto		End
					
ResetButton:		
					goto		End

ResetWatchdog:	
					goto		End

ResetMeas:	
					ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin5
					
					ramadr		ControlRegister
					bitclr		r, VALID_DATA
					
					ramadr		RawValue
					move		r, x

TranslateRaw:
					ramadr		RawZero
					move		z, r
					
					sub			z, x
					
					ramadr		Factor
					mult24		z, r
					
					ramadr		Zero
					add		z, r

					ramadr		Result
					move		r, z
					
					move		x, z	
<COMMENT>
	!!! Merene hodnoty musi byt dostatecne male, aby nedoslo k preteceni pri vypoctu prumeru !!!
<ENDCOMMENT>				
LowPass:	
					ramadr		LowPassInput
					add		r, x
	
					ramadr		LowPassCounter
					shiftR		r
					
					gotoEQ		ComputeLowPass
					ramadr		LowPassOutput		; else take old value
					move		x, r
					goto		Detection
					
					; compute new low pass value
ComputeLowPass:		
					ramadr		LowPassPeriod
					move		y, r
					ramadr		LowPassCounter
					move		r, y
					ramadr		LowPassInput
					move		x, r
					
Iter:				shiftR		y
					skipEQ		2
					shiftR		x
					goto		Iter

					ramadr		AveragingWindowNative
					move		z, r
					jsub		FAKE_AVG_ROUTINE			; rollAvg: Replace with EC21
					
					ramadr		LowPassOutput
					move		r, x
					
					ramadr		LowPassInput
					clear		r
					
					ramadr		LowPassCounter
					clear		r
					
					
Detection:				
					ramadr		Result
					move		z, r
					
					sub			x, z		; x = Value - LowPassOutput
					
					abs			x		; absolute value
					
					ramadr		HighPassAbs
					move		r, x
					
					ramadr		Range
					
					compare	x, r		; Range - High pass
				

					gotoPos	CheckDuration
					
					ramadr		DurationCounter  
					clear		r				; DurationCounter = 0
					ramadr		ControlRegister
					bitclr		r, STABLE_VALUE
					goto		TooShortWindow

CheckDuration:
					ramadr		Duration
					move		y, r

					ramadr		DurationCounter

					compare	y, r			; Duration - DurationCounter
					
					skipEQ		3
					incr		r
					compare	y, r
					gotoNE		TooShortWindow
					
					ramadr		ControlRegister
					skipBitS	r, STABLE_VALUE, 2
					bitset		r, STABLE_VALUE
					goto		DoInterrupt
					
TooShortWindow:
					ramadr		ControlRegister
					gotoBitC	r, DIAGNOSTICS, MeasEnd
				
DoInterrupt:		
					bitset		r, VALID_DATA

					ramadr		ConfigRegAddr + 17
					bitset		r, IoPin5
					
MeasEnd:
					goto		End
					
					
ResetSleep:		



					;ramadr		ControlRegister
					;incr		r
				
					ramadr		ControlRegister
					gotoBitC	r, WAKE_UP, End

					;ramadr		RawZero
					;move		r, 11708
					;ramadr		RawFullRange
				;	move		r, 220000
				;	ramadr		Zero
				;	move		r, 0
				;	ramadr		FullRange
				;	move		r, 10000

					
MeasureStart:	
; Calculate factor
					ramadr		RawZero
					move		x, r
					
					ramadr		RawFullRange
					
					sub			x, r

					ramadr		FullRange	
					move		y, r
					
					div24		y, x						; (FullRange << 24) / (RawFullRange - RawZero)
					
					ramadr		Factor
					move		r, y

; Clear
					ramadr		DurationCounter
					clear		r

					ramadr		LowPassOutput
					clear		r
					
					ramadr		LowPassInput
					clear		r

; Filter value is limited - decrease it if neccessary
					ramadr		AveragingWindow
					move		x, r
					ramadr		LowPassPeriod
					move		r, 1			; LowPassPeriod = 1

FilterDivide:					
					compare	x, FILTER_MAX
					skipPos	3			; Filter OK, can leave cycle
					shiftL		r			; LowPassPeriod *= 2
					shiftR		x			; Filter /= 2
					goto		FilterDivide	; Check again

					
					ramadr		AveragingWindow		; Store filter
					move		r, x
					
					
					move		x, r
					ramadr		LowPassCounter  ; Initiate, LowPassCounter = LowPassPeriod
					move		r, x


<COMMENT>
Compiler doesn't allow initAvg routine to be initialized with non-constant second parameter.

By examination of hex file, one can see that initAvg x, CONST compiles to:
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3120); jsub2_0 (3120);

and rollAvg x, CONST
loadByte_1 (2,some_number); loadByte_0 (2,some_number); jsub2_1 (3105);  jsub2_0 (3105);

where:
some_number = f(CONST) = (CONST << (CONST - 1)) | ((1 << (CONST - 2)) - 1)

After compilation replace manually two fake calls of FAKE_AVG_ROUTINE with EC and 30 opcodes ( = jsub(3120), INIT ) or EC and 21 opcodes ( =  jsub (3105), ROLL )

<ENDCOMMENT>
					; Recalculation

					ramadr		AveragingWindow
					move		x, r
					move		y, r

AWShift1:								; Calculate W << (W - 1)
					compare	y, 1
					skipEQ		3
					decr		y
					shiftL		x
					goto		AWShift1
					
					move		z, 1
					move		y, r
AWShift2:								; Calculate 1 << (W - 2)
					compare	y, 2
					skipEQ		3
					decr		y
					shiftL		z
					goto		AWShift2
					
					decr		z
					
					or			z, x

				 	ramadr		AveragingWindowNative
				 	move		r, z
				 	
					ramadr		RawZero	; offset
					move		x, r
					
					jsub		FAKE_AVG_ROUTINE			; initAvg: Replace with EC30

				
					
					ramadr		ConfigRegAddr + 1
					bitclr		r, TdcSleepMode
			
					goto		End		
							
ResetUnknown:	
					goto		End		
					
End:				ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin4
					
					clrwdt
					stop
					
FAKE_AVG_ROUTINE:
					jsubret
; --- EOF ---