;--------------------------------------------------------
;	File: Untitled.asm
;
;	Author: unknown
;--------------------------------------------------------

#include	"AlgConfig.h"


CONST		initAvg_Addr		3120

; User RAM
<COMMENT>
CONST RAM_START_SEGMENT0		0
CONST RAM_LENGTH_SEGMENT0		16

CONST RAM_START_SEGMENT1		32
CONST RAM_LENGTH_SEGMENT1		16

; Used by ROM
CONST RAM_START_SEGMENT2		112
CONST RAM_LENGTH_SEGMENT2		16
<ENDCOMMENT>

CONST	FILTER_INIT_WAIT				0
CONST	FILTER_STABLE_WAIT		1
CONST	FILTER_STABLE				2

; RAM variables
CONST ControlRegister			1
CONST	WAKE_UP					0

CONST ResultRegister				2

CONST RawRegister				4

CONST FilterState			32
CONST	Inversion			33
CONST RawZero			34	
CONST RawFullRange		5
CONST Zero				36	
CONST FullRange			37
CONST	AveragingWindow	38


CONST Factor				3

; Registers
CONST StatusRegister			22
CONST FlagResetPOR			19 ; Power-On-Reset
CONST FlagResetButton		18 ; POR - RST pin
CONST FlagResetWD			17 ; POR - Watchdog
CONST FlagResetMeas			16 ; Wake-up - new measurement done
CONST FlagResetSleep		15 ; Wake-up - regularly from sleep mode

CONST ConfigRegAddr	48


CONST IoPin4				6 	; 17
CONST IoPin5				7 	; 17
CONST	TdcSleepMode		17 	; 1


ResetCheck:		ramadr		ConfigRegAddr + 17   ; program entry
					bitset		r, IoPin4
					
					ramadr		StatusRegister	
					gotoBitS	r, FlagResetMeas, ResetMeas
					gotoBitS	r, FlagResetSleep, ResetSleep
					gotoBitS	r, FlagResetPOR, PORCheck
					goto		ResetUnknown

PORCheck:			gotoBitS	r, FlagResetButton, ResetButton
					gotoBitS	r, FlagResetWD, ResetWatchdog
					goto		ResetPOR	

ResetPOR:			ramadr		FilterState
					clear		r
					ramadr		ControlRegister
					clear		r
					goto		End
					
ResetButton:		
					goto		End

ResetWatchdog:	
					goto		End

ResetMeas:	
					ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin5
					
					rollAvg		x, 17

					ramadr		RawRegister
					move		r, x

					ramadr		RawZero
					move		y, r
					
					sub			y, x
					
					
					ramadr		Factor
					mult24		y, r
					
					ramadr		Zero
					add		y, r



					ramadr		ResultRegister
					move		r, y
					
					ramadr		ConfigRegAddr + 17
					bitset		r, IoPin5
					
					goto		End
					
					
ResetSleep:		
<COMMENT>
					; Recalculate AveragingWindow to native value: Native = (W << (W - 1)) | ((1 << (W - 2)) - 1)
					ramadr		AveragingWindow
					move		x, r
					decr		x
					shiftL		r, x
					
					decr		x
					move		y, 1
					shiftL		y, x
					decr		y
					
					or			r, y
<ENDCOMMENT>
					ramadr		ControlRegister
					incr		r
					ramadr		ControlRegister
					gotoBitC	r, 1, End

					ramadr		RawZero
					move		r, 11708
					ramadr		RawFullRange
					move		r, 220000
					ramadr		Zero
					move		r, 0
					ramadr		FullRange
					move		r, 10000

					
MeasureStart:		
					ramadr		FullRange	
					move		x, r
					ramadr		RawFullRange
					div24		x, r							; (FullRange << 24) / RawFullRange
					
					ramadr		Factor
					move		r, x

					ramadr		RawZero
					move		x, r
					initAvg		x, 17
					
					ramadr		ConfigRegAddr + 1
					bitclr		r, TdcSleepMode
					
					goto		End		
							
ResetUnknown:	
					goto		End		
					
End:				ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin4
					
					clrwdt
					stop
; --- EOF ---