;--------------------------------------------------------
;	File: Bat2SwitchingFilter.asm
;
;	Author: Veit Electronics
;--------------------------------------------------------

#include	"_ConfigDefault.h"

#include	"_Bat2Definitions.h"

CONST		SWITCH1		10
CONST		SWITCH2		40

#include	"_RunResetCheckDefault.h"

ResetMeas:	
					ramadr		ConfigRegAddr + 17
					bitclr		r, IoPin5				; clear interrupt flag

					ramadr		ControlRegister
					bitclr		r, VALID_DATA			; data not valid


					; IS_USING_FINE_CONTROL = IS_USING_FINE
					ramadr		ControlRegister
					bitclr		r, IS_USING_FINE_CONTROL
					ramadr		MyStatus
					skipBitC	r, IS_USING_FINE, 2
					ramadr		ControlRegister
					bitset		r, IS_USING_FINE_CONTROL

					ramadr		MyStatus
					skipBitC	r, SWITCHED, 2
					bitclr		r, SWITCHED
					gotoBitC	r, IS_USING_FINE, End

					ramadr		RawValue
					move		r, x
					jsub		TranslateRaw
					ramadr		Weight
					move		r, z

Prefilter:
					ramadr		PrefilterInput
					add		r, z
	
					ramadr		PrefilterCounter
					decr		r
					skipEQ		1
					goto		MeasurementEnd

					ramadr		PrefilterInput
					move		z, r
					clear		r
					
					ramadr		Prefilter
					move		y, r
					
					ramadr		PrefilterCounter
					move		r, y
					
					divmod		z, y

Filter:
					jsub		FilterDo
					ramadr		FilterOutput
					move		r, x

					
CheckCalibration:
					ramadr		ControlRegister
					gotoBitC	r, CALIBRATION, NormalOperation

					ramadr		ControlRegister
					gotoBitC	r, CALIBRATION_LOW, CheckHigh
					bitclr		r, CALIBRATION_LOW
					ramadr		MyStatus
					gotoBitC	r, IS_USING_FINE, DoInterrupt
					jsub		SwitchAccuracyHw
					goto		DoInterrupt

CheckHigh:					
					ramadr		ControlRegister
					gotoBitC	r, CALIBRATION_HIGH, DoInterrupt
					bitclr		r, CALIBRATION_HIGH
					ramadr		MyStatus
					gotoBitS	r, IS_USING_FINE, DoInterrupt
					jsub		SwitchAccuracyHw
					goto		DoInterrupt

NormalOperation:



					ramadr		Switcher
					incr		r
					
					compare	r, SWITCH1
					gotoNE  	CheckSecondSwitch
					jsub		SwitchAccuracyHw
CheckSecondSwitch:					
					compare	r, SWITCH2
					gotoNE  	DoInterrupt
					clear		r
					jsub		SwitchAccuracyHw

DoInterrupt:		
					ramadr		ControlRegister
					bitset		r, VALID_DATA

					ramadr		ConfigRegAddr + 17
					bitset		r, IoPin5

MeasurementEnd:
					jsub		SwitchAccuracyPost
					goto 		End
;-------------------------------------------------------------------------------------------------------------------------------------					
; -------------------------------------------------------- INITIALISATION -------------------------------------------------
;-------------------------------------------------------------------------------------------------------------------------------------				
ResetSleep:		
					ramadr		ControlRegister
					gotoBitC	r, WAKE_UP, End
MeasureStart:	
					ramadr		Switcher
					clear		r
					ramadr		Deny
					clear		r

					jsub		PrefilterInit
					jsub		FilterInit
					jsub 		ComputeFactors

					jsub		SwitchAccuracyHw
					jsub		SwitchAccuracyPost

					ramadr		MyStatus
					bitset		r, IS_USING_FINE
					bitclr		r, SWITCHED
					
					ramadr		ConfigRegAddr + 1
					bitclr		r, TdcSleepMode	; wake up TDC
					
End:				
					clrwdt
					stop


#include "_Bat2Subroutines.h"

; --- EOF ---