﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace WindowsFormsApplication1 {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            // Vytvorit adresar C:\Paradox a do nej zkopirovat tabulku Sms.db ze SW k BAT2 - funguje
            
            // Vytvorim spojeni
            using (OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\\Paradox;Extended Properties=Paradox 5.x;")) {
                connection.Open();
                using (OleDbCommand command = connection.CreateCommand()) {
                    command.CommandText = "SELECT * FROM Sms ORDER BY DAY_NUMBER";
                    using (OleDbDataReader reader = command.ExecuteReader()) {
                        listBox1.Items.Clear();
                        while (reader.Read()) {
                            StringBuilder builder = new StringBuilder();

                            for (int i = 0; i < reader.FieldCount; i++) {
                                builder.Append(reader[i] + "   |   ");
                            }

                            listBox1.Items.Add(builder.ToString());
                        }
                    }
                }
            }
        }

    }
}
