//******************************************************************************
//
//   AtFlash.cpp   Atmel flash high level module
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "AtFlash.h"

#pragma package(smart_init)

#define FLASH_FLUSH_ADDRESS  0xFFFFFFFEL    // invalid virtual address - flush
#define INVALID_PAGE         0xFFFF         // invalid page address

// AT45DB161 only :
#define MkBufferAddress( a)     ((a) & 0x1FF)
#define MkPageAddress( a)       ((word)((a) >> 9))

//******************************************************************************
// Constructor
//******************************************************************************

TAtFlash::TAtFlash() : TAt45dbxx()
// Constructor
{
   WritePage = INVALID_PAGE;
   WriteOffset = 0;
   ReadPage  = INVALID_PAGE;
   ReadOffset  = 0;
} // TAtFlash

//******************************************************************************
// Destructor
//******************************************************************************

TAtFlash::~TAtFlash()
// Destructor
{
} // ~TAtFlash

//******************************************************************************
// Read
//******************************************************************************

bool TAtFlash::Read( dword Address, void *Data, int Length)
// Read from virtual <Address>
{
   if( Length == 0){
      IERROR;
   }
   ReadPage   = MkPageAddress(   Address);
   ReadOffset = MkBufferAddress( Address);
   int Size    = FLASH_VIRTUAL_PAGE - ReadOffset;         // up to end of page
   int Index   = 0;                                        // into Data array
   if( Size > Length){
      Size = Length;                                       // read inside page
   }
   do {
      if( !ReadArray( ReadPage, ReadOffset, &((byte *)Data)[ Index], Size)){
         return( false);
      }
      Length -= Size;                  // remainder of data
      Index  += Size;                  // move data pointer
      // next page :
      ReadPage++;
      ReadOffset = 0;
      // next fragment :
      Size = Length;
      if( Size > FLASH_VIRTUAL_PAGE){
         Size = FLASH_VIRTUAL_PAGE;    // cut to page size
      }
   } while( Length);
   return( true);
} // Read

//******************************************************************************
// Write
//******************************************************************************

bool TAtFlash::Write( dword Address, void *Data, int Length)
// Write to virtual <Address>
{
   if( Length == 0){
      IERROR;
   }
   word NewPage = MkPageAddress( Address);
   if( NewPage != WritePage){
      // page change
      SwapBuffer( WritePage, NewPage);
   } // else same page
   WritePage   = NewPage;
   WriteOffset = MkBufferAddress( Address);
   int Size    = FLASH_VIRTUAL_PAGE - WriteOffset;         // up to end of page
   int Index   = 0;                    // into Data array
   if( Size > Length){
      // does fit inside page 
      Size = Length;
   } else {
      // at least one-times up to end of page
      do {
         // write into buffer :
         if( !WriteBuffer( WriteOffset, &((byte *)Data)[ Index], Size)){
            return( false);
         }
         // save buffer {
         if( !SwapBuffer( WritePage, WritePage + 1)){
            return( false);
         }
         Length -= Size;               // remainder of data
         Index  += Size;               // move data pointer
         // next page :
         WritePage++;
         WriteOffset = 0;
         // next fragment :
         Size = Length;
         if( Size > FLASH_VIRTUAL_PAGE){
            Size = FLASH_VIRTUAL_PAGE; // cut to page size
         }
      } while( Size == FLASH_VIRTUAL_PAGE);
      // remainder doesn't fill whole page
   }
   // test remainder of page :
   if( Size){
      // write page fragment into buffer only :
      if( !WriteBuffer( WriteOffset, &((byte *)Data)[ Index], Size)){
         return( false);
      }
      WriteOffset += Size;
   } else {
      WritePage = INVALID_PAGE;        // new page without modification, make it invalid
   }
   return( true);
} // Write

//******************************************************************************
// Flush
//******************************************************************************

bool TAtFlash::Flush()
// Flush all written data
{
   if( !SwapBuffer( WritePage, INVALID_PAGE)){
      return( false);
   }
   WritePage = INVALID_PAGE;
   return( true);
} // Flush

//------------------------------------------------------------------------------

//******************************************************************************
// Swap
//******************************************************************************

bool TAtFlash::SwapBuffer( word OldPage, word NewPage)
// Save <OldPage>, read <NewPage>
{
   if( OldPage < FLASH_PAGES){
      // in the range, save the old page
      if( !SaveBuffer( OldPage)){
         return( false);
      }
      if( !WaitForReady()){
         return( false);
      }
   }
   if( NewPage >= FLASH_PAGES){
      return( true);                   // out of capacity, don't load the new page
   }
   // load the new page into buffer :
   if( !LoadBuffer( NewPage)){
      return( false);
   }
   if( !WaitForReady()){
      return( false);
   }
   return( true);
} // SwapBuffer

