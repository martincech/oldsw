//******************************************************************************
//
//   Eeprom.h      EEPROM AT95256
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef EepromH
#define EepromH

#include "UsbSpi.h"

//******************************************************************************
// Eeprom
//******************************************************************************

class TEeprom {
public :
   TEeprom( int PageSize = 32);
   // Constructor
   ~TEeprom();
   // Destructor
   bool Connected();
   // Check if the adapter/module are present

   bool ReadPage( word Address, void *Data, int Length);
   // Read data from EEPROM (limited size)
   bool Read( word Address, void *Data, int Length);
   // Read data from EEPROM (any size)
   bool WritePage( word Address, void *Data, int Length);
   // Write data into EEPROM page (please check the page boundaries)
   bool Write( word Address, void *Data, int Length);
   // Write data into EEPROM (any size)

   __property TUsbSpi *Spi = {read=FSpi};
//------------------------------------------------------------------------------
protected :
   TUsbSpi *FSpi;        // SPI via USB
   int      FPageSize;   // memory page size            

   bool CheckConnect();
   // Try connect adapter

   bool Wren();
   // Write enable command
   bool Wrdi();
   // Write disable command
   bool Rdsr( byte &Value);
   // Read status register
   bool Wrsr( byte Value);
   // Write status register
   bool WaitForReady();
   // Wait for flash ready
}; // TEeprom

#endif
