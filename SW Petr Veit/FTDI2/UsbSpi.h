//******************************************************************************
//
//   UsbSpi.h     USB SPI mode
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef UsbSpiH
#define UsbSpiH

#ifndef LoggerH
   #include "../Library/Serial/Logger.h"
#endif

#include "..\Library\Unisys\SysDef.h"
#include "FTD2XX.h"

//******************************************************************************
// TUsbSpi
//******************************************************************************

class TUsbSpi
{
public:
   // SPI Clock idle level :
   typedef enum {
      CLOCK_LOW,
      CLOCK_HIGH,
   } TClockPolarity;
   // SPI Read strobe :
   typedef enum {
      READ_ON_FALL,
      READ_ON_RISE,
   } TReadStrobe;
   // SPI Write strobe :
   typedef enum {
      WRITE_ON_FALL,
      WRITE_ON_RISE,
   } TWriteStrobe;

   //--- class functions :
   static bool Locate( TName Name, TName AuxName, TIdentifier &Identifier);
   // Find device by <Name>, returns <Identifier>
   static bool Access( TIdentifier Identifier);
   // Check device access by <Identifier>

   TUsbSpi();
   // Constructor
   virtual ~TUsbSpi();
   // Destructor

   //--- inherited from TSerial :

   bool Open( TIdentifier Identifier);
   // Open device by <Identifier>
   void Close();
   // Close device

   bool SetClockRate( int Hz);
   // Set clock rate [Hz]
   void SetMode( TClockPolarity Polarity, TReadStrobe ReadStrobe, TWriteStrobe WriteStrobe);
   // Set SPI mode
   bool ClrCS();
   // Device chipselect L
   bool SetCS();
   // Device chipselect H
   bool Write( void *Data, int Length);
   // Write <Data> of size <Length>
   bool Read( void *Data, int Length);
   // Read data <Data> of size <Length>
   bool WriteByte( byte Data);
   // Write single byte
   bool ReadByte( byte &Data);
   // Read single byte

   void Flush();
   // Make input/output queue empty

   // USB implementation :
   __property TIdentifier Identifier    = {read=FIdentifier};
   __property TName       Name          = {read=GetName};
   __property bool        IsOpen        = {read=GetIsOpen};
   __property TString     SerialNumber  = {read=FSerialNumber};
   __property int         TxLatency     = {read=FTxLatency, write=SetTxLatency};
   __property TLogger    *Logger        = {read=FLogger, write=FLogger};

//------------------------------------------------------------------------------
protected:
   TIdentifier FIdentifier;        // USB identifier
   FT_HANDLE   FHandle;            // USB device handle
   TString     FUsbName;           // USB device name
   TString     FSerialNumber;      // USB device serial number
   int         FTxLatency;         // USB device output buffer latency
   TLogger    *FLogger;            // raw data logger

   int         FClockRate;         // SPI clock rate
   bool        FClockHigh;         // SPI clock idle level
   byte        FModeMask;          // SPI mode mask

   virtual TName GetName();
   // Get device name
   virtual bool  GetIsOpen();
   // Check if device is opened

   void SetTxLatency( int Latency);
   // Set devices output buffer latency
   bool Synchronize();
   // Synchronize MPSSE device
   bool Send( void *Data, int Length);
   // Send <Data> of size <Length>
   int Receive( void *Data, int Length);
   // Receive data <Data> of size <Length>, returns Rx length
}; // TUsbSpi

#endif
