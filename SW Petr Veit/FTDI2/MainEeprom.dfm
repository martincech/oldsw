object MainForm: TMainForm
  Left = 269
  Top = 188
  Width = 786
  Height = 652
  Caption = 'Flash demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnDestroy = Destroy
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object LblAdapter: TLabel
    Left = 70
    Top = 608
    Width = 6
    Height = 13
    Caption = '?'
  end
  object Label1: TLabel
    Left = 16
    Top = 608
    Width = 43
    Height = 13
    Caption = 'Adapter :'
  end
  object Label2: TLabel
    Left = 328
    Top = 608
    Width = 41
    Height = 13
    Caption = 'Module :'
  end
  object LblModule: TLabel
    Left = 376
    Top = 608
    Width = 55
    Height = 13
    Caption = 'Not present'
  end
  object Label3: TLabel
    Left = 640
    Top = 290
    Width = 37
    Height = 13
    Caption = '@ (hex)'
  end
  object Label4: TLabel
    Left = 639
    Top = 331
    Width = 47
    Height = 13
    Caption = 'Size (dec)'
  end
  object MainMemo: TMemo
    Left = 0
    Top = 32
    Width = 633
    Height = 569
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 0
  end
  object BtnRead: TButton
    Left = 664
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 1
    OnClick = BtnReadClick
  end
  object BtnCheck: TCheckBox
    Left = 8
    Top = 8
    Width = 97
    Height = 17
    Caption = 'Check'
    Checked = True
    State = cbChecked
    TabOrder = 2
  end
  object BtnWrite: TButton
    Left = 664
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Write'
    TabOrder = 3
    OnClick = BtnWriteClick
  end
  object BtnReadAll: TButton
    Left = 664
    Top = 240
    Width = 75
    Height = 25
    Caption = 'Read All'
    TabOrder = 4
    OnClick = BtnReadAllClick
  end
  object TxtAddress: TEdit
    Left = 692
    Top = 288
    Width = 73
    Height = 21
    TabOrder = 5
    Text = '0'
  end
  object TxtSize: TEdit
    Left = 692
    Top = 328
    Width = 73
    Height = 21
    TabOrder = 6
    Text = '512'
  end
  object BtnWriteFragment: TButton
    Left = 664
    Top = 368
    Width = 81
    Height = 25
    Caption = 'Write Fragment'
    TabOrder = 7
    OnClick = BtnWriteFragmentClick
  end
  object Timer: TTimer
    OnTimer = TimerTimer
    Left = 712
    Top = 16
  end
end
