//******************************************************************************
//
//   Main.cpp     SPI diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
#include "UsbSpi.h"
#include <stdio.h>
#include <time.h>

#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

#define USB_DEVICE_NAME   "VEIT USB Reader A"           // USB adapter name
#define USB_DUTCHMAN_NAME "ComScale USB Reader A"       // USB adapter name

TUsbSpi *Usb;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// On Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt    = new TCrt( MainMemo);
   Logger = new TCrtLogger( Crt);
   Usb    = new TUsbSpi;
   Usb->Logger = Logger;
   TIdentifier Identifier;
   if( !Usb->Locate( USB_DEVICE_NAME, USB_DUTCHMAN_NAME, Identifier)){
      Crt->printf( "Unable locate device\n");
      return;
   }
   if( !Usb->Open( Identifier)){
      Crt->printf( "Unable open device\n");
      return;
   }
   Crt->printf( "Open '%s'\n", Usb->Name);
} // Create

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Crt;
   delete Logger;
   delete Usb;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Init
//******************************************************************************

void __fastcall TMainForm::BtnInitClick(TObject *Sender)
{
   int      Rate;
   sscanf( MainRate->Text.c_str(),    "%d", &Rate);
   Usb->Flush();
   Usb->SetMode( TUsbSpi::CLOCK_LOW,
                 TUsbSpi::READ_ON_FALL,
                 TUsbSpi::WRITE_ON_FALL);
   if( !Usb->SetClockRate( Rate)){
      Crt->printf( "Unable set sampling rate\n");
      return;
   }
   if( !Usb->SetCS()){
      Crt->printf( "Unable deselect\n");
      return;
   }
//   Usb->Flush();
} // BtnInitClick

//******************************************************************************
// Send
//******************************************************************************

void __fastcall TMainForm::MainSendClick(TObject *Sender)
{
   unsigned Data;
   sscanf( MainData->Text.c_str(),    "%X", &Data);
   if( !Usb->WriteByte( Data)){
      Crt->printf( "Tx error\n");
      return;
   }
} // MainSendClick

//******************************************************************************
// Read
//******************************************************************************

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   byte Data;
   if( !Usb->ReadByte( Data)){
      Crt->printf( "Rx error\n");
      LblRead->Caption = "?";
      return;
   }
   LblRead->Caption = IntToHex( Data, 2);
} // BtnReadClick

//******************************************************************************
// Receive
//******************************************************************************

void __fastcall TMainForm::BtnReceiveClick(TObject *Sender)
{
/*
   if( !Usb->DumpRx()){
      Crt->printf( "No data\n");
   }
*/
} // BtnReceiveClick

//******************************************************************************
// Select/Deselect
//******************************************************************************

void __fastcall TMainForm::BtnSelectClick(TObject *Sender)
{
   if( !Usb->ClrCS()){
      Crt->printf( "CS error\n");
   }
} // BtnSelectClick

void __fastcall TMainForm::BtnDeselectClick(TObject *Sender)
{
   if( !Usb->SetCS()){
      Crt->printf( "CS error\n");
   }
} // BtnDeselectClick

