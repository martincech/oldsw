object MainForm: TMainForm
  Left = 303
  Top = 233
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'VEIT USB driver uninstall'
  ClientHeight = 258
  ClientWidth = 491
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 24
    Width = 200
    Height = 13
    Caption = '1) Disconnect the USB reader from the PC'
  end
  object Label2: TLabel
    Left = 24
    Top = 56
    Width = 443
    Height = 13
    Caption = 
      '2) Remove the old drivers in Control panel - Add/Remove programs' +
      ' - VEIT USB Reader Driver'
  end
  object Label3: TLabel
    Left = 24
    Top = 88
    Width = 213
    Height = 13
    Caption = '3) Run the Uninstall.exe to finish the removal:'
  end
  object Label4: TLabel
    Left = 24
    Top = 120
    Width = 63
    Height = 13
    Caption = '4) Restart PC'
  end
  object Label5: TLabel
    Left = 24
    Top = 152
    Width = 449
    Height = 33
    AutoSize = False
    Caption = 
      '5) Connect the USB reader. If no new device is detected, remove ' +
      'VEIT USB Reader A and VEIT USB Reader B in Device manager (right' +
      '-click and choose Uninstall)'
    WordWrap = True
  end
  object Label6: TLabel
    Left = 24
    Top = 192
    Width = 142
    Height = 13
    Caption = '6) Disconnect the USB reader'
  end
  object Label7: TLabel
    Left = 24
    Top = 224
    Width = 235
    Height = 13
    Caption = '7) Connect the USB reader and install new drivers'
  end
  object Button1: TButton
    Left = 248
    Top = 83
    Width = 75
    Height = 25
    Caption = 'Uninstall'
    TabOrder = 0
    OnClick = Button1Click
  end
end
