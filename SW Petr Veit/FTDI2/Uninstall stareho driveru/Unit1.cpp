//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <registry.hpp>


#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void TMainForm::DeleteKey(TRegistry *Registry, AnsiString Key) {
  // Vymaze klic <Key>
  try {
    if (!Registry->KeyExists(Key)) {
      ShowMessage("<" + Key + "> doesn't exist");
      throw 1;
    }
    if (!Registry->DeleteKey(Key)) {
      ShowMessage("Canot delete <" + Key + ">");
      throw 1;
    }
  } catch(...) {}
}

#define KEY     "SYSTEM\\CurrentControlSet\\Services\\FTCD2XX"

void __fastcall TMainForm::Button1Click(TObject *Sender)
{
  TRegistry *Registry = new TRegistry();

  Registry->RootKey = HKEY_LOCAL_MACHINE;

  DeleteKey(Registry, KEY);

  delete Registry;

  ShowMessage("Uninstall completed, you must RESTART your PC before connecting the USB reader.");
}
//---------------------------------------------------------------------------
