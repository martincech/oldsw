//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("SpiDemo.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("UsbSpi.cpp");
USELIB("..\FTDI-USB\FTDI.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Packet Demo";
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
