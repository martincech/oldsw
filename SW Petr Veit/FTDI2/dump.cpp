   bool DumpRx();
   // Dump Rx buffer to Logger

//******************************************************************************
// Dump Rx
//******************************************************************************

bool TUsbSpi::DumpRx()
// Dump Rx buffer Logger
{
   if( !IsOpen){
      return( false);
   }
   DWORD Available;
   DWORD r;
   byte  RxData;
   if_fterr( FT_GetQueueStatus( FHandle, &Available)){
      RwdReport( "Rx queue error");
      return( false);
   }
   if( Available == 0){
      return( false);
   }
   for( DWORD i = 0; i < Available; i++){
      if( !FT_W32_ReadFile( FHandle, &RxData, 1, &r, NULL)){
         RwdReport( "Rx error");
         return( false);
      }
      RwdRx( &RxData, r);
   }
   return( true);
} // DumpRx
