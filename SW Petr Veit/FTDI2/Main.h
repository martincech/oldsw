//******************************************************************************
//
//   Main.h        TX diagnostics
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include "../Crt/Crt.h"
#include "../Serial/CrtLogger.h"

#include <ComCtrls.hpp>
//---------------------------------------------------------------------------

class TMainForm : public TForm
{
__published:	// IDE-managed Components
   TMemo *MainMemo;
   TStatusBar *StatusBar1;
   TButton *MainSend;
   TLabel *Label4;
   TEdit *MainData;
        TEdit *MainRate;
        TLabel *Label1;
        TButton *BtnInit;
        TButton *BtnRead;
        TLabel *LblRead;
        TButton *BtnReceive;
        TButton *BtnSelect;
        TButton *BtnDeselect;
   void __fastcall Create(TObject *Sender);
   void __fastcall Destroy(TObject *Sender);
   void __fastcall MemoResize(TObject *Sender);
   void __fastcall MainSendClick(TObject *Sender);
        void __fastcall BtnInitClick(TObject *Sender);
        void __fastcall BtnReadClick(TObject *Sender);
        void __fastcall BtnReceiveClick(TObject *Sender);
        void __fastcall BtnSelectClick(TObject *Sender);
        void __fastcall BtnDeselectClick(TObject *Sender);
public :	// User declarations
   TCrt          *Crt;
   __fastcall TMainForm(TComponent* Owner);
};

//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------

#endif
