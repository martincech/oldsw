//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("ModuleDemo.res");
USEFORM("MainModule.cpp", MainForm);
USEUNIT("UsbSpi.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("At45dbxx.cpp");
USEUNIT("AtFlash.cpp");
USELIB("..\FTDI-USB\FTDI.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
