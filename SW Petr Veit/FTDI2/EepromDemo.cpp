//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("EepromDemo.res");
USEFORM("MainEeprom.cpp", MainForm);
USEUNIT("UsbSpi.cpp");
USEUNIT("..\Library\CRT\Crt.cpp");
USEUNIT("..\Library\Serial\CrtLogger.cpp");
USEUNIT("Eeprom.cpp");
USELIB("..\FTDI-USB\FTDI.lib");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
