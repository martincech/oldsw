//******************************************************************************
//
//   At45dbxx.cpp  Atmel flash AT45DBxx
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "At45dbxx.h"

#pragma package(smart_init)

#define USB_DEVICE_NAME   "VEIT USB Reader A"           // USB adapter name
#define USB_DUTCHMAN_NAME "ComScale USB Reader A"       // USB adapter name

#define SPI_CLOCK_RATE  1000000        // clock rate [Hz]

// status register :
#define FLASH_STATUS_RDY   0x80        // ready bit
#define FLASH_STATUS_COMP  0x40        // comparation flag
// ready timeout :
#define FLASH_TIMEOUT        50        // max. busy duration [ms]
// commands :
#define FLASH_ARRAY_READ      0xE8     // SPI mode 0 or 3
#define FLASH_PAGE_READ       0xD2
#define FLASH_BUFFER1_READ    0xD4
#define FLASH_BUFFER2_READ    0xD6
#define FLASH_STATUS_READ     0xD7

#define FLASH_BUFFER1_WRITE   0x84
#define FLASH_BUFFER2_WRITE   0x87
#define FLASH_BUFFER1_SAVE    0x83     // buffer 1 to main memory page with Erase
#define FLASH_BUFFER2_SAVE    0x86     // buffer 2 to main memory page with Erase
#define FLASH_PAGE_ERASE      0x81

#define FLASH_PAGE_TO_BUFFER1 0x53
#define FLASH_PAGE_TO_BUFFER2 0x55


//------- AT45DB161 only :
#define FLASH_SIGNATURE      0x2C
#define FLASH_SIGNATURE_MASK 0x3C

#define MkBufferAddress( a)     ((a) & 0x1FF)
#define MkPageAddress( a)       ((word)((a) >> 9))

#define BufferLowAddress( b)    ((b) & 0x00FF)
#define BufferHighAddress( b)   (((b) >> 8) & 0x01)
#define PageLowAddress( p)      ((p) << 2)
#define PageHighAddress( p)     ((p) >> 6)

//******************************************************************************
// Constructor
//******************************************************************************

TAt45dbxx::TAt45dbxx()
// Constructor
{
   FSpi = new TUsbSpi;
} // TAt45dbxx

//******************************************************************************
// Destructor
//******************************************************************************

TAt45dbxx::~TAt45dbxx()
// Destructor
{
   if( FSpi){
      delete FSpi;
   }
} // ~TAt45dbxx

//******************************************************************************
// Connected
//******************************************************************************

bool TAt45dbxx::Connected()
// Check if the adapter/module are present
{
   // read chip signature :
   byte Value;
   if( !Status( Value)){
      return( false);                  // probably no adapter
   }
   if( Value == 0xFF){
      return( false);                  // module not present
   }
   if( (Value & FLASH_SIGNATURE_MASK) != FLASH_SIGNATURE){
      return( false);
   }
   return( true);
} // Connected

//******************************************************************************
// Ready
//******************************************************************************

bool TAt45dbxx::WaitForReady()
// Wait for flash ready
{
   byte Value;
   for( int i = 0; i < FLASH_TIMEOUT; i++){
      if( !Status( Value)){
         return( false);               // probably no adapter
      }
      if( Value & FLASH_STATUS_RDY){
         return( true);                // done
      }
      Sleep( 1);
   }
   return( false);                     // timeout
} // WaitForReady

//******************************************************************************
// Write buffer
//******************************************************************************

bool TAt45dbxx::WriteBuffer( word Offset, void *Data, int Length)
// Write data into flash buffer
{
   if( Length > FLASH_PAGE_SIZE){
      IERROR;
   }
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   // prepare command :
   byte Buffer[ FLASH_PAGE_SIZE + 16];
   int Index = 0;
   Buffer[ Index++] = FLASH_BUFFER1_WRITE;
   Buffer[ Index++] = 0;               // don't care
   Buffer[ Index++] = BufferHighAddress( Offset);
   Buffer[ Index++] = BufferLowAddress( Offset);
   // data :
   for( int i = 0; i < Length; i++){
      Buffer[ Index++] = ((byte *)Data)[ i];
   }
   // send command & data :
   if( !FSpi->Write( Buffer, Index)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // WriteBuffer

//******************************************************************************
// Save buffer
//******************************************************************************

bool TAt45dbxx::SaveBuffer( word Page)
// Save buffer to flash array
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   // prepare command :
   byte Buffer[ 16];
   int Index = 0;
   Buffer[ Index++] = FLASH_BUFFER1_SAVE;
   Buffer[ Index++] = PageHighAddress( Page);
   Buffer[ Index++] = PageLowAddress( Page);
   Buffer[ Index++] = 0;               // don't care
   // send command & data :
   if( !FSpi->Write( Buffer, Index)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // SaveBuffer

//******************************************************************************
// Load buffer
//******************************************************************************

bool TAt45dbxx::LoadBuffer( word Page)
// Load buffer from flash array
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   // prepare command :
   byte Buffer[ 16];
   int Index = 0;
   Buffer[ Index++] = FLASH_PAGE_TO_BUFFER1;
   Buffer[ Index++] = PageHighAddress( Page);
   Buffer[ Index++] = PageLowAddress( Page);
   Buffer[ Index++] = 0;               // don't care
   // send command & data :
   if( !FSpi->Write( Buffer, Index)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // LoadBuffer

//******************************************************************************
// Read
//******************************************************************************

bool TAt45dbxx::ReadArray( word Page, word Offset, void *Data, int Length)
// Read data from flash array
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   // prepare command :
   byte Buffer[ 16];
   int Index = 0;
   Buffer[ Index++] = FLASH_ARRAY_READ;
   Buffer[ Index++] = PageHighAddress( Page);
   Buffer[ Index++] = PageLowAddress( Page) | BufferHighAddress( Offset);
   Buffer[ Index++] = BufferLowAddress( Offset);
   Buffer[ Index++] = 0;               // additional don't care
   Buffer[ Index++] = 0;               // additional don't care
   Buffer[ Index++] = 0;               // additional don't care
   Buffer[ Index++] = 0;               // additional don't care
   // send command :
   if( !FSpi->Write( Buffer, Index)){
      return( false);
   }
   // read data :
   if( !FSpi->Read( Data, Length)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // ReadArray

//------------------------------------------------------------------------------

//******************************************************************************
// CheckConnect
//******************************************************************************

bool TAt45dbxx::CheckConnect()
// Try connect adapter
{
   if( FSpi->IsOpen){
      return( true);                   // already opened
   }
   // open adapter :
   TIdentifier Identifier;
   if( !FSpi->Locate( USB_DEVICE_NAME, USB_DUTCHMAN_NAME, Identifier)){
      return( false);
   }
   if( !FSpi->Open( Identifier)){
      return( false);
   }
   // set SPI parameters :
   FSpi->Flush();
   FSpi->SetMode( TUsbSpi::CLOCK_LOW,
                  TUsbSpi::READ_ON_FALL,
                  TUsbSpi::WRITE_ON_FALL);
   if( !FSpi->SetClockRate( SPI_CLOCK_RATE)){
      return( false);
   }
   // deselect & inactive clock :
   if( !FSpi->SetCS()){
      return( false);
   }
   return( true);
} // CheckConnect

//******************************************************************************
// Status
//******************************************************************************

bool TAt45dbxx::Status( byte &Value)
// Read flash status
{
   if( !CheckConnect()){
      return( false);
   }
   if( !FSpi->ClrCS()){                 // chipselect
      return( false);
   }
   if( !FSpi->WriteByte( FLASH_STATUS_READ)){
      return( false);
   }
   if( !FSpi->ReadByte( Value)){
      return( false);
   }
   if( !FSpi->SetCS()){                 // deselect
      return( false);
   }
   return( true);
} // Status

