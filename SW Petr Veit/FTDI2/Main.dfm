object MainForm: TMainForm
  Left = 283
  Top = 212
  AutoScroll = False
  Caption = 'Packet diagnostics'
  ClientHeight = 547
  ClientWidth = 748
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = Create
  OnDestroy = Destroy
  OnResize = MemoResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 115
    Top = 16
    Width = 29
    Height = 13
    Caption = 'Data :'
  end
  object Label1: TLabel
    Left = 437
    Top = 19
    Width = 29
    Height = 13
    Caption = 'Rate :'
  end
  object LblRead: TLabel
    Left = 672
    Top = 192
    Width = 23
    Height = 13
    Caption = '0x??'
  end
  object MainMemo: TMemo
    Left = 0
    Top = 80
    Width = 625
    Height = 448
    Anchors = [akLeft, akTop, akRight, akBottom]
    Lines.Strings = (
      '')
    TabOrder = 2
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 528
    Width = 748
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object MainSend: TButton
    Left = 272
    Top = 14
    Width = 76
    Height = 21
    Caption = 'Send'
    TabOrder = 1
    OnClick = MainSendClick
  end
  object MainData: TEdit
    Left = 179
    Top = 14
    Width = 73
    Height = 21
    TabOrder = 0
    Text = '0'
  end
  object MainRate: TEdit
    Left = 488
    Top = 16
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '1000000'
  end
  object BtnInit: TButton
    Left = 648
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Init'
    TabOrder = 5
    OnClick = BtnInitClick
  end
  object BtnRead: TButton
    Left = 648
    Top = 144
    Width = 75
    Height = 25
    Caption = 'Read'
    TabOrder = 6
    OnClick = BtnReadClick
  end
  object BtnReceive: TButton
    Left = 648
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Receive'
    TabOrder = 7
    OnClick = BtnReceiveClick
  end
  object BtnSelect: TButton
    Left = 648
    Top = 352
    Width = 75
    Height = 25
    Caption = 'Select'
    TabOrder = 8
    OnClick = BtnSelectClick
  end
  object BtnDeselect: TButton
    Left = 648
    Top = 392
    Width = 75
    Height = 25
    Caption = 'Deselect'
    TabOrder = 9
    OnClick = BtnDeselectClick
  end
end
