                                               //******************************************************************************
// Synchronize
//******************************************************************************

bool TUsbSpi::Synchronize()
// Synchronize MPSSE device
{
   Flush();
   DWORD w;
   DWORD r;
   DWORD Available;
   // Send 0xAA (invalid command code) :
   byte  TxData = 0xAA;
   byte  RxData;
   do {
      if( !FT_W32_WriteFile( FHandle, &TxData, 1, &w, NULL)){
         RwdReport( "Unable send");
         return( false);   // timeout or error
      }
      RwdTx( &TxData, 1);
      if_fterr( FT_GetQueueStatus( FHandle, &Available)){
         RwdReport( "Rx queue error");
         return( false);
      }
   } while( Available < 1);
   // Check for 0xFA 0xAA return :
   for( DWORD i = 0; i < Available; i++){
      if( !FT_W32_ReadFile( FHandle, &RxData, 1, &r, NULL)){
         RwdReport( "Rx error");
         return( false);
      }
      RwdRx( &RxData, r);
   }
   // Send 0xAB (invalid command code) :
   TxData = 0xAB;
   if( !FT_W32_WriteFile( FHandle, &TxData, 1, &w, NULL)){
      RwdReport( "Unable send");
      return( false);   // timeout or error
   }
   RwdTx( &TxData, 1);
   // Wait for Rx :
   do {
      if_fterr( FT_GetQueueStatus( FHandle, &Available)){
         RwdReport( "Rx queue error");
         return( false);
      }
   } while( Available < 1);
   // Check for 0xFA 0xAB return :
   for( DWORD i = 0; i < Available; i++){
      if( !FT_W32_ReadFile( FHandle, &RxData, 1, &r, NULL)){
         RwdReport( "Rx error");
         return( false);
      }
      RwdRx( &RxData, r);
   }
   return( true);
} // Synchronize
