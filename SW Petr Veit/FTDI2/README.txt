             Postup ozivovani USB ctecky



1. Podle manualu nainstalujeme program MProg
2. Pripojime nenaprogramovanou ctecku k PC
   a provedeme instalaci implicitnich ovladacu
   (viz manual MProg, pozor, detekuji se celkem
   ctyri zarizeni 2x RS232 a 2x USB Serial)
3. Do adresare Program Files/MProg 2.3/Templates
   zkopirujeme soubor VEITRDR.ept
4. Spustime MProg
   - zvolime File/Open a vybereme soubor VEITRDR.ept
   - zvolime Device/Scan, ve spodnim okne by se melo
     objevit 'Number of blank devices = 1"
   - zvolime Device/Program
   - naprogramovani muzeme zkontrolovat funkci
     Tools/Read and parse (po vyvolani funkce
     by se formular nemel zmenit)
5. Ukoncime MProg, odpojime ctecku od USB
6. Po chvilce pripojime ctecku k USB
7. Detekuje se nezname zarizeni 'VEIT Reader'
   zadame cestu k ovladacum (root instalacniho CD)
   Instaluje se zarizeni VEIT Reader A,
   po chvili system detekuje zarizeni VEIT Reader B,
   opet zadame cestu k ovladacum a potvrdime
8. Spustime program pro USB ctecku a zkontrolujeme
   jeho spravnou funkci (mel by detekovat adapter
   VEIT Reader A a popr. vlozeny modul)

