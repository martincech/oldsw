//******************************************************************************
//
//   MainFlash.cpp  Flash low level demo
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MainFlash.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainForm *MainForm;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TMainForm::TMainForm(TComponent* Owner)
        : TForm(Owner)
{
} // TMainForm

//******************************************************************************
// Create
//******************************************************************************

void __fastcall TMainForm::Create(TObject *Sender)
{
   Crt       = new TCrt( MainMemo);
   Logger    = new TCrtLogger( Crt);
   Flash     = new TAt45dbxx;
   Flash->Spi->Logger = Logger;
   Connected = false;
} // Create

//******************************************************************************
// Destroy
//******************************************************************************

void __fastcall TMainForm::Destroy(TObject *Sender)
{
   delete Flash;
   delete Logger;
   delete Crt;
} // Destroy

//******************************************************************************
// Resize
//******************************************************************************

void __fastcall TMainForm::MemoResize(TObject *Sender)
// Memo resize callback
{
   Crt->Resize( this->Canvas);
} // Resize

//******************************************************************************
// Timer
//******************************************************************************

void __fastcall TMainForm::TimerTimer(TObject *Sender)
{
   if( !BtnCheck->Checked){
      Flash->Spi->Logger = 0;
   } else {
      Crt->printf( "* Tick\n");
   }
   Connected = Flash->Connected();
   if( !BtnCheck->Checked){
      Flash->Spi->Logger = Logger;
   }
   LblAdapter->Caption = Flash->Spi->Name;      // at least Adapter name
   if( Connected){
      LblModule->Caption = "OK";
   } else {
      LblModule->Caption = "Not present";
   }
} // TimerTimer

//******************************************************************************
// Read
//******************************************************************************

void __fastcall TMainForm::BtnReadClick(TObject *Sender)
{
   byte Buffer[ 512];
   if( !Connected){
      return;
   }
   if( !Flash->ReadArray(0, 0, Buffer, sizeof( Buffer))){
      Crt->printf( "Unable read\n");
      return;
   }
} // BtnReadClick

//******************************************************************************
// Write
//******************************************************************************

#define TEST_SIZE FLASH_PAGE_SIZE

void __fastcall TMainForm::BtnWriteClick(TObject *Sender)
{
   byte Buffer[ TEST_SIZE];
   word TEST_PAGE = rand() % FLASH_PAGES;
   if( !Connected){
      return;
   }
   for( int i = 0; i < TEST_SIZE; i++){
      Buffer[ i] = rand();
   }
   if( !Flash->LoadBuffer( TEST_PAGE)){
      Crt->printf( "Unable load buffer\n");
      return;
   }
   if( !Flash->WriteBuffer( 0, Buffer, TEST_SIZE)){
      Crt->printf( "Unable write buffer\n");
      return;
   }
   if( !Flash->SaveBuffer( TEST_PAGE)){
      Crt->printf( "Unable save buffer\n");
      return;
   }
   if( !Flash->WaitForReady()){
      Crt->printf( "Flash not ready\n");
      return;
   }
   byte Buffer2[ TEST_SIZE];
   if( !Flash->ReadArray( TEST_PAGE, 0, Buffer2, TEST_SIZE)){
      Crt->printf( "Unable read flash\n");
      return;
   }
   if( !memequ( Buffer, Buffer2, TEST_SIZE)){
      Crt->printf( "Verification failed\n");
      return;
   }
   Crt->printf( "* Verification OK\n");
} // BtnWriteClick

