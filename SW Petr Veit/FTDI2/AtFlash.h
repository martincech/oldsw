//******************************************************************************
//
//   AtFlash.h     Atmel flash high level module
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#ifndef AtFlashH
#define AtFlashH

#include "At45dbxx.h"

#define FLASH_VIRTUAL_PAGE        512                       // virtual page size
#ifndef FLASH_SIZE
  #define FLASH_SIZE ((dword)FLASH_PAGES * FLASH_VIRTUAL_PAGE)// memory size
#endif

//******************************************************************************
// AtFlash
//******************************************************************************

class TAtFlash : public TAt45dbxx {
public :
   TAtFlash();
   // Constructor
   ~TAtFlash();
   // Destructor

   bool Read( dword Address, void *Data, int Length);
   // Read from virtual <Address>
   bool Write( dword Address, void *Data, int Length);
   // Write to virtual <Address>
   bool Flush();
   // Flush all written data
//------------------------------------------------------------------------------
protected :
   // write context address :
   word WritePage;
   word WriteOffset;
   // read context address :
   word ReadPage;
   word ReadOffset;

   bool SwapBuffer( word OldPage, word NewPage);
   // Save <OldPage>, read <NewPage>
}; // TAtFlash

#endif
