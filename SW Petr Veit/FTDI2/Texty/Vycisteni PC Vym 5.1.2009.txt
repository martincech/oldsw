Doporucuji nasledujici uklid na disku
(viz minuly email) :

- z www.ftdichip.com stahnout MProg3.0
- z www.ftdichip.com stahnout program FTClean

- na disku v adresari Windows\Inf prohledat soubory
  oem<n>.inf kde <n> je cislo. Smazat vsechny, ktere
  se tykaji FTDI, vcetne prislusneho oem<n>.PNF

- odinstalovat vsechny nove ovladace ve spravci zarizeni

- odinstalovat vsechny ovladace FTDI (ktere pujdou)
  pomoci Control panelu/Instalovane programy

- pomoci programu FTClean nebo pomoci ftuninst.exe
  odstranit vsechny zbyvajici stare ovladace FTDI

- smazat Windows\System32\ft*.* (az na uvedene dva)

- v registru Windows odstranit klice v sekci Uninstall,
  aby se Ovladace nezobrazovaly v Control panelu/Instalovane programy

- pripojit nejake zarizeni, treba Bat1 a pouzit vyvojarsky
  ovladac

Program FTClean jsem nezkousel, ale podle popisu by
mel zrusit vsechny ovladace FTDI. Pritom ale vyrobi
na disku nejake *.ini soubory, ktere se maji potom smazat.
