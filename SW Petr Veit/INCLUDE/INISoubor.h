// Header tvorbu, cteni INI souboru

#include <Soubor32.h>
//#include <Stringyw.h>



bool ZapisDoINISouboru(const char *Soubor, char *Radek, char *Co) {
  HANDLE h, h2;
  char s[100], TMP[150];
  bool ObsahujeRadek=false;

  try {
    if (OtevriSouborProCteni(&h,Soubor,OPEN_EXISTING)) {
      // Pokud existuje, hledam v INI, zda tam uz radek neni
      while (NactiRadekZeSouboru(h,s)) { // Dokud jsou tam nejake radky
        if (s[0]==';') continue;  // Jde o komentar 
        // Porovnam
        if (strnicmp(s,Radek,strlen(Radek))==0) {  // Nasel jsem radek
          ObsahujeRadek=true;
          break;
        }
      } // while
      CloseHandle(h);  // Zavru ho
    } // if

    if (ObsahujeRadek) { // Musim zmenit
      // Chci zmenit hodnotu existujiciho radku
      // Vrati jmeno TMP unikatniho souboru, napr "ENI0413.TMP"
      // Tecka znamena, ze to udela v akt. adresari
      if (GetTempFileName(".", "ENI", 1, TMP)==0) throw 1;
      // Zkopiruju (pripadne i prepise) do TMP
      if (!CopyFile(Soubor,TMP,false)) throw 1;
      // Ted vytvorim cisty INI
      if (!OtevriSouborProZapis(&h2,Soubor,CREATE_ALWAYS)) return false;
      // Otevru TMP pro cteni:
      if (OtevriSouborProCteni(&h,TMP,OPEN_EXISTING)) {
        while (NactiRadekZeSouboru(h,s)) { // Dokud jsou tam nejake radky
          if (s[0]==';') continue;  // Jde o komentar 
          // Porovnam
          if (strnicmp(s,Radek,strlen(Radek))==0) {  // Nasel jsem radek
            // Dosel jsem na radek, ktery chcu menit, ten nekopiruju, ale vytvorim novy
            strcpy(s,Radek); strcat(s,"="); strcat(s,Co);  // Vytvorim radek
          }
          if (!ZapisRadekDoSouboru(h2,s)) throw 1;  // Zapisu s (to je bud stejne nebo modifikovane)
        } // while
      } // if
      // Vymazu TMP - schvalne neni if(!Dele..
      CloseHandle(h);
      DeleteFile(TMP);
    } else { // Musim vytvorit na konci novy
      // Zapisu tedy na konec souboru novy radek
      if (!OtevriSouborProZapis(&h,Soubor,OPEN_EXISTING)) 
        if (!OtevriSouborProZapis(&h,Soubor,CREATE_ALWAYS)) return false;
      SetFilePointer(h,0,NULL,FILE_END);  // Na konec - schvalne bez vyjimek
      strcpy(s,Radek); strcat(s,"="); strcat(s,Co);  // Vytvorim radek
      if (!ZapisRadekDoSouboru(h,s)) throw 1;
    }
  } //try
  catch (...) {
    CloseHandle(h);
    CloseHandle(h2);
    return false;
  }
  CloseHandle(h2);
  CloseHandle(h);
  return true;
}


bool NactiZINISouboru(const char *Soubor, char *Radek, char *Kam) {
  HANDLE h;
  char s[100];
  int i;

  try {
    if (OtevriSouborProCteni(&h,Soubor,OPEN_EXISTING)) { // Kdyz existuje
      // Pokud existuje, hledam v INI, zda tam uz radek neni
      while (NactiRadekZeSouboru(h,s)) { // Dokud jsou tam nejake radky
        // Porovnam
        if (s[0]==';') continue;  // Jde o komentar
        if (strnicmp(s,Radek,strlen(Radek))==0) {  // Nasel jsem radek
          // Odstranim znaky pred '=' vcetne a zbytek vratim
          if ((i=NajdiZnak(s,'='))<0) throw 1;
          VymazZnaky(s,0,i+1);  // Vymazu i s '='
          strcpy(Kam,s);
          break;
        }
      } // while
      CloseHandle(h);  // Zavru ho
    } else return false;  // Neexistuje
  } //try
  catch (...) {
    CloseHandle(h);
    return false;
  }
  CloseHandle(h);
  return true;
}


