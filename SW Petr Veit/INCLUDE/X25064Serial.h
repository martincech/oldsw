// Ovladani seriove ctecky pro EEPROM Xicor 25064


#ifndef __X25064Serial_H   // Aby to nedefinovalo vicekrat
#define __X25064Serial_H


#include <serial32.h>    // Header pro seriovou komunikaci



// Prikazy, ktere posila PC do ctecky
#define CMD_VERSION 0x10           // Poslu cislo verze
#define CMD_READ 0x20              // Poslu bajt ze zadane adresy
#define CMD_WRITE 0x30             // Zapisu poslany bajt na zadanou adresu
#define CMD_INSERT 0x40            // Poslu, zda je modul vlozeny ve ctecce.
#define CMD_INIT 0x50              // Poslu, zda je modul vlozeny ve ctecce.
#define CMD_BLOCK_READ_START 0x60  // Odstartuju blokove cteni
#define CMD_BLOCK_READ 0x70        // Nacte jeden bajt z bloku a posle ho
#define CMD_BLOCK_READ_STOP 0x80   // Ukoncim blokove cteni
// Odpovedi
#define ACK 0x02
#define NAK 0x03





unsigned short int SeriovyPort=2;  // Cislo COM portu
static HANDLE IdPort=NULL;



bool InicializaceCteckaSerial() {
  // Otevre port a posle inicializacni prikaz do ctecky
  char s[10];
  try {
    // Pokud je port otevreny, zavru ho
    if (IdPort!=NULL) CloseHandle(IdPort);
    // Otevru a nastavim port
    if (!OtevriPort(SeriovyPort,&IdPort,9600,8,2,1)) return false;
    if (!NastaveniBuferu(IdPort,9000,9000)) {
      throw(1);
    }//if
    if (!NastaveniTimeOutu(IdPort,500)) { // Timeout
      throw(1);
    }//if
    // Pokusim se zapsat inicializaci do ctecky - ctecka by mela odpovedet ACK
    s[0]=CMD_INIT;s[1]=0;
    ZapisPort(IdPort,s,1);
    s[0]=0;
    if (NactiPort(IdPort,s,1)<1) throw 1;
    if (s[0]!=ACK) throw 1;
  }
  catch (...) {
    CloseHandle(IdPort);
    IdPort=NULL;
    return false;
  }
  return true;
}

void KonecCteckaSerial() {
  // Zavre port
  try {
    if (IdPort) CloseHandle(IdPort);
    IdPort=NULL;
  } catch (...) {}
}

bool ZjistiVlozeniCteckaSerial() {
  // Zjisti, zda je modul vlozen
  char s[10];
  // Pokusim se zapsat inicializaci do ctecky - ctecka by mela odpovedet ACK
  s[0]=CMD_INIT;s[1]=0;
  ZapisPort(IdPort,s,1);
  s[0]=0;
  if (NactiPort(IdPort,s,1)<1) return false;
  if (s[0]!=ACK) return false;
  return true;
}




// ------------------------------------------------------------------
// Operace s daty v karte

unsigned char NactiByteCteckaSerial(unsigned short int Adresa) {
  char s[10];

  s[0]=CMD_READ;
  s[1]=(unsigned char)((Adresa & 0x0FF00) >> 8);   // Rozhodim adresu na 2 bajty
  s[2]=(unsigned char)(Adresa & 0x0FF);
  ZapisPort(IdPort,s,3);

  s[0]=0;
  if (NactiPort(IdPort,s,1)<1) return 0xFF;   // Aby to odpovidalo paralelni ctecce - ta pri nepripojeni taky posilala FF
  return (unsigned char)s[0];
}

void NactiCteckaSerial(int PocetBytu, unsigned short int Adresa, char* Kam) {
  // Nacte PocetBytu bajtu a adresy Adresa do Kam
  char s[10];
  // Poslu zahajeni blokoveho cteni od zadane adresy
  s[0]=CMD_BLOCK_READ_START;
  s[1]=(unsigned char)((Adresa & 0x0FF00) >> 8);   // Rozhodim adresu na 2 bajty
  s[2]=(unsigned char)(Adresa & 0x0FF);
  s[3]=(unsigned char)((PocetBytu & 0x0FF00) >> 8);   // Rozhodim pocet bajtu na 2 bajty
  s[4]=(unsigned char)(PocetBytu & 0x0FF);
  ZapisPort(IdPort,s,5);
  s[0]=0;
  if (NactiPort(IdPort,s,1)<1) return;
  if (s[0]!=ACK) return;
  // Nacitam pozadovane bajty
  for (int i=0;i<PocetBytu;i++) {
    s[0]=0;
    if (NactiPort(IdPort,s,1)<1) {
      // Pokud by se seknul hned u 1. bajtu, cekalo by to hrozne dlouho. Proto, kdyz zjistim 1. neprijaty znak, vyplnim cele
      // pole FF a odejdu.
      for (int ii=i; ii<PocetBytu; ii++) Kam[ii]=0xFF;   // Aby to odpovidalo paralelni ctecce - ta pri nepripojeni taky posilala FF
      break;
    }
    Kam[i]=(unsigned char)s[0];
  }
  // Ukoncim blokove cteni
  s[0]=CMD_BLOCK_READ_STOP;
  ZapisPort(IdPort,s,1);
  s[0]=0;
  if (NactiPort(IdPort,s,1)<1) return;
  if (s[0]!=ACK) return;
}

bool UlozByteCteckaSerial(unsigned short int Adresa, unsigned char Co) {
  // Nacte PocetBytu bajtu a adresy Adresa do Kam
  char s[10];

  s[0]=CMD_WRITE;
  s[1]=(unsigned char)((Adresa & 0x0FF00) >> 8);   // Rozhodim adresu na 2 bajty
  s[2]=(unsigned char)(Adresa & 0x0FF);
  s[3]=(unsigned char)Co;
  ZapisPort(IdPort,s,4);

  s[0]=0;
  if (NactiPort(IdPort,s,1)<1) return false;   // Aby to odpovidalo paralelni ctecce - ta pri nepripojeni taky posilala FF
  if ((unsigned char)s[0]!=ACK) return false;
  return true;
}

void UlozCteckaSerial(int PocetBytu, unsigned short int Adresa, char* Co) {
  // Nacte PocetBytu bajtu a adresy Adresa do Kam

  for (int i=Adresa;i<Adresa+PocetBytu;i++) {
    UlozByteCteckaSerial(i,Co[i-Adresa]);
  }
}

#endif

