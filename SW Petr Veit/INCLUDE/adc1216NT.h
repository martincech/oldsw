// Funkce pro komunikaci s kartou ADICOM ADC1216s pro Windows NT
// Vyuzivaji se funkce z CW1216.DLL od Adicomu
// Copyright 1999 VEIT Electronics
// Hlavni aplikace musi prilinkovat soubor CW1216.LIB - vytvorene pomoci prikazu "IMPLIB CW1216.DLL"
// Dale musi byt jeste konfiguracni soubor ADC1216NT.par


#ifndef __ADC1216NT_H   // Aby to nedefinovalo vicekrat
#define __ADC1216NT_H



// Funkce z CW1216.DLL:
extern "C" _stdcall bool SimpleInit(unsigned char *ParamFile);
extern "C" _stdcall void SimpleDone();
extern "C" _stdcall double SimpleGetVoltage(unsigned char Channel);
//extern "C" _stdcall void SimplePutVoltage(double Voltage);
extern "C" _stdcall unsigned char SimpleGetDigital();
//extern "C" _stdcall void SimpleSetDigital(unsigned char Value);
//extern "C" _stdcall void SimpleSetup();
//extern "C" _stdcall void SimpleSetAdressDialog();


unsigned short int outaddr;


bool OtevriKartu() {
  bool b;
  unsigned char s[20]="ADC1216NT.PAR";
  b=SimpleInit(s);
  return b;
}

void ZavriKartu() {
  SimpleDone();
}

double NactiKartu(unsigned int Kanal, int Pocet) {
// Nacte napeti ze zadaneho kanalu
  // Pri pouziti ovladacu od Adicomu jejich funkce dava primo napeti, ne prevod => nemusim rucne pocitat
  double Suma=0, f;
  if (Kanal<1) return 0;   // Kvuli kompatibilite se starsimi verzemi - tam by se mohl objevit i kanal c. 0
  if (Pocet<=0) Pocet=1;
  try {
    for (int i=0;i<Pocet;i++) {
      Suma+=SimpleGetVoltage((unsigned char)Kanal);
    }
    f=(double)Suma/Pocet;
    // Udelano pro rozsah 0 az 5V
    return f;
  } catch (...) {
    return 0;
  }
}

bool NactiDigitalniVstup(unsigned short *Vysledek) {
// Nacte digitalni vstup a vysledek vrati v ukazateli Vysledek
  *Vysledek=SimpleGetDigital();
  return true;
}


#endif
