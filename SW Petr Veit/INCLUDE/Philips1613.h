// Header pro komunikaci s jednotkou Philips 1613


//#include <serial32.h>
//#include <Stringyw.h>


const SOH=1;
const STX=2;
const ETX=3;
const EOT=4;
const ENQ=5;
const ACK=6;
const POL=112;


bool PhilipsNactiVahuStr(HANDLE h, char* Neto, char *Ustaleni) {
  // Vraci vzdy hmotnost v kilogramech, i kdyz je na vaze nastaveno tuny!!
  char s[100],s1[20];
  int Pocet;
  unsigned char BCC=0;
  double Hmotnost;


  // Dotazu se na vahu
  s[0]=SOH;
  s[1]='A';  // Adresa
  s[2]=STX;
  s[3]='W';  // Prikaz WNA - cte netto
  s[4]='N';
  s[5]='A';
  s[6]=ETX;

  // Vypoctu BCC
  for (int i=3; i<=6; i++) {
    BCC=BCC^(unsigned char)s[i];
  }

  s[7]=BCC;
  s[8]=ENQ;
  s[9]=0;
  ZapisPort(h,s,9);
  // Cekam na ACK
  Pocet=NactiPort(h, s, 1);
/*  if (Pocet==1 && (unsigned char)s[0]==EOT) {
    // Ukoncim komunikaci master->Slave a cekam na odpoved jednotky
    s[0]=EOT;
    ZapisPort(h,s,1);
    return false;
  }
  if (Pocet==0 || (unsigned char)s[0]!=ACK) return false;*/

  // 5.6.2001: Jdu vzdycky dal
  if (Pocet==0) return false;


  // Ukoncim komunikaci master->Slave a cekam na odpoved jednotky
  s[0]=EOT;
  ZapisPort(h,s,1);
  // Vyzadam si odpoved
  s[0]=SOH;
  s[1]='A';  // Adresa
  s[2]=POL;
  s[3]=ENQ;
  ZapisPort(h,s,4);
  // Nactu vahu poslanou z jednotky
  Pocet=NactiPort(h, s, 17);
  if (Pocet>0 && Pocet<17) {
    // Poslal asi nejaky error - napr. pretizeni
    // Poslu zaverecny ACK
    s[0]=ACK;
    ZapisPort(h,s,1);
    return false;
  }
  if (Pocet<17) return false;
  // Poslu zaverecny ACK
  s[0]=ACK;
  ZapisPort(h,s,1);
  // Zkontroluju (s[0] se sice prepsalo, ale to me nezajima)
  if (s[3]!='Q' || s[4]!='N' || s[5]!='A') return false;
  KopirujZnaky(s1,s,7,5);   // Do s1 dam hmotnost
  String Str=s1;
  Str="0," + Str;
  try {
    Hmotnost=Str.ToDouble();
    KopirujZnaky(s1,s,12,1);   // Do s1 dam exponent hmotnosti
    for (int i=0; i<String(s1).ToInt(); i++) Hmotnost*=10.0;
  } catch(...) {
    return false;
  }

  Hmotnost/=1000.0;    // Vratim hmotnost v tunach


  Str=String(Hmotnost);

  strcpy(s1, Str.c_str());
  int i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Neto,s1);

  // Ustaleni
  KopirujZnaky(s1,s,13,1);   // Do s1 dam ustaleni
  strcpy(Ustaleni,s1);

  return true;
}

bool PhilipsNactiUstalenouVahuStr(HANDLE h, char* Neto) {
  char Net[100], Ust[10];
  if (!PhilipsNactiVahuStr(h, Net, Ust)) return false;
  if (Ust[0]!='1') return false;   // Vaha neni ustalena
  strcpy(Neto,Net);   // Zkopiruju ustalene netto
  return true;
}

bool PhilipsNactiParametryStr(HANDLE h, char *VV, char* Jednotky, char *Max, char *Dilek) {
  // Digi10 - napevno

  // VV - cislo jednotky
  strcpy(VV,"1");
  // Jednotka = " g", "kg", " t"
  strcpy(Jednotky,"t");
  // Max
  strcpy(Max,"50.00");
  // Ustaleni
  strcpy(Dilek,"2500");
  return true;
}

/*bool PhilipsNactiParametryStr(HANDLE h, char* Jednotky, char *Max, char *Dilek) {
  // BLBNE !!!!!!!!!!!!!!!!!!!!!
  char s[100],s1[10];
  int Pocet;
  unsigned char BCC=0;

  // Reset komunikace - musi byt na kazdem startu
  s[0]=EOT;
  ZapisPort(h,s,1);
  // Cekam na EOT - nemusi ale nic poslat
  Pocet=NactiPort(h, s, 1);


  do {
  // Dotazu se na vahu
  s[0]=SOH;
  s[1]='A';  // Adresa
  s[2]=STX;
  s[3]='I';  // Prikaz I - cte informace
  s[4]=ETX;


  // Vypoctu BCC
  for (int i=3; i<=4; i++) {
    BCC=BCC^(unsigned char)s[i];
  }

  s[5]=BCC;
  s[6]=ENQ;
  s[7]=0;
  ZapisPort(h,s,7);
  // Cekam na ACK
  Pocet=NactiPort(h, s, 1);
  if (Pocet==0) return false;
  } while ((unsigned char)s[0]!=ACK);
//  if (Pocet==0 || (unsigned char)s[0]!=ACK) return false;
  // Ukoncim komunikaci master->Slave a cekam na odpoved jednotky
  s[0]=EOT;
  ZapisPort(h,s,1);
  // Vyzadam si odpoved
  s[0]=SOH;
  s[1]='A';  // Adresa
  s[2]=POL;
  s[3]=ENQ;
  ZapisPort(h,s,4);
  // Nactu string poslany z jednotky - nema pevny pocet znaku!!
  for (int i=0; i<=99; i++) {
    if (i==99) return false;   // Nenacetl jsem ETX
    Pocet=NactiPort(h, s1, 1);
    if (Pocet<1) return false;
    s[i]=s1[0];
    s[i+1]=0;
    if (s[i]==ETX) {
      // Po ETX je jeste BCC a konec
      Pocet=NactiPort(h, s1, 1);
      if (Pocet<1) return false;
      break;
    }
  }
  // Poslu zaverecny ACK
  s[0]=ACK;
  ZapisPort(h,s,1);
  // Zkontroluju (s[0] se sice prepsalo, ale to me nezajima)
  if (s[3]!='Q' || s[4]!='I') return false;
  KopirujZnaky(s1,s,7,5);   // Do s1 dam hmotnost
  String Str=s1;
  Str="0," + Str;




  return true;
}*/

