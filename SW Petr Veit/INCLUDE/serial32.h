// Header pro 32-bitovou seriovou komunikaci
// 14.1.2002: Odstraneny TEXTCHYBYSERIAL a CISLOCHYBYSERIAL

#ifndef Serial32H
#define Serial32H

#include <stdlib.h>




/*const char *TEXTCHYBYSERIAL[8]={
"Port nejde otev��t (CreateFile)",
"Nelze p�e��st konfiguraci portu (GetCommState)",
"Port nejde nakonfigurovat (SetCommState)",
"Port nejde zav��t (CloseHandle)",
"Chyba p�i nastavov�n� timeoutu portu (SetCommTimeouts)",
"Chyba p�i nastavov�n� buferu portu (SetupComm)",
"Chyba p�i �ten� z portu (ReadFile)",
"Chyba p�i z�pisu na port (WriteFile)"};

int CISLOCHYBYSERIAL=-1;*/



bool OtevriPort(int port,HANDLE* hCom,int BaudRate,int ByteSize,int Parity,int StopBits) {
// Otevre port, nastavi hCom
// Je-li vse v poradku, vrati 1
// Je-li chyba, vraci false a v CISLOCHYBYSERIAL vraci index textu chyby v TEXTCHYBYSERIAL

  DCB dcb;
  bool fSuccess;

  char s[20]="",s1[3]="";
  itoa(port,s1,10);
  if (port < 10) {
    strcpy(s,"COM");
  } else {
    strcpy(s,"\\\\.\\COM");     // 8.7.2008: Pro porty >= COM10 viz http://support.microsoft.com/kb/115831
  }
  strcat(s,s1);

  *hCom = CreateFile(s,GENERIC_READ | GENERIC_WRITE,0,NULL,OPEN_EXISTING,0,NULL);
  if (*hCom == INVALID_HANDLE_VALUE) {
//    CISLOCHYBYSERIAL=0;
    return false;
  }

  fSuccess = GetCommState(*hCom, &dcb);
  if (!fSuccess) {
//    CISLOCHYBYSERIAL=1;
    return false;
  }

  dcb.BaudRate = BaudRate;
  dcb.ByteSize = (BYTE) ByteSize;
  dcb.Parity = (BYTE) Parity; // 0=no, 1=odd, 2=even, 3=mark, 4=space
  if (StopBits==1) StopBits=0;
  dcb.StopBits = (BYTE) StopBits;
/*  dcb.BaudRate = 9600;
  dcb.ByteSize = 8;
  dcb.Parity = NOPARITY;
  dcb.StopBits = TWOSTOPBITS;*/

  fSuccess = SetCommState(*hCom, &dcb);

  if (!fSuccess) {
//    CISLOCHYBYSERIAL=2;
    return false;
  }
//  CISLOCHYBYSERIAL=-1;  // Vse je OK
  return true;
}

bool ZavriPort(HANDLE hCom) {
  if (CloseHandle(hCom)) {
//    CISLOCHYBYSERIAL=-1;
    return true;
  } else {
//    CISLOCHYBYSERIAL=3;
    return false;
  }
}

bool NastaveniTimeOutu(HANDLE hCom, int Kolikms) {
  COMMTIMEOUTS c;
  bool fSuccess;

  c.ReadIntervalTimeout=Kolikms;
  c.ReadTotalTimeoutMultiplier=1;
  c.ReadTotalTimeoutConstant=Kolikms;

  fSuccess=SetCommTimeouts(hCom,&c);

  if (!fSuccess) {
//    CISLOCHYBYSERIAL=4;
    return false;
  }
//  CISLOCHYBYSERIAL=-1;
  return true;
}

bool NastaveniBuferu(HANDLE hCom, int Vst, int Vyst) {
  bool fSuccess=SetupComm(hCom,Vst,Vyst);

  if (!fSuccess) {
//    CISLOCHYBYSERIAL=5;
    return false;
  }
//  CISLOCHYBYSERIAL=-1;
  return true;
}

int NactiPort(HANDLE hCom,char *str, int pocet) {
  char s[250]="";
//  int i=pocet;
  if (pocet>249) pocet=249;  // Nemuzu vetsi nez 249 (pole s[])
  if (!ReadFile(hCom,&s,pocet,(LPDWORD) &pocet,NULL)) {
//    CISLOCHYBYSERIAL=6;
    return -1; // Chyba (vnitrni)
  }
  // Pokud nacte napr. 20 znaku a na 5. pozici je 00H, zkopiruje
  // do str normalni strcpy(str,s) jen techto 5 znaku - toto zkopiruje cele
  for (int j=0;j<=pocet+1;j++) str[j]=s[j];
//  if (pocet<i) CISLOCHYBYSERIAL=6; else CISLOCHYBYSERIAL=-1; // Bez chyby jen kdyz nacetl vsechny znaky
  return pocet; // Kdyz nenacetl ani 1 znak, vrati 0 (jina chyba nez -1)
}

int ZapisPort(HANDLE hCom,char *str, int pocet) {
  if (!WriteFile(hCom,str,pocet,(LPDWORD) &pocet,NULL)) {
//    CISLOCHYBYSERIAL=7;
    return -1; // Chyba
  }
//  CISLOCHYBYSERIAL=-1;
  return pocet;
}

int NactiPortUnsigned(HANDLE hCom,unsigned char *str, int pocet) {
  unsigned char s[250]="";
//  int i=pocet;
  if (pocet>249) pocet=249;  // Nemuzu vetsi nez 249 (pole s[])
  if (!ReadFile(hCom,&s,pocet,(LPDWORD) &pocet,NULL)) {
//    CISLOCHYBYSERIAL=6;
    return -1; // Chyba (vnitrni)
  }
  // Pokud nacte napr. 20 znaku a na 5. pozici je 00H, zkopiruje
  // do str normalni strcpy(str,s) jen techto 5 znaku - toto zkopiruje cele
  for (int j=0;j<=pocet+1;j++) str[j]=s[j];
//  if (pocet<i) CISLOCHYBYSERIAL=6; else CISLOCHYBYSERIAL=-1; // Bez chyby jen kdyz nacetl vsechny znaky
  return pocet; // Kdyz nenacetl ani 1 znak, vrati 0 (jina chyba nez -1)
}

int ZapisPortUnsigned(HANDLE hCom,unsigned char *str, int pocet) {
  if (!WriteFile(hCom,str,pocet,(LPDWORD) &pocet,NULL)) {
//    CISLOCHYBYSERIAL=7;
    return -1; // Chyba
  }
//  CISLOCHYBYSERIAL=-1;
  return pocet;
}


/*void VypisChyboveHlaseni(int CisloPortu=0) {
  char s[20]="Chyba (COM",s1[4];
  if (CisloPortu>0) {
    itoa(CisloPortu,s1,10); strcat(s,s1); strcat(s,")");
  } else strcpy(s,"Chyba");  // Kdyby nezadal cislo portu
  MessageBox(NULL,TEXTCHYBYSERIAL[CISLOCHYBYSERIAL], s, MB_OK);
}*/

void PridejBCC(char *s) {  // Prida BCC (Block Check Character) ke stringu s
  int Delka=strlen(s);
  int Pocet;
  unsigned char BCC=0;
  int BCCi=0;

  // Schvalne delano tak slozite (s BCCi), aby to nehlasilo warningy
  for (int j=1;j<=128;j=j*2) { // Pro vsechny bity
    Pocet=0;
    for (int i=1;i<=Delka-1;i++) {  // Nejdriv 0. bit vsech bajtu atd... (bez 1. znaku -STX)
      if (s[i] & j) Pocet++;  // Je bit
    }
    if (Pocet%2>0) {
      BCCi=BCCi|j;
      BCC=(unsigned char)BCCi;
    }
  }
  s[Delka]=BCC; s[Delka+1]=0;
}

#endif
