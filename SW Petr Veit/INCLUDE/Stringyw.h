#include <STRING.H>
#include <STDLIB.H>
#include <MATH.H>

// ----------- prototypy fci ----------------

char *VlozZnak(char c,char *s, int pozice);
char *VymazZnak(char *s, int pozice);
int NajdiZnak(char *s,char c);
void VymazZnaky(char *s,int odkud,int pocet);
void KopirujZnaky(char *s2,const char *s1,int odkud,int pocet);
double StrToRel(char input[30]);
void RelToStr(char *str, double d, int Pocet);
void RelToStrFix(char *str, double d, int Pocet);
int Zaokrouhli(double f);
void NactiCas(char *ss);
void NactiDatumProSoubor(char *ss);


void PrevedNaBCD(char *ss);
int HexToInt(char *ss);
int HexToInt2(unsigned char *ss,int pocet);




// ----------- definice fci ----------------

char *VlozZnak(char c,char *s, int pozice) {
  int i,delka;
  delka=strlen(s);
  for (i=delka+1;i>=pozice;i--) {
    s[i]=s[i-1];
  }
  s[delka+1]=0;
  s[pozice]=c;
  return s;
}

char *VymazZnak(char *s, int pozice) {
  int i,delka;
  if (s[0]==0 || s[pozice]==0) return s; // Musi byt
  delka=strlen(s);
  for (i=pozice;i<=delka;i++) {
    s[i]=s[i+1];
  }
  return s;
}


double StrToRel(char input[30]) {
  char *endptr;
  return strtod(input, &endptr);
}

int NajdiZnak(char *s,char c) {
  char *ptr;
  ptr=strchr(s,c);
  if (ptr) return ptr-s;
    else return -1;
}

void VymazZnaky(char *s,int odkud,int pocet) {
  int i,j,delka;
  if (s[0]==0) return;
  for (i=0;i<=pocet-1;i++) {
    delka=strlen(s);
    for(j=odkud;j<=delka;j++) {
      s[j]=s[j+1];
    }
  }
}

void KopirujZnaky(char *s2,const char *s1,int odkud,int pocet) {
  // okdud...0=1.znak, 1=2.znak atd.
  int i,j;
  j=0;
  for (i=odkud;i<=odkud+pocet-1;i++) {
    s2[j]=s1[i];
    j++;
  }
  s2[j]=0; // Ukonceni stringu
}

void RelToStr(char *s, double d, int Pocet) {
// Pocet...pocet des. mist
  char str[15];  // !!!!!!! Musim pracovat s lok. stringem - jinak nevyzpytatelne chyby

  gcvt(d,10,str);

  strcpy(s,str);  

/*   char s1[15],s2[15],s[20]="";
   int i=d;
   itoa(i,s1,10);
   d-=i;
   if (d<0) {
   d=-d;
   }
   for (int j=1;j<=Pocet;j++) d=d*10;
   i=d;
   if (i<0) i=-i;
   itoa(i,s2,10);
   strcpy(s,s1);
   if (Pocet>0) {
     strcat(s,".");
     strcat(s,s2);
   }
   strcpy(str,s);*/
 }

 void RelToStrFix(char *s, double d, int Pocet) {
// Pocet...pocet des. mist napevno - vrati treba 5.000 misto 5.0


  char str[15];  // !!!!!!! Musim pracovat s lok. stringem - jinak nevyzpytatelne chyby

  int ii;
  gcvt(d,10,str); // Prevod udelam na 10 des. mist
  // Kdyby byl delsi, tak ho useknu
  ii=NajdiZnak(str,'.');
  if (ii<0) strcat(str,".0");  // Kdyz se ma prevest 4.0, gcvt vrati "4" a nenajdu tam '.'
  str[NajdiZnak(str,'.')+Pocet+1]=0;
  // Kdyz je kratsi, tak ho prodlouzim
  ii=strlen(str)-1-NajdiZnak(str,'.');
  for (int j=1;j<=(Pocet-ii);j++) strcat(str,"0");


  strcpy(s,str);

/*   char s1[15],s2[15],s[20]="";
   int i=d;
   itoa(i,s1,10);
   d-=i;
   if (d<0) {
     d=-d;
   }
   for (int j=1;j<=Pocet;j++) { d=d*10;
   i=d;
   if (i<0) i=-i;
   itoa(i,s2,10);
   strcpy(s,s1);
   if (Pocet>0) {
     for (i=strlen(s2);i<=Pocet-1;i++) strcat(s2,"0"); // Doplnim na pocet des. mist
     strcat(s,".");
     strcat(s,s2);
   }
   }
   strcpy(str,s);*/
 }

int Zaokrouhli(double f) {
  int i=10*f+5;
  return i/10;
}

void NactiCas(char *ss) {
  char s[10]="",s1[5]="";
  SYSTEMTIME Cas;
  GetLocalTime(&Cas);
  itoa(Cas.wHour,s1,10); if (s1[1]==0) VlozZnak('0',s1,0);
  strcat(s,s1); strcat(s,":");
  itoa(Cas.wMinute,s1,10); if (s1[1]==0) VlozZnak('0',s1,0);
  strcat(s,s1); strcat(s,":");
  itoa(Cas.wSecond,s1,10); if (s1[1]==0) VlozZnak('0',s1,0);
  strcat(s,s1);
  strcpy(ss,s);
}

void NactiDatum(char *ss) {
  char s[10]="",s1[5]="";
  SYSTEMTIME Datum;
  GetLocalTime(&Datum);
  itoa(Datum.wDay,s1,10);
  strcat(s,s1); strcat(s,".");
  itoa(Datum.wMonth,s1,10);
  strcat(s,s1); strcat(s,".");
  itoa(Datum.wYear,s1,10);
  strcat(s,s1);
  strcpy(ss,s);
}

void NactiDatumProSoubor(char *ss) {
  char s[12]="",s1[7]="";
  SYSTEMTIME Cas;
  GetLocalTime(&Cas);
  itoa(Cas.wDay,s1,10); if (s1[1]==0) VlozZnak('0',s1,0);
  strcat(s,s1);
  itoa(Cas.wMonth,s1,10); if (s1[1]==0) VlozZnak('0',s1,0);
  strcat(s,s1);
  itoa(Cas.wYear,s1,10);
  strcat(s,s1);
  strcpy(ss,s);
}

int PrevedZBCD(char ch) {
  // Prevede 0x52 na 52 integer
  char s2[5]="";
  itoa((unsigned char)ch,s2,16);  // Musi byt (unsigned char)
  return atoi(s2);
}

void PrevedNaBCD(char *ss) {
  // Prevadi vzdy 2 znaky na BCD
  char s[200]="",s1[5]="",s2[200]="";
  int i;

  strcpy(s,ss);
  while (strlen(s)>=2) {
    KopirujZnaky(s1,s,0,2);
    VymazZnaky(s,0,2);
    i=HexToInt(s1);
    s1[0]=(char)i; s1[1]=0;
    strcat(s2,s1);
  }

  strcpy(ss,s2);
}

int HexToInt(char *ss) {
  // ss napr "FFA9"
  char s[20]="",ch;
  int i,Maska=1,Hodnota=0;

  strcpy(s,ss);
  for (i=strlen(s)-1;i>=0;i--) { // Pro jedn. znaky
    ch=s[i];
    switch(s[i]) {
      case '0': { ch=0; break; }
      case '1': { ch=1; break; }
      case '2': { ch=2; break; }
      case '3': { ch=3; break; }
      case '4': { ch=4; break; }
      case '5': { ch=5; break; }
      case '6': { ch=6; break; }
      case '7': { ch=7; break; }                 
      case '8': { ch=8; break; }
      case '9': { ch=9; break; } 
      case 'A': { ch=10; break; }
      case 'B': { ch=11; break; }
      case 'C': { ch=12; break; }
      case 'D': { ch=13; break; }
      case 'E': { ch=14; break; }
      case 'F': { ch=15; break; }
    }
    Hodnota+=ch*Maska;
    Maska=Maska*16;
  }
  return Hodnota;
}

int HexToInt2(unsigned char *ss,int pocet) {
  // ss napr "/x01/x256"
  unsigned char s[20]="";
  int i,Maska=1,Hodnota=0;

  for (i=0;i<=pocet-1;i++) s[i]=ss[i];
  for (i=pocet-1;i>=0;i--) { // Pro jedn. znaky
    Hodnota+=s[i]*Maska;
    Maska=Maska*256;
  }
  return Hodnota;
}



double ZaokrouhliAritmeticky(double d) {
  // Fce, ktera vrati cislo zaokrouhlene na desetniky aritmeticky (tzn. na 1 des. misto)
  // Zaokrouhluje se financnicky => zaporne hodnoty musim delat stejne jako kladne => 17.68->17.7, -17.68->-17.7
  bool Zaporne=false;
  double Zbytek;

  if (d<0) {  // Pokud je cislo zaporne, nastavim flag a udelam z nej kladne
    Zaporne=true;
    d=-d;   // Pro vypocty vzdy pouzivam abs. hodnotu - jinak to delalo blbe zaporna cisla
  }
  // V d je ted napr. 17.68
  d=d*10;
  // Ted je v d napr. 176.8 => zaokrouhlim na cele cislo
  Zbytek=d-(int)d;
  // Ve Zbytek je ted 0.8 => pokud je Zbytek vetsi nez 0.5, zaokrouhlim nahoru, jinak dolu
  if (Zbytek<0.5) d=floor(d); else d=ceil(d);
  // V d je ted napr. 177 => vydelim deseti
  d=d/10;
  // Nastavim zpet spravne znamenko
  if (Zaporne) d=-d;  // Pokud bylo cislo zaporne, zase ho udelam zaporne
  return d;
}

double ZaokrouhliNahoru(double d) {
  // Fce, ktera vrati cislo zaokrouhlene na desetniky nahoru (na 1 des. misto)
  // Zaokrouhluje se financnicky => zaporne hodnoty musim delat stejne jako kladne => 17.63->17.7, -17.63->-17.7
  bool Zaporne=false;

  if (d<0) {  // Pokud je cislo zaporne, nastavim flag a udelam z nej kladne
    Zaporne=true;
    d=-d;   // Pro vypocty vzdy pouzivam abs. hodnotu - jinak to delalo blbe zaporna cisla
  }
  // V d je ted napr. 17.68
  d=d*10;
  // Ted je v d napr. 176.8 => zaokrouhlim na cele cislo nahoru
  d=ceil(d);
  // V d je ted napr. 177 => vydelim deseti
  d=d/10;
  // Nastavim zpet spravne znamenko
  if (Zaporne) d=-d;  // Pokud bylo cislo zaporne, zase ho udelam zaporne
  return d;
}

