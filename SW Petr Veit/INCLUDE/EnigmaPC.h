// Header pro komunikaci s jednotkou EnigmaPC / Mvz5

// 19.12.2001: Vsude doplnena kontrola CRC


#include <serial32.h>
#include <Stringyw.h>


int DelkaProtokolu(char *s) {
  // Je-li BCC=0H, da mne strlen() spatnou velikost => musim sam, hledam ETX a pak +1 (BCC)
  int i=1;
  while (s[i-1]!=3) i++;  // Hledam ETX=03H
  i++;  // Jeste pridam BCC
  return i;
}


// ******************************************************************
// ************* FCE koncici Str vraceji vysledky ve stringu ********
// ******************************************************************
bool NactiVahuStr(HANDLE h, char *VV, char* Neto, char *Tara, char *Ustaleni, char *Chyba) {
  // Telegram SC, ale vse vraci ve stringech - vhodne pro Compact
  char s[100],s1[10];
  int i;
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#SC#0#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,32)<32) return false; // Nactu z Enigmy odpoved - 32 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (i=1; i<31; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[31]) return false;    // Statne CRC

  // Nejdriv zkontroluju prijaty string (kontroluju pozice #)
  if (s[3]!='#' || s[6]!='#' || s[15]!='#' || s[24]!='#' || s[26]!='#' || s[29]!='#') {
    // Nekde nastala chyba
    return false;
  }
  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Neto
  KopirujZnaky(s1,s,7,8);   // Do s1 dam neto
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Neto,s1);
  // Tara
  KopirujZnaky(s1,s,16,8);   // Do s1 dam taru
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Tara,s1);
  // Ustaleni
  KopirujZnaky(s1,s,25,1);   // Do s1 dam ustaleni
  strcpy(Ustaleni,s1);
  // Chyba
  KopirujZnaky(s1,s,27,2);   // Do s1 dam chybu
  strcpy(Chyba,s1);
  return true;
}

bool NactiUstalenouVahuStr(HANDLE h, char *VV, char* Neto, char *Tara, char *Chyba) {
  // Telegram SC, ale vse vraci ve stringech - vhodne pro Compact
  char s[50],s1[10];
  int i;
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#TS#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,35)<35) return false; // Nactu z Enigmy odpoved - 35 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (i=1; i<34; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[34]) return false;    // Statne CRC

  // VV - cislo jednotky
  if (VV!=NULL) {
    KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
    strcpy(VV,s1);
  }
  // Neto
  if (Neto!=NULL) {
    KopirujZnaky(s1,s,7,8);   // Do s1 dam neto
    while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
    i=NajdiZnak(s1,',');
    if (i>=0) s1[i]='.';  // Zmena carky na tecku
    strcpy(Neto,s1);
  }
  // Tara
  if (Tara!=NULL) {
    KopirujZnaky(s1,s,16,8);   // Do s1 dam taru
    while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
    i=NajdiZnak(s1,',');
    if (i>=0) s1[i]='.';  // Zmena carky na tecku
    strcpy(Tara,s1);
  }
  // Ustaleni
  if (Chyba!=NULL) {
    KopirujZnaky(s1,s,31,1);   // Do s1 dam chybu
    strcpy(Chyba,s1);
  }
  return true;
}

bool NactiParametryStr(HANDLE h, char *VV, char* Jednotky, char *Max, char *Dilek) {
  // Telegram PV, ale vse vraci ve stringech - vhodne pro Compact
  char s[100],s1[10];
  int i;
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#PV#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,27)<27) return false; // Nactu z Enigmy odpoved - 27 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (i=1; i<26; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[26]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Jednotka = " g", "kg", " t"
  KopirujZnaky(s1,s,7,2);   // Do s1 dam Jendotky
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne mezery
  strcpy(Jednotky,s1);
  // Max
  KopirujZnaky(s1,s,10,7);   // Do s1 dam max. vazivost
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne mezery
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Max,s1);
  // Ustaleni
  KopirujZnaky(s1,s,18,6);   // Do s1 dam Dilek
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne mezery
  strcpy(Dilek,s1);
  return true;
}




// ******************************************************************
// ************* FCE koncici Str vraceji vysledky ve stringu ********
// ******************************************************************

bool Taruj(HANDLE h, char *VV, char* Chyba) {
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#AT#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,11)<11) return false; // Nactu z Enigmy odpoved - 11 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<10; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[10]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Chyba
  KopirujZnaky(s1,s,7,1);   // Do s1 dam Chybu
  strcpy(Chyba,s1);
  return true;
}

bool TarujBezUstaleni(HANDLE h, char *VV, char* Chyba) {
  // Novy telegram ve verzi 5.03 ze dne 19.10.2000. Vytaruje, i kdyz neni vaha ustalena
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#AU#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,11)<11) return false; // Nactu z Enigmy odpoved - 11 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<10; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[10]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Chyba
  KopirujZnaky(s1,s,7,1);   // Do s1 dam Chybu
  strcpy(Chyba,s1);
  return true;
}

bool Nuluj(HANDLE h, char *VV, char* Chyba) {
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#NL#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,11)<11) return false; // Nactu z Enigmy odpoved - 11 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<10; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[10]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Chyba
  KopirujZnaky(s1,s,7,1);   // Do s1 dam Chybu
  strcpy(Chyba,s1);
  return true;
}

bool NactiDigVystupy(HANDLE h, char *VV, char *Vysledek) {
  // Nacte stav vsech dig. vystupu Enigmy
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#MR#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,21)<21) return false; // Nactu z Enigmy odpoved - 21 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<20; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[20]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Jednotlive vystupy
  for (int i=0; i<4; i++) {
    Vysledek[i]=s[8+i*3];
  }//for i
  return true;
}

bool NactiDigVystup(HANDLE h, char *VV, int CisloVystupu, char *Typ, double *ZadaneNetto, double *ZadanaHystereze) {
  // Nacte stav a charakter jednoho dig. vystupu Enigmy.
  // CisloVystupu je od jednicky.
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#OR#");
  itoa(CisloVystupu, s1, 10);
  strcat(s,s1);
  strcat(s,"#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,31)<31) return false; // Nactu z Enigmy odpoved
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<30; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[30]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  if (s[27]!='0') return false;   // Chybovy kod
  if (Typ!=NULL) {
    Typ[0]=s[9]; Typ[1]=0;   // L, H, R (rostouci) nebo F (klesajici)
  }//if
  if (ZadaneNetto!=NULL) {
    KopirujZnaky(s1,s,11,7);
    for (int i=0; i<(int)strlen(s1); i++) if (s1[i]==',') s1[i]='.';  // Carku nahradim teckou
    *ZadaneNetto=StrToRel(s1);
  }//if
  if (ZadanaHystereze!=NULL) {
    KopirujZnaky(s1,s,19,7);
    for (int i=0; i<(int)strlen(s1); i++) if (s1[i]==',') s1[i]='.';  // Carku nahradim teckou
    *ZadanaHystereze=StrToRel(s1);
  }//if
  return true;
}

bool ProgramujDigVystupy(HANDLE h, char *VV, int V1, int V2, int V3, int V4) {
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#MW#");
  if (V1==1) strcat(s,"H#"); else strcat(s,"L#");
  if (V2==1) strcat(s,"H#"); else strcat(s,"L#");
  if (V3==1) strcat(s,"H#"); else strcat(s,"L#");
  if (V4==1) strcat(s,"H#"); else strcat(s,"L#");
  strcat(s,"\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,9)<9) return false; // Nactu z Enigmy odpoved - 9 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<8; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[8]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  return true;
}

bool ProgramujDigVystup(HANDLE h, char *VV, int CisloVystupu, bool Hodnota) {
  // Naprogramuje 1 vystup jako obecny (tj. L nebo H).
  // CisloVystupu je od jednicky. True=H, false=L.
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#OW#");

  itoa(CisloVystupu, s1, 10);
  strcat(s,s1);
  strcat(s,"#");
  if (Hodnota) strcat(s,"H#"); else strcat(s,"L#");
  strcat(s,"\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  // !!!!!!!!!!!!!!!!
  if (NactiPort(h,s,11)<11) return false; // Nactu z Enigmy odpoved - 9 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<10; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[10]) return false;    // Statne CRC

  if (s[7]!='0') return false;     // Nula znamena, ze nenastala chyba
  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  return true;
}

bool ProgramujDigVystupJakoStandardni(HANDLE h, char *VV, int CisloVystupu) {
  // Naprogramuje 1 vystup jako standardni s predtim nastavenymi hodnotami.
  // CisloVystupu je od jednicky.
  // Delam to tak, ze nejdriv nactu typy vsech vystupu, jeden zmenim a zase vsechny zapisu
  char s[100],s1[10];
  char Vysledek[5];
  unsigned char CRC=0;

  // Nacteni typu vsech vystupu
  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#MR#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,21)<21) return false; // Nactu z Enigmy odpoved - 21 znaku
  // V s je ted cely telegram:
  // Typy jednotlivych vystupu
  for (int i=0; i<4; i++) {
    Vysledek[i]=s[7+i*3];   // 'S' (standardni) nebo 'G' (obecny)
    if (Vysledek[i]=='G') Vysledek[i]=s[8+i*3];   // Pokud je obecny, musim tam dat H nebo L (kvuli dalsimu kodu)
  }//for i
  // Ted zmenim zadany vystup na standardni
  Vysledek[CisloVystupu-1]='S';
  // A zapisu to do Enigmy
  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#MW#");
  for (int i=0; i<4; i++) {
    if (Vysledek[i]=='S') strcat(s,"S#");
      else if (Vysledek[i]=='H') strcat(s,"H#");
      else strcat(s,"L#");
  }//for i
  strcat(s,"\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,9)<9) return false; // Nactu z Enigmy odpoved - 9 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<8; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[8]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);

  return true;
}

bool ProgramujDigVystupJakoStandardniSMezi(HANDLE h, char *VV, int CisloVystupu, char *Charakter, int Mez, int Hystereze) {
  // Naprogramuje 1 vystup jako standardni a nastavi nove hodnoty
  // CisloVystupu je od jednicky.
  // Charakter je bud "R" (rostouci) nebo "F" (klesajici)
  char s[100],s1[10], s2[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#OW#");

  itoa(CisloVystupu, s1, 10);
  strcat(s,s1);
  strcat(s,"#");
  strcat(s,Charakter);
  strcat(s,"#");

  itoa(Mez,s1,10);
  s2[0]=0;
  for (int i=1;i<=(int)(7-strlen(s1));i++) strcat(s2,"0"); // doplneni nul k vaze
  strcat(s2,s1); // Vaha ktere se ma dosahnout i s pocatecnimi nulami (7 znaku)
  strcat(s,s2);
//  strcat(s,FormatFloat("0000000", Mez).c_str());

  strcat(s,"#");

  itoa(Hystereze, s1, 10);
  s2[0]=0;
  for (int i=1;i<=(int)(7-strlen(s1));i++) strcat(s2,"0"); // doplneni nul k vaze
  strcat(s2,s1); // Vaha ktere se ma dosahnout i s pocatecnimi nulami (7 znaku)
  strcat(s,s2);
//  strcat(s,FormatFloat("0000000", Hystereze).c_str());

  strcat(s,"#");

  strcat(s,"\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,11)<11) return false; // Nactu z Enigmy odpoved - 9 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<10; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[10]) return false;    // Statne CRC

  if (s[7]!='0') return false;     // Nula znamena, ze nenastala chyba
  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  return true;
}

bool NactiDigVstup(HANDLE h, char *VV, int CisloVstupu) {

  // RADEJI VYUZIVAT FCI NactiDigVstupSChybou()  !!!!!!!!!!!!!!

  // Nacte stav zadaneho dig. vstupu Enigmy a vrati jej (true=H, false=L)
  // CisloVstupu je od jednicky
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#IR#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,21)<21) return false; // Nactu z Enigmy odpoved - 21 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<20; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[20]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Nactu zadany vstup
  if (s[8+(CisloVstupu-1)*3]=='H') return true; else return false;
}

bool NactiDigVstupSChybou(HANDLE h, char *VV, int CisloVstupu, bool &Hodnota) {
  // Nacte stav zadaneho dig. vstupu Enigmy a vrati jej v promenne Hodnota (true=H, false=L)
  // Pokud dojde k chybe, vrati false, jinak true
  // CisloVstupu je od jednicky
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#IR#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,21)<21) return false; // Nactu z Enigmy odpoved - 21 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<20; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[20]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Nactu zadany vstup
  if (s[8+(CisloVstupu-1)*3]=='H') Hodnota=true; else Hodnota=false;
  return true;    // Vse v poradku
}

bool ProgramujDigVstupy(HANDLE h, char *VV, char *V1, char *V2, char *V3, char *V4) {
  // Programuje vsecny vystupy. Muze byt "S" (standardni) nebo "G" (obecny)
  char s[100],s1[10];
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#IW#");
  strcat(s,V1);
  strcat(s,"#");
  strcat(s,V2);
  strcat(s,"#");
  strcat(s,V3);
  strcat(s,"#");
  strcat(s,V4);
  strcat(s,"#");
  strcat(s,"\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  if (NactiPort(h,s,9)<9) return false; // Nactu z Enigmy odpoved - 9 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<8; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[8]) return false;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  return true;
}

void NactiVahu(HANDLE h, char *VV, float* Neto, float *Tara, bool *Ustaleni, int *Chyba) {
  // Telegram SC
  char s[100],s1[10];
  int i;
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#SC#0#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram
  NactiPort(h,s,32); // Nactu z Enigmy odpoved - 32 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (i=1; i<31; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[31]) return;    // Statne CRC

  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Neto
  KopirujZnaky(s1,s,7,8);   // Do s1 dam neto
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  *Neto=StrToRel(s1);
  // Tara
  KopirujZnaky(s1,s,16,8);   // Do s1 dam taru
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  *Tara=StrToRel(s1);
  // Ustaleni
  KopirujZnaky(s1,s,25,1);   // Do s1 dam ustaleni
  if (s1[0]=='1') *Ustaleni=true; else *Ustaleni=false;
  // Chyba
  KopirujZnaky(s1,s,27,2);   // Do s1 dam chybu
  *Chyba=atoi(s1);
}

bool NastavTaru(HANDLE h, char *VV, int Tara) {
  // Nastavi rucne taru
  // Tara musi byt cele cislo, tzn. pokud chci napr. 6,75, musim zadat 675.
  char s[100],s1[10], s2[10];
//  int i;
  unsigned char CRC=0;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#ET#");

  itoa(Tara, s1, 10);
  s2[0]=0;
  for (int i=1;i<=(int)(7-strlen(s1));i++) strcat(s2,"0"); // doplneni nul k vaze
  strcat(s2,s1); // Vaha ktere se ma dosahnout i s pocatecnimi nulami (7 znaku)
  strcat(s,s2);
//  strcat(s, FormatFloat("0000000", Tara).c_str());

  strcat(s,"#\003");
  PridejBCC(s);
  if (!ZapisPort(h,s,DelkaProtokolu(s))) return false; // Poslu do Enigmy telegram
  if (NactiPort(h,s,11)<11) return false; // Nactu z Enigmy odpoved - 32 znaku
  // V s je ted cely telegram:

  // 19.12.2001: Zkontroluju CRC - nezahrnuju tam STX a BCC
  for (int i=1; i<10; i++) CRC^=(unsigned char)s[i];
  if ((unsigned char)CRC!=(unsigned char)s[10]) return false;    // Statne CRC

  if (s[7]!='0') return false;    // Chybovy kod
  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  return true;
}

bool NactiBrutto(HANDLE h, char *VV, double &Brutto) {
  char s1[10],s2[10],s3[10],s4[10];
  if (!NactiVahuStr(h, VV, s1, s2, s3, s4)) return false;
  Brutto=atof(s1)+atof(s2);
  return true;
}
