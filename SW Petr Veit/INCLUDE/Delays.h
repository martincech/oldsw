#include <TIME.H>
#include <DOS.H>

void Cekej(DWORD ms) { // Ceka ms milisekund
  DWORD t1,t2;
  t1=GetTickCount();
  do {
    t2=GetTickCount();
  } while (t2-t1<ms);
}

double RozdilCasu(struct time &T1) { // Vraci rozdil casu T1 od casu skutecneho
  struct time T2;
  double Rozdil,R0;
  R0=(float)0.01*T1.ti_hund+(float)T1.ti_sec+(float)60*T1.ti_min+(float)3600*T1.ti_hour;
  gettime(&T2);
  Rozdil=(float)0.01*T2.ti_hund+(float)T2.ti_sec+(float)60*T2.ti_min+(float)3600*T2.ti_hour-R0;
  if (Rozdil<0) Rozdil=0;
  return Rozdil;
}

