// Header pro komunikaci s jednotkou Vishay VT200/220



#define STX 0x02
#define ETX 0x03

bool Vt200NactiVahuStr(HANDLE h, char* Neto, char *Ustaleni) {
  char s[100],s1[20];
  int Pocet;
  double Hmotnost;
  String Str;

  // Dotazu se na vahu
  s[0] = STX;
  s[1] = 0x41;
  s[2] = 0x3F;
  s[3] = 0x30;
  s[4] = 0x3C;
  s[5] = 0x34;
  s[6] = ETX;

  ZapisPort(h,s,7); // Poslu telegram

  if (NactiPort(h, s, 18) < 18) {
    return false;
  }
  // Kontrola pevnych znaku
  if (s[0] != STX || s[17] != ETX) {
    return false;       // Spatny format zpravy
  }

  // Testy
/*  strcpy(s, "A?0P+018.2510351");
  strcpy(s, "A?0@+017.02103?0");
  strcpy(s, "A?0H+^^^^^^102<1");
  strcpy(s, "A?0H-UUUUUU103;1");*/

  // Prevedu hmotnost
  KopirujZnaky(s1, s, 6, 6);   // Do s1 dam hmotnost bez znamenka
  if (s1[0] == '^') {
    // Pretizeni
    strcpy(Neto, "70.00");
  } else if (s1[0] == 'U') {
    // Podteceni
    strcpy(Neto, "70.00");
  } else {
    // Hmotnost v poradku
    Hmotnost = StrToRel(s1);
    // Znamenko hmotnsoti
    if (s[5] == '-') {
      Hmotnost = -Hmotnost;
    }
    // Prevedu na string
    Str = String(Hmotnost);
    strcpy(s1, Str.c_str());
    int i = NajdiZnak(s1,',');
    if (i >= 0) s1[i] = '.';  // Zmena carky na tecku
    strcpy(Neto, s1);
  }//else

  // Ustaleni
  if (s[4] & 0x10) {
    strcpy(Ustaleni, "1");
  } else {
    strcpy(Ustaleni, "0");
  }

  return true;
}

bool Vt200NactiUstalenouVahuStr(HANDLE h, char* Neto) {
  char Net[100], Ust[10];
  if (!Vt200NactiVahuStr(h, Net, Ust)) return false;
  if (Ust[0]!='1') return false;   // Vaha neni ustalena
  strcpy(Neto,Net);   // Zkopiruju ustalene netto
  return true;
}

bool Vt200NactiParametryStr(HANDLE h, char *VV, char* Jednotky, char *Max, char *Dilek) {
  // VT200 - napevno

  // VV - cislo jednotky
  strcpy(VV,"1");
  // Jednotka = " g", "kg", " t"
  strcpy(Jednotky,"t");
  // Max
  strcpy(Max,"60.00");
  // Ustaleni
  strcpy(Dilek,"3000");   // 3000 dilku
  return true;
}


