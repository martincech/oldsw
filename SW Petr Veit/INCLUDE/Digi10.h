// Header pro komunikaci s jednotkou EnigmaPC / Mvz5


//#include <serial32.h>
//#include <Stringyw.h>



// ******************************************************************
// ************* FCE koncici Str vraceji vysledky ve stringu ********
// ******************************************************************
bool Digi10NactiVahuStr(HANDLE h, char *VV, char* Neto, char *Tara, char *Ustaleni, char *Chyba) {
  // Digi10 - ne
  strcpy(VV,"1");
  strcpy(Neto,"0");
  strcpy(Tara,"0");
  strcpy(Ustaleni,"0");
  strcpy(Chyba,"");
  return true;
}

bool Digi10NactiUstalenouVahuStr(HANDLE h, char *VV, char* Neto, char *Tara, char *Chyba) {
  // Telegram SC, ale vse vraci ve stringech - vhodne pro Compact
  char s[50],s1[10];
  double d;
  String Str;
  int i;

  s[0]=0x05; s[1]=0;   // Prikaz ENQ = 05h
  ZapisPort(h,s,1);
  // DIGI posila DC1 or DC2
  if (NactiPort(h,s,1)<1) return false;
  // Poslu ACK
  s[0]=0x06; s[1]=0;
  ZapisPort(h,s,1);
  // DIGI posila data
  if (NactiPort(h,s,30)<30) return false;

  // VV - cislo jednotky
  if (VV!=NULL) strcpy(VV,"1");
  // Neto
  if (Neto!=NULL) {
    KopirujZnaky(s1,s,11,7);   // Do s1 dam neto
    while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
    d=StrToRel(s1);
    d=d/1000;   // Prevedu na tuny
    Str=FormatFloat("0.00",d);
    strcpy(s1, Str.c_str());
    i=NajdiZnak(s1,',');
    if (i>=0) s1[i]='.';  // Zmena carky na tecku
    strcpy(Neto,s1);
  }
  // Tara
  if (Tara!=NULL) strcpy(Tara,"0");
  // Ustaleni
  if (Chyba!=NULL) strcpy(Chyba,"");

  // Poslu ACK
  s[0]=0x06; s[1]=0;
  ZapisPort(h,s,1);
  // DIGI posila EOT
  if (NactiPort(h,s,1)<1) return false;

  return true;
}

bool Digi10NactiParametryStr(HANDLE h, char *VV, char* Jednotky, char *Max, char *Dilek) {
  // Digi10 - napevno

  // VV - cislo jednotky
  strcpy(VV,"1");
  // Jednotka = " g", "kg", " t"
  strcpy(Jednotky,"t");
  // Max
  strcpy(Max,"50.00");
  // Ustaleni
  strcpy(Dilek,"3000");
  return true;
}




// ******************************************************************
// ************* FCE koncici Str vraceji vysledky ve stringu ********
// ******************************************************************

bool Digi10Taruj(HANDLE h, char *VV, char* Chyba) {
  // Digi10 - netaruje
  return false;
}

bool Digi10Nuluj(HANDLE h, char *VV, char* Chyba) {
  // Digi10 - nenuluje
  return false;
}

bool Digi10ProgramujDigVystupy(HANDLE h, char *VV, int V1, int V2, int V3, int V4) {
  // Digi10 - ne
  return false;
}

void Digi10NactiVahu(HANDLE h, char *VV, float* Neto, float *Tara, bool *Ustaleni, int *Chyba) {
}

