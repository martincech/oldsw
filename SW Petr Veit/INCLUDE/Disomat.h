// Header pro komunikaci s jednotkou Schenck Disomat - pro Fenix


//#include <serial32.h>
//#include <Stringyw.h>


/*int DelkaProtokolu(char *s) {
  // Je-li BCC=0H, da mne strlen() spatnou velikost => musim sam, hledam ETX a pak +1 (BCC)
  int i=1;
  while (s[i-1]!=3) i++;  // Hledam ETX=03H
  i++;  // Jeste pridam BCC
  return i;
}*/


// ******************************************************************
// ************* FCE koncici Str vraceji vysledky ve stringu ********
// ******************************************************************
bool DisomatNactiVahuStr(HANDLE h, char *VV, char* Neto, char *Tara, char *Ustaleni, char *Chyba) {
  // Telegram PC
  char s[100],s1[10];
  int i;

  // --------- Poslu telegram do Disomatu
  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#PC#12345678#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu telegram do Disomatu
  // Disomat posila string s promennou delkou (kvuli textu chyby)
  // --------- Nactu vse zacinajici STX a koncici ETX
  s[0]=0;
  do {  // Budu cist znak po znaku az dojdu k ETX, pak prectu jeste 1 bajt - BCC
    if (NactiPort(h,s1,1)<1) return false; // Chyba, znak neni ve fronte
    strcat(s,s1);   // Nacteny znak pridam do stringu
  } while (s1[0]!=3); // Dokud nenactu ETX
  // Jeste za ETX je BCC - ten taky nactu a je nacteny cely telegram
  if (NactiPort(h,s1,1)<1) return false; // Chyba, znak neni ve fronte
  strcat(s,s1);   // Nacteny BCC pridam do stringu

  // --------- Ted mam v s cely string, ktery mne poslal Disomat => analyzuju
  // Nejdriv zkontroluju prijaty string (kontroluju pozice #)
  if (s[3]!='#' || s[6]!='#' || s[15]!='#') {  // Od 15 je to promenne
    // Nekde nastala chyba
    return false;
  }
  // WM - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Vymazu prvni znaky ze stringu
  VymazZnaky(s,0,16);
  // Ted uz je rozmisteni '#' promenne

  // Neto
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam neto
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Neto,s1);

  // Tara
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam taru
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Tara,s1);

  // Ustaleni - ctu status bajt
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam staus bajt - to jsou 2 znaky - napr. "c0"
  i=HexToInt(s1);
  if ((i&128)==128) strcpy(Ustaleni,"1"); else strcpy(Ustaleni,"0");  // Pokud je nastaven bit, je ustaleni

  // Chybu nedelam...
  return true;
}

bool DisomatNactiUstalenouVahuStr(HANDLE h, char *VV, char* Neto, char *Tara, char *Chyba) {
  // Telegram TS
  char s[50],s1[10];
  int i;

  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#TS#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram

  // Disomat hned posle, ze ceka na ustaleni - pevne 11 znaku
  if (NactiPort(h,s,11)<11) return false;

  // Ted nactu, jestli se ustalil
  // Disomat posila string s promennou delkou (kvuli textu chyby)
  // --------- Nactu vse zacinajici STX a koncici ETX
  s[0]=0;
  do {  // Budu cist znak po znaku az dojdu k ETX, pak prectu jeste 1 bajt - BCC
    if (NactiPort(h,s1,1)<1) return false; // Chyba, znak neni ve fronte
    strcat(s,s1);   // Nacteny znak pridam do stringu
  } while (s1[0]!=3); // Dokud nenactu ETX
  // Jeste za ETX je BCC - ten taky nactu a je nacteny cely telegram
  if (NactiPort(h,s1,1)<1) return false; // Chyba, znak neni ve fronte
  strcat(s,s1);   // Nacteny BCC pridam do stringu

  // --------- Ted mam v s cely string, ktery mne poslal Disomat => analyzuju
  // Nejdriv zkontroluju prijaty string (kontroluju pozice #)
  if (s[3]!='#' || s[6]!='#') {
    // Nekde nastala chyba
    return false;
  }

  // WM - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Vymazu prvni znaky ze stringu
  VymazZnaky(s,0,7);

  // Neto
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam neto
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  if (Neto!=NULL) {
    i=NajdiZnak(s1,',');
    if (i>=0) s1[i]='.';  // Zmena carky na tecku
    strcpy(Neto,s1);
  }

  // Tara
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam taru
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  if (Tara!=NULL) {
    i=NajdiZnak(s1,',');
    if (i>=0) s1[i]='.';  // Zmena carky na tecku
    strcpy(Tara,s1);
  }  

  // Ustaleni - ctu status bajt
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam staus bajt - to jsou 2 znaky - napr. "c0"
  i=HexToInt(s1);
  if ((i&128)==128) {  // Je ustalena
    if (Chyba!=NULL) Chyba[0]=0;
    return true;
  } else {
    if (Chyba!=NULL) strcpy(Chyba,"1");
    return false;
  }
}


bool DisomatNactiParametryStr(HANDLE h, char *VV, char* Jednotky, char *Max, char *Dilek) {
  // Telegram PC - je to stejny telegram jako v NactiVahuStr()
  char s[100],s1[10];
  int i;

  // --------- Poslu telegram do Disomatu
  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#PC#12345678#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu telegram do Disomatu
  // Disomat posila string s promennou delkou (kvuli textu chyby)
  // --------- Nactu vse zacinajici STX a koncici ETX
  s[0]=0;
  do {  // Budu cist znak po znaku az dojdu k ETX, pak prectu jeste 1 bajt - BCC
    if (NactiPort(h,s1,1)<1) return false; // Chyba, znak neni ve fronte
    strcat(s,s1);   // Nacteny znak pridam do stringu
  } while (s1[0]!=3); // Dokud nenactu ETX
  // Jeste za ETX je BCC - ten taky nactu a je nacteny cely telegram
  if (NactiPort(h,s1,1)<1) return false; // Chyba, znak neni ve fronte
  strcat(s,s1);   // Nacteny BCC pridam do stringu

  // --------- Ted mam v s cely string, ktery mne poslal Disomat => analyzuju
  // Nejdriv zkontroluju prijaty string (kontroluju pozice #)
  if (s[3]!='#' || s[6]!='#' || s[15]!='#') {  // Od 15 je to promenne
    // Nekde nastala chyba
    return false;
  }

  // WM - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Vymazu prvni znaky ze stringu
  VymazZnaky(s,0,16);
  // Ted uz je rozmisteni '#' promenne

  // Neto
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  // Tara
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  // Ustaleni - ctu status bajt
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim

  // Max v kilogramech - podle nactenych jednotek to musim pozdeji prevest!!!
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam Max
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Max,s1);

  // Dilky
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam dilky
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  i=NajdiZnak(s1,',');
  if (i>=0) s1[i]='.';  // Zmena carky na tecku
  strcpy(Dilek,s1);

  // Jednotky
  i=NajdiZnak(s,'#');   // Najdu konec cisla
  KopirujZnaky(s1,s,0,i);   // Do s1 dam jednotky
  while (s1[0]==' ') VymazZnak(s1,0);  // Umazu nadbytecne nuly
  VymazZnaky(s,0,i+1);   // Vymazu cislo vcetne znaku '#' za nim
  i=atoi(s1);
  switch (i) {
    case 0: {
      strcpy(Jednotky,"kg");
      break;
    }
    case 1: {
      strcpy(Jednotky,"g");
      // Musim upravit max. vazivost na g:
      i=atoi(Max);
      itoa(i*1000,Max,10);
      break;
    }
    default: {
      strcpy(Jednotky,"t");
      // Musim upravit max. vazivost na t:
      i=atoi(Max);
      itoa(i/1000,Max,10);
    }
  }

  return true;
}




bool DisomatNuluj(HANDLE h, char *VV, char* Chyba) {
  char s[100],s1[10];

  // --------- Poslu telegram do Disomatu
  s[0]=2;s[1]=0;
  strcat(s,VV);
  strcat(s,"#AZ#\003");
  PridejBCC(s);
  ZapisPort(h,s,DelkaProtokolu(s)); // Poslu do Enigmy telegram

  // --------- Nactu telegram z Disomatu - nulovani ma pevnou delku 11 bajtu
  // Po vyslani zadosti o nulovani Disomat okamzite odpovi, ze zacina nulovat
  if (NactiPort(h,s,11)<11) return false; // Nactu z Disomatu odpoved - 11 znaku
  // Nactu znovu, zda opravdu vynuloval
  if (NactiPort(h,s,11)<11) return false; // Nactu z Disomatu odpoved - 11 znaku

  // V s je ted cely telegram:
  // VV - cislo jednotky
  KopirujZnaky(s1,s,1,2);   // Do s1 dam VV
  strcpy(VV,s1);
  // Chyba
  KopirujZnaky(s1,s,7,1);   // Do s1 dam Chybu
  strcpy(Chyba,s1);
  return true;
}


