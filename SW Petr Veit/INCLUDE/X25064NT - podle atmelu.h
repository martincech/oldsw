// Ovladani EEPROM Xicor 25064 pomoci SPI serioveho protokolu pres
// paralelni port
// Prevzato z Xicor SerialFlash DataBook

// Pouzivam zde knihovno DriverLINX, takze to jde i v NT.
// Hlavni aplikace musi prilinkovat soubor DLPORTIO.LIB.


#ifndef __X25064NT_H   // Aby to nedefinovalo vicekrat
#define __X25064NT_H


#include <dlportio.h>    // Header knihovny DriverLINX


// **************** Timto se uz da ovladat i EEPROM primo, bez invertoru,
// **************** je zde volba zda inverotvat, ci ne.

unsigned short int PORT=0x378;  // Adresa LPT1

unsigned char Maska=0xff;        // Maska 11111111  (napajeci piny vzdy high)

int PocetDelay=0;  // Kalibrace pro delay

// !!!!!!!!!!!!!!!!!1 Toto nastavuje aplikace, ktera unit pouziva !!!!!!!!!!!!!!!!!!!!
bool Invertovat=false;   // Jestli je mezi PC a kartou invertor (napr. v EggCrusher ano, ale u Misaka ne)




// Fce v assembleru:
unsigned char InPort(unsigned short int Adresa) {
  return DlPortReadPortUchar(Adresa);
}

void OutPort(unsigned short int Adresa, unsigned char Co) {
  DlPortWritePortUchar(Adresa, Co);
}


// Cekani
void InitDelay() {  // Kalibruje cekani pro podle rychlosti pocitace - volat na zacatku
  // Pro Xicor staci delay 1us
  // 3.10.2001: Nyni se pouzivaji EEPROMy Atmel, ktere jsou pomalejsi - jeste pri 20us to blblo, dal jsem radsi 100us, to uz je v poradku
  const PocetMikrosekund=10;   // Kolik mikrosekund chcu cekat
  DWORD t1,t2,t;
  PocetDelay=20000000;
  t1=GetTickCount();
  for (int i=1;i<=PocetDelay;i++);
  t2=GetTickCount();
  t=t2-t1;// V t je ted pocet milisekund potrebnych k 20 milionu NOPu
  PocetDelay=PocetDelay*PocetMikrosekund/(1000*t);
}

void Delay() {
//  Cekej(1);
  for (int i=1;i<=PocetDelay;i++);
}


// -----------------------------------------------------------------
// Operace s piny - tady se provadi pripadne inverze

// Piny jsou v bajtu rozmisteny nasledovne:
//  bit: 7   6   5   4   3   2   1   0
//       CS  Ucc Ucc Ucc Ucc Ucc SDI SCL
// Ucc ... napajeci piny (vsechny vzdy High - to zajisti maska, ktera je na zacatku 0xFF, na napajeci piny se pak uz nikdy nepristupuje)


void SCLHigh() {
  // SCL = D0
  if (Invertovat) Maska=Maska&(unsigned char)0xFE; else Maska=Maska|(unsigned char)0x01;
  OutPort(PORT,Maska);
}

void SCLLow() {
  // SCL = D0
  if (Invertovat) Maska=Maska|(unsigned char)0x01; else Maska=Maska&(unsigned char)0xFE;
  OutPort(PORT,Maska);
}

// ---------------
void SDIHigh() {
  // SDI = D1
  if (Invertovat) Maska=Maska&(unsigned char)0xFD; else Maska=Maska|(unsigned char)0x02;
  OutPort(PORT,Maska);
}

void SDILow() {
  // SDI = D1
  if (Invertovat) Maska=Maska|(unsigned char)0x02; else Maska=Maska&(unsigned char)0xFD;
  OutPort(PORT,Maska);
}

// ---------------
void CSHigh() {
  // CS = D7
  if (Invertovat) Maska=Maska&(unsigned char)0x7F; else Maska=Maska|(unsigned char)0x80;
  OutPort(PORT,Maska);
}

void CSLow() {
  // CS = D7
  if (Invertovat) Maska=Maska|(unsigned char)0x80; else Maska=Maska&(unsigned char)0x7F;
  OutPort(PORT,Maska);
}

// ---------------
void Hodiny() {
  SCLHigh();
  Delay();
  SCLLow();
  Delay();////////////
}

// ---------------
 unsigned char NactiSDO() {   // Pouziva se jen u karet - tam je misto SDO pripojena detekce karty
  // 7. bit n a adr. 379H (=378H+1) je negovany BUSY => pred vyhodnocenim neguju
  unsigned char ch=InPort(PORT+1);
  ch =(unsigned char)( ~ch & 128);  // Tady neguju nacteny bajt kvuli negaci BUSY (vzi vyse)
  ch=(unsigned char)(ch>>7);                 // Posunu na LSB
  Hodiny();                 // Udelam hodiny
  // Pokud se ma invertovat, invertuju:
  if (Invertovat) {
    if (ch==1) ch=0; else ch=1;
  }
//  if (ch==1) return 0; else return 1;  // Musim invertovat
  return ch;
}

// ------------------------------------------------------------------
// Operace s bajty - tady uz se nezajimam o to, zda invertovat ci ne (to obslouzi fce SDIHigh(), SDILow() atd.)

void PosliByteSPI(unsigned char Co) {
  for (unsigned char i=0; i<=7; i++) {
    SCLLow(); Delay();
    if ((Co & 0x80) ==0) SDILow();  // Kdyz neni bit, dam do nuly
      else SDIHigh();               // Jinak do 1
    Co=(unsigned char)(Co<<1);
    Delay();   ////////////////////////////
    SCLHigh();
    Delay();
  }
///////////  SDILow();
}

unsigned char NactiByteSPI() {
  // 7. bit n a adr. 379H (=378H+1) je negovany BUSY => pred vyhodnocenim neguju
  unsigned char byte=0,ch;
  for (int count=0; count<=7; count++) {
///////////    Hodiny();
    SCLLow();  //////////////////
    Delay();
    byte=(unsigned char)(byte << 1);
    ch=InPort(PORT+1);
    ch =(unsigned char)(~ch & 128);  // Tady neguju nacteny bajt kvuli negaci BUSY (viz vyse)
    ch=(unsigned char)(ch>>7);                 // Posunu na LSB
    if (ch==1) byte=(unsigned char)(byte | 0x01);
    SCLHigh();  //////////////////
    Delay();  ////////////////
  }
  return byte;
}

unsigned char Rdsr() {  // Nacte Status Register
  unsigned char byte;
////////////  SCLLow();
  Delay();   /////////////  Toto sice v atmelu nebylo, ale pres dlouhy drat by to mohlo byt treba
  CSLow();
  Delay();    ///////////////
  PosliByteSPI(0x05);
  byte=NactiByteSPI();
////////////  SCLLow();
  Delay();  /////////////
  CSHigh();
  Delay(); //////////
  return byte;
}

void WipPoll() {   // Pocka na provedeni zapisu
  unsigned char byte;
//  unsigned char ch=0;
  // 4.10.2001: Zapis muze trvat i 20ms, cekam maximalne 40ms
  int PocatecniCas=GetTickCount();
  do {
    byte=Rdsr();     // Nactu Status Register
    byte=(unsigned char)(byte & 0x01);  // Jen WIP bit
//    ch++;
  } while (byte!=0 && /*ch<250*/(GetTickCount()-PocatecniCas < 40)); // Dokud neni zapsano nebo max. 250x, aby se to neseklo kdyz tam neni karta
  Delay(); ///////////
}

void Wren() {   // Prikaz Write Enable
  CSLow();
  Delay();  /////////////
  PosliByteSPI(0x06);     // Prikaz Wren
  SCLLow();
  Delay();   /////////////
  CSHigh();
  Delay();
}

void Wrdi() {   // Prikaz Write Disable      //////////////////// oddelano, neni treba
  CSLow();
  Delay();
  PosliByteSPI(0x04);     // Prikaz Wrdi
  SCLLow();
  Delay();
  CSHigh();
  Delay();
}

void Wrsr(unsigned char byte) {  // Zapise Status Register
  CSLow();
  Delay();
  PosliByteSPI(0x01);
  PosliByteSPI(byte);
  SCLHigh();
  Delay();  //////////////
  CSHigh();
  Delay();//////////////
}

// ------------------------------------------------------------------
// Operace s daty v karte

unsigned char NactiByteSPI(short int Adresa) {
  unsigned char AdresaHi, AdresaLo;
//////////  Wren();
//////////  CSHigh();
//////////  SCLLow();
//////////  Delay();
  CSLow();
  Delay();  ////////////
  PosliByteSPI(0x03);                   // Poslu prikaz Read
  AdresaHi=(unsigned char)((Adresa & 0x0FF00) >> 8);   // Rozhodim adresu na 2 bajty
  AdresaLo=(unsigned char)(Adresa & 0x0FF);
  PosliByteSPI(AdresaHi);               // Poslu adresu
  PosliByteSPI(AdresaLo);
  // Vyuziju AdresaHi
  AdresaHi=NactiByteSPI();  // Nactu jed. bajty
/////////  SCLLow();
  CSHigh();
  Delay();
  return AdresaHi;
}

void NactiSPI(int PocetBytu, short int Adresa, char* Kam) {
  // Nacte PocetBytu bajtu a adresy Adresa do Kam
  for (short int i=Adresa;i<Adresa+PocetBytu;i++) {
    Kam[i-Adresa]=NactiByteSPI(i);
  }
}

void UlozByteSPI(short int Adresa, unsigned char Co) {
  // Nacte PocetBytu bajtu a adresy Adresa do Kam
  unsigned char AdresaHi, AdresaLo;

  WipPoll();
  Wrsr(0);
  Wren();                                                            // Set write enable latch
  Delay();/////////////
  CSLow();
  PosliByteSPI(0x02);                   // Poslu prikaz Write
  AdresaHi=(unsigned char)((Adresa & 0x0FF00) >> 8);   // Rozhodim adresu na 2 bajty
  AdresaLo=(unsigned char)(Adresa & 0x0FF);
  PosliByteSPI(AdresaHi);               // Poslu adresu
  PosliByteSPI(AdresaLo);
  PosliByteSPI(Co);  // Nactu jed. bajty
  SCLLow();
  Delay();
  CSHigh();
  Delay();
  WipPoll();
  Delay();
}

void UlozSPI(int PocetBytu, short int Adresa, char* Co) {
  // Nacte PocetBytu bajtu a adresy Adresa do Kam

  for (short int i=Adresa;i<Adresa+PocetBytu;i++) {
    UlozByteSPI(i,Co[i-Adresa]);
  }
}

#endif

