// Header pro zakodovani jmena - pro demoverzi Fenixu


int ZakodujString(String Str) {
  // Vrati cislo
  int Kod=0;
  for (int i=1; i<=Str.Length(); i++) {
    if (i%10!=0) Kod+=1346+Str[i];  // Pro liche pricitam - aby to pro prvni znak nenasobilo nulu
      else Kod*=Str[i]/5;
  }
  // Pokud je kod prilis kratky, tak jej prodlouzim
  if (Kod<=0) Kod=987484613;   // Kdyby nahodou vysel Kod nulovy, tak by se to zacyklilo
  while (Kod<100000000) Kod*=11;
  return Kod;
}

unsigned long NactiSerioveCisloDisku() {
  // Nacte seriove cislo logickeho disku C:
  char VolumeLabel[255], FileSystem[255];
  DWORD SerioveCislo, DW, SysFlags;

  GetVolumeInformation("c:\\", VolumeLabel, sizeof(VolumeLabel),
                         &SerioveCislo, &DW, &SysFlags,
                         FileSystem, sizeof(FileSystem));
  return SerioveCislo;
}

String NactiSerioveCisloDiskuVHexu() {
  // Nacte ser. cislo pomoci NactiSerioveCisloDisku() a prevede jej na HEX
  char s[30];
  ultoa((unsigned long)NactiSerioveCisloDisku(), s, 16);
  return String(s).UpperCase();
}

int ZakodujSerioveCisloDisku() {
  // Nacte seriove cislo disku pomoci NactiSerioveCisloDisku(), prevede to na HEX a zakoduje jej pomoci ZakodujString()
  return ZakodujString(NactiSerioveCisloDiskuVHexu());
}
