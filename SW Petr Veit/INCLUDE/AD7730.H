// Ovladani AD prevodniku AD7730 pres paralelni port

// 21.9.1999: predelano na objekt

#ifndef __AD7730_H   // Aby to nedefinovalo vicekrat
#define __AD7730_H



class TAD7730 {
private:
  int PocetDelay;  // Kalibrace pro delay

  unsigned char InPort(unsigned short int Adresa);
  void OutPort(unsigned short int Adresa, unsigned char Co);
  void Delay();
  void SCLHigh();
  void SCLLow();
  void SDIHigh();
  void SDILow();
  void Hodiny();
  bool NactiRDY();
  void PosliByte(unsigned char Co);
  unsigned char NactiByte();

public:
  unsigned short int PORT;  // Adresa LPT1

  TAD7730();
  void InitDelay();
  bool KalibraceAD7730();
  unsigned int NactiAD7730();
};




// Konstruktor
TAD7730::TAD7730() {
  PocetDelay=0;
  PORT=0x278;
}


// Fce v assembleru:
unsigned char TAD7730::InPort(unsigned short int Adresa) {
  unsigned short int i; // Musi byt udelano pres AX!!
  asm {
    mov dx, Adresa
    in al, dx
    mov ah,0
    mov i, ax
  }
  return (unsigned char) i;
}

void TAD7730::OutPort(unsigned short int Adresa, unsigned char Co) {
  asm {
    mov dx, Adresa
    mov al, Co
    out dx, al
  }
}


// Cekani
void TAD7730::InitDelay() {  // Kalibruje cekani pro podle rychlosti pocitace - volat na zacatku
  // Pro Xicor staci delay 1us
  int Pocetus=120;     // Kolik us chcu cekat
  DWORD t1,t2,t;
  PocetDelay=20000000;
  double d;
  t1=GetTickCount();
  for (int i=1;i<=PocetDelay;i++);
  t2=GetTickCount();
  t=t2-t1;// V t je ted pocet milisekund potrebnych k 20 milionu NOPu
  // Musim pomoci double, protoze ani long uz nestaci!!!
  d=PocetDelay;
  d*=Pocetus;
  d/=(1000*t);
  PocetDelay=d;
}

void TAD7730::Delay() {
  for (int i=1;i<=PocetDelay;i++);
}


// -----------------------------------------------------------------
// Operace s piny - tady se provadi pripadne inverze

void TAD7730::SCLHigh() {
  // SCL = STROBE (adresa 0x37A, bit 0)
  // STROBE je invertovany
  OutPort((unsigned short int)(PORT+2),0);   // Negovany => pro high dam do nuly
}
void TAD7730::SCLLow() {
  // SCL = STROBE
  OutPort((unsigned short int)(PORT+2),1);
}

// ---------------
// SDI je pro me vystup, pro prevodnik tedy Din
void TAD7730::SDIHigh() {
  // SDI (Din pro prevodnik) = DATA0 (adresa 0x378, bit 0)
  // SDI neni negovany
  OutPort(PORT,1);
}
void TAD7730::SDILow() {
  OutPort(PORT,0);
}

// ---------------
void TAD7730::Hodiny() {
  SCLHigh();
  Delay();
  SCLLow();
  Delay();
  SCLHigh();
  Delay();
}

// ---------------
bool TAD7730::NactiRDY() {
  // Vrati true, kdyz je pripraveny novy prevod
  // RDY = PE (adresa 0x379, bit 5)
  // RDY neni negovany
  unsigned ch=InPort((unsigned short int)(PORT+1));
  if ((ch &32) == 32) {
    return false;
  } else {
    return true;
  }
}



// ------------------------------------------------------------------
// Operace s bajty - tady uz se nezajimam o to, zda invertovat ci ne (to obslouzi fce SDIHigh(), SDILow() atd.)

void TAD7730::PosliByte(unsigned char Co) {
  Delay();
  for (unsigned char i=0; i<8; i++) {
    if ((Co & 0x80) ==0) SDILow();  // Kdyz neni bit, dam do nuly
      else SDIHigh();               // Jinak do 1
    Co=(unsigned char)(Co<<1);
    Hodiny();
  }
}

unsigned char TAD7730::NactiByte() {
  // 7. bit n a adr. 379H (=378H+1) je negovany BUSY => pred vyhodnocenim neguju
  unsigned char byte=0,ch;
  Delay();
  for (int count=0; count<8; count++) {
    Hodiny();
    byte=(unsigned char)(byte << 1);
    ch=InPort((unsigned short int)(PORT+1));
    if ((ch & 0x80) == 0) byte=(unsigned char)(byte | 0x01);
   }
  return byte;
}


// ------------------------------------------------------------------
// Operace s daty z prevodniku

bool TAD7730::KalibraceAD7730() {
  SDIHigh();
  Delay();
  // Reset
  for (int i=0; i<80; i++) Hodiny();

  PosliByte(0x03);
  PosliByte(0x80);
  PosliByte(0x00);
  PosliByte(0x10);
  PosliByte(0x02);
  PosliByte(0x21);
  PosliByte(0x90);

  // Cekam, dokud neskonci kalibrace
  // Melo by to trvat max 0.5s
  int t1=GetTickCount();
  while (!NactiRDY()) {
    // Pokud musim cekat 1500ms (uz i 700ms je spatne), neco neni v poradku
    if ((GetTickCount()-t1) > 1500) return false;
  }
  return true;
}

unsigned int TAD7730::NactiAD7730() {
  union {
    unsigned int i;
    unsigned char ch[4];
  } Union;

  // Cekam, dokud neni hotovy dalsi prevod (abych necetl 2x stejny prevod)
  // Melo by to trvat max 20ms
  int t1=GetTickCount();
  while (!NactiRDY()) {
    // Pokud musim cekat 500ms (uz i 30ms je spatne), neco neni v poradku
    if ((GetTickCount()-t1) > 500) return 0;
  }

  // Nacte prevod z prevodniku
  PosliByte(0x11);
  Union.ch[3]=0;   // ADC je 24 bitovy => 4. bajt je vzdy nulovy
  Union.ch[2]=NactiByte();
  Union.ch[1]=NactiByte();
  Union.ch[0]=NactiByte();
  return Union.i;
}


#endif

