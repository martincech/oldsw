// Header pro 32-bitovou praci se soubory
// Copyright 1996 VEIT Electronics





bool OtevriSouborProCteni(HANDLE *soub,const char *nazev,DWORD Jak) {
// Jak:
// CREATE_ALWAYS vytvori novy/prepise stary
// OPEN_EXISTING otevre existujici, kdyz neex., fails
  *soub=CreateFile(nazev,GENERIC_READ,0,NULL,Jak,FILE_ATTRIBUTE_NORMAL,0);
  if (*soub==INVALID_HANDLE_VALUE) return false; else return true; // Soubor nejde otevrit
}

bool ExistujeSoubor(const char *nazev) {
  HANDLE h;
  if (OtevriSouborProCteni(&h,nazev,OPEN_EXISTING)) {
    CloseHandle(h);
    return true; // Existuje
  } else return false; // Neexistuje
}

bool OtevriSouborProZapis(HANDLE *soub,const char *nazev,DWORD Jak) {
  *soub=CreateFile(nazev,GENERIC_WRITE,0,NULL,Jak,FILE_ATTRIBUTE_NORMAL,0);
  if (*soub==INVALID_HANDLE_VALUE) return false; else return true; // Soubor nejde otevrit
}

bool ZavriSoubor(HANDLE soub) {
  return CloseHandle(soub);
}

bool ZapisDoSouboru(HANDLE soub,char *s,int pocet=-1) { // Zapise do souboru cely/pocet znaku s
  if (pocet<0) pocet=strlen(s);
  return WriteFile(soub,s,pocet,(LPDWORD) &pocet,NULL);
}

bool ZapisDoSouboruStr(HANDLE soub,String Str) { // Zapise do souboru cely/pocet znaku s
//  char s[250];
//  strcpy(s,Str.c_str());
  int pocet=Str.Length();
  return WriteFile(soub,Str.c_str(),pocet,(LPDWORD) &pocet,NULL);
}

bool NactiZeSouboru(HANDLE soub,char *s,int pocet) { // Nacte ze souboru cely/pocet znaku do s
  if (!ReadFile(soub,s,pocet,(LPDWORD) &pocet,NULL)) {
    s[0]=0; return false;
  }
  if (pocet==0) { // Je konec souboru
    s[0]=0; return false;
  }
  return true;
}

bool NactiRadekZeSouboru(HANDLE soub,char* s1,int pocet=-1) {
  char s[500]="",s2[10]="",s3[5]="";
  int poc=1,pocitadlo=0;
//  if (pocet<0) pocet=sizeof(s1)-2;
  if (pocet<0) pocet=499;
  do {
     strset(s2,0);
     if (!ReadFile(soub,&s2,poc,(LPDWORD) &poc,NULL)) {
       s[0]=0; return false; // Nenacetl nic
     };
     if (poc==0 && pocitadlo==0) { // Je konec souboru
       s1[0]=0; return false;
     }
     pocitadlo++; if (pocitadlo>pocet) {strcpy(s1,s);return true;}  // Aby nemohla vzniknout nekonecna smycka
     if (s3[1]!=0) s3[0]=s3[1];
     s3[1]=s2[0];
     s3[2]=0;
    if (s2[0]!='\n') strcat(s,s2);
  } while(strcmp(s3,"\r\n")!=0);
  s[strlen(s)-1]=0; // usekni \r na konci
  strcpy(s1,s);
  return true;
}

bool ZapisRadekDoSouboru(HANDLE soub,char* s1) {
  char s[200];
  int pocet;
  strcpy(s,s1);
  strcat(s,"\r\n");
  pocet=strlen(s);
  return WriteFile(soub,s,pocet,(LPDWORD) &pocet,NULL);
}

bool ZapisRadekDoSouboruStr(HANDLE soub,String Str) {
  char s[250];
  int pocet;
  strcpy(s,Str.c_str());
  strcat(s,"\r\n");
  pocet=strlen(s);
  return WriteFile(soub,s,pocet,(LPDWORD) &pocet,NULL);
}


//---------------- Prevody jednotlivych kodovani
void PrevodNaKamenicky(char *s) {
  // Prevede string na kodovani Kameniku
  for (int i=0; i<(int)strlen(s); i++) {  // Projedu vsechny znaky
    switch ((unsigned char)s[i]) {
      case 236: s[i]=136; break;  // �
      case 154: s[i]=168; break;  // �
      case 232: s[i]=135; break;  // �
      case 239: s[i]=131; break;  // �
      case 248: s[i]=169; break;  // �
      case 158: s[i]=145; break;  // �
      case 253: s[i]=152; break;  // �
      case 225: s[i]=160; break;  // �
      case 237: s[i]=161; break;  // �
      case 233: s[i]=130; break;  // �
      case 243: s[i]=162; break;  // �
      case 157: s[i]=159; break;  // �
      case 249: s[i]=150; break;  // �
      case 250: s[i]=163; break;  // �
      case 242: s[i]=164; break;  // �
      case 193: s[i]=143; break;  // �
      case 200: s[i]=128; break;  // �
      case 207: s[i]=133; break;  // �
      case 201: s[i]=144; break;  // �
      case 204: s[i]=137; break;  // �
      case 205: s[i]=139; break;  // �
      case 210: s[i]=165; break;  // �
      case 211: s[i]=149; break;  // �
      case 216: s[i]=158; break;  // �
      case 138: s[i]=155; break;  // �
      case 141: s[i]=134; break;  // �
      case 218: s[i]=151; break;  // �
      case 217: s[i]=166; break;  // �
      case 221: s[i]=157; break;  // �
      case 142: s[i]=146; break;  // �
    }//switch
  }
}

String PrevodNaKamenickyStr(String s) {
  // Prevede Winowsovsky string na kodovani Kameniku
  for (int i=1; i<=s.Length(); i++) {  // Projedu vsechny znaky
    switch ((unsigned char)s[i]) {
      case 236: s[i]=136; break;  // �
      case 154: s[i]=168; break;  // �
      case 232: s[i]=135; break;  // �
      case 239: s[i]=131; break;  // �
      case 248: s[i]=169; break;  // �
      case 158: s[i]=145; break;  // �
      case 253: s[i]=152; break;  // �
      case 225: s[i]=160; break;  // �
      case 237: s[i]=161; break;  // �
      case 233: s[i]=130; break;  // �
      case 243: s[i]=162; break;  // �
      case 157: s[i]=159; break;  // �
      case 249: s[i]=150; break;  // �
      case 250: s[i]=163; break;  // �
      case 242: s[i]=164; break;  // �
      case 193: s[i]=143; break;  // �
      case 200: s[i]=128; break;  // �
      case 207: s[i]=133; break;  // �
      case 201: s[i]=144; break;  // �
      case 204: s[i]=137; break;  // �
      case 205: s[i]=139; break;  // �
      case 210: s[i]=165; break;  // �
      case 211: s[i]=149; break;  // �
      case 216: s[i]=158; break;  // �
      case 138: s[i]=155; break;  // �
      case 141: s[i]=134; break;  // �
      case 218: s[i]=151; break;  // �
      case 217: s[i]=166; break;  // �
      case 221: s[i]=157; break;  // �
      case 142: s[i]=146; break;  // �
    }//switch
  }
  return s;
}

String PrevodZKamenickyStr(String s) {
  // Prevede Winowsovsky string ve formatu Kameniku na normalni windows kodovani (inverzni kamenicke kodovani)
  for (int i=1; i<=s.Length(); i++) {  // Projedu vsechny znaky
    switch ((unsigned char)s[i]) {
      case 136: s[i]=236; break;  // �
      case 168: s[i]=154; break;  // �
      case 135: s[i]=232; break;  // �
      case 131: s[i]=239; break;  // �
      case 169: s[i]=248; break;  // �
      case 145: s[i]=158; break;  // �
      case 152: s[i]=253; break;  // �
      case 160: s[i]=225; break;  // �
      case 161: s[i]=237; break;  // �
      case 130: s[i]=233; break;  // �
      case 162: s[i]=243; break;  // �
      case 159: s[i]=157; break;  // �
      case 150: s[i]=249; break;  // �
      case 163: s[i]=250; break;  // �
      case 164: s[i]=242; break;  // �
      case 143: s[i]=193; break;  // �
      case 128: s[i]=200; break;  // �
      case 133: s[i]=207; break;  // �
      case 144: s[i]=201; break;  // �
      case 137: s[i]=204; break;  // �
      case 139: s[i]=205; break;  // �
      case 165: s[i]=210; break;  // �
      case 149: s[i]=211; break;  // �
      case 158: s[i]=216; break;  // �
      case 155: s[i]=138; break;  // �
      case 134: s[i]=141; break;  // �
      case 151: s[i]=218; break;  // �
      case 166: s[i]=217; break;  // �
      case 157: s[i]=221; break;  // �
      case 146: s[i]=142; break;  // �
    }//switch
  }
  return s;
}

void PrevodNaLatin2(char *s) {
  // Prevede Winowsovsky string na kodovani Latin2
  for (int i=0; i<(int)strlen(s); i++) {  // Projedu vsechny znaky
    switch ((unsigned char)s[i]) {
      case 236: s[i]=216; break;  // �
      case 154: s[i]=231; break;  // �
      case 232: s[i]=159; break;  // �
      case 239: s[i]=212; break;  // �
      case 248: s[i]=253; break;  // �
      case 158: s[i]=167; break;  // �
      case 253: s[i]=236; break;  // �
      case 225: s[i]=160; break;  // �
      case 237: s[i]=161; break;  // �
      case 233: s[i]=130; break;  // �
      case 243: s[i]=162; break;  // �
      case 157: s[i]=156; break;  // �
      case 249: s[i]=133; break;  // �
      case 250: s[i]=163; break;  // �
      case 242: s[i]=229; break;  // �
      case 193: s[i]=181; break;  // �
      case 200: s[i]=172; break;  // �
      case 207: s[i]=210; break;  // �
      case 201: s[i]=144; break;  // �
      case 204: s[i]=183; break;  // �
      case 205: s[i]=214; break;  // �
      case 210: s[i]=213; break;  // �
      case 211: s[i]=224; break;  // �
      case 216: s[i]=252; break;  // �
      case 138: s[i]=230; break;  // �
      case 141: s[i]=155; break;  // �
      case 218: s[i]=233; break;  // �
      case 217: s[i]=222; break;  // �
      case 221: s[i]=237; break;  // �
      case 142: s[i]=166; break;  // �
    }//switch
  }
}

String PrevodNaLatin2Str(String s) {
  // Prevede Winowsovsky string na kodovani Latin2
  for (int i=1; i<=s.Length(); i++) {  // Projedu vsechny znaky
    switch ((unsigned char)s[i]) {
      case 236: s[i]=216; break;  // �
      case 154: s[i]=231; break;  // �
      case 232: s[i]=159; break;  // �
      case 239: s[i]=212; break;  // �
      case 248: s[i]=253; break;  // �
      case 158: s[i]=167; break;  // �
      case 253: s[i]=236; break;  // �
      case 225: s[i]=160; break;  // �
      case 237: s[i]=161; break;  // �
      case 233: s[i]=130; break;  // �
      case 243: s[i]=162; break;  // �
      case 157: s[i]=156; break;  // �
      case 249: s[i]=133; break;  // �
      case 250: s[i]=163; break;  // �
      case 242: s[i]=229; break;  // �
      case 193: s[i]=181; break;  // �
      case 200: s[i]=172; break;  // �
      case 207: s[i]=210; break;  // �
      case 201: s[i]=144; break;  // �
      case 204: s[i]=183; break;  // �
      case 205: s[i]=214; break;  // �
      case 210: s[i]=213; break;  // �
      case 211: s[i]=224; break;  // �
      case 216: s[i]=252; break;  // �
      case 138: s[i]=230; break;  // �
      case 141: s[i]=155; break;  // �
      case 218: s[i]=233; break;  // �
      case 217: s[i]=222; break;  // �
      case 221: s[i]=237; break;  // �
      case 142: s[i]=166; break;  // �
    }//switch
  }//for
  return s;
}

void PrevodNaBezDiakritiky(char *s) {
  // Prevede Winowsovsky string na kodovani bez diakritiky
  for (int i=0; i<(int)strlen(s); i++) {  // Projedu vsechny znaky
    switch ((unsigned char)s[i]) {
      case 236: s[i]='e'; break;  // �
      case 154: s[i]='s'; break;  // �
      case 232: s[i]='c'; break;  // �
      case 239: s[i]='d'; break;  // �
      case 248: s[i]='r'; break;  // �
      case 158: s[i]='z'; break;  // �
      case 253: s[i]='y'; break;  // �
      case 225: s[i]='a'; break;  // �
      case 237: s[i]='i'; break;  // �
      case 233: s[i]='e'; break;  // �
      case 243: s[i]='o'; break;  // �
      case 157: s[i]='t'; break;  // �
      case 249: s[i]='u'; break;  // �
      case 250: s[i]='u'; break;  // �
      case 242: s[i]='n'; break;  // �
      case 193: s[i]='A'; break;  // �
      case 200: s[i]='C'; break;  // �
      case 207: s[i]='D'; break;  // �
      case 201: s[i]='E'; break;  // �
      case 204: s[i]='E'; break;  // �
      case 205: s[i]='I'; break;  // �
      case 210: s[i]='N'; break;  // �
      case 211: s[i]='O'; break;  // �
      case 216: s[i]='R'; break;  // �
      case 138: s[i]='S'; break;  // �
      case 141: s[i]='T'; break;  // �
      case 218: s[i]='U'; break;  // �
      case 217: s[i]='U'; break;  // �
      case 221: s[i]='Y'; break;  // �
      case 142: s[i]='Z'; break;  // �
    }//switch
  }
}

