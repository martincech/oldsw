//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Version.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TVersionForm *VersionForm;
//---------------------------------------------------------------------------
__fastcall TVersionForm::TVersionForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TVersionForm::FormShow(TObject *Sender)
{
  OkButton->SetFocus();        
}
//---------------------------------------------------------------------------

