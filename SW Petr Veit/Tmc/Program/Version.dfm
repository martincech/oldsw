object VersionForm: TVersionForm
  Left = 201
  Top = 447
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Version'
  ClientHeight = 168
  ClientWidth = 274
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 96
    Height = 13
    Caption = 'Version of the scale:'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 104
    Width = 241
    Height = 10
    Shape = bsBottomLine
  end
  object Version110RadioButton: TRadioButton
    Left = 32
    Top = 48
    Width = 225
    Height = 17
    Caption = '1.10 or higher'
    TabOrder = 0
  end
  object Version109RadioButton: TRadioButton
    Left = 32
    Top = 72
    Width = 113
    Height = 17
    Caption = '1.09'
    TabOrder = 1
  end
  object OkButton: TButton
    Left = 88
    Top = 128
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object CancelButton: TButton
    Left = 184
    Top = 128
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
