//---------------------------------------------------------------------------

#ifndef VersionH
#define VersionH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TVersionForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TRadioButton *Version110RadioButton;
        TRadioButton *Version109RadioButton;
        TButton *OkButton;
        TButton *CancelButton;
        TBevel *Bevel1;
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TVersionForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TVersionForm *VersionForm;
//---------------------------------------------------------------------------
#endif
