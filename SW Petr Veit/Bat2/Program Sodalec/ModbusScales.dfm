object ModbusScalesForm: TModbusScalesForm
  Left = 389
  Top = 265
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Add / remove scales'
  ClientHeight = 327
  ClientWidth = 514
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ScalesStringGrid: TStringGrid
    Left = 16
    Top = 16
    Width = 481
    Height = 257
    ColCount = 3
    DefaultColWidth = 120
    DefaultRowHeight = 16
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
    TabOrder = 0
    OnDblClick = ScalesStringGridDblClick
    OnKeyDown = ScalesStringGridKeyDown
    ColWidths = (
      120
      120
      208)
    RowHeights = (
      16
      16)
  end
  object NewButton: TButton
    Left = 16
    Top = 288
    Width = 75
    Height = 25
    Caption = 'New'
    TabOrder = 1
    OnClick = NewButtonClick
  end
  object EditButton: TButton
    Left = 104
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Edit'
    TabOrder = 2
    OnClick = EditButtonClick
  end
  object OkButton1: TButton
    Left = 336
    Top = 288
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 3
    OnClick = OkButton1Click
  end
  object CancelButton: TButton
    Left = 423
    Top = 288
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object DeleteButton: TButton
    Left = 192
    Top = 288
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 5
    OnClick = DeleteButtonClick
  end
end
