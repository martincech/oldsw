//***************************************************************************
//
//    ModbusOnline.cpp - Online table of scales
//    Version 1.0
//
//***************************************************************************

#ifndef MODBUSONLINE_H
#define MODBUSONLINE_H

#include "ModbusOnlineScale.h"

//---------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
// Online
//---------------------------------------------------------------------------

class TModbusOnline {

private:
  vector<TModbusOnlineScale *> ScaleList;               // Seznam vah
  TModbusOnlineScale           *ActiveScale;            // Vaha, ze ktere se prave nacitaji data
  int                          ActiveScaleIndex;        // Index aktualni vahy v seznamu <ScaleList>
  int                          Repetitions;             // Max. pocet opakovani pri chybe prenosu

  bool LoadFirstScale();
  // Nahraje prvni vahu jako aktivni
  bool LoadNextScale();
  // Nahraje dalsi vahu v seznamu jako aktivni

public:
  TModbusOnline();

  int GetActiveScaleIndex() { return ActiveScaleIndex; }

  void ClearScaleList();
  // Smaze vsechny vahy ze seznamu
  void ClearScaleData();
  // Vynuluje data u vsech vah
  int ScalesCount();
  // Vrati pocet vah v seznamu
  void AddScale(TModbusScale *Scale);
  // Prida vahu <Scale> do seznamu
  TModbusOnlineScale *GetScale(int Index);
  // Vrati vahu s indexem <Index>

  bool LoadScale(int Index);
  // Nahraje vahu s indexem <Index> jako aktivni

  bool InitExecute(int NewRepetitions);
  // Inicializace akce
  bool Execute();
  // Samotna akce, po nacteni vsech dat aktualni vahy vrati true

};

#endif // MODBUSONLINE_H

