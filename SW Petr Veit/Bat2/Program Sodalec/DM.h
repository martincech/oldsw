//---------------------------------------------------------------------------

#ifndef DMH
#define DMH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Db.hpp>
#include <DBTables.hpp>
//---------------------------------------------------------------------------

#include "..\..\Library\Excel\Excel.h"

#include "Konstanty.h"  // Konstanty resources

#include "MFlash.h"
//#include "MReader.h"
#include "..\..\Ftdi2\AtFlash.h"
#include <ImgList.hpp>     // USB ctecka
#include "SFolders.h"
#include "Curve.h"
#include "CurveList.h"
#include "ModbusScaleList.h"


// Cislo verze SW
#define VERSION_SW_SODALEC_INITIAL 0x0150    // Puvodni cislo verze Sodalec
#define VERSION_SW      "1.53"          // Melo by odpovidat cislu verze samotne vahy
#define VERSION_BUILD   "1"             // Cislo podverze (pokud se meni jen SW beze zmeny FW vahy)

// Verze odberatele (nazev vahy, nazev programu, kontakt)
#define VERSION_BAT2      1             // Vaha Bat2
//#define VERSION_PS1      1              // Vaha Ps1 pro Dacs Dansko
//#define VERSION_COMSCALE 1              // Vaha ComScale pro Big Dutchman

// Struktura s parametry podle verze odberatele
typedef struct {
  String                ScaleTitle;     // Nazev vahy (Bat2, PS1, ...)
  String                FileExtension;  // Pripona datoveho souboru
  TStringList           *Contacts;      // Kontakt na dodavatele programu
  Graphics::TBitmap     *Logo;          // Logo dodavatele
} TVersion;

// Typ dat v souboru
typedef enum {
  DATA_VAZENI,
  DATA_ONLINE
} TTypDat;

// Informace o prave otevrenem souboru
typedef struct {
  String        Jmeno;          // Jmeno souboru, ktery je prave otevreny. Pokud je prazdne, neni otevreny zadny soubor
  int           MinDen, MaxDen; // Minimalni a maximalni cislo dne obsazene v souboru
  int           Den;            // Prave zobrazeny den
  bool          Pohlavi;        // Prave zobrazene pohlavi
  String        Jednotky;       // Jednotky kg/lb
  String        JmenoPorovnani; // Jmeno souboru porovnani, ktery je prave otevreny
  TTypDat       TypDat;         // Typ dat vazeni/online
  bool          Waiting;        // YES/NO ceka se na start vazeni
} TSoubor;


#define PRIPONA_HARDCOPY        "bin"   // Pripona souboru s kopii modulu

#define PrepoctiNaKg(Cislo) (double)((double)Cislo / (double)1000.0)
#define PrepoctiZKg(Cislo)  (double)((double)Cislo * (double)1000.0 + 0.5)      // Musim se zaokrouhlenim, 0.030kg jinak dava 29.
#define FORMAT_HMOTNOSTI        "0.000"

// Index krivky pro porovnani
typedef enum {
  COMPARE_CURVE_MAIN_FORM,              // Krivka v HlavniForm
  COMPARE_CURVE_WEIGHING_FORM,          // Krivka v WeighingForm
  _COMPARE_CURVE_COUNT
} TCompareCurveIndex;

// Nastaveni Modbusu
typedef struct {
  int       Speed;              // Rychlost v baudech
  TParity   Parity;             // Typ parity
  int       ReplyDelay;         // Zpozdeni mezi prijmem prikazu a odpovedi v milisekundach, kvantovani s TIMER0_PERIOD
  int       SilentInterval;     // Mezera mezi dvema pakety MODBUS v milisekundach
  TProtocol Protocol;           // Typ komunikacniho protokolu
  int       Repetitions;        // Pocet opakovani vysilani pri chybe prenosu
} TModbusSetup;

// Hodnoty cekani se lisi od vahy
#define MIN_MODBUS_REPLY_DELAY          1
#define MAX_MODBUS_REPLY_DELAY          9999
#define DEFAULT_MODBUS_REPLY_DELAY      2000
#define MIN_MODBUS_SILENT_INTERVAL      1
#define MAX_MODBUS_SILENT_INTERVAL      9999
#define DEFAULT_MODBUS_SILENT_INTERVAL  50

// Pocty opakovani prenosu pri chybe
#define MIN_MODBUS_REPETITIONS          1
#define MAX_MODBUS_REPETITIONS          9
#define DEFAULT_MODBUS_REPETITIONS      3

#define TEXT_SPACE      ' '             // Znak mezera

// Nastaveni programu
typedef struct {
  String       ModemName;       // Jmeno posledne pouziteho GSM modemu
  TModbusSetup Modbus;          // Nastaveni MODBUS
} TNastaveni;




class TData : public TDataModule
{
__published:	// IDE-managed Components
        TTable *ZaznamyTable;
        TTable *HlavickaTable;
        TDataSource *HlavickaDataSource;
        TTable *StatistikaTable;
        TDataSource *StatistikaDataSource;
        TTable *NastaveniTable;
        TDataSource *NastaveniDataSource;
        TTable *HejnaTable;
        TDataSource *HejnaDataSource;
        TTable *KonfiguraceTable;
        TDataSource *KonfiguraceDataSource;
        TTable *KalibraceTable;
        TDataSource *KalibraceDataSource;
        TQuery *ZaznamyQuery;
        TDataSource *ZaznamyQueryDataSource;
        TSmallintField *ZaznamyQueryTIME_HOUR;
        TFloatField *ZaznamyQueryWEIGHT;
        TIntegerField *ZaznamyQuerySAMPLE;
        TTable *CZaznamyTable;
        TTable *CHlavickaTable;
        TDataSource *CHlavickaDataSource;
        TTable *CStatistikaTable;
        TDataSource *CStatistikaDataSource;
        TTable *CHejnaTable;
        TDataSource *CHejnaDataSource;
        TTable *CKonfiguraceTable;
        TDataSource *CKonfiguraceDataSource;
        TTable *CKalibraceTable;
        TDataSource *CKalibraceDataSource;
        TDataSource *CZaznamyQueryDataSource;
        TQuery *CZaznamyQuery;
        TSmallintField *SmallintField1;
        TFloatField *FloatField1;
        TIntegerField *IntegerField1;
        TTable *NastaveniVahTable;
        TDataSource *NastaveniVahDataSource;
        TTable *NastaveniHejnTable;
        TDataSource *NastaveniHejnDataSource;
        TQuery *TiskQuery;
        TDataSource *TiskDataSource;
        TTable *OnlineTable;
        TQuery *OnlineQuery;
        TIntegerField *IntegerField2;
        TDataSource *OnlineQueryDataSource;
        TSmallintField *OnlineQueryTIME_HOUR;
        TFloatField *OnlineQueryWEIGHT;
        TBooleanField *OnlineQuerySAVED;
        TTable *COnlineTable;
        TBooleanField *OnlineQuerySTABLE;
        TQuery *StatistikaQuery;
        TDataSource *StatistikaQueryDataSource;
        TSmallintField *StatistikaQueryDAY_NUMBER;
        TDateTimeField *StatistikaQueryDATE_TIME;
        TFloatField *StatistikaQuerySTAT_XSUM;
        TFloatField *StatistikaQuerySTAT_X2SUM;
        TIntegerField *StatistikaQuerySTAT_COUNT;
        TFloatField *StatistikaQueryHIST_VALUE;
        TIntegerField *StatistikaQueryHIST_COUNT;
        TFloatField *StatistikaQueryHIST_VALUE_1;
        TIntegerField *StatistikaQueryHIST_COUNT_1;
        TFloatField *StatistikaQueryHIST_VALUE_2;
        TIntegerField *StatistikaQueryHIST_COUNT_2;
        TFloatField *StatistikaQueryHIST_VALUE_3;
        TIntegerField *StatistikaQueryHIST_COUNT_3;
        TFloatField *StatistikaQueryHIST_VALUE_4;
        TIntegerField *StatistikaQueryHIST_COUNT_4;
        TFloatField *StatistikaQueryHIST_VALUE_5;
        TIntegerField *StatistikaQueryHIST_COUNT_5;
        TFloatField *StatistikaQueryHIST_VALUE_6;
        TIntegerField *StatistikaQueryHIST_COUNT_6;
        TFloatField *StatistikaQueryHIST_VALUE_7;
        TIntegerField *StatistikaQueryHIST_COUNT_7;
        TFloatField *StatistikaQueryHIST_VALUE_8;
        TIntegerField *StatistikaQueryHIST_COUNT_8;
        TFloatField *StatistikaQueryHIST_VALUE_9;
        TIntegerField *StatistikaQueryHIST_COUNT_9;
        TFloatField *StatistikaQueryHIST_VALUE_10;
        TIntegerField *StatistikaQueryHIST_COUNT_10;
        TFloatField *StatistikaQueryHIST_VALUE_11;
        TIntegerField *StatistikaQueryHIST_COUNT_11;
        TFloatField *StatistikaQueryHIST_VALUE_12;
        TIntegerField *StatistikaQueryHIST_COUNT_12;
        TFloatField *StatistikaQueryHIST_VALUE_13;
        TIntegerField *StatistikaQueryHIST_COUNT_13;
        TFloatField *StatistikaQueryHIST_VALUE_14;
        TIntegerField *StatistikaQueryHIST_COUNT_14;
        TFloatField *StatistikaQueryHIST_VALUE_15;
        TIntegerField *StatistikaQueryHIST_COUNT_15;
        TFloatField *StatistikaQueryHIST_VALUE_16;
        TIntegerField *StatistikaQueryHIST_COUNT_16;
        TFloatField *StatistikaQueryHIST_VALUE_17;
        TIntegerField *StatistikaQueryHIST_COUNT_17;
        TFloatField *StatistikaQueryHIST_VALUE_18;
        TIntegerField *StatistikaQueryHIST_COUNT_18;
        TFloatField *StatistikaQueryHIST_VALUE_19;
        TIntegerField *StatistikaQueryHIST_COUNT_19;
        TFloatField *StatistikaQueryHIST_VALUE_20;
        TIntegerField *StatistikaQueryHIST_COUNT_20;
        TFloatField *StatistikaQueryHIST_VALUE_21;
        TIntegerField *StatistikaQueryHIST_COUNT_21;
        TFloatField *StatistikaQueryHIST_VALUE_22;
        TIntegerField *StatistikaQueryHIST_COUNT_22;
        TFloatField *StatistikaQueryHIST_VALUE_23;
        TIntegerField *StatistikaQueryHIST_COUNT_23;
        TFloatField *StatistikaQueryHIST_VALUE_24;
        TIntegerField *StatistikaQueryHIST_COUNT_24;
        TFloatField *StatistikaQueryHIST_VALUE_25;
        TIntegerField *StatistikaQueryHIST_COUNT_25;
        TFloatField *StatistikaQueryHIST_VALUE_26;
        TIntegerField *StatistikaQueryHIST_COUNT_26;
        TFloatField *StatistikaQueryHIST_VALUE_27;
        TIntegerField *StatistikaQueryHIST_COUNT_27;
        TFloatField *StatistikaQueryHIST_VALUE_28;
        TIntegerField *StatistikaQueryHIST_COUNT_28;
        TFloatField *StatistikaQueryHIST_VALUE_29;
        TIntegerField *StatistikaQueryHIST_COUNT_29;
        TFloatField *StatistikaQueryHIST_VALUE_30;
        TIntegerField *StatistikaQueryHIST_COUNT_30;
        TFloatField *StatistikaQueryHIST_VALUE_31;
        TIntegerField *StatistikaQueryHIST_COUNT_31;
        TFloatField *StatistikaQueryHIST_VALUE_32;
        TIntegerField *StatistikaQueryHIST_COUNT_32;
        TFloatField *StatistikaQueryHIST_VALUE_33;
        TIntegerField *StatistikaQueryHIST_COUNT_33;
        TFloatField *StatistikaQueryHIST_VALUE_34;
        TIntegerField *StatistikaQueryHIST_COUNT_34;
        TFloatField *StatistikaQueryHIST_VALUE_35;
        TIntegerField *StatistikaQueryHIST_COUNT_35;
        TFloatField *StatistikaQueryHIST_VALUE_36;
        TIntegerField *StatistikaQueryHIST_COUNT_36;
        TFloatField *StatistikaQueryHIST_VALUE_37;
        TIntegerField *StatistikaQueryHIST_COUNT_37;
        TFloatField *StatistikaQueryHIST_VALUE_38;
        TIntegerField *StatistikaQueryHIST_COUNT_38;
        TFloatField *StatistikaQueryTARGET;
        TFloatField *StatistikaQueryYESTERDAY_AVERAGE;
        TFloatField *StatistikaQueryYESTERDAY_AVERAGE_1;
        TBooleanField *StatistikaQueryUSE_REAL_UNI;
        TSmallintField *StatistikaQueryREAL_UNI;
        TFloatField *StatistikaQueryAVERAGE;
        TFloatField *StatistikaQueryGAIN;
        TFloatField *StatistikaQuerySIGMA;
        TSmallintField *StatistikaQueryUNI;
        TDateTimeField *StatistikaQueryDATE_TIME_DATE;
        TQuery *StatistikaVyberQuery;
        TSmallintField *SmallintField2;
        TDateTimeField *DateTimeField1;
        TFloatField *FloatField2;
        TFloatField *FloatField3;
        TIntegerField *IntegerField3;
        TFloatField *FloatField4;
        TIntegerField *IntegerField4;
        TFloatField *FloatField5;
        TIntegerField *IntegerField5;
        TFloatField *FloatField6;
        TIntegerField *IntegerField6;
        TFloatField *FloatField7;
        TIntegerField *IntegerField7;
        TFloatField *FloatField8;
        TIntegerField *IntegerField8;
        TFloatField *FloatField9;
        TIntegerField *IntegerField9;
        TFloatField *FloatField10;
        TIntegerField *IntegerField10;
        TFloatField *FloatField11;
        TIntegerField *IntegerField11;
        TFloatField *FloatField12;
        TIntegerField *IntegerField12;
        TFloatField *FloatField13;
        TIntegerField *IntegerField13;
        TFloatField *FloatField14;
        TIntegerField *IntegerField14;
        TFloatField *FloatField15;
        TIntegerField *IntegerField15;
        TFloatField *FloatField16;
        TIntegerField *IntegerField16;
        TFloatField *FloatField17;
        TIntegerField *IntegerField17;
        TFloatField *FloatField18;
        TIntegerField *IntegerField18;
        TFloatField *FloatField19;
        TIntegerField *IntegerField19;
        TFloatField *FloatField20;
        TIntegerField *IntegerField20;
        TFloatField *FloatField21;
        TIntegerField *IntegerField21;
        TFloatField *FloatField22;
        TIntegerField *IntegerField22;
        TFloatField *FloatField23;
        TIntegerField *IntegerField23;
        TFloatField *FloatField24;
        TIntegerField *IntegerField24;
        TFloatField *FloatField25;
        TIntegerField *IntegerField25;
        TFloatField *FloatField26;
        TIntegerField *IntegerField26;
        TFloatField *FloatField27;
        TIntegerField *IntegerField27;
        TFloatField *FloatField28;
        TIntegerField *IntegerField28;
        TFloatField *FloatField29;
        TIntegerField *IntegerField29;
        TFloatField *FloatField30;
        TIntegerField *IntegerField30;
        TFloatField *FloatField31;
        TIntegerField *IntegerField31;
        TFloatField *FloatField32;
        TIntegerField *IntegerField32;
        TFloatField *FloatField33;
        TIntegerField *IntegerField33;
        TFloatField *FloatField34;
        TIntegerField *IntegerField34;
        TFloatField *FloatField35;
        TIntegerField *IntegerField35;
        TFloatField *FloatField36;
        TIntegerField *IntegerField36;
        TFloatField *FloatField37;
        TIntegerField *IntegerField37;
        TFloatField *FloatField38;
        TIntegerField *IntegerField38;
        TFloatField *FloatField39;
        TIntegerField *IntegerField39;
        TFloatField *FloatField40;
        TIntegerField *IntegerField40;
        TFloatField *FloatField41;
        TIntegerField *IntegerField41;
        TFloatField *FloatField42;
        TIntegerField *IntegerField42;
        TFloatField *FloatField43;
        TFloatField *FloatField44;
        TFloatField *FloatField45;
        TBooleanField *BooleanField1;
        TSmallintField *SmallintField3;
        TFloatField *FloatField46;
        TFloatField *FloatField47;
        TFloatField *FloatField48;
        TSmallintField *SmallintField5;
        TDateTimeField *DateTimeField2;
        TDataSource *StatistikaVyberDataSource;
        TTable *SmsTable;
        TQuery *SmsOneGenderQuery;
        TDataSource *SmsOneGenderDataSource;
        TStringField *SmsTableGSM_NUMBER;
        TSmallintField *SmsTableDAY_NUMBER;
        TDateField *SmsTableDAY_DATE;
        TSmallintField *SmsTableFEMALE_COUNT;
        TFloatField *SmsTableFEMALE_AVERAGE;
        TFloatField *SmsTableFEMALE_GAIN;
        TFloatField *SmsTableFEMALE_SIGMA;
        TSmallintField *SmsTableFEMALE_UNI;
        TSmallintField *SmsTableMALE_COUNT;
        TFloatField *SmsTableMALE_AVERAGE;
        TFloatField *SmsTableMALE_GAIN;
        TFloatField *SmsTableMALE_SIGMA;
        TSmallintField *SmsTableMALE_UNI;
        TStringField *SmsOneGenderQueryGSM_NUMBER;
        TSmallintField *SmsOneGenderQueryDAY_NUMBER;
        TDateField *SmsOneGenderQueryDAY_DATE;
        TSmallintField *SmsOneGenderQueryFEMALE_COUNT;
        TFloatField *SmsOneGenderQueryFEMALE_AVERAGE;
        TFloatField *SmsOneGenderQueryFEMALE_GAIN;
        TFloatField *SmsOneGenderQueryFEMALE_SIGMA;
        TSmallintField *SmsOneGenderQueryFEMALE_UNI;
        TDateTimeField *SmsTableINSERT_DATE_TIME;
        TDateTimeField *SmsTableEDIT_DATE_TIME;
        TStringField *SmsTableNOTE;
        TStringField *SmsOneGenderQueryNOTE;
        TTable *FKonfiguraceTable;
        TTable *FStatistikaTable;
        TTable *FiltersTable;
        TDateTimeField *SmsOneGenderQueryINSERT_DATE_TIME;
        TTable *NumbersTable;
        TImageList *LogoImageList;
        TDatabase *Database;
        TTable *CurvesTable;
        TFloatField *StatistikaQueryCOMPARE;
        TFloatField *StatistikaQueryDIFFERENCE;
        TFloatField *StatistikaVyberQueryCOMPARE;
        TFloatField *StatistikaVyberQueryDIFFERENCE;
        TFloatField *SmsOneGenderQueryCOMPARE;
        TFloatField *SmsOneGenderQueryDIFFERENCE;
        TTable *ModbusScalesTable;
        TFloatField *StatistikaQueryCV;
        TFloatField *StatistikaVyberQueryCV2;
        TSmallintField *SmsTableFEMALE_CV;
        TSmallintField *SmsTableMALE_CV;
        TSmallintField *SmsOneGenderQueryFEMALE_CV;
        TStringField *SmsTableID;
        TStringField *SmsOneGenderQueryID;
        void __fastcall ZaznamyQueryCalcFields(TDataSet *DataSet);
        void __fastcall StatistikaQueryCalcFields(TDataSet *DataSet);
        void __fastcall DataModuleDestroy(TObject *Sender);
        void __fastcall SmsOneGenderQueryCalcFields(TDataSet *DataSet);
private:	// User declarations

  TGrowthCurve *CompareCurve[_COMPARE_CURVE_COUNT]; // Rustova krivka pro porovnani pro prave otevreny soubor / databazi vazeni

  int Dpi;

public:		// User declarations
  TLocateOptions SearchOptions;  // Pro hledani v tabulkach

  TNastaveni Nastaveni;
  TSoubor Soubor;

//  TMReader *Adapter;
  TMFlash  *Module;
  TAtFlash *UsbFlash;

  TVersion Version;                     // Udaje podle verze odberatele

  TSpecialFolders *SpecialFolders;      // Umisteni specialnich adresaru Windows + priprava dat

  TExcel *Excel;                        // Export do excelu

  int GsmModemPortNumber;               // Cislo portu GSM modemu zadane v INI souboru

  void CreateVersionStrings();
  bool ModulPripraven();
  void LoadCurvesFromDatabase(TCurveList *CurveList);
  void SaveCurvesToDatabase(TCurveList *CurveList);
  void LoadCurvesToStrings(TStrings *Strings);

  void LoadModbusScalesFromDatabase(TModbusScaleList *ScaleList);
  void SaveModbusScalesToDatabase(TModbusScaleList *ScaleList);
  void LoadModbusScalesToStrings(TStrings *Strings);
  void SetModbusPortParameters();
  void SetModbusReplyDelay(int NewReplyDelay);

  void SetCompareCurve(TCompareCurveIndex Index, TGrowthCurve *Curve);
  TGrowthCurve * GetCompareCurve(TCompareCurveIndex Index);


        __fastcall TData(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TData *Data;
//---------------------------------------------------------------------------
#endif
