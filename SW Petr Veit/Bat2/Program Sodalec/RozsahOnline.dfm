object RozsahOnlineForm: TRozsahOnlineForm
  Left = 634
  Top = 212
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Range'
  ClientHeight = 239
  ClientWidth = 280
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 103
    Top = 48
    Width = 3
    Height = 13
    Caption = '-'
  end
  object Label3: TLabel
    Left = 16
    Top = 24
    Width = 88
    Height = 13
    Caption = 'Range of samples:'
  end
  object Bevel1: TBevel
    Left = 15
    Top = 176
    Width = 250
    Height = 10
    Shape = bsBottomLine
  end
  object Label1: TLabel
    Left = 16
    Top = 80
    Width = 51
    Height = 13
    Caption = 'Averaging:'
  end
  object Label4: TLabel
    Left = 104
    Top = 104
    Width = 38
    Height = 13
    Caption = 'samples'
  end
  object Label5: TLabel
    Left = 16
    Top = 144
    Width = 249
    Height = 33
    AutoSize = False
    Caption = 'Note: export of large amounts of samples can take a long time.'
    WordWrap = True
  end
  object OdEdit: TEdit
    Left = 40
    Top = 45
    Width = 57
    Height = 21
    MaxLength = 6
    TabOrder = 0
    OnKeyPress = OdEditKeyPress
  end
  object DoEdit: TEdit
    Left = 111
    Top = 45
    Width = 57
    Height = 21
    MaxLength = 6
    TabOrder = 1
    OnKeyPress = OdEditKeyPress
  end
  object Button1: TButton
    Left = 96
    Top = 200
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 190
    Top = 200
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object PrumerEdit: TEdit
    Left = 40
    Top = 101
    Width = 57
    Height = 21
    MaxLength = 4
    TabOrder = 4
    OnKeyPress = OdEditKeyPress
  end
end
