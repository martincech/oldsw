//***************************************************************************
//
//    CurveList.cpp - List of growth curves
//    Version 1.0
//
//***************************************************************************

#ifndef CURVELIST_H
#define CURVELIST_H

#include "Curve.h"
#include <system.hpp>           // AnsiString

class TCurveList {

private:
  vector<TGrowthCurve *> CurveList;           // Seznam krivek

  typedef vector<TGrowthCurve *>::iterator TCurveListIterator;

  TCurveListIterator TCurveList::GetIterator(int Index);
  bool CheckIndex(int Index);
  void Sort(void);


public:
  ~TCurveList(void);

  void Clear(void);
  int  Count(void);
  int  GetIndex(AnsiString Name);
  bool Exists(AnsiString Name);
  bool CheckName(AnsiString Name);
  bool Add(AnsiString Name);
  bool Add(TGrowthCurve *SourceCurve, AnsiString Name);
  bool Delete(int Index);
  bool Rename(int Index, AnsiString Name);
  TGrowthCurve * GetCurve(int Index);

};


#endif // CURVELIST_H
