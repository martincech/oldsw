//---------------------------------------------------------------------------

#ifndef ModbusCompareH
#define ModbusCompareH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TModbusCompareForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TComboBox *CompareComboBox;
        TButton *OkButton;
        TButton *CancelButton;
private:	// User declarations
public:		// User declarations
        __fastcall TModbusCompareForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusCompareForm *ModbusCompareForm;
//---------------------------------------------------------------------------
#endif
