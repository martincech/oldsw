//***************************************************************************
//
//    CurveList.cpp - List of growth curves
//    Version 1.0
//
//***************************************************************************

#include "CurveList.h"
#include "c51\Bat2.h"           // Maximalni hodnoty krivky
#include "DM.h"                 // PrepoctiNaKg


//---------------------------------------------------------------------------
// Destruktor
//---------------------------------------------------------------------------

TCurveList::~TCurveList(void) {
  Clear();
}

//---------------------------------------------------------------------------
// Najeti na urcitou krivku
//---------------------------------------------------------------------------

vector<TGrowthCurve *>::iterator TCurveList::GetIterator(int Index) {
  // Vrati iterator na krivku s indexem <Index>. Neosetruje prazdny seznam.
  TCurveListIterator CurveListIterator;

  CurveListIterator = CurveList.begin();
  while (Index) {
    CurveListIterator++;
    Index--;
  }
  return CurveListIterator;
}

//---------------------------------------------------------------------------
// Kontrola indexu
//---------------------------------------------------------------------------

bool TCurveList::CheckIndex(int Index) {
  // Pokud je index <Index> platny v ramci seznamu krivek, vrati true
  return (bool)(Index >= 0 && (unsigned)Index < CurveList.size());
}

//---------------------------------------------------------------------------
// Trideni
//---------------------------------------------------------------------------

void TCurveList::Sort(void) {
  // Setridi seznam podle nazvu krivek
  TCurveListIterator Iterator1, Iterator2;
  TGrowthCurve *Curve;

  for (Iterator1 = CurveList.begin(); Iterator1 < CurveList.end() - 1; Iterator1++) {
    for (Iterator2 = Iterator1 + 1; Iterator2 < CurveList.end(); Iterator2++) {
      if ((*Iterator1)->GetName() > (*Iterator2)->GetName()) {
        // Prohodim prvky
        Curve = (*Iterator1);
        (*Iterator1) = (*Iterator2);
        (*Iterator2) = Curve;
      }
    }
  }
}

//---------------------------------------------------------------------------
// Smazani vsech krivek
//---------------------------------------------------------------------------

void TCurveList::Clear(void) {
  // Smazu vsechny krivky
  for (TCurveListIterator CurveListIterator = CurveList.begin(); CurveListIterator != CurveList.end(); CurveListIterator++) {
    delete (*CurveListIterator);        // Smazu vsechny krivky
  }
  // Smazu seznam
  CurveList.clear();
}

//---------------------------------------------------------------------------
// Pocet krivek v seznamu
//---------------------------------------------------------------------------

int TCurveList::Count(void) {
  return CurveList.size();
}

//---------------------------------------------------------------------------
// Nalezeni indexu
//---------------------------------------------------------------------------

int TCurveList::GetIndex(AnsiString Name) {
  // Vrati index krivky s nazvem <Name> nebo -1, pokud krivka neexistuje
  TCurveListIterator CurveListIterator;
  int Index = 0;

  for (CurveListIterator = CurveList.begin(); CurveListIterator != CurveList.end(); CurveListIterator++) {
    if ((*CurveListIterator)->GetName().UpperCase() == Name.UpperCase()) {
      return Index;             // Zadany nazev uz v seznamu existuje
    }
    Index++;
  }
  return -1;                    // Neexistuje
}

//---------------------------------------------------------------------------
// Kontrola jmena krivky
//---------------------------------------------------------------------------

bool TCurveList::Exists(AnsiString Name) {
  // Kontrola, zda uz krivka v senamu neexistuje
  return (GetIndex(Name) >= 0);
}

bool TCurveList::CheckName(AnsiString Name) {
  // Kontrola delky nazvu
  if (Name.Length() == 0) {
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Pridani nove krivky
//---------------------------------------------------------------------------

bool TCurveList::Add(AnsiString Name) {
  TGrowthCurve *Curve;

  // Kontrola jmena
  if (!CheckName(Name)) {
    return false;               // Jmeno ma neplatny format
  }
  if (Exists(Name)) {
    return false;               // Jmeno uz v seznamu existuje
  }

  // Ulozim novou krivku
  Curve = new TGrowthCurve(Name,                      // Zalozim novou krivku
                     CURVE_MAX_POINTS,
                     CURVE_FIRST_DAY_NUMBER,
                     CURVE_MAX_DAY,
                     PrepoctiNaKg(MIN_TARGET_WEIGHT),
                     PrepoctiNaKg(MAX_TARGET_WEIGHT));
  CurveList.push_back(Curve);   // Pridam ukazatel do kontejneru

  // Na zaver setridim podle nazvu
  Sort();

  return true;
}

//---------------------------------------------------------------------------
// Pridani nove krivky pomoci kopirovani
//---------------------------------------------------------------------------

bool TCurveList::Add(TGrowthCurve *SourceCurve, AnsiString Name) {
  TGrowthCurve *NewCurve;

  // Overim zdrojovou krivku
  if (!SourceCurve) {
    return false;
  }

  // Zalozim novou krivku se zadanym nazvem
  if (!Add(Name)) {
    return false;
  }

  // Zjistim ukazatel na pridanou krivku
  if ((NewCurve = GetCurve(GetIndex(Name))) == NULL) {
    return false;
  }

  // Zkopiruju body krivky ze zdrojove krivky
  for (int i = 0; i < SourceCurve->Count(); i++) {
    NewCurve->Add(SourceCurve->GetDay(i), SourceCurve->GetWeight(i));
  }

  return true;
}

//---------------------------------------------------------------------------
// Smazani krivky
//---------------------------------------------------------------------------

bool TCurveList::Delete(int Index) {
  // Smaze krivku na pozici <Index>
  if (!CheckIndex(Index)) {
    return false;
  }
  CurveList.erase(GetIterator(Index));
  return true;
}

//---------------------------------------------------------------------------
// Prejmenovani krivky
//---------------------------------------------------------------------------

bool TCurveList::Rename(int Index, AnsiString Name) {
  // Prejmenuje krivku na pozici <Index> s novym nazvem <Name> a setridi seznam
  if (!CheckIndex(Index)) {
    return false;
  }

  // Kontrola jmena
  if (!CheckName(Name)) {
    return false;               // Jmeno ma neplatny format
  }
  if (Exists(Name)) {
    return false;               // Jmeno uz v seznamu existuje - novynazev musi byt unikatni
  }

  // Nastavim nove jmeno
  GetCurve(Index)->SetName(Name);

  // Na zaver setridim podle nazvu
  Sort();

  return true;
}

//---------------------------------------------------------------------------
// Vraceni ukazatele na krivku
//---------------------------------------------------------------------------

TGrowthCurve * TCurveList::GetCurve(int Index) {
  // Vrati ukazatel na krivku na pozici <Index>
  if (!CheckIndex(Index)) {
    return NULL;
  }
  return (*GetIterator(Index));
}

