object SimulaceVysledkyForm: TSimulaceVysledkyForm
  Left = 266
  Top = 224
  Width = 600
  Height = 400
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Simulation'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 592
    Height = 279
    ActivePage = StatistikaTabSheet
    Align = alClient
    TabOrder = 0
    object StatistikaTabSheet: TTabSheet
      Caption = 'Statistics'
      object Label3: TLabel
        Left = 16
        Top = 32
        Width = 34
        Height = 13
        Caption = 'Target:'
      end
      object CilovaHmotnostLabel: TLabel
        Left = 104
        Top = 32
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '00,000'
      end
      object Label5: TLabel
        Left = 16
        Top = 48
        Width = 43
        Height = 13
        Caption = 'Average:'
      end
      object PrumerLabel: TLabel
        Left = 104
        Top = 48
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '00,000'
      end
      object Label7: TLabel
        Left = 16
        Top = 72
        Width = 31
        Height = 13
        Caption = 'Count:'
      end
      object PocetLabel: TLabel
        Left = 107
        Top = 72
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = '00000'
      end
      object Label9: TLabel
        Left = 16
        Top = 88
        Width = 62
        Height = 13
        Caption = 'St. deviation:'
      end
      object SigmaLabel: TLabel
        Left = 110
        Top = 88
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = '0,000'
      end
      object Label11: TLabel
        Left = 16
        Top = 120
        Width = 49
        Height = 13
        Caption = 'Uniformity:'
      end
      object UniLabel: TLabel
        Left = 119
        Top = 120
        Width = 18
        Height = 13
        Alignment = taRightJustify
        Caption = '000'
      end
      object Label13: TLabel
        Left = 16
        Top = 104
        Width = 16
        Height = 13
        Caption = 'Cv:'
      end
      object CvLabel: TLabel
        Left = 119
        Top = 104
        Width = 18
        Height = 13
        Alignment = taRightJustify
        Caption = '000'
      end
      object CilovaHmotnostJednotkyLabel: TLabel
        Left = 144
        Top = 32
        Width = 12
        Height = 13
        Caption = 'kg'
      end
      object PrumerJednotkyLabel: TLabel
        Left = 144
        Top = 48
        Width = 12
        Height = 13
        Caption = 'kg'
      end
      object Label20: TLabel
        Left = 144
        Top = 120
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label21: TLabel
        Left = 144
        Top = 104
        Width = 8
        Height = 13
        Caption = '%'
      end
    end
    object HistogramTabSheet: TTabSheet
      Caption = 'Histogram'
      ImageIndex = 1
      object HistogramChart: TChart
        Left = 0
        Top = 0
        Width = 584
        Height = 251
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        MarginBottom = 3
        MarginLeft = 1
        MarginTop = 5
        Title.Text.Strings = (
          'TChart')
        Title.Visible = False
        BottomAxis.ExactDateTime = False
        BottomAxis.Grid.Visible = False
        BottomAxis.Increment = 0.001
        BottomAxis.MinorTickLength = 1
        BottomAxis.Title.Caption = 'Weight'
        LeftAxis.Grid.Color = 11184810
        LeftAxis.Grid.Style = psSolid
        LeftAxis.Labels = False
        LeftAxis.Title.Caption = 'Count'
        Legend.Visible = False
        View3D = False
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Series1: TBarSeries
          Marks.Arrow.Visible = False
          Marks.ArrowLength = 2
          Marks.Font.Charset = DEFAULT_CHARSET
          Marks.Font.Color = clBlack
          Marks.Font.Height = -9
          Marks.Font.Name = 'Arial'
          Marks.Font.Style = []
          Marks.Frame.Visible = False
          Marks.Style = smsValue
          Marks.Transparent = True
          Marks.Visible = True
          SeriesColor = clRed
          BarWidthPercent = 90
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1
          YValues.Order = loNone
        end
      end
    end
  end
  object PohlaviPanel: TPanel
    Left = 0
    Top = 0
    Width = 592
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 13
      Width = 38
      Height = 13
      Caption = 'Gender:'
    end
    object SpeedButton1: TSpeedButton
      Left = 72
      Top = 12
      Width = 15
      Height = 16
      Enabled = False
      Flat = True
      Glyph.Data = {
        1E010000424D1E010000000000007600000028000000120000000E0000000100
        040000000000A800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808888888
        8088880000008888088888888088880000008800000888800000880000008888
        0888888880888800000088880888888880888800000088800088888800088800
        0000870888078870888078000000808888808808888808000000088888880088
        8888800000000888888800888888800000000888888800888888800000008088
        8880880888880800000087088807887088807800000088800088888800088800
        0000}
      NumGlyphs = 2
    end
    object SpeedButton2: TSpeedButton
      Left = 112
      Top = 12
      Width = 15
      Height = 15
      Enabled = False
      Flat = True
      Glyph.Data = {
        FA000000424DFA000000000000007600000028000000160000000B0000000100
        0400000000008400000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888000888888
        8800088888008708880788887088807888008088888088880888880888000888
        8888088088888880880008888888088088888880880008888888088088888880
        8800808888808888088888088800870888008808708880088000888000880808
        880008808000888888888008888888880000888888800008888888000000}
      NumGlyphs = 2
    end
    object PohlaviSamiceRadioButton: TRadioButton
      Left = 56
      Top = 12
      Width = 17
      Height = 17
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = PohlaviSamiceRadioButtonClick
    end
    object PohlaviSamciRadioButton: TRadioButton
      Left = 96
      Top = 12
      Width = 17
      Height = 17
      TabOrder = 1
      OnClick = PohlaviSamiceRadioButtonClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 320
    Width = 592
    Height = 53
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object Button1: TButton
      Left = 408
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Repeat'
      ModalResult = 1
      TabOrder = 0
    end
    object Button2: TButton
      Left = 504
      Top = 16
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      Default = True
      ModalResult = 2
      TabOrder = 1
    end
  end
end
