//***************************************************************************
//
//    CurveGrid.cpp - Growth curve in a StringGrid
//    Version 1.0
//
//***************************************************************************

#include "CurveGrid.h"
#include "DM.h"                 // FORMAT_HMOTNOSTI
#include "Konstanty.h"

//---------------------------------------------------------------------------
// Smazani tabulky
//---------------------------------------------------------------------------

void CurveGridClear(TStringGrid *Grid) {
  // Vymazu celou tabulku <Grid> (nemazu zahlavi)
  for (int row = 1; row < Grid->RowCount; row++) {
    for (int col = 1; col < Grid->ColCount; col++) {
      Grid->Cells[col][row] = "";
    }
  }
}

//---------------------------------------------------------------------------
// Zobrazeni obsahu krivky
//---------------------------------------------------------------------------

void CurveGridShow(TStringGrid *Grid) {
  // Zobrazi popis v tabulce <Grid>
  Grid->RowCount    = CURVE_MAX_POINTS + 1;
  Grid->Cells[1][0] = LoadStr(DEN);
  Grid->Cells[2][0] = LoadStr(HMOTNOST);
  for (int i = 1; i <= CURVE_MAX_POINTS; i++) {
    Grid->Cells[0][i] = String(i);
  }
}

//---------------------------------------------------------------------------
// Zobrazeni obsahu krivky
//---------------------------------------------------------------------------

void CurveGridShow(TStringGrid *Grid, TGrowthCurve *Curve) {
  // Zobrazi krivku <Curve> v tabulce <Grid>

  // Vymazu celou tabulku (nemazu zahlavi)
  CurveGridClear(Grid);

  // Vyplnim tabulku
  for (int row = 1; row <= Curve->Count(); row++) {
    Grid->Cells[1][row] = Curve->GetDay(row - 1);
    Grid->Cells[2][row] = FormatFloat(FORMAT_HMOTNOSTI, Curve->GetWeight(row - 1));
  }
}

//---------------------------------------------------------------------------
// Zjisteni poctu zadanych dnu v tabulce
//---------------------------------------------------------------------------

int CurveGridNumberOfDays(TStringGrid *Grid) {
  // Vrati pocet zadanych dnu v tabulce <Grid>
  int Count = 0;

  for (int row = 1; row < Grid->RowCount; row++) {
    // Zkontroluju, zda zadal cislo
    if (Grid->Cells[1][row].IsEmpty()) {
      break;
    }
    Count++;
  }
  return Count;
}

//---------------------------------------------------------------------------
// Kontrola udaju zadanych v tabulce
//---------------------------------------------------------------------------

bool CurveGridCheckData(TStringGrid *Grid) {
  // Zkontroluje hodnoty zadane v tabulce <Grid>
  int DaysCount;
  int Day;
  double Weight;
  TGridRect Rect;
  TGrowthCurve *Curve;        // Budu vytvaret jakoby novou krivku, aby probihala i kontrola existence dne ve krivce

  // Zjistim pocet dnu zadanych v tabulce
  DaysCount = CurveGridNumberOfDays(Grid);

  // Zalozim novou testovaci krivku
  Curve = new TGrowthCurve("Check",
                     CURVE_MAX_POINTS,
                     CURVE_FIRST_DAY_NUMBER,
                     CURVE_MAX_DAY,
                     PrepoctiNaKg(MIN_TARGET_WEIGHT),
                     PrepoctiNaKg(MAX_TARGET_WEIGHT));

  if (Curve == NULL) {
    return false;
  }

  try {
    // Projizdim jednotlive radky
    for (int row = 1; row <= DaysCount; row++) {

      // Kontrola dne
      try {
        Day = Grid->Cells[1][row].ToInt();
        if (!Curve->CheckDay(Day)) {
          throw 1;                // Den ma bud spatny rozsah, nebo uz v krivce existuje
        }
      } catch(...) {
        Rect.Left   = 1;
        Rect.Top    = row;
        Rect.Right  = 1;
        Rect.Bottom = row;
        Grid->Selection = Rect;
        Grid->SetFocus();
        delete Curve;
        return false;
      }

      // Kontrola hmotnosti
      try {
        Weight = Grid->Cells[2][row].ToDouble();
        if (!Curve->CheckWeight(Weight)) {
          throw 1;                // Den ma bud spatny rozsah, nebo uz v krivce existuje
        }
      } catch(...) {
        Rect.Left   = 2;
        Rect.Top    = row;
        Rect.Right  = 2;
        Rect.Bottom = row;
        Grid->Selection = Rect;
        Grid->SetFocus();
        delete Curve;
        return false;
      }

      // Pridam bod do testovaci krivky (nutne pro kontrolu existence dne ve krivce)
      if (!Curve->Add(Day, Weight)) {
        throw 1;
      }

    }//for
  } catch(...) {}

  delete Curve;
  return true;
}

//---------------------------------------------------------------------------
// Ulozeni obsahu krivky
//---------------------------------------------------------------------------

bool CurveGridSave(TStringGrid *Grid, TGrowthCurve *Curve) {
  // Ulozi krivku <Curve> zadanou v tabulce <Grid>
  int DaysCount;

  // Zjistim pocet dnu zadanych v tabulce
  DaysCount = CurveGridNumberOfDays(Grid);

  // Zkontroluju, zda je tabulka zadana korektne
  if (!CurveGridCheckData(Grid)) {
    return false;
  }

  // Smazu vsechny dosavadni body v krivce
  Curve->Clear();

  // Ulozim body z tabulky, hodnoty v tabulce jsou zadane spravne (testoval jsem vyse)
  for (int row = 1; row <= DaysCount; row++) {
    Curve->Add(Grid->Cells[1][row].ToInt(), Grid->Cells[2][row].ToDouble());
  }

  return true;
}

