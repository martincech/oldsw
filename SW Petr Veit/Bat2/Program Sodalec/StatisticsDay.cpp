//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "StatisticsDay.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TStatisticsDayForm *StatisticsDayForm;
//---------------------------------------------------------------------------
__fastcall TStatisticsDayForm::TStatisticsDayForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TStatisticsDayForm::FormShow(TObject *Sender)
{
  TodayRadioButton->Checked = true;
  TodayRadioButton->SetFocus();
  DayEdit->Clear();        
}
//---------------------------------------------------------------------------
void __fastcall TStatisticsDayForm::AnotherDayRadioButtonClick(
      TObject *Sender)
{
  DayEdit->SetFocus();        
}
//---------------------------------------------------------------------------
void __fastcall TStatisticsDayForm::Button1Click(TObject *Sender)
{
  if (AnotherDayRadioButton->Checked) {
    try {
      DayEdit->Text.ToInt();
    } catch(...) {
      DayEdit->SetFocus();
      return;   // Nezadal platne cislo
    }
  }//if
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TStatisticsDayForm::DayEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57)) {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------


