//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "FindFile.h"
#include "File.h"
#include "CheckId.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFindFileForm *FindFileForm;

//---------------------------------------------------------------------------
// Kontrola zadaneho filtru
//---------------------------------------------------------------------------

bool TFindFileForm::CheckValues() {
  // Kontrola zadaneho filtru, muze byt i prazdne
  try {
    if (!ScalesEdit->Text.IsEmpty() && !CheckId(ScalesEdit->Text)) {
      ScalesEdit->SetFocus();
      throw 1;  // Id. cislo nesmi byt 0
    }
    if (!DayEdit->Text.IsEmpty() && DayEdit->Text.ToInt() < 0) {
      DayEdit->SetFocus();
      throw 1;  // Den pouze kladny
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return false;
  }
  return true;  // Vse je zadane v poradku
}

//---------------------------------------------------------------------------
// Vyhledani souboru podle zadaneho filtru a ulozeni jmen souboru do listboxu
//---------------------------------------------------------------------------

bool TFindFileForm::FileFitsFilter(AnsiString Scale, int Day, TDate From, TDate Till) {
  // Pokud prave otevreny soubor pro hledani odpovida zadanym podminkam, vrati true. Soubor je rozkodovany do tabulek.
  bool Fits = true;     // Podminky jsou AND => default je true a libovolna nesplnena podminka neguje

  // Id. cislo vahy
  if (Scale != "") {      // Chce testovat podle podminky pro cislo vahy
    try {
      Data->FKonfiguraceTable->Open();
      if (Data->FKonfiguraceTable->FieldByName("ID")->AsString != Scale) {
        Fits = false;
      }
    } catch(...) {
      Fits = false;     // Neco nevyslo (spatny/stary soubor atd.), nevyhovuje
    }
    Data->FKonfiguraceTable->Close();
  }//if

  // Cislo dne a datum
  if (Day >= 0 || (int)From != NULL || (int)Till != NULL) {  // Chce testovat aspon podle nektere z podminek
    TQuery *Query = new TQuery(NULL);
    Query->DatabaseName = Data->Database->DatabaseName;
    TDateTime DateTime;
    try {
      Query->SQL->Clear();
      Query->SQL->Add("SELECT COUNT(DAY_NUMBER) AS RECORD_COUNT FROM \"" + Data->FStatistikaTable->TableName + "\"");
      Query->SQL->Add("WHERE 1=1");
      if (Day >= 0) {      // Chce testovat podle podminky pro cislo dne
        Query->SQL->Add("AND DAY_NUMBER=" + String(Day));
      }
      if ((int)From != NULL) {  // Chce testovat podle podminky pro zacatek data
        DateTime = (int)From;
        Query->SQL->Add("AND DATE_TIME >= '" + DateTime.DateTimeString() + "'");
      }
      if ((int)Till != NULL) {  // Chce testovat podle podminky pro konec data
        DateTime = (int)Till;
        DateTime += TDateTime(23, 59, 59, 0);    // Sloupec DATE_TIME je ve formatu data i casu => musim i cas 23:59
        Query->SQL->Add("AND DATE_TIME <= '" + DateTime.DateTimeString() + "'");
      }
      Query->Open();
      if (Query->FieldByName("RECORD_COUNT")->AsInteger == 0) {
        Fits = false;   // Zadny zaznam nevyhovuje zadanym podminkam
      }
    } catch(...) {
      Fits = false;     // Neco nevyslo (spatny/stary soubor atd.), nevyhovuje
    }
    delete Query;
  }//if

  return Fits;
}

void TFindFileForm::FindFiles(String Directory, AnsiString Scale, int Day, TDate From, TDate Till, TListBox *ListBox) {
  // Vyhleda soubory v <Directory> vcetne podadresaru a zobrazi je v <ListBox>, ktery uz musi byt smazany
  // Parametr je nezadany (tj. se na nej netestuje) pri techto hodnotach:
  // <Scale>    ""
  // <Day>      -1
  // <From>     NULL
  // <Till>     NULL
  TSearchRec SearchRec;
  int Result;
  String FullName;

  for (Result = FindFirst(Directory + "\\*.*", 0xFFFFFFFF, SearchRec); Result == 0; Result = FindNext(SearchRec)) {
    Application->ProcessMessages();
    if (Stopped) {
      break;            // Uzivatel ukoncil hledani
    }
    if (SearchRec.Name == "." || SearchRec.Name == "..") {
      continue;         // Odfiltruju root
    }
    FullName = Directory + "\\" + SearchRec.Name;
    if (SearchRec.Attr & faDirectory) {
      // Jde o adresar, projedu ho rekurzivnim volanim
      FindFiles(FullName, Scale, Day, From, Till, ListBox);
      continue;
    }
    // Jde o soubor
    if (ExtractFileExt(SearchRec.Name).LowerCase() != "." + Data->Version.FileExtension) {
      continue;         // Nejde o soubor z Bat2
    }
    // Jde o soubor z Bat2 - otestuji ho podle podminek
    FileDecodeFileToTables(FullName, false, true);     // Otevru soubor pro hledani
    if (!FileFitsFilter(Scale, Day, From, Till)) {
      continue;         // Soubor nevyhovuje podminkam
    }
    // Soubor vyhovuje zadanym podminkam, pridam ho do seznamu
    ListBox->Items->Add(FullName);     // Pridam soubor do seznamu
  }//for
  FindClose(SearchRec);
}


//---------------------------------------------------------------------------
__fastcall TFindFileForm::TFindFileForm(TComponent* Owner)
        : TForm(Owner)
{
  // Nastavim default adresar na Dokumenty
  DirectoryListBox->Directory = Data->SpecialFolders->Documents;
}
//---------------------------------------------------------------------------

void __fastcall TFindFileForm::FormShow(TObject *Sender)
{
  // Povolim / zakazu tlacitka
  StopButton->Enabled   = false;
  SearchButton->Enabled = true;
  OpenButton->Enabled   = false;
  ScalesEdit->SetFocus();
  // Adresar v DirectoryListBox nijak nemenim, poprve tam bude default adresar a podruhe to, to zvolil predtim
}
//---------------------------------------------------------------------------
void __fastcall TFindFileForm::SearchButtonClick(TObject *Sender)
{
  AnsiString Scale = "";      // Default bez filtru
  int Day    = -1;
  TDate From = NULL;
  TDate Till = NULL;

  if (!CheckValues()) {
    return;     // Filtr neni spravne zadany
  }

  Stopped = false;
  DirectoryListBox->OpenCurrent();      // Aby otevrel pripadny adresar, na kterem je najety (default je nutne poklikat a ne jen vybrat)
  StopButton->Enabled   = true;
  SearchButton->Enabled = false;
  OpenButton->Enabled   = false;
  FilesListBox->Clear();

  // Nastavim filtr pro fci FindFiles() - jen pokud jsou
  Scale = GetIdWithoutSpaces(ScalesEdit->Text);
  if (!DayEdit->Text.IsEmpty()) {
    Day = DayEdit->Text.ToInt();
  }
  if (FromCheckBox->Checked) {
    From = FromDateTimePicker->Date;
  }
  if (TillCheckBox->Checked) {
    Till = TillDateTimePicker->Date;
  }

  // Hledam
  Screen->Cursor = crAppStart;
  try {
    FindFiles(DirectoryListBox->Directory, Scale, Day, From, Till, FilesListBox);
  } catch(...) {}

  // Povolim zase tlacitka
  StopButton->Enabled   = false;
  SearchButton->Enabled = true;
  if (FilesListBox->Items->Count > 0) {
    OpenButton->Enabled   = true;       // Jen pokud je v zseznamu nejaky soubor
  }
  FilesListBox->SetFocus();

  Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------
void __fastcall TFindFileForm::ScalesEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57)) {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------
void __fastcall TFindFileForm::StopButtonClick(TObject *Sender)
{
  Stopped = true;
}
//---------------------------------------------------------------------------
void __fastcall TFindFileForm::Button1Click(TObject *Sender)
{
  Stopped = true;       // Stopnu pripadne vyhledavani
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------
void __fastcall TFindFileForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  Stopped = true;       // Stopnu pripadne vyhledavani
}
//---------------------------------------------------------------------------
void __fastcall TFindFileForm::OpenButtonClick(TObject *Sender)
{
  if (FilesListBox->ItemIndex < 0) {
    return;     // Neni vybrany zadny soubor
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------

