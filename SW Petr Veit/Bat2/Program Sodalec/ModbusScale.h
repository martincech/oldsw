//***************************************************************************
//
//    ModbusScale.cpp - Scale connected to the RS-485 network
//    Version 1.0
//
//***************************************************************************

#ifndef MODBUSSCALE_H
#define MODBUSSCALE_H

#include <system.hpp>           // AnsiString
#include <vector.h>

#pragma pack(push, 1)           // byte alignment
  #include "c51/Bat2.h"         // flash data definition
#pragma pack(pop)               // original alignment

//---------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------

// Pripojeni vahy
typedef struct {
  int        Address;           // Sitova adresa
  int        Port;              // Cislo serioveho portu
} TModbusScaleConnection;

// Data nactena z vahy
typedef struct {
  TConfig       Config;         // Nastaveni vahy
  TFlocks       Flocks;         // Definice jednotlivych hejn
  TLongDateTime DateTime;       // Aktualni datum a cas
  bool          Pause;          // Pauza vazeni
  vector<TArchive *> ArchiveList;       // Seznam vysledku jednotlivych dnu, aktualni den vazeni vzdy na konci seznamu
  TCalibration  Calibration;    // Kalibrace
} TScaleData;

typedef struct {
  byte           Online;                // Flag, zda se jedna o online vazeni
  word           InitialDayNumber;      // Cislo prvniho dne vazeni
  byte           FlockNumber;           // Cislo hejna, podle ktereho se ma vazit, nebo FLOCK_EMPTY_NUMBER pri rychlem vazeni
  TQuickWeighing QuickWeighing;         // Parametry zadane pro rychle vazeni (bez pouziti hejna)
  byte           WaitingForStart;       // Flag, zda se bude cekat na opozdeny start vazeni
  byte           ComparisonFlock;       // Cislo hejna, se kterym se porovnavaji vysledky, pripadne FLOCK_EMPTY_NUMBER pokud se neporovnava
  TLongDateTime  DateTime;              // Datum zahajeni vykrmu
} TModbusStartParameters;

// Vysledek posledni operace
typedef enum {
  MB_RESULT_OK,                 // Odpoved v poradku
  MB_RESULT_TIMEOUT,            // Timeout, vaha neodpovida
  MB_RESULT_INVALID_ADDRESS,    // Neplatna adresa
  MB_RESULT_INVALID_VALUE,      // Neplatna hodnota pri zapisu do registru
  MB_RESULT_ERROR               // Jina chyba
} TModbusScaleResult;

// Diagnosticke citace
typedef struct {
  int Total;
  int Error;
  int Exception;
  int Message;
  int Response;
  int Nak;
  int Busy;
  int Overrun;
} TModbusScaleCounters;

//---------------------------------------------------------------------------
// Vaha
//---------------------------------------------------------------------------

class TModbusScale {

private:
  // Definice vahy
  AnsiString             Name;          // Nazev vahy
  TModbusScaleConnection Connection;    // Pripojeni vahy

  int  MessageCounter;                  // Interni citac vyslanych zprav
  int  ErrorCounter;                    // Interni citac chybnych odpovedi

  void ClearErrors();
  // Snuluje posledni chybu a vyjimku

  TModbusScaleResult ReadRegisters(int Start, int Stop);
  // Nacte registry <Start>..<Stop>
  TModbusScaleResult ReadReply();
  // Nacte odpoved z vahy
  TModbusScaleResult ReadAndDecodeRegisters(int Start, int Stop);
  // Nacte registry <Start>..<Stop> a dekoduje je do struktur
  TModbusScaleResult WriteRegister(int Address, int Value);
  // Zapise do registru <Address> hodnotu <Value> a vrati odpoved
  TModbusScaleResult WriteRegisters(int Start, int Stop, unsigned short int *Data);
  // Zapise do registru <Start>..<Stop> hodnotu v <Data> a vrati odpoved

  void DeleteLastDayFromArchiveList();
  // Vymaze posledni den ze seznamu
  TModbusScaleResult ReadAndDecodeArchive();
  // Nacte a dekoduje statistiku

  TModbusScaleResult DecodeRegisters();
  // Dekoduje prijate registry
  TModbusScaleResult DecodeRegister(int Address, int Value);
  // Dekoduje registr <Address> s hodnotu <Value>

  bool CheckLetter(byte ch);
  // Zkontroluje hodnotu znaku <ch>
  bool CheckIdLetter(byte ch);
  // Zkontroluje hodnotu znaku <ch>

  void SaveWordMsb(void *Data, unsigned short int Value);
  // Ulozi do <Data> hodnotu <Value> jako 2 bajtovy MSB
  void SaveWordLsb(void *Data, unsigned short int Value);
  // Ulozi do <Data> hodnotu <Value> jako 2 bajtovy LSB

  void SetDefaultConfig(TConfig *Config);
  // Nastavi implicitni hodnoty parametru, ktere se z vahy nenacitaji

protected:
  bool Connected;                       // Modbus je inicializovany a port je otevren

public:
  TScaleData             ScaleData;
  TModbusStartParameters ModbusStartParameters;
  TModbusScaleCounters   Counters;

  TModbusScale();

  AnsiString GetName()    { return Name; }
  int GetAddress()        { return Connection.Address; }
  int GetPort()           { return Connection.Port; }
  int GetMessageCounter() { return MessageCounter; }
  int GetErrorCounter()   { return ErrorCounter; }

  bool SetName(AnsiString NewName);
  bool SetAddress(int NewAddress);
  bool SetPort(int NewPort);

  bool Connect();
  // Otevre port a inicializuje Modbus podle parametru v <Connection>

  TModbusScaleResult ReadConfig();
  // Nacte konfiguraci do globalniho <ScaleData.Config>
  TModbusScaleResult ReadConfigRep(int Repetitions);
  // Nacte konfiguraci do globalniho <ScaleData.Config>, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult WriteConfig(TConfig *Config, bool WeighingStarted);
  // Zapise do vahy nastaveni <Config>. Pokud je <WeighingStarted> true, zapisuje jen ty polozky, ktere lze menit behem vazeni.
  TModbusScaleResult WriteConfigRep(TConfig *Config, bool WeighingStarted, int Repetitions);
  // Zapise do vahy nastaveni <Config>. Pokud je <WeighingStarted> true, zapisuje jen ty polozky, ktere lze menit behem vazeni.
  // Pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult ReadPause();
  // Nacte pauzu vazeni
  TModbusScaleResult ReadPauseRep(int Repetitions);
  // Nacte pauzu vazeni, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult ReadFlock(int FlockNumber);
  // Nacte hejno cislo <FlockNumber>
  TModbusScaleResult ReadFlockRep(int FlockNumber, int Repetitions);
  // Nacte hejno cislo <FlockNumber>, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult ReadFlocks();
  // Nacte vsechna hejna
  TModbusScaleResult ReadFlocksRep(int Repetitions);
  // Nacte vsechna hejna, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult WriteFlock(TFlock *Flock);
  // Zapise do vahy hejno <Flock> (hejno nesmi byt prazdne, k mazani hejna se pouziva jina fce).
  TModbusScaleResult WriteFlockRep(TFlock *Flock, int Repetitions);
  // Zapise do vahy hejno <Flock>, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult DeleteFlock(int FlockNumber);
  // Smaze hejno cislo <FlockNumber>
  TModbusScaleResult DeleteFlockRep(int FlockNumber, int Repetitions);
  // Smaze hejno cislo <FlockNumber>, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult WriteFlocks(TFlock *Flock, int WeighedFlock);
  // Zapise do vahy vsechna hejna <Flock>, <WeighedFlock> je cislo hejna, ktere se prave vazi nebo -1
  TModbusScaleResult WriteFlocksRep(TFlock *Flock, int WeighedFlock, int Repetitions);
  // Zapise do vahy vsechna hejna <Flock>, <WeighedFlock> je cislo hejna, ktere se prave vazi nebo -1
  // Pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult ReadDateTime();
  // Nacte datum a cas
  TModbusScaleResult ReadDateTimeRep(int Repetitions);
  // Nacte datum a cas, pri chybe zkusi <Repetitions> krat opakovat.

  void ClearArchiveList();
  // Vymaze dosavadni vysledky
  TModbusScaleResult ReadTodayFromArchive();
  // Nacte statistiku aktualniho dne vazeni
  TModbusScaleResult ReadTodayFromArchiveRep(int Repetitions);
  // Nacte statistiku aktualniho dne vazeni, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult ReadFirstDayFromArchive();
  // Nacte statistiku prvniho dne v archivu
  TModbusScaleResult ReadFirstDayFromArchiveRep(int Repetitions);
  // Nacte statistiku prvniho dne v archivu, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult ReadNextDayFromArchive();
  // Nacte statistiku dalsiho dne v archivu
  TModbusScaleResult ReadNextDayFromArchiveRep(int Repetitions);
  // Nacte statistiku dalsiho dne v archivu, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult ReadCalibration();
  // Nacte kalibraci
  TModbusScaleResult ReadCalibrationRep(int Repetitions);
  // Nacte kalibraci, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult ReadCounters();
  // Nacte diagnosticke citace
  TModbusScaleResult ReadCountersRep(int Repetitions);
  // Nacte diagnosticke citace, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult StartWeighing(TModbusStartParameters *ModbusStartParameters);
  // Zahaji vazeni s parametry <ModbusStartParameters>
  TModbusScaleResult StartWeighingRep(TModbusStartParameters *ModbusStartParameters, int Repetitions);
  // Zahaji vazeni s parametry <ModbusStartParameters>, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult PauseWeighing(bool Pause);
  // Zapauzuje/odpauzuje vazeni
  TModbusScaleResult PauseWeighingRep(bool Pause, int Repetitions);
  // Zahaji vazeni s parametry <ModbusStartParameters>, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult StopWeighing();
  // Stop vazeni
  TModbusScaleResult StopWeighingRep(int Repetitions);
  // Stop vazeni, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult SetDateTime(TLongDateTime *DateTime);
  // Nastavi datum a cas na <DateTime>
  TModbusScaleResult SetDateTimeRep(TLongDateTime *DateTime, int Repetitions);
  // Zahaji vazeni s parametry <ModbusStartParameters>, pri chybe zkusi <Repetitions> krat opakovat.

  TModbusScaleResult SetCompare(int Flock);
  // Nastavi porovnani na hejno <Flock>
  TModbusScaleResult SetCompareRep(int Flock, int Repetitions);
  // Nastavi porovnani na hejno <Flock>, pri chybe zkusi <Repetitions> krat opakovat.

  void BuildModuleData(byte *Data);
  // Sestavi dosud nactena data do struktury odpovidajici pametovemu modulu a ulozi ji do <Data>.

};

#endif // MODBUSSCALE_H

