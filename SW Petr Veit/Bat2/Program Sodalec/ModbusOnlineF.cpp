//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusOnlineF.h"
#include "ModbusScaleList.h"
#include "DM.h"
#include "Konstanty.h"
#include "ModbusScales.h"
#include "ModbusPortSetup.h"
#include "ModbusStartWeighing.h"
#include "ModbusErrors.h"
#include "ModbusCompare.h"
#include "ModbusWait.h"
#include "ScaleSetup.h"
#include "ModbusScaleSetupF.h"
#include "File.h"
#include "Convert.h"
#include "Dpi.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TModbusOnlineForm *ModbusOnlineForm;

//---------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------

#define WeighingBothGenders()                                                                                   \
 ((Scale->ScaleData.Config.WeighingStart.UseFlock                                                               \
   && Scale->ScaleData.Flocks[Scale->ScaleData.Config.WeighingStart.CurrentFlock].Header.UseBothGenders)        \
  ||                                                                                                            \
  (!Scale->ScaleData.Config.WeighingStart.UseFlock                                                              \
   && Scale->ScaleData.Config.WeighingStart.QuickWeighing.UseBothGenders))

#define REPLY_DELAY_START_WEIGHING      5000            // ReplyDelay pri startovani vazeni (je treba delsi)

//---------------------------------------------------------------------------
// Smazani tabulky
//---------------------------------------------------------------------------

void TModbusOnlineForm::ClearTable() {
  // Vymazu celou tabulku (nemazu zahlavi)
  for (int row = 1; row < ScalesStringGrid->RowCount; row++) {
    for (int col = 0; col < ScalesStringGrid->ColCount; col++) {
      ScalesStringGrid->Cells[col][row] = "";
    }
  }
  ScalesStringGrid->RowCount = 2;
}

//---------------------------------------------------------------------------
// Zobrazeni seznamu vah
//---------------------------------------------------------------------------

void TModbusOnlineForm::DisplayScaleList() {
  // Prekresli tabulku vah
  TModbusOnlineScale *Scale;
  TArchive *Archive;
  AnsiString Str;

  // Vymazu celou tabulku (nemazu zahlavi)
  ClearTable();

  // Nastavim spravny pocet radku
  if (ModbusOnline->ScalesCount() == 0) {
    ScalesStringGrid->RowCount = 2;             // Minimalne 2 radky, aby zustala zachovana hlavicka tabulky
    return;
  }
  ScalesStringGrid->RowCount = ModbusOnline->ScalesCount() + 1;

  for (int Index = 0; Index < ModbusOnline->ScalesCount(); Index++) {
    // Nactu ukazatel na vahu ze seznamu
    if ((Scale = ModbusOnline->GetScale(Index)) == NULL) {
      return;
    }

    // Vyplnim tabulku

    // Pozice nacitani
    if (ModbusOnline->GetActiveScaleIndex() == Index) {
      ScalesStringGrid->Cells[0][Index + 1] = ">";
    } else {
      ScalesStringGrid->Cells[0][Index + 1] = "";
    }

    // Nazev vahy
    ScalesStringGrid->Cells[1][Index + 1] = GetScaleName(Scale);

    // Dalsi sloupce zobrazuju jen pokud je vaha pripojena
    if (!Scale->getLive()) {
      // Vaha neni pripojena => vymazu vsechny dalsi sloupce a koncim
      for (int i = 2; i < ScalesStringGrid->ColCount; i++) {
        ScalesStringGrid->Cells[i][Index + 1] = "";
      }
      continue;
    }

    // Datum a cas
    TDateTime dt;
    dt = TDateTime(Scale->ScaleData.DateTime.Year, Scale->ScaleData.DateTime.Month, Scale->ScaleData.DateTime.Day)
       + TDateTime(Scale->ScaleData.DateTime.Hour, Scale->ScaleData.DateTime.Min, 0, 0);
    ScalesStringGrid->Cells[2][Index + 1] = dt.FormatString("ddddd h:nn");

    // Stav
    AnsiString OnlineStr;
    if (Scale->ScaleData.Config.WeighingStart.Online) {
      OnlineStr = " (" + LoadStr(ONLINE) + ")";
    } else {
      OnlineStr = "";
    }
    if (Scale->ScaleData.Config.WeighingStart.Running) {
      if (Scale->ScaleData.Pause) {
        ScalesStringGrid->Cells[3][Index + 1] = LoadStr(STR_RS485_PAUSED) + OnlineStr;
      } else {
        ScalesStringGrid->Cells[3][Index + 1] = LoadStr(STR_RS485_WEIGHING) + OnlineStr;
      }
    } else if (Scale->ScaleData.Config.WeighingStart.WaitingForStart) {
      ScalesStringGrid->Cells[3][Index + 1] = LoadStr(STR_RS485_WAITING) + OnlineStr;
    } else {
      ScalesStringGrid->Cells[3][Index + 1] = LoadStr(STR_RS485_STOPPED);
    }

    // Pokud se nevazi, statistiku nezobrazuju
    if (!Scale->ScaleData.Config.WeighingStart.Running) {
      for (int i = 4; i < ScalesStringGrid->ColCount; i++) {
        ScalesStringGrid->Cells[i][Index + 1] = "";
      }
      continue;
    }

    // Ulozim si archiv aktualniho dne vazeni - vzdy na konci seznamu (muze tam byt i vice dnu po nacteni kompletniho archivu)
    Archive = Scale->ScaleData.ArchiveList.back();

    // Den vazeni
    ScalesStringGrid->Cells[4][Index + 1] = Archive->DayNumber;

    // Cilova hmotnost
    Str = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg(Archive->TargetWeight[GENDER_FEMALE]));
    if (WeighingBothGenders()) {
      Str += " / " + FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg(Archive->TargetWeight[GENDER_MALE]));
    }
    ScalesStringGrid->Cells[5][Index + 1] = Str;

    // Pocet vazeni
    Str = Archive->Stat[GENDER_FEMALE].Count;
    if (WeighingBothGenders()) {
      Str += " / " + AnsiString(Archive->Stat[GENDER_MALE].Count);
    }
    ScalesStringGrid->Cells[6][Index + 1] = Str;

    // Prumerna hmotnost
    float Average[_GENDER_COUNT];
    Average[GENDER_FEMALE] = StatAverage(&Archive->Stat[GENDER_FEMALE]);
    Str = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average[GENDER_FEMALE]));
    if (WeighingBothGenders()) {
      Average[GENDER_MALE] = StatAverage(&Archive->Stat[GENDER_MALE]);
      Str += " / " + FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average[GENDER_MALE]));
    }
    ScalesStringGrid->Cells[7][Index + 1] = Str;

    // Denni prirustek
    if (Archive->DayNumber == CURVE_FIRST_DAY_NUMBER + Scale->ScaleData.Config.WeighingStart.CurveDayShift) {
      // Jde o 1. den vazeni, prirustek je 0. Pokud bych ho spocital, vysel by prirustek roven prumerne hmotnosti, coz je blbost.
      Str = FormatFloat(FORMAT_HMOTNOSTI, 0);
      if (WeighingBothGenders()) {
        Str += " / " + FormatFloat(FORMAT_HMOTNOSTI, 0);
      }
    } else {
      // Jde o nektery dalsi den vazeni, prirustek muzu spocitat
      Str = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)(Average[GENDER_FEMALE] - Archive->LastAverage[GENDER_FEMALE])));
      if (WeighingBothGenders()) {
        Str += " / " + FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)(Average[GENDER_MALE] - Archive->LastAverage[GENDER_MALE])));
      }
    }
    ScalesStringGrid->Cells[8][Index + 1] = Str;

    // Uniformita
    if (Archive->RealUniformityUsed) {
      Str = Archive->RealUniformity[GENDER_FEMALE];
      if (WeighingBothGenders()) {
        Str += " / " + FormatFloat(FORMAT_HMOTNOSTI, Archive->RealUniformity[GENDER_MALE]);
      }
    } else {
      Str = (int)StatUniformity(Average[GENDER_FEMALE], StatSigma(&Archive->Stat[GENDER_FEMALE]),
                                Scale->ScaleData.Config.WeighingStart.UniformityRange);
      if (WeighingBothGenders()) {
        Str += " / " + AnsiString((int)StatUniformity(Average[GENDER_MALE], StatSigma(&Archive->Stat[GENDER_MALE]),
                                                      Scale->ScaleData.Config.WeighingStart.UniformityRange));
      }
    }
    ScalesStringGrid->Cells[9][Index + 1] = Str;


  }//for
}

//---------------------------------------------------------------------------
// Ukonceni online threadu
//---------------------------------------------------------------------------

void TModbusOnlineForm::QuitOnlineThread() {
  // Ukonci online thread
  if (!ModbusOnlineThread) {
    return;
  }
  // Smazu existujici thread
  while (ModbusOnlineThread->Suspended) {
    ModbusOnlineThread->Resume();
  }
  ModbusOnlineThread->Terminate();
  ModbusOnlineThread->WaitFor();
  delete ModbusOnlineThread;
  ModbusOnlineThread = NULL;
}

//---------------------------------------------------------------------------
// Ukonceni nacitani na pozadi
//---------------------------------------------------------------------------

void TModbusOnlineForm::StopReading() {
  // Zastavi nacitani na pozadi
  Screen->Cursor = crHourGlass;
  try {
    OnlineTimer->Enabled = false;
    QuitOnlineThread();
  } catch(...) {}
  Screen->Cursor = crDefault;
}

//---------------------------------------------------------------------------
// Zahajeni nacitani na pozadi
//---------------------------------------------------------------------------

void TModbusOnlineForm::StartReading() {
  // Zahaji nacitani na pozadi

  // Zalozim thread a rovnou ho rozjedu
  ModbusOnlineThread = new TModbusOnlineThread(true);   // Zatim zastavim
  ModbusOnlineThread->SetModbusOnline(ModbusOnline);    // Nastavim online
  ModbusOnlineThread->Resume();                         // Rozjedu

  OnlineTimer->Enabled = true;
}

//---------------------------------------------------------------------------
// Nacteni vah
//---------------------------------------------------------------------------

void TModbusOnlineForm::LoadScales() {
  // Nacte seznam vah z databaze

  // Nahraju vsechny vahy z databaze
  TModbusScaleList *ScaleList = new TModbusScaleList();
  Data->LoadModbusScalesFromDatabase(ScaleList);

  // Ulozim nactene vahy do seznamu online vah
  ModbusOnline->ClearScaleList();
  for (int i = 0; i < ScaleList->Count(); i++) {
    ModbusOnline->AddScale(ScaleList->GetScale(i));
  }
}

//---------------------------------------------------------------------------
// Nacteni vybrane vahy
//---------------------------------------------------------------------------

TModbusOnlineScale *TModbusOnlineForm::GetSelectedScale() {
  // Vrati ukazatel na vahu vybranou v seznamu
  TModbusOnlineScale *Scale;

  // Zjistim vybranou vahu
  if (ModbusOnline->ScalesCount() == 0) {
    return NULL;        // V seznamu neni zadna vaha
  }

  // Nactu ukazatel na vahu, kterou ma prave vybranou
  if ((Scale = ModbusOnline->GetScale(ScalesStringGrid->Row - 1)) == NULL) {
    return NULL;
  }

  return Scale;
}

//---------------------------------------------------------------------------
// Pauza vazeni
//---------------------------------------------------------------------------

void TModbusOnlineForm::PauseWeighing(bool Pause) {
  // Zapauzuje/odpauzuje vazeni
  TModbusOnlineScale *Scale;
  TModbusScaleResult  Result;

  // Nactu vahu vybranou v tabulce
  if ((Scale = GetSelectedScale()) == NULL) {
    return;     // Seznam je prazdny
  }

  if (!Scale->getLive()) {
    return;     // Vaha neni pripojena
  }

  if (!Scale->ScaleData.Config.WeighingStart.Running || Scale->ScaleData.Config.WeighingStart.WaitingForStart) {
    return;     // Vaha nevazi nebo ceka na start
  }

  // Ukoncim nacitani na pozadi
  StopReading();

  Screen->Cursor = crHourGlass;
  try {
    if (!Scale->Connect()) {
      throw 1;
    }
    if (Scale->PauseWeighingRep(Pause, Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }

    // Nactu a zobrazim novy stav vahy
    ReadAndDisplaySelectedScale();
  } catch(...) {
    MessageBox(NULL,LoadStr(CHYBA).c_str(), Application->Title.c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
  }
  Screen->Cursor = crDefault;

  // Start nacitani na pozadi
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}

//---------------------------------------------------------------------------
// Aktivace ikon
//---------------------------------------------------------------------------

#define EnableMenu(Menu, Button, Enable)      \
  Menu->Enabled = Enable;                     \
  Button->Enabled = Enable;                   \

#define EnableStartMenu(Enable)         EnableMenu(Start1,           StartSpeedButton,    Enable)
#define EnableStopMenu(Enable)          EnableMenu(Stop1,            StopSpeedButton,     Enable)
#define EnablePauseMenu(Enable)         EnableMenu(Pause1,           PauseSpeedButton,    Enable)
#define EnableContinueMenu(Enable)      EnableMenu(Continue1,        ContinueSpeedButton, Enable)
#define EnableDownloadMenu(Enable)      EnableMenu(Downloadresults1, DownloadSpeedButton, Enable)
#define EnableDateTimeMenu(Enable)      EnableMenu(Dateandtime1,     DateTimeSpeedButton, Enable)
#define EnableSetupMenu(Enable)         EnableMenu(Setup1,           SetupSpeedButton,    Enable)
#define EnableCompareMenu(Enable)       EnableMenu(Compare1,         CompareSpeedButton,  Enable)

void TModbusOnlineForm::ActivateMenu(TModbusOnlineScale *Scale) {
  // Podle stavu vahy <Scale> aktivuje/zakaze menu

  if (!Scale || !Scale->getLive()) {
    EnableStartMenu(false);
    EnableStopMenu(false);
    EnablePauseMenu(false);
    EnableContinueMenu(false);
    EnableDownloadMenu(false);
    EnableDateTimeMenu(false);
    EnableSetupMenu(false);
    EnableCompareMenu(false);
    return;     // Vaha neni definovana nebo pripojena
  }

  // Start
  EnableStartMenu(!Scale->ScaleData.Config.WeighingStart.Running && !Scale->ScaleData.Config.WeighingStart.WaitingForStart);

  // Stop
  EnableStopMenu(Scale->ScaleData.Config.WeighingStart.Running || Scale->ScaleData.Config.WeighingStart.WaitingForStart);

  // Pauza/odpauza
  if (Scale->ScaleData.Config.WeighingStart.Running) {
    if (Scale->ScaleData.Pause) {
      EnableContinueMenu(true);
      EnablePauseMenu(false);
    } else {
      EnableContinueMenu(false);
      EnablePauseMenu(true);
    }
  } else {
    EnableContinueMenu(false);
    EnablePauseMenu(false);
  }

  // Stazeni dat
  EnableDownloadMenu(!Scale->ScaleData.Config.WeighingStart.WaitingForStart && !Scale->ScaleData.Config.WeighingStart.Online);

  // Nastaveni casu
  EnableDateTimeMenu(!Scale->ScaleData.Config.WeighingStart.Running && !Scale->ScaleData.Config.WeighingStart.WaitingForStart);

  // Nastaveni vahy
  EnableSetupMenu(true);

  // Porovnani
  EnableCompareMenu(true);
}

//---------------------------------------------------------------------------
// Nacteni a zobrazeni aktualne vybrane vahy
//---------------------------------------------------------------------------

void TModbusOnlineForm::ReadAndDisplaySelectedScale() {
  // Nacte kompletni stav vybrane vahy a zobrazi

  // Zastavim nacitani na pozadi, pokud by jeste nebylo zastavene
  if (OnlineTimer->Enabled) {
    StopReading();
  }

  // Nastavim vybranou vahu jako aktivni
  ModbusOnline->LoadScale(ScalesStringGrid->Row - 1);

  // Nactu data
  while (!ModbusOnline->Execute()) {
  }

  // Nacetl jsem kompetni data z vahy, zobrazim
  int Row = ScalesStringGrid->Row;
  DisplayScaleList();
  ScalesStringGrid->Row = Row;

  // Aktivuju menu podle stavu vybrane vahy - ten se mohl nactenim zmenit
  ActivateMenu(GetSelectedScale());
}

//---------------------------------------------------------------------------
// Nazev vahy vcetne adresy
//---------------------------------------------------------------------------

AnsiString TModbusOnlineForm::GetScaleName(TModbusScale *Scale) {
  // Vrati nazev vahy spolu s adresou
  if (!Scale) {
    return "";
  }
  return AnsiString(Scale->GetAddress()) + ": " + Scale->GetName();
}

//---------------------------------------------------------------------------
// Zalozeni prikazoveho threadu
//---------------------------------------------------------------------------

void TModbusOnlineForm::CreateCommandThread(TModbusScale *Scale, int Repetitions) {
  // Zalozi novy thread prikazu s vahou <Scale> a <Repetitions> opakovani pri chybe

  // Ukoncim stavajici thread
  QuitCommandThread();

  // Rozjedu novy thread
  ModbusCommandThread = new TModbusThread(true);       // Zatim zastaveny
  ModbusCommandThread->Busy        = false;
  ModbusCommandThread->Command     = THREAD_COMMAND_IDLE;
  ModbusCommandThread->Repetitions = Repetitions;
  ModbusCommandThread->Scale       = Scale;
  ModbusCommandThread->Resume();                       // Rozjedu
}

//---------------------------------------------------------------------------
// Ukonceni prikazoveho threadu
//---------------------------------------------------------------------------

void TModbusOnlineForm::QuitCommandThread() {
  // Ukonci thread prikazu
  if (!ModbusCommandThread) {
    return;
  }
  // Smazu existujici thread
  ModbusCommandThread->Command = THREAD_COMMAND_IDLE;
  while (ModbusCommandThread->Suspended) {
    ModbusCommandThread->Resume();
  }
  ModbusCommandThread->Terminate();
  ModbusCommandThread->WaitFor();
  delete ModbusCommandThread;
  ModbusCommandThread = NULL;
}

//---------------------------------------------------------------------------
// Start prikazu
//---------------------------------------------------------------------------

bool TModbusOnlineForm::StartCommand(TModbusThreadCommand Command, int Value) {
  // Odstartuje prikaz <Command> s hodnotou <Value>
  if (ModbusCommandThread->Busy) {
    // Nejaky prikaz se prave provadi
    return false;
  }

  ModbusCommandThread->CommandValue = Value;
  ModbusCommandThread->Command      = Command;
  ModbusCommandThread->Busy         = true;            // Odstartuje provadeni prikazu

  return true;
}

//---------------------------------------------------------------------------
// Vykonani prikazu
//---------------------------------------------------------------------------

bool TModbusOnlineForm::ExecuteCommand(TModbusThreadCommand Command) {
  // Vykona prikaz <Command>
  int LastNumber = 0;

  if (!StartCommand(Command, 0)) {
    return false;
  }
  while (ModbusCommandThread->Busy) {
    Application->ProcessMessages();
    if (ModbusWaitForm->Canceled) {
      return false;        // Kliknul na Cancel
    }
    // Obnovim zobrazeni u nacitani archivu, pokud doslo ke zmene cisla dne
    if (Command == THREAD_COMMAND_READ_ARCHIVE && LastNumber != ModbusCommandThread->CurrentNumber) {
      ModbusWaitForm->WaitLabel->Caption = LoadStr(STR_RS485_WAIT) + "...  (" + AnsiString(ModbusCommandThread->CurrentNumber) + ")";
      ModbusWaitForm->Refresh();
      LastNumber = ModbusCommandThread->CurrentNumber;
    }
  }
  if (ModbusCommandThread->Result != MB_RESULT_OK) {
    return false;
  }

  return true;
}

//---------------------------------------------------------------------------
__fastcall TModbusOnlineForm::TModbusOnlineForm(TComponent* Owner)
        : TForm(Owner)
{
  ModbusOnline = new TModbusOnline();
  ModbusOnlineThread  = NULL;
  ModbusCommandThread = NULL;

  // Nastavim hinty ikon
  StartSpeedButton->Hint    = Start1->Caption;
  StopSpeedButton->Hint     = Stop1->Caption;
  PauseSpeedButton->Hint    = Pause1->Caption;
  ContinueSpeedButton->Hint = Continue1->Caption;

  DownloadSpeedButton->Hint = Downloadresults1->Caption;

  DateTimeSpeedButton->Hint = Dateandtime1->Caption;
  SetupSpeedButton->Hint    = Setup1->Caption;
  CompareSpeedButton->Hint  = Compare1->Caption;

  AddSpeedButton->Hint  = Addremovescales1->Caption;
  PortSpeedButton->Hint = Communicationparameters1->Caption;

  ErrorsSpeedButton->Hint = Communicationerrors1->Caption;

  // Nastavim vysku radku vsech StringGridu podle aktualniho DPI
  DpiSetStringGrid(ScalesStringGrid);
}
//---------------------------------------------------------------------------
void __fastcall TModbusOnlineForm::FormShow(TObject *Sender)
{
  OnlineTimer->Enabled = false;
  FileSaved = false;            // Zatim jsem zadna data nestahnul

  // Nahraju vsechny vahy z databaze
  LoadScales();

  // Inicializace cteni dat
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);

  // Popis tabulky
  ScalesStringGrid->Cells[0][0] = "";
  ScalesStringGrid->Cells[1][0] = LoadStr(STR_RS485_SCALE);
  ScalesStringGrid->Cells[2][0] = LoadStr(STR_RS485_DATE_TIME);
  ScalesStringGrid->Cells[3][0] = LoadStr(STR_RS485_MODE);
  ScalesStringGrid->Cells[4][0] = LoadStr(DEN);
  ScalesStringGrid->Cells[5][0] = LoadStr(CILOVA_HMOTNOST);
  ScalesStringGrid->Cells[6][0] = LoadStr(POCET);
  ScalesStringGrid->Cells[7][0] = LoadStr(STR_RS485_AVERAGE);
  ScalesStringGrid->Cells[8][0] = LoadStr(STR_RS485_DAILY_GAIN);
  ScalesStringGrid->Cells[9][0] = LoadStr(STR_RS485_UNIFORMITY);

  // Zobrazim seznam vah
  DisplayScaleList();
  ScalesStringGrid->SetFocus();

  // Aktivace menu
  ActivateMenu(NULL);

  StartReading();       // Zahaji nacitani na pozadi
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::FormDestroy(TObject *Sender)
{
  delete ModbusOnline;
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::OnlineTimerTimer(TObject *Sender)
{
  if (!ModbusOnlineThread->NewData) {
    // Jeste pokracuju v nacitani
    return;
  }

  // Z vahy jsem vse nacetl, prekreslim
  int Row = ScalesStringGrid->Row;
  DisplayScaleList();
  ScalesStringGrid->Row = Row;

  // Aktivuju menu podle stavu vybrane vahy - ten se mohl nactenim zmenit
  ActivateMenu(GetSelectedScale());

  // Shodim flag, tim se zaroven opetovne rozjede thread (handshake)
  ModbusOnlineThread->NewData = false;
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  StopReading();        // Ukoncim nacitani na pozadi
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Addremovescales1Click(TObject *Sender)
{
  StopReading();        // Ukoncim nacitani na pozadi
  if (ModbusScalesForm->ShowModal() == mrOk) {
    // Zmenil seznam vah
    LoadScales();
    ModbusOnline->ClearScaleData();
    ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
    DisplayScaleList();
    ActivateMenu(NULL);         // Aktivace menu
  }
  // Inicializace cteni dat
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Communicationparameters1Click(
      TObject *Sender)
{
  StopReading();        // Ukoncim nacitani na pozadi
  if (ModbusPortSetupForm->ShowModal() == mrOk) {
    // Zmenil nastaveni komunikace
    ModbusOnline->ClearScaleData();
    ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
    DisplayScaleList();
    ActivateMenu(NULL);         // Aktivace menu
  }
  // Inicializace cteni dat
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Start1Click(TObject *Sender)
{
  TModbusOnlineScale *Scale;
  TModbusStartParameters Parameters;
  TModbusScaleResult  Result;

  // Nactu vahu vybranou v tabulce
  if ((Scale = GetSelectedScale()) == NULL) {
    return;     // Seznam je prazdny
  }

  if (!Scale->getLive()) {
    return;     // Vaha neni pripojena
  }

  if (Scale->ScaleData.Config.WeighingStart.Running || Scale->ScaleData.Config.WeighingStart.WaitingForStart) {
    return;     // Vaha uz vazi
  }

  // Ukoncim nacitani na pozadi
  StopReading();

  Screen->Cursor = crHourGlass;
  try {

    // Nactu z vahy aktualni nastaveni
    if (!Scale->Connect()) {
      throw 1;
    }
    if (Scale->ReadConfigRep(Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }
    if (Scale->ReadFlocksRep(Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }

    Screen->Cursor = crDefault;

    // Zeptam se na parametry vazeni
    ModbusStartWeighingForm->SetFlock(&Scale->ScaleData.Flocks[0]);
    ModbusStartWeighingForm->SetStartParameters(&Parameters);
    if (ModbusStartWeighingForm->ShowModal() != mrOk) {
      throw 1;
    }
    Refresh();          // Jinak to vypada hnusne

    Screen->Cursor = crHourGlass;

    // Nastavim docasne vetsi timeout (start vazeni dost trva)
    Data->SetModbusReplyDelay(REPLY_DELAY_START_WEIGHING);
    if (!Scale->Connect()) {
      throw 1;
    }

    // Rozjedu vazeni
    if (Scale->StartWeighingRep(&Parameters, Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      MessageBox(NULL,LoadStr(CHYBA).c_str(), Application->Title.c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
      throw 1;
    }

    // Nastavim zpet na default timeout
    Data->SetModbusPortParameters();
    if (!Scale->Connect()) {
      throw 1;
    }

    // Nactu a zobrazim novy stav vahy
    ReadAndDisplaySelectedScale();

  } catch(...) {}
  Screen->Cursor = crDefault;

  // Start nacitani na pozadi
  Data->SetModbusPortParameters();      // Nastavim zpet na default parametry komunikace (Connect() zavola fce StartReading())
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Pause1Click(TObject *Sender)
{
  PauseWeighing(true);
}
//---------------------------------------------------------------------------


void __fastcall TModbusOnlineForm::Continue1Click(TObject *Sender)
{
  PauseWeighing(false);
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Stop1Click(TObject *Sender)
{
  // Zastavi vazeni
  TModbusOnlineScale *Scale;
  TModbusScaleResult  Result;

  // Nactu vahu vybranou v tabulce
  if ((Scale = GetSelectedScale()) == NULL) {
    return;     // Seznam je prazdny
  }

  if (!Scale->getLive()) {
    return;     // Vaha neni pripojena
  }

  if (!Scale->ScaleData.Config.WeighingStart.Running && !Scale->ScaleData.Config.WeighingStart.WaitingForStart) {
    return;     // Vaha nevazi ani neceka na start
  }

  // Zpetam se
  if (MessageBox(NULL, LoadStr(STR_RS485_STOP_WEIGHING).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
    return;
  }

  // Ukoncim nacitani na pozadi
  StopReading();

  Screen->Cursor = crHourGlass;
  try {
    if (!Scale->Connect()) {
      throw 1;
    }
    if (Scale->StopWeighingRep(Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }

    // Nactu a zobrazim novy stav vahy
    ReadAndDisplaySelectedScale();
  } catch(...) {
    MessageBox(NULL,LoadStr(CHYBA).c_str(), Caption.c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
  }
  Screen->Cursor = crDefault;

  // Start nacitani na pozadi
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Dateandtime1Click(TObject *Sender)
{
  // Nastaveni data a casu
  TModbusOnlineScale *Scale;
  TModbusScaleResult  Result;
  TLongDateTime       DateTime;
  unsigned short Day, Month, Year;
  unsigned short Hour, Min, Sec, Msec;

  // Nactu vahu vybranou v tabulce
  if ((Scale = GetSelectedScale()) == NULL) {
    return;     // Seznam je prazdny
  }

  if (!Scale->getLive()) {
    return;     // Vaha neni pripojena
  }

  if (Scale->ScaleData.Config.WeighingStart.Running || Scale->ScaleData.Config.WeighingStart.WaitingForStart) {
    return;     // Datum lze nastavit jen pri vypnutem vazeni
  }

  if (MessageBox(NULL, LoadStr(STR_RS485_SET_DATETIME).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
    return;
  }

  // Ukoncim nacitani na pozadi
  StopReading();

  Screen->Cursor = crHourGlass;
  try {
    if (!Scale->Connect()) {
      throw 1;
    }

    Date().DecodeDate(&Year, &Month, &Day);
    Time().DecodeTime(&Hour, &Min, &Sec, &Msec);
    DateTime.Day   = Day;
    DateTime.Month = Month;
    DateTime.Year  = Year;
    DateTime.Hour  = Hour;
    DateTime.Min   = Min;
    if (Scale->SetDateTimeRep(&DateTime, Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }

    // Nactu a zobrazim novy stav vahy
    ReadAndDisplaySelectedScale();
  } catch(...) {
    MessageBox(NULL,LoadStr(CHYBA).c_str(), Caption.c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
  }
  Screen->Cursor = crDefault;

  // Start nacitani na pozadi
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Downloadresults1Click(TObject *Sender)
{
  TModbusOnlineScale *Scale;
  TModbusScaleResult  Result;
  byte               *RawData = NULL;
  bool               OpenFileNow = false;

  // Nactu vahu vybranou v tabulce
  if ((Scale = GetSelectedScale()) == NULL) {
    return;     // Seznam je prazdny
  }

  if (!Scale->getLive()) {
    return;     // Vaha neni pripojena
  }

  if (Scale->ScaleData.Config.WeighingStart.WaitingForStart) {
    return;     // Vaha ceka na start, v archivu nic neni
  }

  if (Scale->ScaleData.Config.WeighingStart.Online) {
    return;     // V online rezimu je archiv prazdny
  }

  // Ukoncim nacitani na pozadi
  StopReading();

  Screen->Cursor = crDefault;           // Uzivatel muze klikat, vratim normalni kurzor
  Enabled = false;                      // Zakazu okno, aby nemohl odejit z ModbusWaitForm
  ModbusWaitForm->WaitLabel->Caption = LoadStr(STR_RS485_WAIT) + "...";
  ModbusWaitForm->Show();
  ModbusWaitForm->Refresh();

  // Zalozim novy thread prikazu a nastavim ho na vybranou vahu
  CreateCommandThread(Scale, Data->Nastaveni.Modbus.Repetitions);

  try {

    // Nactu z vahy vsechna data
    if (!Scale->Connect()) {
      throw 1;
    }

    // Nastaveni
    if (!ExecuteCommand(THREAD_COMMAND_READ_CONFIG)) {
      throw 1;
    }

    // Hejna
    if (!ExecuteCommand(THREAD_COMMAND_READ_FLOCKS)) {
      throw 1;
    }

    // Datum a cas
    if (!ExecuteCommand(THREAD_COMMAND_READ_DATETIME)) {
      throw 1;
    }

    // Archiv
    if (!ExecuteCommand(THREAD_COMMAND_READ_ARCHIVE)) {
      throw 1;
    }

    // Kalibrace
    if (!ExecuteCommand(THREAD_COMMAND_READ_CALIBRATION)) {
      throw 1;
    }

    // Rozhazu nactena data do formy odpovidajici pametovemu modulu
    RawData = new byte[FLASH_SIZE];        // Kapacita flash
    memset(RawData, 0, FLASH_SIZE);        // Radeji nuluju, pri prohazovani LSB/MSB u floatu vznikal NAN a hazelo to vyjimku
    Scale->BuildModuleData(RawData);       // Sestavi data do struktury odpovidajici pametovemu modulu

    // Ulozim
    FileSaved = true;           // Oznacim uz ted, musim zaznamenat zmenu tabulek
    Data->Module->LoadRawData(RawData);
    KonvertujData();
    FileCreateHeader();
    if (!FileSaveDataToNewFile()) {
      throw 1;
    }

    // Zeptam se, zda chce stahnuty soubor hned otevrit
    if (MessageBox(NULL, LoadStr(STR_RS485_OPEN_FILE).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDYES) {
      OpenFileNow = true;
    }
  } catch(...) {}

  // Smazu buffer (pokud uz byl vytvoren)
  if (RawData) {
    delete RawData;
  }

  // Ukoncim thread
  QuitCommandThread();

  ModbusWaitForm->Close();
  Enabled = true;               // Opet povolim okno
  SetFocus();                   // Jinak zustane okno neaktivni
  Screen->Cursor = crDefault;

  if (OpenFileNow) {
    // Chce otevrit soubor
    ModalResult = mrOk;
  }

  // Start nacitani na pozadi
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Setup1Click(TObject *Sender)
{
  // Nastaveni vah
  TModbusOnlineScale *Scale;
  TModbusScaleResult  Result;
  TScaleSetup *ScaleSetup = NULL;

  // Nactu vahu vybranou v tabulce
  if ((Scale = GetSelectedScale()) == NULL) {
    return;     // Seznam je prazdny
  }

  if (!Scale->getLive()) {
    return;     // Vaha neni pripojena
  }

  // Ukoncim nacitani na pozadi
  StopReading();

  bool ShowError = true;
  Screen->Cursor = crHourGlass;
  try {
    if (!Scale->Connect()) {
      throw 1;
    }

    // Nactu stavajici nastaveni
    if (Scale->ReadConfigRep(Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }
    if (Scale->ReadFlocksRep(Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }

    ScaleSetup = new TScaleSetup("RS-485", &Scale->ScaleData.Config, &Scale->ScaleData.Flocks[0]);
    TConfig Config;
    TFlocks Flocks;

    // Nactu nastaveni do okna
    bool Started = Scale->ScaleData.Config.WeighingStart.WaitingForStart || Scale->ScaleData.Config.WeighingStart.Running;
    int WeighedFlock;
    if (Started && Scale->ScaleData.Config.WeighingStart.UseFlock) {
      // Vazi podle hejna <Config.WeighingStart.CurrentFlock>, toto hejno nelze editovat
      WeighedFlock = Scale->ScaleData.Config.WeighingStart.CurrentFlock;
    } else {
      WeighedFlock = -1;
    }
    ModbusScaleSetupForm->ScaleSetupFrame1->SetEditMode(true, Started, WeighedFlock);
    ModbusScaleSetupForm->ScaleSetupFrame1->LoadSetup(ScaleSetup);
    Screen->Cursor = crDefault;
    if (ModbusScaleSetupForm->ShowModal() != mrOk) {
      ShowError = false;        // Nechci zobrazovat chybu
      throw 1;
    }
    Refresh();          // Jinak to vypada hnusne
    Screen->Cursor = crHourGlass;

    // Prevezmu zadane nastaveni
    ScaleSetup->SaveConfig(&Config);
    ScaleSetup->SaveFlocks(&Flocks[0]);

    // Ulozim rucne do configu verzi, tu ScaleSetup->SaveConfig() neobsahuje. Funkce Scale->WriteConfigRep se chova
    // jinak pro vahy verze 1.10 a jinak pro 1.11
    Config.Version = Scale->ScaleData.Config.Version;

    // Ulozim do vahy
    if (Scale->WriteConfigRep(&Config, Config.WeighingStart.WaitingForStart || Config.WeighingStart.Running,
                              Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }
    if (Scale->WriteFlocksRep(&Flocks[0], WeighedFlock, Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }

  } catch(...) {
    if (ShowError) {
      MessageBox(NULL,LoadStr(CHYBA).c_str(), Caption.c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    }  
  }
  if (ScaleSetup) {
    delete ScaleSetup;
  }
  Screen->Cursor = crDefault;
  // Start nacitani na pozadi
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Compare1Click(TObject *Sender)
{
  // Nastaveni porovnani
  char FlockName[FLOCK_NAME_MAX_LENGTH + 1];
  int FlockNumber[FLOCK_NAME_MAX_LENGTH + 1];  // Vcetne FLOCK_EMPTY_NUMBER na zacatku
  AnsiString Str;
  TModbusOnlineScale *Scale;
  TModbusScaleResult  Result;
  int CompareFlockIndex;

  // Nactu vahu vybranou v tabulce
  if ((Scale = GetSelectedScale()) == NULL) {
    return;     // Seznam je prazdny
  }

  if (!Scale->getLive()) {
    return;     // Vaha neni pripojena
  }

  // Ukoncim nacitani na pozadi
  StopReading();

  Screen->Cursor = crHourGlass;
  try {

    // Nactu z vahy aktualni nastaveni
    if (!Scale->Connect()) {
      throw 1;
    }
    if (Scale->ReadConfigRep(Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }
    if (Scale->ReadFlocksRep(Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      throw 1;
    }

    Screen->Cursor = crDefault;

    // Vyplnim hejna
    ModbusCompareForm->CompareComboBox->Clear();
    ModbusCompareForm->CompareComboBox->Items->Add(LoadStr(NONE));
    CompareFlockIndex = 0;      // Pokud se neporovnava, bude ukazovat na <none>
    FlockNumber[0] = FLOCK_EMPTY_NUMBER;
    for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
      if (Scale->ScaleData.Flocks[i].Header.Number == FLOCK_EMPTY_NUMBER) {
        continue;         // Hejno neni definovane
      }
      if (!Scale->ScaleData.Flocks[i].Header.UseCurves) {
        continue;         // Hejno nema krivky
      }
      for (int j = 0; j < FLOCK_NAME_MAX_LENGTH; j++) {
        FlockName[j] = Scale->ScaleData.Flocks[i].Header.Title[j];
      }
      FlockName[FLOCK_NAME_MAX_LENGTH] = 0;         // Zakoncim
      Str = AnsiString(i) + ": " + AnsiString(FlockName);
      ModbusCompareForm->CompareComboBox->Items->Add(Str);
      FlockNumber[ModbusCompareForm->CompareComboBox->Items->Count - 1] = i;         // Ulozim si cislo hejna
      if (Scale->ScaleData.Config.ComparisonFlock == i) {
        CompareFlockIndex = ModbusCompareForm->CompareComboBox->Items->Count - 1;    // Ulozim si pozici prave porovnavaneho hejna
      }
    }
    // Oznacim prave porovnavane hejno
    ModbusCompareForm->CompareComboBox->ItemIndex = CompareFlockIndex;

    // Zeptam se na provnani
    if (ModbusCompareForm->ShowModal() != mrOk) {
      throw 1;
    }
    Refresh();          // Jinak to vypada hnusne

    Screen->Cursor = crHourGlass;

    // Nastavim porovnani
    if (Scale->SetCompareRep(FlockNumber[ModbusCompareForm->CompareComboBox->ItemIndex],
        Data->Nastaveni.Modbus.Repetitions) != MB_RESULT_OK) {
      MessageBox(NULL,LoadStr(CHYBA).c_str(), Application->Title.c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
      throw 1;
    }

  } catch(...) {}
  Screen->Cursor = crDefault;

  // Start nacitani na pozadi
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::ScalesStringGridClick(TObject *Sender)
{
  // Aktivuju menu podle stavu vybrane vahy
  ActivateMenu(GetSelectedScale());
}
//---------------------------------------------------------------------------

void __fastcall TModbusOnlineForm::Communicationerrors1Click(
      TObject *Sender)
{
  // Zobrazim chyby prenosu
  TModbusScale *Scale;
  double Percentage;

  // Ukoncim nacitani na pozadi
  StopReading();

  try {

    // Nastavim spravny pocet radku
    if (ModbusOnline->ScalesCount() == 0) {
      ModbusErrorsForm->ScalesStringGrid->RowCount = 2;   // Minimalne 2 radky, aby zustala zachovana hlavicka tabulky
    } else {
      ModbusErrorsForm->ScalesStringGrid->RowCount = ModbusOnline->ScalesCount() + 1;
    }

    // Vyplnim tabulku
    for (int i = 0; i < ModbusOnline->ScalesCount(); i++) {
      if ((Scale = ModbusOnline->GetScale(i)) == NULL) {
        break;
      }
      ModbusErrorsForm->ScalesStringGrid->Cells[0][i + 1] = GetScaleName(Scale);
      if (Scale->GetMessageCounter() == 0) {
        Percentage = 100;
      } else {
        Percentage = 100.0 * (double)Scale->GetErrorCounter() / (double)Scale->GetMessageCounter();
        if (Percentage > 100) {
          Percentage = 100;
        }
      }
      ModbusErrorsForm->ScalesStringGrid->Cells[1][i + 1] = FormatFloat("0.00", Percentage) + "%  ("
                                                          + AnsiString(Scale->GetErrorCounter()) + " / "
                                                          + AnsiString(Scale->GetMessageCounter()) + ")";
    }

    ModbusErrorsForm->ShowModal();
  } catch(...) {}

  // Start nacitani na pozadi
  ModbusOnline->InitExecute(Data->Nastaveni.Modbus.Repetitions);
  StartReading();
}
//---------------------------------------------------------------------------


