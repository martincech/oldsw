//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Weighing.h"
#include "DM.h"
#include "WeighingRecord.h"
#include "FindFile.h"
#include "WeighingOverwrite.h"
#include "ReportReport.h"
#include "GrafReport.h"
#include "Hlavni.h"
#include "WeighingFilters.h"
#include "GSM.h"
#include "SelectModem.h"
#include "Number.h"
#include "StatisticsDay.h"
#include "GsmLog.h"
#include "Print.h"
#include "Curves.h"
#include "CheckId.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TWeighingForm *WeighingForm;
//---------------------------------------------------------------------------


// Stringy v log souboru:

const String LOG_START_REQUEST     = "START REQUEST";
const String LOG_START_READ        = "START READ";
const String LOG_CONNECT_ERROR     = "CONNECT ERROR";
const String LOG_CONNECT_OK        = "CONNECT OK";
const String LOG_RESET_ERROR       = "RESET ERROR";
const String LOG_RESET_OK          = "RESET OK";
const String LOG_CHECK_ERROR       = "CHECK ERROR";
const String LOG_CHECK_OK          = "CHECK OK";
const String LOG_REGISTER_ERROR    = "REGISTER ERROR";
const String LOG_REGISTER_OK       = "REGISTER OK";
const String LOG_SIGNAL            = "SIGNAL";
const String LOG_SEND_ERROR        = "SEND ERROR";
const String LOG_SEND_OK           = "SEND OK";
const String LOG_READ_ERROR        = "READ ERROR";
const String LOG_READ_OK           = "READ OK";
const String LOG_DECODE_ERROR      = "DECODE ERROR";
const String LOG_DECODE_OK         = "DECODE OK";
const String LOG_DELETE_ERROR      = "DELETE ERROR";
const String LOG_DELETE_OK         = "DELETE OK";

//---------------------------------------------------------------------------
// Zapis do logu
//---------------------------------------------------------------------------

static void WriteLogSms(String Prefix, String Number, String Message) {
  // Zapise do logu posilani nabo prijem SMS
  GsmLogWriteLine(Prefix + " " + Number + " <" + Message + ">");
}

//---------------------------------------------------------------------------
// Vyber modemu
//---------------------------------------------------------------------------

bool TWeighingForm::SelectModem() {
  // Pokud zadal port modemu rucne v INI souboru, pouziju natvrdo zadany port
  if (Data->GsmModemPortNumber > 0) {
    ReadStatus.PortName = "COM" + String(Data->GsmModemPortNumber);
    return true;
  }

  // Cislo portu neni zadane v INI, automaticky detekuji a necham uzivatele vybrat ze seznamu

  // Nactu seznam modemu
  Screen->Cursor = crHourGlass;
  try {
    AutoDetectModem(SelectModemForm->ModemListBox);
  } catch(...) {}
  Screen->Cursor = crDefault;
  // Default vyberu posledne pouzity modem
  int Index = SelectModemForm->ModemListBox->Items->IndexOf(Data->Nastaveni.ModemName);
  if (Index >= 0) {
    SelectModemForm->ModemListBox->ItemIndex = Index;   // Vyberu naposledy pouzity modem
  } else {
    SelectModemForm->ModemListBox->ItemIndex = 0;       // Vyberu prvni
  }
  // Vyber modemu
  if (SelectModemForm->ShowModal() != mrOk) {
    return false;
  }
  // Ulozim vybrany modem jako posledne pouzity
  Data->NastaveniTable->Open();
  Data->NastaveniTable->Edit();
  Data->NastaveniTable->FieldByName("MODEM_NAME")->AsString = SelectModemForm->ModemListBox->Items->Strings[SelectModemForm->ModemListBox->ItemIndex];
  Data->Nastaveni.ModemName = Data->NastaveniTable->FieldByName("MODEM_NAME")->AsString;        // ZAroven ulozim i do lok. promenne, fce NactiNAstaveni() je nedostupna
  Data->NastaveniTable->Post();
  Data->NastaveniTable->Close();
  // Uzloim nazev portu
  ReadStatus.PortName = Data->Nastaveni.ModemName.SubString(1, Data->Nastaveni.ModemName.Pos(":") - 1);         // Zkopiruju "COMx"
  return true;
}

//---------------------------------------------------------------------------
// Nacteni sily signalu
//---------------------------------------------------------------------------

void TWeighingForm::ReadSignalStrength() {
  // Nacte silu signalu
  int SignalStrength;

  GsmModem->SignalStrength(SignalStrength);      // Returns relative signal strength 10..31 or 99 if unknown
  if (SignalStrength == 99) {
    ReadStatus.Signal = 0;  // Neznama sila signalu
  } else {
    ReadStatus.Signal = 100 * (SignalStrength - 10) / 21;   // Prevedu 10..31 na 0..100%
    if (ReadStatus.Signal > 100) {
      ReadStatus.Signal = 100;      // Radeji omezim na max. 100%
    }
    if (ReadStatus.Signal < 0) {
      ReadStatus.Signal = 0;        // Radeji omezim na min. 0% (pokud se modem vytahne s USB, ukazuje -47%)
    }

  }//else
}

//---------------------------------------------------------------------------
// Autodetekce GSM modemu na virtualnim COM portu
//---------------------------------------------------------------------------

#define MAX_COM_NUMBER 50       // 17.9.2008: Zmena ze 20 na 50

void TWeighingForm::AutoDetectModem(TListBox *ListBox) {
  // Detekuje modemy na jednotlivych portech a vypise je do seznamu
  ListBox->Clear();
  GsmModem->Disconnect(); // disconnect active device (unable to detect connected devices)
  String ComName;
  String DeviceModel;
  for( int i = 1; i <= MAX_COM_NUMBER; i++){
    ComName = "COM" + String(i);
    if( !TGsmModem::Detect( ComName, DeviceModel)){
      continue;
    }
    ListBox->Items->Add(ComName + ":  " + DeviceModel);
  }
}

//---------------------------------------------------------------------------
// Automaticke nacitani SMS na pozadi
//---------------------------------------------------------------------------

#define READING_SPACE "       "

void TWeighingForm::IncreaseAddress() {
  // Posune se na dalsi adresu
  ReadStatus.Address++;           // Adresa pro pristi cteni
  if (ReadStatus.Address >= GsmModem->MemorySize) {
    ReadStatus.Address = 0;       // Preskocim zpet na 1. pozici
  }
}


void TWeighingForm::StringConnecting(int Phase) {
  StatusLabel->Caption = LoadStr(SMS_CONNECTING) + " (" + String(Phase) + "/4)";
  StatusLabel->Refresh();
}

void TWeighingForm::SetReadControls(bool Running) {
  // Zobrazi nebo skryje ikony a menu podle toho, zda je odstartovane nacitani
  // Ikony
  StartToolButton->Enabled   = !Running;
  StopToolButton->Enabled    = Running;
  RequestToolButton->Enabled = !Running;
  NewToolButton->Enabled     = !Running;
  EditToolButton->Enabled    = !Running;
  DeleteToolButton->Enabled  = !Running;
  FindToolButton->Enabled    = !Running;
  PrintToolButton->Enabled   = !Running;
  ExcelToolButton->Enabled   = !Running;
  // Menu
  FileMenu->Enabled         = !Running;
  StartReadingMenu->Enabled = !Running;
  StopReadingMenu->Enabled  = Running;
  RequestMenu->Enabled      = !Running;
  RecordsMenu->Enabled      = !Running;
  // Controls
  FilterPanel->Enabled = !Running;
  PageControl->Enabled = !Running;
  // Status
  StatusLabel->Visible = Running;
  StatusBevel->Visible = Running;
}

void TWeighingForm::StartReadingSms(bool Request) {
  GsmLogStart();        // Start logovani
  if (Request) {
    GsmLogWriteLine(LOG_START_REQUEST);
    ReadStatus.Mode        = MODE_REQUEST;
  } else {
    GsmLogWriteLine(LOG_START_READ);
    ReadStatus.Mode        = MODE_READING;
  }
  ReadStatus.SmsCount     = 0;   // Zatim 0 nactenych
  ReadStatus.ModemStatus  = MODEM_INIT;
  ReadStatus.Signal       = 0;
  ReadStatus.Operator     = "";
  ReadStatus.ErrorCounter = 0;  // Nuluju pocitadlo
  ReadTimer->Enabled  = true;   // Rozjedu nacitani na pozadi
  PageControl->ActivePage = TableTabSheet;
  RecordsDBGrid->SetFocus();
  ReadAll();                    // Zrusi vsechny filtry
  SetReadControls(true);
  StatusLabel->Caption = LoadStr(SMS_CONNECTING);
  Screen->Cursor = crAppStart;
}

void TWeighingForm::StopReadingSms() {
  GsmLogStop();         // Ukoncim logovani
  ReadStatus.Mode     = MODE_IDLE;
  ReadTimer->Enabled  = false;
  SetReadControls(false);
  ReadAll();
  PageControl->ActivePage = TableTabSheet;
  RecordsDBGrid->SetFocus();
  Screen->Cursor = crDefault;
}


//---------------------------------------------------------------------------
// Kontrola existence zaznamu, ktery je zadany v WeighingRecordForm
//---------------------------------------------------------------------------

bool TWeighingForm::CanSaveRecord() {
  // Pokud zaznam zadany v WeighingRecordForm existuje, zepta se na prepis a pripadne smaze stary zaznam
  // Pokud je mozne zaznam ulozit, vrati true. Pokud uz existuje a nechce prepsat, vrati false.

  // Podivam se, zda uz zaznam se zadanymi parametry neexistuje
  if (RecordExists(GetIdWithoutSpaces(WeighingRecordForm->ScalesEdit->Text), WeighingRecordForm->DayEdit->Text.ToInt(), WeighingRecordForm->DateTimePicker->Date)) {
    // Zaznam jiz existuje, zeptam se, zda ho chce prepsat. Stary zaznam je jiz vyplnen z fce RecordExists()
    // Vyplnim novy zaznam
    // Samice
    WeighingOverwriteForm->NewFemaleCountLabel->Caption   = WeighingRecordForm->FemalesCountEdit->Text;
    WeighingOverwriteForm->NewFemaleAverageLabel->Caption = WeighingRecordForm->FemalesAverageEdit->Text;
    WeighingOverwriteForm->NewFemaleGainLabel->Caption    = WeighingRecordForm->FemalesGainEdit->Text;
    WeighingOverwriteForm->NewFemaleSigmaLabel->Caption   = WeighingRecordForm->FemalesSigmaEdit->Text;
    WeighingOverwriteForm->NewFemaleCvLabel->Caption      = WeighingRecordForm->FemalesCvEdit->Text;
    WeighingOverwriteForm->NewFemaleUniLabel->Caption     = WeighingRecordForm->FemalesUniEdit->Text;
    // Samci
    if (WeighingRecordForm->BothGendersCheckBox->Checked) {      // Jsou obe pohlavi
      // Samci jsou zadani
      WeighingOverwriteForm->NewMaleCountLabel->Caption   = WeighingRecordForm->MalesCountEdit->Text;
      WeighingOverwriteForm->NewMaleAverageLabel->Caption = WeighingRecordForm->MalesAverageEdit->Text;
      WeighingOverwriteForm->NewMaleGainLabel->Caption    = WeighingRecordForm->MalesGainEdit->Text;
      WeighingOverwriteForm->NewMaleSigmaLabel->Caption   = WeighingRecordForm->MalesSigmaEdit->Text;
      WeighingOverwriteForm->NewMaleCvLabel->Caption      = WeighingRecordForm->MalesCvEdit->Text;
      WeighingOverwriteForm->NewMaleUniLabel->Caption     = WeighingRecordForm->MalesUniEdit->Text;
    } else {
      // Samci se nepouzivaji
      WeighingOverwriteForm->NewMaleCountLabel->Caption   = "";
      WeighingOverwriteForm->NewMaleAverageLabel->Caption = "";
      WeighingOverwriteForm->NewMaleGainLabel->Caption    = "";
      WeighingOverwriteForm->NewMaleSigmaLabel->Caption   = "";
      WeighingOverwriteForm->NewMaleCvLabel->Caption      = "";
      WeighingOverwriteForm->NewMaleUniLabel->Caption     = "";
    }
    if (WeighingOverwriteForm->ShowModal() != mrOk) {
      return false;   // Nechce prepsat
    }
    // Smazu stary zaznam
    DeleteRecord(GetIdWithoutSpaces(WeighingRecordForm->ScalesEdit->Text), WeighingRecordForm->DayEdit->Text.ToInt(), WeighingRecordForm->DateTimePicker->Date);
  }
  return true;
}

//---------------------------------------------------------------------------
// Ulozeni zaznamu zadaneho v dialogu
//---------------------------------------------------------------------------

void TWeighingForm::InsertNewRecord(TDateTime InsertDate, String GsmNumber) {
  // Ulozi zaznam zadany v WeighingRecordForm do tabulky jako novy zaznam (tj. pokud uz existuje, dotaze se na prepis)

  // Podivam se, zda uz zaznam se zadanymi parametry neexistuje
  if (!CanSaveRecord()) {
    return;     // Uz existuje a nechce prepsat
  }

  // Ulozim zaznam
  try {
    Data->SmsTable->Open();
    Data->SmsTable->Append();
    Data->SmsTable->Edit();
    if (!GsmNumber.IsEmpty()) {
      Data->SmsTable->FieldByName("GSM_NUMBER")->AsString = GsmNumber;
    }
    Data->SmsTable->FieldByName("INSERT_DATE_TIME")->AsDateTime = InsertDate;
    Data->SmsTable->FieldByName("ID")->AsString                 = GetIdWithoutSpaces(WeighingRecordForm->ScalesEdit->Text);
    Data->SmsTable->FieldByName("DAY_NUMBER")->AsInteger        = WeighingRecordForm->DayEdit->Text.ToInt();
    Data->SmsTable->FieldByName("DAY_DATE")->AsDateTime         = WeighingRecordForm->DateTimePicker->Date;
    Data->SmsTable->FieldByName("NOTE")->AsString               = WeighingRecordForm->NoteEdit->Text;
    Data->SmsTable->FieldByName("FEMALE_COUNT")->AsInteger = WeighingRecordForm->FemalesCountEdit->Text.ToInt();
    Data->SmsTable->FieldByName("FEMALE_AVERAGE")->AsFloat = WeighingRecordForm->FemalesAverageEdit->Text.ToDouble();
    Data->SmsTable->FieldByName("FEMALE_GAIN")->AsFloat    = WeighingRecordForm->FemalesGainEdit->Text.ToDouble();
    Data->SmsTable->FieldByName("FEMALE_SIGMA")->AsFloat   = WeighingRecordForm->FemalesSigmaEdit->Text.ToDouble();
    Data->SmsTable->FieldByName("FEMALE_CV")->AsInteger    = WeighingRecordForm->FemalesCvEdit->Text.ToInt();
    Data->SmsTable->FieldByName("FEMALE_UNI")->AsInteger   = WeighingRecordForm->FemalesUniEdit->Text.ToInt();
    if (WeighingRecordForm->BothGendersCheckBox->Checked) {
      Data->SmsTable->FieldByName("MALE_COUNT")->AsInteger = WeighingRecordForm->MalesCountEdit->Text.ToInt();
      Data->SmsTable->FieldByName("MALE_AVERAGE")->AsFloat = WeighingRecordForm->MalesAverageEdit->Text.ToDouble();
      Data->SmsTable->FieldByName("MALE_GAIN")->AsFloat    = WeighingRecordForm->MalesGainEdit->Text.ToDouble();
      Data->SmsTable->FieldByName("MALE_SIGMA")->AsFloat   = WeighingRecordForm->MalesSigmaEdit->Text.ToDouble();
      Data->SmsTable->FieldByName("MALE_CV")->AsInteger    = WeighingRecordForm->MalesCvEdit->Text.ToInt();
      Data->SmsTable->FieldByName("MALE_UNI")->AsInteger   = WeighingRecordForm->MalesUniEdit->Text.ToInt();
    }
    Data->SmsTable->Post();
  } catch(...) {}
  Data->SmsTable->Close();
}

//---------------------------------------------------------------------------
// Zobrazeni rozkodovane SMS v dialogu
//---------------------------------------------------------------------------

bool TWeighingForm::EditDecodedSms(TDecodedSms *Sms, TDateTime InsertDate, bool ShowDialog) {
  // Zobrazi dekodovanou SMS v dialogu (jakoby zadaval rucne novy zaznam) a pokud klepne na OK, vrati true
  // Pokud je <ShowDialog> false, pouze ulozi hodnoty do okna a nepta se na editaci

  // Vyplnim info o zaznamu
  WeighingRecordForm->InsertLabel->Caption = InsertDate;
  WeighingRecordForm->EditLabel->Caption   = "";          // U noveho zaznamu editace nemuze byt
  WeighingRecordForm->GSMLabel->Caption    = Sms->Number; // Rucne pridany zaznam nema GSM cislo

  // Prevyplnim nejake udaje
  WeighingRecordForm->ScalesEdit->Text     = Sms->Id;
  WeighingRecordForm->DayEdit->Text        = String(Sms->Day);
  WeighingRecordForm->DateTimePicker->Date = Sms->Date;
  WeighingRecordForm->NoteEdit->Text           = "";

  WeighingRecordForm->FemalesCountEdit->Text   = String(Sms->FemaleStat.Count);
  WeighingRecordForm->FemalesAverageEdit->Text = String(Sms->FemaleStat.Average);
  WeighingRecordForm->FemalesGainEdit->Text    = String(Sms->FemaleStat.Gain);
  WeighingRecordForm->FemalesSigmaEdit->Text   = String(Sms->FemaleStat.Sigma);
  WeighingRecordForm->FemalesCvEdit->Text      = String(Sms->FemaleStat.Cv);
  WeighingRecordForm->FemalesUniEdit->Text     = String(Sms->FemaleStat.Uni);

  WeighingRecordForm->BothGendersCheckBox->Checked = Sms->UseGender;
  if (WeighingRecordForm->BothGendersCheckBox->Checked) {
    WeighingRecordForm->MalesCountEdit->Text   = String(Sms->MaleStat.Count);
    WeighingRecordForm->MalesAverageEdit->Text = String(Sms->MaleStat.Average);
    WeighingRecordForm->MalesGainEdit->Text    = String(Sms->MaleStat.Gain);
    WeighingRecordForm->MalesSigmaEdit->Text   = String(Sms->MaleStat.Sigma);
    WeighingRecordForm->MalesCvEdit->Text      = String(Sms->MaleStat.Cv);
    WeighingRecordForm->MalesUniEdit->Text     = String(Sms->MaleStat.Uni);
  } else {
    WeighingRecordForm->MalesCountEdit->Text   = "";
    WeighingRecordForm->MalesAverageEdit->Text = "";
    WeighingRecordForm->MalesGainEdit->Text    = "";
    WeighingRecordForm->MalesSigmaEdit->Text   = "";
    WeighingRecordForm->MalesCvEdit->Text      = "";
    WeighingRecordForm->MalesUniEdit->Text     = "";
  }  

  if (!ShowDialog) {
    return true;        // Nechce zobrazovat dialog
  }

  return (bool)(WeighingRecordForm->ShowModal() == mrOk);
}


//---------------------------------------------------------------------------
// Rozkodovani textu SMS
//---------------------------------------------------------------------------

static void DeleteWord(String *Text, String *Word) {
  // Vymaze z <Text> prvni slovo zakoncene mezerou a vrati ho v <Word>.
  // Pokud se narazi driv na konec stringu nez na mezeru, hodi vyjimku
  int SpaceIndex;       // Index nalezene mezery

  SpaceIndex = Text->Pos(" ");  // Najdu pozici prvni mezery zleva

  if (SpaceIndex == 0) {
    *Word = *Text;      // String uz obsahuje pouze 1 slovo bez mezery (zakoncene koncem stringu)
    throw 1;            // Narazil jsem na konec stringu
  }
  *Word = Text->SubString(1, SpaceIndex - 1);   // Slovo bez mezery
  Text->Delete(1, SpaceIndex);                  // Umazu slovo i s mezerou
}

static double ConvertToDouble(String Text) {
  // Prevede cislo z SMS na double (v SMS je pouzita desetinna tecka). Pokud ma text neplatny format, hodi vyjimku.
  int i;
  if ((i = Text.Pos(".")) > 0) {
    Text[i] = DecimalSeparator; // Tecku nahradim spravnym oddelovacem
  }
  return Text.ToDouble();       // Pri neplatnem formatu hodi vyjimku
}

static void DecodeStat(String *Text, TStatSms *Stat) {
  // Dekoduje statistiku z textu, ktery musi zacinat poctem kusu (tj. uz bez "Cnt"). Pri neplatnem formatu hodi vyjimku.
  String Str;

  // Cnt (slovo "cnt" uz ve stringu neni)
  DeleteWord(Text, &Str);
  Stat->Count = Str.ToInt();
  // Avg
  DeleteWord(Text, &Str);
  DeleteWord(Text, &Str);
  Stat->Average = ConvertToDouble(Str);
  // Gain
  DeleteWord(Text, &Str);
  DeleteWord(Text, &Str);
  Stat->Gain = ConvertToDouble(Str);
  // Sig
  DeleteWord(Text, &Str);
  DeleteWord(Text, &Str);
  Stat->Sigma = ConvertToDouble(Str);
  // Cv
  DeleteWord(Text, &Str);
  DeleteWord(Text, &Str);
  Stat->Cv = Str.ToInt();
  // Uni
  DeleteWord(Text, &Str);
  DeleteWord(Text, &Str);
  Stat->Uni = Str.ToInt();
}

bool TWeighingForm::DecodeSms(String Number, String Text, TDecodedSms *Sms) {
  // Dekoduje SMS s textem <Text> prijatou z cisla <Number> a ulozi obsah do <Sms>
  // Pri uspesne konverzi vrati true
  // Format textu SMS:
  // SCALES 00 DAY 000 01.01.2005 FEMALES: Cnt 0000 Avg 00.000 Gain 00.000 Sig 0.000 Cv 000 Uni 000 MALES: Cnt 0000 Avg 00.000 Gain 00.000 Sig 0.000 Cv 000 Uni 000
  String Str;
  int Position;
  int Day, Month, Year;                 // Pro dekodovani datumu

  try {
    Sms->Number = Number;
    DeleteWord(&Text, &Str);            // Smazu SCALES

    DeleteWord(&Text, &Str);            // Cislo vah
    Sms->Id = Str;

    DeleteWord(&Text, &Str);            // Smazu DAY

    DeleteWord(&Text, &Str);            // Cislo dne
    Sms->Day = Str.ToInt();

    DeleteWord(&Text, &Str);            // Datum dne
    // Den
    if ((Position = Str.Pos(".")) == 0) {
      throw 1;          // Tecka za dnem chybi
    }
    Day = Str.SubString(1, Position - 1).ToInt();
    Str.Delete(1, Position);    // Smazu i s teckou
    // Mesic
    if ((Position = Str.Pos(".")) == 0) {
      throw 1;          // Tecka za mesicem chybi
    }
    Month = Str.SubString(1, Position - 1).ToInt();
    Str.Delete(1, Position);    // Smazu i s teckou
    // Rok
    Year = Str.ToInt();
    Sms->Date = TDateTime(Year, Month, Day);

    DeleteWord(&Text, &Str);            // Smazu FEMALES: nebo Cnt, podle toho, zda pouziva 2 pohlavi
    Sms->UseGender = !isdigit(Text[1]); // Pokud je prvni znak cislo, pouziva jen 1 pohlavi (je zde rovnou pocet).
    if (Sms->UseGender) {
      DeleteWord(&Text, &Str);          // Smazu Cnt
    }
    DecodeStat(&Text, &Sms->FemaleStat);  // Dekoduju statistiku pro samice nebo pro vsechny

    if (Sms->UseGender) {
      DeleteWord(&Text, &Str);          // Smazu MALES:
      DeleteWord(&Text, &Str);          // Smazu Cnt
      DecodeStat(&Text, &Sms->MaleStat);  // Dekoduju statistiku pro samce
    }
  } catch(...) {
    return false;       // Neco se nepovedlo (neplatny format)
  }
  return true;
}

//---------------------------------------------------------------------------
// Nastaveni filtru
//---------------------------------------------------------------------------

void TWeighingForm::SetFilter(String Filter) {
  // Nastavi zvoleny filtr
  Data->FiltersTable->Open();
  try {
    TLocateOptions LocateOptions;
    LocateOptions.Clear();
    LocateOptions << loCaseInsensitive;
    if (!Data->FiltersTable->Locate("NAME", Filter, LocateOptions)) {
      throw 1;     // Nenasel jsem
    }
    // Nastavim filtr
    // Cislo vahy
    if (!Data->FiltersTable->FieldByName("ID")->IsNull) {
      // Je zadane cislo vahy
      int Index = ScaleComboBox->Items->IndexOf(Data->FiltersTable->FieldByName("ID")->AsString);
      if (Index >= 0) {
        ScaleComboBox->ItemIndex = Index;       // Cislo vahy ve filtru je i v seznamu vah => nastavim vahu
      } else {
        ScaleComboBox->ItemIndex = 0;           // Cislo vahy ve filtru neni obsazene v tabulce => nastavim all
      }
    }//else cislo vahy nemenim
    // Datumy
    if (!Data->FiltersTable->FieldByName("DATE_FROM")->IsNull) {
      FromDateTimePicker->Date = Data->FiltersTable->FieldByName("DATE_FROM")->AsDateTime;
    }//else datum nemenim
    if (!Data->FiltersTable->FieldByName("DATE_TILL")->IsNull) {
      TillDateTimePicker->Date = Data->FiltersTable->FieldByName("DATE_TILL")->AsDateTime;
    }//else datum nemenim
  } catch(...) {}
  Data->FiltersTable->Close();
}

//---------------------------------------------------------------------------
// Export do Excelu
//---------------------------------------------------------------------------

#define EXCEL_REPORT_RADEK1  1          // Na kterem radku zacinaji zaznamy

void TWeighingForm::ExcelExportReport(TQuery *Query) {
  // Exportuje report v <Query> do Excelu
  int PocetZaznamu;
  String Str;
  double CompareWeight;

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Vyplnim hlavicku
/*  Data->Excel->PutStr(1, 1, HlavniForm->ReportTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, CreateStringReportFile());
  Data->Excel->PutStr(1, 3, LoadStr(POHLAVI));
  Data->Excel->PutStr(2, 3, CreateStringReportGender(PohlaviSamiceRadioButton->Checked));*/
  // Hlavicky sloupcu
  int Column = 1;
  for (int i = 0; i < RecordsDBGrid->Columns->Count; i++) {
    if (!RecordsDBGrid->Columns->Items[i]->Visible) {
      continue;                 // Sloupec je schovany
    }
    Data->Excel->PutStr(Column++, EXCEL_REPORT_RADEK1, RecordsDBGrid->Columns->Items[i]->Title->Caption);
  }
  // Vyplnim hodnoty
  Query->First();
  PocetZaznamu = 0;
  while (!Query->Eof) {
    Data->Excel->PutStr(  1,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("DAY_DATE")->AsDateTime.DateString());
    Data->Excel->PutStr(  2,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("ID")->AsString);
    Data->Excel->PutInt(  3,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("DAY_NUMBER")->AsInteger);
    Data->Excel->PutInt(  4,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("COUNT_HEADS")->AsInteger);
    Data->Excel->PutFloat(5,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("AVERAGE_WEIGHT")->AsFloat);
    Data->Excel->PutFloat(6,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("GAIN")->AsFloat);
    Data->Excel->PutFloat(7,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("SIGMA")->AsFloat);
    Data->Excel->PutInt(  8,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("CV")->AsInteger);
    Data->Excel->PutInt(  9,  EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("UNI")->AsInteger);

    // Porovnani s teoretickou rustovou krivkou
    if (Data->GetCompareCurve(COMPARE_CURVE_WEIGHING_FORM)) {
      // Prave se porovnava
      CompareWeight = FormatFloat(FORMAT_HMOTNOSTI, Data->GetCompareCurve(COMPARE_CURVE_WEIGHING_FORM)->CalculateWeight(Query->FieldByName("DAY_NUMBER")->AsInteger)).ToDouble();
      Data->Excel->PutFloat(10, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, CompareWeight);
      Data->Excel->PutFloat(11, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("AVERAGE_WEIGHT")->AsFloat
                                                                      - CompareWeight);
      // Pokud se porovnava, poznamka je o 2 sloupce dal
      Data->Excel->PutStr(12, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("NOTE")->AsString);
    } else {
      // Neporovnava se, poznamka je hned za uniformitou
      Data->Excel->PutStr(10, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("NOTE")->AsString);
    }

    Query->Next();
    PocetZaznamu++;
  }//while
}

//---------------------------------------------------------------------------
// Vyplneni hlavicky na report
//---------------------------------------------------------------------------

String TWeighingForm::CreateStringReportFile() {
  return WeighingForm->Caption + " (" + ScaleTitleLabel->Caption + " " + ScaleComboBox->Text + ", "
         + FromDateTimePicker->Date.DateString()
         + " - " + TillDateTimePicker->Date.DateString() + ")";   // Jako soubor dam "Weighing database" s datumem
}

String TWeighingForm::CreateStringReportGender(bool FemaleChecked) {
  if (FemaleChecked) {
    return LoadStr(SAMICE) + " ( + " + LoadStr(SAMEC) + ")";
  } else {
    return LoadStr(SAMEC);
  }
}

void TWeighingForm::CreateReportHeader(TQRLabel *FileLabel, TQRLabel *GenderTitleLabel, TQRLabel *GenderLabel) {
  // Vyplni soubor a pohlavi v reportu <Report>
  FileLabel->Caption = CreateStringReportFile();
  // Pohlavi
  GenderTitleLabel->Enabled = true;      // Tady nerozpoznam, zda je to s 2 pohlavimi => vzdy zobrazuju
  GenderLabel->Enabled      = true;
  GenderLabel->Caption = CreateStringReportGender(PohlaviSamiceRadioButton->Checked);
}

//---------------------------------------------------------------------------
// Vykresleni grafu
//---------------------------------------------------------------------------

void TWeighingForm::DrawGraph(TChart *Chart) {
  // Vykresli zvoleny typ grafu do <Chart> za zvolene casove obdobi a zvolenou vahu
  String Column;        // Sloupec v SQL, ktery se bude vykreslovat
  int LastDay;          // Cislo dne v predchozim kroku
  TDate StartDate = NULL, StopDate = NULL;
  TGrowthCurve *Curve;

  // Vymazu vsechna data z grafu
  for (int i = 0; i < GraphChart->SeriesCount(); i++) {
    GraphChart->Series[i]->Clear();
  }
  GraphChart->LeftAxis->Title->Caption = "";
  GraphTimeRangeLabel->Caption = "";
  PrintGraphMenu->Enabled = false;      // Default zakazu menu pro tisk grafu
  CompareGraphLabel->Visible = false;   // Default schovam legendu - pokud bych nezobrazoval graf, zustala by zobrazena

  // Graf muzu vykreslit jen pokud je v tabulce zobrazena jen 1 vaha
  if (ScaleComboBox->ItemIndex == 0) {
    return;     // Je tam vic vah, graf necham prazdny
  }

  PrintGraphMenu->Enabled = true;      // Povolim menu pro tisk grafu

  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    // Vyberu zaznamy podle zadanych podminek
    CreateSQLRecords(Query);
    Query->Open();
    // Podle vybraneho typu grafu zvolim sloupce v SQL, ktery budu vykreslovat, zaroven i dilek osy Y grafu
    switch (GraphTypeComboBox->ItemIndex) {
      case 0:  Column = "COUNT_HEADS";    GraphChart->LeftAxis->Increment = 1;     break;
      case 1:  Column = "AVERAGE_WEIGHT"; GraphChart->LeftAxis->Increment = 0.001; break;
      case 2:  Column = "GAIN";           GraphChart->LeftAxis->Increment = 0.001; break;
      case 3:  Column = "SIGMA";          GraphChart->LeftAxis->Increment = 0.001; break;
      case 4:  Column = "CV";             GraphChart->LeftAxis->Increment = 1;     break;
      default: Column = "UNI";            GraphChart->LeftAxis->Increment = 1;
    }//switch
    // Nastavim nazev osy Y
    GraphChart->LeftAxis->Title->Caption = GraphTypeComboBox->Text;
    // Vykreslim pozadovany graf - skoncim bud az na konci Query (tj. podle casove podminky), nebo kdyz dojedu na den mimo
    // posloupnost (mensi den nez predchozi)
    Query->First();
    LastDay = -1;
    if (!Query->Eof) {
      StartDate = Query->FieldByName("DAY_DATE")->AsDateTime;
    }
    while (!Query->Eof) {
      if (Query->FieldByName("DAY_NUMBER")->AsInteger <= LastDay) {
        break;  // Konci posloupnost dnu, i kdyz jsem jeste nedorazil na konec casoveho vyberu
      }
      Chart->Series[CURVE_REAL]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                       Query->FieldByName(Column)->AsFloat,
                                       "",   // Popisek
                                       clTeeColor);
      LastDay = Query->FieldByName("DAY_NUMBER")->AsInteger;    // Zapamatuju si den posleniho zaznamu
      StopDate = Query->FieldByName("DAY_DATE")->AsDateTime;    // Datum posledniho zaznamu
      Query->Next();
    }//while
    Chart->UndoZoom();  // Osy na default
    // Zobrazim casovy usek v grafu (nemusi odpovidat zadanemu casovemu filtru)
    if ((int)StartDate != NULL) {
      GraphTimeRangeLabel->Caption = StartDate.DateString() + " - " + StopDate.DateString();
    }  
  } catch(...) {}
  delete Query;

  // Porovnani
  Curve = Data->GetCompareCurve(COMPARE_CURVE_WEIGHING_FORM);
  if (Curve && GraphTypeComboBox->ItemIndex == 1) {
    // Chce porovnavat a zrovna je zobrazena rustova krivka => vykreslim i porovnani
    Chart->Series[CURVE_COMPARE]->Active = true;
    Chart->Series[CURVE_COMPARE]->Title  = CompareComboBox->Text;

    // Vynesu krivku do grafu
    Chart->Series[CURVE_COMPARE]->Clear();      // Vymazu z grafu vsechny predchozi hodnoty
    for (int i = 0; i < Curve->Count(); i++) {
      Chart->Series[CURVE_COMPARE]->AddXY(Curve->GetDay(i),
                                          Curve->GetWeight(i),
                                          "",   // Popisek
                                          clTeeColor);
    }

    // Zobrazim legendu
    CompareGraphLabel->Caption = CompareComboBox->Text;
    CompareGraphLabel->Visible = true;
  } else {
    // Schovam prubeh v grafu
    Chart->Series[CURVE_COMPARE]->Active = false;
    CompareGraphLabel->Visible = false;         // Schovam legendu
  }
}

//---------------------------------------------------------------------------
// Hledani a mazani zaznamu
//---------------------------------------------------------------------------

bool TWeighingForm::RecordExists(AnsiString Scales, int Day, TDate Date) {
  // Pokud zaznam s danymi parametry existuje, vrati true a zaroven vyplni udaje stareho zaznamu do WeighingOverwriteForm
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  bool Exists;

  Query->SQL->Add("SELECT * FROM " + Data->SmsTable->TableName);
  Query->SQL->Add("WHERE ID = '" + Scales + "'");
  Query->SQL->Add("  AND DAY_NUMBER = " + String(Day));
  Query->SQL->Add("  AND DAY_DATE = :DayDate");                 // Je treba delat pomoci parametru, jinak to pri ruznych formatech datumu blbne
  Query->ParamByName("DayDate")->Value = TDate((int)Date);      // Musi byt jen celociselna cast, jinak to porovnana i cas
  try {
    Query->Open();
    Exists = (bool)(Query->RecordCount > 0);
    // Vyplnim dialog pro prepis
    WeighingOverwriteForm->ScaleLabel->Caption    = Scales;
    WeighingOverwriteForm->DayLabel->Caption      = Day;
    WeighingOverwriteForm->DateLabel->Caption     = Date.DateString();
    WeighingOverwriteForm->InsertedLabel->Caption = Query->FieldByName("INSERT_DATE_TIME")->AsString;
    WeighingOverwriteForm->GSMLabel->Caption      = Query->FieldByName("GSM_NUMBER")->AsString;
    // Samice
    WeighingOverwriteForm->OldFemaleCountLabel->Caption   = Query->FieldByName("FEMALE_COUNT")->AsString;
    WeighingOverwriteForm->OldFemaleAverageLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, Query->FieldByName("FEMALE_AVERAGE")->AsFloat);
    WeighingOverwriteForm->OldFemaleGainLabel->Caption    = FormatFloat(FORMAT_HMOTNOSTI, Query->FieldByName("FEMALE_GAIN")->AsFloat);
    WeighingOverwriteForm->OldFemaleSigmaLabel->Caption   = FormatFloat("0.000", Query->FieldByName("FEMALE_SIGMA")->AsFloat);
    WeighingOverwriteForm->OldFemaleCvLabel->Caption      = Query->FieldByName("FEMALE_CV")->AsString;
    WeighingOverwriteForm->OldFemaleUniLabel->Caption     = Query->FieldByName("FEMALE_UNI")->AsString;
    // Samci
    if (Query->FieldByName("MALE_COUNT")->IsNull) {
      // Samci se nepouzivaji
      WeighingOverwriteForm->OldMaleCountLabel->Caption   = "";
      WeighingOverwriteForm->OldMaleAverageLabel->Caption = "";
      WeighingOverwriteForm->OldMaleGainLabel->Caption    = "";
      WeighingOverwriteForm->OldMaleSigmaLabel->Caption   = "";
      WeighingOverwriteForm->OldMaleCvLabel->Caption      = "";
      WeighingOverwriteForm->OldMaleUniLabel->Caption     = "";
    } else {
      // Samci jsou zadani
      WeighingOverwriteForm->OldMaleCountLabel->Caption   = Query->FieldByName("MALE_COUNT")->AsString;
      WeighingOverwriteForm->OldMaleAverageLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, Query->FieldByName("MALE_AVERAGE")->AsFloat);
      WeighingOverwriteForm->OldMaleGainLabel->Caption    = FormatFloat(FORMAT_HMOTNOSTI, Query->FieldByName("MALE_GAIN")->AsFloat);
      WeighingOverwriteForm->OldMaleSigmaLabel->Caption   = FormatFloat("0.000", Query->FieldByName("MALE_SIGMA")->AsFloat);
      WeighingOverwriteForm->OldMaleCvLabel->Caption      = Query->FieldByName("MALE_CV")->AsString;
      WeighingOverwriteForm->OldMaleUniLabel->Caption     = Query->FieldByName("MALE_UNI")->AsString;
    }
    Query->Close();
    delete Query;
    return Exists;
  } catch(...) {}
  delete Query;
  return false;         // Nejaka chyba
}

void TWeighingForm::DeleteRecord(AnsiString Scales, int Day, TDate Date) {
  // Smaze zaznam s danymi parametry
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;

  Query->SQL->Add("DELETE FROM " + Data->SmsTable->TableName);
  Query->SQL->Add("WHERE ID = '" + Scales + "'");
  Query->SQL->Add("  AND DAY_NUMBER = " + String(Day));
  Query->SQL->Add("  AND DAY_DATE = :DayDate");                 // Je treba delat pomoci parametru, jinak to pri ruznych formatech datumu blbne
  Query->ParamByName("DayDate")->Value = TDate((int)Date);      // Musi byt jen celociselna cast, jinak to porovnana i cas
  try {
    Query->ExecSQL();
  } catch(...) {}
  delete Query;
}

//---------------------------------------------------------------------------
// Nacteni seznamu z tabulky
//---------------------------------------------------------------------------

void TWeighingForm::ReadFilters() {
  // Nacte vsechny filtry a ulozi jejich seznam do comboboxu
  // Nacte vsechny filtry do seznamu
  FilterComboBox->Clear();
  Data->FiltersTable->Open();
  try {
    for (Data->FiltersTable->First(); !Data->FiltersTable->Eof; Data->FiltersTable->Next()) {
      FilterComboBox->Items->Add(Data->FiltersTable->FieldByName("NAME")->AsString);
    }
  } catch(...) {}
  Data->FiltersTable->Close();
}

void TWeighingForm::ReadScales() {
  // Nacte vsechna identifikacni cisla v databazi a ulozi jejich seznam do comboboxu
  // Vlozim do comboboxu "All"
  ScaleComboBox->Clear();
  ScaleComboBox->Items->Add(LoadStr(SMS_ALL_SCALES));

  // Nactu seznam vah, ktere jsou v tabulce
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  Query->SQL->Add("SELECT ID FROM " + Data->SmsTable->TableName);
  Query->SQL->Add("GROUP BY ID");
  Query->SQL->Add("ORDER BY ID");
  try {
    Query->Open();
    Query->First();
    while (!Query->Eof) {
      ScaleComboBox->Items->Add(Query->FieldByName("ID")->AsString);
      Query->Next();
    }
    Query->Close();
  } catch(...) {}
  delete Query;
  ScaleComboBox->ItemIndex = 0;         // Vyberu vsechny vahy
}

void TWeighingForm::ReadDate() {
  // Nacte z tabulky minimalni a maximalni datum a ulozi je do lokalnich promennych a comboboxu
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  Query->SQL->Add("SELECT MIN(DAY_DATE) AS MIN_DATE, MAX(DAY_DATE) AS MAX_DATE FROM " + Data->SmsTable->TableName);
  try {
    Query->Open();
    MinDate = Query->FieldByName("MIN_DATE")->AsDateTime;
    MaxDate = Query->FieldByName("MAX_DATE")->AsDateTime;
    Query->Close();
    // Zobrazim vsechny zaznamy
    if ((int)MinDate == 0) {
      FromDateTimePicker->Date = Date();        // Neni tam zadny zaznam
    } else {
      FromDateTimePicker->Date = MinDate;
    }
    if ((int)MaxDate == 0) {
      TillDateTimePicker->Date = Date();        // Neni tam zadny zaznam
    } else {
      TillDateTimePicker->Date = MaxDate;
    }
  } catch(...) {}
  delete Query;
}

//---------------------------------------------------------------------------
// Nacteni zaznamu z tabulky
//---------------------------------------------------------------------------

void TWeighingForm::CreateSQLRecords(TQuery *Query) {
  // Vytvori do <Query> SQL dotaz pro vyber zaznamu podle nastavenych podminek
  String GenderPrefix;

  // Nastavim zvolene pohlavi - predponu
  if (PohlaviSamiceRadioButton->Checked) {
    GenderPrefix = "FEMALE_";
  } else {
    GenderPrefix = "MALE_";
  }

  Query->SQL->Clear();
  Query->SQL->Add("SELECT GSM_NUMBER, ID, DAY_NUMBER, DAY_DATE, INSERT_DATE_TIME,"
                   + GenderPrefix + "COUNT AS COUNT_HEADS,"
                   + GenderPrefix + "AVERAGE AS AVERAGE_WEIGHT,"
                   + GenderPrefix + "GAIN AS GAIN,"
                   + GenderPrefix + "SIGMA AS SIGMA,"
                   + GenderPrefix + "CV AS CV,"
                   + GenderPrefix + "UNI AS UNI,"
                   "NOTE"
                   + " FROM " + Data->SmsTable->TableName);

  // Podminka datumu - je treba delat pomoci parametru, jinak to pri ruznych formatech datumu blbne
  Query->SQL->Add("WHERE DAY_DATE >= :MinDate");
  Query->SQL->Add("  AND DAY_DATE <= :MaxDate");
  Query->ParamByName("MinDate")->Value = TDate((int)FromDateTimePicker->Date);          // Musi byt jen celociselna cast, jinak to porovnana i cas
  Query->ParamByName("MaxDate")->Value = TDate((int)TillDateTimePicker->Date);

  // Podminka vahy
  if (ScaleComboBox->ItemIndex > 0) {
    Query->SQL->Add("AND ID = '" + ScaleComboBox->Text + "'");
  }

  // Pohlavi - beru jen vyplnene udaje (pokud zadal jen 1 pohlavi, pole "MALE_" jsou vsechna prazdna
  Query->SQL->Add("AND " + GenderPrefix + "COUNT IS NOT NULL");

  // Seradim podle data
  Query->SQL->Add("ORDER BY DAY_DATE, ID");
}

void TWeighingForm::ReadRecords() {
  // Nacte vsechny zaznamy odpovidajici zadanym podminkam do Data->SmsOneGenderQuery

  // Skryju nebo zobrazim sloupce pro porovnani
  bool Compare = Data->GetCompareCurve(COMPARE_CURVE_WEIGHING_FORM);
  // Porovnani (stejne jako graf) muzu vykreslit jen pokud je v tabulce zobrazena jen 1 vaha
  // 8.11.2007: Vykresluju u vsech vah, uzivatel potrebuje jednim pohledem zkontrolovat vsechny vahy za posledni den
/*  if (ScaleComboBox->ItemIndex == 0) {
    Compare = false;            // Je tam vic vah, porovnani schovam
  }*/
  RecordsDBGrid->Columns->Items[9]->Visible  = Compare;
  RecordsDBGrid->Columns->Items[10]->Visible = Compare;

  // Vytvorim dotaz
  CreateSQLRecords(Data->SmsOneGenderQuery);

  // Otevru
  Data->SmsOneGenderQuery->Close();
  try {
    Data->SmsOneGenderQuery->Open();
    Data->SmsOneGenderQuery->Last();       // Prejdu na posledni
  } catch(...) {}
}

//---------------------------------------------------------------------------
// Nacteni a zobrazeni vsech dat
//---------------------------------------------------------------------------

void TWeighingForm::ReadAll() {
  // Nactu seznamy
  ReadFilters();
  ReadScales();
  ReadDate();
  // Nactu data
  ReadRecords();
  DrawGraph(GraphChart);
}

// --------------------------------------------------------------------------
// Nacteni rustovych krivek
// --------------------------------------------------------------------------

void TWeighingForm::LoadCurves() {
  // Nacte rustove krivky do vsech comboboxu
  Data->LoadCurvesToStrings(CompareComboBox->Items);
} // LoadCurves


//---------------------------------------------------------------------------
__fastcall TWeighingForm::TWeighingForm(TComponent* Owner)
        : TForm(Owner)
{
  GsmModem                   = new TGsmModem;
  GsmModem->BaudRate         = 38400;
  GsmModem->SmsRxTimeout     = 500;     // Prechod na textovy mod: 250 nejede, 350 jede sem tam, 450 funguje vzdy
  GsmModem->CmdTimeout       = 200;     // Kvuli novym modemum ES75, minimalni hodnota je 130.
  GsmModem->SmsDeleteTimeout = 2500;
}
//---------------------------------------------------------------------------
void __fastcall TWeighingForm::FormShow(TObject *Sender)
{
  StopReadingSms();
  OpenFoundFile = false;                        // Default neotviram
  PageControl->ActivePage = TableTabSheet;      // Default zobrazim tabulku
  GraphTypeComboBox->ItemIndex = 0;             // Default 1. graf v seznamu
  ReadAll();                                    // Nactu vse
  RecordsDBGrid->SetFocus();
  // Nastavim hinty u ikon
  StartToolButton->Hint   = StartReadingMenu->Caption;
  StopToolButton->Hint    = StopReadingMenu->Caption;
  RequestToolButton->Hint = RequestMenu->Caption;
  NewToolButton->Hint     = NewMenu->Caption;
  EditToolButton->Hint    = EditMenu->Caption;
  DeleteToolButton->Hint  = DeleteMenu->Caption;
  FindToolButton->Hint    = FindMenu->Caption;
  PrintToolButton->Hint   = PrintMenu->Caption;
  ExcelToolButton->Hint   = ExcelMenu->Caption;

  // Porovnani
  LoadCurves();                                 // Nactu teoreticke krivky
  CompareComboBox->ItemIndex = 0;               // Zrusim porovnani
  CompareComboBoxChange(NULL);                  // Vynuluje porovnavaci krivku a schova legendu
}
//---------------------------------------------------------------------------
void __fastcall TWeighingForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  StopReadingSms();
  Data->SmsOneGenderQuery->Close();
}
//---------------------------------------------------------------------------


void __fastcall TWeighingForm::NewMenuClick(TObject *Sender)
{
  // Prida rucne novy zaznam
  AnsiString Id;
  int Day;
  TDate DayDate;
  TDateTime InsertDate;    // Aby zobrazene datum a datum ulozene do tabulky odpovidalo

  PageControl->ActivePage = TableTabSheet;
  RecordsDBGrid->SetFocus();

  // Vyplnim info o zaznamu
  InsertDate = Now();
  WeighingRecordForm->InsertLabel->Caption = InsertDate;
  WeighingRecordForm->EditLabel->Caption   = "";         // U noveho zaznamu editace nemuze byt
  WeighingRecordForm->GSMLabel->Caption    = "";         // Rucne pridany zaznam nema GSM cislo

  // Prevyplnim nejake udaje
  if (ScaleComboBox->ItemIndex > 0) {
    WeighingRecordForm->ScalesEdit->Text = ScaleComboBox->Text;
  } else {
    WeighingRecordForm->ScalesEdit->Text = "";
  }
  WeighingRecordForm->DayEdit->Text = "";
  WeighingRecordForm->DateTimePicker->Date = Date();
  WeighingRecordForm->NoteEdit->Text           = "";
  WeighingRecordForm->FemalesCountEdit->Text   = "";
  WeighingRecordForm->FemalesAverageEdit->Text = "";
  WeighingRecordForm->FemalesGainEdit->Text    = "";
  WeighingRecordForm->FemalesSigmaEdit->Text   = "";
  WeighingRecordForm->FemalesCvEdit->Text      = "";
  WeighingRecordForm->FemalesUniEdit->Text     = "";
  WeighingRecordForm->MalesCountEdit->Text   = "";
  WeighingRecordForm->MalesAverageEdit->Text = "";
  WeighingRecordForm->MalesGainEdit->Text    = "";
  WeighingRecordForm->MalesSigmaEdit->Text   = "";
  WeighingRecordForm->MalesCvEdit->Text      = "";
  WeighingRecordForm->MalesUniEdit->Text     = "";

  if (WeighingRecordForm->ShowModal() != mrOk) {  // Zaroven kontroluje spravnost zadanych udaju
    return;
  }
  // Ulozim zadany zaznam do tabulky
  InsertNewRecord(InsertDate, "");      // Bez GSM cisla (vklada rucne)
  // Zapamatuju si nove udaje
  Id      = WeighingRecordForm->ScalesEdit->Text;
  Day     = WeighingRecordForm->DayEdit->Text.ToInt();
  DayDate = WeighingRecordForm->DateTimePicker->Date;
  // Nactu vse znovu
  ReadAll();
  // Najedu na novy zaznam
  TLocateOptions LocateOptions;
  LocateOptions.Clear();
  LocateOptions << loCaseInsensitive;
  Variant LocateValues[3];
  LocateValues[0] = Variant(Id);
  LocateValues[1] = Variant(Day);
  LocateValues[2] = Variant(DayDate);
  if (!Data->SmsOneGenderQuery->Locate("ID;DAY_NUMBER;DAY_DATE", VarArrayOf(LocateValues, 2), LocateOptions)) {
    Data->SmsOneGenderQuery->First();       // Nenasel jsem, vratim na prvni
  }
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::FromDateTimePickerChange(TObject *Sender)
{
  FilterComboBox->ItemIndex = -1;       // Zrusim vyber filtru
  ReadRecords();
  DrawGraph(GraphChart);
}
//---------------------------------------------------------------------------


void __fastcall TWeighingForm::DeleteMenuClick(TObject *Sender)
{
  bool OtherRecords;
  AnsiString Id;
  int Day;
  TDate Date;
  
  PageControl->ActivePage = TableTabSheet;
  RecordsDBGrid->SetFocus();
  // Smazu zaznam na aktualni pozici
  if (Data->SmsOneGenderQuery->RecordCount == 0) {
    return;     // V tabulce neni zadny zaznam
  }
  if (MessageBox(NULL, LoadStr(SMS_DELETE_RECORD).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
    return;   // Nechce smazat
  }
  // Pokusim se nalezt zaznam za nebo pred timto zaznamem, abych se na nej po smazani vratil
  if (Data->SmsOneGenderQuery->FindNext()) {
    // Za aktualnim zaznamem je jeste zaznam
    OtherRecords = true;
    Id   = Data->SmsOneGenderQuery->FieldByName("ID")->AsString;
    Day  = Data->SmsOneGenderQuery->FieldByName("DAY_NUMBER")->AsInteger;
    Date = Data->SmsOneGenderQuery->FieldByName("DAY_DATE")->AsDateTime;
    Data->SmsOneGenderQuery->Prior();   //       Vratim na puvodni zaznam
  } else if (Data->SmsOneGenderQuery->FindPrior()) {
    // Za aktualnim zaznamem neni zaznam, ale je pred
    OtherRecords = true;
    Id   = Data->SmsOneGenderQuery->FieldByName("ID")->AsString;
    Day  = Data->SmsOneGenderQuery->FieldByName("DAY_NUMBER")->AsInteger;
    Date = Data->SmsOneGenderQuery->FieldByName("DAY_DATE")->AsDateTime;
    Data->SmsOneGenderQuery->Next();    //       Vratim na puvodni zaznam
  } else {
    // Mazu posledni zaznam
    OtherRecords = false;
  }
  // Smazu zaznam
  DeleteRecord(Data->SmsOneGenderQuery->FieldByName("ID")->AsString,
               Data->SmsOneGenderQuery->FieldByName("DAY_NUMBER")->AsInteger,
               Data->SmsOneGenderQuery->FieldByName("DAY_DATE")->AsDateTime);

  if (OtherRecords) {
    // V tabulce jeste zbyly nejake zaznamy, nactu jen tabulku a graf
    ReadRecords();
    DrawGraph(GraphChart);
    // Najedu na blizky zaznam
    TLocateOptions LocateOptions;
    LocateOptions.Clear();
    LocateOptions << loCaseInsensitive;
    Variant LocateValues[3];
    LocateValues[0] = Variant(Id);
    LocateValues[1] = Variant(Day);
    LocateValues[2] = Variant(Date);
    if (!Data->SmsOneGenderQuery->Locate("ID;DAY_NUMBER;DAY_DATE", VarArrayOf(LocateValues, 2), LocateOptions)) {
      Data->SmsOneGenderQuery->First();       // Nenasel jsem, vratim na prvni
    }
  } else {
    // Smazal jsem posledni zaznam, nactu vse znovu
    ReadAll();
  }
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::EditMenuClick(TObject *Sender)
{
  // Upravi zaznam na aktualni pozici
  bool Changed;
  AnsiString Id;
  int Day;
  TDate Date;

  PageControl->ActivePage = TableTabSheet;
  RecordsDBGrid->SetFocus();

  if (Data->SmsOneGenderQuery->RecordCount == 0) {
    return;     // V tabulce neni zadny zaznam
  }

  // Zapamatuju si puvodni udaje
  Id   = Data->SmsOneGenderQuery->FieldByName("ID")->AsString;
  Day  = Data->SmsOneGenderQuery->FieldByName("DAY_NUMBER")->AsInteger;
  Date = Data->SmsOneGenderQuery->FieldByName("DAY_DATE")->AsDateTime;

  // Najedu na zaznam v tabulce
  TLocateOptions LocateOptions;
  LocateOptions.Clear();
  LocateOptions << loCaseInsensitive;
  Variant LocateValues[3];
  LocateValues[0] = Variant(Id);
  LocateValues[1] = Variant(Day);
  LocateValues[2] = Variant(Date);
  Data->SmsTable->Open();
  try {
    if (!Data->SmsTable->Locate("ID;DAY_NUMBER;DAY_DATE", VarArrayOf(LocateValues, 2), LocateOptions)) {
      throw 1;     // Nenasel jsem
    }
    // Ulozim hodnoty z tabulky do okna
    // Vyplnim info o zaznamu
    WeighingRecordForm->InsertLabel->Caption = Data->SmsTable->FieldByName("INSERT_DATE_TIME")->AsString;
    WeighingRecordForm->EditLabel->Caption   = Data->SmsTable->FieldByName("EDIT_DATE_TIME")->AsString;
    WeighingRecordForm->GSMLabel->Caption    = Data->SmsTable->FieldByName("GSM_NUMBER")->AsString;
    WeighingRecordForm->ScalesEdit->Text     = Data->SmsTable->FieldByName("ID")->AsString;
    WeighingRecordForm->DayEdit->Text        = Data->SmsTable->FieldByName("DAY_NUMBER")->AsString;
    WeighingRecordForm->DateTimePicker->Date = Data->SmsTable->FieldByName("DAY_DATE")->AsDateTime;
    WeighingRecordForm->NoteEdit->Text       = Data->SmsTable->FieldByName("NOTE")->AsString;
    WeighingRecordForm->FemalesCountEdit->Text   = Data->SmsTable->FieldByName("FEMALE_COUNT")->AsString;
    WeighingRecordForm->FemalesAverageEdit->Text = Data->SmsTable->FieldByName("FEMALE_AVERAGE")->AsString;
    WeighingRecordForm->FemalesGainEdit->Text    = Data->SmsTable->FieldByName("FEMALE_GAIN")->AsString;
    WeighingRecordForm->FemalesSigmaEdit->Text   = Data->SmsTable->FieldByName("FEMALE_SIGMA")->AsString;
    WeighingRecordForm->FemalesCvEdit->Text      = Data->SmsTable->FieldByName("FEMALE_CV")->AsString;
    WeighingRecordForm->FemalesUniEdit->Text     = Data->SmsTable->FieldByName("FEMALE_UNI")->AsString;
    WeighingRecordForm->BothGendersCheckBox->Checked = !Data->SmsTable->FieldByName("MALE_COUNT")->IsNull;
    if (WeighingRecordForm->BothGendersCheckBox->Checked) {
      WeighingRecordForm->MalesCountEdit->Text   = Data->SmsTable->FieldByName("MALE_COUNT")->AsString;
      WeighingRecordForm->MalesAverageEdit->Text = Data->SmsTable->FieldByName("MALE_AVERAGE")->AsString;
      WeighingRecordForm->MalesGainEdit->Text    = Data->SmsTable->FieldByName("MALE_GAIN")->AsString;
      WeighingRecordForm->MalesSigmaEdit->Text   = Data->SmsTable->FieldByName("MALE_SIGMA")->AsString;
      WeighingRecordForm->MalesCvEdit->Text      = Data->SmsTable->FieldByName("MALE_CV")->AsString;
      WeighingRecordForm->MalesUniEdit->Text     = Data->SmsTable->FieldByName("MALE_UNI")->AsString;
    } else {
      WeighingRecordForm->MalesCountEdit->Text   = "";
      WeighingRecordForm->MalesAverageEdit->Text = "";
      WeighingRecordForm->MalesGainEdit->Text    = "";
      WeighingRecordForm->MalesSigmaEdit->Text   = "";
      WeighingRecordForm->MalesCvEdit->Text      = "";
      WeighingRecordForm->MalesUniEdit->Text     = "";
    }

    // Zobrazim okno pro upravu
    if (WeighingRecordForm->ShowModal() != mrOk) {  // Zaroven kontroluje spravnost zadanych udaju
      throw 1;
    }

    // 13.3.2007: Zkontroluju, zda chce jen updatnout stary zaznam nebo zmenil klicove parametry
    if (Id   == WeighingRecordForm->ScalesEdit->Text &&
        Day  == WeighingRecordForm->DayEdit->Text.ToInt()    &&
        Date == TDate((int)WeighingRecordForm->DateTimePicker->Date)) {
      // Nezmenil klicove parametry: Id. cislo, cislo dne a datum => chce jen updatnout puvodni zaznam.
      // Pokracuju rovnou na editaci zaznamu, uzivatele se na nic neptam.
    } else {
      // Zmenil klicove parametry zaznamu => v tabulce by uz mohl byt starsi zaznam s touto kombinaci klicovych parametru
      // Pokud najdu zaznam se stejnymi parametry, smazu ho (jinak by vznikly 2 duplicitni zaznamy)
      if (!CanSaveRecord()) {
        throw 1;  // Nechce prepsat
      }
    }

    // Ulozim zmeny
    Data->SmsTable->Edit();
    Data->SmsTable->FieldByName("EDIT_DATE_TIME")->AsDateTime = Now();  // Datum editace
    Data->SmsTable->FieldByName("ID")->AsString            = GetIdWithoutSpaces(WeighingRecordForm->ScalesEdit->Text);
    Data->SmsTable->FieldByName("DAY_NUMBER")->AsInteger   = WeighingRecordForm->DayEdit->Text.ToInt();
    Data->SmsTable->FieldByName("DAY_DATE")->AsDateTime    = WeighingRecordForm->DateTimePicker->Date;
    Data->SmsTable->FieldByName("NOTE")->AsString          = WeighingRecordForm->NoteEdit->Text;
    Data->SmsTable->FieldByName("FEMALE_COUNT")->AsInteger = WeighingRecordForm->FemalesCountEdit->Text.ToInt();
    Data->SmsTable->FieldByName("FEMALE_AVERAGE")->AsFloat = WeighingRecordForm->FemalesAverageEdit->Text.ToDouble();
    Data->SmsTable->FieldByName("FEMALE_GAIN")->AsFloat    = WeighingRecordForm->FemalesGainEdit->Text.ToDouble();
    Data->SmsTable->FieldByName("FEMALE_SIGMA")->AsFloat   = WeighingRecordForm->FemalesSigmaEdit->Text.ToDouble();
    Data->SmsTable->FieldByName("FEMALE_CV")->AsInteger    = WeighingRecordForm->FemalesCvEdit->Text.ToInt();
    Data->SmsTable->FieldByName("FEMALE_UNI")->AsInteger   = WeighingRecordForm->FemalesUniEdit->Text.ToInt();
    if (WeighingRecordForm->BothGendersCheckBox->Checked) {
      Data->SmsTable->FieldByName("MALE_COUNT")->AsInteger = WeighingRecordForm->MalesCountEdit->Text.ToInt();
      Data->SmsTable->FieldByName("MALE_AVERAGE")->AsFloat = WeighingRecordForm->MalesAverageEdit->Text.ToDouble();
      Data->SmsTable->FieldByName("MALE_GAIN")->AsFloat    = WeighingRecordForm->MalesGainEdit->Text.ToDouble();
      Data->SmsTable->FieldByName("MALE_SIGMA")->AsFloat   = WeighingRecordForm->MalesSigmaEdit->Text.ToDouble();
      Data->SmsTable->FieldByName("MALE_CV")->AsInteger    = WeighingRecordForm->MalesCvEdit->Text.ToInt();
      Data->SmsTable->FieldByName("MALE_UNI")->AsInteger   = WeighingRecordForm->MalesUniEdit->Text.ToInt();
    } else {
      // Pouziva jen 1 pohlavi => musim smazat udaje pro samce, pred editaci mohly byt vyplnene. Pordle hodnoty NULL pak
      // poznam, ze v zaznamu je jen 1 pohlavi
      Data->SmsTable->FieldByName("MALE_COUNT")->Clear();
      Data->SmsTable->FieldByName("MALE_AVERAGE")->Clear();
      Data->SmsTable->FieldByName("MALE_GAIN")->Clear();
      Data->SmsTable->FieldByName("MALE_SIGMA")->Clear();
      Data->SmsTable->FieldByName("MALE_CV")->Clear();
      Data->SmsTable->FieldByName("MALE_UNI")->Clear();
    }
    Data->SmsTable->Post();
    Changed = (bool)(Id   != Data->SmsTable->FieldByName("ID")->AsString
                  || Day  != Data->SmsTable->FieldByName("DAY_NUMBER")->AsInteger
                  || Date != Data->SmsTable->FieldByName("DAY_DATE")->AsDateTime);
    // Zapamatuju si nove udaje
    Id   = Data->SmsTable->FieldByName("ID")->AsString;
    Day  = Data->SmsTable->FieldByName("DAY_NUMBER")->AsInteger;
    Date = Data->SmsTable->FieldByName("DAY_DATE")->AsDateTime;
  } catch(...) {}
  Data->SmsTable->Close();
  if (Changed) {
    // Zmenil zakladni parametry, nactu vse znovu
    ReadAll();
  } else {
    // Menil jen statistiku, nactu jen tabulku a graf
    ReadRecords();
    DrawGraph(GraphChart);
  }//else
  // Najedu na editovany zaznam - v kazdem pripade
  LocateOptions.Clear();
  LocateOptions << loCaseInsensitive;
  LocateValues[0] = Variant(Id);
  LocateValues[1] = Variant(Day);
  LocateValues[2] = Variant(Date);
  if (!Data->SmsOneGenderQuery->Locate("ID;DAY_NUMBER;DAY_DATE", VarArrayOf(LocateValues, 2), LocateOptions)) {
    Data->SmsOneGenderQuery->First();       // Nenasel jsem, vratim na prvni
  }
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::FindMenuClick(TObject *Sender)
{
  String File;
  // Nastavim podminky podle vybraneho radku
  if (Data->SmsOneGenderQuery->RecordCount == 0) {
    // Zadny zaznam v tabulce neni => nuluju podminky
    FindFileForm->ScalesEdit->Text = "";
    FindFileForm->DayEdit->Text    = "";
    FindFileForm->FromDateTimePicker->Date = Date();
    FindFileForm->FromCheckBox->Checked    = false;
    FindFileForm->TillDateTimePicker->Date = Date();
    FindFileForm->TillCheckBox->Checked    = false;
  } else {
    // Nejaky zaznam tam je => podminky nastavim podle prave vybraneho zaznamu
    FindFileForm->ScalesEdit->Text = Data->SmsOneGenderQuery->FieldByName("ID")->AsString;
    FindFileForm->DayEdit->Text    = Data->SmsOneGenderQuery->FieldByName("DAY_NUMBER")->AsString;
    FindFileForm->FromDateTimePicker->Date = Data->SmsOneGenderQuery->FieldByName("DAY_DATE")->AsDateTime;
    FindFileForm->FromCheckBox->Checked    = true;
    FindFileForm->TillDateTimePicker->Date = Data->SmsOneGenderQuery->FieldByName("DAY_DATE")->AsDateTime;
    FindFileForm->TillCheckBox->Checked    = true;
  }
  FindFileForm->FilesListBox->Clear();
  if (FindFileForm->ShowModal() != mrOk) {
    return;
  }
  // Otevru vybrany soubor
  if (FindFileForm->FilesListBox->ItemIndex < 0) {
    return;     // Zadny soubor neni vybrany
  }
  OpenFoundFile = true;        // Po zavreni tohoto okna otevre nalezeny soubor
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::GraphTypeComboBoxChange(TObject *Sender)
{
  DrawGraph(GraphChart);
}
//---------------------------------------------------------------------------


void __fastcall TWeighingForm::PrintReportMenuClick(TObject *Sender)
{
  // Vytisk reportu se zaznamy
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;

  try {
    // Vytvorim SQL se zaznamy
    CreateSQLRecords(Query);
    Query->Open();
    Query->First();
    // Nastavim query a sloupce (report se vyuziva i v databazi vazeni a tam se prehazuji sloupce)
    ReportReportForm->DataSet                  = Query;
    ReportReportForm->DayQRDBText->DataSet     = Query;
    ReportReportForm->DateQRDBText->DataSet    = Query;
    ReportReportForm->CountQRDBText->DataSet   = Query;
    ReportReportForm->AverageQRDBText->DataSet = Query;
    ReportReportForm->GainQRDBText->DataSet    = Query;
    ReportReportForm->SigmaQRDBText->DataSet   = Query;
    ReportReportForm->CvQRDBText->DataSet      = Query;
    ReportReportForm->UniQRDBText->DataSet     = Query;
    ReportReportForm->DayQRDBText->DataField     = "DAY_NUMBER";
    ReportReportForm->DateQRDBText->DataField    = "DAY_DATE";
    ReportReportForm->CountQRDBText->DataField   = "COUNT_HEADS";
    ReportReportForm->AverageQRDBText->DataField = "AVERAGE_WEIGHT";
    ReportReportForm->GainQRDBText->DataField    = "GAIN";
    ReportReportForm->SigmaQRDBText->DataField   = "SIGMA";
    ReportReportForm->CvQRDBText->DataField      = "CV";
    ReportReportForm->UniQRDBText->DataField     = "UNI";
    // Tisk
    CreateReportHeader(ReportReportForm->SouborQRLabel, ReportReportForm->PohlaviNazevQRLabel, ReportReportForm->PohlaviQRLabel);
    ReportReportForm->Preview();
  } catch(...) {}
  delete Query;
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::PrintGraphMenuClick(TObject *Sender)
{
  // Vytiskne graf
  GrafReportForm->NazevQRLabel->Caption = GraphTypeComboBox->Text;
  CreateReportHeader(GrafReportForm->SouborQRLabel, GrafReportForm->PohlaviNazevQRLabel, GrafReportForm->PohlaviQRLabel);
  PrintCopyCharts(GraphChart->Series[CURVE_REAL], GrafReportForm->HistogramQRChart->Chart->Series[0]);

  // Druhy prubeh zakazu
  GrafReportForm->HistogramQRChart->Chart->Series[1]->Active = false;

  // Teoretickou rustovou krivku jako treti prubeh
  if (Data->GetCompareCurve(COMPARE_CURVE_WEIGHING_FORM) && GraphTypeComboBox->ItemIndex == 1) {
    // Zobrazim porovnani
    PrintCopyCharts(GraphChart->Series[CURVE_COMPARE], GrafReportForm->HistogramQRChart->Chart->Series[2]);
    GrafReportForm->HistogramQRChart->Chart->Series[2]->Active = true;
    GrafReportForm->HistogramQRChart->Chart->Legend->Visible = true;                    // Zobrazim legendu
  } else {
    // Schovam prubeh
    GrafReportForm->HistogramQRChart->Chart->Series[2]->Active = false;
    GrafReportForm->HistogramQRChart->Chart->Legend->Visible = false;                   // Schovam legendu
  }

  // Ctvrty prubeh zakazu
  GrafReportForm->HistogramQRChart->Chart->Series[3]->Active = false;

  GrafReportForm->HistogramQRChart->Chart->BottomAxis->Title->Caption = GraphChart->BottomAxis->Title->Caption;
  GrafReportForm->HistogramQRChart->Chart->LeftAxis->Title->Caption   = GraphChart->LeftAxis->Title->Caption;
  GrafReportForm->HistogramQRChart->Chart->LeftAxis->Increment = GraphChart->LeftAxis->Increment;
  try {
    GrafReportForm->Preview();
  } catch(...) {}
  GrafReportForm->HistogramQRChart->Chart->LeftAxis->Increment = 0;     // Zpet na default
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::ExcelMenuClick(TObject *Sender)
{
  // Export tabulky se zaznamy
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;

  try {
    // Vytvorim SQL se zaznamy
    CreateSQLRecords(Query);
    Query->Open();
    Query->First();
    // Export
    ExcelExportReport(Query);
  } catch(...) {}
  delete Query;
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::FiltersMenuClick(TObject *Sender)
{
  if (FilterComboBox->ItemIndex < 0) {
    WeighingFiltersForm->SelectedFilter = "";   // Neni nic vybrane
  } else {
    WeighingFiltersForm->SelectedFilter = FilterComboBox->Items->Strings[FilterComboBox->ItemIndex];    // Prave vybrany filtr
  }
  WeighingFiltersForm->ShowModal();
  ReadFilters();
  FilterComboBox->ItemIndex = -1;       // Aby nebyl zadny vybrany
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::FilterComboBoxChange(TObject *Sender)
{
  if (FilterComboBox->ItemIndex < 0) {
    return;     // Neni nic vybrane
  }
  SetFilter(FilterComboBox->Items->Strings[FilterComboBox->ItemIndex]);
  ReadRecords();
  DrawGraph(GraphChart);
}
//---------------------------------------------------------------------------


void __fastcall TWeighingForm::RecordsDBGridKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if (Key == 46) {
    // Delete = mazani zaznamu
    DeleteMenuClick(NULL);
  }
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::RequestMenuClick(TObject *Sender)
{
  // Telefonni cislo
  if (NumberForm->ShowModal() != mrOk) {
    return;
  }
  ReadStatus.RequestNumber = NumberForm->NumberEdit->Text;
  // Den statistiky
  if (StatisticsDayForm->ShowModal() != mrOk) {
    return;
  }
  if (StatisticsDayForm->TodayRadioButton->Checked) {
    ReadStatus.RequestDay = -1;
  } else {
    ReadStatus.RequestDay = StatisticsDayForm->DayEdit->Text.ToInt();
  }
  // Vyber modemu
  if (!SelectModem()) {
    return;
  }
  // Odstartuju request SMS
  StartReadingSms(true);
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::StartReadingMenuClick(TObject *Sender)
{
  if (!SelectModem()) {
    return;
  }
  // Odstartuju nacitani SMS na pozadi
  StartReadingSms(false);
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::StopReadingMenuClick(TObject *Sender)
{
  StopReadingSms();
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::PrintToolButtonClick(TObject *Sender)
{
  if (PageControl->ActivePage == TableTabSheet) {
    PrintReportMenuClick(NULL);        // Tisknu report
  } else {
    PrintGraphMenuClick(NULL);         // Tisknu graf
  }
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::RecordsDBGridDrawColumnCell(TObject *Sender,
      const TRect &Rect, int DataCol, TColumn *Column,
      TGridDrawState State)
{

  if ((int)Date() == (int)Data->SmsOneGenderQuery->FieldByName("INSERT_DATE_TIME")->AsDateTime) {
    RecordsDBGrid->Canvas->Font->Style = TFontStyles() << fsBold;       // Zaznam byl vlozeny dnes
  }
  RecordsDBGrid->DefaultDrawColumnCell(Rect, DataCol, Column, State);
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::ReadTimerTimer(TObject *Sender)
{
  String Message;
  String From;
  String Str;
  TDecodedSms DecodedSms;
  TDateTime InsertDate;

  if (ReadStatus.Mode == MODE_IDLE) {
    return;     // Nenacita se
  }
  switch (ReadStatus.ModemStatus) {
    case MODEM_INIT:
      StringConnecting(1);
      if (!GsmModem->Connect(ReadStatus.PortName)) {
        GsmLogWriteLine(LOG_CONNECT_ERROR);
        return;
      }
      GsmLogWriteLine(LOG_CONNECT_OK);
      StringConnecting(2);
      if (!GsmModem->Reset()) {
        GsmLogWriteLine(LOG_RESET_ERROR);
        return;
      }
      GsmLogWriteLine(LOG_RESET_OK);
      ReadStatus.ModemStatus = MODEM_CONNECTED;
      break;

    case MODEM_CONNECTED:
      StringConnecting(3);
      if (!GsmModem->Check()) {
        GsmLogWriteLine(LOG_CHECK_ERROR);
        return;
      }
      GsmLogWriteLine(LOG_CHECK_OK);
      // Sila signalu
      ReadSignalStrength();
      // Operator
      StringConnecting(4);
      if (!GsmModem->IsRegistered()) {
        GsmLogWriteLine(LOG_REGISTER_ERROR);
        return; // Neni prihlasen do site
      }
      GsmModem->Operator(ReadStatus.Operator);
      ReadStatus.Operator.Delete(ReadStatus.Operator.Pos("\""), 1);   // Umazu prvni uvozovky
      ReadStatus.Operator.Delete(ReadStatus.Operator.Pos("\""), 100); // Umazu od posledni uvozovky vse dal
      GsmLogWriteLine(LOG_REGISTER_OK + " <" + ReadStatus.Operator + ">");
      if (ReadStatus.Mode == MODE_READING) {
        ReadStatus.ModemStatus = MODEM_WAITING; // Automaticke nacitani
      } else {
        ReadStatus.ModemStatus = MODEM_SENDING; // Request
      }
      break;

    case MODEM_SENDING:
      ReadSignalStrength();     // Aktualni sila signalu (operator se nemohl zmenit)
      GsmLogWriteLine("");
      GsmLogWriteLine(LOG_SIGNAL + " " + ReadStatus.Signal + "%");
      StatusLabel->Caption = ReadStatus.Operator + " (" + String(ReadStatus.Signal) + "%)" + READING_SPACE + LoadStr(SMS_SENDING);
      StatusLabel->Refresh();
      // Poslu SMS
      Message = "STAT";
      if (ReadStatus.RequestDay >= 0) {
        Message += " " + String(ReadStatus.RequestDay); // Chce nejaky den
      }
      Screen->Cursor = crHourGlass;     // Trva to
      if (!GsmModem->SmsSend(ReadStatus.RequestNumber, Message.c_str())) {
        WriteLogSms(LOG_SEND_ERROR, ReadStatus.RequestNumber, Message);
        Screen->Cursor = crAppStart;
        return;
      }
      WriteLogSms(LOG_SEND_OK, ReadStatus.RequestNumber, Message);
      Screen->Cursor = crAppStart;
      ReadStatus.ModemStatus = MODEM_WAITING;   // Pockam, az modem SMS prijme
      break;

    case MODEM_WAITING:
      ReadSignalStrength();     // Aktualni sila signalu (operator se nemohl zmenit)
      Str = ReadStatus.Operator + " (" + String(ReadStatus.Signal) + "%)" + READING_SPACE + LoadStr(SMS_WAITING);
      if (ReadStatus.Mode != MODE_REQUEST) {
        Str += "  (" + String(ReadStatus.SmsCount) + " " + LoadStr(SMS_MESSAGES) + ")";
      }
      StatusLabel->Caption = Str;
      StatusLabel->Refresh();
      if (GsmModem->SmsCount() < 1) {
        return;         // V pameti neni zadna SMS, cekam dal
      }
      // Do modemu prave prisla nejaka SMS, zacnu cist
      ReadStatus.ModemStatus = MODEM_READING; // Zacnu nacitat
      ReadStatus.Address     = 0;
      break;

    case MODEM_READING:
      if (GsmModem->SmsCount() < 1) {
        ReadStatus.ModemStatus = MODEM_WAITING;         // V pameti neni zadna SMS, prejdu zpet na cekani
        return;
      }
      ReadSignalStrength();     // Aktualni sila signalu (operator se nemohl zmenit)
      GsmLogWriteLine("");
      GsmLogWriteLine(LOG_SIGNAL + " " + ReadStatus.Signal + "%");
      // Zobrazim
      Str = ReadStatus.Operator + " (" + String(ReadStatus.Signal) + "%)" + READING_SPACE + LoadStr(SMS_READING);
      if (ReadStatus.Mode == MODE_REQUEST) {
        Str += "... (" + String(ReadStatus.Address) + "/" + String(GsmModem->MemorySize) + ")";    // Pri requestu nezobrazuju pocet nactenych zprav
      } else {
        Str += ": " + String(ReadStatus.SmsCount) + " " + LoadStr(SMS_MESSAGES) + " (" + String(ReadStatus.Address) + "/" + String(GsmModem->MemorySize) + ")";
      }
      StatusLabel->Caption = Str;
      StatusLabel->Refresh();
      // Nactu SMS
      if (!GsmModem->SmsReceive(ReadStatus.Address, From, Message)) {
        ReadStatus.ErrorCounter++;
        if (ReadStatus.ErrorCounter < 4) {
          return;     // Pokusim se v dalsim kroku znovu
        }
        // SMS z teto pozice opravdu nelze nacist i pres nekolik pokusu. Je patrne v neplatnem formatu (diakritika atd.)
        // nebo jde o odeslanou zpravu ulozenou na SIM / v pameti. Smazu ji.
        GsmLogWriteLine(LOG_READ_ERROR + " " + ReadStatus.Address);
        if (ReadStatus.Mode == MODE_REQUEST) {
          // Pri requestu nesmim vynechat ani jedinou pozici. SMS muze dojit napr. na pozici 50, ale nepodari se mne nacist
          // 4x pozici 30, tak by se to ukoncilo a requestnuta SMS by v modemu zustala. Musim pokracovat dal, neplatne SMS
          // smazu pripadne az u dalsiho vycitani po pulnoci.
          IncreaseAddress();      // Adresa pro pristi cteni
          break;
        }
        ReadStatus.ErrorCounter = 0;    // Toto pocitadlo pouziva i rezim MODEM_DELETING
        ReadStatus.ModemStatus  = MODEM_DELETING;
        break;
      }//if
      // Nacetl jsem uspesne danou pozici
      ReadStatus.ErrorCounter = 0;      // Nuluju pocitadlo chyb
      if (From == "") {                 // Na teto adrese neni ulozena SMS, pozice je prazdna
        WriteLogSms(LOG_READ_OK + " " + String(ReadStatus.Address), From, "");     // Prazdna SMS, radsi smazu obsah
        IncreaseAddress();              // Adresa pro pristi cteni
        break;
      }//if
      WriteLogSms(LOG_READ_OK + " " + String(ReadStatus.Address), From, Message);
      // Zpracovani SMS
      if (ReadStatus.Mode == MODE_REQUEST && From != ReadStatus.RequestNumber) {
        IncreaseAddress();      // Adresa pro pristi cteni
        break;                  // Dal request na urcite tel. cislo, ale neni to SMS s odpovedi z tohoto cisla - zpravu ignoruju a nemazu
      }
      // Rozkoduju SMS
      if (!DecodeSms(From, Message, &DecodedSms)) {
        GsmLogWriteLine(LOG_DECODE_ERROR);
        ReadStatus.ModemStatus = MODEM_DELETING;        // Neplatny tvar SMS, SMS smazu
        break;
      }
      if (ReadStatus.Mode == MODE_REQUEST) {
        // Pokud chce statistiku z urciteho dne, musi SMS obsahovat tento den
        if (ReadStatus.RequestDay >= 0 && ReadStatus.RequestDay !=DecodedSms.Day) {
          IncreaseAddress();      // Adresa pro pristi cteni
          break;                  // Neni to SMS s odpovedi na vyzadany den - zpravu ignoruju a nemazu
        }
      }//if
      // Jde o platnou SMS (bud automaticky nactenou, nebo vyzadanou)
      GsmLogWriteLine(LOG_DECODE_OK);
      // Ulozim SMS
      InsertDate = Now();
      ReadTimer->Enabled = false;     // Fce EditDecodedSms() muze editovat a InsertNewRecord() se muze ptat na prepis
      try {
        Screen->Cursor = crDefault;
        if (!EditDecodedSms(&DecodedSms, InsertDate, (bool)(ReadStatus.Mode == MODE_REQUEST))) { // Edituje pouze pokud jde o request, jinak jen ulozi hodnoty do okna, z ktereho hodnoty bere fce InsertNewRecord()
          throw 1;    // Nechce ulozit
        }
        // Ulozim zadany zaznam do tabulky
        InsertNewRecord(InsertDate, DecodedSms.Number);
      } catch(...) {}
      Screen->Cursor = crAppStart;
      ReadTimer->Enabled = true;
      ReadAll();                                // Nactu vse znovu
      ReadStatus.SmsCount++;                    // Prictu platnou SMS
      ReadStatus.ModemStatus = MODEM_DELETING;  // Smazu zpracovanou SMS
      break;

    case MODEM_DELETING:
      if (!GsmModem->SmsDelete(ReadStatus.Address)) {
        ReadStatus.ErrorCounter++;
        if (ReadStatus.ErrorCounter < 4) {
          return;     // Pokusim se v dalsim kroku znovu
        }
        // SMS z teto pozice opravdu nelze smazat i pres nekolik pokusu. Beru ji jako smazanou a uz se dal nepokousim
        GsmLogWriteLine(LOG_DELETE_ERROR + " " + ReadStatus.Address);
      }//if
      GsmLogWriteLine(LOG_DELETE_OK + " " + ReadStatus.Address);
      // Pokud jsem nacetl odpoved z requestu, koncim nacitani
      if (ReadStatus.Mode == MODE_REQUEST) {
        StopReadingSms();
      } else {
        IncreaseAddress();                      // Adresa pro pristi cteni
        ReadStatus.ModemStatus = MODEM_READING; // Zpet na automaticke nacitani
      }//else
      break;
      
  }//switch
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::Growthcurves1Click(TObject *Sender)
{
  // Editace rustovych krivek
  CurvesForm->ShowModal();
  LoadCurves();                         // Nactu teoreticke krivky
  CompareComboBox->ItemIndex = 0;       // Default vyberu zadne porovnani
  CompareComboBoxChange(NULL);          // Prekresli rustovou krivku
}
//---------------------------------------------------------------------------

void __fastcall TWeighingForm::CompareComboBoxChange(TObject *Sender)
{
  // Nastavim porovnani s teoretickou rustovou krivkou
  TGrowthCurve *Curve;
  TCurveList *CurveList;

  if (CompareComboBox->ItemIndex > 0) {
    // Vybral nekterou krivku
    // Nahraju vybranou krivku
    CurveList = new TCurveList();
    try {
      Data->LoadCurvesFromDatabase(CurveList);
      if ((Curve = CurveList->GetCurve(CompareComboBox->ItemIndex - 1)) == NULL) {
        throw 1;
      }
      // Nastavim krivku
      Data->SetCompareCurve(COMPARE_CURVE_WEIGHING_FORM, Curve);
    } catch (...) {}
    delete CurveList;

  } else {
    // Nechce porovnavat
    Data->SetCompareCurve(COMPARE_CURVE_WEIGHING_FORM, NULL);
  }

  // Na zaver obnovim zobrazeni
  ReadRecords();
  DrawGraph(GraphChart);
}
//---------------------------------------------------------------------------

