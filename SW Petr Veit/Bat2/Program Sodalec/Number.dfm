object NumberForm: TNumberForm
  Left = 219
  Top = 164
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Phone number'
  ClientHeight = 304
  ClientWidth = 257
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 16
    Top = 88
    Width = 225
    Height = 161
    Caption = 'Existing numbers'
    TabOrder = 0
    object NumbersListBox: TListBox
      Left = 16
      Top = 24
      Width = 193
      Height = 97
      ItemHeight = 13
      TabOrder = 0
      OnClick = NumbersListBoxClick
      OnDblClick = NumbersListBoxDblClick
    end
    object NewButton: TButton
      Left = 16
      Top = 128
      Width = 89
      Height = 19
      Caption = 'New'
      TabOrder = 1
      OnClick = NewButtonClick
    end
    object DeleteButton: TButton
      Left = 120
      Top = 128
      Width = 89
      Height = 19
      Caption = 'Delete'
      TabOrder = 2
      OnClick = DeleteButtonClick
    end
  end
  object GroupBox2: TGroupBox
    Left = 16
    Top = 8
    Width = 225
    Height = 65
    Caption = 'Phone number'
    TabOrder = 1
    object NumberEdit: TEdit
      Left = 16
      Top = 24
      Width = 193
      Height = 21
      TabOrder = 0
      Text = 'NumberEdit'
      OnKeyPress = NumberEditKeyPress
    end
  end
  object OkButton: TButton
    Left = 72
    Top = 264
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 2
    OnClick = OkButtonClick
  end
  object Button2: TButton
    Left = 168
    Top = 264
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
