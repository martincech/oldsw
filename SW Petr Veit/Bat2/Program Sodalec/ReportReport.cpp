//----------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "ReportReport.h"
#include "DM.h"
//----------------------------------------------------------------------------
#pragma resource "*.dfm"
TReportReportForm *ReportReportForm;
//----------------------------------------------------------------------------
__fastcall TReportReportForm::TReportReportForm(TComponent* Owner)
    : TQuickRep(Owner)
{
}
//----------------------------------------------------------------------------
void __fastcall TReportReportForm::QuickRepBeforePrint(
      TCustomQuickRep *Sender, bool &PrintReport)
{
  Counter = 0;  // Nuluju pocitadlo vytisknutych radku
}
//---------------------------------------------------------------------------
void __fastcall TReportReportForm::DetailBand1BeforePrint(
      TQRCustomBand *Sender, bool &PrintBand)
{
  // Oddelujici caru zobrazim pouze kaqzdsy 7. den (oddeluji tydny)
  Counter++;
  CaraQRShape->Enabled = (bool)(Counter % 7 == 0);
}
//---------------------------------------------------------------------------
