//----------------------------------------------------------------------------
#ifndef RecordReportH
#define RecordReportH
//----------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\QuickRpt.hpp>
#include <vcl\QRCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Qrctrls.hpp>
//----------------------------------------------------------------------------
class TRecordReportForm : public TQuickRep
{
__published:
        TQRBand *TitleBand1;
        TQRLabel *QRLabel1;
        TQRSysData *QRSysData1;
        TQRLabel *QRLabel11;
        TQRLabel *InsertedQRLabel;
        TQRLabel *QRLabel2;
        TQRLabel *QRLabel3;
        TQRLabel *LastEditQRLabel;
        TQRLabel *GsmNumberQRLabel;
        TQRLabel *QRLabel6;
        TQRLabel *QRLabel7;
        TQRLabel *QRLabel8;
        TQRLabel *QRLabel9;
        TQRLabel *ScaleQRLabel;
        TQRLabel *DayQRLabel;
        TQRLabel *DateQRLabel;
        TQRLabel *NoteQRLabel;
        TQRLabel *FemalesQRLabel;
        TQRLabel *QRLabel12;
        TQRLabel *QRLabel13;
        TQRLabel *QRLabel14;
        TQRLabel *QRLabel15;
        TQRLabel *QRLabel16;
        TQRLabel *QRLabel17;
        TQRLabel *FemalesCountQRLabel;
        TQRLabel *FemalesAverageQRLabel;
        TQRLabel *FemalesGainQRLabel;
        TQRLabel *FemalesSigmaQRLabel;
        TQRLabel *FemalesCvQRLabel;
        TQRLabel *FemalesUniQRLabel;
        TQRLabel *MalesCountQRLabel;
        TQRLabel *MalesAverageQRLabel;
        TQRLabel *MalesGainQRLabel;
        TQRLabel *MalesSigmaQRLabel;
        TQRLabel *MalesCvQRLabel;
        TQRLabel *MalesUniQRLabel;
        TQRLabel *MalesQRLabel;
private:
public:
   __fastcall TRecordReportForm::TRecordReportForm(TComponent* Owner);

        void SetMalesVisible(bool Visible);

};
//----------------------------------------------------------------------------
extern TRecordReportForm *RecordReportForm;
//----------------------------------------------------------------------------
#endif