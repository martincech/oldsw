//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusPortSetup.h"
#include "Konstanty.h"
#include "DM.h"

#pragma pack(push, 1)           // byte alignment
  #include "c51/Bat2.h"         // flash data definition
#pragma pack(pop)               // original alignment

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TModbusPortSetupForm *ModbusPortSetupForm;

//---------------------------------------------------------------------------
// Kontrola zadanych hodnot
//---------------------------------------------------------------------------

void TModbusPortSetupForm::CheckEdit(TEdit *Edit, int Min, int Max) {
  // Kontrola hodnoty v editu <Edit>. V pripade chyby hodi vyjimku.
  int Value;

  try {
    Value = Edit->Text.ToInt();
    if (Value < Min || Value > Max) {
      throw 1;
    }
  } catch(...) {
    Edit->SetFocus();
    throw;
  }
}

bool TModbusPortSetupForm::CheckValues() {
  // Kontrola zadanych hodnot
  int Value;

  try {
    if (SpeedComboBox->ItemIndex < 0) {
      SpeedComboBox->SetFocus();
      throw 1;
    }
    if (ParityComboBox->ItemIndex < 0) {
      ParityComboBox->SetFocus();
      throw 1;
    }
    if (ProtocolComboBox->ItemIndex < 0) {
      ProtocolComboBox->SetFocus();
      throw 1;
    }
    // Hodnoty cekani se lisi od vahy
    CheckEdit(ReplyDelayEdit,     MIN_MODBUS_REPLY_DELAY,     MAX_MODBUS_REPLY_DELAY);
    CheckEdit(SilentIntervalEdit, MIN_MODBUS_SILENT_INTERVAL, MAX_MODBUS_SILENT_INTERVAL);
    if (RepetitionsComboBox->ItemIndex < 0) {
      RepetitionsComboBox->SetFocus();
      throw 1;
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(), LoadStr(CHYBA).c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return false;
  }
  return true;  // Vse je zadane v poradku
}

//---------------------------------------------------------------------------
__fastcall TModbusPortSetupForm::TModbusPortSetupForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TModbusPortSetupForm::OkButtonClick(TObject *Sender)
{
  // Kontrola zadanych hodnot
  if (!CheckValues()) {
    return;
  }

  // Ulozim do lokalnich promennych
  switch (SpeedComboBox->ItemIndex) {
    case RS485_SPEED_1200:
      Data->Nastaveni.Modbus.Speed = 1200;
      break;
    case RS485_SPEED_2400:
      Data->Nastaveni.Modbus.Speed = 2400;
      break;
    case RS485_SPEED_4800:
      Data->Nastaveni.Modbus.Speed = 4800;
      break;
    case RS485_SPEED_9600:
      Data->Nastaveni.Modbus.Speed = 9600;
      break;
    case RS485_SPEED_19200:
      Data->Nastaveni.Modbus.Speed = 19200;
      break;
    case RS485_SPEED_38400:
      Data->Nastaveni.Modbus.Speed = 38400;
      break;
    case RS485_SPEED_57600:
      Data->Nastaveni.Modbus.Speed = 57600;
      break;
    case RS485_SPEED_115200:
      Data->Nastaveni.Modbus.Speed = 115200;
      break;
  }
  Data->Nastaveni.Modbus.Parity         = (TParity)ParityComboBox->ItemIndex;
  Data->Nastaveni.Modbus.Protocol       = (TProtocol)ProtocolComboBox->ItemIndex;
  Data->Nastaveni.Modbus.ReplyDelay     = ReplyDelayEdit->Text.ToInt();
  Data->Nastaveni.Modbus.SilentInterval = SilentIntervalEdit->Text.ToInt();
  Data->Nastaveni.Modbus.Repetitions    = RepetitionsComboBox->ItemIndex + 1;

  // Ulozim do DB
  Data->NastaveniTable->Open();
  Data->NastaveniTable->Edit();
  Data->NastaveniTable->FieldByName("MODBUS_SPEED")->AsInteger           = Data->Nastaveni.Modbus.Speed;
  Data->NastaveniTable->FieldByName("MODBUS_PARITY")->AsInteger          = Data->Nastaveni.Modbus.Parity;
  Data->NastaveniTable->FieldByName("MODBUS_REPLAY_DELAY")->AsInteger    = Data->Nastaveni.Modbus.ReplyDelay;
  Data->NastaveniTable->FieldByName("MODBUS_SILENT_INTERVAL")->AsInteger = Data->Nastaveni.Modbus.SilentInterval;
  Data->NastaveniTable->FieldByName("MODBUS_PROTOCOL")->AsInteger        = Data->Nastaveni.Modbus.Protocol;
  Data->NastaveniTable->FieldByName("MODBUS_REPETITIONS")->AsInteger     = Data->Nastaveni.Modbus.Repetitions;
  Data->NastaveniTable->Post();
  Data->NastaveniTable->Close();

  // Predam prametry MODBUSu
  Data->SetModbusPortParameters();

  ModalResult = mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TModbusPortSetupForm::FormShow(TObject *Sender)
{
  switch (Data->Nastaveni.Modbus.Speed) {
    case 1200:
      SpeedComboBox->ItemIndex = RS485_SPEED_1200;
      break;
    case 2400:
      SpeedComboBox->ItemIndex = RS485_SPEED_2400;
      break;
    case 4800:
      SpeedComboBox->ItemIndex = RS485_SPEED_4800;
      break;
    case 9600:
      SpeedComboBox->ItemIndex = RS485_SPEED_9600;
      break;
    case 19200:
      SpeedComboBox->ItemIndex = RS485_SPEED_19200;
      break;
    case 38400:
      SpeedComboBox->ItemIndex = RS485_SPEED_38400;
      break;
    case 57600:
      SpeedComboBox->ItemIndex = RS485_SPEED_57600;
      break;
    case 115200:
      SpeedComboBox->ItemIndex = RS485_SPEED_115200;
      break;
    default:
      SpeedComboBox->ItemIndex = DEFAULT_RS485_SPEED;
  }
  ParityComboBox->ItemIndex      = Data->Nastaveni.Modbus.Parity;
  ProtocolComboBox->ItemIndex    = Data->Nastaveni.Modbus.Protocol;
  ReplyDelayEdit->Text           = Data->Nastaveni.Modbus.ReplyDelay;
  SilentIntervalEdit->Text       = Data->Nastaveni.Modbus.SilentInterval;
  RepetitionsComboBox->ItemIndex = Data->Nastaveni.Modbus.Repetitions - 1;

  SpeedComboBox->SetFocus();
}
//---------------------------------------------------------------------------

