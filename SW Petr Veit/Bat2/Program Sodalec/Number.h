//---------------------------------------------------------------------------

#ifndef NumberH
#define NumberH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TNumberForm : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TListBox *NumbersListBox;
        TButton *NewButton;
        TButton *DeleteButton;
        TGroupBox *GroupBox2;
        TEdit *NumberEdit;
        TButton *OkButton;
        TButton *Button2;
        void __fastcall NumberEditKeyPress(TObject *Sender, char &Key);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall OkButtonClick(TObject *Sender);
        void __fastcall NumbersListBoxClick(TObject *Sender);
        void __fastcall NewButtonClick(TObject *Sender);
        void __fastcall DeleteButtonClick(TObject *Sender);
        void __fastcall NumbersListBoxDblClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        void ReadNumbers();
        String DecodeNumber();

        
        __fastcall TNumberForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TNumberForm *NumberForm;
//---------------------------------------------------------------------------
#endif
