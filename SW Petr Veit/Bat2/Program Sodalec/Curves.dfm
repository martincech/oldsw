object CurvesForm: TCurvesForm
  Left = 262
  Top = 181
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Growth curves'
  ClientHeight = 469
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CurvesGroupBox: TGroupBox
    Left = 16
    Top = 80
    Width = 233
    Height = 369
    Caption = 'Available curves'
    TabOrder = 0
    object CurvesListBox: TListBox
      Left = 16
      Top = 24
      Width = 201
      Height = 257
      ItemHeight = 13
      TabOrder = 0
      OnClick = CurvesListBoxClick
    end
    object NewCurveButton: TButton
      Left = 16
      Top = 296
      Width = 93
      Height = 25
      Caption = 'New'
      TabOrder = 1
      OnClick = NewCurveButtonClick
    end
    object DeleteCurveButton: TButton
      Left = 124
      Top = 328
      Width = 93
      Height = 25
      Caption = 'Delete'
      TabOrder = 4
      OnClick = DeleteCurveButtonClick
    end
    object RenameCurveButton: TButton
      Left = 16
      Top = 328
      Width = 93
      Height = 25
      Caption = 'Rename'
      TabOrder = 3
      OnClick = RenameCurveButtonClick
    end
    object CopyCurveButton: TButton
      Left = 124
      Top = 296
      Width = 93
      Height = 25
      Caption = 'Copy'
      TabOrder = 2
      OnClick = CopyCurveButtonClick
    end
  end
  object DefinitionGroupBox: TGroupBox
    Left = 272
    Top = 80
    Width = 257
    Height = 337
    Caption = 'Definition'
    TabOrder = 1
    object CurveStringGrid: TStringGrid
      Left = 16
      Top = 24
      Width = 225
      Height = 257
      ColCount = 3
      DefaultRowHeight = 16
      RowCount = 31
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
      TabOrder = 0
      ColWidths = (
        25
        85
        84)
      RowHeights = (
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16
        16)
    end
    object SaveCurveButton: TButton
      Left = 148
      Top = 296
      Width = 93
      Height = 25
      Caption = 'Save'
      TabOrder = 1
      OnClick = SaveCurveButtonClick
    end
  end
  object DecriptionPanel: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 65
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 513
      Height = 49
      AutoSize = False
      Caption = 
        'Here you can define theoretical growth curves supplied by the pr' +
        'oducer of your poultry. These curves can be then compared with r' +
        'eal results.'
      WordWrap = True
    end
  end
  object OkButton: TButton
    Left = 359
    Top = 432
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 3
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 454
    Top = 432
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
