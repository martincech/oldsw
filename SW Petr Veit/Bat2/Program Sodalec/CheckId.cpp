#include "DM.h"


//------------------------------------------------------------------------------
// Kontrola identifikacniho cisla
//------------------------------------------------------------------------------

#define VALID_CHARS_COUNT       10
static const char ValidChars[VALID_CHARS_COUNT] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

static bool CheckChar(char ch) {
  for (int i = 0; i < VALID_CHARS_COUNT; i++) {
    if (ch == ValidChars[i]) {
      return true;
    }
  }
  return false;         // Nenasel jsem
}

bool CheckId(AnsiString Id) {
  if (Id.Length() > ID_MAX_LENGTH) {
    return false;
  }

  for (int i = 1; i < Id.Length(); i++) {
    if (!CheckChar(Id[i])) {
      return false;
    }
  }

  return true;
}


//------------------------------------------------------------------------------
// Vynechani mezer
//------------------------------------------------------------------------------

AnsiString GetIdWithoutSpaces(AnsiString Id) {
  // Smazu mezery nalevo
  Id = Id.TrimLeft();

  if (Id == "") {
    // ID je sama mezera, to by nemelo nastat
    return "";     // Vratim prazdny string
  }

  for (int i = 1; i <= Id.Length(); i++) {
    if (Id[i] == TEXT_SPACE) {
      // Narazil jsem na 1. mezeru, zde zakoncim string
      return Id.SetLength(i);
    }
  }

  // Napravo nebyla zadna mezera
  return Id;
}


//------------------------------------------------------------------------------
// Prevod identifikacniho cisla na string
//------------------------------------------------------------------------------

AnsiString GetId(byte *Id) {
  char s[ID_MAX_LENGTH + 1];    // Vcetne zakoncovaci nuly

  for (int i = 0; i < ID_MAX_LENGTH; i++) {
    s[i] = Id[i];
  }
  s[ID_MAX_LENGTH] = 0;       // Zakoncovaci nula

  return GetIdWithoutSpaces(AnsiString(s));
}


//------------------------------------------------------------------------------
// Prevod identifikacniho cisla ze stringu na pole znaku bez zakoncovaci nuly
//------------------------------------------------------------------------------

void GetId(AnsiString Id, byte *s) {
  // Vynuluju cele pole
  for (int i = 0; i < ID_MAX_LENGTH; i++) {
    s[i] = TEXT_SPACE;
  }

  // Ulozim jednotlive znaky zprava
  s += ID_MAX_LENGTH - Id.Length();
  for (int i = 1; i <= Id.Length(); i++) {
    *s = Id[i];
    s++;
  }

  // V configu je to bez zakoncovaci nuly
}
