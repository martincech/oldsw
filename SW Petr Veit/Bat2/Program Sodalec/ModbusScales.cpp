//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusScales.h"
#include "Konstanty.h"
#include "DM.h"
#include "ModbusEditScale.h"
#include "Dpi.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TModbusScalesForm *ModbusScalesForm;

//---------------------------------------------------------------------------
// Smazani tabulky
//---------------------------------------------------------------------------

void TModbusScalesForm::ClearTable() {
  // Vymazu celou tabulku (nemazu zahlavi)
  for (int row = 1; row < ScalesStringGrid->RowCount; row++) {
    for (int col = 0; col < ScalesStringGrid->ColCount; col++) {
      ScalesStringGrid->Cells[col][row] = "";
    }
  }
  ScalesStringGrid->RowCount = 2;
}

//---------------------------------------------------------------------------
// Zobrazeni seznamu vah
//---------------------------------------------------------------------------

void TModbusScalesForm::DisplayScaleList() {
  TModbusScale *Scale;

  // Vymazu celou tabulku (nemazu zahlavi)
  ClearTable();

  // Nastavim spravny pocet radku
  if (ScaleList->Count() == 0) {
    ScalesStringGrid->RowCount = 2;             // Minimalne 2 radky, aby zustala zachovana hlavicka tabulky
    return;
  }
  ScalesStringGrid->RowCount = ScaleList->Count() + 1;

  for (int Index = 0; Index < ScaleList->Count(); Index++) {
    // Nactu ukazatel na vahu ze seznamu
    if ((Scale = ScaleList->GetScale(Index)) == NULL) {
      return;
    }

    // Vyplnim tabulku
    ScalesStringGrid->Cells[0][Index + 1] = AnsiString(Scale->GetAddress());
    ScalesStringGrid->Cells[1][Index + 1] = LoadStr(STR_RS485_PORT_PREFIX) + AnsiString(Scale->GetPort());
    ScalesStringGrid->Cells[2][Index + 1] = Scale->GetName();
  }  
}

//---------------------------------------------------------------------------
__fastcall TModbusScalesForm::TModbusScalesForm(TComponent* Owner)
        : TForm(Owner)
{
  ScaleList = new TModbusScaleList();

  // Nastavim vysku radku vsech StringGridu podle aktualniho DPI
  DpiSetStringGrid(ScalesStringGrid);
}
//---------------------------------------------------------------------------
void __fastcall TModbusScalesForm::FormShow(TObject *Sender)
{
  // Nahraju vsechny vahy z databaze
  Data->LoadModbusScalesFromDatabase(ScaleList);

  // Popis tabulky
  ScalesStringGrid->Cells[0][0] = LoadStr(STR_RS485_ADDRESS);
  ScalesStringGrid->Cells[1][0] = LoadStr(STR_RS485_PORT);
  ScalesStringGrid->Cells[2][0] = LoadStr(STR_RS485_NAME);

  // Zobrazim seznam vah
  DisplayScaleList();
  ScalesStringGrid->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TModbusScalesForm::NewButtonClick(TObject *Sender)
{
  int Address;

  // Zeptam se na parametry nove vahy
  ModbusEditScaleForm->AddressComboBox->ItemIndex = 0;
  ModbusEditScaleForm->PortComboBox->ItemIndex    = 0;
  ModbusEditScaleForm->NameEdit->Text             = "";
  if (ModbusEditScaleForm->ShowModal() != mrOk) {
    return;
  }

  // Zkontroluju, zda uz vaha neexistuje
  Address = ModbusEditScaleForm->AddressComboBox->ItemIndex + 1;
  if (ScaleList->Exists(Address)) {
    // Vaha uz v seznamu existuje
    MessageBox(NULL,LoadStr(STR_RS485_EXISTS).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Zalozim novou vahu
  if (!ScaleList->Add(Address, ModbusEditScaleForm->PortComboBox->ItemIndex + 1, ModbusEditScaleForm->NameEdit->Text)) {
    return;             // Nemelo by selhat, adresu jsem uz kontroloval drive
  }

  // Obnovim zobrazeni seznamu vah
  DisplayScaleList();

  // Vyberu prave zalozenou krivku v seznamu
  ScalesStringGrid->Row = ScaleList->GetIndex(Address) + 1;
  ScalesStringGrid->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TModbusScalesForm::FormDestroy(TObject *Sender)
{
  delete ScaleList;
}
//---------------------------------------------------------------------------


void __fastcall TModbusScalesForm::EditButtonClick(TObject *Sender)
{
  int NewAddress;
  TModbusScale *Scale;
  int Index;

  if (ScaleList->Count() == 0) {
    return;             // Neni co editovat
  }

  // Nactu vahu, na ktere prave stoji
  Index = ScalesStringGrid->Row - 1;
  Scale = ScaleList->GetScale(Index);

  // Edituju parametry vahy
  ModbusEditScaleForm->AddressComboBox->ItemIndex = Scale->GetAddress() - 1;
  ModbusEditScaleForm->PortComboBox->ItemIndex    = Scale->GetPort() - 1;
  ModbusEditScaleForm->NameEdit->Text             = Scale->GetName();
  if (ModbusEditScaleForm->ShowModal() != mrOk) {
    return;
  }

  // Zkontroluju, zda uz vaha neexistuje
  NewAddress = ModbusEditScaleForm->AddressComboBox->ItemIndex + 1;
  if (NewAddress != Scale->GetAddress()) {
    // Zmenil i adresu => nova adresa nesmi existovat
    if (ScaleList->Exists(NewAddress)) {
      // Vaha uz v seznamu existuje
      MessageBox(NULL,LoadStr(STR_RS485_EXISTS).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
      return;
    }
  }// else adresu nezmenil, nic netestuju

  // Smazu puvodni vahu
  if (!ScaleList->Delete(Index)) {
    return;
  }

  // Zalozim vahu s novymi parametry
  if (!ScaleList->Add(NewAddress, ModbusEditScaleForm->PortComboBox->ItemIndex + 1, ModbusEditScaleForm->NameEdit->Text)) {
    return;
  }

  // Obnovim zobrazeni seznamu vah
  DisplayScaleList();

  // Vyberu prave editovanou vahu v seznamu
  ScalesStringGrid->Row = Index + 1;
  ScalesStringGrid->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TModbusScalesForm::OkButton1Click(TObject *Sender)
{
  Data->SaveModbusScalesToDatabase(ScaleList);
  ModalResult = mrOk;        
}
//---------------------------------------------------------------------------

void __fastcall TModbusScalesForm::ScalesStringGridDblClick(TObject *Sender)
{
  EditButtonClick(NULL);        
}
//---------------------------------------------------------------------------

void __fastcall TModbusScalesForm::DeleteButtonClick(TObject *Sender)
{
  int Index;
  TModbusScale *Scale;

  if (ScaleList->Count() == 0) {
    return;             // Neni co mazat
  }

  // Nactu vahu, na ktere prave stoji
  Index = ScalesStringGrid->Row - 1;
  Scale = ScaleList->GetScale(Index);

  // Zeptam se
  AnsiString Str;
  Str.printf(LoadStr(STR_RS485_DELETE).c_str(), AnsiString(Scale->GetAddress()) + ": " + Scale->GetName());
  if (MessageBox(NULL, Str.c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
    return;
  }

  // Smazu vahu, na ktere prave stoji
  if (!ScaleList->Delete(Index)) {
    return;
  }

  // Obnovim zobrazeni seznamu vah
  DisplayScaleList();

  // Vyberu predchozi krivku v seznamu
  if (ScaleList->Count() == 0) {
    ScalesStringGrid->Row = 1;
  } else {
    if (Index < ScaleList->Count()) {
      ScalesStringGrid->Row = Index + 1;
    } else {
      ScalesStringGrid->Row = Index;
    }
  }
  ScalesStringGrid->SetFocus();
}
//---------------------------------------------------------------------------


void __fastcall TModbusScalesForm::ScalesStringGridKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if (Key == 46) {
    // Delete
    DeleteButtonClick(NULL);
  }
}
//---------------------------------------------------------------------------

