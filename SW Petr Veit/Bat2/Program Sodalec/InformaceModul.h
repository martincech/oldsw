//---------------------------------------------------------------------------

#ifndef InformaceModulH
#define InformaceModulH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TInformaceModulForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TButton *StartButton;
        TButton *Button2;
        TLabel *IdLabel;
        TLabel *VerzeLabel;
        TBevel *Bevel1;
        TLabel *Label4;
        TLabel *DataLabel;
private:	// User declarations
public:		// User declarations
        __fastcall TInformaceModulForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TInformaceModulForm *InformaceModulForm;
//---------------------------------------------------------------------------
#endif
