// Hlavni okno
#define HLAVNI_SPATNA_HODNOTA           1003

#define HLAVNI_EXCEL_NENI               1101

#define HLAVNI_JAZYK_RESTART            1201

// Nastaveni vah
#define NASTAVENIVAH_NOVA_SADA          2003
#define NASTAVENIVAH_NOVA_SADA_ZADEJTE  2004
#define NASTAVENIVAH_SADA_EXISTUJE      2005
#define NASTAVENIVAH_NOVE_HEJNO         2006
#define NASTAVENIVAH_NOVE_HEJNO_ZADEJTE 2007
#define NASTAVENIVAH_HEJNO_EXISTUJE     2008
#define NASTAVENIVAH_SMAZAT_HEJNO       2009
#define NASTAVENIVAH_SMAZAT_HEJNO_TEXT  2010
#define NASTAVENIVAH_SMAZAT_SADU        2011
#define NASTAVENIVAH_SMAZAT_SADU_TEXT   2012
#define NASTAVENIVAH_UPLOAD_OK          2013

// Databaze SMS
#define SMS_ALL_SCALES                  3001
#define SMS_DELETE_RECORD               3002
#define SMS_FILTER_OVERWRITE            3003
#define SMS_FILTER_DELETE               3004
#define SMS_FILTER_CREATE_AFTER_COPY    3005
#define SMS_CONNECTING                  3006
#define SMS_SENDING                     3007
#define SMS_WAITING                     3008
#define SMS_READING                     3009
#define SMS_MESSAGES                    3010
#define SMS_NUMBER_EXISTS               3011
#define SMS_NUMBER_DELETE               3012

// HW verze vah (od verze 1.10 a vyssi)
#define STR_HW_VERSION_GSM              4001
#define STR_HW_VERSION_LW1              4002
#define STR_HW_VERSION_MODBUS           4003

// Rustove krivky
#define STR_GROWTH_CURVES_NAME          5001
#define STR_GROWTH_CURVES_EXISTS        5002
#define STR_GROWTH_CURVES_DELETE        5003

// Seznam vah RS-485
#define STR_RS485_ADDRESS               6001
#define STR_RS485_PORT                  6002
#define STR_RS485_NAME                  6003
#define STR_RS485_PORT_PREFIX           6004
#define STR_RS485_EXISTS                6005
#define STR_RS485_DELETE                6006
#define STR_RS485_SCALE                 6007
#define STR_RS485_DATE_TIME             6008
#define STR_RS485_MODE                  6009
#define STR_RS485_AVERAGE               6010
#define STR_RS485_STOPPED               6011
#define STR_RS485_WAITING               6012
#define STR_RS485_WEIGHING              6013
#define STR_RS485_PAUSED                6014
#define STR_RS485_DAILY_GAIN            6015
#define STR_RS485_UNIFORMITY            6016
#define STR_RS485_SET_DATETIME          6017
#define STR_RS485_STOP_WEIGHING         6018
#define STR_RS485_ERRORS                6019
#define STR_RS485_WAIT                  6020
#define STR_RS485_OPEN_FILE             6021

// Obecne
#define CHYBA                           60001
#define CHYBA_SERIOVYPORT               60002
#define CHYBA_PRIPOJENI                 60003
#define CHYBA_MODUL                     60004
#define SAMICE                          60005
#define SAMEC                           60006
#define KG                              60007
#define LB                              60008
#define DEN                             60009
#define HODINA                          60010
#define HMOTNOST                        60011
#define POHLAVI                         60012
#define POCET                           60013
#define CILOVA_HMOTNOST                 60014
#define DATUM                           60015
#define SOUBOR                          60016
#define VAZENI                          60017
#define ONLINE                          60018
#define NONE                            60019
#define WEIGHT_DIFFERENCE               60020

