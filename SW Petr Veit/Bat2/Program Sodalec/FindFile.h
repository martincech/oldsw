//---------------------------------------------------------------------------

#ifndef FindFileH
#define FindFileH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <FileCtrl.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "DM.h"
//---------------------------------------------------------------------------
class TFindFileForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel3;
        TGroupBox *GroupBox3;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TEdit *ScalesEdit;
        TEdit *DayEdit;
        TDateTimePicker *FromDateTimePicker;
        TDateTimePicker *TillDateTimePicker;
        TGroupBox *GroupBox1;
        TDriveComboBox *DriveComboBox;
        TDirectoryListBox *DirectoryListBox;
        TButton *SearchButton;
        TButton *StopButton;
        TPanel *Panel1;
        TGroupBox *GroupBox2;
        TListBox *FilesListBox;
        TPanel *Panel2;
        TPanel *Panel4;
        TPanel *Panel5;
        TPanel *Panel6;
        TPanel *Panel7;
        TPanel *Panel9;
        TPanel *Panel10;
        TPanel *Panel8;
        TButton *OpenButton;
        TPanel *Panel11;
        TButton *Button1;
        TCheckBox *FromCheckBox;
        TCheckBox *TillCheckBox;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall SearchButtonClick(TObject *Sender);
        void __fastcall ScalesEditKeyPress(TObject *Sender, char &Key);
        void __fastcall StopButtonClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall OpenButtonClick(TObject *Sender);
private:	// User declarations
        bool Stopped;   // YES/NO zrusil vyhledavani

public:		// User declarations
        __fastcall TFindFileForm(TComponent* Owner);

        bool CheckValues();
        bool FileFitsFilter(AnsiString Scale, int Day, TDate From, TDate Till);
        void FindFiles(String Directory, AnsiString Scale, int Day, TDate From, TDate Till, TListBox *ListBox);

};
//---------------------------------------------------------------------------
extern PACKAGE TFindFileForm *FindFileForm;
//---------------------------------------------------------------------------
#endif
