//---------------------------------------------------------------------------


#ifndef ScaleSetupFrmH
#define ScaleSetupFrmH
//---------------------------------------------------------------------------

#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <DBCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Mask.hpp>

#include "ScaleSetup.h"

//---------------------------------------------------------------------------
class TScaleSetupFrame : public TFrame
{
__published:	// IDE-managed Components
        TPageControl *ScaleSetupPageControl;
        TTabSheet *IdentificationTabSheet;
        TTabSheet *FlocksTabSheet;
        TTabSheet *StatisticsTabSheet;
        TTabSheet *GsmTabSheet;
        TTabSheet *Rs485TabSheet;
        TTabSheet *ScaleTabSheet;
        TTabSheet *BacklightTabSheet;
        TLabel *Label1;
        TEdit *IdentificationNumberEdit;
        TGroupBox *GroupBox1;
        TListBox *FlocksListBox;
        TButton *NewFlockButton;
        TButton *DeleteFlockButton;
        TGroupBox *GroupBox2;
        TLabel *Label22;
        TCheckBox *FlockGendersCheckBox;
        TCheckBox *FlockCurvesCheckBox;
        TLabel *Label24;
        TLabel *Label25;
        TEdit *FlockNameEdit;
        TEdit *FlockWeighFromEdit;
        TEdit *FlockWeighTillEdit;
        TPageControl *FlockCurvesPageControl;
        TTabSheet *FlockCurveFTabSheet;
        TStringGrid *FlockCurveFStringGrid;
        TButton *FlockCurveFLoadButton;
        TTabSheet *FlockCurveMTabSheet;
        TButton *FlockCurveMLoadButton;
        TTabSheet *FlockWeightTabSheet;
        TLabel *Label26;
        TPanel *FlockWeightMPanel;
        TLabel *Label27;
        TEdit *FlockWeightFEdit;
        TEdit *FlockWeightMEdit;
        TStringGrid *FlockCurveMStringGrid;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label5;
        TLabel *Label6;
        TEdit *StatisticsHistogramRangeEdit;
        TLabel *Label4;
        TEdit *StatisticsUniformityRangeEdit;
        TLabel *Label7;
        TCheckBox *GsmUseCheckBox;
        TCheckBox *GsmMidnightCheckBox;
        TLabel *Label8;
        TEdit *GsmPeriod1Edit;
        TLabel *Label9;
        TCheckBox *GsmRequestCheckBox;
        TCheckBox *GsmCheckCheckBox;
        TLabel *Label10;
        TEdit *GsmNumber1Edit;
        TEdit *GsmNumber2Edit;
        TEdit *GsmNumber3Edit;
        TEdit *GsmNumber4Edit;
        TEdit *GsmNumber5Edit;
        TLabel *Label11;
        TComboBox *Rs485SpeedComboBox;
        TLabel *Label12;
        TLabel *Label13;
        TComboBox *Rs485ParityComboBox;
        TLabel *Label14;
        TLabel *Label15;
        TEdit *Rs485ReplyDelayEdit;
        TLabel *Label16;
        TEdit *Rs485SilentIntervalEdit;
        TLabel *Label17;
        TLabel *Label18;
        TComboBox *Rs485ProtocolComboBox;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TLabel *Label23;
        TLabel *Label28;
        TLabel *Label29;
        TLabel *Label30;
        TEdit *ScaleMarginAboveFEdit;
        TEdit *ScaleMarginBelowFEdit;
        TEdit *ScaleMarginAboveMEdit;
        TEdit *ScaleMarginBelowMEdit;
        TEdit *ScaleFilterEdit;
        TEdit *ScaleStabilizationEdit;
        TEdit *ScaleStabilizationTimeEdit;
        TLabel *Label31;
        TLabel *Label32;
        TLabel *Label33;
        TLabel *Label34;
        TCheckBox *ScaleGainCheckBox;
        TLabel *Label35;
        TComboBox *ScaleSaveUponComboBox;
        TLabel *Label36;
        TComboBox *ScaleUnitsComboBox;
        TLabel *Label37;
        TComboBox *BacklightComboBox;
        TLabel *Label38;
        TLabel *Label39;
        TButton *SaveFlockButton;
        TTabSheet *CorrectionTabSheet;
        TLabel *Label40;
        TEdit *CorrectionDay1Edit;
        TEdit *CorrectionDay2Edit;
        TLabel *Label41;
        TEdit *CorrectionCorrectionEdit;
        TLabel *Label42;
        TCheckBox *CorrectionUseCheckBox;
        TLabel *Label43;
        TLabel *Label44;
        TLabel *Label45;
        TEdit *GsmDay1Edit;
        TLabel *Label47;
        TEdit *GsmPeriod2Edit;
        TLabel *Label48;
        TLabel *Label46;
        TDateTimePicker *GsmTimeDateTimePicker;
        void __fastcall FlocksListBoxClick(TObject *Sender);
        void __fastcall FlockCurveFLoadButtonClick(TObject *Sender);
        void __fastcall NewFlockButtonClick(TObject *Sender);
        void __fastcall DeleteFlockButtonClick(TObject *Sender);
        void __fastcall SaveFlockButtonClick(TObject *Sender);
        void __fastcall FlockGendersCheckBoxClick(TObject *Sender);

private:	// User declarations
        bool Rs485Mode;                 // Edituje nastaveni pres RS-485
        bool WeighingStarted;           // Vaha prave vazi
        int WeighedFlock;               // Cislo hejna, podle ktereho se vazi nebo -1
        TScaleSetup *ScaleSetup;        // Nastaveni, se kterym se pracuje

        TEdit *GsmNumberEdits[GSM_NUMBER_MAX_COUNT];    // Edity GSm cisel v poli

        bool FlockIsLoading;            // Prave se nahrava hejno, pozdrzim zobrazeni

        void DisplayFlockList();
        void ClearFlockDisplay();
        bool CheckHour(TEdit *Edit);
        bool CheckInitialWeight(TEdit *Edit);
        bool CheckDataInFlock();
        void ShowFlockTabs(bool UseCurves, bool UseBothGenders);
        void ShowFlock(int Index);
        bool SaveFlock(int Index);

public:		// User declarations
        __fastcall TScaleSetupFrame(TComponent* Owner);

        void SetEditMode(bool Rs485, bool Started, int FlockNumber);
        // Nastavi mod editace. <Rs485> = edituje se nastaveni RS-485 vahy, <Started> = vaha prave vazi.
        // V <FlockNumber> je cislo prave vazeneho hejna nebo -1, pokud se nevazi podle zadneho hejna.
        void LoadSetup(TScaleSetup *NewScaleSetup);
        // Zobrazi nastaveni <NewScaleSetup>

        bool CheckData();
        // Zkontroluje zadane hodnoty celeho nastaveni krome hejn. Pokud je nektera hodnota neplatna, nastavi focus na dany edit.
        bool SaveSetup();
        // Ulozi zadane nastaveni do <ScaleSetup>

};
//---------------------------------------------------------------------------
extern PACKAGE TScaleSetupFrame *ScaleSetupFrame;
//---------------------------------------------------------------------------
#endif
