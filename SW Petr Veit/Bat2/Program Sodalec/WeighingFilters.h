//---------------------------------------------------------------------------

#ifndef WeighingFiltersH
#define WeighingFiltersH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TWeighingFiltersForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TListBox *ListBox;
        TButton *NewButton;
        TButton *EditButton;
        TButton *DeleteButton;
        TButton *CloseButton;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall NewButtonClick(TObject *Sender);
        void __fastcall EditButtonClick(TObject *Sender);
        void __fastcall DeleteButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations

        String SelectedFilter;  // Filtr, ktery se ma po zobrazeni okna vybrat


        bool CheckFilterInTable(String Filter);
        void SaveFilter();

        void ReadFilters();


        __fastcall TWeighingFiltersForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TWeighingFiltersForm *WeighingFiltersForm;
//---------------------------------------------------------------------------
#endif
