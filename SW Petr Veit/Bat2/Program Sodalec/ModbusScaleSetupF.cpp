//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusScaleSetupF.h"
#include "Konstanty.h"
#include "DM.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "ScaleSetupFrm"
#pragma resource "*.dfm"
TModbusScaleSetupForm *ModbusScaleSetupForm;
//---------------------------------------------------------------------------
__fastcall TModbusScaleSetupForm::TModbusScaleSetupForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TModbusScaleSetupForm::FormShow(TObject *Sender)
{
  ScaleSetupFrame1->ScaleSetupPageControl->ActivePage = ScaleSetupFrame1->IdentificationTabSheet;
}
//---------------------------------------------------------------------------

void __fastcall TModbusScaleSetupForm::CancelButtonClick(TObject *Sender)
{
  ModalResult = mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TModbusScaleSetupForm::OkButtonClick(TObject *Sender)
{
  if (!ScaleSetupFrame1->CheckData()) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Ulozim zadane nastaveni
  ScaleSetupFrame1->SaveSetup();

  ModalResult = mrOk;
}
//---------------------------------------------------------------------------





