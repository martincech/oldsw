//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "NewNumber.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TNewNumberForm *NewNumberForm;
//---------------------------------------------------------------------------
__fastcall TNewNumberForm::TNewNumberForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TNewNumberForm::NumberEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57)) {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------
void __fastcall TNewNumberForm::FormShow(TObject *Sender)
{
  NumberEdit->Text = "";
  NumberEdit->SetFocus();
  NoteEdit->Text = "";
}
//---------------------------------------------------------------------------
void __fastcall TNewNumberForm::Button1Click(TObject *Sender)
{
  if (NumberEdit->Text.IsEmpty()) {
    NumberEdit->SetFocus();
    return;
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
