�
 TCURVESFORM 0#  TPF0TCurvesForm
CurvesFormLeftTop� BorderIconsbiSystemMenu BorderStylebsSingleCaptionGrowth curvesClientHeight�ClientWidth Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxCurvesGroupBoxLeftTopPWidth� HeightqCaptionAvailable curvesTabOrder  TListBoxCurvesListBoxLeftTopWidth� Height
ItemHeightTabOrder OnClickCurvesListBoxClick  TButtonNewCurveButtonLeftTop(Width]HeightCaptionNewTabOrderOnClickNewCurveButtonClick  TButtonDeleteCurveButtonLeft|TopHWidth]HeightCaptionDeleteTabOrderOnClickDeleteCurveButtonClick  TButtonRenameCurveButtonLeftTopHWidth]HeightCaptionRenameTabOrderOnClickRenameCurveButtonClick  TButtonCopyCurveButtonLeft|Top(Width]HeightCaptionCopyTabOrderOnClickCopyCurveButtonClick   	TGroupBoxDefinitionGroupBoxLeftTopPWidthHeightQCaption
DefinitionTabOrder TStringGridCurveStringGridLeftTopWidth� HeightColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsUT 
RowHeights   TButtonSaveCurveButtonLeft� Top(Width]HeightCaptionSaveTabOrderOnClickSaveCurveButtonClick   TPanelDecriptionPanelLeft Top Width HeightAAlignalTop
BevelOuterbvNoneEnabledTabOrder TLabelLabel1LeftTopWidthHeight1AutoSizeCaption�Here you can define theoretical growth curves supplied by the producer of your poultry. These curves can be then compared with real results.WordWrap	   TButtonOkButtonLeftgTop�WidthKHeightCaptionOKTabOrderOnClickOkButtonClick  TButtonCancelButtonLeft�Top�WidthKHeightCaptionCancelModalResultTabOrder   