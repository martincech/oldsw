//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusWait.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TModbusWaitForm *ModbusWaitForm;
//---------------------------------------------------------------------------
__fastcall TModbusWaitForm::TModbusWaitForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TModbusWaitForm::FormShow(TObject *Sender)
{
  Canceled = false;
}
//---------------------------------------------------------------------------

void __fastcall TModbusWaitForm::CancelButtonClick(TObject *Sender)
{
  Canceled = true;
}
//---------------------------------------------------------------------------

