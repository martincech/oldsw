//*****************************************************************************
//
//    Stat.c  -  Statistics utility
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "Stat.h"
#include <math.h>

#define MIN_AVERAGE        1e-03         // mensi hodnota povazovana za 0
#define MIN_SIGMA          1e-03         // mensi hodnota povazovana za 0
#define DEFAULT_UNIFORMITY 100           // implicitni uniformita (pri chybe)

#define INV_SQRT_2PI    0.3989422804     // 1/(SQRT(2*PI))

// Konstanty aproximacniho polynomu :
#define POLY_P          0.231641900
#define POLY_B1         0.319381530
#define POLY_B2        -0.356563782
#define POLY_B3         1.781477937
#define POLY_B4        -1.821255978
#define POLY_B5         1.330274429


// Promenne pro presnou uniformitu
#ifdef STAT_REAL_UNIFORMITY

static TNumber __xdata__ StatRealUniMin;
static TNumber __xdata__ StatRealUniMax;
static TNumber __xdata__ StatRealUniAverage;
static word __xdata__ StatRealUniCountIn;
static word __xdata__ StatRealUniCountOut;

#endif // STAT_REAL_UNIFORMITY


//******************************************************************************
// Mazani
//******************************************************************************

void StatClear( TStatistic __xdata__ *statistic)
// smaze statistiku
{
   statistic->XSuma  = 0;
   statistic->X2Suma = 0;
   statistic->Count  = 0;
} // StatClear

//******************************************************************************
// Pridani
//******************************************************************************

void StatAdd( TStatistic __xdata__ *statistic, TNumber x)
// prida do statistiky
{
   statistic->XSuma  += x;
   statistic->X2Suma += x * x;
   statistic->Count++;
} // StatAdd

//******************************************************************************
// Prumer
//******************************************************************************

TNumber StatAverage( TStatistic __xdata__ *statistic)
// vrati stredni hodnotu
{
   if( statistic->Count == 0){
      return( 0);
   }
   return( statistic->XSuma / statistic->Count);
} // StatAverage

//******************************************************************************
// Odchylka
//******************************************************************************

TNumber StatSigma( TStatistic __xdata__ *statistic)
// vrati smerodatnou odchylku
{
TNumber tmp;

   if (statistic->Count <= 1) {
      return( 0);
   }
   tmp = statistic->XSuma * statistic->XSuma / statistic->Count;
   tmp = statistic->X2Suma - tmp;
   if( tmp <= MIN_SIGMA){
      return( 0);
   }
   return( sqrt( 1/(TNumber)(statistic->Count - 1) * tmp));
} // StatSigma

//******************************************************************************
// Variace
//******************************************************************************

TNumber StatVariation( TNumber average, TNumber sigma)
// vrati variaci (procentni smerodatna odchylka)
{
   if( average < MIN_AVERAGE){
      return( 0);         // "deleni nulou"
   }
   return( sigma / average * 100);
} // StatVariation

#ifdef STAT_UNIFORMITY

//******************************************************************************
// Uniformita
//******************************************************************************

TNumber StatUniformity( TNumber average, TNumber sigma, byte UniformityWidth)
// vrati procentualni pocet vzorku padajicich do rozsahu stredni
// hodnoty +-UniformityWidth%
// Sirka <UniformityWidth> se zadava v %, 10 znamena +-10%
{
TNumber x, qx;        // normalizovana odchylka, gaussint

   if( sigma < MIN_SIGMA){
      return( DEFAULT_UNIFORMITY);      // "deleni nulou"
   }
   x = (TNumber)UniformityWidth / (TNumber)100 * average / sigma; // normalizace absolutni tolerance pomoci sigma
   if( x > 3){
      return( 100);                     // >3*sigma, vsechny hodnoty v toleranci
   }
   qx  = GaussIntegral( x, Gauss( x));  // plocha krivky nad pozadovanou mez
   qx *= 2;                             // plocha krivky nad a pod pozadovaou mez
   qx  = 1 - qx;                        // plocha uvnitr mezi
   return( qx * 100);                   // v procentech
} // StatUniformity

//******************************************************************************
// Gaussova krivka
//******************************************************************************

TNumber Gauss( TNumber x)
// gaussova krivka
{
   return( INV_SQRT_2PI * exp( -x * x / 2));
} // Gauss

//******************************************************************************
// Integrace Gaussovy krivky
//******************************************************************************

TNumber GaussIntegral( TNumber x, TNumber gaussx)
// gaussova krivka integrovana od <x> do +oo, <gaussx> je hodnota gaussovy
// krivky pro <x>
{
TNumber t;
TNumber tn;
TNumber poly;

   t = 1 /(1 + POLY_P * x); // zakladni mocnina
   poly  = POLY_B1 * t;     // prvni aproximace
   tn    = t*t;             // druha mocnina
   poly += POLY_B2 * tn;
   tn   *= t;               // treti mocnina
   poly += POLY_B3 * tn;
   tn   *= t;               // ctvrta mocnina
   poly += POLY_B4 * tn;
   tn   *= t;               // pata mocnina
   poly += POLY_B5 * tn;
   return( gaussx * poly);
} // GaussIntegral

#endif

//******************************************************************************
// Vypocet presne uniformity primo ze zadanych vzorku
//******************************************************************************

#ifdef STAT_REAL_UNIFORMITY

// Vyuziva tyto globalni promenne (kvuli rychlosti je nepredavam parametrem):
//  - TNumber StatRealUniMin: spodni mez okoli prumeru
//  - TNumber StatRealUniMax: horni mez okoli prumeru
//  - TNumber StatRealUniAverage: prumer celeho souboru vzorku, ze ktereho se uniformita pocita
//  - word StatRealUniCountIn: pocet vzorku v pasmu okoli
//  - word StatRealUniCountOut: pocet vzorku mimo pasmo okoli

void StatRealUniformityInit(TNumber Min, TNumber Max, TNumber Average) {
  // Inicializuje globalni promenne
  StatRealUniMin = Min;
  StatRealUniMax = Max;
  StatRealUniAverage = Average;
  StatRealUniCountOut = 0;
  StatRealUniCountIn = 0;
}

void StatRealUniformityAdd(TNumber Number) {
  // Prida novy vzorek do uniformity
  if (Number < StatRealUniMin || Number > StatRealUniMax) {
    // Vzorek je mimo pasmo
    StatRealUniCountOut++;
    return;
  }//if
  // Vzorek je v pasmu
  StatRealUniCountIn++;
}

byte StatRealUniformityGet() {
  // Vypocte uniformitu v celych procentech, vrati cislo 0 - 100
  if (StatRealUniCountIn == 0 && StatRealUniCountOut == 0) {
    return 100;
  } else {
    return (dword)(100L * (dword)StatRealUniCountIn / (dword)((dword)StatRealUniCountIn + (dword)StatRealUniCountOut));
  }//else
}

#endif // STAT_REAL_UNIFORMITY
