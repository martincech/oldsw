//*****************************************************************************
//
//    Stat.h  -  Statistics utility
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Stat_H__
   #define __Stat_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Popisovac statistiky :

typedef struct {
   TNumber XSuma;
   TNumber X2Suma;
   word    Count;
} TStatistic;  // 10 bajtu

//-----------------------------------------------------------------------------

void StatClear( TStatistic __xdata__ *statistic);
// smaze statistiku

void StatAdd( TStatistic __xdata__ *statistic, TNumber x);
// prida do statistiky

TNumber StatAverage( TStatistic __xdata__ *statistic);
// vrati stredni hodnotu

TNumber StatSigma( TStatistic __xdata__ *statistic);
// vrati smerodatnou odchylku

TNumber StatVariation( TNumber average, TNumber sigma);
// vrati variaci (procentni smerodatna odchylka)

TNumber StatUniformity( TNumber average, TNumber sigma, byte UniformityWidth);
// vrati procentualni pocet vzorku padajicich do rozsahu stredni
// hodnoty +-UniformityWidth*100%

TNumber Gauss( TNumber x);
// gaussova krivka

TNumber GaussIntegral( TNumber x, TNumber gaussx);
// gaussova krivka integrovana od <x> do +oo, <gaussx> je hodnota gaussovy
// krivky pro <x>

void StatRealUniformityInit(TNumber Min, TNumber Max, TNumber Average);
// Inicializuje globalni promenne

void StatRealUniformityAdd(TNumber Number);
// Prida novy vzorek do uniformity

byte StatRealUniformityGet();
// Vypocte uniformitu v celych procentech

#endif
