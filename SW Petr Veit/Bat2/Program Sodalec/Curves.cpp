//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Curves.h"
#include "CurveList.h"
#include "Konstanty.h"
#include "Input.h"
#include "c51\Bat2.h"
#include "DM.h"
#include "CurveGrid.h"
#include "Dpi.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCurvesForm *CurvesForm;


#define NAME_MAX_LENGTH         20              // Maximalni delka nazvu krivky


//---------------------------------------------------------------------------
// Zobrazeni seznamu krivek
//---------------------------------------------------------------------------

void TCurvesForm::DisplayCurveList() {
  CurvesListBox->Clear();
  for (int i = 0; i < CurveList->Count(); i++) {
    CurvesListBox->Items->Add(CurveList->GetCurve(i)->GetName());
  }
}

//---------------------------------------------------------------------------
// Zobrazeni obsahu krivky
//---------------------------------------------------------------------------

void TCurvesForm::ShowCurve(int Index) {
  // Zobrazi krivku na pozici <Index>
  TGrowthCurve *Curve;

  // Nactu ukazatel na krivku ze seznamu
  if ((Curve = CurveList->GetCurve(Index)) == NULL) {
    return;
  }
  CurveGridShow(CurveStringGrid, Curve);
}

//---------------------------------------------------------------------------
// Ulozeni obsahu krivky
//---------------------------------------------------------------------------

bool TCurvesForm::SaveCurve(int Index) {
  // Ulozi krivku na pozici <Index>
  TGrowthCurve *Curve;

  // Zjistim ukazatel na krivku
  if ((Curve = CurveList->GetCurve(Index)) == NULL) {
    return false;
  }

  if (!CurveGridSave(CurveStringGrid, Curve)) {
    return false;
  }

  return true;
}

//---------------------------------------------------------------------------
__fastcall TCurvesForm::TCurvesForm(TComponent* Owner)
        : TForm(Owner)
{
  CurveList = new TCurveList();

  // Nastavim vysku radku vsech StringGridu podle aktualniho DPI
  DpiSetStringGrid(CurveStringGrid);
}
//---------------------------------------------------------------------------
void __fastcall TCurvesForm::FormDestroy(TObject *Sender)
{
  delete CurveList;
}
//---------------------------------------------------------------------------

void __fastcall TCurvesForm::FormShow(TObject *Sender)
{
  // Nahraju vsechny krivky z databaze
  Data->LoadCurvesFromDatabase(CurveList);

  // Zobrazim seznam krivek
  DisplayCurveList();
  CurvesListBox->SetFocus();

  // Popis tabulky rustove krivky
  CurveGridShow(CurveStringGrid);

  // Najedu na prvni krivku a zobrazim jeji obsah
  CurvesListBox->SetFocus();
  if (CurvesListBox->Items->Count > 0) {
    // Nejaka krivka v seznamu je, zobrazim ji
    CurvesListBox->ItemIndex = 0;
    ShowCurve(CurvesListBox->ItemIndex);
  } else {
    // Seznam je prazdny, jen smazu obsah tabulky
    CurveGridClear(CurveStringGrid);
  }
}
//---------------------------------------------------------------------------

void __fastcall TCurvesForm::NewCurveButtonClick(TObject *Sender)
{
  // Zalozim novou krivku
  AnsiString NewName;

  InputForm->Caption        = Caption;
  InputForm->Label->Caption = LoadStr(STR_GROWTH_CURVES_NAME);
  InputForm->Edit->Clear();
  if (InputForm->ShowModal() != mrOk) {
    return;
  }

  NewName = InputForm->Edit->Text.SubString(1, NAME_MAX_LENGTH);                // Nazev muze mit max. NAME_MAX_LENGTH znaku

  // Kontrola jmena
  if (!CurveList->CheckName(NewName)) {
    // Jmeno ma neplatny format
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  if (CurveList->Exists(NewName)) {
    // Jmeno uz v seznamu existuje
    MessageBox(NULL,LoadStr(STR_GROWTH_CURVES_EXISTS).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Zalozim novou krivku
  if (!CurveList->Add(NewName)) {
    return;             // Nemelo by selhat, jmeno jsem uz kontroloval drive
  }

  // Obnovim zobrazeni seznamu krivek
  DisplayCurveList();

  // Vyberu prave zalozenou krivku v seznamu
  CurvesListBox->ItemIndex = CurveList->GetIndex(NewName);
  CurvesListBox->SetFocus();

  // Zobrazim obsah krivky do tabulky
  ShowCurve(CurvesListBox->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TCurvesForm::DeleteCurveButtonClick(TObject *Sender)
{
  // Smazu vybranou krivku
  if (CurvesListBox->ItemIndex < 0) {
    return;             // Zadna krivka neni vybrana
  }

  if (MessageBox(NULL, LoadStr(STR_GROWTH_CURVES_DELETE).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
    return;             // Nechce smazat
  }

  // Smazu vybranou krivku
  if (!CurveList->Delete(CurvesListBox->ItemIndex)) {
    return;
  }

  // Obnovim zobrazeni seznamu krivek
  DisplayCurveList();

  // Vyberu prvni krivku v seznamu
  CurvesListBox->ItemIndex = 0;
  CurvesListBox->SetFocus();
  ShowCurve(CurvesListBox->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TCurvesForm::RenameCurveButtonClick(TObject *Sender)
{
  // Prejmenuju vybranou krivku
  AnsiString OldName, NewName;

  if (CurvesListBox->ItemIndex < 0) {
    return;             // Zadna krivka neni vybrana
  }

  OldName = CurveList->GetCurve(CurvesListBox->ItemIndex)->GetName();

  // Zeptam se na nove jmeno
  InputForm->Caption         = Caption;
  InputForm->Label->Caption  = LoadStr(STR_GROWTH_CURVES_NAME);
  InputForm->Edit->Text      = OldName;
  InputForm->Edit->SelStart  = 0;               // Vyberu cely text
  InputForm->Edit->SelLength = 100;
  if (InputForm->ShowModal() != mrOk) {
    return;
  }

  NewName = InputForm->Edit->Text.SubString(1, NAME_MAX_LENGTH);        // Nazev muze mit max. NAME_MAX_LENGTH znaku

  if (NewName.UpperCase() == OldName.UpperCase()) {
    return;             // Jmeno nezmenil
  }

  // Kontrola jmena
  if (!CurveList->CheckName(NewName)) {
    // Jmeno ma neplatny format
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  if (CurveList->Exists(NewName)) {
    // Jmeno uz v seznamu existuje
    MessageBox(NULL,LoadStr(STR_GROWTH_CURVES_EXISTS).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Nastavim nove jmeno
  if (!CurveList->Rename(CurvesListBox->ItemIndex, NewName)) {
    return;
  }

  // Obnovim zobrazeni seznamu krivek
  DisplayCurveList();

  // Vyberu krivku v seznamu
  CurvesListBox->ItemIndex = CurveList->GetIndex(NewName);
  CurvesListBox->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TCurvesForm::SaveCurveButtonClick(TObject *Sender)
{
  // Ulozi obsah vybrane krivky
  if (CurvesListBox->ItemIndex < 0) {
    return;             // Zadna krivka neni vybrana
  }

  // Ulozim z tabulky
  if (!SaveCurve(CurvesListBox->ItemIndex)) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Zobrazim zformatovane hodnoty
  ShowCurve(CurvesListBox->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TCurvesForm::CurvesListBoxClick(TObject *Sender)
{
  // Zobrazim obsah vybrane krivky
  if (CurvesListBox->ItemIndex < 0) {
    return;             // Zadna krivka neni vybrana
  }

  // Zobrazim obsah krivky do tabulky
  ShowCurve(CurvesListBox->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TCurvesForm::CopyCurveButtonClick(TObject *Sender)
{
  // Zkopiruje obsah prave vybrane krivky do nove krivky
  AnsiString NewName;
  TGrowthCurve *OldCurve, *NewCurve;

  if (CurvesListBox->ItemIndex < 0) {
    return;             // Zadna krivka neni vybrana
  }

  InputForm->Caption        = Caption;
  InputForm->Label->Caption = LoadStr(STR_GROWTH_CURVES_NAME);
  InputForm->Edit->Clear();
  if (InputForm->ShowModal() != mrOk) {
    return;
  }

  NewName = InputForm->Edit->Text.SubString(1, NAME_MAX_LENGTH);                // Nazev muze mit max. NAME_MAX_LENGTH znaku

  // Kontrola jmena
  if (!CurveList->CheckName(NewName)) {
    // Jmeno ma neplatny format
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  if (CurveList->Exists(NewName)) {
    // Jmeno uz v seznamu existuje
    MessageBox(NULL,LoadStr(STR_GROWTH_CURVES_EXISTS).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Vytvorim novou krivku zkopirovanim z puvodni
  if (!CurveList->Add(CurveList->GetCurve(CurvesListBox->ItemIndex), NewName)) {
    return;
  }

  // Obnovim zobrazeni seznamu krivek
  DisplayCurveList();

  // Vyberu prave zalozenou krivku v seznamu
  CurvesListBox->ItemIndex = CurveList->GetIndex(NewName);
  CurvesListBox->SetFocus();

  // Zobrazim obsah krivky do tabulky
  ShowCurve(CurvesListBox->ItemIndex);
}
//---------------------------------------------------------------------------


void __fastcall TCurvesForm::OkButtonClick(TObject *Sender)
{
  // Ulozim krivky do databaze
  Data->SaveCurvesToDatabase(CurveList);
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------

