//******************************************************************************
//
//   KConvert.cpp  Keil data conversions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "KConvert.h"

//******************************************************************************
// Word
//******************************************************************************

word KConvertGetWord(  word X)
{
TDataConvertor dc;
byte           tmp;

   dc.w = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 1];
   dc.array[ 1] = tmp;
   return( dc.w);
} // GetWord

//******************************************************************************
// Int16
//******************************************************************************

int16 KConvertGetInt(   int16 X)
{
TDataConvertor dc;
byte           tmp;

   dc.w = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 1];
   dc.array[ 1] = tmp;
   return( (int16)dc.w);
} // GetInt

//******************************************************************************
// Dword
//******************************************************************************

dword KConvertGetDword( dword X)
{
TDataConvertor dc;
byte           tmp;

   dc.dw = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 3];
   dc.array[ 3] = tmp;
   tmp = dc.array[ 1];
   dc.array[ 1] = dc.array[ 2];
   dc.array[ 2] = tmp;
   return( dc.dw);
} // GetDword

//******************************************************************************
// Long32
//******************************************************************************

long32 KConvertGetLong(  long32 X)
{
TDataConvertor dc;
byte           tmp;

   dc.dw = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 3];
   dc.array[ 3] = tmp;
   tmp = dc.array[ 1];
   dc.array[ 1] = dc.array[ 2];
   dc.array[ 2] = tmp;
   return( (long32)dc.dw);
} // GetLong

//******************************************************************************
// Float
//******************************************************************************

float  KConvertGetFloat( float X)
{
TDataConvertor dc;
byte           tmp;

   dc.f = X;
   tmp = dc.array[ 0];
   dc.array[ 0] = dc.array[ 3];
   dc.array[ 3] = tmp;
   tmp = dc.array[ 1];
   dc.array[ 1] = dc.array[ 2];
   dc.array[ 2] = tmp;
   return( dc.f);
} // GetFloat

//******************************************************************************
// DateTime
//******************************************************************************

TDateTime KConvertGetDateTime( TLongDateTime X)
{
TDateTime Dt;
   Dt = 0.0;
   try {
      Dt = TDateTime( (word)KConvertGetWord(X.Year), (word)X.Month, (word)X.Day) +
           TDateTime( (word)X.Hour, (word)X.Min, (word)0, 0);
   } catch(...){
   }
   return( Dt);
} // GetDateTime

TLongDateTime KConvertPutDateTime( TDateTime X)
{
TLongDateTime Cas;
unsigned short Year, Month, Day, Hour, Min, Sec, Msec;

   X.DecodeDate( &Year, &Month, &Day);
   X.DecodeTime( &Hour, &Min, &Sec, &Msec);
   Cas.Min   = (byte)Min;
   Cas.Hour  = (byte)Hour;
   Cas.Day   = (byte)Day;
   Cas.Month = (byte)Month;
   Cas.Year  = (word)KConvertPutWord( Year);
   return( Cas);
} // PutDateTime

//******************************************************************************
// String
//******************************************************************************

AnsiString KConvertGetString( byte *S, int Length)
{
AnsiString As;

   if( Length == 0){
      // terminator 0
      As = AnsiString( (char *)S);
   } else {
      // omezeni delkou
      As = AnsiString( (char *)S, (unsigned char)Length);
   }
   As = As.TrimRight();         // odstraneni pravostrannych mezer
   return( As);
} // GetString

void KConvertPutString( byte *S, AnsiString X, int Length)
{
   if( Length == 0){
      IERROR;
   }
   for (int i = 0; i < Length; i++) {
     S[i] = ' ';                        // Vymezerovani, pozor: strnset() nelze pouzit (neprodlouzi retezec)
   }
   if( X.Length() < Length){
      Length = X.Length();             // kratsi retezec
   }
   strncpy( S, X.c_str(), Length);     // jen vyznamne znaky
} // PutString

//-----------------------------------------------------------------------------
// Prohozeni LSB a MSB
//-----------------------------------------------------------------------------

void KConvertSwapWord(word *Data) {
  // Prohodi LSB a MSB v <Data>
  *Data = KConvertGetWord(*Data);
}

void KConvertSwapDWord(dword *Data) {
  // Prohodi LSB a MSB v <Data>
  *Data = KConvertGetDword(*Data);
}

void KConvertSwapLong(long32 *Data) {
  // Prohodi LSB a MSB v <Data>
  *Data = KConvertGetLong(*Data);
}

void KConvertSwapFloat(float *Data) {
  // Prohodi LSB a MSB v <Data>
  // Konverzi pomoci KConvertGetFloat() muze vzniknout NAN a potom to haze vyjimku. Delam rucne pomoci dword.
  TDataConvertor dc;
  byte           tmp;

  dc.f = *Data;
  tmp = dc.array[ 0];
  dc.array[ 0] = dc.array[ 3];
  dc.array[ 3] = tmp;
  tmp = dc.array[ 1];
  dc.array[ 1] = dc.array[ 2];
  dc.array[ 2] = tmp;

  // Ulozim musim pomoci ukazatele na dword, hodnota float muze byt NAN
  dword *ptr = (dword *)Data;
  *ptr = dc.dw;
}

