//******************************************************************************
//
//   MFlash.cpp   Memory module Flash decoder
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MFlash.h"
#include <stdio.h>
#include "DM.h"
#include "AtFlash.h"


#define MTRIALS        3                // pocet pokusu o komunikaci
#define MREAD_PACKET CTECKA_MAX_PACKET  // velikost paketu pri cteni

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
//  Konstruktor
//******************************************************************************

TMFlash::TMFlash()
// Konstruktor
{
//   FAdapter = 0;
   FRawData = new byte[ FLASH_SIZE];       // kapacita flash
   FConfigSection    = (TConfigSection *)    &FRawData[ FL_CONFIG_BASE];
   FArchiv           = (TArchiveDailyInfo *) &FRawData[ FL_ARCHIVE_BASE];
   FOnline           = (TOnlineSample *)     &FRawData[ FL_ONLINE_BASE];
} // TMFlash

//******************************************************************************
// Destruktor
//******************************************************************************

TMFlash::~TMFlash()
// Destruktor
{
   if( FRawData){
      delete[] FRawData;
   }
} // ~TMFlash

//******************************************************************************
// Adapter
//******************************************************************************

bool TMFlash::CheckAdapter()
// Kontroluje adapter
{
  if (!UsbFlash) {
    return false;            // Nedefinovana ctacka
  }
  return UsbFlash->Connected();
} // CheckAdapter

//******************************************************************************
// Modul
//******************************************************************************

bool TMFlash::CheckModule()
// Otestuje pritomnost modulu
{
  // USB ctecka
  if (!UsbFlash) {
    return false;            // Nedefinovana ctacka
  }
  return UsbFlash->Connected();
} // CheckModule

//******************************************************************************
// Cteni konfigurace
//******************************************************************************

bool TMFlash::ReadConfigSection()
// Nacte konfiguracni sekci modulu
{
  // USB ctecka
  if (!UsbFlash) {
    return false;            // Nedefinovana ctacka
  }
  if (!UsbFlash->Connected()) {
    return false;
  }
  if (!UsbFlash->Read(FL_CONFIG_BASE, &FRawData[ FL_CONFIG_BASE], FL_CONFIG_SIZE)) {
    return false;
  }
  return true;
} // ReadConfigSection

//******************************************************************************
// Zapis konfigurace
//******************************************************************************

bool TMFlash::WriteConfigSection()
// Zapise konfiguracni sekci modulu
{
   // USB ctecka
   unsigned char VerifyBuffer[FL_CONFIG_SIZE];
   if (!UsbFlash) {
     return false;            // Nedefinovana ctacka
   }
   if (!UsbFlash->Connected()) {
     return false;
   }
   if (!UsbFlash->Write(FL_CONFIG_BASE, &FRawData[ FL_CONFIG_BASE], FL_CONFIG_SIZE)) {
     return false;
   }
   if (!UsbFlash->Read(FL_CONFIG_BASE, VerifyBuffer, FL_CONFIG_SIZE)) {
     return false;
   }
   for (int i = 0; i < FL_CONFIG_SIZE; i++) {
     if (VerifyBuffer[i] != FRawData[FL_CONFIG_BASE + i]) {
       return false;  // Verifikace neprobehla
     }
   }
   return true;
} // WriteConfigSection

//******************************************************************************
// Cteni start
//******************************************************************************

bool TMFlash::ReadData()
// Zahaji cteni dat, vraci false pri chybe
{
   // USB ctecka
   if (!UsbFlash) {
     return false;            // Nedefinovana ctacka
   }
   if (!UsbFlash->Connected()) {
     return false;
   }
   FReadOffset = 0;         // od zacatku Flash
   return( true);
} // ReadData

//******************************************************************************
// Cteni paketu
//******************************************************************************

bool TMFlash::ReadPacket( bool &Done)
// Precte data, vraci false pri chybe. Nastavi <Done> je-li nacteno vse
{
   Done = false;
   int Size = MREAD_PACKET;            // velikost paketu
   int Remainder = FLASH_SIZE - FReadOffset;

   if (Remainder == 0) {
      Done = true;                     // Uz nic nezbyva, vse je nacteno
      return true;
   } else if( Remainder < MREAD_PACKET){
      Done = true;                     // posledni cteni
      Size = Remainder;                // posledni paket muze byt kratsi
   }
   // USB ctecka
   if (!UsbFlash->Read(FReadOffset, &FRawData[ FReadOffset], Size)) {
     return false;
   }
   FReadOffset += Size;
   return true;
} // ReadPacket

//******************************************************************************
// Konec cteni
//******************************************************************************

void TMFlash::ReadFinalize()
// Uzavre proceduru cteni dat
{
   // pro jistotu nastavime ofsety :
   FConfigSection    = (TConfigSection *)    &FRawData[ FL_CONFIG_BASE];
   FArchiv           = (TArchiveDailyInfo *) &FRawData[ FL_ARCHIVE_BASE];
} // ReadFinalize

//******************************************************************************
//  Archiv
//******************************************************************************

void TMFlash::ArchivFirst()
// Nastavi archiv pred prvni zaznam
{
   // hledani markeru :
   for( int i = 0; i < FL_ARCHIVE_DAYS; i++){
      if( *(byte *)&FArchiv[ i] == FL_ARCHIVE_EMPTY){
         if( i == 0){
            FCurArchiv = -1;           // prazdny archiv
         } else {
            FCurArchiv  = 0;           // od zacatku
         }
         FLastArchiv = i;              // po znacku
         return;
      }
      if( *(byte *)&FArchiv[ i] == FL_ARCHIVE_FULL){
         if( i == FL_ARCHIVE_DAYS - 1){
            FCurArchiv = 0;            // znacka na konci
         } else {
            FCurArchiv = i + 1;        // zacatek za znackou
         }
         FLastArchiv = i;              // po znacku
         return;
      }
   }
   FCurArchiv = -1;                    // nenalezena znacka
} // ArchivFirst

TArchive *TMFlash::ArchivNext()
// Nasledujici zaznam (0=posledni)
{
   if( FCurArchiv < 0){
      return( 0);                      // konec
   }
   int Index = FCurArchiv;             // pamatuj aktualni pozici
   // vypocti nasledujici polozku :
   FCurArchiv++;
   if( FCurArchiv >= FL_ARCHIVE_DAYS){
      // jsme na konci
      FCurArchiv =  0;                 // pokracuje od zacatku
   }
   if( FCurArchiv == FLastArchiv){
      FCurArchiv = -1;                 // jsme na znacce, konec
   }
   FArchivDay = &FArchiv[ Index];
   return( &FArchivDay->Archive);
} // ArchivNext

//******************************************************************************
//  Zaznam vzorku
//******************************************************************************

void TMFlash::LoggerFirstSample()
// Nastavi logger pred prvni vzorek
{
   // hledani markeru :
   for( int i = 0; i < FL_LOGGER_SAMPLES; i++){
      if( *(byte *)&FArchivDay->Samples[ i] == LG_SAMPLE_EMPTY){
         if( i == 0){
            FCurSample = -1;           // prazdny archiv
         } else {
            FCurSample  = 0;           // od zacatku
         }
         FLastSample = i;              // po znacku
         return;
      }
      if( *(byte *)&FArchivDay->Samples[ i] == LG_SAMPLE_FULL){
         if( i == FL_LOGGER_SAMPLES - 1){
            FCurSample = 0;            // znacka na konci
         } else {
            FCurSample = i + 1;        // zacatek za znackou
         }
         FLastSample = i;              // po znacku
         return;
      }
   }
   FCurSample = -1;                    // nenalezena znacka
} // LoggerFirstSample

TLoggerSample *TMFlash::LoggerNextSample()
// Nastavi logger na dalsi vzorek (0=posledni)
{
   if( FCurSample < 0){
      return( 0);                      // konec
   }
   int Index = FCurSample;             // pamatuj aktualni pozici
   // vypocti nasledujici polozku :
   FCurSample++;
   if( FCurSample >= FL_LOGGER_SAMPLES){
      // jsme na konci
      FCurSample =  0;                 // pokracuje od zacatku
   }
   if( FCurSample == FLastSample){
      FCurSample = -1;                 // jsme na znacce, konec
   }
   return( &FArchivDay->Samples[ Index]);
} // LoggerNextSample

//******************************************************************************
//  Online
//******************************************************************************

void TMFlash::OnlineFirst()
// Nastavi online zaznamnik pred prvni zaznam
{
   // hledani markeru :
   for( int i = 0; i < FL_ONLINE_SAMPLES; i++){
      if( *(byte *)&FOnline[ i] == ON_SAMPLE_EMPTY){
         if( i == 0){
            FCurOnline = -1;           // prazdny archiv
         } else {
            FCurOnline  = 0;           // od zacatku
         }
         FLastOnline = i;              // po znacku
         return;
      }
      if( *(byte *)&FOnline[ i] == ON_SAMPLE_FULL){
         if( i == FL_ONLINE_SAMPLES - 1){
            FCurOnline = 0;            // znacka na konci
         } else {
            FCurOnline = i + 1;        // zacatek za znackou
         }
         FLastOnline = i;              // po znacku
         return;
      }
   }
   FCurOnline = -1;                    // nenalezena znacka
} // OnlineFirst

TOnlineSample *TMFlash::OnlineNext()
// Nasledujici zaznam (0=posledni)
{
   if( FCurOnline < 0){
      return( 0);                      // konec
   }
   int Index = FCurOnline;             // pamatuj aktualni pozici
   // vypocti nasledujici polozku :
   FCurOnline++;
   if( FCurOnline >= FL_ONLINE_SAMPLES){
      // jsme na konci
      FCurOnline =  0;                 // pokracuje od zacatku
   }
   if( FCurOnline == FLastOnline){
      FCurOnline = -1;                 // jsme na znacce, konec
   }
   return( &FOnline[ Index]);
} // OnlineNext

//******************************************************************************
//  Nacteni dat
//******************************************************************************

bool TMFlash::Load( AnsiString File)
// Nacte data ze souboru
{
   if( !FileExists( File)){
      return( false);
   }
   FILE *f;
   f = fopen( File.c_str(), "rb");
   if( !f){
      return( false);
   }
   fread( FRawData, FLASH_SIZE, 1, f);
   fclose( f);
   ReadFinalize();       // upravy surovych dat
   return( true);
} // Load

//******************************************************************************
// Ulozeni dat
//******************************************************************************

void TMFlash::Save( AnsiString File)
// Ulozi data do souboru
{
   FILE *f;
   f = fopen( File.c_str(), "wb");
   fwrite( FRawData, FLASH_SIZE, 1, f);
   fclose( f);
} // Save

//******************************************************************************
//  Nacteni surovych dat
//******************************************************************************

void TMFlash::LoadRawData(byte *Data) {
  // Nacte surova data <Data>
  memcpy(FRawData, Data, FLASH_SIZE);
} // LoadRawData

//******************************************************************************
// Prubeh cteni
//******************************************************************************

int TMFlash::GetReadProgress()
// Vraci pocet promile nactenych dat
{
   return( (FReadOffset * 1000) / FLASH_SIZE);
} // GetReadProgress

//******************************************************************************
// Cislo verze
//******************************************************************************

int TMFlash::GetConfigVersion() {
  // Nacte z ConfigSection cislo verze
  // Cislo verze je na stejne pozici v TConfigSection i TConfigSection109, muzu tak nacist z libovolne verze
  return KConvertGetWord(FConfigSection->Config.Version);
} // GetConfigVersion

//******************************************************************************
// Typ dat
//******************************************************************************

bool TMFlash::GetConfigOnline() {
  // Nacte z ConfigSection typ dat, vrati true pokud jde o online data
  return FConfigSection->Config.WeighingStart.Online;
} // GetConfigOnline

