//---------------------------------------------------------------------------

#ifndef Rs485PortSetupH
#define Rs485PortSetupH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TModbusPortSetupForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label6;
        TLabel *Label7;
        TLabel *Label8;
        TComboBox *SpeedComboBox;
        TComboBox *ParityComboBox;
        TLabel *Label9;
        TEdit *ReplyDelayEdit;
        TEdit *SilentIntervalEdit;
        TLabel *Label10;
        TComboBox *ProtocolComboBox;
        TBevel *Bevel1;
        TButton *OkButton;
        TButton *CancelButton;
        TComboBox *RepetitionsComboBox;
        TLabel *Label1;
        void __fastcall OkButtonClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
        void CheckEdit(TEdit *Edit, int Min, int Max);
        // Kontrola hodnoty v editu <Edit>. V pripade chyby hodi vyjimku.
        bool CheckValues();
        // Kontrola zadanych hodnot

public:		// User declarations

        __fastcall TModbusPortSetupForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusPortSetupForm *ModbusPortSetupForm;
//---------------------------------------------------------------------------
#endif
