//******************************************************************************
//
//   ReadProgress.cpp  Read progress frame
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "ReadProgress.h"
#include <stdio.h>
#include "DM.h"


//---------------------------------------------------------------------
#pragma resource "*.dfm"
TReadProgressForm *ReadProgressForm;

//******************************************************************************
//  Konstruktor
//******************************************************************************

__fastcall TReadProgressForm::TReadProgressForm(TComponent* AOwner)
	: TForm(AOwner)
{
} // TReadProgressForm

//******************************************************************************
//  Modalni dialog
//******************************************************************************

bool __fastcall TReadProgressForm::Execute( TForm *Parent)
{
   if( !Data->Module->ReadData()){
      return( false);
   }
   ProgressBar->Position   = 0;
   Parent->Enabled         = false;         // zakaz interakci s rodicem
   this->Show();                            // zobraz toto okno
   Application->ProcessMessages();          // vyber cekajici zpravy
//   Screen->Cursor          = crHourGlass;   // presypaci hodiny
   // cteni paketu :
   bool Done = false;
   DoCancel  = false;                       // predcasne ukonceni
   int Min, Sec;                            // cas do konce
   int TotalTime;

   TotalTime = 2 * 60 + 54;       // pocet sekund do konce

   Min = TotalTime / 60;
   Sec = TotalTime % 60;
   AnsiString RTime;
   RTime.printf( "00:%02d:%02d", Min, Sec);
   LblTime->Caption = RTime;
   while( !Done){
      if( !Data->Module->ReadPacket( Done)){
         break;
      }
      ProgressBar->Position = Data->Module->ReadProgress / 10;
      char Buffer[ 20];
      sprintf( Buffer, "%6.1f%%", (double)Data->Module->ReadProgress / 10);
      LblProgress->Caption = Buffer;
      Min = Sec = (TotalTime * (1000 - Data->Module->ReadProgress)) / 1000;
      Min /= 60;
      Sec %= 60;
      RTime.printf( "00:%02d:%02d", Min, Sec);
      LblTime->Caption = RTime;
      // vyber cekajici zpravy :
//      Screen->Cursor = crDefault;           // zrus hodiny
      Application->ProcessMessages();
//      Screen->Cursor = crHourGlass;
      if( DoCancel){
         // predcasne ukonceni
         break;
      }
   }
   // uklid okna :
   this->Visible   = false;                 // schovej toto okno
   Parent->Enabled = true;
//   Screen->Cursor  = crDefault;             // zrus hodiny
   // dokoncovaci operace :
   if( !Done){
      return( false);
   }
   Data->Module->ReadFinalize();
   return( true);
} // Execute


//******************************************************************************
//  Ukonceni
//******************************************************************************

void __fastcall TReadProgressForm::Cancel(TObject *Sender)
{
   DoCancel = true;
} // Cancel

