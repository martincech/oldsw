//***************************************************************************
//
//    FlockList.cpp - List of flocks (FlockClass)
//    Version 1.0
//
//***************************************************************************

#ifndef FLOCKLIST_H
#define FLOCKLIST_H

#include "Flock.h"

class TFlockList {

private:
  vector<TFlockClass *> FlockList;           // Seznam hejn

  typedef vector<TFlockClass *>::iterator TFlockListIterator;

  vector<TFlockClass *>::iterator GetIterator(int Index);
  bool CheckIndex(int Index);
  void Sort();


public:
  ~TFlockList();

  void Clear();
  int Count();
  bool IsFull();
  int GetIndex(int Number);
  bool Exists(int Number);
  bool Add(TFlockClass *Flock);
  bool Delete(int Index);
  TFlockClass * GetFlock(int Index);
};


#endif // FLOCKLIST_H
