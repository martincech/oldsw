object ModbusOnlineForm: TModbusOnlineForm
  Left = 224
  Top = 194
  Width = 905
  Height = 550
  Caption = 'RS-485 scales'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ScalesStringGrid: TStringGrid
    Left = 0
    Top = 37
    Width = 897
    Height = 467
    Align = alClient
    ColCount = 10
    DefaultColWidth = 80
    DefaultRowHeight = 16
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowSelect]
    TabOrder = 0
    OnClick = ScalesStringGridClick
    ColWidths = (
      10
      147
      113
      104
      80
      80
      80
      80
      80
      80)
    RowHeights = (
      16
      16)
  end
  object TopPanel: TPanel
    Left = 0
    Top = 0
    Width = 897
    Height = 37
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Bevel2: TBevel
      Left = 0
      Top = 30
      Width = 897
      Height = 7
      Align = alBottom
      Shape = bsTopLine
    end
    object Bevel3: TBevel
      Left = 0
      Top = 0
      Width = 897
      Height = 2
      Align = alTop
      Shape = bsTopLine
    end
    object StartSpeedButton: TSpeedButton
      Left = 8
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888808888888888888880088888
        8888888880008888888888888000088888888888800000888888888880000088
        8888888880000888888888888000888888888888800888888888888880888888
        8888888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Start1Click
    end
    object StopSpeedButton: TSpeedButton
      Left = 32
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888880000000000888888000000000
        0888888000000000088888800000000008888880000000000888888000000000
        0888888000000000088888800000000008888880000000000888888000000000
        0888888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Stop1Click
    end
    object DownloadSpeedButton: TSpeedButton
      Left = 120
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00881111111888
        8888111110111118888888111011188888888888808888080000888880888888
        8888888880888808000088888088888888888888808888080000888000008888
        88888880383088881188888083808881111888803830881111118880AAA08888
        1188888038308111118888800000811118888888808888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Downloadresults1Click
    end
    object PauseSpeedButton: TSpeedButton
      Left = 56
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888800880088888888880088008
        8888888880088008888888888008800888888888800880088888888880088008
        8888888880088008888888888008800888888888800880088888888880088008
        8888888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Pause1Click
    end
    object SetupSpeedButton: TSpeedButton
      Left = 184
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF00C0C0C00000FFFF00FF000000C0C0C000FFFF0000FFFFFF00DADADADADADA
        DADAADADADADADADADADD00ADADADA00DADAA010ADADA080ADADDA010ADA080A
        DADAADA010A080ADADADDADA01080ADADADAADADA080ADADADADDADA08010ADA
        D0DA000080A010AD030D00000ADA010003300DA00DADA003300DDAD00ADAD033
        30DAAD000DAD003330ADDADADA0033000ADAADADADA0000DADAD}
      ParentShowHint = False
      ShowHint = True
      OnClick = Setup1Click
    end
    object PortSpeedButton: TSpeedButton
      Left = 272
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00880888888888
        8888088000000888888880FFFFFF8088888888000000F8088888888888880F08
        88888888888808808888888888880000888888888880F77708888888880F8888
        70888888880F888870888888880F888870888888880F88887088888880F88888
        8708888880FFFFFFFF08888888B0B0B0B0888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Communicationparameters1Click
    end
    object AddSpeedButton: TSpeedButton
      Left = 248
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00881111111888
        8888111110111118888888111011188888888888808888888888888880888888
        8888888880888888888888888088888888888888808888888888888000008888
        88888880383088888888888083808888778888803830888877888880AAA08877
        7777888038308877777788800000888877888888808888887788}
      ParentShowHint = False
      ShowHint = True
      OnClick = Addremovescales1Click
    end
    object Bevel7: TBevel
      Left = 111
      Top = 7
      Width = 4
      Height = 18
      Shape = bsLeftLine
    end
    object Bevel8: TBevel
      Left = 239
      Top = 7
      Width = 4
      Height = 18
      Shape = bsLeftLine
    end
    object ContinueSpeedButton: TSpeedButton
      Left = 80
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888008808888888888800880088
        8888888800880008888888880088000088888888008800000888888800880000
        0888888800880000888888880088000888888888008800888888888800880888
        8888888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Continue1Click
    end
    object Bevel1: TBevel
      Left = 151
      Top = 7
      Width = 4
      Height = 18
      Shape = bsLeftLine
    end
    object DateTimeSpeedButton: TSpeedButton
      Left = 160
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888880000088888888800FFFFF008888880FFFF1FFFF088880FFFFFFFFFF
        F08880FFFFFFFFFFF0880FFFFFFFFFFFFF080FFFFFFFFFFFFF080F1FFFF0000F
        1F080FFFFFF0FFFFFF080FFFFFF0FFFFFF0880FFFFF0FFFFF08880FFFFF0FFFF
        F088880FFFF0FFFF088888800FFFFF0088888888800000888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Dateandtime1Click
    end
    object CompareSpeedButton: TSpeedButton
      Left = 208
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888800000008
        8888888880000088888888888800088888888888888088888888000000000000
        0000811188888888CCC8811188888888CCC8811188888888CCC8881888888888
        8C88888888888888888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Compare1Click
    end
    object Bevel4: TBevel
      Left = 303
      Top = 7
      Width = 4
      Height = 18
      Shape = bsLeftLine
    end
    object ErrorsSpeedButton: TSpeedButton
      Left = 312
      Top = 5
      Width = 24
      Height = 22
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00881111111888
        8888111110111118800088111011188808888888808888880888888880888888
        8888888880888808880888888088888080888888808888880888888000008880
        80888880383088088808888083808888888888803830888808888880AAA08888
        0888888038300000888888800000888888888888808888888888}
      ParentShowHint = False
      ShowHint = True
      OnClick = Communicationerrors1Click
    end
  end
  object OnlineTimer: TTimer
    Enabled = False
    Interval = 200
    OnTimer = OnlineTimerTimer
    Left = 816
    Top = 8
  end
  object MainMenu1: TMainMenu
    Left = 848
    Top = 8
    object Scale1: TMenuItem
      Caption = 'Scale'
      object Start1: TMenuItem
        Caption = 'Start weighing'
        OnClick = Start1Click
      end
      object Stop1: TMenuItem
        Caption = 'Stop weighing'
        OnClick = Stop1Click
      end
      object Pause1: TMenuItem
        Caption = 'Pause weighing'
        OnClick = Pause1Click
      end
      object Continue1: TMenuItem
        Caption = 'Continue weighing'
        OnClick = Continue1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object Downloadresults1: TMenuItem
        Caption = 'Download results...'
        OnClick = Downloadresults1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Dateandtime1: TMenuItem
        Caption = 'Set date and time'
        OnClick = Dateandtime1Click
      end
      object Setup1: TMenuItem
        Caption = 'Scale setup'
        OnClick = Setup1Click
      end
      object Compare1: TMenuItem
        Caption = 'Compare'
        OnClick = Compare1Click
      end
    end
    object Options1: TMenuItem
      Caption = 'Options'
      object Addremovescales1: TMenuItem
        Caption = 'Add / remove scales...'
        OnClick = Addremovescales1Click
      end
      object Communicationparameters1: TMenuItem
        Caption = 'Communication parameters...'
        OnClick = Communicationparameters1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Communicationerrors1: TMenuItem
        Caption = 'Communication errors...'
        OnClick = Communicationerrors1Click
      end
    end
  end
end
