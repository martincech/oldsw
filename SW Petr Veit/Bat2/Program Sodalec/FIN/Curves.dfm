�
 TCURVESFORM 00  TPF0TCurvesForm
CurvesFormLeftTop� BorderIconsbiSystemMenu BorderStylebsSingleCaption
Kasvuk�yr�ClientHeight�ClientWidth Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxCurvesGroupBoxLeftTopPWidth� HeightqCaptionK�ytett�viss� olevatTabOrder  TListBoxCurvesListBoxLeftTopWidth� Height
ItemHeightTabOrder OnClickCurvesListBoxClick  TButtonNewCurveButtonLeftTop(Width]HeightCaptionUusiTabOrderOnClickNewCurveButtonClick  TButtonDeleteCurveButtonLeft|TopHWidth]HeightCaptionPoistaTabOrderOnClickDeleteCurveButtonClick  TButtonRenameCurveButtonLeftTopHWidth]HeightCaptionNime� uudelleenTabOrderOnClickRenameCurveButtonClick  TButtonCopyCurveButtonLeft|Top(Width]HeightCaptionKopioiTabOrderOnClickCopyCurveButtonClick   	TGroupBoxDefinitionGroupBoxLeftTopPWidthHeightQCaption
M��rittelyTabOrder TStringGridCurveStringGridLeftTopWidth� HeightColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsUT 
RowHeights   TButtonSaveCurveButtonLeft� Top(Width]HeightCaptionTallennaTabOrderOnClickSaveCurveButtonClick   TPanelDecriptionPanelLeft Top Width HeightAAlignalTop
BevelOuterbvNoneEnabledTabOrder TLabelLabel1LeftTopWidthHeight1AutoSizeCaption�Voit m��ritell� t�st� teoreettisen kasvuk�yr�n. M��rittelyn j�lkeen teoreettista ja todellista kasvuk�yr�� voidaan vertailla kesken��n.WordWrap	   TButtonOkButtonLeftgTop�WidthKHeightCaptionOKTabOrderOnClickOkButtonClick  TButtonCancelButtonLeft�Top�WidthKHeightCaptionPeruutaModalResultTabOrder   