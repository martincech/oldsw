//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "RozsahDnu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TRozsahDnuForm *RozsahDnuForm;
//---------------------------------------------------------------------------
__fastcall TRozsahDnuForm::TRozsahDnuForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TRozsahDnuForm::OdEditKeyPress(TObject *Sender, char &Key)
{
  // Filtruju cislice (musim i backspace)
  //   Backspace      0   az   9
  //       |          |        |
  // 3.9.2001: Chtel natvrdo carku, i kdyz ve Win byla nastavena tecka a pak to blblo
//  if (Key!=8 && (Key<48 || Key>57) && Key!=DecimalSeparator) Key=0;  // Vyberu jen cisla a carku
  if (Key!=8 && (Key<48 || Key>57)) Key=0;  // Vyberu jen cisla
}
//---------------------------------------------------------------------------
void __fastcall TRozsahDnuForm::FormShow(TObject *Sender)
{
  AktualniRadioButton->SetFocus();        
}
//---------------------------------------------------------------------------

