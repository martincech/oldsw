//---------------------------------------------------------------------------

#ifndef ModbusWaitH
#define ModbusWaitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TModbusWaitForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *WaitLabel;
        TButton *CancelButton;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall CancelButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        bool Canceled;

        __fastcall TModbusWaitForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusWaitForm *ModbusWaitForm;
//---------------------------------------------------------------------------
#endif
