//---------------------------------------------------------------------------

#ifndef HlavniH
#define HlavniH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Menus.hpp>
#include <ToolWin.hpp>
#include "CSPIN.h"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Buttons.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>




// Indexy prubehu celkove aktivity
#define INDEX_AKTIVITA_CELKEM       0
#define INDEX_AKTIVITA_CELKEM_COMP  1

// Indexy prubehu rustove krivky
#define KRIVKA_REAL        0
#define KRIVKA_TARGET      1
#define KRIVKA_COMP_REAL   2
#define KRIVKA_COMP_TARGET 3
#define KRIVKA_THEORY      4            // Porovnani s teoretickou rustovou krivkou
#define KRIVKA_STATISTICS  5            // Statistika na prave ose Y

// Indexy statisticke hodnoty zobrazene na prave ose rustove krivky (odpovida CurveRightAxisComboBox)
enum {
  CURVE_STAT_NONE,
  CURVE_STAT_DAILY_GAIN,
  CURVE_STAT_COUNT,
  CURVE_STAT_SIGMA,
  CURVE_STAT_CV,
  CURVE_STAT_UNIFORMITY,
  _CURVE_STAT_DEFAULT = CURVE_STAT_DAILY_GAIN
};

//---------------------------------------------------------------------------
class THlavniForm : public TForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl;
        TTabSheet *StatistikaTabSheet;
        TMainMenu *MainMenu1;
        TMenuItem *Soubor1;
        TMenuItem *Otevt1;
        TMenuItem *Uloit1;
        TMenuItem *N14;
        TMenuItem *Tisk1;
        TMenuItem *Export1;
        TMenuItem *Konecprogramu1;
        TMenuItem *Modul1;
        TMenuItem *Nastzznamy1;
        TMenuItem *Nastaven1;
        TMenuItem *Jazyk1;
        TMenuItem *Anglicky1;
        TMenuItem *Francouzsky1;
        TMenuItem *Npovda1;
        TMenuItem *Oprogramu1;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
        TTabSheet *HistogramTabSheet;
        TTabSheet *VazeniTabSheet;
        TTabSheet *NastaveniTabSheet;
        TTabSheet *AktivitaDenTabSheet;
        TDBGrid *DBGrid1;
        TLabel *Label3;
        TLabel *CilovaHmotnostLabel;
        TLabel *Label5;
        TLabel *PrumerLabel;
        TLabel *Label7;
        TLabel *PocetLabel;
        TLabel *Label9;
        TLabel *SigmaLabel;
        TLabel *Label11;
        TLabel *UniLabel;
        TLabel *Label13;
        TLabel *CvLabel;
        TLabel *Label15;
        TLabel *PrirustekLabel;
        TLabel *CilovaHmotnostJednotkyLabel;
        TLabel *PrumerJednotkyLabel;
        TLabel *PrirustekJednotkyLabel;
        TLabel *Label20;
        TLabel *Label21;
        TChart *HistogramChart;
        TBarSeries *Series1;
        TChart *AktivitaDenChart;
        TBarSeries *BarSeries1;
        TMenuItem *Readdatafromfile1;
        TMenuItem *Readdatatofile1;
        TOpenDialog *OpenDialogHardCopy;
        TSaveDialog *SaveDialogHardCopy;
        TTabSheet *KrivkaTabSheet;
        TChart *KrivkaChart;
        TLineSeries *BarSeries2;
        TLineSeries *Series2;
        TMenuItem *Openforcomparsion1;
        TMenuItem *N2;
        TPanel *StatistikaPorovnaniPanel;
        TLabel *CilovaHmotnostPorovnaniLabel;
        TLabel *PrumerPorovnaniLabel;
        TLabel *PocetPorovnaniLabel;
        TLabel *SigmaPorovnaniLabel;
        TLabel *CvPorovnaniLabel;
        TLabel *PrirustekPorovnaniLabel;
        TLabel *CilovaHmotnostJednotkyPorovnaniLabel;
        TLabel *PrumerJednotkyPorovnaniLabel;
        TLabel *PrirustekJednotkyPorovnaniLabel;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *StatistikaPorovnaniLabel;
        TPanel *HistogramPorovnaniPanel;
        TSplitter *HistogramSplitter;
        TChart *HistogramPorovnaniChart;
        TBarSeries *BarSeries3;
        TPanel *Panel1;
        TLabel *HistogramPorovnaniLabel;
        TPanel *Panel2;
        TCheckBox *HistogramPorovnaniOsaCheckBox;
        TPanel *Panel3;
        TBevel *Bevel2;
        TBevel *Bevel3;
        TSpeedButton *OpenSpeedButton;
        TSpeedButton *SaveSpeedButton;
        TSpeedButton *ReadSpeedButton;
        TPanel *ZahlaviPanel;
        TLabel *Label1;
        TLabel *DatumLabel;
        TSpeedButton *PrvniDenSpeedButton;
        TSpeedButton *PredchoziDenSpeedButton;
        TSpeedButton *DalsiDenSpeedButton;
        TSpeedButton *PosledniDenSpeedButton;
        TPanel *PohlaviPanel;
        TLabel *Label2;
        TSpeedButton *SpeedButton1;
        TSpeedButton *SpeedButton2;
        TRadioButton *PohlaviSamiceRadioButton;
        TRadioButton *PohlaviSamciRadioButton;
        TComboBox *DenComboBox;
        TBevel *Bevel5;
        TBevel *Bevel1;
        TPanel *AktivitaPorovnaniPanel;
        TPanel *Panel5;
        TLabel *AktivitaPorovnaniLabel;
        TPanel *Panel6;
        TChart *AktivitaDenPorovnaniChart;
        TBarSeries *BarSeries4;
        TSplitter *AktivitaSplitter;
        TLineSeries *Series3;
        TLineSeries *Series4;
        TPanel *KrivkaPanel;
        TCheckBox *KrivkaRealCheckBox;
        TCheckBox *KrivkaTargetCheckBox;
        TPanel *KrivkaPorovnaniPanel;
        TCheckBox *KrivkaCRealCheckBox;
        TCheckBox *KrivkaCTargetCheckBox;
        TLabel *KrivkaPorovnaniLabel;
        TMenuItem *N3;
        TMenuItem *Config1;
        TLabel *UniPorovnaniLabel;
        TPageControl *NastaveniPageControl;
        TTabSheet *NastaveniHejnaTabSheet;
        TLabel *Label4;
        TListBox *SeznamHejnListBox;
        TPanel *HejnaPanel;
        TLabel *Label6;
        TLabel *Label24;
        TLabel *Label25;
        TDBEdit *JmenoDBEdit;
        TDBEdit *DBEdit15;
        TDBEdit *DBEdit16;
        TPageControl *KrivkyPageControl;
        TTabSheet *KrivkaSamiceTabSheet;
        TStringGrid *KrivkaSamiceStringGrid;
        TTabSheet *KrivkaSamciTabSheet;
        TStringGrid *KrivkaSamciStringGrid;
        TTabSheet *PocatecniHmotnostTabSheet;
        TLabel *Label26;
        TDBEdit *PocatecniHmotnostSamiceDBEdit;
        TPanel *PocatecniHmotnostSamciPanel;
        TLabel *Label27;
        TDBEdit *PocatecniHmotnostSamciDBEdit;
        TTabSheet *NastaveniStatistikaTabSheet;
        TLabel *Label8;
        TLabel *Label10;
        TLabel *Label12;
        TLabel *Label14;
        TLabel *Label16;
        TLabel *Label28;
        TDBEdit *RozsahUniformityDBEdit;
        TDBEdit *RozsahHistogramuDBEdit;
        TTabSheet *NastaveniGsmTabSheet;
        TLabel *Label29;
        TDBCheckBox *DBCheckBox1;
        TDBCheckBox *DBCheckBox2;
        TDBCheckBox *DBCheckBox3;
        TDBCheckBox *DBCheckBox4;
        TDBEdit *DBEdit3;
        TDBEdit *DBEdit4;
        TDBEdit *DBEdit5;
        TDBEdit *DBEdit6;
        TDBEdit *DBEdit7;
        TTabSheet *NastaveniVahyTabSheet;
        TLabel *Label30;
        TLabel *Label31;
        TLabel *Label32;
        TLabel *Label33;
        TLabel *Label34;
        TLabel *Label35;
        TLabel *Label36;
        TLabel *Label37;
        TLabel *Label38;
        TLabel *Label39;
        TLabel *Label40;
        TLabel *Label41;
        TDBEdit *SamiceOkoliNadDBEdit;
        TDBEdit *SamiceOkoliPodDBEdit;
        TDBEdit *SamciOkoliNadDBEdit;
        TDBEdit *SamciOkoliPodDBEdit;
        TDBEdit *UstaleniDBEdit;
        TDBEdit *DobaUstaleniDBEdit;
        TDBRadioGroup *DBRadioGroup1;
        TDBCheckBox *DBCheckBox5;
        TDBCheckBox *DBCheckBox6;
        TTabSheet *NastaveniVazeniTabSheet;
        TDBCheckBox *DBCheckBox7;
        TLabel *Label42;
        TEdit *NastaveniHejnoEdit;
        TMenuItem *MenuExportSamples;
        TMenuItem *MenuExportHistogram;
        TMenuItem *MenuExportDayActivity;
        TMenuItem *MenuPrintSamples;
        TMenuItem *MenuPrintHistogram;
        TMenuItem *MenuPrintDayActivity;
        TMenuItem *MenuExportGrowthCurve;
        TTabSheet *AktivitaCelkemTabSheet;
        TChart *AktivitaCelkemChart;
        TLineSeries *LineSeries1;
        TLineSeries *LineSeries2;
        TMenuItem *MenuExportTotalActivity;
        TMenuItem *MenuPrintTotalActivity;
        TTabSheet *InformaceTabSheet;
        TLabel *Label17;
        TLabel *VerzeLabel;
        TLabel *Label18;
        TDBEdit *DBEdit1;
        TLabel *Label19;
        TDBText *DBText1;
        TMenuItem *MenuPrintGrowthCurve;
        TTabSheet *OnlineTabSheet;
        TDBGrid *OnlineDBGrid;
        TTabSheet *OnlineGrafTabSheet;
        TChart *OnlineChart;
        TFastLineSeries *LineSeries3;
        TMenuItem *MenuPrintOnline;
        TMenuItem *MenuExportOnline;
        TMenuItem *Finsky1;
        TLabel *Label43;
        TDBEdit *FiltrDBEdit;
        TPanel *OnlineGrafPanel;
        TLabel *Label44;
        TComboBox *OnlinePrumerovaniComboBox;
        TLabel *Label45;
        TMenuItem *N4;
        TMenuItem *MenuSimulation;
        TLabel *Label46;
        TLabel *OnlineHmotnostLabel;
        TLabel *OnlineHmotnostJednotkyLabel;
        TSpeedButton *OnlineZoomInSpeedButton;
        TSpeedButton *OnlineZoomOutSpeedButton;
        TBevel *Bevel4;
        TBevel *Bevel6;
        TTabSheet *NastaveniPodsvitTabSheet;
        TDBRadioGroup *DBRadioGroup2;
        TLabel *Label47;
        TDBEdit *DBEdit2;
        TLabel *Label48;
        TLabel *Label49;
        TDBEdit *DBEdit8;
        TCheckBox *NastaveniGainCheckBox;
        TDBCheckBox *DBCheckBox8;
        TTabSheet *ReportTabSheet;
        TDBGrid *ReportDBGrid;
        TMenuItem *MenuPrintReport;
        TMenuItem *MenuExportReport;
        TMenuItem *Turkce1;
        TMenuItem *N5;
        TMenuItem *Database1;
        TMenuItem *Weighingdatabase1;
        TMenuItem *N6;
        TMenuItem *Addtoweighingdatabase1;
        TMenuItem *Findfile1;
        TMenuItem *N7;
        TSpeedButton *FindSpeedButton;
        TSpeedButton *SetupSpeedButton;
        TSpeedButton *DatabaseSpeedButton;
        TSpeedButton *AddSpeedButton;
        TBevel *Bevel7;
        TBevel *Bevel8;
        TMenuItem *Dansk1;
        TMenuItem *Espanol1;
        TDBRadioGroup *DBRadioGroup3;
        TTabSheet *NastaveniRs485TabSheet;
        TLabel *Label50;
        TDBEdit *DBEdit9;
        TLabel *Label51;
        TDBEdit *DBEdit10;
        TLabel *Label53;
        TDBEdit *DBEdit12;
        TLabel *Label54;
        TDBEdit *DBEdit13;
        TLabel *Label55;
        TLabel *Label56;
        TLabel *Label57;
        TDBRadioGroup *DBRadioGroup4;
        TMenuItem *Growthcurves1;
        TPanel *KrivkaTeoriePanel;
        TLabel *Label52;
        TComboBox *KrivkaPorovnatComboBox;
        TSpeedButton *KrivkaPorovnatSpeedButton;
        TLineSeries *Series5;
        TPanel *ReportPanel;
        TPanel *Panel4;
        TLabel *Label58;
        TSpeedButton *ReportPorovnatSpeedButton;
        TComboBox *ReportPorovnatComboBox;
        TLabel *Label59;
        TDBEdit *DBEdit11;
        TMenuItem *Deutsch1;
        TMenuItem *Memorymodule1;
        TMenuItem *Diagnostics1;
        TMenuItem *RS4851;
        TSpeedButton *Rs485SpeedButton;
        TBevel *Bevel9;
        TDBRadioGroup *DBRadioGroup5;
        TLineSeries *Series6;
        TComboBox *CurveRightAxisComboBox;
        TLabel *Label60;
        TMenuItem *Russian1;
        TTabSheet *TabSheetCorrection;
        TLabel *Label61;
        TDBEdit *DBEdit14;
        TLabel *Label62;
        TLabel *Label64;
        TDBEdit *DBEdit18;
        TLabel *Label63;
        TDBEdit *DBEdit17;
        TLabel *Label65;
        TLabel *Label66;
        TDBEdit *DBEdit19;
        TLabel *Label68;
        TDBEdit *DBEdit20;
        TLabel *Label69;
        TLabel *Label67;
        TDBEdit *HourDBEdit;
        TDBEdit *MinDBEdit;
        TLabel *Label70;
        void __fastcall Oprogramu1Click(TObject *Sender);
        void __fastcall Konecprogramu1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Readdatafromfile1Click(TObject *Sender);
        void __fastcall Readdatatofile1Click(TObject *Sender);
        void __fastcall DenComboBoxChange(TObject *Sender);
        void __fastcall PohlaviSamiceRadioButtonClick(TObject *Sender);
        void __fastcall PohlaviSamciRadioButtonClick(TObject *Sender);
        void __fastcall PrvniDenSpeedButtonClick(TObject *Sender);
        void __fastcall PredchoziDenSpeedButtonClick(TObject *Sender);
        void __fastcall DalsiDenSpeedButtonClick(TObject *Sender);
        void __fastcall PosledniDenSpeedButtonClick(TObject *Sender);
        void __fastcall Openforcomparsion1Click(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall HistogramPorovnaniOsaCheckBoxClick(
          TObject *Sender);
        void __fastcall Otevt1Click(TObject *Sender);
        void __fastcall Uloit1Click(TObject *Sender);
        void __fastcall Nastzznamy1Click(TObject *Sender);
        void __fastcall KrivkaCRealCheckBoxClick(TObject *Sender);
        void __fastcall KrivkaCTargetCheckBoxClick(TObject *Sender);
        void __fastcall KrivkaRealCheckBoxClick(TObject *Sender);
        void __fastcall KrivkaTargetCheckBoxClick(TObject *Sender);
        void __fastcall Config1Click(TObject *Sender);
        void __fastcall SeznamHejnListBoxClick(TObject *Sender);
        void __fastcall MenuExportSamplesClick(TObject *Sender);
        void __fastcall MenuExportHistogramClick(TObject *Sender);
        void __fastcall MenuExportDayActivityClick(TObject *Sender);
        void __fastcall MenuExportGrowthCurveClick(TObject *Sender);
        void __fastcall MenuPrintSamplesClick(TObject *Sender);
        void __fastcall MenuExportTotalActivityClick(TObject *Sender);
        void __fastcall MenuPrintHistogramClick(TObject *Sender);
        void __fastcall MenuPrintDayActivityClick(TObject *Sender);
        void __fastcall MenuPrintTotalActivityClick(TObject *Sender);
        void __fastcall MenuPrintGrowthCurveClick(TObject *Sender);
        void __fastcall Anglicky1Click(TObject *Sender);
        void __fastcall Francouzsky1Click(TObject *Sender);
        void __fastcall MenuPrintOnlineClick(TObject *Sender);
        void __fastcall MenuExportOnlineClick(TObject *Sender);
        void __fastcall Finsky1Click(TObject *Sender);
        void __fastcall OnlinePrumerovaniComboBoxChange(TObject *Sender);
        void __fastcall MenuSimulationClick(TObject *Sender);
        void __fastcall OnlineChartMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall OnlineZoomInSpeedButtonClick(TObject *Sender);
        void __fastcall OnlineZoomOutSpeedButtonClick(TObject *Sender);
        void __fastcall MenuPrintReportClick(TObject *Sender);
        void __fastcall MenuExportReportClick(TObject *Sender);
        void __fastcall Turkce1Click(TObject *Sender);
        void __fastcall Weighingdatabase1Click(TObject *Sender);
        void __fastcall Addtoweighingdatabase1Click(TObject *Sender);
        void __fastcall Findfile1Click(TObject *Sender);
        void __fastcall Dansk1Click(TObject *Sender);
        void __fastcall Espanol1Click(TObject *Sender);
        void __fastcall OnlineChartUndoZoom(TObject *Sender);
        void __fastcall Growthcurves1Click(TObject *Sender);
        void __fastcall KrivkaPorovnatComboBoxChange(TObject *Sender);
        void __fastcall Deutsch1Click(TObject *Sender);
        void __fastcall RS4851Click(TObject *Sender);
        void __fastcall CurveRightAxisComboBoxChange(TObject *Sender);
        void __fastcall ReportDBGridKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall Russian1Click(TObject *Sender);
private:	// User declarations

  bool ZobrazitPorovnani;               // Flag, zda je otevren soubor pro porovnani
  int PrumerovaniOnline;                // Prumerovani prave zobrazeneho online grafu

public:		// User declarations
        __fastcall THlavniForm(TComponent* Owner);

  void SetVersionStrings();
  void SetDefaultFilePath();

  void OpenFoundFile();

  void CopyToDatabase(TQuery *Query, bool Pohlavi);

  bool ZvolRozsahDnu(int *Min, int *Max);
  bool ZvolRozsahOnline(int *Min, int *Max, int *Average);
  bool JeOtevrenySoubor();

  String NactiNazevHejna();
  void NactiSeznamHejn();
  void VyberPrvnihejno();
  void ZobrazKrivkuHejna(bool Pohlavi);
  void ZobrazKrivkyHejna();
  void ZobrazHejno(int Hejno);

  void SchovejPrvkyPorovnani();
  void ZobrazPrvkyPorovnani();

  void HideCompareCurveItems();  

  bool PouzivaObePohlavi();

  bool NastavDen(int CisloDne);

  void FillDays();
  void ShowFirstDay();
  void OtevriTabulkyJakoSoubor(String Jmeno);

  void UlozTabulkyDoSouboru();
  bool UlozDataDoNovehoSouboru();

  void ZavriTabulky();
  void OtevriTabulky();
  void ZavriTabulkyPorovnani();
  void OtevriTabulkyPorovnani();

  void VytvorHlavicku();

  bool ZobrazKonfiguraci();
  bool NactiKonfiguraci();
  bool UlozKonfiguraci();
  bool NactiDataZModulu();

  bool NovySoubor();
  bool UlozSoubor(String Nazev);
  bool OtevriSoubor(String Nazev, bool Porovnani, bool Find);

  void ZobrazReport();
  void ZobrazStatistiku();
  void ZobrazHistogram();
  void ZobrazAktivitu();
  void ZobrazAktivituCelkem();
  void ZobrazRustovouKrivku();
  void ZobrazNastaveni();
  void VyplnSimulaci();
  void ZobrazInformace();
  void ZobrazOnlineGraf();
  void ZobrazDen(int Den);

  void NastavNazevOkna(String NazevSouboru);
  void NactiNastaveni();
  void UpgradeTables();

  void CheckLanguage(int Language);
  void SetLanguage(int Language);

  void LoadCurves();
  void HideCompareCurve();

  void DeleteDayFromTable(TTable *Table, int Day);
  void DeleteDay(int Day);

};
//---------------------------------------------------------------------------
extern PACKAGE THlavniForm *HlavniForm;
//---------------------------------------------------------------------------
#endif

