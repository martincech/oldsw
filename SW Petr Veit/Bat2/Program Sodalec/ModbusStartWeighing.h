//---------------------------------------------------------------------------

#ifndef ModbusStartWeighingH
#define ModbusStartWeighingH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>

#pragma pack(push, 1)           // byte alignment
  #include "c51/Bat2.h"         // flash data definition
#pragma pack(pop)               // original alignment

#include "ModbusScale.h"

//---------------------------------------------------------------------------
class TModbusStartWeighingForm : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *WeightGroupBox;
        TRadioButton *UseDirectRadioButton;
        TCheckBox *UseBotheGendersCheckBox;
        TLabel *Label1;
        TEdit *TargetFemalesEdit;
        TLabel *Label2;
        TEdit *TargetMalesEdit;
        TRadioButton *UseCurveRadioButton;
        TComboBox *FlockComboBox;
        TGroupBox *CompareGroupBox;
        TCheckBox *CompareCheckBox;
        TComboBox *CompareComboBox;
        TButton *Button1;
        TButton *Button2;
        TGroupBox *StartGroupBox;
        TRadioButton *StartNowRadioButton;
        TRadioButton *StartLaterRadioButton;
        TDateTimePicker *StartDatePicker;
        TDateTimePicker *StartTimePicker;
        TEdit *StartingDayEdit;
        TLabel *Label3;
        TGroupBox *ModeGroupBox;
        TRadioButton *StandardWeighingRadioButton;
        TRadioButton *OnlineWeighingRadioButton;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall UseDirectRadioButtonClick(TObject *Sender);
        void __fastcall UseBotheGendersCheckBoxClick(TObject *Sender);
        void __fastcall StartNowRadioButtonClick(TObject *Sender);
        void __fastcall CompareCheckBoxClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
        TFlock                 *Flock;                  // Ukazatel na prvni hejno
        TModbusStartParameters *ModbusStartParameters;  // Struktura, ktera se vyplni

        void CheckEditInt(TEdit *Edit, int Min, int Max);
        // Kontrola hodnoty v editu <Edit>. V pripade chyby hodi vyjimku.
        void CheckEditFloat(TEdit *Edit, double Min, double Max);
        // Kontrola hodnoty v editu <Edit>. V pripade chyby hodi vyjimku.
        bool CheckValues();
        // Kontrola zadanych hodnot

        int GetFlockNumber(TComboBox *Box);
        // Zjisteni cisla hejna z Comboboxu <Box>
public:		// User declarations
        void SetFlock(TFlock *NewFlock);
        void SetStartParameters(TModbusStartParameters *NewModbusStartParameters);
        __fastcall TModbusStartWeighingForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusStartWeighingForm *ModbusStartWeighingForm;
//---------------------------------------------------------------------------
#endif
