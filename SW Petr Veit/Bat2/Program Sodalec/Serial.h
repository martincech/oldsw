//******************************************************************************
//
//   Serial.h     Serial data stream template
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef SerialH
   #define SerialH

#ifndef SysDefH
   #include "SysDef.h"
#endif

#ifdef __PERSISTENT__
   #ifndef ObjectMemoryH
      #include "../Memory/ObjectMemory.h"
   #endif

   // Name of template :
   #define SERIAL_NAME TName( "Serial")
#endif

//******************************************************************************
// TSerial
//******************************************************************************

class TSerial
{
public :

   //---------------- Functions

   static bool Locate( TName Name, TIdentifier &Identifier);
   // Find device by <Name>, returns <Identifier>
   static bool Access( TIdentifier Identifier);
   // Check device access by <Identifier>
   TSerial();
   // Constructor
   virtual ~TSerial();
   // Destructor
   virtual bool Open( TIdentifier Identifier) = 0;
   // Open device by <Identifier>
   virtual void Close() = 0;
   // Close device
   virtual int Write( void *Data, int Length) = 0;
   // Write <Data> of size <Length>, returns written length (or 0 if error)
   virtual int Read( void *Data, int Length) = 0;
   // Read data <Data> of size <Length>, returns true length (or 0 if error)
   virtual void Flush() = 0;
   // Make input/output queue empty
   virtual void SetRxNowait() = 0;
   // Set timing of receiver - return collected data immediately
   virtual void SetRxWait( int TotalTime, int IntercharacterTime) = 0;
   // Set timing of receiver - waits for first character up to <TotalTime>
   // returns after <IntercharacterTime> break
#ifdef __PERSISTENT__
   virtual bool Load( TObjectMemory *Memory) = 0;
   // Load setup from <Memory>
   virtual void Save( TObjectMemory *Memory) = 0;
   // Save setup to <Memory>
#endif
  __property TIdentifier Identifier             = {read=FIdentifier};
  __property TName       Name                   = {read=GetName};
  __property bool        IsOpen                 = {read=GetIsOpen};

//------------------------------------------------------------------------------
protected :
   TIdentifier FIdentifier;

   virtual TName GetName() = 0;
   // Get device name
   virtual bool  GetIsOpen() = 0;
   // Check if device is opened
}; // TSerial


//******************************************************************************
// inline
//******************************************************************************

inline TSerial::TSerial()
// Constructor
{
   FIdentifier = INVALID_IDENTIFIER;
} // TSerial

inline TSerial::~TSerial()
{
} // ~TSerial

#endif
