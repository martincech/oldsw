//***************************************************************************
//
//    Flock.cpp - Flock definition saved in setup
//    Version 1.0
//
//***************************************************************************

#ifndef FLOCK_H
#define FLOCK_H

#include <system.hpp>           // AnsiString
#include "Curve.h"

#pragma pack(push, 1)           // byte alignment
  #include "c51/Bat2.h"         // flash data definition
#pragma pack(pop)               // original alignment

//---------------------------------------------------------------------------
// Hejno
//---------------------------------------------------------------------------

class TFlockClass {

public:
  int          Number;                          // Cislo hejna
  AnsiString   Title;                           // Nazev hejna
  bool         UseBothGenders;                  // Flag, zda pouzivat obe pohlavi
  bool         UseCurves;                       // Zda se maji pouzivat rustove krivky nebo automat
  int          WeighFrom;                       // Omezeni ukladani od zadane hodiny vcetne (FLOCK_TIME_LIMIT_EMPTY = neomezuje se)
  int          WeighTill;                       // Omezeni ukladani do zadane hodiny vcetne (FLOCK_TIME_LIMIT_EMPTY = neomezuje se)
  double       InitialWeight[_GENDER_COUNT];    // Pocatecni cilove hmotnosti pro obe pohlavi v automatickem rezimu
  TGrowthCurve *GrowthCurve[_GENDER_COUNT];     // Rustove krivky pro obe pohlavi

  void Init();
  TFlockClass();
  TFlockClass(int FlockNumber);
  TFlockClass(TFlock *Flock);
  ~TFlockClass();

  void Load(TFlock *Flock);
  // Prevezme parametry hejna <Flock>
  void Save(TFlock *Flock);
  // Ulozi parametry do hejna <Flock>

  static bool CheckNumber(int Number);
  static bool CheckTitle(AnsiString Title);
  static bool CheckHour(int Hour);
  static bool CheckInitialWeight(double Weight);

};

#endif // FLOCK_H

