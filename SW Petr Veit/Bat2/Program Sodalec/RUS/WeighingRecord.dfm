�
 TWEIGHINGRECORDFORM 0f  TPF0TWeighingRecordFormWeighingRecordFormLeft� Top�BorderIconsbiSystemMenu BorderStylebsSingleCaption������ClientHeight7ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TButtonButton1Left�TopWidthKHeightCaption	���������Default	TabOrderOnClickButton1Click  TButtonButton2Left&TopWidthKHeightCancel	Caption������ModalResultTabOrder  	TGroupBox	GroupBox1LeftTopWidthHeightaCaption���������� � ������TabOrder  TLabelLabel16LeftTopWidth9HeightCaption
���������:  TLabelLabel17LeftTop0WidthnHeightCaption��������� ��������:  TLabelLabel18LeftTopHWidth>HeightCaption
GSM �����:  TLabelInsertLabelLeft� TopWidthnHeightAutoSizeCaptionInsertLabel  TLabel	EditLabelLeft� Top0WidthnHeightAutoSizeCaption	EditLabel  TLabelGSMLabelLeft� TopHWidthnHeightAutoSizeCaptionGSMLabel   	TGroupBox	GroupBox2LeftTop� WidthHeight� Caption����TabOrder TLabelLabel1LeftTopWidthHeightCaption����:  TLabelLabel2LeftTop0WidthHeightCaption����:  TLabelLabel3LeftTopHWidthHeightCaption����:  TLabelLabel19LeftTop`WidthBHeightCaption����������:  TEdit
ScalesEditLeft� TopWidthYHeight	MaxLength
TabOrder 
OnKeyPressScalesEditKeyPress  TEditDayEditLeft� Top-WidthYHeight	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TDateTimePickerDateTimePickerLeft� TopEWidthYHeightCalAlignmentdtaLeftDateUn!�1���@TimeUn!�1���@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder  TEditNoteEditLeft� Top]WidthYHeight	MaxLengthTabOrder   	TGroupBox	GroupBox3Left TopWidthQHeight� Caption
����������TabOrder 	TCheckBoxBothGendersCheckBoxLeftTopWidth� HeightCaption������������ ��� ����TabOrder OnClickBothGendersCheckBoxClick  	TGroupBoxFemalesGroupBoxLeftTop0Width� Height� Caption	�����/���TabOrder TLabelLabel4LeftTopWidthHeightCaption����:  TLabelLabel5LeftTop0Width.HeightCaption�������:  TLabelLabel6LeftTopHWidth.HeightCaption�������:  TLabelLabel7LeftTop`WidthPHeightCaption��. ����������:  TLabelLabel8LeftTopxWidthHeightCaption��:  TLabelLabel9LeftTop� WidthHeightCaption���:  TEditFemalesCountEditLeft`TopWidth!Height	MaxLengthTabOrder 
OnKeyPressScalesEditKeyPress  TEditFemalesAverageEditLeft`Top-Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditFemalesGainEditLeft`TopEWidth!HeightTabOrder
OnKeyPressFemalesGainEditKeyPress  TEditFemalesSigmaEditLeft`Top]Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditFemalesCvEditLeft`TopuWidth!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TEditFemalesUniEditLeft`Top� Width!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress   	TGroupBoxMalesGroupBoxLeft� Top0Width� Height� Caption�����TabOrder TLabelLabel10LeftTopWidthHeightCaption����:  TLabelLabel11LeftTop0Width.HeightCaption�������:  TLabelLabel12LeftTopHWidth.HeightCaption�������:  TLabelLabel13LeftTop`WidthPHeightCaption��. ����������:  TLabelLabel14LeftTopxWidthHeightCaption��:  TLabelLabel15LeftTop� WidthHeightCaption���:  TEditMalesCountEditLeft`TopWidth!Height	MaxLengthTabOrder 
OnKeyPressScalesEditKeyPress  TEditMalesAverageEditLeft`Top-Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditMalesGainEditLeft`TopEWidth!HeightTabOrder
OnKeyPressFemalesGainEditKeyPress  TEditMalesSigmaEditLeft`Top]Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditMalesCvEditLeft`TopuWidth!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TEditMalesUniEditLeft`Top� Width!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress    TButtonPrintButtonLeftTopWidthKHeightCaption������TabOrderOnClickPrintButtonClick   