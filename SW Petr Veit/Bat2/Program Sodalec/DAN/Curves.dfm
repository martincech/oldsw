�
 TCURVESFORM 0  TPF0TCurvesForm
CurvesFormLeftTop� BorderIconsbiSystemMenu BorderStylebsSingleCaptionV�kstkurverClientHeight�ClientWidth Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxCurvesGroupBoxLeftTopPWidth� HeightqCaptionTilg�ngelige v�kstkurverTabOrder  TListBoxCurvesListBoxLeftTopWidth� Height
ItemHeightTabOrder OnClickCurvesListBoxClick  TButtonNewCurveButtonLeftTop(Width]HeightCaptionNyTabOrderOnClickNewCurveButtonClick  TButtonDeleteCurveButtonLeft|TopHWidth]HeightCaptionSletTabOrderOnClickDeleteCurveButtonClick  TButtonRenameCurveButtonLeftTopHWidth]HeightCaptionOmd�bTabOrderOnClickRenameCurveButtonClick  TButtonCopyCurveButtonLeft|Top(Width]HeightCaptionKopierTabOrderOnClickCopyCurveButtonClick   	TGroupBoxDefinitionGroupBoxLeftTopPWidthHeightQCaption
DefinitionTabOrder TStringGridCurveStringGridLeftTopWidth� HeightColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsUT 
RowHeights   TButtonSaveCurveButtonLeft� Top(Width]HeightCaptionGemTabOrderOnClickSaveCurveButtonClick   TPanelDecriptionPanelLeft Top Width HeightAAlignalTop
BevelOuterbvNoneEnabledTabOrder TLabelLabel1LeftTopWidthHeight1AutoSizeCaption~Her kan du opgive teoretiske v�kstkurver fra leverand�r af din fjerkr�. Disse kan bliver sammenlignet med de reale resultater.WordWrap	   TButtonOkButtonLeftgTop�WidthKHeightCaptionOKTabOrderOnClickOkButtonClick  TButtonCancelButtonLeft�Top�WidthKHeightCaptionAnnullerModalResultTabOrder   