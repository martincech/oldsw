ÿ
 THLAVNIFORM 0¾  TPF0THlavniForm
HlavniFormLeft  Top Width Height&CaptionBat2Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightõ	Font.NameMS Sans Serif
Font.Style 	Icon.Data
6             è  &        (    (       @                                               ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ                                                                                 															 															 															  													    											      									       									        							         							         							         							         							         							          					           					           					           					           					            			             			             			              	               	               	                                                        ÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿÿ   ªªª«ªªª«ªªª«êªª¯úªª¿þªªÿþªªÿÿª«ÿÿª«ÿÿª«ÿÿª«ÿÿª«ÿÿª«ÿÿê¯ÿÿê¯ÿÿê¯ÿÿê¯ÿÿê¯ÿÿú¿ÿÿú¿ÿÿú¿ÿÿþÿÿÿþÿÿÿþÿÿÿÿÿÿÿÿÿÿÿÿÿÿ(                À                                       ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ                 	    	            	     	     	     	                  	       	            ÿÿ       À  à  ð  ð  ø?  ø?  ø?  ø?  ü  ü  þÿ  þÿ  ÿÿ  Menu	MainMenu1OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnResize
FormResizePixelsPerInch`
TextHeight TPageControlPageControlLeft Top%WidthHeightÇ
ActivePageNastaveniTabSheetAlignalClientTabOrder Visible 	TTabSheetReportTabSheetCaptionRapport
ImageIndex
 TDBGridReportDBGridLeft Top9WidthHeightrAlignalClient
DataSourceData.StatistikaQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightõTitleFont.NameMS Sans SerifTitleFont.Style 	OnKeyDownReportDBGridKeyDownColumnsExpanded	FieldName
DAY_NUMBERTitle.AlignmenttaCenterTitle.CaptionDagVisible	 	AlignmenttaRightJustifyExpanded	FieldNameDATE_TIME_DATETitle.AlignmenttaCenterTitle.CaptionDatoWidthJVisible	 Expanded	FieldName
STAT_COUNTTitle.AlignmenttaCenterTitle.CaptionAntalVisible	 Expanded	FieldNameAVERAGE_WEIGHTTitle.AlignmenttaCenterTitle.Caption
GennemsnitVisible	 Expanded	FieldNameGAINTitle.AlignmenttaCenterTitle.CaptionDaglig tilvækstVisible	 Expanded	FieldNameSIGMATitle.AlignmenttaCenterTitle.CaptionSt. afvigelseVisible	 Expanded	FieldNameCVTitle.AlignmenttaCenterTitle.CaptionCv [%]Visible	 Expanded	FieldNameUNITitle.AlignmenttaCenterTitle.CaptionUNI [%]Visible	 Expanded	FieldNameCOMPARETitle.AlignmenttaCenterTitle.Caption
SammenlignVisible	 Expanded	FieldName
DIFFERENCETitle.AlignmenttaCenterTitle.Caption
ForskellenVisible	    TPanelReportPanelLeft Top WidthHeight9AlignalTop
BevelOuterbvNoneTabOrder TPanelPanel4LeftxTop WidthHeight9AlignalRight
BevelOuterbvNoneTabOrder  TLabelLabel58LeftPTopWidthSHeight	AlignmenttaRightJustifyCaptionSammenlign med:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonReportPorovnatSpeedButtonLeftsTopWidthHeightCaption...Flat	OnClickGrowthcurves1Click  	TComboBoxReportPorovnatComboBoxLeft° TopWidthÁ HeightStylecsDropDownList
ItemHeight TabOrder OnChangeKrivkaPorovnatComboBoxChange     	TTabSheetVazeniTabSheetCaption	Vejninger
ImageIndex TDBGridDBGrid1Left Top WidthHeight·AlignalClient
DataSourceData.ZaznamyQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightõTitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameSAMPLETitle.AlignmenttaCenterTitle.CaptionVejningVisible	 Expanded	FieldName	TIME_HOURTitle.AlignmenttaCenterTitle.CaptionTimerVisible	 Expanded	FieldNameWEIGHTTitle.AlignmenttaCenterTitle.CaptionVægtVisible	     	TTabSheetStatistikaTabSheetCaption	Statistik
ImageIndex TLabelLabel3LeftTop Width%HeightCaptionØnsket:  TLabelCilovaHmotnostLabelLefthTop Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelLabel5LeftTop0Width;HeightCaptionGennemsnit:  TLabelPrumerLabelLefthTop0Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelLabel7LeftTopXWidthHeightCaptionAntal:  TLabel
PocetLabelLeftkTopXWidthHeight	AlignmenttaRightJustifyCaption00000  TLabelLabel9LeftTophWidth=HeightCaptionSt. afvigelse:  TLabel
SigmaLabelLeftnTophWidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelLabel11LeftTop Width5HeightCaptionUniformitet:  TLabelUniLabelLeftwTop WidthHeight	AlignmenttaRightJustifyCaption000  TLabelLabel13LeftTopxWidthHeightCaptionCv:  TLabelCvLabelLeftwTopxWidthHeight	AlignmenttaRightJustifyCaption000  TLabelLabel15LeftTop@WidthEHeightCaptionDaglig tilvækst:  TLabelPrirustekLabelLefthTop@Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelCilovaHmotnostJednotkyLabelLeft Top WidthHeightCaptionkg  TLabelPrumerJednotkyLabelLeft Top0WidthHeightCaptionkg  TLabelPrirustekJednotkyLabelLeft Top@WidthHeightCaptionkg  TLabelLabel20Left Top WidthHeightCaption%  TLabelLabel21Left TopxWidthHeightCaption%  TPanelStatistikaPorovnaniPanelLeft° TopWidth Height 
BevelOuterbvNoneTabOrder  TLabelCilovaHmotnostPorovnaniLabelLeftTopWidth!Height	AlignmenttaRightJustifyCaption00,000  TLabelPrumerPorovnaniLabelLeftTop(Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelPocetPorovnaniLabelLeftTopPWidthHeight	AlignmenttaRightJustifyCaption00000  TLabelSigmaPorovnaniLabelLeftTop`WidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelUniPorovnaniLabelLeftTop WidthHeight	AlignmenttaRightJustifyCaption000  TLabelCvPorovnaniLabelLeftToppWidthHeight	AlignmenttaRightJustifyCaption000  TLabelPrirustekPorovnaniLabelLeftTop8Width!Height	AlignmenttaRightJustifyCaption00,000  TLabel$CilovaHmotnostJednotkyPorovnaniLabelLeft0TopWidthHeightCaptionkg  TLabelPrumerJednotkyPorovnaniLabelLeft0Top(WidthHeightCaptionkg  TLabelPrirustekJednotkyPorovnaniLabelLeft0Top8WidthHeightCaptionkg  TLabelLabel22Left0Top WidthHeightCaption%  TLabelLabel23Left0ToppWidthHeightCaption%  TLabelStatistikaPorovnaniLabelLeftTopWidthGHeightCaptionSammenligning    	TTabSheetHistogramTabSheetCaption	Histogram
ImageIndex 	TSplitterHistogramSplitterLeft TopWidthHeightCursorcrVSplitAlignalBottomBeveled	  TChartHistogramChartLeft Top WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.Increment;ßOnõ?BottomAxis.MinorTickLengthBottomAxis.Title.CaptionVægtLeftAxis.Grid.Colorªªª LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionAntalLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeriesSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelHistogramPorovnaniPanelLeft TopWidthHeight© AlignalBottom
BevelOuterbvNoneTabOrder TChartHistogramPorovnaniChartLeft TopWidthHeight BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.Increment;ßOnõ?BottomAxis.MinorTickLengthBottomAxis.Title.CaptionVægtLeftAxis.Grid.Colorªªª LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionAntalLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeries
BarSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelPanel1Left Top WidthHeightAlignalTop
BevelOuterbvNoneTabOrder TLabelHistogramPorovnaniLabelLeftTopWidthGHeightCaptionSammenligning  TPanelPanel2LeftWTop Width¹ HeightAlignalRight
BevelOuterbvNoneTabOrder  	TCheckBoxHistogramPorovnaniOsaCheckBoxLeftPTopWidthYHeightCaption
Samme akseTabOrder OnClick"HistogramPorovnaniOsaCheckBoxClick      	TTabSheetAktivitaDenTabSheetCaptionDaglig aktivitet
ImageIndex 	TSplitterAktivitaSplitterLeft TopWidthHeightCursorcrVSplitAlignalBottomBeveled	  TChartAktivitaDenChartLeft Top WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.IncrementBottomAxis.MaximumBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionTid [timer]LeftAxis.Grid.Colorªªª LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionAntalLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeries
BarSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZOffsetPercent<XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelAktivitaPorovnaniPanelLeft TopWidthHeight© AlignalBottom
BevelOuterbvNoneTabOrder TPanelPanel5Left Top WidthHeightAlignalTop
BevelOuterbvNoneTabOrder  TLabelAktivitaPorovnaniLabelLeftTopWidthGHeightCaptionSammenligning  TPanelPanel6LeftWTop Width¹ HeightAlignalRight
BevelOuterbvNoneTabOrder    TChartAktivitaDenPorovnaniChartLeft TopWidthHeight BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.IncrementBottomAxis.MaximumBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionTid [timer]LeftAxis.Grid.Colorªªª LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionAntalLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder 
TBarSeries
BarSeries4Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZOffsetPercent<XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone     	TTabSheetAktivitaCelkemTabSheetCaptionTotal aktivitet
ImageIndex TChartAktivitaCelkemChartLeft Top WidthHeight·BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.Grid.Colorªªª BottomAxis.Grid.StylepsSolidBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionDagLeftAxis.Grid.Colorªªª LeftAxis.Grid.StylepsSolidLeftAxis.Title.CaptionAntalLegend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0View3DAlignalClient
BevelOuterbvNoneTabOrder  TLineSeriesLineSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclRedTitle	AktivitetPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesLineSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclNavyTitleSammenligningPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone    	TTabSheetKrivkaTabSheetCaption
Vækstkurve
ImageIndex TChartKrivkaChartLeft Top>WidthHeightmBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.Grid.Colorªªª BottomAxis.Grid.StylepsSolidBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionDagLeftAxis.ExactDateTimeLeftAxis.Grid.Colorªªª LeftAxis.Grid.StylepsSolidLeftAxis.IncrementÍÌÌÌÌÌÌÌû?LeftAxis.Title.CaptionVægtLegend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0Legend.VisibleRightAxis.Grid.VisibleRightAxis.Title.Caption	StatistikRightAxis.Title.Font.CharsetDEFAULT_CHARSETRightAxis.Title.Font.ColorclGreenRightAxis.Title.Font.HeightõRightAxis.Title.Font.NameArialRightAxis.Title.Font.Style View3DAlignalClient
BevelOuterbvNoneTabOrder  TLineSeries
BarSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclRedTitleRealPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclNavyTitleØnsketPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclPurpleTitle
Real saml.Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries4Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColor@ TitleØnsket saml.Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries5Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclOliveTitleTheoryPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries6Marks.ArrowLengthMarks.VisibleSeriesColorclGreenTitle	StatistikVertAxis
aRightAxisPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone   TPanelKrivkaPanelLeft Top WidthHeight>AlignalTop
BevelOuterbvNoneTabOrder 	TCheckBoxKrivkaRealCheckBoxLeftTopWidth9HeightCaptionRealChecked	Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightõ	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontState	cbCheckedTabOrder OnClickKrivkaRealCheckBoxClick  	TCheckBoxKrivkaTargetCheckBoxLeft`TopWidthIHeightCaptionØnsketChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKrivkaTargetCheckBoxClick  TPanelKrivkaPorovnaniPanelLeft¸ Top Width¹ Height"
BevelOuterbvNoneTabOrder TLabelKrivkaPorovnaniLabelLeft TopWidthJHeightCaptionSammenligning:  	TCheckBoxKrivkaCRealCheckBoxLeft TopWidth1HeightCaptionRealChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder OnClickKrivkaCRealCheckBoxClick  	TCheckBoxKrivkaCTargetCheckBoxLeftPTopWidthAHeightCaptionØnsketChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKrivkaCTargetCheckBoxClick   TPanelKrivkaTeoriePanelLeftxTop WidthHeight>AlignalRight
BevelOuterbvNoneTabOrder TLabelLabel52LeftPTopWidthSHeight	AlignmenttaRightJustifyCaptionSammenlign med:Font.CharsetDEFAULT_CHARSET
Font.ColorclOliveFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonKrivkaPorovnatSpeedButtonLeftsTopWidthHeightCaption...Flat	OnClickGrowthcurves1Click  TLabelLabel60LeftmTop+Width6Height	AlignmenttaRightJustifyCaptionHøjre aksel:Font.CharsetDEFAULT_CHARSET
Font.ColorclGreenFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxKrivkaPorovnatComboBoxLeft° TopWidthÁ HeightStylecsDropDownList
ItemHeight TabOrder OnChangeKrivkaPorovnatComboBoxChange  	TComboBoxCurveRightAxisComboBoxLeft° Top(WidthÁ HeightStylecsDropDownList
ItemHeightTabOrderOnChangeCurveRightAxisComboBoxChangeItems.Strings<ingen>Daglig tilvækstAntalSt. afvigelseCvUniformitet      	TTabSheetOnlineTabSheetCaptionOnline tabel
ImageIndex TDBGridOnlineDBGridLeft Top WidthHeight·AlignalClient
DataSourceData.OnlineQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightõTitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameSAMPLETitle.AlignmenttaCenterTitle.CaptionVejningVisible	 Expanded	FieldName	TIME_HOURTitle.AlignmenttaCenterTitle.CaptionTimerVisible	 Expanded	FieldNameWEIGHTTitle.AlignmenttaCenterTitle.CaptionVægtVisible	 	AlignmenttaCenterExpanded	FieldNameSTABLETitle.AlignmenttaCenterTitle.CaptionStabilWidth>Visible	 	AlignmenttaCenterExpanded	FieldNameSAVEDTitle.AlignmenttaCenterTitle.CaptionGemtWidth?Visible	     	TTabSheetOnlineGrafTabSheetCaptionOnline grafik
ImageIndex	 TChartOnlineChartLeft Top)WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.Visible
OnUndoZoomOnlineChartUndoZoomBottomAxis.Grid.Colorªªª BottomAxis.Grid.StylepsSolidBottomAxis.LabelsSeparationBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.StartPositionBottomAxis.EndPositioncBottomAxis.Title.Caption	VejningerLeftAxis.AxisValuesFormat	#,##0.000LeftAxis.ExactDateTimeLeftAxis.Grid.Colorªªª LeftAxis.Grid.StylepsSolidLeftAxis.Increment;ßOnõ?LeftAxis.StartPositionLeftAxis.Title.CaptionVægtLegend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0Legend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder OnMouseMoveOnlineChartMouseMove TFastLineSeriesLineSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.VisibleSeriesColorclRedTitleOnlineLinePen.ColorclRedXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone   TPanelOnlineGrafPanelLeft Top WidthHeight)AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel44LeftpTopWidth$HeightCaptionMidling:  TLabelLabel45Leftð TopWidth+HeightCaption	vejninger  TLabelLabel46LeftHTopWidthHeightCaptionVægt:  TLabelOnlineHmotnostLabelLeftTopWidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelOnlineHmotnostJednotkyLabelLeft TopWidthHeightCaptionkg  TSpeedButtonOnlineZoomInSpeedButtonLeftTopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ {°»{°  »°    OnClickOnlineZoomInSpeedButtonClick  TSpeedButtonOnlineZoomOutSpeedButtonLeft(TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ {°»{°  »°    OnClickOnlineZoomOutSpeedButtonClick  TBevelBevel4LeftXTopWidthHeightShape
bsLeftLine  TBevelBevel6Left-TopWidthHeightShape
bsLeftLine  	TComboBoxOnlinePrumerovaniComboBoxLeft¸ TopWidth1HeightStylecsDropDownListDropDownCount
ItemHeightTabOrder OnChangeOnlinePrumerovaniComboBoxChangeItems.Strings12345678910111213141516171819202122232425262728293031323334353637383940     	TTabSheetNastaveniTabSheetCaption	Opsætning
ImageIndex TPageControlNastaveniPageControlLeftTopWidthHeight©
ActivePageNastaveniVahyTabSheetTabOrder  	TTabSheetNastaveniVazeniTabSheetCaptionVejning
ImageIndex TLabelLabel42LeftTopXWidthHeightCaptionHold:  TLabelLabel59LeftToppWidth<HeightCaptionSammenlign:  TDBCheckBoxDBCheckBox7LeftTopWidthHeightCaptionVejning påbegyndt	DataFieldSTART_PROGRESS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder ValueCheckedTrueValueUncheckedFalse  TEditNastaveniHejnoEditLefthTopSWidthQHeightReadOnly	TabOrder  TDBCheckBoxDBCheckBox8LeftTop0WidthHeightCaptionAfventer opstart	DataFieldSTART_WAITING
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit11LefthTopmWidthQHeight	DataFieldCOMPARE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniHejnaTabSheetCaptionHold
ImageIndex TLabelLabel4LeftTopWidthHeightCaptionHold:  TListBoxSeznamHejnListBoxLeftTopWidthKHeight!
ItemHeightSorted	TabOrder OnClickSeznamHejnListBoxClick  TPanel
HejnaPanelLefthTopWidthñ HeightY
BevelOuterbvNoneTabOrder TLabelLabel6LeftTopWidthHeightCaptionNavn:  TLabelLabel24LeftTophWidth'HeightCaption	Veje fra:  TLabelLabel25Left TophWidth
HeightCaptiontil:  TDBEditJmenoDBEditLeftXTopWidth Height	DataFieldNAME
DataSourceData.HejnaDataSourceReadOnly	TabOrder   TDBEditDBEdit15LeftPTopeWidthHeight	DataField
WEIGH_FROM
DataSourceData.HejnaDataSourceReadOnly	TabOrder  TDBEditDBEdit16Left  TopeWidthHeight	DataFieldWEIGH_TO
DataSourceData.HejnaDataSourceReadOnly	TabOrder  TPageControlKrivkyPageControlLeftTop Widthá Height© 
ActivePageKrivkaSamiceTabSheetTabOrder 	TTabSheetKrivkaSamiceTabSheetCaptionHunner TStringGridKrivkaSamiceStringGridLeft Top WidthÙ Height AlignalClientColCountDefaultRowHeightRowCountTabOrder 	ColWidthsB]    	TTabSheetKrivkaSamciTabSheetCaptionHanner
ImageIndex TStringGridKrivkaSamciStringGridLeft Top WidthÙ Height AlignalClientColCountDefaultRowHeightRowCountTabOrder 	ColWidthsB]    	TTabSheetPocatecniHmotnostTabSheetCaptionBegyndelsesvægt
ImageIndex TLabelLabel26LeftTopWidth&HeightCaptionHunner:  TDBEditPocatecniHmotnostSamiceDBEditLeft`TopWidthaHeight	DataFieldFEMALE_WEIGHT00
DataSourceData.HejnaDataSourceReadOnly	TabOrder   TPanelPocatecniHmotnostSamciPanelLeftTop0WidthÉ Height!
BevelOuterbvNoneTabOrder TLabelLabel27LeftTopWidth&HeightCaptionHanner:  TDBEditPocatecniHmotnostSamciDBEditLeftXTop WidthaHeight	DataFieldMALE_WEIGHT00
DataSourceData.HejnaDataSourceReadOnly	TabOrder      TDBCheckBoxDBCheckBox5LeftTop0WidthÑ HeightCaptionAnvend begge køn	DataField
USE_GENDER
DataSourceData.HejnaDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox6LeftTopHWidthÑ HeightCaptionAnvend vækstkurve	DataField
USE_CURVES
DataSourceData.HejnaDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse    	TTabSheetNastaveniStatistikaTabSheetCaption	Statistik TLabelLabel8LeftTopWidthWHeightCaptionHistogram interval:  TLabelLabel10LeftTop0Width_HeightCaptionUniformitets interval:  TLabelLabel12Left~Top0WidthHeightCaption±  TLabelLabel14Left¬ Top0WidthHeightCaption%  TLabelLabel16Left¬ TopWidthHeightCaption%  TLabelLabel28Left~TopWidthHeightCaption±  TDBEditRozsahUniformityDBEditLeft Top-Width!Height	DataField	UNI_RANGE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditRozsahHistogramuDBEditLeft TopWidth!Height	DataField
HIST_RANGE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder    	TTabSheetNastaveniGsmTabSheetCaptionGSM
ImageIndex TLabelLabel29LeftTopØ Width"HeightCaptionNumre:  TLabelLabel48Left"TopHWidth*HeightCaption	Period 1:  TLabelLabel49Left TopHWidthHeightCaptiondage  TLabelLabel66Left"Top`WidthHeightCaptionDay 1:  TLabelLabel68Left"TopxWidth*HeightCaption	Period 2:  TLabelLabel69Left TopxWidthHeightCaptiondays  TLabelLabel67LeftÈ TopHWidthNHeightCaptionTime of sending:  TLabelLabel70LeftOTopHWidthHeightCaption:  TDBCheckBoxDBCheckBox1LeftTopWidthHeightCaptionAnvend GSM kommunikation	DataFieldGSM_USE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2LeftTop0WidthHeightCaptionSend statistik ved midnat	DataFieldGSM_SEND_MIDNIGHT
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox3LeftTop WidthHeightCaptionSend data på forespørgsel	DataFieldGSM_SEND_REQUEST
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox4LeftTop° WidthHeightCaption
Tjek numre	DataFieldGSM_CHECK_NUMBERS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit3LeftTopè Width Height	DataFieldGSM_NUMBER00
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit4LeftTop Width Height	DataFieldGSM_NUMBER01
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit5LeftTopWidth Height	DataFieldGSM_NUMBER02
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit6LeftTop0Width Height	DataFieldGSM_NUMBER03
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit7LeftTopHWidth Height	DataFieldGSM_NUMBER04
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder	  TDBEditDBEdit8Left`TopEWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT1
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit19Left`Top]Width!Height	DataFieldGSM_DAY_MIDNIGHT1
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder
  TDBEditDBEdit20Left`TopuWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT2
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEdit
HourDBEditLeft(TopEWidth!Height	DataFieldGSM_MIDNIGHT_SEND_HOUR
DataSourceData.KonfiguraceDataSource	MaxLengthTabOrder  TDBEdit	MinDBEditLeftXTopEWidth!Height	DataFieldGSM_MIDNIGHT_SEND_MIN
DataSourceData.KonfiguraceDataSource	MaxLengthTabOrder   	TTabSheetNastaveniRs485TabSheetCaptionRS-485
ImageIndex TLabelLabel50LeftTopWidth<HeightCaptionNet adresse:  TLabelLabel51LeftTop0Width1HeightCaption
Baud rate:  TLabelLabel53LeftTopØ Width[HeightCaptionForsinkelse af svar:  TLabelLabel54LeftTopð WidthJHeightCaptionStilhedsinterval:  TLabelLabel55Left Top0WidthHeightCaptionBd  TLabelLabel56Left TopØ WidthHeightCaptionms  TLabelLabel57Left Topð WidthHeightCaptionms  TDBEditDBEdit9LeftÈ TopWidthQHeight	DataFieldRS485_ADDRESS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   TDBEditDBEdit10LeftÈ Top-WidthQHeight	DataFieldRS485_SPEED
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit12LeftÈ TopÕ WidthQHeight	DataFieldRS485_REPLY_DELAY
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit13LeftÈ Topí WidthQHeight	DataFieldRS485_SILENT_INTERVAL
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBRadioGroupDBRadioGroup4LeftTopPWidth	Height1CaptionParitetColumns	DataFieldRS485_PARITY
DataSourceData.KonfiguraceDataSourceItems.Strings8-n-18-e-18-o-18-n-2 ReadOnly	TabOrderValues.Strings0123   TDBRadioGroupDBRadioGroup5LeftTop Width	Height1CaptionProtokolColumns	DataFieldRS485_PROTOCOL
DataSourceData.KonfiguraceDataSourceItems.Strings
MODBUS RTUMODBUS ASCII ReadOnly	TabOrderValues.Strings0123    	TTabSheetNastaveniVahyTabSheetCaptionVægt
ImageIndex TLabelLabel30LeftTop0Width HeightCaptionMargin over ønsket for hunner:  TLabelLabel31LeftTop0WidthHeightCaption%  TLabelLabel32LeftTopHWidth HeightCaptionMargin under ønsket for hunner:  TLabelLabel33LeftTopHWidthHeightCaption%  TLabelLabel34LeftTop`Width HeightCaptionMargin over ønsket for hanner:  TLabelLabel35LeftTop`WidthHeightCaption%  TLabelLabel36LeftTopxWidth HeightCaptionMargin under ønsket for hanner:  TLabelLabel37LeftTopxWidthHeightCaption%  TLabelLabel38LeftTop¨ Width;HeightCaptionStabilisering:  TLabelLabel39LeftÞ Top¨ WidthHeightCaption±  TLabelLabel40LeftTop¨ WidthHeightCaption%  TLabelLabel41LeftTopÀ WidthKHeightCaptionStabiliseringstid:  TLabelLabel43LeftTop WidthHeightCaptionFilter:  TLabelLabel47LeftTopWidthiHeightCaptionIdentifikationsnummer:  TDBEditSamiceOkoliNadDBEditLeftè Top-Width!Height	DataFieldF_MARGIN_ABOVE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamiceOkoliPodDBEditLeftè TopEWidth!Height	DataFieldF_MARGIN_UNDER
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamciOkoliNadDBEditLeftè Top]Width!Height	DataFieldM_MARGIN_ABOVE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamciOkoliPodDBEditLeftè TopuWidth!Height	DataFieldM_MARGIN_UNDER
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditUstaleniDBEditLeftè Top¥ Width!Height	DataFieldSTABILIZATION
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDobaUstaleniDBEditLeftè Top½ Width!Height	DataFieldSTABILIZATION_TIME
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBRadioGroupDBRadioGroup1LeftTopHWidthù Height1CaptionEnhederColumns	DataFieldUNITS
DataSourceData.KonfiguraceDataSourceItems.Stringskglb ReadOnly	TabOrder
Values.Strings01   TDBEditFiltrDBEditLeftè Top Width!Height	DataFieldFILTER_VALUE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit2Leftè TopWidthyHeight	DataFieldID
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TCheckBoxNastaveniGainCheckBoxLeftTopØ Widthù HeightCaptionAnvend tilvækst i auto modeTabOrder  TDBRadioGroupDBRadioGroup3LeftTopð Widthù HeightICaptionGem vejning ved	DataField	JUMP_MODE
DataSourceData.KonfiguraceDataSourceItems.StringsPåstigning af platformAfstigning af platformBåde på- og afstigning ReadOnly	TabOrder	Values.Strings012    	TTabSheetTabSheetCorrectionCaptionKorrektionskurve
ImageIndex TLabelLabel61LeftTopWidth HeightCaptionDag 1:  TLabelLabel62LeftTopHWidth3HeightCaptionKorrektion:  TLabelLabel64LeftTop0Width HeightCaptionDag 2:  TLabelLabel63Left~TopHWidthHeightCaption+  TLabelLabel65Left¬ TopHWidthHeightCaption%  TDBEditDBEdit14Left TopWidth!Height	DataFieldCORRECTION_DAY1
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   TDBEditDBEdit18Left Top-Width!Height	DataFieldCORRECTION_DAY2
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit17Left TopEWidth!Height	DataFieldCORRECTION_CORRECTION
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniPodsvitTabSheetCaptionBaggrudsbelysning
ImageIndex TDBRadioGroupDBRadioGroup2LeftTopWidthÉ Height9CaptionBaggrudsbelysningColumns	DataField	BACKLIGHT
DataSourceData.KonfiguraceDataSourceItems.StringsSlukTændAuto ReadOnly	TabOrder Values.Strings012      	TTabSheetInformaceTabSheetCaption
Egenskaber
ImageIndex TLabelLabel17LeftTop WidthNHeightCaptionVersion nummer:  TLabel
VerzeLabelLeftxTop Width5HeightCaption
VerzeLabel  TLabelLabel18LeftTopXWidth;HeightCaptionBemærkning:  TLabelLabel19LeftTop8Width8HeightCaptionHentet den:  TDBTextDBText1LeftxTop8Width¹ Height	DataField	DATE_TIME
DataSourceData.HlavickaDataSource  TDBEditDBEdit1LeftxTopUWidthé Height	DataFieldNOTE
DataSourceData.HlavickaDataSourceTabOrder     TPanelPanel3Left Top WidthHeight%AlignalTop
BevelOuterbvNoneTabOrder TBevelBevel2Left TopWidthHeightAlignalBottomShape	bsTopLine  TBevelBevel3Left Top WidthHeightAlignalTopShape	bsTopLine  TSpeedButtonOpenSpeedButtonLeftTopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                    ÀÀÀ    ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ wwwwwwwwwwwwwwww     ww 33330ww3333w°33330wû3333¿°     ûûûûww¿¿¿¿wwû   wwp wwww wwwwwwpwwwwwwwwwp wwwwwwwwwwParentShowHintShowHint	OnClickOtevt1Click  TSpeedButtonSaveSpeedButtonLeft TopWidthHeightEnabledFlat	
Glyph.Data
ú   ö   BMö       v   (                                                    ÀÀÀ    ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ wwwwwwwww      p3   wp3   wp3   wp3    p333333p3    3p0wwwwp0wwwwp0wwwwp0wwwwp0wwww p0wwwwp      wwwwwwwwParentShowHintShowHint	OnClickUloit1Click  TSpeedButtonReadSpeedButtonLeft`TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ        wpøøøøpÿÿÿðParentShowHintShowHint	OnClickNastzznamy1Click  TSpeedButtonFindSpeedButtonLeft8TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                ÀÀÀ   ÿ ÀÀÀ  ÿÿ ÿ   ÀÀÀ ÿÿ  ÿÿÿ ÚÚÚÚÚÚÚÚ    ­­­ÿÿÿ
ÚÚ ­  
Ú 
   ­ ÿð 
Úÿðp­­pà ðzÚþþð­  ð
Ú þÿð­Úp   zÚ­ p­­ÚÚ 
ÚÚ­­§ ­­­ParentShowHintShowHint	OnClickFindfile1Click  TSpeedButtonSetupSpeedButtonLeftxTopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                ÀÀÀ   ÿ ÀÀÀ  ÿÿ ÿ   ÀÀÀ ÿÿ  ÿÿÿ ÚÚÚÚÚÚÚÚ­­­­­­­­Ð
ÚÚÚ ÚÚ ­­ ­­Ú
Ú
ÚÚ­  ­­­ÚÚ
ÚÚÚ­­ ­­­­ÚÚ
ÚÐÚ   ­  
Ú 0 ­ 0ÚÐ
ÚÐ30Ú­ ­ 30­ÚÚÚ 3 
Ú­­­  ­­ParentShowHintShowHint	OnClickConfig1Click  TSpeedButtonDatabaseSpeedButtonLeftÈ TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                ÀÀÀ   ÿ ÀÀÀ  ÿÿ ÿ   ÀÀÀ ÿÿ  ÿÿÿ ÚÚÚÚÚÚÚÚ­­­­­­­­ÚÚÚÐ    ­­­ ÿÿÿðÚÚ  ð ð­­ðÿÿÿðÐ  ð ð ÿðÿÿÿðÐð ð ð ÿðÿÿÿðÐð      ÿÿÿÿ­Ðð    
Ú ÿÿÿð­­­Ð    ÚÚÚ­­­­­­­­ParentShowHintShowHint	OnClickWeighingdatabase1Click  TSpeedButtonAddSpeedButtonLeftà TopWidthHeightEnabledFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ   ÿððð  0ÿð»»  ðð  ððÿð   ÿÿ  ParentShowHintShowHint	OnClickAddtoweighingdatabase1Click  TBevelBevel7LeftWTopWidthHeightShape
bsLeftLine  TBevelBevel8Left TopWidthHeightShape
bsLeftLine  TSpeedButtonRs485SpeedButtonLeft  TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ    8080ª   80  ParentShowHintShowHint	OnClickRS4851Click  TBevelBevel9Left¿ TopWidthHeightShape
bsLeftLine  TPanelZahlaviPanelLeftTopWidthÁHeight
BevelOuterbvNoneTabOrder Visible TLabelLabel1LeftTopWidthHeightCaptionDag:  TLabel
DatumLabelLeft¦ TopWidth;Height	AlignmenttaCenterAutoSizeCaption
00.00.0000  TSpeedButtonPrvniDenSpeedButtonLeft{Top WidthHeightFlat	
Glyph.Data
²   ®   BM®       v   (               8                                      ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ                 OnClickPrvniDenSpeedButtonClick  TSpeedButtonPredchoziDenSpeedButtonLeft Top WidthHeightFlat	
Glyph.Data
      BM       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ    OnClickPredchoziDenSpeedButtonClick  TSpeedButtonDalsiDenSpeedButtonLeftâ Top WidthHeightFlat	
Glyph.Data
      BM       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ           OnClickDalsiDenSpeedButtonClick  TSpeedButtonPosledniDenSpeedButtonLeft÷ Top WidthHeightFlat	
Glyph.Data
²   ®   BM®       v   (               8                                      ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ                 OnClickPosledniDenSpeedButtonClick  TBevelBevel5Left TopWidthHeightShape
bsLeftLine  TPanelPohlaviPanelLeft Top Width Height
BevelOuterbvNoneTabOrder  TLabelLabel2LeftTopWidthHeightCaptionKøn:  TSpeedButtonSpeedButton1LeftXTopWidthHeightEnabledFlat	
Glyph.Data
"    BM      v   (               ¨                                      ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ                         px                     px        	NumGlyphs  TSpeedButtonSpeedButton2Left TopWidthHeightEnabledFlat	
Glyph.Data
þ   ú   BMú       v   (                                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ    px       p          	NumGlyphs  TBevelBevel1Left TopWidthHeightShape
bsLeftLine  TRadioButtonPohlaviSamiceRadioButtonLeftHTopWidthHeightChecked	TabOrder TabStop	OnClickPohlaviSamiceRadioButtonClick  TRadioButtonPohlaviSamciRadioButtonLeftpTopWidthHeightTabOrderOnClickPohlaviSamciRadioButtonClick   	TComboBoxDenComboBoxLeft8TopWidth1HeightStylecsDropDownListDropDownCount
ItemHeightTabOrderOnChangeDenComboBoxChange    	TMainMenu	MainMenu1Left¨Top 	TMenuItemSoubor1CaptionFil 	TMenuItemOtevt1CaptionÅbnShortCutO@OnClickOtevt1Click  	TMenuItemUloit1CaptionGemShortCutS@OnClickUloit1Click  	TMenuItemN7Caption-  	TMenuItem	Findfile1CaptionFind fil...OnClickFindfile1Click  	TMenuItemN14Caption-  	TMenuItemOpenforcomparsion1CaptionÅbn for sammenligningOnClickOpenforcomparsion1Click  	TMenuItemN2Caption-  	TMenuItemTisk1CaptionUdskriv 	TMenuItemMenuPrintReportCaptionRapportOnClickMenuPrintReportClick  	TMenuItemMenuPrintSamplesCaption	VejningerOnClickMenuPrintSamplesClick  	TMenuItemMenuPrintHistogramCaption	HistogramOnClickMenuPrintHistogramClick  	TMenuItemMenuPrintDayActivityCaptionDaglig aktivitetOnClickMenuPrintDayActivityClick  	TMenuItemMenuPrintTotalActivityCaptionTotal aktivitetOnClickMenuPrintTotalActivityClick  	TMenuItemMenuPrintGrowthCurveCaption
VækstkurveOnClickMenuPrintGrowthCurveClick  	TMenuItemMenuPrintOnlineCaptionOnline grafikOnClickMenuPrintOnlineClick   	TMenuItemExport1CaptionEksporter til Excel 	TMenuItemMenuExportReportCaptionRapportOnClickMenuExportReportClick  	TMenuItemMenuExportSamplesCaption	VejningerOnClickMenuExportSamplesClick  	TMenuItemMenuExportHistogramCaption	HistogramOnClickMenuExportHistogramClick  	TMenuItemMenuExportDayActivityCaptionDaglig aktivitetOnClickMenuExportDayActivityClick  	TMenuItemMenuExportTotalActivityCaptionTotal aktivitetOnClickMenuExportTotalActivityClick  	TMenuItemMenuExportGrowthCurveCaption
VækstkurveOnClickMenuExportGrowthCurveClick  	TMenuItemMenuExportOnlineCaptionOnline tabelOnClickMenuExportOnlineClick   	TMenuItemN4Caption-  	TMenuItemMenuSimulationCaptionSimulering...OnClickMenuSimulationClick  	TMenuItemN5Caption-  	TMenuItemKonecprogramu1CaptionAbrydOnClickKonecprogramu1Click   	TMenuItemModul1CaptionVægt 	TMenuItemMemorymodule1CaptionHukommelsesmodul 	TMenuItemNastzznamy1CaptionLæs data...OnClickNastzznamy1Click  	TMenuItemConfig1CaptionOpsætning...OnClickConfig1Click  	TMenuItemDiagnostics1Caption
Diagnostik 	TMenuItemReaddatatofile1CaptionGem fra modul til fil...OnClickReaddatatofile1Click  	TMenuItemReaddatafromfile1CaptionIndlæs fra fil til modul...OnClickReaddatafromfile1Click    	TMenuItemRS4851CaptionRS-485 vægte...OnClickRS4851Click  	TMenuItemN3Caption-  	TMenuItemGrowthcurves1CaptionVækstkurver...OnClickGrowthcurves1Click   	TMenuItem	Database1CaptionDatabase 	TMenuItemWeighingdatabase1CaptionVis veje database...OnClickWeighingdatabase1Click  	TMenuItemN6Caption-  	TMenuItemAddtoweighingdatabase1CaptionTilføj fil til veje database...EnabledOnClickAddtoweighingdatabase1Click   	TMenuItem	Nastaven1CaptionIndstillinger 	TMenuItemJazyk1CaptionSprog 	TMenuItemDansk1CaptionDanskOnClickDansk1Click  	TMenuItemDeutsch1CaptionDeutschOnClickDeutsch1Click  	TMenuItem	Anglicky1CaptionEnglishOnClickAnglicky1Click  	TMenuItemEspanol1CaptionEspanolOnClickEspanol1Click  	TMenuItemFrancouzsky1CaptionFrançaisOnClickFrancouzsky1Click  	TMenuItemRussian1CaptionRussianOnClickRussian1Click  	TMenuItemFinsky1CaptionSuomiOnClickFinsky1Click  	TMenuItemTurkce1CaptionTurkceOnClickTurkce1Click    	TMenuItemNpovda1CaptionHjælp 	TMenuItem
Oprogramu1CaptionOm...OnClickOprogramu1Click    TOpenDialog
OpenDialogFilter
Bat2|*.bt2OptionsofHideReadOnlyofPathMustExistofFileMustExistofEnableSizing LeftèTop   TSaveDialog
SaveDialogFilter
Bat2|*.bt2OptionsofOverwritePromptofHideReadOnlyofPathMustExistofEnableSizing LeftÊTop  TOpenDialogOpenDialogHardCopyFilterBat2 hardcopy|*.binOptionsofHideReadOnlyofPathMustExistofFileMustExistofEnableSizing Leftè  TSaveDialogSaveDialogHardCopyFilterBat2 hardcopy|*.binOptionsofOverwritePromptofHideReadOnlyofPathMustExistofEnableSizing LeftÊTopüÿ     