//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SimulaceStart.h"
#include "Konstanty.h"
#include "Simulate.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSimulaceStartForm *SimulaceStartForm;


static bool KontrolaCislaInt(TEdit *Edit) {
  try {
    if (Edit->Text.ToInt() == 0) {
      throw 1;      // Nesmi zadat nulu
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    Edit->SetFocus();
    return false;
  }
  return true;
}

static bool KontrolaCislaDouble(TEdit *Edit) {
  try {
    if (Edit->Text.ToDouble() == 0) {
      throw 1;      // Nesmi zadat nulu
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    Edit->SetFocus();
    return false;
  }
  return true;
}

#define ZkontrolujRozsah(Edit, Typ, Min, Max)   \
  try {                                         \
    if (Edit->Text.IsEmpty()) {                 \
      throw 1;                                  \
    }                                           \
    if (Edit->Text.Typ < Min || Edit->Text.Typ > Max) {     \
      throw 1;                                  \
    }                                           \
  } catch(...) {                                \
    Edit->SetFocus();                           \
    return;                                     \
  }


//---------------------------------------------------------------------------
__fastcall TSimulaceStartForm::TSimulaceStartForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSimulaceStartForm::OdEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //   Backspace      0   az   9
  //       |          |        |
  // 3.9.2001: Chtel natvrdo carku, i kdyz ve Win byla nastavena tecka a pak to blblo
//  if (Key!=8 && (Key<48 || Key>57) && Key!=DecimalSeparator) Key=0;  // Vyberu jen cisla a carku
  if (Key!=8 && (Key<48 || Key>57)) Key=0;  // Vyberu jen cisla
}
//---------------------------------------------------------------------------
void __fastcall TSimulaceStartForm::FormShow(TObject *Sender)
{
  CilovaHmotnostSamiceEdit->SetFocus();
  ObePohlaviCheckBoxClick(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TSimulaceStartForm::Button1Click(TObject *Sender)
{
  // Kontrola zadanych cisel
  if (RozsahRadioButton->Checked) {
    if (!KontrolaCislaInt(OdEdit)) {
      return;
    }
    if (!KontrolaCislaInt(DoEdit)) {
      return;
    }
  }//if
  if (!KontrolaCislaDouble(CilovaHmotnostSamiceEdit)) {
    return;
  }
  if (ObePohlaviCheckBox->Checked && !KontrolaCislaDouble(CilovaHmotnostSamciEdit)) {
    return;
  }
  ZkontrolujRozsah(RozsahHistogramuEdit, ToInt(),    MIN_HISTOGRAM_RANGE,              MAX_HISTOGRAM_RANGE);
  ZkontrolujRozsah(RozsahUniformityEdit, ToInt(),    MIN_UNIFORMITY_RANGE,             MAX_UNIFORMITY_RANGE);
  ZkontrolujRozsah(SamiceOkoliNadEdit,   ToInt(),    MIN_MARGIN,                       MAX_MARGIN);
  ZkontrolujRozsah(SamiceOkoliPodEdit,   ToInt(),    MIN_MARGIN,                       MAX_MARGIN);
  ZkontrolujRozsah(SamciOkoliNadEdit,    ToInt(),    MIN_MARGIN,                       MAX_MARGIN);
  ZkontrolujRozsah(SamciOkoliPodEdit,    ToInt(),    MIN_MARGIN,                       MAX_MARGIN);
  ZkontrolujRozsah(FiltrEdit,            ToInt(),    MIN_FILTER,                       MAX_FILTER);
  ZkontrolujRozsah(UstaleniEdit,         ToDouble(), (MIN_STABILIZATION_RANGE / 10.0), (MAX_STABILIZATION_RANGE / 10.0));
  ZkontrolujRozsah(DobaUstaleniEdit,     ToInt(),    MIN_STABILIZATION_TIME,           MAX_STABILIZATION_TIME);
  if (JumpModeRadioGroup->ItemIndex < 0) {
    return;             // Neni vybrany zadny rezim (nemelo by nastat)
  }

  // Vse je v poradku, zkopiruju udaje do struktury
  Simulate.RozdelovatPohlavi = ObePohlaviCheckBox->Checked;
  Simulate.NormovanaHmotnostSamice = FormatFloat("0", 1000.0 * CilovaHmotnostSamiceEdit->Text.ToDouble()).ToInt();  // Blbne zaokrouhleni
  if (Simulate.RozdelovatPohlavi) {
    Simulate.NormovanaHmotnostSamci = FormatFloat("0", 1000.0 * CilovaHmotnostSamciEdit->Text.ToDouble()).ToInt();
  }
  Simulate.SamiceOkoliNad = SamiceOkoliNadEdit->Text.ToInt();
  Simulate.SamiceOkoliPod = SamiceOkoliPodEdit->Text.ToInt();
  Simulate.SamciOkoliNad = SamciOkoliNadEdit->Text.ToInt();
  Simulate.SamciOkoliPod = SamciOkoliPodEdit->Text.ToInt();
  Simulate.Filtr = FiltrEdit->Text.ToInt();
  Simulate.UstaleniNaslapneVahy = 10.0 * UstaleniEdit->Text.ToDouble();
  Simulate.DelkaUstaleniNaslapneVahy = DobaUstaleniEdit->Text.ToInt();
  Simulate.RozsahHistogramu = RozsahHistogramuEdit->Text.ToInt();
  Simulate.RozsahUniformity = RozsahUniformityEdit->Text.ToInt();
  // Vypoctu pomocne veliciny
  Simulate.HorniMezSamice=(100L+Simulate.SamiceOkoliNad)*Simulate.NormovanaHmotnostSamice;
  Simulate.HorniMezSamice+=50L;  // Zaokrouhlim
  Simulate.HorniMezSamice/=100L;
  Simulate.DolniMezSamice=(100L-Simulate.SamiceOkoliPod)*Simulate.NormovanaHmotnostSamice;
  Simulate.DolniMezSamice+=50L;  // Zaokrouhlim
  Simulate.DolniMezSamice/=100L;
  // Pokud se pouziva rodeleni podle pohlavi, nactu horni a dolni mez hmotnosti i pro samce
  if (Simulate.RozdelovatPohlavi) {
    Simulate.HorniMezSamci=(100L+Simulate.SamciOkoliNad)*Simulate.NormovanaHmotnostSamci;
    Simulate.HorniMezSamci+=50L;  // Zaokrouhlim
    Simulate.HorniMezSamci/=100L;
    Simulate.DolniMezSamci=(100L-Simulate.SamciOkoliPod)*Simulate.NormovanaHmotnostSamci;
    Simulate.DolniMezSamci+=50L;  // Zaokrouhlim
    Simulate.DolniMezSamci/=100L;
  }//if
  // Dosadim do obou histogramu stredni hodnoty a krok
  Simulate.HistSamice.Center = Simulate.NormovanaHmotnostSamice;
  HistogramSetStep(&Simulate.HistSamice, Simulate.RozsahHistogramu);
  if (Simulate.RozdelovatPohlavi) {
    Simulate.HistSamci.Center = Simulate.NormovanaHmotnostSamci;
    HistogramSetStep(&Simulate.HistSamci, Simulate.RozsahHistogramu);
  }
  Simulate.JumpMode = (TJumpMode)JumpModeRadioGroup->ItemIndex;

  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TSimulaceStartForm::UstaleniEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //   Backspace      0   az   9
  //       |          |        |
  // 3.9.2001: Chtel natvrdo carku, i kdyz ve Win byla nastavena tecka a pak to blblo
  if (Key!=8 && (Key<48 || Key>57) && Key!=DecimalSeparator) Key=0;  // Vyberu jen cisla a carku
//  if (Key!=8 && (Key<48 || Key>57)) Key=0;  // Vyberu jen cisla
}
//---------------------------------------------------------------------------
void __fastcall TSimulaceStartForm::ObePohlaviCheckBoxClick(
      TObject *Sender)
{
  CilovaHmotnostSamciEdit->Visible = ObePohlaviCheckBox->Checked;
  Label6->Visible = ObePohlaviCheckBox->Checked;
}
//---------------------------------------------------------------------------
