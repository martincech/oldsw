�
 TWEIGHINGRECORDFORM 0u  TPF0TWeighingRecordFormWeighingRecordFormLeftTop�BorderIconsbiSystemMenu BorderStylebsSingleCaptionEnregistrementClientHeight7ClientWidthSColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TButtonButton1Left�TopWidthKHeightCaptionSauvegarderDefault	TabOrderOnClickButton1Click  TButtonButton2Left�TopWidthKHeightCancel	CaptionAnnulerModalResultTabOrder  	TGroupBox	GroupBox1LeftTopWidth� HeightaCaptionInformation enregistrementTabOrder  TLabelLabel16LeftTopWidth+HeightCaption
Insertion:  TLabelLabel17LeftTop0WidthMHeightCaptionDerniere �dition:  TLabelLabel18LeftTopHWidthCHeightCaptionNum�ro GSM:  TLabelInsertLabelLeftpTopWidthnHeightAutoSizeCaptionInsertLabel  TLabel	EditLabelLeftpTop0WidthnHeightAutoSizeCaption	EditLabel  TLabelGSMLabelLeftpTopHWidthnHeightAutoSizeCaptionGSMLabel   	TGroupBox	GroupBox2LeftTop� Width� Height� CaptionBalancesTabOrder TLabelLabel1LeftTopWidth&HeightCaptionEchelle:  TLabelLabel2LeftTop0WidthHeightCaptionJour:  TLabelLabel3LeftTopHWidthHeightCaptionDate:  TLabelLabel19LeftTop`WidthHeightCaptionNote:  TEdit
ScalesEditLeftxTopWidthYHeight	MaxLength
TabOrder 
OnKeyPressScalesEditKeyPress  TEditDayEditLeftxTop-WidthYHeight	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TDateTimePickerDateTimePickerLeftxTopEWidthYHeightCalAlignmentdtaLeftDateUn!�1���@TimeUn!�1���@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder  TEditNoteEditLeftxTop]WidthYHeight	MaxLengthTabOrder   	TGroupBox	GroupBox3Left TopWidthAHeight� CaptionStatistiquesTabOrder 	TCheckBoxBothGendersCheckBoxLeftTopWidth� HeightCaptionSexes melangesTabOrder OnClickBothGendersCheckBoxClick  	TGroupBoxFemalesGroupBoxLeftTop0Width� Height� CaptionFemelles / TousTabOrder TLabelLabel4LeftTopWidth(HeightCaptionNombre:  TLabelLabel5LeftTop0Width/HeightCaptionMoyenne:  TLabelLabel6LeftTopHWidthHeightCaptionGMQ:  TLabelLabel7LeftTop`Width3HeightCaptionEcart type:  TLabelLabel8LeftTopxWidthHeightCaptionCv:  TLabelLabel9LeftTop� Width2HeightCaptionUniformite:  TEditFemalesCountEditLeftXTopWidth!Height	MaxLengthTabOrder 
OnKeyPressScalesEditKeyPress  TEditFemalesAverageEditLeftXTop-Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditFemalesGainEditLeftXTopEWidth!HeightTabOrder
OnKeyPressFemalesGainEditKeyPress  TEditFemalesSigmaEditLeftXTop]Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditFemalesCvEditLeftXTopuWidth!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TEditFemalesUniEditLeftXTop� Width!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress   	TGroupBoxMalesGroupBoxLeft� Top0Width� Height� CaptionMalesTabOrder TLabelLabel10LeftTopWidth(HeightCaptionNombre:  TLabelLabel11LeftTop0Width/HeightCaptionMoyenne:  TLabelLabel12LeftTopHWidthHeightCaptionGMQ:  TLabelLabel13LeftTop`Width3HeightCaptionEcart type:  TLabelLabel14LeftTopxWidthHeightCaptionCv:  TLabelLabel15LeftTop� Width2HeightCaptionUniformite:  TEditMalesCountEditLeftXTopWidth!Height	MaxLengthTabOrder 
OnKeyPressScalesEditKeyPress  TEditMalesAverageEditLeftXTop-Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditMalesGainEditLeftXTopEWidth!HeightTabOrder
OnKeyPressFemalesGainEditKeyPress  TEditMalesSigmaEditLeftXTop]Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditMalesCvEditLeftXTopuWidth!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TEditMalesUniEditLeftXTop� Width!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress    TButtonPrintButtonLeftTopWidthKHeightCaptionImprimerTabOrderOnClickPrintButtonClick   