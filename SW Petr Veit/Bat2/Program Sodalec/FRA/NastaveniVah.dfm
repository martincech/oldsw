�
 TNASTAVENIVAHFORM 004  TPF0TNastaveniVahFormNastaveniVahFormLeftyTopfBorderIconsbiSystemMenu BorderStylebsSingleCaptionReglageClientHeight�ClientWidthOColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth0HeightCaption	Reglages:  TButton	NewButtonLeftTop� WidthyHeightCaptionNouveauTabOrderOnClickNewButtonClick  TButton
SaveButtonLeftTopWidthyHeightCaptionSauvegarderTabOrderOnClickSaveButtonClick  TButtonUploadButtonLeftTopXWidthyHeightCaptionChargerTabOrderOnClickUploadButtonClick  TListBoxSeznamListBoxLeftTop WidthyHeight� 
ItemHeightSorted	TabOrder OnClickSeznamListBoxClick  TPageControlPageControlLeft� TopWidth�Height�
ActivePageHejnaTabSheetTabOrder 	TTabSheetHejnaTabSheetCaptionLots
ImageIndex TLabelLabel23LeftTopWidthHeightCaptionLots:  TListBoxSeznamHejnListBoxLeftTopWidthKHeight� 
ItemHeightSorted	TabOrder OnClickSeznamHejnListBoxClick  TButtonNewFlockButtonLeftTop WidthKHeightCaptionNouveauTabOrderOnClickNewFlockButtonClick  TButtonSaveFlockButtonLeftTop WidthKHeightCaptionSauvegarderTabOrderOnClickSaveFlockButtonClick  TButtonDeleteFlockButtonLeftTop@WidthKHeightCaptionEffacerTabOrderOnClickDeleteFlockButtonClick  TPanel
HejnaPanelLefthTopWidth� HeightY
BevelOuterbvNoneTabOrder TLabelLabel22LeftTopWidthHeightCaptionNom:  TLabelLabel24LeftTophWidthCHeightCaptionPesee depuis:  TLabelLabel25LeftpTophWidth'HeightCaptionJusqu'a:  TDBEditJmenoDBEditLeftXTopWidth� Height	DataFieldNAME
DataSourceData.NastaveniHejnDataSourceTabOrder   TDBEditDBEdit15LeftPTopeWidthHeight	DataField
WEIGH_FROM
DataSourceData.NastaveniHejnDataSourceTabOrder  TDBEditDBEdit16Left� TopeWidthHeight	DataFieldWEIGH_TO
DataSourceData.NastaveniHejnDataSourceTabOrder  TPageControlKrivkyPageControlLeftTop� Width� Height� 
ActivePageKrivkaSamiceTabSheetTabOrder 	TTabSheetKrivkaSamiceTabSheetCaptionFemelles TStringGridKrivkaSamiceStringGridLeft Top Width� Height� AlignalTopColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsB] 
RowHeights   TButtonFemalesLoadButtonLeft� Top� WidthKHeightCaption
Transf�rerTabOrderOnClickFemalesLoadButtonClick   	TTabSheetKrivkaSamciTabSheetCaptionMales
ImageIndex TStringGridKrivkaSamciStringGridLeft Top Width� Height� ColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsB]   TButtonMalesLoadButtonLeft� Top� WidthKHeightCaption
Transf�rerTabOrderOnClickFemalesLoadButtonClick   	TTabSheetPocatecniHmotnostTabSheetCaptionPoids initial
ImageIndex TLabelLabel26LeftTopWidth,HeightCaption	Femelles:  TDBEditPocatecniHmotnostSamiceDBEditLeft`TopWidthaHeight	DataFieldFEMALE_WEIGHT00
DataSourceData.NastaveniHejnDataSourceTabOrder   TPanelPocatecniHmotnostSamciPanelLeftTop0Width� Height!
BevelOuterbvNoneTabOrder TLabelLabel27LeftTopWidthHeightCaptionMales:  TDBEditPocatecniHmotnostSamciDBEditLeftXTop WidthaHeight	DataFieldMALE_WEIGHT00
DataSourceData.NastaveniHejnDataSourceTabOrder      	TCheckBoxPohlaviCheckBoxLeftTop0Width� HeightCaptionSexes melangesTabOrderOnClickPohlaviCheckBoxClick  	TCheckBoxKrivkyCheckBoxLeftTopHWidth� HeightCaptionUtilise courbes croiss.TabOrderOnClickKrivkyCheckBoxClick   TButtonButton1Left TopHWidth3HeightCaptionButton1TabOrderVisibleOnClickButton1Click   	TTabSheetStatistikaTabSheetCaptionStatistiques TLabelLabel2LeftTopWidthKHeightCaptionEchelle histogr.:  TLabelLabel3LeftTop0WidthNHeightCaptionEchelle uniform.:  TLabelLabel4Left� Top0WidthHeightCaption�  TLabelLabel7Left� Top0WidthHeightCaption%  TLabelLabel6Left� TopWidthHeightCaption%  TLabelLabel5Left� TopWidthHeightCaption�  TDBEditRozsahUniformityDBEditLeft� Top-Width!Height	DataField	UNI_RANGE
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditRozsahHistogramuDBEditLeft� TopWidth!Height	DataField
HIST_RANGE
DataSourceData.NastaveniVahDataSourceTabOrder    	TTabSheetGsmTabSheetCaptionGSM
ImageIndex TLabelLabel8LeftTop� Width-HeightCaptionNumeros:  TLabelLabel48Left"TopHWidth0HeightCaption
P�riode 1:  TLabelLabel49Left� TopHWidthHeightCaptionJours  TLabelLabel28Left"Top`Width HeightCaptionJour 1:  TLabelLabel30Left"TopxWidth0HeightCaption
P�riode 2:  TLabelLabel31Left� TopxWidthHeightCaptionJours  TLabelLabel29Left� TopHWidthFHeightCaptionHeure d�envoi:  TLabelLabel32LeftOTopHWidthHeightCaption:  TDBCheckBoxDBCheckBox1LeftTopWidthHeightCaptionUtiliser le GSM	DataFieldGSM_USE
DataSourceData.NastaveniVahDataSourceTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2LeftTop0WidthHeightCaptionEnvoi statistiques a minuit	DataFieldGSM_SEND_MIDNIGHT
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox3LeftTop� WidthHeightCaptionEnvoi donnees sur demande	DataFieldGSM_SEND_REQUEST
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox4LeftTop� WidthHeightCaptionVerifier numeros	DataFieldGSM_CHECK_NUMBERS
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit3LeftTop� Width� Height	DataFieldGSM_NUMBER00
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit4LeftTop� Width� Height	DataFieldGSM_NUMBER01
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit5LeftTopWidth� Height	DataFieldGSM_NUMBER02
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit6LeftTop(Width� Height	DataFieldGSM_NUMBER03
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit7LeftTop@Width� Height	DataFieldGSM_NUMBER04
DataSourceData.NastaveniVahDataSourceTabOrder	
OnKeyPressDBEdit3KeyPress  TDBEditPeriod1DBEditLeftXTopEWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT1
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEdit
Day1DBEditLeftXTop]Width!Height	DataFieldGSM_DAY_MIDNIGHT1
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder
  TDBEditPeriod2DBEditLeftXTopuWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT2
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEdit
HourDBEditLeft(TopEWidth!Height	DataFieldGSM_MIDNIGHT_SEND_HOUR
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEdit	MinDBEditLeftXTopEWidth!Height	DataFieldGSM_MIDNIGHT_SEND_MIN
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder   	TTabSheetRs485TabSheetCaptionRS-485
ImageIndex TLabelLabel51LeftTopWidth%HeightCaptionVitesse:  TLabelLabel55Left� TopWidthHeightCaptionBd  TLabelLabel53LeftTop� Width[HeightCaptionTemps de r�ponse:  TLabelLabel54LeftTopWidthEHeightCaptionTemporisation:  TLabelLabel57Left� TopWidthHeightCaptionms  TLabelLabel56Left� Top� WidthHeightCaptionms  TDBRadioGroupRs485ParityDBRadioGroupLeftTop8Width� HeightICaptionParit�Columns	DataFieldRS485_PARITY
DataSourceData.NastaveniVahDataSourceItems.Strings8-n-18-e-18-o-18-n-2 TabOrderValues.Strings0123   TDBEditRs485ReplyDelayDBEditLeft� Top� Width!Height	DataFieldRS485_REPLY_DELAY
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditRs485SilentIntervalDBEditLeft� TopWidth!Height	DataFieldRS485_SILENT_INTERVAL
DataSourceData.NastaveniVahDataSourceTabOrder  TDBComboBoxRs485SpeedDBComboBoxLeft� TopWidthQHeightStylecsDropDownList	DataFieldRS485_SPEED
DataSourceData.NastaveniVahDataSource
ItemHeightItems.Strings1200240048009600192003840057600115200 TabOrder   TDBRadioGroupDBRadioGroup4LeftTop� Width� HeightICaption	Protocole	DataFieldRS485_PROTOCOL
DataSourceData.NastaveniVahDataSourceItems.Strings
MODBUS RTUMODBUS ASCII TabOrderValues.Strings0123    	TTabSheetVahyTabSheetCaptionBalances
ImageIndex TLabelLabel9LeftTopWidth� HeightCaption'Marge au-dessus objectif pour femelles:  TLabelLabel10LeftTopWidthHeightCaption%  TLabelLabel11LeftTop0Width� HeightCaption(Marge en-dessous objectif pour femelles:  TLabelLabel12LeftTop0WidthHeightCaption%  TLabelLabel13LeftTopHWidth� HeightCaption$Marge au-dessus objectif pour males:  TLabelLabel14LeftTopHWidthHeightCaption%  TLabelLabel15LeftTop`Width� HeightCaption%Marge en-dessous objectif pour males:  TLabelLabel16LeftTop`WidthHeightCaption%  TLabelLabel17LeftTop� Width;HeightCaptionStabilisation:  TLabelLabel18Left� Top� WidthHeightCaption�  TLabelLabel19LeftTop� WidthHeightCaption%  TLabelLabel20LeftTop� WidthkHeightCaptionTemps de stabilisation:  TLabelLabel21LeftTopxWidthHeightCaptionFiltre:  TDBEditSamiceOkoliNadDBEditLeft� TopWidth!Height	DataFieldF_MARGIN_ABOVE
DataSourceData.NastaveniVahDataSourceTabOrder   TDBEditSamiceOkoliPodDBEditLeft� Top-Width!Height	DataFieldF_MARGIN_UNDER
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditSamciOkoliNadDBEditLeft� TopEWidth!Height	DataFieldM_MARGIN_ABOVE
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditSamciOkoliPodDBEditLeft� Top]Width!Height	DataFieldM_MARGIN_UNDER
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditUstaleniDBEditLeft� Top� Width!Height	DataFieldSTABILIZATION
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditDobaUstaleniDBEditLeft� Top� Width!Height	DataFieldSTABILIZATION_TIME
DataSourceData.NastaveniVahDataSourceTabOrder  TDBRadioGroupDBRadioGroup1LeftTop(WidthyHeight1CaptionUnitesColumns	DataFieldUNITS
DataSourceData.NastaveniVahDataSourceItems.Stringskglb TabOrder	Values.Strings01   TDBEditFiltrDBEditLeft� TopuWidth!Height	DataFieldFILTER_VALUE
DataSourceData.NastaveniVahDataSourceTabOrder  	TCheckBoxNastaveniGainCheckBoxLeftTop� Width� HeightCaption%Utiliser le calcul automatique du GMQTabOrderOnClickNastaveniGainCheckBoxClick  TDBRadioGroupDBRadioGroup3LeftTop� Width� HeightICaptionSauveg. �chantillon lors de	DataField	JUMP_MODE
DataSourceData.NastaveniVahDataSourceItems.StringsEntr�e sur balanceSortie de balanceDes deux TabOrderValues.Strings012    	TTabSheetCorrectionTabSheetCaption
Correction
ImageIndex TLabelLabel61LeftTop8Width HeightCaptionJour 1:  TLabelLabel64LeftTopPWidth HeightCaptionJour 2:  TLabelLabel62LeftTophWidth3HeightCaptionCorrection:  TLabelLabel63Left~TophWidthHeightCaption+  TLabelLabel65Left� TophWidthHeightCaption%  TDBEditCorrectionCorrectionDBEditLeft� TopeWidth!Height	DataFieldCORRECTION_CORRECTION
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEditCorrectionDay2DBEditLeft� TopMWidth!Height	DataFieldCORRECTION_DAY2
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEditCorrectionDay1DBEditLeft� Top5Width!Height	DataFieldCORRECTION_DAY1
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  	TCheckBoxUseCorrectionCheckBoxLeftTopWidthQHeightCaption Utiliser la courbe de correctionTabOrder    	TTabSheetPodsvitTabSheetCaption	Eclairage
ImageIndex TDBRadioGroupDBRadioGroup2LeftTopWidth1Height9Caption	EclairageColumns	DataField	BACKLIGHT
DataSourceData.NastaveniVahDataSourceItems.StringsArr�t�	En marcheAutomatique TabOrder Values.Strings012     TButtonDownloadButtonLeftTopxWidthyHeightCaption	DechargerTabOrderOnClickDownloadButtonClick  TButtonDeleteButtonLeftTop8WidthyHeightCaptionEffacerTabOrderOnClickDeleteButtonClick   