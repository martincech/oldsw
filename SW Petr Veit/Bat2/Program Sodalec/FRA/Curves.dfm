�
 TCURVESFORM 0`  TPF0TCurvesForm
CurvesFormLeftTop� BorderIconsbiSystemMenu BorderStylebsSingleCaptionCourbes de croissanceClientHeight�ClientWidth Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter	OnDestroyFormDestroyOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxCurvesGroupBoxLeftTopPWidth� HeightqCaptionCourbes disponiblesTabOrder  TListBoxCurvesListBoxLeftTopWidth� Height
ItemHeightTabOrder OnClickCurvesListBoxClick  TButtonNewCurveButtonLeftTop(Width]HeightCaptionNouveauTabOrderOnClickNewCurveButtonClick  TButtonDeleteCurveButtonLeft|TopHWidth]HeightCaptionEffacerTabOrderOnClickDeleteCurveButtonClick  TButtonRenameCurveButtonLeftTopHWidth]HeightCaptionRenommerTabOrderOnClickRenameCurveButtonClick  TButtonCopyCurveButtonLeft|Top(Width]HeightCaptionCopierTabOrderOnClickCopyCurveButtonClick   	TGroupBoxDefinitionGroupBoxLeftTopPWidthHeightQCaption
D�finitionTabOrder TStringGridCurveStringGridLeftTopWidth� HeightColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsUT 
RowHeights   TButtonSaveCurveButtonLeft� Top(Width]HeightCaptionSauvegarderTabOrderOnClickSaveCurveButtonClick   TPanelDecriptionPanelLeft Top Width HeightAAlignalTop
BevelOuterbvNoneEnabledTabOrder TLabelLabel1LeftTopWidthHeight1AutoSizeCaption�Ici vous pouvez inscrire les courbes de croissance th�oriques fournies par le s�lectionneur de votre souche. Ces courbes pourront alors �tre compar�es aux r�sultats obtenus.WordWrap	   TButtonOkButtonLeftgTop�WidthKHeightCaptionOKTabOrderOnClickOkButtonClick  TButtonCancelButtonLeft�Top�WidthKHeightCaptionAnnulerModalResultTabOrder   