object ModbusPortSetupForm: TModbusPortSetupForm
  Left = 392
  Top = 432
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Communication parameters'
  ClientHeight = 224
  ClientWidth = 325
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 16
    Top = 16
    Width = 34
    Height = 13
    Caption = 'Speed:'
  end
  object Label4: TLabel
    Left = 128
    Top = 35
    Width = 13
    Height = 13
    Caption = 'Bd'
  end
  object Label5: TLabel
    Left = 184
    Top = 16
    Width = 58
    Height = 13
    Caption = 'Reply delay:'
  end
  object Label6: TLabel
    Left = 184
    Top = 64
    Width = 66
    Height = 13
    Caption = 'Silent interval:'
  end
  object Label7: TLabel
    Left = 296
    Top = 35
    Width = 13
    Height = 13
    Caption = 'ms'
  end
  object Label8: TLabel
    Left = 296
    Top = 83
    Width = 13
    Height = 13
    Caption = 'ms'
  end
  object Label9: TLabel
    Left = 16
    Top = 64
    Width = 29
    Height = 13
    Caption = 'Parity:'
  end
  object Label10: TLabel
    Left = 16
    Top = 112
    Width = 42
    Height = 13
    Caption = 'Protocol:'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 168
    Width = 293
    Height = 9
    Shape = bsTopLine
  end
  object Label1: TLabel
    Left = 184
    Top = 112
    Width = 103
    Height = 13
    Caption = 'Number of repetitions:'
  end
  object SpeedComboBox: TComboBox
    Left = 16
    Top = 32
    Width = 105
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      '1200'
      '2400'
      '4800'
      '9600'
      '19200'
      '38400'
      '57600'
      '115200')
  end
  object ParityComboBox: TComboBox
    Left = 16
    Top = 80
    Width = 105
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    Items.Strings = (
      '8-n-1'
      '8-e-1'
      '8-o-1'
      '8-n-2')
  end
  object ReplyDelayEdit: TEdit
    Left = 184
    Top = 32
    Width = 105
    Height = 21
    MaxLength = 4
    TabOrder = 3
    Text = 'ReplyDelayEdit'
  end
  object SilentIntervalEdit: TEdit
    Left = 184
    Top = 80
    Width = 105
    Height = 21
    MaxLength = 4
    TabOrder = 4
    Text = 'SilentIntervalEdit'
  end
  object ProtocolComboBox: TComboBox
    Left = 16
    Top = 128
    Width = 105
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    Items.Strings = (
      'MODBUS RTU'
      'MODBUS ASCII')
  end
  object OkButton: TButton
    Left = 136
    Top = 184
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 6
    OnClick = OkButtonClick
  end
  object CancelButton: TButton
    Left = 234
    Top = 184
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 7
  end
  object RepetitionsComboBox: TComboBox
    Left = 184
    Top = 128
    Width = 105
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    Items.Strings = (
      '1'
      '2'
      '3'
      '4'
      '5'
      '6'
      '7'
      '8'
      '9')
  end
end
