object SimulaceStartForm: TSimulaceStartForm
  Left = 227
  Top = 353
  BorderStyle = bsSingle
  Caption = 'Simulation'
  ClientHeight = 362
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 16
    Top = 16
    Width = 289
    Height = 129
    Caption = 'Input data'
    TabOrder = 0
    object Label2: TLabel
      Left = 207
      Top = 96
      Width = 3
      Height = 13
      Caption = '-'
    end
    object OdEdit: TEdit
      Left = 144
      Top = 93
      Width = 57
      Height = 21
      MaxLength = 6
      TabOrder = 4
      OnKeyPress = OdEditKeyPress
    end
    object DoEdit: TEdit
      Left = 215
      Top = 93
      Width = 57
      Height = 21
      MaxLength = 6
      TabOrder = 5
      OnKeyPress = OdEditKeyPress
    end
    object GrafRadioButton: TRadioButton
      Left = 16
      Top = 24
      Width = 257
      Height = 17
      Caption = 'Samples in online chart'
      TabOrder = 0
    end
    object DnesRadioButton: TRadioButton
      Left = 16
      Top = 48
      Width = 257
      Height = 17
      Caption = 'Actual day'
      TabOrder = 1
    end
    object VseRadioButton: TRadioButton
      Left = 16
      Top = 72
      Width = 257
      Height = 17
      Caption = 'All days'
      TabOrder = 2
    end
    object RozsahRadioButton: TRadioButton
      Left = 16
      Top = 96
      Width = 113
      Height = 17
      Caption = 'Range of samples:'
      TabOrder = 3
    end
  end
  object Button1: TButton
    Left = 440
    Top = 320
    Width = 75
    Height = 25
    Caption = 'Start'
    Default = True
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 536
    Top = 320
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object GroupBox2: TGroupBox
    Left = 320
    Top = 16
    Width = 289
    Height = 281
    Caption = 'Scales'
    TabOrder = 3
    object Label30: TLabel
      Left = 16
      Top = 24
      Width = 152
      Height = 13
      Caption = 'Margin above target for females:'
    end
    object Label32: TLabel
      Left = 16
      Top = 48
      Width = 149
      Height = 13
      Caption = 'Margin under target for females:'
    end
    object Label34: TLabel
      Left = 16
      Top = 72
      Width = 143
      Height = 13
      Caption = 'Margin above target for males:'
    end
    object Label36: TLabel
      Left = 16
      Top = 96
      Width = 140
      Height = 13
      Caption = 'Margin under target for males:'
    end
    object Label43: TLabel
      Left = 16
      Top = 120
      Width = 25
      Height = 13
      Caption = 'Filter:'
    end
    object Label38: TLabel
      Left = 16
      Top = 144
      Width = 59
      Height = 13
      Caption = 'Stabilization:'
    end
    object Label41: TLabel
      Left = 16
      Top = 168
      Width = 81
      Height = 13
      Caption = 'Stabilization time:'
    end
    object Label39: TLabel
      Left = 222
      Top = 144
      Width = 6
      Height = 13
      Caption = '�'
    end
    object Label40: TLabel
      Left = 268
      Top = 144
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label37: TLabel
      Left = 268
      Top = 96
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label35: TLabel
      Left = 268
      Top = 72
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label33: TLabel
      Left = 268
      Top = 48
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label31: TLabel
      Left = 268
      Top = 24
      Width = 8
      Height = 13
      Caption = '%'
    end
    object SamiceOkoliNadEdit: TEdit
      Left = 232
      Top = 21
      Width = 33
      Height = 21
      MaxLength = 2
      TabOrder = 0
      Text = 'SamiceOkoliNadEdit'
      OnKeyPress = OdEditKeyPress
    end
    object SamiceOkoliPodEdit: TEdit
      Left = 232
      Top = 45
      Width = 33
      Height = 21
      MaxLength = 2
      TabOrder = 1
      Text = 'SamiceOkoliPodEdit'
      OnKeyPress = OdEditKeyPress
    end
    object SamciOkoliNadEdit: TEdit
      Left = 232
      Top = 69
      Width = 33
      Height = 21
      MaxLength = 2
      TabOrder = 2
      Text = 'SamciOkoliNadEdit'
      OnKeyPress = OdEditKeyPress
    end
    object SamciOkoliPodEdit: TEdit
      Left = 232
      Top = 93
      Width = 33
      Height = 21
      MaxLength = 2
      TabOrder = 3
      Text = 'SamciOkoliPodEdit'
      OnKeyPress = OdEditKeyPress
    end
    object FiltrEdit: TEdit
      Left = 232
      Top = 117
      Width = 33
      Height = 21
      MaxLength = 2
      TabOrder = 4
      Text = 'FiltrEdit'
      OnKeyPress = OdEditKeyPress
    end
    object UstaleniEdit: TEdit
      Left = 232
      Top = 141
      Width = 33
      Height = 21
      MaxLength = 4
      TabOrder = 5
      Text = 'UstaleniEdit'
      OnKeyPress = UstaleniEditKeyPress
    end
    object DobaUstaleniEdit: TEdit
      Left = 232
      Top = 165
      Width = 33
      Height = 21
      MaxLength = 1
      TabOrder = 6
      Text = 'DobaUstaleniEdit'
      OnKeyPress = OdEditKeyPress
    end
    object JumpModeRadioGroup: TRadioGroup
      Left = 16
      Top = 192
      Width = 250
      Height = 73
      Caption = 'Save sample upon'
      ItemIndex = 2
      Items.Strings = (
        'Entering the scale'
        'Leaving the scale'
        'Both')
      TabOrder = 7
    end
  end
  object GroupBox3: TGroupBox
    Left = 16
    Top = 264
    Width = 289
    Height = 81
    Caption = 'Statistics'
    TabOrder = 2
    object Label8: TLabel
      Left = 16
      Top = 24
      Width = 80
      Height = 13
      Caption = 'Histogram range:'
    end
    object Label10: TLabel
      Left = 16
      Top = 48
      Width = 79
      Height = 13
      Caption = 'Uniformity range:'
    end
    object Label12: TLabel
      Left = 222
      Top = 48
      Width = 6
      Height = 13
      Caption = '�'
    end
    object Label28: TLabel
      Left = 222
      Top = 24
      Width = 6
      Height = 13
      Caption = '�'
    end
    object Label14: TLabel
      Left = 268
      Top = 48
      Width = 8
      Height = 13
      Caption = '%'
    end
    object Label16: TLabel
      Left = 268
      Top = 24
      Width = 8
      Height = 13
      Caption = '%'
    end
    object RozsahHistogramuEdit: TEdit
      Left = 232
      Top = 21
      Width = 33
      Height = 21
      MaxLength = 2
      TabOrder = 0
      Text = 'RozsahHistogramuEdit'
      OnKeyPress = OdEditKeyPress
    end
    object RozsahUniformityEdit: TEdit
      Left = 232
      Top = 45
      Width = 33
      Height = 21
      MaxLength = 2
      TabOrder = 1
      Text = 'RozsahUniformityEdit'
      OnKeyPress = OdEditKeyPress
    end
  end
  object GroupBox4: TGroupBox
    Left = 16
    Top = 152
    Width = 289
    Height = 105
    Caption = 'Target'
    TabOrder = 1
    object Label5: TLabel
      Left = 16
      Top = 48
      Width = 122
      Height = 13
      Caption = 'Target weight for females:'
    end
    object Label6: TLabel
      Left = 16
      Top = 72
      Width = 113
      Height = 13
      Caption = 'Target weight for males:'
    end
    object ObePohlaviCheckBox: TCheckBox
      Left = 16
      Top = 24
      Width = 129
      Height = 17
      Caption = 'Use both genders'
      TabOrder = 0
      OnClick = ObePohlaviCheckBoxClick
    end
    object CilovaHmotnostSamiceEdit: TEdit
      Left = 192
      Top = 45
      Width = 73
      Height = 21
      TabOrder = 1
      Text = 'CilovaHmotnostSamiceEdit'
      OnKeyPress = UstaleniEditKeyPress
    end
    object CilovaHmotnostSamciEdit: TEdit
      Left = 192
      Top = 69
      Width = 73
      Height = 21
      TabOrder = 2
      Text = 'CilovaHmotnostSamciEdit'
      OnKeyPress = UstaleniEditKeyPress
    end
  end
end
