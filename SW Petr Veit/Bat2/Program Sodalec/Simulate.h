//---------------------------------------------------------------------------

#ifndef SimulateH
#define SimulateH

#include "c51/Average.h"  // Klouzavy prumer
#include <vector.h>

#pragma pack( push, 1)                  // byte alignment
   #include "Bat2.h"                    // flash data definition
#pragma pack( pop)                      // original alignment

//---------------------------------------------------------------------------


/*// Struktura prumeru
typedef struct {
  TAverageValue Values[AVERAGE_CAPACITY]; // Jednotlive vzorky klouzaveho prumeru
  TAverageCount Length;                   // Aktualni delka klouzaveho prumeru
  TAverageCount Count;                    // Pocet prvku aktualne ulozenych ve Values[]
} TAverage;*/

// Popisovac vahy :
typedef struct {
   long       Suma;                     // Suma prevodu pro prumerovani
   byte       Pocet;                    // Pocet vzroku v sume
   byte       ZahoditVzorek;            // Flag, ze se ma nasledujici nacteny vzorek zahodit

   long       Hmotnost;                 // Aktualni okamzita hmotnost na vaze
   byte       NovaHmotnost;             // Flag, ze se nacetla nova aktualni hmotnost
   long       PosledniUlozenaHmotnost;  // Posledni ulozena hmotnost
   byte       PosledniUlozenePohlavi;   // Pohlavi u polsedne ulozene hmotnosti

   // Detekce nastupu
   TAverage   Average;                  // Klouzavy prumer
   long       PosledniUstalenaHmotnost;
   byte       Ustaleni;                 // Flag, ze je vaha ustalena

   // Pauza vazeni
   byte       CasovaPauza;              // Flag, zda je prave zapauzovane vazeni kvuli casovemu omezeni
   byte       Pauza;                    // Flag, zda je prave zapauzovane vazeni (casove nebo rucne)
} TNVaha;

typedef enum {
  DETECTION_OLD,                        // Puvodni algoritmus
  DETECTION_NEW                         // Novy algoritmus od Ivose
} TDetectionAlgorithm;


// Data simulace
typedef struct {

  // Zadane veliciny
  bool               RozdelovatPohlavi;
  unsigned short int NormovanaHmotnostSamice, NormovanaHmotnostSamci;  // Normovana hmotnost nactena z rustove krivky pro samice a samce. Pokud se pohlavi nerozdeluje, bere se pro samice.

  // Nastaveni vah
  byte SamiceOkoliNad;                 // Okoli nad prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamiceOkoliPod;                 // Okoli pod prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamciOkoliNad;                  // Okoli nad prumerem pro samce v procentech
  byte SamciOkoliPod;                  // Okoli pod prumerem pro samce v procentech
  byte Filtr;                          // Filtr prevodniku
  byte UstaleniNaslapneVahy;           // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
  byte DelkaUstaleniNaslapneVahy;      // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
  byte RozsahHistogramu;               // Rozsah histogramu v +- % stredni hodnoty
  byte RozsahUniformity;               // Rozsah uniformity v +- %

  // Pomocne veliciny
  unsigned int HorniMezSamice, DolniMezSamice;                   // Dolni a horni mez hmotnosti z rustove krivky samic, pripadne pro obe pohlavi, pokud se rozdeleni na samce a samice nepouziva
  unsigned int HorniMezSamci, DolniMezSamci;                     // Dolni a horni mez hmotnosti z rustove krivky samcu

  TNVaha NVaha;

  TStatistic StatSamice;              // statistika / statistika samic
  THistogram HistSamice;              // histogram  / histogram  samic
  TStatistic StatSamci;               // statistika / statistika samcu
  THistogram HistSamci;               // histogram  / histogram  samcu

  TJumpMode JumpMode;

  // Novy detekcni algoritmus
  TDetectionAlgorithm DetectionAlgorithm;       // Pouzity algoritmus
  bool UsePrefilter;                  // Pouziti predfiltrace vzorku u stareho prevodniku
  TCalibration Calibration;           // Kalibrace vah

  vector<int> Samples[_GENDER_COUNT]; // Zvazene vzorky samic a samcu

} TSimulate;
extern TSimulate Simulate;



void SimulateInit();
  // Inicializace

void SimulateStep(double Weight);
  // 1 krok simulace s novou hmotnosti <Weight>


#endif
