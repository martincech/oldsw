//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusStartWeighing.h"
#include "DM.h"

#include "Konstanty.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TModbusStartWeighingForm *ModbusStartWeighingForm;



//---------------------------------------------------------------------------
// Nastaveni seznamu hejn
//---------------------------------------------------------------------------

void TModbusStartWeighingForm::SetFlock(TFlock *NewFlock) {
  Flock = NewFlock;
}

//---------------------------------------------------------------------------
// Nastaveni datove struktury
//---------------------------------------------------------------------------

void TModbusStartWeighingForm::SetStartParameters(TModbusStartParameters *NewModbusStartParameters) {
  ModbusStartParameters = NewModbusStartParameters;
}

//---------------------------------------------------------------------------
// Kontrola zadanych parametru
//---------------------------------------------------------------------------

void TModbusStartWeighingForm::CheckEditInt(TEdit *Edit, int Min, int Max) {
  // Kontrola hodnoty v editu <Edit>. V pripade chyby hodi vyjimku.
  int Value;

  try {
    Value = Edit->Text.ToInt();
    if (Value < Min || Value > Max) {
      throw 1;
    }
  } catch(...) {
    Edit->SetFocus();
    throw;
  }
}

void TModbusStartWeighingForm::CheckEditFloat(TEdit *Edit, double Min, double Max) {
  // Kontrola hodnoty v editu <Edit>. V pripade chyby hodi vyjimku.
  double Value;

  try {
    Value = Edit->Text.ToDouble();
    if (Value < Min || Value > Max) {
      throw 1;
    }
  } catch(...) {
    Edit->SetFocus();
    throw;
  }
}

bool TModbusStartWeighingForm::CheckValues() {
  // Kontrola zadanych hodnot
  try {

    // Hmotnost
    if (UseDirectRadioButton->Checked) {
      // Zadal parametry primo
      CheckEditFloat(TargetFemalesEdit, PrepoctiNaKg(MIN_TARGET_WEIGHT), PrepoctiNaKg(MAX_TARGET_WEIGHT));
      if (UseBotheGendersCheckBox->Checked) {
        CheckEditFloat(TargetMalesEdit, PrepoctiNaKg(MIN_TARGET_WEIGHT), PrepoctiNaKg(MAX_TARGET_WEIGHT));
      }
    } else {
      // Pouziva hejno
      if (FlockComboBox->ItemIndex < 0) {
        FlockComboBox->SetFocus();
        throw 1;
      }
    }

    // Start vazeni
    CheckEditInt(StartingDayEdit, CURVE_FIRST_DAY_NUMBER, CURVE_MAX_DAY);

    // Porovnani
    if (CompareCheckBox->Checked) {
      if (CompareComboBox->ItemIndex < 0) {
        CompareComboBox->SetFocus();
        throw 1;
      }
      // Hejno vybrane k porovnani nesmi byt stejne, jako hejno, podle ktereho se vazi
      if (UseCurveRadioButton->Checked && GetFlockNumber(FlockComboBox) == GetFlockNumber(CompareComboBox)) {
        CompareComboBox->SetFocus();
        throw 1;
      }
    }

  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(), LoadStr(CHYBA).c_str(), MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Zjisteni cisla hejna z Comboboxu
//---------------------------------------------------------------------------

int TModbusStartWeighingForm::GetFlockNumber(TComboBox *Box) {
  // Zjisteni cisla hejna z Comboboxu <Box>
  return Box->Text.SubString(1, 1).ToInt();
}

//---------------------------------------------------------------------------
__fastcall TModbusStartWeighingForm::TModbusStartWeighingForm(TComponent* Owner)
        : TForm(Owner)
{
  Flock                 = NULL;
  ModbusStartParameters = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TModbusStartWeighingForm::FormShow(TObject *Sender)
{
  unsigned short Hour, Min, Sec, Msec;
  char FlockName[FLOCK_NAME_MAX_LENGTH + 1];
  AnsiString Str;

  UseDirectRadioButton->Checked = true;
  UseBotheGendersCheckBox->Checked = false;
  TargetFemalesEdit->Text = "";
  TargetMalesEdit->Text   = "";
  FlockComboBox->ItemIndex = -1;
  UseDirectRadioButtonClick(NULL);

  StandardWeighingRadioButton->Checked = true;

  StartingDayEdit->Text = "";
  StartNowRadioButton->Checked = true;
  StartDatePicker->Date = Date();
  Time().DecodeTime(&Hour, &Min, &Sec, &Msec);
  StartTimePicker->Time = TDateTime(Hour, Min, 0, 0);      // Sekundy nuluju
  StartNowRadioButtonClick(NULL);

  CompareCheckBox->Checked = false;
  CompareComboBox->ItemIndex = -1;
  CompareCheckBoxClick(NULL);

  // Vyplnim hejna
  if (!Flock) {
    return;
  }

  FlockComboBox->Clear();
  CompareComboBox->Clear();
  for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
    if (Flock[i].Header.Number == FLOCK_EMPTY_NUMBER) {
      continue;               // Hejno neni definovane
    }
    for (int j = 0; j < FLOCK_NAME_MAX_LENGTH; j++) {
      FlockName[j] = Flock[i].Header.Title[j];
    }
    FlockName[FLOCK_NAME_MAX_LENGTH] = 0;         // Zakoncim
    Str = AnsiString(i) + ": " + AnsiString(FlockName);
    // Seznam vsech hejn
    ModbusStartWeighingForm->FlockComboBox->Items->Add(Str);
    // K porovnani dam jen hejna s krivkou
    if (Flock[i].Header.UseCurves) {
      ModbusStartWeighingForm->CompareComboBox->Items->Add(Str);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TModbusStartWeighingForm::UseDirectRadioButtonClick(
      TObject *Sender)
{
  bool UseCurve = UseCurveRadioButton->Checked;

  UseBotheGendersCheckBox->Enabled = !UseCurve;
  TargetFemalesEdit->Enabled       = !UseCurve;
  TargetMalesEdit->Enabled         = !UseCurve && UseBotheGendersCheckBox->Checked;

  FlockComboBox->Enabled           = UseCurve;
}
//---------------------------------------------------------------------------

void __fastcall TModbusStartWeighingForm::UseBotheGendersCheckBoxClick(
      TObject *Sender)
{
  TargetMalesEdit->Enabled = UseBotheGendersCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TModbusStartWeighingForm::StartNowRadioButtonClick(
      TObject *Sender)
{
  bool StartNow = StartNowRadioButton->Checked;

  StartDatePicker->Enabled = !StartNow;
  StartTimePicker->Enabled = !StartNow;
}
//---------------------------------------------------------------------------

void __fastcall TModbusStartWeighingForm::CompareCheckBoxClick(TObject *Sender)
{
  CompareComboBox->Enabled = CompareCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall TModbusStartWeighingForm::Button1Click(TObject *Sender)
{
  unsigned short Day, Month, Year;
  unsigned short Hour, Min, Sec, Msec;

  // Kontrola zadanych hodnot
  if (!CheckValues()) {
    return;
  }

  // Vyplnim strukutru - pozor, vsechny nenastavovane polozky musim nulovat, aby tam nebyly blbosti (vaha by vratila chybu rozsahu)
  if (!ModbusStartParameters) {
    return;
  }

  // Hmotnost
  ModbusStartParameters->QuickWeighing.UseBothGenders               = 0;
  ModbusStartParameters->QuickWeighing.InitialWeight[GENDER_FEMALE] = MIN_TARGET_WEIGHT;
  ModbusStartParameters->QuickWeighing.InitialWeight[GENDER_MALE]   = MIN_TARGET_WEIGHT;
  if (UseDirectRadioButton->Checked) {
    // Zadal parametry primo
    ModbusStartParameters->FlockNumber = FLOCK_EMPTY_NUMBER;
    ModbusStartParameters->QuickWeighing.InitialWeight[GENDER_FEMALE] = PrepoctiZKg(TargetFemalesEdit->Text.ToDouble());
    ModbusStartParameters->QuickWeighing.UseBothGenders               = UseBotheGendersCheckBox->Checked;
    if (UseBotheGendersCheckBox->Checked) {
      ModbusStartParameters->QuickWeighing.InitialWeight[GENDER_MALE] = PrepoctiZKg(TargetMalesEdit->Text.ToDouble());
    }
  } else {
    // Pouziva hejno
    ModbusStartParameters->FlockNumber = GetFlockNumber(FlockComboBox);
  }

  // Online
  ModbusStartParameters->Online = OnlineWeighingRadioButton->Checked;

  // Start vazeni
  ModbusStartParameters->InitialDayNumber = StartingDayEdit->Text.ToInt();
  ModbusStartParameters->WaitingForStart  = StartLaterRadioButton->Checked;
  if (StartLaterRadioButton->Checked) {
    StartDatePicker->DateTime.DecodeDate(&Year, &Month, &Day);
    StartTimePicker->DateTime.DecodeTime(&Hour, &Min, &Sec, &Msec);
    ModbusStartParameters->DateTime.Day   = Day;
    ModbusStartParameters->DateTime.Month = Month;
    ModbusStartParameters->DateTime.Year  = Year;
    ModbusStartParameters->DateTime.Hour  = Hour;
    ModbusStartParameters->DateTime.Min   = Min;
  } else {
    ModbusStartParameters->DateTime.Day   = 1;
    ModbusStartParameters->DateTime.Month = 1;
    ModbusStartParameters->DateTime.Year  = 2000;
    ModbusStartParameters->DateTime.Hour  = 1;
    ModbusStartParameters->DateTime.Min   = 1;
  }

  // Porovnani
  if (CompareCheckBox->Checked) {
    ModbusStartParameters->ComparisonFlock = GetFlockNumber(CompareComboBox);
  } else {
    ModbusStartParameters->ComparisonFlock = FLOCK_EMPTY_NUMBER;
  }

  ModalResult = mrOk;
}
//---------------------------------------------------------------------------

