object WeighingOverwriteForm: TWeighingOverwriteForm
  Left = 243
  Top = 217
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Overwrite'
  ClientHeight = 320
  ClientWidth = 482
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 174
    Height = 13
    Caption = 'This record already exists, overwrite?'
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 30
    Height = 13
    Caption = 'Scale:'
  end
  object Label3: TLabel
    Left = 16
    Top = 64
    Width = 22
    Height = 13
    Caption = 'Day:'
  end
  object Label4: TLabel
    Left = 16
    Top = 80
    Width = 26
    Height = 13
    Caption = 'Date:'
  end
  object ScaleLabel: TLabel
    Left = 72
    Top = 48
    Width = 53
    Height = 13
    Caption = 'ScaleLabel'
  end
  object DayLabel: TLabel
    Left = 72
    Top = 64
    Width = 45
    Height = 13
    Caption = 'DayLabel'
  end
  object DateLabel: TLabel
    Left = 72
    Top = 80
    Width = 49
    Height = 13
    Caption = 'DateLabel'
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 112
    Width = 217
    Height = 193
    Caption = 'Old record'
    TabOrder = 0
    object Label5: TLabel
      Left = 16
      Top = 40
      Width = 31
      Height = 13
      Caption = 'Count:'
    end
    object Label6: TLabel
      Left = 16
      Top = 56
      Width = 43
      Height = 13
      Caption = 'Average:'
    end
    object Label7: TLabel
      Left = 16
      Top = 72
      Width = 25
      Height = 13
      Caption = 'Gain:'
    end
    object Label8: TLabel
      Left = 16
      Top = 88
      Width = 62
      Height = 13
      Caption = 'St. deviation:'
    end
    object Label9: TLabel
      Left = 16
      Top = 104
      Width = 16
      Height = 13
      Caption = 'Cv:'
    end
    object Label10: TLabel
      Left = 16
      Top = 120
      Width = 19
      Height = 13
      Caption = 'Uni:'
    end
    object Label11: TLabel
      Left = 16
      Top = 152
      Width = 41
      Height = 13
      Caption = 'Inserted:'
    end
    object Label12: TLabel
      Left = 16
      Top = 168
      Width = 65
      Height = 13
      Caption = 'GSM number:'
    end
    object OldFemaleCountLabel: TLabel
      Left = 116
      Top = 40
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '9999'
    end
    object OldFemaleAverageLabel: TLabel
      Left = 116
      Top = 56
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object OldFemaleGainLabel: TLabel
      Left = 116
      Top = 72
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object OldFemaleSigmaLabel: TLabel
      Left = 116
      Top = 88
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0,000'
    end
    object OldFemaleCvLabel: TLabel
      Left = 116
      Top = 104
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object OldFemaleUniLabel: TLabel
      Left = 116
      Top = 120
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object InsertedLabel: TLabel
      Left = 96
      Top = 152
      Width = 64
      Height = 13
      Caption = 'InsertedLabel'
    end
    object GSMLabel: TLabel
      Left = 96
      Top = 168
      Width = 50
      Height = 13
      Caption = 'GSMLabel'
    end
    object OldMaleCountLabel: TLabel
      Left = 164
      Top = 40
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '9999'
    end
    object OldMaleAverageLabel: TLabel
      Left = 164
      Top = 56
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object OldMaleGainLabel: TLabel
      Left = 164
      Top = 72
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object OldMaleSigmaLabel: TLabel
      Left = 164
      Top = 88
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0,000'
    end
    object OldMaleCvLabel: TLabel
      Left = 164
      Top = 104
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object OldMaleUniLabel: TLabel
      Left = 164
      Top = 120
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object SpeedButton1: TSpeedButton
      Left = 119
      Top = 19
      Width = 13
      Height = 16
      Enabled = False
      Flat = True
      Glyph.Data = {
        1E010000424D1E010000000000007600000028000000120000000E0000000100
        040000000000A800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808888888
        8088880000008888088888888088880000008800000888800000880000008888
        0888888880888800000088880888888880888800000088800088888800088800
        0000870888078870888078000000808888808808888808000000088888880088
        8888800000000888888800888888800000000888888800888888800000008088
        8880880888880800000087088807887088807800000088800088888800088800
        0000}
      NumGlyphs = 2
    end
    object SpeedButton2: TSpeedButton
      Left = 179
      Top = 19
      Width = 15
      Height = 15
      Enabled = False
      Flat = True
      Glyph.Data = {
        FA000000424DFA000000000000007600000028000000160000000B0000000100
        0400000000008400000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888000888888
        8800088888008708880788887088807888008088888088880888880888000888
        8888088088888880880008888888088088888880880008888888088088888880
        8800808888808888088888088800870888008808708880088000888000880808
        880008808000888888888008888888880000888888800008888888000000}
      NumGlyphs = 2
    end
    object Shape1: TShape
      Left = 10
      Top = 35
      Width = 196
      Height = 1
      Pen.Color = clGray
    end
    object Shape2: TShape
      Left = 112
      Top = 20
      Width = 1
      Height = 117
      Pen.Color = clGray
    end
    object Shape3: TShape
      Left = 160
      Top = 20
      Width = 1
      Height = 117
      Pen.Color = clGray
    end
    object SpeedButton10: TSpeedButton
      Left = 133
      Top = 19
      Width = 23
      Height = 15
      Enabled = False
      Flat = True
      Glyph.Data = {
        66010000424D660100000000000076000000280000002C0000000A0000000100
        040000000000F000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00878888888888
        8888888878878888888888888888887800007888878888888888888887788887
        8888888888888887000078887778888888777888877888777888888877788887
        0000788887888888878887888778888788888887888788870000788877788878
        8788878887788877788878878887888700007887888787778788878887788788
        8787778788878887000078878887887888777778877887888788788877777887
        0000788788878888888887788778878887888888888778870000788877788888
        8888888887788877788888888888888700008788888888888888888878878888
        88888888888888780000}
      NumGlyphs = 2
    end
  end
  object Button1: TButton
    Left = 296
    Top = 280
    Width = 75
    Height = 25
    Caption = 'Overwrite'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object Button2: TButton
    Left = 390
    Top = 280
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object GroupBox2: TGroupBox
    Left = 248
    Top = 112
    Width = 217
    Height = 153
    Caption = 'New record'
    TabOrder = 3
    object Label19: TLabel
      Left = 16
      Top = 40
      Width = 31
      Height = 13
      Caption = 'Count:'
    end
    object Label20: TLabel
      Left = 16
      Top = 56
      Width = 43
      Height = 13
      Caption = 'Average:'
    end
    object Label21: TLabel
      Left = 16
      Top = 72
      Width = 25
      Height = 13
      Caption = 'Gain:'
    end
    object Label22: TLabel
      Left = 16
      Top = 88
      Width = 62
      Height = 13
      Caption = 'St. deviation:'
    end
    object Label23: TLabel
      Left = 16
      Top = 104
      Width = 16
      Height = 13
      Caption = 'Cv:'
    end
    object Label24: TLabel
      Left = 16
      Top = 120
      Width = 19
      Height = 13
      Caption = 'Uni:'
    end
    object NewFemaleCountLabel: TLabel
      Left = 116
      Top = 40
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '9999'
    end
    object NewFemaleAverageLabel: TLabel
      Left = 116
      Top = 56
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object NewFemaleGainLabel: TLabel
      Left = 116
      Top = 72
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object NewFemaleSigmaLabel: TLabel
      Left = 116
      Top = 88
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0,000'
    end
    object NewFemaleCvLabel: TLabel
      Left = 116
      Top = 104
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object NewFemaleUniLabel: TLabel
      Left = 116
      Top = 120
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object NewMaleCountLabel: TLabel
      Left = 164
      Top = 40
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '9999'
    end
    object NewMaleAverageLabel: TLabel
      Left = 164
      Top = 56
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object NewMaleGainLabel: TLabel
      Left = 164
      Top = 72
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99,999'
    end
    object NewMaleSigmaLabel: TLabel
      Left = 164
      Top = 88
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0,000'
    end
    object NewMaleCvLabel: TLabel
      Left = 164
      Top = 104
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object NewMaleUniLabel: TLabel
      Left = 164
      Top = 120
      Width = 35
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '100'
    end
    object SpeedButton4: TSpeedButton
      Left = 179
      Top = 19
      Width = 15
      Height = 15
      Enabled = False
      Flat = True
      Glyph.Data = {
        FA000000424DFA000000000000007600000028000000160000000B0000000100
        0400000000008400000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888000888888
        8800088888008708880788887088807888008088888088880888880888000888
        8888088088888880880008888888088088888880880008888888088088888880
        8800808888808888088888088800870888008808708880088000888000880808
        880008808000888888888008888888880000888888800008888888000000}
      NumGlyphs = 2
    end
    object Shape4: TShape
      Left = 10
      Top = 35
      Width = 196
      Height = 1
      Pen.Color = clGray
    end
    object Shape5: TShape
      Left = 112
      Top = 20
      Width = 1
      Height = 117
      Pen.Color = clGray
    end
    object Shape6: TShape
      Left = 160
      Top = 20
      Width = 1
      Height = 117
      Pen.Color = clGray
    end
    object SpeedButton3: TSpeedButton
      Left = 119
      Top = 19
      Width = 13
      Height = 16
      Enabled = False
      Flat = True
      Glyph.Data = {
        1E010000424D1E010000000000007600000028000000120000000E0000000100
        040000000000A800000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808888888
        8088880000008888088888888088880000008800000888800000880000008888
        0888888880888800000088880888888880888800000088800088888800088800
        0000870888078870888078000000808888808808888808000000088888880088
        8888800000000888888800888888800000000888888800888888800000008088
        8880880888880800000087088807887088807800000088800088888800088800
        0000}
      NumGlyphs = 2
    end
    object SpeedButton5: TSpeedButton
      Left = 133
      Top = 19
      Width = 23
      Height = 15
      Enabled = False
      Flat = True
      Glyph.Data = {
        66010000424D660100000000000076000000280000002C0000000A0000000100
        040000000000F000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00878888888888
        8888888878878888888888888888887800007888878888888888888887788887
        8888888888888887000078887778888888777888877888777888888877788887
        0000788887888888878887888778888788888887888788870000788877788878
        8788878887788877788878878887888700007887888787778788878887788788
        8787778788878887000078878887887888777778877887888788788877777887
        0000788788878888888887788778878887888888888778870000788877788888
        8888888887788877788888888888888700008788888888888888888878878888
        88888888888888780000}
      NumGlyphs = 2
    end
  end
end
