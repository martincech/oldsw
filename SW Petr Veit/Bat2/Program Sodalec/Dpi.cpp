//***************************************************************************
//
//    Dpi.cpp - Correction of DPI in Windows
//    Version 1.0
//
//***************************************************************************

#include "Dpi.h"

//-----------------------------------------------------------------------------
// Definice
//-----------------------------------------------------------------------------

#define ORIGINAL_DPI    96              // Standardni DPI ve Windows

//-----------------------------------------------------------------------------
// Lokalni promenne
//-----------------------------------------------------------------------------

static int Dpi = ORIGINAL_DPI;          // Aktualne nastavene DPI

//---------------------------------------------------------------------------
// Nastaveni DPI
//---------------------------------------------------------------------------

void DpiSet(int NewDpi) {
  // Nastavi DPI na <NewDpi>
  Dpi = NewDpi;
}

//---------------------------------------------------------------------------
// Korekce StringListu
//---------------------------------------------------------------------------

void DpiSetStringGrid(TStringGrid *StringGrid) {
  // Nastavi vysku radku ve <StringGrid> podle nastaveneho DPI, volat v kontruktoru okna
  if (Dpi == ORIGINAL_DPI) {
    // Zatim nenastavil, nic nemenim
    return;
  }
  StringGrid->DefaultRowHeight = StringGrid->DefaultRowHeight * Dpi / ORIGINAL_DPI;
}

