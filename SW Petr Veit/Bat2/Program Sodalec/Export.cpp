//***************************************************************************
//
//   Export.cpp        Export to Excel
//   Version 1.0
//
//***************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "Export.h"
#include "Konstanty.h"
#include "Hlavni.h"
#include "DM.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// Export reportu
//---------------------------------------------------------------------------

#define EXCEL_REPORT_RADEK1  1          // Na kterem radku zacinaji zaznamy

void ExportReport(TQuery *Query, bool BothGenders) {
  // Exportuje report v <Query> do Excelu
  int PocetZaznamu;
  String Str;

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Vyplnim hlavicku
/*  Data->Excel->PutStr(1, 1, HlavniForm->ReportTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, Data->Soubor.Jmeno);
  if (BothGenders) {
    Data->Excel->PutStr(1, 3, LoadStr(POHLAVI));
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      Str = LoadStr(SAMICE);
    } else {
      Str = LoadStr(SAMEC);
    }
    Data->Excel->PutStr(2, 3, Str);
  }*/

  // Hlavicky sloupcu
  int Column = 1;
  for (int i = 0; i < HlavniForm->ReportDBGrid->Columns->Count; i++) {
    if (!HlavniForm->ReportDBGrid->Columns->Items[i]->Visible) {
      continue;                 // Sloupec je schovany
    }
    Data->Excel->PutStr(Column++, EXCEL_REPORT_RADEK1, HlavniForm->ReportDBGrid->Columns->Items[i]->Title->Caption);
  }

  // Vyplnim hodnoty
  Query->First();
  PocetZaznamu = 0;
  while (!Query->Eof) {
    Data->Excel->PutInt(   1, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("DAY_NUMBER")->AsInteger);
    Data->Excel->PutStr(   2, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("DATE_TIME_DATE")->AsDateTime.DateString());
    Data->Excel->PutInt(   3, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("STAT_COUNT")->AsInteger);
    Data->Excel->PutFloat( 4, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("AVERAGE_WEIGHT")->AsFloat);
    Data->Excel->PutFloat( 5, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("GAIN")->AsFloat);
    Data->Excel->PutFloat( 6, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("SIGMA")->AsFloat);
    Data->Excel->PutFloat( 7, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("CV")->AsFloat);
    Data->Excel->PutInt(   8, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("UNI")->AsInteger);
    if (Data->GetCompareCurve(COMPARE_CURVE_MAIN_FORM)) {
      // Prave se porovnava
      Data->Excel->PutFloat( 9, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("COMPARE")->AsFloat);
      Data->Excel->PutFloat(10, EXCEL_REPORT_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("DIFFERENCE")->AsFloat);
    }
    Query->Next();
    PocetZaznamu++;
  }//while
}

//---------------------------------------------------------------------------
// Export vzorku
//---------------------------------------------------------------------------

#define EXCEL_VZORKY_RADEK1  4          // Na kterem radku zacinaji zaznamy

void ExportSamples(TQuery *Query) {
  // Exportuje vzorky v <Query> do Excelu
  int PocetZaznamu;

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Vyplnim hlavicku
  Data->Excel->PutStr(1, 1, HlavniForm->VazeniTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, Data->Soubor.Jmeno);
  Data->Excel->PutStr(1, EXCEL_VZORKY_RADEK1, LoadStr(DEN));
  Data->Excel->PutStr(2, EXCEL_VZORKY_RADEK1, LoadStr(HODINA));
  Data->Excel->PutStr(3, EXCEL_VZORKY_RADEK1, LoadStr(HMOTNOST) + " [" + Data->Soubor.Jednotky + "]");
  Data->Excel->PutStr(4, EXCEL_VZORKY_RADEK1, LoadStr(POHLAVI));

  // Vyplnim hodnoty
  Query->First();
  PocetZaznamu = 0;
  while (!Query->Eof) {
    Data->Excel->PutInt(1, EXCEL_VZORKY_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("DAY_NUMBER")->AsInteger);
    Data->Excel->PutInt(2, EXCEL_VZORKY_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("TIME_HOUR")->AsInteger);
    Data->Excel->PutFloat(3, EXCEL_VZORKY_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("WEIGHT")->AsFloat);
    if (Query->FieldByName("GENDER")->AsBoolean == GENDER_FEMALE) {
      Data->Excel->PutStr(4, EXCEL_VZORKY_RADEK1 + 1 + PocetZaznamu, LoadStr(SAMICE));
    } else {
      Data->Excel->PutStr(4, EXCEL_VZORKY_RADEK1 + 1 + PocetZaznamu, LoadStr(SAMEC));
    }
    Query->Next();
    PocetZaznamu++;
  }//while
}

//---------------------------------------------------------------------------
// Export histogramu
//---------------------------------------------------------------------------

#define EXCEL_HISTOGRAM_RADEK1  7          // Na kterem radku zacinaji zaznamy

void ExportHistogram(TChart *Chart, bool BothGenders) {
  // Exportuje histogram v <Chart> do Excelu
  int PocetZaznamu;
  String Str;

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Vyplnim hlavicku
  Data->Excel->PutStr(1, 1, HlavniForm->HistogramTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, Data->Soubor.Jmeno);
  Data->Excel->PutStr(1, 3, LoadStr(DEN));
  Data->Excel->PutInt(2, 3, Data->Soubor.Den);
  Data->Excel->PutStr(1, 4, LoadStr(DATUM));
  Data->Excel->PutStr(2, 4, HlavniForm->DatumLabel->Caption);
  if (BothGenders) {
    Data->Excel->PutStr(1, 5, LoadStr(POHLAVI));
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      Str = LoadStr(SAMICE);
    } else {
      Str = LoadStr(SAMEC);
    }
    Data->Excel->PutStr(2, 5, Str);
  }//if
  Data->Excel->PutStr(1, EXCEL_HISTOGRAM_RADEK1, LoadStr(HMOTNOST) + " [" + Data->Soubor.Jednotky + "]");
  Data->Excel->PutStr(2, EXCEL_HISTOGRAM_RADEK1, LoadStr(POCET));
  // Vyplnim hodnoty
  for (int Index = 0; Index < Chart->Series[0]->Count(); Index++) {
    Data->Excel->PutFloat(1, EXCEL_HISTOGRAM_RADEK1 + 1 + Index, Chart->Series[0]->XValue[Index]);
    Data->Excel->PutFloat(2, EXCEL_HISTOGRAM_RADEK1 + 1 + Index, Chart->Series[0]->YValue[Index]);
  }
}

//---------------------------------------------------------------------------
// Export denni aktivity
//---------------------------------------------------------------------------

#define EXCEL_AKTIVITA_DEN_RADEK1  7          // Na kterem radku zacinaji zaznamy

void ExportDayActivity(TChart *Chart, bool BothGenders) {
  // Exportuje aktivitu v <Chart> do Excelu
  int PocetZaznamu;
  String Str;

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Vyplnim hlavicku
  Data->Excel->PutStr(1, 1, HlavniForm->AktivitaDenTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, Data->Soubor.Jmeno);
  Data->Excel->PutStr(1, 3, LoadStr(DEN));
  Data->Excel->PutInt(2, 3, Data->Soubor.Den);
  Data->Excel->PutStr(1, 4, LoadStr(DATUM));
  Data->Excel->PutStr(2, 4, HlavniForm->DatumLabel->Caption);
  if (BothGenders) {
    Data->Excel->PutStr(1, 5, LoadStr(POHLAVI));
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      Str = LoadStr(SAMICE);
    } else {
      Str = LoadStr(SAMEC);
    }
    Data->Excel->PutStr(2, 5, Str);
  }
  Data->Excel->PutStr(1, EXCEL_AKTIVITA_DEN_RADEK1, LoadStr(HODINA));
  Data->Excel->PutStr(2, EXCEL_AKTIVITA_DEN_RADEK1, LoadStr(POCET));
  // Vyplnim hodnoty
  for (int Index = 0; Index < Chart->Series[0]->Count(); Index++) {
    Data->Excel->PutFloat(1, EXCEL_AKTIVITA_DEN_RADEK1 + 1 + Index, Chart->Series[0]->XValue[Index]);
    Data->Excel->PutFloat(2, EXCEL_AKTIVITA_DEN_RADEK1 + 1 + Index, Chart->Series[0]->YValue[Index]);
  }
}

//---------------------------------------------------------------------------
// Export celkove aktivity
//---------------------------------------------------------------------------

#define EXCEL_AKTIVITA_CELKEM_RADEK1  5          // Na kterem radku zacinaji zaznamy

void ExportTotalActivity(TChart *Chart, bool BothGenders) {
  // Exportuje celkovou aktivitu v <Chart> do Excelu
  int PocetZaznamu;
  String Str;

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Vyplnim hlavicku
  Data->Excel->PutStr(1, 1, HlavniForm->AktivitaCelkemTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, Data->Soubor.Jmeno);
  Data->Excel->PutStr(1, EXCEL_AKTIVITA_CELKEM_RADEK1, LoadStr(DEN));
  Data->Excel->PutStr(2, EXCEL_AKTIVITA_CELKEM_RADEK1, LoadStr(POCET));
  if (BothGenders) {
    Data->Excel->PutStr(1, 3, LoadStr(POHLAVI));
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      Str = LoadStr(SAMICE);
    } else {
      Str = LoadStr(SAMEC);
    }
    Data->Excel->PutStr(2, 3, Str);
  }
  // Vyplnim hodnoty
  for (int Index = 0; Index < Chart->Series[0]->Count(); Index++) {
    Data->Excel->PutFloat(1, EXCEL_AKTIVITA_CELKEM_RADEK1 + 1 + Index, Chart->Series[0]->XValue[Index]);
    Data->Excel->PutFloat(2, EXCEL_AKTIVITA_CELKEM_RADEK1 + 1 + Index, Chart->Series[0]->YValue[Index]);
  }
}

#define EXCEL_KRIVKA_RADEK1  5          // Na kterem radku zacinaji zaznamy

//---------------------------------------------------------------------------
// Export rustove krivky
//---------------------------------------------------------------------------

void ExportGrowthCurve(TChart *Chart, bool BothGenders, TGrowthCurve *TheoreticalCurve) {
  // Exportuje rustovou krivku v <Chart> do Excelu
  // Pokud neni <TheoreticalCurve> NULL, exportuje i teoretickou rustovou krivku
  String Str;
  int PocetZaznamu;
  int Day;
  double RealWeight, TheoreticalWeight;
  bool UseStatistics;
  int StatisticsColumn;

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Vyplnim hlavicku
  Data->Excel->PutStr(1, 1, HlavniForm->KrivkaTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, Data->Soubor.Jmeno);
  if (BothGenders) {
    Data->Excel->PutStr(1, 3, LoadStr(POHLAVI));
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      Str = LoadStr(SAMICE);
    } else {
      Str = LoadStr(SAMEC);
    }
    Data->Excel->PutStr(2, 3, Str);
  }
  UseStatistics = Chart->Series[5]->Active;
  StatisticsColumn = 4;
  Data->Excel->PutStr(1, EXCEL_KRIVKA_RADEK1, LoadStr(DEN));
  Data->Excel->PutStr(2, EXCEL_KRIVKA_RADEK1, LoadStr(CILOVA_HMOTNOST) + " [" + Data->Soubor.Jednotky + "]");
  Data->Excel->PutStr(3, EXCEL_KRIVKA_RADEK1, LoadStr(HMOTNOST) + " [" + Data->Soubor.Jednotky + "]");
  if (TheoreticalCurve) {
    Data->Excel->PutStr(4, EXCEL_KRIVKA_RADEK1, TheoreticalCurve->GetName() + " [" + Data->Soubor.Jednotky + "]");
    Data->Excel->PutStr(5, EXCEL_KRIVKA_RADEK1, LoadStr(WEIGHT_DIFFERENCE) + " [" + Data->Soubor.Jednotky + "]");
    if (UseStatistics) {
      StatisticsColumn = 6;     // Posunu az za porovnani
    }
  }
  if (UseStatistics) {
    Data->Excel->PutStr(StatisticsColumn, EXCEL_KRIVKA_RADEK1, Chart->Series[5]->Title);
  }

  // Vyplnim hodnoty
  for (int Index = 0; Index < Chart->Series[0]->Count(); Index++) {
    Day        = Chart->Series[0]->XValue[Index];
    RealWeight = Chart->Series[0]->YValue[Index];
    Data->Excel->PutFloat(  1, EXCEL_KRIVKA_RADEK1 + 1 + Index, Day);
    Data->Excel->PutFloat(  2, EXCEL_KRIVKA_RADEK1 + 1 + Index, Chart->Series[1]->YValue[Index]);  // Cilova hmotnost
    Data->Excel->PutFloat(  3, EXCEL_KRIVKA_RADEK1 + 1 + Index, FormatFloat(FORMAT_HMOTNOSTI, RealWeight).ToDouble());  // Opravdova hmotnost

    // Pokud porovnava s teoretickou krivkou, exportuju i ji a rozdil
    if (TheoreticalCurve) {
      TheoreticalWeight = FormatFloat(FORMAT_HMOTNOSTI, TheoreticalCurve->CalculateWeight(Day)).ToDouble();
      Data->Excel->PutFloat(4, EXCEL_KRIVKA_RADEK1 + 1 + Index, FormatFloat(FORMAT_HMOTNOSTI, TheoreticalWeight).ToDouble());
      Data->Excel->PutFloat(5, EXCEL_KRIVKA_RADEK1 + 1 + Index, FormatFloat(FORMAT_HMOTNOSTI, RealWeight - TheoreticalWeight).ToDouble());
    }

    // Statistika
    if (UseStatistics) {
      Data->Excel->PutFloat(StatisticsColumn, EXCEL_KRIVKA_RADEK1 + 1 + Index, FormatFloat(FORMAT_HMOTNOSTI, Chart->Series[5]->YValue[Index]).ToDouble());
    }
  }
}

//---------------------------------------------------------------------------
// Export online vazeni
//---------------------------------------------------------------------------

#define EXCEL_ONLINE_RADEK1  6          // Na kterem radku zacinaji zaznamy

void ExportOnline(TQuery *Query, int Min, int Max, int Average) {
  // Exportuje online v <Query> do Excelu
  int PocetZaznamu;
  double Suma;
  int Pocet;
  String Str;
  int Zaznam;     // Aktualni zaznam

  // Otevru Excel
  if (!Data->Excel->Open(Data->Version.ScaleTitle)) {
    MessageBox(NULL,LoadStr(HLAVNI_EXCEL_NENI).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Vyplnim hlavicku
  Data->Excel->PutStr(1, 1, HlavniForm->OnlineTabSheet->Caption);
  Data->Excel->PutStr(1, 2, LoadStr(SOUBOR));
  Data->Excel->PutStr(2, 2, Data->Soubor.Jmeno);
  Data->Excel->PutStr(1, 3, LoadStr(DEN));
  Data->Excel->PutInt(2, 3, Data->Soubor.Den);
  Data->Excel->PutStr(1, 4, LoadStr(DATUM));
  Data->Excel->PutStr(2, 4, HlavniForm->DatumLabel->Caption);
  Data->Excel->PutStr(1, EXCEL_ONLINE_RADEK1, HlavniForm->OnlineDBGrid->Columns->Items[0]->Title->Caption);   // Sample
  Data->Excel->PutStr(2, EXCEL_ONLINE_RADEK1, HlavniForm->OnlineDBGrid->Columns->Items[1]->Title->Caption);   // Hour
  Data->Excel->PutStr(3, EXCEL_ONLINE_RADEK1, HlavniForm->OnlineDBGrid->Columns->Items[2]->Title->Caption);   // Weight
  Data->Excel->PutStr(4, EXCEL_ONLINE_RADEK1, HlavniForm->OnlineDBGrid->Columns->Items[3]->Title->Caption);   // Stable
  Data->Excel->PutStr(5, EXCEL_ONLINE_RADEK1, HlavniForm->OnlineDBGrid->Columns->Items[4]->Title->Caption);   // Saved

  // Vyplnim hodnoty
  Query->First();
  Query->MoveBy(Min - 1);  // Najedu na prvni zaznam
  PocetZaznamu = 0;
  Zaznam = Min;
  Suma = 0;
  Pocet = 0;
  while (!Query->Eof) {
    Zaznam++;
    if (Zaznam >= Max) {
      break;   // Uz jsem an konci
    }
    Suma += Query->FieldByName("WEIGHT")->AsFloat;
    Pocet++;
    if (Pocet < Average) {
      Query->Next();
      continue;
    }
    Suma /= (double)Pocet;
    Data->Excel->PutInt(1, EXCEL_ONLINE_RADEK1 + 1 + PocetZaznamu, PocetZaznamu + 1);
    Data->Excel->PutInt(2, EXCEL_ONLINE_RADEK1 + 1 + PocetZaznamu, Query->FieldByName("TIME_HOUR")->AsInteger);
    Data->Excel->PutFloat(3, EXCEL_ONLINE_RADEK1 + 1 + PocetZaznamu, Suma);
    if (Query->FieldByName("STABLE")->AsBoolean) {
      Str = "1";
    } else {
      Str = "0";
    }
    Data->Excel->PutStr(4, EXCEL_ONLINE_RADEK1 + 1 + PocetZaznamu, Str);
    if (Query->FieldByName("SAVED")->AsBoolean) {
      Str = "1";
    } else {
      Str = "0";
    }
    Data->Excel->PutStr(5, EXCEL_ONLINE_RADEK1 + 1 + PocetZaznamu, Str);
    Query->Next();
    PocetZaznamu++;
    if (PocetZaznamu > 65000) {
      break;   // Excel vic nevezme
    }
    Suma = 0;
    Pocet = 0;
  }//while
}
