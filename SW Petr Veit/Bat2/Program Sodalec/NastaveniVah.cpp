//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "NastaveniVah.h"
#include "DM.h"
#include "Input.h"
#include "Version.h"
#include "Curves.h"
#include "Dpi.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TNastaveniVahForm *NastaveniVahForm;

static void VymazTabulku(TStringGrid *Grid);
// Vymazu celou tabulku (nemazu zahlavi)


//---------------------------------------------------------------------------
// Ulozeni konfigurace do databaze
//---------------------------------------------------------------------------

static void SaveConfig110ToTable() {
  // Ulozi do NastaveniVahTable konfiguraci verze 1.10 a vyssi
  Data->NastaveniVahTable->FieldByName("F_MARGIN_ABOVE")->AsInteger     = Data->Module->ConfigSection->Config.MarginAbove[GENDER_FEMALE];
  Data->NastaveniVahTable->FieldByName("F_MARGIN_UNDER")->AsInteger     = Data->Module->ConfigSection->Config.MarginBelow[GENDER_FEMALE];
  Data->NastaveniVahTable->FieldByName("M_MARGIN_ABOVE")->AsInteger     = Data->Module->ConfigSection->Config.MarginAbove[GENDER_MALE];
  Data->NastaveniVahTable->FieldByName("M_MARGIN_UNDER")->AsInteger     = Data->Module->ConfigSection->Config.MarginBelow[GENDER_MALE];
  Data->NastaveniVahTable->FieldByName("FILTER_VALUE")->AsInteger       = Data->Module->ConfigSection->Config.Filter;
  Data->NastaveniVahTable->FieldByName("STABILIZATION")->AsFloat        = (double)Data->Module->ConfigSection->Config.StabilizationRange / 10.0;
  Data->NastaveniVahTable->FieldByName("STABILIZATION_TIME")->AsInteger = Data->Module->ConfigSection->Config.StabilizationTime;
  Data->NastaveniVahTable->FieldByName("AUTO_MODE")->AsInteger          = Data->Module->ConfigSection->Config.AutoMode;
  Data->NastaveniVahTable->FieldByName("JUMP_MODE")->AsInteger          = Data->Module->ConfigSection->Config.JumpMode;
  Data->NastaveniVahTable->FieldByName("UNITS")->AsInteger              = Data->Module->ConfigSection->Config.Units;
  Data->NastaveniVahTable->FieldByName("HIST_RANGE")->AsInteger         = Data->Module->ConfigSection->Config.HistogramRange;
  Data->NastaveniVahTable->FieldByName("UNI_RANGE")->AsInteger          = Data->Module->ConfigSection->Config.UniformityRange;
  // TGsm
  Data->NastaveniVahTable->FieldByName("GSM_USE")->AsBoolean            = Data->Module->ConfigSection->Config.Gsm.Use;
  Data->NastaveniVahTable->FieldByName("GSM_SEND_MIDNIGHT")->AsBoolean  = Data->Module->ConfigSection->Config.Gsm.SendMidnight;
  Data->NastaveniVahTable->FieldByName("GSM_PERIOD_MIDNIGHT1")->AsInteger = Data->Module->ConfigSection->Config.Gsm.PeriodMidnight1;
  Data->NastaveniVahTable->FieldByName("GSM_DAY_MIDNIGHT1")->AsInteger  = KConvertGetWord(Data->Module->ConfigSection->Config.Gsm.DayMidnight1);
  Data->NastaveniVahTable->FieldByName("GSM_PERIOD_MIDNIGHT2")->AsInteger = Data->Module->ConfigSection->Config.Gsm.PeriodMidnight2;
  Data->NastaveniVahTable->FieldByName("GSM_MIDNIGHT_SEND_HOUR")->AsInteger = Data->Module->ConfigSection->Config.Gsm.MidnightSendHour;
  Data->NastaveniVahTable->FieldByName("GSM_MIDNIGHT_SEND_MIN")->AsInteger = Data->Module->ConfigSection->Config.Gsm.MidnightSendMin;
  Data->NastaveniVahTable->FieldByName("GSM_SEND_REQUEST")->AsBoolean   = Data->Module->ConfigSection->Config.Gsm.SendOnRequest;
  Data->NastaveniVahTable->FieldByName("GSM_EXEC_REQUEST")->AsBoolean   = false;
  Data->NastaveniVahTable->FieldByName("GSM_CHECK_NUMBERS")->AsBoolean  = Data->Module->ConfigSection->Config.Gsm.CheckNumbers;
  Data->NastaveniVahTable->FieldByName("GSM_COUNT")->AsInteger          = Data->Module->ConfigSection->Config.Gsm.NumberCount;
  for (int i = 0; i < GSM_NUMBER_MAX_COUNT; i++) {
    Data->NastaveniVahTable->FieldByName("GSM_NUMBER" + FormatFloat("00", i))->AsString = KConvertGetString(Data->Module->ConfigSection->Config.Gsm.Numbers[i], GSM_NUMBER_MAX_LENGTH);
  }
  // Podsvit
  Data->NastaveniVahTable->FieldByName("BACKLIGHT")->AsInteger          = Data->Module->ConfigSection->Config.Backlight;
  // RS-485
  Data->NastaveniVahTable->FieldByName("RS485_SPEED")->AsInteger           = KConvertGetDword(Data->Module->ConfigSection->Config.Rs485.Speed);
  Data->NastaveniVahTable->FieldByName("RS485_PARITY")->AsInteger          = Data->Module->ConfigSection->Config.Rs485.Parity;
  Data->NastaveniVahTable->FieldByName("RS485_REPLY_DELAY")->AsInteger     = KConvertGetWord(Data->Module->ConfigSection->Config.Rs485.ReplyDelay);
  Data->NastaveniVahTable->FieldByName("RS485_SILENT_INTERVAL")->AsInteger = Data->Module->ConfigSection->Config.Rs485.SilentInterval;
  Data->NastaveniVahTable->FieldByName("RS485_PROTOCOL")->AsInteger        = Data->Module->ConfigSection->Config.Rs485.Protocol;

  // Porovnani vysledku v preddefinovanem nastaveni neni

  // Kompenzacni krivka
  Data->NastaveniVahTable->FieldByName("CORRECTION_DAY1")->AsInteger     = KConvertGetWord(Data->Module->ConfigSection->Config.WeightCorrection.Day1);
  Data->NastaveniVahTable->FieldByName("CORRECTION_DAY2")->AsInteger     = KConvertGetWord(Data->Module->ConfigSection->Config.WeightCorrection.Day2);
  Data->NastaveniVahTable->FieldByName("CORRECTION_CORRECTION")->AsFloat = (double)Data->Module->ConfigSection->Config.WeightCorrection.Correction / 10.0;   // Bajt na desetiny
} // SaveConfig110ToTable

void TNastaveniVahForm::UlozKonfiguraciDoTabulky() {
  // Ulozi nastaveni ze struktury do tabulky
  // TKonfigurace
  Data->NastaveniVahTable->Edit();
  SaveConfig110ToTable();
  Data->NastaveniVahTable->Post();
}

//---------------------------------------------------------------------------
// Ulozeni hejn do databaze
//---------------------------------------------------------------------------

static void SaveFlocksToTable(TFlock *Flock) {
  // Ulozi hejna <Flock> ze struktury do tabulky

  // Ulozim vsechna platna hejna
  for (int Hejno = 0; Hejno < FLOCKS_MAX_COUNT; Hejno++) {
    if (Flock->Header.Number != Hejno) {
      Flock++;          // Toto hejno neni platne, jdu na dalsi hejno
      continue;
    }
    Data->NastaveniHejnTable->Append();
    Data->NastaveniHejnTable->Edit();
    Data->NastaveniHejnTable->FieldByName("SET")->AsString         = Data->NastaveniVahTable->FieldByName("SET")->AsString;
    Data->NastaveniHejnTable->FieldByName("NUMBER")->AsInteger     = Flock->Header.Number;
    Data->NastaveniHejnTable->FieldByName("NAME")->AsString        = KConvertGetString(Flock->Header.Title, FLOCK_NAME_MAX_LENGTH);
    Data->NastaveniHejnTable->FieldByName("USE_CURVES")->AsBoolean = Flock->Header.UseCurves;
    Data->NastaveniHejnTable->FieldByName("USE_GENDER")->AsBoolean = Flock->Header.UseBothGenders;
    if (Flock->Header.WeighFrom != FLOCK_TIME_LIMIT_EMPTY) {
      Data->NastaveniHejnTable->FieldByName("WEIGH_FROM")->AsInteger = Flock->Header.WeighFrom;
    }
    if (Flock->Header.WeighTill != FLOCK_TIME_LIMIT_EMPTY) {
      Data->NastaveniHejnTable->FieldByName("WEIGH_TO")->AsInteger   = Flock->Header.WeighTill;
    }
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      Data->NastaveniHejnTable->FieldByName("FEMALE_DAY"    + FormatFloat("00", i))->AsInteger = KConvertGetWord(Flock->GrowthCurve[GENDER_FEMALE][i].Day);
      Data->NastaveniHejnTable->FieldByName("FEMALE_WEIGHT" + FormatFloat("00", i))->AsFloat   = PrepoctiNaKg(KConvertGetWord(Flock->GrowthCurve[GENDER_FEMALE][i].Weight));
    }
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      Data->NastaveniHejnTable->FieldByName("MALE_DAY"      + FormatFloat("00", i))->AsInteger = KConvertGetWord(Flock->GrowthCurve[GENDER_MALE][i].Day);
      Data->NastaveniHejnTable->FieldByName("MALE_WEIGHT"   + FormatFloat("00", i))->AsFloat   = PrepoctiNaKg(KConvertGetWord(Flock->GrowthCurve[GENDER_MALE][i].Weight));
    }
    Data->NastaveniHejnTable->Post();
    Flock++;            // Jdu na dalsi hejno
  }//for
}

void TNastaveniVahForm::UlozHejnaDoTabulky() {
  // Ulozi hejna ze struktury do tabulky. Musi byt vybrana sada, do ktere se hejna kopiruji (tj. i filtr na NastaveniHejnTable).

  // Smazu vsechna hejna aktualni sady (filtr by mel byt nastaven)
  Data->NastaveniHejnTable->First();
  while (!Data->NastaveniHejnTable->Eof) {
    Data->NastaveniHejnTable->Delete();
  }
  // Ulozim hejna
  SaveFlocksToTable(Data->Module->ConfigSection->Flocks);
}

//---------------------------------------------------------------------------
// Ulozeni konfigurace do modulu
//---------------------------------------------------------------------------

bool TNastaveniVahForm::UlozKonfiguraciDoModulu() {
  // Ulozi strukturu konfigurace do pametoveho modulu
  byte Config[FL_CONFIG_SIZE];               // surova Flash Data - sekce konfigu
  byte *p;

  if (!Data->ModulPripraven()) {
    return false;
  }
  // Zapisu konfiguraci
  Screen->Cursor = crHourGlass;
  if (!Data->Module->WriteConfigSection()) {
    Screen->Cursor = crDefault;
    return false;
  }
  // Ulozim si data, ktere jsem vysilal
  p = (byte *)Data->Module->ConfigSection;      // Konfiguracni sekce zacina na stejne pozici u vsech verzi, neni treba rozlisovat
  for (int i = 0; i < FL_CONFIG_SIZE; i++) {
    Config[i] = *p;
    p++;
  }
  // Nactu zapsanou konfiguraci
  if (!Data->Module->ReadConfigSection()) {
    Screen->Cursor = crDefault;
    return false;
  }
  // Verifikuji
  p = (byte *)Data->Module->ConfigSection;
  for (int i = 0; i < FL_CONFIG_SIZE; i++) {
    if (Config[i] != *p) {
      Screen->Cursor = crDefault;
      return false;
    }
    p++;
  }

  Screen->Cursor = crDefault;
  return true;
}

//---------------------------------------------------------------------------
// Ulozeni konfigurace z aktualni sady do struktury
//---------------------------------------------------------------------------

static byte CfgCalcChecksum(byte __xdata__ *cfg, int Size) {
  // Spocita a vrati Kontrolni soucet konfigurace zadane v <cfg> o delce <Size>
  byte i;
  byte sum;

  sum = 0;
  // bez koncove sumy :
  for (i = 0; i < Size - 1; i++) {
    sum += cfg[i];
  }
  return sum;
} // CfgCalcChecksum

static void SaveConfig110ToStruct() {
  bool NacitatDalsiCisla = true;
  String Cislo;

  // TKonfigurace
  for (int i = 0; i < ID_MAX_LENGTH; i++) {
    Data->Module->ConfigSection->Config.IdentificationNumber[i] = TEXT_SPACE;      // Id. cislo nastavim na same mezery
  }
  Data->Module->ConfigSection->Config.MarginAbove[GENDER_FEMALE] = Data->NastaveniVahTable->FieldByName("F_MARGIN_ABOVE")->AsInteger;
  Data->Module->ConfigSection->Config.MarginBelow[GENDER_FEMALE] = Data->NastaveniVahTable->FieldByName("F_MARGIN_UNDER")->AsInteger;
  Data->Module->ConfigSection->Config.MarginAbove[GENDER_MALE]   = Data->NastaveniVahTable->FieldByName("M_MARGIN_ABOVE")->AsInteger;
  Data->Module->ConfigSection->Config.MarginBelow[GENDER_MALE]   = Data->NastaveniVahTable->FieldByName("M_MARGIN_UNDER")->AsInteger;
  Data->Module->ConfigSection->Config.Filter                     = Data->NastaveniVahTable->FieldByName("FILTER_VALUE")->AsInteger;
  Data->Module->ConfigSection->Config.StabilizationRange         = Data->NastaveniVahTable->FieldByName("STABILIZATION")->AsFloat * 10.0;
  Data->Module->ConfigSection->Config.StabilizationTime          = Data->NastaveniVahTable->FieldByName("STABILIZATION_TIME")->AsInteger;
  Data->Module->ConfigSection->Config.AutoMode                   = (TAutoMode)Data->NastaveniVahTable->FieldByName("AUTO_MODE")->AsInteger;
  Data->Module->ConfigSection->Config.JumpMode                   = (TJumpMode)Data->NastaveniVahTable->FieldByName("JUMP_MODE")->AsInteger;
  Data->Module->ConfigSection->Config.Units                      = (TUnits)Data->NastaveniVahTable->FieldByName("UNITS")->AsInteger;
  Data->Module->ConfigSection->Config.HistogramRange             = Data->NastaveniVahTable->FieldByName("HIST_RANGE")->AsInteger;
  Data->Module->ConfigSection->Config.UniformityRange            = Data->NastaveniVahTable->FieldByName("UNI_RANGE")->AsInteger;
  // TGsm
  Data->Module->ConfigSection->Config.Gsm.Use              = Data->NastaveniVahTable->FieldByName("GSM_USE")->AsBoolean;
  Data->Module->ConfigSection->Config.Gsm.SendMidnight     = Data->NastaveniVahTable->FieldByName("GSM_SEND_MIDNIGHT")->AsBoolean;
  Data->Module->ConfigSection->Config.Gsm.PeriodMidnight1  = Data->NastaveniVahTable->FieldByName("GSM_PERIOD_MIDNIGHT1")->AsInteger;
  Data->Module->ConfigSection->Config.Gsm.DayMidnight1     = KConvertPutWord(Data->NastaveniVahTable->FieldByName("GSM_DAY_MIDNIGHT1")->AsInteger);
  Data->Module->ConfigSection->Config.Gsm.PeriodMidnight2  = Data->NastaveniVahTable->FieldByName("GSM_PERIOD_MIDNIGHT2")->AsInteger;
  Data->Module->ConfigSection->Config.Gsm.MidnightSendHour = Data->NastaveniVahTable->FieldByName("GSM_MIDNIGHT_SEND_HOUR")->AsInteger;
  Data->Module->ConfigSection->Config.Gsm.MidnightSendMin  = Data->NastaveniVahTable->FieldByName("GSM_MIDNIGHT_SEND_MIN")->AsInteger;
  Data->Module->ConfigSection->Config.Gsm.SendOnRequest    = Data->NastaveniVahTable->FieldByName("GSM_SEND_REQUEST")->AsBoolean;
  Data->Module->ConfigSection->Config.Gsm.CheckNumbers     = Data->NastaveniVahTable->FieldByName("GSM_CHECK_NUMBERS")->AsBoolean;
  Data->Module->ConfigSection->Config.Gsm.NumberCount      = 0;
  for (int i = 0; i < GSM_NUMBER_MAX_COUNT; i++) {
    try {
      Data->NastaveniVahTable->FieldByName("GSM_NUMBER" + FormatFloat("00", i))->AsString.ToDouble();
      if (NacitatDalsiCisla) {
        Data->Module->ConfigSection->Config.Gsm.NumberCount++;
        Cislo = Data->NastaveniVahTable->FieldByName("GSM_NUMBER" + FormatFloat("00", i))->AsString;
        while (Cislo.Length() < GSM_NUMBER_MAX_LENGTH) {
          Cislo += " ";   // Doplnim mezerami
        }
      }
    } catch(...) {
      NacitatDalsiCisla = false;  // Prvni prazdne cislo ukoncuje
      Cislo = "";
      for (int i = 0; i < GSM_NUMBER_MAX_LENGTH; i++) {
        Cislo += " ";
      }
    }
    KConvertPutString(Data->Module->ConfigSection->Config.Gsm.Numbers[i], Cislo, GSM_NUMBER_MAX_LENGTH);
  }//for
  // Podsvit
  Data->Module->ConfigSection->Config.Backlight = (TBacklight)Data->NastaveniVahTable->FieldByName("BACKLIGHT")->AsInteger;
  // RS-485
  Data->Module->ConfigSection->Config.Rs485.Address        = DEFAULT_RS485_ADDRESS;   // Nastavim na default, stejne se nekopiruje
  Data->Module->ConfigSection->Config.Rs485.Speed          = KConvertPutDword(Data->NastaveniVahTable->FieldByName("RS485_SPEED")->AsInteger);
  Data->Module->ConfigSection->Config.Rs485.Parity         = (TParity)Data->NastaveniVahTable->FieldByName("RS485_PARITY")->AsInteger;
  Data->Module->ConfigSection->Config.Rs485.ReplyDelay     = KConvertPutWord(Data->NastaveniVahTable->FieldByName("RS485_REPLY_DELAY")->AsInteger);
  Data->Module->ConfigSection->Config.Rs485.SilentInterval = Data->NastaveniVahTable->FieldByName("RS485_SILENT_INTERVAL")->AsInteger;
  Data->Module->ConfigSection->Config.Rs485.Protocol       = (TProtocol)Data->NastaveniVahTable->FieldByName("RS485_PROTOCOL")->AsInteger;
  // Porovnani
  Data->Module->ConfigSection->Config.ComparisonFlock = FLOCK_EMPTY_NUMBER;          // Default s nicim neporovnavam

  // Kompenzacni krivka
  Data->Module->ConfigSection->Config.WeightCorrection.Day1 = KConvertPutWord(Data->NastaveniVahTable->FieldByName("CORRECTION_DAY1")->AsInteger);
  Data->Module->ConfigSection->Config.WeightCorrection.Day2 = KConvertPutWord(Data->NastaveniVahTable->FieldByName("CORRECTION_DAY2")->AsInteger);
  double d = Data->NastaveniVahTable->FieldByName("CORRECTION_CORRECTION")->AsFloat;    // Musim rucne, jinak spatne zaokrouhluje
  d *= 10.0;
  Data->Module->ConfigSection->Config.WeightCorrection.Correction = d;

  // Reserved nuluju
  for (int i = 0; i < CONFIG_RESERVED_SIZE; i++) {
    Data->Module->ConfigSection->Config.Reserved[i] = 0;
  }
  // Checksum
  Data->Module->ConfigSection->Config.Checksum  = CfgCalcChecksum((byte *)&Data->Module->ConfigSection->Config, sizeof(TConfig));
} // SaveConfig110ToStruct

void TNastaveniVahForm::UlozSaduDoStruktury(int Version) {
  // Ulozi konfiguraci z prave vybrane sady do struktury verze <Version>

  // Verze - ta je umistena shodne ve vsech verzich
  Data->Module->ConfigSection->Config.Version = KConvertPutWord(Version);
  // Dalsi polozky konfigurace
  SaveConfig110ToStruct();
} // UlozSaduDoStruktury

//---------------------------------------------------------------------------
// Ulozeni hejn z aktualni sady do struktury
//---------------------------------------------------------------------------

static void SaveFlocksToStruct(TFlock *Flock) {
  // Ulozi hejna <Flock> z tabulky do struktury
  int Hejno;
  AnsiString Str;
  word w;
  double d;

  // Smazu vsechna hejna
  for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
    Flock[i].Header.Number = FLOCK_EMPTY_NUMBER;
  }
  // A ulozim ty, ktere zadal
  Data->NastaveniHejnTable->First();
  while (!Data->NastaveniHejnTable->Eof) {
    Hejno = Data->NastaveniHejnTable->FieldByName("NUMBER")->AsInteger;
    Flock[Hejno].Header.Number = Data->NastaveniHejnTable->FieldByName("NUMBER")->AsInteger;
    Str = Data->NastaveniHejnTable->FieldByName("NAME")->AsString.UpperCase();
    while (Str.Length() < FLOCK_NAME_MAX_LENGTH) {
      Str += " ";
    }
    KConvertPutString(Flock[Hejno].Header.Title, Str, FLOCK_NAME_MAX_LENGTH);
    Flock[Hejno].Header.UseCurves      = Data->NastaveniHejnTable->FieldByName("USE_CURVES")->AsBoolean;
    Flock[Hejno].Header.UseBothGenders = Data->NastaveniHejnTable->FieldByName("USE_GENDER")->AsBoolean;
    if (Data->NastaveniHejnTable->FieldByName("WEIGH_FROM")->IsNull) {
      Flock[Hejno].Header.WeighFrom    = FLOCK_TIME_LIMIT_EMPTY;
    } else {
      Flock[Hejno].Header.WeighFrom    = Data->NastaveniHejnTable->FieldByName("WEIGH_FROM")->AsInteger;
    }
    if (Data->NastaveniHejnTable->FieldByName("WEIGH_TO")->IsNull) {
      Flock[Hejno].Header.WeighTill    = FLOCK_TIME_LIMIT_EMPTY;
    } else {
      Flock[Hejno].Header.WeighTill    = Data->NastaveniHejnTable->FieldByName("WEIGH_TO")->AsInteger;
    }
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      d = Data->NastaveniHejnTable->FieldByName("FEMALE_WEIGHT" + FormatFloat("00", i))->AsFloat * 1000.0;
      w = d;
      Flock[Hejno].GrowthCurve[GENDER_FEMALE][i].Day    = KConvertGetWord((word)Data->NastaveniHejnTable->FieldByName("FEMALE_DAY" + FormatFloat("00", i))->AsInteger);
      Flock[Hejno].GrowthCurve[GENDER_FEMALE][i].Weight = KConvertGetWord(w);
    }
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      d = Data->NastaveniHejnTable->FieldByName("MALE_WEIGHT" + FormatFloat("00", i))->AsFloat * 1000.0;
      w = d;
      Flock[Hejno].GrowthCurve[GENDER_MALE][i].Day    = KConvertGetWord((word)Data->NastaveniHejnTable->FieldByName("MALE_DAY" + FormatFloat("00", i))->AsInteger);
      Flock[Hejno].GrowthCurve[GENDER_MALE][i].Weight = KConvertGetWord(w);
    }

    Data->NastaveniHejnTable->Next();
  }
}

void TNastaveniVahForm::UlozHejnaDoStruktury() {
  // Ulozi hejna z prave vybrane sady do struktury
  SaveFlocksToStruct(Data->Module->ConfigSection->Flocks);
}

//---------------------------------------------------------------------------
// Nacteni vsech sad
//---------------------------------------------------------------------------

void TNastaveniVahForm::NactiSady() {
  // Nacte vsechny sady do seznamu
  bool Enable;

  SeznamListBox->Clear();
  Data->NastaveniVahTable->First();
  while (!Data->NastaveniVahTable->Eof) {
    SeznamListBox->Items->Add(Data->NastaveniVahTable->FieldByName("SET")->AsString);
    Data->NastaveniVahTable->Next();
  }
  Enable = (bool)(SeznamListBox->Items->Count > 0);
  HejnaTabSheet->Enabled      = Enable;
  StatistikaTabSheet->Enabled = Enable;
  GsmTabSheet->Enabled        = Enable;
  VahyTabSheet->Enabled       = Enable;
}

//---------------------------------------------------------------------------
// Vyber sady ze seznamu
//---------------------------------------------------------------------------

void TNastaveniVahForm::VyberPrvniSadu() {
  // Vybere ze seznamu prvni sadu
  if (SeznamListBox->Items->Count > 0) {
    SeznamListBox->ItemIndex = 0;   // Vyberu prvni v seznamu
    SeznamListBoxClick(NULL);
    SeznamListBox->SetFocus();
  }
}

void TNastaveniVahForm::VyberSadu(String Sada) {
  // Vybere ze seznamu zadanou sadu s nazvem <Sada>
  int Pozice;

  Pozice = SeznamListBox->Items->IndexOf(Sada);
  if (Pozice >= 0) {
    SeznamListBox->ItemIndex = Pozice;   // Vyberu prvni v seznamu
    SeznamListBoxClick(NULL);
    SeznamListBox->SetFocus();
  }
}

//---------------------------------------------------------------------------
// Zalozeni nove sady
//---------------------------------------------------------------------------

bool TNastaveniVahForm::NovaSada(bool CreateDefaultFlock) {
  // Default hejno pro broilery
  const int BROILERS_DAYS_COUNT = 7;
  const int BroilersDays[BROILERS_DAYS_COUNT]       = {0,     1,     2,     3,     4,     5,     6};
  const double BroilersWeights[BROILERS_DAYS_COUNT] = {0.042, 0.049, 0.059, 0.075, 0.094, 0.117, 0.144};

  // Zalozi novou sadu
  InputForm->Caption        = LoadStr(NASTAVENIVAH_NOVA_SADA);
  InputForm->Label->Caption = LoadStr(NASTAVENIVAH_NOVA_SADA_ZADEJTE);
  InputForm->Edit->Text     = "";
  if (InputForm->ShowModal() != mrOk) {
    return false;
  }
  InputForm->Edit->Text = InputForm->Edit->Text.SubString(1,15);   // Max 15 znaku
  if (Data->NastaveniVahTable->Locate("SET", InputForm->Edit->Text, Data->SearchOptions)) {
    // Sada uz existuje
    MessageBox(NULL,LoadStr(NASTAVENIVAH_SADA_EXISTUJE).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return false;
  }
  // Zalozim novou sadu
  AnsiString SetName = InputForm->Edit->Text;
  try {
    Data->NastaveniVahTable->Append();
    Data->NastaveniVahTable->Edit();
    Data->NastaveniVahTable->FieldByName("SET")->AsString = SetName;
    VyplnDefaultHodnotySady();
    Data->NastaveniVahTable->Post();

    // 16.5.2009: Na zadost Richarda Wagnera z DACS zalozim rovnou standardni hejno pro broilery
    if (CreateDefaultFlock) {
      Data->NastaveniHejnTable->Append();
      Data->NastaveniHejnTable->Edit();
      Data->NastaveniHejnTable->FieldByName("SET")->AsString         = SetName;
      Data->NastaveniHejnTable->FieldByName("NUMBER")->AsInteger     = 0;
      Data->NastaveniHejnTable->FieldByName("NAME")->AsString        = "BROILERS";
      Data->NastaveniHejnTable->FieldByName("USE_GENDER")->AsBoolean = false;
      Data->NastaveniHejnTable->FieldByName("USE_CURVES")->AsBoolean = true;      // Pouziju krivku pro prvnich par dnu

      for (int i = 0; i < BROILERS_DAYS_COUNT; i++) {
        Data->NastaveniHejnTable->FieldByName("FEMALE_DAY"    + FormatFloat("00", i))->AsInteger = BroilersDays[i];
        Data->NastaveniHejnTable->FieldByName("FEMALE_WEIGHT" + FormatFloat("00", i))->AsFloat   = BroilersWeights[i];
      }

      Data->NastaveniHejnTable->Post();
    }

    NactiSady();
    VyberSadu(InputForm->Edit->Text);
  } catch(...) {
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Nacteni hejn do seznamu
//---------------------------------------------------------------------------

void TNastaveniVahForm::NactiSeznamHejn(String Sada) {
  // Nacte vsechna hejna sady <Sada> do seznamu
  Data->NastaveniHejnTable->Filter = "SET='" + Sada + "'";
  SeznamHejnListBox->Clear();
  Data->NastaveniHejnTable->First();
  while (!Data->NastaveniHejnTable->Eof) {
    SeznamHejnListBox->Items->Add(Data->NastaveniHejnTable->FieldByName("NUMBER")->AsString);
    Data->NastaveniHejnTable->Next();
  }
  HejnaPanel->Enabled = (bool)(SeznamHejnListBox->Items->Count > 0);
}

//---------------------------------------------------------------------------
// Zobrazeni prvniho hejna
//---------------------------------------------------------------------------

void TNastaveniVahForm::VyberPrvnihejno() {
  if (SeznamHejnListBox->Items->Count > 0) {
    // V sade jsou nejaka hejna, vyberu prvni z nich
    SeznamHejnListBox->ItemIndex = 0;
    SeznamHejnListBoxClick(NULL);
  } else {
    // Zadne hejno v sade neni
    VymazTabulku(KrivkaSamiceStringGrid);
    VymazTabulku(KrivkaSamciStringGrid);
  }
}

//---------------------------------------------------------------------------
// Zobrazeni obsahu sady
//---------------------------------------------------------------------------

void TNastaveniVahForm::ZobrazSadu(String Sada) {
  // Zobrazi obsah sady s nazvem <Sada>
  if (!Data->NastaveniVahTable->Locate("SET", Sada, Data->SearchOptions)) {
    return;     // Sada neexistuje
  }
  // Hejna
  NactiSeznamHejn(Sada);
  VyberPrvnihejno();
  // Zobrazim rucne automaticky mod
  NastaveniGainCheckBox->Checked = (bool)(Data->NastaveniVahTable->FieldByName("AUTO_MODE")->AsInteger == AUTOMODE_WITH_GAIN);

  // Zobrazim rucne pouziti kompenzace
  if (Data->NastaveniVahTable->FieldByName("CORRECTION_CORRECTION")->IsNull) {
    UseCorrectionCheckBox->Checked = false;     // Hodnota neni vyplnena (napr. po upgradu stare verze tabulky)
  } else {
    UseCorrectionCheckBox->Checked = (bool)(Data->NastaveniVahTable->FieldByName("CORRECTION_CORRECTION")->AsFloat > 0);
  }
}

//---------------------------------------------------------------------------
// Kontrola rozsahu
//---------------------------------------------------------------------------

static int PocetZadanychDnuVeKrivce(TStringGrid *Grid) {
  // Vrati pocet zadanych dnu ve krivce <Grid>
  int Pocet = 0;

  for (int row = 1; row < Grid->RowCount; row++) {
    // Zkontroluju, zda zadal cislo
    if (Grid->Cells[1][row].IsEmpty()) {
      break;
    }
    Pocet++;
  }//for
  return Pocet;
}

bool TNastaveniVahForm::KontrolaRozsahuKrivky(bool Pohlavi) {
  // Zkontroluje rozsah u krivky se zadanym pohlavim
  TStringGrid *Grid;
  TGridRect Rect;
  int Den;
  int PocetDnu;       // Pocet dnu zadanych v tabulce
  double Hmotnost;
  TStringList *StringList;

  // Nastavim pohlavi
  if (Pohlavi == GENDER_FEMALE) {
    Grid = KrivkaSamiceStringGrid;
  } else {
    Grid = KrivkaSamciStringGrid;
  }
  PocetDnu = PocetZadanychDnuVeKrivce(Grid);
  StringList = new TStringList();
  try {
    // Kontrola, zda zadal platna cisla
    for (int row = 1; row <= PocetDnu; row++) {
      // Zkontroluju, zda zadal platny den
      try {
        Den = Grid->Cells[1][row].ToInt();
        if (Den < CURVE_FIRST_DAY_NUMBER || Den > CURVE_MAX_DAY) {
          throw 1;
        }
        if (StringList->IndexOf(String(Den)) >= 0) {
          throw 1;
        }
        StringList->Add(String(Den));
      } catch(...) {
        Rect.Left   = 1;
        Rect.Top    = row;
        Rect.Right  = 1;
        Rect.Bottom = row;
        Grid->Selection = Rect;
        Grid->SetFocus();
        throw;
      }
      // Zkontroluju, zda zadal platnou hmotnost
      try {
        Hmotnost = Grid->Cells[2][row].ToDouble();
        if (Hmotnost < PrepoctiNaKg(MIN_TARGET_WEIGHT) || Hmotnost > PrepoctiNaKg(MAX_TARGET_WEIGHT)) {
          throw 1;
        }
      } catch(...) {
        Rect.Left   = 2;
        Rect.Top    = row;
        Rect.Right  = 2;
        Rect.Bottom = row;
        Grid->Selection = Rect;
        Grid->SetFocus();
        throw;
      }
    }
    delete StringList;
    return true;
  } catch(...) {
    delete StringList;
    return false;
  }
}

bool TNastaveniVahForm::KontrolaRozsahuHejna() {
  // Zkontroluje rozsah u vsech hodnot aktualniho hejna
  double Hmotnost;
  int Hodina;

  // Krivky
  if (KrivkyCheckBox->Checked) {
    // Pouziva rustove krivky
    // Samice
    if (!KontrolaRozsahuKrivky(GENDER_FEMALE)) {
      return false;
    }
    // Samci
    if (PohlaviCheckBox->Checked) {
      if (!KontrolaRozsahuKrivky(GENDER_MALE)) {
        return false;
      }
    }
  } else {
    // Rychle zadani vazeni
    // Samice
    try {
      Hmotnost = Data->NastaveniHejnTable->FieldByName("FEMALE_WEIGHT00")->AsFloat;
      if (Hmotnost <= 0.0 || Hmotnost > PrepoctiNaKg(CURVE_MAX_WEIGHT)) {
        throw 1;
      }
    } catch(...) {
      PocatecniHmotnostSamiceDBEdit->SetFocus();
      return false;
    }
    // Samci
    if (PohlaviCheckBox->Checked) {
      try {
        Hmotnost = Data->NastaveniHejnTable->FieldByName("MALE_WEIGHT00")->AsFloat;
        if (Hmotnost <= 0.0 || Hmotnost > PrepoctiNaKg(CURVE_MAX_WEIGHT)) {
          throw 1;
        }
      } catch(...) {
        PocatecniHmotnostSamciDBEdit->SetFocus();
        return false;
      }
    }
  }
  // Omezeni vazeni
  if (!Data->NastaveniHejnTable->FieldByName("WEIGH_FROM")->IsNull) {
    Hodina = Data->NastaveniHejnTable->FieldByName("WEIGH_FROM")->AsInteger;
    if (Hodina > FLOCK_TIME_LIMIT_MAX_HOUR) {
      return false;
    }
  }
  if (!Data->NastaveniHejnTable->FieldByName("WEIGH_TO")->IsNull) {
    Hodina = Data->NastaveniHejnTable->FieldByName("WEIGH_TO")->AsInteger;
    if (Hodina > FLOCK_TIME_LIMIT_MAX_HOUR) {
      return false;
    }
  }
  return true;
}

#define ZkontrolujRozsahPoleSady(Pole, Typ, Min, Max, TabSheet, Control)   \
  try {                                                                    \
    if (Data->NastaveniVahTable->FieldByName(Pole)->IsNull) {              \
      throw 1;                                                             \
    }                                                                      \
    if (Data->NastaveniVahTable->FieldByName(Pole)->Typ < Min              \
     || Data->NastaveniVahTable->FieldByName(Pole)->Typ > Max) {           \
      throw 1;                                                             \
    }                                                                      \
  } catch(...) {                                                           \
    PageControl->ActivePage = TabSheet;                                    \
    Control->SetFocus();                                                   \
    return false;                                                          \
  }

bool TNastaveniVahForm::KontrolaRozsahuSady() {
  // Zkontroluje rozsah u vsech hodnot sady
  ZkontrolujRozsahPoleSady("HIST_RANGE",            AsInteger, MIN_HISTOGRAM_RANGE,       MAX_HISTOGRAM_RANGE,     StatistikaTabSheet, RozsahHistogramuDBEdit);
  ZkontrolujRozsahPoleSady("UNI_RANGE",             AsInteger, MIN_UNIFORMITY_RANGE,      MAX_UNIFORMITY_RANGE,    StatistikaTabSheet, RozsahUniformityDBEdit);
  ZkontrolujRozsahPoleSady("F_MARGIN_ABOVE",        AsInteger, MIN_MARGIN,                MAX_MARGIN,              VahyTabSheet, SamiceOkoliNadDBEdit);
  ZkontrolujRozsahPoleSady("F_MARGIN_UNDER",        AsInteger, MIN_MARGIN,                MAX_MARGIN,              VahyTabSheet, SamiceOkoliPodDBEdit);
  ZkontrolujRozsahPoleSady("M_MARGIN_ABOVE",        AsInteger, MIN_MARGIN,                MAX_MARGIN,              VahyTabSheet, SamciOkoliNadDBEdit);
  ZkontrolujRozsahPoleSady("M_MARGIN_UNDER",        AsInteger, MIN_MARGIN,                MAX_MARGIN,              VahyTabSheet, SamciOkoliPodDBEdit);
  ZkontrolujRozsahPoleSady("FILTER_VALUE",          AsInteger, MIN_FILTER,                MAX_FILTER,              VahyTabSheet, FiltrDBEdit);
  ZkontrolujRozsahPoleSady("STABILIZATION",         AsFloat,   (MIN_STABILIZATION_RANGE / 10.0), (MAX_STABILIZATION_RANGE / 10.0), VahyTabSheet, UstaleniDBEdit);
  ZkontrolujRozsahPoleSady("STABILIZATION_TIME",    AsInteger, MIN_STABILIZATION_TIME,    MAX_STABILIZATION_TIME,  VahyTabSheet, DobaUstaleniDBEdit);
  ZkontrolujRozsahPoleSady("GSM_PERIOD_MIDNIGHT1",  AsInteger, MIN_GSM_PERIOD_MIDNIGHT,   MAX_GSM_PERIOD_MIDNIGHT, GsmTabSheet, Period1DBEdit);
  ZkontrolujRozsahPoleSady("GSM_DAY_MIDNIGHT1",     AsInteger, 0,                         CURVE_MAX_DAY,           GsmTabSheet, Day1DBEdit);
  ZkontrolujRozsahPoleSady("GSM_PERIOD_MIDNIGHT2",  AsInteger, MIN_GSM_PERIOD_MIDNIGHT,   MAX_GSM_PERIOD_MIDNIGHT, GsmTabSheet, Period2DBEdit);
  ZkontrolujRozsahPoleSady("GSM_MIDNIGHT_SEND_HOUR",AsInteger, 0,                         23,                      GsmTabSheet, HourDBEdit);
  ZkontrolujRozsahPoleSady("GSM_MIDNIGHT_SEND_MIN", AsInteger, 0,                         59,                      GsmTabSheet, MinDBEdit);
  ZkontrolujRozsahPoleSady("RS485_REPLY_DELAY",     AsInteger, MIN_RS485_REPLY_DELAY,     MAX_RS485_REPLY_DELAY,     Rs485TabSheet, Rs485ReplyDelayDBEdit);
  ZkontrolujRozsahPoleSady("RS485_SILENT_INTERVAL", AsInteger, MIN_RS485_SILENT_INTERVAL, MAX_RS485_SILENT_INTERVAL, Rs485TabSheet, Rs485SilentIntervalDBEdit);

  if (UseCorrectionCheckBox->Checked) {
    // Pouziva kompenzaci, zkontroluju zadane hodnoty
    ZkontrolujRozsahPoleSady("CORRECTION_DAY1",       AsInteger, 0, CURVE_MAX_DAY, CorrectionTabSheet, CorrectionDay1DBEdit);
    ZkontrolujRozsahPoleSady("CORRECTION_DAY2",       AsInteger, Data->NastaveniVahTable->FieldByName("CORRECTION_DAY1")->AsInteger + 1, CURVE_MAX_DAY, CorrectionTabSheet, CorrectionDay2DBEdit);
    ZkontrolujRozsahPoleSady("CORRECTION_CORRECTION", AsInteger, 0.01, 25.5, CorrectionTabSheet, CorrectionCorrectionDBEdit);
  } else {
    // Nechce pouzit kompenzaci, nastavim nuly
    Data->NastaveniVahTable->Edit();
    Data->NastaveniVahTable->FieldByName("CORRECTION_DAY1")->AsInteger     = 0;
    Data->NastaveniVahTable->FieldByName("CORRECTION_DAY2")->AsInteger     = 0;
    Data->NastaveniVahTable->FieldByName("CORRECTION_CORRECTION")->AsFloat = 0;
  }

  return true;
}

//---------------------------------------------------------------------------
// Zobrazeni obsahu hejna
//---------------------------------------------------------------------------

static void VymazTabulku(TStringGrid *Grid) {
  // Vymazu celou tabulku (nemazu zahlavi)
  for (int row = 1; row < Grid->RowCount; row++) {
    for (int col = 1; col < Grid->ColCount; col++) {
      Grid->Cells[col][row] = "";
    }
  }
}

void TNastaveniVahForm::ZobrazKrivkuHejna(bool Pohlavi) {
  // Zobrazi krivku s pohlavim <Pohlavi> aktualniho hejna
  String Predpona;
  TStringGrid *Grid;

  // Nastavim pohlavi
  if (Pohlavi == GENDER_FEMALE) {
    Predpona = "FEMALE";
    Grid = KrivkaSamiceStringGrid;
  } else {
    Predpona = "MALE";
    Grid = KrivkaSamciStringGrid;
  }
  // Vymazu celou tabulku (nemazu zahlavi)
  VymazTabulku(Grid);
  // Ulozim z databaze do tabulky
  for (int i = 0; i < CURVE_MAX_POINTS; i++) {
    if (Data->NastaveniHejnTable->FieldByName(Predpona + "_WEIGHT" + FormatFloat("00", i))->AsFloat == 0) {
      break;   // Konec krivky
    }
    Grid->Cells[1][i + 1] = Data->NastaveniHejnTable->FieldByName(Predpona + "_DAY" + FormatFloat("00", i))->AsString;
    Grid->Cells[2][i + 1] = FormatFloat(FORMAT_HMOTNOSTI, Data->NastaveniHejnTable->FieldByName(Predpona + "_WEIGHT" + FormatFloat("00", i))->AsFloat);
  }//for
}

void TNastaveniVahForm::UlozKrivkuHejna(bool Pohlavi) {
  // Ulozi zadanou krivku s pohlavim <Pohlavi> aktualniho hejna do databaze. NastaveniHejnTable musi byt v edit modu.
  String Predpona;
  TStringGrid *Grid;
  int PocetDnu;       // Pocet dnu zadanych v tabulce

  // Nastavim pohlavi
  if (Pohlavi == GENDER_FEMALE) {
    Predpona = "FEMALE";
    Grid = KrivkaSamiceStringGrid;
  } else {
    Predpona = "MALE";
    Grid = KrivkaSamciStringGrid;
  }
  PocetDnu = PocetZadanychDnuVeKrivce(Grid);
  // Smazu krivku v databazi
  for (int i = 0; i < CURVE_MAX_POINTS; i++) {
    Data->NastaveniHejnTable->FieldByName(Predpona + "_DAY" + FormatFloat("00", i))->AsInteger  = 0;
    Data->NastaveniHejnTable->FieldByName(Predpona + "_WEIGHT" + FormatFloat("00", i))->AsFloat = 0;
  }//for
  // Ulozim z gridu do databaze
  for (int i = 0; i < PocetDnu; i++) {
    Data->NastaveniHejnTable->FieldByName(Predpona + "_DAY" + FormatFloat("00", i))->AsInteger = Grid->Cells[1][i + 1].ToInt();
    Data->NastaveniHejnTable->FieldByName(Predpona + "_WEIGHT" + FormatFloat("00", i))->AsFloat = Grid->Cells[2][i + 1].ToDouble();
  }//for
}

void TNastaveniVahForm::ZobrazKrivkyHejna() {
  // Zobrazi krivky aktualniho hejna
  if (KrivkyCheckBox->Checked) {
    KrivkaSamiceTabSheet->TabVisible      = true;
    ZobrazKrivkuHejna(GENDER_FEMALE);
    KrivkaSamciTabSheet->TabVisible       = PohlaviCheckBox->Checked;
    if (PohlaviCheckBox->Checked) {
      ZobrazKrivkuHejna(GENDER_MALE);
    }
    KrivkyPageControl->ActivePage = KrivkaSamiceTabSheet;
    PocatecniHmotnostTabSheet->TabVisible = false;
  } else {
    KrivkaSamiceTabSheet->TabVisible      = false;
    KrivkaSamciTabSheet->TabVisible       = false;
    PocatecniHmotnostTabSheet->TabVisible = true;
    PocatecniHmotnostSamciPanel->Visible  = PohlaviCheckBox->Checked;
  }//else
}

void TNastaveniVahForm::ZobrazHejno(int Hejno) {
  // Zobrazi obsah hejna cislo <Hejno>
  if (!Data->NastaveniHejnTable->Locate("NUMBER", Hejno, Data->SearchOptions)) {
    // Hejno neexistuje
    KrivkyPageControl->Visible = false;
    return;
  }
  KrivkyPageControl->Visible = true;
  PohlaviCheckBox->Checked = Data->NastaveniHejnTable->FieldByName("USE_GENDER")->AsBoolean;
  KrivkyCheckBox->Checked  = Data->NastaveniHejnTable->FieldByName("USE_CURVES")->AsBoolean;
  ZobrazKrivkyHejna();
}

//---------------------------------------------------------------------------
// Trideni hejna
//---------------------------------------------------------------------------

static void SetridKrivku(TStringGrid *Grid) {
  // Setridi zadanou krivku podle dne. Volat az po uspesne kontrole rozsahu krivky, zde se jiz nic netestuje.
  int PocetDnu;         // Pocet dnu zadanych v tabulce
  String Den, Hmotnost; // Pro prohozeni

  PocetDnu = PocetZadanychDnuVeKrivce(Grid);
  if (PocetDnu == 0) {
    return;
  }
  for (int row = 1; row <= PocetDnu; row++) {
    for (int row2 = row + 1; row2 <= PocetDnu; row2++) {
      if (Grid->Cells[1][row2].ToInt() < Grid->Cells[1][row].ToInt()) {
        // Prohodim
        Den      = Grid->Cells[1][row];
        Hmotnost = Grid->Cells[2][row];
        Grid->Cells[1][row]  = Grid->Cells[1][row2];
        Grid->Cells[2][row]  = Grid->Cells[2][row2];
        Grid->Cells[1][row2] = Den;
        Grid->Cells[2][row2] = Hmotnost;
      }//if
    }//for
  }//for
}

void TNastaveniVahForm::SetridKrivky() {
  // Setridi obe rustove krivky podle dne
  SetridKrivku(KrivkaSamiceStringGrid);
  if (PohlaviCheckBox->Checked) {
    SetridKrivku(KrivkaSamciStringGrid);
  }
}

//---------------------------------------------------------------------------
// Inicializace zobrazeni
//---------------------------------------------------------------------------

void TNastaveniVahForm::InicializaceZobrazeni() {
  // Inicializuje zobrazovaci prvky
  // Rustove krivky
  KrivkaSamiceStringGrid->RowCount = CURVE_MAX_POINTS + 1;
  KrivkaSamciStringGrid->RowCount  = CURVE_MAX_POINTS + 1;
  KrivkaSamiceStringGrid->Cells[1][0] = LoadStr(DEN);
  KrivkaSamciStringGrid->Cells[1][0]  = LoadStr(DEN);
  KrivkaSamiceStringGrid->Cells[2][0] = LoadStr(HMOTNOST);
  KrivkaSamciStringGrid->Cells[2][0]  = LoadStr(HMOTNOST);
  for (int i = 1; i <= CURVE_MAX_POINTS; i++) {
    KrivkaSamiceStringGrid->Cells[0][i] = String(i);
    KrivkaSamciStringGrid->Cells[0][i]  = String(i);
  }
}

//---------------------------------------------------------------------------
// Default hodnoty
//---------------------------------------------------------------------------

void TNastaveniVahForm::VyplnDefaultHodnotySady() {
  // Vyplni standardni hodnoty sady, sada uz musi byt v edit modu
  Data->NastaveniVahTable->FieldByName("F_MARGIN_ABOVE")->AsInteger     = DEFAULT_FEMALES_MARGIN_ABOVE;
  Data->NastaveniVahTable->FieldByName("F_MARGIN_UNDER")->AsInteger     = DEFAULT_FEMALES_MARGIN_BELOW;
  Data->NastaveniVahTable->FieldByName("M_MARGIN_ABOVE")->AsInteger     = DEFAULT_MALES_MARGIN_ABOVE;
  Data->NastaveniVahTable->FieldByName("M_MARGIN_UNDER")->AsInteger     = DEFAULT_MALES_MARGIN_BELOW;
  Data->NastaveniVahTable->FieldByName("FILTER_VALUE")->AsInteger       = DEFAULT_FILTER;
  Data->NastaveniVahTable->FieldByName("STABILIZATION")->AsFloat        = DEFAULT_STABILIZATION_RANGE / 10.0;
  Data->NastaveniVahTable->FieldByName("STABILIZATION_TIME")->AsInteger = DEFAULT_STABILIZATION_TIME;
  Data->NastaveniVahTable->FieldByName("AUTO_MODE")->AsInteger          = DEFAULT_AUTO_MODE;
  Data->NastaveniVahTable->FieldByName("JUMP_MODE")->AsInteger          = DEFAULT_JUMP_MODE;
  Data->NastaveniVahTable->FieldByName("UNITS")->AsInteger              = DEFAULT_UNITS;
  Data->NastaveniVahTable->FieldByName("HIST_RANGE")->AsInteger         = DEFAULT_HISTOGRAM_RANGE;
  Data->NastaveniVahTable->FieldByName("UNI_RANGE")->AsInteger          = DEFAULT_UNIFORMITY_RANGE;
  Data->NastaveniVahTable->FieldByName("GSM_USE")->AsBoolean            = DEFAULT_GSM_USE;
  Data->NastaveniVahTable->FieldByName("GSM_SEND_MIDNIGHT")->AsBoolean  = DEFAULT_GSM_SEND_MIDNIGHT;
  Data->NastaveniVahTable->FieldByName("GSM_PERIOD_MIDNIGHT1")->AsInteger = DEFAULT_GSM_PERIOD_MIDNIGHT;
  Data->NastaveniVahTable->FieldByName("GSM_DAY_MIDNIGHT1")->AsInteger  = DEFAULT_GSM_DAY_MIDNIGHT;
  Data->NastaveniVahTable->FieldByName("GSM_PERIOD_MIDNIGHT2")->AsInteger = DEFAULT_GSM_PERIOD_MIDNIGHT;
  Data->NastaveniVahTable->FieldByName("GSM_MIDNIGHT_SEND_HOUR")->AsInteger = DEFAULT_GSM_SEND_HOUR_MIDNIGHT;
  Data->NastaveniVahTable->FieldByName("GSM_MIDNIGHT_SEND_MIN")->AsInteger = DEFAULT_GSM_SEND_MIN_MIDNIGHT;
  Data->NastaveniVahTable->FieldByName("GSM_SEND_REQUEST")->AsBoolean   = DEFAULT_GSM_SEND_REQUEST;
  Data->NastaveniVahTable->FieldByName("GSM_EXEC_REQUEST")->AsBoolean   = false;
  Data->NastaveniVahTable->FieldByName("GSM_CHECK_NUMBERS")->AsBoolean  = DEFAULT_GSM_CHECK_NUMBERS;
  Data->NastaveniVahTable->FieldByName("GSM_COUNT")->AsInteger          = DEFAULT_GSM_NUMBER_COUNT;
  Data->NastaveniVahTable->FieldByName("BACKLIGHT")->AsInteger          = BACKLIGHT_ON;
  // RS-485
  Data->NastaveniVahTable->FieldByName("RS485_SPEED")->AsInteger           = DEFAULT_RS485_SPEED;
  Data->NastaveniVahTable->FieldByName("RS485_PARITY")->AsInteger          = DEFAULT_RS485_PARITY;
  Data->NastaveniVahTable->FieldByName("RS485_REPLY_DELAY")->AsInteger     = DEFAULT_RS485_REPLY_DELAY;
  Data->NastaveniVahTable->FieldByName("RS485_SILENT_INTERVAL")->AsInteger = DEFAULT_RS485_SILENT_INTERVAL;
  Data->NastaveniVahTable->FieldByName("RS485_PROTOCOL")->AsInteger        = 0;         // Tady zatim zadnou default hodnotu nemam

  // Kompenzacni krivka - default nekompenzuju
  Data->NastaveniVahTable->FieldByName("CORRECTION_DAY1")->AsInteger     = 0;
  Data->NastaveniVahTable->FieldByName("CORRECTION_DAY2")->AsInteger     = 0;
  Data->NastaveniVahTable->FieldByName("CORRECTION_CORRECTION")->AsFloat = 0;
}



//---------------------------------------------------------------------------
__fastcall TNastaveniVahForm::TNastaveniVahForm(TComponent* Owner)
        : TForm(Owner)
{
  // Nastavim vysku radku vsech StringGridu podle aktualniho DPI
  DpiSetStringGrid(KrivkaSamiceStringGrid);
  DpiSetStringGrid(KrivkaSamciStringGrid);

  InicializaceZobrazeni();
}
//---------------------------------------------------------------------------
void __fastcall TNastaveniVahForm::SaveButtonClick(TObject *Sender)
{
  if (SeznamListBox->ItemIndex < 0) {
    return;
  }
  if (!KontrolaRozsahuSady()) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  if (Data->NastaveniVahTable->State == dsEdit || Data->NastaveniVahTable->State == dsInsert) {
    Data->NastaveniVahTable->Post();
  }
}
//---------------------------------------------------------------------------
void __fastcall TNastaveniVahForm::DBEdit3KeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //   Backspace      0   az   9
  //       |          |        |
  // 3.9.2001: Chtel natvrdo carku, i kdyz ve Win byla nastavena tecka a pak to blblo
//  if (Key!=8 && (Key<48 || Key>57) && Key!=DecimalSeparator) Key=0;  // Vyberu jen cisla a carku
  if (Key!=8 && (Key<48 || Key>57)) Key=0;  // Vyberu jen cisla
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::FormShow(TObject *Sender)
{
  SeznamListBox->Clear();
  SeznamHejnListBox->Clear();
  HejnaPanel->Enabled = false;
  PageControl->ActivePage = HejnaTabSheet;
  Data->NastaveniVahTable->Open();
  Data->NastaveniHejnTable->Open();
  NactiSady();
  VyberPrvniSadu();
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  Data->NastaveniVahTable->Close();
  Data->NastaveniHejnTable->Close();
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::NewButtonClick(TObject *Sender)
{
  NovaSada(true);       // Vcetne default sady pro broilery
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::NewFlockButtonClick(TObject *Sender)
{
  // Zalozi nove hejno
  int Cislo;

  InputForm->Caption        = LoadStr(NASTAVENIVAH_NOVE_HEJNO);
  InputForm->Label->Caption = LoadStr(NASTAVENIVAH_NOVE_HEJNO_ZADEJTE);
  InputForm->Edit->Text     = "";
  if (InputForm->ShowModal() != mrOk) {
    return;
  }
  try {
    Cislo = InputForm->Edit->Text.ToInt();
    if (Cislo < 0 || Cislo >= FLOCKS_MAX_COUNT) {
      throw 1;
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  if (Data->NastaveniHejnTable->Locate("NUMBER", Cislo, Data->SearchOptions)) {
    // Hejno uz existuje
    MessageBox(NULL,LoadStr(NASTAVENIVAH_HEJNO_EXISTUJE).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  // Zalozim nove hejno
  Data->NastaveniHejnTable->Append();
  Data->NastaveniHejnTable->Edit();
  Data->NastaveniHejnTable->FieldByName("SET")->AsString         = Data->NastaveniVahTable->FieldByName("SET")->AsString;
  Data->NastaveniHejnTable->FieldByName("NUMBER")->AsInteger     = Cislo;
  Data->NastaveniHejnTable->FieldByName("USE_GENDER")->AsBoolean = false;
  Data->NastaveniHejnTable->FieldByName("USE_CURVES")->AsBoolean = false;
  Data->NastaveniHejnTable->Post();
  NactiSeznamHejn(Data->NastaveniVahTable->FieldByName("SET")->AsString);
  // Vyberu nove hejno
  SeznamHejnListBox->ItemIndex = SeznamHejnListBox->Items->IndexOf(String(Cislo));
  SeznamHejnListBoxClick(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::SeznamListBoxClick(TObject *Sender)
{
  ZobrazSadu(SeznamListBox->Items->Strings[SeznamListBox->ItemIndex]);
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::SeznamHejnListBoxClick(TObject *Sender)
{
  try {
    ZobrazHejno(SeznamHejnListBox->Items->Strings[SeznamHejnListBox->ItemIndex].ToInt());
  } catch(...) {}  
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::SaveFlockButtonClick(TObject *Sender)
{
  // Ulozi aktualni hejno
  if (SeznamHejnListBox->ItemIndex < 0) {
    return;
  }
  if (!KontrolaRozsahuHejna()) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  SetridKrivky();
  Data->NastaveniHejnTable->Edit();
  Data->NastaveniHejnTable->FieldByName("NAME")->AsString = Data->NastaveniHejnTable->FieldByName("NAME")->AsString.UpperCase(); 
  Data->NastaveniHejnTable->FieldByName("USE_GENDER")->AsBoolean = PohlaviCheckBox->Checked;
  Data->NastaveniHejnTable->FieldByName("USE_CURVES")->AsBoolean = KrivkyCheckBox->Checked;
  if (KrivkyCheckBox->Checked) {
    UlozKrivkuHejna(GENDER_FEMALE);
    if (PohlaviCheckBox->Checked) {
      UlozKrivkuHejna(GENDER_MALE);
    }
  }//if
  Data->NastaveniHejnTable->Post();
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::PohlaviCheckBoxClick(TObject *Sender)
{
  ZobrazKrivkyHejna();
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::KrivkyCheckBoxClick(TObject *Sender)
{
  ZobrazKrivkyHejna();
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::DeleteFlockButtonClick(TObject *Sender)
{
  if (SeznamHejnListBox->ItemIndex < 0) {
    return;
  }
  if (MessageBox(NULL,LoadStr(NASTAVENIVAH_SMAZAT_HEJNO_TEXT).c_str(),LoadStr(NASTAVENIVAH_SMAZAT_HEJNO).c_str(),MB_ICONQUESTION | MB_YESNO | MB_TASKMODAL) == IDNO) {
    return;
  }
  if (Data->NastaveniHejnTable->RecordCount == 0) {
    return;
  }
  Data->NastaveniHejnTable->Delete();
  NactiSeznamHejn(Data->NastaveniVahTable->FieldByName("SET")->AsString);
  if (SeznamHejnListBox->Items->Count > 0) {
    // V sade jsou nejaka hejna, vyberu prvni z nich
    SeznamHejnListBox->ItemIndex = 0;
    SeznamHejnListBoxClick(NULL);
  }
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::DeleteButtonClick(TObject *Sender)
{
  if (SeznamListBox->ItemIndex < 0) {
    return;
  }
  if (MessageBox(NULL,LoadStr(NASTAVENIVAH_SMAZAT_SADU_TEXT).c_str(),LoadStr(NASTAVENIVAH_SMAZAT_SADU).c_str(),MB_ICONQUESTION | MB_YESNO | MB_TASKMODAL) == IDNO) {
    return;
  }
  // Smazu vsechna hejna aktualni sady (filtr by mel byt nastaven)
  Data->NastaveniHejnTable->First();
  while (!Data->NastaveniHejnTable->Eof) {
    Data->NastaveniHejnTable->Delete();
  }
  // Smazu sadu
  Data->NastaveniVahTable->Delete();
  NactiSady();
  VyberPrvniSadu();
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::UploadButtonClick(TObject *Sender)
{
  // Nahraju vybranou sadu do pametoveho modulu
  int Version;                  // Verze vah

  if (SeznamListBox->ItemIndex < 0) {
    return;
  }

  // Kontrola rozsahu
  if (!KontrolaRozsahuSady()) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }
  Data->NastaveniHejnTable->First();
  while (!Data->NastaveniHejnTable->Eof) {
    if (!KontrolaRozsahuHejna()) {
      SeznamHejnListBox->ItemIndex = SeznamHejnListBox->Items->IndexOf(Data->NastaveniHejnTable->FieldByName("NUMBER")->AsString);
      MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
      return;
    }
    Data->NastaveniHejnTable->Next();
  }

  // Zeptam se na verzi vah
  VersionForm->Version110RadioButton->Checked = true;           // Default vzdy 1.10
  if (VersionForm->ShowModal() != mrOk) {
    return;
  }
  if (VersionForm->Version110RadioButton->Checked) {
    Version = VERSION;          // Beru aktualni verzi z Bat2.h, vsechny novejsi verze jsou kompatibilni s 1.10
  } else {
    Version = 0x0109;
  }

  // Ulozim
  UlozSaduDoStruktury(Version);
  UlozHejnaDoStruktury();       // Verze uz je ulozena ve strukture, vezmu ji odtud
  VyberPrvnihejno();            // Vratim se zpet na prvni hejno
  if (!UlozKonfiguraciDoModulu()) {
    MessageBox(NULL,LoadStr(CHYBA).c_str(),Data->Version.ScaleTitle.c_str(),MB_OK | MB_TASKMODAL);
    return;
  }
  MessageBox(NULL,LoadStr(NASTAVENIVAH_UPLOAD_OK).c_str(),Data->Version.ScaleTitle.c_str(),MB_OK | MB_TASKMODAL);
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::Button1Click(TObject *Sender)
{
  // Vyplnim vsechna hejna - testovaci rutina
  Data->NastaveniHejnTable->First();
  while (!Data->NastaveniHejnTable->Eof) {
    Data->NastaveniHejnTable->Delete();
  }

  for (int Hejno = 0; Hejno < FLOCKS_MAX_COUNT; Hejno++) {
    Data->NastaveniHejnTable->Append();
    Data->NastaveniHejnTable->Edit();
    Data->NastaveniHejnTable->FieldByName("SET")->AsString = Data->NastaveniVahTable->FieldByName("SET")->AsString;
    Data->NastaveniHejnTable->FieldByName("NUMBER")->AsInteger = Hejno;
    Data->NastaveniHejnTable->FieldByName("NAME")->AsString = "Flock " + String(Hejno);
    Data->NastaveniHejnTable->FieldByName("USE_CURVES")->AsBoolean = true;
    Data->NastaveniHejnTable->FieldByName("USE_GENDER")->AsBoolean = true;
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      Data->NastaveniHejnTable->FieldByName("FEMALE_DAY" + FormatFloat("00", i))->AsInteger = i + 1;
      Data->NastaveniHejnTable->FieldByName("FEMALE_WEIGHT" + FormatFloat("00", i))->AsFloat = (double)(i + 1) / 10.0;
    }
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      Data->NastaveniHejnTable->FieldByName("MALE_DAY" + FormatFloat("00", i))->AsInteger = 10.0 * (i + 1);
      Data->NastaveniHejnTable->FieldByName("MALE_WEIGHT" + FormatFloat("00", i))->AsFloat = 10.0 * (double)(i + 1) / 10.0;
    }

    Data->NastaveniHejnTable->Post();
  }
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::DownloadButtonClick(TObject *Sender)
{
  if (!NovaSada(false)) {
    return;
  }
  if (!Data->ModulPripraven()) {
    return;
  }
  // Nactu konfiguraci z modulu
  Screen->Cursor = crHourGlass;
  if (!Data->Module->ReadConfigSection()) {
    Screen->Cursor = crDefault;
    return;
  }
  Screen->Cursor = crDefault;

  if (Data->Module->GetConfigVersion() < VERSION_SW_SODALEC_INITIAL) {
    // Modul obsahuje data ze standardni vahy, ta neni kompatibilni s verzi Sodalec
    // Novy set zustane zalozen, jen v nem budou default hodnoty
    MessageBox(NULL,LoadStr(CHYBA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  UlozKonfiguraciDoTabulky();
  UlozHejnaDoTabulky();
  SeznamListBoxClick(NULL);   // Aby zobrazil i hejna sady
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::NastaveniGainCheckBoxClick(
      TObject *Sender)
{
  // Nastavim rucne automaticky mod
  Data->NastaveniVahTable->Edit();
  if (NastaveniGainCheckBox->Checked) {
    Data->NastaveniVahTable->FieldByName("AUTO_MODE")->AsInteger = AUTOMODE_WITH_GAIN;
  } else {
    Data->NastaveniVahTable->FieldByName("AUTO_MODE")->AsInteger = AUTOMODE_WITHOUT_GAIN;
  }
}
//---------------------------------------------------------------------------

void __fastcall TNastaveniVahForm::FemalesLoadButtonClick(TObject *Sender)
{
  // Chce nahrat krivku ze seznamu teoretickych krivek
  TStringGrid  *Grid;
  TCurveList   *CurveList;
  TGrowthCurve *Curve;

  // Nahravani je spolecne pro samice i samce - ulozim si tabulku, do ktere krivku nahraju
  if ((TButton *)Sender == FemalesLoadButton) {
    Grid = KrivkaSamiceStringGrid;
  } else {
    Grid = KrivkaSamciStringGrid;
  }

  // Zeptam se na krivku
  if (CurvesForm->ShowModal() != mrOk) {
    return;             // Dal Cancel
  }
  if (CurvesForm->CurvesListBox->ItemIndex < 0) {
    return;             // V seznamu neni zadna krivka
  }

  if ((CurveList = new TCurveList()) == NULL) {
    return;
  }
  try {
    // Nactu vsechny krivky do pameti
    Data->LoadCurvesFromDatabase(CurveList);

    // Najdu vybranou krivku v seznamu
    if ((Curve = CurveList->GetCurve(CurvesForm->CurvesListBox->ItemIndex)) == NULL) {
      throw 1;          // Nenasel jsem krivku v seznamu (nemelo by nastat)
    }

    // Ulozim vsechny body ze krivky do tabulky
    VymazTabulku(Grid);
    for (int i = 0; i < Curve->Count(); i++) {
      Grid->Cells[1][i + 1] = Curve->GetDay(i);
      Grid->Cells[2][i + 1] = FormatFloat(FORMAT_HMOTNOSTI, Curve->GetWeight(i));
    }

    // Nastavim focus na prvni bunku v tabulce
    Grid->Row = 1;      // Pozice 0 je zahlavi tabulky
    Grid->Col = 1;

  } catch(...) {}

  delete CurveList;
}
//---------------------------------------------------------------------------

