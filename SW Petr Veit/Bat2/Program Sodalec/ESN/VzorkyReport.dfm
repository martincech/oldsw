�
 TVZORKYREPORTFORM 0�   TPF0TVzorkyReportFormVzorkyReportFormLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetData.TiskQueryFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Valuesd�d4dd  PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBand
TitleBand1Left&Top&Width�Height;Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values~�����@c5����v�	@ BandTyperbTitle TQRLabelQRLabel1Left TopWidth^HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values痪���J�@ e�UUUUU�@��TUUU��@ 	AlignmenttaLeftJustifyAlignToBand	AutoSize	AutoStretchCaptionMuestrasColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1LeftmTopWidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@      b�	@e�UUUUU�@�{����R�@ 	AlignmenttaRightJustifyAlignToBand	AutoSize	ColorclWhiteDataqrsDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTextImpreso TransparentFontSize  TQRLabel	QRLabel11Left Top(Width/HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@ 頻����@�VUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionArchivo:ColorclWhiteTransparentWordWrap	FontSize
  TQRLabelSouborQRLabelLeft8Top(Width\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@~����*�@頻����@~����j�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionSouborQRLabelColorclWhiteTransparentWordWrap	FontSize
   TQRBandDetailBand1Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values�/UUUU�@c5����v�	@ BandTyperbDetail 	TQRDBText	QRDBText1Left Top WidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@         �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.TiskQuery	DataField
DAY_NUMBERFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText2LeftjTop WidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@�{����:�@        �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.TiskQuery	DataField	TIME_HOURFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText3Left� Top WidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@ª���:�@        �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.TiskQuery	DataFieldWEIGHTFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Mask0.000
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText8LeftATop WidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@      T�@        �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.TiskQuery	DataFieldGENDERFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OnPrintQRDBText8Print
ParentFontTransparentWordWrap	FontSize   TQRBandPageHeaderBand1Left&TopaWidth�Height2Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values~����J�@c5����v�	@ BandTyperbColumnHeader TQRLabelQRLabel2Left TopWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottom	Frame.DrawLeftFrame.DrawRightSize.Values      ��@ 2Ъ����@       �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaptionD�aColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	FontSize  TQRLabelQRLabel3LeftjTopWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottom	Frame.DrawLeftFrame.DrawRightSize.Values      ��@�{����:�@2Ъ����@       �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaptionHoraColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	FontSize  TQRLabelQRLabel4Left� TopWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottom	Frame.DrawLeftFrame.DrawRightSize.Values      ��@ª���:�@2Ъ����@       �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaptionPesoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	FontSize  TQRLabelQRLabel5LeftATopWidthZHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottom	Frame.DrawLeftFrame.DrawRightSize.Values      ��@      T�@2Ъ����@       �@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaptionG�neroColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	FontSize   TQRBandPageFooterBand1Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValueshUUUU��@c5����v�	@ BandTyperbPageFooter 
TQRSysData
QRSysData2LeftFTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@      |�	@       �@�{�����@ 	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTextP�gina TransparentFontSize   TQRBandSummaryBand1Left&Top� Width�Height+Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values頻����@c5����v�	@ BandType	rbSummary TQRLabelQRLabel8Left TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@ 2Ъ�����@痪���
�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTotal:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRExprQRExpr1LeftPTopWidthwHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUUU�@c5������	@2Ъ�����@?�UUUUm�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
ExpressionCOUNTFontSize    