//***************************************************************************
//
//   Convert.cpp      Conversions from data structures to tables
//   Version 1.0
//
//***************************************************************************

#include "DM.h"
#include "CheckId.h"

// --------------------------------------------------------------------------
// Lokalni funkce
// --------------------------------------------------------------------------

static bool KonvertujKonfiguraci();
  // Konvertuje konfiguraci do tabulky

static bool KonvertujHejna();
  // Konvertuje hejna do tabulky

static bool KonvertujKalibraci();
  // Konvertuje kalibraci do tabulky

static int NajdiPocatecniHodinuLoggeru();
  // Stanovi hodinu prvniho ulozeneho vzorku dne

static void SmazArchiv();
  // Smaze tabulky archivu

static bool KonvertujArchiv();
  // Konvertuje archiv do tabulky

static int NajdiPocatecniHodinuOnline();
  // Stanovi hodinu prvniho ulozeneho online zaznamu

static void SmazOnline();
  // smaze online zaznamy z tabulky

static bool KonvertujOnline();
  // Konvertuje online zaznamy do tabulky

static void ConvertConfig110();
  // Zkonvertuje strukturu TConfig verze 1.10 a vyssi

static void ConvertFlocks(TFlock *Flock);
  // Prevede do tabulky hejna <Flock>

static void ConvertCalibration110();
  // Zkonvertuje kalibraci verze 1.10 a vyssi

// --------------------------------------------------------------------------
// Kompletni konverze vsech dat
// --------------------------------------------------------------------------

bool KonvertujData() {
  // Projde nactena data z modulu a rozdeli je do tabulek
  if (!KonvertujKonfiguraci()) {
    return false;
  }
  if (!KonvertujHejna()) {
    return false;
  }
  if (!KonvertujKalibraci()) {
    return false;
  }
  if (Data->KonfiguraceTable->FieldByName("START_ONLINE")->AsBoolean) {
    if (!KonvertujOnline()) {
      return false;
    }
    SmazArchiv();       // Smazu obsah archivu, at zbytecne nezabira misto
  } else {
    if (!KonvertujArchiv()) {
      return false;
    }
    SmazOnline();      // Smazu obsah online, at zbytecne nezabira misto
  }
  return true;
}

// --------------------------------------------------------------------------
// Konverze konfigurace z nactenych dat do tabulky
// --------------------------------------------------------------------------

static bool KonvertujKonfiguraci() {
  // Konvertuje konfiguraci do tabulky
  try {
    Data->KonfiguraceTable->DisableControls();
    Data->KonfiguraceTable->Close();
    Data->KonfiguraceTable->EmptyTable();
    Data->KonfiguraceTable->Open();
    Data->KonfiguraceTable->Append();
    Data->KonfiguraceTable->Edit();
    ConvertConfig110();
    Data->KonfiguraceTable->Post();
  } catch(...) {
    Data->KonfiguraceTable->EnableControls();
    return false;
  }
  Data->KonfiguraceTable->EnableControls();
  return true;
}

// --------------------------------------------------------------------------
// Konverze hejn z nactenych dat do tabulky
// --------------------------------------------------------------------------

static bool KonvertujHejna() {
  // Konvertuje hejna do tabulky
  try {
    Data->HejnaTable->DisableControls();
    Data->HejnaTable->Close();
    Data->HejnaTable->EmptyTable();
    Data->HejnaTable->Open();
    ConvertFlocks(Data->Module->ConfigSection->Flocks);
  } catch(...) {
    Data->HejnaTable->EnableControls();
    return false;
  }
  Data->HejnaTable->EnableControls();
  return true;
}

// --------------------------------------------------------------------------
// Konverze kalibrace z nactenych dat do tabulky
// --------------------------------------------------------------------------

static bool KonvertujKalibraci() {
  // Konvertuje kalibraci do tabulky
  try {
    Data->KalibraceTable->DisableControls();
    Data->KalibraceTable->Close();
    Data->KalibraceTable->EmptyTable();
    Data->KalibraceTable->Open();
    Data->KalibraceTable->Append();
    Data->KalibraceTable->Edit();
    Data->KalibraceTable->FieldByName("PLATFORM")->AsInteger = 0;       // Beru natvrdo 1. plosinu i pro verze 1.09
    ConvertCalibration110();
    Data->KalibraceTable->Post();
  } catch(...) {
    Data->KalibraceTable->EnableControls();
    return false;
  }
  return true;
}

// --------------------------------------------------------------------------
// Konverze archivu
// --------------------------------------------------------------------------

static int NajdiPocatecniHodinuLoggeru() {
  // Stanovi hodinu prvniho ulozeneho vzorku dne
  TLoggerSample *Sample;
  int Hodina = 0;
  bool NalezenaHodina = false;    // Flag, ze jsem jiz nacetl zaznam hodiny
  bool NalezenaHmotnost = false;  // Flag, ze jsem jiz nacetl zaznam hmotnosti

  try {
    // Projizdim jednotlive vzorky dne
    Data->Module->LoggerFirstSample();
    while(1) {
      Sample = Data->Module->LoggerNextSample();
      if (!Sample) {
        break;
      }
      if (((Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger >= 0x0153)
            && Sample->Flag != LG_SAMPLE_HOUR) || 
            ((Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger < 0x0153)
            && Sample->Flag & LG_SAMPLE_VALUE)) {
        // Jde o zaznam hmotnosti
        if (NalezenaHodina) {
          // Pred hmotnosti jsem uz nacetl hodinu
          return Hodina;
        }
        NalezenaHmotnost = true;
      } else {
        // Jde o zaznam nove hodiny
        Hodina = (byte)(KConvertGetWord(Sample->Value) & LG_SAMPLE_MASK_HOUR);
        if (NalezenaHmotnost) {
          // Pred touto hodinou uz byly nejake hmotnosti, u kterych jsem zatim neurcil hodinu
          if (Hodina > 0) {
            return Hodina - 1;
          } else {
            // Toto by ani nemelo nastat
            return 0;
          }
        }
        NalezenaHodina = true;
      }//else
    }//while
  } catch(...) {}
  // Pokud dosel az sem, neobsahuje bude jsem nenalezl zadnou hmotnost, nebo zadnou hodinu
  if (NalezenaHodina) {
    // Logger neobsahuje zadnou hmotnost
    return Hodina;
  } else if (NalezenaHmotnost) {
    // Logger neobsahuje zadnou hodinu
    return 0;
  } else {
    // Loger neobsahuje nic
    return 0;
  }
}

static void SmazArchiv() {
  // Smaze tabulky archivu
  Data->StatistikaTable->Close();
  Data->StatistikaTable->EmptyTable();
  Data->ZaznamyTable->Close();
  Data->ZaznamyTable->EmptyTable();
}

static bool KonvertujArchiv() {
  // Konvertuje archiv do tabulky
  TArchive *Archiv;
  TLoggerSample *Sample;
  TDateTime ZacatekVykrmu;    // Datum pocatku vykrmu
  TDateTime CisloDne;         // Cislo dne vykrmu
  int Hodina;                 // Hodina aktualniho vzorku
  double Hmotnost;

  try {
    ZacatekVykrmu = Data->KonfiguraceTable->FieldByName("START_DATE")->AsDateTime;

    // Otevru tabulku statistiky
    Data->StatistikaTable->DisableControls();
    Data->StatistikaTable->Close();
    Data->StatistikaTable->EmptyTable();
    Data->StatistikaTable->Open();
    // Otevru tabulku loggeru
    Data->ZaznamyTable->DisableControls();
    Data->ZaznamyTable->Close();
    Data->ZaznamyTable->EmptyTable();
    Data->ZaznamyTable->Open();
    // Cyklim jednotlive dny
    Data->Module->ArchivFirst();

    bool IsFirstFound = false;
    int CorruptCount = 0;       // Jen pro debug

    while (1) {
      Archiv = Data->Module->ArchivNext();
      if (!Archiv) {
        // Narazil jsem na konec FIFO

        if (IsFirstFound) {
          // Prvni den jsem nasel a projel jsem cely archiv az do konce => koncim
          break;
        }

        // Po projiti celeho archivu jsem nenasel prvni den vazeni, zrejme je v archivu vice dnu nez je jeho kapacita
        // a dny uz roluji. Dalsi moznost je, ze je prvni den v archivu poskozen a proto ho nenajdu. V tom pripade se stejne
        // pokusim archiv dekodovat od prvni polozky.
        // Nenasel jsem cislo prvniho dne vazeni
        if (CorruptCount == 0) {
          // Archiv je zcela prazdny
          break;
        }
        // V archivu prvni den vazeni chybi, zrejme se FIFO roluje. Projedu archiv jeste jednou od zacatku s tim, jakobych
        // uz prvni den nasel
        Data->Module->ArchivFirst();
        IsFirstFound = true;
        continue;

      }

      // Odrolovani korupce dat. Nekdy se stava, ze se marker ve Fifo zmrsi z 0xFE na 0xFF, takze se Fifo interpretuje jako prepisovane. Pri prochazeni
      // archivu se tak zda, ze dny jsou ve Fifo posunute az na konec a na zacatku jsou nesmysly. Pri dekodovani dat z pametoveho modulu
      // tak staci v archivu najit prvni den a pokracovat az od nej. Ve vaze jsou sice data ponicena, ale pokusim se je v PC aspon rekonstruovat.
      if (!IsFirstFound) {
        if (KConvertGetWord(Archiv->DayNumber) != Data->KonfiguraceTable->FieldByName("START_CURVE_MOVE")->AsInteger) {
          CorruptCount++;
          continue;
        }
        IsFirstFound = true;
      }

      // Statistika
      Data->StatistikaTable->Append();
      Data->StatistikaTable->Edit();
      CisloDne = KConvertGetWord(Archiv->DayNumber);   // Vyuziva se vicekrat
      Data->StatistikaTable->FieldByName("DAY_NUMBER")->AsInteger = CisloDne;
      Data->StatistikaTable->FieldByName("DATE_TIME")->AsDateTime = KConvertGetDateTime(Archiv->DateTime);
      Data->StatistikaTable->FieldByName("FEMALE_STAT_XSUM")->AsFloat = KConvertGetFloat(Archiv->Stat[GENDER_FEMALE].XSuma);
      Data->StatistikaTable->FieldByName("FEMALE_STAT_X2SUM")->AsFloat = KConvertGetFloat(Archiv->Stat[GENDER_FEMALE].X2Suma);
      Data->StatistikaTable->FieldByName("FEMALE_STAT_COUNT")->AsInteger = KConvertGetWord(Archiv->Stat[GENDER_FEMALE].Count);
      Data->StatistikaTable->FieldByName("MALE_STAT_XSUM")->AsFloat = KConvertGetFloat(Archiv->Stat[GENDER_MALE].XSuma);
      Data->StatistikaTable->FieldByName("MALE_STAT_X2SUM")->AsFloat = KConvertGetFloat(Archiv->Stat[GENDER_MALE].X2Suma);
      Data->StatistikaTable->FieldByName("MALE_STAT_COUNT")->AsInteger = KConvertGetWord(Archiv->Stat[GENDER_MALE].Count);
      for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
        Data->StatistikaTable->FieldByName("FEMALE_HIST_VALUE" + FormatFloat("00", i))->AsFloat = KConvertGetLong(Archiv->Hist[GENDER_FEMALE].Center) + (i - HISTOGRAM_SLOTS / 2) * KConvertGetLong(Archiv->Hist[GENDER_FEMALE].Step);
        Data->StatistikaTable->FieldByName("FEMALE_HIST_COUNT" + FormatFloat("00", i))->AsInteger = KConvertGetWord(Archiv->Hist[GENDER_FEMALE].Slot[i]);
      }//for
      for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
        Data->StatistikaTable->FieldByName("MALE_HIST_VALUE" + FormatFloat("00", i))->AsFloat = KConvertGetLong(Archiv->Hist[GENDER_MALE].Center) + (i - HISTOGRAM_SLOTS / 2) * KConvertGetLong(Archiv->Hist[GENDER_MALE].Step);
        Data->StatistikaTable->FieldByName("MALE_HIST_COUNT" + FormatFloat("00", i))->AsInteger = KConvertGetWord(Archiv->Hist[GENDER_MALE].Slot[i]);
      }//for
      Data->StatistikaTable->FieldByName("FEMALE_TARGET")->AsFloat = PrepoctiNaKg(KConvertGetWord(Archiv->TargetWeight[GENDER_FEMALE]));
      Data->StatistikaTable->FieldByName("MALE_TARGET")->AsFloat = PrepoctiNaKg(KConvertGetWord(Archiv->TargetWeight[GENDER_MALE]));
      Data->StatistikaTable->FieldByName("FEMALE_YESTERDAY_AVERAGE")->AsFloat = PrepoctiNaKg(KConvertGetWord(Archiv->LastAverage[GENDER_FEMALE]));
      Data->StatistikaTable->FieldByName("MALE_YESTERDAY_AVERAGE")->AsFloat = PrepoctiNaKg(KConvertGetWord(Archiv->LastAverage[GENDER_MALE]));
      Data->StatistikaTable->FieldByName("USE_REAL_UNI")->AsBoolean = Archiv->RealUniformityUsed;
      Data->StatistikaTable->FieldByName("FEMALE_REAL_UNI")->AsInteger = Archiv->RealUniformity[GENDER_FEMALE];
      Data->StatistikaTable->FieldByName("MALE_REAL_UNI")->AsInteger = Archiv->RealUniformity[GENDER_MALE];
      Data->StatistikaTable->Post();

      // Logger za tento den
      Hodina = NajdiPocatecniHodinuLoggeru();   // Nactu pocatecni hodinu
      Data->Module->LoggerFirstSample();
      while(1) {
        Sample = Data->Module->LoggerNextSample();
        if (!Sample) {
          break;
        }
        if (((Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger >= 0x0153)
            && Sample->Flag != LG_SAMPLE_HOUR) || 
            ((Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger < 0x0153)
            && Sample->Flag & LG_SAMPLE_VALUE)){
          // Jde o zaznam hmotnosti, pridam zaznam do tabulky
          Data->ZaznamyTable->Append();
          Data->ZaznamyTable->Edit();
          Data->ZaznamyTable->FieldByName("DAY_NUMBER")->AsInteger = CisloDne;
          Data->ZaznamyTable->FieldByName("TIME_HOUR")->AsInteger = Hodina;
          Hmotnost = PrepoctiNaKg(KConvertGetWord((word)(Sample->Value) & LG_SAMPLE_MASK_WEIGHT));
          if (Sample->Flag & LG_SAMPLE_WEIGHT_MSB) {
            Hmotnost += PrepoctiNaKg(0x10000);
          }
          Data->ZaznamyTable->FieldByName("WEIGHT")->AsFloat = Hmotnost;
          if(Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger >= 0x0153){
            Data->ZaznamyTable->FieldByName("GENDER")->AsBoolean = (bool)(Sample->Flag & LG_SAMPLE_GENDER);
          }else{
             Data->ZaznamyTable->FieldByName("GENDER")->AsBoolean = (bool)(Sample->Flag & LG_SAMPLE_GENDER_PRE153);
          }
          Data->ZaznamyTable->Post();
        } else {
          // Jde o zaznam nove hodiny
          Hodina = (byte)(KConvertGetWord(Sample->Value) & LG_SAMPLE_MASK_HOUR);
        }//else
      }//while
    }//while
    Data->StatistikaTable->EnableControls();
    Data->ZaznamyTable->EnableControls();
  } catch(...) {
    Data->StatistikaTable->EnableControls();
    Data->ZaznamyTable->EnableControls();
    return false;
  }
  return true;
}

// --------------------------------------------------------------------------
// Konverze online vazeni
// --------------------------------------------------------------------------

static int NajdiPocatecniHodinuOnline() {
  // Stanovi hodinu prvniho ulozeneho online zaznamu
  TOnlineSample *Sample;
  int Hodina = 0;
  bool NalezenaHodina = false;    // Flag, ze jsem jiz nacetl zaznam hodiny
  bool NalezenaHmotnost = false;  // Flag, ze jsem jiz nacetl zaznam hmotnosti

  try {
    // Projizdim jednotlive vzorky dne
    Data->Module->OnlineFirst();
    while(1) {
      Sample = Data->Module->OnlineNext();
      if (!Sample) {
        break;
      }
      if (Sample->Flag & ON_SAMPLE_WEIGHT) {
        // Jde o zaznam hmotnosti
        if (NalezenaHodina) {
          // Pred hmotnosti jsem uz nacetl hodinu
          return Hodina;
        }
        NalezenaHmotnost = true;
      } else {
        // Jde o zaznam nove hodiny
        Hodina = (byte)(KConvertGetWord(Sample->Value) & ON_SAMPLE_MASK_HOUR);
        if (NalezenaHmotnost) {
          // Pred touto hodinou uz byly nejake hmotnosti, u kterych jsem zatim neurcil hodinu
          if (Hodina > 0) {
            return Hodina - 1;
          } else {
            // Toto by ani nemelo nastat
            return 0;
          }
        }
        NalezenaHodina = true;
      }//else
    }//while
  } catch(...) {}
  // Pokud dosel az sem, neobsahuje bude jsem nenalezl zadnou hmotnost, nebo zadnou hodinu
  if (NalezenaHodina) {
    // Logger neobsahuje zadnou hmotnost
    return Hodina;
  } else if (NalezenaHmotnost) {
    // Logger neobsahuje zadnou hodinu
    return 0;
  } else {
    // Loger neobsahuje nic
    return 0;
  }
}

static void SmazOnline() {
  // smaze online zaznamy z tabulky
  Data->OnlineTable->Close();
  Data->OnlineTable->EmptyTable();
}

static bool KonvertujOnline() {
  // Konvertuje online zaznamy do tabulky
  TOnlineSample *Sample;
  TDateTime ZacatekVykrmu;    // Datum pocatku vykrmu
  TDateTime CisloDne;         // Cislo dne vykrmu
  int Hodina;                 // Hodina aktualniho vzorku
  int PredchoziHodina;        // Hodina predchoziho vzorku
  double Hmotnost;

  try {
    ZacatekVykrmu = Data->KonfiguraceTable->FieldByName("START_DATE")->AsDateTime;

    // Otevru tabulku online zaznamu
    Data->OnlineTable->DisableControls();
    Data->OnlineTable->Close();
    Data->OnlineTable->EmptyTable();
    Data->OnlineTable->Open();

    // Cyklim jednotlive zaznamy
    CisloDne = Data->KonfiguraceTable->FieldByName("START_CURVE_MOVE")->AsInteger + CURVE_FIRST_DAY_NUMBER;
    Hodina = NajdiPocatecniHodinuOnline();   // Nactu pocatecni hodinu
    PredchoziHodina = Hodina;
    Data->Module->OnlineFirst();
    while(1) {
      Sample = Data->Module->OnlineNext();
      if (!Sample) {
        break;
      }
      if (Sample->Flag & ON_SAMPLE_WEIGHT) {
        // Jde o zaznam hmotnosti, pridam zaznam do tabulky
        Data->OnlineTable->Append();
        Data->OnlineTable->Edit();
        Data->OnlineTable->FieldByName("DAY_NUMBER")->AsInteger = CisloDne;
        Data->OnlineTable->FieldByName("TIME_HOUR")->AsInteger = Hodina;
        Hmotnost =  PrepoctiNaKg(KConvertGetWord(Sample->Value));
        Hmotnost += PrepoctiNaKg((int)((Sample->Flag & ON_SAMPLE_WEIGHT_MSB) << 16));  // Msb bity hmotnosti
        if (Sample->Flag & ON_SAMPLE_SIGN) {
          Hmotnost = -Hmotnost;   // Znamenko
        }
        Data->OnlineTable->FieldByName("WEIGHT")->AsFloat   = Hmotnost;
        Data->OnlineTable->FieldByName("SAVED")->AsBoolean  = (bool)(Sample->Flag & ON_SAMPLE_SAVED);
        Data->OnlineTable->FieldByName("STABLE")->AsBoolean = (bool)(Sample->Flag & ON_SAMPLE_STABLE);
        Data->OnlineTable->Post();
      } else {
        // Jde o zaznam nove hodiny
        Hodina = (byte)(KConvertGetWord(Sample->Value) & ON_SAMPLE_MASK_HOUR);
        if (Hodina < PredchoziHodina) {
          CisloDne++;   // Prechod na dalsi den (z 23->0). Pokud by se preslo rucne v debug modu, musi byt hodina noveho dne mensi nez konecna hodina predchoziho dne
        }
        PredchoziHodina = Hodina;
      }//else
    }//while
    Data->OnlineTable->EnableControls();
  } catch(...) {
    Data->OnlineTable->EnableControls();
    return false;
  }
  return true;
}

// --------------------------------------------------------------------------
// Konverze konfigurace verze 1.10
// --------------------------------------------------------------------------

static void ConvertConfig110() {
  // Zkonvertuje strukturu TConfig verze 1.10 a vyssi
  // TKonfigurace
  Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger         = KConvertGetWord(Data->Module->ConfigSection->Config.Version);
  Data->KonfiguraceTable->FieldByName("BUILD")->AsInteger              = Data->Module->ConfigSection->Config.Build;
  Data->KonfiguraceTable->FieldByName("HW_VERSION")->AsInteger         = Data->Module->ConfigSection->Config.HwVersion;
  Data->KonfiguraceTable->FieldByName("ID")->AsString                  = GetId(Data->Module->ConfigSection->Config.IdentificationNumber);
  Data->KonfiguraceTable->FieldByName("LANGUAGE")->AsInteger           = Data->Module->ConfigSection->Config.Language;
  Data->KonfiguraceTable->FieldByName("F_MARGIN_ABOVE")->AsInteger     = Data->Module->ConfigSection->Config.MarginAbove[GENDER_FEMALE];
  Data->KonfiguraceTable->FieldByName("F_MARGIN_UNDER")->AsInteger     = Data->Module->ConfigSection->Config.MarginBelow[GENDER_FEMALE];
  Data->KonfiguraceTable->FieldByName("M_MARGIN_ABOVE")->AsInteger     = Data->Module->ConfigSection->Config.MarginAbove[GENDER_MALE];
  Data->KonfiguraceTable->FieldByName("M_MARGIN_UNDER")->AsInteger     = Data->Module->ConfigSection->Config.MarginBelow[GENDER_MALE];
  Data->KonfiguraceTable->FieldByName("FILTER_VALUE")->AsInteger       = Data->Module->ConfigSection->Config.Filter;
  Data->KonfiguraceTable->FieldByName("STABILIZATION")->AsFloat        = (double)Data->Module->ConfigSection->Config.StabilizationRange / 10.0;
  Data->KonfiguraceTable->FieldByName("STABILIZATION_TIME")->AsInteger = Data->Module->ConfigSection->Config.StabilizationTime;
  Data->KonfiguraceTable->FieldByName("AUTO_MODE")->AsInteger          = Data->Module->ConfigSection->Config.AutoMode;
  Data->KonfiguraceTable->FieldByName("JUMP_MODE")->AsInteger          = Data->Module->ConfigSection->Config.JumpMode;
  Data->KonfiguraceTable->FieldByName("UNITS")->AsInteger              = Data->Module->ConfigSection->Config.Units;
  Data->KonfiguraceTable->FieldByName("HIST_RANGE")->AsInteger         = Data->Module->ConfigSection->Config.HistogramRange;
  Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger          = Data->Module->ConfigSection->Config.UniformityRange;
  // TStartVykrmu
  Data->KonfiguraceTable->FieldByName("START_PROGRESS")->AsBoolean   = Data->Module->ConfigSection->Config.WeighingStart.Running;
  Data->KonfiguraceTable->FieldByName("START_WAITING")->AsBoolean    = Data->Module->ConfigSection->Config.WeighingStart.WaitingForStart;    // 21.9.2005
  Data->KonfiguraceTable->FieldByName("START_USE_FLOCK")->AsBoolean  = Data->Module->ConfigSection->Config.WeighingStart.UseFlock;
  Data->KonfiguraceTable->FieldByName("START_FLOCK")->AsInteger      = Data->Module->ConfigSection->Config.WeighingStart.CurrentFlock;
  Data->KonfiguraceTable->FieldByName("START_CURVE_MOVE")->AsInteger = KConvertGetWord(Data->Module->ConfigSection->Config.WeighingStart.CurveDayShift);
  try {
    // 3.2.2005: Osetreno spatne datum zahajeni vykrmu - stalo se, ze se to pokurvilo statickou elektrinou a pak nesel
    // modul vubec stahnout.
    Data->KonfiguraceTable->FieldByName("START_DATE")->AsDateTime      = TDateTime(KConvertGetWord(Data->Module->ConfigSection->Config.WeighingStart.DateTime.Year),
                                                                                   Data->Module->ConfigSection->Config.WeighingStart.DateTime.Month,
                                                                                   Data->Module->ConfigSection->Config.WeighingStart.DateTime.Day)
                                                                       + TDateTime(Data->Module->ConfigSection->Config.WeighingStart.DateTime.Hour,
                                                                                   Data->Module->ConfigSection->Config.WeighingStart.DateTime.Min,
                                                                                   0,
                                                                                   0);
  } catch (...) {
    Data->KonfiguraceTable->FieldByName("START_DATE")->AsDateTime = Now();
  }
  Data->KonfiguraceTable->FieldByName("START_FAST_USE_GENDER")->AsBoolean  = Data->Module->ConfigSection->Config.WeighingStart.QuickWeighing.UseBothGenders;
  Data->KonfiguraceTable->FieldByName("START_FAST_INIT_WEIGHT_F")->AsFloat = PrepoctiNaKg(KConvertGetWord(Data->Module->ConfigSection->Config.WeighingStart.QuickWeighing.InitialWeight[GENDER_FEMALE]));
  Data->KonfiguraceTable->FieldByName("START_FAST_INIT_WEIGHT_M")->AsFloat = PrepoctiNaKg(KConvertGetWord(Data->Module->ConfigSection->Config.WeighingStart.QuickWeighing.InitialWeight[GENDER_MALE]));
  Data->KonfiguraceTable->FieldByName("START_UNI_RANGE")->AsInteger        = Data->Module->ConfigSection->Config.WeighingStart.UniformityRange;
  Data->KonfiguraceTable->FieldByName("START_ONLINE")->AsBoolean           = Data->Module->ConfigSection->Config.WeighingStart.Online;
  // TGsm
  Data->KonfiguraceTable->FieldByName("GSM_USE")->AsBoolean            = Data->Module->ConfigSection->Config.Gsm.Use;
  Data->KonfiguraceTable->FieldByName("GSM_SEND_MIDNIGHT")->AsBoolean  = Data->Module->ConfigSection->Config.Gsm.SendMidnight;
  Data->KonfiguraceTable->FieldByName("GSM_PERIOD_MIDNIGHT1")->AsInteger = Data->Module->ConfigSection->Config.Gsm.PeriodMidnight1;
  Data->KonfiguraceTable->FieldByName("GSM_DAY_MIDNIGHT1")->AsInteger  = KConvertGetWord(Data->Module->ConfigSection->Config.Gsm.DayMidnight1);
  Data->KonfiguraceTable->FieldByName("GSM_PERIOD_MIDNIGHT2")->AsInteger = Data->Module->ConfigSection->Config.Gsm.PeriodMidnight2;
  Data->KonfiguraceTable->FieldByName("GSM_MIDNIGHT_SEND_HOUR")->AsInteger = Data->Module->ConfigSection->Config.Gsm.MidnightSendHour;
  Data->KonfiguraceTable->FieldByName("GSM_MIDNIGHT_SEND_MIN")->AsInteger = Data->Module->ConfigSection->Config.Gsm.MidnightSendMin;
  Data->KonfiguraceTable->FieldByName("GSM_SEND_REQUEST")->AsBoolean   = Data->Module->ConfigSection->Config.Gsm.SendOnRequest;
  Data->KonfiguraceTable->FieldByName("GSM_EXEC_REQUEST")->AsBoolean   = false;
  Data->KonfiguraceTable->FieldByName("GSM_CHECK_NUMBERS")->AsBoolean  = Data->Module->ConfigSection->Config.Gsm.CheckNumbers;
  Data->KonfiguraceTable->FieldByName("GSM_COUNT")->AsInteger          = Data->Module->ConfigSection->Config.Gsm.NumberCount;
  for (int i = 0; i < GSM_NUMBER_MAX_COUNT; i++) {
    Data->KonfiguraceTable->FieldByName("GSM_NUMBER" + FormatFloat("00", i))->AsString = KConvertGetString(Data->Module->ConfigSection->Config.Gsm.Numbers[i], GSM_NUMBER_MAX_LENGTH);
  }
  // Podsvit
  Data->KonfiguraceTable->FieldByName("BACKLIGHT")->AsInteger          = Data->Module->ConfigSection->Config.Backlight;
  // RS-485
  Data->KonfiguraceTable->FieldByName("RS485_ADDRESS")->AsInteger         = Data->Module->ConfigSection->Config.Rs485.Address;
  Data->KonfiguraceTable->FieldByName("RS485_SPEED")->AsInteger           = KConvertGetDword(Data->Module->ConfigSection->Config.Rs485.Speed);
  Data->KonfiguraceTable->FieldByName("RS485_PARITY")->AsInteger          = Data->Module->ConfigSection->Config.Rs485.Parity;
  Data->KonfiguraceTable->FieldByName("RS485_REPLY_DELAY")->AsInteger     = KConvertGetWord(Data->Module->ConfigSection->Config.Rs485.ReplyDelay);
  Data->KonfiguraceTable->FieldByName("RS485_SILENT_INTERVAL")->AsInteger = Data->Module->ConfigSection->Config.Rs485.SilentInterval;
  Data->KonfiguraceTable->FieldByName("RS485_PROTOCOL")->AsInteger        = Data->Module->ConfigSection->Config.Rs485.Protocol;
  // Porovnani
  Data->KonfiguraceTable->FieldByName("COMPARE")->AsInteger = Data->Module->ConfigSection->Config.ComparisonFlock;

  // Kompenzacni krivka
  Data->KonfiguraceTable->FieldByName("CORRECTION_DAY1")->AsInteger     = KConvertGetWord(Data->Module->ConfigSection->Config.WeightCorrection.Day1);
  Data->KonfiguraceTable->FieldByName("CORRECTION_DAY2")->AsInteger     = KConvertGetWord(Data->Module->ConfigSection->Config.WeightCorrection.Day2);
  Data->KonfiguraceTable->FieldByName("CORRECTION_CORRECTION")->AsFloat = (double)Data->Module->ConfigSection->Config.WeightCorrection.Correction / 10.0;   // Bajt na desetiny
} // ConvertConfig110

// --------------------------------------------------------------------------
// Konverze hejn
// --------------------------------------------------------------------------

static void ConvertFlocks(TFlock *Flock) {
  // Prevede do tabulky hejna <Flock>
  for (int Hejno = 0; Hejno < FLOCKS_MAX_COUNT; Hejno++) {
    Data->HejnaTable->Append();
    Data->HejnaTable->Edit();
    Data->HejnaTable->FieldByName("NUMBER")->AsInteger     = Flock->Header.Number;
    Data->HejnaTable->FieldByName("NAME")->AsString        = KConvertGetString(Flock->Header.Title, FLOCK_NAME_MAX_LENGTH);
    Data->HejnaTable->FieldByName("USE_CURVES")->AsBoolean = Flock->Header.UseCurves;
    Data->HejnaTable->FieldByName("USE_GENDER")->AsBoolean = Flock->Header.UseBothGenders;
    if (Flock->Header.WeighFrom != FLOCK_TIME_LIMIT_EMPTY) {
      Data->HejnaTable->FieldByName("WEIGH_FROM")->AsInteger = Flock->Header.WeighFrom;
    }
    if (Flock->Header.WeighTill != FLOCK_TIME_LIMIT_EMPTY) {
      Data->HejnaTable->FieldByName("WEIGH_TO")->AsInteger = Flock->Header.WeighTill;
    }
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      Data->HejnaTable->FieldByName("FEMALE_DAY"    + FormatFloat("00", i))->AsInteger = KConvertGetWord(Flock->GrowthCurve[GENDER_FEMALE][i].Day);
      Data->HejnaTable->FieldByName("FEMALE_WEIGHT" + FormatFloat("00", i))->AsFloat   = PrepoctiNaKg(KConvertGetWord(Flock->GrowthCurve[GENDER_FEMALE][i].Weight));
    }
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      Data->HejnaTable->FieldByName("MALE_DAY"      + FormatFloat("00", i))->AsInteger = KConvertGetWord(Flock->GrowthCurve[GENDER_MALE][i].Day);
      Data->HejnaTable->FieldByName("MALE_WEIGHT"   + FormatFloat("00", i))->AsFloat   = PrepoctiNaKg(KConvertGetWord(Flock->GrowthCurve[GENDER_MALE][i].Weight));
    }
    Data->HejnaTable->Post();
    Flock++;            // Jdu na dalsi hejno
  }//for
}

// --------------------------------------------------------------------------
// Konverze kalibrace verze 1.10
// --------------------------------------------------------------------------

static void ConvertCalibration110() {
  // Zkonvertuje kalibraci verze 1.10 a vyssi
  Data->KalibraceTable->FieldByName("CAL_ZERO")->AsInteger  = KConvertGetLong(Data->Module->ConfigSection->Calibration.ZeroCalibration);
  Data->KalibraceTable->FieldByName("CAL_RANGE")->AsInteger = KConvertGetLong(Data->Module->ConfigSection->Calibration.RangeCalibration);
  Data->KalibraceTable->FieldByName("RANGE")->AsInteger     = KConvertGetLong(Data->Module->ConfigSection->Calibration.Range);
  Data->KalibraceTable->FieldByName("DIVISION")->AsInteger  = Data->Module->ConfigSection->Calibration.Division;
}

