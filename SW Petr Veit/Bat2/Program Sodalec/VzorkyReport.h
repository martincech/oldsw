//----------------------------------------------------------------------------
#ifndef VzorkyReportH
#define VzorkyReportH
//----------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\QuickRpt.hpp>
#include <vcl\QRCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Qrctrls.hpp>
//----------------------------------------------------------------------------
class TVzorkyReportForm : public TQuickRep
{
__published:
        TQRBand *TitleBand1;
        TQRLabel *QRLabel1;
        TQRSysData *QRSysData1;
        TQRLabel *QRLabel11;
        TQRLabel *SouborQRLabel;
        TQRBand *DetailBand1;
        TQRDBText *QRDBText1;
        TQRDBText *QRDBText2;
        TQRDBText *QRDBText3;
        TQRDBText *QRDBText8;
        TQRBand *PageHeaderBand1;
        TQRLabel *QRLabel2;
        TQRLabel *QRLabel3;
        TQRLabel *QRLabel4;
        TQRLabel *QRLabel5;
        TQRBand *PageFooterBand1;
        TQRSysData *QRSysData2;
        TQRBand *SummaryBand1;
        TQRLabel *QRLabel8;
        TQRExpr *QRExpr1;
        void __fastcall QRDBText8Print(TObject *sender, AnsiString &Value);
private:
public:
   __fastcall TVzorkyReportForm::TVzorkyReportForm(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern TVzorkyReportForm *VzorkyReportForm;
//----------------------------------------------------------------------------
#endif