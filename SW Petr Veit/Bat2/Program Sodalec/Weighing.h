//---------------------------------------------------------------------------

#ifndef WeighingH
#define WeighingH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Buttons.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>


#include <qrctrls.hpp>
#include <ImgList.hpp>
#include <ToolWin.hpp>
#include "GSM.h"


typedef enum {
  MODE_IDLE,            // Nic se nedeje
  MODE_READING,         // Automaticke nacitani SMS
  MODE_REQUEST          // Request SMS
} TModemMode;

typedef enum {
  MODEM_INIT,           // Ceka se na inicializaci
  MODEM_CONNECTED,      // Modem je pripojen k PC
  MODEM_SENDING,        // Posilam SMS s requestem
  MODEM_WAITING,        // Cekam, dokud pamet neobsahuje alespon 1 SMS 
  MODEM_READING,        // Nacitam SMS z aktualni pozice
  MODEM_DELETING        // Mazu nactenou a zpracovanou SMS z aktualni pozice
} TModemStatus;

typedef struct {
  TModemMode    Mode;
  String        PortName;       // Nazev portu modemu, napr. "COM1"
  TModemStatus  ModemStatus;    // V jake fazi je modem
  String        RequestNumber;  // Cislo telefonu, ze ktereho se vyzaduje statistika
  int           RequestDay;     // Cislo dne pozadovane statistiky
  int           Signal;         // Sila signalu v procentech
  String        Operator;       // Nazev operatora
  int           SmsCount;       // Pocet jiz nactenych SMS
  int           Address;        // Adresa pozice nacitane SMS
  int           ErrorCounter;   // Pocitadlo chybne nactenych SMS
} TReadStatus;

// Statistika pro 1 pohlavi
typedef struct {
  int           Count;
  double        Average;
  double        Gain;
  double        Sigma;
  int           Cv;
  int           Uni;
} TStatSms;

// Dekodovana SMS
typedef struct {
  String        Number;         // Cislo telefonu
  String        Id;             // Id. cislo vahy
  int           Day;            // Cislo dne
  TDate         Date;           // Datum dne
  bool          UseGender;      // YES/NO pouzivaji se obe pohlavi
  TStatSms      FemaleStat;     // Statistika pro samice nebo vsechny
  TStatSms      MaleStat;       // Statistika pro samice nebo vsechny
} TDecodedSms;

// Indexy prubehu
enum {
  CURVE_REAL = 0,
  CURVE_COMPARE,
};

//---------------------------------------------------------------------------
class TWeighingForm : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TMenuItem *GsmMenu;
        TMenuItem *StartReadingMenu;
        TMenuItem *StopReadingMenu;
        TMenuItem *N1;
        TMenuItem *RequestMenu;
        TMenuItem *RecordsMenu;
        TMenuItem *NewMenu;
        TMenuItem *N2;
        TMenuItem *DeleteMenu;
        TPanel *FilterPanel;
        TLabel *Label3;
        TDateTimePicker *FromDateTimePicker;
        TLabel *Label4;
        TDateTimePicker *TillDateTimePicker;
        TBevel *Bevel4;
        TLabel *ScaleTitleLabel;
        TComboBox *ScaleComboBox;
        TBevel *Bevel1;
        TLabel *Label2;
        TRadioButton *PohlaviSamiceRadioButton;
        TSpeedButton *SpeedButton1;
        TRadioButton *PohlaviSamciRadioButton;
        TSpeedButton *SpeedButton2;
        TBevel *Bevel2;
        TBevel *Bevel3;
        TMenuItem *EditMenu;
        TMenuItem *FindMenu;
        TPageControl *PageControl;
        TTabSheet *TableTabSheet;
        TTabSheet *GraphTabSheet;
        TDBGrid *RecordsDBGrid;
        TPanel *Panel2;
        TLabel *Label5;
        TComboBox *GraphTypeComboBox;
        TChart *GraphChart;
        TLineSeries *LineSeries1;
        TLabel *Label6;
        TLabel *GraphTimeRangeLabel;
        TSpeedButton *SpeedButton10;
        TMenuItem *FileMenu;
        TMenuItem *N3;
        TMenuItem *PrintMenu;
        TMenuItem *ExcelMenu;
        TMenuItem *PrintReportMenu;
        TMenuItem *PrintGraphMenu;
        TMenuItem *N4;
        TMenuItem *FiltersMenu;
        TLabel *Label1;
        TComboBox *FilterComboBox;
        TBevel *Bevel9;
        TTimer *ReadTimer;
        TToolBar *ToolBar1;
        TToolButton *StartToolButton;
        TToolButton *StopToolButton;
        TToolButton *ToolButton3;
        TToolButton *RequestToolButton;
        TToolButton *ToolButton5;
        TToolButton *NewToolButton;
        TToolButton *EditToolButton;
        TToolButton *DeleteToolButton;
        TToolButton *ToolButton9;
        TToolButton *FindToolButton;
        TImageList *IconsImageList;
        TToolButton *ToolButton1;
        TToolButton *PrintToolButton;
        TToolButton *ExcelToolButton;
        TSpeedButton *FiltersSpeedButton;
        TPanel *ReadStatusPanel;
        TLabel *StatusLabel;
        TBevel *StatusBevel;
        TPanel *ComparePanel;
        TBevel *Bevel5;
        TLabel *Label7;
        TComboBox *CompareComboBox;
        TSpeedButton *CompareSpeedButton;
        TMenuItem *N5;
        TMenuItem *Growthcurves1;
        TLineSeries *Series1;
        TPanel *CompareGraphPanel;
        TLabel *CompareGraphLabel;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall NewMenuClick(TObject *Sender);
        void __fastcall FromDateTimePickerChange(TObject *Sender);
        void __fastcall DeleteMenuClick(TObject *Sender);
        void __fastcall EditMenuClick(TObject *Sender);
        void __fastcall FindMenuClick(TObject *Sender);
        void __fastcall GraphTypeComboBoxChange(TObject *Sender);
        void __fastcall PrintReportMenuClick(TObject *Sender);
        void __fastcall PrintGraphMenuClick(TObject *Sender);
        void __fastcall ExcelMenuClick(TObject *Sender);
        void __fastcall FiltersMenuClick(TObject *Sender);
        void __fastcall FilterComboBoxChange(TObject *Sender);
        void __fastcall RecordsDBGridKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall RequestMenuClick(TObject *Sender);
        void __fastcall StartReadingMenuClick(TObject *Sender);
        void __fastcall StopReadingMenuClick(TObject *Sender);
        void __fastcall PrintToolButtonClick(TObject *Sender);
        void __fastcall RecordsDBGridDrawColumnCell(TObject *Sender,
          const TRect &Rect, int DataCol, TColumn *Column,
          TGridDrawState State);
        void __fastcall ReadTimerTimer(TObject *Sender);
        void __fastcall Growthcurves1Click(TObject *Sender);
        void __fastcall CompareComboBoxChange(TObject *Sender);
private:	// User declarations
        TDate   MinDate, MaxDate;       // Minimalni a maximalni datum obsazeny v tabulce
        TReadStatus     ReadStatus;     // Stav automatickeho nacitani SMS
        TGsmModem    *GsmModem;                   // GSM modem

public:		// User declarations

        bool SelectModem();
        void ReadSignalStrength();
        void AutoDetectModem(TListBox *ListBox);

        void IncreaseAddress();
        void StringConnecting(int Phase);
        void SetReadControls(bool Running);
        void StartReadingSms(bool Request);
        void StopReadingSms();

        bool CanSaveRecord();
        void InsertNewRecord(TDateTime InsertDate, String GsmNumber);

        bool EditDecodedSms(TDecodedSms *Sms, TDateTime InsertDate, bool ShowDialog);
        bool DecodeSms(String Number, String Text, TDecodedSms *Sms);

        void SetFilter(String Filter);

        void ExcelExportReport(TQuery *Query);

        String CreateStringReportFile();
        String CreateStringReportGender(bool FemaleChecked);
        void CreateReportHeader(TQRLabel *FileLabel, TQRLabel *GenderTitleLabel, TQRLabel *GenderLabel);

        void DrawGraph(TChart *Chart);

        bool OpenFoundFile;     // YES/NO po zavreni tohoto okna otevre nalezeny soubor

        bool RecordExists(AnsiString Scales, int Day, TDate Date);
        void DeleteRecord(AnsiString Scales, int Day, TDate Date);

        void ReadFilters();
        void ReadScales();
        void ReadDate();
        void CreateSQLRecords(TQuery *Query);
        void ReadRecords();
        void ReadAll();

        void LoadCurves();

        __fastcall TWeighingForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TWeighingForm *WeighingForm;
//---------------------------------------------------------------------------
#endif
