//---------------------------------------------------------------------------

#ifndef ModbusOnlineThreadH
#define ModbusOnlineThreadH
//---------------------------------------------------------------------------
#include <Classes.hpp>
//---------------------------------------------------------------------------

#include "ModbusOnline.h"

class TModbusOnlineThread : public TThread
{
private:
protected:
        TModbusOnline *ModbusOnline;

        void __fastcall Execute();
public:
        bool NewData;           // Jsou nactena data vahy 
        void SetModbusOnline(TModbusOnline *NewModbusOnline);

        __fastcall TModbusOnlineThread(bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
