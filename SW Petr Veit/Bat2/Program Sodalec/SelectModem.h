//---------------------------------------------------------------------------

#ifndef SelectModemH
#define SelectModemH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TSelectModemForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TListBox *ModemListBox;
        TButton *Button1;
        TButton *Button2;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ModemListBoxDblClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TSelectModemForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TSelectModemForm *SelectModemForm;
//---------------------------------------------------------------------------
#endif
