//***************************************************************************
//
//    ModbusScale.cpp - Scale connected to the RS-485 network
//    Version 1.0
//
//***************************************************************************

#include "ModbusScale.h"
#include "MB.h"
#include "MBCom.h"
#include "MBcb.h"
#include "MBcbPar.h"
#include "KConvert.h"
#include "../../../Intel51/Vym/Bat2s/MbReg.h"


//-----------------------------------------------------------------------------
// Definice
//-----------------------------------------------------------------------------

// Kontrola rozsahu
#define CheckRange(Val, Min, Max)     (Val >= Min && Val <= Max)

// Kontrola rozsahu, ulozeni <Value> a vraceni
#define SaveToItem(Item, Min, Max)              \
  if (!CheckRange(Value, Min, Max)) {           \
    return MB_RESULT_INVALID_VALUE;             \
  }                                             \
  Item = Value;                                 \
  return MB_RESULT_OK;

// Kontrola rozsahu, ulozeni <Value> s typem a vraceni
#define SaveTypeToItem(Type, Item, Min, Max)    \
  if (!CheckRange(Value, Min, Max)) {           \
    return MB_RESULT_INVALID_VALUE;             \
  }                                             \
  Item = (Type)Value;                           \
  return MB_RESULT_OK;

// Kontrola adresy
#define AddressInRange(Min, Max) (Address >= Min && Address <= Max)

// Vrati LSB bajt z cisla <Data>
#define GetWordLsb(Data)        (Data & 0x00FF)

// Vrati MSB bajt z cisla <Data>
#define GetWordMsb(Data)        (Data >> 8)

//---------------------------------------------------------------------------
// Konstruktor
//---------------------------------------------------------------------------

TModbusScale::TModbusScale() {
  Connected      = false;
  MessageCounter = 0;           // Snulovat citac zaslanych zprav
  ErrorCounter   = 0;           // Snulovat citac chyb
}

//---------------------------------------------------------------------------
// Set
//---------------------------------------------------------------------------

bool TModbusScale::SetName(AnsiString NewName) {
  Name = NewName;
  return true;
}

bool TModbusScale::SetAddress(int NewAddress) {
  if (NewAddress < MIN_RS485_ADDRESS || NewAddress > MAX_RS485_ADDRESS) {
    return false;
  }
  Connection.Address = NewAddress;
  return true;
}

bool TModbusScale::SetPort(int NewPort) {
  Connection.Port = NewPort;
  return true;
}

//---------------------------------------------------------------------------
// Pripojeni k vaze
//---------------------------------------------------------------------------

bool TModbusScale::Connect() {
  // Otevre port a inicializuje Modbus podle parametru v <Connection>
  Connected       = false;
  MbcbFlockNumber = FLOCK_EMPTY_NUMBER;         // Zatim nenacitam zadne hejno

  // Predam parametry komunikace
  MbcbConnection.Port    = Connection.Port;
  MbcbConnection.Address = Connection.Address;

  // Otevru port a inicializuji Modbus
  try {
    MBInitialize();
  } catch(...) {
    return false;
  }
  Connected = true;     // Jsem pripojen
  return true;
}

//---------------------------------------------------------------------------
// Smazani chyb
//---------------------------------------------------------------------------

void TModbusScale::ClearErrors() {
  // Snuluje posledni chybu a vyjimku
  MbcbError     = MBE_OK;
  MbcbException = MBEX_NONE;
}

//---------------------------------------------------------------------------
// Nacteni registru
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadRegisters(int Start, int Stop) {
  // Nacte registry <Start>..<Stop>
  if (!Connected) {
    return MB_RESULT_ERROR;     // Modbus neni inicializovan
  }

  // Nactu pozadovane registry
  try {
    MBReadRegisters(Start, Stop - Start + 1);
  } catch (...) {
    return MB_RESULT_ERROR;     // Port neni dostupny
  }

  // Prictu pocet zprav
  MessageCounter++;

  // Pockam na odpoved
  return ReadReply();
}

//---------------------------------------------------------------------------
// Nacteni odpovedi
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadReply() {
  // Nacte odpoved z vahy

  // Snuluji posledni chybu a vyjimku
  ClearErrors();

  // Cekam na odpoved
  do {
    try {
      MBExecute();
    } catch (...) {
      return MB_RESULT_ERROR;
    }
  } while (ComStatus != COM_STATUS_STOPPED);

  // Zkontroluji vysledek
  // Nejdriv vyjimku
  switch (MbcbException) {
    case MBEX_NONE:
      break;            // Bez vyjimky, pokracuju na kontrolu chyby
    case MBEX_ILLEGAL_DATA_ADDRESS:
      return MB_RESULT_INVALID_ADDRESS;
    case MBEX_ILLEGAL_DATA_VALUE:
      return MB_RESULT_INVALID_VALUE;
    default:
      return MB_RESULT_ERROR;
  }
  // Pokud nedoslo k vyjimce, zkontroluji chybu
  switch (MbcbError) {
    case MBE_OK:
      return MB_RESULT_OK;
    case MBE_TIMEOUT:
      ErrorCounter++;
      return MB_RESULT_TIMEOUT;
    default:
      ErrorCounter++;
      return MB_RESULT_ERROR;
  }
}

//---------------------------------------------------------------------------
// Nacteni a dekodovani registru
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadAndDecodeRegisters(int Start, int Stop) {
  // Nacte registry <Start>..<Stop> a dekoduje je do struktur
  TModbusScaleResult Result;

  Result = ReadRegisters(Start, Stop);
  if (Result != MB_RESULT_OK) {
    return Result;
  }
  return DecodeRegisters();
}

//---------------------------------------------------------------------------
// Zapis do registru
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::WriteRegister(int Address, int Value) {
  // Zapise do registru <Address> hodnotu <Value> a vrati odpoved
  if (!Connected) {
    return MB_RESULT_ERROR;     // Modbus neni inicializovan
  }

  try {
    MBWriteSingleRegister(Address, Value);
  } catch (...) {
    return MB_RESULT_ERROR;     // Port neni dostupny
  }

  // Prictu pocet zprav
  MessageCounter++;

  return ReadReply();
}

//---------------------------------------------------------------------------
// Zapis do registru
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::WriteRegisters(int Start, int Stop, unsigned short int *Data) {
  // Zapise do registru <Start>..<Stop> hodnotu v <Data> a vrati odpoved
  if (!Connected) {
    return MB_RESULT_ERROR;     // Modbus neni inicializovan
  }

  // Zapisu pozadovane registry
  try {
    MBWriteRegisters(Start, Stop - Start + 1, Data);
  } catch (...) {
    return MB_RESULT_ERROR;     // Port neni dostupny
  }

  // Prictu pocet zprav
  MessageCounter++;

  // Pockam na odpoved
  return ReadReply();
}

//---------------------------------------------------------------------------
// Nacteni konfigurace
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadConfig() {
  // Nacte konfiguraci do globalniho <ScaleData.Config>
  SetDefaultConfig(&ScaleData.Config);          // Nastavim default hodnotuy u parametru, ktere se z vahy nenacitaji
  return ReadAndDecodeRegisters(MBREG_CONFIG_VERSION, MBREG_CONFIG_CORRECTION_CORRECTION);      // Muzu cist vcetne kompenzace, u vahy 1.10 to nevadi
}

TModbusScaleResult TModbusScale::ReadConfigRep(int Repetitions) {
  // Nacte konfiguraci do globalniho <ScaleData.Config>, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadConfig();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Zapis konfigurace pri vazeni
//---------------------------------------------------------------------------

#define SaveConfigRegister(Register, Value)      Data[Register - MBREG_CONFIG_HISTOGRAMRANGE] = Value

TModbusScaleResult TModbusScale::WriteConfig(TConfig *Config, bool WeighingStarted) {
  // Zapise do vahy nastaveni <Config>. Pokud je <WeighingStarted> true, zapisuje jen ty polozky, ktere lze menit behem vazeni.
  unsigned short int Data[MBREG_CONFIG_CORRECTION_CORRECTION - MBREG_CONFIG_HISTOGRAMRANGE + 1];
  TModbusScaleResult Result;

  // Ulozim parametry do pole
  SaveConfigRegister(MBREG_CONFIG_HISTOGRAMRANGE,       Config->HistogramRange);
  SaveConfigRegister(MBREG_CONFIG_UNIFORMITYRANGE,      Config->UniformityRange);
  SaveConfigRegister(MBREG_CONFIG_MARGIN_ABOVE_FEMALES, Config->MarginAbove[GENDER_FEMALE]);
  SaveConfigRegister(MBREG_CONFIG_MARGIN_ABOVE_MALES,   Config->MarginAbove[GENDER_MALE]);
  SaveConfigRegister(MBREG_CONFIG_MARGIN_BELOW_FEMALES, Config->MarginBelow[GENDER_FEMALE]);
  SaveConfigRegister(MBREG_CONFIG_MARGIN_BELOW_MALES,   Config->MarginBelow[GENDER_MALE]);
  SaveConfigRegister(MBREG_CONFIG_FILTER,               Config->Filter);
  SaveConfigRegister(MBREG_CONFIG_STABILIZATIONRANGE,   Config->StabilizationRange);
  SaveConfigRegister(MBREG_CONFIG_STABILIZATIONTIME,    Config->StabilizationTime);
  SaveConfigRegister(MBREG_CONFIG_AUTOMODE,             Config->AutoMode);
  SaveConfigRegister(MBREG_CONFIG_JUMPMODE,             Config->JumpMode);
  SaveConfigRegister(MBREG_CONFIG_UNITS,                Config->Units);
  SaveConfigRegister(MBREG_CONFIG_BACKLIGHT,            Config->Backlight);

  // Zapisu do vahy cast configu spolecnou pro verzi 1.10 i 1.11
  if (WeighingStarted) {
    // Vaha vazi, zapisuju jen registry, ktere muzu zapsat behem vazeni
    Result = WriteRegisters(MBREG_CONFIG_MARGIN_ABOVE_FEMALES, MBREG_CONFIG_BACKLIGHT, Data + (MBREG_CONFIG_MARGIN_ABOVE_FEMALES - MBREG_CONFIG_HISTOGRAMRANGE));
  } else {
    // Vazeni je zastaveno, muzu zapsat vsechny registry
    Result = WriteRegisters(MBREG_CONFIG_HISTOGRAMRANGE, MBREG_CONFIG_BACKLIGHT, Data);
  }
  if (Result != MB_RESULT_OK) {
    return Result;
  }

  // U verze 1.11 zapisu navic kompenzacni krivku. Registry configu spolecneho pro verze 1.10 a 1.11 jsou jinde,
  // tj. stejne to musim zapisovat nadvakrat
  if (Config->Version >= 0x0111) {
    SaveConfigRegister(MBREG_CONFIG_CORRECTION_DAY1,       Config->WeightCorrection.Day1);
    SaveConfigRegister(MBREG_CONFIG_CORRECTION_DAY2,       Config->WeightCorrection.Day2);
    SaveConfigRegister(MBREG_CONFIG_CORRECTION_CORRECTION, Config->WeightCorrection.Correction);

    if ((Result = WriteRegisters(MBREG_CONFIG_CORRECTION_DAY1, MBREG_CONFIG_CORRECTION_CORRECTION, Data + (MBREG_CONFIG_CORRECTION_DAY1 - MBREG_CONFIG_HISTOGRAMRANGE))) != MB_RESULT_OK) {
      return Result;
    }
  }

  // Ulozim config
  return WriteRegister(MBREG_CONFIG_SAVE, 0);
}

TModbusScaleResult TModbusScale::WriteConfigRep(TConfig *Config, bool WeighingStarted, int Repetitions) {
  // Zapise do vahy nastaveni <Config>. Pokud je <WeighingStarted> true, zapisuje jen ty polozky, ktere lze menit behem vazeni.
  // Pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = WriteConfig(Config, WeighingStarted);
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Nacteni pauzy vazeni
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadPause() {
  // Nacte pauzu vazeni
  return ReadAndDecodeRegisters(MBREG_WEIGHING_PAUSE, MBREG_WEIGHING_PAUSE);
}

TModbusScaleResult TModbusScale::ReadPauseRep(int Repetitions) {
  // Nacte pauzu vazeni, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadPause();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Nacteni hejna
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadFlock(int FlockNumber) {
  // Nacte hejno cislo <FlockNumber>.
  TModbusScaleResult Result;

  // Kontrola cisla
  if (FlockNumber >= FLOCKS_MAX_COUNT) {
    return MB_RESULT_INVALID_VALUE;
  }

  // Zapamatuju si cislo hejna, ktere ctu
  MbcbFlockNumber = FlockNumber;

  // Zapisu prikaz, vaha nahraje pozadovane hejno do pameti
  if ((Result = WriteRegister(MBREG_FLOCK_LOAD, FlockNumber)) != MB_RESULT_OK) {
    if (Result == MB_RESULT_INVALID_VALUE) {
      // Hejno neexistuje, ulozim a dal nic nenacitam
      ScaleData.Flocks[FlockNumber].Header.Number = FLOCK_EMPTY_NUMBER;
      return MB_RESULT_OK;
    }
    // Chyba prenosu
    return Result;
  }

  if (MBCGetMode() == MB_RTU_MODE) {
    // V RTU modu muzu nacitat naraz 125 registru, zvladnu to nadvakrat
    // Nactu a dekoduju prvni cast hejna
    if ((Result = ReadAndDecodeRegisters(MBREG_FLOCK_HEADER_NUMBER, MBREG_FLOCK_CURVE_F_WEIGHT_30)) != MB_RESULT_OK) {
      return Result;
    }
    // Nactu a dekoduju druhou cast hejna
    return ReadAndDecodeRegisters(MBREG_FLOCK_CURVE_M_DAY_1, MBREG_FLOCK_CURVE_M_WEIGHT_30);
  } else {
    // V ASCII modu muzu nacitat naraz jen 61 registru, musim natrikrat

    // Nactu a dekoduju prvni cast hejna
    if ((Result = ReadAndDecodeRegisters(MBREG_FLOCK_HEADER_NUMBER, MBREG_FLOCK_CURVE_F_WEIGHT_11)) != MB_RESULT_OK) {
      return Result;
    }
    // Nactu a dekoduju druhou cast hejna
    if ((Result = ReadAndDecodeRegisters(MBREG_FLOCK_CURVE_F_WEIGHT_12, MBREG_FLOCK_CURVE_M_DAY_30)) != MB_RESULT_OK) {
      return Result;
    }
    // Nactu a dekoduju treti cast hejna
    return ReadAndDecodeRegisters(MBREG_FLOCK_CURVE_M_WEIGHT_1, MBREG_FLOCK_CURVE_M_WEIGHT_30);
  }
}

TModbusScaleResult TModbusScale::ReadFlockRep(int FlockNumber, int Repetitions) {
  // Nacte hejno cislo <FlockNumber>, pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult Result;
  int ErrorCounter = 0;

  do {
    if ((Result = ReadFlock(FlockNumber)) == MB_RESULT_OK) {
      return MB_RESULT_OK;
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Nacteni vsech hejn
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadFlocks() {
  // Nacte vsechna hejna
  for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
    if (ReadFlock(i) != MB_RESULT_OK) {
      // Chyba pri cteni
      return MB_RESULT_ERROR;             // U nektereho hejna doslo k chybe pri cteni, vsechna hejna se tedy nenacetla
    }
  }
  return MB_RESULT_OK;
}

TModbusScaleResult TModbusScale::ReadFlocksRep(int Repetitions) {
  // Nacte vsechna hejna, pri chybe zkusi <Repetitions> krat opakovat.
  for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
    if (ReadFlockRep(i, Repetitions) != MB_RESULT_OK) {
      // Chyba pri cteni
      return MB_RESULT_ERROR;             // U nektereho hejna doslo k chybe pri cteni, vsechna hejna se tedy nenacetla
    }
  }
  return MB_RESULT_OK;
}

//---------------------------------------------------------------------------
// Zapis hejna
//---------------------------------------------------------------------------

#define SaveFlockRegister(Register, Value)      Data[Register - MBREG_FLOCK_HEADER_NUMBER] = Value

static void CopyFlockTitle(unsigned char *Title, unsigned short int *Data) {
  for (int i = 0; i < FLOCK_NAME_MAX_LENGTH; i += 2) {
    *Data  = Title[i] << 8;
    *Data |= Title[i + 1];
    Data++;
  }
}

static void ClearCurve(int StartDayRegister, int StartWeightRegister, unsigned short int *Data) {
  for (int i = 0; i < CURVE_MAX_POINTS; i++) {
    SaveFlockRegister(StartDayRegister    + i, CURVE_FIRST_DAY_NUMBER);
    SaveFlockRegister(StartWeightRegister + i, CURVE_END_WEIGHT);
  }
}

static void SaveCurve(TCurvePoint *Curve, int StartDayRegister, int StartWeightRegister, unsigned short int *Data) {
  // Smazu vsechny body krivky
  ClearCurve(StartDayRegister, StartWeightRegister, Data);

  // Ulozim vsechny pouzite body
  for (int i = 0; i < CURVE_MAX_POINTS; i++) {
    if (Curve[i].Weight == CURVE_END_WEIGHT) {
      // Konec krivky (v registrech mam uz ulozene same zakonceni, tj. manualne zakoncovat nemusim)
      break;
    }
    SaveFlockRegister(StartDayRegister    + i, Curve[i].Day);
    SaveFlockRegister(StartWeightRegister + i, Curve[i].Weight);
  }
}

static void SaveInitialWeight(int Weight, int StartDayRegister, int StartWeightRegister, unsigned short int *Data) {
  // Smazu vsechny body krivky
  ClearCurve(StartDayRegister, StartWeightRegister, Data);
  // Ulozim pocatecni hmotnost jako prvni bod krivky
  SaveFlockRegister(StartDayRegister,    CURVE_FIRST_DAY_NUMBER);
  SaveFlockRegister(StartWeightRegister, Weight);
}

TModbusScaleResult TModbusScale::WriteFlock(TFlock *Flock) {
  // Zapise do vahy hejno <Flock> (hejno nesmi byt prazdne, k mazani hejna se pouziva jina fce).
  unsigned short int Data[MBREG_FLOCK_CURVE_M_WEIGHT_30 - MBREG_FLOCK_HEADER_NUMBER + 1];
  TModbusScaleResult Result;

  // Ulozim parametry do pole
  SaveFlockRegister(MBREG_FLOCK_HEADER_NUMBER,         Flock->Header.Number);
  CopyFlockTitle(Flock->Header.Title, &Data[MBREG_FLOCK_HEADER_TITLE_1 - MBREG_FLOCK_HEADER_NUMBER]);
  SaveFlockRegister(MBREG_FLOCK_HEADER_USECURVES,      Flock->Header.UseCurves);
  SaveFlockRegister(MBREG_FLOCK_HEADER_USEBOTHGENDERS, Flock->Header.UseBothGenders);
  SaveFlockRegister(MBREG_FLOCK_HEADER_WEIGHFROM,      Flock->Header.WeighFrom);
  SaveFlockRegister(MBREG_FLOCK_HEADER_WEIGHTILL,      Flock->Header.WeighTill);
  ClearCurve(MBREG_FLOCK_CURVE_M_DAY_1, MBREG_FLOCK_CURVE_M_WEIGHT_1, Data);    // Default smazu krivku samcu, abych to nemusel resit pozdeji
  if (Flock->Header.UseCurves) {
    SaveCurve(Flock->GrowthCurve[GENDER_FEMALE], MBREG_FLOCK_CURVE_F_DAY_1, MBREG_FLOCK_CURVE_F_WEIGHT_1, Data);
    if (Flock->Header.UseBothGenders) {
      SaveCurve(Flock->GrowthCurve[GENDER_MALE], MBREG_FLOCK_CURVE_M_DAY_1, MBREG_FLOCK_CURVE_M_WEIGHT_1, Data);
    }
  } else {
    SaveInitialWeight(Flock->GrowthCurve[GENDER_FEMALE][0].Weight, MBREG_FLOCK_CURVE_F_DAY_1, MBREG_FLOCK_CURVE_F_WEIGHT_1, Data);
    if (Flock->Header.UseBothGenders) {
      SaveInitialWeight(Flock->GrowthCurve[GENDER_MALE][0].Weight, MBREG_FLOCK_CURVE_M_DAY_1, MBREG_FLOCK_CURVE_M_WEIGHT_1, Data);
    }
  }

  // Zapisu hejno do vahy
  if (MBCGetMode() == MB_RTU_MODE) {
    // V RTU modu muzu zapsat naraz 125 registru, zvladnu to nadvakrat
    // Nactu a dekoduju prvni cast hejna
    if ((Result = WriteRegisters(MBREG_FLOCK_HEADER_NUMBER, MBREG_FLOCK_CURVE_F_WEIGHT_30, Data)) != MB_RESULT_OK) {
      return Result;
    }
    if ((Result = WriteRegisters(MBREG_FLOCK_CURVE_M_DAY_1, MBREG_FLOCK_CURVE_M_WEIGHT_30,
                                 Data + (MBREG_FLOCK_CURVE_M_DAY_1 - MBREG_FLOCK_HEADER_NUMBER))) != MB_RESULT_OK) {
      return Result;
    }
  } else {
    // V ASCII modu muzu zapsat naraz jen 61 registru, musim natrikrat
    if ((Result = WriteRegisters(MBREG_FLOCK_HEADER_NUMBER, MBREG_FLOCK_CURVE_F_WEIGHT_11, Data)) != MB_RESULT_OK) {
      return Result;
    }
    if ((Result = WriteRegisters(MBREG_FLOCK_CURVE_F_WEIGHT_12, MBREG_FLOCK_CURVE_M_DAY_30,
                                 Data + (MBREG_FLOCK_CURVE_F_WEIGHT_12 - MBREG_FLOCK_HEADER_NUMBER))) != MB_RESULT_OK) {
      return Result;
    }
    if ((Result = WriteRegisters(MBREG_FLOCK_CURVE_M_WEIGHT_1, MBREG_FLOCK_CURVE_M_WEIGHT_30,
                                 Data + (MBREG_FLOCK_CURVE_M_WEIGHT_1 - MBREG_FLOCK_HEADER_NUMBER))) != MB_RESULT_OK) {
      return Result;
    }
  }

  // Ulozim hejno
  return WriteRegister(MBREG_FLOCK_SAVE, 0);
}

TModbusScaleResult TModbusScale::WriteFlockRep(TFlock *Flock, int Repetitions) {
  // Zapise do vahy hejno <Flock>, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = WriteFlock(Flock);
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Smazani hejna
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::DeleteFlock(int FlockNumber) {
  // Smaze hejno cislo <FlockNumber>

  // Kontrola cisla
  if (FlockNumber >= FLOCKS_MAX_COUNT) {
    return MB_RESULT_INVALID_VALUE;
  }

  // Zapisu prikaz, vaha nahraje pozadovane hejno do pameti
  return WriteRegister(MBREG_FLOCK_DELETE, FlockNumber);
}

TModbusScaleResult TModbusScale::DeleteFlockRep(int FlockNumber, int Repetitions) {
  // Smaze hejno cislo <FlockNumber>, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = DeleteFlock(FlockNumber);
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Zapis vsech hejn
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::WriteFlocks(TFlock *Flock, int WeighedFlock) {
  // Zapise do vahy vsechna hejna <Flock>, <WeighedFlock> je cislo hejna, ktere se prave vazi nebo -1
  TModbusScaleResult Result;

  for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
    if (i == WeighedFlock) {
      // Podle tohoto hejna se prave vazi, nemuzu ho modifikovat ani mazat
      Flock++;          // Prejdu na dalsi hejno
      continue;
    }
    if (Flock->Header.Number == i) {
      // Hejno je zadane
      Result = WriteFlock(Flock);
    } else {
      // Hejno je prazdne
      Result = DeleteFlock(i);
    }
    if (Result != MB_RESULT_OK) {
      return Result;
    }

    Flock++;            // Prejdu na dalsi hejno
  }
  return MB_RESULT_OK;
}

TModbusScaleResult TModbusScale::WriteFlocksRep(TFlock *Flock, int WeighedFlock, int Repetitions) {
  // Zapise do vahy vsechna hejna <Flock>, <WeighedFlock> je cislo hejna, ktere se prave vazi nebo -1
  // Pri chybe zkusi <Repetitions> krat opakovat.
  TModbusScaleResult Result;

  for (int i = 0; i < FLOCKS_MAX_COUNT; i++) {
    if (i == WeighedFlock) {
      // Podle tohoto hejna se prave vazi, nemuzu ho modifikovat ani mazat
      Flock++;          // Prejdu na dalsi hejno
      continue;
    }
    if (Flock->Header.Number == i) {
      // Hejno je zadane
      Result = WriteFlockRep(Flock, Repetitions);
    } else {
      // Hejno je prazdne
      Result = DeleteFlockRep(i, Repetitions);
    }
    if (Result != MB_RESULT_OK) {
      return Result;
    }

    Flock++;            // Prejdu na dalsi hejno
  }
  return MB_RESULT_OK;
}

//---------------------------------------------------------------------------
// Nacteni data a casu
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadDateTime() {
  // Nacte datum a cas
  return ReadAndDecodeRegisters(MBREG_DATETIME_DAY, MBREG_DATETIME_MIN);
}

TModbusScaleResult TModbusScale::ReadDateTimeRep(int Repetitions) {
  // Nacte datum a cas, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadDateTime();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Nacteni statistiky z archivu
//---------------------------------------------------------------------------

void TModbusScale::ClearArchiveList() {
  // Vymaze dosavadni vysledky
  for (vector<TArchive *>::iterator Iterator = ScaleData.ArchiveList.begin(); Iterator != ScaleData.ArchiveList.end(); Iterator++) {
    delete (*Iterator);         // Smazu vsechny dny
  }
  // Smazu seznam
  ScaleData.ArchiveList.clear();
}

void TModbusScale::DeleteLastDayFromArchiveList() {
  // Vymaze posledni den ze seznamu
  if (ScaleData.ArchiveList.size() == 0) {
    return;             // Seznam je prazdny
  }
  // Smazu archiv
  TArchive *Archive;
  Archive = ScaleData.ArchiveList.back();
  delete Archive;
  // Smazu den ze seznamu
  ScaleData.ArchiveList.pop_back();
}

TModbusScaleResult TModbusScale::ReadAndDecodeArchive() {
  // Nacte a dekoduje statistiku
  TModbusScaleResult Result;

  // Zalozim novy den, do ktereho se bude statistika ukladat pri dekodovani
  TArchive *Archive = new TArchive;
  ScaleData.ArchiveList.push_back(Archive);

  // Nactu a dekoduju statistiku
  if (MBCGetMode() == MB_RTU_MODE) {

    // V RTU modu muzu nacitat naraz 125 registru, zvladnu to naraz
    if ((Result = ReadAndDecodeRegisters(MBREG_RESULTS_DAYNUMBER, MBREG_RESULTS_REALUNIFORMITY_M)) != MB_RESULT_OK) {
      DeleteLastDayFromArchiveList();      // Smazu vytvoreny den ze seznamu
      return Result;
    }

  } else {

    // V ASCII modu muzu nacitat naraz jen 61 registru, musim natrikrat
    if ((Result = ReadAndDecodeRegisters(MBREG_RESULTS_DAYNUMBER, MBREG_RESULTS_HIST_F_COUNT_39)) != MB_RESULT_OK) {
      DeleteLastDayFromArchiveList();      // Smazu vytvoreny den ze seznamu
      return Result;
    }
    if ((Result = ReadAndDecodeRegisters(MBREG_RESULTS_HIST_M_CENTER_HI, MBREG_RESULTS_REALUNIFORMITY_M)) != MB_RESULT_OK) {
      DeleteLastDayFromArchiveList();      // Smazu vytvoreny den ze seznamu
      return Result;
    }

  }

  return MB_RESULT_OK;
}

TModbusScaleResult TModbusScale::ReadTodayFromArchive() {
  // Nacte statistiku aktualniho dne vazeni
  TModbusScaleResult Result;

  // Smazu dosavadni vysledky
  ClearArchiveList();

  // Zapisu prikaz
  if ((Result = WriteRegister(MBREG_RESULTS_LOAD_TODAY, 0)) != MB_RESULT_OK) {
    return Result;
  }

  // Nactu a dekoduju statistiku
  return ReadAndDecodeArchive();
}

TModbusScaleResult TModbusScale::ReadTodayFromArchiveRep(int Repetitions) {
  // Nacte statistiku aktualniho dne vazeni, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadTodayFromArchive();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

TModbusScaleResult TModbusScale::ReadFirstDayFromArchive() {
  // Nacte statistiku prvniho dne v archivu
  TModbusScaleResult Result;

  // Smazu dosavadni vysledky
  ClearArchiveList();

  // Zapisu prikaz
  if ((Result = WriteRegister(MBREG_RESULTS_LOAD_FIRST_DAY, 0)) != MB_RESULT_OK) {
    return Result;
  }

  // Nactu a dekoduju statistiku
  return ReadAndDecodeArchive();
}

TModbusScaleResult TModbusScale::ReadFirstDayFromArchiveRep(int Repetitions) {
  // Nacte statistiku prvniho dne v archivu, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadFirstDayFromArchive();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

TModbusScaleResult TModbusScale::ReadNextDayFromArchive() {
  // Nacte statistiku dalsiho dne v archivu
  TModbusScaleResult Result;

  // Zapisu prikaz
  if ((Result = WriteRegister(MBREG_RESULTS_LOAD_NEXT_DAY, 0)) != MB_RESULT_OK) {
    return Result;
  }

  // Nactu a dekoduju statistiku
  return ReadAndDecodeArchive();
}

TModbusScaleResult TModbusScale::ReadNextDayFromArchiveRep(int Repetitions) {
  // Nacte statistiku dalsiho dne v archivu, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadNextDayFromArchive();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Nacteni kalibrace
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::ReadCalibration() {
  // Nacte kalibraci
  return ReadAndDecodeRegisters(MBREG_CAL_ZEROCAL_HI, MBREG_CAL_DIVISION);
}

TModbusScaleResult TModbusScale::ReadCalibrationRep(int Repetitions) {
  // Nacte kalibraci, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadCalibration();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Dekodovani nactenych dat
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::DecodeRegisters() {
  // Dekoduje prijate registry
  TScaleData OldScaleData = ScaleData;          // Zaloha soucasneho stavu
  TModbusScaleResult Result;

  if (MbcbRegisters.Count == 0) {
    return MB_RESULT_INVALID_ADDRESS;           // Nejsou nacteny zadne registry
  }

  for (int i = 0; i < MbcbRegisters.Count; i++) {
    Result = DecodeRegister(MbcbRegisters.Address, MbcbRegisters.Data[i]);
    if (Result != MB_RESULT_OK) {
      ScaleData = OldScaleData;                 // Obnovim data (dekodovanim predchozich registru jsem data castecne modifikoval)
      return Result;
    }
    MbcbRegisters.Address++;
  }
  return MB_RESULT_OK;
}

//---------------------------------------------------------------------------
// Dekodovani registru
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::DecodeRegister(int Address, int Value) {
  // Dekoduje registr <Address> s hodnotu <Value>
  TArchive *Archive;            // Archiv, do ktereho zapisuju vysledky nactene z vahy

  // Hromadna kontrola
  if (AddressInRange(MBREG_FLOCK_HEADER_NUMBER, MBREG_FLOCK_CURVE_M_WEIGHT_30)) {
    if (MbcbFlockNumber >= FLOCKS_MAX_COUNT) {
      return MB_RESULT_INVALID_VALUE;             // Zadne hejno zatim nenahral
    }
  }
  if (AddressInRange(MBREG_RESULTS_DAYNUMBER, MBREG_RESULTS_REALUNIFORMITY_M)) {
    if (ScaleData.ArchiveList.size() == 0) {
      return MB_RESULT_INVALID_VALUE;             // Zadny den zatim neni zalozen
    }
    Archive = ScaleData.ArchiveList.back();     // Ukladam do posledniho archivu v seznamu
  }

  // Souvisle useky
  if (AddressInRange(MBREG_FLOCK_CURVE_F_DAY_1, MBREG_FLOCK_CURVE_F_DAY_30)) {
    ScaleData.Flocks[MbcbFlockNumber].GrowthCurve[GENDER_FEMALE][Address - MBREG_FLOCK_CURVE_F_DAY_1].Day = Value;
    return MB_RESULT_OK;
  }
  if (AddressInRange(MBREG_FLOCK_CURVE_F_WEIGHT_1, MBREG_FLOCK_CURVE_F_WEIGHT_30)) {
    // Hmotnost musi byt v mezich MIN_TARGET_WEIGHT..MAX_TARGET_WEIGHT nebo 0, ktera ukoncuje krivku
    ScaleData.Flocks[MbcbFlockNumber].GrowthCurve[GENDER_FEMALE][Address - MBREG_FLOCK_CURVE_F_WEIGHT_1].Weight = Value;
    return MB_RESULT_OK;
  }
  if (AddressInRange(MBREG_FLOCK_CURVE_M_DAY_1, MBREG_FLOCK_CURVE_M_DAY_30)) {
    ScaleData.Flocks[MbcbFlockNumber].GrowthCurve[GENDER_MALE][Address - MBREG_FLOCK_CURVE_M_DAY_1].Day = Value;
    return MB_RESULT_OK;
  }
  if (AddressInRange(MBREG_FLOCK_CURVE_M_WEIGHT_1, MBREG_FLOCK_CURVE_M_WEIGHT_30)) {
    // Hmotnost musi byt v mezich MIN_TARGET_WEIGHT..MAX_TARGET_WEIGHT nebo 0, ktera ukoncuje krivku
    ScaleData.Flocks[MbcbFlockNumber].GrowthCurve[GENDER_MALE][Address - MBREG_FLOCK_CURVE_M_WEIGHT_1].Weight = Value;
    return MB_RESULT_OK;
  }
  if (AddressInRange(MBREG_RESULTS_HIST_F_COUNT_1, MBREG_RESULTS_HIST_F_COUNT_39)) {
    Archive->Hist[GENDER_FEMALE].Slot[Address - MBREG_RESULTS_HIST_F_COUNT_1] = Value;
    return MB_RESULT_OK;
  }
  if (AddressInRange(MBREG_RESULTS_HIST_M_COUNT_1, MBREG_RESULTS_HIST_M_COUNT_39)) {
    Archive->Hist[GENDER_MALE].Slot[Address - MBREG_RESULTS_HIST_M_COUNT_1] = Value;
    return MB_RESULT_OK;
  }

  switch (Address) {

    // Konfigurace -----------------------------------------------------------------------------
    case MBREG_CONFIG_VERSION:
      ScaleData.Config.Version = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_BUILD:
      ScaleData.Config.Build = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_HWVERSION:
      ScaleData.Config.HwVersion = (THwVersion)Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_ID_0:
    case MBREG_CONFIG_ID_1:
    case MBREG_CONFIG_ID_2:
    case MBREG_CONFIG_ID_3:
    case MBREG_CONFIG_ID_4:
      if (!CheckIdLetter(GetWordMsb(Value)) || !CheckIdLetter(GetWordLsb(Value))) {
        return MB_RESULT_INVALID_VALUE;
      }
      ScaleData.Config.IdentificationNumber[2 * (Address - MBREG_CONFIG_ID_0)]     = GetWordMsb(Value);
      ScaleData.Config.IdentificationNumber[2 * (Address - MBREG_CONFIG_ID_0) + 1] = GetWordLsb(Value);
      return MB_RESULT_OK;
    case MBREG_CONFIG_LANGUAGE:
      ScaleData.Config.Language = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_MARGIN_ABOVE_FEMALES:
      SaveToItem(ScaleData.Config.MarginAbove[GENDER_FEMALE], MIN_MARGIN, MAX_MARGIN);
    case MBREG_CONFIG_MARGIN_ABOVE_MALES:
      SaveToItem(ScaleData.Config.MarginAbove[GENDER_MALE], MIN_MARGIN, MAX_MARGIN);
    case MBREG_CONFIG_MARGIN_BELOW_FEMALES:
      SaveToItem(ScaleData.Config.MarginBelow[GENDER_FEMALE], MIN_MARGIN, MAX_MARGIN);
    case MBREG_CONFIG_MARGIN_BELOW_MALES:
      SaveToItem(ScaleData.Config.MarginBelow[GENDER_MALE], MIN_MARGIN, MAX_MARGIN);
    case MBREG_CONFIG_FILTER:
      SaveToItem(ScaleData.Config.Filter, MIN_FILTER, MAX_FILTER);
    case MBREG_CONFIG_STABILIZATIONRANGE:
      SaveToItem(ScaleData.Config.StabilizationRange, MIN_STABILIZATION_RANGE, MAX_STABILIZATION_RANGE);
    case MBREG_CONFIG_STABILIZATIONTIME:
      SaveToItem(ScaleData.Config.StabilizationTime, MIN_STABILIZATION_TIME, MAX_STABILIZATION_TIME);
    case MBREG_CONFIG_AUTOMODE:
      SaveTypeToItem(TAutoMode, ScaleData.Config.AutoMode, 0, _AUTOMODE_COUNT - 1);
    case MBREG_CONFIG_JUMPMODE:
      SaveTypeToItem(TJumpMode, ScaleData.Config.JumpMode, 0, _JUMPMODE_COUNT - 1);
    case MBREG_CONFIG_UNITS:
      SaveTypeToItem(TUnits, ScaleData.Config.Units, 0, _UNITS_COUNT - 1);
    case MBREG_CONFIG_HISTOGRAMRANGE:
      SaveToItem(ScaleData.Config.HistogramRange, MIN_HISTOGRAM_RANGE, MAX_HISTOGRAM_RANGE);
    case MBREG_CONFIG_UNIFORMITYRANGE:
      SaveToItem(ScaleData.Config.UniformityRange, MIN_UNIFORMITY_RANGE, MAX_UNIFORMITY_RANGE);
    case MBREG_CONFIG_WS_RUNNING:
      ScaleData.Config.WeighingStart.Running = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_WS_WAITING:
      ScaleData.Config.WeighingStart.WaitingForStart = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_WS_USEFLOCK:
      ScaleData.Config.WeighingStart.UseFlock = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_WS_CURRENTFLOCK:
      if (Value != FLOCK_EMPTY_NUMBER) {
        // FLOCK_EMPTY_NUMBER znaci vazeni bez hejna
        if (Value >= FLOCKS_MAX_COUNT) {
          return MB_RESULT_INVALID_VALUE;         // Radeji kontrola cisla
        }
      }
      ScaleData.Config.WeighingStart.CurrentFlock = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_WS_CURVEDAYSHIFT:
      SaveToItem(ScaleData.Config.WeighingStart.CurveDayShift, 0, CURVE_MAX_DAY);
    case MBREG_CONFIG_WS_DATETIME_HOUR:
      SaveToItem(ScaleData.Config.WeighingStart.DateTime.Hour, 0, 23);
    case MBREG_CONFIG_WS_DATETIME_MIN:
      SaveToItem(ScaleData.Config.WeighingStart.DateTime.Min, 0, 59);
    case MBREG_CONFIG_WS_DATETIME_DAY:
      SaveToItem(ScaleData.Config.WeighingStart.DateTime.Day , 1, 31);
    case MBREG_CONFIG_WS_DATETIME_MONTH:
      SaveToItem(ScaleData.Config.WeighingStart.DateTime.Month, 1, 12);
    case MBREG_CONFIG_WS_DATETIME_YEAR:
      SaveToItem(ScaleData.Config.WeighingStart.DateTime.Year, 1, 3000);
    case MBREG_CONFIG_WS_QW_USEBOTHGENDERS:
      ScaleData.Config.WeighingStart.QuickWeighing.UseBothGenders = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_WS_QW_INITIALWFEMALES:
      SaveToItem(ScaleData.Config.WeighingStart.QuickWeighing.InitialWeight[GENDER_FEMALE], MIN_TARGET_WEIGHT, MAX_TARGET_WEIGHT);
    case MBREG_CONFIG_WS_QW_INITIALWMALES:
      if (ScaleData.Config.WeighingStart.QuickWeighing.UseBothGenders) {
        SaveToItem(ScaleData.Config.WeighingStart.QuickWeighing.InitialWeight[GENDER_MALE], MIN_TARGET_WEIGHT, MAX_TARGET_WEIGHT);
      } else {
        ScaleData.Config.WeighingStart.QuickWeighing.InitialWeight[GENDER_MALE] = Value;
        return MB_RESULT_OK;
      }
    case MBREG_CONFIG_WS_UNIFORMITYRANGE:
      SaveToItem(ScaleData.Config.WeighingStart.UniformityRange, MIN_UNIFORMITY_RANGE, MAX_UNIFORMITY_RANGE);
    case MBREG_CONFIG_WS_ONLINE:
      ScaleData.Config.WeighingStart.Online = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_BACKLIGHT:
      SaveTypeToItem(TBacklight, ScaleData.Config.Backlight, 0, _BACKLIGHT_COUNT - 1);
    case MBREG_CONFIG_RS485_ADDRESS:
      SaveToItem(ScaleData.Config.Rs485.Address, MIN_RS485_ADDRESS, MAX_RS485_ADDRESS);
    case MBREG_CONFIG_RS485_SPEED_HI:
      SaveWordMsb(&ScaleData.Config.Rs485.Speed, Value);
      return MB_RESULT_OK;
    case MBREG_CONFIG_RS485_SPEED_LO:
      SaveWordLsb(&ScaleData.Config.Rs485.Speed, Value);
      return MB_RESULT_OK;
    case MBREG_CONFIG_RS485_PARITY:
      SaveTypeToItem(TParity, ScaleData.Config.Rs485.Parity, 0, _RS485_PARITY_COUNT - 1);
    case MBREG_CONFIG_RS485_REPLYDELAY:
      SaveToItem(ScaleData.Config.Rs485.ReplyDelay, MIN_RS485_REPLY_DELAY, MAX_RS485_REPLY_DELAY);
    case MBREG_CONFIG_RS485_SILENTINTERVAL:
      SaveToItem(ScaleData.Config.Rs485.SilentInterval, 0, 0xFF);
    case MBREG_CONFIG_RS485_PROTOCOL:
      ScaleData.Config.Rs485.Protocol = (TProtocol)Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_COMPARISONFLOCK:
      if (Value != FLOCK_EMPTY_NUMBER) {
        // FLOCK_EMPTY_NUMBER znaci vazeni bez hejna
        if (Value >= FLOCKS_MAX_COUNT) {
          return MB_RESULT_INVALID_VALUE;         // Radeji kontrola cisla
        }
      }
      ScaleData.Config.ComparisonFlock = Value;
      return MB_RESULT_OK;
    case MBREG_CONFIG_CORRECTION_DAY1:
      SaveToItem(ScaleData.Config.WeightCorrection.Day1, 0, CURVE_MAX_DAY);
    case MBREG_CONFIG_CORRECTION_DAY2:
      SaveToItem(ScaleData.Config.WeightCorrection.Day2, 0, CURVE_MAX_DAY);
    case MBREG_CONFIG_CORRECTION_CORRECTION:
      SaveToItem(ScaleData.Config.WeightCorrection.Correction, 0, 0xFF);

    // Hejna -----------------------------------------------------------------------------
    // Nacitam hejno, ktere jsem si naposledy vyzadal
    case MBREG_FLOCK_HEADER_NUMBER:
      SaveToItem(ScaleData.Flocks[MbcbFlockNumber].Header.Number, 0, FLOCKS_MAX_COUNT);
    case MBREG_FLOCK_HEADER_TITLE_1:
    case MBREG_FLOCK_HEADER_TITLE_2:
    case MBREG_FLOCK_HEADER_TITLE_3:
    case MBREG_FLOCK_HEADER_TITLE_4:
      if (!CheckLetter(GetWordMsb(Value)) || !CheckLetter(GetWordLsb(Value))) {
        return MB_RESULT_INVALID_VALUE;
      }
      ScaleData.Flocks[MbcbFlockNumber].Header.Title[2 * (Address - MBREG_FLOCK_HEADER_TITLE_1)]     = GetWordMsb(Value),
      ScaleData.Flocks[MbcbFlockNumber].Header.Title[2 * (Address - MBREG_FLOCK_HEADER_TITLE_1) + 1] = GetWordLsb(Value);
      return MB_RESULT_OK;
    case MBREG_FLOCK_HEADER_USECURVES:
      ScaleData.Flocks[MbcbFlockNumber].Header.UseCurves = Value;
      return MB_RESULT_OK;
    case MBREG_FLOCK_HEADER_USEBOTHGENDERS:
      ScaleData.Flocks[MbcbFlockNumber].Header.UseBothGenders = Value;
      return MB_RESULT_OK;
    case MBREG_FLOCK_HEADER_WEIGHFROM:
      ScaleData.Flocks[MbcbFlockNumber].Header.WeighFrom = Value;
      return MB_RESULT_OK;
    case MBREG_FLOCK_HEADER_WEIGHTILL:
      ScaleData.Flocks[MbcbFlockNumber].Header.WeighTill = Value;
      return MB_RESULT_OK;

    // Datum a cas -----------------------------------------------------------------------------
    case MBREG_DATETIME_DAY:
      SaveToItem(ScaleData.DateTime.Day, 1, 31);
    case MBREG_DATETIME_MONTH:
      SaveToItem(ScaleData.DateTime.Month, 1, 12);
    case MBREG_DATETIME_YEAR:
      SaveToItem(ScaleData.DateTime.Year, 1, 3000);
    case MBREG_DATETIME_HOUR:
      SaveToItem(ScaleData.DateTime.Hour, 0, 23);
    case MBREG_DATETIME_MIN:
      SaveToItem(ScaleData.DateTime.Min, 0, 59);

    // Stav vazeni -----------------------------------------------------------------------------
    case MBREG_WEIGHING_PAUSE:
      ScaleData.Pause = Value;
      return MB_RESULT_OK;

    // Vysledky vazeni -----------------------------------------------------------------------------
    case MBREG_RESULTS_DAYNUMBER:
      SaveToItem(Archive->DayNumber, 0, CURVE_MAX_DAY);
    case MBREG_RESULTS_DATETIME_DAY:
      SaveToItem(Archive->DateTime.Day, 0, 31);
    case MBREG_RESULTS_DATETIME_MONTH:
      SaveToItem(Archive->DateTime.Month, 0, 12);
    case MBREG_RESULTS_DATETIME_YEAR:
      SaveToItem(Archive->DateTime.Year, 0, 3000);
    case MBREG_RESULTS_DATETIME_HOUR:
      SaveToItem(Archive->DateTime.Hour, 0, 23);
    case MBREG_RESULTS_DATETIME_MIN:
      SaveToItem(Archive->DateTime.Min, 0, 59);
    case MBREG_RESULTS_STAT_F_XSUM_HI:
      SaveWordMsb(&Archive->Stat[GENDER_FEMALE].XSuma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_F_XSUM_LO:
      SaveWordLsb(&Archive->Stat[GENDER_FEMALE].XSuma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_F_X2SUM_HI:
      SaveWordMsb(&Archive->Stat[GENDER_FEMALE].X2Suma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_F_X2SUM_LO:
      SaveWordLsb(&Archive->Stat[GENDER_FEMALE].X2Suma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_F_COUNT:
      Archive->Stat[GENDER_FEMALE].Count = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_M_XSUM_HI:
      SaveWordMsb(&Archive->Stat[GENDER_MALE].XSuma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_M_XSUM_LO:
      SaveWordLsb(&Archive->Stat[GENDER_MALE].XSuma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_M_X2SUM_HI:
      SaveWordMsb(&Archive->Stat[GENDER_MALE].X2Suma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_M_X2SUM_LO:
      SaveWordLsb(&Archive->Stat[GENDER_MALE].X2Suma, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_STAT_M_COUNT:
      Archive->Stat[GENDER_MALE].Count = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_F_CENTER_HI:
      SaveWordMsb(&Archive->Hist[GENDER_FEMALE].Center, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_F_CENTER_LO:
      SaveWordLsb(&Archive->Hist[GENDER_FEMALE].Center, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_F_STEP_HI:
      SaveWordMsb(&Archive->Hist[GENDER_FEMALE].Step, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_F_STEP_LO:
      SaveWordLsb(&Archive->Hist[GENDER_FEMALE].Step, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_M_CENTER_HI:
      SaveWordMsb(&Archive->Hist[GENDER_MALE].Center, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_M_CENTER_LO:
      SaveWordLsb(&Archive->Hist[GENDER_MALE].Center, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_M_STEP_HI:
      SaveWordMsb(&Archive->Hist[GENDER_MALE].Step, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_HIST_M_STEP_LO:
      SaveWordLsb(&Archive->Hist[GENDER_MALE].Step, Value);
      return MB_RESULT_OK;
    case MBREG_RESULTS_LASTAVERAGE_F:
      Archive->LastAverage[GENDER_FEMALE] = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_LASTAVERAGE_M:
      Archive->LastAverage[GENDER_MALE] = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_TARGETWEIGHT_F:
      Archive->TargetWeight[GENDER_FEMALE] = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_TARGETWEIGHT_M:
      Archive->TargetWeight[GENDER_MALE] = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_REALUNIFORMITYUSED:
      Archive->RealUniformityUsed = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_REALUNIFORMITY_F:
      Archive->RealUniformity[GENDER_FEMALE] = Value;
      return MB_RESULT_OK;
    case MBREG_RESULTS_REALUNIFORMITY_M:
      Archive->RealUniformity[GENDER_MALE] = Value;
      return MB_RESULT_OK;

    // Kalibrace -----------------------------------------------------------------------------
    case MBREG_CAL_ZEROCAL_HI:
      SaveWordMsb(&ScaleData.Calibration.ZeroCalibration, Value);
      return MB_RESULT_OK;
    case MBREG_CAL_ZEROCAL_LO:
      SaveWordLsb(&ScaleData.Calibration.ZeroCalibration, Value);
      return MB_RESULT_OK;
    case MBREG_CAL_RANGECAL_HI:
      SaveWordMsb(&ScaleData.Calibration.RangeCalibration, Value);
      return MB_RESULT_OK;
    case MBREG_CAL_RANGECAL_LO:
      SaveWordLsb(&ScaleData.Calibration.RangeCalibration, Value);
      return MB_RESULT_OK;
    case MBREG_CAL_RANGE_HI:
      SaveWordMsb(&ScaleData.Calibration.Range, Value);
      return MB_RESULT_OK;
    case MBREG_CAL_RANGE_LO:
      SaveWordLsb(&ScaleData.Calibration.Range, Value);
      return MB_RESULT_OK;
    case MBREG_CAL_DIVISION:
      ScaleData.Calibration.Division = Value;
      return MB_RESULT_OK;


    default:
      return MB_RESULT_INVALID_ADDRESS;

  }//switch
}

//---------------------------------------------------------------------------
// Nacteni diagnostickych citacu
//---------------------------------------------------------------------------

/*TModbusScaleResult TModbusScale::ReadCounters() {
  // Nacte diagnosticke citace
  TModbusScaleResult Result;

  try {

    MBReadTotalCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

    MBReadErrorCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

    MBReadExceptionCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

    MBReadMessageCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

    MBReadNoResponseCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

    MBReadNakCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

    MBReadBusyCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

    MBReadOverrunCount();
    MessageCounter++;     // Prictu pocet zprav
    if ((Result = ReadReply()) != MB_RESULT_OK) {
      return Result;
    }

  } catch (...) {
    return MB_RESULT_ERROR;     // Port neni dostupny
  }

  // Ulozim
  Counters.Total     = MbcbCounters.Total;
  Counters.Error     = MbcbCounters.Error;
  Counters.Exception = MbcbCounters.Exception;
  Counters.Message   = MbcbCounters.Message;
  Counters.Response  = MbcbCounters.Response;
  Counters.Nak       = MbcbCounters.Nak;
  Counters.Busy      = MbcbCounters.Busy;
  Counters.Overrun   = MbcbCounters.Overrun;

  return MB_RESULT_OK;
}

TModbusScaleResult TModbusScale::ReadCountersRep(int Repetitions) {
  // Nacte diagnosticke citace, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = ReadCounters();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}*/

//---------------------------------------------------------------------------
// Start vazeni
//---------------------------------------------------------------------------

#define SaveStartWeighingRegister(Register, Value)      Data[Register - MBREG_WEIGHING_ONLINE] = Value

TModbusScaleResult TModbusScale::StartWeighing(TModbusStartParameters *ModbusStartParameters) {
  // Zahaji vazeni s parametry <ModbusStartParameters>
  unsigned short int Data[MBREG_WEIGHING_DATETIME_MIN - MBREG_WEIGHING_ONLINE + 1];
  TModbusScaleResult Result;

  // Ulozim parametry do pole
  SaveStartWeighingRegister(MBREG_WEIGHING_ONLINE,             ModbusStartParameters->Online);
  SaveStartWeighingRegister(MBREG_WEIGHING_INITIALDAYNUMBER,   ModbusStartParameters->InitialDayNumber);
  SaveStartWeighingRegister(MBREG_WEIGHING_FLOCKNUMBER,        ModbusStartParameters->FlockNumber);
  SaveStartWeighingRegister(MBREG_WEIGHING_QW_USEBOTHGENDERS,  ModbusStartParameters->QuickWeighing.UseBothGenders);
  SaveStartWeighingRegister(MBREG_WEIGHING_QW_INITIALWFEMALES, ModbusStartParameters->QuickWeighing.InitialWeight[GENDER_FEMALE]);
  SaveStartWeighingRegister(MBREG_WEIGHING_QW_INITIALWMALES,   ModbusStartParameters->QuickWeighing.InitialWeight[GENDER_MALE]);
  SaveStartWeighingRegister(MBREG_WEIGHING_WAITINGFORSTART,    ModbusStartParameters->WaitingForStart);
  SaveStartWeighingRegister(MBREG_WEIGHING_COMPARISONFLOCK,    ModbusStartParameters->ComparisonFlock);
  SaveStartWeighingRegister(MBREG_WEIGHING_DATETIME_DAY,       ModbusStartParameters->DateTime.Day);
  SaveStartWeighingRegister(MBREG_WEIGHING_DATETIME_MONTH,     ModbusStartParameters->DateTime.Month);
  SaveStartWeighingRegister(MBREG_WEIGHING_DATETIME_YEAR,      ModbusStartParameters->DateTime.Year);
  SaveStartWeighingRegister(MBREG_WEIGHING_DATETIME_HOUR,      ModbusStartParameters->DateTime.Hour);
  SaveStartWeighingRegister(MBREG_WEIGHING_DATETIME_MIN,       ModbusStartParameters->DateTime.Min);

  // Zapisu parametry do vahy
  if ((Result = WriteRegisters(MBREG_WEIGHING_ONLINE, MBREG_WEIGHING_DATETIME_MIN, Data)) != MB_RESULT_OK) {
    return Result;
  }

  // Odstartuju vazeni podle zapsanych parametru
  return WriteRegister(MBREG_WEIGHING_START, 0);
}

TModbusScaleResult TModbusScale::StartWeighingRep(TModbusStartParameters *ModbusStartParameters, int Repetitions) {
  // Zahaji vazeni s parametry <ModbusStartParameters>, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = StartWeighing(ModbusStartParameters);
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Pauza vazeni
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::PauseWeighing(bool Pause) {
  // Zapauzuje/odpauzuje vazeni
  return WriteRegister(MBREG_WEIGHING_PAUSE, Pause);
}

TModbusScaleResult TModbusScale::PauseWeighingRep(bool Pause, int Repetitions) {
  // Zapauzuje/odpauzuje vazeni, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = PauseWeighing(Pause);
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Stop vazeni
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::StopWeighing() {
  // Stop vazeni
  return WriteRegister(MBREG_WEIGHING_STOP, 0);
}

TModbusScaleResult TModbusScale::StopWeighingRep(int Repetitions) {
  // Stop vazeni, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = StopWeighing();
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Nastaveni data a casu
//---------------------------------------------------------------------------

#define SaveDateTimeRegister(Register, Value)      Data[Register - MBREG_DATETIME_DAY] = Value

TModbusScaleResult TModbusScale::SetDateTime(TLongDateTime *DateTime) {
  // Nastavi datum a cas na <DateTime>
  unsigned short int Data[MBREG_DATETIME_MIN - MBREG_DATETIME_DAY + 1];
  TModbusScaleResult Result;

  // Ulozim parametry do pole
  SaveDateTimeRegister(MBREG_DATETIME_DAY,   DateTime->Day);
  SaveDateTimeRegister(MBREG_DATETIME_MONTH, DateTime->Month);
  SaveDateTimeRegister(MBREG_DATETIME_YEAR,  DateTime->Year);
  SaveDateTimeRegister(MBREG_DATETIME_HOUR,  DateTime->Hour);
  SaveDateTimeRegister(MBREG_DATETIME_MIN,   DateTime->Min);

  // Zapisu datum a cas do vahy
  if ((Result = WriteRegisters(MBREG_DATETIME_DAY, MBREG_DATETIME_MIN, Data)) != MB_RESULT_OK) {
    return Result;
  }

  // Nastavim cas
  return WriteRegister(MBREG_DATETIME_SAVE, 0);
}

TModbusScaleResult TModbusScale::SetDateTimeRep(TLongDateTime *DateTime, int Repetitions) {
  // Zahaji vazeni s parametry <ModbusStartParameters>, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = SetDateTime(DateTime);
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Nastaveni porovnani
//---------------------------------------------------------------------------

TModbusScaleResult TModbusScale::SetCompare(int Flock) {
  // Nastavi porovnani na hejno <Flock>
  TModbusScaleResult Result;

  // Zapisu hejno do vahy
  if ((Result = WriteRegister(MBREG_CONFIG_COMPARISONFLOCK, Flock)) != MB_RESULT_OK) {
    return Result;
  }

  // Ulozim
  return WriteRegister(MBREG_CONFIG_SAVE, 0);
}

TModbusScaleResult TModbusScale::SetCompareRep(int Flock, int Repetitions) {
  // Nastavi porovnani na hejno <Flock>, pri chybe zkusi <Repetitions> krat opakovat.
  int ErrorCounter = 0;
  TModbusScaleResult Result;

  do {
    Result = SetCompare(Flock);
    if (Result == MB_RESULT_OK || Result == MB_RESULT_INVALID_ADDRESS || Result == MB_RESULT_INVALID_VALUE) {
      return Result;                    // V poradku nebo neplatna adresa / hodnota - neni treba nic opakovat
    }
  } while (++ErrorCounter < Repetitions);

  // Nepodarilo se ani na vicekrat, vaha neodpovida
  return Result;
}

//---------------------------------------------------------------------------
// Sestaveni dat do struktury odpovidajici pametovemu modulu
//---------------------------------------------------------------------------

void TModbusScale::BuildModuleData(byte *Data) {
  // Sestavi dosud nactena data do struktury odpovidajici pametovemu modulu a ulozi ji do <Data>.
  TConfigSection    ConfigSection;
  TArchiveDailyInfo ArchiveDailyInfo;

  // U vsech vicebajtovych polozek je treba prohodit LSB a MSB tak, aby data odpovidala datum stazenym primo z vahy.

  // Konfigurace
  ConfigSection.Config = ScaleData.Config;
  KConvertSwapWord(&ConfigSection.Config.Version);
  KConvertSwapWord(&ConfigSection.Config.WeighingStart.CurveDayShift);
  KConvertSwapWord(&ConfigSection.Config.WeighingStart.DateTime.Year);
  KConvertSwapWord(&ConfigSection.Config.WeighingStart.QuickWeighing.InitialWeight[GENDER_FEMALE]);
  KConvertSwapWord(&ConfigSection.Config.WeighingStart.QuickWeighing.InitialWeight[GENDER_MALE]);
  KConvertSwapWord(&ConfigSection.Config.Gsm.DayMidnight1);
  KConvertSwapDWord(&ConfigSection.Config.Rs485.Speed);
  KConvertSwapWord(&ConfigSection.Config.Rs485.ReplyDelay);
  KConvertSwapWord(&ConfigSection.Config.WeightCorrection.Day1);
  KConvertSwapWord(&ConfigSection.Config.WeightCorrection.Day2);

  // Hejna
  for (int Flock = 0; Flock < FLOCKS_MAX_COUNT; Flock++) {
    ConfigSection.Flocks[Flock] = ScaleData.Flocks[Flock];
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      KConvertSwapWord(&ConfigSection.Flocks[Flock].GrowthCurve[GENDER_FEMALE][i].Day);
      KConvertSwapWord(&ConfigSection.Flocks[Flock].GrowthCurve[GENDER_FEMALE][i].Weight);
      KConvertSwapWord(&ConfigSection.Flocks[Flock].GrowthCurve[GENDER_MALE][i].Day);
      KConvertSwapWord(&ConfigSection.Flocks[Flock].GrowthCurve[GENDER_MALE][i].Weight);
    }
  }

  // Kalibrace
  ConfigSection.Calibration = ScaleData.Calibration;
  KConvertSwapLong(&ConfigSection.Calibration.ZeroCalibration);
  KConvertSwapLong(&ConfigSection.Calibration.RangeCalibration);
  KConvertSwapLong(&ConfigSection.Calibration.Range);

  // Ulozim ConfigSection do dat
  Data += FL_CONFIG_BASE;       // Zacatek configu 
  memcpy(Data, &ConfigSection, sizeof(TConfigSection));

  // Archiv
  Data += FL_ARCHIVE_BASE;      // Posunu se na zacatek archivu
  for (vector<TArchive *>::iterator Iterator = ScaleData.ArchiveList.begin(); Iterator != ScaleData.ArchiveList.end(); Iterator++) {
    ArchiveDailyInfo.Archive = *(*Iterator);

    KConvertSwapWord(&ArchiveDailyInfo.Archive.DayNumber);
    KConvertSwapWord(&ArchiveDailyInfo.Archive.DateTime.Year);
    for (int Gender = GENDER_FEMALE; Gender < _GENDER_COUNT; Gender++) {
      KConvertSwapFloat(&ArchiveDailyInfo.Archive.Stat[Gender].XSuma);
      KConvertSwapFloat(&ArchiveDailyInfo.Archive.Stat[Gender].X2Suma);
      KConvertSwapWord( &ArchiveDailyInfo.Archive.Stat[Gender].Count);
      KConvertSwapLong((int *)&ArchiveDailyInfo.Archive.Hist[Gender].Center);
      KConvertSwapLong((int *)&ArchiveDailyInfo.Archive.Hist[Gender].Step);
      for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
        KConvertSwapWord(&ArchiveDailyInfo.Archive.Hist[Gender].Slot[i]);
      }
      KConvertSwapWord(&ArchiveDailyInfo.Archive.TargetWeight[Gender]);
      KConvertSwapWord(&ArchiveDailyInfo.Archive.LastAverage[Gender]);
    }

    // Logger snuluju
    ArchiveDailyInfo.Samples[0].Flag = LG_SAMPLE_EMPTY;

    // Ulozim archiv do dat
    memcpy(Data, &ArchiveDailyInfo, sizeof(TArchiveDailyInfo));

    // Posunu se na dalsi den a ulozim naraznik
    Data += sizeof(TArchiveDailyInfo);
    *Data = FL_ARCHIVE_EMPTY;
  }
}

//-----------------------------------------------------------------------------
// Kontrola pismene nazvu
//-----------------------------------------------------------------------------

bool TModbusScale::CheckLetter(byte ch) {
  // Zkontroluje hodnotu znaku <ch>
  if (ch >= 'A' && ch <= 'Z') {
    return YES;
  }
  if (ch >= '0' && ch <= '9') {
    return YES;
  }
  if (ch == ' ') {
    return YES;
  }
  return NO;
}

//-----------------------------------------------------------------------------
// Kontrola cislice v identifikacnim cisle
//-----------------------------------------------------------------------------

bool TModbusScale::CheckIdLetter(byte ch) {
  // Zkontroluje hodnotu znaku <ch>
  if (ch >= '0' && ch <= '9') {
    return YES;
  }
  if (ch == ' ') {
    return YES;
  }
  return NO;
}

//-----------------------------------------------------------------------------
// Zapis LSB a MSB
//-----------------------------------------------------------------------------

void TModbusScale::SaveWordMsb(void *Data, unsigned short int Value) {
  // Ulozi do <Data> hodnotu <Value> jako 2 bajtovy MSB
  dword *p = (dword *)Data;
  *p &= 0x0000FFFF;
  *p |= Value << 16;
}

void TModbusScale::SaveWordLsb(void *Data, unsigned short int Value) {
  // Ulozi do <Data> hodnotu <Value> jako 2 bajtovy LSB
  dword *p = (dword *)Data;
  *p &= 0xFFFF0000;
  *p |= Value;
}

//-----------------------------------------------------------------------------
// Default nastaveni
//-----------------------------------------------------------------------------

void TModbusScale::SetDefaultConfig(TConfig *Config) {
  // Nastavi implicitni hodnoty parametru, ktere se z vahy nenacitaji
  byte i, j;

  Config->Gsm.Use                        = DEFAULT_GSM_USE;
  Config->Gsm.SendMidnight               = DEFAULT_GSM_SEND_MIDNIGHT;
  Config->Gsm.PeriodMidnight1            = DEFAULT_GSM_PERIOD_MIDNIGHT;
  Config->Gsm.DayMidnight1               = DEFAULT_GSM_DAY_MIDNIGHT;
  Config->Gsm.PeriodMidnight2            = DEFAULT_GSM_PERIOD_MIDNIGHT;
  Config->Gsm.MidnightSendHour           = DEFAULT_GSM_SEND_HOUR_MIDNIGHT;
  Config->Gsm.MidnightSendMin            = DEFAULT_GSM_SEND_MIN_MIDNIGHT;
  Config->Gsm.SendOnRequest              = DEFAULT_GSM_SEND_REQUEST;
  Config->Gsm.CheckNumbers               = DEFAULT_GSM_CHECK_NUMBERS;
  Config->Gsm.NumberCount                = DEFAULT_GSM_NUMBER_COUNT;
  for (j = 0; j < GSM_NUMBER_MAX_COUNT; j++) {
    for (i = 0; i < GSM_NUMBER_MAX_LENGTH; i++) {
      Config->Gsm.Numbers[j][i] = ' ';
    }
  }

  // Nastavim default hodnoty i u kompenzace, vahy 1.10 ji nemaji
  Config->WeightCorrection.Day1       = 0;
  Config->WeightCorrection.Day2       = 0;
  Config->WeightCorrection.Correction = 0;

  // Reserved bajty nastavim default na 0
  for (j = 0; j < CONFIG_RESERVED_SIZE; j++) {
    Config->Reserved[j] = 0;
  }
}
