//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Input.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TInputForm *InputForm;
//---------------------------------------------------------------------------
__fastcall TInputForm::TInputForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TInputForm::Button1Click(TObject *Sender)
{
  if (Edit->Text.IsEmpty()) ModalResult=mrCancel; else ModalResult=mrOk;         
}
//---------------------------------------------------------------------------
void __fastcall TInputForm::FormShow(TObject *Sender)
{
  Edit->SetFocus();        
}
//---------------------------------------------------------------------------
