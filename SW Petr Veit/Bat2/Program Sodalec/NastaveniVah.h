//---------------------------------------------------------------------------

#ifndef NastaveniVahH
#define NastaveniVahH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <DBCtrls.hpp>
#include <Mask.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------



#define ID_WITH_SETUP           ""       // Identifikacni cislo modulu, ktery obsahuje pouze nastaveni (nepovolim stahovani dat)



class TNastaveniVahForm : public TForm
{
__published:	// IDE-managed Components
        TButton *NewButton;
        TButton *SaveButton;
        TButton *UploadButton;
        TListBox *SeznamListBox;
        TLabel *Label1;
        TPageControl *PageControl;
        TTabSheet *StatistikaTabSheet;
        TTabSheet *HejnaTabSheet;
        TButton *DownloadButton;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TDBEdit *RozsahUniformityDBEdit;
        TLabel *Label7;
        TLabel *Label6;
        TDBEdit *RozsahHistogramuDBEdit;
        TLabel *Label5;
        TTabSheet *GsmTabSheet;
        TTabSheet *VahyTabSheet;
        TDBCheckBox *DBCheckBox1;
        TDBCheckBox *DBCheckBox2;
        TDBCheckBox *DBCheckBox3;
        TDBCheckBox *DBCheckBox4;
        TDBEdit *DBEdit3;
        TLabel *Label8;
        TDBEdit *DBEdit4;
        TDBEdit *DBEdit5;
        TDBEdit *DBEdit6;
        TDBEdit *DBEdit7;
        TLabel *Label9;
        TDBEdit *SamiceOkoliNadDBEdit;
        TLabel *Label10;
        TLabel *Label11;
        TDBEdit *SamiceOkoliPodDBEdit;
        TLabel *Label12;
        TLabel *Label13;
        TDBEdit *SamciOkoliNadDBEdit;
        TLabel *Label14;
        TLabel *Label15;
        TDBEdit *SamciOkoliPodDBEdit;
        TLabel *Label16;
        TLabel *Label17;
        TLabel *Label18;
        TDBEdit *UstaleniDBEdit;
        TLabel *Label19;
        TLabel *Label20;
        TDBEdit *DobaUstaleniDBEdit;
        TListBox *SeznamHejnListBox;
        TButton *DeleteButton;
        TButton *NewFlockButton;
        TButton *SaveFlockButton;
        TLabel *Label23;
        TButton *DeleteFlockButton;
        TDBRadioGroup *DBRadioGroup1;
        TPanel *HejnaPanel;
        TLabel *Label22;
        TLabel *Label24;
        TLabel *Label25;
        TDBEdit *JmenoDBEdit;
        TDBEdit *DBEdit15;
        TDBEdit *DBEdit16;
        TPageControl *KrivkyPageControl;
        TTabSheet *KrivkaSamiceTabSheet;
        TStringGrid *KrivkaSamiceStringGrid;
        TTabSheet *KrivkaSamciTabSheet;
        TStringGrid *KrivkaSamciStringGrid;
        TTabSheet *PocatecniHmotnostTabSheet;
        TLabel *Label26;
        TDBEdit *PocatecniHmotnostSamiceDBEdit;
        TPanel *PocatecniHmotnostSamciPanel;
        TLabel *Label27;
        TDBEdit *PocatecniHmotnostSamciDBEdit;
        TCheckBox *PohlaviCheckBox;
        TCheckBox *KrivkyCheckBox;
        TButton *Button1;
        TLabel *Label21;
        TDBEdit *FiltrDBEdit;
        TTabSheet *PodsvitTabSheet;
        TDBRadioGroup *DBRadioGroup2;
        TLabel *Label48;
        TDBEdit *Period1DBEdit;
        TLabel *Label49;
        TCheckBox *NastaveniGainCheckBox;
        TTabSheet *Rs485TabSheet;
        TLabel *Label51;
        TLabel *Label55;
        TDBRadioGroup *Rs485ParityDBRadioGroup;
        TLabel *Label53;
        TLabel *Label54;
        TDBEdit *Rs485ReplyDelayDBEdit;
        TDBEdit *Rs485SilentIntervalDBEdit;
        TLabel *Label57;
        TLabel *Label56;
        TDBComboBox *Rs485SpeedDBComboBox;
        TDBRadioGroup *DBRadioGroup3;
        TButton *FemalesLoadButton;
        TButton *MalesLoadButton;
        TDBRadioGroup *DBRadioGroup4;
        TTabSheet *CorrectionTabSheet;
        TLabel *Label61;
        TLabel *Label64;
        TLabel *Label62;
        TDBEdit *CorrectionCorrectionDBEdit;
        TDBEdit *CorrectionDay2DBEdit;
        TDBEdit *CorrectionDay1DBEdit;
        TLabel *Label63;
        TLabel *Label65;
        TCheckBox *UseCorrectionCheckBox;
        TLabel *Label28;
        TDBEdit *Day1DBEdit;
        TLabel *Label30;
        TDBEdit *Period2DBEdit;
        TLabel *Label31;
        TLabel *Label29;
        TDBEdit *HourDBEdit;
        TDBEdit *MinDBEdit;
        TLabel *Label32;
        void __fastcall SaveButtonClick(TObject *Sender);
        void __fastcall DBEdit3KeyPress(TObject *Sender, char &Key);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall NewButtonClick(TObject *Sender);
        void __fastcall NewFlockButtonClick(TObject *Sender);
        void __fastcall SeznamListBoxClick(TObject *Sender);
        void __fastcall SeznamHejnListBoxClick(TObject *Sender);
        void __fastcall SaveFlockButtonClick(TObject *Sender);
        void __fastcall PohlaviCheckBoxClick(TObject *Sender);
        void __fastcall KrivkyCheckBoxClick(TObject *Sender);
        void __fastcall DeleteFlockButtonClick(TObject *Sender);
        void __fastcall DeleteButtonClick(TObject *Sender);
        void __fastcall UploadButtonClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall DownloadButtonClick(TObject *Sender);
        void __fastcall NastaveniGainCheckBoxClick(TObject *Sender);
        void __fastcall FemalesLoadButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
  void UlozKonfiguraciDoTabulky();
  void UlozHejnaDoTabulky();

  bool UlozKonfiguraciDoModulu();

  void UlozSaduDoStruktury(int Version);
  void UlozHejnaDoStruktury();

  void NactiSady();
  void VyberPrvniSadu();
  void VyberSadu(String Sada);
  bool NovaSada(bool CreateDefaultFlock);

  void InicializaceZobrazeni();
  void NactiSeznamHejn(String Sada);
  void VyberPrvnihejno();
  void ZobrazSadu(String Sada);
  void ZobrazKrivkyHejna();
  void ZobrazKrivkuHejna(bool Pohlavi);
  void UlozKrivkuHejna(bool Pohlavi);
  void ZobrazHejno(int Hejno);

  void SetridKrivky();

  void VyplnDefaultHodnotySady();

  bool KontrolaRozsahuKrivky(bool Pohlavi);
  bool KontrolaRozsahuHejna();
  bool KontrolaRozsahuSady();

        __fastcall TNastaveniVahForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TNastaveniVahForm *NastaveniVahForm;
//---------------------------------------------------------------------------
#endif
