object NastaveniVahForm: TNastaveniVahForm
  Left = 358
  Top = 240
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Setup'
  ClientHeight = 413
  ClientWidth = 591
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 24
    Height = 13
    Caption = 'Sets:'
  end
  object NewButton: TButton
    Left = 16
    Top = 248
    Width = 121
    Height = 25
    Caption = 'New'
    TabOrder = 1
    OnClick = NewButtonClick
  end
  object SaveButton: TButton
    Left = 16
    Top = 280
    Width = 121
    Height = 25
    Caption = 'Save'
    TabOrder = 2
    OnClick = SaveButtonClick
  end
  object UploadButton: TButton
    Left = 16
    Top = 344
    Width = 121
    Height = 25
    Caption = 'Upload'
    TabOrder = 4
    OnClick = UploadButtonClick
  end
  object SeznamListBox: TListBox
    Left = 16
    Top = 32
    Width = 121
    Height = 201
    ItemHeight = 13
    Sorted = True
    TabOrder = 0
    OnClick = SeznamListBoxClick
  end
  object PageControl: TPageControl
    Left = 152
    Top = 16
    Width = 425
    Height = 385
    ActivePage = GsmTabSheet
    TabOrder = 6
    object HejnaTabSheet: TTabSheet
      Caption = 'Flocks'
      ImageIndex = 1
      object Label23: TLabel
        Left = 16
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Flocks:'
      end
      object SeznamHejnListBox: TListBox
        Left = 16
        Top = 24
        Width = 75
        Height = 217
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
        OnClick = SeznamHejnListBoxClick
      end
      object NewFlockButton: TButton
        Left = 16
        Top = 256
        Width = 75
        Height = 25
        Caption = 'New'
        TabOrder = 1
        OnClick = NewFlockButtonClick
      end
      object SaveFlockButton: TButton
        Left = 16
        Top = 288
        Width = 75
        Height = 25
        Caption = 'Save'
        TabOrder = 2
        OnClick = SaveFlockButtonClick
      end
      object DeleteFlockButton: TButton
        Left = 16
        Top = 320
        Width = 75
        Height = 25
        Caption = 'Delete'
        TabOrder = 3
        OnClick = DeleteFlockButtonClick
      end
      object HejnaPanel: TPanel
        Left = 104
        Top = 8
        Width = 241
        Height = 345
        BevelOuter = bvNone
        TabOrder = 4
        object Label22: TLabel
          Left = 8
          Top = 16
          Width = 31
          Height = 13
          Caption = 'Name:'
        end
        object Label24: TLabel
          Left = 8
          Top = 104
          Width = 57
          Height = 13
          Caption = 'Weigh from:'
        end
        object Label25: TLabel
          Left = 136
          Top = 104
          Width = 12
          Height = 13
          Caption = 'till:'
        end
        object JmenoDBEdit: TDBEdit
          Left = 88
          Top = 13
          Width = 129
          Height = 21
          DataField = 'NAME'
          DataSource = Data.NastaveniHejnDataSource
          TabOrder = 0
        end
        object DBEdit15: TDBEdit
          Left = 80
          Top = 101
          Width = 25
          Height = 21
          DataField = 'WEIGH_FROM'
          DataSource = Data.NastaveniHejnDataSource
          TabOrder = 3
        end
        object DBEdit16: TDBEdit
          Left = 160
          Top = 101
          Width = 25
          Height = 21
          DataField = 'WEIGH_TO'
          DataSource = Data.NastaveniHejnDataSource
          TabOrder = 4
        end
        object KrivkyPageControl: TPageControl
          Left = 8
          Top = 136
          Width = 225
          Height = 201
          ActivePage = KrivkaSamiceTabSheet
          TabOrder = 5
          object KrivkaSamiceTabSheet: TTabSheet
            Caption = 'Females'
            object KrivkaSamiceStringGrid: TStringGrid
              Left = 0
              Top = 0
              Width = 217
              Height = 129
              Align = alTop
              ColCount = 3
              DefaultRowHeight = 16
              RowCount = 31
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
              TabOrder = 0
              ColWidths = (
                25
                66
                93)
              RowHeights = (
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16
                16)
            end
            object FemalesLoadButton: TButton
              Left = 135
              Top = 140
              Width = 75
              Height = 25
              Caption = 'Load'
              TabOrder = 1
              OnClick = FemalesLoadButtonClick
            end
          end
          object KrivkaSamciTabSheet: TTabSheet
            Caption = 'Males'
            ImageIndex = 1
            object KrivkaSamciStringGrid: TStringGrid
              Left = 0
              Top = 0
              Width = 217
              Height = 129
              ColCount = 3
              DefaultRowHeight = 16
              RowCount = 31
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goAlwaysShowEditor]
              TabOrder = 0
              ColWidths = (
                25
                66
                93)
            end
            object MalesLoadButton: TButton
              Left = 135
              Top = 140
              Width = 75
              Height = 25
              Caption = 'Load'
              TabOrder = 1
              OnClick = FemalesLoadButtonClick
            end
          end
          object PocatecniHmotnostTabSheet: TTabSheet
            Caption = 'Initial weight'
            ImageIndex = 2
            object Label26: TLabel
              Left = 16
              Top = 24
              Width = 42
              Height = 13
              Caption = 'Females:'
            end
            object PocatecniHmotnostSamiceDBEdit: TDBEdit
              Left = 96
              Top = 21
              Width = 97
              Height = 21
              DataField = 'FEMALE_WEIGHT00'
              DataSource = Data.NastaveniHejnDataSource
              TabOrder = 0
            end
            object PocatecniHmotnostSamciPanel: TPanel
              Left = 8
              Top = 48
              Width = 201
              Height = 33
              BevelOuter = bvNone
              TabOrder = 1
              object Label27: TLabel
                Left = 8
                Top = 3
                Width = 31
                Height = 13
                Caption = 'Males:'
              end
              object PocatecniHmotnostSamciDBEdit: TDBEdit
                Left = 88
                Top = 0
                Width = 97
                Height = 21
                DataField = 'MALE_WEIGHT00'
                DataSource = Data.NastaveniHejnDataSource
                TabOrder = 0
              end
            end
          end
        end
        object PohlaviCheckBox: TCheckBox
          Left = 8
          Top = 48
          Width = 209
          Height = 17
          Caption = 'Use both genders'
          TabOrder = 1
          OnClick = PohlaviCheckBoxClick
        end
        object KrivkyCheckBox: TCheckBox
          Left = 8
          Top = 72
          Width = 209
          Height = 17
          Caption = 'Use growth curves'
          TabOrder = 2
          OnClick = KrivkyCheckBoxClick
        end
      end
      object Button1: TButton
        Left = 288
        Top = 72
        Width = 51
        Height = 17
        Caption = 'Button1'
        TabOrder = 5
        Visible = False
        OnClick = Button1Click
      end
    end
    object StatistikaTabSheet: TTabSheet
      Caption = 'Statistics'
      object Label2: TLabel
        Left = 16
        Top = 24
        Width = 80
        Height = 13
        Caption = 'Histogram range:'
      end
      object Label3: TLabel
        Left = 16
        Top = 48
        Width = 79
        Height = 13
        Caption = 'Uniformity range:'
      end
      object Label4: TLabel
        Left = 134
        Top = 48
        Width = 6
        Height = 13
        Caption = '�'
      end
      object Label7: TLabel
        Left = 184
        Top = 48
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label6: TLabel
        Left = 184
        Top = 24
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label5: TLabel
        Left = 134
        Top = 24
        Width = 6
        Height = 13
        Caption = '�'
      end
      object RozsahUniformityDBEdit: TDBEdit
        Left = 144
        Top = 45
        Width = 33
        Height = 21
        DataField = 'UNI_RANGE'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 1
      end
      object RozsahHistogramuDBEdit: TDBEdit
        Left = 144
        Top = 21
        Width = 33
        Height = 21
        DataField = 'HIST_RANGE'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 0
      end
    end
    object GsmTabSheet: TTabSheet
      Caption = 'GSM'
      ImageIndex = 2
      object Label8: TLabel
        Left = 16
        Top = 208
        Width = 45
        Height = 13
        Caption = 'Numbers:'
      end
      object Label48: TLabel
        Left = 34
        Top = 72
        Width = 42
        Height = 13
        Caption = 'Period 1:'
      end
      object Label49: TLabel
        Left = 130
        Top = 72
        Width = 22
        Height = 13
        Caption = 'days'
      end
      object Label28: TLabel
        Left = 34
        Top = 96
        Width = 31
        Height = 13
        Caption = 'Day 1:'
      end
      object Label30: TLabel
        Left = 34
        Top = 120
        Width = 42
        Height = 13
        Caption = 'Period 2:'
      end
      object Label31: TLabel
        Left = 130
        Top = 120
        Width = 22
        Height = 13
        Caption = 'days'
      end
      object Label29: TLabel
        Left = 200
        Top = 72
        Width = 78
        Height = 13
        Caption = 'Time of sending:'
      end
      object Label32: TLabel
        Left = 335
        Top = 72
        Width = 3
        Height = 13
        Caption = ':'
      end
      object DBCheckBox1: TDBCheckBox
        Left = 16
        Top = 24
        Width = 273
        Height = 17
        Caption = 'Use GSM communication'
        DataField = 'GSM_USE'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 0
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object DBCheckBox2: TDBCheckBox
        Left = 16
        Top = 48
        Width = 273
        Height = 17
        Caption = 'Send statistics at midnight'
        DataField = 'GSM_SEND_MIDNIGHT'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 1
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object DBCheckBox3: TDBCheckBox
        Left = 16
        Top = 152
        Width = 273
        Height = 17
        Caption = 'Send data on request'
        DataField = 'GSM_SEND_REQUEST'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 3
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object DBCheckBox4: TDBCheckBox
        Left = 16
        Top = 176
        Width = 273
        Height = 17
        Caption = 'Check numbers'
        DataField = 'GSM_CHECK_NUMBERS'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 4
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 224
        Width = 153
        Height = 21
        DataField = 'GSM_NUMBER00'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 5
        OnKeyPress = DBEdit3KeyPress
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 248
        Width = 153
        Height = 21
        DataField = 'GSM_NUMBER01'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 6
        OnKeyPress = DBEdit3KeyPress
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 272
        Width = 153
        Height = 21
        DataField = 'GSM_NUMBER02'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 7
        OnKeyPress = DBEdit3KeyPress
      end
      object DBEdit6: TDBEdit
        Left = 16
        Top = 296
        Width = 153
        Height = 21
        DataField = 'GSM_NUMBER03'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 8
        OnKeyPress = DBEdit3KeyPress
      end
      object DBEdit7: TDBEdit
        Left = 16
        Top = 320
        Width = 153
        Height = 21
        DataField = 'GSM_NUMBER04'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 9
        OnKeyPress = DBEdit3KeyPress
      end
      object Period1DBEdit: TDBEdit
        Left = 88
        Top = 69
        Width = 33
        Height = 21
        DataField = 'GSM_PERIOD_MIDNIGHT1'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 2
        TabOrder = 2
      end
      object Day1DBEdit: TDBEdit
        Left = 88
        Top = 93
        Width = 33
        Height = 21
        DataField = 'GSM_DAY_MIDNIGHT1'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 3
        TabOrder = 10
      end
      object Period2DBEdit: TDBEdit
        Left = 88
        Top = 117
        Width = 33
        Height = 21
        DataField = 'GSM_PERIOD_MIDNIGHT2'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 2
        TabOrder = 11
      end
      object HourDBEdit: TDBEdit
        Left = 296
        Top = 69
        Width = 33
        Height = 21
        DataField = 'GSM_MIDNIGHT_SEND_HOUR'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 2
        TabOrder = 12
      end
      object MinDBEdit: TDBEdit
        Left = 344
        Top = 69
        Width = 33
        Height = 21
        DataField = 'GSM_MIDNIGHT_SEND_MIN'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 2
        TabOrder = 13
      end
    end
    object Rs485TabSheet: TTabSheet
      Caption = 'RS-485'
      ImageIndex = 5
      object Label51: TLabel
        Left = 16
        Top = 24
        Width = 34
        Height = 13
        Caption = 'Speed:'
      end
      object Label55: TLabel
        Left = 232
        Top = 24
        Width = 13
        Height = 13
        Caption = 'Bd'
      end
      object Label53: TLabel
        Left = 16
        Top = 240
        Width = 58
        Height = 13
        Caption = 'Reply delay:'
      end
      object Label54: TLabel
        Left = 16
        Top = 264
        Width = 66
        Height = 13
        Caption = 'Silent interval:'
      end
      object Label57: TLabel
        Left = 184
        Top = 264
        Width = 13
        Height = 13
        Caption = 'ms'
      end
      object Label56: TLabel
        Left = 184
        Top = 240
        Width = 13
        Height = 13
        Caption = 'ms'
      end
      object Rs485ParityDBRadioGroup: TDBRadioGroup
        Left = 16
        Top = 56
        Width = 161
        Height = 73
        Caption = 'Parity'
        Columns = 2
        DataField = 'RS485_PARITY'
        DataSource = Data.NastaveniVahDataSource
        Items.Strings = (
          '8-n-1'
          '8-e-1'
          '8-o-1'
          '8-n-2')
        TabOrder = 1
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
      object Rs485ReplyDelayDBEdit: TDBEdit
        Left = 144
        Top = 237
        Width = 33
        Height = 21
        DataField = 'RS485_REPLY_DELAY'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 2
      end
      object Rs485SilentIntervalDBEdit: TDBEdit
        Left = 144
        Top = 261
        Width = 33
        Height = 21
        DataField = 'RS485_SILENT_INTERVAL'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 3
      end
      object Rs485SpeedDBComboBox: TDBComboBox
        Left = 144
        Top = 21
        Width = 81
        Height = 21
        Style = csDropDownList
        DataField = 'RS485_SPEED'
        DataSource = Data.NastaveniVahDataSource
        ItemHeight = 13
        Items.Strings = (
          '1200'
          '2400'
          '4800'
          '9600'
          '19200'
          '38400'
          '57600'
          '115200')
        TabOrder = 0
      end
      object DBRadioGroup4: TDBRadioGroup
        Left = 16
        Top = 144
        Width = 161
        Height = 73
        Caption = 'Protocol'
        DataField = 'RS485_PROTOCOL'
        DataSource = Data.NastaveniVahDataSource
        Items.Strings = (
          'MODBUS RTU'
          'MODBUS ASCII')
        TabOrder = 4
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
    end
    object VahyTabSheet: TTabSheet
      Caption = 'Scale'
      ImageIndex = 3
      object Label9: TLabel
        Left = 16
        Top = 24
        Width = 152
        Height = 13
        Caption = 'Margin above target for females:'
      end
      object Label10: TLabel
        Left = 268
        Top = 24
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label11: TLabel
        Left = 16
        Top = 48
        Width = 149
        Height = 13
        Caption = 'Margin under target for females:'
      end
      object Label12: TLabel
        Left = 268
        Top = 48
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label13: TLabel
        Left = 16
        Top = 72
        Width = 143
        Height = 13
        Caption = 'Margin above target for males:'
      end
      object Label14: TLabel
        Left = 268
        Top = 72
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label15: TLabel
        Left = 16
        Top = 96
        Width = 140
        Height = 13
        Caption = 'Margin under target for males:'
      end
      object Label16: TLabel
        Left = 268
        Top = 96
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label17: TLabel
        Left = 16
        Top = 144
        Width = 59
        Height = 13
        Caption = 'Stabilization:'
      end
      object Label18: TLabel
        Left = 222
        Top = 144
        Width = 6
        Height = 13
        Caption = '�'
      end
      object Label19: TLabel
        Left = 268
        Top = 144
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label20: TLabel
        Left = 16
        Top = 168
        Width = 81
        Height = 13
        Caption = 'Stabilization time:'
      end
      object Label21: TLabel
        Left = 16
        Top = 120
        Width = 25
        Height = 13
        Caption = 'Filter:'
      end
      object SamiceOkoliNadDBEdit: TDBEdit
        Left = 232
        Top = 21
        Width = 33
        Height = 21
        DataField = 'F_MARGIN_ABOVE'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 0
      end
      object SamiceOkoliPodDBEdit: TDBEdit
        Left = 232
        Top = 45
        Width = 33
        Height = 21
        DataField = 'F_MARGIN_UNDER'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 1
      end
      object SamciOkoliNadDBEdit: TDBEdit
        Left = 232
        Top = 69
        Width = 33
        Height = 21
        DataField = 'M_MARGIN_ABOVE'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 2
      end
      object SamciOkoliPodDBEdit: TDBEdit
        Left = 232
        Top = 93
        Width = 33
        Height = 21
        DataField = 'M_MARGIN_UNDER'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 3
      end
      object UstaleniDBEdit: TDBEdit
        Left = 232
        Top = 141
        Width = 33
        Height = 21
        DataField = 'STABILIZATION'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 5
      end
      object DobaUstaleniDBEdit: TDBEdit
        Left = 232
        Top = 165
        Width = 33
        Height = 21
        DataField = 'STABILIZATION_TIME'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 6
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 16
        Top = 296
        Width = 121
        Height = 49
        Caption = 'Units'
        Columns = 2
        DataField = 'UNITS'
        DataSource = Data.NastaveniVahDataSource
        Items.Strings = (
          'kg'
          'lb')
        TabOrder = 9
        Values.Strings = (
          '0'
          '1')
      end
      object FiltrDBEdit: TDBEdit
        Left = 232
        Top = 117
        Width = 33
        Height = 21
        DataField = 'FILTER_VALUE'
        DataSource = Data.NastaveniVahDataSource
        TabOrder = 4
      end
      object NastaveniGainCheckBox: TCheckBox
        Left = 16
        Top = 192
        Width = 249
        Height = 17
        Caption = 'Use gain in automatic mode'
        TabOrder = 7
        OnClick = NastaveniGainCheckBoxClick
      end
      object DBRadioGroup3: TDBRadioGroup
        Left = 16
        Top = 216
        Width = 249
        Height = 73
        Caption = 'Save sample upon'
        DataField = 'JUMP_MODE'
        DataSource = Data.NastaveniVahDataSource
        Items.Strings = (
          'Entering the scale'
          'Leaving the scale'
          'Both')
        TabOrder = 8
        Values.Strings = (
          '0'
          '1'
          '2')
      end
    end
    object CorrectionTabSheet: TTabSheet
      Caption = 'Correction'
      ImageIndex = 6
      object Label61: TLabel
        Left = 16
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Day 1:'
      end
      object Label64: TLabel
        Left = 16
        Top = 80
        Width = 31
        Height = 13
        Caption = 'Day 2:'
      end
      object Label62: TLabel
        Left = 16
        Top = 104
        Width = 51
        Height = 13
        Caption = 'Correction:'
      end
      object Label63: TLabel
        Left = 126
        Top = 104
        Width = 6
        Height = 13
        Caption = '+'
      end
      object Label65: TLabel
        Left = 172
        Top = 104
        Width = 8
        Height = 13
        Caption = '%'
      end
      object CorrectionCorrectionDBEdit: TDBEdit
        Left = 136
        Top = 101
        Width = 33
        Height = 21
        DataField = 'CORRECTION_CORRECTION'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 4
        TabOrder = 3
      end
      object CorrectionDay2DBEdit: TDBEdit
        Left = 136
        Top = 77
        Width = 33
        Height = 21
        DataField = 'CORRECTION_DAY2'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 3
        TabOrder = 2
      end
      object CorrectionDay1DBEdit: TDBEdit
        Left = 136
        Top = 53
        Width = 33
        Height = 21
        DataField = 'CORRECTION_DAY1'
        DataSource = Data.NastaveniVahDataSource
        MaxLength = 3
        TabOrder = 1
      end
      object UseCorrectionCheckBox: TCheckBox
        Left = 16
        Top = 24
        Width = 337
        Height = 17
        Caption = 'Use correction curve'
        TabOrder = 0
      end
    end
    object PodsvitTabSheet: TTabSheet
      Caption = 'Backlight'
      ImageIndex = 4
      object DBRadioGroup2: TDBRadioGroup
        Left = 16
        Top = 24
        Width = 201
        Height = 57
        Caption = 'Backlight'
        Columns = 3
        DataField = 'BACKLIGHT'
        DataSource = Data.NastaveniVahDataSource
        Items.Strings = (
          'Off'
          'On'
          'Auto')
        TabOrder = 0
        Values.Strings = (
          '0'
          '1'
          '2')
      end
    end
  end
  object DownloadButton: TButton
    Left = 16
    Top = 376
    Width = 121
    Height = 25
    Caption = 'Download'
    TabOrder = 5
    OnClick = DownloadButtonClick
  end
  object DeleteButton: TButton
    Left = 16
    Top = 312
    Width = 121
    Height = 25
    Caption = 'Delete'
    TabOrder = 3
    OnClick = DeleteButtonClick
  end
end
