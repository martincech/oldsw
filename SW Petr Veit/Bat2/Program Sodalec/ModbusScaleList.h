//***************************************************************************
//
//    ModbusScale.cpp - Scale connected to the Modbus network
//    Version 1.0
//
//***************************************************************************

#ifndef MODBUSSCALELIST_H
#define MODBUSSCALELIST_H

#include <vector.h>
#include <system.hpp>           // AnsiString

#include "ModbusScale.h"

//---------------------------------------------------------------------------
// Seznam vah
//---------------------------------------------------------------------------

class TModbusScaleList {

private:
  vector<TModbusScale *> ModbusScaleList;         // Seznam vah

  typedef vector<TModbusScale *>::iterator TModbusScaleListIterator;

  TModbusScaleListIterator GetIterator(int Index);
  bool CheckIndex(int Index);
  void Sort(void);

public:
  ~TModbusScaleList(void);

  void Clear(void);
  int Count(void);
  int GetIndex(int Address);
  bool Exists(int Address);
  bool Add(int Address, int Port, AnsiString Name);
  bool Delete(int Index);
  bool Edit(int Index, int Address, int Port, AnsiString Name);
  TModbusScale * GetScale(int Index);

};

#endif // MODBUSSCALELIST_H

