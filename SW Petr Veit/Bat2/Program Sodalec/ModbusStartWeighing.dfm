object ModbusStartWeighingForm: TModbusStartWeighingForm
  Left = 447
  Top = 174
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Start weighing'
  ClientHeight = 373
  ClientWidth = 536
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object WeightGroupBox: TGroupBox
    Left = 16
    Top = 16
    Width = 249
    Height = 241
    Caption = 'Weight'
    TabOrder = 0
    object Label1: TLabel
      Left = 48
      Top = 72
      Width = 122
      Height = 13
      Caption = 'Target weight for females:'
    end
    object Label2: TLabel
      Left = 48
      Top = 120
      Width = 113
      Height = 13
      Caption = 'Target weight for males:'
    end
    object UseDirectRadioButton: TRadioButton
      Left = 16
      Top = 24
      Width = 225
      Height = 17
      Caption = 'Use direct parameters:'
      TabOrder = 0
      OnClick = UseDirectRadioButtonClick
    end
    object UseBotheGendersCheckBox: TCheckBox
      Left = 48
      Top = 48
      Width = 193
      Height = 17
      Caption = 'Use both genders'
      TabOrder = 1
      OnClick = UseBotheGendersCheckBoxClick
    end
    object TargetFemalesEdit: TEdit
      Left = 48
      Top = 88
      Width = 185
      Height = 21
      MaxLength = 6
      TabOrder = 2
      Text = 'TargetFemalesEdit'
    end
    object TargetMalesEdit: TEdit
      Left = 48
      Top = 136
      Width = 185
      Height = 21
      MaxLength = 6
      TabOrder = 3
      Text = 'TargetMalesEdit'
    end
    object UseCurveRadioButton: TRadioButton
      Left = 16
      Top = 176
      Width = 225
      Height = 17
      Caption = 'Use a predefined flock:'
      TabOrder = 4
      OnClick = UseDirectRadioButtonClick
    end
    object FlockComboBox: TComboBox
      Left = 48
      Top = 200
      Width = 185
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 5
    end
  end
  object CompareGroupBox: TGroupBox
    Left = 280
    Top = 192
    Width = 241
    Height = 89
    Caption = 'Compare'
    TabOrder = 3
    object CompareCheckBox: TCheckBox
      Left = 16
      Top = 24
      Width = 193
      Height = 17
      Caption = 'Compare with:'
      TabOrder = 0
      OnClick = CompareCheckBoxClick
    end
    object CompareComboBox: TComboBox
      Left = 16
      Top = 48
      Width = 209
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object Button1: TButton
    Left = 352
    Top = 328
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 4
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 447
    Top = 328
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 5
  end
  object StartGroupBox: TGroupBox
    Left = 280
    Top = 16
    Width = 241
    Height = 161
    Caption = 'Start'
    TabOrder = 2
    object Label3: TLabel
      Left = 16
      Top = 24
      Width = 59
      Height = 13
      Caption = 'Starting day:'
    end
    object StartNowRadioButton: TRadioButton
      Left = 16
      Top = 72
      Width = 193
      Height = 17
      Caption = 'Start now'
      TabOrder = 1
      OnClick = StartNowRadioButtonClick
    end
    object StartLaterRadioButton: TRadioButton
      Left = 16
      Top = 96
      Width = 193
      Height = 17
      Caption = 'Start later:'
      TabOrder = 2
      OnClick = StartNowRadioButtonClick
    end
    object StartDatePicker: TDateTimePicker
      Left = 40
      Top = 120
      Width = 97
      Height = 21
      CalAlignment = dtaLeft
      Date = 39364.6041637731
      Time = 39364.6041637731
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkDate
      ParseInput = False
      TabOrder = 3
    end
    object StartTimePicker: TDateTimePicker
      Left = 144
      Top = 120
      Width = 81
      Height = 21
      CalAlignment = dtaLeft
      Date = 39364.6041637731
      Time = 39364.6041637731
      DateFormat = dfShort
      DateMode = dmComboBox
      Kind = dtkTime
      ParseInput = False
      TabOrder = 4
    end
    object StartingDayEdit: TEdit
      Left = 16
      Top = 41
      Width = 209
      Height = 21
      MaxLength = 3
      TabOrder = 0
      Text = 'StartingDayEdit'
    end
  end
  object ModeGroupBox: TGroupBox
    Left = 16
    Top = 272
    Width = 249
    Height = 81
    Caption = 'Mode'
    TabOrder = 1
    object StandardWeighingRadioButton: TRadioButton
      Left = 16
      Top = 24
      Width = 225
      Height = 17
      Caption = 'Standard weighing'
      TabOrder = 0
    end
    object OnlineWeighingRadioButton: TRadioButton
      Left = 16
      Top = 48
      Width = 225
      Height = 17
      Caption = 'Diagnostic online weighing'
      TabOrder = 1
    end
  end
end
