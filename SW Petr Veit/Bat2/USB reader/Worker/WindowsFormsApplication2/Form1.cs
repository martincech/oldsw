﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Veit.UsbReader;
using System.Runtime.InteropServices;
using Veit.StructConversion;
using Veit.Bat2Flash;
using Veit.BinaryFile;
using Veit.Bat2Stat;

namespace WindowsFormsApplication2 {
    public partial class Form1 : Form {
        private Bat2FlashReaderWorker readerWorker;
        private Button activeButton;
        
        public Form1() {
            InitializeComponent();

            readerWorker = new Bat2FlashReaderWorker(ReaderWorker_ProgressChanged, ReaderWorker_RunWorkerCompleted);
        }

        private void button2_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter   = "hardcopy file (*.bin)|*.bin";
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            byte[] data = BinaryFile.ReadFromFile(openFileDialog.FileName, AtFlash.FLASH_SIZE);

            Bat2Struct.TFlash flash = new Bat2Struct.TFlash();

            // Prevedu pole bajtu na strukturu
            flash = StructConversion.ArrayToStruct<Bat2Struct.TFlash>(data);
            flash.Swap();

            // Dekoder
            Bat2FlashDecoder decoder = new Bat2FlashDecoder(data);
            Bat2Struct.TArchive archive;
            decoder.FirstDay();
            while (decoder.NextDay(out archive)) {
                Bat2Stat stat = new Bat2Stat(archive.Stat[0]);
                int count = archive.Stat[0].Count;
                float average = stat.Average();
                float sigma = stat.Sigma();
                float cv = stat.Variation(average, sigma);
            }
        }

        private void button1_Click(object sender, EventArgs e) {
            AtFlash atFlash = new AtFlash();
            
            byte[] data;
            if (!atFlash.Read(0, out data, AtFlash.FLASH_SIZE)) {
                MessageBox.Show("Error");
                return;
            }

            BinaryFile.WriteToFile(data, "test.bin");
        }

        private void button3_Click(object sender, EventArgs e) {
            Bat2FlashReader flashReader = new Bat2FlashReader();
            if (!flashReader.IsModulePresent()) {
                MessageBox.Show("Module not connected");
                return;
            }

            // Nacteni configu
            if (!flashReader.ReadConfigSection()) {
                MessageBox.Show("Can't read config");
                return;
            }
            Bat2Struct.TFlash flash = new Bat2Struct.TFlash();
            flash = StructConversion.ArrayToStruct<Bat2Struct.TFlash>(flashReader.RawData);
            flash.Swap();
            
            // Nacteni archivu
            if (!flashReader.StartReadData()) {
                MessageBox.Show("Can't start");
                return;
            }

            bool done;
            while (flashReader.ReadPacket(out done)) {
                button3.Text = flashReader.ReadProgress.ToString("N1") + " %";
                button3.Refresh();
                if (done) {
                    BinaryFile.WriteToFile(flashReader.RawData, "test.bin");
                    MessageBox.Show("Finished");
                    return;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e) {
            activeButton = (Button)sender;
            if (readerWorker.IsBusy) {
                readerWorker.Stop();
            } else {
                activeButton.Text = "Reading";
                readerWorker.StartReadAll();
            }
        }

        private void button5_Click(object sender, EventArgs e) {
            activeButton = (Button)sender;
            if (readerWorker.IsBusy) {
                readerWorker.Stop();
            } else {
                activeButton.Text = "Reading";
                readerWorker.StartReadConfig();
            }
        }

        private void ReaderWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            button4.Text = "Reading (" + e.ProgressPercentage.ToString() + "%)";
        }

        private void ReaderWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //e.Cancelled: use this if you need to determine how the worker thread completed. 
            //             Must set e.Cancel to True in DoWork (False by default) for this be reliable

            //e.Result   : Like this.piCalcWorker.ReportProgress() allows us to pass a user token
            //             to ProgressChanged via the ProgressChangedEventArgs, you can pass a result
            //             object back to RunWorkerCompleted vie RunWorkerCompletedEventArgs.Result,
            //             but only if not cancelled, as e.Result, when cancelled, is an exception:
            //             Result	'e.Result' threw an exception of type 'System.InvalidOperationException'	object {System.InvalidOperationException}

            if (e.Cancelled) {
                // Rucne ukoncil
                activeButton.Text = "Cancelled";
                return;
            }

            WorkerResult result = (WorkerResult)e.Result;
            
            if (!result.IsSuccessful) {
                activeButton.Text = "Error";
                return;
            }

            BinaryFile.WriteToFile(result.Data, "test.bin");
            activeButton.Text = "OK";
        }

        private void button6_Click(object sender, EventArgs e) {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter   = "hardcopy file (*.bin)|*.bin";
            openFileDialog.FileName = "";
            if (openFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            byte[] data = BinaryFile.ReadFromFile(openFileDialog.FileName, AtFlash.FLASH_SIZE);
            AtFlash atFlash = new AtFlash();
            
            if (!atFlash.Write(0, data, AtFlash.FLASH_SIZE)) {
                MessageBox.Show("Error");
                return;
            }

        }

        private void button7_Click(object sender, EventArgs e) {
            AtFlash atFlash = new AtFlash();
            
            Button button = (Button)sender;

            byte[] data;
            int days = 0;
            for (int i = Bat2Struct.FL_ARCHIVE_BASE; i < AtFlash.FLASH_SIZE; i += Bat2Struct.FL_ARCHIVE_DAY_SIZE) {
                if (!atFlash.Read(i, out data, 1)) {
                    MessageBox.Show("Error");
                    return;
                }
                
                if (data[0] == Bat2Struct.FL_ARCHIVE_EMPTY) {
                    button.Text = days.ToString() + " days";
                    return;
                }

                days++;
            }

            button.Text = "Not found";
        }

    }
}
