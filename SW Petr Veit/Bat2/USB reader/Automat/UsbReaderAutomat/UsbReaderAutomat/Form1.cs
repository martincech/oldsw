﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Veit.Bat2Flash;
using Veit.BinaryFile;

namespace UsbReaderAutomat {
    public partial class Form1 : Form {
        public enum ModuleReadingState {
            NO_MODULE,      // Zadny modul neni vlozen
            CHECK,          // Modul zasunuty, kontrola modulu
            NO_DATA,        // Modul neobsahuje zadna data
            READING,        // Probiha cteni
            READ_FINISHED   // Vsechna data nactena
        }

        public  struct ModuleReadingData {
            /// <summary>
            /// Current state of reading
            /// </summary>
            public ModuleReadingState State;
            
            /// <summary>
            /// Identification number of the scale
            /// </summary>
            public int IdentificationNumber;
            
            /// <summary>
            /// Reading progress in percents
            /// </summary>
            public int Progress;
            
            /// <summary>
            /// Decoded data
            /// </summary>
            public Bat2FlashDecoder FlashDecoder;
        }
        
        private Bat2FlashReaderAutomat flashReaderAutomat;
        private ModuleReadingData moduleReadingData;

        public Form1() {
            InitializeComponent();
            flashReaderAutomat = new Bat2FlashReaderAutomat(ReaderAutomat_ProgressChanged, ReaderAutomat_RunWorkerCompleted);

            moduleReadingData = new ModuleReadingData();
            moduleReadingData.State = ModuleReadingState.NO_MODULE;
        }

        private void ShowProgress(bool visible) {
            progressBarReading.Visible = visible;
            labelProgress.Visible      = visible;
        }
        
        private void ShowResults(bool visible) {
            dataGridViewArchive.Visible = visible;
        }

        private void ShowProgressValue(int identificationNumber, int progress) {
            if (identificationNumber == 0) {
                labelReadStatus.Text = "Reading data";
            } else {
                labelReadStatus.Text = "Reading data (scale " + moduleReadingData.IdentificationNumber.ToString() + ")";
            }
            ShowProgress(true);
            ShowResults(false);
            progressBarReading.Value = progress;
            labelProgress.Text       = progress.ToString() + "%";
        }

        private void ShowArchive(Bat2FlashDecoder decoder) {
            dataGridViewArchive.Rows.Clear();

            Bat2Struct.TArchive archive;
            decoder.FirstDay();
            while (decoder.NextDay(out archive)) {
                dataGridViewArchive.Rows.Add(archive.DayNumber.ToString(),
                                             new DateTime(archive.DateTime.Year, archive.DateTime.Month, archive.DateTime.Day).ToShortDateString(),
                                             "",
                                             "");
            }

        }

        private void ShowReadState(ModuleReadingData moduleReadingData) {
            switch (moduleReadingData.State) {
                case ModuleReadingState.NO_MODULE:
                    labelReadStatus.Text = "Memory module not found";
                    ShowProgress(false);
                    ShowResults(false);
                    break;

                case ModuleReadingState.CHECK:
                    labelReadStatus.Text = "Checking data";
                    ShowProgress(false);
                    ShowResults(false);
                    break;

                case ModuleReadingState.NO_DATA:
                    labelReadStatus.Text = "Memory module contains no data";
                    ShowProgress(false);
                    ShowResults(false);
                    break;

                case ModuleReadingState.READING:
                    ShowProgressValue(moduleReadingData.IdentificationNumber, moduleReadingData.Progress);
                    break;

                case ModuleReadingState.READ_FINISHED:
                    labelReadStatus.Text = "Reading finished";
                    ShowProgress(false);
                    ShowResults(true);
                    ShowArchive(moduleReadingData.FlashDecoder);
                    break;
            }
        }
        
        private void ReaderAutomat_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            WorkerData data = (WorkerData)e.UserState;
            buttonStart.Text = data.WorkerStatus.ToString() + " (" + e.ProgressPercentage.ToString() + "%)";
            listBox1.Items.Add(buttonStart.Text);
            listBox1.SelectedIndex = listBox1.Items.Count - 1;

            if (data.WorkerStatus == WorkerStatus.DATA_READING_FINISHED) {
                BinaryFile.WriteToFile(data.Data, "test.bin");
            }

            switch (data.WorkerStatus) {
                case WorkerStatus.INIT:
                case WorkerStatus.DETECT:
                case WorkerStatus.CANCEL:
                    moduleReadingData.State = ModuleReadingState.NO_MODULE;
                    break;

                case WorkerStatus.WAIT_AFTER_DETECT:
                case WorkerStatus.CONFIG_READING:
                    moduleReadingData.State = ModuleReadingState.CHECK;
                    break;

                case WorkerStatus.WRONG_DATA:
                    moduleReadingData.State = ModuleReadingState.NO_DATA;
                    break;

                case WorkerStatus.MARKER_READING_START:
                    moduleReadingData.State                = ModuleReadingState.READING;
                    moduleReadingData.Progress             = 0;
                    moduleReadingData.IdentificationNumber = e.ProgressPercentage;
                    break;

                case WorkerStatus.MARKER_READING:
                case WorkerStatus.DATA_READING_START:
                    moduleReadingData.Progress             = 0;
                    break;

                
                case WorkerStatus.DATA_READING:
                    moduleReadingData.Progress = e.ProgressPercentage;
                    break;

                case WorkerStatus.DATA_READING_FINISHED:
                    moduleReadingData.State = ModuleReadingState.READ_FINISHED;
                    moduleReadingData.FlashDecoder = new Bat2FlashDecoder(data.Data);
                    break;
            }
            ShowReadState(moduleReadingData);
        }

        private void ReaderAutomat_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //e.Cancelled: use this if you need to determine how the worker thread completed. 
            //             Must set e.Cancel to True in DoWork (False by default) for this be reliable

            //e.Result   : Like this.piCalcWorker.ReportProgress() allows us to pass a user token
            //             to ProgressChanged via the ProgressChangedEventArgs, you can pass a result
            //             object back to RunWorkerCompleted vie RunWorkerCompletedEventArgs.Result,
            //             but only if not cancelled, as e.Result, when cancelled, is an exception:
            //             Result	'e.Result' threw an exception of type 'System.InvalidOperationException'	object {System.InvalidOperationException}

            if (e.Cancelled) {
                // Rucne ukoncil
                buttonStart.Text = "Cancelled";
                return;
            }

        }

        private void buttonStart_Click(object sender, EventArgs e) {
            if (flashReaderAutomat.IsBusy) {
                flashReaderAutomat.Stop();
            } else {
                listBox1.Items.Clear();
                flashReaderAutomat.Start();
            }
        }

    }
}
