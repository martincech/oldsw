﻿namespace UsbReaderAutomat {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonStart = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.labelReadStatus = new System.Windows.Forms.Label();
            this.progressBarReading = new System.Windows.Forms.ProgressBar();
            this.labelProgress = new System.Windows.Forms.Label();
            this.dataGridViewArchive = new System.Windows.Forms.DataGridView();
            this.ColumnDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAverage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArchive)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(12, 12);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(157, 23);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 41);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(188, 251);
            this.listBox1.TabIndex = 1;
            // 
            // labelReadStatus
            // 
            this.labelReadStatus.AutoSize = true;
            this.labelReadStatus.Location = new System.Drawing.Point(222, 15);
            this.labelReadStatus.Name = "labelReadStatus";
            this.labelReadStatus.Size = new System.Drawing.Size(85, 13);
            this.labelReadStatus.TabIndex = 2;
            this.labelReadStatus.Text = "labelReadStatus";
            // 
            // progressBarReading
            // 
            this.progressBarReading.Location = new System.Drawing.Point(224, 41);
            this.progressBarReading.Name = "progressBarReading";
            this.progressBarReading.Size = new System.Drawing.Size(208, 14);
            this.progressBarReading.TabIndex = 3;
            // 
            // labelProgress
            // 
            this.labelProgress.AutoSize = true;
            this.labelProgress.Location = new System.Drawing.Point(438, 41);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(33, 13);
            this.labelProgress.TabIndex = 4;
            this.labelProgress.Text = "100%";
            // 
            // dataGridViewArchive
            // 
            this.dataGridViewArchive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewArchive.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDay,
            this.ColumnDate,
            this.ColumnCount,
            this.ColumnAverage});
            this.dataGridViewArchive.Location = new System.Drawing.Point(224, 80);
            this.dataGridViewArchive.Name = "dataGridViewArchive";
            this.dataGridViewArchive.Size = new System.Drawing.Size(495, 218);
            this.dataGridViewArchive.TabIndex = 5;
            // 
            // ColumnDay
            // 
            this.ColumnDay.HeaderText = "Day";
            this.ColumnDay.Name = "ColumnDay";
            // 
            // ColumnDate
            // 
            this.ColumnDate.HeaderText = "Date";
            this.ColumnDate.Name = "ColumnDate";
            // 
            // ColumnCount
            // 
            this.ColumnCount.HeaderText = "Count";
            this.ColumnCount.Name = "ColumnCount";
            // 
            // ColumnAverage
            // 
            this.ColumnAverage.HeaderText = "Average";
            this.ColumnAverage.Name = "ColumnAverage";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 310);
            this.Controls.Add(this.dataGridViewArchive);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.progressBarReading);
            this.Controls.Add(this.labelReadStatus);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.buttonStart);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewArchive)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label labelReadStatus;
        private System.Windows.Forms.ProgressBar progressBarReading;
        private System.Windows.Forms.Label labelProgress;
        private System.Windows.Forms.DataGridView dataGridViewArchive;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAverage;
    }
}

