﻿using System;
using System.Collections.Generic;
using System.Text;

/*

 Verze 1.0.0:
   - 22.7.2011: Prvni verze

 Verze 1.1.0:
   - 3.9.2011: Z USA hlasili, ze exportni soubor je behem behu programu zablokovany a nelze s nim pracovat. Ja jsem to zkousel a zablokovany nebyl,
               nicmene FileStream jsem dal radeji do bloku using{}. Zmeny v Csv.cs, fce SaveToFile().
   - 4.9.2011: Do App.config pridana polozka InactivityTimer, ktera udava pocet sekund neaktivity, po jejimz ubehnuti se aplikace automaticky ukonci.
   - 5.9.2011: Do CSV souboru se uklada i datum a cas nacteni SMS.

 Verze 1.2.0:
   - 12.1.2012: SMS s jinym formatem nez z BAT2 se ukladaji do extra souboru. Do App.config pridana polozka GarbageFile.


 */

namespace Bat2SmsCsv {
    
    /// <summary>
    /// Current SW version
    /// </summary>
    public static class SwVersion {
        /// <summary>
        /// Major version number, corresponding to scale major version
        /// </summary>
        public const int MAJOR = 1;
        
        /// <summary>
        /// Minor version number, corresponding to scale minor version
        /// </summary>
        public const int MINOR = 2;

        /// <summary>
        /// Build number
        /// </summary>
        public const int BUILD = 0;

        /// <summary>
        /// Get version string, for example "1.0.0"
        /// </summary>
        /// <returns>Version string</returns>
        public static new string ToString() {
            return MAJOR.ToString() + "." + MINOR.ToString() + "." + BUILD.ToString();
        }
    }


}
