﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Veit.GsmWorker;
using Veit.Bat2;

namespace Bat2SmsCsv {
    /// <summary>
    /// Action that should be performed after all background workers are stopped
    /// </summary>
    enum ActionAfterCancel {
        NONE,
        QUIT,       // Ukonci aplikaci
    }

    public partial class FormMain : Form {
        private List<SmsStatus> smsStatuses;
        
        /// <summary>
        /// Background workers for reading the SMS
        /// </summary>
        private SmsReadWorkers smsReadWorkers;

        /// <summary>
        /// Action that should be performed after all background workers are stopped
        /// </summary>
        private ActionAfterCancel actionAfterCancel;

        /// <summary>
        /// Inactiviyt time in seconds
        /// </summary>
        private int inactivityTimer;

        public FormMain() {
            InitializeComponent();

            // Vytvorim seznam se stavy vsech modemu, zatim tam ulozim jen cislo portu
            smsStatuses = new List<SmsStatus>();
            foreach (int portNumber in AppConfig.ModemPorts) {
                SmsStatus smsStatus = new SmsStatus();
                smsStatus.PortNumber = portNumber;
                smsStatuses.Add(smsStatus);
            }
            
            // Rozjedu backgroundworker
            smsReadWorkers = new SmsReadWorkers(AppConfig.ModemPorts, GsmWorker_ProgressChanged, gsmWorker_RunWorkerCompleted);
            smsReadWorkers.Start();

            // Odpocet necinnosti
            inactivityTimer = 0;
            timerSecondTick.Enabled = true;

/*            DecodedSms sms = new DecodedSms();
            sms.Average = 1;
            sms.Date = DateTime.Now;
            sms.PhoneNumber = "1111";
            SaveSms.Save(sms, "test.txt");*/
        }

        private int GetModemIndex(int portNumber) {
            for (int i = 0; i < smsStatuses.Count; i++) {
                if (smsStatuses[i].PortNumber == portNumber) {
                    return i;
                }
            }
            return -1;      // Nenasel jsem
        }

        private void RedrawModems(int index) {
            dataGridViewModems.Rows.Clear();
            foreach (SmsStatus status in smsStatuses) {
                string eventText;
                switch (status.Event) {
                    case SmsEvent.SMS_READING:
                    case SmsEvent.SMS_READ_ERROR:
                    case SmsEvent.SMS_DELETING:
                    case SmsEvent.SMS_DELETE_OK:
                    case SmsEvent.SMS_DELETE_ERROR:
                    case SmsEvent.SMS_SENDING:
                    case SmsEvent.SMS_SEND_OK:
                    case SmsEvent.SMS_SEND_ERROR:
                    case SmsEvent.SMS_READ_OK:
                        eventText = status.Event.ToString() + " (" + index.ToString() + ")";
                        break;

                    default:
                        eventText = status.Event.ToString();
                        break;
                    
                }

                dataGridViewModems.Rows.Add(status.ModemName + " (COM" + status.PortNumber.ToString() + ")",
                                            status.OperatorName,
                                            status.OperatorName == "" ? "" : status.SignalStrength.ToString() + " %",
                                            eventText);
            }
        }
        
        private void AddSms(SmsStatus status) {
            // Dekoduju SMS
            DecodedSms decodedSms;
            bool isValid = SmsDecode.Decode(status.SmsNumber, status.SmsText, out decodedSms);

            // Ulozim do CSV
            bool isSaved = false;
            if (isValid) {
                // SMS z BAT2
                isSaved = SaveSms.SaveBat2Sms(decodedSms, AppConfig.OutputFile);
            } else {
                // Jiny format ulozim do jineho souboru
                isSaved = SaveSms.SaveGarbage(status.SmsNumber, status.SmsText, AppConfig.GarbageFile);
            }

            // Pridam SMS do tabulky
            dataGridViewSms.Rows.Add(DateTime.Now.ToString("G"),        // Vcetne sekund
                                     status.SmsNumber,
                                     status.SmsText,
                                     isValid,
                                     isSaved);
        }
        
        private void GsmWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Preberu status
            SmsStatus status = (SmsStatus)e.UserState;

            // Aktualizuju status v seznamu modemu
            smsStatuses[GetModemIndex(status.PortNumber)] = new SmsStatus(status);

            // Prekreslim stav modemu
            RedrawModems(e.ProgressPercentage);

            // Pokud prijal SMS, zobrazim ji a exportuju
            if (status.Event == SmsEvent.SMS_READ_OK) {
                AddSms(status);
                inactivityTimer = 0;        // Nuluju cas necinnosti
            }
        }

        /// <summary>
        /// Perform action after all workers are cancelled
        /// </summary>
        private void PerformActionAfterCancel() {
            // Akci provede vzdy az ten worker, ktery se zastavi jako posledni
            if (smsReadWorkers.IsBusy) {
                return;     // Nektery worker jeste bezi, cekam dal
            }

            switch (actionAfterCancel) {
                case ActionAfterCancel.QUIT:
                    Close();
                    break;
            }
        }

        private void gsmWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //e.Cancelled: use this if you need to determine how the worker thread completed. 
            //             Must set e.Cancel to True in DoWork (False by default) for this be reliable

            //e.Result   : Like this.piCalcWorker.ReportProgress() allows us to pass a user token
            //             to ProgressChanged via the ProgressChangedEventArgs, you can pass a result
            //             object back to RunWorkerCompleted vie RunWorkerCompletedEventArgs.Result,
            //             but only if not cancelled, as e.Result, when cancelled, is an exception:
            //             Result	'e.Result' threw an exception of type 'System.InvalidOperationException'	object {System.InvalidOperationException}

            if (!e.Cancelled) {
                return;     // Worker nemuze skoncit sam od sebe, jede v nekonecne smycce
            }

            // Provedu akci po ukonceni workeru
            switch (actionAfterCancel) {
                case ActionAfterCancel.QUIT:
                    // Ukoncuje vsechny workery
                    PerformActionAfterCancel();
                    break;
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            // Pred ukoncenim aplikace ukoncim worker a pockam, az se opravdu ukonci
            if (!smsReadWorkers.IsBusy) {
                return;             // Worker uz nebezi, muzu skoncit
            }

            // Worker bezi, zastavim ho a pockam, az se ukonci
            this.Cursor = Cursors.WaitCursor;
            smsReadWorkers.Stop();
            actionAfterCancel = ActionAfterCancel.QUIT;
            e.Cancel = true;        // Zatim pockam
        }

        private void timerSecondTick_Tick(object sender, EventArgs e) {
            if (AppConfig.InactivityTimer <= 0) {
                return;     // Timer se nepouziva
            }
            if (++inactivityTimer >= AppConfig.InactivityTimer) {
                Close();
            }
        }

    }
}
