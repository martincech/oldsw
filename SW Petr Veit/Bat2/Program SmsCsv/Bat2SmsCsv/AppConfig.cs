﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Bat2SmsCsv {
    public static class AppConfig {
        /// <summary>
        /// List of COM port numbers with connected modems
        /// </summary>
        public static readonly List<int> ModemPorts;

        /// <summary>
        /// Full path to an output file for BAT2 messages
        /// </summary>
        public static readonly string OutputFile;

        /// <summary>
        /// Full path to a file for all other messages
        /// </summary>
        public static readonly string GarbageFile;

        /// <summary>
        /// Time of inactivity in seconds, after which the application exits automatically. Zero value means inactive timer.
        /// </summary>
        public static readonly int InactivityTimer;

        /// <summary>
        /// Decode port numbers
        /// </summary>
        /// <param name="text">Port numbers separated with a comma. Example: "1,2,3"</param>
        /// <returns>List of port numbers</returns>
        private static List<int> DecodePortNumbers(string text) {
            List<int> portList = new List<int>();
            text = text.Trim();
            string[] array = text.Split(',');
            foreach (string port in array) {
                portList.Add(Int32.Parse(port));
            }
            return portList;
        }

        static AppConfig() {
            ModemPorts  = DecodePortNumbers(ConfigurationManager.AppSettings["ModemComPorts"]);
            OutputFile  = ConfigurationManager.AppSettings["OutputFile"];
            GarbageFile = ConfigurationManager.AppSettings["GarbageFile"];
            if (ConfigurationManager.AppSettings["InactivityTimer"] != null) {
                InactivityTimer = int.Parse(ConfigurationManager.AppSettings["InactivityTimer"]);
            } else {
                InactivityTimer = 0;
            }
        }
    }
}
