﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat2Sms {
    /// <summary>
    /// Modem used for reading and sending of SMS
    /// </summary>
    public class UsedModem : DetectedModem, IComparable<UsedModem> {
        /// <summary>
        /// Modem should be used for receiving of SMS
        /// </summary>
        public bool IsUsedForReceiving;

        /// <summary>
        /// Modem should be used for sending of SMS
        /// </summary>
        public bool IsUsedForSending;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="portNumber"></param>
        /// <param name="name"></param>
        /// <param name="isUsedForReceiving"></param>
        /// <param name="isUsedForSending"></param>
        public UsedModem(int portNumber, string name, bool isUsedForReceiving, bool isUsedForSending)
            : base(portNumber, name) {
            IsUsedForReceiving = isUsedForReceiving;
            IsUsedForSending   = isUsedForSending;
        }

        /// <summary>
        /// Default compare method (by port number)
        /// </summary>
        /// <param name="modem">Other modem to compare to</param>
        /// <returns>1, -1 or 0</returns>
        public int CompareTo(UsedModem modem) {
            return this.PortNumber.CompareTo(modem.PortNumber);
        }

    }
}
