﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Veit.Database;
using System.Data.Common;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Services;
using System.Configuration;

namespace Bat2Sms {
    /// <summary>
    /// Firebird database type
    /// </summary>
    public enum DatabaseType {
        EMBEDDED,
        SERVER
    }
    
    public abstract class Database {
        /// <summary>
        /// Database name (applies in app.config and also in file name)
        /// </summary>
        protected string databaseName;

        /// <summary>
        /// Connection string to DB
        /// </summary>
        private readonly string connectionString;
        
        /// <summary>
        /// Database factory used for connection
        /// </summary>
        public DatabaseFactory Factory { get { return factory; } }
        protected DatabaseFactory factory;
        
/*        /// <summary>
        /// Database file name including path
        /// </summary>
        public readonly string DatabaseFileName;*/

        /// <summary>
        /// Database type (embedded or server)
        /// </summary>
        public DatabaseType DatabaseType { get { return databaseType; } }
        private DatabaseType databaseType;

        /// <summary>
        ///  Constructor
        /// </summary>
        public Database(string databaseName) {
            // Ulozim si jmeno databaze
            this.databaseName = databaseName;
            
            // Nactu connection string
            connectionString = GetConnectionString(databaseName);

            // Z connection stringu zjistim, o jaky typ DB jde (embedded nebo server)
            FbConnectionStringBuilder builder = new FbConnectionStringBuilder(connectionString);
            databaseType = builder.ServerType == FbServerType.Embedded ? DatabaseType.EMBEDDED : DatabaseType.SERVER;

            // Vytvorim spojeni
            factory = new DatabaseFactory(FirebirdClientFactory.Instance, connectionString);
            factory.SetTypesFirebird();
        }

        private string GetConnectionString(string databaseName) {
            // Pokud je v app.config uveden connection string, vezmu ho odsud. V tomto pripade muze jit
            // o embedded i server. Pokud v app.config connection string uveden neni, jde o embedded
            // databazi umistenou v Documents and Settings aktualniho uzivatele.

            // Prohledam vsechny connection stringy v app.config a hledam string s nazvem CONNECTION_STRING_NAME
            foreach (ConnectionStringSettings connection in ConfigurationManager.ConnectionStrings) { 
                if (connection.Name == databaseName) {
                    // Nasel jsem, rovnou ho vratim
                    return connection.ConnectionString;
                }
            }
      
            // V app.config neni connection string uveden, pouziju cestu uctu uzivatele (toto je default nastaveni)
            FbConnectionStringBuilder builder = new FbConnectionStringBuilder();
            builder.Database   = Program.DataFolder + @"\" + databaseName + ".fdb";
            builder.UserID     = "bat2sms";
            builder.Password   = "veit";
            builder.Charset    = "UTF8";
            builder.Pooling    = false;
            builder.ServerType = FbServerType.Embedded;
            return builder.ToString();
        }

        /// <summary>
        /// Check if database exists
        /// </summary>
        /// <returns>True if exists</returns>
        public bool Exists() {
            try {
                factory.OpenConnection();
            } catch {
                return false;       // Neexistuje nebo nejede server
            }

            // Databaze existuje, zavru spojeni
            factory.CloseConnection();
            return true;
        }

        /// <summary>
        /// Create empty database
        /// </summary>
        public void CreateDatabase() {
            FbConnection.CreateDatabase(connectionString);
        }
        
        /// <summary>
        /// Check integrity of the database file
        /// </summary>
        /// <returns>True if the database is OK</returns>
        public bool CheckIntegrity() {
/*            FbValidation validation = new FbValidation();
            validation.ConnectionString = connectionString;
            validation.Options = FbValidationFlags.CheckDatabase;
            validation.Execute();*/
            return true;
        }

    }
}
