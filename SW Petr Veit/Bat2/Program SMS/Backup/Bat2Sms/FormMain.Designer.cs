﻿namespace Bat2Sms {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonDetectModems = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonDetectModems
            // 
            this.buttonDetectModems.Location = new System.Drawing.Point(12, 12);
            this.buttonDetectModems.Name = "buttonDetectModems";
            this.buttonDetectModems.Size = new System.Drawing.Size(99, 23);
            this.buttonDetectModems.TabIndex = 0;
            this.buttonDetectModems.Text = "Detect modems";
            this.buttonDetectModems.UseVisualStyleBackColor = true;
            this.buttonDetectModems.Click += new System.EventHandler(this.buttonDetectModems_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.buttonDetectModems);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonDetectModems;
    }
}

