﻿using System;
using System.Collections.Generic;
using System.Text;
using Modem;

namespace Bat2Sms {
    /// <summary>
    /// GSM modem connected to serial port
    /// </summary>
    public class DetectedModem {
        /// <summary>
        /// COM port number
        /// </summary>
        public int PortNumber;

        /// <summary>
        /// Modem name
        /// </summary>
        public string Name;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="portNumber"></param>
        /// <param name="name"></param>
        public DetectedModem(int portNumber, string name) {
            PortNumber = portNumber;
            Name       = name;
        }

    }
}
