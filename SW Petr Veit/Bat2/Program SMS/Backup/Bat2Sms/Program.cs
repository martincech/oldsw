﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using Veit.SFolders;
using SingleInstance;

namespace Bat2Sms {
    static class Program {
        /// <summary>
        /// Application name
        /// </summary>
        public const string ApplicationName = "BAT2 SMS";

        /// <summary>
        /// Data folder name in user's profile
        /// </summary>
        public static string DataFolder { get { return dataFolder; } }
        private static string dataFolder;

        /// <summary>
        /// Folder for temporary files in user's profile
        /// </summary>
        public static string TempFolder { get { return tempFolder; } }
        private static string tempFolder;

        /// <summary>
        /// Database instance with config
        /// </summary>
        public static DatabaseConfig DatabaseConfig;

        /// <summary>
        /// Prepare working directories
        /// </summary>
        public static void PrepareDirectories() {
            // Nactu nazev pracovniho adresare (struktura "Veit\Bat1\V7")
            SFolders sFolders = new SFolders(@"Veit\Bat2Sms\V" + SwVersion.MAJOR.ToString());
            sFolders.PrepareData(null);             // Pokud neexistuje, vytvorim pracovni adresar
            dataFolder = sFolders.DataFolder;       // Zapamatuju si pracovni adresar
            
            // Adresar pro docasne soubory
            tempFolder = dataFolder + @"\Temp";     // Zapamatuju si adresar pro docasne soubory
            if (!Directory.Exists(tempFolder)) {
                // Pokud neexistuje, vytvorim
                Directory.CreateDirectory(tempFolder);  
            }
        }

        /// <summary>
        /// Database initialization
        /// </summary>
        private static bool InitDatabase(FormSplash splash) {
            // Zalozim objekt databaze
            DatabaseConfig = new DatabaseConfig();

            // Zkontroluju databazi
            if (DatabaseConfig.Exists()) {
                // Zkontroluju integritu dat v databazi
                if (!DatabaseConfig.CheckIntegrity()) {
                    // Korupce dat, musi obnovit ze zalohy
                    if (splash != null) {
                        splash.Hide();          // Schovam splash screen
                    }
                    try {
/*                        MessageBox.Show(Properties.Resources.DATABASE_CORRUPTED, ApplicationName);
                        FormRestoreBackup form = new FormRestoreBackup();
                        if (form.ShowDialog() != DialogResult.OK) {
                            return false;
                        }*/
                    } finally {
                        if (splash != null) {
                            splash.Show();      // Na zaver opet zobrazim
                        }
                    }
                }

                // Zkontroluji platnost nastaveni
//                Database.CheckSetup();
                
                // Zkontroluji verzi databaze (az po pripadne obnove ze zalohy)
//                Database.Update();              

                // Provedu automatickou zalohu databaze po startu programu
//                StartupBackup.Backup(true);     // Integritu jsem uz otestoval, neni treba znovu

                // Periodicka zaloha kazdych 7 dni
//                PeriodicBackup.Execute();
            
            } else {
                // Database neexistuje
                DatabaseConfig.CreateDatabase();      // Vytvorim prazdnou databazi
                DatabaseConfig.CreateTables();        // Vytvorim vsechny tabulky
            }

            return true;
        }

        /// <summary>
        /// Data directory and database initialization
        /// </summary>
        public static bool InitData(FormSplash splash) {
            // Priprava adresarove struktury - trva kratce, text ve splashi ani nezobrazuju
            PrepareDirectories();

            // Inicializuji databazi
            if (!InitDatabase(splash)) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            // Pokud uz program jednou bezi, nespoustim ho podruhe, ale prepnu na jiz spusteny
            if (SingleApplication.IsAlreadyRunning()) {
				SingleApplication.SwitchToCurrentInstance();
				return;
			}
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Zobrazim splash
            FormSplash splash = new FormSplash();
            splash.Show();
            splash.Refresh();       // Aby se hned vykreslilo

            try {
                // Inicializuji adresarovou strukturu a databazi
                if (!InitData(splash)) {
                    Application.Exit();
                    return;
                }

                // Nactu nastaveni programu
//                Database.LoadSetup();
            } finally {
                splash.Close();
                splash.Dispose();
            }

            // Pokud zatim neni zvoleny zadny jazyk, necham uzivatele vybrat
/*            if (Setup.Language == SwLanguage.UNDEFINED) {
                new FormLanguage().ShowDialog();    // Bud vybere, nebo zustane UNDEFINED (vykresli se anglictina)
            }
            
            // Nastavim jazyk
            SwLanguageClass.SetLanguage(Setup.Language);*/

            Application.Run(new FormMain());
        }
    }
}
