﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat2Sms {
    public partial class FormMain : Form {
        public FormMain() {
            InitializeComponent();
        }

        private void buttonDetectModems_Click(object sender, EventArgs e) {
            new FormDetectModems().ShowDialog();
        }
    }
}
