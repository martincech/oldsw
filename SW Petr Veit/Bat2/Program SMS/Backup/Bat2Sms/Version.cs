﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat2Sms {

    /// <summary>
    /// Scale version that the SW supports. Data from higher scale versions can be read, but the config cannot be changed.
    /// </summary>
    public static class ScaleVersion {
        /// <summary>
        /// Major version number
        /// </summary>
        public const int MAJOR = 4;
        
        /// <summary>
        /// Minor version number, changed always when config changes (including new languages)
        /// </summary>
        public const int MINOR = 0;
    }

    
    /// <summary>
    /// Current SW version
    /// </summary>
    public static class SwVersion {
        /// <summary>
        /// Major version number, corresponding to scale major version
        /// </summary>
        public const int MAJOR = ScaleVersion.MAJOR;
        
        /// <summary>
        /// Minor version number, corresponding to scale minor version
        /// </summary>
        public const int MINOR = ScaleVersion.MINOR;

        /// <summary>
        /// Database version number, changed when the database structure changes
        /// </summary>
        public const int DATABASE = 0;

        /// <summary>
        /// Build number
        /// </summary>
        public const int BUILD = 0;

        /// <summary>
        /// Get version string, for example "7.0.0.0"
        /// </summary>
        /// <returns>Version string</returns>
        public static new string ToString() {
            return MAJOR.ToString() + "." + MINOR.ToString() + "." + DATABASE.ToString() + "." + BUILD.ToString();
        }
    }


}
