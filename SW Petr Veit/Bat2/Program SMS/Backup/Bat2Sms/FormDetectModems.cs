﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat2Sms {
    public partial class FormDetectModems : Form {
        private DetectedModemCollection detectedModemCollection;

        /// <summary>
        /// Values are being loaded into the controls
        /// </summary>
        private bool isLoading;

        private void EnableButtons(bool isEnabled) {
            buttonOk.Enabled     = isEnabled;
            buttonCancel.Enabled = isEnabled;
            buttonDetect.Enabled = isEnabled;
        }
        
        private void DetectModems() {
            // Automaticky detekuju modemy
            detectedModemCollection = new DetectedModemCollection();
            detectedModemCollection.Detect();
        
            // Nactu dosud pouzivane modemy z DB
            UsedModemCollection usedModemCollection = Program.DatabaseConfig.LoadModemCollection();
            
            // Zobrazim detekovane modemy, zaroven vyplnim stavajici pouziti jednotlivych modemu
            dataGridViewModems.Rows.Clear();
            foreach (DetectedModem modem in detectedModemCollection) {
                bool isUsedForReceiving = false, isUsedForSending = false;
                int index = usedModemCollection.GetIndex(modem.PortNumber);
                if (index >= 0) {
                    // Tento modem se pouziva, vyplnim jeho pouziti
                    UsedModem usedModem = usedModemCollection[index];
                    isUsedForReceiving = usedModem.IsUsedForReceiving;
                    isUsedForSending   = usedModem.IsUsedForSending;
                }

                dataGridViewModems.Rows.Add(modem.Name + "  (COM" + modem.PortNumber.ToString() + ")", isUsedForReceiving, isUsedForSending);
            }
        }
        
        public FormDetectModems() {
            InitializeComponent();

            // Standardne zakazu tlacitka, dokud se nedetekuji modemy
            EnableButtons(false);
        }

        private void FormDetectModems_Shown(object sender, EventArgs e) {
            Refresh();
            
            isLoading = true;
            Cursor.Current = Cursors.WaitCursor;
            try {
                DetectModems();
            } finally {
                Cursor.Current = Cursors.Default;
                isLoading = false;
            }
            
            // Povolim tlacitka
            EnableButtons(true);
        }

        private void dataGridViewModems_CellValueChanged(object sender, DataGridViewCellEventArgs e) {
            // Obslouzim zaskrtnuti posilani SMS, to lze povolit pouze u jednoho modemu
            if (isLoading) {
                return;
            }

            if (e.RowIndex < 0) {
                return;             // Zahlavi
            }
            
            if (e.ColumnIndex != 2) {
                return;             // Zmenil jiny sloupec, nezajima me
            }

            if ((bool)dataGridViewModems.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != true) {
                return;             // Checkbox odsktrnul, nemusim delat nic
            }

            // Zaskrtnul nektery modem pro odesilani, musim odesilani zrusit u vsech ostatnich modemu
            for (int i = 0; i < dataGridViewModems.Rows.Count; i++) {
                if (i == e.RowIndex) {
                    continue;       // Radek, na ktery prave kliknul vynecham, zde prave checkbox zaskrtnul
                }
                dataGridViewModems.Rows[i].Cells[e.ColumnIndex].Value = false;
            }
        }

        private void dataGridViewModems_CurrentCellDirtyStateChanged(object sender, EventArgs e) {
            // Event CellValueChanged se vyvola az po opusteni bunky => vyvolam CellValueChanged rucne hned.
            if (dataGridViewModems.IsCurrentCellDirty) { 
                dataGridViewModems.CommitEdit(DataGridViewDataErrorContexts.Commit); 
            } 

        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Vytvorim ze zadane tabulky seznam modemu
            UsedModemCollection modemCollection = new UsedModemCollection();
            for (int i = 0; i < detectedModemCollection.Count; i++) {
                bool isUsedForReceiving = (bool)dataGridViewModems.Rows[i].Cells[1].Value;
                bool isUsedForSending   = (bool)dataGridViewModems.Rows[i].Cells[2].Value;

                if (!(isUsedForReceiving || isUsedForSending)) {
                    continue;       // Modem nechce vubec pouzivat, do DB ho neni treba ukladat
                }

                modemCollection.Add(new UsedModem(detectedModemCollection[i].PortNumber,
                                                  detectedModemCollection[i].Name,
                                                  isUsedForReceiving,
                                                  isUsedForSending));
            }

            // Zkontroluju, zda vybral aspon 1 modem pro prijem a prave 1 modem pro odesilani
            int receiveCount = 0, sendCount = 0;
            foreach (UsedModem modem in modemCollection) {
                if (modem.IsUsedForReceiving) {
                    receiveCount++;
                }
                if (modem.IsUsedForSending) {
                    sendCount++;
                }
            }

            if (receiveCount < 1) {
                MessageBox.Show(Properties.Resources.SELECT_MODEMS_SELECT_RECEIVE, Text, MessageBoxButtons.OK);
                return;
            }
            if (sendCount != 1) {
                MessageBox.Show(Properties.Resources.SELECT_MODEMS_SELECT_SEND, Text, MessageBoxButtons.OK);
                return;
            }

            // Modemy zadal v poradku, ulozim do DB
            Program.DatabaseConfig.SaveModemCollection(modemCollection);

            
            DialogResult = DialogResult.OK;
        }

        private void buttonDetect_Click(object sender, EventArgs e) {
            // Zakazu tlacitka
            EnableButtons(false);

            // Smazu tabulku
            dataGridViewModems.Rows.Clear();
            Refresh();

            // Detekuju modemy a zobrazim
            Cursor.Current = Cursors.WaitCursor;
            try {
                DetectModems();
            } finally {
                Cursor.Current = Cursors.Default;
            }
            
            // Povolim tlacitka
            EnableButtons(true);
        }
    }
}
