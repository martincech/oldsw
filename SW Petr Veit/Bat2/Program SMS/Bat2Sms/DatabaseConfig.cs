﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat2Sms {
    /// <summary>
    /// Database with config data stored on local computer, not shared in the network.
    /// </summary>
    class DatabaseConfig : Database {
        /// <summary>
        /// Modems table
        /// </summary>
        public  DatabaseTableConfigModems ModemsTable { get { return modemsTable; } }
        private DatabaseTableConfigModems modemsTable;

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseConfig()
            : base("Bat2SmsConfig") {

            // Vytvorim vsechny tabulky
            modemsTable = new DatabaseTableConfigModems(factory);
        }

        /// <summary>
        /// Create all tables
        /// </summary>
        public void CreateTables() {
            // Otevru spojeni
            factory.OpenConnection();

            try {
                modemsTable.Create();
            } finally {
                // Nakonec vzdy zavru spojeni
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load collection of used modems
        /// </summary>
        /// <returns>Collection of modems sorted by port number</returns>
        public UsedModemCollection LoadModemCollection() {
            Factory.OpenConnection();
            try {
                return ModemsTable.LoadModemCollection();
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Save collection of modems
        /// </summary>
        /// <param name="modemCollection">Collection of modems to save</param>
        public void SaveModemCollection(UsedModemCollection modemCollection) {
            Factory.OpenConnection();
            try {
                ModemsTable.SaveModemCollection(modemCollection);
            } finally {
                Factory.CloseConnection();
            }
        }


    }
}
