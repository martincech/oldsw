﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat2Sms {
    public class DatabaseTableConfigModems : DatabaseTable {
        // Tabulka se seznamem pouzitych GSM modemu
    
        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableConfigModems(DatabaseFactory factory)
            : base("Modems", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "ModemId "            + factory.TypeInteger  + " NOT NULL PRIMARY KEY,"
                 + "PortNumber "         + factory.TypeByte     + ","
                 + "IsUsedForReceiving " + factory.TypeByte     + ","
                 + "IsUsedForSending "   + factory.TypeByte
                 + ")");

            // Vytvorim autoincrement
            CreateFirebirdAutoincrement("ModemId", 0);
        }

        /// <summary>
        /// Update table definition
        /// </summary>
        public void Update() {
        }

        /// <summary>
        /// Add new modem
        /// </summary>
        /// <param name="modem">Modem to add</param>
        /// <returns>Id of new modem</returns>
        public int Add(UsedModem modem) {
            // Cislo portu nesmi byt 0
            if (modem.PortNumber == 0) {
                throw new Exception("Modem COM port number cannot be zero");
            }
            
            // Ulozim novy modem
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " "
                                                           + "(PortNumber, IsUsedForReceiving, IsUsedForSending) "
                                                           + "VALUES (@PortNumber, @IsUsedForReceiving, @IsUsedForSending) "
                                                           + "RETURNING ModemId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@PortNumber",         modem.PortNumber));
                command.Parameters.Add(factory.CreateParameter("@IsUsedForReceiving", modem.IsUsedForReceiving));
                command.Parameters.Add(factory.CreateParameter("@IsUsedForSending",   modem.IsUsedForSending));
                return (int)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Read current record from reader and convert it to a UsedModem instance
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private UsedModem ReaderToModem(DbDataReader reader) {
            return new UsedModem((short)reader["PortNumber"],
                                 "",    // Nazev ponecham prazdny
                                 (bool)((short)reader["IsUsedForReceiving"] == 1),
                                 (bool)((short)reader["IsUsedForSending"]   == 1));
        }
        
        /// <summary>
        /// Load collection of modems
        /// </summary>
        /// <returns>Collection of modems</returns>
        public UsedModemCollection LoadModemCollection() {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName)) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    UsedModemCollection modemCollection = new UsedModemCollection();
                    while (reader.Read()) {
                        modemCollection.Add(ReaderToModem(reader));
                    }
                    return modemCollection;
                }
            }
        }

        /// <summary>
        /// Save collection of modems
        /// </summary>
        /// <param name="modemCollection">Collection of modems to save</param>
        public void SaveModemCollection(UsedModemCollection modemCollection) {
            // Smazu celou tabulku
            Clear();

            // Ulozim vsechny modemy
            foreach (UsedModem modem in modemCollection) {
                Add(modem);
            }
        }

/*        /// <summary>
        /// Update scale parameters (all without name)
        /// </summary>
        public void Update(Scale scale) {
            // Jmeno musi byt zadane
            if (scale.Name == "") {
                throw new Exception("Scale name cannot be empty");
            }
            
            // Vaha uz musi mit zadany ID, tj. uz je v databazi ulozena
            if (scale.Id <= 0) {
                throw new Exception("Scale ID <= 0");
            }

            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "Name = @Name, Note = @Note,"
                                                           + "Connection = @Connection, GsmNumber = @GsmNumber, "
                                                           + "PortNumber = @PortNumber, Address = @Address "
                                                           + "WHERE ScaleId = @ScaleId")) {
                command.Parameters.Clear();
                ScaleToCommand(scale, command);
                command.Parameters.Add(factory.CreateParameter("@ScaleId", scale.Id));
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Delete scale
        /// </summary>
        /// <param name="scaleId">Scale Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long scaleId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE ScaleId = @ScaleId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleId", scaleId));
                return command.ExecuteNonQuery();
            }
        }*/


    }
}
