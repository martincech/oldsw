﻿using System;
using System.Collections.Generic;
using System.Text;
using Modem;
using System.Collections.ObjectModel;

namespace Bat2Sms {
    /// <summary>
    /// Collection of detected modems connected to the PC
    /// </summary>
    public class DetectedModemCollection : Collection<DetectedModem> {
        /// <summary>
        /// Detect GSM modem
        /// </summary>
        /// <param name="portNumber">COM port number</param>
        /// <returns>True if successful</returns>
        private bool DetectGsmModem(int portNumber, out string name) {
            if (!Gsm.Detect("COM" + portNumber.ToString(), out name)) {
                return false;
            }

            // Gsm.Detect() detekuje i klasicke telefonni modemy, musim vyfiltrovat pouze GSM modemy
            Gsm modem = new Gsm();
            modem.PortName          = "COM" + portNumber.ToString();
            modem.BaudRate          = 38400;
            modem.CmdTimeout        = 200;     // [ms]
            modem.SmsCenterTimeout  = 15;      // [s]
            modem.SmsReceiveTimeout = 1;       // [s]
            modem.SmsDeleteTimeout  = 3;       // [s]
            modem.RxBufferSize      = 512;     // COM Rx buffer
            modem.MemoryAtSim       = true;    // use SMS memory at SIM card
            try {
                if (!modem.Reset()) {
                    return false;           // Nejde o GSM modem
                }
            } finally {
                modem.Disconnect();
            }
            return true;
        }
        
        /// <summary>
        /// Detect all modems connected from COM1 up to specified port number
        /// </summary>
        /// <param name="maxPortNumber">Maximum COM port number</param>
        public void Detect(int maxPortNumber) {
            string name;

            Clear();
            for (int i = 1; i < maxPortNumber; i++) {
                if (!DetectGsmModem(i, out name)) {
                    continue;
                }
                Add(new DetectedModem(i, name));
            }
        }

        /// <summary>
        /// Detect all modems connected to COM1..COM99
        /// </summary>
        public void Detect() {
            Detect(99);
        }
    }
}
