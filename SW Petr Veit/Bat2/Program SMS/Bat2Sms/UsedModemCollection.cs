﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;

namespace Bat2Sms {
    /// <summary>
    /// Collection of modems used for receiving and sending
    /// </summary>
    public class UsedModemCollection : Collection<UsedModem> {

        /// <summary>
        /// Check if modem exists
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if exists</returns>
        public bool Exists(int portNumber) {
            return ((List<UsedModem>)Items).Exists(delegate(UsedModem modem) { return modem.PortNumber == portNumber; });
        }

        /// <summary>
        /// Sort the collection by port number
        /// </summary>
        private void Sort() {
            ((List<UsedModem>)Items).Sort();
        }

        /// <summary>
        /// Check if modem port number is valid
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if valid</returns>
        public bool CheckPortNumber(int portNumber) {
            // Port nesmi byt 0
            if (portNumber == 0) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Add modem to the collection
        /// </summary>
        /// <param name="modem">Modem to add</param>
        /// <returns>True if successful</returns>
        public new bool Add(UsedModem modem) {
            // Kontrola cisla portu
            if (!CheckPortNumber(modem.PortNumber)) {
                return false;       // Neplatne cislo portu
            }
            
            // Zkontroluju, zda uz modem se zadanym cislem portu v seznamu neexistuje
            if (Exists(modem.PortNumber)) {
                return false;
            }

            // Pridam do seznamu a setridim podle jmena
            Add(modem);
            Sort();

            return true;
        }

        /// <summary>
        /// Find modem index
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Index in the collection</returns>
        public int GetIndex(int portNumber) {
            for (int i = 0; i < Count; i++) {
                if (this[i].PortNumber == portNumber) {
                    return i;
                }
            }

            // Nenasel jsem
            return -1;
        }


    }
}
