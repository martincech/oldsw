//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "WeighingRecord.h"
#include "RecordReport.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TWeighingRecordForm *WeighingRecordForm;

//---------------------------------------------------------------------------
// Kontrola zadanych udaju
//---------------------------------------------------------------------------

bool TWeighingRecordForm::CheckIntEdit(TEdit *Edit) {
  // Zkontroluje, zda je zadana hodnota v <Edit> platny integer
  try {
    if (Edit->Text.IsEmpty()) {
      throw 1;  // Prazdne
    }
    Edit->Text.ToInt();
    return true;
  } catch(...) {
    // Jedno z cisel nenapsal spravne
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    Edit->SetFocus();
    return false;
  }
}

bool TWeighingRecordForm::CheckDoubleEdit(TEdit *Edit) {
  // Zkontroluje, zda je zadana hodnota v <Edit> platny double
  try {
    if (Edit->Text.IsEmpty()) {
      throw 1;  // Prazdne
    }
    Edit->Text.ToDouble();
    return true;
  } catch(...) {
    // Jedno z cisel nenapsal spravne
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    Edit->SetFocus();
    return false;
  }
}

#define CheckInt(Edit)          \
  if (!CheckIntEdit(Edit)) {    \
    return false;               \
  }

#define CheckDouble(Edit)       \
  if (!CheckDoubleEdit(Edit)) { \
    return false;               \
  }

bool TWeighingRecordForm::CheckValues() {
  CheckInt(ScalesEdit);
  if (ScalesEdit->Text.ToInt() < MIN_IDENTIFICATION_NUMBER || ScalesEdit->Text.ToInt() > MAX_IDENTIFICATION_NUMBER) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    ScalesEdit->SetFocus();
    return false;
  }
  CheckInt(DayEdit);
  // Samice nebo vsechno
  CheckInt(   FemalesCountEdit);
  CheckDouble(FemalesAverageEdit);
  CheckDouble(FemalesGainEdit);
  CheckDouble(FemalesSigmaEdit);
  CheckInt(   FemalesCvEdit);
  CheckInt(   FemalesUniEdit);
  // Samci
  if (BothGendersCheckBox->Checked) {
    CheckInt(   MalesCountEdit);
    CheckDouble(MalesAverageEdit);
    CheckDouble(MalesGainEdit);
    CheckDouble(MalesSigmaEdit);
    CheckInt(   MalesCvEdit);
    CheckInt(   MalesUniEdit);
  }
  return true;  // Vse je v poradku
}


//---------------------------------------------------------------------------
__fastcall TWeighingRecordForm::TWeighingRecordForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TWeighingRecordForm::ScalesEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57)) {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------
void __fastcall TWeighingRecordForm::FemalesAverageEditKeyPress(
      TObject *Sender, char &Key)
{
  // Filtruju cislice (musim i backspace) a carku
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57) && Key != DecimalSeparator) {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------
void __fastcall TWeighingRecordForm::BothGendersCheckBoxClick(
      TObject *Sender)
{
  MalesGroupBox->Enabled = BothGendersCheckBox->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TWeighingRecordForm::FormShow(TObject *Sender)
{
  ScalesEdit->SetFocus();        
}
//---------------------------------------------------------------------------
void __fastcall TWeighingRecordForm::Button1Click(TObject *Sender)
{
  if (!CheckValues()) {
    return;             // Nektera hodnota je zadana spatne
  }
  ModalResult = mrOk;   // Vse v poradku
}
//---------------------------------------------------------------------------

void __fastcall TWeighingRecordForm::FemalesGainEditKeyPress(
      TObject *Sender, char &Key)
{
  // Filtruju cislice (musim i backspace) a carku a minus
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57) && Key != DecimalSeparator && Key != '-') {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------

void __fastcall TWeighingRecordForm::PrintButtonClick(TObject *Sender)
{
  // Vytiskne zaznam
  RecordReportForm->InsertedQRLabel->Caption  = InsertLabel->Caption;
  RecordReportForm->LastEditQRLabel->Caption  = EditLabel->Caption;
  RecordReportForm->GsmNumberQRLabel->Caption = GSMLabel->Caption;

  RecordReportForm->ScaleQRLabel->Caption = ScalesEdit->Text;
  RecordReportForm->DayQRLabel->Caption   = DayEdit->Text;
  RecordReportForm->DateQRLabel->Caption  = DateTimePicker->Date.DateString();
  RecordReportForm->NoteQRLabel->Caption  = NoteEdit->Text;

  // Statistika samic
  RecordReportForm->FemalesCountQRLabel->Caption   = FemalesCountEdit->Text;
  RecordReportForm->FemalesAverageQRLabel->Caption = FemalesAverageEdit->Text;
  RecordReportForm->FemalesGainQRLabel->Caption    = FemalesGainEdit->Text;
  RecordReportForm->FemalesSigmaQRLabel->Caption   = FemalesSigmaEdit->Text;
  RecordReportForm->FemalesCvQRLabel->Caption      = FemalesCvEdit->Text;
  RecordReportForm->FemalesUniQRLabel->Caption     = FemalesUniEdit->Text;

  // Statistika samcu
  if (BothGendersCheckBox->Checked) {
    // Chce i statistiku samcu
    RecordReportForm->SetMalesVisible(true);
    RecordReportForm->MalesCountQRLabel->Caption   = MalesCountEdit->Text;
    RecordReportForm->MalesAverageQRLabel->Caption = MalesAverageEdit->Text;
    RecordReportForm->MalesGainQRLabel->Caption    = MalesGainEdit->Text;
    RecordReportForm->MalesSigmaQRLabel->Caption   = MalesSigmaEdit->Text;
    RecordReportForm->MalesCvQRLabel->Caption      = MalesCvEdit->Text;
    RecordReportForm->MalesUniQRLabel->Caption     = MalesUniEdit->Text;
  } else {
    // Jen 1 pohlavi, skryju samce
    RecordReportForm->SetMalesVisible(false);
  }

  RecordReportForm->Preview();
}
//---------------------------------------------------------------------------


