object InformaceModulForm: TInformaceModulForm
  Left = 321
  Top = 367
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Module information'
  ClientHeight = 157
  ClientWidth = 212
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 40
    Width = 101
    Height = 13
    Caption = 'Identification number:'
  end
  object Label2: TLabel
    Left = 16
    Top = 72
    Width = 38
    Height = 13
    Caption = 'Version:'
  end
  object Label3: TLabel
    Left = 16
    Top = 16
    Width = 78
    Height = 13
    Caption = 'Download data?'
  end
  object IdLabel: TLabel
    Left = 155
    Top = 40
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'IdLabel'
  end
  object VerzeLabel: TLabel
    Left = 137
    Top = 72
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'VerzeLabel'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 104
    Width = 177
    Height = 9
    Shape = bsTopLine
  end
  object Label4: TLabel
    Left = 16
    Top = 56
    Width = 26
    Height = 13
    Caption = 'Data:'
  end
  object DataLabel: TLabel
    Left = 141
    Top = 56
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'DataLabel'
  end
  object StartButton: TButton
    Left = 16
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Start'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 120
    Top = 120
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
end
