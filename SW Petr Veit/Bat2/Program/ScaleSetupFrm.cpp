//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ScaleSetupFrm.h"
#include "DM.h"
#include "KConvert.h"
#include "Konstanty.h"
#include "Curves.h"
#include "CurveGrid.h"
#include "Input.h"
#include "Dpi.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TScaleSetupFrame *ScaleSetupFrame;

//---------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------


const unsigned int RS485_SPEEDS[_RS485_SPEED_COUNT] = {1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200};     // Baudove rychlosti

//---------------------------------------------------------------------------
// Nastaveni rezimu zobrazeni
//---------------------------------------------------------------------------

void TScaleSetupFrame::SetEditMode(bool Rs485, bool Started, int FlockNumber) {
  // Nastavi mod editace. <Rs485> = edituje se nastaveni RS-485 vahy, <Started> = vaha prave vazi.
  // V <FlockNumber> je cislo prave vazeneho hejna nebo -1, pokud se nevazi podle zadneho hejna.
  Rs485Mode       = Rs485;
  WeighingStarted = Started;
  WeighedFlock    = FlockNumber;

  // Zaktivnim prvni stranku, ktera je viditelna vzdy
  ScaleSetupPageControl->ActivePage = FlocksTabSheet;

  // Skryju/zobrazim stranky podle rezimu
  IdentificationTabSheet->TabVisible = Rs485Mode;          // Id. cislo jen pri RS-485
  StatisticsTabSheet->TabVisible     = !WeighingStarted;   // Statistiku muze menit jen pokud je vazeni zastaveno
  GsmTabSheet->TabVisible            = !Rs485Mode;         // Parametry GSM jen u pam. modulu
  Rs485TabSheet->TabVisible          = !Rs485Mode;         // Parametry RS-485 jen u pam. modulu

  // Zaktivnim prvni viditelnou stranku
  if (Rs485Mode) {
    ScaleSetupPageControl->ActivePage = IdentificationTabSheet;
  } // else ponecham aktivni FlocksTabSheet
}

//---------------------------------------------------------------------------
// Nahrani nastaveni
//---------------------------------------------------------------------------

void TScaleSetupFrame::LoadSetup(TScaleSetup *NewScaleSetup) {
  // Zobrazi nastaveni <NewScaleSetup>.

  // Ulozim
  ScaleSetup = NewScaleSetup;

  // Identifikacni cislo
  if (IdentificationTabSheet->TabVisible) {
    IdentificationNumberEdit->Text = ScaleSetup->IdentificationNumber;
  }

  // Hejna
  DisplayFlockList();

  // Statistika
  if (StatisticsTabSheet->TabVisible) {
    StatisticsHistogramRangeEdit->Text  = ScaleSetup->HistogramRange;
    StatisticsUniformityRangeEdit->Text = ScaleSetup->UniformityRange;
  }

  // Gsm
  if (GsmTabSheet->TabVisible) {
    GsmUseCheckBox->Checked = ScaleSetup->Gsm.Use;
    GsmMidnightCheckBox->Checked = ScaleSetup->Gsm.SendMidnight;
    GsmPeriodEdit->Text = ScaleSetup->Gsm.PeriodMidnight;
    GsmRequestCheckBox->Checked = ScaleSetup->Gsm.SendOnRequest;
    GsmCheckCheckBox->Checked = ScaleSetup->Gsm.CheckNumbers;
    for (int i = 0; i < ScaleSetup->Gsm.NumberCount; i++) {
      GsmNumberEdits[i]->Text = KConvertGetString(ScaleSetup->Gsm.Numbers[i], GSM_NUMBER_MAX_LENGTH);
    }
    for (int i = ScaleSetup->Gsm.NumberCount; i < GSM_NUMBER_MAX_COUNT; i++) {
      GsmNumberEdits[i]->Text = "";       // Zbytek vymazu
    }
  }

  // RS-485
  if (Rs485TabSheet->TabVisible) {
    for (int i = 0; i < _RS485_SPEED_COUNT; i++) {
      if (ScaleSetup->Rs485.Speed == RS485_SPEEDS[i]) {
        // Nasel jsem
        Rs485SpeedComboBox->ItemIndex = i;
        break;
      }
    }
    Rs485ParityComboBox->ItemIndex   = ScaleSetup->Rs485.Parity;
    Rs485ProtocolComboBox->ItemIndex = ScaleSetup->Rs485.Protocol;
    Rs485ReplyDelayEdit->Text        = ScaleSetup->Rs485.ReplyDelay;
    Rs485SilentIntervalEdit->Text    = ScaleSetup->Rs485.SilentInterval;
  }

  // Vahy
  ScaleMarginAboveFEdit->Text      = ScaleSetup->MarginAbove[GENDER_FEMALE];
  ScaleMarginBelowFEdit->Text      = ScaleSetup->MarginBelow[GENDER_FEMALE];
  ScaleMarginAboveMEdit->Text      = ScaleSetup->MarginAbove[GENDER_MALE];
  ScaleMarginBelowMEdit->Text      = ScaleSetup->MarginBelow[GENDER_MALE];
  ScaleFilterEdit->Text            = ScaleSetup->Filter;
  ScaleStabilizationEdit->Text     = FormatFloat("0.0", ScaleSetup->StabilizationRange);
  ScaleStabilizationTimeEdit->Text = ScaleSetup->StabilizationTime;
  ScaleGainCheckBox->Checked       = (ScaleSetup->AutoMode == AUTOMODE_WITH_GAIN);
  ScaleSaveUponComboBox->ItemIndex = ScaleSetup->JumpMode;
  ScaleUnitsComboBox->ItemIndex    = ScaleSetup->Units;

  // Kompenzacni krivka
  if (ScaleSetup->WeightCorrectionCorrection > 0) {
    CorrectionUseCheckBox->Checked = true;
    CorrectionDay1Edit->Text       = ScaleSetup->WeightCorrectionDay1;
    CorrectionDay2Edit->Text       = ScaleSetup->WeightCorrectionDay2;
    CorrectionCorrectionEdit->Text = FormatFloat("0.0", ScaleSetup->WeightCorrectionCorrection);
  } else {
    CorrectionUseCheckBox->Checked = false;
    CorrectionDay1Edit->Text       = "";
    CorrectionDay2Edit->Text       = "";
    CorrectionCorrectionEdit->Text = "";
  }

  // Podsvit
  BacklightComboBox->ItemIndex = ScaleSetup->Backlight;
}

//---------------------------------------------------------------------------
// Zobrazeni seznamu hejn
//---------------------------------------------------------------------------

void TScaleSetupFrame::DisplayFlockList() {
  TFlockClass *FlockClass;
  AnsiString Str;

  FlocksListBox->Clear();
  for (int i = 0; i < ScaleSetup->FlockList->Count(); i++) {
    FlockClass = ScaleSetup->FlockList->GetFlock(i);
    Str = FlockClass->Number;
    if (FlockClass->Number == WeighedFlock) {
      Str += " (" + LoadStr(STR_RS485_WEIGHING) + ")";
    }
    FlocksListBox->Items->Add(Str);
  }

  // Vyberu prvni hejno (pokud existuje)
  if (ScaleSetup->FlockList->Count() > 0) {
    FlocksListBox->ItemIndex = 0;
    ShowFlock(0);
  } else {
    // Zadne hejno v seznamu neni, jen smazu predchozi udaje
    ClearFlockDisplay();
  }
}

//---------------------------------------------------------------------------
// Smazani obsahu hejna
//---------------------------------------------------------------------------

void TScaleSetupFrame::ClearFlockDisplay() {
  // Smaze vsechny udaje z hejn
  FlockNameEdit->Text = "";
  FlockGendersCheckBox->Checked = false;
  FlockCurvesCheckBox->Checked  = false;
  FlockWeighFromEdit->Text = "";
  FlockWeighTillEdit->Text = "";
  CurveGridClear(FlockCurveFStringGrid);
  CurveGridClear(FlockCurveMStringGrid);
  FlockWeightFEdit->Text = "";
  FlockWeightMEdit->Text = "";
}

//---------------------------------------------------------------------------
// Zobrazeni obsahu hejna
//---------------------------------------------------------------------------

void TScaleSetupFrame::ShowFlockTabs(bool UseCurves, bool UseBothGenders) {
  // Zobrazi/skryje stranky
  if (UseCurves) {
    // Pouziva krivky
    FlockCurveFTabSheet->TabVisible = true;
    FlockCurveMTabSheet->TabVisible = UseBothGenders;
    FlockWeightTabSheet->TabVisible = false;
  } else {
    // Bez krivek
    FlockWeightTabSheet->TabVisible = true;
    FlockCurveFTabSheet->TabVisible = false;
    FlockCurveMTabSheet->TabVisible = false;
    FlockWeightMPanel->Visible = UseBothGenders;
  }
}  

void TScaleSetupFrame::ShowFlock(int Index) {
  // Zobrazi hejno na pozici <Index>
  TFlockClass *Flock;

  // Potlaceni blikani behem nahravani
  FlockIsLoading = true;

  // Vymazu predchozi udaje
  ClearFlockDisplay();

  // Nactu ukazatel na hejno ze seznamu
  if ((Flock = ScaleSetup->FlockList->GetFlock(Index)) == NULL) {
    FlockIsLoading = false;
    return;
  }

  // Vyplnim parametry hejna
  FlockNameEdit->Text = Flock->Title;
  FlockGendersCheckBox->Checked = Flock->UseBothGenders;
  FlockCurvesCheckBox->Checked  = Flock->UseCurves;
  if (Flock->WeighFrom != FLOCK_TIME_LIMIT_EMPTY) {
    FlockWeighFromEdit->Text = Flock->WeighFrom;
  }
  if (Flock->WeighTill != FLOCK_TIME_LIMIT_EMPTY) {
    FlockWeighTillEdit->Text = Flock->WeighTill;
  }
  ShowFlockTabs(Flock->UseCurves, Flock->UseBothGenders);
  if (Flock->UseCurves) {
    // Pouziva krivky
    CurveGridShow(FlockCurveFStringGrid, Flock->GrowthCurve[GENDER_FEMALE]);
    if (Flock->UseBothGenders) {
      CurveGridShow(FlockCurveMStringGrid, Flock->GrowthCurve[GENDER_MALE]);
    }
  } else {
    // Bez krivek
    FlockWeightFEdit->Text = FormatFloat(FORMAT_HMOTNOSTI, Flock->InitialWeight[GENDER_FEMALE]);
    if (Flock->UseBothGenders) {
      FlockWeightMEdit->Text = FormatFloat(FORMAT_HMOTNOSTI, Flock->InitialWeight[GENDER_MALE]);
    }
  }

  // Pokud se podle vybraneho hejna vazi, nepovolim ulozeni zmen a smazani hejna
  bool Weighing = (Flock->Number == WeighedFlock);
  DeleteFlockButton->Enabled = !Weighing;
  SaveFlockButton->Enabled   = !Weighing;

  FlockIsLoading = false;
}

//---------------------------------------------------------------------------
// Kontrola zadanych udaju hejna
//---------------------------------------------------------------------------

bool TScaleSetupFrame::CheckHour(TEdit *Edit) {
  // Zkontroluje hodinu zadanou v <Edit>
  if (Edit->Text == "") {
    return true;
  }
  try {
    if (!TFlockClass::CheckHour(Edit->Text.ToInt())) {
      throw 1;
    }
  } catch(...) {
    Edit->SetFocus();
    return false;
  }
  return true;
}

bool TScaleSetupFrame::CheckInitialWeight(TEdit *Edit) {
  // Zkontroluje pocatecni hmotnost zadanou v <Edit>
  try {
    if (!TFlockClass::CheckInitialWeight(Edit->Text.ToDouble())) {
      throw 1;
    }
  } catch(...) {
    Edit->SetFocus();
    return false;
  }
  return true;
}

bool TScaleSetupFrame::CheckDataInFlock() {
  // Zkontroluje parametry zadane u hejna. Pokud je nektera hodnota neplatna, nastavi focus na dany edit.
  if (!TFlockClass::CheckTitle(FlockNameEdit->Text.UpperCase())) {
    FlockNameEdit->SetFocus();
    return false;
  }
  if (!CheckHour(FlockWeighFromEdit)) {
    return false;
  }
  if (!CheckHour(FlockWeighTillEdit)) {
    return false;
  }
  if (FlockCurvesCheckBox->Checked) {
    if (!CurveGridCheckData(FlockCurveFStringGrid)) {
      return false;
    }
    if (FlockGendersCheckBox->Checked && !CurveGridCheckData(FlockCurveMStringGrid)) {
      return false;
    }
  } else {
    if (!CheckInitialWeight(FlockWeightFEdit)) {
      return false;
    }
    if (FlockGendersCheckBox->Checked && !CheckInitialWeight(FlockWeightMEdit)) {
      return false;
    }
  }

  return true;
}

//---------------------------------------------------------------------------
// Ulozeni obsahu hejna
//---------------------------------------------------------------------------

#define SaveHour(Edit, Hour)    Hour = Edit->Text == "" ? FLOCK_TIME_LIMIT_EMPTY : Edit->Text.ToInt()

bool TScaleSetupFrame::SaveFlock(int Index) {
  // Ulozi parametry hejna na pozici <Index>

  // Kontrola zadanych udaju
  if (!CheckDataInFlock()) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return false;
  }

  // Zjistim vybrane hejno
  TFlockClass *Flock = ScaleSetup->FlockList->GetFlock(FlocksListBox->ItemIndex);
  if (Flock == NULL) {
    return false;
  }

  // Ulozim zadane udaje do vybraneho hejna (zadane hodnoty jsou uz zkontrolovane)
  Flock->Title          = FlockNameEdit->Text.UpperCase();
  Flock->UseBothGenders = FlockGendersCheckBox->Checked;
  Flock->UseCurves      = FlockCurvesCheckBox->Checked;
  SaveHour(FlockWeighFromEdit, Flock->WeighFrom);
  SaveHour(FlockWeighTillEdit, Flock->WeighTill);
  if (Flock->UseCurves) {
    // Pouziva krivky
    CurveGridSave(FlockCurveFStringGrid, Flock->GrowthCurve[GENDER_FEMALE]);
    if (Flock->UseBothGenders) {
      CurveGridSave(FlockCurveMStringGrid, Flock->GrowthCurve[GENDER_MALE]);
    }
  } else {
    // Bez krivek
    Flock->InitialWeight[GENDER_FEMALE] = FlockWeightFEdit->Text.ToDouble();
    if (Flock->UseBothGenders) {
      Flock->InitialWeight[GENDER_MALE] = FlockWeightMEdit->Text.ToDouble();
    }
  }

  return true;
}

//---------------------------------------------------------------------------
// Kontrola zadanych udaju
//---------------------------------------------------------------------------

#define CheckValue(Edit, Conversion, TabSheet, Function)         \
  if (TabSheet->TabVisible) {                           \
    try {                                               \
      if (!Function(Edit->Text.Conversion())) {           \
        throw 1;                                        \
      }                                                 \
    } catch(...) {                                      \
      ScaleSetupPageControl->ActivePage = TabSheet;     \
      Edit->SetFocus();                                 \
      return false;                                     \
    }                                                   \
  }

#define CheckIntValue(Edit, TabSheet, Function)         CheckValue(Edit, ToInt,    TabSheet, Function)
#define CheckDoubleValue(Edit, TabSheet, Function)      CheckValue(Edit, ToDouble, TabSheet, Function)

bool TScaleSetupFrame::CheckData() {
  // Zkontroluje zadane hodnoty celeho nastaveni krome hejn. Pokud je nektera hodnota neplatna, nastavi focus na dany edit.

  // Identifikacni cislo
  CheckIntValue(IdentificationNumberEdit, IdentificationTabSheet, ScaleSetup->CheckIndentificationNumber);

  // Statistika
  CheckIntValue(StatisticsHistogramRangeEdit,  StatisticsTabSheet, ScaleSetup->CheckHistogramRange);
  CheckIntValue(StatisticsUniformityRangeEdit, StatisticsTabSheet, ScaleSetup->CheckUniformityRange);

  // GSM
  if (GsmTabSheet->TabVisible) {
    if (GsmMidnightCheckBox->Checked) {
      CheckIntValue(GsmPeriodEdit, GsmTabSheet, ScaleSetup->CheckGsmPeriod);
    }
    for (int i = 0; i < GSM_NUMBER_MAX_COUNT; i++) {
      if (!ScaleSetup->CheckGsmNumber(GsmNumberEdits[i]->Text)) {
        ScaleSetupPageControl->ActivePage = GsmTabSheet;
        GsmNumberEdits[i]->SetFocus();
        return false;
      }
    }
  }

  // RS-485
  CheckIntValue(Rs485ReplyDelayEdit,     Rs485TabSheet, ScaleSetup->CheckRs485ReplyDelay);
  CheckIntValue(Rs485SilentIntervalEdit, Rs485TabSheet, ScaleSetup->CheckRs485SilentInterval);

  // Vahy
  CheckIntValue(ScaleMarginAboveFEdit,      ScaleTabSheet, ScaleSetup->CheckMargin);
  CheckIntValue(ScaleMarginBelowFEdit,      ScaleTabSheet, ScaleSetup->CheckMargin);
  CheckIntValue(ScaleMarginAboveMEdit,      ScaleTabSheet, ScaleSetup->CheckMargin);
  CheckIntValue(ScaleMarginBelowMEdit,      ScaleTabSheet, ScaleSetup->CheckMargin);
  CheckIntValue(ScaleFilterEdit,            ScaleTabSheet, ScaleSetup->CheckFilter);
  CheckDoubleValue(ScaleStabilizationEdit,  ScaleTabSheet, ScaleSetup->CheckStabilizationRange);
  CheckIntValue(ScaleStabilizationTimeEdit, ScaleTabSheet, ScaleSetup->CheckStabilizationTime);

  // Kompenzace
  if (CorrectionUseCheckBox->Checked) {
    CheckIntValue(CorrectionDay1Edit, CorrectionTabSheet, ScaleSetup->CheckCorrectionDay);
    CheckIntValue(CorrectionDay2Edit, CorrectionTabSheet, ScaleSetup->CheckCorrectionDay);
    if (CorrectionDay2Edit->Text.ToInt() <= CorrectionDay1Edit->Text.ToInt()) {
        // Den 2 musi byt vyssi nez den 1. Musim rucne prejit na Edit Day2
        ScaleSetupPageControl->ActivePage = CorrectionTabSheet;
        CorrectionDay2Edit->SetFocus();
        return false;
    }
    CheckDoubleValue(CorrectionCorrectionEdit, CorrectionTabSheet, ScaleSetup->CheckCorrectionCorrection);
  }

  return true;
}

//---------------------------------------------------------------------------
// Ulozeni nastaveni
//---------------------------------------------------------------------------

bool TScaleSetupFrame::SaveSetup() {
  // Ulozi zadane nastaveni do <ScaleSetup>

  // Kontrola hodnot
  if (!CheckData()) {
    return false;
  }

  // Identifikacni cislo
  if (IdentificationTabSheet->TabVisible) {
    ScaleSetup->IdentificationNumber = IdentificationNumberEdit->Text.ToInt();
  }

  // Hejna se ukladaji zvlast

  // Statistika
  if (StatisticsTabSheet->TabVisible) {
    ScaleSetup->HistogramRange  = StatisticsHistogramRangeEdit->Text.ToInt();
    ScaleSetup->UniformityRange = StatisticsUniformityRangeEdit->Text.ToInt();
  }

  // Gsm
  if (GsmTabSheet->TabVisible) {
    ScaleSetup->Gsm.Use            = GsmUseCheckBox->Checked;
    ScaleSetup->Gsm.SendMidnight   = GsmMidnightCheckBox->Checked;
    ScaleSetup->Gsm.PeriodMidnight = GsmPeriodEdit->Text.ToInt();
    ScaleSetup->Gsm.SendOnRequest  = GsmRequestCheckBox->Checked;
    ScaleSetup->Gsm.CheckNumbers   = GsmCheckCheckBox->Checked;
    for (int i = 0; i < ScaleSetup->Gsm.NumberCount; i++) {
      KConvertPutString(ScaleSetup->Gsm.Numbers[i], GsmNumberEdits[i]->Text, GSM_NUMBER_MAX_LENGTH);
    }
  }

  // RS-485
  if (Rs485TabSheet->TabVisible) {
    ScaleSetup->Rs485.Speed          = RS485_SPEEDS[Rs485SpeedComboBox->ItemIndex];
    ScaleSetup->Rs485.Parity         = (TParity)Rs485ParityComboBox->ItemIndex;
    ScaleSetup->Rs485.Protocol       = (TProtocol)Rs485ProtocolComboBox->ItemIndex;
    ScaleSetup->Rs485.ReplyDelay     = Rs485ReplyDelayEdit->Text.ToInt();
    ScaleSetup->Rs485.SilentInterval = Rs485SilentIntervalEdit->Text.ToInt();
  }

  // Vahy
  ScaleSetup->MarginAbove[GENDER_FEMALE] = ScaleMarginAboveFEdit->Text.ToInt();
  ScaleSetup->MarginBelow[GENDER_FEMALE] = ScaleMarginBelowFEdit->Text.ToInt();
  ScaleSetup->MarginAbove[GENDER_MALE]   = ScaleMarginAboveMEdit->Text.ToInt();
  ScaleSetup->MarginBelow[GENDER_MALE]   = ScaleMarginBelowMEdit->Text.ToInt();
  ScaleSetup->Filter                     = ScaleFilterEdit->Text.ToInt();
  ScaleSetup->StabilizationRange         = ScaleStabilizationEdit->Text.ToDouble();
  ScaleSetup->StabilizationTime          = ScaleStabilizationTimeEdit->Text.ToInt();
  ScaleSetup->AutoMode                   = ScaleGainCheckBox->Checked ? AUTOMODE_WITH_GAIN : AUTOMODE_WITHOUT_GAIN;
  ScaleSetup->JumpMode                   = (TJumpMode)ScaleSaveUponComboBox->ItemIndex;
  ScaleSetup->Units                      = (TUnits)ScaleUnitsComboBox->ItemIndex;

  // Kompenzacni krivka
  if (CorrectionUseCheckBox->Checked) {
    ScaleSetup->WeightCorrectionDay1       = CorrectionDay1Edit->Text.ToInt();
    ScaleSetup->WeightCorrectionDay2       = CorrectionDay2Edit->Text.ToInt();
    ScaleSetup->WeightCorrectionCorrection = CorrectionCorrectionEdit->Text.ToDouble();
  } else {
    ScaleSetup->WeightCorrectionDay1       = 0;
    ScaleSetup->WeightCorrectionDay2       = 0;
    ScaleSetup->WeightCorrectionCorrection = 0;
  }

  // Podsvit
  ScaleSetup->Backlight = (TBacklight)BacklightComboBox->ItemIndex;

  return true;
}

//---------------------------------------------------------------------------
__fastcall TScaleSetupFrame::TScaleSetupFrame(TComponent* Owner)
        : TFrame(Owner)
{
  FlockIsLoading = false;
  WeighedFlock   = -1;

  // Nastavim vysku radku vsech StringGridu podle aktualniho DPI
  DpiSetStringGrid(FlockCurveFStringGrid);
  DpiSetStringGrid(FlockCurveMStringGrid);

  // Popis tabulky rustove krivky
  CurveGridShow(FlockCurveFStringGrid);
  CurveGridShow(FlockCurveMStringGrid);

  // Edity tel. cisel pro zjednoduseni
  GsmNumberEdits[0] = GsmNumber1Edit;
  GsmNumberEdits[1] = GsmNumber2Edit;
  GsmNumberEdits[2] = GsmNumber3Edit;
  GsmNumberEdits[3] = GsmNumber4Edit;
  GsmNumberEdits[4] = GsmNumber5Edit;
}
//---------------------------------------------------------------------------
void __fastcall TScaleSetupFrame::FlocksListBoxClick(TObject *Sender)
{
  ShowFlock(FlocksListBox->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TScaleSetupFrame::FlockCurveFLoadButtonClick(
      TObject *Sender)
{
  // Chce nahrat krivku ze seznamu teoretickych krivek
  TStringGrid  *Grid;
  TCurveList   *CurveList;
  TGrowthCurve *Curve;

  // Nahravani je spolecne pro samice i samce - ulozim si tabulku, do ktere krivku nahraju
  if ((TButton *)Sender == FlockCurveFLoadButton) {
    Grid = FlockCurveFStringGrid;
  } else {
    Grid = FlockCurveMStringGrid;
  }

  // Zeptam se na krivku
  if (CurvesForm->ShowModal() != mrOk) {
    return;             // Dal Cancel
  }
  if (CurvesForm->CurvesListBox->ItemIndex < 0) {
    return;             // V seznamu neni zadna krivka
  }

  if ((CurveList = new TCurveList()) == NULL) {
    return;
  }
  try {
    // Nactu vsechny krivky do pameti
    Data->LoadCurvesFromDatabase(CurveList);

    // Najdu vybranou krivku v seznamu
    if ((Curve = CurveList->GetCurve(CurvesForm->CurvesListBox->ItemIndex)) == NULL) {
      throw 1;          // Nenasel jsem krivku v seznamu (nemelo by nastat)
    }

    // Ulozim vsechny body ze krivky do tabulky
    CurveGridShow(Grid, Curve);

    // Nastavim focus na prvni bunku v tabulce
    Grid->Row = 1;      // Pozice 0 je zahlavi tabulky
    Grid->Col = 1;

  } catch(...) {}

  delete CurveList;
}
//---------------------------------------------------------------------------

void __fastcall TScaleSetupFrame::NewFlockButtonClick(TObject *Sender)
{
  // Zalozi nove hejno
  int FlockNumber;

  // Zeptam se na cislo hejna
  InputForm->Caption        = LoadStr(NASTAVENIVAH_NOVE_HEJNO);
  InputForm->Label->Caption = LoadStr(NASTAVENIVAH_NOVE_HEJNO_ZADEJTE);
  InputForm->Edit->Text     = "";
  if (InputForm->ShowModal() != mrOk) {
    return;
  }
  try {
    FlockNumber = InputForm->Edit->Text.ToInt();
    if (FlockNumber < 0 || FlockNumber >= FLOCKS_MAX_COUNT) {
      throw 1;
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Zkontroluju, zda uz hejno neexisuje
  if (ScaleSetup->FlockList->Exists(FlockNumber)) {
    // Hejno uz existuje
    MessageBox(NULL,LoadStr(NASTAVENIVAH_HEJNO_EXISTUJE).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return;
  }

  // Zalozim nove hejno
  TFlockClass *Flock = new TFlockClass(FlockNumber);
  ScaleSetup->FlockList->Add(Flock);

  // Vyberu a zobrazim nove hejno
  DisplayFlockList();
  FlocksListBox->ItemIndex = ScaleSetup->FlockList->GetIndex(FlockNumber);
  ShowFlock(FlocksListBox->ItemIndex);
}
//---------------------------------------------------------------------------


void __fastcall TScaleSetupFrame::DeleteFlockButtonClick(TObject *Sender)
{
  // Smazu vybrane hejno
  if (FlocksListBox->ItemIndex < 0) {
    return;
  }
  if (MessageBox(NULL,LoadStr(NASTAVENIVAH_SMAZAT_HEJNO_TEXT).c_str(),LoadStr(NASTAVENIVAH_SMAZAT_HEJNO).c_str(),MB_ICONQUESTION | MB_YESNO | MB_TASKMODAL) == IDNO) {
    return;
  }

  // Pokud se podle vybraneho hejna vazi, nepovolim ulozeni zmen a smazani hejna (tlacitko uz je zakazane, ale radsi jeste
  // kontroluju i zde.
  if (ScaleSetup->FlockList->GetFlock(FlocksListBox->ItemIndex)->Number == WeighedFlock) {
    return;
  }

  ScaleSetup->FlockList->Delete(FlocksListBox->ItemIndex);

  // Obnovim seznam a vyberu a prvni hejno
  DisplayFlockList();
}
//---------------------------------------------------------------------------

void __fastcall TScaleSetupFrame::SaveFlockButtonClick(TObject *Sender)
{
  // Ulozim zmeny v hejnu
  if (FlocksListBox->ItemIndex < 0) {
    return;
  }

  // Pokud se podle vybraneho hejna vazi, nepovolim ulozeni zmen a smazani hejna (tlacitko uz je zakazane, ale radsi jeste
  // kontroluju i zde.
  if (ScaleSetup->FlockList->GetFlock(FlocksListBox->ItemIndex)->Number == WeighedFlock) {
    return;
  }

  if (!SaveFlock(FlocksListBox->ItemIndex)) {
    return;
  }
  // Naformatuju zadane hodnoty
  ShowFlock(FlocksListBox->ItemIndex);
}
//---------------------------------------------------------------------------

void __fastcall TScaleSetupFrame::FlockGendersCheckBoxClick(TObject *Sender)
{
  if (FlockIsLoading) {
    return;             // Behem nahravani nevykresluju (jinak blika)
  }
  ShowFlockTabs(FlockCurvesCheckBox->Checked, FlockGendersCheckBox->Checked);
}
//---------------------------------------------------------------------------


