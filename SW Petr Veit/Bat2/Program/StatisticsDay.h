//---------------------------------------------------------------------------

#ifndef StatisticsDayH
#define StatisticsDayH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TStatisticsDayForm : public TForm
{
__published:	// IDE-managed Components
        TRadioButton *TodayRadioButton;
        TRadioButton *AnotherDayRadioButton;
        TEdit *DayEdit;
        TButton *Button1;
        TButton *Button2;
        TBevel *Bevel1;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall AnotherDayRadioButtonClick(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall DayEditKeyPress(TObject *Sender, char &Key);
private:	// User declarations
public:		// User declarations
        __fastcall TStatisticsDayForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TStatisticsDayForm *StatisticsDayForm;
//---------------------------------------------------------------------------
#endif
