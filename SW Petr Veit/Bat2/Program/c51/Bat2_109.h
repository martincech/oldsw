//*****************************************************************************
//
//    Bat2_109.h - common data definitions for Bat2 version 1.09 for C++  Builder
//    Version 1.0, (c) P.Veit & VymOs
//
//*****************************************************************************

#ifndef __Bat2_109_H__
   #define __Bat2_109_H__

#include "Bat2.h"

//-----------------------------------------------------------------------------
// Kalibrace
//-----------------------------------------------------------------------------

#define MAX_POCET_PLOSIN 10             // Maximalni pocet plosin

// Kalibrace 1 plosiny
typedef struct {
  int32 KalibraceNuly;                 // Kalibrace nuly
  int32 KalibraceRozsahu;              // Kalibrace rozsahu
  int32 Rozsah;                        // Rozsah 0..NOSNOST_NASLAPNYCH_VAH
  byte Dilek;                          // Dilek vah
} TKalibracePlosiny; // 13 bajtu

// Kalibrace
typedef struct {
  byte PouzitaPlosina;                  // Cislo prave pouzite plosiny
  TKalibracePlosiny Plosiny[MAX_POCET_PLOSIN];
} TKalibrace;   // 131 bajtu

//-----------------------------------------------------------------------------
// Konfiguracni data
//-----------------------------------------------------------------------------

typedef struct {
  // Interni data, pro ulozeni v EEPROM
  word Verze;                          // VERZE
  byte IdentifikacniCislo;             // identifikace zarizeni
  byte Jazyk;                          // Jazykova verze + jazyk

  byte SamiceOkoliNad;                 // Okoli nad prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamiceOkoliPod;                 // Okoli pod prumerem pro samice nebo vsechny (pokud rozdeleni pohlavi nepouziva) v procentech
  byte SamciOkoliNad;                  // Okoli nad prumerem pro samce v procentech
  byte SamciOkoliPod;                  // Okoli pod prumerem pro samce v procentech
  byte Filtr;                          // Filtr prevodniku
  byte UstaleniNaslapneVahy;           // Ustaleni hmotnosti v +- desetinach procenta (max +-25.0%)
  byte DelkaUstaleniNaslapneVahy;      // Delka ustaleni hmotnosti v krocich prevodu (cca 0.5sec) (2 - 9)
  TAutoMode TypAutoRezimu;             // Typ rezimu automatickeho hledani cilove hmotnosti (bez nebo s pouzitim denniho prirustku)

  TUnits Jednotky;                     // Zobrazovane jednotky

  byte RozsahHistogramu;               // Rozsah histogramu v +- % stredni hodnoty
  byte RozsahUniformity;               // Rozsah uniformity v +- %

  TWeighingStart StartVykrmu;

  TGsm Gsm;

  TBacklight Podsvit;                  // Nastaveny rezim podsvitu
  byte Baterie;                        // Vaha je napajena z baterie - zatim se nepouziva

  byte KontrolniSuma;                  // Kontrolni soucet
} TKonfigurace;  // 150 bajtu, pri provedeni zmen zkontrolovat fci CfgSaveConfigFromModule()

extern TKonfigurace       Konfigurace;     // Buffer konfigurace v externi RAM

// struktura konfiguracni sekce :
typedef struct {
  TKonfigurace Konfigurace;            // Konfigurace
  TFlocks      Hejna;                  // Definice jednotlivych hejn
  TKalibrace   Kalibrace;              // Kalibrace zkopirovana z interni EEPROM
} TConfigSection109; // 2780 bajtu


//-----------------------------------------------------------------------------
#endif
