//*****************************************************************************
//
//    Hardware.h -  Borland cross definitions
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "..\..\Library\Unisys\uni.h"             // zakladni datove typy
#include "..\..\Library\Unisys\SysDef.h"          // zakladni datove typy

// eliminace XDATA :

#define __xdata__                      // prazdna definice

typedef short           int16;
typedef int             long32;

#define IEP_SIZE        2048           // kapacita EEPROM
#ifndef FLASH_SIZE
  #define FLASH_SIZE      (4096 * 512)   // kapacita Flash
#endif  

typedef sdword TRawValue;              // surova hodnota vazeni

typedef float TNumber;                 // zakladni datovy typ

typedef word      THistogramCount;     // pocet vzorku
typedef TRawValue THistogramValue;     // hodnota vzorku
#define HISTOGRAM_MIN_STEP    1        // Minimalni hodnota kroku v THistogramValue

#define HISTOGRAM_SLOTS       39       // pocet sloupcu - sude i liche cislo (max.254)
#define HISTOGRAM_SET_STEP       1

#define STAT_UNIFORMITY 1
#define STAT_REAL_UNIFORMITY 1         // povoleni presneho vypoctu uniformity

//-----------------------------------------------------------------------------
// Klouzavy prumer
//-----------------------------------------------------------------------------

typedef byte   TAverageCount;     // Pocet vzorku
typedef long   TAverageValue;     // Hodnota vzorku

#define AVERAGE_CAPACITY 9        // Kapacita (maximalni pocet prvku) klouzaveho prumeru

// Podmineny preklad
#define AVERAGE_FULL    1         // Test zaplneni
#define AVERAGE_STEADY  1         // Test zaplneni


//-----------------------------------------------------------------------------
// Filtrace
//-----------------------------------------------------------------------------

#define FILTER_MAX_AVERAGING  60       // max. width of averaging window

// Basic data types :
typedef long   TRawWeight;             // weight data
typedef long   TLongWeight;            // weight sum
typedef byte   TSamplesCount;          // samples counter

//#define FILTER_VISIBLE    1          // show filter data

#define ADC_PREFILTER  3             // pocet vzorku pro prefiltr

//-----------------------------------------------------------------------------
// Modbus RS232
//-----------------------------------------------------------------------------

#define COM_DATA_SIZE        520      // velikost bufferu
typedef int com_pointer_t;            // datovy typ ukazatele do bufferu

#define COM_LEADER_CHAR    ':'
#define COM_TRAILER_CHAR   '\n'

//-----------------------------------------------------------------------------
// Modbus
//-----------------------------------------------------------------------------

// podminena kompilace :
#define MB_ENABLE_DISCRETE         1   // povoleni operaci se skupinou DISCRETE
#define MB_ENABLE_COILS            1   // povoleni operaci se skupinou COILS
#define MB_ENABLE_INPUT_REGISTERS  1   // povoleni operaci se skupinou Holding REGISTERS
#define MB_ENABLE_REGISTERS        1   // povoleni operaci se skupinou Holding REGISTERS
#define MB_ENABLE_FIFO             1   // povoleni operaci s FIFO registry
#define MB_ENABLE_EXCEPTION_STATUS 1   // povoleni funkce exception status
#define MB_ENABLE_DIAGNOSTIC       1   // povoleni diagnostickych operaci
#define MB_ENABLE_COM_EVENT        1   // povoleni komunikacnich udalosti
#define MB_ENABLE_SLAVE_ID         1   // povoleni identifikace
#define MB_ENABLE_IDENTIFICATION   1   // povoleni textove identifikace



#endif
    