//---------------------------------------------------------------------------

#ifndef NewNumberH
#define NewNumberH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TNewNumberForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TLabel *Label2;
        TEdit *NumberEdit;
        TEdit *NoteEdit;
        TButton *Button1;
        TButton *Button2;
        void __fastcall NumberEditKeyPress(TObject *Sender, char &Key);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TNewNumberForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TNewNumberForm *NewNumberForm;
//---------------------------------------------------------------------------
#endif
