object WeighingFilterForm: TWeighingFilterForm
  Left = 253
  Top = 368
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Filter'
  ClientHeight = 223
  ClientWidth = 232
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 48
    Width = 30
    Height = 13
    Caption = 'Scale:'
  end
  object Label3: TLabel
    Left = 16
    Top = 72
    Width = 26
    Height = 13
    Caption = 'From:'
  end
  object Label2: TLabel
    Left = 16
    Top = 96
    Width = 16
    Height = 13
    Caption = 'Till:'
  end
  object Label4: TLabel
    Left = 16
    Top = 16
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label5: TLabel
    Left = 16
    Top = 128
    Width = 26
    Height = 13
    Caption = 'Note:'
  end
  object Bevel1: TBevel
    Left = 16
    Top = 168
    Width = 201
    Height = 9
    Shape = bsTopLine
  end
  object ScalesEdit: TEdit
    Left = 96
    Top = 45
    Width = 121
    Height = 21
    MaxLength = 5
    TabOrder = 1
    OnKeyPress = ScalesEditKeyPress
  end
  object FromDateTimePicker: TDateTimePicker
    Left = 96
    Top = 69
    Width = 121
    Height = 21
    CalAlignment = dtaLeft
    Date = 38650.6882599074
    Time = 38650.6882599074
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 3
  end
  object TillDateTimePicker: TDateTimePicker
    Left = 96
    Top = 93
    Width = 121
    Height = 21
    CalAlignment = dtaLeft
    Date = 38650.6882599074
    Time = 38650.6882599074
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 5
  end
  object FromCheckBox: TCheckBox
    Left = 80
    Top = 72
    Width = 16
    Height = 17
    TabOrder = 2
  end
  object TillCheckBox: TCheckBox
    Left = 80
    Top = 95
    Width = 16
    Height = 17
    TabOrder = 4
  end
  object Button1: TButton
    Left = 48
    Top = 184
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 7
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 142
    Top = 184
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 8
  end
  object NameEdit: TEdit
    Left = 96
    Top = 13
    Width = 121
    Height = 21
    MaxLength = 20
    TabOrder = 0
  end
  object NoteEdit: TEdit
    Left = 96
    Top = 125
    Width = 121
    Height = 21
    MaxLength = 50
    TabOrder = 6
  end
end
