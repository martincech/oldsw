//---------------------------------------------------------------------------



/*
Verze 1.00
  - Poslano 4.1.2005 do Francie

Verze 1.01
  - 5.1.2005: Pridana Finstina


Verze 1.02
  - 23.1.2005: Pridana volba filtru do konfigurace a do nastaveni vah.

Verze 1.03
  - 27.1.2005: Pridana filtrace online grafu
  - 29.1.2005: Podpora novych polozek konfigurace vah verze 1.04
  - 30.1.2005: Pridana simulace z online zaznamu, vcetne francouzskeho prekladu. Finstina jeste neni.

Verze 1.04
  - 31.1.2005: Pridan finsky preklad simulace

Verze 1.05
  - 3.2.2005: Osetreno spatne datum zahajeni vykrmu - stalo se, ze se to pokurvilo statickou elektrinou a pak nesel modul vubec stahnout.
  - 13.2.2005: Vaha v nactenych zaznamech zobrazuje i nastaveny podsvit
  - 13.2.2005: V nastaveni vah je mozne nastavit i podsvit. 
  - 13.2.2005: Zaznamy s identifikacnim cislem 0 nejde stahnout. Id = 0 je v modulu, kterym se prenasi jen konfigurace.
  - 13.2.2005: V nastaveni otevreneho souboru se zobrazuje i identifikacni cislo.

Verze 1.06
  - 29.3.2005: Adaptace na verzi vah 1.06 - do nastaveni pridan gain v automatickem rezimu a perioda vysilani SMS. Prelozeno
               do francouzstiny i finstiny.

Verze 1.07
  - 21.9.2005: Podpora zobrazeni rustove krivky ode dne 0, v ZobrazKrivkuHejna() spatna podminka pro zakonceni krivky.
  - 21.9.2005: Podpora editace krivek ode dne 0, v ZobrazKrivkuHejna() stejna chybna podminka jako vyse
  - 22.9.2005: Zobrazeni, tisk a export reportu s tabulkou se statistikou pro vsechny dny
  - 26.9.2005: Pridana Turectina
  - 26.9.2005: Pri nastaveni prumerovani online grafu to po otevreni noveho online souboru nechavalo max. osy X z minula a nesel
               zobrazit cely graf. Po otevreni nastavim Min a Max zpet na auto.

Verze 1.08
  - Preskoceno (vahy 1.08 se posilaly se SW 1.07)

Verze 1.09.0
  - 8.12.2005: Po ulozeni souboru (ikona Save) zmizel report v zalozce Report a po ulozeni se znovu neotevrel.
  - 15.12.2005: Databaze vazeni
  - 15.12.2005: Podpora USB GSM modemu a prijmu SMS do PC
  - 15.12.2005: Vyhledavani souboru bt2 na disku podle zadanych parametru
  - 15.12.2005: Zavedeno cislo buildu za cislo verze, pri zmene SW bez zmeny FW se zmeni jen build, aby sedela verze pri prenosu
                nastaveni z PC do vahy
  - 15.12.2005: Podpora USB ctecky
  - 15.12.2005: V hlavnim okne je vic ikon
  - 15.12.2005: Pridane hinty ikon, nastavene na polozky menu (nejsou nove extra stringy)
  - 15.12.2005: Zmena seriove komunikace (Uart.cpp -> ComUart.cpp), nyni se vyuziva modul z knihovny

Verze 1.09.1
  - 15.3.2006: Veskera komunikace s GSM modemem se nyni loguje do souboru Bat2Gsm.log
  - 15.3.2006: Datum v SMS se dekodoval spatne pomoci TDate - v zemich, kde nepouzivali d.m.yyy to blblo, nyni dekoduju rucne.
  - 15.3.2006: Chyba v Gsm.cpp, na WinXP a modemu MC75 neslo odeslat SMS, bylo treba pockat 1 sec mezi Rx a Tx. Timeouty beze zmeny.
  - 16.3.2006: Databaze vazeni nefungovala s jinymi formaty datumu, napr. "25-3-2006" v Dansku atd. Problem je ve filtrovani SQL
               pres TQuery podle datumu, filtrovat je treba pres Query->ParamByName(), zmeny jsou jen ve Weighing.cpp.
  - 16.3.2006: Zmenen format datumu pole DATE_TIME_DATE v StatistikaQuery a StatistikaVyberQuery z "d.m.yyyy" na "ddddd", aby se
               datum zobrazoval ve formatu Windows a ne natvrdo s teckami.

Verze 1.09.2
  - 10.5.2006: Opraven preklad do Turectiny

Verze 1.09.3
  - 18.5.2006: V modulu Gsm.cpp opraveny chyby Ivosem, v nekterych pripadech se neuvolnovala alokovana pamet

Verze 1.09.4
  - 5.6.2006: Prelozeno do Danstiny
  - 5.6.2006: Moznost prizpusobit SW jinemu prodejci: jiny nazev programu, jina pripona souboru, jiny kontakt a logo vyrobce.
  - 8.6.2006: Moznost behu na WinXP s ucty. Pozor, vsechny za behu vytvarene TTable a TQuery musi mit nastaven DatabaseName

Verze 1.09.5
  - 28.6.2006: Zmena v Gsm.cpp: konstanta MemoryMode[] zmenena tak, ze se pouzivaji pouze SMS pozice na SIM karte. Dosud se
               pouzivaly jak pozice na SIM, tak v pameti modemu. Ve spojeni s Oskarovou SIM a modemem MC39i to ale blblo pri
               nacitani SMS z pameti modemu (pozice 25-50), cekani GsmModem->SmsRxTimeout = 250 v kontruktoru TWeighingForm
               bylo malo. Pro pamet modemu by bylo treba 500ms a vice, coz by ale uz zadrhavalo uzivatelovy reakce.
               Po prvnim nastaveni si pamet modem pamatuje i po vypnuti.
  - 29.6.2006: Zruseno nastaveni InitialDir u vsech Open a SaveDialogu. Nyni to po prvnim startu nabidne slozku Dokumenty, pri
               dalsim pouziti to uz drzi posledni cestu. Driv to neustale chodilo do slozky Dokumenty.
  - 4.7.2006: Predelan prijem SMS, nejprve se ceka, az je v pameti aspon 1 SMS a az pote se zacnou vycitat pozice. Pokud se behem
              vycitani zjisti, ze pamet neobsahuje zadnou SMS, spadne to zpet do cekani na prijem.
  - 4.7.2006: V souvislosti s prijmem SMS jsem musel pridat 1 string do RC a v anglictine to blblo. Musel jsem do projektu pridat
              anglictinu jako dalsi jazyk.

Verze 1.09.6
  - 3.8.2006: V Australii nesly nacitat SMS do PC, tamni operatori pouzivaji nejakou zvlastni komprimaci obsahu SMS, ktera neni
              nikde poradne zdokumentovana. Zmena v Gsm.cpp: prechod z binarniho na textovy format SMS, kde komprimace neni.
  - 3.8.2006: V textovem modu SMS se prenasi vic dat, bylo treba prodlouzit GsmModem->SmsRxTimeout z 250 na 500ms.
  - 3.8.2006: Kvuli prodlouzeni timeoutu prodlouzen i interval timeru pro vycitani z modemu z 600 na 1000ms. Pri 600ms bylo menu
              moc zaseknute.

Verze 1.09.7
  - 26.9.2006: Win98 a 95 neobsahuji shfolder.dll, proto program vubec nesel spustit. Predelano na dynamicke volani DLL.
  - 29.9.2006: Logo v "O Programu" zmeneno na nepruhledne s bilym pozadim. Pruhlednost blbla.

Verze 1.10.0:
  - 6.3.2007: Konverze nactenych dat, tisk a export do Excelu rozhazene do extra souboru
  - 7.3.2007: Pri spusteni programu se provadi kontrola sloupcu v tabulkach, ktere se kopiruji do pracovniho adresare. Pokud
              sloupce neodpovidaji originalu, tabulka se zkopiruje znovu. Bude tak mozny upgrade na novejsi verze tabulek.
              Zmeny v SFolders.cpp.
  - 9.3.2007: Pri zadavani souboru pri ukladani to mrsilo nazev, pri zadani "1.1.2007" v SaveDialog to ulozilo "1.1.bt2".
              Do dialogu bylo treba nastavit DefaultExt, zmeny v SetVersionStrings()
  - 13.3.2007: Pokud mel otevreny online graf, mel zazoomovane, zmenil filtr a pak odzoomoval, pomrsila se osa X. Pri
               odzoomovani meritko osy X resetnu.
  - 13.3.2007: Simulace se nyni provadi podle algoritmu verze 1.10, vcerne realne uniformity a naskoku-seskoku
  - 13.3.2007: V databazi vazeni nesly editovat zaznamy, ulozeni vzdy to skoncilo chybou.
  - 16.3.2007: Pridana moznost tisku 1 zaznamu v databazi vazeni (po requestu nebo pri editaci)
  - 16.3.2007: Kompletne odstranena podpora seriove ctecky - nebylo uz ji sice mozne zvolit, ale v kodu
               byla stale. Odstraneny moduly MReader.cpp a PripojeniVah.cpp, zrusen sloupec PORT v Setup.db.
  - 17.3.2007: Export do Excelu predelany tak, aby vuyzival tridu z Library, modul Excel.cpp zrusen.
  - 18.3.2007: Editace id. cisla vahy v FindFile, WeighingFilter a WeighingRecord rozsirena na 3 cislice.
  - 26.3.2007: Okna NumberForm, WeighingFiler a WeighingFilers byly omylem Sizeable, zmeneno na Single,
               protoze Single se korektne zvetsuje se zvednutym DPI ve Windows.
  - 12.4.2007: Moznost definice teoretickych rustovych krivek. Rustovou krivku v *.bt2 i v databazi vazeni je mozne porovnavat
               s temito krivkami, stejne jako ve vaze.
  - 13.4.2007: Chyba v TNastaveniVahForm::KontrolaRozsahuKrivky(), pri ukladani krivky program nepovolil 0.030kg, coz je korektni
               cilova hmotnost.

Verze 1.10.1:
  - 24.6.2007: Build s FTD2XX.lib z noveho ovladace USB ctecky, ktery je kompatibilni s Vista. S novym ovladacem by to fungovalo
               i bez rebuildu, ale stejne bych delal novou verzi kvuli Vista.
  - 24.6.2007: Uprava SFolders, ktere nefungovaly ve Vista. Prechod na adresare jednotlivych uzivatelu, misto spolecnych.
  - 24.6.2007: Kvuli novym modemum ES75 pridan v konstruktoru TWeighingForm radek GsmModem->CmdTimeout = 200.
  - 27.6.2007: Mensi zmeny v nemcine.

Verze 1.10.2:
  - 6.11.2007: Po otevreni souboru hned po spusteni programu se v seznamu krivek pro porovnani nenacetla polozka "<none>".
               Nacitala se v konstruktoru pres Data a jeste to zrejme nebylo k dispozici. Nyni nacitam seznam pri otevirani souboru.
  - 6.11.2007: Pridana podpora pro vahy pripojene pres MODBUS.
  - 8.11.2007: Porovnani se v tabulce DB vazeni zobrazovalo jen pokud byla vybrana jen 1 vaha. Nyni vykresluju u vsech vah,
               uzivatel potrebuje jednim pohledem zkontrolovat vsechny vahy za posledni den.
  - 14.11.2007: Pri vyssim DPI ve Windows byly radky vsech StringGridu moc male. Nyni se vsechny StringGridy v konstruktoru okna
                nastavi na vyssi radky. Nevim proc, ale u hlavniho okna to nefunguje. U vsech ostatnich ano.
  - 15.11.2007: U grafu rustove krivky v HlavniForm lze nyni na pravou osu vykreslit libovolnou statistickou hodnotu.

Verze 1.10.3:
  - 5.12.2007: U vah do verze 1.10.2 se mohlo stat, ze se pri kopirovani dat do modulu nezkopirovala kalibrace. V simulaci sice
               stale pouzivam stary algoritmus detekce, ale jsou tam i casti kodu noveho algoritmu a pouziva se tam kalibrace.
               Pokud v modulu kalibrace nebyla, simulace se vubec neprovedla (napr. pri testu u Machandera 12/2007).

Verze 1.10.4:
  - 13.12.2007: Moznost provizorniho sitoveho provozu, pokud je v programovem adresari soubor Bat2.ini, lze v nem uvest rucne
                cestu k datovym souborum (rucne zadana cesta se pouzije misto adresare Documents and Settings uctu uzivatele).
                Format INI souboru:
                [Bat2]
                DataPath=X:\Bat2\Data\
                (Pozor, cesta musi byt zakoncena koncovym lomitkem, jinak nejdou stahovat data z modulu - SMS ale i tak funguje)

Verze 1.10.5:
  - 19.12.2007: V zalozce Report je mozne klavesou Del mazat jednotlive dny. Soubor je pak treba ulozit.                 

Verze 1.10.6:
  - 19.2.2008: V zaznamech stazenych z modulu a z RS-485 se nyni pracuje s CV s presnosti na 1 desetinu (chtel to tak Dwain).
               V databazi vazeni se stale pracuje na cele cislo, kvuli zpetne kompatibilite.

Verze 1.10.7:
  - 9.7.2008: Podpora identifikacniho cisla az 32767 (dosud 999). Vaha podporuje az 65535, ale ve vsech tabulkach mam sloupec
              nadefinovany jako short int, tj. max. 32767. Paradox neumi udelat zmenu typu pole, tj. byly by slozite upgrady.
              Zmeny v FindFile, WeighingRecord, ScalesSetupFrm a WeighingRecord, pouze tady muze uzivatel zmenit id. cislo,
              vsude jinde se jen zobrazuje. I stary SW verze 1.10.6 je kompatibilni s novymi vahami s id cislem >999, funguje
              vse krome nastaveni vah pres MODBUS, kde se nastavuje id. cislo vahy.
  - 18.7.2008: FindFileForm: pokud zadal neplatne podminky (napr. cislo vahy 0), po oznameni chyby zustalo tlacitko Search zasednute.

Verze 1.10.8:
  - 17.9.2008: Maximalni cislo COM portu GSM modemu zvyseno z COM20 na COM50. U Inauena byl problem, ze se modem detekoval jako >COM20
               a rucni shozeni pod 20 bylo problematicke. Pokud nekdo pouziva bluetooth mys a jine veci, porty az do COM20 mohou byt
               obsazene. Zmena ve Weighing.cpp, MAX_COM_NUMBER

Verze 1.11.0:
  - 5.11.2008: Upgrade na vahy verze 1.11.0, kde pribyla kompenzacni krivka
  - 6.11.2008: Z MODBUSu odstranena podpora diagnostickych citacu, u vahy 1.11 to bylo z duvodu nedostatku pameti zruseno. Na funkcnost
               to nema vliv, stejne se citace nikde nepouzivaly. Zmeny v ModbusThread a ModbusScale.

Verze 1.11.1:
  - 5.1.2009: Oprava male chybky v UsbSpi.cpp od Vym (zlepsena detekce USB zarizeni).

Verze 1.11.2:
  - 16.5.2009: Oprava algoritmu detekce v simulaci podle vah verze 1.11.1, zmeny v Simulate.cpp. Zkousel jsem rozdil v detekci
               na celkem 4 online souborech a rozdil v prumerne hmotnosti je vetsinou do 2g pri hmotnosti 1.5kg. Maximalni
               rozdil byl 10g pri 1.5kg (pouze v 1 pripade).
  - 16.5.2009: Na zadost Richara Wagnera se pri zalozeni noveho nastaveni vahy automaticky vytvori hejno pro broilery, ktere
               obsahuje rustovou krivku pro prvnich 7 dnu vazeni. Zmeny v NastaveniVah.cpp.

Verze 1.11.3:
  - 11.1.2010: Pridana customizace pro Big Dutchman, vaha ComScale
  - 11.1.2010: Podpora USB ctecky pro Big Dutchman, prechod na podepsany ovladac FTDI 2.06.00 

Verze 1.11.4:
  - 14.6.2010: Podpora GSM modemu Teltonika, modul Gsm automaticky rozpozna Siemens nebo Teltoniku.

Verze 1.11.5:
  - 14.7.2010: U prvniho dne vazeni se zobrazoval prirustek roven prumerne hmotnosti, coz je blbost, ma byt nula. Ve vaze jsem
               to uz mel osetrene, ale v SW ne. Upravy jsou vsude mozne (zalozky Report + Statistics + Growth curve, tisk i export
               reportu a rustove krivky, porovnani v zalozce Statistics, kopirovani zaznamu do DB vazeni, dialog prepisu pri kopirovani).

Verze 1.11.6:
  - 5.5.2011: Nekdy se stava, ze se marker ve Fifo zmrsi z 0xFE na 0xFF, takze se Fifo interpretuje jako prepisovane. Pri prochazeni
              archivu se tak zda, ze dny jsou ve Fifo posunute az na konec a na zacatku jsou nesmysly. Pri dekodovani dat z pametoveho modulu
              tak staci v archivu najit prvni den a pokracovat az od nej. Ve vaze jsou sice data ponicena, ale pokusim se je v PC aspon
              rekonstruovat. Zmeny v Convert::KonvertujArchiv().

Verze 1.11.7:
  - 28.9.2011: Z neznamych duvodu prestala fungovat spanelstina (reklamoval Jaume). Vzdycky mel soubor spanelstiny priponu ESP a vse fungovalo.
               Najednou to vyzadovalo priponu ESN, coz je jine trideni. Nevim, zda to nesouvisi s prechodem notebooku na anglicke Win7,
               taky nevim, jestli jsem projekt nejprve na anglicjkych Win prelozil nebo preklad zustal z ceskych XP)

Verze 1.11.8:
  - 17.10.2012: Pridane publikovani dat z Weighing database na Internet.

Verze 1.11.9:
  - 25.4.2013: Neslo vubec nacitat z pametoveho modulu, program vytuhnul. Ve fci KonvertujArchiv() jsem zapomnel nekonecnou smycku debug kodu.

*/


#include <vcl.h>
#pragma hdrstop

#include "reinit.hpp"  // Pro preklad

#include "Hlavni.h"
#include "DM.h"
#include "OProgramu.h"
#include "InformaceModul.h"
#include "ReadProgress.h"
#include "MFlash.h"
#include "NastaveniVah.h"
#include "RozsahDnu.h"
#include "Jazyk.h"
#include "RozsahOnline.h"
#include "SimulaceStart.h"
#include "Simulate.h"
#include "SimulaceVysledky.h"
#include "Weighing.h"
#include "FindFile.h"
#include "WeighingOverwrite.h"
#include "WeighingFilter.h"
#include "WeighingFilters.h"
#include "Convert.h"
#include "Print.h"
#include "Export.h"
#include "Curves.h"
#include "CurveList.h"
#include "File.h"
#include "ModbusOnlineF.h"
#include "Dpi.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
THlavniForm *HlavniForm;



static void ResetOsy() {
  // Resetnu osy u grafu
  HlavniForm->HistogramChart->UndoZoom();
  HlavniForm->AktivitaDenChart->UndoZoom();
  HlavniForm->AktivitaCelkemChart->UndoZoom();
  HlavniForm->KrivkaChart->UndoZoom();
  HlavniForm->OnlineChart->UndoZoom();
}

//---------------------------------------------------------------------------
// Nastaveni jazyka
//---------------------------------------------------------------------------

void THlavniForm::CheckLanguage(int Language) {
  // Odskrtnu vsechny jazyky v menu
  Anglicky1->Checked    = false;
  Francouzsky1->Checked = false;
  Finsky1->Checked      = false;
  Turkce1->Checked      = false;
  Dansk1->Checked       = false;
  Espanol1->Checked     = false;
  Deutsch1->Checked     = false;
  // Zaskrtnu pouzity jazyk v menu
  switch (Language) {
    case LANG_ENGLISH: {
      Anglicky1->Checked = true;
      break;
    }
    case LANG_FRENCH: {
      Francouzsky1->Checked = true;
      break;
    }
    case LANG_FINNISH: {
      Finsky1->Checked = true;
      break;
    }
    case LANG_TURKISH: {
      Turkce1->Checked = true;
      break;
    }
    case LANG_DANISH: {
      Dansk1->Checked = true;
      break;
    }
    case LANG_SPANISH: {
      Espanol1->Checked = true;
      break;
    }
    case LANG_GERMAN: {
      Deutsch1->Checked = true;
      break;
    }
    case LANG_RUSSIAN: {
      Russian1->Checked = true;
      break;
    }
    default: {
      // Je to nejaky jiny jazyk => nastavim anglictinu
      Anglicky1->Checked = true;
      break;
    }
  }//switch
}

void THlavniForm::SetLanguage(int Language) {
  // Nastavi jazyk <Language>
  // Ulozim jazyk do nastaveni
  Data->NastaveniTable->Open();
  Data->NastaveniTable->Edit();
  Data->NastaveniTable->FieldByName("LANGUAGE")->AsInteger = Language;
  Data->NastaveniTable->Post();
  Data->NastaveniTable->Close();
  // Zaskrtnu menu s nastavenym jazykem
  CheckLanguage(Language);
  // Zobrazim info, ze musi restartovat
  MessageBox(NULL,LoadStr(HLAVNI_JAZYK_RESTART).c_str(),Data->Version.ScaleTitle.c_str(),MB_ICONINFORMATION | MB_OK | MB_TASKMODAL);
}

//---------------------------------------------------------------------------
// Nastaveni stringu podle verze odberatele
//---------------------------------------------------------------------------

void THlavniForm::SetVersionStrings() {
  // Nastavi ruzne nazvy podle verze obderatele, struktura Data->Version uz je vytvorena z modulu TData
  // Filtry u dialogu na otevreni/ulozeni souboru
  SaveDialog->Filter         = Data->Version.ScaleTitle + "|*." + Data->Version.FileExtension;
  OpenDialog->Filter         = Data->Version.ScaleTitle + "|*." + Data->Version.FileExtension;
  OpenDialogHardCopy->Filter = Data->Version.ScaleTitle + " hardcopy|*." + String(PRIPONA_HARDCOPY);
  SaveDialogHardCopy->Filter = Data->Version.ScaleTitle + " hardcopy|*." + String(PRIPONA_HARDCOPY);
  // Default pripona pri ukladani - pokud se zadna nenastavi, po zadani "1.1.2007" to vytvori soubor "1.1.bt2"
  SaveDialog->DefaultExt         = "." + Data->Version.FileExtension;
  SaveDialogHardCopy->DefaultExt = "." + String(PRIPONA_HARDCOPY);
  // Nazev aplikace - popis aplikace na hlavni liste
  Application->Title = Data->Version.ScaleTitle;
}

//---------------------------------------------------------------------------
// Otevreni nalezeneho souboru
//---------------------------------------------------------------------------

void THlavniForm::OpenFoundFile() {
  // Otevre soubor vybrany v dialogu hledani
  String File;

  if (FindFileForm->FilesListBox->ItemIndex < 0) {
    return;     // Zadny soubor neni vybrany
  }
  File = FindFileForm->FilesListBox->Items->Strings[FindFileForm->FilesListBox->ItemIndex];
  FileCloseTables();
  FileDecodeFileToTables(File, false, false);
  OtevriTabulkyJakoSoubor(File);
}

//---------------------------------------------------------------------------
// Kopirovani dat do databaze vazeni
//---------------------------------------------------------------------------

void THlavniForm::CopyToDatabase(TQuery *Query, bool Pohlavi) {
  // Zkopiruje den na aktualni pozici v Query s pohlavim <Pohlavi> do databaze vazeni. Data->SmsTable uz musi byt v edit modu.
  String Predpona;
  TStatistic Stat;
  float Average, Sigma;

  // Nastavim pohlavi
  if (Pohlavi == GENDER_FEMALE) {
    Predpona = "FEMALE";
  } else {
    Predpona = "MALE";
  }
  Stat.XSuma  = Query->FieldByName(Predpona + "_STAT_XSUM")->AsFloat;
  Stat.X2Suma = Query->FieldByName(Predpona + "_STAT_X2SUM")->AsFloat;
  Stat.Count  = Query->FieldByName(Predpona + "_STAT_COUNT")->AsInteger;
  Average = StatAverage(&Stat);
  Sigma = StatSigma(&Stat);
  Data->SmsTable->FieldByName(Predpona + "_COUNT")->AsInteger = Stat.Count;
  Data->SmsTable->FieldByName(Predpona + "_AVERAGE")->AsFloat = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average)).ToDouble();  // V Keilu to delam pres int
  if (Query->FieldByName("DAY_NUMBER")->AsInteger == Data->Soubor.MinDen) {
    // Prvni den je prirustek 0
    Data->SmsTable->FieldByName(Predpona + "_GAIN")->AsFloat  = 0;
  } else {
    // Dalsi dny prirustek normalne spocitam
    Data->SmsTable->FieldByName(Predpona + "_GAIN")->AsFloat  = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average) - Query->FieldByName(Predpona + "_YESTERDAY_AVERAGE")->AsFloat).ToDouble();
  }
  Data->SmsTable->FieldByName(Predpona + "_SIGMA")->AsFloat   = FormatFloat("0.000", (double)Sigma / 1000.0).ToDouble();
  Data->SmsTable->FieldByName(Predpona + "_CV")->AsInteger    = FormatFloat("0", StatVariation(Average, Sigma)).ToInt(); // Zatim na cela procenta
  if (Query->FieldByName("USE_REAL_UNI")->AsBoolean) {
    Data->SmsTable->FieldByName(Predpona + "_UNI")->AsInteger = Query->FieldByName(Predpona + "_REAL_UNI")->AsInteger;
  } else {
    Data->SmsTable->FieldByName(Predpona + "_UNI")->AsInteger = FormatFloat("0", StatUniformity(Average, Sigma, Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger)).ToInt();
  }
}

//---------------------------------------------------------------------------
// Volba rozsahu dnu
//---------------------------------------------------------------------------

bool THlavniForm::ZvolRozsahDnu(int *Min, int *Max) {
  // Necha uzivatele zvolit rozsah dnu
  RozsahDnuForm->OdEdit->Text = Data->Soubor.MinDen;
  RozsahDnuForm->DoEdit->Text = Data->Soubor.MaxDen;
  RozsahDnuForm->AktualniRadioButton->Checked = true;
  if (RozsahDnuForm->ShowModal() != mrOk) {
    return false;
  }
  if (RozsahDnuForm->AktualniRadioButton->Checked) {
    // Aktualni den
    *Min = Data->Soubor.Den;
    *Max = Data->Soubor.Den;
    return true;
  } else if (RozsahDnuForm->VsechnyRadioButton->Checked) {
    // Vsechny dny
    *Min = Data->Soubor.MinDen;
    *Max = Data->Soubor.MaxDen;
    return true;
  }//else
  // Rozsah
  try {
    if (RozsahDnuForm->OdEdit->Text.IsEmpty()) {
      *Min = Data->Soubor.MinDen;   // Nezadal nic, dam minimum
    } else {
      *Min = RozsahDnuForm->OdEdit->Text.ToInt();
    }
    if (RozsahDnuForm->DoEdit->Text.IsEmpty()) {
      *Max = Data->Soubor.MaxDen;   // Nezadal nic, dam maximum
    } else {
      *Max = RozsahDnuForm->DoEdit->Text.ToInt();
    }
  } catch(...) {
    // Jedno z cisel nenapsal spravne
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Volba rozsahu online zaznamu
//---------------------------------------------------------------------------

bool THlavniForm::ZvolRozsahOnline(int *Min, int *Max, int *Average) {
  // Necha uzivatele zvolit rozsah dnu a prumerovani
  if (Data->OnlineQuery->RecordCount == 0) {
    return false;
  }
  RozsahOnlineForm->OdEdit->Text     = 1;
  RozsahOnlineForm->DoEdit->Text     = Data->OnlineQuery->RecordCount;
  RozsahOnlineForm->PrumerEdit->Text = 1;
  if (RozsahOnlineForm->ShowModal() != mrOk) {
    return false;
  }
  *Min     = RozsahOnlineForm->OdEdit->Text.ToInt();
  *Max     = RozsahOnlineForm->DoEdit->Text.ToInt();
  *Average = RozsahOnlineForm->PrumerEdit->Text.ToInt();
  return true;
}

//---------------------------------------------------------------------------
// Test otevreni souboru
//---------------------------------------------------------------------------

bool THlavniForm::JeOtevrenySoubor() {
  // Pokud je otevreny nejaky soubor, vrati true
  if (!PageControl->Visible) {
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Zobrazeni v nastaveni
//---------------------------------------------------------------------------

String THlavniForm::NactiNazevHejna() {
  // Nacte a vrati nazev hejna, podle ktereho se krmilo nebo krmi
  String Hejno;

  if (!Data->KonfiguraceTable->FieldByName("START_USE_FLOCK")->AsBoolean) {
    return QUICK_WEIGHING_TITLE;   // Nepouziva hejno, jde o rychle vazeni
  }
  if (!Data->HejnaTable->Locate("NUMBER", Data->KonfiguraceTable->FieldByName("START_FLOCK")->AsInteger, Data->SearchOptions)) {
    return "";                          // Hejno, podle ktereho se krmi nebo krmilo, neexistuje
  }
  Hejno = Data->KonfiguraceTable->FieldByName("START_FLOCK")->AsString + ": " + Data->HejnaTable->FieldByName("NAME")->AsString;
  Data->HejnaTable->First();            // Vratim na prvni zaznam
  return Hejno;
}

static void VymazTabulku(TStringGrid *Grid);
// Vymazu celou tabulku (nemazu zahlavi)

void THlavniForm::NactiSeznamHejn() {
  // Nacte vsechna hejna sady <Sada> do seznamu
  int Cislo;

  SeznamHejnListBox->Clear();
  Data->HejnaTable->First();
  Cislo = 0;
  while (!Data->HejnaTable->Eof) {
    if (Data->HejnaTable->FieldByName("NUMBER")->AsInteger == Cislo) {
      SeznamHejnListBox->Items->Add(Data->HejnaTable->FieldByName("NUMBER")->AsString);
    }
    Data->HejnaTable->Next();
    Cislo++;
  }//while
}

void THlavniForm::VyberPrvnihejno() {
  // Vybere ze seznamu prvni hejno
  if (SeznamHejnListBox->Items->Count > 0) {
    // V sade jsou nejaka hejna, vyberu prvni z nich
    SeznamHejnListBox->ItemIndex = 0;
    SeznamHejnListBoxClick(NULL);
  } else {
    // Zadne hejno v sade neni
    VymazTabulku(KrivkaSamiceStringGrid);
    VymazTabulku(KrivkaSamciStringGrid);
  }//else
}

static void VymazTabulku(TStringGrid *Grid) {
  // Vymazu celou tabulku (nemazu zahlavi)
  for (int row = 1; row < Grid->RowCount; row++) {
    for (int col = 1; col < Grid->ColCount; col++) {
      Grid->Cells[col][row] = "";
    }
  }
}

void THlavniForm::ZobrazKrivkuHejna(bool Pohlavi) {
  // Zobrazi krivku s pohlavim <Pohlavi> aktualniho hejna
  String Predpona;
  TStringGrid *Grid;

  // Nastavim pohlavi
  if (Pohlavi == GENDER_FEMALE) {
    Predpona = "FEMALE";
    Grid = KrivkaSamiceStringGrid;
  } else {
    Predpona = "MALE";
    Grid = KrivkaSamciStringGrid;
  }
  // Vymazu celou tabulku (nemazu zahlavi)
  VymazTabulku(Grid);
  // Ulozim z databaze do tabulky
  for (int i = 0; i < CURVE_MAX_POINTS; i++) {
    if (Data->HejnaTable->FieldByName(Predpona + "_WEIGHT" + FormatFloat("00", i))->AsFloat == 0) {
      break;   // Konec krivky
    }
    Grid->Cells[1][i + 1] = Data->HejnaTable->FieldByName(Predpona + "_DAY" + FormatFloat("00", i))->AsString;
    Grid->Cells[2][i + 1] = FormatFloat(FORMAT_HMOTNOSTI, Data->HejnaTable->FieldByName(Predpona + "_WEIGHT" + FormatFloat("00", i))->AsFloat);
  }//for
}

void THlavniForm::ZobrazKrivkyHejna() {
  // Zobrazi krivky aktualniho hejna
  if (Data->HejnaTable->FieldByName("USE_CURVES")->AsBoolean) {
    KrivkaSamiceTabSheet->TabVisible      = true;
    ZobrazKrivkuHejna(GENDER_FEMALE);
    KrivkaSamciTabSheet->TabVisible       = Data->HejnaTable->FieldByName("USE_GENDER")->AsBoolean;
    if (Data->HejnaTable->FieldByName("USE_GENDER")->AsBoolean) {
      ZobrazKrivkuHejna(GENDER_MALE);
    }
    KrivkyPageControl->ActivePage = KrivkaSamiceTabSheet;
    PocatecniHmotnostTabSheet->TabVisible = false;
  } else {
    KrivkaSamiceTabSheet->TabVisible      = false;
    KrivkaSamciTabSheet->TabVisible       = false;
    PocatecniHmotnostTabSheet->TabVisible = true;
    PocatecniHmotnostSamciPanel->Visible  = Data->HejnaTable->FieldByName("USE_GENDER")->AsBoolean;
  }//else
}

void THlavniForm::ZobrazHejno(int Hejno) {
  // Zobrazi obsah hejna cislo <Hejno>
  if (!Data->HejnaTable->Locate("NUMBER", Hejno, Data->SearchOptions)) {
    // Hejno neexistuje
    KrivkyPageControl->Visible = false;
    return;
  }
  KrivkyPageControl->Visible = true;
  ZobrazKrivkyHejna();
}

// --------------------------------------------------------------------------
// Zobrazeni a schovani prvku pro porovnani
// --------------------------------------------------------------------------

void THlavniForm::SchovejPrvkyPorovnani() {
  // Schova vsechny prvky pro porovnani souboru
  StatistikaPorovnaniPanel->Visible = false;
  // Histogram
  HistogramPorovnaniPanel->Visible = false;
  HistogramSplitter->Visible       = false;
  // Aktivita
  AktivitaPorovnaniPanel->Visible = false;
  AktivitaSplitter->Visible       = false;
  // Aktivita celkem
  AktivitaCelkemChart->Series[INDEX_AKTIVITA_CELKEM_COMP]->Active = false;
  AktivitaCelkemChart->Legend->Visible = false;
  // Rustova krivka
  KrivkaCRealCheckBox->Checked   = false;
  KrivkaCTargetCheckBox->Checked = false;
  KrivkaPorovnaniPanel->Visible  = false;
}

void THlavniForm::ZobrazPrvkyPorovnani() {
  // Zobrazi vsechny prvky pro porovnani souboru
  String Str;

  StatistikaPorovnaniPanel->Visible = true;
  // Histogram
  HistogramPorovnaniPanel->Visible = true;
  HistogramSplitter->Visible       = true;
  // Aktivita
  AktivitaPorovnaniPanel->Visible = true;
  AktivitaSplitter->Visible       = true;
  // Aktivita celkem
  AktivitaCelkemChart->Series[INDEX_AKTIVITA_CELKEM_COMP]->Active = true;
  AktivitaCelkemChart->Legend->Visible = true;
  // Rustova krivka
  KrivkaCRealCheckBox->Checked   = true;
  KrivkaCTargetCheckBox->Checked = true;
  KrivkaPorovnaniPanel->Visible  = true;
  // Nazev souboru porovnani
  Str = ChangeFileExt(ExtractFileName(Data->Soubor.JmenoPorovnani), "");
  StatistikaPorovnaniLabel->Caption = Str;
  HistogramPorovnaniLabel->Caption  = Str;
  AktivitaPorovnaniLabel->Caption   = Str;
  KrivkaPorovnaniLabel->Caption     = Str;
  // Nastavim velikosti grafu
  FormResize(NULL);
}

// --------------------------------------------------------------------------
// Schovani prvku pro porovnani s teoretickou rustovou krivkou
// --------------------------------------------------------------------------

void THlavniForm::HideCompareCurveItems() {
  // Schova vsechny prvky pro porovnani s teoretickou rustovou krivkou
  KrivkaPorovnatComboBox->ItemIndex = 0;        // Vyberu bez porovnani, coz i schova krivku v grafu
  ReportPorovnatComboBox->ItemIndex = 0;        // To same v zalozce report
}

// --------------------------------------------------------------------------
// Rozdeleni pohlavi
// --------------------------------------------------------------------------

bool THlavniForm::PouzivaObePohlavi() {
  // Zjisti, zda se pouzivaji obe pohlavi
  if (Data->KonfiguraceTable->FieldByName("START_USE_FLOCK")->AsBoolean) {
    // Krmi se podle nejakeho hejna
    if (!Data->HejnaTable->Locate("NUMBER", Data->KonfiguraceTable->FieldByName("START_FLOCK")->AsInteger, Data->SearchOptions)) {
      return false;   // Nenasel jsem hejno
    }
    return Data->HejnaTable->FieldByName("USE_GENDER")->AsBoolean;
  } else {
    // Krmi se pomoci rucne zadanych parametru
    return Data->KonfiguraceTable->FieldByName("START_FAST_USE_GENDER")->AsBoolean;
  }
}

// --------------------------------------------------------------------------
// Zobrazeni dne
// --------------------------------------------------------------------------

bool THlavniForm::NastavDen(int CisloDne) {
  // Nastavim a zobrazim den na zadany den
  int Pozice;

  Pozice = DenComboBox->Items->IndexOf(String(CisloDne));
  if (Pozice < 0) {
    return false;   // Zadany den neni v seznamu
  }
  DenComboBox->ItemIndex = Pozice;
  Data->Soubor.Den = CisloDne;

  // Zobrazim nastaveny den
  ZobrazDen(Data->Soubor.Den);
  ResetOsy();
  return true;
}

// --------------------------------------------------------------------------
// Otevreni souboru
// --------------------------------------------------------------------------

static void ZobrazMenu(bool Vazeni, bool Waiting) {
  // Zobrazi nebo skryje polozky menu. POkud se ceka na start vazeni, skryje vse.
  // Povolim otevreni porovnani - jen u vazeni
  HlavniForm->Openforcomparsion1->Enabled     = (Vazeni && !Waiting);           // Dovolim mu otevreni souboru pro porovnani
  // Nastavim menu pro tisk
  HlavniForm->MenuPrintReport->Enabled        = (Vazeni && !Waiting);
  HlavniForm->MenuPrintSamples->Enabled       = (Vazeni && !Waiting);
  HlavniForm->MenuPrintHistogram->Enabled     = (Vazeni && !Waiting);
  HlavniForm->MenuPrintDayActivity->Enabled   = (Vazeni && !Waiting);
  HlavniForm->MenuPrintTotalActivity->Enabled = (Vazeni && !Waiting);
  HlavniForm->MenuPrintGrowthCurve->Enabled   = (Vazeni && !Waiting);
  HlavniForm->MenuPrintOnline->Enabled        = (!Vazeni && !Waiting);
  // Nastavim menu pro export
  HlavniForm->MenuExportReport->Enabled        = (Vazeni && !Waiting);
  HlavniForm->MenuExportSamples->Enabled       = (Vazeni && !Waiting);
  HlavniForm->MenuExportHistogram->Enabled     = (Vazeni && !Waiting);
  HlavniForm->MenuExportDayActivity->Enabled   = (Vazeni && !Waiting);
  HlavniForm->MenuExportTotalActivity->Enabled = (Vazeni && !Waiting);
  HlavniForm->MenuExportGrowthCurve->Enabled   = (Vazeni && !Waiting);
  HlavniForm->MenuExportOnline->Enabled        = (!Vazeni && !Waiting);
  // Nastavim menu pro simulaci
  HlavniForm->MenuSimulation->Enabled          = (!Vazeni && !Waiting);
  // Ikona Save je po otevreni libovolneho souboru vzdy aktivni
  HlavniForm->SaveSpeedButton->Enabled = true;
  // Menu pro databazi vazeni
  HlavniForm->AddSpeedButton->Enabled          = (Vazeni && !Waiting);
  HlavniForm->Addtoweighingdatabase1->Enabled  = (Vazeni && !Waiting);
}

static void ZobrazZalozky(bool Vazeni, bool Waiting) {
  // Zobrazi nebo skryje zalozky pro vazeni/online. POkud se ceka na start vazeni, skryje vse
  HlavniForm->ReportTabSheet->TabVisible         = (Vazeni && !Waiting);
  HlavniForm->VazeniTabSheet->TabVisible         = (Vazeni && !Waiting);
  HlavniForm->StatistikaTabSheet->TabVisible     = (Vazeni && !Waiting);
  HlavniForm->HistogramTabSheet->TabVisible      = (Vazeni && !Waiting);
  HlavniForm->AktivitaDenTabSheet->TabVisible    = (Vazeni && !Waiting);
  HlavniForm->AktivitaCelkemTabSheet->TabVisible = (Vazeni && !Waiting);
  HlavniForm->KrivkaTabSheet->TabVisible         = (Vazeni && !Waiting);
  HlavniForm->OnlineTabSheet->TabVisible         = (!Vazeni && !Waiting);
  HlavniForm->OnlineGrafTabSheet->TabVisible     = (!Vazeni && !Waiting);
  if (Waiting) {
    HlavniForm->PageControl->ActivePage = HlavniForm->NastaveniTabSheet;
  } else if (Vazeni) {
    HlavniForm->PageControl->ActivePage = HlavniForm->ReportTabSheet;
  } else {
    HlavniForm->PageControl->ActivePage = HlavniForm->OnlineTabSheet;
  }
}


void THlavniForm::FillDays() {
  // Vyplni seznam dnu, max a min den Data->Soubor
  TQuery *Query;

  // Seznam dnu
  Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    Query->SQL->Clear();
    if (Data->Soubor.TypDat == DATA_VAZENI) {
      Query->SQL->Add("SELECT DAY_NUMBER FROM \"" + Data->StatistikaTable->TableName + "\"");
    } else {
      Query->SQL->Add("SELECT DAY_NUMBER FROM \"" + Data->OnlineTable->TableName + "\"");
    }
    Query->SQL->Add("GROUP BY DAY_NUMBER");
    Query->SQL->Add("ORDER BY DAY_NUMBER");
    Query->Open();
    Query->First();
    DenComboBox->Clear();
    while (!Query->Eof) {
      DenComboBox->Items->Add(Query->FieldByName("DAY_NUMBER")->AsString);
      Query->Next();
    }
  } catch(...) {}
  delete Query;

  // Minimalni a maximalni den
  Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    Query->SQL->Clear();
    if (Data->Soubor.TypDat == DATA_VAZENI) {
      Query->SQL->Add("SELECT MIN(DAY_NUMBER) AS MIN_DAY, MAX(DAY_NUMBER) AS MAX_DAY FROM \"" + Data->StatistikaTable->TableName + "\"");
    } else {
      Query->SQL->Add("SELECT MIN(DAY_NUMBER) AS MIN_DAY, MAX(DAY_NUMBER) AS MAX_DAY FROM \"" + Data->OnlineTable->TableName + "\"");
    }
    Query->Open();
    Data->Soubor.MinDen = Query->FieldByName("MIN_DAY")->AsInteger;
    Data->Soubor.MaxDen = Query->FieldByName("MAX_DAY")->AsInteger;
  } catch(...) {
    Data->Soubor.MinDen = 1;
    Data->Soubor.MaxDen = 1;
  }
  delete Query;
}

void THlavniForm::ShowFirstDay() {
  // Nastavi prvni den a zobrazi ho
  NastavDen(Data->Soubor.MinDen);
  CurveRightAxisComboBox->ItemIndex = _CURVE_STAT_DEFAULT;      // Nastavim na default
  ZobrazRustovouKrivku();
  ZobrazAktivituCelkem();
  ZobrazReport();
}

void THlavniForm::OtevriTabulkyJakoSoubor(String Jmeno) {
  // Otevre vytvorene tabulky jako soubour s nazvem <Jmeno>
  TQuery *Query;

  Data->Soubor.Jmeno = Jmeno;
  NastavNazevOkna(Jmeno);
  FileOpenTables();
  // Osetrim, aby mela HlavickaTable vzdy jen 1 zaznam
  while (Data->HlavickaTable->RecordCount>1) {
    Data->HlavickaTable->Last();
    Data->HlavickaTable->Delete();
  }//while

  // Udaje, ktere se zjistuji jen pri otevirani souboru

  // Jednotky
  if (Data->KonfiguraceTable->FieldByName("UNITS")->AsInteger == UNITS_KG) {
    Data->Soubor.Jednotky = LoadStr(KG);
  } else {
    Data->Soubor.Jednotky = LoadStr(LB);
  }
  CilovaHmotnostJednotkyLabel->Caption = Data->Soubor.Jednotky;
  PrumerJednotkyLabel->Caption         = Data->Soubor.Jednotky;
  PrirustekJednotkyLabel->Caption      = Data->Soubor.Jednotky;
  CilovaHmotnostJednotkyLabel->Caption = Data->Soubor.Jednotky;   // Online
  SimulaceVysledkyForm->CilovaHmotnostJednotkyLabel->Caption = Data->Soubor.Jednotky;  // Simulace
  SimulaceVysledkyForm->PrumerJednotkyLabel->Caption         = Data->Soubor.Jednotky;
  // Prumerovani online na 1
  OnlinePrumerovaniComboBox->ItemIndex = 0;
  PrumerovaniOnline = 1;

  // Typ dat
  Data->Soubor.TypDat  = (TTypDat)Data->HlavickaTable->FieldByName("DATA_TYPE")->AsInteger;
  Data->Soubor.Waiting = Data->KonfiguraceTable->FieldByName("START_WAITING")->AsBoolean;

  if (Data->Soubor.Waiting) {
    // Pokud se ceka na start vazeni, vyplnim pouze neco a vetsinu prvku skryju - archiv je nepristupny
    ZobrazNastaveni();
    ZobrazInformace();
    // Skryju ovladaci prvky
    ZahlaviPanel->Visible = false;
    ZobrazZalozky(true, true);
    PageControl->Visible = true;
    ZobrazMenu(true, true);
    return;
  }

  // Seznam dnu, max a min den
  FillDays();

  // Schovam porovnani (pokud mel nejake porovnani otevrene, musi znovu)
  SchovejPrvkyPorovnani();
  ZobrazitPorovnani = false;

  // Nactu teoreticke krivky
  LoadCurves();

  // Schovam porovnani s teoretickou rustovou krivkou
  HideCompareCurveItems();
  KrivkaPorovnatComboBoxChange(KrivkaPorovnatComboBox);         // Prekresli rustovou krivku

  // Nastavim den na prvni den
  Data->Soubor.Pohlavi = GENDER_FEMALE;
  ShowFirstDay();
  ZobrazNastaveni();
  ZobrazInformace();
  VyplnSimulaci();

  // Zobrazim v okne
  ZahlaviPanel->Visible = true;
  ZobrazZalozky((bool)(Data->Soubor.TypDat == DATA_VAZENI), false);
  PageControl->Visible = true;

  // Pokud se pouzivaji obe pohlavi, zobrazim i panel pro nastaveni pohlavi
  if (Data->Soubor.TypDat == DATA_VAZENI) {
    PohlaviPanel->Visible = PouzivaObePohlavi();
  } else {
    PohlaviPanel->Visible = false;
  }

  ResetOsy();
  // 26.9.2005: Pri nastaveni prumerovani menim min a max => po otevreni noveho souboru vynuluju. Nelze dat do ResetOsy(),
  // nastavovalo by to i pri prechodu na novy den.
  HlavniForm->OnlineChart->BottomAxis->Automatic = true;

  ZobrazMenu((bool)(Data->Soubor.TypDat == DATA_VAZENI), false);
}

void THlavniForm::ZavriTabulkyPorovnani() {
  // Zavre vsechny tabulky souboru pro porovnani
  Data->CZaznamyQuery->Close();
  Data->CZaznamyTable->Close();
  Data->CStatistikaTable->Close();
  Data->CKonfiguraceTable->Close();
  Data->CHejnaTable->Close();
  Data->CKalibraceTable->Close();
  Data->COnlineTable->Close();
  Data->CHlavickaTable->Close();
}

void THlavniForm::OtevriTabulkyPorovnani() {
  // Smazu filtry
  Data->CZaznamyTable->Filter     = "";
  Data->CStatistikaTable->Filter  = "";
  Data->CKonfiguraceTable->Filter = "";
  Data->CHejnaTable->Filter       = "";
  Data->CKalibraceTable->Filter   = "";
  Data->COnlineTable->Filter      = "";
  Data->CHlavickaTable->Filter    = "";
  // Otevru
  Data->CZaznamyTable->Open();
  Data->CStatistikaTable->Open();
  Data->CKonfiguraceTable->Open();
  Data->CHejnaTable->Open();
  Data->CKalibraceTable->Open();
  Data->COnlineTable->Open();
  Data->CHlavickaTable->Open();
}

// --------------------------------------------------------------------------
// Zobrazeni konfigurace z nactenych dat
// --------------------------------------------------------------------------

bool THlavniForm::ZobrazKonfiguraci() {
  // Zobrazim konfiguraci, pokud stiskne OK, vrati true
  String Str;
  int Id;
  int Version = Data->Module->GetConfigVersion();

  if (Version >= 0x0110) {
    Id = KConvertGetWord(Data->Module->ConfigSection->Config.IdentificationNumber);     // Od verze 1.10 je id. cislo na 2 bajty
  } else {
    Id = Data->Module->ConfigSection109->Konfigurace.IdentifikacniCislo;        // Ve verzi 1.09 je to na 1 bajt
  }

  InformaceModulForm->IdLabel->Caption = Id;

  if (Data->Module->GetConfigOnline()) {
    InformaceModulForm->DataLabel->Caption = LoadStr(ONLINE);
  } else {
    InformaceModulForm->DataLabel->Caption = LoadStr(VAZENI);
  }

  Str = IntToHex(Version, 4);
  Str.Insert(".", 3);
  if (Str[1] == '0') {
    Str.Delete(1, 1);           // Umazu pocatecni nulu
  }
  if (Version >= 0x0110) {
    // U verze 1.10 a vyssi doplnim build a HW verzi
    Str += "." + AnsiString(Data->Module->ConfigSection->Config.Build) + "  ";
    switch (Data->Module->ConfigSection->Config.HwVersion) {
      case HW_VERSION_GSM:
        Str += LoadStr(STR_HW_VERSION_GSM);
        break;

      case HW_VERSION_DACS_LW1:
        Str += LoadStr(STR_HW_VERSION_LW1);
        break;
    }
  }//if
  InformaceModulForm->VerzeLabel->Caption = Str;
  InformaceModulForm->StartButton->Enabled = (bool)(Id != ID_WITH_SETUP);        // Pokud modul obsahuje jen setup, neni co stahovat
  return (bool)(InformaceModulForm->ShowModal() == mrOk);
}

// --------------------------------------------------------------------------
// Nacteni a zobrazeni konfigurace z modulu
// --------------------------------------------------------------------------

bool THlavniForm::NactiKonfiguraci() {
  if (!Data->ModulPripraven()) {
    return false;
  }
  // Nactu konfiguraci
  Screen->Cursor = crHourGlass;
  if (!Data->Module->ReadConfigSection()) {
    Screen->Cursor = crDefault;
    return false;
  }
  Screen->Cursor = crDefault;

  // Zobrazim konfiguraci
  return ZobrazKonfiguraci();
}

// --------------------------------------------------------------------------
// Ulozeni konfigurace do modulu
// --------------------------------------------------------------------------

bool THlavniForm::UlozKonfiguraci() {
  if (!Data->ModulPripraven()) {
    return false;
  }
  // Nactu konfiguraci
  Screen->Cursor = crHourGlass;
  if (!Data->Module->WriteConfigSection()) {
    Screen->Cursor = crDefault;
    return false;
  }
  Screen->Cursor = crDefault;
  return true;
}

// --------------------------------------------------------------------------
// Nacteni dat z modulu
// --------------------------------------------------------------------------

bool THlavniForm::NactiDataZModulu() {
  // Nactu veskera data z modulu
  if (!NactiKonfiguraci()) {
    return false;
  }

  // Nactu cely modul - predame rizeni do dialogu
  return ReadProgressForm->Execute(this);
}

// --------------------------------------------------------------------------
// Vytvoreni SQL dotazu reportu se statistikou
// --------------------------------------------------------------------------

static String CreateReportSQL() {
  // Vrati SQL prikaz pro vyber statistiky pro zobrazeni, tisk a export reportu pro vybrane pohlavi
  String Predpona;      // Predpona pro pohlavi
  String Select;

  // Nastavim pohlavi
  if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
    Predpona = "FEMALE";
  } else {
    Predpona = "MALE";
  }
  // Vytvorim Query
  Select = "SELECT DAY_NUMBER, DATE_TIME, "
         + Predpona + "_STAT_XSUM AS STAT_XSUM, "
         + Predpona + "_STAT_X2SUM AS STAT_X2SUM, "
         + Predpona + "_STAT_COUNT AS STAT_COUNT, ";
  for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
    Select += Predpona + "_HIST_VALUE" + FormatFloat("00", i) + " AS HIST_VALUE, ";
    Select += Predpona + "_HIST_COUNT" + FormatFloat("00", i) + " AS HIST_COUNT, ";
  }//for
  Select += Predpona + "_TARGET AS TARGET, ";
  Select += Predpona + "_YESTERDAY_AVERAGE AS YESTERDAY_AVERAGE, ";
  Select += Predpona + "_YESTERDAY_AVERAGE AS YESTERDAY_AVERAGE, ";
  Select += "USE_REAL_UNI, ";
  Select += Predpona + "_REAL_UNI AS REAL_UNI";
  Select += " FROM " + Data->StatistikaTable->TableName;
  return Select;
}

// --------------------------------------------------------------------------
// Zobrazeni reportu se statistikou
// --------------------------------------------------------------------------

void THlavniForm::ZobrazReport() {
  // Zobrazi report se statistikou pro vybrane pohlavi

  // Skryju nebo zobrazim sloupce pro porovnani
  bool Compare = Data->GetCompareCurve(COMPARE_CURVE_MAIN_FORM);
  ReportDBGrid->Columns->Items[8]->Visible = Compare;
  ReportDBGrid->Columns->Items[9]->Visible = Compare;

  Data->StatistikaQuery->Close();
  Data->StatistikaQuery->SQL->Clear();
  Data->StatistikaQuery->SQL->Add(CreateReportSQL());
  Data->StatistikaQuery->Open();
}

// --------------------------------------------------------------------------
// Zobrazeni statistiky
// --------------------------------------------------------------------------

void THlavniForm::ZobrazStatistiku() {
  // Zobrazi statistiku za vybrany den a pohlavi
  TStatistic Stat;
  String Pohlavi;     // FEMALE/MALE
  float Average, Sigma;

  if (PohlaviSamiceRadioButton->Checked) {
    Pohlavi = "FEMALE";
  } else {
    Pohlavi = "MALE";
  }

  // Aktualni otevreny soubor
  Stat.XSuma  = Data->StatistikaTable->FieldByName(Pohlavi + "_STAT_XSUM")->AsFloat;
  Stat.X2Suma = Data->StatistikaTable->FieldByName(Pohlavi + "_STAT_X2SUM")->AsFloat;
  Stat.Count  = Data->StatistikaTable->FieldByName(Pohlavi + "_STAT_COUNT")->AsInteger;
  Average = StatAverage(&Stat);
  Sigma = StatSigma(&Stat);
  CilovaHmotnostLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, Data->StatistikaTable->FieldByName(Pohlavi + "_TARGET")->AsFloat);
  PrumerLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average));   // V Keilu to delam pres int
  if (Data->Soubor.Den == Data->Soubor.MinDen) {
    // Prvni den je prirustek 0
    PrirustekLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, 0);
  } else {
    // Dalsi dny prirustek normalne spocitam
    PrirustekLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average) - Data->StatistikaTable->FieldByName(Pohlavi + "_YESTERDAY_AVERAGE")->AsFloat);
  }
  PocetLabel->Caption = Stat.Count;
  SigmaLabel->Caption = FormatFloat("0.000", (double)Sigma / 1000.0);
  CvLabel->Caption = FormatFloat("0.0", StatVariation(Average,Sigma));
  if (Data->StatistikaTable->FieldByName("USE_REAL_UNI")->AsBoolean) {
    UniLabel->Caption = FormatFloat("0", Data->StatistikaTable->FieldByName(Pohlavi + "_REAL_UNI")->AsInteger);
  } else {
    UniLabel->Caption = FormatFloat("0", StatUniformity(Average, Sigma, Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger));
  }

  // Soubor porovnani
  if (!ZobrazitPorovnani) {
    return;
  }
  Stat.XSuma  = Data->CStatistikaTable->FieldByName(Pohlavi + "_STAT_XSUM")->AsFloat;
  Stat.X2Suma = Data->CStatistikaTable->FieldByName(Pohlavi + "_STAT_X2SUM")->AsFloat;
  Stat.Count  = Data->CStatistikaTable->FieldByName(Pohlavi + "_STAT_COUNT")->AsInteger;
  Average = StatAverage(&Stat);
  Sigma = StatSigma(&Stat);
  CilovaHmotnostPorovnaniLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, Data->CStatistikaTable->FieldByName(Pohlavi + "_TARGET")->AsFloat);
  PrumerPorovnaniLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average));   // V Keilu to delam pres int
  // Soubor porovnani ma prvni den vazeni odlisny od prave otevreneho souboru, nactu ho z CKonfiguraceTable
  if (Data->Soubor.Den == Data->CKonfiguraceTable->FieldByName("START_CURVE_MOVE")->AsInteger + CURVE_FIRST_DAY_NUMBER) {
    // Prvni den je prirustek 0
    PrirustekPorovnaniLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, 0);
  } else {
    // Dalsi dny prirustek normalne spocitam
    PrirustekPorovnaniLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average) - Data->CStatistikaTable->FieldByName(Pohlavi + "_YESTERDAY_AVERAGE")->AsFloat);
  }
  PocetPorovnaniLabel->Caption = Stat.Count;
  SigmaPorovnaniLabel->Caption = FormatFloat("0.000", (double)Sigma / 1000.0);
  CvPorovnaniLabel->Caption = FormatFloat("0.0", StatVariation(Average,Sigma));
  if (Data->CStatistikaTable->FieldByName("USE_REAL_UNI")->AsBoolean) {
    UniPorovnaniLabel->Caption = FormatFloat("0", Data->CStatistikaTable->FieldByName(Pohlavi + "_REAL_UNI")->AsInteger);
  } else {
    UniPorovnaniLabel->Caption = FormatFloat("0", StatUniformity(Average, Sigma, Data->CKonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger));
  }
}

// --------------------------------------------------------------------------
// Zobrazeni histogramu
// --------------------------------------------------------------------------

void THlavniForm::ZobrazHistogram() {
  // Zobrazi histogram za vybrany den a pohlavi
  // Data bere ze StatistikaTable, ktera uz musi mit nastaveny filtr na zvoleny den
  String Pohlavi;     // FEMALE/MALE

  if (PohlaviSamiceRadioButton->Checked) {
    Pohlavi = "FEMALE";
  } else {
    Pohlavi = "MALE";
  }

  // Zaznamy
  HistogramChart->Series[0]->Clear();                   // Vymazu z grafu vsechny predchozi hodnoty
  if (Data->StatistikaTable->RecordCount > 0) {
    for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
      HistogramChart->Series[0]->AddXY(PrepoctiNaKg(Data->StatistikaTable->FieldByName(Pohlavi + "_HIST_VALUE" + FormatFloat("00", i))->AsFloat),
                                       Data->StatistikaTable->FieldByName(Pohlavi + "_HIST_COUNT" + FormatFloat("00", i))->AsFloat,
                                       "",   // Popisek
                                       clTeeColor);
    }//for i
  }//if
  HistogramChart->BottomAxis->AdjustMaxMin();  // Prepocitam osu X, aby se pripadne porovnani nastavilo stejne (pokud je nastavena stejna osa)

  // Porovnani
  if (ZobrazitPorovnani) {
    HistogramPorovnaniChart->Series[0]->Clear();
    if (Data->CStatistikaTable->RecordCount > 0) {
      for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
        HistogramPorovnaniChart->Series[0]->AddXY(PrepoctiNaKg(Data->CStatistikaTable->FieldByName(Pohlavi + "_HIST_VALUE" + FormatFloat("00", i))->AsFloat),
                                                  Data->CStatistikaTable->FieldByName(Pohlavi + "_HIST_COUNT" + FormatFloat("00", i))->AsFloat,
                                                  "",   // Popisek
                                                  clTeeColor);
      }//for i
    }//if
    HistogramPorovnaniOsaCheckBoxClick(NULL);   // Nastavim osy
  }//if
}

// --------------------------------------------------------------------------
// Zobrazeni aktivity
// --------------------------------------------------------------------------

#define VytvorQueryAktivity(Table)                                                           \
  Query->SQL->Clear();                                                                       \
  Query->SQL->Add("SELECT TIME_HOUR, COUNT(*) AS POCET FROM \"" + Table->TableName + "\"");  \
  Query->SQL->Add("WHERE DAY_NUMBER = " + String(Data->Soubor.Den));                         \
  Query->SQL->Add("AND GENDER = " + Pohlavi);                                                \
  Query->SQL->Add("GROUP BY TIME_HOUR");                                                     \
  Query->Open();

void THlavniForm::ZobrazAktivitu() {
  // Zobrazi aktivitu za vybrany den
  String Pohlavi;     // FEMALE/MALE
  TQuery *Query;
  int Pocet;

  if (PohlaviSamiceRadioButton->Checked) {
    Pohlavi = "FALSE";
  } else {
    Pohlavi = "TRUE";
  }

  Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    // Zaznamy
    VytvorQueryAktivity(Data->ZaznamyTable);
    Query->First();
    AktivitaDenChart->Series[0]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
    // Vlozim do grafu vzdy vsechny hodiny, i kdyz tam aktivita nebyla. Pokud bych nevlozil, blbnula by mne sirka sloupce
    for (int Hodina = 0; Hodina < 24; Hodina++) {
      if (Query->Locate("TIME_HOUR", Hodina, Data->SearchOptions)) {
        Pocet = Query->FieldByName("POCET")->AsInteger;
      } else {
        Pocet = 0;
      }
      AktivitaDenChart->Series[0]->AddXY(Hodina, Pocet, "", clTeeColor);
    }//for

    // Porovnani
    if (ZobrazitPorovnani) {
      VytvorQueryAktivity(Data->CZaznamyTable);
      Query->First();
      AktivitaDenPorovnaniChart->Series[0]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
      // Vlozim do grafu vzdy vsechny hodiny, i kdyz tam aktivita nebyla. Pokud bych nevlozil, blbnula by mne sirka sloupce
      for (int Hodina = 0; Hodina < 24; Hodina++) {
        if (Query->Locate("TIME_HOUR", Hodina, Data->SearchOptions)) {
          Pocet = Query->FieldByName("POCET")->AsInteger;
        } else {
          Pocet = 0;
        }
        AktivitaDenPorovnaniChart->Series[0]->AddXY(Hodina, Pocet, "", clTeeColor);
      }//for
    }//if
  } catch(...) {}
  delete Query;
}

// --------------------------------------------------------------------------
// Zobrazeni celkove aktivity
// --------------------------------------------------------------------------

#define VytvorQueryAktivityCelkem(Table, Pohlavi)                                                               \
  Query->SQL->Clear();                                                                                          \
  Query->SQL->Add("SELECT DAY_NUMBER, " + Pohlavi + "_STAT_COUNT AS POCET FROM \"" + Table->TableName + "\"");  \
  Query->SQL->Add("ORDER BY DAY_NUMBER");                                                                       \
  Query->Open();


void THlavniForm::ZobrazAktivituCelkem() {
  // Zobrazi rustovou krivku za cely vykrm
  // Volat po otevreni souboru a pri zmene pohlavi
  String Pohlavi;     // FEMALE/MALE
  TQuery *Query;
  TStatistic Stat;
  float Average;

  if (PohlaviSamiceRadioButton->Checked) {
    Pohlavi = "FEMALE";
  } else {
    Pohlavi = "MALE";
  }

  Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    // Zaznamy
    VytvorQueryAktivityCelkem(Data->StatistikaTable, Pohlavi);
    Query->First();
    AktivitaCelkemChart->Series[INDEX_AKTIVITA_CELKEM]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
    while (!Query->Eof) {
      AktivitaCelkemChart->Series[INDEX_AKTIVITA_CELKEM]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                                          Query->FieldByName("POCET")->AsInteger,
                                                          "",   // Popisek
                                                          clTeeColor);
      Query->Next();
    }//while

    // Porovnani
    if (ZobrazitPorovnani) {
      VytvorQueryAktivityCelkem(Data->CStatistikaTable, Pohlavi);
      Query->First();
      AktivitaCelkemChart->Series[INDEX_AKTIVITA_CELKEM_COMP]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
      while (!Query->Eof) {
        AktivitaCelkemChart->Series[INDEX_AKTIVITA_CELKEM_COMP]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                                            Query->FieldByName("POCET")->AsInteger,
                                                            "",   // Popisek
                                                            clTeeColor);
        Query->Next();
      }//while
    }//if
  } catch(...) {}
  delete Query;
}

// --------------------------------------------------------------------------
// Zobrazeni rustove krivky
// --------------------------------------------------------------------------

#define VytvorQueryRustoveKrivky(Table)                                         \
  Query->SQL->Clear();                                                          \
  Query->SQL->Add("SELECT DAY_NUMBER, "                                         \
                  + Pohlavi + "_STAT_XSUM AS STAT_XSUM, "                       \
                  + Pohlavi + "_STAT_X2SUM AS STAT_X2SUM, "                     \
                  + Pohlavi + "_STAT_COUNT AS STAT_COUNT, "                     \
                  + Pohlavi + "_TARGET AS TARGET, "                             \
                  + Pohlavi + "_YESTERDAY_AVERAGE AS YESTERDAY_AVERAGE, "       \
                  + "USE_REAL_UNI, " + Pohlavi + "_REAL_UNI AS REAL_UNI "       \
                  + "FROM \"" + Table->TableName + "\"");                       \
  Query->Open();

void THlavniForm::ZobrazRustovouKrivku() {
  // Zobrazi rustovou krivku za cely vykrm
  // Volat po otevreni souboru a pri zmene pohlavi
  String Pohlavi;     // FEMALE/MALE
  TQuery *Query;
  TStatistic Stat;
  float Average, Sigma;
  TGrowthCurve *Curve;
  double Value;

  if (PohlaviSamiceRadioButton->Checked) {
    Pohlavi = "FEMALE";
  } else {
    Pohlavi = "MALE";
  }

  Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;

  try {
    // Zaznamy
    VytvorQueryRustoveKrivky(Data->StatistikaTable);
    Query->First();
    KrivkaChart->Series[KRIVKA_REAL]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
    KrivkaChart->Series[KRIVKA_TARGET]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
    while (!Query->Eof) {
      Stat.XSuma = Query->FieldByName("STAT_XSUM")->AsFloat;
      Stat.Count = Query->FieldByName("STAT_COUNT")->AsInteger;
      Average = PrepoctiNaKg((int)StatAverage(&Stat));
      if (Average > 0) {
        KrivkaChart->Series[KRIVKA_REAL]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                      Average,
                                      "",   // Popisek
                                      clTeeColor);
      }//if
      if (Query->FieldByName("TARGET")->AsFloat > 0) {
        KrivkaChart->Series[KRIVKA_TARGET]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                      Query->FieldByName("TARGET")->AsFloat,
                                      "",   // Popisek
                                      clTeeColor);
      }//if
      Query->Next();
    }//while

    // Statistika na prave ose X - vyuziju uz existujici Query
    if (CurveRightAxisComboBox->ItemIndex == 0) {
      // Nechce zobrazit
      KrivkaChart->Series[KRIVKA_STATISTICS]->Active = false;
    } else {
      // Zobrazim vybrany prubeh
      KrivkaChart->Series[KRIVKA_STATISTICS]->Active = true;
      KrivkaChart->Series[KRIVKA_STATISTICS]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
      KrivkaChart->Series[KRIVKA_STATISTICS]->Title  = CurveRightAxisComboBox->Text;
      KrivkaChart->RightAxis->Title->Caption         = CurveRightAxisComboBox->Text;
      Query->First();
      while (!Query->Eof) {
        // Vypocet potrebnych velicin
        Stat.XSuma  = Query->FieldByName("STAT_XSUM")->AsFloat;
        Stat.X2Suma = Query->FieldByName("STAT_X2SUM")->AsFloat;
        Stat.Count  = Query->FieldByName("STAT_COUNT")->AsInteger;
        Average = PrepoctiNaKg((int)StatAverage(&Stat));
        Sigma   = StatSigma(&Stat);

        // Vypocet pozadovane hodnoty
        switch (CurveRightAxisComboBox->ItemIndex) {
          case CURVE_STAT_DAILY_GAIN:
          if (Query->FieldByName("DAY_NUMBER")->AsInteger == Data->Soubor.MinDen) {
            // Prvni den je prirustek 0
            Value = 0;
          } else {
            // Dalsi dny prirustek normalne spocitam
            Value = FormatFloat(FORMAT_HMOTNOSTI, Average - Query->FieldByName("YESTERDAY_AVERAGE")->AsFloat).ToDouble();
          }
            break;
          case CURVE_STAT_COUNT:
            Value = Stat.Count;
            break;
          case CURVE_STAT_SIGMA:
            Value = FormatFloat("0.000", (double)Sigma / 1000.0).ToDouble();
            break;
          case CURVE_STAT_CV:
            Value = FormatFloat("0.0", StatVariation(Average, Sigma / 1000.0)).ToDouble();
            break;
          default: // CURVE_STAT_UNIFORMITY:
            if (Query->FieldByName("USE_REAL_UNI")->AsBoolean) {
              Value = Query->FieldByName("REAL_UNI")->AsInteger;
            } else {
              Value = FormatFloat("0", StatUniformity(Average, Sigma / 1000.0, Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger)).ToInt();
            }
        }

        // Pridam hodnotu do grafu
        KrivkaChart->Series[KRIVKA_STATISTICS]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                                      Value,
                                                      "",   // Popisek
                                                      clTeeColor);
        Query->Next();
      }//while
    }

    // Porovnani
    if (ZobrazitPorovnani) {
      VytvorQueryRustoveKrivky(Data->CStatistikaTable);
      Query->First();
      KrivkaChart->Series[KRIVKA_COMP_REAL]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
      KrivkaChart->Series[KRIVKA_COMP_TARGET]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
      while (!Query->Eof) {
        Stat.XSuma = Query->FieldByName("STAT_XSUM")->AsFloat;
        Stat.Count = Query->FieldByName("STAT_COUNT")->AsInteger;
        Average = PrepoctiNaKg((int)StatAverage(&Stat));
        if (Average > 0) {
          KrivkaChart->Series[KRIVKA_COMP_REAL]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                        Average,
                                        "",   // Popisek
                                        clTeeColor);
        }//if
        if (Query->FieldByName("TARGET")->AsFloat > 0) {
          KrivkaChart->Series[KRIVKA_COMP_TARGET]->AddXY(Query->FieldByName("DAY_NUMBER")->AsInteger,
                                        Query->FieldByName("TARGET")->AsFloat,
                                        "",   // Popisek
                                        clTeeColor);
        }//if
        Query->Next();
      }//while
    }//if

    // Porovnani s teoretickou rustovou krivkou
    Curve = Data->GetCompareCurve(COMPARE_CURVE_MAIN_FORM);
    if (Curve) {
      // Zobrazim prubeh v grafu
      KrivkaChart->Series[KRIVKA_THEORY]->Active = true;
      KrivkaChart->Series[KRIVKA_THEORY]->Title  = KrivkaPorovnatComboBox->Text;

      // Vynesu krivku do grafu
      KrivkaChart->Series[KRIVKA_THEORY]->Clear();      // Vymazu z grafu vsechny predchozi hodnoty
      for (int i = 0; i < Curve->Count(); i++) {
        KrivkaChart->Series[KRIVKA_THEORY]->AddXY(Curve->GetDay(i),
                                                  Curve->GetWeight(i),
                                                  "",   // Popisek
                                                  clTeeColor);
      }
    } else {
      // Schovam prubeh v grafu
      KrivkaChart->Series[KRIVKA_THEORY]->Active = false;
    }

  } catch(...) {}
  delete Query;
}

// --------------------------------------------------------------------------
// Zobrazeni nastaveni vahy
// --------------------------------------------------------------------------

void THlavniForm::ZobrazNastaveni() {
  // Zobrazi nastaveni vahy
  // Volat jen po otevreni souboru
  NastaveniHejnoEdit->Text = NactiNazevHejna();
  NactiSeznamHejn();
  VyberPrvnihejno();
  NastaveniGainCheckBox->Checked = (bool)(Data->KonfiguraceTable->FieldByName("AUTO_MODE")->AsInteger == AUTOMODE_WITH_GAIN);
  NastaveniPageControl->ActivePage = NastaveniVazeniTabSheet;
}

// --------------------------------------------------------------------------
// Vyplneni pro online simulaci
// --------------------------------------------------------------------------

void THlavniForm::VyplnSimulaci() {
  // Vyplni okno pro online simulaci
  // Volat jen po otevreni souboru
  // Data
  SimulaceStartForm->GrafRadioButton->Checked = true;
  SimulaceStartForm->OdEdit->Text = 1;
  SimulaceStartForm->DoEdit->Text = Data->OnlineQuery->RecordCount;
  // Hmotnost
  SimulaceStartForm->ObePohlaviCheckBox->Checked = false;
  SimulaceStartForm->CilovaHmotnostSamiceEdit->Text = "";
  SimulaceStartForm->CilovaHmotnostSamciEdit->Text = "";
  // Statistika
  SimulaceStartForm->RozsahHistogramuEdit->Text = Data->KonfiguraceTable->FieldByName("HIST_RANGE")->AsString;
  SimulaceStartForm->RozsahUniformityEdit->Text = Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsString;
  // Nastaveni vah
  SimulaceStartForm->SamiceOkoliNadEdit->Text = Data->KonfiguraceTable->FieldByName("F_MARGIN_ABOVE")->AsString;
  SimulaceStartForm->SamiceOkoliPodEdit->Text = Data->KonfiguraceTable->FieldByName("F_MARGIN_UNDER")->AsString;
  SimulaceStartForm->SamciOkoliNadEdit->Text = Data->KonfiguraceTable->FieldByName("M_MARGIN_ABOVE")->AsString;
  SimulaceStartForm->SamciOkoliPodEdit->Text = Data->KonfiguraceTable->FieldByName("M_MARGIN_UNDER")->AsString;
  SimulaceStartForm->FiltrEdit->Text = "1";   // Filtr zde neznamena primo filtr, ale prumerovani
  SimulaceStartForm->UstaleniEdit->Text = Data->KonfiguraceTable->FieldByName("STABILIZATION")->AsString;
  SimulaceStartForm->DobaUstaleniEdit->Text = Data->KonfiguraceTable->FieldByName("STABILIZATION_TIME")->AsString;
}

// --------------------------------------------------------------------------
// Zobrazeni informaci o souboru
// --------------------------------------------------------------------------

void THlavniForm::ZobrazInformace() {
  // Zobrazi informace o souboru
  // Volat jen po otevreni souboru
  String Str;

  Str = IntToHex(Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger, 4);
  Str.Insert(".", 3);
  if (Str[1] == '0') {
    Str.Delete(1, 1);           // Umazu pocatecni nulu
  }
  if (Data->KonfiguraceTable->FieldByName("VERSION_FW")->AsInteger >= 0x0110) {
    // U verze 1.10 a vyssi doplnim build a HW verzi
    Str += "." + Data->KonfiguraceTable->FieldByName("BUILD")->AsString + "  ";
    switch (Data->KonfiguraceTable->FieldByName("HW_VERSION")->AsInteger) {
      case HW_VERSION_GSM:
        Str += LoadStr(STR_HW_VERSION_GSM);
        break;

      case HW_VERSION_DACS_LW1:
        Str += LoadStr(STR_HW_VERSION_LW1);
        break;
    }
  }//if
  VerzeLabel->Caption = Str;
}

// --------------------------------------------------------------------------
// Zobrazeni online grafu
// --------------------------------------------------------------------------

void THlavniForm::ZobrazOnlineGraf() {
  // Zobrazi graf online mereni za vybrany den
  int Prumerovani;
  double Hmotnost = 0;
  int Pocet = 0;

  Prumerovani = OnlinePrumerovaniComboBox->ItemIndex + 1;
  if (Prumerovani <= 0) {
    return;
  }

  try {
    // Zaznamy
    Data->OnlineQuery->DisableControls();
    Data->OnlineQuery->First();
    OnlineChart->Series[0]->Clear();    // Vymazu z grafu vsechny predchozi hodnoty
    while (!Data->OnlineQuery->Eof) {
      Hmotnost += Data->OnlineQuery->FieldByName("WEIGHT")->AsFloat;
      Pocet++;
      if (Pocet >= Prumerovani) {
        Hmotnost /= (double)Pocet;
        Hmotnost = FormatFloat("0.000", Hmotnost).ToDouble();     // Zaokrouhlim na gramy
        OnlineChart->Series[0]->AddY(Hmotnost, "", clTeeColor);
        Pocet = 0;
        Hmotnost = 0;
      }//if
      Data->OnlineQuery->Next();
    }//for
  } catch(...) {}
  Data->OnlineQuery->First();
  Data->OnlineQuery->EnableControls();
}

// --------------------------------------------------------------------------
// Zobrazeni dne
// --------------------------------------------------------------------------

static void NastavFiltrNaZaznamy(TQuery *Query, String Filtr) {
  // Nastavim filtr na zaznamy
  try {
    Query->Close();
    Query->SQL->Clear();
    Query->SQL->Add("SELECT TIME_HOUR, WEIGHT FROM \"" + Data->ZaznamyTable->TableName + "\"");
    Query->SQL->Add("WHERE " + Filtr);
    Query->Open();
  } catch(...) {}
}

static void NastavFiltrNaOnline(TQuery *Query, String Filtr) {
  // Nastavim filtr na online zaznamy
  try {
    Query->Close();
    Query->SQL->Clear();
    Query->SQL->Add("SELECT TIME_HOUR, WEIGHT, SAVED, STABLE FROM \"" + Data->OnlineTable->TableName + "\"");
    Query->SQL->Add("WHERE " + Filtr);
    Query->Open();
  } catch(...) {}
}

void THlavniForm::ZobrazDen(int Den) {
  // Zobrazi nastaveny den a zvolene pohlavi
  String FiltrZaznamy;    // Sestavuju postupne
  String FiltrStatistika;

  if (Den < Data->Soubor.MinDen || Den > Data->Soubor.MaxDen) {
    return;
  }

  Screen->Cursor = crHourGlass;
  try {
    // Nastavim filtr na tabulky se zaznamy a se statistikou
    FiltrZaznamy    = "DAY_NUMBER = " + String(Den);
    FiltrStatistika = "DAY_NUMBER = " + String(Den);

    // Pohlavi
    if (Data->Soubor.Pohlavi == GENDER_FEMALE) {
      PohlaviSamiceRadioButton->Checked = true;
      FiltrZaznamy += " AND GENDER = FALSE";
    } else {
      PohlaviSamciRadioButton->Checked = true;
      FiltrZaznamy += " AND GENDER = TRUE";
    }//else

    // Nastavim filtr na zaznamy
    NastavFiltrNaZaznamy(Data->ZaznamyQuery, FiltrZaznamy);
    NastavFiltrNaOnline(Data->OnlineQuery, "DAY_NUMBER = " + String(Den));
    Data->StatistikaTable->Filter = "DAY_NUMBER = " + String(Den);

    // Nastavim filtr na zaznamy porovnani
    if (ZobrazitPorovnani) {
      NastavFiltrNaZaznamy(Data->CZaznamyQuery, FiltrZaznamy);
      Data->CStatistikaTable->Filter = "DAY_NUMBER = " + String(Den);
    }//if

    // Nactu datum daneho dne a zobrazim ho v hlavicce
    if (Data->Soubor.TypDat == DATA_VAZENI) {
      DatumLabel->Caption = Data->StatistikaTable->FieldByName("DATE_TIME")->AsDateTime.FormatString("ddddd");
    } else {
      TDateTime d;
      d = Data->KonfiguraceTable->FieldByName("START_DATE")->AsDateTime
        + Den
        - Data->KonfiguraceTable->FieldByName("START_CURVE_MOVE")->AsInteger
        - CURVE_FIRST_DAY_NUMBER;
      DatumLabel->Caption = d.FormatString("ddddd");
    }

    ZobrazStatistiku();
    ZobrazHistogram();
    ZobrazAktivitu();
    ZobrazOnlineGraf();
  } catch(...) {}
  Screen->Cursor = crDefault;
}

// --------------------------------------------------------------------------
// Nazev okna
// --------------------------------------------------------------------------

void THlavniForm::NastavNazevOkna(String NazevSouboru) {
  String Str;
//  Str=LoadStr(HLAVNI_NAZEV_PROGRAMU);
  Str = Data->Version.ScaleTitle;
  if (!NazevSouboru.IsEmpty()) {
    Str += " - ";
    Str += ExtractFileName(NazevSouboru);
  }//else
  Caption = Str;
}

// --------------------------------------------------------------------------
// Nacteni nastaveni
// --------------------------------------------------------------------------

// Pokud neexistuje, prida pole <Field> s typem <Type>
#define CheckField(Field, Type)                                         \
  Table->Open();                                                        \
  if (Table->FieldDefs->IndexOf(Field) < 0) {                           \
    Table->Close();                                                     \
    Query->SQL->Clear();                                                \
    Query->SQL->Add("ALTER TABLE \"" + Table->TableName + "\"");        \
    Query->SQL->Add("ADD " Field " " Type);                             \
    Query->ExecSQL();                                                   \
  }                                                                     \
  Table->Close();

void THlavniForm::NactiNastaveni() {
  // Nactu z NastaveniTable do lok. promennych
  int Port;

  // Zkontroluju, zda db nastaveni neni nejaka stara a obsahuje vsechna potrebna pole.
  TTable *Table = new TTable(NULL);
  Table->DatabaseName = Data->Database->DatabaseName;
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;

  // 5.12.2005: Ve verzi 1.08 jsem pridal pole MODEM_NAME (A100)
  try {
    Table->TableName = Data->NastaveniTable->TableName;
    Table->Open();
    if (Table->FieldDefs->IndexOf("MODEM_NAME") < 0) {
      // Jde o starsi soubor, pole zde neexistuje, pridam ho
      Table->Close();
      Query->SQL->Clear();
      Query->SQL->Add("ALTER TABLE \"" + Table->TableName + "\"");
      Query->SQL->Add("ADD MODEM_NAME CHAR(100)");
      Query->ExecSQL();
    }//if

    // 8.10.2007: Ve verzi 1.10.2 jsem pridal kompletni nastaveni pro Modbus
    CheckField("MODBUS_SPEED",           "INTEGER");
    CheckField("MODBUS_PARITY",          "SMALLINT");
    CheckField("MODBUS_REPLAY_DELAY",    "SMALLINT");
    CheckField("MODBUS_SILENT_INTERVAL", "SMALLINT");
    CheckField("MODBUS_PROTOCOL",        "SMALLINT");
    CheckField("MODBUS_REPETITIONS",     "SMALLINT");

  } catch(...) {}
  Table->Close();
  delete Query;
  delete Table;

  // Nactu nastaveni
  try {
    Data->NastaveniTable->Open();
    // Osetrim, aby mela NastaveniTable vzdy jen 1 zaznam
    while (Data->NastaveniTable->RecordCount>1) {
      Data->NastaveniTable->Last();
      Data->NastaveniTable->Delete();
    }//while
    Data->Nastaveni.ModemName             = Data->NastaveniTable->FieldByName("MODEM_NAME")->AsString;
    Data->Nastaveni.Modbus.Speed          = Data->NastaveniTable->FieldByName("MODBUS_SPEED")->AsInteger;
    Data->Nastaveni.Modbus.Parity         = (TParity)Data->NastaveniTable->FieldByName("MODBUS_PARITY")->AsInteger;
    Data->Nastaveni.Modbus.ReplyDelay     = Data->NastaveniTable->FieldByName("MODBUS_REPLAY_DELAY")->AsInteger;
    Data->Nastaveni.Modbus.SilentInterval = Data->NastaveniTable->FieldByName("MODBUS_SILENT_INTERVAL")->AsInteger;
    Data->Nastaveni.Modbus.Protocol       = (TProtocol)Data->NastaveniTable->FieldByName("MODBUS_PROTOCOL")->AsInteger;
    Data->Nastaveni.Modbus.Repetitions    = Data->NastaveniTable->FieldByName("MODBUS_REPETITIONS")->AsInteger;
  } catch(...) {}

  Data->NastaveniTable->Close();

  // Kontrola rozsahu
  switch (Data->Nastaveni.Modbus.Speed) {
    case 1200:
    case 2400:
    case 4800:
    case 9600:
    case 19200:
    case 38400:
    case 57600:
    case 115200:
      break;            // V poradku
    default:
      Data->Nastaveni.Modbus.Speed = DEFAULT_RS485_SPEED;
  }
  if (Data->Nastaveni.Modbus.Parity >= _RS485_PARITY_COUNT) {
    Data->Nastaveni.Modbus.Parity = DEFAULT_RS485_PARITY;
  }  
  // Hodnoty cekani se lisi od vahy
  if (Data->Nastaveni.Modbus.ReplyDelay < MIN_MODBUS_REPLY_DELAY || Data->Nastaveni.Modbus.ReplyDelay > MAX_MODBUS_REPLY_DELAY) {
    Data->Nastaveni.Modbus.ReplyDelay = DEFAULT_MODBUS_REPLY_DELAY;
  }
  if (Data->Nastaveni.Modbus.SilentInterval < MIN_MODBUS_SILENT_INTERVAL || Data->Nastaveni.Modbus.SilentInterval > MAX_MODBUS_SILENT_INTERVAL) {
    Data->Nastaveni.Modbus.SilentInterval = DEFAULT_MODBUS_SILENT_INTERVAL;
  }
  if (Data->Nastaveni.Modbus.Protocol >= _RS485_PROTOCOL_COUNT) {
    Data->Nastaveni.Modbus.Protocol = DEFAULT_RS485_PROTOCOL;
  }
  if (Data->Nastaveni.Modbus.Repetitions < MIN_MODBUS_REPETITIONS || Data->Nastaveni.Modbus.Repetitions > MAX_MODBUS_REPETITIONS) {
    Data->Nastaveni.Modbus.Repetitions = DEFAULT_MODBUS_REPETITIONS;
  }
}

// --------------------------------------------------------------------------
// Nacteni rustovych krivek
// --------------------------------------------------------------------------

void THlavniForm::LoadCurves() {
  // Nacte rustove krivky do vsech comboboxu
  Data->LoadCurvesToStrings(KrivkaPorovnatComboBox->Items);     // Seznam v zalozce Growth curve
  Data->LoadCurvesToStrings(ReportPorovnatComboBox->Items);     // Seznam v zalozce Report
} // LoadCurves

// --------------------------------------------------------------------------
// Zruseni zobrazeni porovnani
// --------------------------------------------------------------------------

void THlavniForm::HideCompareCurve() {
  KrivkaPorovnatComboBox->ItemIndex = 0;                        // Default vyberu zadne porovnani
  ReportPorovnatComboBox->ItemIndex = 0;                        // To same v zalozce report
  KrivkaPorovnatComboBoxChange(KrivkaPorovnatComboBox);         // Prekresli rustovou krivku
}

// --------------------------------------------------------------------------
// Smazani dne
// --------------------------------------------------------------------------

void THlavniForm::DeleteDayFromTable(TTable *Table, int Day) {
  // Smaze z tabulky <Table> den <Day>
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;

  Query->SQL->Add("DELETE FROM " + Table->TableName);
  Query->SQL->Add("WHERE DAY_NUMBER = " + String(Day));
  try {
    Query->ExecSQL();
  } catch(...) {}
  delete Query;
}

void THlavniForm::DeleteDay(int Day) {
  // Smaze den <Day>
  DeleteDayFromTable(Data->StatistikaTable, Day);       // Statistiky
  DeleteDayFromTable(Data->ZaznamyTable,    Day);       // Jednotlive vzroky
}

//---------------------------------------------------------------------------
__fastcall THlavniForm::THlavniForm(TComponent* Owner)
        : TForm(Owner)
{
  int Jazyk;

  // Nastavim aktualni DPI (nactu z hlavniho okna)
  DpiSet(PixelsPerInch);

  // Nastavim vysku radku vsech StringGridu podle aktualniho DPI (nevim proc, ale u hlavniho okna to nefunguje)
  DpiSetStringGrid(KrivkaSamiceStringGrid);
  DpiSetStringGrid(KrivkaSamciStringGrid);

  FileSetSaveDialog(SaveDialog);
  FileNewFile();

  // Seriova ctecka
//  Data->Adapter  = new TMReader;
  Data->Module   = new TMFlash;
//  Data->Module->Adapter = Data->Adapter;
  // USB ctecka
  Data->UsbFlash = new TAtFlash;
  Data->Module->UsbFlash = Data->UsbFlash;

  NactiNastaveni();                     // Nacteni nastaveni, pripadne upgrade tabulky na novou verzi
  Data->SetModbusPortParameters();      // Predam parametry MODBUSu
//  LoadCurves();                         // Nactu teoreticke krivky

  // Nactu jazyk z nastaveni
  // Musi byt uz tady, jeste pred nacitanim stringu z HLAVNI_POPIS_
  Data->NastaveniTable->Open();
  // Pokud je pole JAZYK prazdne, jde o prvni spusteni programu, necham ho vybrat jazyk
  if (Data->NastaveniTable->FieldByName("LANGUAGE")->IsNull) {
    // Jde o prvni spusteni
    JazykForm = new TJazykForm(NULL);
    try {
      JazykForm->ShowModal();
      if (JazykForm->FrancouzskyRadioButton->Checked) {
        Jazyk = LANG_FRENCH;
      } else if (JazykForm->FinskyRadioButton->Checked) {
        Jazyk = LANG_FINNISH;
      } else if (JazykForm->TureckyRadioButton->Checked) {
        Jazyk = LANG_TURKISH;
      } else if (JazykForm->DanskyRadioButton->Checked) {
        Jazyk = LANG_DANISH;
      } else if (JazykForm->SpanelskyRadioButton->Checked) {
        Jazyk = LANG_SPANISH;
      } else if (JazykForm->NemeckyRadioButton->Checked) {
        Jazyk = LANG_GERMAN;
      } else if (JazykForm->RuskyRadioButton->Checked) {
        Jazyk = LANG_RUSSIAN;
      } else {
        Jazyk = LANG_ENGLISH;
      }//else
    } catch(...) {}
    delete JazykForm;
    // Ulozim jazyk do nastaveni
    Data->NastaveniTable->Edit();
    Data->NastaveniTable->FieldByName("LANGUAGE")->AsInteger = Jazyk;
    Data->NastaveniTable->Post();
  } else {
    // Uz jazyk jednou vybral, jen ho nactu
    Jazyk = Data->NastaveniTable->FieldByName("LANGUAGE")->AsInteger;
  }//else
  Data->NastaveniTable->Close();
  // Zaskrtnu jazyk v menu
  CheckLanguage(Jazyk);
  // A nastavim jazyk
  if (LoadNewResourceModule(Jazyk) != 0) {
    ReinitializeForms();
  }

  Data->Soubor.Jmeno = "";      // Default neni otevreny zadny soubor
  NastavNazevOkna(Data->Soubor.Jmeno);

  // Nastavim ruzne nazvy podle verze odberatele
  SetVersionStrings();

  // Schovam porovnani
  SchovejPrvkyPorovnani();
  ZobrazitPorovnani = false;

  // Schovam porovnani s teoretickou rustovou krivkou
  HideCompareCurveItems();

  // Nastavim hinty ikon
  OpenSpeedButton->Hint     = Otevt1->Caption;
  SaveSpeedButton->Hint     = Uloit1->Caption;
  FindSpeedButton->Hint     = Findfile1->Caption;
  ReadSpeedButton->Hint     = Nastzznamy1->Caption;
  SetupSpeedButton->Hint    = Config1->Caption;
  Rs485SpeedButton->Hint    = RS4851->Caption;
  DatabaseSpeedButton->Hint = Weighingdatabase1->Caption;
  AddSpeedButton->Hint      = Addtoweighingdatabase1->Caption;
}
//---------------------------------------------------------------------------


void __fastcall THlavniForm::Oprogramu1Click(TObject *Sender)
{
  OProgramuForm->NameLabel->Caption  = Data->Version.ScaleTitle;
  OProgramuForm->VerzeLabel->Caption = String(VERSION_SW) + "." + String(VERSION_BUILD);
  OProgramuForm->CompanyMemo->Lines->Clear();
  OProgramuForm->CompanyMemo->Lines->AddStrings(Data->Version.Contacts);
  OProgramuForm->LogoImage->Picture->Assign(Data->Version.Logo);

  OProgramuForm->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Konecprogramu1Click(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::FormClose(TObject *Sender,
      TCloseAction &Action)
{
//  delete Data->Adapter;
  delete Data->Module;
  delete Data->UsbFlash;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Readdatafromfile1Click(TObject *Sender)
{
  if (!OpenDialogHardCopy->Execute()) return;
  if (OpenDialogHardCopy->FileName.IsEmpty()) return;   // Nevybral nic

  try {
    Screen->Cursor = crHourGlass;
    Data->Module->Load(OpenDialogHardCopy->FileName);
    // Zobrazim konfiguraci
    if (!ZobrazKonfiguraci()) {
      throw 1;
    }
    KonvertujData();
    FileCreateHeader();
    if (!FileSaveDataToNewFile()) {
      throw 1;
    }
    OtevriTabulkyJakoSoubor(SaveDialog->FileName);
  } catch(...) {}
  Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Readdatatofile1Click(TObject *Sender)
{
  String Jmeno;

  // Nactu veskera data z modulu
  if (!NactiKonfiguraci()) {
    return;
  }

  if (!SaveDialogHardCopy->Execute()) return;
  if (SaveDialogHardCopy->FileName.IsEmpty()) return;   // Nevybral nic
  Jmeno = ChangeFileExt(SaveDialogHardCopy->FileName, "." + String(PRIPONA_HARDCOPY));

  // Nactu cely modul - predame rizeni do dialogu
  if (!ReadProgressForm->Execute(this)) {
    return;
  }

  Data->Module->Save(Jmeno);
}
//---------------------------------------------------------------------------


void __fastcall THlavniForm::DenComboBoxChange(TObject *Sender)
{
  // Nastavim den na vybrany den
  NastavDen(DenComboBox->Items->Strings[DenComboBox->ItemIndex].ToInt());
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::PohlaviSamiceRadioButtonClick(TObject *Sender)
{
  // Zmena pohlavi na samice
  Data->Soubor.Pohlavi = GENDER_FEMALE;
  ZobrazDen(Data->Soubor.Den);
  ZobrazRustovouKrivku();
  ZobrazAktivituCelkem();
  ZobrazReport();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::PohlaviSamciRadioButtonClick(TObject *Sender)
{
  // Zmena pohlavi na samce
  Data->Soubor.Pohlavi = GENDER_MALE;
  ZobrazDen(Data->Soubor.Den);
  ZobrazRustovouKrivku();
  ZobrazAktivituCelkem();
  ZobrazReport();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::PrvniDenSpeedButtonClick(TObject *Sender)
{
  // Preskocim a prvni den
  NastavDen(Data->Soubor.MinDen);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::PredchoziDenSpeedButtonClick(TObject *Sender)
{
  // Preskocim a predchozi den
  int NovyDen;
  if (Data->Soubor.Den <= Data->Soubor.MinDen) {
    return;
  }
  NovyDen = Data->Soubor.Den - 1;
  while (DenComboBox->Items->IndexOf(String(NovyDen)) < 0 && NovyDen > Data->Soubor.MinDen) {
    NovyDen--;     // Dalsi den ve statistice chybi, preskocim ho
  }
  NastavDen(NovyDen);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::DalsiDenSpeedButtonClick(TObject *Sender)
{
  // Preskocim a dalsi den
  int NovyDen;
  if (Data->Soubor.Den >= Data->Soubor.MaxDen) {
    return;
  }
  NovyDen = Data->Soubor.Den + 1;
  while (DenComboBox->Items->IndexOf(String(NovyDen)) < 0 && NovyDen < Data->Soubor.MaxDen) {
    NovyDen++;     // Dalsi den ve statistice chybi, preskocim ho
  }
  NastavDen(NovyDen);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::PosledniDenSpeedButtonClick(TObject *Sender)
{
  // Preskocim a posledni den
  NastavDen(Data->Soubor.MaxDen);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Openforcomparsion1Click(TObject *Sender)
{
  String Str;

  if (!JeOtevrenySoubor()) {
    return;
  }
  // Zeptam se na soubor
  if (!OpenDialog->Execute()) {
    return;
  }
  if (OpenDialog->FileName.IsEmpty()) {
    return;   // Nevybral nic
  }
  ZavriTabulkyPorovnani();
  FileDecodeFileToTables(OpenDialog->FileName, true, false);
  OtevriTabulkyPorovnani();
  // Zobrazim prvky porovnani
  Data->Soubor.JmenoPorovnani = OpenDialog->FileName;
  ZobrazitPorovnani = true;
  ZobrazPrvkyPorovnani();
  // Jednotky
  if (Data->CKonfiguraceTable->FieldByName("UNITS")->AsInteger == UNITS_KG) {
    Str = LoadStr(KG);
  } else {
    Str = LoadStr(LB);
  }
  CilovaHmotnostJednotkyPorovnaniLabel->Caption = Str;
  PrumerJednotkyPorovnaniLabel->Caption         = Str;
  PrirustekJednotkyPorovnaniLabel->Caption      = Str;
  // Zobrazim
  ZobrazDen(Data->Soubor.Den);
  ZobrazRustovouKrivku();
  ZobrazAktivituCelkem();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::FormResize(TObject *Sender)
{
  if (ZobrazitPorovnani) {
    HistogramPorovnaniPanel->Height = HistogramTabSheet->Height / 2;
    AktivitaPorovnaniPanel->Height  = HistogramTabSheet->Height / 2;
  }
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::HistogramPorovnaniOsaCheckBoxClick(TObject *Sender)
{
  if (!ZobrazitPorovnani) {
    return;
  }
  if (HistogramPorovnaniOsaCheckBox->Checked) {
    // Chce osu nastavit stejne jako u histogramu oteverneho souboru
    HistogramPorovnaniChart->BottomAxis->SetMinMax(HistogramChart->BottomAxis->Maximum, HistogramChart->BottomAxis->Minimum);
  } else {
    // Chce osu nastavit automaticky
    HistogramPorovnaniChart->BottomAxis->Automatic = true;
  }//else
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Otevt1Click(TObject *Sender)
{
  // Zeptam se na soubor
  if (!OpenDialog->Execute()) {
    return;
  }
  if (OpenDialog->FileName.IsEmpty()) {
    return;   // Nevybral nic
  }
  FileCloseTables();
  FileDecodeFileToTables(OpenDialog->FileName, false, false);
  OtevriTabulkyJakoSoubor(OpenDialog->FileName);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Uloit1Click(TObject *Sender)
{
  if (!JeOtevrenySoubor()) {
    return;
  }
  if (Data->Soubor.Jmeno.IsEmpty()) {
    // Jde o novy soubor => musim ted zvolit jmeno souboru
    // Toto nemuze nastat, nemuze zalozit novy soubor
    return;
  }//if
  FileSaveTablesToFile();
  NastavNazevOkna(Data->Soubor.Jmeno);
  // Zavre to Query => znovu otevru
  NastavDen(DenComboBox->Items->Strings[DenComboBox->ItemIndex].ToInt());
  ZobrazReport();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Nastzznamy1Click(TObject *Sender)
{
  if (!NactiDataZModulu()) {
    return;
  }

  KonvertujData();
  FileCreateHeader();
  if (!FileSaveDataToNewFile()) {
    return;
  }
  OtevriTabulkyJakoSoubor(SaveDialog->FileName);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::KrivkaCRealCheckBoxClick(TObject *Sender)
{
  KrivkaChart->Series[KRIVKA_COMP_REAL]->Active = KrivkaCRealCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::KrivkaCTargetCheckBoxClick(TObject *Sender)
{
  KrivkaChart->Series[KRIVKA_COMP_TARGET]->Active = KrivkaCTargetCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::KrivkaRealCheckBoxClick(TObject *Sender)
{
  KrivkaChart->Series[KRIVKA_REAL]->Active = KrivkaRealCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::KrivkaTargetCheckBoxClick(TObject *Sender)
{
  KrivkaChart->Series[KRIVKA_TARGET]->Active = KrivkaTargetCheckBox->Checked;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Config1Click(TObject *Sender)
{
  NastaveniVahForm->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::SeznamHejnListBoxClick(TObject *Sender)
{
  try {
    ZobrazHejno(SeznamHejnListBox->Items->Strings[SeznamHejnListBox->ItemIndex].ToInt());
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuExportReportClick(TObject *Sender)
{
  // Export reportu do Excelu
  int MinDen, MaxDen;   // Zvoleny rozsah dnu pro export

  if (!JeOtevrenySoubor()) {
    return;
  }
  if (!ZvolRozsahDnu(&MinDen, &MaxDen)) {
    return;
  }
  // Vyberu zaznamy
  try {
    Data->StatistikaVyberQuery->Close();
    Data->StatistikaVyberQuery->SQL->Clear();
    Data->StatistikaVyberQuery->SQL->Add(CreateReportSQL());
    Data->StatistikaVyberQuery->SQL->Add("WHERE DAY_NUMBER>=" + String(MinDen));
    Data->StatistikaVyberQuery->SQL->Add("AND DAY_NUMBER<=" + String(MaxDen));
    Data->StatistikaVyberQuery->Open();
  } catch(...) {
    return;
  }
  // Export
  try {
    Screen->Cursor = crHourGlass;
    ExportReport(Data->StatistikaVyberQuery, PouzivaObePohlavi());
  } catch(...) {}
  Screen->Cursor = crDefault;
  // Zavru Query
  try {
    Data->StatistikaVyberQuery->Close();
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuExportSamplesClick(TObject *Sender)
{
  // Export vzorku do Excelu
  int MinDen, MaxDen;   // Zvoleny rozsah dnu pro export
  TQuery *Query;

  if (!JeOtevrenySoubor()) {
    return;
  }
  if (!ZvolRozsahDnu(&MinDen, &MaxDen)) {
    return;
  }
  // Vyberu zaznamy
  Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    Query->SQL->Clear();
    Query->SQL->Add("SELECT * FROM \"" + Data->ZaznamyTable->TableName + "\"");
    Query->SQL->Add("WHERE DAY_NUMBER>=" + String(MinDen));
    Query->SQL->Add("AND DAY_NUMBER<=" + String(MaxDen));
    Query->Open();
  } catch(...) {
    delete Query;
    return;
  }

  try {
    Screen->Cursor = crHourGlass;
    ExportSamples(Query);
  } catch(...) {}
  Screen->Cursor = crDefault;

  delete Query;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuExportHistogramClick(TObject *Sender)
{
  // Export histogramu do Excelu
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    Screen->Cursor = crHourGlass;
    ExportHistogram(HistogramChart, PouzivaObePohlavi());
  } catch(...) {}
  Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuExportDayActivityClick(TObject *Sender)
{
  // Export aktivity dne do Excelu
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    Screen->Cursor = crHourGlass;
    ExportDayActivity(AktivitaDenChart, PouzivaObePohlavi());
  } catch(...) {}
  Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuExportGrowthCurveClick(TObject *Sender)
{
  // Export rustove krivky do Excelu
  TCurveList *CurveList = NULL;
  TGrowthCurve *Curve   = NULL;

  if (!JeOtevrenySoubor()) {
    return;
  }

  try {
    Screen->Cursor = crHourGlass;

    // Pokud porovnava rustovou krivku, pripravim si ji
    if (KrivkaPorovnatComboBox->ItemIndex > 0) {
      // Nahraju vybranou krivku
      if ((CurveList = new TCurveList()) == NULL) {
        throw 1;
      }
      Data->LoadCurvesFromDatabase(CurveList);
      if ((Curve = CurveList->GetCurve(KrivkaPorovnatComboBox->ItemIndex - 1)) == NULL) {
        throw 1;
      }
    }

    // Exportuju
    ExportGrowthCurve(KrivkaChart, PouzivaObePohlavi(), Curve);
  } catch(...) {}
  if (CurveList) {
    delete CurveList;
  }
  Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuPrintReportClick(TObject *Sender)
{
  // Tisk reportu
  int MinDen, MaxDen;   // Zvoleny rozsah dnu pro export

  if (!JeOtevrenySoubor()) {
    return;
  }
  if (!ZvolRozsahDnu(&MinDen, &MaxDen)) {
    return;
  }
  // Vyberu zaznamy
  try {
    Data->StatistikaVyberQuery->Close();
    Data->StatistikaVyberQuery->SQL->Clear();
    Data->StatistikaVyberQuery->SQL->Add(CreateReportSQL());
    Data->StatistikaVyberQuery->SQL->Add("WHERE DAY_NUMBER>=" + String(MinDen));
    Data->StatistikaVyberQuery->SQL->Add("AND DAY_NUMBER<=" + String(MaxDen));
    Data->StatistikaVyberQuery->Open();
  } catch(...) {
    return;
  }
  // Tisk
  try {
    PrintReport(PouzivaObePohlavi());
  } catch(...) {}
  // Zavru Query
  try {
    Data->StatistikaVyberQuery->Close();
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuPrintSamplesClick(TObject *Sender)
{
  // Tisk vzorku
  int MinDen, MaxDen;   // Zvoleny rozsah dnu pro export

  if (!JeOtevrenySoubor()) {
    return;
  }
  if (!ZvolRozsahDnu(&MinDen, &MaxDen)) {
    return;
  }
  // Vyberu zaznamy
  try {
    Data->TiskQuery->Close();
    Data->TiskQuery->SQL->Clear();
    Data->TiskQuery->SQL->Add("SELECT * FROM \"" + Data->ZaznamyTable->TableName + "\"");
    Data->TiskQuery->SQL->Add("WHERE DAY_NUMBER>=" + String(MinDen));
    Data->TiskQuery->SQL->Add("AND DAY_NUMBER<=" + String(MaxDen));
    Data->TiskQuery->Open();
  } catch(...) {
    return;
  }
  // Tisk
  try {
    PrintSamples();
  } catch(...) {}
  // Zavru Query
  try {
    Data->TiskQuery->Close();
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuExportTotalActivityClick(TObject *Sender)
{
  // Export celkove aktivity do Excelu
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    Screen->Cursor = crHourGlass;
    ExportTotalActivity(AktivitaCelkemChart, PouzivaObePohlavi());
  } catch(...) {}
  Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuPrintHistogramClick(TObject *Sender)
{
  // Tisk histogramu
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    PrintHistogram(HistogramChart, PouzivaObePohlavi());
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuPrintDayActivityClick(TObject *Sender)
{
  // Tisk denni aktivity
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    PrintDayActivity(AktivitaDenChart, PouzivaObePohlavi());
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuPrintTotalActivityClick(TObject *Sender)
{
  // Tisk celkove aktivity
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    PrintTotalActivity(AktivitaCelkemChart, PouzivaObePohlavi());
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuPrintGrowthCurveClick(TObject *Sender)
{
  // Tisk rustove krivky
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    PrintGrowthCurve(KrivkaChart, PouzivaObePohlavi());
  } catch(...) {}
}
//---------------------------------------------------------------------------


void __fastcall THlavniForm::Anglicky1Click(TObject *Sender)
{
  SetLanguage(LANG_ENGLISH);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Francouzsky1Click(TObject *Sender)
{
  SetLanguage(LANG_FRENCH);
}
//---------------------------------------------------------------------------




void __fastcall THlavniForm::MenuPrintOnlineClick(TObject *Sender)
{
  // Tisk online
  if (!JeOtevrenySoubor()) {
    return;
  }
  try {
    PageControl->ActivePage = OnlineGrafTabSheet;  // Musi to zde byt, maximum a minimum osy se nastavi az po vykresleni, nefungovalo to hned po otevreni souboru
    PrintOnline(OnlineChart);
  } catch(...) {}
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuExportOnlineClick(TObject *Sender)
{
  // Export online zaznamu do Excelu
  int Min, Max;         // Zvoleny rozsah dnu pro export
  int Prumerovani;      // Zvolene prumerovani zaznamu

  if (!JeOtevrenySoubor()) {
    return;
  }
  if (!ZvolRozsahOnline(&Min, &Max, &Prumerovani)) {
    return;
  }

  try {
    Screen->Cursor = crHourGlass;
    Data->OnlineQuery->DisableControls();
    ExportOnline(Data->OnlineQuery, Min, Max, Prumerovani);
  } catch(...) {}
  Screen->Cursor = crDefault;
  Data->OnlineQuery->EnableControls();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Finsky1Click(TObject *Sender)
{
  SetLanguage(LANG_FINNISH);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::OnlinePrumerovaniComboBoxChange(
      TObject *Sender)
{
  double MinX, MaxX;  // Minimalni a maximalni hodnota osy X

  Screen->Cursor = crHourGlass;
  try {
    MinX = OnlineChart->BottomAxis->Minimum * (double)PrumerovaniOnline;
    MaxX = OnlineChart->BottomAxis->Maximum * (double)PrumerovaniOnline;
    ZobrazOnlineGraf();
    // Zapamatuju si nove prumerovani
    PrumerovaniOnline = OnlinePrumerovaniComboBox->ItemIndex + 1;
    // Nastavim osu X tak, aby mne zustalo zachovane zobrazeni
    OnlineChart->BottomAxis->SetMinMax(MaxX / (double)PrumerovaniOnline, MinX / (double)PrumerovaniOnline);
  } catch(...) {}
  Screen->Cursor = crDefault;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::MenuSimulationClick(TObject *Sender)
{
  int MinX, MaxX;  // Minimalni a maximalni hodnota osy X

  if (!JeOtevrenySoubor()) {
    return;
  }
  do {
    if (SimulaceStartForm->ShowModal() != mrOk) {
      return;
    }
    Refresh();
    Data->OnlineTable->DisableControls();
    Data->OnlineQuery->DisableControls();
    Screen->Cursor = crHourGlass;
    try {
      // Vyplnim data pro novou detekci (jeste pred volanim SimulateInit)
      Simulate.UsePrefilter = true;
      Data->KalibraceTable->Open();
      try {
        if (Data->KalibraceTable->FieldDefs->IndexOf("CAL_ZERO") >= 0) {
          Simulate.Calibration.ZeroCalibration  = Data->KalibraceTable->FieldByName("CAL_ZERO")->AsInteger;
          Simulate.Calibration.RangeCalibration = Data->KalibraceTable->FieldByName("CAL_RANGE")->AsInteger;
          Simulate.Calibration.Range            = Data->KalibraceTable->FieldByName("RANGE")->AsInteger;
        } else {
          Simulate.Calibration.ZeroCalibration  = Data->KalibraceTable->FieldByName("CAL_ZERO00")->AsInteger;
          Simulate.Calibration.RangeCalibration = Data->KalibraceTable->FieldByName("CAL_RANGE00")->AsInteger;
          Simulate.Calibration.Range            = Data->KalibraceTable->FieldByName("RANGE00")->AsInteger;
        }
      } catch(...) {}  
      Data->KalibraceTable->Close();

      SimulateInit();

      Data->OnlineQuery->First();
      if (SimulaceStartForm->GrafRadioButton->Checked) {
        // Zaznamy zobrazene v grafu
        PageControl->ActivePage = OnlineGrafTabSheet;  // Musi to zde byt, maximum a minimum osy se nastavi az po vykresleni, nefungovalo to hned po otevreni souboru
        Data->OnlineQuery->MoveBy(OnlineChart->BottomAxis->Minimum * PrumerovaniOnline);
        for (int i = 0; i < (OnlineChart->BottomAxis->Maximum - OnlineChart->BottomAxis->Minimum) * PrumerovaniOnline; i++) {
          SimulateStep(Data->OnlineQuery->FieldByName("WEIGHT")->AsFloat);
          Data->OnlineQuery->Next();
          if (Data->OnlineQuery->Eof) {
            break;
          }
        }//for
      } else if (SimulaceStartForm->DnesRadioButton->Checked) {
        // Zaznamy za aktualni den
        while (!Data->OnlineQuery->Eof) {
          SimulateStep(Data->OnlineQuery->FieldByName("WEIGHT")->AsFloat);
          Data->OnlineQuery->Next();
        }//while
      } else if (SimulaceStartForm->VseRadioButton->Checked) {
        // Zaznamy za cele mereni
        Data->OnlineTable->First();
        while (!Data->OnlineTable->Eof) {
          SimulateStep(Data->OnlineTable->FieldByName("WEIGHT")->AsFloat);
          Data->OnlineTable->Next();
        }//while
      } else {
        // Vybrany rozsah zaznamu za aktualni den
        Data->OnlineQuery->MoveBy(SimulaceStartForm->OdEdit->Text.ToInt());
        for (int i = 0; i < SimulaceStartForm->DoEdit->Text.ToInt() - SimulaceStartForm->OdEdit->Text.ToInt(); i++) {
          SimulateStep(Data->OnlineQuery->FieldByName("WEIGHT")->AsFloat);
          Data->OnlineQuery->Next();
          if (Data->OnlineQuery->Eof) {
            break;
          }
        }//for
      }//else
    } catch(...) {}
    Data->OnlineTable->EnableControls();
    Data->OnlineQuery->EnableControls();
    Screen->Cursor = crDefault;
  } while (SimulaceVysledkyForm->ShowModal() == mrOk);
}
//---------------------------------------------------------------------------


void __fastcall THlavniForm::OnlineChartMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
  double x, y;
  OnlineChart->Series[0]->GetCursorValues(x, y);
  // Zobrazim aktualni hmotnost na pozici kurzoru
  OnlineHmotnostLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, y);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::OnlineZoomInSpeedButtonClick(TObject *Sender)
{
  OnlineChart->ZoomPercent(115);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::OnlineZoomOutSpeedButtonClick(TObject *Sender)
{
  OnlineChart->ZoomPercent(80);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Turkce1Click(TObject *Sender)
{
  SetLanguage(LANG_TURKISH);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Weighingdatabase1Click(TObject *Sender)
{
  WeighingForm->ShowModal();
  if (WeighingForm->OpenFoundFile) {
    // Chce otevrit soubor, ktery nasel v databazi
    OpenFoundFile();
  }
  // V WeighingForm mohl editovat krivky pro porovnani => znovu nahraju krivky a zrusim zobrazeni porovnanvaci krivky
  LoadCurves();                 // Nactu teoreticke krivky
  HideCompareCurve();           // Default vyberu zadne porovnani
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Addtoweighingdatabase1Click(TObject *Sender)
{
  // Prida vybrane dny do databaze
  int MinDen, MaxDen;   // Zvoleny rozsah dnu pro export
  TStatistic Stat;
  float Average, Sigma;

  if (!JeOtevrenySoubor()) {
    return;
  }
  if (!ZvolRozsahDnu(&MinDen, &MaxDen)) {
    return;
  }
  // Vyberu zaznamy
  TQuery *Query = new TQuery(NULL);
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    Query->Close();
    Query->SQL->Clear();
    Query->SQL->Add("SELECT * FROM " + Data->StatistikaTable->TableName);
    Query->SQL->Add("WHERE DAY_NUMBER>=" + String(MinDen));
    Query->SQL->Add("AND DAY_NUMBER<=" + String(MaxDen));
    Query->Open();
  } catch(...) {
    delete Query;
    return;
  }
  // Kopirovani
  try {
    Query->First();
    if (Query->Eof) {
      throw 1;  // Neobsahuje zadny zaznam (musim to osetrit kvuli vytvareni filtru dole)
    }
    Screen->Cursor = crHourGlass;
    while (!Query->Eof) {
      // Podivam se, zda uz zaznam v databazi neexistuje
      if (WeighingForm->RecordExists(Data->KonfiguraceTable->FieldByName("ID")->AsInteger,
                                     Query->FieldByName("DAY_NUMBER")->AsInteger,
                                     Query->FieldByName("DATE_TIME")->AsDateTime)) {
        // Zaznam jiz existuje, zeptam se, zda ho chce prepsat. Stary zaznam je jiz vyplnen z fce RecordExists()
        // Vyplnim novy zaznam
        // Samice
        Stat.XSuma  = Query->FieldByName("FEMALE_STAT_XSUM")->AsFloat;
        Stat.X2Suma = Query->FieldByName("FEMALE_STAT_X2SUM")->AsFloat;
        Stat.Count  = Query->FieldByName("FEMALE_STAT_COUNT")->AsInteger;
        Average = StatAverage(&Stat);
        Sigma = StatSigma(&Stat);
        WeighingOverwriteForm->NewFemaleCountLabel->Caption   = String(Stat.Count);
        WeighingOverwriteForm->NewFemaleAverageLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average));  // V Keilu to delam pres int
        if (Query->FieldByName("DAY_NUMBER")->AsInteger == Data->Soubor.MinDen) {
          // Prvni den je prirustek 0
          WeighingOverwriteForm->NewFemaleGainLabel->Caption  = FormatFloat(FORMAT_HMOTNOSTI, 0);
        } else {
          // Dalsi dny prirustek normalne spocitam
          WeighingOverwriteForm->NewFemaleGainLabel->Caption  = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average) - Query->FieldByName("FEMALE_YESTERDAY_AVERAGE")->AsFloat);
        }
        WeighingOverwriteForm->NewFemaleSigmaLabel->Caption   = FormatFloat("0.000", (double)Sigma / 1000.0);
        WeighingOverwriteForm->NewFemaleCvLabel->Caption      = FormatFloat("0", StatVariation(Average, Sigma));  // Zatim na cela procenta
        if (Query->FieldByName("USE_REAL_UNI")->AsBoolean) {
          WeighingOverwriteForm->NewFemaleUniLabel->Caption = Query->FieldByName("FEMALE_REAL_UNI")->AsString;
        } else {
          WeighingOverwriteForm->NewFemaleUniLabel->Caption = FormatFloat("0", StatUniformity(Average, Sigma, Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger));
        }
        // Samci
        if (PouzivaObePohlavi()) {      // Jsou obe pohlavi
          // Samci jsou zadani
          Stat.XSuma  = Query->FieldByName("MALE_STAT_XSUM")->AsFloat;
          Stat.X2Suma = Query->FieldByName("MALE_STAT_X2SUM")->AsFloat;
          Stat.Count  = Query->FieldByName("MALE_STAT_COUNT")->AsInteger;
          Average = StatAverage(&Stat);
          Sigma = StatSigma(&Stat);
          WeighingOverwriteForm->NewMaleCountLabel->Caption   = String(Stat.Count);
          WeighingOverwriteForm->NewMaleAverageLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average));  // V Keilu to delam pres int
          if (Query->FieldByName("DAY_NUMBER")->AsInteger == Data->Soubor.MinDen) {
            // Prvni den je prirustek 0
            WeighingOverwriteForm->NewMaleGainLabel->Caption  = FormatFloat(FORMAT_HMOTNOSTI, 0);
          } else {
            // Dalsi dny prirustek normalne spocitam
            WeighingOverwriteForm->NewMaleGainLabel->Caption  = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average) - Query->FieldByName("MALE_YESTERDAY_AVERAGE")->AsFloat);
          }
          WeighingOverwriteForm->NewMaleSigmaLabel->Caption   = FormatFloat("0.000", (double)Sigma / 1000.0);
          WeighingOverwriteForm->NewMaleCvLabel->Caption      = FormatFloat("0", StatVariation(Average, Sigma));  // Zatim na cela procenta
          if (Query->FieldByName("USE_REAL_UNI")->AsBoolean) {
            WeighingOverwriteForm->NewMaleUniLabel->Caption = Query->FieldByName("MALE_REAL_UNI")->AsString;
          } else {
            WeighingOverwriteForm->NewMaleUniLabel->Caption = FormatFloat("0", StatUniformity(Average, Sigma, Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger));
          }
        } else {
          // Samci se nepouzivaji
          WeighingOverwriteForm->NewMaleCountLabel->Caption   = "";
          WeighingOverwriteForm->NewMaleAverageLabel->Caption = "";
          WeighingOverwriteForm->NewMaleGainLabel->Caption    = "";
          WeighingOverwriteForm->NewMaleSigmaLabel->Caption   = "";
          WeighingOverwriteForm->NewMaleCvLabel->Caption      = "";
          WeighingOverwriteForm->NewMaleUniLabel->Caption     = "";
        }
        Screen->Cursor = crDefault;
        if (WeighingOverwriteForm->ShowModal() != mrOk) {
          // Nechce prepsat
          Screen->Cursor = crHourGlass;
          Query->Next();
          continue;
        }
        Screen->Cursor = crHourGlass;
        // Smazu stary zaznam
        WeighingForm->DeleteRecord(Data->KonfiguraceTable->FieldByName("ID")->AsInteger,
                                   Query->FieldByName("DAY_NUMBER")->AsInteger,
                                   Query->FieldByName("DATE_TIME")->AsDateTime);
      }//if
      // Vlozim zaznam do databaze
      try {
        Data->SmsTable->Open();
        Data->SmsTable->Append();
        Data->SmsTable->Edit();
        Data->SmsTable->FieldByName("INSERT_DATE_TIME")->AsDateTime = Now();
        Data->SmsTable->FieldByName("ID")->AsInteger                = Data->KonfiguraceTable->FieldByName("ID")->AsInteger;
        Data->SmsTable->FieldByName("DAY_NUMBER")->AsInteger        = Query->FieldByName("DAY_NUMBER")->AsInteger;
        Data->SmsTable->FieldByName("DAY_DATE")->AsDateTime         = Query->FieldByName("DATE_TIME")->AsDateTime;
        CopyToDatabase(Query, GENDER_FEMALE);  // Samice
        if (PouzivaObePohlavi()) {      // Jsou obe pohlavi
          CopyToDatabase(Query, GENDER_MALE); // Samci
        }
        Data->SmsTable->Post();
      } catch(...) {}
      Data->SmsTable->Close();
      Query->Next();
    }//while

    Screen->Cursor = crDefault;
    // Zeptam se, zda chce vytvorit novy filtr odpovidajici prave zkopirovanym zaznamum
    if (MessageBox(NULL, LoadStr(SMS_FILTER_CREATE_AFTER_COPY).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
      throw 1;  // Nechce vytvorit filtr
    }

    // Nabidnu mu parametry noveho filtru
    Data->FiltersTable->Open();
    // Zkopiruju udaje do okna
    WeighingFilterForm->NameEdit->Text = "";  // Necham ho zadat
    WeighingFilterForm->NameEdit->Enabled = true;
    WeighingFilterForm->ScalesEdit->Text = Data->KonfiguraceTable->FieldByName("ID")->AsString;       // Tabulka je default otevrena
    Query->First();   // Prvni zaznam obsahuje nejmladsi den
    WeighingFilterForm->FromCheckBox->Checked = true;
    WeighingFilterForm->FromDateTimePicker->Date = Query->FieldByName("DATE_TIME")->AsDateTime;
    Query->Last();    // Posledni obsahuje nejstarsi den
    WeighingFilterForm->TillCheckBox->Checked = true;
    WeighingFilterForm->TillDateTimePicker->Date = Query->FieldByName("DATE_TIME")->AsDateTime;
    WeighingFilterForm->NoteEdit->Text = "";
    // Zobrazim okno
    if (WeighingFilterForm->ShowModal() != mrOk) {
      throw 1;
    }
    // Zkontroluju, zda uz filtr neexistuje
    if (!WeighingFiltersForm->CheckFilterInTable(WeighingFilterForm->NameEdit->Text)) {
      throw 1;  // Uz existuje a nechce smazat
    }
    // Ulozim novy zaznam
    Data->FiltersTable->Append();
    WeighingFiltersForm->SaveFilter();
  } catch(...) {}
  Screen->Cursor = crDefault;
  Data->FiltersTable->Close();
  delete Query;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Findfile1Click(TObject *Sender)
{
  // Vynuluju podminky
  FindFileForm->ScalesEdit->Text = "";
  FindFileForm->DayEdit->Text    = "";
  FindFileForm->FromDateTimePicker->Date = Date();
  FindFileForm->FromCheckBox->Checked    = false;
  FindFileForm->TillDateTimePicker->Date = Date();
  FindFileForm->TillCheckBox->Checked    = false;
  FindFileForm->FilesListBox->Clear();
  if (FindFileForm->ShowModal() != mrOk) {
    return;
  }
  // Otevru vybrany soubor
  OpenFoundFile();
}
//---------------------------------------------------------------------------


void __fastcall THlavniForm::Dansk1Click(TObject *Sender)
{
  SetLanguage(LANG_DANISH);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Espanol1Click(TObject *Sender)
{
  SetLanguage(LANG_SPANISH);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::OnlineChartUndoZoom(TObject *Sender)
{
  // Pokud mel zazoomovane, zmenil filtr a pak odzoomoval, pomrsila se osa X - pri odzoomovani ji resetnu
  OnlineChart->BottomAxis->Automatic = true;
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Growthcurves1Click(TObject *Sender)
{
  // Editace rustovych krivek
  CurvesForm->ShowModal();
  LoadCurves();                                                 // Nactu teoreticke krivky
  HideCompareCurve();                                           // Default vyberu zadne porovnani
}
//---------------------------------------------------------------------------


void __fastcall THlavniForm::KrivkaPorovnatComboBoxChange(TObject *Sender)
{
  // Nastavim porovnani s teoretickou rustovou krivkou
  TGrowthCurve *Curve;
  TCurveList *CurveList;

  // Nastavim stejnou krivku v obou comboboxech
  if ((TComboBox *)Sender == KrivkaPorovnatComboBox) {
    ReportPorovnatComboBox->ItemIndex = KrivkaPorovnatComboBox->ItemIndex;      // Zkopiruji i do druheho comboboxu
  } else {
    KrivkaPorovnatComboBox->ItemIndex = ReportPorovnatComboBox->ItemIndex;
  }

  if (KrivkaPorovnatComboBox->ItemIndex > 0) {
    // Vybral nekterou krivku
    // Nahraju vybranou krivku
    CurveList = new TCurveList();
    try {
      Data->LoadCurvesFromDatabase(CurveList);
      if ((Curve = CurveList->GetCurve(KrivkaPorovnatComboBox->ItemIndex - 1)) == NULL) {
        throw 1;
      }
      // Nastavim krivku
      Data->SetCompareCurve(COMPARE_CURVE_MAIN_FORM, Curve);
    } catch (...) {}
    delete CurveList;

  } else {
    // Nechce porovnavat
    Data->SetCompareCurve(COMPARE_CURVE_MAIN_FORM, NULL);
  }

  // Na zaver obnovim zobrazeni
  ZobrazRustovouKrivku();
  ZobrazReport();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Deutsch1Click(TObject *Sender)
{
  SetLanguage(LANG_GERMAN);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::RS4851Click(TObject *Sender)
{
  if (ModbusOnlineForm->ShowModal() != mrOk) {
    // Nechce otevrit soubor

    if (ModbusOnlineForm->FileSaved) {
      // Stahnul data z vahy, tj. modifikovaly se tabulky prave otevreneho souboru. Pokud je ted nejaky otevreny, musim ho
      // zavrit
      PageControl->Visible = false;
      FileCloseTables();
      Data->Soubor.Jmeno = "";
      NastavNazevOkna("");
    }

    return;
  }
  // Stahnul data a nyni je chce otevrit, nazev souboru zustal v dialogu (tj. stejne jako pri normlanim stahovani z modulu)
  OtevriTabulkyJakoSoubor(SaveDialog->FileName);
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::CurveRightAxisComboBoxChange(TObject *Sender)
{
  ZobrazRustovouKrivku();
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::ReportDBGridKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
  if (Key == 46) {
    // Delete = mazani zaznamu
    int Day;

    // Smazu zaznam na aktualni pozici
    if (Data->StatistikaQuery->RecordCount <= 1) {
      return;     // V tabulce uz zbyva jen posledni zaznam, ktery smazat nemuzu
    }

    // Zeptam se
    if (MessageBox(NULL, LoadStr(SMS_DELETE_RECORD).c_str(), Caption.c_str(), MB_ICONQUESTION | MB_YESNO | MB_SYSTEMMODAL) == IDNO) {
      return;   // Nechce smazat
    }

    // Smazu zaznam
    DeleteDay(Data->StatistikaQuery->FieldByName("DAY_NUMBER")->AsInteger);

    // Znovu nactu seznam dnu
    FillDays();

    // Obnovim zobrazeni
    ShowFirstDay();
  }
}
//---------------------------------------------------------------------------

void __fastcall THlavniForm::Russian1Click(TObject *Sender)
{
  SetLanguage(LANG_RUSSIAN);
}
//---------------------------------------------------------------------------

