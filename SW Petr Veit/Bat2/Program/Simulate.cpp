//******************************************************************************
//
//   Simulate.cpp     Simulace algoritmu vahy Bat2
//
//******************************************************************************

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Simulate.h"
#include "c51/Bat2.h"     // Nosnost vah
#include "c51/Stat.h"     // Statistika
#include "c51/Histogr.h"  // Histogram
#include "c51/Filter.h"   // Novy detekcni algoritmus

#define POCATECNI_HMOTNOST_NASLAPNYCH_VAH    SCALE_CAPACITY    // Hmotnost, kterou pri startu ulozim do pole historie, aby mne to hned po startu neulozilo

TSimulate Simulate;
static float CalConst;          // Predpocitana konstanta kalibrace

#ifdef ADC_PREFILTER
   static TRawWeight Fifo[ ADC_PREFILTER];
   static byte       FifoPointer;
#endif


//---------------------------------------------------------------------------

#pragma package(smart_init)


//-----------------------------------------------------------------------------
// Lokalni funkce
//-----------------------------------------------------------------------------

static void InitFilter();
// Inicializace filtru pro aktualni den vazeni podle aktualni konfigurace a kalibrace.

static long CalculateWeight(long Conversion, TYesNo SubtractZero);
// Vrati hmotnost vypoctenou z prevodu <Conversion>. Pokud je <SubtractZero> true, odecte od <Conversion> prevod nuloveho bodu.

//-----------------------------------------------------------------------------
// Podminka nastupu
//-----------------------------------------------------------------------------

#define Nastup(Rozdil, HorniMez, DolniMez) (Rozdil <= (long)HorniMez && Rozdil >= (long)DolniMez)

//-----------------------------------------------------------------------------
// Automat naslapne vahy
//-----------------------------------------------------------------------------

static TYesNo ZkontrolujNaslapnouVahu(TNVaha *Vaha) {
  // Zkontroluje hmotnost u zadane vahy s aktualni rustovou krivkou a pokud to nekam sedne, tak hmotnost ulozi do <Vaha> a vrati YES
  // Vyuziva hejno nactene v globalni promenne Hejno
  long   PrumernaHmotnost;             // Pro zrychleni
  long   RozdilHmotnosti;              // Pro zrychleni
  TYesNo HmotnostPadlaNaSamice = NO;   // Flagy, zda ustalena hmotnost padla na samce nebo/a na samice
  TYesNo HmotnostPadlaNaSamce  = NO;

  TAverageValue Stabilization;

  switch (Simulate.DetectionAlgorithm) {

    case DETECTION_OLD:
      // Pridam vzorek
      AverageAdd(&Vaha->Average, Vaha->Hmotnost);

      // Cekam, dokud se klouzavy prumer nenaplni
      if (!AverageFull(&Vaha->Average)) {
        return NO;   // Klouzavy prumer jeste neni zaplnen
      }

      // Test ustaleni klouzaveho prumeru

/*      Stabilization = (TAverageValue)Simulate.UstaleniNaslapneVahy * (TAverageValue)Simulate.NormovanaHmotnostSamice / 1000L;
      if (Stabilization < 2) {
        Stabilization = 2;
      }
      if (!AverageSteady(&Vaha->Average, Stabilization)) {*/
      if (!AverageSteady(&Vaha->Average, (TAverageValue)Simulate.UstaleniNaslapneVahy * (TAverageValue)Simulate.NormovanaHmotnostSamice / 1000L)) {
        if (Vaha->Ustaleni) {
          // Pokud byla vaha dosud ustalena, smazu vsechny prvky, pri skokove zmene to nekdy blblo (cilova 1000, ustaleni +-1.0%,
          // skok hmotnosti z 1000->1020 zpusobil oscilace ustaleni).
          AverageClear(&Vaha->Average);
          // Aktualni vzorek, ktery zpusobil neustaleni, schvalne nyni nedavam do klouzaveho prumeru, dam tam az dalsi vzorek.
        }
        Vaha->Ustaleni = NO;
        return NO;   // Vaha neni ustalena
      }


      // Vaha je ustalena, vypoctu aktualni prumernou hmotnost
      PrumernaHmotnost = AverageGetAverage(&Vaha->Average);

      // Pokud byla vaha ustalena i v minulem kroku, stav se nemeni a nic netestuju
      if (Vaha->Ustaleni) {
        Vaha->PosledniUstalenaHmotnost = PrumernaHmotnost;          // 7.4.2009: Aktualizuju posledni ustalenou hmotnost
        return NO;
      }

      // Prave doslo k ustaleni (v minulem kroku vaha ustalena nebyla, ted ano)
      Vaha->Ustaleni   = YES;                        // Nastavim flag ustaleni

      RozdilHmotnosti  = PrumernaHmotnost - Vaha->PosledniUstalenaHmotnost; // Rozdil hmotnosti
      Vaha->PosledniUstalenaHmotnost = PrumernaHmotnost;                    // Zapamatuju si ustalenou hmotnost pro dalsi krok

      break;

    case DETECTION_NEW:
      // Zkontroluju nove ustalenou hodnotu
      if (!GetFilterReady()) {
        return NO;              // Zatim nedoslo k ustaleni
      }

      // Ulozim si rozdil ustalenych hmotnosti
      ClrFilterReady();         // handshake
      RozdilHmotnosti = CalculateWeight(FilterRecord.Weight - FilterRecord.LastStableWeight, NO);
      Vaha->Ustaleni   = YES;                        // Nastavim flag ustaleni
      break;

  }//switch

  // Zkoktroluju, zda rozdil hmotnosti odpovida nastavenemu typu naskoku/seskoku
  switch (Simulate.JumpMode) {
    case JUMPMODE_ENTER:
      // Vaha detekuje pouze naskok, tj. vzrust hmotnosti
      if (RozdilHmotnosti < 0) {
        return NO;      // Doslo k poklesu => dal nevyhodnocuju
      }
      break;

    case JUMPMODE_LEAVE:
      // Vaha detekuje pouze seskok, tj. pokles hmotnosti
      if (RozdilHmotnosti > 0) {
        return NO;      // Doslo ke vzrustu => dal nevyhodnocuju
      }
      break;

      // Default vaha detekuje naskok i seskok, neni treba kontrolovat znamenko
  }//switch
  RozdilHmotnosti = labs(RozdilHmotnosti);    // Rozdil prevedu na kladny

  if (Nastup(RozdilHmotnosti, Simulate.HorniMezSamice, Simulate.DolniMezSamice)) {
    HmotnostPadlaNaSamice = YES;   // Nastavim flag, ze hmotnost padla do oblasti pro samice
  } else if (Simulate.RozdelovatPohlavi && Nastup(RozdilHmotnosti, Simulate.HorniMezSamci, Simulate.DolniMezSamci)) {
    HmotnostPadlaNaSamce = YES;    // Nastavim flag, ze hmotnost padla do oblasti pro samce
  }

  if (!HmotnostPadlaNaSamice && !HmotnostPadlaNaSamce) {
    return NO;  // Hmotnost nikam nepadla
  }

  // Pokud hmotnost padla do obojiho pohlavi, musim urcit, ke kteremu ma blize a rozhodnout tak o pohlavi, ktere pravdepodobneji na vaze stoji
  if (HmotnostPadlaNaSamice && HmotnostPadlaNaSamce) {
    // Musim vypocitat odchylky od normy samic a samcu pro tento den a zvolit to pohlavi, ke kteremu je hmotnost blize
    if (labs( Simulate.NormovanaHmotnostSamice - RozdilHmotnosti) <= labs( Simulate.NormovanaHmotnostSamci - RozdilHmotnosti)) {
      // Je to bliz samicim nebo je to nastejno (v tom pripade preferuju samice, protoze jich byva na hale vic a je tedy vetsi pravdepodobnost, ze je to samice)
      HmotnostPadlaNaSamce = NO;   // Snuluju flag pro samce, protoze je to samice (flag pro samice necham nahozeny)
    } else {
      HmotnostPadlaNaSamice = NO;  // Snuluju flag pro samice, protoze je to samec (flag pro samce necham nahozeny)
    }//else
  }//if

  Vaha->PosledniUlozenaHmotnost = RozdilHmotnosti;
  Vaha->PosledniUlozenePohlavi  = HmotnostPadlaNaSamce;  // 0 = samice, 1 = samec
  return YES;
}  // ZkontrolujNaslapnouVahu

//-----------------------------------------------------------------------------
// Obsluha vsech vah
//-----------------------------------------------------------------------------

TYesNo NVahyObsluha(int Weight)
// Projede vsechny prevody naslapnych vah a pokud je nejaky novy prevod,
// vypocte jeho hmotnost, zkontroluje, zda se ma ulozit atd.
// Automaticky nastavuje promennou Zobrazit
{
  TRawWeight Conversion;
#ifdef ADC_PREFILTER
  TLongWeight FifoSum;
#endif

  switch (Simulate.DetectionAlgorithm) {

    case DETECTION_OLD:
      Simulate.NVaha.NovaHmotnost = NO;

      Simulate.NVaha.Suma += Weight;
      Simulate.NVaha.Pocet++;
      if (Simulate.NVaha.Pocet < Simulate.Filtr) {
        return NO;   // Jeste nemam nasumovany potrebny pocet vzorku
      }//if

      // Vypocti z prevodu novou hmotnost :
      Simulate.NVaha.Suma += (long)(Simulate.NVaha.Pocet / 2);
      Simulate.NVaha.Hmotnost = Simulate.NVaha.Suma / (long)Simulate.NVaha.Pocet;
      // Vynuluju sumu
      Simulate.NVaha.Suma = 0;
      Simulate.NVaha.Pocet = 0;

      //---------------------- vyhodnoceni nastupu :
      if( Simulate.NormovanaHmotnostSamice == 0 ||
          Simulate.RozdelovatPohlavi && Simulate.NormovanaHmotnostSamci == 0){
        // Pokud je mimo krivku (tj. normovana hmotnost je nulova), taky nic nedelam
        return NO;
      }
      break;

    case DETECTION_NEW:
      // Vlozim novy vzorek do filtru
      Conversion = Simulate.Calibration.ZeroCalibration + (float)((float)Weight / CalConst);

#ifdef ADC_PREFILTER
      if (Simulate.UsePrefilter) {
        Fifo[ FifoPointer++] = Conversion;
        if( FifoPointer >= ADC_PREFILTER){
          FifoPointer = 0;
        }
        FifoSum = 0;
        for(int i = 0; i < ADC_PREFILTER; i++){
          FifoSum += Fifo[ i];
        }
        Conversion = FifoSum / ADC_PREFILTER;
      }
#endif

      FilterNextSample(Conversion);
      break;

  }//switch

  // Obslouzim vahu
  return ZkontrolujNaslapnouVahu( &Simulate.NVaha);        // Zkontroluje novou hmotnost
} // NVahyObsluha

//---------------------------------------------------------------------------
// Inicializace
//---------------------------------------------------------------------------

void SimulateInit() {
  // Inicializace
  Simulate.NVaha.Suma          = 0;
  Simulate.NVaha.Pocet         = 0;
  Simulate.NVaha.ZahoditVzorek = YES;
  Simulate.NVaha.Hmotnost      = 0;
  Simulate.NVaha.NovaHmotnost  = NO;

  Simulate.NVaha.PosledniUlozenaHmotnost = 0;
  Simulate.NVaha.PosledniUlozenePohlavi  = 0;

  Simulate.NVaha.PosledniUstalenaHmotnost = POCATECNI_HMOTNOST_NASLAPNYCH_VAH;   // Aby to hned neukladalo
  Simulate.NVaha.Ustaleni = NO;

  // Inicializace klouzaveho prumeru podle konfigurace
  AverageSetLength(&Simulate.NVaha.Average, Simulate.DelkaUstaleniNaslapneVahy);
  AverageClear(&Simulate.NVaha.Average);

  StatClear(&Simulate.StatSamice);
  StatClear(&Simulate.StatSamci);
  HistogramClear(&Simulate.HistSamice);
  HistogramClear(&Simulate.HistSamci);

  Simulate.Samples[GENDER_FEMALE].clear();
  Simulate.Samples[GENDER_MALE].clear();

  // Inicializace noveho detekcniho algoritmu
  // 5.12.2007: U starsich vah se kalibrace ukladala nekorektne, CalConst mohl hazet vyjimku.
  try {
    CalConst = (float)Simulate.Calibration.Range / (float)(Simulate.Calibration.RangeCalibration - Simulate.Calibration.ZeroCalibration);
  } catch(...) {
    CalConst = 1;
  }
  if (CalConst == 0) {
    CalConst = 1;
  }
  Simulate.DetectionAlgorithm = DETECTION_OLD;
  InitFilter();
  FilterStart();             // start filtru
}

// ---------------------------------------------------------------------------------------------
// Nastaveni filtrace
// ---------------------------------------------------------------------------------------------

static void InitFilter() {
  // Inicializace filtru pro aktualni den vazeni podle aktualni konfigurace a kalibrace.
  int StabilizationKg;

  FilterRecord.AveragingWindow = FILTER_MULTIPLIER             * Simulate.Filtr;
  FilterRecord.StableWindow    = STABILIZATION_TIME_MULTIPLIER * Simulate.DelkaUstaleniNaslapneVahy;
  FilterRecord.ZeroWeight      = Simulate.Calibration.ZeroCalibration;
  StabilizationKg = (float)((float)Simulate.UstaleniNaslapneVahy * (float)Simulate.NormovanaHmotnostSamice / 1000.0);
  if (StabilizationKg < 5) {
    StabilizationKg = 5;                // Pod 5 gramu nejdu
  }
  FilterRecord.StableRange     = (float)StabilizationKg / CalConst;
  FilterStop();    // vychozi stav filtru
#ifdef ADC_PREFILTER
   for(int i = 0; i < ADC_PREFILTER; i++){
      Fifo[ i] = 0;
   }
   FifoPointer = 0;
#endif
} // InitFilter

//---------------------------------------------------------------------------
// Krok simulace
//---------------------------------------------------------------------------

void SimulateStep(double Weight) {
  // 1 krok simulace s novou hmotnosti <Weight>
  if (!NVahyObsluha(1000.0 * Weight)) {
    return;  // Nedoslo k nastupu
  }
  // Zvazil se novy vzorek, ulozim do archivu
  if( Simulate.NVaha.PosledniUlozenePohlavi == GENDER_FEMALE){
     StatAdd( &Simulate.StatSamice, Simulate.NVaha.PosledniUlozenaHmotnost);       // hmotnost do statistiky - ve formatu XX.XXX
     HistogramAdd( &Simulate.HistSamice, Simulate.NVaha.PosledniUlozenaHmotnost);  // do histogramu
     Simulate.Samples[GENDER_FEMALE].push_back(Simulate.NVaha.PosledniUlozenaHmotnost);     // Ulozim si vzorek
  } else {
     StatAdd( &Simulate.StatSamci, Simulate.NVaha.PosledniUlozenaHmotnost);        // hmotnost do statistiky - ve formatu XX.XXX
     HistogramAdd( &Simulate.HistSamci, Simulate.NVaha.PosledniUlozenaHmotnost);   // do histogramu
     Simulate.Samples[GENDER_MALE].push_back(Simulate.NVaha.PosledniUlozenaHmotnost);       // Ulozim si vzorek
  }
}

//-----------------------------------------------------------------------------
// Vypocet aktualni hmotnosti
//-----------------------------------------------------------------------------

static long CalculateWeight(long Conversion, TYesNo SubtractZero) {
  // Vrati hmotnost vypoctenou z prevodu <Conversion>. Pokud je <SubtractZero> true, odecte od <Conversion> prevod nuloveho bodu.
  float f;              // Musim bohuzel float

  // Vypoctu hmotnost
  if (SubtractZero) {
    Conversion -= Simulate.Calibration.ZeroCalibration;
  }
  f = (float)Conversion * CalConst;

  // Zaokrouhleni podle dilku
  if (f >= 0) {
    f += (float)Simulate.Calibration.Division / 2.0;
  } else {
    f -= (float)Simulate.Calibration.Division / 2.0;
  }
  Conversion = f;
  return Conversion;
} // CalculateWeight

