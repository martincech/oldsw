//---------------------------------------------------------------------------

#ifndef EditRs485ScaleH
#define EditRs485ScaleH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TModbusEditScaleForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TComboBox *AddressComboBox;
        TComboBox *PortComboBox;
        TEdit *NameEdit;
        TButton *Button1;
        TButton *Button2;
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TModbusEditScaleForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusEditScaleForm *ModbusEditScaleForm;
//---------------------------------------------------------------------------
#endif
