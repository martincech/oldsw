//***************************************************************************
//
//    ModbusOnlineScale.cpp - Scale in an online table
//    Version 1.0
//
//***************************************************************************

#include "ModbusOnlineScale.h"

//---------------------------------------------------------------------------
// Konstruktor
//---------------------------------------------------------------------------

TModbusOnlineScale::TModbusOnlineScale() {
  Clear();
}

//---------------------------------------------------------------------------
// Nulovani dat
//---------------------------------------------------------------------------

void TModbusOnlineScale::Clear() {
  // Vynuluje pracovni data
  Live         = false;
  Command      = ONLINE_COMMAND_IDLE;
  ErrorCounter = 0;
}

//---------------------------------------------------------------------------
// Posun na dalsi prikaz
//---------------------------------------------------------------------------

bool TModbusOnlineScale::MoveToNextCommand() {
  // Presune se na dalsi prikaz, pri prechodu z poseldniho prikazu zpet na prvni vrati true
  switch (Command) {

    case ONLINE_COMMAND_IDLE:
      Command = ONLINE_COMMAND_READ_CONFIG;
      return false;

    case ONLINE_COMMAND_READ_CONFIG:
      Command = ONLINE_COMMAND_READ_DATETIME;
      return false;

    case ONLINE_COMMAND_READ_DATETIME:
      if (ScaleData.Config.WeighingStart.Running) {
        // Vazi se => nactu pauzu a aktualni vysledky
        Command = ONLINE_COMMAND_READ_PAUSE;
        return false;
      }
      // Nevazi se
      Command = ONLINE_COMMAND_READ_CONFIG;
      return true;

    case ONLINE_COMMAND_READ_PAUSE:
      if (ScaleData.Config.WeighingStart.UseFlock) {
        // Vazi podle hejna, nactu ho
        Command = ONLINE_COMMAND_READ_CURRENT_FLOCK;
      } else {
        // Nevazi podle hejna
        Command = ONLINE_COMMAND_READ_TODAY;
      }
      return false;

    case ONLINE_COMMAND_READ_CURRENT_FLOCK:
      Command = ONLINE_COMMAND_READ_TODAY;
      return false;

    case ONLINE_COMMAND_READ_TODAY:
      Command = ONLINE_COMMAND_READ_CONFIG;
      return true;

    default:
      return false;         // Nejaky jiny prikaz
  }
}

//---------------------------------------------------------------------------
// Vykonani prikazu
//---------------------------------------------------------------------------

TModbusScaleResult TModbusOnlineScale::ExecuteCommand() {
  // Provede prikaz <Command>, vrati vysledek prikazu
  if (!Connected) {
    return MB_RESULT_ERROR;             // Vaha neni pripojena
  }

  switch (Command) {
    case ONLINE_COMMAND_IDLE:
      return MB_RESULT_OK;

    case ONLINE_COMMAND_READ_CONFIG:
      // Nactu konfiguraci
      return ReadConfig();

    case ONLINE_COMMAND_READ_DATETIME:
      // Nactu datum a cas
      return ReadDateTime();

    case ONLINE_COMMAND_READ_PAUSE:
      // Nactu pauzu vazeni
      return ReadPause();

    case ONLINE_COMMAND_READ_CURRENT_FLOCK:
      // Nactu hejno, podle ktereho se vazi
      return ReadFlock(ScaleData.Config.WeighingStart.CurrentFlock);

    case ONLINE_COMMAND_READ_TODAY:
      // Nactu dnesek
      return ReadTodayFromArchive();

    default:
      return MB_RESULT_ERROR;
  }
}

//---------------------------------------------------------------------------
// Inicializace vykonavani
//---------------------------------------------------------------------------

void TModbusOnlineScale::InitExecute() {
  // Provede inicializaci pred zacatkem vykonavani prikazu
  Command      = ONLINE_COMMAND_IDLE;
  ErrorCounter = 0;
}

//---------------------------------------------------------------------------
// Vykonani dalsiho prikazu
//---------------------------------------------------------------------------

TModbusOnlineExecution TModbusOnlineScale::ExecuteNextCommand(int Repetitions) {
  // Provede dalsi prikaz, po provedeni posledniho prikazu v rade vrati true
  if (ExecuteCommand() != MB_RESULT_OK) {
    // Prikaz se nazdaril
    if (++ErrorCounter < Repetitions) {
      // Zatim jeste citam
      return ONLINE_EXECUTION_RUNNING;
    }
    // Nepodarilo se ani na vicekrat, vaha neodpovida
    Live = false;       // Vaha nezije
    return ONLINE_EXECUTION_ERROR;
  }

  // Prikaz se podaril, snuluju pocitadlo chyb
  ErrorCounter = 0;

  // Posunu se na dalsi prikaz
  if (MoveToNextCommand()) {
    // Vykonal se posledni prikaz v rade, vsechna data jsou nactena
    Live = true;        // Vaha zije
    return ONLINE_EXECUTION_DONE;
  }

  // Jeste jsem neprovedl posledni prikaz
  return ONLINE_EXECUTION_RUNNING;
}

