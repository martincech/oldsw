//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SelectModem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSelectModemForm *SelectModemForm;
//---------------------------------------------------------------------------
__fastcall TSelectModemForm::TSelectModemForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSelectModemForm::Button1Click(TObject *Sender)
{
  if (ModemListBox->ItemIndex < 0) {
    return;     // Nevybral zadny modem
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TSelectModemForm::FormShow(TObject *Sender)
{
  ModemListBox->SetFocus();
}
//---------------------------------------------------------------------------
void __fastcall TSelectModemForm::ModemListBoxDblClick(TObject *Sender)
{
  // Jakoby stisknul OK
  if (ModemListBox->ItemIndex < 0) {
    return;     // Nevybral zadny modem
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------

