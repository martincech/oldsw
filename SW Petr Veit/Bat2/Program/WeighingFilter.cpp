//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "WeighingFilter.h"
#include "Konstanty.h"  // Konstanty resources
#include "c51/Bat2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TWeighingFilterForm *WeighingFilterForm;


//---------------------------------------------------------------------------
// Kontrola zadaneho filtru
//---------------------------------------------------------------------------

bool TWeighingFilterForm::CheckValues() {
  // Kontrola zadaneho filtru, muze byt i prazdne
  try {
    if (NameEdit->Text.IsEmpty()) {
      NameEdit->SetFocus();
      throw 1;  // Nazev filtru musi byt zadany
    }
    if (!ScalesEdit->Text.IsEmpty() && (ScalesEdit->Text.ToInt() < MIN_IDENTIFICATION_NUMBER || ScalesEdit->Text.ToInt() > MAX_IDENTIFICATION_NUMBER)) {
      ScalesEdit->SetFocus();
      throw 1;  // Id. cislo nesmi byt 0
    }
  } catch(...) {
    MessageBox(NULL,LoadStr(HLAVNI_SPATNA_HODNOTA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
    return false;
  }
  return true;  // Vse je zadane v poradku
}

//---------------------------------------------------------------------------
__fastcall TWeighingFilterForm::TWeighingFilterForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TWeighingFilterForm::FormShow(TObject *Sender)
{
  if (NameEdit->Enabled) {
    NameEdit->SetFocus();       // Zaklada novy filtr
  } else {
    ScalesEdit->SetFocus();     // Edituje existujici filtr, nazev nejde menit
  }
}
//---------------------------------------------------------------------------
void __fastcall TWeighingFilterForm::Button1Click(TObject *Sender)
{
  if (!CheckValues()) {
    return;     // Filtr neni spravne zadany
  }
  ModalResult = mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TWeighingFilterForm::ScalesEditKeyPress(TObject *Sender,
      char &Key)
{
  // Filtruju cislice (musim i backspace)
  //     Backspace       0     az    9
  //         |           |           |
  if (Key != 8 && (Key < 48 || Key > 57)) {
    Key = 0;  // Vyberu jen cisla
  }
}
//---------------------------------------------------------------------------
