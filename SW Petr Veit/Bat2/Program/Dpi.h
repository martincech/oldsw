//***************************************************************************
//
//    Dpi.cpp - Correction of DPI in Windows
//    Version 1.0
//
//***************************************************************************

#ifndef DPI_H
#define DPI_H

#include "Grids.hpp"            // TStringList

void DpiSet(int NewDpi);
// Nastavi DPI na <NewDpi>

void DpiSetStringGrid(TStringGrid *StringGrid);
// Nastavi vysku radku ve <StringGrid> podle nastaveneho DPI, volat v kontruktoru okna

#endif // DPI_H

