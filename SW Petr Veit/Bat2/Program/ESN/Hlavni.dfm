˙
 THLAVNIFORM 0tˇ  TPF0THlavniForm
HlavniFormLeftůTopeWidth Height&CaptionBat2Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightő	Font.NameMS Sans Serif
Font.Style Menu	MainMenu1OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnResize
FormResizePixelsPerInch`
TextHeight TPageControlPageControlLeft Top%WidthHeightÓ
ActivePageKrivkaTabSheetAlignalClientTabOrder Visible 	TTabSheetReportTabSheetCaptionReporte
ImageIndex
 TDBGridReportDBGridLeft Top9WidthHeight~AlignalClient
DataSourceData.StatistikaQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightőTitleFont.NameMS Sans SerifTitleFont.Style 	OnKeyDownReportDBGridKeyDownColumnsExpanded	FieldName
DAY_NUMBERTitle.AlignmenttaCenterTitle.CaptionDíaVisible	 	AlignmenttaRightJustifyExpanded	FieldNameDATE_TIME_DATETitle.AlignmenttaCenterTitle.CaptionFechaWidthJVisible	 Expanded	FieldName
STAT_COUNTTitle.AlignmenttaCenterTitle.CaptionNúmeroVisible	 Expanded	FieldNameAVERAGE_WEIGHTTitle.AlignmenttaCenterTitle.CaptionPromedioVisible	 Expanded	FieldNameGAINTitle.AlignmenttaCenterTitle.CaptionGanacia DiariaVisible	 Expanded	FieldNameSIGMATitle.AlignmenttaCenterTitle.CaptionDesviación stVisible	 Expanded	FieldNameCVTitle.AlignmenttaCenterTitle.CaptionCv [%]Visible	 Expanded	FieldNameUNITitle.AlignmenttaCenterTitle.CaptionUNI [%]Visible	 Expanded	FieldNameCOMPARETitle.AlignmenttaCenterTitle.CaptionCompararVisible	 Expanded	FieldName
DIFFERENCETitle.AlignmenttaCenterTitle.Caption
DiferenciaVisible	    TPanelReportPanelLeft Top WidthHeight9AlignalTop
BevelOuterbvNoneTabOrder TPanelPanel4LeftTop WidthHeight9AlignalRight
BevelOuterbvNoneTabOrder  TLabelLabel58Left^TopWidthEHeight	AlignmenttaRightJustifyCaptionComparar con:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonReportPorovnatSpeedButtonLeftsTopWidthHeightCaption...Flat	OnClickGrowthcurves1Click  	TComboBoxReportPorovnatComboBoxLeft° TopWidthÁ HeightStylecsDropDownList
ItemHeight TabOrder OnChangeKrivkaPorovnatComboBoxChange     	TTabSheetVazeniTabSheetCaptionMuestras
ImageIndex TDBGridDBGrid1Left Top WidthHeightˇAlignalClient
DataSourceData.ZaznamyQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightőTitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameSAMPLETitle.AlignmenttaCenterTitle.CaptionMuestraVisible	 Expanded	FieldName	TIME_HOURTitle.AlignmenttaCenterTitle.CaptionHoraVisible	 Expanded	FieldNameWEIGHTTitle.AlignmenttaCenterTitle.CaptionPesoVisible	     	TTabSheetStatistikaTabSheetCaptionEstadísticas
ImageIndex TLabelLabel3LeftTop Width*HeightCaption	Objetivo:  TLabelCilovaHmotnostLabelLefthTop Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelLabel5LeftTop0Width/HeightCaption	Promedio:  TLabelPrumerLabelLefthTop0Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelLabel7LeftTopXWidth(HeightCaptionNúmero:  TLabel
PocetLabelLeftkTopXWidthHeight	AlignmenttaRightJustifyCaption00000  TLabelLabel9LeftTophWidthCHeightCaptionDesviación st:  TLabel
SigmaLabelLeftnTophWidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelLabel11LeftTop Width;HeightCaptionUniformidad:  TLabelUniLabelLeftwTop WidthHeight	AlignmenttaRightJustifyCaption000  TLabelLabel13LeftTopxWidthHeightCaptionCv:  TLabelCvLabelLeftwTopxWidthHeight	AlignmenttaRightJustifyCaption000  TLabelLabel15LeftTop@WidthIHeightCaptionGanacia Diaria:  TLabelPrirustekLabelLefthTop@Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelCilovaHmotnostJednotkyLabelLeft Top WidthHeightCaptionkg  TLabelPrumerJednotkyLabelLeft Top0WidthHeightCaptionkg  TLabelPrirustekJednotkyLabelLeft Top@WidthHeightCaptionkg  TLabelLabel20Left Top WidthHeightCaption%  TLabelLabel21Left TopxWidthHeightCaption%  TPanelStatistikaPorovnaniPanelLeft° TopWidth Height 
BevelOuterbvNoneTabOrder  TLabelCilovaHmotnostPorovnaniLabelLeftTopWidth!Height	AlignmenttaRightJustifyCaption00,000  TLabelPrumerPorovnaniLabelLeftTop(Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelPocetPorovnaniLabelLeftTopPWidthHeight	AlignmenttaRightJustifyCaption00000  TLabelSigmaPorovnaniLabelLeftTop`WidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelUniPorovnaniLabelLeftTop WidthHeight	AlignmenttaRightJustifyCaption000  TLabelCvPorovnaniLabelLeftToppWidthHeight	AlignmenttaRightJustifyCaption000  TLabelPrirustekPorovnaniLabelLeftTop8Width!Height	AlignmenttaRightJustifyCaption00,000  TLabel$CilovaHmotnostJednotkyPorovnaniLabelLeft0TopWidthHeightCaptionkg  TLabelPrumerJednotkyPorovnaniLabelLeft0Top(WidthHeightCaptionkg  TLabelPrirustekJednotkyPorovnaniLabelLeft0Top8WidthHeightCaptionkg  TLabelLabel22Left0Top WidthHeightCaption%  TLabelLabel23Left0ToppWidthHeightCaption%  TLabelStatistikaPorovnaniLabelLeftTopWidth>HeightCaptionComparación    	TTabSheetHistogramTabSheetCaption
Histográma
ImageIndex 	TSplitterHistogramSplitterLeft TopWidthHeightCursorcrVSplitAlignalBottomBeveled	  TChartHistogramChartLeft Top WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.Increment;ßOnő?BottomAxis.MinorTickLengthBottomAxis.Title.CaptionPesoLeftAxis.Grid.ColorŞŞŞ LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionNúmeroLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeriesSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelHistogramPorovnaniPanelLeft TopWidthHeightŠ AlignalBottom
BevelOuterbvNoneTabOrder TChartHistogramPorovnaniChartLeft TopWidthHeight BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.Increment;ßOnő?BottomAxis.MinorTickLengthBottomAxis.Title.CaptionPesoLeftAxis.Grid.ColorŞŞŞ LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionNúmeroLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeries
BarSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelPanel1Left Top WidthHeightAlignalTop
BevelOuterbvNoneTabOrder TLabelHistogramPorovnaniLabelLeftTopWidth>HeightCaptionComparación  TPanelPanel2LeftWTop Widthš HeightAlignalRight
BevelOuterbvNoneTabOrder  	TCheckBoxHistogramPorovnaniOsaCheckBoxLeft`TopWidthIHeightCaption	Eje igualTabOrder OnClick"HistogramPorovnaniOsaCheckBoxClick      	TTabSheetAktivitaDenTabSheetCaptionActividad diaria
ImageIndex 	TSplitterAktivitaSplitterLeft TopWidthHeightCursorcrVSplitAlignalBottomBeveled	  TChartAktivitaDenChartLeft Top WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.IncrementBottomAxis.MaximumBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionTiempo (horas)LeftAxis.Grid.ColorŞŞŞ LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionNúmeroLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeries
BarSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZOffsetPercent<XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelAktivitaPorovnaniPanelLeft TopWidthHeightŠ AlignalBottom
BevelOuterbvNoneTabOrder TPanelPanel5Left Top WidthHeightAlignalTop
BevelOuterbvNoneTabOrder  TLabelAktivitaPorovnaniLabelLeftTopWidth>HeightCaptionComparación  TPanelPanel6LeftWTop Widthš HeightAlignalRight
BevelOuterbvNoneTabOrder    TChartAktivitaDenPorovnaniChartLeft TopWidthHeight BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.IncrementBottomAxis.MaximumBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionTiempo (horas)LeftAxis.Grid.ColorŞŞŞ LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.CaptionNúmeroLegend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder 
TBarSeries
BarSeries4Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZOffsetPercent<XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone     	TTabSheetAktivitaCelkemTabSheetCaptionActividad total
ImageIndex TChartAktivitaCelkemChartLeft Top WidthHeightˇBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.Grid.ColorŞŞŞ BottomAxis.Grid.StylepsSolidBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionDíaLeftAxis.Grid.ColorŞŞŞ LeftAxis.Grid.StylepsSolidLeftAxis.Title.CaptionNúmeroLegend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0View3DAlignalClient
BevelOuterbvNoneTabOrder  TLineSeriesLineSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclRedTitle	ActividadPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesLineSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclNavyTitleComparaciónPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone    	TTabSheetKrivkaTabSheetCaptionCurva de crecimiento
ImageIndex TChartKrivkaChartLeft Top>WidthHeightyBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.Grid.ColorŞŞŞ BottomAxis.Grid.StylepsSolidBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.CaptionDíaLeftAxis.ExactDateTimeLeftAxis.Grid.ColorŞŞŞ LeftAxis.Grid.StylepsSolidLeftAxis.IncrementÍĚĚĚĚĚĚĚű?LeftAxis.Title.CaptionPesoLegend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0Legend.VisibleRightAxis.Grid.VisibleRightAxis.Title.CaptionEstadísticasRightAxis.Title.Font.CharsetDEFAULT_CHARSETRightAxis.Title.Font.ColorclGreenRightAxis.Title.Font.HeightőRightAxis.Title.Font.NameArialRightAxis.Title.Font.Style View3DAlignalClient
BevelOuterbvNoneTabOrder  TLineSeries
BarSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclRedTitleRealPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclNavyTitleObjetivoPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclPurpleTitle	Comp realPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries4Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColor@ Title	Comp objtPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries5Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclOliveTitleTheoryPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries6Marks.ArrowLengthMarks.VisibleSeriesColorclGreenTitleEstadísticasVertAxis
aRightAxisPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone   TPanelKrivkaPanelLeft Top WidthHeight>AlignalTop
BevelOuterbvNoneTabOrder 	TCheckBoxKrivkaRealCheckBoxLeftTopWidth9HeightCaptionRealChecked	Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightő	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontState	cbCheckedTabOrder OnClickKrivkaRealCheckBoxClick  	TCheckBoxKrivkaTargetCheckBoxLeft`TopWidthIHeightCaptionObjetivoChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKrivkaTargetCheckBoxClick  TPanelKrivkaPorovnaniPanelLeft¸ Top Widthš Height"
BevelOuterbvNoneTabOrder TLabelKrivkaPorovnaniLabelLeft TopWidthAHeightCaptionComparación:  	TCheckBoxKrivkaCRealCheckBoxLeft TopWidth1HeightCaptionRealChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder OnClickKrivkaCRealCheckBoxClick  	TCheckBoxKrivkaCTargetCheckBoxLeftPTopWidthAHeightCaptionObjetivoChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKrivkaCTargetCheckBoxClick   TPanelKrivkaTeoriePanelLeftTop WidthHeight>AlignalRight
BevelOuterbvNoneTabOrder TLabelLabel52Left^TopWidthEHeight	AlignmenttaRightJustifyCaptionComparar con:Font.CharsetDEFAULT_CHARSET
Font.ColorclOliveFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonKrivkaPorovnatSpeedButtonLeftsTopWidthHeightCaption...Flat	OnClickGrowthcurves1Click  TLabelLabel60LeftgTop+Width<Height	AlignmenttaRightJustifyCaptionEje derecho:Font.CharsetDEFAULT_CHARSET
Font.ColorclGreenFont.Heightő	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxKrivkaPorovnatComboBoxLeft° TopWidthÁ HeightStylecsDropDownList
ItemHeightTabOrder OnChangeKrivkaPorovnatComboBoxChange  	TComboBoxCurveRightAxisComboBoxLeft° Top(WidthÁ HeightStylecsDropDownList
ItemHeightTabOrderOnChangeCurveRightAxisComboBoxChangeItems.Strings<ninguna selección>Ganacia DiariaNúmeroDesviación stCvUniformidad      	TTabSheetOnlineTabSheetCaptionTabla en línea
ImageIndex TDBGridOnlineDBGridLeft Top WidthHeightˇAlignalClient
DataSourceData.OnlineQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightőTitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameSAMPLETitle.AlignmenttaCenterTitle.CaptionMuestraVisible	 Expanded	FieldName	TIME_HOURTitle.AlignmenttaCenterTitle.CaptionHoraVisible	 Expanded	FieldNameWEIGHTTitle.AlignmenttaCenterTitle.CaptionPesoVisible	 	AlignmenttaCenterExpanded	FieldNameSTABLETitle.AlignmenttaCenterTitle.CaptionEstableWidth>Visible	 	AlignmenttaCenterExpanded	FieldNameSAVEDTitle.AlignmenttaCenterTitle.CaptionGrabadoWidth?Visible	     	TTabSheetOnlineGrafTabSheetCaptionDiagrama en línea
ImageIndex	 TChartOnlineChartLeft Top)WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.Visible
OnUndoZoomOnlineChartUndoZoomBottomAxis.Grid.ColorŞŞŞ BottomAxis.Grid.StylepsSolidBottomAxis.LabelsSeparationBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.StartPositionBottomAxis.EndPositioncBottomAxis.Title.CaptionMuestrasLeftAxis.AxisValuesFormat	#,##0.000LeftAxis.ExactDateTimeLeftAxis.Grid.ColorŞŞŞ LeftAxis.Grid.StylepsSolidLeftAxis.Increment;ßOnő?LeftAxis.StartPositionLeftAxis.Title.CaptionPesoLegend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0Legend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder OnMouseMoveOnlineChartMouseMove TFastLineSeriesLineSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height÷Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.VisibleSeriesColorclRedTitleOnlineLinePen.ColorclRedXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone   TPanelOnlineGrafPanelLeft Top WidthHeight)AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel44LeftpTopWidthAHeightCaptionPromediando:  TLabelLabel45Leftđ TopWidth*HeightCaptionmuestras  TLabelLabel46LeftHTopWidthHeightCaptionPeso:  TLabelOnlineHmotnostLabelLeftTopWidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelOnlineHmotnostJednotkyLabelLeft TopWidthHeightCaptionkg  TSpeedButtonOnlineZoomInSpeedButtonLeftTopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙ {°ť{°  ť°    OnClickOnlineZoomInSpeedButtonClick  TSpeedButtonOnlineZoomOutSpeedButtonLeft(TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙ {°ť{°  ť°    OnClickOnlineZoomOutSpeedButtonClick  TBevelBevel4LeftXTopWidthHeightShape
bsLeftLine  TBevelBevel6Left-TopWidthHeightShape
bsLeftLine  	TComboBoxOnlinePrumerovaniComboBoxLeft¸ TopWidth1HeightStylecsDropDownListDropDownCount
ItemHeightTabOrder OnChangeOnlinePrumerovaniComboBoxChangeItems.Strings12345678910111213141516171819202122232425262728293031323334353637383940     	TTabSheetNastaveniTabSheetCaption	Programar
ImageIndex TPageControlNastaveniPageControlLeftTopWidthHeightŠ
ActivePageNastaveniVazeniTabSheetTabOrder  	TTabSheetNastaveniVazeniTabSheetCaptionPesando
ImageIndex TLabelLabel42LeftTopXWidthHeightCaptionLote:  TLabelLabel59LeftToppWidth0HeightCaption	Comparar:  TDBCheckBoxDBCheckBox7LeftTopWidthHeightCaptionIniciando pesaje	DataFieldSTART_PROGRESS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder ValueCheckedTrueValueUncheckedFalse  TEditNastaveniHejnoEditLefthTopSWidthQHeightReadOnly	TabOrder  TDBCheckBoxDBCheckBox8LeftTop0WidthHeightCaptionEsperando para iniciar	DataFieldSTART_WAITING
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit11LefthTopmWidthQHeight	DataFieldCOMPARE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniHejnaTabSheetCaptionLotes
ImageIndex TLabelLabel4LeftTopWidthHeightCaptionLotes:  TListBoxSeznamHejnListBoxLeftTopWidthKHeight!
ItemHeightSorted	TabOrder OnClickSeznamHejnListBoxClick  TPanel
HejnaPanelLefthTopWidthń HeightY
BevelOuterbvNoneTabOrder TLabelLabel6LeftTopWidth(HeightCaptionNombre:  TLabelLabel24LeftTophWidth;HeightCaptionPeso desde:  TLabelLabel25LeftxTophWidthHeightCaptionhasta:  TDBEditJmenoDBEditLeftXTopWidth Height	DataFieldNAME
DataSourceData.HejnaDataSourceReadOnly	TabOrder   TDBEditDBEdit15LeftPTopeWidthHeight	DataField
WEIGH_FROM
DataSourceData.HejnaDataSourceReadOnly	TabOrder  TDBEditDBEdit16Left  TopeWidthHeight	DataFieldWEIGH_TO
DataSourceData.HejnaDataSourceReadOnly	TabOrder  TPageControlKrivkyPageControlLeftTop Widthá HeightŠ 
ActivePageKrivkaSamiceTabSheetTabOrder 	TTabSheetKrivkaSamiceTabSheetCaptionHembras TStringGridKrivkaSamiceStringGridLeft Top WidthŮ Height AlignalClientColCountDefaultRowHeightRowCountTabOrder 	ColWidthsB]    	TTabSheetKrivkaSamciTabSheetCaptionMachos
ImageIndex TStringGridKrivkaSamciStringGridLeft Top WidthŮ Height AlignalClientColCountDefaultRowHeightRowCountTabOrder 	ColWidthsB]    	TTabSheetPocatecniHmotnostTabSheetCaptionPeso inicial
ImageIndex TLabelLabel26LeftTopWidth-HeightCaptionHembras:  TDBEditPocatecniHmotnostSamiceDBEditLeft`TopWidthaHeight	DataFieldFEMALE_WEIGHT00
DataSourceData.HejnaDataSourceReadOnly	TabOrder   TPanelPocatecniHmotnostSamciPanelLeftTop0WidthÉ Height!
BevelOuterbvNoneTabOrder TLabelLabel27LeftTopWidth)HeightCaptionMachos:  TDBEditPocatecniHmotnostSamciDBEditLeftXTop WidthaHeight	DataFieldMALE_WEIGHT00
DataSourceData.HejnaDataSourceReadOnly	TabOrder      TDBCheckBoxDBCheckBox5LeftTop0WidthŃ HeightCaptionUsar machos y hembras	DataField
USE_GENDER
DataSourceData.HejnaDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox6LeftTopHWidthŃ HeightCaptionUsar curvas de crecimiento	DataField
USE_CURVES
DataSourceData.HejnaDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse    	TTabSheetNastaveniStatistikaTabSheetCaptionEstadísticas TLabelLabel8LeftTopWidthjHeightCaptionRango del histográma:  TLabelLabel10LeftTop0WidthkHeightCaptionRango de uniformidad:  TLabelLabel12Left~Top0WidthHeightCaptioną  TLabelLabel14LeftŹ Top0WidthHeightCaption%  TLabelLabel16LeftŹ TopWidthHeightCaption%  TLabelLabel28Left~TopWidthHeightCaptioną  TDBEditRozsahUniformityDBEditLeft Top-Width!Height	DataField	UNI_RANGE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditRozsahHistogramuDBEditLeft TopWidth!Height	DataField
HIST_RANGE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder    	TTabSheetNastaveniGsmTabSheetCaptionGSM
ImageIndex TLabelLabel29LeftTop  Width-HeightCaptionNúmeros:  TLabelLabel48Left"TopHWidth(HeightCaptionPeríodo:  TLabelLabel49LeftzTopHWidthHeightCaptionDias  TDBCheckBoxDBCheckBox1LeftTopWidthHeightCaptionUsar comunicación GSM	DataFieldGSM_USE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2LeftTop0WidthHeightCaption Enviar estadísticas a medianoche	DataFieldGSM_SEND_MIDNIGHT
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox3LeftTop`WidthHeightCaptionEnviar datos por solicitud	DataFieldGSM_SEND_REQUEST
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox4LeftTopxWidthHeightCaptionRevisar números	DataFieldGSM_CHECK_NUMBERS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit3LeftTop° Width Height	DataFieldGSM_NUMBER00
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit4LeftTopČ Width Height	DataFieldGSM_NUMBER01
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit5LeftTopŕ Width Height	DataFieldGSM_NUMBER02
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit6LeftTopř Width Height	DataFieldGSM_NUMBER03
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit7LeftTopWidth Height	DataFieldGSM_NUMBER04
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder	  TDBEditDBEdit8LeftPTopEWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniRs485TabSheetCaptionRS-485
ImageIndex TLabelLabel50LeftTopWidth0HeightCaption
Dirección:  TLabelLabel51LeftTop0Width2HeightCaption
Velocidad:  TLabelLabel53LeftTopŘ Width6HeightCaption
Respuesta:  TLabelLabel54LeftTopđ Width,HeightCaption
Intervalo:  TLabelLabel55Left Top0WidthHeightCaptionBd  TLabelLabel56Left TopŘ WidthHeightCaptionms  TLabelLabel57Left Topđ WidthHeightCaptionms  TDBEditDBEdit9LeftČ TopWidthQHeight	DataFieldRS485_ADDRESS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   TDBEditDBEdit10LeftČ Top-WidthQHeight	DataFieldRS485_SPEED
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit12LeftČ TopŐ WidthQHeight	DataFieldRS485_REPLY_DELAY
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit13LeftČ Topí WidthQHeight	DataFieldRS485_SILENT_INTERVAL
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBRadioGroupDBRadioGroup4LeftTopPWidth	Height1CaptionParidadColumns	DataFieldRS485_PARITY
DataSourceData.KonfiguraceDataSourceItems.Strings8-n-18-e-18-o-18-n-2 ReadOnly	TabOrderValues.Strings0123   TDBRadioGroupDBRadioGroup5LeftTop Width	Height1Caption	ProtocoloColumns	DataFieldRS485_PROTOCOL
DataSourceData.KonfiguraceDataSourceItems.Strings
MODBUS RTUMODBUS ASCII ReadOnly	TabOrderValues.Strings0123    	TTabSheetNastaveniVahyTabSheetCaptionBalanzas
ImageIndex TLabelLabel30LeftTop0WidthŽ HeightCaption%Valor sobre el objetivo para hembras:  TLabelLabel31LeftTop0WidthHeightCaption%  TLabelLabel32LeftTopHWidthş HeightCaption'Valor debajo del objetivo para hembras:  TLabelLabel33LeftTopHWidthHeightCaption%  TLabelLabel34LeftTop`WidthŤ HeightCaption$Valor sobre el objetivo para machos:  TLabelLabel35LeftTop`WidthHeightCaption%  TLabelLabel36LeftTopxWidthˇ HeightCaption&Valor debajo del objetivo para machos:  TLabelLabel37LeftTopxWidthHeightCaption%  TLabelLabel38LeftTop¨ WidthCHeightCaptionEstabilización:  TLabelLabel39LeftŢ Top¨ WidthHeightCaptioną  TLabelLabel40LeftTop¨ WidthHeightCaption%  TLabelLabel41LeftTopŔ WidthwHeightCaptionTiempo de estabilización:  TLabelLabel43LeftTop WidthHeightCaptionFiltro:  TLabelLabel47LeftTopWidthxHeightCaptionNúmero de identificación:  TDBEditSamiceOkoliNadDBEditLeftč Top-Width!Height	DataFieldF_MARGIN_ABOVE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamiceOkoliPodDBEditLeftč TopEWidth!Height	DataFieldF_MARGIN_UNDER
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamciOkoliNadDBEditLeftč Top]Width!Height	DataFieldM_MARGIN_ABOVE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamciOkoliPodDBEditLeftč TopuWidth!Height	DataFieldM_MARGIN_UNDER
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditUstaleniDBEditLeftč TopĽ Width!Height	DataFieldSTABILIZATION
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDobaUstaleniDBEditLeftč Top˝ Width!Height	DataFieldSTABILIZATION_TIME
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBRadioGroupDBRadioGroup1LeftTopHWidthů Height1CaptionUnidadesColumns	DataFieldUNITS
DataSourceData.KonfiguraceDataSourceItems.Stringskglb ReadOnly	TabOrder
Values.Strings01   TDBEditFiltrDBEditLeftč Top Width!Height	DataFieldFILTER_VALUE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit2Leftč TopWidth!Height	DataFieldID
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TCheckBoxNastaveniGainCheckBoxLeftTopŘ Widthů HeightCaption#Usar el modo de ganancia automáticaTabOrder  TDBRadioGroupDBRadioGroup3LeftTopđ Widthů HeightICaptionGrabar muestra	DataField	JUMP_MODE
DataSourceData.KonfiguraceDataSourceItems.StringsAlmacenar al entrarAlmacenar al salirAlmacenar al entrar y salir ReadOnly	TabOrder	Values.Strings012    	TTabSheetTabSheetCorrectionCaptionCurva de corrección
ImageIndex TLabelLabel61LeftTopWidthHeightCaptionDía 1:  TLabelLabel62LeftTopHWidth6HeightCaptionCorrección:  TLabelLabel64LeftTop0WidthHeightCaptionDía 2:  TLabelLabel63Left~TopHWidthHeightCaption+  TLabelLabel65LeftŹ TopHWidthHeightCaption%  TDBEditDBEdit14Left TopWidth!Height	DataFieldCORRECTION_DAY1
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   TDBEditDBEdit18Left Top-Width!Height	DataFieldCORRECTION_DAY2
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit17Left TopEWidth!Height	DataFieldCORRECTION_CORRECTION
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniPodsvitTabSheetCaptionRetroiluminaciňn
ImageIndex TDBRadioGroupDBRadioGroup2LeftTopWidth9Height9CaptionRetroiluminaciňnColumns	DataField	BACKLIGHT
DataSourceData.KonfiguraceDataSourceItems.StringsApagado	Encendido
Automático ReadOnly	TabOrder Values.Strings012      	TTabSheetInformaceTabSheetCaptionInformación
ImageIndex TLabelLabel17LeftTop WidthiHeightCaptionVersion de la Balanza:  TLabel
VerzeLabelLeft Top Width5HeightCaption
VerzeLabel  TLabelLabel18LeftTopXWidthHeightCaptionNota:  TLabelLabel19LeftTop8Width=HeightCaptionDescargado:  TDBTextDBText1Left Top8Widthš Height	DataField	DATE_TIME
DataSourceData.HlavickaDataSource  TDBEditDBEdit1Left TopUWidthé Height	DataFieldNOTE
DataSourceData.HlavickaDataSourceTabOrder     TPanelPanel3Left Top WidthHeight%AlignalTop
BevelOuterbvNoneTabOrder TBevelBevel2Left TopWidthHeightAlignalBottomShape	bsTopLine  TBevelBevel3Left Top WidthHeightAlignalTopShape	bsTopLine  TSpeedButtonOpenSpeedButtonLeftTopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                    ŔŔŔ    ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙ wwwwwwwwwwwwwwww     ww 33330ww3333w°33330wű3333ż°     űűűűwwżżżżwwű   wwp wwww wwwwwwpwwwwwwwwwp wwwwwwwwwwParentShowHintShowHint	OnClickOtevt1Click  TSpeedButtonSaveSpeedButtonLeft TopWidthHeightEnabledFlat	
Glyph.Data
ú   ö   BMö       v   (                                                    ŔŔŔ    ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙ wwwwwwwww      p3   wp3   wp3   wp3    p333333p3    3p0wwwwp0wwwwp0wwwwp0wwwwp0wwww p0wwwwp      wwwwwwwwParentShowHintShowHint	OnClickUloit1Click  TSpeedButtonReadSpeedButtonLeft`TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙        wpřřřřp˙˙˙đParentShowHintShowHint	OnClickNastzznamy1Click  TSpeedButtonFindSpeedButtonLeft8TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                ŔŔŔ   ˙ ŔŔŔ  ˙˙ ˙   ŔŔŔ ˙˙  ˙˙˙ ÚÚÚÚÚÚÚÚ    ­­­˙˙˙
ÚÚ ­  
Ú 
   ­ ˙đ 
Ú˙đp­­pŕ đzÚţţđ­  đ
Ú ţ˙đ­Úp   zÚ­ p­­ÚÚ 
ÚÚ­­§ ­­­ParentShowHintShowHint	OnClickFindfile1Click  TSpeedButtonSetupSpeedButtonLeftxTopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                ŔŔŔ   ˙ ŔŔŔ  ˙˙ ˙   ŔŔŔ ˙˙  ˙˙˙ ÚÚÚÚÚÚÚÚ­­­­­­­­Đ
ÚÚÚ ÚÚ ­­ ­­Ú
Ú
ÚÚ­  ­­­ÚÚ
ÚÚÚ­­ ­­­­ÚÚ
ÚĐÚ   ­  
Ú 0 ­ 0ÚĐ
ÚĐ30Ú­ ­ 30­ÚÚÚ 3 
Ú­­­  ­­ParentShowHintShowHint	OnClickConfig1Click  TSpeedButtonDatabaseSpeedButtonLeftČ TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                ŔŔŔ   ˙ ŔŔŔ  ˙˙ ˙   ŔŔŔ ˙˙  ˙˙˙ ÚÚÚÚÚÚÚÚ­­­­­­­­ÚÚÚĐ    ­­­ ˙˙˙đÚÚ  đ đ­­đ˙˙˙đĐ  đ đ ˙đ˙˙˙đĐđ đ đ ˙đ˙˙˙đĐđ      ˙˙˙˙­Đđ    
Ú ˙˙˙đ­­­Đ    ÚÚÚ­­­­­­­­ParentShowHintShowHint	OnClickWeighingdatabase1Click  TSpeedButtonAddSpeedButtonLeftŕ TopWidthHeightEnabledFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙   ˙đđđ  0˙đťť  đđ  đđ˙đ   ˙˙  ParentShowHintShowHint	OnClickAddtoweighingdatabase1Click  TBevelBevel7LeftWTopWidthHeightShape
bsLeftLine  TBevelBevel8Left TopWidthHeightShape
bsLeftLine  TSpeedButtonRs485SpeedButtonLeft  TopWidthHeightFlat	
Glyph.Data
ú   ö   BMö       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙    8080Ş   80  ParentShowHintShowHint	OnClickRS4851Click  TBevelBevel9Leftż TopWidthHeightShape
bsLeftLine  TPanelZahlaviPanelLeftTopWidthÁHeight
BevelOuterbvNoneTabOrder Visible TLabelLabel1LeftTopWidthHeightCaptionDía:  TLabel
DatumLabelLeftŚ TopWidth;Height	AlignmenttaCenterAutoSizeCaption
00.00.0000  TSpeedButtonPrvniDenSpeedButtonLeft{Top WidthHeightFlat	
Glyph.Data
˛   Ž   BMŽ       v   (               8                                      ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙                 OnClickPrvniDenSpeedButtonClick  TSpeedButtonPredchoziDenSpeedButtonLeft Top WidthHeightFlat	
Glyph.Data
      BM       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙    OnClickPredchoziDenSpeedButtonClick  TSpeedButtonDalsiDenSpeedButtonLeftâ Top WidthHeightFlat	
Glyph.Data
      BM       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙           OnClickDalsiDenSpeedButtonClick  TSpeedButtonPosledniDenSpeedButtonLeft÷ Top WidthHeightFlat	
Glyph.Data
˛   Ž   BMŽ       v   (               8                                      ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙                 OnClickPosledniDenSpeedButtonClick  TBevelBevel5Left TopWidthHeightShape
bsLeftLine  TPanelPohlaviPanelLeft Top Width Height
BevelOuterbvNoneTabOrder  TLabelLabel2LeftTopWidth&HeightCaptionGénero:  TSpeedButtonSpeedButton1LeftXTopWidthHeightEnabledFlat	
Glyph.Data
"    BM      v   (               ¨                                      ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙                         px                     px        	NumGlyphs  TSpeedButtonSpeedButton2Left TopWidthHeightEnabledFlat	
Glyph.Data
ţ   ú   BMú       v   (                                                     ŔŔŔ   ˙  ˙   ˙˙ ˙   ˙ ˙ ˙˙  ˙˙˙    px       p          	NumGlyphs  TBevelBevel1Left TopWidthHeightShape
bsLeftLine  TRadioButtonPohlaviSamiceRadioButtonLeftHTopWidthHeightChecked	TabOrder TabStop	OnClickPohlaviSamiceRadioButtonClick  TRadioButtonPohlaviSamciRadioButtonLeftpTopWidthHeightTabOrderOnClickPohlaviSamciRadioButtonClick   	TComboBoxDenComboBoxLeft8TopWidth1HeightStylecsDropDownListDropDownCount
ItemHeightTabOrderOnChangeDenComboBoxChange    	TMainMenu	MainMenu1Left¨Top 	TMenuItemSoubor1CaptionArchivo 	TMenuItemOtevt1CaptionAbrirShortCutO@OnClickOtevt1Click  	TMenuItemUloit1CaptionGrabarShortCutS@OnClickUloit1Click  	TMenuItemN7Caption-  	TMenuItem	Findfile1CaptionUbicar archivo...OnClickFindfile1Click  	TMenuItemN14Caption-  	TMenuItemOpenforcomparsion1CaptionAbrir para compararOnClickOpenforcomparsion1Click  	TMenuItemN2Caption-  	TMenuItemTisk1CaptionImprimir 	TMenuItemMenuPrintReportCaptionReporteOnClickMenuPrintReportClick  	TMenuItemMenuPrintSamplesCaptionMuestrasOnClickMenuPrintSamplesClick  	TMenuItemMenuPrintHistogramCaption
HistográmaOnClickMenuPrintHistogramClick  	TMenuItemMenuPrintDayActivityCaptionActividad diariaOnClickMenuPrintDayActivityClick  	TMenuItemMenuPrintTotalActivityCaptionActividad totalOnClickMenuPrintTotalActivityClick  	TMenuItemMenuPrintGrowthCurveCaptionCurva de crecimientoOnClickMenuPrintGrowthCurveClick  	TMenuItemMenuPrintOnlineCaptionDiagrama en líneaOnClickMenuPrintOnlineClick   	TMenuItemExport1CaptionExportar a Excel 	TMenuItemMenuExportReportCaptionReporteOnClickMenuExportReportClick  	TMenuItemMenuExportSamplesCaptionMuestrasOnClickMenuExportSamplesClick  	TMenuItemMenuExportHistogramCaption
HistográmaOnClickMenuExportHistogramClick  	TMenuItemMenuExportDayActivityCaptionActividad diariaOnClickMenuExportDayActivityClick  	TMenuItemMenuExportTotalActivityCaptionActividad totalOnClickMenuExportTotalActivityClick  	TMenuItemMenuExportGrowthCurveCaptionCurva de crecimientoOnClickMenuExportGrowthCurveClick  	TMenuItemMenuExportOnlineCaptionTabla en líneaOnClickMenuExportOnlineClick   	TMenuItemN4Caption-  	TMenuItemMenuSimulationCaptionSimulación...OnClickMenuSimulationClick  	TMenuItemN5Caption-  	TMenuItemKonecprogramu1CaptionSalirOnClickKonecprogramu1Click   	TMenuItemModul1CaptionBalanzas 	TMenuItemMemorymodule1CaptionMódulo de Memoria 	TMenuItemNastzznamy1CaptionLeer datos...OnClickNastzznamy1Click  	TMenuItemConfig1CaptionProgramar...OnClickConfig1Click  	TMenuItemDiagnostics1CaptionDiagnóstico 	TMenuItemReaddatatofile1CaptionGrabar módulo...OnClickReaddatatofile1Click  	TMenuItemReaddatafromfile1CaptionCargar módulo...OnClickReaddatafromfile1Click    	TMenuItemRS4851CaptionBalanza RS-485...OnClickRS4851Click  	TMenuItemN3Caption-  	TMenuItemGrowthcurves1CaptionCurvas de crecimiento...OnClickGrowthcurves1Click   	TMenuItem	Database1CaptionBase de datos 	TMenuItemWeighingdatabase1CaptionMostrar pesos...OnClickWeighingdatabase1Click  	TMenuItemN6Caption-  	TMenuItemAddtoweighingdatabase1Caption$Ańadir archivo a la base de datos...EnabledOnClickAddtoweighingdatabase1Click   	TMenuItem	Nastaven1CaptionOpciones 	TMenuItemJazyk1CaptionIdioma 	TMenuItemDansk1CaptionDanskOnClickDansk1Click  	TMenuItemDeutsch1CaptionDeutschOnClickDeutsch1Click  	TMenuItem	Anglicky1CaptionEnglishOnClickAnglicky1Click  	TMenuItemEspanol1CaptionEspanolOnClickEspanol1Click  	TMenuItemFrancouzsky1CaptionFrançaisOnClickFrancouzsky1Click  	TMenuItemRussian1CaptionRussianOnClickRussian1Click  	TMenuItemFinsky1CaptionSuomiOnClickFinsky1Click  	TMenuItemTurkce1CaptionTurkceOnClickTurkce1Click    	TMenuItemNpovda1CaptionAyuda 	TMenuItem
Oprogramu1CaptionSobre...OnClickOprogramu1Click    TOpenDialog
OpenDialogFilter
Bat2|*.bt2OptionsofHideReadOnlyofPathMustExistofFileMustExistofEnableSizing LeftčTop   TSaveDialog
SaveDialogFilter
Bat2|*.bt2OptionsofOverwritePromptofHideReadOnlyofPathMustExistofEnableSizing LeftĘTop  TOpenDialogOpenDialogHardCopyFilterBat2 hardcopy|*.binOptionsofHideReadOnlyofPathMustExistofFileMustExistofEnableSizing Leftč  TSaveDialogSaveDialogHardCopyFilterBat2 hardcopy|*.binOptionsofOverwritePromptofHideReadOnlyofPathMustExistofEnableSizing LeftĘTopü˙     