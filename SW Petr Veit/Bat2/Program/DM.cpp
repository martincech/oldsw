//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include <inifiles.hpp>
#include "DM.h"
#include "Excel.h"
#include "MbcbPar.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TData *Data;

//---------------------------------------------------------------------------
// Vytvoreni stringu podle verze odberatele
//---------------------------------------------------------------------------

void TData::CreateVersionStrings() {
  // Podle verze odberatele vytvori stringy pouzivane v programu. Volat pri startu programu

  // Nastavim stringy podle verze

  Version.Contacts = new TStringList();
  Version.Logo = new Graphics::TBitmap();
  
#if defined(VERSION_BAT2)

  // Bat2 od VEIT Electronics
  Version.ScaleTitle    = "Bat2";
  Version.FileExtension = "bt2";
  Version.Contacts->Add("VEIT Electronics");
  Version.Contacts->Add("Modricka 52");
  Version.Contacts->Add("664 48  Moravany");
  Version.Contacts->Add("Czech Republic");
  Version.Contacts->Add("tel: +420 545 235 252");
  Version.Contacts->Add("fax: +420 545 235 256");
  Version.Contacts->Add("e-mail: veit@veit.cz");
  Version.Contacts->Add("www.veit.cz");
  LogoImageList->GetBitmap(0, Version.Logo);

#elif defined(VERSION_PS1)

  // PS1 od Dacs Dansko
  Version.ScaleTitle    = "PS1";
  Version.FileExtension = "ps1";
  Version.Contacts->Add("DACS a/s");
  Version.Contacts->Add("Falkevej 18");
  Version.Contacts->Add("DK - 8766 Nr. Snede");
  Version.Contacts->Add("Denmark");
  Version.Contacts->Add("tel: +45 75 77 19 22");
  Version.Contacts->Add("fax: +45 75 77 19 18");
  Version.Contacts->Add("e-mail: mail@dacs.dk");
  Version.Contacts->Add("www.dacs.dk");
  LogoImageList->GetBitmap(1, Version.Logo);

#elif defined(VERSION_COMSCALE)

  // ComScale pro Big Dutchman
  Version.ScaleTitle    = "ComScale";
  Version.FileExtension = "cs";
  Version.Contacts->Add("Big Dutchman International GmbH");
  Version.Contacts->Add("Postfach 1163");
  Version.Contacts->Add("D-49377 Vechta");
  Version.Contacts->Add("Deutschland");
  Version.Contacts->Add("Telefon +49 (0) 4447 801-0");
  Version.Contacts->Add("Fax +49 (0) 4447 801-237");
  Version.Contacts->Add("e-mail: big@bigdutchman.de");
  Version.Contacts->Add("www.bigdutchman.de");
  LogoImageList->GetBitmap(2, Version.Logo);

#endif
}

// --------------------------------------------------------------------------
// Overeni pripravenosti modulu
// --------------------------------------------------------------------------

bool TData::ModulPripraven() {
  // USB ctecka
  for (int i = 0; i < 10; i++) {              // Nekdy to blblo tak, ze 4x to zahlasilo bez modulu a po 5. pokusu uz to bylo OK
    if (Data->UsbFlash->Connected()) {
      return true;
    }
  }
  MessageBox(NULL,LoadStr(CHYBA_MODUL).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
  return false;
}


//---------------------------------------------------------------------------
// Nahrani vsech krivek z databaze
//---------------------------------------------------------------------------

void TData::LoadCurvesFromDatabase(TCurveList *CurveList) {
  // Nahraje krivky z databaze do <CurveList>
  TGrowthCurve *Curve;

  CurveList->Clear();

  try {
    CurvesTable->Open();
    CurvesTable->First();
    while (!CurvesTable->Eof) {

      // Zalozim novou krivku
      if (!CurveList->Add(CurvesTable->FieldByName("NAME")->AsString)) {
        // Neplatny nazev
        CurvesTable->Next();
        continue;
      }

      if ((Curve = CurveList->GetCurve(CurveList->GetIndex(CurvesTable->FieldByName("NAME")->AsString))) == NULL) {
        // Nemelo by nastat
        CurvesTable->Next();
        continue;
      }

      // Ulozim vsechny body
      for (int i = 0; i < CURVE_MAX_POINTS; i++) {
        if (CurvesTable->FieldByName("WEIGHT" + FormatFloat("00", i))->AsFloat == 0) {
          break;                // Konec krivky
        }
        Curve->Add(CurvesTable->FieldByName("DAY"    + FormatFloat("00", i))->AsInteger,
                   CurvesTable->FieldByName("WEIGHT" + FormatFloat("00", i))->AsFloat);
      }

      // Jdu na dalsi zaznam
      CurvesTable->Next();
    }//while
  } catch(...) {}

  CurvesTable->Close();
}

//---------------------------------------------------------------------------
// Ulozeni vsech krivek do databaze
//---------------------------------------------------------------------------

void TData::SaveCurvesToDatabase(TCurveList *CurveList) {
  // Ulozi krivky z <CurveList> do databaze
  TGrowthCurve *Curve;
  int Point;

  try {
    CurvesTable->EmptyTable();
    CurvesTable->Open();

    for (int i = 0; i < CurveList->Count(); i++) {
      // Nactu krivku ze seznamu
      if ((Curve = CurveList->GetCurve(i)) == NULL) {
        // Nemelo by nastat
        continue;
      }

      // Ulozim ji do tabulky
      CurvesTable->Append();
      CurvesTable->Edit();
      CurvesTable->FieldByName("NAME")->AsString = Curve->GetName();
      for (Point = 0; Point < Curve->Count(); Point++) {
        CurvesTable->FieldByName("DAY"    + FormatFloat("00", Point))->AsInteger = Curve->GetDay(Point);
        CurvesTable->FieldByName("WEIGHT" + FormatFloat("00", Point))->AsFloat   = Curve->GetWeight(Point);
      }

      CurvesTable->Post();
    }//for
  } catch(...) {}

  CurvesTable->Close();
}

// --------------------------------------------------------------------------
// Nacteni rustovych krivek do comboboxu/listboxu
// --------------------------------------------------------------------------

void TData::LoadCurvesToStrings(TStrings *Strings) {
  TCurveList *CurveList;

  // Pridam prvni polozku <none>
  Strings->Clear();
  Strings->Add(LoadStr(NONE));

  if ((CurveList = new TCurveList()) == NULL) {
    return;
  }

  try {

    // Nactu vsechny krivky do pameti
    LoadCurvesFromDatabase(CurveList);

    // Ulozim je do seznamu
    for (int i = 0; i < CurveList->Count(); i++) {
      Strings->Add(CurveList->GetCurve(i)->GetName());
    }

  } catch(...) {}

  delete CurveList;
}

//---------------------------------------------------------------------------
// Nahrani vsech vah RS-485 z databaze
//---------------------------------------------------------------------------

void TData::LoadModbusScalesFromDatabase(TModbusScaleList *ScaleList) {
  // Nahraje vahy z databaze do <ScaleList>
  TModbusScale *Scale;

  ScaleList->Clear();

  try {
    ModbusScalesTable->Open();
    ModbusScalesTable->First();
    while (!ModbusScalesTable->Eof) {
      // Zalozim novou vahu
      if (!ScaleList->Add(ModbusScalesTable->FieldByName("ADDRESS")->AsInteger,
                          ModbusScalesTable->FieldByName("PORT")->AsInteger,
                          ModbusScalesTable->FieldByName("NAME")->AsString)) {
        // Neplatna adresa
        ModbusScalesTable->Next();
        continue;
      }

      // Jdu na dalsi zaznam
      ModbusScalesTable->Next();
    }//while
  } catch(...) {}

  ModbusScalesTable->Close();
}

//---------------------------------------------------------------------------
// Ulozeni vsech vah do databaze
//---------------------------------------------------------------------------

void TData::SaveModbusScalesToDatabase(TModbusScaleList *ScaleList) {
  // Ulozi vahy z <ScaleList> do databaze
  TModbusScale *Scale;
  int Point;

  try {
    ModbusScalesTable->EmptyTable();
    ModbusScalesTable->Open();

    for (int i = 0; i < ScaleList->Count(); i++) {
      // Nactu vahu ze seznamu
      if ((Scale = ScaleList->GetScale(i)) == NULL) {
        // Nemelo by nastat
        continue;
      }

      // Ulozim ji do tabulky
      ModbusScalesTable->Append();
      ModbusScalesTable->Edit();
      ModbusScalesTable->FieldByName("ADDRESS")->AsInteger = Scale->GetAddress();
      ModbusScalesTable->FieldByName("PORT")->AsInteger    = Scale->GetPort();
      ModbusScalesTable->FieldByName("NAME")->AsString     = Scale->GetName();
      ModbusScalesTable->Post();
    }//for
  } catch(...) {}

  ModbusScalesTable->Close();
}

// --------------------------------------------------------------------------
// Nacteni vah do comboboxu/listboxu
// --------------------------------------------------------------------------

void TData::LoadModbusScalesToStrings(TStrings *Strings) {
  TModbusScaleList *ScaleList;
  TModbusScale     *Scale;

  Strings->Clear();
  if ((ScaleList = new TModbusScaleList()) == NULL) {
    return;
  }

  try {
    // Nactu vsechny vahy do pameti
    LoadModbusScalesFromDatabase(ScaleList);

    // Ulozim je do seznamu
    for (int i = 0; i < ScaleList->Count(); i++) {
      Scale = ScaleList->GetScale(i);
      Strings->Add(AnsiString(Scale->GetAddress()) + ": " + Scale->GetName());
    }

  } catch(...) {}

  delete ScaleList;
}

//---------------------------------------------------------------------------
// Nastaveni parametru komunikace
//---------------------------------------------------------------------------

void TData::SetModbusPortParameters() {
  // Nastavi parametry komunikace podle nastaveni
  MbcbModbusSetup.Speed          = Data->Nastaveni.Modbus.Speed;
  MbcbModbusSetup.Parity         = Data->Nastaveni.Modbus.Parity;
  MbcbModbusSetup.ReplyDelay     = Data->Nastaveni.Modbus.ReplyDelay;
  MbcbModbusSetup.SilentInterval = Data->Nastaveni.Modbus.SilentInterval;
  MbcbModbusSetup.Protocol       = Data->Nastaveni.Modbus.Protocol;
}

void TData::SetModbusReplyDelay(int NewReplyDelay) {
  // Nastavi ReplyDelay na <NewReplyDelay>
  MbcbModbusSetup.ReplyDelay = NewReplyDelay;
}

// --------------------------------------------------------------------------
// Nastaveni teoreticke krivky
// --------------------------------------------------------------------------

void TData::SetCompareCurve(TCompareCurveIndex Index, TGrowthCurve *Curve) {
  // Nastavi krivku proporovnani pro prave otevreny soubor na <Curve>

  // Pokud uz nejaka krivka existuje, smazu ji
  if (CompareCurve[Index]) {
    delete CompareCurve[Index];
    CompareCurve[Index] = NULL;
  }

  // Pokud zadal NULL, chce teoretickou krivku zrusit (neporovnava se)
  if (!Curve) {
    return;
  }

  // Vytvorim novou a zkopiruju do ni vsechna data z <Curve>
  CompareCurve[Index] = new TGrowthCurve(*Curve);
}

// --------------------------------------------------------------------------
// Vraceni teoreticke krivky
// --------------------------------------------------------------------------

TGrowthCurve * TData::GetCompareCurve(TCompareCurveIndex Index) {
  // Vrati krivku pro porovnani pro prave otevreny soubor
  return CompareCurve[Index];
}

//---------------------------------------------------------------------------
__fastcall TData::TData(TComponent* Owner)
        : TDataModule(Owner)
{
  SearchOptions << loCaseInsensitive;

  // Vytvoreni stringu podle verze odberatele
  CreateVersionStrings();

  // Priprava dat

  // 13.12.2007: Zjistim, zda v adresari programu existuje INI soubor s rucne nastavenou cestou k datum
  AnsiString IniFileName = ChangeFileExt(Application->ExeName, ".ini");
  if (FileExists(IniFileName)) {
    // Adresar s daty nactu z INI souboru
    TIniFile *IniFile = new TIniFile(ChangeFileExt(Application->ExeName, ".ini"));
    AnsiString DataPath = "";
    try {
      DataPath = IniFile->ReadString(Version.ScaleTitle, "DataPath", "");
    } catch (...) {}
    delete IniFile;
    SpecialFolders = new TSpecialFolders(Version.ScaleTitle, DataPath);
  } else {
    // INI soubor neexistuje, adresar s daty zvolim automaticky
    SpecialFolders = new TSpecialFolders(Version.ScaleTitle);
  }

  // Seznam souboru, ktere se budou kopirovat
  TStringList *Files = new TStringList();
  Files->Add("Filters.db");     // Odpovida tabulkam, ktere se pridavaji do instalu (tj. ktere program automaticky nevytvari za behu)
  Files->Add("Numbers.db");
  Files->Add("Setup.db");
  Files->Add("SetupF.db");
  Files->Add("SetupS.db");
  Files->Add("Sms.db");
  Files->Add("Curves.db");
  Files->Add("ModbusScales.db");
  try {
    SpecialFolders->PrepareData(Files, Session, Database);
    delete Files;
  } catch(...) {
    delete Files;
    throw;              // Je treba opustit program, nepodarilo se pripravit datove soubory
  }

  Excel = new TExcel();

  for (int i = 0; i < _COMPARE_CURVE_COUNT; i++) {
    CompareCurve[i] = NULL;
  }
}
//---------------------------------------------------------------------------

void __fastcall TData::ZaznamyQueryCalcFields(TDataSet *DataSet)
{
  DataSet->FieldByName("SAMPLE")->AsInteger = DataSet->RecNo;
}
//---------------------------------------------------------------------------

void __fastcall TData::StatistikaQueryCalcFields(TDataSet *DataSet)
{
  // Vypoctu statistiku - plati pro StatistikaQuery i StatistikaVyberQuery
  TStatistic Stat;
  float Average, Sigma;

  // Vypocet potrebnych velicin
  Stat.XSuma  = DataSet->FieldByName("STAT_XSUM")->AsFloat;
  Stat.X2Suma = DataSet->FieldByName("STAT_X2SUM")->AsFloat;
  Stat.Count  = DataSet->FieldByName("STAT_COUNT")->AsInteger;
  Average = StatAverage(&Stat);
  Sigma   = StatSigma(&Stat);

  // Ulozeni do calculated sloupcu query
  DataSet->FieldByName("AVERAGE_WEIGHT")->AsFloat = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average)).ToDouble();  // V Keilu to delam pres int
  if (DataSet->FieldByName("DAY_NUMBER")->AsInteger == Soubor.MinDen) {
    // Prvni den je prirustek 0
    DataSet->FieldByName("GAIN")->AsFloat  = 0;
  } else {
    // Dalsi dny prirustek normalne spocitam
    DataSet->FieldByName("GAIN")->AsFloat  = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average) - DataSet->FieldByName("YESTERDAY_AVERAGE")->AsFloat).ToDouble();
  }
  DataSet->FieldByName("SIGMA")->AsFloat   = FormatFloat("0.000", (double)Sigma / 1000.0).ToDouble();
  DataSet->FieldByName("CV")->AsFloat      = FormatFloat("0.0", StatVariation(Average, Sigma)).ToDouble();
  if (DataSet->FieldByName("USE_REAL_UNI")->AsBoolean) {
    DataSet->FieldByName("UNI")->AsInteger = DataSet->FieldByName("REAL_UNI")->AsInteger;
  } else {
    DataSet->FieldByName("UNI")->AsInteger = FormatFloat("0", StatUniformity(Average, Sigma, Data->KonfiguraceTable->FieldByName("UNI_RANGE")->AsInteger)).ToInt();
  }
  DataSet->FieldByName("DATE_TIME_DATE")->AsDateTime = DataSet->FieldByName("DATE_TIME")->AsDateTime;

  // Porovnani s teoretickou rustovou krivkou
  if (CompareCurve[COMPARE_CURVE_MAIN_FORM]) {
    // Prave se porovnava
    DataSet->FieldByName("COMPARE")->AsFloat    = FormatFloat(FORMAT_HMOTNOSTI, CompareCurve[COMPARE_CURVE_MAIN_FORM]->CalculateWeight(DataSet->FieldByName("DAY_NUMBER")->AsInteger)).ToDouble();
    DataSet->FieldByName("DIFFERENCE")->AsFloat = DataSet->FieldByName("AVERAGE_WEIGHT")->AsFloat
                                                - DataSet->FieldByName("COMPARE")->AsFloat;
  }
}
//---------------------------------------------------------------------------

void __fastcall TData::DataModuleDestroy(TObject *Sender)
{
  delete Excel;

  // Pokud porovnaval, smazu krivku
  for (int i = 0; i < _COMPARE_CURVE_COUNT; i++) {
    if (CompareCurve[i]) {
      delete CompareCurve[i];
      CompareCurve[i] = NULL;
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TData::SmsOneGenderQueryCalcFields(TDataSet *DataSet)
{
  // Porovnani s teoretickou rustovou krivkou
  if (CompareCurve[COMPARE_CURVE_WEIGHING_FORM]) {
    // Prave se porovnava
    DataSet->FieldByName("COMPARE")->AsFloat    = FormatFloat(FORMAT_HMOTNOSTI, CompareCurve[COMPARE_CURVE_WEIGHING_FORM]->CalculateWeight(DataSet->FieldByName("DAY_NUMBER")->AsInteger)).ToDouble();
    DataSet->FieldByName("DIFFERENCE")->AsFloat = DataSet->FieldByName("AVERAGE_WEIGHT")->AsFloat
                                                - DataSet->FieldByName("COMPARE")->AsFloat;
  }
}
//---------------------------------------------------------------------------

