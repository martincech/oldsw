//******************************************************************************
//
//   File.cpp      Saving DB tables into a file
//   Version 1.0
//
//******************************************************************************

#ifndef FileH
#define FileH

#include <system.hpp>           // AnsiString
#include <dialogs.hpp>          // TSaveDialog

// --------------------------------------------------------------------------
// Globalni promenne
// --------------------------------------------------------------------------

// Nazvy tabulek dat
extern const String SOUBORDAT;
extern const String SOUBORSTATISTIKY;
extern const String SOUBORKONFIGURACE;
extern const String SOUBORHEJN;
extern const String SOUBORKALIBRACE;
extern const String SOUBORHLAVICKY;
extern const String SOUBORONLINE;

extern const String PREDPONA_POROVNANI;
extern const String PREDPONA_HLEDANI;

// --------------------------------------------------------------------------
// Funkce
// --------------------------------------------------------------------------

void FileSetSaveDialog(TSaveDialog *Dialog);
  // Nastavi dialog na <Dialog>

void FileCreateHeader();
  // Vytvori tabulku hlavicky

void FileCloseTables();
  // Zavre vsechny tabulky otevreneho souboru

void FileOpenTables();
  // Otevre vsechny tabulky

void FileSaveTablesToFile();
  // Pozaviram vsechny tabulky, pripadne zmeny ulozim - muze menit jen hlavicku

bool FileSaveDataToNewFile();
  // Ulozeni nove nactenych dat do noveho souboru

bool FileSaveFile(String Nazev);
  // Ulozim vsechny tabulky do 1 souboru s nazvem Nazev
  // Musi tedy existovat dane DB soubory

bool FileNewFile();
  // Vytvorim nove prazdne soubory DB
  // Pokud je nastaven flag ProStatistiku, vytvari se do jinych DB souboru, puvodni se nesmi prepsat
  // Ve statistice vytvari jiny soubor dat nez fce OtevriTable!!!

bool FileOpenFile(String Nazev, bool Porovnani, bool Find);
  // Otevru soubor Nazev a vytvorim z nej soubor DB s daty a hlavickou
  // Pokud je nastaven <Find>, oteviraji se soubory pri hledani souboru

void FileDecodeFileToTables(String Jmeno, bool Porovnani, bool Find);
  // Otevre zadany soubor, rozhazi jej na jednotlive tabulky a udela pripadne zmeny v DB
  // Pokud je nastaven <Find>, oteviraji se tabulky pri hledani souboru

#endif // FileH 
