//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Bat2.res");
USEFORM("Hlavni.cpp", HlavniForm);
USEFORM("DM.cpp", Data); /* TDataModule: File Type */
USERC("Bat2.rc");
USEFORM("OProgramu.cpp", OProgramuForm);
USEUNIT("MFlash.cpp");
USEUNIT("KConvert.cpp");
USEFORM("InformaceModul.cpp", InformaceModulForm);
USEFORM("ReadProgress.cpp", ReadProgressForm);
USEUNIT("c51\Stat.cpp");
USEFORM("NastaveniVah.cpp", NastaveniVahForm);
USEFORM("Input.cpp", InputForm);
USEFORM("RozsahDnu.cpp", RozsahDnuForm);
USEFORM("VzorkyReport.cpp", VzorkyReportForm); /* TQuickRep: File Type */
USEFORM("BargrafReport.cpp", BargrafReportForm); /* TQuickRep: File Type */
USEFORM("GrafReport.cpp", GrafReportForm); /* TQuickRep: File Type */
USEFORM("Jazyk.cpp", JazykForm);
USEUNIT("REINIT.PAS");
USEFORM("OnlineGrafReport.cpp", OnlineGrafReportForm); /* TQuickRep: File Type */
USEFORM("RozsahOnline.cpp", RozsahOnlineForm);
USEUNIT("Simulate.cpp");
USEFORM("SimulaceStart.cpp", SimulaceStartForm);
USEUNIT("c51\Average.cpp");
USEUNIT("c51\Histogr.cpp");
USEFORM("SimulaceVysledky.cpp", SimulaceVysledkyForm);
USEFORM("ReportReport.cpp", ReportReportForm); /* TQuickRep: File Type */
USEFORM("Weighing.cpp", WeighingForm);
USEFORM("WeighingRecord.cpp", WeighingRecordForm);
USEFORM("FindFile.cpp", FindFileForm);
USEFORM("WeighingOverwrite.cpp", WeighingOverwriteForm);
USEFORM("WeighingFilters.cpp", WeighingFiltersForm);
USEFORM("WeighingFilter.cpp", WeighingFilterForm);
USEUNIT("..\..\FTDI2\UsbSpi.cpp");
USEUNIT("..\..\FTDI2\AtFlash.cpp");
USEUNIT("..\..\FTDI2\At45dbxx.cpp");
USELIB("..\..\FTDI2\FTD2XX.lib");
USEUNIT("..\..\Library\Gsm\GSM.cpp");
USEUNIT("..\..\Library\Serial\UART\ComUart.cpp");
USEFORM("SelectModem.cpp", SelectModemForm);
USEFORM("Number.cpp", NumberForm);
USEFORM("NewNumber.cpp", NewNumberForm);
USEFORM("StatisticsDay.cpp", StatisticsDayForm);
USEUNIT("GsmLog.cpp");
USEUNIT("..\..\Library\SFolders\SFolders.cpp");
USEUNIT("Convert.cpp");
USEUNIT("Print.cpp");
USEUNIT("Print.cpp");
USEUNIT("Export.cpp");
USEFORM("..\..\Tmc\Program\Version.cpp", VersionForm);
USEFORM("RecordReport.cpp", RecordReportForm); /* TQuickRep: File Type */
USEUNIT("..\..\Library\Excel\Excel.cpp");
USEFORM("Curves.cpp", CurvesForm);
USEUNIT("Curve.cpp");
USEUNIT("CurveList.cpp");
USEUNIT("c51\Filter.cpp");
USEUNIT("File.cpp");
USEUNIT("CurveGrid.cpp");
USEUNIT("Flock.cpp");
USEUNIT("FlockList.cpp");
USEFORM("ModbusWait.cpp", ModbusWaitForm);
USEFORM("ModbusCompare.cpp", ModbusCompareForm);
USEFORM("ModbusEditScale.cpp", ModbusEditScaleForm);
USEFORM("ModbusErrors.cpp", ModbusErrorsForm);
USEUNIT("ModbusOnline.cpp");
USEFORM("ModbusOnlineF.cpp", ModbusOnlineForm);
USEUNIT("ModbusOnlineScale.cpp");
USEUNIT("ModbusOnlineThread.cpp");
USEFORM("ModbusPortSetup.cpp", ModbusPortSetupForm);
USEUNIT("ModbusScale.cpp");
USEUNIT("ModbusScaleList.cpp");
USEFORM("ModbusScales.cpp", ModbusScalesForm);
USEFORM("ModbusStartWeighing.cpp", ModbusStartWeighingForm);
USEUNIT("ModbusThread.cpp");
USEUNIT("ScaleSetup.cpp");
USEFORM("ScaleSetupFrm.cpp", ScaleSetupFrame); /* TFrame: File Type */
USEFORM("ModbusScaleSetupF.cpp", ModbusScaleSetupForm);
USEUNIT("..\..\Library\Modbus\MBSSid.cpp");
USEUNIT("..\..\Library\Modbus\MB.cpp");
USEUNIT("..\..\Library\Modbus\MBAscii.cpp");
USEUNIT("..\..\Library\Modbus\MBCom.cpp");
USEUNIT("..\..\Library\Modbus\MBPacket.cpp");
USEUNIT("..\..\Library\Modbus\MBRtu.cpp");
USEUNIT("..\..\Library\Modbus\MBSCoil.cpp");
USEUNIT("..\..\Library\Modbus\MBSDiag.cpp");
USEUNIT("..\..\Library\Modbus\MBSDiscr.cpp");
USEUNIT("..\..\Library\Modbus\MBSIdent.cpp");
USEUNIT("..\..\Library\Modbus\MBSInRegs.cpp");
USEUNIT("..\..\Library\Modbus\MBSRegs.cpp");
USEUNIT("MBcb.cpp");
USEUNIT("Dpi.cpp");
USEUNIT("..\..\Library\Xml\XmlExport.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TData), &Data);
                 Application->CreateForm(__classid(THlavniForm), &HlavniForm);
                 Application->CreateForm(__classid(TOProgramuForm), &OProgramuForm);
                 Application->CreateForm(__classid(TInformaceModulForm), &InformaceModulForm);
                 Application->CreateForm(__classid(TReadProgressForm), &ReadProgressForm);
                 Application->CreateForm(__classid(TNastaveniVahForm), &NastaveniVahForm);
                 Application->CreateForm(__classid(TInputForm), &InputForm);
                 Application->CreateForm(__classid(TRozsahDnuForm), &RozsahDnuForm);
                 Application->CreateForm(__classid(TVzorkyReportForm), &VzorkyReportForm);
                 Application->CreateForm(__classid(TBargrafReportForm), &BargrafReportForm);
                 Application->CreateForm(__classid(TGrafReportForm), &GrafReportForm);
                 Application->CreateForm(__classid(TOnlineGrafReportForm), &OnlineGrafReportForm);
                 Application->CreateForm(__classid(TRozsahOnlineForm), &RozsahOnlineForm);
                 Application->CreateForm(__classid(TSimulaceStartForm), &SimulaceStartForm);
                 Application->CreateForm(__classid(TSimulaceVysledkyForm), &SimulaceVysledkyForm);
                 Application->CreateForm(__classid(TReportReportForm), &ReportReportForm);
                 Application->CreateForm(__classid(TWeighingForm), &WeighingForm);
                 Application->CreateForm(__classid(TWeighingRecordForm), &WeighingRecordForm);
                 Application->CreateForm(__classid(TFindFileForm), &FindFileForm);
                 Application->CreateForm(__classid(TWeighingOverwriteForm), &WeighingOverwriteForm);
                 Application->CreateForm(__classid(TWeighingFiltersForm), &WeighingFiltersForm);
                 Application->CreateForm(__classid(TWeighingFilterForm), &WeighingFilterForm);
                 Application->CreateForm(__classid(TSelectModemForm), &SelectModemForm);
                 Application->CreateForm(__classid(TNumberForm), &NumberForm);
                 Application->CreateForm(__classid(TNewNumberForm), &NewNumberForm);
                 Application->CreateForm(__classid(TStatisticsDayForm), &StatisticsDayForm);
                 Application->CreateForm(__classid(TVersionForm), &VersionForm);
                 Application->CreateForm(__classid(TRecordReportForm), &RecordReportForm);
                 Application->CreateForm(__classid(TCurvesForm), &CurvesForm);
                 Application->CreateForm(__classid(TModbusWaitForm), &ModbusWaitForm);
                 Application->CreateForm(__classid(TModbusCompareForm), &ModbusCompareForm);
                 Application->CreateForm(__classid(TModbusEditScaleForm), &ModbusEditScaleForm);
                 Application->CreateForm(__classid(TModbusErrorsForm), &ModbusErrorsForm);
                 Application->CreateForm(__classid(TModbusOnlineForm), &ModbusOnlineForm);
                 Application->CreateForm(__classid(TModbusPortSetupForm), &ModbusPortSetupForm);
                 Application->CreateForm(__classid(TModbusScalesForm), &ModbusScalesForm);
                 Application->CreateForm(__classid(TModbusStartWeighingForm), &ModbusStartWeighingForm);
                 Application->CreateForm(__classid(TModbusScaleSetupForm), &ModbusScaleSetupForm);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
