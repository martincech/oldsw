//***************************************************************************
//
//    Curve.cpp - Growth curve
//    Version 1.0
//
//***************************************************************************

#ifndef GROWTHCURVE_H
#define GROWTHCURVE_H

#include <vector.h>
#include <system.hpp>           // AnsiString

//---------------------------------------------------------------------------
// Bod v krivce
//---------------------------------------------------------------------------

/*class TCurvePoint {
  friend int operator< (const TCurvePoint& Left, const TCurvePoint& Right) {
    return Left.Day < Right.Day;        // Tridim podle dne
  }

public:
  int    Day;                   // Cislo dne
  double Weight;                // Hmotnost

  TCurvePoint() {}

  TCurvePoint(const TCurvePoint& CurvePoint) {
    Day    = CurvePoint.Day;
    Weight = CurvePoint.Weight;
  }
};*/

struct TGrowthCurvePoint {
  int    Day;                   // Cislo dne
  double Weight;                // Hmotnost
};


//---------------------------------------------------------------------------
// Rustova krivka
//---------------------------------------------------------------------------

class TGrowthCurve {

private:
  int    MaxCount;              // Maximalni pocet prvku
  int    MinDay;                // Minimalni hodnota dne
  int    MaxDay;                // Maximalni hodnota dne
  double MinWeight;             // Minimalni hodnota hmotnosti
  double MaxWeight;             // Maximalni hodnota hmotnosti
  AnsiString Name;              // Jmeno krivky

  vector<TGrowthCurvePoint *> Curve;  // Rustova krivka

  typedef vector<TGrowthCurvePoint *>::iterator TCurveIterator;

  TCurveIterator GetIterator(int Index);
  bool CheckIndex(int Index);
  void Sort(void);

public:
  TGrowthCurve(AnsiString Name, int MaxCount, int MinDay, int MaxDay, double MinWeight, double MaxWeight);
  TGrowthCurve(const TGrowthCurve& SourceCurve);

  ~TGrowthCurve(void);

  bool CheckDay(int Day);
  bool CheckWeight(double Weight);

  void Clear(void);
  int  Count(void);
  bool Add(int Day, double Weight);
  void Delete(int Index);
  bool Edit(int Index, double Weight);

  int        GetDay(int Index);
  double     GetWeight(int Index);
  AnsiString GetName(void);
  void       SetName(AnsiString Name);

  double CalculateWeight(int Day);
};

#endif // GROWTHCURVE_H

