//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ModbusOnlineThread.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall Unit1::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------


void TModbusOnlineThread::SetModbusOnline(TModbusOnline *NewModbusOnline) {
  ModbusOnline = NewModbusOnline;
}

__fastcall TModbusOnlineThread::TModbusOnlineThread(bool CreateSuspended)
        : TThread(CreateSuspended)
{
  ModbusOnline = NULL;
}
//---------------------------------------------------------------------------
void __fastcall TModbusOnlineThread::Execute()
{
        //---- Place thread code here ----
  while (!Terminated) {
    if (!ModbusOnline) {
      continue;
    }
    try {
      if (ModbusOnline->Execute()) {
        // Nacetl jsem kompetni data z vahy
        NewData = true;           // Nastavim flag
        // Pockam, dokud nezpracuje data a pak az znovu rozjedu thread. Pokud bych rovnou pokracoval, mrsi se zobrazeni, protoze
        // hlavni program prekresluje, zatimco thread uz meni data (pokud je v seznamu jen 1 vaha)
        while (!Terminated && NewData);
      }
    } catch(...) {}  
  }
}
//---------------------------------------------------------------------------
