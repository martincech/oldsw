//***************************************************************************
//
//    FlockList.cpp - List of flocks (FlockClass)
//    Version 1.0
//
//***************************************************************************

#include "FlockList.h"


//---------------------------------------------------------------------------
// Destruktor
//---------------------------------------------------------------------------

TFlockList::~TFlockList() {
  Clear();
}

//---------------------------------------------------------------------------
// Najeti na urcite hejno
//---------------------------------------------------------------------------

vector<TFlockClass *>::iterator TFlockList::GetIterator(int Index) {
  // Vrati iterator na hejno s indexem <Index>. Neosetruje prazdny seznam.
  TFlockListIterator Iterator;

  Iterator = FlockList.begin();
  while (Index) {
    Iterator++;
    Index--;
  }
  return Iterator;
}

//---------------------------------------------------------------------------
// Kontrola indexu
//---------------------------------------------------------------------------

bool TFlockList::CheckIndex(int Index) {
  // Pokud je index <Index> platny v ramci seznamu hejn, vrati true
  return (bool)(Index >= 0 && (unsigned)Index < FlockList.size());
}

//---------------------------------------------------------------------------
// Trideni
//---------------------------------------------------------------------------

void TFlockList::Sort() {
  // Setridi seznam podle cisla hejna
  TFlockListIterator Iterator1, Iterator2;
  TFlockClass *Flock;

  for (Iterator1 = FlockList.begin(); Iterator1 < FlockList.end() - 1; Iterator1++) {
    for (Iterator2 = Iterator1 + 1; Iterator2 < FlockList.end(); Iterator2++) {
      if ((*Iterator1)->Number > (*Iterator2)->Number) {
        // Prohodim prvky
        Flock = (*Iterator1);
        (*Iterator1) = (*Iterator2);
        (*Iterator2) = Flock;
      }
    }
  }
}

//---------------------------------------------------------------------------
// Smazani vsech hejn
//---------------------------------------------------------------------------

void TFlockList::Clear() {
  // Smazu vsechny hejna
  for (TFlockListIterator Iterator = FlockList.begin(); Iterator != FlockList.end(); Iterator++) {
    delete (*Iterator);        // Smazu vsechny polozky
  }
  // Smazu seznam
  FlockList.clear();
}

//---------------------------------------------------------------------------
// Pocet hejn v seznamu
//---------------------------------------------------------------------------

int TFlockList::Count() {
  return FlockList.size();
}

//---------------------------------------------------------------------------
// Plny seznam
//---------------------------------------------------------------------------

bool TFlockList::IsFull() {
  return (FlockList.size() >= FLOCKS_MAX_COUNT);
}

//---------------------------------------------------------------------------
// Nalezeni indexu
//---------------------------------------------------------------------------

int TFlockList::GetIndex(int Number) {
  // Vrati index hejna s cislem <Number> nebo -1, pokud hejno neexistuje
  TFlockListIterator Iterator;
  int Index = 0;

  for (Iterator = FlockList.begin(); Iterator != FlockList.end(); Iterator++) {
    if ((*Iterator)->Number == Number) {
      return Index;             // Zadane cislo uz v seznamu existuje
    }
    Index++;
  }
  return -1;                    // Neexistuje
}

//---------------------------------------------------------------------------
// Kontrola cisla hejna
//---------------------------------------------------------------------------

bool TFlockList::Exists(int Number) {
  // Kontrola, zda uz hejno v senamu neexistuje
  return (GetIndex(Number) >= 0);
}

//---------------------------------------------------------------------------
// Pridani noveho hejna pomoci kopirovani
//---------------------------------------------------------------------------

bool TFlockList::Add(TFlockClass *Flock) {
  // Prida do seznamu hejno <Flock>

  // Overim zdrojove hejno
  if (!Flock) {
    return false;
  }

  // Kontrola existence cisla
  if (Exists(Flock->Number)) {
    return false;               // Cislo uz v seznamu existuje
  }

  // Pridam hejno
  FlockList.push_back(Flock);   // Pridam ukazatel do kontejneru

  // Na zaver setridim
  Sort();

  return true;
}

//---------------------------------------------------------------------------
// Smazani hejna
//---------------------------------------------------------------------------

bool TFlockList::Delete(int Index) {
  // Smaze hejno na pozici <Index>
  if (!CheckIndex(Index)) {
    return false;
  }
  FlockList.erase(GetIterator(Index));
  return true;
}

//---------------------------------------------------------------------------
// Vraceni ukazatele na hejno
//---------------------------------------------------------------------------

TFlockClass * TFlockList::GetFlock(int Index) {
  // Vrati ukazatel na hejno na pozici <Index>
  if (!CheckIndex(Index)) {
    return NULL;
  }
  return (*GetIterator(Index));
}

