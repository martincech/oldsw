object InputForm: TInputForm
  Left = 351
  Top = 578
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'InputForm'
  ClientHeight = 120
  ClientWidth = 280
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label: TLabel
    Left = 16
    Top = 24
    Width = 26
    Height = 13
    Caption = 'Label'
  end
  object Edit: TEdit
    Left = 16
    Top = 40
    Width = 249
    Height = 21
    TabOrder = 0
  end
  object Button1: TButton
    Left = 96
    Top = 80
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 190
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
