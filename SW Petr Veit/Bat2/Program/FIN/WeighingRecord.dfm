�
 TWEIGHINGRECORDFORM 0k  TPF0TWeighingRecordFormWeighingRecordFormLeftKTop@BorderIconsbiSystemMenu BorderStylebsSingleCaptionPunnitusClientHeight7ClientWidthSColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TButtonButton1Left�TopWidthKHeightCaptionTallennaDefault	TabOrderOnClickButton1Click  TButtonButton2Left�TopWidthKHeightCancel	CaptionPeruutaModalResultTabOrder  	TGroupBox	GroupBox1LeftTopWidth� HeightaCaptionPunnituksen tiedotTabOrder  TLabelLabel16LeftTopWidth!HeightCaptionLis�tty:  TLabelLabel17LeftTop0WidthPHeightCaptionViimeisin muutos:  TLabelLabel18LeftTopHWidthAHeightCaptionGSM-numero:  TLabelInsertLabelLeftpTopWidthnHeightAutoSizeCaptionInsertLabel  TLabel	EditLabelLeftpTop0WidthnHeightAutoSizeCaption	EditLabel  TLabelGSMLabelLeftpTopHWidthnHeightAutoSizeCaptionGSMLabel   	TGroupBox	GroupBox2LeftTop� Width� Height� CaptionVaakaTabOrder TLabelLabel1LeftTopWidth"HeightCaptionVaaka:  TLabelLabel2LeftTop0WidthHeightCaptionP�iv�:  TLabelLabel3LeftTopHWidthHeightCaptionPvm:  TLabelLabel19LeftTop`WidthHeightCaptionHuom:  TEdit
ScalesEditLeftxTopWidthYHeight	MaxLengthTabOrder 
OnKeyPressScalesEditKeyPress  TEditDayEditLeftxTop-WidthYHeight	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TDateTimePickerDateTimePickerLeftxTopEWidthYHeightCalAlignmentdtaLeftDateUn!�1���@TimeUn!�1���@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder  TEditNoteEditLeftxTop]WidthYHeight	MaxLengthTabOrder   	TGroupBox	GroupBox3Left TopWidthAHeight� CaptionTilastotiedotTabOrder 	TCheckBoxBothGendersCheckBoxLeftTopWidth� HeightCaptionErottele sukupuoletTabOrder OnClickBothGendersCheckBoxClick  	TGroupBoxFemalesGroupBoxLeftTop0Width� Height� CaptionNaaraat / kaikkiTabOrder TLabelLabel4LeftTopWidth8HeightCaption
Lukum��r�:  TLabelLabel5LeftTop0Width7HeightCaptionKeskipaino:  TLabelLabel6LeftTopHWidth!HeightCaptionKasvu:  TLabelLabel7LeftTop`Width@HeightCaptionKeskihajonta:  TLabelLabel8LeftTopxWidthHeightCaptionCv:  TLabelLabel9LeftTop� WidthHeightCaptionUni:  TEditFemalesCountEditLeftXTopWidth!Height	MaxLengthTabOrder 
OnKeyPressScalesEditKeyPress  TEditFemalesAverageEditLeftXTop-Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditFemalesGainEditLeftXTopEWidth!HeightTabOrder
OnKeyPressFemalesGainEditKeyPress  TEditFemalesSigmaEditLeftXTop]Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditFemalesCvEditLeftXTopuWidth!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TEditFemalesUniEditLeftXTop� Width!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress   	TGroupBoxMalesGroupBoxLeft� Top0Width� Height� CaptionUroksetTabOrder TLabelLabel10LeftTopWidth8HeightCaption
Lukum��r�:  TLabelLabel11LeftTop0Width7HeightCaptionKeskipaino:  TLabelLabel12LeftTopHWidth!HeightCaptionKasvu:  TLabelLabel13LeftTop`Width@HeightCaptionKeskihajonta:  TLabelLabel14LeftTopxWidthHeightCaptionCv:  TLabelLabel15LeftTop� WidthHeightCaptionUni:  TEditMalesCountEditLeftXTopWidth!Height	MaxLengthTabOrder 
OnKeyPressScalesEditKeyPress  TEditMalesAverageEditLeftXTop-Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditMalesGainEditLeftXTopEWidth!HeightTabOrder
OnKeyPressFemalesGainEditKeyPress  TEditMalesSigmaEditLeftXTop]Width!HeightTabOrder
OnKeyPressFemalesAverageEditKeyPress  TEditMalesCvEditLeftXTopuWidth!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress  TEditMalesUniEditLeftXTop� Width!Height	MaxLengthTabOrder
OnKeyPressScalesEditKeyPress    TButtonPrintButtonLeftTopWidthKHeightCaptionTulostaTabOrderOnClickPrintButtonClick   