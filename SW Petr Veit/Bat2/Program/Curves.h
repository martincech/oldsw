//---------------------------------------------------------------------------

#ifndef CurvesH
#define CurvesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>

#include "CurveList.h"
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TCurvesForm : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *CurvesGroupBox;
        TListBox *CurvesListBox;
        TButton *NewCurveButton;
        TButton *DeleteCurveButton;
        TGroupBox *DefinitionGroupBox;
        TStringGrid *CurveStringGrid;
        TPanel *DecriptionPanel;
        TButton *RenameCurveButton;
        TButton *CopyCurveButton;
        TButton *SaveCurveButton;
        TButton *OkButton;
        TLabel *Label1;
        TButton *CancelButton;
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall NewCurveButtonClick(TObject *Sender);
        void __fastcall DeleteCurveButtonClick(TObject *Sender);
        void __fastcall RenameCurveButtonClick(TObject *Sender);
        void __fastcall SaveCurveButtonClick(TObject *Sender);
        void __fastcall CurvesListBoxClick(TObject *Sender);
        void __fastcall CopyCurveButtonClick(TObject *Sender);
        void __fastcall OkButtonClick(TObject *Sender);
private:	// User declarations

        TCurveList *CurveList;

        void DisplayCurveList();
        void ShowCurve(int Index);
        bool SaveCurve(int Index);

public:		// User declarations
        __fastcall TCurvesForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCurvesForm *CurvesForm;
//---------------------------------------------------------------------------
#endif
