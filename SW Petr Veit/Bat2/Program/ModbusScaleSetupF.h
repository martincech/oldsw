//---------------------------------------------------------------------------

#ifndef ModbusScaleSetupFH
#define ModbusScaleSetupFH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "ScaleSetupFrm.h"
//---------------------------------------------------------------------------
class TModbusScaleSetupForm : public TForm
{
__published:	// IDE-managed Components
        TScaleSetupFrame *ScaleSetupFrame1;
        TButton *OkButton;
        TButton *CancelButton;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall CancelButtonClick(TObject *Sender);
        void __fastcall OkButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TModbusScaleSetupForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusScaleSetupForm *ModbusScaleSetupForm;
//---------------------------------------------------------------------------
#endif
