//---------------------------------------------------------------------------

#ifndef OProgramuH
#define OProgramuH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
#include <ComCtrls.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TOProgramuForm : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TLabel *VerzeLabel;
        TMemo *CompanyMemo;
        TLabel *NameLabel;
        TBevel *Bevel1;
        TImage *LogoImage;
private:	// User declarations
public:		// User declarations
        __fastcall TOProgramuForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TOProgramuForm *OProgramuForm;
//---------------------------------------------------------------------------
#endif
