//******************************************************************************
//
//   MBCb.c       Modbus callbacks
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "..\..\Intel51\Vym\Inc\MBCb.h"
#include "..\..\Intel51\Vym\Inc\MBPacket.h"
#include "..\..\Intel51\Vym\Inc\MBPdu.h"

#include "MBCbPar.h"

//---------------------------------------------------------------------------
// Definice
//---------------------------------------------------------------------------

// Komunikacni protokol - lisi se od modbusu, duplicitni s Bat2.h
typedef enum {
  RS485_PROTOCOL_MODBUS_RTU,
  RS485_PROTOCOL_MODBUS_ASCII,
  _RS485_PROTOCOL_COUNT
} TProtocol;

//---------------------------------------------------------------------------
// Globalni parametry
//---------------------------------------------------------------------------

TMbcbConnection  MbcbConnection;
TMbcbRegisters   MbcbRegisters;
byte             MbcbError;
byte             MbcbException;
byte             MbcbFlockNumber;
TMbcbCounters    MbcbCounters;
TMbcbModbusSetup MbcbModbusSetup;

//******************************************************************************
// Com Name
//******************************************************************************

TString MBCGetComName( void)
// Vrati nazev COMu
{
   return( "COM" + AnsiString(MbcbConnection.Port));
} // MBCGetComName

//******************************************************************************
// Baud
//******************************************************************************

word MBCGetBaud( void)
// Vrati baudovou rychlost
{
   if (MbcbModbusSetup.Speed == 115200) {
     return(COM_115200Bd);              // Pri 115kBd vraci specialni konstantu, ktera se vejde do wordu
   }
   return( MbcbModbusSetup.Speed);
} // MBCGetBaud

//-----------------------------------------------------------------------------
// Parity
//-----------------------------------------------------------------------------

byte MBCGetParity( void)
// Vrati paritu (viz MBCom.h)
{
   //>>> nacti konfiguraci
   return( MbcbModbusSetup.Parity);
} // MBCGetParity

//-----------------------------------------------------------------------------
// Celkovy timeout
//-----------------------------------------------------------------------------

word MBCGetTotalTimeout( void)
// Vrati timeout odpovedi v [ms]
{
   //>>> nacti konfiguraci
   return(MbcbModbusSetup.ReplyDelay);
} // MBCGetTimeout

//-----------------------------------------------------------------------------
// Meziznakovy timeout
//-----------------------------------------------------------------------------

word MBCGetIntercharacterTimeout( void)
// Vrati meziznakovy timeout v [ms]
{
   //>>> nacti konfiguraci
   return(MbcbModbusSetup.SilentInterval);
} // MBCGetTimeout

//-----------------------------------------------------------------------------
// Mode
//-----------------------------------------------------------------------------

byte MBCGetMode( void)
// Vrati mod komunikace (viz MBPacket.h)
{
   //>>> nacti konfiguraci
   if (MbcbModbusSetup.Protocol == RS485_PROTOCOL_MODBUS_RTU) {
     return( MB_RTU_MODE);
   } else {
     return( MB_ASCII_MODE);
   }
} // MBCGetMode

//-----------------------------------------------------------------------------
// Address
//-----------------------------------------------------------------------------

byte MBCGetSlaveAddress( void)
// Vrati adresu obsluhovaneho zarizeni
{
   //>>> nacti konfiguraci
   return(MbcbConnection.Address);
} // MBCGetOwnAddress

//******************************************************************************
// Error
//******************************************************************************

void MBCError( byte Code)
// Chybove hlaseni
{
  MbcbError = Code;
/*   TString Message;
   switch( Code){
      case MBE_OK :
         Message = "OK";
         break;
      case MBE_PARITY :
         Message = "Parity error";
         break;
      case MBE_OVERRUN :
         Message = "Overrun error";
         break;
      case MBE_TIMEOUT :
         Message = "Timeout";
         break;
      case MBE_FRAME :
         Message = "Frame error";
         break;
      case MBE_WRONG_ADDRESS :
         Message = "Wrong reply address";
         break;
      case MBE_WRONG_FUNCTION :
         Message = "Wrong reply function";
         break;
      case MBE_WRONG_ITEM :
         Message = "Reply data doesn't match send data";
         break;
      case MBE_IMPLEMENTATION :
         Message = "Implementation error";
         break;
      IDEFAULT
   }
   ShowMessage(Message);*/
} // MBCError

//******************************************************************************
// Exception
//******************************************************************************

void MBCException( byte Code)
// Nastala vyjimka
{
  MbcbException = Code;
/*   TString Message;
   Message.printf( "Exception %x", Code);
   ShowMessage(Message.c_str());*/
} // MBCException

//******************************************************************************
// Read Discrete
//******************************************************************************

void MBCReadDiscrete( word Address, word Count, byte *Data)
// Precteny binarni hodnoty
{
} // MBCReadDiscrete

//******************************************************************************
// Read Coils
//******************************************************************************

void MBCReadCoils( word Address, word Count, byte *Data)
// Precteny binarni hodnoty
{
} // MBCReadCoils

//******************************************************************************
// Read Input Registers
//******************************************************************************

void MBCReadInputRegisters( word Address, word Count, word *Data)
// Precteny 16bit hodnoty
{
} // MBCReadInputRegisters

//******************************************************************************
// Read Registers
//******************************************************************************

void MBCReadRegisters( word Address, word Count, word *Data)
// Precteny 16bit hodnoty
{
  // Zkopiruju data do globalni struktury
  MbcbRegisters.Address = Address;
  MbcbRegisters.Count   = Count;
  for (int i = 0; i < Count; i++) {
    MbcbRegisters.Data[i] = MBGetWord(Data[i]);
  }
} // MBCReadRegisters

//******************************************************************************
// Diagnostic Data
//******************************************************************************

void MBCDiagnosticData( word Data)
// Data vracena zarizenim
{
/*   TString Message;
   Message.printf( "Diagnostic data=%04hXh", Data);
   MainForm->SetStatus( Message);*/
} // MBCDiagnosticData

//******************************************************************************
// Diagnostic Counter
//******************************************************************************

void MBCDiagnosticCounter( word CounterCode, word CounterData)
// Vrati hodnotu diagnostickeho citace <CounterCode>, viz MBD_... kostanty
// hodnota citace je <CounterData>
{
  switch (CounterCode) {
    case MBD_TOTAL_COUNT :
      MbcbCounters.Total = CounterData;
      break;
    case MBD_ERROR_COUNT :
      MbcbCounters.Error = CounterData;
      break;
    case MBD_EXCEPTION_COUNT :
      MbcbCounters.Exception = CounterData;
      break;
    case MBD_MESSAGE_COUNT :
      MbcbCounters.Message = CounterData;
      break;
    case MBD_NO_RESPONSE_COUNT :
      MbcbCounters.Response = CounterData;
      break;
    case MBD_NAK_COUNT :
      MbcbCounters.Nak = CounterData;
      break;
    case MBD_BUSY_COUNT :
      MbcbCounters.Busy = CounterData;
      break;
    case MBD_OVERRUN_COUNT :
      MbcbCounters.Overrun = CounterData;
      break;
   }
} // MBDDiagnosticCounter

//******************************************************************************
// Slave Id
//******************************************************************************

void MBCSlaveId( word SlaveId, byte RunIndicator, byte *Data)
// Identifikace slave
{
/*   Data[ SID_ADDITIONAL_DATA_SIZE] = 0; // sorry, ComBuffer je dost dlouhy
   TString Message;
   Message.printf( "ID=%04hXh Run=%c Data=%s", SlaveId, RunIndicator ? 'Y' : 'N', Data);
   MainForm->SetStatus( Message);*/
} // MBCSlaveId

//******************************************************************************
// Vyrobce
//******************************************************************************

void MBCVendorId( byte *Data, byte Size)
// Identifikace vyrobce
{
/*   char Text[ 80];
   memcpy( Text, Data, Size);
   Text[ Size] = 0;
   MainForm->EdtVendor->Text = Text;*/
} // MBCVendorId

//******************************************************************************
// Produkt
//******************************************************************************

void MBCProductCode( byte *Data, byte Size)
// Identifikace produktu
{
/*   char Text[ 80];
   memcpy( Text, Data, Size);
   Text[ Size] = 0;
   MainForm->EdtProduct->Text = Text;*/
} // MBCProductCode

//******************************************************************************
// Verze
//******************************************************************************

void MBCRevisionString( byte *Data, byte Size)
// Cislo verze
{
/*   char Text[ 80];
   memcpy( Text, Data, Size);
   Text[ Size] = 0;
   MainForm->EdtRevision->Text = Text;*/
} // MBCRevision

