//***************************************************************************
//
//   File.cpp      Saving DB tables into a file 
//   Version 1.0
//
//***************************************************************************

#include "File.h"
#include "DM.h"

// --------------------------------------------------------------------------
// Globalni promenne
// --------------------------------------------------------------------------

// Nazvy tabulek dat
const String SOUBORDAT         = "Records.db";
const String SOUBORSTATISTIKY  = "Stat.db";
const String SOUBORKONFIGURACE = "Config.db";
const String SOUBORHEJN        = "Flocks.db";
const String SOUBORKALIBRACE   = "Cal.db";
const String SOUBORHLAVICKY    = "Header.db";
const String SOUBORONLINE      = "Online.db";

const String PREDPONA_POROVNANI = "C";        // Prida se pred nazev souboru
const String PREDPONA_HLEDANI   = "F";        // Prida se pred nazev souboru

// --------------------------------------------------------------------------
// Lokalni promenne
// --------------------------------------------------------------------------

static TSaveDialog *FileSaveDialog = NULL;

// --------------------------------------------------------------------------
// Nastaveni dialogu
// --------------------------------------------------------------------------

void FileSetSaveDialog(TSaveDialog *Dialog) {
  // Nastavi dialog na <Dialog>
  FileSaveDialog = Dialog;
}

// --------------------------------------------------------------------------
// Vytvoreni hlavicky
// --------------------------------------------------------------------------

void FileCreateHeader() {
  // Vytvori tabulku hlavicky
  Data->HlavickaTable->Close();
  Data->HlavickaTable->EmptyTable();
  Data->HlavickaTable->Open();
  Data->HlavickaTable->Append();
  Data->HlavickaTable->Edit();
  Data->HlavickaTable->FieldByName("VERSION_SW")->AsString  = String(VERSION_SW) + "." + String(VERSION_BUILD);
  Data->HlavickaTable->FieldByName("DATE_TIME")->AsDateTime = Now();
  if (Data->KonfiguraceTable->FieldByName("START_ONLINE")->AsBoolean) {
    Data->HlavickaTable->FieldByName("DATA_TYPE")->AsInteger = DATA_ONLINE;
  } else {
    Data->HlavickaTable->FieldByName("DATA_TYPE")->AsInteger = DATA_VAZENI;
  }
  Data->HlavickaTable->Post();
}

// --------------------------------------------------------------------------
// Zavreni vsech a otevreni vsech datovych tabulek
// --------------------------------------------------------------------------

void FileCloseTables() {
  // Zavre vsechny tabulky otevreneho souboru
  Data->ZaznamyQuery->Close();
  Data->ZaznamyTable->Close();
  Data->StatistikaTable->Close();
  Data->StatistikaQuery->Close();
  Data->KonfiguraceTable->Close();
  Data->HejnaTable->Close();
  Data->KalibraceTable->Close();
  Data->OnlineQuery->Close();
  Data->OnlineTable->Close();
  // V informacich mohl zadat poznamku => pripadne ulozim
  if (Data->HlavickaTable->State==dsEdit || Data->HlavickaTable->State==dsInsert) Data->HlavickaTable->Post();
  Data->HlavickaTable->Close();
}

void FileOpenTables() {
  // Otevre vsechny tabulky
  // Smazu filtry
  Data->ZaznamyTable->Filter     = "";
  Data->StatistikaTable->Filter  = "";
  Data->KonfiguraceTable->Filter = "";
  Data->HejnaTable->Filter       = "";
  Data->KalibraceTable->Filter   = "";
  Data->HlavickaTable->Filter    = "";
  Data->OnlineTable->Filter      = "";
  // Otevru
  Data->ZaznamyTable->Open();
  Data->StatistikaTable->Open();
  Data->KonfiguraceTable->Open();
  Data->HejnaTable->Open();
  Data->KalibraceTable->Open();
  Data->OnlineTable->Open();
  Data->HlavickaTable->Open();
}

// --------------------------------------------------------------------------
// Ulozeni dat do souboru
// --------------------------------------------------------------------------

void FileSaveTablesToFile() {
  // Pozaviram vsechny tabulky, pripadne zmeny ulozim - muze menit jen hlavicku
  FileCloseTables();
  if (!FileSaveFile(Data->Soubor.Jmeno)) {
    MessageBox(NULL,LoadStr(CHYBA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
  }
  FileOpenTables();
}

// --------------------------------------------------------------------------
// Ulozeni nove nactenych dat do noveho souboru
// --------------------------------------------------------------------------

bool FileSaveDataToNewFile() {
  // Ulozeni nove nactenych dat do noveho souboru
  if (!FileSaveDialog->Execute()) return false;
  if (FileSaveDialog->FileName.IsEmpty()) return false;   // Nevybral nic
  Data->Soubor.Jmeno = ChangeFileExt(FileSaveDialog->FileName, "." + Data->Version.FileExtension);
  FileSaveTablesToFile();
  return true;
}

// --------------------------------------------------------------------------
// Ulozeni tabulek do souboru
// --------------------------------------------------------------------------

// Zapis a ulozeni velikosti souboru
#define ZapisVelikostSouboru(Soubor, Delka);                                                    \
  if (FindFirst(Data->SpecialFolders->Working + Soubor, faAnyFile, SearchRec) != 0) throw 1;    \
  Delka = SearchRec.Size;                                                                       \
  if (FileWrite(ZapisovanySoubor, (char*)&Delka, 4) < 0) throw 1;                               \


// Zapis souboru
#define ZapisSoubor(Soubor, Delka)                                            \
  Bajty = new char[Delka + 1];                                                \
  CtenySoubor = FileOpen(Data->SpecialFolders->Working + Soubor, fmOpenRead); \
  if (CtenySoubor < 0) throw 1;                                               \
  if (FileRead(CtenySoubor, Bajty, Delka) < 0) throw 1;                       \
  if (FileWrite(ZapisovanySoubor, Bajty, Delka) < 0) throw 1;                 \
  FileClose(CtenySoubor);                                                     \
  delete [] Bajty;                                                            \
  Bajty = NULL;                                                               \

bool FileSaveFile(String Nazev) {
  // Ulozim vsechny tabulky do 1 souboru s nazvem Nazev
  // Musi tedy existovat dane DB soubory

  // Zkontroluju, zda existuji DB soubory, ze kterych se soubor vytvari
  if (!FileExists(Data->SpecialFolders->Working + SOUBORDAT)
   || !FileExists(Data->SpecialFolders->Working + SOUBORSTATISTIKY)
   || !FileExists(Data->SpecialFolders->Working + SOUBORKONFIGURACE)
   || !FileExists(Data->SpecialFolders->Working + SOUBORHEJN)
   || !FileExists(Data->SpecialFolders->Working + SOUBORKALIBRACE)
   || !FileExists(Data->SpecialFolders->Working + SOUBORHLAVICKY)
   || !FileExists(Data->SpecialFolders->Working + SOUBORONLINE)) return false;   // Jeden ze souboru neexistuje

  // Vytvorim soubor
  int ZapisovanySoubor;
  int CtenySoubor;
  char Jmeno[31];
  int DelkaSouboruDat, DelkaSouboruStatistiky, DelkaSouboruKonfigurace, DelkaSouboruHejn, DelkaSouboruKalibrace, DelkaSouboruHlavicky, DelkaSouboruOnline;
  TSearchRec SearchRec;
  char *Bajty = NULL;
  try {
    Nazev = ChangeFileExt(Nazev, "." + Data->Version.FileExtension);
    ZapisovanySoubor = FileCreate(Nazev);
    if (ZapisovanySoubor < 0) throw 1;

    // Zapisu cislo verze programu, ktery tento soubor uklada
    if (FileWrite(ZapisovanySoubor, "Bat2 ", 5)<0) throw 1;   // Hlavicka
    // Verze doplnena nulami zprava
    strcpy(Jmeno, String(String(VERSION_SW) + "." + String(VERSION_BUILD)).c_str());
    for (int i = strlen(Jmeno); i < 10; i++) Jmeno[i] = 0;  // Doplnim na 10 znaku
    if (FileWrite(ZapisovanySoubor, Jmeno, 10) < 0) throw 1;

    // Zapisu velikosti jednotlivych souboru
    ZapisVelikostSouboru(SOUBORDAT, DelkaSouboruDat);
    ZapisVelikostSouboru(SOUBORSTATISTIKY, DelkaSouboruStatistiky);
    ZapisVelikostSouboru(SOUBORKONFIGURACE, DelkaSouboruKonfigurace);
    ZapisVelikostSouboru(SOUBORHEJN, DelkaSouboruHejn);
    ZapisVelikostSouboru(SOUBORKALIBRACE, DelkaSouboruKalibrace);
    ZapisVelikostSouboru(SOUBORHLAVICKY, DelkaSouboruHlavicky);
    ZapisVelikostSouboru(SOUBORONLINE, DelkaSouboruOnline);
    // Zapisu jednotlive soubory
    ZapisSoubor(SOUBORDAT, DelkaSouboruDat);
    ZapisSoubor(SOUBORSTATISTIKY, DelkaSouboruStatistiky);
    ZapisSoubor(SOUBORKONFIGURACE, DelkaSouboruKonfigurace);
    ZapisSoubor(SOUBORHEJN, DelkaSouboruHejn);
    ZapisSoubor(SOUBORKALIBRACE, DelkaSouboruKalibrace);
    ZapisSoubor(SOUBORHLAVICKY, DelkaSouboruHlavicky);
    ZapisSoubor(SOUBORONLINE, DelkaSouboruOnline);
    // Zavru soubor
    FileClose(ZapisovanySoubor);
    return true;
  } catch(...) {
    if (Bajty != NULL) delete Bajty;
    FileClose(ZapisovanySoubor);
    return false;
  }//catch
}

// --------------------------------------------------------------------------
// Novy soubor
// --------------------------------------------------------------------------

static void PridejSloupec(TTable *Table, String Nazev, TFieldType Typ, int Delka = 0) {
  // Pridani sloupce do definice tabulky
  TFieldDef *FieldDef;

  FieldDef           = Table->FieldDefs->AddFieldDef();
  FieldDef->Name     = Nazev;
  FieldDef->DataType = Typ;
  if (Delka > 0) {
    FieldDef->Size   = Delka;
  }
}

bool FileNewFile() {
  // Vytvorim nove prazdne soubory DB
  // Pokud je nastaven flag ProStatistiku, vytvari se do jinych DB souboru, puvodni se nesmi prepsat
  // Ve statistice vytvari jiny soubro dat nez fce OtevriTable!!!
  String SouborDat, SouborStatistiky, SouborKonfigurace, SouborHejn, SouborKalibrace, SouborHlavicky, SouborOnline;

  SouborDat         = Data->SpecialFolders->Working + SOUBORDAT;
  SouborStatistiky  = Data->SpecialFolders->Working + SOUBORSTATISTIKY;
  SouborKonfigurace = Data->SpecialFolders->Working + SOUBORKONFIGURACE;
  SouborHejn        = Data->SpecialFolders->Working + SOUBORHEJN;
  SouborKalibrace   = Data->SpecialFolders->Working + SOUBORKALIBRACE;
  SouborHlavicky    = Data->SpecialFolders->Working + SOUBORHLAVICKY;
  SouborOnline      = Data->SpecialFolders->Working + SOUBORONLINE;  

  // Smazu oba DB soubory (budu je vytvaret znova)
  if (FileExists(SouborDat) && !DeleteFile(SouborDat)) return false;   // Jeden ze souboru nejde smazat
  if (FileExists(SouborStatistiky) && !DeleteFile(SouborStatistiky)) return false;   // Jeden ze souboru nejde smazat
  if (FileExists(SouborKonfigurace) && !DeleteFile(SouborKonfigurace)) return false;   // Jeden ze souboru nejde smazat
  if (FileExists(SouborHejn) && !DeleteFile(SouborHejn)) return false;   // Jeden ze souboru nejde smazat
  if (FileExists(SouborKalibrace) && !DeleteFile(SouborKalibrace)) return false;   // Jeden ze souboru nejde smazat
  if (FileExists(SouborHlavicky) && !DeleteFile(SouborHlavicky)) return false;   // Jeden ze souboru nejde smazat
  if (FileExists(SouborOnline) && !DeleteFile(SouborOnline)) return false;   // Jeden ze souboru nejde smazat

  // Vytvorim DB
  try {
    // --------- Tabulka zaznamu loggeru
    Data->ZaznamyTable->FieldDefs->Clear();
    PridejSloupec(Data->ZaznamyTable, "DAY_NUMBER", ftSmallint);
    PridejSloupec(Data->ZaznamyTable, "TIME_HOUR", ftSmallint);
    PridejSloupec(Data->ZaznamyTable, "WEIGHT", ftFloat);
    PridejSloupec(Data->ZaznamyTable, "GENDER", ftBoolean);
    Data->ZaznamyTable->CreateTable();

    // --------- Tabulka statistiky
    Data->StatistikaTable->FieldDefs->Clear();
    PridejSloupec(Data->StatistikaTable, "DAY_NUMBER", ftSmallint);
    PridejSloupec(Data->StatistikaTable, "DATE_TIME", ftDateTime);
    PridejSloupec(Data->StatistikaTable, "FEMALE_STAT_XSUM", ftFloat);
    PridejSloupec(Data->StatistikaTable, "FEMALE_STAT_X2SUM", ftFloat);
    PridejSloupec(Data->StatistikaTable, "FEMALE_STAT_COUNT", ftInteger);
    PridejSloupec(Data->StatistikaTable, "MALE_STAT_XSUM", ftFloat);
    PridejSloupec(Data->StatistikaTable, "MALE_STAT_X2SUM", ftFloat);
    PridejSloupec(Data->StatistikaTable, "MALE_STAT_COUNT", ftInteger);
    for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
      PridejSloupec(Data->StatistikaTable, "FEMALE_HIST_VALUE" + FormatFloat("00", i), ftFloat);
      PridejSloupec(Data->StatistikaTable, "FEMALE_HIST_COUNT" + FormatFloat("00", i), ftInteger);
    }//for
    for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
      PridejSloupec(Data->StatistikaTable, "MALE_HIST_VALUE" + FormatFloat("00", i), ftFloat);
      PridejSloupec(Data->StatistikaTable, "MALE_HIST_COUNT" + FormatFloat("00", i), ftInteger);
    }//for
    PridejSloupec(Data->StatistikaTable, "FEMALE_YESTERDAY_AVERAGE", ftFloat);
    PridejSloupec(Data->StatistikaTable, "MALE_YESTERDAY_AVERAGE", ftFloat);
    PridejSloupec(Data->StatistikaTable, "FEMALE_TARGET", ftFloat);
    PridejSloupec(Data->StatistikaTable, "MALE_TARGET", ftFloat);
    PridejSloupec(Data->StatistikaTable, "USE_REAL_UNI", ftBoolean);
    PridejSloupec(Data->StatistikaTable, "FEMALE_REAL_UNI", ftSmallint);
    PridejSloupec(Data->StatistikaTable, "MALE_REAL_UNI", ftSmallint);
    Data->StatistikaTable->CreateTable();

    // -------- Tabulka konfigurace
    Data->KonfiguraceTable->FieldDefs->Clear();
    PridejSloupec(Data->KonfiguraceTable, "VERSION_FW", ftInteger);
    PridejSloupec(Data->KonfiguraceTable, "BUILD", ftSmallint);                 // 6.3.2007: ve verzi vah 1.10 a vyssi
    PridejSloupec(Data->KonfiguraceTable, "HW_VERSION", ftSmallint);            // 6.3.2007: ve verzi vah 1.10 a vyssi
    PridejSloupec(Data->KonfiguraceTable, "ID", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "LANGUAGE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "F_MARGIN_ABOVE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "F_MARGIN_UNDER", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "M_MARGIN_ABOVE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "M_MARGIN_UNDER", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "FILTER_VALUE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "STABILIZATION", ftFloat);
    PridejSloupec(Data->KonfiguraceTable, "STABILIZATION_TIME", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "AUTO_MODE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "JUMP_MODE", ftSmallint);             // 6.3.2007: ve verzi vah 1.10 a vyssi
    PridejSloupec(Data->KonfiguraceTable, "UNITS", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "HIST_RANGE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "UNI_RANGE", ftSmallint);
    // TStartVykrmu
    PridejSloupec(Data->KonfiguraceTable, "START_PROGRESS", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "START_WAITING", ftBoolean);  // 21.9.2005
    PridejSloupec(Data->KonfiguraceTable, "START_USE_FLOCK", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "START_FLOCK", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "START_CURVE_MOVE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "START_DATE", ftDateTime);
    PridejSloupec(Data->KonfiguraceTable, "START_FAST_USE_GENDER", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "START_FAST_INIT_WEIGHT_F", ftFloat);
    PridejSloupec(Data->KonfiguraceTable, "START_FAST_INIT_WEIGHT_M", ftFloat);
    PridejSloupec(Data->KonfiguraceTable, "START_UNI_RANGE", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "START_ONLINE", ftBoolean);
    // TGsm
    PridejSloupec(Data->KonfiguraceTable, "GSM_USE", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "GSM_SEND_MIDNIGHT", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "GSM_PERIOD_MIDNIGHT", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "GSM_SEND_REQUEST", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "GSM_EXEC_REQUEST", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "GSM_CHECK_NUMBERS", ftBoolean);
    PridejSloupec(Data->KonfiguraceTable, "GSM_COUNT", ftSmallint);
    for (int i = 0; i < GSM_NUMBER_MAX_COUNT; i++) {
      PridejSloupec(Data->KonfiguraceTable, "GSM_NUMBER" + FormatFloat("00", i), ftString, GSM_NUMBER_MAX_LENGTH);
    }//for
    // Podsvit
    PridejSloupec(Data->KonfiguraceTable, "BACKLIGHT", ftSmallint);
    // RS-485
    PridejSloupec(Data->KonfiguraceTable, "RS485_ADDRESS",         ftSmallint); // 6.3.2007: ve verzi vah 1.10 a vyssi
    PridejSloupec(Data->KonfiguraceTable, "RS485_SPEED",           ftInteger);
    PridejSloupec(Data->KonfiguraceTable, "RS485_PARITY",          ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "RS485_REPLY_DELAY",     ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "RS485_SILENT_INTERVAL", ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "RS485_PROTOCOL",        ftSmallint);
    // Porovnani
    PridejSloupec(Data->KonfiguraceTable, "COMPARE", ftSmallint);               // 10.4.2007: ve verzi vah 1.10 a vyssi

    // Kompenzacni krivka
    PridejSloupec(Data->KonfiguraceTable, "CORRECTION_DAY1",       ftSmallint); // 5.11.2007: ve verzi vah 1.11 a vyssi
    PridejSloupec(Data->KonfiguraceTable, "CORRECTION_DAY2",       ftSmallint);
    PridejSloupec(Data->KonfiguraceTable, "CORRECTION_CORRECTION", ftFloat);

    Data->KonfiguraceTable->CreateTable();

    // -------- Tabulka hejn
    Data->HejnaTable->FieldDefs->Clear();
    PridejSloupec(Data->HejnaTable, "NUMBER", ftSmallint);
    PridejSloupec(Data->HejnaTable, "NAME", ftString, FLOCK_NAME_MAX_LENGTH);
    PridejSloupec(Data->HejnaTable, "USE_CURVES", ftBoolean);
    PridejSloupec(Data->HejnaTable, "USE_GENDER", ftBoolean);
    PridejSloupec(Data->HejnaTable, "WEIGH_FROM", ftSmallint);
    PridejSloupec(Data->HejnaTable, "WEIGH_TO", ftSmallint);
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      PridejSloupec(Data->HejnaTable, "FEMALE_DAY" + FormatFloat("00", i), ftInteger);   // Default hodfnota je 0xFFFF, nestaci smallint
      PridejSloupec(Data->HejnaTable, "FEMALE_WEIGHT" + FormatFloat("00", i), ftFloat);
    }//for
    for (int i = 0; i < CURVE_MAX_POINTS; i++) {
      PridejSloupec(Data->HejnaTable, "MALE_DAY" + FormatFloat("00", i), ftInteger);   // Default hodfnota je 0xFFFF, nestaci smallint
      PridejSloupec(Data->HejnaTable, "MALE_WEIGHT" + FormatFloat("00", i), ftFloat);
    }//for
    Data->HejnaTable->CreateTable();

    // --------- Tabulka online zaznamu
    Data->OnlineTable->FieldDefs->Clear();
    PridejSloupec(Data->OnlineTable, "DAY_NUMBER", ftSmallint);
    PridejSloupec(Data->OnlineTable, "TIME_HOUR", ftSmallint);
    PridejSloupec(Data->OnlineTable, "WEIGHT", ftFloat);
    PridejSloupec(Data->OnlineTable, "SAVED", ftBoolean);
    PridejSloupec(Data->OnlineTable, "STABLE", ftBoolean);
    Data->OnlineTable->CreateTable();

    // -------- Tabulka kalibrace
    Data->KalibraceTable->FieldDefs->Clear();
    PridejSloupec(Data->KalibraceTable, "PLATFORM",  ftSmallint);       // 6.3.2007: Ve verzi 1.10 uz sice neni, ale necham to zatim tam
    PridejSloupec(Data->KalibraceTable, "CAL_ZERO",  ftInteger);
    PridejSloupec(Data->KalibraceTable, "CAL_RANGE", ftInteger);
    PridejSloupec(Data->KalibraceTable, "RANGE",     ftInteger);
    PridejSloupec(Data->KalibraceTable, "DIVISION",  ftSmallint);
    Data->KalibraceTable->CreateTable();

    // -------- Tabulka hlavicky
    Data->HlavickaTable->FieldDefs->Clear();
    PridejSloupec(Data->HlavickaTable, "VERSION_SW", ftString, 10);  // Verze SW, ktery modul nacetl
    PridejSloupec(Data->HlavickaTable, "DATE_TIME", ftDateTime);     // Datum nacteni dat z modulu
    PridejSloupec(Data->HlavickaTable, "DATA_TYPE", ftSmallint);     // Typ dat - DATA_VAZENI nebo DATA_ONLINE
    PridejSloupec(Data->HlavickaTable, "NOTE", ftString, 30);        // Poznamka k celemu souboru
    Data->HlavickaTable->CreateTable();

    return true;
  } catch(...) {
    return false;
  }
}

// --------------------------------------------------------------------------
// Otevri existujici soubor
// --------------------------------------------------------------------------

// Nacteni souboru
#define NactiSoubor(Soubor, Delka)           \
  Bajty = new char[Delka + 1];               \
  ZapisovanySoubor = FileCreate(Soubor);     \
  FileRead(CtenySoubor, Bajty, Delka);       \
  FileWrite(ZapisovanySoubor, Bajty, Delka); \
  FileClose(ZapisovanySoubor);               \
  delete [] Bajty;                           \
  Bajty = NULL;                              \

// Smazani tabulky
#define SouborNejdeSmazat(Soubor) (FileExists(Soubor) && !DeleteFile(Soubor))

bool FileOpenFile(String Nazev, bool Porovnani, bool Find) {
  // Otevru soubor Nazev a vytvorim z nej soubor DB s daty a hlavickou
  // Pokud je nastaven <Find>, oteviraji se soubory pri hledani souboru
  String SouborDat, SouborStatistiky, SouborKonfigurace, SouborHejn, SouborKalibrace, SouborHlavicky, SouborOnline;
  String Predpona = "";

  if (Porovnani) {
    // Otevira soubor pro porovnani
    Predpona = PREDPONA_POROVNANI;
  } else if (Find) {
    // Otevira soubor pro porovnani
    Predpona = PREDPONA_HLEDANI;
  }

  SouborDat         = Data->SpecialFolders->Working + Predpona + SOUBORDAT;
  SouborStatistiky  = Data->SpecialFolders->Working + Predpona + SOUBORSTATISTIKY;
  SouborKonfigurace = Data->SpecialFolders->Working + Predpona + SOUBORKONFIGURACE;
  SouborHejn        = Data->SpecialFolders->Working + Predpona + SOUBORHEJN;
  SouborKalibrace   = Data->SpecialFolders->Working + Predpona + SOUBORKALIBRACE;
  SouborHlavicky    = Data->SpecialFolders->Working + Predpona + SOUBORHLAVICKY;
  SouborOnline      = Data->SpecialFolders->Working + Predpona + SOUBORONLINE;

  // Smazu oba DB soubory (budu je vytvaret znova)
  if (SouborNejdeSmazat(SouborDat)) return false;
  if (SouborNejdeSmazat(SouborStatistiky)) return false;
  if (SouborNejdeSmazat(SouborKonfigurace)) return false;
  if (SouborNejdeSmazat(SouborHejn)) return false;
  if (SouborNejdeSmazat(SouborKalibrace)) return false;
  if (SouborNejdeSmazat(SouborHlavicky)) return false;
  if (SouborNejdeSmazat(SouborOnline)) return false;

  // Nactu soubor a vytvorim DB
  int ZapisovanySoubor;
  int CtenySoubor;
  char Jmeno[31];
  int DelkaSouboruDat, DelkaSouboruStatistiky, DelkaSouboruKonfigurace, DelkaSouboruHejn, DelkaSouboruKalibrace, DelkaSouboruHlavicky, DelkaSouboruOnline;
  TSearchRec SearchRec;
  char *Bajty=NULL;
  try {
    Nazev=ChangeFileExt(Nazev,"." + Data->Version.FileExtension);
    CtenySoubor = FileOpen(Nazev, fmOpenRead);
    // Nactu verzi programu, kterym byl soubor ulozen
    FileRead(CtenySoubor, Jmeno, 5);   // Identifikace "Bat2 " zkontroluju a vyhodim
    Jmeno[5] = 0;  // Zakoncim
    if (String(Jmeno) != "Bat2 ") throw 1;    // Spatna hlavicka - nejde o soubor typu Bat2
    FileRead(CtenySoubor, Jmeno, 10);
    // Nactu velikosti jednotlivych souboru
    FileRead(CtenySoubor, (char*)&DelkaSouboruDat, 4);
    FileRead(CtenySoubor, (char*)&DelkaSouboruStatistiky, 4);
    FileRead(CtenySoubor, (char*)&DelkaSouboruKonfigurace, 4);
    FileRead(CtenySoubor, (char*)&DelkaSouboruHejn, 4);
    FileRead(CtenySoubor, (char*)&DelkaSouboruKalibrace, 4);
    FileRead(CtenySoubor, (char*)&DelkaSouboruHlavicky, 4);
    FileRead(CtenySoubor, (char*)&DelkaSouboruOnline, 4);
    // Nactu jednotlive soubory
    NactiSoubor(SouborDat, DelkaSouboruDat);
    NactiSoubor(SouborStatistiky, DelkaSouboruStatistiky);
    NactiSoubor(SouborKonfigurace, DelkaSouboruKonfigurace);
    NactiSoubor(SouborHejn, DelkaSouboruHejn);
    NactiSoubor(SouborKalibrace, DelkaSouboruKalibrace);
    NactiSoubor(SouborHlavicky, DelkaSouboruHlavicky);
    NactiSoubor(SouborOnline, DelkaSouboruOnline);
    // Zavru soubor
    FileClose(CtenySoubor);
    return true;
  } catch(...) {
    if (Bajty!=NULL) delete Bajty;
    FileClose(ZapisovanySoubor);
    FileClose(CtenySoubor);
    return false;
  }//catch
}

// --------------------------------------------------------------------------
// Rozhozeni zadaneho souboru do tabulek
// --------------------------------------------------------------------------

static void AddFieldToTable(TTable *ExistingTable, AnsiString Field, AnsiString Type) {
  // Pokud v tabulce <ExistingTable> neexistuje pole <Field>, zalozi nove s typem <Type>
  TTable *Table = new TTable(NULL);
  TQuery *Query = new TQuery(NULL);

  Table->DatabaseName = Data->Database->DatabaseName;
  Query->DatabaseName = Data->Database->DatabaseName;
  try {
    Table->TableName = ExistingTable->TableName;
    Table->Open();
    if (Table->FieldDefs->IndexOf(Field) < 0) {
      // Pole neexistuje, pridam ho
      Table->Close();
      Query->SQL->Clear();
      Query->SQL->Add("ALTER TABLE \"" + Table->TableName + "\"");
      Query->SQL->Add("ADD " + Field + " " + Type);
      Query->ExecSQL();
    }//if
  } catch(...) {}
  Table->Close();
  delete Table;
  delete Query;
}

void FileDecodeFileToTables(String Jmeno, bool Porovnani, bool Find) {
  // Otevre zadany soubor, rozhazi jej na jednotlive tabulky a udela pripadne zmeny v DB
  // Pokud je nastaven <Find>, oteviraji se tabulky pri hledani souboru
  if (!FileOpenFile(Jmeno, Porovnani, Find)) {
    MessageBox(NULL,LoadStr(CHYBA).c_str(),LoadStr(CHYBA).c_str(),MB_ICONEXCLAMATION | MB_OK | MB_TASKMODAL);
  }

  // Zkontroluju, zda vytvorene db nejsou nejake stare a obsahuje vsechna potrebna pole. Otevru db jako testovaci tabulku
  // a zkontroluju existenci novych sloupcu. Pokud nektere nebude obsahovat, pridam je tam pomoci SQL prikazu ALTER TABLE

  // 23.1.2005: Ve verzi 1.02 jsem pridal pole FILTER_VALUE (Short) do konfigurace.
  AddFieldToTable(Data->KonfiguraceTable, "FILTER_VALUE", "SMALLINT");

  // 13.2.2005: Ve verzi 1.05 jsem pridal pole BACKLIGHT (Short) do konfigurace.
  AddFieldToTable(Data->KonfiguraceTable, "BACKLIGHT", "SMALLINT");

  // 9.3.2005: Ve verzi 1.06 jsem pridal pole AUTO_MODE (Short) do konfigurace.
  AddFieldToTable(Data->KonfiguraceTable, "AUTO_MODE", "SMALLINT");

  // 9.3.2005: Ve verzi 1.06 jsem pridal pole GSM_PERIOD_MIDNIGHT (Short) do konfigurace.
  AddFieldToTable(Data->KonfiguraceTable, "GSM_PERIOD_MIDNIGHT", "SMALLINT");

  // 21.9.2005: Ve verzi 1.07 jsem pridal pole START_WAITING (Bool) do konfigurace.
  AddFieldToTable(Data->KonfiguraceTable, "START_WAITING", "BOOLEAN");

  // 6.3.2007: Ve verzi 1.10 jsem pridal spoustu poli
  AddFieldToTable(Data->KonfiguraceTable, "BUILD",                 "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "HW_VERSION",            "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "JUMP_MODE",             "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "RS485_ADDRESS",         "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "RS485_SPEED",           "INTEGER");
  AddFieldToTable(Data->KonfiguraceTable, "RS485_PARITY",          "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "RS485_REPLY_DELAY",     "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "RS485_SILENT_INTERVAL", "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "RS485_PROTOCOL",        "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "COMPARE",               "SMALLINT");

  // 5.11.2008: Ve verzi 1.11 jsem pridal kompenzacni krivku
  AddFieldToTable(Data->KonfiguraceTable, "CORRECTION_DAY1",       "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "CORRECTION_DAY2",       "SMALLINT");
  AddFieldToTable(Data->KonfiguraceTable, "CORRECTION_CORRECTION", "FLOAT");
}

