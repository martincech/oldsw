object ReadProgressForm: TReadProgressForm
  Left = 471
  Top = 429
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Read progress'
  ClientHeight = 111
  ClientWidth = 268
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object LblProgress: TLabel
    Left = 214
    Top = 46
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = '100.0%'
  end
  object Label1: TLabel
    Left = 20
    Top = 46
    Width = 75
    Height = 13
    Caption = 'Remaining time:'
  end
  object LblTime: TLabel
    Left = 107
    Top = 46
    Width = 42
    Height = 13
    Caption = '00:00:00'
  end
  object BtnCancel: TButton
    Left = 98
    Top = 76
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 0
    OnClick = Cancel
  end
  object ProgressBar: TProgressBar
    Left = 19
    Top = 24
    Width = 230
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 1
  end
end
