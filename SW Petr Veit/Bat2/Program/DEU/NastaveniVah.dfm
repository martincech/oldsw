�
 TNASTAVENIVAHFORM 071  TPF0TNastaveniVahFormNastaveniVahFormLeft�TopqBorderIconsbiSystemMenu BorderStylebsSingleCaptionEinstellungClientHeight�ClientWidthOColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidthTHeightCaptionEinstellungss�tze:  TButton	NewButtonLeftTop� WidthyHeightCaptionNeuTabOrderOnClickNewButtonClick  TButton
SaveButtonLeftTopWidthyHeightCaption	SpeichernTabOrderOnClickSaveButtonClick  TButtonUploadButtonLeftTopXWidthyHeightCaptionUploadTabOrderOnClickUploadButtonClick  TListBoxSeznamListBoxLeftTop WidthyHeight� 
ItemHeightSorted	TabOrder OnClickSeznamListBoxClick  TPageControlPageControlLeft� TopWidth�Height�
ActivePageHejnaTabSheetTabOrder 	TTabSheetHejnaTabSheetCaptionHerden
ImageIndex TLabelLabel23LeftTopWidth&HeightCaptionHerden:  TListBoxSeznamHejnListBoxLeftTopWidthKHeight� 
ItemHeightSorted	TabOrder OnClickSeznamHejnListBoxClick  TButtonNewFlockButtonLeftTop WidthKHeightCaptionNeuTabOrderOnClickNewFlockButtonClick  TButtonSaveFlockButtonLeftTop WidthKHeightCaption	SpeichernTabOrderOnClickSaveFlockButtonClick  TButtonDeleteFlockButtonLeftTop@WidthKHeightCaptionL�schenTabOrderOnClickDeleteFlockButtonClick  TPanel
HejnaPanelLefthTopWidth!HeightY
BevelOuterbvNoneTabOrder TLabelLabel22LeftTopWidthHeightCaptionName:  TLabelLabel24LeftTophWidth=HeightCaptionWiegen von:  TLabelLabel25Left� TophWidthHeightCaptionbis:  TDBEditJmenoDBEditLeftXTopWidth� Height	DataFieldNAME
DataSourceData.NastaveniHejnDataSourceTabOrder   TDBEditDBEdit15LeftPTopeWidthHeight	DataField
WEIGH_FROM
DataSourceData.NastaveniHejnDataSourceTabOrder  TDBEditDBEdit16Left� TopeWidthHeight	DataFieldWEIGH_TO
DataSourceData.NastaveniHejnDataSourceTabOrder  TPageControlKrivkyPageControlLeftTop� WidthHeight� 
ActivePageKrivkaSamiceTabSheetTabOrder 	TTabSheetKrivkaSamiceTabSheetCaptionWeibliche Tiere TStringGridKrivkaSamiceStringGridLeft Top Width	Height� AlignalTopColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsB] 
RowHeights   TButtonFemalesLoadButtonLeft� Top� WidthKHeightCaptionLadenTabOrderOnClickFemalesLoadButtonClick   	TTabSheetKrivkaSamciTabSheetCaptionM�nnliche Tiere
ImageIndex TStringGridKrivkaSamciStringGridLeft Top Width	Height� AlignalTopColCountDefaultRowHeightRowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditinggoAlwaysShowEditor TabOrder 	ColWidthsB]   TButtonMalesLoadButtonLeft� Top� WidthKHeightCaptionLadenTabOrderOnClickFemalesLoadButtonClick   	TTabSheetPocatecniHmotnostTabSheetCaptionAnfangsgewicht
ImageIndex TLabelLabel26LeftTopWidthMHeightCaptionWeibliche Tiere:  TDBEditPocatecniHmotnostSamiceDBEditLefthTopWidthaHeight	DataFieldFEMALE_WEIGHT00
DataSourceData.NastaveniHejnDataSourceTabOrder   TPanelPocatecniHmotnostSamciPanelLeftTop0Width� Height!
BevelOuterbvNoneTabOrder TLabelLabel27LeftTopWidthOHeightCaptionM�nnliche Tiere:  TDBEditPocatecniHmotnostSamciDBEditLeft`Top WidthaHeight	DataFieldMALE_WEIGHT00
DataSourceData.NastaveniHejnDataSourceTabOrder      	TCheckBoxPohlaviCheckBoxLeftTop0Width� HeightCaptionBeide Geschlechter verwendenTabOrderOnClickPohlaviCheckBoxClick  	TCheckBoxKrivkyCheckBoxLeftTopHWidth� HeightCaptionWachstumskurven verwendenTabOrderOnClickKrivkyCheckBoxClick   TButtonButton1Left TopHWidth3HeightCaptionButton1TabOrderVisibleOnClickButton1Click   	TTabSheetStatistikaTabSheetCaption	Statistik TLabelLabel2LeftTopWidthaHeightCaptionHistogramm-Bereich:  TLabelLabel3LeftTop0Width]HeightCaptionUniformit�tsbereich:  TLabelLabel4Left� Top0WidthHeightCaption�  TLabelLabel7Left� Top0WidthHeightCaption%  TLabelLabel6Left� TopWidthHeightCaption%  TLabelLabel5Left� TopWidthHeightCaption�  TDBEditRozsahUniformityDBEditLeft� Top-Width!Height	DataField	UNI_RANGE
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditRozsahHistogramuDBEditLeft� TopWidth!Height	DataField
HIST_RANGE
DataSourceData.NastaveniVahDataSourceTabOrder    	TTabSheetGsmTabSheetCaptionGSM
ImageIndex TLabelLabel8LeftTop� Width0HeightCaptionNummern:  TLabelLabel48Left"TopHWidth9HeightCaptionZeitintervall:  TLabelLabel49Left� TopHWidthHeightCaptionTage  TDBCheckBoxDBCheckBox1LeftTopWidthHeightCaptionGSM Kommunikation verwenden	DataFieldGSM_USE
DataSourceData.NastaveniVahDataSourceTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2LeftTop0WidthHeightCaptionStatistik um Mitternacht senden	DataFieldGSM_SEND_MIDNIGHT
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox3LeftTop`WidthHeightCaptionDaten auf Abruf senden	DataFieldGSM_SEND_REQUEST
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox4LeftTopxWidthHeightCaptionNummern pr�fen	DataFieldGSM_CHECK_NUMBERS
DataSourceData.NastaveniVahDataSourceTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit3LeftTop� Width� Height	DataFieldGSM_NUMBER00
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit4LeftTop� Width� Height	DataFieldGSM_NUMBER01
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit5LeftTop� Width� Height	DataFieldGSM_NUMBER02
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit6LeftTop� Width� Height	DataFieldGSM_NUMBER03
DataSourceData.NastaveniVahDataSourceTabOrder
OnKeyPressDBEdit3KeyPress  TDBEditDBEdit7LeftTopWidth� Height	DataFieldGSM_NUMBER04
DataSourceData.NastaveniVahDataSourceTabOrder	
OnKeyPressDBEdit3KeyPress  TDBEditPeriodaDBEditLeftXTopEWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT
DataSourceData.NastaveniVahDataSourceTabOrder   	TTabSheetRs485TabSheetCaptionRS-485
ImageIndex TLabelLabel51LeftTopWidthQHeightCaptionGeschwindigkeit:  TLabelLabel55Left� TopWidthHeightCaptionBd  TLabelLabel53LeftTop� WidthbHeightCaptionAntwortverz�gerung:  TLabelLabel54LeftTopWidth@HeightCaptionRuhe-Modus:  TLabelLabel57Left� TopWidthHeightCaptionms  TLabelLabel56Left� Top� WidthHeightCaptionms  TDBRadioGroupRs485ParityDBRadioGroupLeftTop8Width� HeightICaptionParit�tColumns	DataFieldRS485_PARITY
DataSourceData.NastaveniVahDataSourceItems.Strings8-n-18-e-18-o-18-n-2 TabOrderValues.Strings0123   TDBEditRs485ReplyDelayDBEditLeft� Top� Width!Height	DataFieldRS485_REPLY_DELAY
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditRs485SilentIntervalDBEditLeft� TopWidth!Height	DataFieldRS485_SILENT_INTERVAL
DataSourceData.NastaveniVahDataSourceTabOrder  TDBComboBoxRs485SpeedDBComboBoxLeft� TopWidthQHeightStylecsDropDownList	DataFieldRS485_SPEED
DataSourceData.NastaveniVahDataSource
ItemHeightItems.Strings1200240048009600192003840057600115200 TabOrder   TDBRadioGroupDBRadioGroup4LeftTop� Width� HeightICaption	Protokoll	DataFieldRS485_PROTOCOL
DataSourceData.NastaveniVahDataSourceItems.Strings
MODBUS RTUMODBUS ASCII TabOrderValues.Strings0123    	TTabSheetVahyTabSheetCaptionWaage
ImageIndex TLabelLabel9LeftTopWidthHeightCaption6�berschreitung des Sollgewichts bei weiblichen Tieren:  TLabelLabel10LeftDTopWidthHeightCaption%  TLabelLabel11LeftTop0WidthHeightCaption7Unterschreitung des Sollgewichts bei weiblichen Tieren:  TLabelLabel12LeftDTop0WidthHeightCaption%  TLabelLabel13LeftTopHWidthHeightCaption6�berschreitung des Sollgewichts bei m�nnlichen Tieren:  TLabelLabel14LeftDTopHWidthHeightCaption%  TLabelLabel15LeftTop`WidthHeightCaption7Unterschreitung des Sollgewichts bei m�nnlichen Tieren:  TLabelLabel16LeftDTop`WidthHeightCaption%  TLabelLabel17LeftTop� WidthAHeightCaptionStabilisierung:  TLabelLabel18LeftTop� WidthHeightCaption�  TLabelLabel19LeftDTop� WidthHeightCaption%  TLabelLabel20LeftTop� WidthVHeightCaptionStabilisierungszeit:  TLabelLabel21LeftTopxWidthHeightCaptionFilter:  TDBEditSamiceOkoliNadDBEditLeft TopWidth!Height	DataFieldF_MARGIN_ABOVE
DataSourceData.NastaveniVahDataSourceTabOrder   TDBEditSamiceOkoliPodDBEditLeft Top-Width!Height	DataFieldF_MARGIN_UNDER
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditSamciOkoliNadDBEditLeft TopEWidth!Height	DataFieldM_MARGIN_ABOVE
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditSamciOkoliPodDBEditLeft Top]Width!Height	DataFieldM_MARGIN_UNDER
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditUstaleniDBEditLeft Top� Width!Height	DataFieldSTABILIZATION
DataSourceData.NastaveniVahDataSourceTabOrder  TDBEditDobaUstaleniDBEditLeft Top� Width!Height	DataFieldSTABILIZATION_TIME
DataSourceData.NastaveniVahDataSourceTabOrder  TDBRadioGroupDBRadioGroup1LeftTop(Width1Height1Caption	EinheitenColumns	DataFieldUNITS
DataSourceData.NastaveniVahDataSourceItems.Stringskglb TabOrder	Values.Strings01   TDBEditFiltrDBEditLeft TopuWidth!Height	DataFieldFILTER_VALUE
DataSourceData.NastaveniVahDataSourceTabOrder  	TCheckBoxNastaveniGainCheckBoxLeftTop� Width� HeightCaption&Zunahme im Automatik Betrieb verwendenTabOrderOnClickNastaveniGainCheckBoxClick  TDBRadioGroupDBRadioGroup3LeftTop� Width1HeightICaptionGewicht speichern bei	DataField	JUMP_MODE
DataSourceData.NastaveniVahDataSourceItems.StringsBetreten der WaageVerlassen der WaageBetreten und Verlassen TabOrderValues.Strings012    	TTabSheetCorrectionTabSheetCaptionKorrekturkurve
ImageIndex TLabelLabel61LeftTop8WidthHeightCaptionTag 1:  TLabelLabel64LeftTopPWidthHeightCaptionTag 2:  TLabelLabel62LeftTophWidth.HeightCaption
Korrektur:  TLabelLabel63Left~TophWidthHeightCaption+  TLabelLabel65Left� TophWidthHeightCaption%  TDBEditCorrectionCorrectionDBEditLeft� TopeWidth!Height	DataFieldCORRECTION_CORRECTION
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEditCorrectionDay2DBEditLeft� TopMWidth!Height	DataFieldCORRECTION_DAY2
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  TDBEditCorrectionDay1DBEditLeft� Top5Width!Height	DataFieldCORRECTION_DAY1
DataSourceData.NastaveniVahDataSource	MaxLengthTabOrder  	TCheckBoxUseCorrectionCheckBoxLeftTopWidthQHeightCaptionKorrekturkurve verwendenTabOrder    	TTabSheetPodsvitTabSheetCaptionHintergrundbeleuchtung
ImageIndex TDBRadioGroupDBRadioGroup2LeftTopWidthHeight9CaptionHintergrundbeleuchtungColumns	DataField	BACKLIGHT
DataSourceData.NastaveniVahDataSourceItems.StringsAusEinAutomatisch TabOrder Values.Strings012     TButtonDownloadButtonLeftTopxWidthyHeightCaptionHerunterladenTabOrderOnClickDownloadButtonClick  TButtonDeleteButtonLeftTop8WidthyHeightCaptionL�schenTabOrderOnClickDeleteButtonClick   