//***************************************************************************
//
//    Curve.cpp - Growth curve
//    Version 1.0
//
//***************************************************************************

#include "Curve.h"

//---------------------------------------------------------------------------
// Konstruktor
//---------------------------------------------------------------------------

TGrowthCurve::TGrowthCurve(AnsiString Name, int MaxCount, int MinDay, int MaxDay, double MinWeight, double MaxWeight) {
  this->Name      = Name;
  this->MaxCount  = MaxCount;
  this->MinDay    = MinDay;
  this->MaxDay    = MaxDay;
  this->MinWeight = MinWeight;
  this->MaxWeight = MaxWeight;
}

//---------------------------------------------------------------------------
// Kopirovaci konstruktor
//---------------------------------------------------------------------------

TGrowthCurve::TGrowthCurve(const TGrowthCurve& SourceCurve) {
  TGrowthCurvePoint *CurvePoint;
  TGrowthCurvePoint *SourceCurvePoint;

  // Staticke promenne
  MaxCount  = SourceCurve.MaxCount;
  MinDay    = SourceCurve.MinDay;
  MaxDay    = SourceCurve.MaxDay;
  MinWeight = SourceCurve.MinWeight;
  MaxWeight = SourceCurve.MaxWeight;
  Name      = SourceCurve.Name;

  // Zkopiruju body krivky ze zdrojove krivky

  vector<TGrowthCurvePoint *> CurveVector(SourceCurve.Curve);

  for (TCurveIterator CurveIterator = CurveVector.begin(); CurveIterator != CurveVector.end(); CurveIterator++) {
    // Nactu bod ze zdrojove krivky
    SourceCurvePoint = *CurveIterator;

    // Ulozim novy bod
    CurvePoint = new TGrowthCurvePoint();               // Zalozim novy bod
    *CurvePoint = *SourceCurvePoint;                    // Vyplnim hodnoty
    Curve.push_back(CurvePoint);                        // Pridam ukazatel do kontejneru
  }
}

//---------------------------------------------------------------------------
// Destruktor
//---------------------------------------------------------------------------

TGrowthCurve::~TGrowthCurve(void) {
  Clear();
}

//---------------------------------------------------------------------------
// Najeti na urcity prvek
//---------------------------------------------------------------------------

vector<TGrowthCurvePoint *>::iterator TGrowthCurve::GetIterator(int Index) {
  // Vrati iterator na bod s indexem <Index>. Neosetruje prazdny seznam.
  TCurveIterator CurveIterator;

  CurveIterator = Curve.begin();
  while (Index) {
    CurveIterator++;
    Index--;
  }
  return CurveIterator;
}

//---------------------------------------------------------------------------
// Kontrola indexu
//---------------------------------------------------------------------------

bool TGrowthCurve::CheckIndex(int Index) {
  // Pokud je index <Index> platny v ramci krivky, vrati true
  return (bool)((unsigned)Index < Curve.size());
}

//---------------------------------------------------------------------------
// Trideni
//---------------------------------------------------------------------------

/*int Compareee(TGrowthCurvePoint *Point1, TGrowthCurvePoint *Point2) {
  return Point1 < Point2;
}*/

void TGrowthCurve::Sort(void) {
  // Setridi krivku podle dne
//  sort(Curve.begin(), Curve.end(), Compareee);

  TCurveIterator Iterator1, Iterator2;
  TGrowthCurvePoint CurvePoint;

  for (Iterator1 = Curve.begin(); Iterator1 < Curve.end() - 1; Iterator1++) {
    for (Iterator2 = Iterator1 + 1; Iterator2 < Curve.end(); Iterator2++) {
      if ((*Iterator1)->Day > (*Iterator2)->Day) {
        // Prohodim prvky
        CurvePoint = *(*Iterator1);
        *(*Iterator1) = *(*Iterator2);
        *(*Iterator2) = CurvePoint;
      }
    }
  }
}

//---------------------------------------------------------------------------
// Smazani vsech bodu
//---------------------------------------------------------------------------

void TGrowthCurve::Clear(void) {
  // Smazu vsechny body
  for (TCurveIterator CurveIterator = Curve.begin(); CurveIterator != Curve.end(); CurveIterator++) {
    delete (*CurveIterator);
  }
  // Smazu seznam
  Curve.clear();
}

//---------------------------------------------------------------------------
// Kontrola rozsahu a existence dne
//---------------------------------------------------------------------------

bool TGrowthCurve::CheckDay(int Day) {
  TCurveIterator CurveIterator;

  // Kontrola rozsahu
  if (Day < MinDay || Day > MaxDay) {
    return false;
  }

  // Kontrola, zda uz den v krivce neexistuje
  for (CurveIterator = Curve.begin(); CurveIterator != Curve.end(); CurveIterator++) {
    if ((*CurveIterator)->Day == Day) {
      return false;             // Zadany den uz v krivce existuje
    }
  }

  return true;
}

//---------------------------------------------------------------------------
// Kontrola rozsahu hmotnosti
//---------------------------------------------------------------------------

bool TGrowthCurve::CheckWeight(double Weight) {
  if (Weight < MinWeight || Weight > MaxWeight) {
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Pocet bodu v krivce
//---------------------------------------------------------------------------

int TGrowthCurve::Count(void) {
  return Curve.size();
}

//---------------------------------------------------------------------------
// Pridani bodu
//---------------------------------------------------------------------------

bool TGrowthCurve::Add(int Day, double Weight) {
  TGrowthCurvePoint *CurvePoint;
  TCurveIterator CurveIterator;

  // Kontrola rozsahu
  if (!CheckDay(Day)) {
    return false;               // Den je mimo rozsah nebo uz v krivce je
  }
  if (!CheckWeight(Weight)) {
    return false;               // Hmotnost je mimo rozsah
  }

  // Kontrola, zda je na novy bod misto
  if (Curve.size() >= (unsigned)MaxCount) {
    return false;               // Krivka uz je plna
  }

  // Ulozim novy bod
  CurvePoint = new TGrowthCurvePoint();       // Zalozim novy bod
  CurvePoint->Day    = Day;             // Vyplnim hodnoty
  CurvePoint->Weight = Weight;
  Curve.push_back(CurvePoint);          // Pridam ukazatel do kontejneru

  // Na zaver setridim podle dne
//  Curve.sort();
  Sort();

  return true;
}

//---------------------------------------------------------------------------
// Smazani bodu
//---------------------------------------------------------------------------

void TGrowthCurve::Delete(int Index) {
  // Smaze bod na pozici <Index>
  if (!CheckIndex(Index)) {
    return;
  }
  Curve.erase(GetIterator(Index));
}

//---------------------------------------------------------------------------
// Editace bodu
//---------------------------------------------------------------------------

bool TGrowthCurve::Edit(int Index, double Weight) {
  // Ulozi do bodu na pozici <Index> hmotnost <Weight>
  if (!CheckIndex(Index)) {
    return false;
  }
  if (!CheckWeight(Weight)) {
    return false;
  }
  (*GetIterator(Index))->Weight = Weight;
  return true;
}

//---------------------------------------------------------------------------
// Nacteni dne
//---------------------------------------------------------------------------

int TGrowthCurve::GetDay(int Index) {
  if (!CheckIndex(Index)) {
    return -1;
  }
  return (*GetIterator(Index))->Day;
}

//---------------------------------------------------------------------------
// Nacteni hmotnosti
//---------------------------------------------------------------------------

double TGrowthCurve::GetWeight(int Index) {
  if (!CheckIndex(Index)) {
    return 0;
  }
  return (*GetIterator(Index))->Weight;
}

//---------------------------------------------------------------------------
// Nacteni jmena
//---------------------------------------------------------------------------

AnsiString TGrowthCurve::GetName(void) {
  return Name;
}

//---------------------------------------------------------------------------
// Nastaveni jmena
//---------------------------------------------------------------------------

void TGrowthCurve::SetName(AnsiString Name) {
  this->Name = Name;
}

//---------------------------------------------------------------------------
// Vypocet hmotnosti pro zadany den
//---------------------------------------------------------------------------

double TGrowthCurve::CalculateWeight(int Day) {
  int LastIndex = 0;

  // Pokud ve krivce nic neni, vratim nulu
  if (Curve.size() == 0) {
    return 0;                           // Krivka je prazdna
  }

  // Pokud je den mensi nez minimalni den ve krivce, vratim hmotnost prvniho dne
  if (Day < GetDay(0)) {
    return GetWeight(0);                // Pod limitem vracime minimalni hmotnost
  }

  // Projizdim jednotlive body krivky
  for (int i = 0; i < (int)Curve.size(); i++) {
    if (GetDay(i) == Day) {
      return GetWeight(i);              // Nasel jsem primo zadany den => vratim primo hmotnost ve krivce (kvuli 1. bodu zde tento test musi byt)
    }
    if (GetDay(i) > Day) {
      // Nasel jsem prvni den, ktery lezi za zadanym dnem => aproximuju z predchoziho a tohoto dne. LastPoint je uz v tomto miste urcite inicializovan.
      // Vypoctu hmotnost podle vzorce primky:
      //     Y2-Y1
      // Y = ----- * (X-X1) + Y1
      //     X2-X1
      // Musim to pocitat long, protoze se tam muze nasobit 999 * 64000
      // Rozdil hmotnosti (CurvePoint->Weight - LastPoint->Weight) muze pri klesajici krivce vyjit zaporny => obe hodnoty musim prevest zvlast na long
      // Rozdil dnu (CurvePoint->Day - LastPoint->Day) i (Day - LastPoint->Day) vyjde vzdy kladny, protoze krivka je setridena podle dnu vzestupne
      // => rozdil dnu muzu pocitat ve word
      return (GetWeight(i) - GetWeight(LastIndex)) * (double)(Day - GetDay(LastIndex))
                    / (double)(GetDay(i) - GetDay(LastIndex))
                    + GetWeight(LastIndex);
    }
    LastIndex = i;
  }

  // Nenasel jsem zadny bod krivky, ktery by byl roven nebo vetsi nez zadany den => den vypadl z krivky, vracim hmotnost posledniho dne v krivce
  return GetWeight(LastIndex);          // CurvePoint uz je neplatny (mimo definovanou krivku) - musim vzit predchozi bod
}

