//***************************************************************************
//
//    ModbusScaleList.cpp - List of scales connected to the RS-485 network
//    Version 1.0
//
//***************************************************************************

#include "ModbusScaleList.h"

//---------------------------------------------------------------------------
// Destruktor
//---------------------------------------------------------------------------

TModbusScaleList::~TModbusScaleList() {
  Clear();
}

//---------------------------------------------------------------------------
// Najeti na urcitou vahu
//---------------------------------------------------------------------------

vector<TModbusScale *>::iterator TModbusScaleList::GetIterator(int Index) {
  // Vrati iterator na vahu s indexem <Index>. Neosetruje prazdny seznam.
  TModbusScaleListIterator Iterator;

  Iterator = ModbusScaleList.begin();
  while (Index) {
    Iterator++;
    Index--;
  }
  return Iterator;
}

//---------------------------------------------------------------------------
// Kontrola indexu
//---------------------------------------------------------------------------

bool TModbusScaleList::CheckIndex(int Index) {
  // Pokud je index <Index> platny v ramci seznamu krivek, vrati true
  return (bool)(Index >= 0 && (unsigned)Index < ModbusScaleList.size());
}

//---------------------------------------------------------------------------
// Trideni
//---------------------------------------------------------------------------

void TModbusScaleList::Sort(void) {
  // Setridi seznam podle adresy
  TModbusScaleListIterator Iterator1, Iterator2;
  TModbusScale *Scale;

  for (Iterator1 = ModbusScaleList.begin(); Iterator1 < ModbusScaleList.end() - 1; Iterator1++) {
    for (Iterator2 = Iterator1 + 1; Iterator2 < ModbusScaleList.end(); Iterator2++) {
      if ((*Iterator1)->GetAddress() > (*Iterator2)->GetAddress()) {
        // Prohodim prvky
        Scale = (*Iterator1);
        (*Iterator1) = (*Iterator2);
        (*Iterator2) = Scale;
      }
    }
  }
}

//---------------------------------------------------------------------------
// Smazani vsech vah
//---------------------------------------------------------------------------

void TModbusScaleList::Clear(void) {
  // Smaze vsechny vahy
  for (TModbusScaleListIterator Iterator = ModbusScaleList.begin(); Iterator != ModbusScaleList.end(); Iterator++) {
    delete (*Iterator);         // Smazu vsechny vahy
  }
  // Smazu seznam
  ModbusScaleList.clear();
}

//---------------------------------------------------------------------------
// Pocet vah v seznamu
//---------------------------------------------------------------------------

int TModbusScaleList::Count(void) {
  return ModbusScaleList.size();
}

//---------------------------------------------------------------------------
// Nalezeni indexu
//---------------------------------------------------------------------------

int TModbusScaleList::GetIndex(int Address) {
  // Vrati index vahy s adresou <Address> nebo -1, pokud vaha neexistuje
  TModbusScaleListIterator Iterator;
  int Index = 0;

  for (Iterator = ModbusScaleList.begin(); Iterator != ModbusScaleList.end(); Iterator++) {
    if ((*Iterator)->GetAddress() == Address) {
      return Index;             // Vaha uz v seznamu existuje
    }
    Index++;
  }
  return -1;                    // Neexistuje
}

//---------------------------------------------------------------------------
// Kontrola cisla vah
//---------------------------------------------------------------------------

bool TModbusScaleList::Exists(int Address) {
  // Kontrola, zda uz vaha v senamu neexistuje
  return (GetIndex(Address) >= 0);
}

//---------------------------------------------------------------------------
// Pridani nove vahy
//---------------------------------------------------------------------------

bool TModbusScaleList::Add(int Address, int Port, AnsiString Name) {
  TModbusScale *Scale;

  // Kontrola adresy
  if (Exists(Address)) {
    return false;               // Vaha uz v seznamu existuje
  }

  // Ulozim novou vahu
  Scale = new TModbusScale;
  Scale->SetAddress(Address);
  Scale->SetPort(Port);
  Scale->SetName(Name);
  ModbusScaleList.push_back(Scale);      // Pridam ukazatel do kontejneru

  // Na zaver setridim podle nazvu
  Sort();

  return true;
}

//---------------------------------------------------------------------------
// Smazani vahy
//---------------------------------------------------------------------------

bool TModbusScaleList::Delete(int Index) {
  // Smaze vahu na pozici <Index>
  if (!CheckIndex(Index)) {
    return false;
  }
  ModbusScaleList.erase(GetIterator(Index));
  return true;
}

//---------------------------------------------------------------------------
// Editace vahy
//---------------------------------------------------------------------------

bool TModbusScaleList::Edit(int Index, int Address, int Port, AnsiString Name) {
  // Zapise do vahy na pozici <Index> nove udaje <Address>, <Port> a <Name> a setridi seznam
  if (!CheckIndex(Index)) {
    return false;
  }

  // Kontrola adresy
  if (Exists(Address)) {
    return false;               // Adresa uz v seznamu existuje - musi byt unikatni
  }

  // Nastavim nove jmeno
  TModbusScale *Scale;
  Scale = GetScale(Index);
  Scale->SetAddress(Address);
  Scale->SetPort(Port);
  Scale->SetName(Name);

  // Na zaver setridim podle adresy
  Sort();

  return true;
}

//---------------------------------------------------------------------------
// Vraceni ukazatele na vahu
//---------------------------------------------------------------------------

TModbusScale * TModbusScaleList::GetScale(int Index) {
  // Vrati ukazatel na vahu na pozici <Index>
  if (!CheckIndex(Index)) {
    return NULL;
  }
  return (*GetIterator(Index));
}
