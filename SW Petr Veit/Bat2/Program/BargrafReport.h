//----------------------------------------------------------------------------
#ifndef BargrafReportH
#define BargrafReportH
//----------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\QuickRpt.hpp>
#include <vcl\QRCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Qrctrls.hpp>
#include <DBChart.hpp>
#include <Chart.hpp>
#include <QrTee.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
//----------------------------------------------------------------------------
class TBargrafReportForm : public TQuickRep
{
__published:
        TQRBand *TitleBand1;
        TQRLabel *NazevQRLabel;
        TQRSysData *QRSysData1;
        TQRLabel *QRLabel11;
        TQRLabel *SouborQRLabel;
        TQRLabel *QRLabel2;
        TQRLabel *QRLabel3;
        TQRLabel *PohlaviNazevQRLabel;
        TQRLabel *DenQRLabel;
        TQRLabel *DatumQRLabel;
        TQRLabel *PohlaviQRLabel;
        TQRDBChart *QRDBChart1;
        TQRChart *HistogramQRChart;
        TBarSeries *Series1;
private:
public:
   __fastcall TBargrafReportForm::TBargrafReportForm(TComponent* Owner);
};
//----------------------------------------------------------------------------
extern TBargrafReportForm *BargrafReportForm;
//----------------------------------------------------------------------------
#endif