�
 THLAVNIFORM 0�  TPF0THlavniForm
HlavniFormLeft�Top� Width Height&CaptionBat2Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style Menu	MainMenu1OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnResize
FormResizePixelsPerInch`
TextHeight TPageControlPageControlLeft Top%WidthHeight�
ActivePageKrivkaTabSheetAlignalClientTabOrder Visible 	TTabSheetReportTabSheetCaption�����
ImageIndex
 TDBGridReportDBGridLeft Top9WidthHeight~AlignalClient
DataSourceData.StatistikaQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 	OnKeyDownReportDBGridKeyDownColumnsExpanded	FieldName
DAY_NUMBERTitle.AlignmenttaCenterTitle.Caption����Visible	 	AlignmenttaRightJustifyExpanded	FieldNameDATE_TIME_DATETitle.AlignmenttaCenterTitle.Caption����WidthJVisible	 Expanded	FieldName
STAT_COUNTTitle.AlignmenttaCenterTitle.Caption����Visible	 Expanded	FieldNameAVERAGE_WEIGHTTitle.AlignmenttaCenterTitle.Caption�������Visible	 Expanded	FieldNameGAINTitle.AlignmenttaCenterTitle.Caption������� �������Width`Visible	 Expanded	FieldNameSIGMATitle.AlignmenttaCenterTitle.Caption��. ����������Visible	 Expanded	FieldNameCVTitle.AlignmenttaCenterTitle.Caption�� [%]Visible	 Expanded	FieldNameUNITitle.AlignmenttaCenterTitle.Caption��� [%]Visible	 Expanded	FieldNameCOMPARETitle.AlignmenttaCenterTitle.Caption��������Visible	 Expanded	FieldName
DIFFERENCETitle.AlignmenttaCenterTitle.Caption�������Visible	    TPanelReportPanelLeft Top WidthHeight9AlignalTop
BevelOuterbvNoneTabOrder TPanelPanel4Left�Top Width�Height9AlignalRight
BevelOuterbvNoneTabOrder  TLabelLabel58LeftgTopWidth<Height	AlignmenttaRightJustifyCaption�������� �:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonReportPorovnatSpeedButtonLeftsTopWidthHeightCaption...Flat	OnClickGrowthcurves1Click  	TComboBoxReportPorovnatComboBoxLeft� TopWidth� HeightStylecsDropDownList
ItemHeight TabOrder OnChangeKrivkaPorovnatComboBoxChange     	TTabSheetVazeniTabSheetCaption
����������
ImageIndex TDBGridDBGrid1Left Top WidthHeight�AlignalClient
DataSourceData.ZaznamyQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameSAMPLETitle.AlignmenttaCenterTitle.Caption	���������Visible	 Expanded	FieldName	TIME_HOURTitle.AlignmenttaCenterTitle.Caption���Visible	 Expanded	FieldNameWEIGHTTitle.AlignmenttaCenterTitle.Caption���Visible	     	TTabSheetStatistikaTabSheetCaption
����������
ImageIndex TLabelLabel3LeftTop WidthHeightCaption����:  TLabelCilovaHmotnostLabelLeftxTop Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelLabel5LeftTop0Width.HeightCaption�������:  TLabelPrumerLabelLeftxTop0Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelLabel7LeftTopXWidthHeightCaption����:  TLabel
PocetLabelLeft{TopXWidthHeight	AlignmenttaRightJustifyCaption00000  TLabelLabel9LeftTophWidthPHeightCaption��. ����������:  TLabel
SigmaLabelLeft~TophWidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelLabel11LeftTop� WidthLHeightCaption������������:  TLabelUniLabelLeft� Top� WidthHeight	AlignmenttaRightJustifyCaption000  TLabelLabel13LeftTopxWidthHeightCaption��:  TLabelCvLabelLeft� TopxWidthHeight	AlignmenttaRightJustifyCaption000  TLabelLabel15LeftTop@Width\HeightCaption������� �������:  TLabelPrirustekLabelLeftxTop@Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelCilovaHmotnostJednotkyLabelLeft� Top WidthHeightCaption��  TLabelPrumerJednotkyLabelLeft� Top0WidthHeightCaption��  TLabelPrirustekJednotkyLabelLeft� Top@WidthHeightCaption��  TLabelLabel20Left� Top� WidthHeightCaption%  TLabelLabel21Left� TopxWidthHeightCaption%  TPanelStatistikaPorovnaniPanelLeft� TopWidth� Height� 
BevelOuterbvNoneTabOrder  TLabelCilovaHmotnostPorovnaniLabelLeftTopWidth!Height	AlignmenttaRightJustifyCaption00,000  TLabelPrumerPorovnaniLabelLeftTop(Width!Height	AlignmenttaRightJustifyCaption00,000  TLabelPocetPorovnaniLabelLeftTopPWidthHeight	AlignmenttaRightJustifyCaption00000  TLabelSigmaPorovnaniLabelLeftTop`WidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelUniPorovnaniLabelLeftTop� WidthHeight	AlignmenttaRightJustifyCaption000  TLabelCvPorovnaniLabelLeftToppWidthHeight	AlignmenttaRightJustifyCaption000  TLabelPrirustekPorovnaniLabelLeftTop8Width!Height	AlignmenttaRightJustifyCaption00,000  TLabel$CilovaHmotnostJednotkyPorovnaniLabelLeft0TopWidthHeightCaption��  TLabelPrumerJednotkyPorovnaniLabelLeft0Top(WidthHeightCaption��  TLabelPrirustekJednotkyPorovnaniLabelLeft0Top8WidthHeightCaption��  TLabelLabel22Left0Top� WidthHeightCaption%  TLabelLabel23Left0ToppWidthHeightCaption%  TLabelStatistikaPorovnaniLabelLeftTopWidth7HeightCaption	���������    	TTabSheetHistogramTabSheetCaption�����������
ImageIndex 	TSplitterHistogramSplitterLeft TopWidthHeightCursorcrVSplitAlignalBottomBeveled	  TChartHistogramChartLeft Top WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.Increment;�O��n��?BottomAxis.MinorTickLengthBottomAxis.Title.Caption���LeftAxis.Grid.Color��� LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.Caption����Legend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeriesSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelHistogramPorovnaniPanelLeft TopWidthHeight� AlignalBottom
BevelOuterbvNoneTabOrder TChartHistogramPorovnaniChartLeft TopWidthHeight� BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.Increment;�O��n��?BottomAxis.MinorTickLengthBottomAxis.Title.Caption���LeftAxis.Grid.Color��� LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.Caption����Legend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeries
BarSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelPanel1Left Top WidthHeightAlignalTop
BevelOuterbvNoneTabOrder TLabelHistogramPorovnaniLabelLeftTopWidth7HeightCaption	���������  TPanelPanel2LeftWTop Width� HeightAlignalRight
BevelOuterbvNoneTabOrder  	TCheckBoxHistogramPorovnaniOsaCheckBoxLeft`TopWidthIHeightCaption	�� �� ���TabOrder OnClick"HistogramPorovnaniOsaCheckBoxClick      	TTabSheetAktivitaDenTabSheetCaption������� ����������
ImageIndex 	TSplitterAktivitaSplitterLeft TopWidthHeightCursorcrVSplitAlignalBottomBeveled	  TChartAktivitaDenChartLeft Top WidthHeightBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.IncrementBottomAxis.MaximumBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.Caption����� [����]LeftAxis.Grid.Color��� LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.Caption����Legend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder  
TBarSeries
BarSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZOffsetPercent<XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelAktivitaPorovnaniPanelLeft TopWidthHeight� AlignalBottom
BevelOuterbvNoneTabOrder TPanelPanel5Left Top WidthHeightAlignalTop
BevelOuterbvNoneTabOrder  TLabelAktivitaPorovnaniLabelLeftTopWidth7HeightCaption	���������  TPanelPanel6LeftWTop Width� HeightAlignalRight
BevelOuterbvNoneTabOrder    TChartAktivitaDenPorovnaniChartLeft TopWidthHeight� BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.AutomaticBottomAxis.AutomaticMaximumBottomAxis.AutomaticMinimumBottomAxis.ExactDateTimeBottomAxis.Grid.VisibleBottomAxis.IncrementBottomAxis.MaximumBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.Caption����� [����]LeftAxis.Grid.Color��� LeftAxis.Grid.StylepsSolidLeftAxis.LabelsLeftAxis.Title.Caption����Legend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder 
TBarSeries
BarSeries4Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.Visible	SeriesColorclRedBarWidthPercentZOffsetPercent<XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone     	TTabSheetAktivitaCelkemTabSheetCaption����� ����������
ImageIndex TChartAktivitaCelkemChartLeft Top WidthHeight�BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.Grid.Color��� BottomAxis.Grid.StylepsSolidBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.Caption����LeftAxis.Grid.Color��� LeftAxis.Grid.StylepsSolidLeftAxis.Title.Caption����Legend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0View3DAlignalClient
BevelOuterbvNoneTabOrder  TLineSeriesLineSeries1Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclRedTitle
����������Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesLineSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclNavyTitle	���������Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone    	TTabSheetKrivkaTabSheetCaption������ �����
ImageIndex TChartKrivkaChartLeft Top>WidthHeightyBackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.VisibleBottomAxis.Grid.Color��� BottomAxis.Grid.StylepsSolidBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.Title.Caption����LeftAxis.ExactDateTimeLeftAxis.Grid.Color��� LeftAxis.Grid.StylepsSolidLeftAxis.Increment���������?LeftAxis.Title.Caption���Legend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0Legend.VisibleRightAxis.Grid.VisibleRightAxis.Title.Caption
����������RightAxis.Title.Font.CharsetDEFAULT_CHARSETRightAxis.Title.Font.ColorclGreenRightAxis.Title.Font.Height�RightAxis.Title.Font.NameArialRightAxis.Title.Font.Style View3DAlignalClient
BevelOuterbvNoneTabOrder  TLineSeries
BarSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclRedTitle�������Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries2Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclNavyTitle����Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclPurpleTitle
Real comp.Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries4Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColor@�� TitleTarget comp.Pointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries5Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.Transparent	Marks.VisibleSeriesColorclOliveTitleTheoryPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone  TLineSeriesSeries6Marks.ArrowLengthMarks.VisibleSeriesColorclGreenTitle
StatisticsVertAxis
aRightAxisPointer.HorizSizePointer.InflateMargins	Pointer.Pen.VisiblePointer.StylepsRectanglePointer.VertSizePointer.Visible	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone   TPanelKrivkaPanelLeft Top WidthHeight>AlignalTop
BevelOuterbvNoneTabOrder 	TCheckBoxKrivkaRealCheckBoxLeftTopWidthAHeightCaption�������Checked	Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontState	cbCheckedTabOrder OnClickKrivkaRealCheckBoxClick  	TCheckBoxKrivkaTargetCheckBoxLeft`TopWidthIHeightCaption����Checked	Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKrivkaTargetCheckBoxClick  TPanelKrivkaPorovnaniPanelLeft� Top Width� Height"
BevelOuterbvNoneTabOrder TLabelKrivkaPorovnaniLabelLeft TopWidth:HeightCaption
���������:  	TCheckBoxKrivkaCRealCheckBoxLeft TopWidthIHeightCaption�������Checked	Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder OnClickKrivkaCRealCheckBoxClick  	TCheckBoxKrivkaCTargetCheckBoxLeftPTopWidthAHeightCaption����Checked	Font.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKrivkaCTargetCheckBoxClick   TPanelKrivkaTeoriePanelLeft�Top Width�Height>AlignalRight
BevelOuterbvNoneTabOrder TLabelLabel52LeftgTopWidth<Height	AlignmenttaRightJustifyCaption�������� �:Font.CharsetDEFAULT_CHARSET
Font.ColorclOliveFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonKrivkaPorovnatSpeedButtonLeftsTopWidthHeightCaption...Flat	OnClickGrowthcurves1Click  TLabelLabel60LefteTop+Width>Height	AlignmenttaRightJustifyCaption������ ���:Font.CharsetDEFAULT_CHARSET
Font.ColorclGreenFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxKrivkaPorovnatComboBoxLeft� TopWidth� HeightStylecsDropDownList
ItemHeightTabOrder OnChangeKrivkaPorovnatComboBoxChange  	TComboBoxCurveRightAxisComboBoxLeft� Top(Width� HeightStylecsDropDownList
ItemHeightTabOrderOnChangeCurveRightAxisComboBoxChangeItems.Strings<�� �������>������� �������������. ������������������������      	TTabSheetOnlineTabSheetCaption������� ��-����
ImageIndex TDBGridOnlineDBGridLeft Top WidthHeight�AlignalClient
DataSourceData.OnlineQueryDataSourceOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameSAMPLETitle.AlignmenttaCenterTitle.Caption	���������Visible	 Expanded	FieldName	TIME_HOURTitle.AlignmenttaCenterTitle.Caption���Visible	 Expanded	FieldNameWEIGHTTitle.AlignmenttaCenterTitle.Caption���Visible	 	AlignmenttaCenterExpanded	FieldNameSTABLETitle.AlignmenttaCenterTitle.Caption	���������Width>Visible	 	AlignmenttaCenterExpanded	FieldNameSAVEDTitle.AlignmenttaCenterTitle.Caption	���������Width?Visible	     	TTabSheetOnlineGrafTabSheetCaption��-���� ����
ImageIndex	 TChartOnlineChartLeft Top)WidthHeight�BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClearMarginBottom
MarginLeft	MarginTopTitle.Text.StringsTChart Title.Visible
OnUndoZoomOnlineChartUndoZoomBottomAxis.Grid.Color��� BottomAxis.Grid.StylepsSolidBottomAxis.LabelsSeparationBottomAxis.MinorTickCountBottomAxis.MinorTicks.VisibleBottomAxis.StartPositionBottomAxis.EndPositioncBottomAxis.Title.Caption
����������LeftAxis.AxisValuesFormat	#,##0.000LeftAxis.ExactDateTimeLeftAxis.Grid.Color��� LeftAxis.Grid.StylepsSolidLeftAxis.Increment;�O��n��?LeftAxis.StartPositionLeftAxis.Title.Caption���Legend.AlignmentlaTopLegend.ColorclSilverLegend.ColorWidthLegend.Frame.VisibleLegend.ShadowSize Legend.TextStyleltsPlainLegend.TopPos0Legend.VisibleView3DAlignalClient
BevelOuterbvNoneTabOrder OnMouseMoveOnlineChartMouseMove TFastLineSeriesLineSeries3Marks.Arrow.VisibleMarks.ArrowLengthMarks.Font.CharsetDEFAULT_CHARSETMarks.Font.ColorclBlackMarks.Font.Height�Marks.Font.NameArialMarks.Font.Style Marks.Frame.VisibleMarks.StylesmsValueMarks.VisibleSeriesColorclRedTitleOnlineLinePen.ColorclRedXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone   TPanelOnlineGrafPanelLeft Top WidthHeight)AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel44LeftpTopWidthAHeightCaption����������:  TLabelLabel45Left� TopWidthEHeightCaption�����������  TLabelLabel46LefthTopWidthHeightCaption���:  TLabelOnlineHmotnostLabelLeft�TopWidthHeight	AlignmenttaRightJustifyCaption0,000  TLabelOnlineHmotnostJednotkyLabelLeft�TopWidthHeightCaption��  TSpeedButtonOnlineZoomInSpeedButtonLeftTopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ��������������������{������������{����  ����������������������������  �������������������������������  ����OnClickOnlineZoomInSpeedButtonClick  TSpeedButtonOnlineZoomOutSpeedButtonLeft(TopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ��������������������{������������{����  ����������������������������  �������������������������������  ����OnClickOnlineZoomOutSpeedButtonClick  TBevelBevel4LeftXTopWidthHeightShape
bsLeftLine  TBevelBevel6LeftMTopWidthHeightShape
bsLeftLine  	TComboBoxOnlinePrumerovaniComboBoxLeft� TopWidth1HeightStylecsDropDownListDropDownCount
ItemHeightTabOrder OnChangeOnlinePrumerovaniComboBoxChangeItems.Strings12345678910111213141516171819202122232425262728293031323334353637383940     	TTabSheetNastaveniTabSheetCaption	���������
ImageIndex TPageControlNastaveniPageControlLeftTopWidthHeight�
ActivePageNastaveniVazeniTabSheetTabOrder  	TTabSheetNastaveniVazeniTabSheetCaption�����������
ImageIndex TLabelLabel42LeftTopXWidthHeightCaption����:  TLabelLabel59LeftToppWidth3HeightCaption	��������:  TDBCheckBoxDBCheckBox7LeftTopWidthHeightCaption����������� ������	DataFieldSTART_PROGRESS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder ValueCheckedTrueValueUncheckedFalse  TEditNastaveniHejnoEditLefthTopSWidthQHeightReadOnly	TabOrder  TDBCheckBoxDBCheckBox8LeftTop0WidthHeightCaption�������� �������	DataFieldSTART_WAITING
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit11LefthTopmWidthQHeight	DataFieldCOMPARE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniHejnaTabSheetCaption����
ImageIndex TLabelLabel4LeftTopWidthHeightCaption����:  TListBoxSeznamHejnListBoxLeftTopWidthKHeight!
ItemHeightSorted	TabOrder OnClickSeznamHejnListBoxClick  TPanel
HejnaPanelLefthTopWidth� HeightY
BevelOuterbvNoneTabOrder TLabelLabel6LeftTopWidthHeightCaption���:  TLabelLabel24LeftTophWidthJHeightCaption���������� �:  TLabelLabel25Left� TophWidthHeightCaption��:  TDBEditJmenoDBEditLeftXTopWidth� Height	DataFieldNAME
DataSourceData.HejnaDataSourceReadOnly	TabOrder   TDBEditDBEdit15LeftXTopeWidthHeight	DataField
WEIGH_FROM
DataSourceData.HejnaDataSourceReadOnly	TabOrder  TDBEditDBEdit16Left� TopeWidthHeight	DataFieldWEIGH_TO
DataSourceData.HejnaDataSourceReadOnly	TabOrder  TPageControlKrivkyPageControlLeftTop� Width� Height� 
ActivePageKrivkaSamiceTabSheetTabOrder 	TTabSheetKrivkaSamiceTabSheetCaption����� TStringGridKrivkaSamiceStringGridLeft Top Width� Height� AlignalClientColCountDefaultRowHeightRowCountTabOrder 	ColWidthsB]    	TTabSheetKrivkaSamciTabSheetCaption�����
ImageIndex TStringGridKrivkaSamciStringGridLeft Top Width� Height� AlignalClientColCountDefaultRowHeightRowCountTabOrder 	ColWidthsB]    	TTabSheetPocatecniHmotnostTabSheetCaption��������� ���
ImageIndex TLabelLabel26LeftTopWidth$HeightCaption�����:  TDBEditPocatecniHmotnostSamiceDBEditLeft`TopWidthaHeight	DataFieldFEMALE_WEIGHT00
DataSourceData.HejnaDataSourceReadOnly	TabOrder   TPanelPocatecniHmotnostSamciPanelLeftTop0Width� Height!
BevelOuterbvNoneTabOrder TLabelLabel27LeftTopWidth&HeightCaption�����:  TDBEditPocatecniHmotnostSamciDBEditLeftXTop WidthaHeight	DataFieldMALE_WEIGHT00
DataSourceData.HejnaDataSourceReadOnly	TabOrder      TDBCheckBoxDBCheckBox5LeftTop0Width� HeightCaption������������ ��� ����	DataField
USE_GENDER
DataSourceData.HejnaDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox6LeftTopHWidth� HeightCaption������������ ������ �����	DataField
USE_CURVES
DataSourceData.HejnaDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse    	TTabSheetNastaveniStatistikaTabSheetCaption
���������� TLabelLabel8LeftTopWidth~HeightCaption�������� �����������:  TLabelLabel10LeftTop0Width� HeightCaption�������� ������������:  TLabelLabel12Left� Top0WidthHeightCaption�  TLabelLabel14Left� Top0WidthHeightCaption%  TLabelLabel16Left� TopWidthHeightCaption%  TLabelLabel28Left� TopWidthHeightCaption�  TDBEditRozsahUniformityDBEditLeft� Top-Width!Height	DataField	UNI_RANGE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditRozsahHistogramuDBEditLeft� TopWidth!Height	DataField
HIST_RANGE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder    	TTabSheetNastaveniGsmTabSheetCaptionGSM
ImageIndex TLabelLabel29LeftTop� Width+HeightCaption������:  TLabelLabel48Left"TopHWidth)HeightCaption������:  TLabelLabel49LeftzTopHWidthHeightCaption����  TDBCheckBoxDBCheckBox1LeftTopWidthHeightCaption������������ GSM �����	DataFieldGSM_USE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2LeftTop0WidthHeightCaption���������� ���������� � �������	DataFieldGSM_SEND_MIDNIGHT
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox3LeftTop`WidthHeightCaption ���������� ���������� �� �������	DataFieldGSM_SEND_REQUEST
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox4LeftTopxWidthHeightCaption��������� ������	DataFieldGSM_CHECK_NUMBERS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit3LeftTop� Width� Height	DataFieldGSM_NUMBER00
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit4LeftTop� Width� Height	DataFieldGSM_NUMBER01
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit5LeftTop� Width� Height	DataFieldGSM_NUMBER02
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit6LeftTop� Width� Height	DataFieldGSM_NUMBER03
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit7LeftTopWidth� Height	DataFieldGSM_NUMBER04
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder	  TDBEditDBEdit8LeftPTopEWidth!Height	DataFieldGSM_PERIOD_MIDNIGHT
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniRs485TabSheetCaptionRS-485
ImageIndex TLabelLabel50LeftTopWidth"HeightCaption�����:  TLabelLabel51LeftTop0Width3HeightCaption	��������:  TLabelLabel53LeftTop� Width[HeightCaption�������� ������:  TLabelLabel54LeftTop� WidthhHeightCaption�������� ��������:  TLabelLabel55Left Top0WidthHeightCaptionBd  TLabelLabel56Left Top� WidthHeightCaption��  TLabelLabel57Left Top� WidthHeightCaption��  TDBEditDBEdit9Left� TopWidthQHeight	DataFieldRS485_ADDRESS
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   TDBEditDBEdit10Left� Top-WidthQHeight	DataFieldRS485_SPEED
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit12Left� Top� WidthQHeight	DataFieldRS485_REPLY_DELAY
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit13Left� Top� WidthQHeight	DataFieldRS485_SILENT_INTERVAL
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBRadioGroupDBRadioGroup4LeftTopPWidth	Height1Caption�������Columns	DataFieldRS485_PARITY
DataSourceData.KonfiguraceDataSourceItems.Strings8-n-18-e-18-o-18-n-2 ReadOnly	TabOrderValues.Strings0123   TDBRadioGroupDBRadioGroup5LeftTop� Width	Height1Caption��������Columns	DataFieldRS485_PROTOCOL
DataSourceData.KonfiguraceDataSourceItems.Strings
MODBUS RTUMODBUS ASCII ReadOnly	TabOrderValues.Strings0123    	TTabSheetNastaveniVahyTabSheetCaption����
ImageIndex TLabelLabel30LeftTop0Width� HeightCaption!���������� ����� ����� ��� �����:  TLabelLabel31LeftTop0WidthHeightCaption%  TLabelLabel32LeftTopHWidth� HeightCaption ���������� ���� ����� ��� �����:  TLabelLabel33LeftTopHWidthHeightCaption%  TLabelLabel34LeftTop`Width� HeightCaption"���������� ����� ����� ��� ������:  TLabelLabel35LeftTop`WidthHeightCaption%  TLabelLabel36LeftTopxWidth� HeightCaption!���������� ���� ����� ��� ������:  TLabelLabel37LeftTopxWidthHeightCaption%  TLabelLabel38LeftTop� WidthKHeightCaption������������:  TLabelLabel39Left� Top� WidthHeightCaption�  TLabelLabel40LeftTop� WidthHeightCaption%  TLabelLabel41LeftTop� WidthnHeightCaption����� ������������:  TLabelLabel43LeftTop� Width+HeightCaption������:  TLabelLabel47LeftTopWidth� HeightCaption����������������� �����:  TDBEditSamiceOkoliNadDBEditLeft� Top-Width!Height	DataFieldF_MARGIN_ABOVE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamiceOkoliPodDBEditLeft� TopEWidth!Height	DataFieldF_MARGIN_UNDER
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamciOkoliNadDBEditLeft� Top]Width!Height	DataFieldM_MARGIN_ABOVE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditSamciOkoliPodDBEditLeft� TopuWidth!Height	DataFieldM_MARGIN_UNDER
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditUstaleniDBEditLeft� Top� Width!Height	DataFieldSTABILIZATION
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDobaUstaleniDBEditLeft� Top� Width!Height	DataFieldSTABILIZATION_TIME
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBRadioGroupDBRadioGroup1LeftTopHWidth� Height1Caption�������Columns	DataFieldUNITS
DataSourceData.KonfiguraceDataSourceItems.Strings���. ReadOnly	TabOrder
Values.Strings01   TDBEditFiltrDBEditLeft� Top� Width!Height	DataFieldFILTER_VALUE
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit2Left� TopWidth!Height	DataFieldID
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TCheckBoxNastaveniGainCheckBoxLeftTop� WidthaHeightCaption+������������ ������ � �������������� ������TabOrder  TDBRadioGroupDBRadioGroup3LeftTop� Width� HeightICaption��������� ������� ���	DataField	JUMP_MODE
DataSourceData.KonfiguraceDataSourceItems.Strings
���� ���������� �� �������� ReadOnly	TabOrder	Values.Strings012    	TTabSheetTabSheetCorrectionCaption������ ���������
ImageIndex TLabelLabel61LeftTopWidth'HeightCaption���� 1:  TLabelLabel62LeftTopHWidth:HeightCaption
���������:  TLabelLabel64LeftTop0Width'HeightCaption���� 2:  TLabelLabel63Left~TopHWidthHeightCaption+  TLabelLabel65Left� TopHWidthHeightCaption%  TDBEditDBEdit14Left� TopWidth!Height	DataFieldCORRECTION_DAY1
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   TDBEditDBEdit18Left� Top-Width!Height	DataFieldCORRECTION_DAY2
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder  TDBEditDBEdit17Left� TopEWidth!Height	DataFieldCORRECTION_CORRECTION
DataSourceData.KonfiguraceDataSourceReadOnly	TabOrder   	TTabSheetNastaveniPodsvitTabSheetCaption	���������
ImageIndex TDBRadioGroupDBRadioGroup2LeftTopWidth� Height9Caption	���������Columns	DataField	BACKLIGHT
DataSourceData.KonfiguraceDataSourceItems.Strings����.���.���� ReadOnly	TabOrder Values.Strings012      	TTabSheetInformaceTabSheetCaption
����������
ImageIndex TLabelLabel17LeftTop WidthEHeightCaption���� ������:  TLabel
VerzeLabelLeftxTop Width5HeightCaption
VerzeLabel  TLabelLabel18LeftTopXWidthBHeightCaption����������:  TLabelLabel19LeftTop8Width:HeightCaption
���������:  TDBTextDBText1LeftxTop8Width� Height	DataField	DATE_TIME
DataSourceData.HlavickaDataSource  TDBEditDBEdit1LeftxTopUWidth� Height	DataFieldNOTE
DataSourceData.HlavickaDataSourceTabOrder     TPanelPanel3Left Top WidthHeight%AlignalTop
BevelOuterbvNoneTabOrder TBevelBevel2Left TopWidthHeightAlignalBottomShape	bsTopLine  TBevelBevel3Left Top WidthHeightAlignalTopShape	bsTopLine  TSpeedButtonOpenSpeedButtonLeftTopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwwwwwwwwww     ww 33330ww3333w�33330w�3333��     ����ww����ww�   wwp wwww wwwwwwpwwwwwwwwwp wwwwwwwwwwParentShowHintShowHint	OnClickOtevt1Click  TSpeedButtonSaveSpeedButtonLeft TopWidthHeightEnabledFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� wwwwwwwww      p3   wp3   wp3   wp3    p333333p3    3p0wwwwp0wwwwp0wwwwp0wwwwp0wwww p0wwwwp      wwwwwwwwParentShowHintShowHint	OnClickUloit1Click  TSpeedButtonReadSpeedButtonLeft`TopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� �����  �������������  �������������  ����������������� ���wp���������������������������p��������������ParentShowHintShowHint	OnClickNastzznamy1Click  TSpeedButtonFindSpeedButtonLeft8TopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                   �  �   �� �   � � ��  ��� ���   � ���  �� �   ��� ��  ��� ��������    ������
�� ����� ���
� 
�� � �� �� 
����p��p� ��z������  ��
� ������p   �zڭ���p���� ��
�ڭ�� ���ParentShowHintShowHint	OnClickFindfile1Click  TSpeedButtonSetupSpeedButtonLeftxTopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                   �  �   �� �   � � ��  ��� ���   � ���  �� �   ��� ��  ��� �������ڭ��������
��� �ڠ�������
�
�ڭ��������
��ڭ���������
���  ���  
� 0���0��
��30ڭ � 30���� 3 
ڭ��� ��ParentShowHintShowHint	OnClickConfig1Click  TSpeedButtonDatabaseSpeedButtonLeft� TopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                   �  �   �� �   � � ��  ��� ���   � ���  �� �   ��� ��  ��� �������ڭ�����������    ����������  � �������  � ��������� � ���������     ��������    
ڠ���𭭭�    ��ڭ�������ParentShowHintShowHint	OnClickWeighingdatabase1Click  TSpeedButtonAddSpeedButtonLeft� TopWidthHeightEnabledFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ������������������������������  ���������������  0�������  ��  ��������   ���������������������  �����������������ParentShowHintShowHint	OnClickAddtoweighingdatabase1Click  TBevelBevel7LeftWTopWidthHeightShape
bsLeftLine  TBevelBevel8Left� TopWidthHeightShape
bsLeftLine  TSpeedButtonRs485SpeedButtonLeft� TopWidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ������������������������� �����������������������  �����80������������80�������  ����80������  ������������ParentShowHintShowHint	OnClickRS4851Click  TBevelBevel9Left� TopWidthHeightShape
bsLeftLine  TPanelZahlaviPanelLeftTopWidth�Height
BevelOuterbvNoneTabOrder Visible TLabelLabel1LeftTopWidthHeightCaption����:  TLabel
DatumLabelLeft� TopWidth;Height	AlignmenttaCenterAutoSizeCaption
00.00.0000  TSpeedButtonPrvniDenSpeedButtonLeft{Top WidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               8                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ������ ���� � �� � � ��   � �� � � ���� � ������ OnClickPrvniDenSpeedButtonClick  TSpeedButtonPredchoziDenSpeedButtonLeft� Top WidthHeightFlat	
Glyph.Data
�   �   BM�       v   (                                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ������� �� �� �������OnClickPredchoziDenSpeedButtonClick  TSpeedButtonDalsiDenSpeedButtonLeft� Top WidthHeightFlat	
Glyph.Data
�   �   BM�       v   (                                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ��� �� � � �  � � �� ��� OnClickDalsiDenSpeedButtonClick  TSpeedButtonPosledniDenSpeedButtonLeft� Top WidthHeightFlat	
Glyph.Data
�   �   BM�       v   (               8                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ������ �� ��� � � �� �   �� � � �� �� ��� ������ OnClickPosledniDenSpeedButtonClick  TBevelBevel5Left TopWidthHeightShape
bsLeftLine  TPanelPohlaviPanelLeft Top Width� Height
BevelOuterbvNoneTabOrder  TLabelLabel2LeftTopWidthHeightCaption���:  TSpeedButtonSpeedButton1LeftXTopWidthHeightEnabledFlat	
Glyph.Data
"    BM      v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� ��������   ��������   �  ��  �   ��������   ��������   �� ��� �   ���p��x   �������   ��� ����   ��� ����   ��� ����   �������   ���p��x   �� ��� �   	NumGlyphs  TSpeedButtonSpeedButton2Left� TopWidthHeightEnabledFlat	
Glyph.Data
�   �   BM�       v   (               �                        �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� �� ���� �� ����p��x� ��������� ��������� ��������� ��������� ��������� �� �p��� �� �� �� ���������  ���� ���   	NumGlyphs  TBevelBevel1Left TopWidthHeightShape
bsLeftLine  TRadioButtonPohlaviSamiceRadioButtonLeftHTopWidthHeightChecked	TabOrder TabStop	OnClickPohlaviSamiceRadioButtonClick  TRadioButtonPohlaviSamciRadioButtonLeftpTopWidthHeightTabOrderOnClickPohlaviSamciRadioButtonClick   	TComboBoxDenComboBoxLeft8TopWidth1HeightStylecsDropDownListDropDownCount
ItemHeightTabOrderOnChangeDenComboBoxChange    	TMainMenu	MainMenu1Left�Top 	TMenuItemSoubor1Caption���� 	TMenuItemOtevt1Caption�������ShortCutO@OnClickOtevt1Click  	TMenuItemUloit1Caption	���������ShortCutS@OnClickUloit1Click  	TMenuItemN7Caption-  	TMenuItem	Findfile1Caption����� ����...OnClickFindfile1Click  	TMenuItemN14Caption-  	TMenuItemOpenforcomparsion1Caption������� ��� ���������OnClickOpenforcomparsion1Click  	TMenuItemN2Caption-  	TMenuItemTisk1Caption������ 	TMenuItemMenuPrintReportCaption�����OnClickMenuPrintReportClick  	TMenuItemMenuPrintSamplesCaption
����������OnClickMenuPrintSamplesClick  	TMenuItemMenuPrintHistogramCaption�����������OnClickMenuPrintHistogramClick  	TMenuItemMenuPrintDayActivityCaption������� ����������OnClickMenuPrintDayActivityClick  	TMenuItemMenuPrintTotalActivityCaption����� ����������OnClickMenuPrintTotalActivityClick  	TMenuItemMenuPrintGrowthCurveCaption������ �����OnClickMenuPrintGrowthCurveClick  	TMenuItemMenuPrintOnlineCaption��-���� ����OnClickMenuPrintOnlineClick   	TMenuItemExport1Caption������� � Excel 	TMenuItemMenuExportReportCaption�����OnClickMenuExportReportClick  	TMenuItemMenuExportSamplesCaption
����������OnClickMenuExportSamplesClick  	TMenuItemMenuExportHistogramCaption�����������OnClickMenuExportHistogramClick  	TMenuItemMenuExportDayActivityCaption������� ����������OnClickMenuExportDayActivityClick  	TMenuItemMenuExportTotalActivityCaption����� ����������OnClickMenuExportTotalActivityClick  	TMenuItemMenuExportGrowthCurveCaption������ �����OnClickMenuExportGrowthCurveClick  	TMenuItemMenuExportOnlineCaption������� ��-����OnClickMenuExportOnlineClick   	TMenuItemN4Caption-  	TMenuItemMenuSimulationCaption���������...OnClickMenuSimulationClick  	TMenuItemN5Caption-  	TMenuItemKonecprogramu1Caption�����OnClickKonecprogramu1Click   	TMenuItemModul1Caption���� 	TMenuItemMemorymodule1Caption������ ������ 	TMenuItemNastzznamy1Caption������� ������...OnClickNastzznamy1Click  	TMenuItemConfig1Caption���������...OnClickConfig1Click  	TMenuItemDiagnostics1Caption����������� 	TMenuItemReaddatatofile1Caption��������� � ������ � ����...OnClickReaddatatofile1Click  	TMenuItemReaddatafromfile1Caption��������� �� ����� � ������...OnClickReaddatafromfile1Click    	TMenuItemRS4851CaptionRS-485 ����...OnClickRS4851Click  	TMenuItemN3Caption-  	TMenuItemGrowthcurves1Caption������ �����...OnClickGrowthcurves1Click   	TMenuItem	Database1Caption���� ������ 	TMenuItemWeighingdatabase1Caption#�������� ���� ������ �����������...OnClickWeighingdatabase1Click  	TMenuItemN6Caption-  	TMenuItemAddtoweighingdatabase1Caption*�������� ���� � ���� ������ �����������...EnabledOnClickAddtoweighingdatabase1Click   	TMenuItem	Nastaven1Caption����� 	TMenuItemJazyk1Caption���� 	TMenuItemDansk1CaptionDanskOnClickDansk1Click  	TMenuItemDeutsch1CaptionDeutschOnClickDeutsch1Click  	TMenuItem	Anglicky1CaptionEnglishOnClickAnglicky1Click  	TMenuItemEspanol1CaptionEspanolOnClickEspanol1Click  	TMenuItemFrancouzsky1CaptionFran�aisOnClickFrancouzsky1Click  	TMenuItemRussian1CaptionRussianOnClickRussian1Click  	TMenuItemFinsky1CaptionSuomiOnClickFinsky1Click  	TMenuItemTurkce1CaptionTurkceOnClickTurkce1Click    	TMenuItemNpovda1Caption������ 	TMenuItem
Oprogramu1Caption� ���������...OnClickOprogramu1Click    TOpenDialog
OpenDialogFilter
Bat2|*.bt2OptionsofHideReadOnlyofPathMustExistofFileMustExistofEnableSizing Left�Top   TSaveDialog
SaveDialogFilter
Bat2|*.bt2OptionsofOverwritePromptofHideReadOnlyofPathMustExistofEnableSizing Left�Top  TOpenDialogOpenDialogHardCopyFilterBat2 hardcopy|*.binOptionsofHideReadOnlyofPathMustExistofFileMustExistofEnableSizing Left�  TSaveDialogSaveDialogHardCopyFilterBat2 hardcopy|*.binOptionsofOverwritePromptofHideReadOnlyofPathMustExistofEnableSizing Left�Top��     