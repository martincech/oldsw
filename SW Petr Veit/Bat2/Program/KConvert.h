//******************************************************************************
//
//   KConvert.h   Keil data conversions
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef KConvertH
   #define KConvertH

#pragma pack( push, 1)                  // byte alignment
   #include "Bat2.h"                    // flash data definition
#pragma pack( pop)                      // original alignment

//******************************************************************************
//  Konverze z Keil :
//******************************************************************************

word   KConvertGetWord(  word X);
int16  KConvertGetInt(   int16 X);
dword  KConvertGetDword( dword X);
long32 KConvertGetLong(  long32 X);
float  KConvertGetFloat( float X);
TDateTime  KConvertGetDateTime( TLongDateTime X);
AnsiString KConvertGetString( byte *S, int Length = 0);

//******************************************************************************
//  Konverze do Keil :
//******************************************************************************

#define KConvertPutWord( X)  KConvertGetWord( X)
#define KConvertPutInt( X)   KConvertGetInt( X)
#define KConvertPutDword( X) KConvertGetDword( X)
#define KConvertPutLong( X)  KConvertGetLong( X)
#define KConvertPutFloat( X) KConvertGetFloat( X)

TLongDateTime KConvertPutDateTime( TDateTime X);
void KConvertPutString( byte *S, AnsiString X, int Length);

//-----------------------------------------------------------------------------
// Prohozeni LSB a MSB
//-----------------------------------------------------------------------------

void KConvertSwapWord(word *Data);
void KConvertSwapDWord(dword *Data);
void KConvertSwapLong(long32 *Data);
void KConvertSwapFloat(float *Data);


#endif
