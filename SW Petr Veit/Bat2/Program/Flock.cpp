//***************************************************************************
//
//    Flock.cpp - Flock definition saved in setup
//    Version 1.0
//
//***************************************************************************

#include "Flock.h"
#include "KConvert.h"
#include "DM.h"

//-----------------------------------------------------------------------------
// Konstruktor
//-----------------------------------------------------------------------------

void TFlockClass::Init() {
  int Gender;

  // Zalozim rustove krivky
  for (Gender = 0; Gender < _GENDER_COUNT; Gender++) {
    GrowthCurve[Gender] = new TGrowthCurve(AnsiString(Gender), CURVE_MAX_POINTS, CURVE_FIRST_DAY_NUMBER, CURVE_MAX_DAY,
                                           PrepoctiNaKg(MIN_TARGET_WEIGHT), PrepoctiNaKg(MAX_TARGET_WEIGHT));
  }

  // Vyplnim nejake default parametry
  Number = 0;
  Title = "";                           // Nazev hejna
  UseBothGenders= false;
  UseCurves = false;
  WeighFrom = FLOCK_TIME_LIMIT_EMPTY;
  WeighTill = FLOCK_TIME_LIMIT_EMPTY;
  for (Gender = 0; Gender < _GENDER_COUNT; Gender++) {
    InitialWeight[Gender] = PrepoctiNaKg(MIN_TARGET_WEIGHT);
  }
} // TFlockClass

TFlockClass::TFlockClass() {
  Init();
} // TFlockClass

TFlockClass::TFlockClass(int FlockNumber) {
  // Zalozi nove hejno s cislem <FlockNumber>
  Init();
  Number = FlockNumber;
} // TFlockClass

TFlockClass::TFlockClass(TFlock *Flock) {
  // Prevezme parametry hejna <Flock>
  Init();
  Load(Flock);
} // TFlockClass

//-----------------------------------------------------------------------------
// Destruktor
//-----------------------------------------------------------------------------

TFlockClass::~TFlockClass() {
  // Smazu rustove krivky
  for (int i = 0; i < _GENDER_COUNT; i++) {
    delete GrowthCurve[i];
  }
} // ~TFlock

//-----------------------------------------------------------------------------
// Nahrani parametru
//-----------------------------------------------------------------------------

void TFlockClass::Load(TFlock *Flock) {
  // Prevezme parametry hejna <Flock>
  char s[FLOCK_NAME_MAX_LENGTH + 1];    // O zakoncovaci znak delsi nez nazev ve vaze
  int i;
  int Gender;

  Number         = Flock->Header.Number;
  Title          = KConvertGetString(Flock->Header.Title, FLOCK_NAME_MAX_LENGTH);        // Odmazu koncove mezery a zakoncim
  UseBothGenders = Flock->Header.UseBothGenders;
  UseCurves      = Flock->Header.UseCurves;
  WeighFrom      = Flock->Header.WeighFrom;
  WeighTill      = Flock->Header.WeighTill;

  // Default smazu cilove hmotnosti a rustove krivky
  for (Gender = 0; Gender < _GENDER_COUNT; Gender++) {
    InitialWeight[Gender] = PrepoctiNaKg(MIN_TARGET_WEIGHT);
    GrowthCurve[Gender]->Clear();
  }

  // Zjistim pocet pouzivanych pohlavi
  int GenderCount = UseBothGenders ? 2 : 1;

  // Ulozim krivky nebo pocatecni cilove hmotnosti pro pouzita pohlavi
  for (Gender = 0; Gender < GenderCount; Gender++) {
    if (UseCurves) {
      // Pridam vsechny body krivky (krivka je zakoncena hmotnosti CURVE_END_WEIGHT)
      for (int Day = 0; Day < CURVE_MAX_POINTS && Flock->GrowthCurve[Gender][Day].Weight != CURVE_END_WEIGHT; Day++) {
        if (!GrowthCurve[Gender]->Add(Flock->GrowthCurve[Gender][Day].Day, PrepoctiNaKg(Flock->GrowthCurve[Gender][Day].Weight))) {
          break;
        }
      }
    } else {
      // Ulozim si cilovou hmotnost (v hejnu jako prvni bod krivky)
      InitialWeight[Gender] = PrepoctiNaKg(Flock->GrowthCurve[Gender][0].Weight);
    }
  }
} // Load

//-----------------------------------------------------------------------------
// Ulozeni parametru
//-----------------------------------------------------------------------------

void TFlockClass::Save(TFlock *Flock) {
  // Ulozi parametry do hejna <Flock>
  int i;
  int Gender;
  int Day;

  Flock->Header.Number         = Number;
  KConvertPutString(Flock->Header.Title, Title, FLOCK_NAME_MAX_LENGTH);         // Do nazvu pridam koncove mezery
  Flock->Header.UseBothGenders = UseBothGenders;
  Flock->Header.UseCurves      = UseCurves;
  Flock->Header.WeighFrom      = WeighFrom;
  Flock->Header.WeighTill      = WeighTill;

  // Zjistim pocet pouzivanych pohlavi
  int GenderCount = UseBothGenders ? 2 : 1;

  // Default smazu rustove krivky (zapisu do vsech bodu zakonceni krivky)
  for (Gender = 0; Gender < _GENDER_COUNT; Gender++) {
    for (Day = 0; Day < CURVE_MAX_POINTS; Day++) {
      Flock->GrowthCurve[Gender][Day].Weight = CURVE_END_WEIGHT;
    }  
  }

  // Ulozim krivky nebo pocatecni cilove hmotnosti pro pouzita pohlavi
  for (Gender = 0; Gender < GenderCount; Gender++) {
    if (UseCurves) {
      // Ulozim vsechny body krivky
      for (Day = 0; Day < GrowthCurve[Gender]->Count(); Day++) {
        Flock->GrowthCurve[Gender][Day].Day    = GrowthCurve[Gender]->GetDay(Day);
        Flock->GrowthCurve[Gender][Day].Weight = PrepoctiZKg(GrowthCurve[Gender]->GetWeight(Day));
      }
    } else {
      // Ulozim cilovou hmotnost (v hejnu jako prvni bod krivky)
      Flock->GrowthCurve[Gender][0].Day    = CURVE_FIRST_DAY_NUMBER;
      Flock->GrowthCurve[Gender][0].Weight = PrepoctiZKg(InitialWeight[Gender]);
    }
  }
} // Save

//-----------------------------------------------------------------------------
// Kontrola hodnot
//-----------------------------------------------------------------------------

bool TFlockClass::CheckNumber(int Number) {
  return (Number >= 0 && Number < FLOCKS_MAX_COUNT);
}

bool TFlockClass::CheckTitle(AnsiString Title) {
  if (Title.Length() > FLOCK_NAME_MAX_LENGTH) {
    return false;       // Nazev je moc dlouhy
  }
  for (int i = 1; i <= Title.Length(); i++) {
    if ((Title[i] < 'A' || Title[i] > 'Z') && (Title[i] < '0' || Title[i] > '9') && Title[i] != ' ') {
      return false;     // Nepovoleny znak
    }
  }
  return true;
}

bool TFlockClass::CheckHour(int Hour) {
  return (Hour == FLOCK_TIME_LIMIT_EMPTY || (Hour >= 0 && Hour <= FLOCK_TIME_LIMIT_MAX_HOUR));
}

bool TFlockClass::CheckInitialWeight(double Weight) {
  return (Weight >= PrepoctiNaKg(MIN_TARGET_WEIGHT) && Weight <= PrepoctiNaKg(MAX_TARGET_WEIGHT));
}

