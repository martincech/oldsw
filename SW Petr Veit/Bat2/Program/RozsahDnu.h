//---------------------------------------------------------------------------

#ifndef RozsahDnuH
#define RozsahDnuH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TRozsahDnuForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label2;
        TEdit *OdEdit;
        TEdit *DoEdit;
        TButton *Button1;
        TButton *Button2;
        TLabel *Label3;
        TRadioButton *AktualniRadioButton;
        TRadioButton *VsechnyRadioButton;
        TRadioButton *RozsahRadioButton;
        TBevel *Bevel1;
        void __fastcall OdEditKeyPress(TObject *Sender, char &Key);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TRozsahDnuForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TRozsahDnuForm *RozsahDnuForm;
//---------------------------------------------------------------------------
#endif
