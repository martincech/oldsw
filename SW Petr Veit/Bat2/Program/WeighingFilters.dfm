object WeighingFiltersForm: TWeighingFiltersForm
  Left = 225
  Top = 291
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Predefined filters'
  ClientHeight = 208
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 30
    Height = 13
    Caption = 'Filters:'
  end
  object ListBox: TListBox
    Left = 16
    Top = 32
    Width = 257
    Height = 161
    ItemHeight = 13
    Sorted = True
    TabOrder = 0
  end
  object NewButton: TButton
    Left = 288
    Top = 32
    Width = 75
    Height = 25
    Caption = 'New'
    TabOrder = 1
    OnClick = NewButtonClick
  end
  object EditButton: TButton
    Left = 288
    Top = 64
    Width = 75
    Height = 25
    Caption = 'Edit'
    TabOrder = 2
    OnClick = EditButtonClick
  end
  object DeleteButton: TButton
    Left = 288
    Top = 96
    Width = 75
    Height = 25
    Caption = 'Delete'
    TabOrder = 3
    OnClick = DeleteButtonClick
  end
  object CloseButton: TButton
    Left = 288
    Top = 168
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Close'
    ModalResult = 2
    TabOrder = 4
  end
end
