//---------------------------------------------------------------------------

#ifndef JazykH
#define JazykH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TJazykForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TRadioButton *AnglickyRadioButton;
        TRadioButton *FrancouzskyRadioButton;
        TButton *Button1;
        TBevel *Bevel1;
        TRadioButton *FinskyRadioButton;
        TRadioButton *TureckyRadioButton;
        TRadioButton *DanskyRadioButton;
        TRadioButton *SpanelskyRadioButton;
        TRadioButton *NemeckyRadioButton;
        TRadioButton *RuskyRadioButton;
private:	// User declarations
public:		// User declarations
        __fastcall TJazykForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TJazykForm *JazykForm;
//---------------------------------------------------------------------------
#endif
