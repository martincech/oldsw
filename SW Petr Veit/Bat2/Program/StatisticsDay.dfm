object StatisticsDayForm: TStatisticsDayForm
  Left = 225
  Top = 170
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Statistics'
  ClientHeight = 178
  ClientWidth = 203
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 16
    Top = 120
    Width = 169
    Height = 9
    Shape = bsTopLine
  end
  object TodayRadioButton: TRadioButton
    Left = 16
    Top = 24
    Width = 129
    Height = 17
    Caption = 'Today'
    TabOrder = 0
  end
  object AnotherDayRadioButton: TRadioButton
    Left = 16
    Top = 56
    Width = 137
    Height = 17
    Caption = 'Another day:'
    TabOrder = 1
    OnClick = AnotherDayRadioButtonClick
  end
  object DayEdit: TEdit
    Left = 42
    Top = 78
    Width = 41
    Height = 21
    MaxLength = 3
    TabOrder = 2
    Text = 'DayEdit'
    OnKeyPress = DayEditKeyPress
  end
  object Button1: TButton
    Left = 16
    Top = 136
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 111
    Top = 136
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
