//***************************************************************************
//
//    ScaleSetup.cpp - Setup of scale
//    Version 1.0
//
//***************************************************************************

#include "ScaleSetup.h"


//-----------------------------------------------------------------------------
// Definice
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Konstruktor
//-----------------------------------------------------------------------------

TScaleSetup::TScaleSetup(AnsiString NewName, TConfig *Config, TFlock *Flock) {
  // Prebere parametry z <Config> a <Flock>
  Name = NewName;

  FlockList = new TFlockList();

  LoadConfig(Config);
  LoadFlocks(Flock);
}

//-----------------------------------------------------------------------------
// Destruktor
//-----------------------------------------------------------------------------

TScaleSetup::~TScaleSetup() {
  delete FlockList;
}

//-----------------------------------------------------------------------------
// Prebrani configu
//-----------------------------------------------------------------------------

void TScaleSetup::LoadConfig(TConfig *Config) {
  // Prebere config z <Config>
  IdentificationNumber = Config->IdentificationNumber;
  for (int Gender = GENDER_FEMALE; Gender < _GENDER_COUNT; Gender++) {
    MarginAbove[Gender] = Config->MarginAbove[Gender];
    MarginBelow[Gender] = Config->MarginBelow[Gender];
  }
  Filter             = Config->Filter;
  StabilizationRange = (double)Config->StabilizationRange / 10.0;
  StabilizationTime  = Config->StabilizationTime;
  AutoMode           = Config->AutoMode;
  JumpMode           = Config->JumpMode;
  Units              = Config->Units;
  HistogramRange     = Config->HistogramRange;
  UniformityRange    = Config->UniformityRange;
  Backlight          = Config->Backlight;
  Gsm                = Config->Gsm;
  Rs485              = Config->Rs485;
  WeightCorrectionDay1 = Config->WeightCorrection.Day1;
  WeightCorrectionDay2 = Config->WeightCorrection.Day2;
  WeightCorrectionCorrection = (double)Config->WeightCorrection.Correction / 10.0;   // Bajt na desetiny
}

//-----------------------------------------------------------------------------
// Ulozeni configu
//-----------------------------------------------------------------------------

void TScaleSetup::SaveConfig(TConfig *Config) {
  // Ulozi config do <Config>
  Config->IdentificationNumber = IdentificationNumber;
  for (int Gender = GENDER_FEMALE; Gender < _GENDER_COUNT; Gender++) {
    Config->MarginAbove[Gender] = MarginAbove[Gender];
    Config->MarginBelow[Gender] = MarginBelow[Gender];
  }
  Config->Filter             = Filter;
  Config->StabilizationRange = 10.0 * StabilizationRange;
  Config->StabilizationTime  = StabilizationTime;
  Config->AutoMode           = AutoMode;
  Config->JumpMode           = JumpMode;
  Config->Units              = Units;
  Config->HistogramRange     = HistogramRange;
  Config->UniformityRange    = UniformityRange;
  Config->Backlight          = Backlight;
  Config->Gsm                = Gsm;
  Config->Rs485              = Rs485;

  Config->WeightCorrection.Day1 = WeightCorrectionDay1;
  Config->WeightCorrection.Day2 = WeightCorrectionDay2;
  double d = WeightCorrectionCorrection;    // Musim rucne, jinak spatne zaokrouhluje
  d *= 10.0;
  Config->WeightCorrection.Correction = d;
}

//-----------------------------------------------------------------------------
// Prebrani hejn
//-----------------------------------------------------------------------------

void TScaleSetup::LoadFlocks(TFlock *Flock) {
  // Prebere hejna z <Flock>
  TFlockClass *FlockClass;

  FlockList->Clear();

  for (int FlockNumber = 0; FlockNumber < FLOCKS_MAX_COUNT; FlockNumber++) {
    if (Flock->Header.Number != FlockNumber) {
      // Hejno neni zadane
      Flock++;          // Prejdu na dalsi hejno
      continue;
    }

    // Zalozim nove hejno a ulozim do nej parametry z Flock
    FlockClass = new TFlockClass(Flock);

    // Pridam hejno do seznamu
    FlockList->Add(FlockClass);

    // Prejdu na dalsi hejno
    Flock++;
  }
}

//-----------------------------------------------------------------------------
// Ulozeni hejn
//-----------------------------------------------------------------------------

void TScaleSetup::SaveFlocks(TFlock *Flock) {
  // Ulozi hejna do <Flock>
//  TFlockClass *FlockClass;

  for (int FlockNumber = 0; FlockNumber < FLOCKS_MAX_COUNT; FlockNumber++) {
    if (!FlockList->Exists(FlockNumber)) {
      // Hejno neni definovane
      Flock->Header.Number = FLOCK_EMPTY_NUMBER;        // Oznacim jako nedefinovane
      Flock++;                                          // Prejdu na dalsi hejno
      continue;
    }

    // Nactu hejno
    FlockList->GetFlock(FlockList->GetIndex(FlockNumber))->Save(Flock);

    // Prejdu na dalsi hejno
    Flock++;
  }
}

//-----------------------------------------------------------------------------
// Kontrola hodnot
//-----------------------------------------------------------------------------

bool TScaleSetup::CheckIndentificationNumber(int Value) {
  return (Value >= MIN_IDENTIFICATION_NUMBER && Value <= MAX_IDENTIFICATION_NUMBER);
}

bool TScaleSetup::CheckMargin(int Value) {
  return (Value >= MIN_MARGIN && Value <= MAX_MARGIN);
}

bool TScaleSetup::CheckFilter(int Value) {
  return (Value >= MIN_FILTER && Value <= MAX_FILTER);
}

bool TScaleSetup::CheckStabilizationRange(double Value) {
  return (Value >= (double)(MIN_STABILIZATION_RANGE / 10.0) && Value <= (double)(MAX_STABILIZATION_RANGE / 10.0));
}

bool TScaleSetup::CheckStabilizationTime(int Value) {
  return (Value >= MIN_STABILIZATION_TIME && Value <= MAX_STABILIZATION_TIME);
}

bool TScaleSetup::CheckHistogramRange(int Value) {
  return (Value >= MIN_HISTOGRAM_RANGE && Value <= MAX_HISTOGRAM_RANGE);
}

bool TScaleSetup::CheckUniformityRange(int Value) {
  return (Value >= MIN_UNIFORMITY_RANGE && Value <= MAX_UNIFORMITY_RANGE);
}

bool TScaleSetup::CheckGsmPeriod(int Value) {
  return (Value >= MIN_GSM_PERIOD_MIDNIGHT && Value <= MAX_GSM_PERIOD_MIDNIGHT);
}

bool TScaleSetup::CheckGsmNumber(AnsiString Number) {
  if (Number.Length() > GSM_NUMBER_MAX_LENGTH) {
    return false;
  }
  for (int i = 1; i <= Number.Length(); i++) {
    if (Number[i] < '0' || Number[i] > '9') {
      return false;
    }
  }
  return true;
}

bool TScaleSetup::CheckRs485ReplyDelay(int Value) {
  return (Value >= MIN_RS485_REPLY_DELAY && Value <= MAX_RS485_REPLY_DELAY);
}

bool TScaleSetup::CheckRs485SilentInterval(int Value) {
  return (Value >= MIN_RS485_SILENT_INTERVAL && Value <= MAX_RS485_SILENT_INTERVAL);
}

bool TScaleSetup::CheckCorrectionDay(int Value) {
  return (Value >= 0 && Value <= CURVE_MAX_DAY);
}

bool TScaleSetup::CheckCorrectionCorrection(double Value) {
  return (Value >= 0.1 && Value <= 25.5);
}

