//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "SimulaceVysledky.h"
#include "Simulate.h"
#include "DM.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TSimulaceVysledkyForm *SimulaceVysledkyForm;


// --------------------------------------------------------------------------
// Zobrazeni statistiky
// --------------------------------------------------------------------------

#define LOGGER_SIZE     (FL_LOGGER_SAMPLES - 1)         // Max. pocet vzorku v loggeru

void TSimulaceVysledkyForm::ZobrazStatistiku() {
  // Zobrazi statistiku za vybrane pohlavi
  TStatistic *Stat;
  float Average, Sigma;
//  int TotalUni;          // Uniformita ze vsech vzorku (i presahujici kapacitu loggeru)
  int LimitedUni;        // Uniformita ze vzorku loggeru (tj. poslednich FL_LOGGER_SAMPLES - 1 vzorku)
//  int StatUni;           // Uniformita pomoci Stat
  vector<int> *Samples;
  vector<int>::iterator Iterator;

  if (PohlaviSamiceRadioButton->Checked) {
    Stat = &Simulate.StatSamice;
    Samples = &Simulate.Samples[GENDER_FEMALE];
  } else {
    Stat = &Simulate.StatSamci;
    Samples = &Simulate.Samples[GENDER_MALE];
  }

  Average = StatAverage(Stat);
  Sigma = StatSigma(Stat);
  if (PohlaviSamiceRadioButton->Checked) {
    CilovaHmotnostLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg(Simulate.NormovanaHmotnostSamice));
  } else {
    CilovaHmotnostLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg(Simulate.NormovanaHmotnostSamci));
  }
  PrumerLabel->Caption = FormatFloat(FORMAT_HMOTNOSTI, PrepoctiNaKg((int)Average));   // V Keilu to delam pres int
  PocetLabel->Caption = Stat->Count;
  SigmaLabel->Caption = FormatFloat("0.000", (double)Sigma / 1000.0);
  CvLabel->Caption = FormatFloat("0.0", StatVariation(Average,Sigma));

  // Uniformita ze vsech vzorku
/*  StatRealUniformityInit(Average * (float)(100 - Simulate.RozsahUniformity) / 100.0,
                         Average * (float)(100 + Simulate.RozsahUniformity) / 100.0,
                         Average);
  for (Iterator = Samples->begin(); Iterator != Samples->end(); Iterator++) {
    StatRealUniformityAdd(*Iterator);
  }
  TotalUni = StatRealUniformityGet();*/

  // Uniformita z poslednich LOGGER_SIZE vzorku (odpovida vaze)
  int SkipSamples = Samples->size() - LOGGER_SIZE;
  Iterator = Samples->begin();
  if (SkipSamples > 0) {
    // Je treba neco preskocit
    for ( ; SkipSamples > 0; SkipSamples--) {
      Iterator++;
    }
  }
  StatRealUniformityInit(Average * (float)(100 - Simulate.RozsahUniformity) / 100.0,
                         Average * (float)(100 + Simulate.RozsahUniformity) / 100.0,
                         Average);
  for ( ; Iterator != Samples->end(); Iterator++) {
    StatRealUniformityAdd(*Iterator);
  }
  LimitedUni = StatRealUniformityGet();

  // Uniformita pomoci Stat
//  StatUni = StatUniformity(Average, Sigma, Simulate.RozsahUniformity);

  // Zobrazim uniformitu
  UniLabel->Caption = FormatFloat("0", LimitedUni);             // Zobrazuju uniformitu z loggeru
}

// --------------------------------------------------------------------------
// Zobrazeni histogramu
// --------------------------------------------------------------------------

void TSimulaceVysledkyForm::ZobrazHistogram() {
  // Zobrazi histogram za vybrane pohlavi
  THistogram *Histogram;

  if (PohlaviSamiceRadioButton->Checked) {
    Histogram = &Simulate.HistSamice;
  } else {
    Histogram = &Simulate.HistSamci;
  }

  // Zaznamy
  HistogramChart->Series[0]->Clear();                   // Vymazu z grafu vsechny predchozi hodnoty
  for (int i = 0; i < HISTOGRAM_SLOTS; i++) {
    int j = Histogram->Center + (i - HISTOGRAM_SLOTS / 2) * Histogram->Step;
    float f = PrepoctiNaKg(j);
//    HistogramChart->Series[0]->AddXY(PrepoctiNaKg(Histogram->Center + (i - HISTOGRAM_SLOTS / 2) * Histogram->Step),
    HistogramChart->Series[0]->AddXY(f,
                                     Histogram->Slot[i],
                                     "",   // Popisek
                                     clTeeColor);
  }//for i
}


//---------------------------------------------------------------------------
__fastcall TSimulaceVysledkyForm::TSimulaceVysledkyForm(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TSimulaceVysledkyForm::FormShow(TObject *Sender)
{
  PohlaviSamiceRadioButton->Checked = true;
  PohlaviPanel->Visible = Simulate.RozdelovatPohlavi;
  ZobrazStatistiku();
  ZobrazHistogram();
}
//---------------------------------------------------------------------------
void __fastcall TSimulaceVysledkyForm::PohlaviSamiceRadioButtonClick(
      TObject *Sender)
{
  ZobrazStatistiku();
  ZobrazHistogram();
}
//---------------------------------------------------------------------------


