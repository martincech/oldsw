//---------------------------------------------------------------------------

#ifndef SimulaceVysledkyH
#define SimulaceVysledkyH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TSimulaceVysledkyForm : public TForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl1;
        TTabSheet *StatistikaTabSheet;
        TTabSheet *HistogramTabSheet;
        TLabel *Label3;
        TLabel *CilovaHmotnostLabel;
        TLabel *Label5;
        TLabel *PrumerLabel;
        TLabel *Label7;
        TLabel *PocetLabel;
        TLabel *Label9;
        TLabel *SigmaLabel;
        TLabel *Label11;
        TLabel *UniLabel;
        TLabel *Label13;
        TLabel *CvLabel;
        TLabel *CilovaHmotnostJednotkyLabel;
        TLabel *PrumerJednotkyLabel;
        TLabel *Label20;
        TLabel *Label21;
        TChart *HistogramChart;
        TBarSeries *Series1;
        TPanel *PohlaviPanel;
        TLabel *Label2;
        TSpeedButton *SpeedButton1;
        TSpeedButton *SpeedButton2;
        TRadioButton *PohlaviSamiceRadioButton;
        TRadioButton *PohlaviSamciRadioButton;
        TPanel *Panel1;
        TButton *Button1;
        TButton *Button2;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall PohlaviSamiceRadioButtonClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TSimulaceVysledkyForm(TComponent* Owner);
        void ZobrazStatistiku();
        void ZobrazHistogram();
};
//---------------------------------------------------------------------------
extern PACKAGE TSimulaceVysledkyForm *SimulaceVysledkyForm;
//---------------------------------------------------------------------------
#endif
