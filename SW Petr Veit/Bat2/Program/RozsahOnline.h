//---------------------------------------------------------------------------

#ifndef RozsahOnlineH
#define RozsahOnlineH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TRozsahOnlineForm : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label2;
        TLabel *Label3;
        TBevel *Bevel1;
        TEdit *OdEdit;
        TEdit *DoEdit;
        TButton *Button1;
        TButton *Button2;
        TLabel *Label1;
        TEdit *PrumerEdit;
        TLabel *Label4;
        TLabel *Label5;
        void __fastcall OdEditKeyPress(TObject *Sender, char &Key);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TRozsahOnlineForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TRozsahOnlineForm *RozsahOnlineForm;
//---------------------------------------------------------------------------
#endif
