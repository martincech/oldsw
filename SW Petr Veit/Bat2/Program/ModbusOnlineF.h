//---------------------------------------------------------------------------

#ifndef ModbusOnlineScalesH
#define ModbusOnlineScalesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>

#include "ModbusOnline.h"
#include "ModbusOnlineThread.h"
#include "ModbusThread.h"
#include <Menus.hpp>
#include <Buttons.hpp>

//---------------------------------------------------------------------------
class TModbusOnlineForm : public TForm
{
__published:	// IDE-managed Components
        TStringGrid *ScalesStringGrid;
        TTimer *OnlineTimer;
        TMainMenu *MainMenu1;
        TMenuItem *Scale1;
        TMenuItem *Options1;
        TMenuItem *Addremovescales1;
        TMenuItem *Communicationparameters1;
        TMenuItem *Start1;
        TMenuItem *Stop1;
        TMenuItem *Pause1;
        TMenuItem *Continue1;
        TMenuItem *Downloadresults1;
        TMenuItem *Dateandtime1;
        TMenuItem *Setup1;
        TMenuItem *Compare1;
        TPanel *TopPanel;
        TBevel *Bevel2;
        TBevel *Bevel3;
        TSpeedButton *StartSpeedButton;
        TSpeedButton *StopSpeedButton;
        TSpeedButton *DownloadSpeedButton;
        TSpeedButton *PauseSpeedButton;
        TSpeedButton *SetupSpeedButton;
        TSpeedButton *PortSpeedButton;
        TSpeedButton *AddSpeedButton;
        TBevel *Bevel7;
        TBevel *Bevel8;
        TSpeedButton *ContinueSpeedButton;
        TBevel *Bevel1;
        TSpeedButton *DateTimeSpeedButton;
        TSpeedButton *CompareSpeedButton;
        TMenuItem *Communicationerrors1;
        TMenuItem *N1;
        TBevel *Bevel4;
        TSpeedButton *ErrorsSpeedButton;
        TMenuItem *N2;
        TMenuItem *N3;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall OnlineTimerTimer(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Addremovescales1Click(TObject *Sender);
        void __fastcall Communicationparameters1Click(TObject *Sender);
        void __fastcall Start1Click(TObject *Sender);
        void __fastcall Pause1Click(TObject *Sender);
        void __fastcall Continue1Click(TObject *Sender);
        void __fastcall Stop1Click(TObject *Sender);
        void __fastcall Dateandtime1Click(TObject *Sender);
        void __fastcall Downloadresults1Click(TObject *Sender);
        void __fastcall Setup1Click(TObject *Sender);
        void __fastcall Compare1Click(TObject *Sender);
        void __fastcall ScalesStringGridClick(TObject *Sender);
        void __fastcall Communicationerrors1Click(TObject *Sender);
private:	// User declarations
        TModbusOnline *ModbusOnline;
        TModbusOnlineThread *ModbusOnlineThread;        // Nacitani stavu vsech vah v tabulce
        TModbusThread       *ModbusCommandThread;       // Provadeni prikazu u 1 vahy

        void ClearTable();
        // Vymazu celou tabulku (nemazu zahlavi)
        void DisplayScaleList();
        // Prekresli tabulku vah
        void QuitOnlineThread();
        // Ukonci online thread
        void StopReading();
        // Zastavi nacitani na pozadi
        void StartReading();
        // Zahaji nacitani na pozadi

        void LoadScales();
        // Nacte seznam vah z databaze

        TModbusOnlineScale *GetSelectedScale();
        // Vrati ukazatel na vahu vybranou v seznamu

        void PauseWeighing(bool Pause);
        // Zapauzuje/odpauzuje vazeni

        void ActivateMenu(TModbusOnlineScale *Scale);
        // Podle stavu vahy <Scale> aktivuje/zakaze menu

        void ReadAndDisplaySelectedScale();
        // Nacte kompletni stav vybrane vahy a zobrazi

        AnsiString GetScaleName(TModbusScale *Scale);
        // Vrati nazev vahy spolu s adresou

        void CreateCommandThread(TModbusScale *Scale, int Repetitions);
        // Zalozi novy thread prikazu s vahou <Scale> a <Repetitions> opakovani pri chybe
        void QuitCommandThread();
        // Ukonci thread prikazu
        bool StartCommand(TModbusThreadCommand Command, int Value);
        // Odstartuje prikaz <Command> s hodnotou <Value>
        bool ExecuteCommand(TModbusThreadCommand Command);
        // Vykona prikaz <Command>

public:		// User declarations
        bool FileSaved;         // V prubehu otevreni okna stahnul zaznamy a ulozil je do souboru

        __fastcall TModbusOnlineForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TModbusOnlineForm *ModbusOnlineForm;
//---------------------------------------------------------------------------
#endif
