﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;             // Decimal separator
using Veit.Bat2Modbus;

namespace Bat2sModbusSms {
    /// <summary>
    /// Create SMS from Modbus state
    /// </summary>
    static class SmsBuilder {

        /// <summary>
        /// Convert float number to string with '.' as a decimal separator
        /// </summary>
        /// <param name="value">Float value to compare</param>
        /// <returns>Converted string</returns>
        private static string FloatToString(float value) {
            // Nahradim desetinny oddelovac teckou
            return  value.ToString("N3").Replace(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator, ".");
        }

        /// <summary>
        /// Create SMS text from Mobdus scale state
        /// </summary>
        /// <param name="state">Scale state</param>
        /// <returns>SMS text</returns>
        private static string CreateWeighingSms(CurrentState state) {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("SCALE ");
            stringBuilder.Append(state.ScaleName);

            stringBuilder.Append(" DAY ");
            stringBuilder.Append(state.StatisticsBuilder.Statistics.DayNumber.ToString());
            stringBuilder.Append(" ");
            stringBuilder.Append(state.StatisticsBuilder.Statistics.DateTimeBuilder.Day.ToString());
            stringBuilder.Append(".");
            stringBuilder.Append(state.StatisticsBuilder.Statistics.DateTimeBuilder.Month.ToString());
            stringBuilder.Append(".");
            stringBuilder.Append(state.StatisticsBuilder.Statistics.DateTimeBuilder.Year.ToString());

            stringBuilder.Append(" Cnt ");
            stringBuilder.Append(state.StatisticsBuilder.Statistics.Count.ToString());

            stringBuilder.Append(" Avg ");
            stringBuilder.Append(SmsBuilder.FloatToString(state.StatisticsBuilder.Statistics.Average));

            stringBuilder.Append(" Gain ");
            stringBuilder.Append(SmsBuilder.FloatToString(state.DailyGain));

            stringBuilder.Append(" Sig ");
            stringBuilder.Append(SmsBuilder.FloatToString(state.StatisticsBuilder.Statistics.Sigma));

            stringBuilder.Append(" Cv ");
            int cv = (int)state.StatisticsBuilder.Statistics.Cv;
            stringBuilder.Append(cv.ToString());

            stringBuilder.Append(" Uni ");
            stringBuilder.Append(state.StatisticsBuilder.Statistics.Uniformity.ToString());

            return stringBuilder.ToString();
        }

        public static string CreateSms(CurrentState state) {
            // Osetrim stav, kdy vaha nevazi
            if (!state.IsStarted) {
                return "STOPPED";
            } else {
                if (state.IsWaiting) {
                    return "WAITING";
                }
            }

            // Vaha vazi, vytvorim SMS se statistikou
            return CreateWeighingSms(state);
        }
    }
}
