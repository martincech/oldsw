﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Globalization;

namespace Bat2sModbusSms {
    /// <summary>
    /// Codes for supported languages
    /// </summary>
    public enum SwLanguage {
        UNDEFINED,      // Poradi se nesmi menit
        ENGLISH,        
        FRENCH,
        _COUNT
    }

    public static class SwLanguageClass {
        // Jmena jazyku
        private const string STR_ENGLISH    = "English";
        private const string STR_FRENCH     = "Français";

        public static List<string> GetLanguageStrings() {
            List<string> list = new List<string>();
            for (SwLanguage language = SwLanguage.ENGLISH; language < SwLanguage._COUNT; language++) {
                list.Add(GetLanguageString(language));
            }
            list.Sort();        // Setridim podle abecedy
            return list;
        }

        public static string GetLanguageString(SwLanguage language) {
            switch (language) {
                case SwLanguage.ENGLISH:
                    return STR_ENGLISH;
                case SwLanguage.FRENCH:
                    return STR_FRENCH;
                default:
                    return "";
            }
        }

        public static SwLanguage GetLanguageCode(string languageStr) {
            if (languageStr == STR_ENGLISH) {
                return SwLanguage.ENGLISH;
            } else if (languageStr == STR_FRENCH) {
                return SwLanguage.FRENCH;
            } else {
                return SwLanguage.UNDEFINED;
            }
        }
        
        public static void SetLanguage(SwLanguage language) {
            string str;

            // Prevedu kod jazyka na string
            switch (language) {
                case SwLanguage.FRENCH:
                    str = "fr";
                    break;
                default:
                    str = "en";
                    break;
            }

            // Nastavim jazyk pro vsechna okna a resources
            CultureInfo cultureInfo = new System.Globalization.CultureInfo(str);
            Properties.Resources.Culture          = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
        }
    }
}
