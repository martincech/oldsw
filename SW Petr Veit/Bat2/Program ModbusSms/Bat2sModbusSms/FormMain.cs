﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Veit.Bat2Modbus;
using Veit.DrawingControl;
using Veit.GsmWorker;

namespace Bat2sModbusSms {
    enum ActionAfterCancel {
        NONE,
        SETUP,      // Zobrazi setup
        QUIT,       // Ukonci aplikaci
        SEND_SMS    // Odesle SMS
    }
    
    public partial class FormMain : Form {
        /// <summary>
        /// Worker for Modbus scales
        /// </summary>
        Bat2ModbusOnlineWorker modbusWorker;
        
        /// <summary>
        /// List of current scale state
        /// </summary>
        List<ScaleState> scaleStateCollection;

        /// <summary>
        /// List of scale state from previous step (needed for midnight detection)
        /// </summary>
        List<CurrentState> lastStateCollection;

        /// <summary>
        /// List of SMS messages waiting to be sent
        /// </summary>
        List<string> smsTextCollection;

        /// <summary>
        /// Index of scale in the list
        /// </summary>
        int currentScaleIndex;

        /// <summary>
        /// Action that should be performed after the Modbus worker is cancelled
        /// </summary>
        ActionAfterCancel actionAfterCancel;

        /// <summary>
        /// Worker for GSM standby operation
        /// </summary>
        SmsStandbyWorkers smsStandbyWorkers;

        /// <summary>
        /// Worker for sending SMS
        /// </summary>
        SmsSendWorker smsSendWorker;
        
        public FormMain() {
            InitializeComponent();

            // Vytvorim seznam SMS
            smsTextCollection = new List<string>();
            
            // Vytvorim a rozjedu vsechny workery
            StartWorkers();
        }

        private Device.Serial.Uart.Parity ConvertParity(ModbusParity parity) {
            switch (parity) {
                case ModbusParity.EVEN:
                    return Device.Serial.Uart.Parity.Even;
                case ModbusParity.MARK:
                    return Device.Serial.Uart.Parity.Mark;
                case ModbusParity.NONE:
                    return Device.Serial.Uart.Parity.None;
                default:
                    return Device.Serial.Uart.Parity.Odd;
            }
        }

        private Veit.ModbusProtocol.ModbusPacket.PacketMode ConvertProtocol(ModbusProtocol protocol) {
            switch (protocol) {
                case ModbusProtocol.ASCII:
                    return Veit.ModbusProtocol.ModbusPacket.PacketMode.ASCII;
                default:
                    return Veit.ModbusProtocol.ModbusPacket.PacketMode.RTU;
            }
        }
        
        /// <summary>
        /// Create list of Modbus scales and worker
        /// </summary>
        private void CreateModbusWorker() {
            // Vytvorim seznam vah
            scaleStateCollection = new List<ScaleState>();
            for (int i = Program.Setup.ModbusScales.FirstAddress; i <= Program.Setup.ModbusScales.LastAddress; i++) {
                scaleStateCollection.Add(new ScaleState(Program.Setup.ModbusScales.PortNumber, i));
            }
/*            for (int i = 0; i < 15; i++) {
                scaleStateCollection.Add(new ScaleState(Program.Setup.ModbusScales.PortNumber, 2));
            }*/

            // Vytvorim seznam poslednich stavu vsech vah (poradi odpovida seznamu vah scaleStateCollection)
            lastStateCollection = new List<CurrentState>();
            for (int i = Program.Setup.ModbusScales.FirstAddress; i <= Program.Setup.ModbusScales.LastAddress; i++) {
                lastStateCollection.Add(null);      // Default neznam zadny posledni stav
            }
/*            for (int i = 0; i < 15; i++) {
                lastStateCollection.Add(null);      // Default neznam zadny posledni stav
            }*/

            // Vytvorim worker
            List<Bat2Slave> scaleCollection = new List<Bat2Slave>();
            foreach (ScaleState state in scaleStateCollection) {
                scaleCollection.Add(state.Slave);
            }
            modbusWorker = new Bat2ModbusOnlineWorker(ModbusWorker_ProgressChanged,
                                                      ModbusWorker_RunWorkerCompleted,
                                                      Program.Setup.ModbusProtocol.Speed,
                                                      ConvertParity(Program.Setup.ModbusProtocol.Parity),
                                                      8,
                                                      Program.Setup.ModbusProtocol.ReplyTimeout,
                                                      Program.Setup.ModbusProtocol.SilentInterval,
                                                      Program.Setup.ModbusProtocol.NumberOfAttempts,
                                                      ConvertProtocol(Program.Setup.ModbusProtocol.Protocol),
                                                      scaleCollection);

            actionAfterCancel = ActionAfterCancel.NONE;
        }

        /// <summary>
        /// Start Modbus worker
        /// </summary>
        private void StartModbusWorker() {
            currentScaleIndex = 0;
            modbusWorker.Start();
        }

        /// <summary>
        /// Create GSM Standby workers
        /// </summary>
        private void CreateGsmStandbyWorkers() {
            List<int> portList = new List<int>();
            portList.Add(Program.Setup.Gsm.PortNumber);
            smsStandbyWorkers = new SmsStandbyWorkers(portList, GsmStandbyWorkers_ProgressChanged, GsmStandbyWorkers_RunWorkerCompleted);
        }

        /// <summary>
        /// Start GSM Standby workers
        /// </summary>
        private void StartGsmStandbyWorkers() {
            smsStandbyWorkers.Start();
        }

        /// <summary>
        /// Create GSM send worker
        /// </summary>
        private void CreateGsmSendWorker() {
            smsSendWorker = new SmsSendWorker(Program.Setup.Gsm.PortNumber, GsmSendWorker_ProgressChanged, GsmSendWorker_RunWorkerCompleted);
        }

        /// <summary>
        /// Check if any worker is running
        /// </summary>
        /// <returns>True if running</returns>
        private bool IsWorkerRunning() {
            if (modbusWorker.IsBusy) {
                return true;
            }
            if (smsStandbyWorkers.IsBusy) {
                return true;
            }
            if (smsSendWorker.IsBusy) {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Create all workers and start workers for idle operation
        /// </summary>
        private void StartWorkers() {
            // Vytvorim seznam vah a worker, automaticky rozjedu
            CreateModbusWorker();
            StartModbusWorker();

            // Vytvorim worker pro cteni sily signalu z GSM modemu, automaticky rozjedu
            CreateGsmStandbyWorkers();
            StartGsmStandbyWorkers();

            // Vytvorim worker pro posilani SMS, ponecham zastaveny
            CreateGsmSendWorker();
        }

        /// <summary>
        /// Stop all workers
        /// </summary>
        /// <param name="actionAfterCancel">Action to perform after the workers are stopped</param>
        private void StopWorkers(ActionAfterCancel actionAfterCancel) {
            this.actionAfterCancel = actionAfterCancel;     // Akci provede ten worker, ktery se ukonci jako posledni
            modbusWorker.Stop();
            smsStandbyWorkers.Stop();
            smsSendWorker.Stop();
        }
        
        /// <summary>
        /// Perform action after all workers are cancelled
        /// </summary>
        private void PerformActionAfterCancel() {
            // Akci provede vzdy az ten worker, ktery se zastavi jako posledni
            if (IsWorkerRunning()) {
                return;     // Nektery worker jeste bezi, cekam dal
            }

            switch (actionAfterCancel) {
                case ActionAfterCancel.SETUP:
                    actionAfterCancel   = ActionAfterCancel.NONE;     // Snuluju hned, pak by mohla byt vyjimka
                    buttonSetup.Enabled = true;
                    this.Cursor         = Cursors.Default;
                    ShowSetup();
                    break;

                case ActionAfterCancel.QUIT:
                    Close();
                    break;
            }
        }
        
        /// <summary>
        /// Show setup form. After it is closed, the Modbus worker is started again.
        /// </summary>
        private void ShowSetup() {
            FormSetup form = new FormSetup(Program.Setup);
            if (form.ShowDialog() == DialogResult.OK) {
                Program.DatabaseSetup.SaveSetup();
            }

            // Vytvorim a rozjedu vsechny workery
            StartWorkers();
        }
        
        /// <summary>
        /// Select scale in the grid
        /// </summary>
        /// <param name="index">Index to select</param>
        private void SelectScale(int index) {
            if (dataGridViewScales.Rows.Count == 0) {
                return;     // Tabulka je prazdna
            }
            if (index < 0) {
                return;     // Zadny vyber
            }
            if (index >= dataGridViewScales.Rows.Count) {
                index = dataGridViewScales.Rows.Count - 1;     // Omezim
            }
            dataGridViewScales.ClearSelection();
            dataGridViewScales.Rows[index].Selected = true;
        }

        /// <summary>
        /// Redraw all data
        /// </summary>
        private void RedrawScales() {
            DrawingControl.SuspendDrawing(dataGridViewScales);
            try {
                // Zapamatuju si vybrany radek
                int cursorPosition = dataGridViewScales.SelectedRows.Count == 0 ? -1 : dataGridViewScales.SelectedRows[0].Index;
                
                dataGridViewScales.Rows.Clear();

                for (int i = 0; i < scaleStateCollection.Count; i++) {
                    ScaleState state = scaleStateCollection[i];
                    string indexText = i == currentScaleIndex ? ">" : "";
                    
                    // Pokud je vaha offline, zobrazuju jen index a jmeno vahy
                    if (!state.IsOnline) {
                        dataGridViewScales.Rows.Add(indexText, 
                                                    state.Slave.Address.ToString(),
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    "",
                                                    state.ErrorCounter.ToString() + " / " + state.MessageCounter.ToString());
                        continue;
                    }

                    string modeText;
                    if (state.State.IsStarted) {
                        if (state.State.IsPaused) {
                            modeText = Properties.Resources.WEIGHING_PAUSED;
                        } else {
                            modeText = Properties.Resources.WEIGHING_STARTED;
                        }
                    } else {
                        if (state.State.IsWaiting) {
                            modeText = Properties.Resources.WEIGHING_WAITING;
                        } else {
                            modeText = Properties.Resources.WEIGHING_STOPPED;
                        }
                    }

                    dataGridViewScales.Rows.Add(indexText, 
                                                state.Slave.Address.ToString(),
                                                state.State.ScaleName,
                                                state.State.DateTimeBuilder.DateTime.ToString(),
                                                modeText,
                                                state.State.IsStarted ? state.State.StatisticsBuilder.Statistics.DayNumber.ToString() : "",
                                                state.State.IsStarted ? DisplayFormat.RoundWeight(state.State.StatisticsBuilder.Statistics.Target) : "",
                                                state.State.IsStarted ? state.State.StatisticsBuilder.Statistics.Count.ToString() : "",
                                                state.State.IsStarted ? DisplayFormat.RoundWeight(state.State.StatisticsBuilder.Statistics.Average) : "",
                                                state.State.IsStarted ? DisplayFormat.RoundWeight(state.State.DailyGain) : "",
                                                state.State.IsStarted ? state.State.StatisticsBuilder.Statistics.Uniformity.ToString() : "",
                                                state.ErrorCounter.ToString() + " / " + state.MessageCounter.ToString());
                }

                // Na zaver vyberu radek, ktery byl predtim vybrany
                SelectScale(cursorPosition);

            } finally {
                DrawingControl.ResumeDrawing(dataGridViewScales);
            }
        }

        /// <summary>
        /// Save state to the list of last states
        /// </summary>
        /// <param name="index">Index in the list</param>
        /// <param name="lastState">State to save</param>
        private void SaveLastState(int index, CurrentState lastState) {
            lastStateCollection.RemoveAt(index);
            lastStateCollection.Insert(index, new CurrentState(lastState));
        }

        /// <summary>
        /// Check if midnight SMS should be sent
        /// </summary>
        /// <param name="dayNumber">Day number</param>
        /// <returns>True if should be sent</returns>
        private bool CheckSendingDay(int dayNumber) {
            // Do dne DayMidnight1 vcetne posilam s periodou PeriodMidnight1, pak s periodou PeriodMidnight2
            if (dayNumber <= Program.Setup.Gsm.DayMidnight1) {
                if (dayNumber % Program.Setup.Gsm.PeriodMidnight1 == 0) {
                    return true;    // Dnes mam posilat => poslu SMS o pulnoci
                }
            } else {
                if (dayNumber % Program.Setup.Gsm.PeriodMidnight2 == 0) {
                    return true;    // Dnes mam posilat => poslu SMS o pulnoci
                }
            }
            return false;
        }

        /// <summary>
        /// Add SMS in the sending buffer
        /// </summary>
        /// <param name="text">SMS text</param>
        private void AddSms(string text) {
            smsTextCollection.Add(text);
            listBoxSms.Items.Add(text);
        }
        
        /// <summary>
        /// Check if midnight has occured
        /// </summary>
        /// <param name="scaleState">Current scale state</param>
        /// <param name="lastState">Scale state in previous step</param>
        private void CheckMidnight(ScaleState scaleState, CurrentState lastState) {
            if (lastState == null) {
                return;         // Posledni stav jeste nebyl ulozen, musim pockat na dalsi krok
            }

            // SMS posilam jen pokud vaha vazi
            if (!scaleState.State.IsStarted) {
                return;
            }
            
            // Pokud jsou datumy rozdilne, doslo k pulnoci
            if (scaleState.State.DateTimeBuilder.DateTime.Date == lastState.DateTimeBuilder.DateTime.Date) {
                return;         // Datumy jsou shodne
            }

            // Doslo k pulnoci, zkontroluju, zda mam tento den SMS posilat
            if (!CheckSendingDay(lastState.StatisticsBuilder.Statistics.DayNumber)) {
                return;
            }
            
            // Vytvorim pulnocni SMS a ulozim ji do fronty SMS k odeslani
            AddSms(SmsBuilder.CreateSms(lastState));
        }

        /// <summary>
        /// Check number of midnight SMS and start sending if needed
        /// </summary>
        private void CheckMidnightSms() {
            if (smsTextCollection.Count == 0) {
                return;
            }

            // V seznamu je nejaka SMS k odeslani, ukoncim worker pro cteni sily signalu a zahajim vysilani SMS
            actionAfterCancel = ActionAfterCancel.SEND_SMS;
            smsStandbyWorkers.Stop();
        }

        /// <summary>
        /// Starts sending of first SMS in the list
        /// </summary>
        private void StartSendingFirstSms() {
            // Vytvorim seznam cisel v mezinarodnim formatu, tj. vcetne pocatecniho "+"
            List<string> numberCollection = new List<string>();
            foreach (string number in Program.Setup.Gsm.MidnightNumberCollection) {
                numberCollection.Add("+" + number);
            }

            // Zahajim odesilani
            smsSendWorker.Start(numberCollection, smsTextCollection[0]);
        }
        
        /// <summary>
        /// Start sending of first SMS in the list
        /// </summary>
        private void StartSmsSend() {
            if (smsTextCollection.Count == 0) {
                return;
            }

            // Vezmu prvni SMS v seznamu a odeslu ji na vsechna telefonni cisla
            StartSendingFirstSms();
        }
        
        private void ModbusWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Preberu data
            WorkerData data        = (WorkerData)e.UserState;
            currentScaleIndex      = e.ProgressPercentage;       // Ukladam globalne
            ScaleState scaleState  = scaleStateCollection[currentScaleIndex];
            CurrentState lastState = lastStateCollection[currentScaleIndex];

            scaleState.MessageCounter = data.MessageCounter;
            scaleState.ErrorCounter   = data.ErrorCounter;

            switch (data.Event) {
                case WorkerEvent.READ_ERROR:
                    scaleState.IsOnline = false;
                    break;

                case WorkerEvent.READ_OK:
                    scaleState.IsOnline = true;
                    scaleState.State    = data.State;

                    // Porovnam aktualni stav s poslednim stavem a detekuju pulnoc
                    CheckMidnight(scaleState, lastState);
                
                    // Ulozim si posledni stav
                    SaveLastState(currentScaleIndex, scaleState.State);
                    
                    break;
            }

            RedrawScales();
        }

        private void ModbusWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //e.Cancelled: use this if you need to determine how the worker thread completed. 
            //             Must set e.Cancel to True in DoWork (False by default) for this be reliable

            //e.Result   : Like this.piCalcWorker.ReportProgress() allows us to pass a user token
            //             to ProgressChanged via the ProgressChangedEventArgs, you can pass a result
            //             object back to RunWorkerCompleted vie RunWorkerCompletedEventArgs.Result,
            //             but only if not cancelled, as e.Result, when cancelled, is an exception:
            //             Result	'e.Result' threw an exception of type 'System.InvalidOperationException'	object {System.InvalidOperationException}

            if (!e.Cancelled) {
                return;     // Worker nemuze skoncit sam od sebe, jede v nekonecne smycce
            }

            // Provedu akci po ukonceni workeru
            PerformActionAfterCancel();
        }

        private void GsmStandbyWorkers_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Preberu status
            SmsStatus status = (SmsStatus)e.UserState;

            // Zobrazim stav modemu
            switch (status.Event) {
                case SmsEvent.READY:
                    labelGsmStatus.Text = status.Event.ToString() + "  " + status.OperatorName + "  " + status.SignalStrength + "%";

                    // Pokud je v seznamu nejaka SMS k poslani, odeslu
                    CheckMidnightSms();

                    break;

                default:
                    labelGsmStatus.Text = status.Event.ToString();
                    break;
            }
        }

        private void GsmStandbyWorkers_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //e.Cancelled: use this if you need to determine how the worker thread completed. 
            //             Must set e.Cancel to True in DoWork (False by default) for this be reliable

            //e.Result   : Like this.piCalcWorker.ReportProgress() allows us to pass a user token
            //             to ProgressChanged via the ProgressChangedEventArgs, you can pass a result
            //             object back to RunWorkerCompleted vie RunWorkerCompletedEventArgs.Result,
            //             but only if not cancelled, as e.Result, when cancelled, is an exception:
            //             Result	'e.Result' threw an exception of type 'System.InvalidOperationException'	object {System.InvalidOperationException}


            if (!e.Cancelled) {
                return;     // Worker nemuze skoncit sam od sebe, jede v nekonecne smycce
            }

            // Provedu akci po ukonceni workeru
            switch (actionAfterCancel) {
                case ActionAfterCancel.SEND_SMS:
                    // Chce poslat SMS, ukoncuje se pouze worker pro periodecke cteni sily signalu,
                    // worker pro Modbus jede dal
                    StartSmsSend();
                    break;
                
                default:
                    // Ukoncuje vsechny workery
                    PerformActionAfterCancel();
                    break;
            }
            
        }

        private void GsmSendWorker_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            // Preberu status
            SmsStatus status = (SmsStatus)e.UserState;

            // Zobrazim stav modemu
            switch (status.Event) {
                case SmsEvent.SMS_DELETING:
                case SmsEvent.SMS_DELETE_OK:
                case SmsEvent.SMS_DELETE_ERROR:
                case SmsEvent.SMS_SENDING:
                case SmsEvent.SMS_SEND_OK:
                case SmsEvent.SMS_SEND_ERROR:
                    labelGsmStatus.Text = status.Event.ToString() + " (" + e.ProgressPercentage.ToString() + ")";
                    break;
            }
        }

        private void GsmSendWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //e.Cancelled: use this if you need to determine how the worker thread completed. 
            //             Must set e.Cancel to True in DoWork (False by default) for this be reliable

            //e.Result   : Like this.piCalcWorker.ReportProgress() allows us to pass a user token
            //             to ProgressChanged via the ProgressChangedEventArgs, you can pass a result
            //             object back to RunWorkerCompleted vie RunWorkerCompletedEventArgs.Result,
            //             but only if not cancelled, as e.Result, when cancelled, is an exception:
            //             Result	'e.Result' threw an exception of type 'System.InvalidOperationException'	object {System.InvalidOperationException}

            if (e.Cancelled) {
                // Rucne ukoncil posilani SMS

                // Provedu akci po ukonceni workeru
                PerformActionAfterCancel();

                return;
            }

            // Odeslal SMS
            SmsStatus status = (SmsStatus)e.UserState;

            // Smazu odeslanou SMS (prvni v seznamu)
            if (smsTextCollection.Count == 0) {
                return;
            }
            smsTextCollection.RemoveAt(0);
            listBoxSms.Items.RemoveAt(0);

            // Pokud je v seznamu dalsi SMS, zahajim odesilani teto SMS
            // Pokud uz v seznamu zadna SMS neni, zahajim opet periodicke vycitani sily signalu
            if (smsTextCollection.Count > 0) {
                StartSendingFirstSms();
            } else {
                StartGsmStandbyWorkers();
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            // Pred ukoncenim aplikace ukoncim worker a pockam, az se opravdu ukonci
            if (!modbusWorker.IsBusy) {
                return;             // Worker uz nebezi, muzu skoncit
            }

            // Worker bezi, zastavim ho a pockam, az se ukonci
            this.Cursor = Cursors.WaitCursor;
            StopWorkers(ActionAfterCancel.QUIT);
            e.Cancel = true;        // Zatim pockam
        }

        private void buttonSetup_Click(object sender, EventArgs e) {
            if (modbusWorker.IsBusy) {
                // Worker bezi, zastavim ho a az se ukonci, az pote zobrazim okno s nastavenim
                buttonSetup.Enabled = false;
                this.ActiveControl  = null;     // Jinak to vybere vedlejsi button a vypada to blbe
                this.Cursor         = Cursors.WaitCursor;
                StopWorkers(ActionAfterCancel.SETUP);
                return;
            }
            
            // Worker nebezi, muzu zobrazit setup ihned
            ShowSetup();
        }

        private void buttonTestSms_Click(object sender, EventArgs e) {
            // Vytvorim testovaci SMS a ulozim ji do fronty SMS k odeslani
            AddSms(Properties.Resources.TEST_SMS_TEXT);
        }


    }
}
