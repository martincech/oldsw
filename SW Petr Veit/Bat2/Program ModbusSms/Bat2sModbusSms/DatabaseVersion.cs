﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat2sModbusSms {
    /// <summary>
    /// Version of SW that used the database last time. Meaning is the same as in class SwVersion.
    /// </summary>
    public struct DatabaseVersion {
        /// <summary>
        /// Major version number
        /// </summary>
        public int Major;
        
        /// <summary>
        /// Minor version number
        /// </summary>
        public int Minor;

        /// <summary>
        /// Database version number
        /// </summary>
        public int Database;

        /// <summary>
        /// Build number
        /// </summary>
        public int Build;
    }
}
