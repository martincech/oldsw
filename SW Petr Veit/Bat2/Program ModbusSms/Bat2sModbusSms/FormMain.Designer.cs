﻿namespace Bat2sModbusSms {
    partial class FormMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.buttonSetup = new System.Windows.Forms.Button();
            this.dataGridViewScales = new System.Windows.Forms.DataGridView();
            this.ColumnIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnScale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnAverage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnDailyGain = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnUniformity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnErrors = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.listBoxSms = new System.Windows.Forms.ListBox();
            this.labelGsmStatus = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonTestSms = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewScales)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonSetup
            // 
            this.buttonSetup.AutoSize = true;
            this.buttonSetup.Location = new System.Drawing.Point(3, 3);
            this.buttonSetup.Name = "buttonSetup";
            this.buttonSetup.Size = new System.Drawing.Size(75, 23);
            this.buttonSetup.TabIndex = 2;
            this.buttonSetup.Text = "Setup";
            this.buttonSetup.UseVisualStyleBackColor = true;
            this.buttonSetup.Click += new System.EventHandler(this.buttonSetup_Click);
            // 
            // dataGridViewScales
            // 
            this.dataGridViewScales.AllowUserToAddRows = false;
            this.dataGridViewScales.AllowUserToDeleteRows = false;
            this.dataGridViewScales.AllowUserToResizeRows = false;
            this.dataGridViewScales.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewScales.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewScales.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewScales.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewScales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewScales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnIndex,
            this.ColumnAddress,
            this.ColumnScale,
            this.ColumnDateTime,
            this.ColumnMode,
            this.ColumnDay,
            this.ColumnTarget,
            this.ColumnCount,
            this.ColumnAverage,
            this.ColumnDailyGain,
            this.ColumnUniformity,
            this.ColumnErrors});
            this.tableLayoutPanel1.SetColumnSpan(this.dataGridViewScales, 2);
            this.dataGridViewScales.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewScales.Location = new System.Drawing.Point(11, 11);
            this.dataGridViewScales.MultiSelect = false;
            this.dataGridViewScales.Name = "dataGridViewScales";
            this.dataGridViewScales.ReadOnly = true;
            this.dataGridViewScales.RowHeadersVisible = false;
            this.dataGridViewScales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewScales.Size = new System.Drawing.Size(854, 242);
            this.dataGridViewScales.TabIndex = 0;
            // 
            // ColumnIndex
            // 
            this.ColumnIndex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnIndex.HeaderText = "";
            this.ColumnIndex.MinimumWidth = 12;
            this.ColumnIndex.Name = "ColumnIndex";
            this.ColumnIndex.ReadOnly = true;
            this.ColumnIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnIndex.Width = 12;
            // 
            // ColumnAddress
            // 
            this.ColumnAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnAddress.HeaderText = "Address";
            this.ColumnAddress.MinimumWidth = 60;
            this.ColumnAddress.Name = "ColumnAddress";
            this.ColumnAddress.ReadOnly = true;
            this.ColumnAddress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnAddress.Width = 60;
            // 
            // ColumnScale
            // 
            this.ColumnScale.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnScale.HeaderText = "Scale";
            this.ColumnScale.MinimumWidth = 60;
            this.ColumnScale.Name = "ColumnScale";
            this.ColumnScale.ReadOnly = true;
            this.ColumnScale.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnScale.Width = 60;
            // 
            // ColumnDateTime
            // 
            this.ColumnDateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnDateTime.HeaderText = "Date and time";
            this.ColumnDateTime.MinimumWidth = 120;
            this.ColumnDateTime.Name = "ColumnDateTime";
            this.ColumnDateTime.ReadOnly = true;
            this.ColumnDateTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDateTime.Width = 120;
            // 
            // ColumnMode
            // 
            this.ColumnMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnMode.HeaderText = "Mode";
            this.ColumnMode.MinimumWidth = 60;
            this.ColumnMode.Name = "ColumnMode";
            this.ColumnMode.ReadOnly = true;
            this.ColumnMode.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnMode.Width = 60;
            // 
            // ColumnDay
            // 
            this.ColumnDay.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnDay.HeaderText = "Day";
            this.ColumnDay.MinimumWidth = 50;
            this.ColumnDay.Name = "ColumnDay";
            this.ColumnDay.ReadOnly = true;
            this.ColumnDay.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnDay.Width = 50;
            // 
            // ColumnTarget
            // 
            this.ColumnTarget.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnTarget.HeaderText = "Target";
            this.ColumnTarget.MinimumWidth = 60;
            this.ColumnTarget.Name = "ColumnTarget";
            this.ColumnTarget.ReadOnly = true;
            this.ColumnTarget.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnTarget.Width = 60;
            // 
            // ColumnCount
            // 
            this.ColumnCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnCount.HeaderText = "Count";
            this.ColumnCount.MinimumWidth = 60;
            this.ColumnCount.Name = "ColumnCount";
            this.ColumnCount.ReadOnly = true;
            this.ColumnCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnCount.Width = 60;
            // 
            // ColumnAverage
            // 
            this.ColumnAverage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnAverage.HeaderText = "Average";
            this.ColumnAverage.MinimumWidth = 60;
            this.ColumnAverage.Name = "ColumnAverage";
            this.ColumnAverage.ReadOnly = true;
            this.ColumnAverage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnAverage.Width = 60;
            // 
            // ColumnDailyGain
            // 
            this.ColumnDailyGain.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnDailyGain.HeaderText = "Daily gain";
            this.ColumnDailyGain.MinimumWidth = 60;
            this.ColumnDailyGain.Name = "ColumnDailyGain";
            this.ColumnDailyGain.ReadOnly = true;
            this.ColumnDailyGain.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            this.ColumnDailyGain.Width = 78;
            // 
            // ColumnUniformity
            // 
            this.ColumnUniformity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.ColumnUniformity.HeaderText = "Uniformity";
            this.ColumnUniformity.MinimumWidth = 60;
            this.ColumnUniformity.Name = "ColumnUniformity";
            this.ColumnUniformity.ReadOnly = true;
            this.ColumnUniformity.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ColumnUniformity.Width = 60;
            // 
            // ColumnErrors
            // 
            this.ColumnErrors.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnErrors.HeaderText = "Errors";
            this.ColumnErrors.MinimumWidth = 90;
            this.ColumnErrors.Name = "ColumnErrors";
            this.ColumnErrors.ReadOnly = true;
            this.ColumnErrors.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // listBoxSms
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.listBoxSms, 2);
            this.listBoxSms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxSms.FormattingEnabled = true;
            this.listBoxSms.IntegralHeight = false;
            this.listBoxSms.Location = new System.Drawing.Point(11, 288);
            this.listBoxSms.Margin = new System.Windows.Forms.Padding(3, 3, 3, 10);
            this.listBoxSms.Name = "listBoxSms";
            this.listBoxSms.Size = new System.Drawing.Size(854, 93);
            this.listBoxSms.TabIndex = 1;
            // 
            // labelGsmStatus
            // 
            this.labelGsmStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelGsmStatus.AutoSize = true;
            this.labelGsmStatus.Location = new System.Drawing.Point(8, 408);
            this.labelGsmStatus.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.labelGsmStatus.Name = "labelGsmStatus";
            this.labelGsmStatus.Size = new System.Drawing.Size(80, 13);
            this.labelGsmStatus.TabIndex = 3;
            this.labelGsmStatus.Text = "labelGsmStatus";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label1, 2);
            this.label1.Location = new System.Drawing.Point(8, 272);
            this.label1.Margin = new System.Windows.Forms.Padding(0, 16, 3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "SMS to send:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewScales, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelGsmStatus, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.listBoxSms, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(8);
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(876, 429);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.buttonSetup);
            this.flowLayoutPanel1.Controls.Add(this.buttonTestSms);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(706, 391);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(162, 29);
            this.flowLayoutPanel1.TabIndex = 6;
            this.flowLayoutPanel1.WrapContents = false;
            // 
            // buttonTestSms
            // 
            this.buttonTestSms.Location = new System.Drawing.Point(84, 3);
            this.buttonTestSms.Name = "buttonTestSms";
            this.buttonTestSms.Size = new System.Drawing.Size(75, 23);
            this.buttonTestSms.TabIndex = 3;
            this.buttonTestSms.Text = "Test SMS";
            this.buttonTestSms.UseVisualStyleBackColor = true;
            this.buttonTestSms.Click += new System.EventHandler(this.buttonTestSms_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(876, 429);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMain";
            this.Text = "BAT2s ModbusSms";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewScales)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonSetup;
        private System.Windows.Forms.DataGridView dataGridViewScales;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnScale;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDay;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnAverage;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDailyGain;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnUniformity;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnErrors;
        private System.Windows.Forms.ListBox listBoxSms;
        private System.Windows.Forms.Label labelGsmStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button buttonTestSms;

    }
}

