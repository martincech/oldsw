//******************************************************************************
//
//   Filter.c     Filtering
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __C51__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
   #include "Filter.h"
#else
   #include "..\inc\Filter.h"
#endif


#ifdef FILTER_VISIBLE
   #define LowPass  FilterRecord.LowPass
   #define HighPass FilterRecord.HighPass
#endif

TFilterRecord __xdata__ FilterRecord;
static byte             StableCounter;

TRawWeight  __xdata__ Fifo[ FILTER_MAX_AVERAGING];
byte                  FifoPointer;
TLongWeight           FifoSum;

// Local functions :

void FifoInitialize( TRawWeight Fill);
// Initialize FIFO data

TRawWeight FifoAverage( void);
// Calculate average of the contents

void FifoPut( TRawWeight Weight);
// Put to FIFO

//******************************************************************************
// Start
//******************************************************************************

void FilterStart( void)
// Initialize & start filtering
{
   FifoInitialize( FilterRecord.ZeroWeight);
   FilterRecord.LastStableWeight = FilterRecord.ZeroWeight;
   FilterRecord.Status = FILTER_WAIT;  // clear ready also
   StableCounter = 0;
} // FilterStart

//******************************************************************************
// Stop
//******************************************************************************

void FilterStop( void)
// Stop filtering
{
   FilterRecord.Status = FILTER_STOP;  // clear ready also
} // FilterStop

//******************************************************************************
// Sample
//******************************************************************************

void FilterNextSample( TRawWeight Sample)
// Process next sample
{
#ifndef FILTER_VISIBLE
   TRawWeight LowPass;
   TRawWeight HighPass;
#endif
byte       LastStatus;

   FilterRecord.RawWeight = Sample;                     // read sample
   FifoPut( Sample);                                    // save sample
   // filtering :
   LowPass    = FifoAverage();
   HighPass   = FilterRecord.RawWeight - LowPass;
   LastStatus = GetFilterStatus();     // remember old status
   // check for stability range :
#ifndef FILTER_VISIBLE
   // may use HighPass as temporary
   if( HighPass < 0){
      HighPass = -HighPass;   // absolute value
   }
   if( HighPass < FilterRecord.StableRange){
      if( StableCounter < FilterRecord.StableWindow){
         StableCounter++;
      } // else saturation
   } else {
      StableCounter = 0;
   }
#else
   // don't modify HighPass
   {
   TRawWeight TmpHighPass;
      TmpHighPass = HighPass;
      if( TmpHighPass < 0){
         TmpHighPass = -TmpHighPass;   // absolute value
      }
      if( TmpHighPass < FilterRecord.StableRange){
         if( StableCounter < FilterRecord.StableWindow){
            StableCounter++;
         } // else saturation
      } else {
         StableCounter = 0;
      }
   }
#endif
   // check for stability duration :
   if( StableCounter < FilterRecord.StableWindow){
      // short stable value or unstable
      FilterRecord.Status = FILTER_WAIT;  // clear ready also
      if( LastStatus == FILTER_STABLE){
         // fall edge of the stable
         FilterRecord.LastStableWeight = FilterRecord.Weight; // remember last weight
      }
      return;
   }
   // stable value
   if( LastStatus == FILTER_WAIT){
      // rise edge of the stable
      SetFilterReady();                // handshaking
   }
   SetFilterStatus( FILTER_STABLE);    // stable value
   FilterRecord.Weight = LowPass;      // set on averaged value
} // FilterNextSample

//******************************************************************************
// Fifo initialize
//******************************************************************************

void FifoInitialize( TRawWeight Fill)
// Initialize FIFO data
{
TSamplesCount i;

   FifoSum = 0;
   for( i = 0; i < FilterRecord.AveragingWindow; i++){
      Fifo[ i] = Fill;
      FifoSum += Fill;
   }
   FifoPointer = 0;
} // FifoInitialize

//******************************************************************************
// Fifo average
//******************************************************************************

TRawWeight FifoAverage( void)
// Calculate average of the contents
{
   return( FifoSum / FilterRecord.AveragingWindow);
} // FifoAverage

//******************************************************************************
// Fifo put
//******************************************************************************

void FifoPut( TRawWeight Weight)
// Put to FIFO
{
   FifoSum -= Fifo[ FifoPointer];           // remove old value
   Fifo[ FifoPointer++] = Weight;           // save new value
   FifoSum += Weight;                       // add new value
   if( FifoPointer >= FilterRecord.AveragingWindow){
      FifoPointer = 0;
   }
} // FifoPut
