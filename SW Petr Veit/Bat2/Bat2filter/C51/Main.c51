//*****************************************************************************
//
//    Main.c51 -  Bat3 main
//    Verze 1.00  VymOs
//
//*****************************************************************************

#include "Hardware.h"
#include "..\inc\System.h"     // Operacni system
#include "..\inc\El12864.h"    // obecny zobrazovac
#include "..\inc\Font.h"       // font
#include "..\inc\Pca.h"        // zvuky pomoci PCA
#include "..\inc\Kbd.h"        // klavesnice
#include "..\inc\conio.h"      // Display
#include "..\inc\Ads1241f.h"   // A/D prevodnik s filtrem

// odpocet necinnosti :
#define USER_EVENT_TIMEOUT 20    // uzivatel neobsluhuje dele nez ... s

#define SetSecondTimer()  SecondCounter = 1000 / TIMER0_PERIOD   // Novy cyklus odpocitavani 1 sekundy

volatile word        SecondCounter = 0;      // Odpocitavani 1s
volatile bit         SecondExpired = 0;      // Priznak odpocitani 1s


TRawWeight Weight;

//------------------------------------------------------------------------------
//  Main
//------------------------------------------------------------------------------

void main()
{
bit Toggle;

   //>>> display
   BatCS1  = 0;                // deselect displeje
   BatCS2  = 0;
   BacklightOn();              // zapni podsvit
   //<<<

   // systemove nastaveni :
   TimingSetup();              // Nastaveni X2 modu
   EnableXRAM();               // Povoleni pristupu k vnitrni XRAM
   WDTPRG = WDTPRG_2090;       // start Watch Dogu
   WatchDog();

   // casovac :
   SetSecondTimer();           // start odpocitavani sekundy
   EnableInts();               // Povoleni preruseni
   Timer0Run( TIMER0_PERIOD);  // spust casovac 0

   // prevodnik :
   AdcInit();


   PcaBeep( NOTE_C4, VOL_10, 200);  // uvodni pipnuti

   //>>> display
   DisplayInit();
   DisplayClear();
   DisplaySetFont( FONT_LUCIDA6);
   DisplayOn();
   DisplayGotoRC( 0, 0);
   cputs( "Start");
   //<<<

   // inicializace :

   //---------------------------------------------------------------------------
   Toggle = 0;
   while( 1){
      DisplayGotoRC( 0, 0);
      switch(SysWaitEvent()){
         case K_ENTER :
            PcaBeep( NOTE_C3, VOL_10, 200);
            DisplayClrEol();
            cputs( "Enter");

            DisplayGotoRC( 3, 0);
            DisplayClrEol();
            AdcStop();
            FilterRecord.AveragingWindow = 20;
            FilterRecord.StableWindow    = 10;
            FilterRecord.ZeroWeight      =  6000L;
            FilterRecord.StableRange     =  5000L;
            AdcStart( WEIGHT_CHANNEL);
            break;

         case K_ESC :
            PcaBeep( NOTE_D3, VOL_10, 200);
            DisplayClrEol();
            cputs( "Esc");
            AdcStop();
            DisplayGotoRC( 2, 0);
            DisplayClrEol();
            DisplayGotoRC( 3, 0);
            DisplayClrEol();
            break;

         case K_UP :
            PcaBeep( NOTE_E3, VOL_10, 200);
            DisplayClrEol();
            cputs( "Up");
            break;

         case K_DOWN :
            PcaBeep( NOTE_F3, VOL_10, 200);
            DisplayClrEol();
            cputs( "Down");
            break;

         case K_STABLE :
            DisplayGotoRC( 3, 0);
            DisplayClrEol();
            if( Toggle){
               cputs( "/ ");
            } else {
               cputs( "\ ");
            }
            Toggle = !Toggle;
            cint32( Weight);
            break;

         case K_REDRAW :
            DisplayGotoRC( 2, 0);
            DisplayClrEol();
            Weight = AdcRawRead();
            cint32( Weight);
            cputs( "  :");
            switch( GetFilterStatus()){
               case FILTER_STOP :
                  putchar( '-');
                  break;
               case FILTER_WAIT :
                  putchar( 'W');
                  break;
               case FILTER_STABLE :
                  putchar( 'S');
                  break;
               default :
                  putchar( '?');
                  break;
            }
            break;
         default :
            break;
      }
   }
} // main

//------------------------------------------------------------------------------
// Prepnuti kontextu na "operacni system" viz System.h
//------------------------------------------------------------------------------

byte SysYield( void)
// Prepnuti kontextu na operacni system. Vraci udalost
{
byte        Key;
static byte Timeout = 0;

   WatchDog();
   // ustalena hodnota z filtru :
   DisableInts();
   if( AdcDataReady()){
      Weight = AdcRead();
      EnableInts();
      return( K_STABLE);
   }
   EnableInts();
   // nejprve casovac periodicke cinnosti :
   if( SecondExpired){
      // uplynula 1s
      SecondExpired = 0;        // zrus priznak
      Timeout++;                // pocitame necinnost
      return( K_REDRAW);
   }
   // testuj necinnost :
   if( Timeout > USER_EVENT_TIMEOUT){
      Timeout = 0;             // zahajime dalsi cekani
      return( K_TIMEOUT);
   }
   // cteni klavesy :
   Key = KbdGet();
   if( Key != K_IDLE){
      Timeout = 0;              // konec necinnosti
      return( Key);
   }
   return( K_IDLE);             // zadna udalost
} // SysYield

//-----------------------------------------------------------------------------
// Cekani na udalost viz System.h
//-----------------------------------------------------------------------------

byte SysWaitEvent( void)
// Cekani na udalost
{
byte Key;

   while( 1){
      Key = SysYield();
      if( Key != K_IDLE){
         return( Key);      // neprazdna udalost
      }
   }
} // SysWaitEvent

//-------------------------------------------------------------------------------
// Preruseni casovace
//-------------------------------------------------------------------------------

#define SetIsSecond() SecondExpired = 1; SetSecondTimer()


static void Timer0Handler() interrupt INT_TIMER0
// Prerusovaci rutina casovace 0
{

   SetTimer0( TIMER0_PERIOD);                      // S touto periodou

   CheckTrigger( SecondCounter, SetIsSecond());    // casovac 1 s
   KbdTrigger();
   PcaTrigger();                                   // handler asynchronniho pipani
} // Timer0Handler

//------------------------------------------------------------------------------
//  Zpozdeni
//------------------------------------------------------------------------------

void SysDelay( word ms)
// Zpozdeni v milisekundach
// Uprava pro krystal 18.432MHz
{
byte m;                                                                     // m = R5, n = R6+R7
                                                                            // T = 1.085 mikrosec.
  while(ms > 0) {              // DELAY: SETB C/MOV A,R7/SUBB A,#0/MOV A,R6/SUBB A,#0/JC KONEC *7T*
    WDTRST=0x1E;
    WDTRST=0xE1;
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    // Pro X2 mod musim 2x tolik!!
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 255; do --m; while(m != 0);                                // MOV R5,#255/DJNZ R5,$ *511T*
    m = 253; do --m; while(m != 0);                                // MOV R5,#193/DJNZ R5,$ *387T*
    --ms;                          // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*
  }                                                                             // SJMP DELAY *2T*
} // SysDelay
