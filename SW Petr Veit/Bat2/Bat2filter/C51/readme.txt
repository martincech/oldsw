
      Filtrace merenych hodnot

Upraveny zdrojovy text prevodniku je Ads1241f.h/.c51.
Do adresare Moduly je doplnen adresar Filter se souborem
Filter.c51 (Filter.h je v Inc).
Modul Filter je podrizen prevodniku, takze by nemel
byt problem prenest filtraci do jinych prevodniku.
Prefiltr se zapoji definici v Hardware.h :

#define ADC_PREFILTER  3               // pocet vzorku pro prefiltr

neni-li symbol definovan, prefiltr se nepreklada


Pouziti :

1. AdcInit() zrejme v Mainu - inicializace prevodniku
2. Naplneni hodnot fitru parametry :
   globalni struktura FilterRecord (viz Filter.h)
3. AdcStart( channel) inicializuje filtraci a odstartuje
   prevod
4. zakazat preruseni, funkce AdcDataReady() vraci true
   je-li k dispozici ustalena hodnota.
   V tom pripade lze ustalenou hodnotu cist funkci AdcRead().
   Funkce AdcRead() vrati rozdil predchozi stabilni hodnoty
   a aktualni stabilni hodnoty.
   Povolit preruseni
5. Vazeni lze zastavit volanim AdcStop()
6. Aktualni hodnota na prevodniku se da precist kdykoliv
   funkci AdcRawRead(). (Samozrejme jen je-li AdcStart());

POZOR : parametry FilterRecord je mozne menit jen ve stavu AdcStop()

Parametry filtru :

   byte AveragingWindow  - delka klouzaveho prumeru ve vzorcich
   byte StableWindow     - delka trvani stabilniho udaje ve vzorcich
   TWeight ZeroWeight    - hodnota prevodniku pro hmotnost 0kg
                           (pouziva se jako vychozi pro FIFO)
   TWeight StableRange   - interval ustaleneho udaje v LSB prevodniku

Hodnota AveragingWindow musi byt mensi nebo rovna FILTER_MAX_AVERAGING
(delka pole pro klouzavy prumer)

Pozn.: Pro pouziti v BAT1 bude mozna vhodne definovat funkci AdcRestart(),
       ktera bude volat jen FilterStart() pro inicializaci hodnot FIFO
       po uspesnem vazeni. (Inicializace filtru pro start z hodnoty
       ZeroWeight)
