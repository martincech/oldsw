object MainForm: TMainForm
  Left = 222
  Top = 114
  Width = 931
  Height = 625
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 478
    Width = 923
    Height = 2
    Cursor = crVSplit
    Align = alBottom
  end
  object Graph: TDBChart
    Left = 0
    Top = 97
    Width = 923
    Height = 381
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    OnUndoZoom = GraphUndoZoom
    OnZoom = GraphZoom
    BottomAxis.DateTimeFormat = 'h:mm'
    Legend.Visible = False
    View3D = False
    Align = alClient
    TabOrder = 0
    OnMouseUp = GraphMouseUp
    object WeightSeries: TFastLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      ShowInLegend = False
      LinePen.Color = clRed
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HMOTNOST'
    end
    object LowPassSeries: TFastLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      ShowInLegend = False
      LinePen.Color = clGreen
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'LO_PASS'
    end
    object StableWeightSeries: TFastLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clWhite
      LinePen.Color = clWhite
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
    object LastStableWeightSeries: TFastLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      LinePen.Color = clYellow
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 923
    Height = 97
    Align = alTop
    TabOrder = 1
    object Label3: TLabel
      Left = 496
      Top = 16
      Width = 48
      Height = 13
      Caption = 'Averaging'
    end
    object Label1: TLabel
      Left = 704
      Top = 16
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label2: TLabel
      Left = 8
      Top = 8
      Width = 34
      Height = 13
      Caption = 'Display'
    end
    object Label4: TLabel
      Left = 24
      Top = 24
      Width = 28
      Height = 13
      Caption = 'Width'
    end
    object Label5: TLabel
      Left = 24
      Top = 48
      Width = 28
      Height = 13
      Caption = 'Offset'
    end
    object Label8: TLabel
      Left = 496
      Top = 40
      Width = 60
      Height = 13
      Caption = 'Stable range'
    end
    object Label9: TLabel
      Left = 704
      Top = 40
      Width = 12
      Height = 13
      Caption = 'kg'
    end
    object Label10: TLabel
      Left = 496
      Top = 64
      Width = 30
      Height = 13
      Caption = 'Stable'
    end
    object Label11: TLabel
      Left = 704
      Top = 64
      Width = 40
      Height = 13
      Caption = 'Samples'
    end
    object Label12: TLabel
      Left = 216
      Top = 80
      Width = 29
      Height = 13
      Caption = 'Data :'
    end
    object LblFileName: TLabel
      Left = 256
      Top = 80
      Width = 41
      Height = 13
      Caption = 'demo.db'
    end
    object Label13: TLabel
      Left = 24
      Top = 72
      Width = 35
      Height = 13
      Caption = 'Prefilter'
    end
    object StableWeightLabel: TLabel
      Left = 784
      Top = 16
      Width = 64
      Height = 13
      Caption = 'Stable weight'
    end
    object LastStableWeightLabel: TLabel
      Left = 784
      Top = 32
      Width = 85
      Height = 13
      Caption = 'Last stable weight'
    end
    object StableLabel: TLabel
      Left = 784
      Top = 56
      Width = 75
      Height = 13
      Caption = 'Stable YES/NO'
    end
    object EdtAveraging: TEdit
      Left = 576
      Top = 8
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '20'
    end
    object BtnRedraw: TButton
      Left = 368
      Top = 16
      Width = 75
      Height = 25
      Caption = 'Redraw'
      TabOrder = 1
      OnClick = BtnRedrawClick
    end
    object EdtWidth: TEdit
      Left = 64
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '2000'
    end
    object EdtOffset: TEdit
      Left = 64
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 3
      Text = '0'
    end
    object BtnBackward: TButton
      Left = 392
      Top = 48
      Width = 30
      Height = 25
      Caption = '<<<'
      TabOrder = 4
      OnClick = BtnBackwardClick
    end
    object BtnForward: TButton
      Left = 432
      Top = 48
      Width = 30
      Height = 25
      Caption = '>>>'
      TabOrder = 5
      OnClick = BtnForwardClick
    end
    object BtnFirst: TButton
      Left = 352
      Top = 48
      Width = 30
      Height = 25
      Caption = '|<<<'
      TabOrder = 6
      OnClick = BtnFirstClick
    end
    object EdtStableRange: TEdit
      Left = 576
      Top = 32
      Width = 121
      Height = 21
      TabOrder = 7
      Text = '0,01'
    end
    object EdtStable: TEdit
      Left = 576
      Top = 56
      Width = 121
      Height = 21
      TabOrder = 8
      Text = '10'
    end
    object BtnFile: TButton
      Left = 216
      Top = 48
      Width = 75
      Height = 25
      Caption = 'File'
      TabOrder = 9
      OnClick = BtnFileClick
    end
    object EdtPrefilter: TEdit
      Left = 64
      Top = 64
      Width = 121
      Height = 21
      TabOrder = 10
      Text = '1'
    end
    object StableMultiplierEdit: TEdit
      Left = 864
      Top = 53
      Width = 33
      Height = 21
      TabOrder = 11
      Text = '100'
    end
  end
  object LowGraph: TDBChart
    Left = 0
    Top = 480
    Width = 923
    Height = 118
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      '')
    Title.Visible = False
    BottomAxis.DateTimeFormat = 'h:mm'
    Legend.Visible = False
    View3D = False
    Align = alBottom
    TabOrder = 2
    object LineSeries3: TFastLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlack
      ShowInLegend = False
      Title = 'HighPassSeries'
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      XValues.ValueSource = 'MS'
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
      YValues.ValueSource = 'HI_PASS'
    end
    object LineSeries4: TFastLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clNavy
      Title = 'StableSeries'
      LinePen.Color = clNavy
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1
      YValues.Order = loNone
    end
  end
  object FileOpenDialog: TOpenDialog
    DefaultExt = 'db'
    FileName = 'demo'
    Filter = 'Database|*.db'
    Left = 472
    Top = 8
  end
end
