//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <DBChart.hpp>
#include <ExtCtrls.hpp>
#include <Chart.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
        TDBChart *Graph;
        TPanel *Panel1;
        TLabel *Label3;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label4;
        TLabel *Label5;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label10;
        TLabel *Label11;
        TLabel *Label12;
        TLabel *LblFileName;
        TLabel *Label13;
        TEdit *EdtAveraging;
        TButton *BtnRedraw;
        TEdit *EdtWidth;
        TEdit *EdtOffset;
        TButton *BtnBackward;
        TButton *BtnForward;
        TButton *BtnFirst;
        TEdit *EdtStableRange;
        TEdit *EdtStable;
        TButton *BtnFile;
        TEdit *EdtPrefilter;
        TOpenDialog *FileOpenDialog;
        TLabel *StableWeightLabel;
        TLabel *LastStableWeightLabel;
        TLabel *StableLabel;
        TDBChart *LowGraph;
        TFastLineSeries *WeightSeries;
        TFastLineSeries *LowPassSeries;
        TFastLineSeries *StableWeightSeries;
        TFastLineSeries *LastStableWeightSeries;
        TFastLineSeries *LineSeries3;
        TFastLineSeries *LineSeries4;
        TSplitter *Splitter1;
        TEdit *StableMultiplierEdit;
        void __fastcall BtnRedrawClick(TObject *Sender);
        void __fastcall BtnBackwardClick(TObject *Sender);
        void __fastcall BtnForwardClick(TObject *Sender);
        void __fastcall BtnFirstClick(TObject *Sender);
        void __fastcall BtnFileClick(TObject *Sender);
        void __fastcall GraphZoom(TObject *Sender);
        void __fastcall GraphUndoZoom(TObject *Sender);
        void __fastcall GraphMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
private:	// User declarations

  TLineSeries *HighPassSeriesLow;
  TLineSeries *StableSeriesLow;

public:		// User declarations
        __fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
