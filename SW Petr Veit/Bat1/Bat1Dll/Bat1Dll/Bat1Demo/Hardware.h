//******************************************************************************
//
//   Hardware.h    Parameters
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef __Hardware_H__
   #define __Hardware_H__

#include "../../Library/Unisys/uni.h"  // zakladni datove typy
#include <stddef.h>                    // macro offsetof
#include <stdio.h>

#define __packed                       // empty definition

//-----------------------------------------------------------------------------
// EEPROM
//-----------------------------------------------------------------------------

#define EEP_PAGE_SIZE  256
#define EEP_SIZE       131072

#ifdef __cplusplus
extern "C" {
#endif

extern byte *_Eeprom;

#ifdef __cplusplus
}
#endif

//-----------------------------------------------------------------------------
// Accumulator maintenance
//-----------------------------------------------------------------------------

#define PACCU_LOGGER_SIZE  508         // logger records count
#define ACCU_LOGGER_PERIOD   300       // logger period [s]

//-----------------------------------------------------------------------------
// Secure file / accumulator logger
//-----------------------------------------------------------------------------

#define SFILE_MARKER        0xFFFF                         // marker value
#define SFILE_MARKER_SIZE   sizeof( word)                  // marker size
#define SFILE_START         offsetof( TEeprom, AccuLogger) // start address
#define SFILE_RECORD_SIZE   sizeof( TPAccuRecord)          // record size
#define SFILE_COUNT         PACCU_LOGGER_SIZE              // max. records count

//-----------------------------------------------------------------------------
// File System
//-----------------------------------------------------------------------------

#define	FDIR_SIZE         199                    // Maximum number of files (directory entries)
#define FS_BLOCK_SIZE     64                     // block size [byte]
#define FAT_SIZE          1652                   // FAT size (total blocks count)

#define FS_OFFSET         offsetof( TEeprom, Filesystem)  // filesystem offset (from EEP start)

//-----------------------------------------------------------------------------
// Statistics
//-----------------------------------------------------------------------------

typedef double TNumber;                 // basic data type
typedef word   TStatCount;              // statistical samples count

//-----------------------------------------------------------------------------
// Histogram
//-----------------------------------------------------------------------------

typedef word THistogramCount;           // samples count
typedef long THistogramValue;           // sample value, must be signed
#define HISTOGRAM_MIN_STEP    1         // min. step in THistogramValue

#define HISTOGRAM_SLOTS          39     // columns count even/odd (max.254)
#define HISTOGRAM_NORM           1000   // normalized value by max column
#define HISTOGRAM_NORMALIZE_STEP 1      // normalize step size

//-----------------------------------------------------------------------------
// Date/Time
//-----------------------------------------------------------------------------

#define DT_ENABLE_DST 1
#define DtGetDstType()        _DstMode

#ifdef __cplusplus
extern "C" {
#endif

extern int _DstMode;

#ifdef __cplusplus
}
#endif

//-----------------------------------------------------------------------------

//#include "MemoryDef.h"     // EEPROM offset calculations

#endif
