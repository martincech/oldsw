//******************************************************************************
//
//   Ds.c          Data Set utilities
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifdef __WIN32__
   // Borland only
   #include <vcl.h>
   #pragma hdrstop
#endif

#include "../../Inc/File/Ds.h"
#include "../../Inc/File/Fd.h"
#include "../../Inc/File/Db.h"
#include <string.h>
#include "DsUser.h"            // project directory

// Remark : index in the dataset is the bit index of the FdirHandle
//          index 0 is the current file

#define DsCurrentOnly()  _CurrentDataSet[ 0] = 0x01
#define DsIsCurrent()   (_CurrentDataSet[ 0] & 0x01)

static TFDataSet    _CurrentDataSet;
static TFdirHandle  _CurrHandle;

// Local functions :

static TYesNo NextDb( TFdirHandle Previous);
// Open next database in the set after <Previous>

//------------------------------------------------------------------------------
// Clear
//------------------------------------------------------------------------------

void DsClear( TFDataSet DataSet)
// Clear dataset
{
int i;

   // clear all bits :
   for( i = 0; i < DS_SIZE; i++){
      DataSet[ i] = 0;
   }
} // DsClear

//------------------------------------------------------------------------------
// Select all
//------------------------------------------------------------------------------

void DsSelectAll( TFDataSet DataSet)
// Select all files to dataset
{
TFdirHandle Handle;

   FdSetClass( DS_CLASS);
   DsClear( DataSet);
   FdFindBegin();
   while( (Handle = FdFindNext()) != FDIR_INVALID){
      DsAdd( DataSet, Handle);
   }
} // DsSelectAll

//------------------------------------------------------------------------------
// Copy
//------------------------------------------------------------------------------

void DsCopy( TFDataSet Dst, TFDataSet Src)
// Copy dataset
{
   memcpy( Dst, Src, sizeof( TFDataSet));
} // DsCopy

//------------------------------------------------------------------------------
// Add
//------------------------------------------------------------------------------

void DsAdd( TFDataSet DataSet, TFdirHandle Handle)
// Add file to dataset
{
   DataSet[ Handle / 8] |= (1 << (Handle % 8));
} // DsAdd

//------------------------------------------------------------------------------
// Remove
//------------------------------------------------------------------------------

void DsRemove( TFDataSet DataSet, TFdirHandle Handle)
// Remove file from dataset
{
   DataSet[ Handle / 8] &= ~(1 << (Handle % 8));
} // DsRemove

//------------------------------------------------------------------------------
// Contains
//------------------------------------------------------------------------------

TYesNo DsContains( TFDataSet DataSet, TFdirHandle Handle)
// Returns YES if <DataSet> contains <Handle>
{
   return( DataSet[ Handle / 8] & (1 << (Handle % 8)));
} // DsContains

//------------------------------------------------------------------------------
// Is Empty
//------------------------------------------------------------------------------

TYesNo DsIsEmpty( TFDataSet DataSet)
// Empty dataset
{
TYesNo Empty;
int    i;

   Empty = YES;
   for( i = 0; i < DS_SIZE; i++){
      if( DataSet[ i]){
         Empty = NO;
         break;
      }
   }
   return( Empty);
} // DsIsEmpty

//------------------------------------------------------------------------------
// Set Current
//------------------------------------------------------------------------------

void DsSetCurrent( void)
// Set dataset to currently opened file
{
   DsClear( _CurrentDataSet);
   DsCurrentOnly();
} // DsAddCurrent

//------------------------------------------------------------------------------
// Set
//------------------------------------------------------------------------------

void DsSet( TFDataSet DataSet)
// Set <DataSet> as current
{
int i;

   memcpy( _CurrentDataSet, DataSet, sizeof( TFDataSet));
   // check for handle validity :
   FdSetClass( DS_CLASS);
   for( i = 1; i <= FDIR_SIZE; i++){
      if( !DsContains( _CurrentDataSet, i)){
         continue;
      }
      if( !FdValid( i)){
         DsRemove( _CurrentDataSet, i);// remove invalid handle
      }
   }
} // DsSet

//------------------------------------------------------------------------------
// Begin
//------------------------------------------------------------------------------

void DsBegin( void)
// Set before start of the first table
{
   if( DsIsCurrent()){
      // Current db only :
      DbBegin();                       // set at first record
      return;
   }
   NextDb( FDIR_INVALID);              // find first db in the dataset
} // DsBegin

//------------------------------------------------------------------------------
// Next
//------------------------------------------------------------------------------

TYesNo DsNext( void *Record)
// Get next record
{
   if( DsIsCurrent()){
      return( DbNext( Record));
   }
   if( !_CurrHandle){
      return( NO);
   }
   forever {
      if( DbNext( Record)){
         return( YES);
      }
      // find next file in the dataset :
      if( !NextDb( _CurrHandle)){
         return( NO);
      }
   }
} // DsNext

//******************************************************************************

//------------------------------------------------------------------------------
// Next file
//------------------------------------------------------------------------------

static TYesNo NextDb( TFdirHandle Previous)
// Open next database in the set after <Previous>
{
int  i;

   // Dataset operation :
   if( Previous){
      DbClose();                       // close previous db
   }
   _CurrHandle = FDIR_INVALID;
   // find next handle :
   for( i = Previous + 1; i <= FDIR_SIZE; i++){
      if( DsContains( _CurrentDataSet, i)){
         _CurrHandle = i;
         break;
      }
   }
   if( !_CurrHandle){
      return( NO);
   }
   DbOpen( _CurrHandle, NO, YES);      // sequential access, read only
   DbBegin();                          // before first record
   return( YES);
} // NextDb
