//*****************************************************************************
//
//    UsbDef.h     USB protocol definition
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __UsbDef_H__
   #define __UsbDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#ifndef __FdirDef_H__
   #include "../Inc/File/FdirDef.h"
#endif

#define USB_MAX_DATA      128           // max. packet data
#define USB_FORMAT_MAGIC  0x5AA5A55A    // format command magic

//------------------------------------------------------------------------------
// USB commands
//------------------------------------------------------------------------------

typedef enum {
//  Short packet --------------------------------------------------------------
   USB_CMD_UNDEFINED,

   USB_CMD_VERSION,           // get version (no data)
   USB_CMD_GET_STATUS,        // get power status (no data)
   USB_CMD_POWER_OFF,         // switch power off (no data)
   USB_CMD_RELOAD,            // reload configuration
   USB_CMD_GET_TIME,          // get actual clock
   USB_CMD_SET_TIME,          // set device clock

   USB_CMD_ADDRESS,           // set current address( data = address)
   USB_CMD_GET_ADDRESS,       // get current address (no data)

   USB_CMD_READ,              // read EEPROM data( data = size)

   USB_CMD_DIRECTORY_BEGIN,   // set directory read at begin (no data)
   USB_CMD_DIRECTORY_NEXT,    // get directory item (no data)

   USB_CMD_FILE_OPEN,         // open file (data = file handle)
   USB_CMD_FILE_CLOSE,        // close current file (no data)
   USB_CMD_FILE_READ,         // read current file (data = size)

   USB_CMD_FORMAT,            // format filesystem (data = magic)

//  Long packet ---------------------------------------------------------------
   USB_CMD_WRITE,             // write EEPROM data

   USB_CMD_FILE_CREATE,       // create file
   USB_CMD_FILE_WRITE,        // write at end of current file

//-----------------------------------------------------------------------------
   USB_CMD_ERROR,             // error reply
   _USB_CMD_LAST
} TUsbCmdCode;
// long packet == write data

//------------------------------------------------------------------------------
// USB long commands
//------------------------------------------------------------------------------

typedef dword TUsbCmd;                                   // command code

#define USB_CMD_SIZE      sizeof( TUsbCmd)               // command code size
#define USB_WRITE_SIZE   (USB_MAX_DATA - USB_CMD_SIZE)   // write command data sizes

// Write data (USB_CMD_WRITE, USB_CMD_FILE_WRITE) :
typedef struct {
   TUsbCmd Cmd;
   byte    Data[ USB_WRITE_SIZE];
} TUsbCmdWrite;

// File create :
typedef struct {
   TUsbCmd Cmd;
   word Class;                         // file class
   char Name[FDIR_NAME_LENGTH + 1];    // file name
   char Note[FDIR_NOTE_LENGTH + 1];    // file note
} TUsbCmdFileCreate;

// Command union :
typedef union {
   TUsbCmd           Cmd;
   TUsbCmdWrite      Write;     // EEPROM / file write
   TUsbCmdFileCreate Create;    // file create
} TUsbCmdUnion;

//------------------------------------------------------------------------------
// USB reply
//------------------------------------------------------------------------------

#define USB_CMD_REPLY  0x80   // reply flag

// USB reply data :
// USB_CMD_VERSION         version number
// USB_CMD_STATUS          0 = power off, else power on
// USB_CMD_POWER_OFF       echo data
// USB_CMD_RELOAD          echo data
// USB_CMD_GET_TIME        actual clock
// USB_CMD_SET_TIME        echo clock
// USB_CMD_ADDRESS         echo address
// USB_CMD_GET_ADDRESS     address
// USB_CMD_READ            long packet with data
// USB_CMD_DIRECTORY_BEGIN count
// USB_CMD_DIRECTORY_NEXT  long packet with directory item
// USB_CMD_FILE_OPEN       echo file handle
// USB_CMD_FILE_CLOSE      no data
// USB_CMD_FILE_READ       long packet with data

// USB_CMD_WRITE           last address
// USB_CMD_FILE_CREATE     file handle
// USB_CMD_FILE_WRITE      last address

// USB_CMD_DIRECTORY_NEXT reply :
typedef struct {
   word      Handle;              // file handle
   word      Class;               // file class
   TFdirInfo Info;                // directory item
} TUsbDirectoryItem;

#endif
