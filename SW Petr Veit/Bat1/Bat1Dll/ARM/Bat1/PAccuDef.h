//*****************************************************************************
//
//    PAccuDef.h    Accumulator maintenance data
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __PAccuDef_H__
   #define __PAccuDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

//-----------------------------------------------------------------------------
//   Calibration data
//-----------------------------------------------------------------------------

typedef word TPAccuCrc;

typedef struct {
   word      Range;          // physical range - calibration coefficient
   word      UMax;           // 100% capacity voltage [LSB]
   word      UMid;           // 50%  capacity voltage [LSB]
   word      UMin;           // 0%   capacity voltage [LSB]
   word      UWarn;          // warning voltage [LSB]
   word      ChargeTime;     // charge time [min]
   word      DischargeTime;  // discharge time [min]
   TPAccuCrc CheckSum;       // security checksum                
} TPAccuData;

#define PACCU_ADC_RANGE    1024        // ADC range [LSB]

// Voltage [mV] of the raw value [LSB] :
#define PAccuGetVoltage( RawValue, Range)  (((dword)(RawValue) * (Range)) / PACCU_ADC_RANGE)
#define PAccuGetRawVoltage( Value, Range)  (((dword)(Value) * PACCU_ADC_RANGE ) / (Range))

//-----------------------------------------------------------------------------
//   Logger data
//-----------------------------------------------------------------------------

// discharge curve item :
typedef struct {
   word Time;                // discharge time [min]
   word Voltage;             // discharge voltage [LSB]
} TPAccuRecord;

// discharge curve array :
typedef TPAccuRecord TPAccuLogger[ PACCU_LOGGER_SIZE];

#endif
