//*****************************************************************************
//
//    ConfigDef.h  -  Configuration definition
//    Version 1.0  (c) VymOs & P.Veit
//
//*****************************************************************************

#ifndef __ConfigDef_H__
  #define __ConfigDef_H__

#ifndef __FdirCfg_H__
   #include "../Inc/File/FdirDef.h"    // directory entry handle
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"           // weighing parameters
#endif

#ifndef __PStatisticDef_H__
   #include "PStatisticDef.h"          // statistic parameters
#endif

#ifndef __CountryDef_H__
   #include "../Inc/CountryDef.h"      // locale data
#endif

#ifndef __ComDef_H__
   #include "../Inc/ComDef.h"          // serial communication parameters
#endif

//-----------------------------------------------------------------------------
// Weighing configuration
//-----------------------------------------------------------------------------

// Weighing units :
typedef enum {
   UNITS_KG,
   UNITS_G,
   UNITS_LB,
   _UNITS_COUNT
} TUnitsEnum;

// Weighing capacity :
typedef enum {
   CAPACITY_NORMAL,                    // 30kg/60lb
   CAPACITY_EXTENDED,                  // 50kg/100lb
   _CAPACITY_COUNT
} TWeighingCapacity;

// Units descriptor :
typedef struct {
   dword   Range;                      // weighing range
   byte    Units;                      // weighing units TUnitsEnum
   byte    Decimals;                   // decimals count
   word    MaxDivision;                // division limit
   TWeight Division;                   // scale division
} TUnits;

//-----------------------------------------------------------------------------
// Device configuration
//-----------------------------------------------------------------------------

#define VOLUME_MAX     9               // volume 0..MAX

// Sounds configuration
typedef struct {
   byte ToneDefault;                   // Tone of the saving without limits
   byte ToneLight;                     // Tone of the below limit
   byte ToneOk;                        // Tone of the within limits
   byte ToneHeavy;                     // Tone of the above limits
   byte ToneKeyboard;                  // Tone of the keyboard click
   byte EnableSpecial;                 // Enable special sounds
   byte VolumeKeyboard;                // Volume of the keyboard click
   byte VolumeSaving;                  // Volume of the saving beep
} TSoundsConfig;

// Display mode
typedef enum {
   DISPLAY_MODE_BASIC,                 // Basic display mode
   DISPLAY_MODE_ADVANCED,              // Advanced display mode
   DISPLAY_MODE_LARGE,                 // Large display mode
   _DISPLAY_MODE_COUNT
} TDisplayMode;

#define BACKLIGHT_MAX           9      // backlight intensity 0..MAX
#define CONTRAST_MAX           64      // contrast intensity 0..MAX
#define BACKLIGHT_DURATION_MAX 99      // backlight duration [s]

// Backlight mode
typedef enum {
   BACKLIGHT_MODE_AUTO,                // Backlight in automatic mode
   BACKLIGHT_MODE_ON,	               // Backlight is always on
   BACKLIGHT_MODE_OFF,	               // Backlight is always off
   _BACKLIGHT_MODE_COUNT
} TBacklightMode;

// Backlight configuration
typedef struct {
   byte Mode;		               // TBacklightMode Backlight mode   
   byte Intensity;	               // Backlight intensity
   word Duration;                      // Backlight duration [s]
} TBacklightConfig;

// Display configuration
typedef struct {
   byte             Mode;              // TDisplayMode Display mode
   byte             Contrast;          // Display contrast
   word             _Spare;
   TBacklightConfig Backlight;         // Backlight setup
} TDisplayConfig;

// Print constants 
#define PAPER_WIDTH_MAX      99        // Max. paper width

// Print configuration
typedef struct {
   byte PaperWidth;                    // Paper width [mm]
   byte CommunicationFormat;           // Serial format TComFormat enum
   word CommunicationSpeed;            // Serial speed [Bd]
} TPrintConfig;


#define POWER_OFF_TIMEOUT_MAX   (99 * 60)

#define	SCALE_NAME_LENGTH	15	// Maximum length of the scale name
#define PASSWORD_LENGTH         4       // Number of password keys

typedef word TConfigCrc;                // Configuration checksum

// Global configuration
typedef struct {
   word Version;                                 // Version
   byte Build;                                   // Build
   byte HwVersion;                               // HW version

   char ScaleName[ SCALE_NAME_LENGTH + 1];	 // Scale name + ending zero
   char Password[ PASSWORD_LENGTH];              // Service password

   TCountry             Country;                 // Country data
   TUnits               Units;                   // Weighing units
   TSoundsConfig        Sounds;                  // Setup of sounds
   TDisplayConfig       Display;                 // Setup of display
   TPrintConfig         Printer;                 // Setup of printer
   word                 KeyboardTimeout;         // Keyboard inactivity timeout [s]
   word                 PowerOffTimeout;         // Auto power off timeout [s]
   TFdirHandle          LastFile;                // Last working file
   byte                 EnableFileParameters;    // File specific parameters (YES/NO)
   byte                 PrintProtocol;           // Last protocol type
   byte                 _Spare;                  // dword alignment
   TWeighingParameters  WeighingParameters;      // Global weighing parameters
   TStatisticParameters StatisticParameters;     // Statistic parameters
} TConfig;

extern TConfig Config;

#define CONFIG_DATA_SIZE  (256 - sizeof( TConfigCrc))  // prealocated config data size

// Spare configuration
typedef struct {
   byte       Data[ CONFIG_DATA_SIZE];
   TConfigCrc CheckSum;
} TConfigSpare;

// Config union
typedef union {
   TConfig      Config;                // real data
   TConfigSpare Spare;                 // spare only
} TConfigUnion;

#endif
