//******************************************************************************
//
//   SdbDef.h     Samples database definitions
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __SdbDef_H__
   #define __SdbDef_H__

#ifndef __Uni_H__
   #include "../Inc/Uni.h"
#endif

#ifndef __DtDef_H__
   #include "../Inc/DtDef.h"           // date and time
#endif

#ifndef __WeightDef_H__
   #include "WeightDef.h"              // weight types       
#endif

#ifndef __PWeighingDef_H__
   #include "PWeighingDef.h"           // weighing parameters
#endif

#ifdef __WIN32__
   #pragma pack( push, 1)              // byte alignment
#endif

#define SDB_CLASS          1           // database class

// Flags used in samples
typedef enum {
  FLAG_NONE,		               // no flag saved (flags not in use during weighing)
  FLAG_LIGHT,		               // below lower limit
  FLAG_OK,	                       // within lower and higher limit
  FLAG_HEAVY,		               // above higher limit
  FLAG_MALE,                           // male sex
  FLAG_FEMALE,                         // female sex
  FLAG_INVALID = 0xFF                  // invalid flag (internally used only)
} TSampleFlagEnum;

typedef byte TSampleFlag;

//-----------------------------------------------------------------------------
// Configuration
//-----------------------------------------------------------------------------

typedef TWeighingParameters TSdbConfig;

//-----------------------------------------------------------------------------
// Record
//-----------------------------------------------------------------------------

// One sample of weight
typedef struct {
  TTimestamp  Timestamp;               // Date and time
  TWeight     Weight;                  // Weight of the sample
  TSampleFlag Flag;                    // Flag of the sample (TSampleFlagEnum)
} __packed TSdbRecord;

#ifdef __WIN32__
   #pragma pack( pop)                  // original alignment
#endif

#endif
