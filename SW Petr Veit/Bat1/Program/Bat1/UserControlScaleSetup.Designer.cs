﻿namespace Bat1 {
    partial class UserControlScaleSetup {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleSetup));
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonSaveToScale = new System.Windows.Forms.Button();
            this.buttonSaveTemplate = new System.Windows.Forms.Button();
            this.buttonOpenTemplate = new System.Windows.Forms.Button();
            this.buttonLoadFromScale = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonImport = new System.Windows.Forms.Button();
            this.buttonExport = new System.Windows.Forms.Button();
            this.loadFromWeighingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.scaleNameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dateAndTimeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonNew
            // 
            resources.ApplyResources(this.buttonNew, "buttonNew");
            this.buttonNew.Name = "buttonNew";
            this.toolTip1.SetToolTip(this.buttonNew, resources.GetString("buttonNew.ToolTip"));
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // buttonSaveToScale
            // 
            resources.ApplyResources(this.buttonSaveToScale, "buttonSaveToScale");
            this.buttonSaveToScale.Name = "buttonSaveToScale";
            this.toolTip1.SetToolTip(this.buttonSaveToScale, resources.GetString("buttonSaveToScale.ToolTip"));
            this.buttonSaveToScale.UseVisualStyleBackColor = true;
            this.buttonSaveToScale.Click += new System.EventHandler(this.buttonSaveToScale_Click);
            // 
            // buttonSaveTemplate
            // 
            resources.ApplyResources(this.buttonSaveTemplate, "buttonSaveTemplate");
            this.buttonSaveTemplate.Name = "buttonSaveTemplate";
            this.toolTip1.SetToolTip(this.buttonSaveTemplate, resources.GetString("buttonSaveTemplate.ToolTip"));
            this.buttonSaveTemplate.UseVisualStyleBackColor = true;
            this.buttonSaveTemplate.Click += new System.EventHandler(this.buttonSaveTemplate_Click);
            // 
            // buttonOpenTemplate
            // 
            resources.ApplyResources(this.buttonOpenTemplate, "buttonOpenTemplate");
            this.buttonOpenTemplate.Name = "buttonOpenTemplate";
            this.toolTip1.SetToolTip(this.buttonOpenTemplate, resources.GetString("buttonOpenTemplate.ToolTip"));
            this.buttonOpenTemplate.UseVisualStyleBackColor = true;
            this.buttonOpenTemplate.Click += new System.EventHandler(this.buttonOpenTemplate_Click);
            // 
            // buttonLoadFromScale
            // 
            resources.ApplyResources(this.buttonLoadFromScale, "buttonLoadFromScale");
            this.buttonLoadFromScale.Name = "buttonLoadFromScale";
            this.toolTip1.SetToolTip(this.buttonLoadFromScale, resources.GetString("buttonLoadFromScale.ToolTip"));
            this.buttonLoadFromScale.UseVisualStyleBackColor = true;
            this.buttonLoadFromScale.Click += new System.EventHandler(this.buttonLoadFromScale_Click);
            // 
            // flowLayoutPanel1
            // 
            resources.ApplyResources(this.flowLayoutPanel1, "flowLayoutPanel1");
            this.flowLayoutPanel1.Controls.Add(this.buttonNew);
            this.flowLayoutPanel1.Controls.Add(this.buttonOpenTemplate);
            this.flowLayoutPanel1.Controls.Add(this.buttonSaveTemplate);
            this.flowLayoutPanel1.Controls.Add(this.buttonLoadFromScale);
            this.flowLayoutPanel1.Controls.Add(this.buttonSaveToScale);
            this.flowLayoutPanel1.Controls.Add(this.buttonImport);
            this.flowLayoutPanel1.Controls.Add(this.buttonExport);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.toolTip1.SetToolTip(this.flowLayoutPanel1, resources.GetString("flowLayoutPanel1.ToolTip"));
            // 
            // buttonImport
            // 
            resources.ApplyResources(this.buttonImport, "buttonImport");
            this.buttonImport.Name = "buttonImport";
            this.toolTip1.SetToolTip(this.buttonImport, resources.GetString("buttonImport.ToolTip"));
            this.buttonImport.UseVisualStyleBackColor = true;
            this.buttonImport.Click += new System.EventHandler(this.buttonImport_Click);
            // 
            // buttonExport
            // 
            resources.ApplyResources(this.buttonExport, "buttonExport");
            this.buttonExport.Name = "buttonExport";
            this.toolTip1.SetToolTip(this.buttonExport, resources.GetString("buttonExport.ToolTip"));
            this.buttonExport.UseVisualStyleBackColor = true;
            this.buttonExport.Click += new System.EventHandler(this.buttonExport_Click);
            // 
            // loadFromWeighingToolStripMenuItem
            // 
            resources.ApplyResources(this.loadFromWeighingToolStripMenuItem, "loadFromWeighingToolStripMenuItem");
            this.loadFromWeighingToolStripMenuItem.Name = "loadFromWeighingToolStripMenuItem";
            // 
            // exportToolStripMenuItem
            // 
            resources.ApplyResources(this.exportToolStripMenuItem, "exportToolStripMenuItem");
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            // 
            // importToolStripMenuItem
            // 
            resources.ApplyResources(this.importToolStripMenuItem, "importToolStripMenuItem");
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            // 
            // toolStripMenuItem1
            // 
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            // 
            // scaleNameToolStripMenuItem
            // 
            resources.ApplyResources(this.scaleNameToolStripMenuItem, "scaleNameToolStripMenuItem");
            this.scaleNameToolStripMenuItem.Name = "scaleNameToolStripMenuItem";
            // 
            // dateAndTimeToolStripMenuItem
            // 
            resources.ApplyResources(this.dateAndTimeToolStripMenuItem, "dateAndTimeToolStripMenuItem");
            this.dateAndTimeToolStripMenuItem.Name = "dateAndTimeToolStripMenuItem";
            // 
            // UserControlScaleSetup
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayoutPanel1);
            this.Name = "UserControlScaleSetup";
            this.toolTip1.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLoadFromScale;
        private System.Windows.Forms.Button buttonOpenTemplate;
        private System.Windows.Forms.Button buttonSaveToScale;
        private System.Windows.Forms.Button buttonSaveTemplate;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.ToolStripMenuItem scaleNameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dateAndTimeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem loadFromWeighingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button buttonImport;
        private System.Windows.Forms.Button buttonExport;
    }
}
