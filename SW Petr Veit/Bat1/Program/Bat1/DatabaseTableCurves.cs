﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;
using Veit.Scale;

namespace Bat1 {
    
    /// <summary>
    /// Curves table
    /// </summary>
    public class DatabaseTableCurves : DatabaseTable {

        // Tabulka teoretickych rustovych krivek.

        /// <summary>
        /// Maximum length of curve name
        /// </summary>
        public static int NAME_MAX_LENGTH = 30;

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableCurves(DatabaseFactory factory)
            : base("Curves", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "CurveId " + factory.TypeLong + " PRIMARY KEY AUTOINCREMENT,"
                 + "Name "    + factory.TypeNVarChar + "(" + NAME_MAX_LENGTH.ToString() + ") NOT NULL,"
                 + "Note "    + factory.TypeNVarChar + "(100),"      // Poznamka krivky v PC muze byt dlouha
                 + "Units "   + factory.TypeByte                     // Pouzite jednotky
                 + ")");
        }

        /// <summary>
        /// Add new curve
        /// </summary>
        /// <param name="curve">Curve</param>
        /// <returns>Id of new curve</returns>
        public long Add(Curve curve) {
            // Jmeno musi byt zadane
            if (curve.Name == "") {
                throw new Exception("Curve name cannot be empty");
            }

            // Krivka nesmi mit zadany ID. Pokud ma ID vyplneny, krivka uz byla do DB ulozena a musim pouzit Update.
            if (curve.Id > 0) {
                throw new Exception("Curve ID > 0");
            }
            
            // Ulozim novou krivku
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (NULL, "
                                                           + "@Name, @Note, @Units); SELECT last_insert_rowid()")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Name",        curve.Name));
                command.Parameters.Add(factory.CreateParameter("@Note",        curve.Note));
                command.Parameters.Add(factory.CreateParameter("@Units", (byte)curve.Units));
                return (long)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Update curve data
        /// </summary>
        /// <param name="curve">Curve</param>
        /// <returns>Number of rows affected</returns>
        public int Update(Curve curve) {
            // Jmeno musi byt zadane
            if (curve.Name == "") {
                throw new Exception("Curve name cannot be empty");
            }
            
            // Krivka uz musi mit zadany ID, tj. uz je v databazi ulozena
            if (curve.Id <= 0) {
                throw new Exception("Curve ID <= 0");
            }

            // Updatuju krivku
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "Name = @Name, Note = @Note, Units = @Units "
                                                           + "WHERE CurveId = @CurveId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@CurveId", (long)curve.Id));
                command.Parameters.Add(factory.CreateParameter("@Name",          curve.Name));
                command.Parameters.Add(factory.CreateParameter("@Note",          curve.Note));
                command.Parameters.Add(factory.CreateParameter("@Units",   (byte)curve.Units));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Delete curve
        /// </summary>
        /// <param name="weighingId">Curve Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long curveId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE CurveId = @CurveId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@CurveId", curveId));
                return command.ExecuteNonQuery();
            }
        }

        private Curve ReaderToCurve(DbDataReader reader) {
            Curve curve = new Curve();
            curve.Id    = (long)        reader["CurveId"];
            curve.Name  = (string)      reader["Name"];
            curve.Note  = (string)      reader["Note"];
            curve.Units = (Units)(byte) reader["Units"];

            return curve;
        }
        
        /// <summary>
        /// Load curve from database
        /// </summary>
        /// <param name="curveId">Curve Id</param>
        /// <returns>Curve instance</returns>
        public Curve Load(long curveId) {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE CurveId = @CurveId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@CurveId", curveId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        throw new Exception(String.Format("Curve Id {0} don't exist", curveId));
                    }
                    return ReaderToCurve(reader);
                }
            }
        }

        /// <summary>
        /// Load curve list from database
        /// </summary>
        /// <returns>CurveList instance</returns>
        public CurveList LoadList() {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName)) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    CurveList curveList = new CurveList();
                    while (reader.Read()) {
                        curveList.Add(ReaderToCurve(reader));
                    }

                    return curveList;
                }
            }
        }
    
    }
}