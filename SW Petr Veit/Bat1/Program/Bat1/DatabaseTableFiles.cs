﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    public class DatabaseTableFiles : DatabaseTable {

        // Tabulka se soubory vcetne file-specific configu. Soubory jsou primarne soucasti globalniho configu,
        // ktery se vytvori pri kazdem stazeni dat z vahy. Soubory je tedy mozne mazat pouze pokud se zaroven
        // smaze i cely globalni config. Na jednotlive soubory se odkazuji i vysledky stazene z vahy v tabulce
        // Weighings.
    
        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableFiles(DatabaseFactory factory)
            : base("Files", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "FileId "             + factory.TypeLong + " PRIMARY KEY AUTOINCREMENT,"
                 + "ScaleConfigId "      + factory.TypeLong     + " NOT NULL,"      // Nastaveni vahy, ke kteremu soubor patri
                 + "Name "               + factory.TypeNVarChar + "(15) NOT NULL,"
                 + "Note "               + factory.TypeNVarChar + "(15),"           // Poznamka ve vaze, tj. pouze 15 znaku
                 + "EnableMoreBirds "    + factory.TypeByte     + ","               // Pridano ve verzi 7.0.1.0, spada do File parameters
                 + "NumberOfBirds "      + factory.TypeByte     + ","
                 + "WeightSortingMode "  + factory.TypeByte     + ","
                 + "LowLimit "           + factory.TypeDouble   + ","
                 + "HighLimit "          + factory.TypeDouble   + ","
                 + "SavingMode "         + factory.TypeByte     + ","
                 + "Filter "             + factory.TypeDouble   + ","
                 + "StabilizationTime "  + factory.TypeDouble   + ","
                 + "StabilizationRange " + factory.TypeDouble   + ","
                 + "MinimumWeight "      + factory.TypeDouble   + ","
                 + "CONSTRAINT FK_P_ScaleConfigs FOREIGN KEY (ScaleConfigId) REFERENCES ScaleConfigs(ScaleConfigId)"
                 + ")");

            // Index
/*            using (DbCommand command = factory.CreateCommand("CREATE INDEX IDX_FilesName ON Files (Name)")) {
                command.ExecuteNonQuery();
            }*/

        }

        /// <summary>
        /// Update table from version 7.0.0 to 7.0.1.0
        /// </summary>
        private void UpdateToVersion_7_0_1_0() {
            // Ve verzi 7.0.1.0 jsem pridal sloupec povoleni vazeni vice kurat. V betaverzich vahy to bylo pouze
            // globalne, ale v prvni verejne verzi se volba presunula do File parameters
            if (ColumnExists("EnableMoreBirds")) {
                return;     // Neni treba update
            }

            // Pridam sloupec EnableMoreBirds
            AddColumn("EnableMoreBirds", factory.TypeByte);

            // Vyplnim ve vsech radcich EnableMoreBirds na false
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET EnableMoreBirds = @EnableMoreBirds")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@EnableMoreBirds", 0));
                command.ExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// Update table definition
        /// </summary>
        public void Update() {
            UpdateToVersion_7_0_1_0();
        }

        /// <summary>
        /// Add new file
        /// </summary>
        /// <param name="file">File to add</param>
        /// <param name="scaleConfigId">ScaleConfig Id this file belongs to</param>
        /// <returns>Id of new file</returns>
        public long Add(File file, long scaleConfigId) {
            // Jmeno musi byt zadane
            if (file.Name == "") {
                throw new Exception("File name cannot be empty");
            }
            
            // Ulozim novy soubor
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " "
                                                           + "(FileId, ScaleConfigId, Name, Note, EnableMoreBirds, "
                                                           + "NumberOfBirds, WeightSortingMode, LowLimit, HighLimit, "
                                                           + "SavingMode, Filter, StabilizationTime, "
                                                           + "StabilizationRange, MinimumWeight) "
                                                           + "VALUES (NULL, @ScaleConfigId, @Name, @Note, @EnableMoreBirds, "
                                                           + "@NumberOfBirds, @WeightSortingMode, @LowLimit, @HighLimit, "
                                                           + "@SavingMode, @Filter, @StabilizationTime, "
                                                           + "@StabilizationRange, @MinimumWeight);"
                                                           + "SELECT last_insert_rowid()")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                command.Parameters.Add(factory.CreateParameter("@Name", file.Name));
                command.Parameters.Add(factory.CreateParameter("@Note", file.Note));
                command.Parameters.Add(factory.CreateParameter("@EnableMoreBirds",         file.FileConfig.WeighingConfig.Saving.EnableMoreBirds));
                command.Parameters.Add(factory.CreateParameter("@NumberOfBirds",     (byte)file.FileConfig.WeighingConfig.Saving.NumberOfBirds));
                command.Parameters.Add(factory.CreateParameter("@WeightSortingMode", (byte)file.FileConfig.WeighingConfig.WeightSorting.Mode));
                command.Parameters.Add(factory.CreateParameter("@LowLimit",                file.FileConfig.WeighingConfig.WeightSorting.LowLimit));
                command.Parameters.Add(factory.CreateParameter("@HighLimit",               file.FileConfig.WeighingConfig.WeightSorting.HighLimit));
                command.Parameters.Add(factory.CreateParameter("@SavingMode",        (byte)file.FileConfig.WeighingConfig.Saving.Mode));
                command.Parameters.Add(factory.CreateParameter("@Filter",                  file.FileConfig.WeighingConfig.Saving.Filter));
                command.Parameters.Add(factory.CreateParameter("@StabilizationTime",       file.FileConfig.WeighingConfig.Saving.StabilisationTime));
                command.Parameters.Add(factory.CreateParameter("@StabilizationRange",      file.FileConfig.WeighingConfig.Saving.StabilisationRange));
                command.Parameters.Add(factory.CreateParameter("@MinimumWeight",           file.FileConfig.WeighingConfig.Saving.MinimumWeight));
                return (long)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Add file list
        /// </summary>
        /// <param name="fileList">File list</param>
        /// <param name="scaleConfigId">ScaleConfig Id this file belongs to</param>
        /// <returns>List of IDs saved to the database</returns>
        public List<long> Add(FileList fileList, long scaleConfigId) {
            List<long> listId = new List<long>();
            foreach (File file in fileList.List) {
                listId.Add(Add(file, scaleConfigId));           // Zaroven vytvarim seznam ID jednotlivych souboru
            }
            return listId;
        }

        /// <summary>
        /// Delete all files that belong to specified config
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long scaleConfigId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Read current record from reader and convert it to a File instance
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private File ReaderToFile(DbDataReader reader) {
            // Nastaveni souboru
            FileConfig fileConfig = new FileConfig();
            fileConfig.WeighingConfig.Saving.EnableMoreBirds    = (bool)((byte) reader["EnableMoreBirds"] == 1);
            fileConfig.WeighingConfig.Saving.NumberOfBirds      = (byte)  reader["NumberOfBirds"];
            fileConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)(byte)reader["WeightSortingMode"];
            fileConfig.WeighingConfig.WeightSorting.LowLimit    = (double)reader["LowLimit"];
            fileConfig.WeighingConfig.WeightSorting.HighLimit   = (double)reader["HighLimit"];
            fileConfig.WeighingConfig.Saving.Mode               = (SavingMode)(byte)reader["SavingMode"];
            fileConfig.WeighingConfig.Saving.Filter             = (double)reader["Filter"];
            fileConfig.WeighingConfig.Saving.StabilisationTime  = (double)reader["StabilizationTime"];
            fileConfig.WeighingConfig.Saving.StabilisationRange = (double)reader["StabilizationRange"];
            fileConfig.WeighingConfig.Saving.MinimumWeight      = (double)reader["MinimumWeight"];

            // Vytvorim novy soubor a vratim
            return new File((string)reader["Name"], (string)reader["Note"], fileConfig);
        }
        
        /// <summary>
        /// Load list of files that belong to specified scale config
        /// </summary>
        /// <param name="scaleConfigId">ScaleConfig ID</param>
        /// <param name="idList">List of file IDs in the database</param>
        /// <returns>List of files</returns>
        public FileList LoadFileList(long scaleConfigId, out List<long> fileIdList) {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    FileList fileList = new FileList();
                    fileIdList = new List<long>();
                    while (reader.Read()) {
                        // ID souboru do seznamu zvlast
                        fileIdList.Add((long)reader["FileId"]);
                        
                        // Nactu soubor
                        fileList.Add(ReaderToFile(reader));
                    }
                    return fileList;
                }
            }
        }

        /// <summary>
        /// Load file from the database
        /// </summary>
        /// <param name="fileId">Item Id</param>
        /// <returns>File instance</returns>
        public File Load(long fileId) {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE FileId = @FileId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FileId", fileId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        // Soubor neexistuje
                        return null;
                    }

                    return ReaderToFile(reader);
                }
            }
        }

        /// <summary>
        /// Load file with a specified name that belongs to a specified scale config
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID that the file belongs to</param>
        /// <param name="name">File name</param>
        /// <returns>File ID</returns>
        public long LoadId(long scaleConfigId, string name) {
            using (DbCommand command = factory.CreateCommand("SELECT FileId FROM " + tableName + " " +
                                                             "WHERE ScaleConfigId = @ScaleConfigId " +
                                                             "AND Name = @Name")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                command.Parameters.Add(factory.CreateParameter("@Name",          name));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        // Soubor neexistuje
                        return -1;
                    }

                    return (long)reader["FileId"];
                }
            }
        }

        /// <summary>
        /// Load file name and note from the database
        /// </summary>
        /// <param name="weighingId">Item Id</param>
        /// <returns>NameNote instance</returns>
        public NameNote LoadNameNote(long fileId) {
            using (DbCommand command = factory.CreateCommand("SELECT Name, Note FROM " + tableName + " WHERE FileId = @FileId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FileId", fileId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        throw new Exception(String.Format("File Id {0} doesn't exist", fileId));
                    }
                    return new NameNote((string)reader["Name"], (string)reader["Note"]);
                }
            }
        }

        /// <summary>
        /// Load file name from the database
        /// </summary>
        /// <param name="weighingId">Item Id</param>
        /// <returns>File name</returns>
        public string LoadName(long fileId) {
            using (DbCommand command = factory.CreateCommand("SELECT Name FROM " + tableName + " WHERE FileId = @FileId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FileId", fileId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        throw new Exception(String.Format("File Id {0} doesn't exist", fileId));
                    }
                    return (string)reader["Name"];
                }
            }
        }

        /// <summary>
        /// Load sorted list of all file names from the database
        /// </summary>
        /// <returns>Sorted list of scale names</returns>
        public List<string> LoadNameList() {
            using (DbCommand command = factory.CreateCommand("SELECT Name FROM " + tableName + " "
                                                           + "GROUP BY Name")) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    List<string> list = new List<string>();
                    while (reader.Read()) {
                        list.Add((string)reader["Name"]);
                    }
                    list.Sort();
                    return list;
                }
            }
        }

        /// <summary>
        /// Load note that has been used last time for a specified file
        /// </summary>
        /// <param name="name">File name</param>
        private string LoadLastNote(string name) {
            using (DbCommand command = factory.CreateCommand("SELECT Note FROM " + tableName + " WHERE Name = @Name")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Name", name));
                using (DbDataReader reader = command.ExecuteReader()) {
                    // Zaznamy jsou setridene podle ID, tj. podle okamziku vlozeni
                    string note = "";
                    while (reader.Read()) {
                        note = (string)reader["Note"];
                    }
                    return note;
                }
            }
        }

        /// <summary>
        /// Load sorted list of all file names and last used notes from the database
        /// </summary>
        /// <returns>List of file names and notes</returns>
        public List<NameNote> LoadNameNoteList() {
            // Nejprve nactu seznam jmen souboru
            List<NameNote> list = new List<NameNote>();
            using (DbCommand command = factory.CreateCommand("SELECT Name, Note FROM " + tableName + " "
                                                           + "GROUP BY Name")) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    while (reader.Read()) {
                        list.Add(new NameNote((string)reader["Name"], ""));
                    }
                    list.Sort(NameNote.CompareByName);
                }
            }

            // Projizdim jednotlive soubory a nacitam naposledu pouzite poznamky
            foreach (NameNote file in list) {
                file.SetNote(LoadLastNote(file.Name));
            }

            return list;
        }

        /// <summary>
        /// Update file name and note
        /// </summary>
        /// <param name="fileId">File Id</param>
        /// <param name="name">New name</param>
        /// <param name="note">New note</param>
        public void Update(long fileId, string name, string note) {
            // Jmeno musi byt zadane
            if (name == "") {
                throw new Exception("File name cannot be empty");
            }

            // Updatuju soubor
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "Name = @Name, Note = @Note "
                                                           + "WHERE FileId = @FileId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FileId", fileId));
                command.Parameters.Add(factory.CreateParameter("@Name",   name));
                command.Parameters.Add(factory.CreateParameter("@Note",   note));
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Change units in config of all files that belong to specified config
        /// </summary>
        /// <param name="scaleConfigId"></param>
        /// <param name="oldUnits"></param>
        /// <param name="newUnits"></param>
        public void ChangeUnits(long scaleConfigId, ScaleConfig scaleConfig, Units oldUnits) {
            // V configu musi zustat vsechny puvodni soubory, zmenily se pouze jednotky a nasledne 
            // se prepocetly vsechny hodnoty v globalnim configu a configu jednotlivych souboru
            
            // Musim updatovat postupne kazdy soubor, u kazdeho souboru muze byt jiny config. Updatuju pouze
            // polozky, na kltere ma vliv zmena jednotek (odpovida fci ScaleConfig.SetNewUnits())
            foreach (File file in scaleConfig.FileList.List) {
                using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                               + "LowLimit = @LowLimit, HighLimit = @HighLimit, MinimumWeight = @MinimumWeight "
                                                               + "WHERE ScaleConfigId = @ScaleConfigId AND Name = @Name")) {
                    command.Parameters.Clear();
                    command.Parameters.Add(factory.CreateParameter("@LowLimit",      file.FileConfig.WeighingConfig.WeightSorting.LowLimit));
                    command.Parameters.Add(factory.CreateParameter("@HighLimit",     file.FileConfig.WeighingConfig.WeightSorting.HighLimit));
                    command.Parameters.Add(factory.CreateParameter("@MinimumWeight", file.FileConfig.WeighingConfig.Saving.MinimumWeight));
                    command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                    command.Parameters.Add(factory.CreateParameter("@Name",          file.Name));
                    command.ExecuteNonQuery();
                }
            }
            
        }


    }
}
