﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Ionic.Utils.Zip;

namespace Bat1 {
    /// <summary>
    /// Database backup
    /// </summary>
    public static class Backup {
        
        /// <summary>
        /// Backup database
        /// </summary>
        /// <param name="backupFileName">File name including path</param>
        /// <param name="compact">True if database should be verified and compacted prior to saving to backup</param>
        private static void ExecuteBackup(string backupFileName, bool compact) {
            // Udelam si kopii databaze do docasneho souboru, original necham radeji netknuty
            string tempFileName = Program.TempFolder + @"\" + Program.DatabaseFileName;
            Program.Database.Copy(tempFileName);

            try {    
                // Vytvorim novy objekt databaze na docasne kopii
                Database database = new Database(tempFileName);
                
                if (compact) {
                    // Provedu VACUUM, cimz se zmensi velikost a opravi nektere chyby
                    database.Vacuum();
                    
                    // Zkontroluju integritu databaze - az po prikazu VACUUM, ktery muze opravit nektere chyby
                    if (!database.CheckIntegrity()) {
                        // Korupce dat, nepovolim zalohovani
                        throw new Exception("Database integrity failure");
                    }
                }

                // Pokud soubor zalohy existuje, smazu ho
                if (System.IO.File.Exists(backupFileName)) {
                    System.IO.File.Delete(backupFileName);
                }
                
                // Zazipuju
                ZipFile zipFile = new ZipFile(backupFileName);
                zipFile.TempFileFolder = Path.GetTempPath();    // Standardni docasny adresar, jinak vytvari soubory v adresari EXE
                zipFile.AddFile(tempFileName, "");              // Bez cesty
                zipFile.Save();
            } finally {
                // Na zaver smazu docasny soubor
                System.IO.File.Delete(tempFileName);
            }
        }

        /// <summary>
        /// Compact and backup database
        /// </summary>
        /// <param name="backupFileName">File name including path</param>
        public static void BackupWithCompaction(string backupFileName) {
            ExecuteBackup(backupFileName, true);
        }

        /// <summary>
        /// Backup database without integrity check and compaction
        /// </summary>
        /// <param name="backupFileName">File name including path</param>
        public static void BackupWithoutCompaction(string backupFileName) {
            ExecuteBackup(backupFileName, false);
        }

        /// <summary>
        /// Restore database from backup
        /// </summary>
        /// <param name="backupFileName">Backup file name including path</param>
        public static bool Restore(string backupFileName) {
            // Zkontroluju, zda soubor existuje
            if (!System.IO.File.Exists(backupFileName)) {
                throw new Exception("File doesn't exist");
            }
            
            // Rozbalim databazi do docasneho adresare
            ZipFile zipFile = new ZipFile(backupFileName);
            zipFile.TempFileFolder = Path.GetTempPath();        // Standardni docasny adresar, jinak vytvari soubory v adresari EXE
            zipFile.ExtractAll(Program.TempFolder, true);       // Vcetne prepisu, pokud uz existuje
            string tempFileName = Program.TempFolder + @"\" + Program.DatabaseFileName;

            try {
                // Zkontroluju, zda je databaze v poradku
                Database database = new Database(tempFileName);
                if (!database.CheckIntegrity()) {
                    // Korupce dat, nepovolim obnovu
                    throw new Exception("Database integrity failure");
                }

                // Zkontroluji platnost nastaveni
                database.CheckSetup();
                
                // Zkontroluji verzi databaze, stejne jako pri spusteni programu
                if (!database.Update()) {
                    return false;       // Databaze je novejsi nez SW, musi upgradovat SW
                }
                
                // Prekopiruji rozbalenou databazi do pracovniho adresare a prepisu tak stavajici databazi
                System.IO.File.Copy(tempFileName, Program.FullDatabaseFileName, true);    // Vcetne prepisu

                return true;
            } finally {
                // Na zaver smazu docasny soubor
                System.IO.File.Delete(tempFileName);
            }
        }
    }
}
