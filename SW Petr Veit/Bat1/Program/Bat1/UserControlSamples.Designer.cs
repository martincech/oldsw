﻿namespace Bat1 {
    partial class UserControlSamples {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlSamples));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panelGrid = new System.Windows.Forms.Panel();
            this.dataGridViewSamples = new System.Windows.Forms.DataGridView();
            this.ColumnDateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnWeight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flowLayoutPanelBottom = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonDivide = new System.Windows.Forms.Button();
            this.buttonUnits = new System.Windows.Forms.Button();
            this.panelGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSamples)).BeginInit();
            this.flowLayoutPanelBottom.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelGrid
            // 
            resources.ApplyResources(this.panelGrid, "panelGrid");
            this.panelGrid.Controls.Add(this.dataGridViewSamples);
            this.panelGrid.Controls.Add(this.flowLayoutPanelBottom);
            this.panelGrid.Name = "panelGrid";
            // 
            // dataGridViewSamples
            // 
            resources.ApplyResources(this.dataGridViewSamples, "dataGridViewSamples");
            this.dataGridViewSamples.AllowUserToAddRows = false;
            this.dataGridViewSamples.AllowUserToDeleteRows = false;
            this.dataGridViewSamples.AllowUserToResizeRows = false;
            this.dataGridViewSamples.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewSamples.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridViewSamples.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dataGridViewSamples.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSamples.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnDateTime,
            this.ColumnWeight,
            this.ColumnFlag});
            this.dataGridViewSamples.MultiSelect = false;
            this.dataGridViewSamples.Name = "dataGridViewSamples";
            this.dataGridViewSamples.ReadOnly = true;
            this.dataGridViewSamples.RowHeadersVisible = false;
            this.dataGridViewSamples.RowTemplate.Height = 18;
            this.dataGridViewSamples.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewSamples.ShowCellToolTips = false;
            this.dataGridViewSamples.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewSamples_CellClick);
            this.dataGridViewSamples.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridViewSamples_KeyDown);
            // 
            // ColumnDateTime
            // 
            this.ColumnDateTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            resources.ApplyResources(this.ColumnDateTime, "ColumnDateTime");
            this.ColumnDateTime.Name = "ColumnDateTime";
            this.ColumnDateTime.ReadOnly = true;
            this.ColumnDateTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // ColumnWeight
            // 
            this.ColumnWeight.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ColumnWeight.DefaultCellStyle = dataGridViewCellStyle1;
            resources.ApplyResources(this.ColumnWeight, "ColumnWeight");
            this.ColumnWeight.Name = "ColumnWeight";
            this.ColumnWeight.ReadOnly = true;
            this.ColumnWeight.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // ColumnFlag
            // 
            this.ColumnFlag.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColumnFlag.DefaultCellStyle = dataGridViewCellStyle2;
            resources.ApplyResources(this.ColumnFlag, "ColumnFlag");
            this.ColumnFlag.Name = "ColumnFlag";
            this.ColumnFlag.ReadOnly = true;
            this.ColumnFlag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // flowLayoutPanelBottom
            // 
            resources.ApplyResources(this.flowLayoutPanelBottom, "flowLayoutPanelBottom");
            this.flowLayoutPanelBottom.Controls.Add(this.buttonDelete);
            this.flowLayoutPanelBottom.Controls.Add(this.buttonDivide);
            this.flowLayoutPanelBottom.Controls.Add(this.buttonUnits);
            this.flowLayoutPanelBottom.Name = "flowLayoutPanelBottom";
            // 
            // buttonDelete
            // 
            resources.ApplyResources(this.buttonDelete, "buttonDelete");
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonDivide
            // 
            resources.ApplyResources(this.buttonDivide, "buttonDivide");
            this.buttonDivide.Name = "buttonDivide";
            this.buttonDivide.UseVisualStyleBackColor = true;
            this.buttonDivide.Click += new System.EventHandler(this.buttonDivide_Click);
            // 
            // buttonUnits
            // 
            resources.ApplyResources(this.buttonUnits, "buttonUnits");
            this.buttonUnits.Name = "buttonUnits";
            this.buttonUnits.UseVisualStyleBackColor = true;
            this.buttonUnits.Click += new System.EventHandler(this.buttonUnits_Click);
            // 
            // UserControlSamples
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelGrid);
            this.Name = "UserControlSamples";
            this.panelGrid.ResumeLayout(false);
            this.panelGrid.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSamples)).EndInit();
            this.flowLayoutPanelBottom.ResumeLayout(false);
            this.flowLayoutPanelBottom.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelGrid;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelBottom;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonDivide;
        private System.Windows.Forms.DataGridView dataGridViewSamples;
        private System.Windows.Forms.Button buttonUnits;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnDateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnWeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnFlag;

    }
}
