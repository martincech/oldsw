﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlScaleConfigDisplay : UserControlScaleConfigBase {

        public UserControlScaleConfigDisplay(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Preberu Read-only
            this.readOnly = readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        public override void Redraw() {
            isLoading = true;

            try {
                comboBoxBacklightMode.SelectedIndex  = (int)scaleConfig.Display.Backlight.Mode;
                numericUpDownBacklightDuration.Text  =      scaleConfig.Display.Backlight.Duration.ToString();
                numericUpDownBacklightIntensity.Text =      scaleConfig.Display.Backlight.Intensity.ToString();
                comboBoxDisplayMode.SelectedIndex    = (int)scaleConfig.Display.Mode;

                // Povoleni zobrazeni na zaklade nastaveni
                SetBacklightMode();
            } finally {
                isLoading = false;
            }
        }

        private void EnableDuration(bool enable) {
            labelDuration.Enabled                  = enable;
            numericUpDownBacklightDuration.Enabled = enable;
            labelDurationSec.Enabled               = enable;
        }
        
        private void EnableIntensity(bool enable) {
            labelIntensity.Enabled                  = enable;
            numericUpDownBacklightIntensity.Enabled = enable;
        }

        private void SetBacklightMode() {
            switch ((BacklightMode)comboBoxBacklightMode.SelectedIndex) {
                case BacklightMode.BACKLIGHT_MODE_AUTO:
                    EnableDuration(true);
                    EnableIntensity(true);
                    break;
                case BacklightMode.BACKLIGHT_MODE_OFF:
                    EnableDuration(false);
                    EnableIntensity(false);
                    break;
                case BacklightMode.BACKLIGHT_MODE_ON:
                    EnableDuration(false);
                    EnableIntensity(true);
                    break;
            }
        }
        
        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }
            scaleConfig.Display.Backlight.Mode = (BacklightMode)comboBoxBacklightMode.SelectedIndex;
            SetBacklightMode();
            scaleConfig.Display.Backlight.Duration = (int)numericUpDownBacklightDuration.Value;
            scaleConfig.Display.Backlight.Intensity = (int)numericUpDownBacklightIntensity.Value;
            scaleConfig.Display.Mode = (DisplayMode)comboBoxDisplayMode.SelectedIndex;
        }

        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e) {
            ControlsToConfig();
        }

    }
}
