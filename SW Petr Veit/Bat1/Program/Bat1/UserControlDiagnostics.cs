﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlDiagnostics : UserControl {
        public UserControlDiagnostics() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            labelDatabase.Text = Program.Database.DatabaseFileName;
        }

        private void buttonRead_Click(object sender, EventArgs e) {
            // Zeptam se na jmeno souboru
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = FileExtension.HARDCOPY;
            saveFileDialog.Filter     = "*." + FileExtension.HARDCOPY + "|*." + FileExtension.HARDCOPY;
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }
            
            Refresh();
            Cursor.Current = Cursors.WaitCursor;
            FormScaleWait form = new FormScaleWait(ParentForm);
            try {
                form.Show();
                form.Refresh();
                Bat1Version7 bat1 = null;
                try {
                    bat1 = new Bat1Version7();
                } catch {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Properties.Resources.DRIVER_NOT_INSTALLED, Program.ApplicationName);
                    return;
                }
                if (!bat1.ReadEeprom(saveFileDialog.FileName)) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                }
            } finally {
                form.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonCopy_Click(object sender, EventArgs e) {
            // Pokud soubor neexistuje, zobrazim chybu (i kdyz by to nemelo nastat)
            if (!System.IO.File.Exists(Program.Database.DatabaseFileName)) {
                MessageBox.Show(Properties.Resources.FILE_DOESNT_EXIST, Program.ApplicationName);
                return;
            }

            // Zeptam se na nazev ciloveho souboru
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = FileExtension.DATABASE;
            saveFileDialog.Filter = Program.ApplicationName + " database file (*." + FileExtension.DATABASE + ")|*." + FileExtension.DATABASE;     // Neni treba prekladat
            if (saveFileDialog.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Zkopiruju soubor, muze to trvat
            Refresh();
            Cursor.Current = Cursors.WaitCursor;
            try {
                Program.Database.Copy(saveFileDialog.FileName);
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }
    }
}
