﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;
using Veit.Bat1;
using Veit.Scale;
using Veit.ScaleStatistics;

namespace Bat1 {
    
    /// <summary>
    /// ScaleConfigs table
    /// </summary>
    public class DatabaseTableScaleConfigs : DatabaseTable {

        // Tabulka s globalnimi nastavenimi vahy. Globalni nastaveni je bud soucasti stazeneho vazeni z vahy
        // (sloupec Name = NULL), nebo jde o preddefinovanou sablonu nastaveni (sloupec Name je vyplnen jako
        // jmeno sablony).

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableScaleConfigs(DatabaseFactory factory)
            : base("ScaleConfigs", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "ScaleConfigId "        + factory.TypeLong     + " PRIMARY KEY AUTOINCREMENT,"
                 + "Name "                 + factory.TypeNVarChar + "(100),"            // Jmeno preddefinovaneho nastaveni v PC, pri stazeni z vahy = NULL
                 + "MajorVersion "         + factory.TypeSmallInt + ","                 // Major cislo verze vahy (1.xx.xx)
                 + "MinorVersion "         + factory.TypeSmallInt + ","                 // Minor cislo verze vahy (x.01.xx)
                 + "BuildVersion "         + factory.TypeSmallInt + ","                 // Cislo buildu vahy (x.xx.01)
                 + "HwVersion "            + factory.TypeByte     + ","                 // Cislo HW verze vahy
                 + "ScaleName "            + factory.TypeNVarChar + "(15),"             // Jmeno vahy 15 znaku
                 + "Country "              + factory.TypeByte     + ","               
                 + "Language "             + factory.TypeByte     + ","               
                 + "DateFormat "           + factory.TypeByte     + ","               
                 + "DateSeparator1 "       + factory.TypeNChar    + "(1),"               
                 + "DateSeparator2 "       + factory.TypeNChar    + "(1),"               
                 + "TimeFormat "           + factory.TypeByte     + ","               
                 + "TimeSeparator "        + factory.TypeNChar    + "(1),"               
                 + "DaylightSavingMode "   + factory.TypeByte     + ","               
                 + "Units "                + factory.TypeByte     + ","               
                 + "UnitsDivision "        + factory.TypeDouble   + ","               
                 + "SoundsToneDefault "    + factory.TypeByte     + ","               
                 + "SoundsToneLight "      + factory.TypeByte     + ","               
                 + "SoundsToneOk "         + factory.TypeByte     + ","               
                 + "SoundsToneHeavy "      + factory.TypeByte     + ","               
                 + "SoundsToneKeyboard "   + factory.TypeByte     + ","               
                 + "SoundsEnableSpecial "  + factory.TypeByte     + ","               
                 + "SoundsVolumeKeyboard " + factory.TypeByte     + ","               
                 + "SoundsVolumeSaving "   + factory.TypeByte     + ","               
                 + "DisplayMode "          + factory.TypeByte     + ","               
                 + "BacklightMode "        + factory.TypeByte     + ","               
                 + "BacklightIntensity "   + factory.TypeByte     + ","               
                 + "BacklightDuration "    + factory.TypeByte     + ","               
                 + "PrinterPaperWidth "    + factory.TypeByte     + ","               
                 + "PrinterCommFormat "    + factory.TypeByte     + ","               
                 + "PrinterCommSpeed "     + factory.TypeInteger  + ","               
                 + "PowerOffTimeout "      + factory.TypeSmallInt + ","               
                 + "EnableMoreBirds "      + factory.TypeByte     + ","               
                 + "EnableFileParameters " + factory.TypeByte     + ","               
                 + "EnablePassword "       + factory.TypeByte     + ","               
                 + "Password "             + factory.TypeInteger  + ","               
                 + "WeightSortingMode "    + factory.TypeByte     + ","               
                 + "SavingMode "           + factory.TypeByte     + ","               
                 + "Filter "               + factory.TypeDouble   + ","               
                 + "StabilisationTime "    + factory.TypeDouble   + ","               
                 + "MinimumWeight "        + factory.TypeDouble   + ","               
                 + "StabilisationRange "   + factory.TypeDouble   + ","               
                 + "UniformityRange "      + factory.TypeByte     + ","               
                 + "HistogramMode "        + factory.TypeByte     + ","               
                 + "HistogramRange "       + factory.TypeByte     + ","               
                 + "HistogramStep "        + factory.TypeDouble   + ","
                 + "ActiveFileIndex "      + factory.TypeSmallInt + ","     // Chci ukladat i zaporne hodnoty (-1)
                 + "WeighingCapacity "     + factory.TypeByte
                 + ")");
        }
        
        /// <summary>
        /// Update table from version 7.0.0 to 7.0.1.0
        /// </summary>
        private void UpdateToVersion_7_0_1_0() {
            // Ve verzi 7.0.1.0 jsem pridal sloupec pro index aktivniho souboru
            if (ColumnExists("ActiveFileIndex")) {
                return;     // Neni treba update
            }

            // Pridam sloupec ActiveFileIndex
            AddColumn("ActiveFileIndex", factory.TypeSmallInt);

            // Vyplnim ve vsech radcich ActiveFileIndex na -1 (tj. bez aktivniho souboru)
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET ActiveFileIndex = @ActiveFileIndex")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ActiveFileIndex", -1));
                command.ExecuteNonQuery();
            }
        }
        
        /// <summary>
        /// Update table from version 7.0.1.0 to 7.0.2.0
        /// </summary>
        private void UpdateToVersion_7_0_2_0() {
            // Ve verzi 7.0.2.0 jsem pridal sloupec pro vazivost 30 / 50kg
            if (ColumnExists("WeighingCapacity")) {
                return;     // Neni treba update
            }

            // Pridam sloupec WeighingCapacity
            AddColumn("WeighingCapacity", factory.TypeByte);

            // Vyplnim ve vsech radcich WeighingCapacity na 30kg
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET WeighingCapacity = @WeighingCapacity")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingCapacity", WeighingCapacity.NORMAL));
                command.ExecuteNonQuery();
            }
        }

/*        /// <summary>
        /// Update table from version 7.0.2.0 to 7.2.2.0
        /// </summary>
        private void UpdateToVersion_7_2_2_0(DatabaseVersion databaseVersion) {
            // Ve verzi 7.2.2.0 je pridana madarstina, doslo tedy k posunu enumu jazyku ve vaze.
            // Jazyk vahy tedy adekvatne posunu ve vsech preddefinovanych nastavenich vah.
            // Configy, ktere jsou soucasti vazeni v DB ponecham beze zmeny. Jazyky sice nebudou
            // sedet, ale kazdy takovy config obsahuje i cislo verze vahy, tj. v pripade potreby
            // si skutecny jazyk budu moci dopocitat.

            // Teoreticky by mohl nastat problem, pokud by ve firme pouzivali vice PC a kolovala
            // by pouze jedna DB pomoci backup-restore. Pokud by na ruznych PC byly ruzne verze SW,
            // k updatu DB by dochazelo vicekrat (pri kazdem obnoveni DB na PC s novejsim SW) a jazyk
            // by tak postupne odroloval buhvi kam. Toto nijak neresim.

            if (databaseVersion.Minor >= 2) {
                return;     // Neni treba update
            }

            // Nactu ID a jazyk u vsech preddefinovanych configu v tabulce



            // Pridam sloupec WeighingCapacity
            AddColumn("WeighingCapacity", factory.TypeByte);

            // Vyplnim ve vsech radcich WeighingCapacity na 30kg
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET WeighingCapacity = @WeighingCapacity")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@WeighingCapacity", WeighingCapacity.NORMAL));
                command.ExecuteNonQuery();
            }
        }*/

        /// <summary>
        /// Update table definition
        /// </summary>
        public void Update() {
            UpdateToVersion_7_0_1_0();
            UpdateToVersion_7_0_2_0();
//            UpdateToVersion_7_2_2_0(databaseVersion);
        }

        /// <summary>
        /// Encode password into single integer number
        /// </summary>
        /// <returns>Encoded password</returns>
        private int EncodePassword(ScaleConfig config) {
            int encoded = 0;
            for (int i = 0; i < Const.PASSWORD_LENGTH; i++) {
                if (config.PasswordConfig.Enable) {
                    encoded += config.PasswordConfig.Password[i];
                } else {
                    encoded += (int)Keys.K_NULL;        // Zakazane heslo = 4 x K_NULL
                }
                encoded *= 10;
            }
            return encoded / 10;        // Musim odstranit posledni nasobeni
        }
        
        /// <summary>
        /// Decode password from an integer number
        /// </summary>
        /// <returns>Decoded password</returns>
        private byte [] DecodePassword(int encoded) {
            byte [] password = new byte[Const.PASSWORD_LENGTH];
            for (int i = Const.PASSWORD_LENGTH - 1; i >= 0 ; i--) {
                password[i] = (byte)(encoded % 10);
                encoded /= 10;
            }
            return password;
        }

        private void AddParameters(ScaleConfig config, DbCommand command) {
            command.Parameters.Add(factory.CreateParameter("@MajorVersion",         config.Version.Major));
            command.Parameters.Add(factory.CreateParameter("@MinorVersion",         config.Version.Minor));
            command.Parameters.Add(factory.CreateParameter("@BuildVersion",         config.Version.Build));
            command.Parameters.Add(factory.CreateParameter("@HwVersion",            config.Version.Hw));
            command.Parameters.Add(factory.CreateParameter("@ScaleName",            config.ScaleName));
            command.Parameters.Add(factory.CreateParameter("@Country",              config.Country));
            command.Parameters.Add(factory.CreateParameter("@Language",             config.LanguageInDatabase));
            command.Parameters.Add(factory.CreateParameter("@DateFormat",           config.DateFormat));
            command.Parameters.Add(factory.CreateParameter("@DateSeparator1",       config.DateSeparator1.ToString()));
            command.Parameters.Add(factory.CreateParameter("@DateSeparator2",       config.DateSeparator2.ToString()));
            command.Parameters.Add(factory.CreateParameter("@TimeFormat",           config.TimeFormat));
            command.Parameters.Add(factory.CreateParameter("@TimeSeparator",        config.TimeSeparator.ToString()));
            command.Parameters.Add(factory.CreateParameter("@DaylightSavingMode",   config.DaylightSavingMode));
            command.Parameters.Add(factory.CreateParameter("@Units",                config.Units.Units));
            command.Parameters.Add(factory.CreateParameter("@UnitsDivision",        config.Units.Division));
            command.Parameters.Add(factory.CreateParameter("@SoundsToneDefault",    config.Sounds.ToneDefault));
            command.Parameters.Add(factory.CreateParameter("@SoundsToneLight",      config.Sounds.ToneLight));
            command.Parameters.Add(factory.CreateParameter("@SoundsToneOk",         config.Sounds.ToneOk));
            command.Parameters.Add(factory.CreateParameter("@SoundsToneHeavy",      config.Sounds.ToneHeavy));
            command.Parameters.Add(factory.CreateParameter("@SoundsToneKeyboard",   config.Sounds.ToneKeyboard));
            command.Parameters.Add(factory.CreateParameter("@SoundsEnableSpecial",  config.Sounds.EnableSpecial));
            command.Parameters.Add(factory.CreateParameter("@SoundsVolumeKeyboard", config.Sounds.VolumeKeyboard));
            command.Parameters.Add(factory.CreateParameter("@SoundsVolumeSaving",   config.Sounds.VolumeSaving));
            command.Parameters.Add(factory.CreateParameter("@DisplayMode",          config.Display.Mode));
            command.Parameters.Add(factory.CreateParameter("@BacklightMode",        config.Display.Backlight.Mode));
            command.Parameters.Add(factory.CreateParameter("@BacklightIntensity",   config.Display.Backlight.Intensity));
            command.Parameters.Add(factory.CreateParameter("@BacklightDuration",    config.Display.Backlight.Duration));
            command.Parameters.Add(factory.CreateParameter("@PrinterPaperWidth",    config.Printer.PaperWidth));
            command.Parameters.Add(factory.CreateParameter("@PrinterCommFormat",    config.Printer.CommunicationFormat));
            command.Parameters.Add(factory.CreateParameter("@PrinterCommSpeed",     config.Printer.CommunicationSpeed));
            command.Parameters.Add(factory.CreateParameter("@PowerOffTimeout",      config.PowerOffTimeout));
            command.Parameters.Add(factory.CreateParameter("@EnableMoreBirds",      config.WeighingConfig.Saving.EnableMoreBirds));
            command.Parameters.Add(factory.CreateParameter("@EnableFileParameters", config.EnableFileParameters));
            command.Parameters.Add(factory.CreateParameter("@EnablePassword",       config.PasswordConfig.Enable));
            command.Parameters.Add(factory.CreateParameter("@Password",             EncodePassword(config)));
            command.Parameters.Add(factory.CreateParameter("@WeightSortingMode",    config.WeighingConfig.WeightSorting.Mode));
            command.Parameters.Add(factory.CreateParameter("@SavingMode",           config.WeighingConfig.Saving.Mode));
            command.Parameters.Add(factory.CreateParameter("@Filter",               config.WeighingConfig.Saving.Filter));
            command.Parameters.Add(factory.CreateParameter("@StabilisationTime",    config.WeighingConfig.Saving.StabilisationTime));
            command.Parameters.Add(factory.CreateParameter("@MinimumWeight",        config.WeighingConfig.Saving.MinimumWeight));
            command.Parameters.Add(factory.CreateParameter("@StabilisationRange",   config.WeighingConfig.Saving.StabilisationRange));
            command.Parameters.Add(factory.CreateParameter("@UniformityRange",      config.StatisticConfig.UniformityRange));
            command.Parameters.Add(factory.CreateParameter("@HistogramMode",        config.StatisticConfig.Histogram.Mode));
            command.Parameters.Add(factory.CreateParameter("@HistogramRange",       config.StatisticConfig.Histogram.Range));
            command.Parameters.Add(factory.CreateParameter("@HistogramStep",        config.StatisticConfig.Histogram.Step));
            command.Parameters.Add(factory.CreateParameter("@ActiveFileIndex",      (short)config.ActiveFileIndex));
            command.Parameters.Add(factory.CreateParameter("@WeighingCapacity",     config.Units.WeighingCapacity));
        }
        
        /// <summary>
        /// Add predefined config on PC to the table
        /// </summary>
        /// <param name="config">Scale config</param>
        /// <param name="name">Name of predefined config on PC</param>
        /// <returns>Config Id</returns>
        public long Add(ScaleConfig config, string name) {
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (NULL, @Name,"
                                                           + "@MajorVersion, @MinorVersion, @BuildVersion, @HwVersion, @ScaleName, @Country,"
                                                           + "@Language, @DateFormat, @DateSeparator1, @DateSeparator2, @TimeFormat, @TimeSeparator,"
                                                           + "@DaylightSavingMode, @Units, @UnitsDivision, @SoundsToneDefault, @SoundsToneLight, @SoundsToneOk,"
                                                           + "@SoundsToneHeavy, @SoundsToneKeyboard, @SoundsEnableSpecial, @SoundsVolumeKeyboard,"
                                                           + "@SoundsVolumeSaving, @DisplayMode, @BacklightMode, @BacklightIntensity, @BacklightDuration,"
                                                           + "@PrinterPaperWidth, @PrinterCommFormat, @PrinterCommSpeed, @PowerOffTimeout,"
                                                           + "@EnableMoreBirds, @EnableFileParameters, @EnablePassword, @Password, @WeightSortingMode,"
                                                           + "@SavingMode, @Filter, @StabilisationTime, @MinimumWeight, @StabilisationRange, @UniformityRange,"
                                                           + "@HistogramMode, @HistogramRange, @HistogramStep, @ActiveFileIndex, @WeighingCapacity"
                                                           + "); "
                                                           + "SELECT last_insert_rowid()")) {
                command.Parameters.Clear();

                command.Parameters.Add(factory.CreateParameter("@Name", name));
                AddParameters(config, command);

                return (long)command.ExecuteScalar();
            }
        }
        
        /// <summary>
        /// Update config parameters
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        /// <param name="config">Scale config</param>
        public void Update(long scaleConfigId, ScaleConfig config) {
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "MajorVersion=@MajorVersion, MinorVersion=@MinorVersion, BuildVersion=@BuildVersion, HwVersion=@HwVersion, ScaleName=@ScaleName, Country=@Country,"
                                                           + "Language=@Language, DateFormat=@DateFormat, DateSeparator1=@DateSeparator1, DateSeparator2=@DateSeparator2, TimeFormat=@TimeFormat, TimeSeparator=@TimeSeparator,"
                                                           + "DaylightSavingMode=@DaylightSavingMode, Units=@Units, UnitsDivision=@UnitsDivision, SoundsToneDefault=@SoundsToneDefault, SoundsToneLight=@SoundsToneLight, SoundsToneOk=@SoundsToneOk,"
                                                           + "SoundsToneHeavy=@SoundsToneHeavy, SoundsToneKeyboard=@SoundsToneKeyboard, SoundsEnableSpecial=@SoundsEnableSpecial, SoundsVolumeKeyboard=@SoundsVolumeKeyboard,"
                                                           + "SoundsVolumeSaving=@SoundsVolumeSaving, DisplayMode=@DisplayMode, BacklightMode=@BacklightMode, BacklightIntensity=@BacklightIntensity, BacklightDuration=@BacklightDuration,"
                                                           + "PrinterPaperWidth=@PrinterPaperWidth, PrinterCommFormat=@PrinterCommFormat, PrinterCommSpeed=@PrinterCommSpeed, PowerOffTimeout=@PowerOffTimeout,"
                                                           + "EnableMoreBirds=@EnableMoreBirds, EnableFileParameters=@EnableFileParameters, EnablePassword=@EnablePassword, Password=@Password, WeightSortingMode=@WeightSortingMode,"
                                                           + "SavingMode=@SavingMode, Filter=@Filter, StabilisationTime=@StabilisationTime, MinimumWeight=@MinimumWeight, StabilisationRange=@StabilisationRange, UniformityRange=@UniformityRange,"
                                                           + "HistogramMode=@HistogramMode, HistogramRange=@HistogramRange, HistogramStep=@HistogramStep, ActiveFileIndex=@ActiveFileIndex, WeighingCapacity=@WeighingCapacity "
                                                           + "WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();

                // Jmeno ponecham
                AddParameters(config, command);
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));

                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Add config donwloaded from the scale
        /// </summary>
        /// <param name="config">Scale config</param>
        /// <returns>Config Id</returns>
        public long Add(ScaleConfig config) {
            return Add(config, null);       // Nastaveni nactena z vahy musi mit jmeno NULL
        }

        /// <summary>
        /// Check if specified predefined config exists
        /// </summary>
        /// <param name="name">Predefined config name</param>
        /// <returns>True if exists</returns>
        public bool Exists(string name) {
            return ValueExists("Name", name);
        }
        
        /// <summary>
        /// Get Id of the specified predefined config. If the name doesn't exist, an exception is thrown.
        /// </summary>
        /// <param name="name">Predefined config name</param>
        /// <returns>Id</returns>
        public long GetId(string name) {
            using (DbCommand command = factory.CreateCommand("SELECT ScaleConfigId FROM " + TableName + " "
                                                           + "WHERE Name = @Name")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Name", name));
                return (long)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Delete config
        /// </summary>
        /// <param name="weighingId">Config Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long scaleConfigId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                return command.ExecuteNonQuery();
            }
        }

        private char FirstChar(string text) {
            // Pozor, u rucniho vazeni je pouzity prazdny string, tj. je treba to osetrit
            if (text == "") {
                return ' ';
            }
            return text[0];
        }

        /// <summary>
        /// Load scale config from database
        /// </summary>
        /// <param name="scaleConfigId">Scale config Id</param>
        /// <returns>ScaleConfig instance</returns>
        public ScaleConfig Load(long scaleConfigId) {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE ScaleConfigId = @ScaleConfigId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@ScaleConfigId", scaleConfigId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        throw new Exception(String.Format("Scale config Id {0} don't exist", scaleConfigId));
                    }
                    ScaleConfig scaleConfig = new ScaleConfig();
                    scaleConfig.Version.Major               = (short)              reader["MajorVersion"];
                    scaleConfig.Version.Minor               = (short)              reader["MinorVersion"];
                    scaleConfig.Version.Build               = (short)              reader["BuildVersion"];
                    scaleConfig.Version.Hw                  = (byte)               reader["HwVersion"];
                    scaleConfig.ScaleName                   = (string)             reader["ScaleName"];
                    scaleConfig.Country                     = (Country)(byte)      reader["Country"];
                    scaleConfig.LanguageInDatabase          = (ScaleLanguagesInDatabase)(byte)reader["Language"];
                    scaleConfig.DateFormat                  = (DateFormat)(byte)   reader["DateFormat"];
                    scaleConfig.DateSeparator1              = FirstChar((string)   reader["DateSeparator1"]);
                    scaleConfig.DateSeparator2              = FirstChar((string)   reader["DateSeparator2"]);
                    scaleConfig.TimeFormat                  = (TimeFormat)(byte)   reader["TimeFormat"];
                    scaleConfig.TimeSeparator               = FirstChar((string)   reader["TimeSeparator"]);
                    scaleConfig.DaylightSavingMode          = (DaylightSavingMode)(byte)reader["DaylightSavingMode"];
                    scaleConfig.Units.Units                 = (Units)(byte)        reader["Units"];
                    scaleConfig.Units.Division              = (double)             reader["UnitsDivision"];
                    scaleConfig.Sounds.ToneDefault          = (Tone)(byte)         reader["SoundsToneDefault"];
                    scaleConfig.Sounds.ToneLight            = (Tone)(byte)         reader["SoundsToneLight"];
                    scaleConfig.Sounds.ToneOk               = (Tone)(byte)         reader["SoundsToneOk"];
                    scaleConfig.Sounds.ToneHeavy            = (Tone)(byte)         reader["SoundsToneHeavy"];
                    scaleConfig.Sounds.ToneKeyboard         = (Tone)(byte)         reader["SoundsToneKeyboard"];
                    scaleConfig.Sounds.EnableSpecial        = (bool)((byte)        reader["SoundsEnableSpecial"] == 1);
                    scaleConfig.Sounds.VolumeKeyboard       = (byte)               reader["SoundsVolumeKeyboard"];
                    scaleConfig.Sounds.VolumeSaving         = (byte)               reader["SoundsVolumeSaving"];
                    scaleConfig.Display.Mode                = (DisplayMode)(byte)  reader["DisplayMode"];
                    scaleConfig.Display.Backlight.Mode      = (BacklightMode)(byte)reader["BacklightMode"];
                    scaleConfig.Display.Backlight.Intensity = (byte)               reader["BacklightIntensity"];
                    scaleConfig.Display.Backlight.Duration  = (byte)               reader["BacklightDuration"];
                    scaleConfig.Printer.PaperWidth          = (byte)               reader["PrinterPaperWidth"];
                    scaleConfig.Printer.CommunicationFormat = (ComFormat)(byte)    reader["PrinterCommFormat"];
                    scaleConfig.Printer.CommunicationSpeed  = (int)                reader["PrinterCommSpeed"];
                    scaleConfig.PowerOffTimeout             = (short)              reader["PowerOffTimeout"];
                    scaleConfig.WeighingConfig.Saving.EnableMoreBirds = (bool)((byte)        reader["EnableMoreBirds"] == 1);
                    scaleConfig.EnableFileParameters        = (bool)((byte)        reader["EnableFileParameters"] == 1);
                    scaleConfig.PasswordConfig.Enable       = (bool)((byte)        reader["EnablePassword"] == 1);
                    scaleConfig.PasswordConfig.Password     = DecodePassword((int) reader["Password"]);
                    scaleConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)(byte)reader["WeightSortingMode"];
                    scaleConfig.WeighingConfig.Saving.Mode               = (SavingMode)(byte)   reader["SavingMode"];
                    scaleConfig.WeighingConfig.Saving.Filter             = (double)             reader["Filter"];
                    scaleConfig.WeighingConfig.Saving.StabilisationTime  = (double)             reader["StabilisationTime"];
                    scaleConfig.WeighingConfig.Saving.MinimumWeight      = (double)             reader["MinimumWeight"];
                    scaleConfig.WeighingConfig.Saving.StabilisationRange = (double)             reader["StabilisationRange"];
                    scaleConfig.StatisticConfig.UniformityRange          = (byte)               reader["UniformityRange"];
                    scaleConfig.StatisticConfig.Histogram.Mode           = (HistogramMode)(byte)reader["HistogramMode"];
                    scaleConfig.StatisticConfig.Histogram.Range          = (byte)               reader["HistogramRange"];
                    scaleConfig.StatisticConfig.Histogram.Step           = (double)             reader["HistogramStep"];
                    scaleConfig.ActiveFileIndex             = (short)              reader["ActiveFileIndex"];
                    scaleConfig.Units.WeighingCapacity      = (WeighingCapacity)(byte)reader["WeighingCapacity"];

                    return scaleConfig;
                }
            }
        }

        /// <summary>
        /// Load predefined config names
        /// </summary>
        /// <returns>List of config names</returns>
        public List<string> LoadNameList() {
            using (DbCommand command = factory.CreateCommand("SELECT Name FROM " + tableName + " WHERE Name IS NOT NULL")) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    List<string> nameList = new List<string>();
                    while (reader.Read()) {
                        nameList.Add((string)reader["Name"]);
                    }
                    return nameList;
                }
            }
        }

        /// <summary>
        /// Load sorted list of all scale names from the database
        /// </summary>
        /// <returns>Sorted list of scale names</returns>
        public List<string> LoadScaleNameList() {
            using (DbCommand command = factory.CreateCommand("SELECT ScaleName FROM " + tableName + " "
                                                           + "GROUP BY ScaleName")) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    List<string> list = new List<string>();
                    while (reader.Read()) {
                        list.Add((string)reader["ScaleName"]);
                    }
                    list.Sort();    // Jmena souboru jsou v ASCII, tj. mohl bych tridit rovnou v SQLite. Trideni Listu bude asi rychlejsi.
                    return list;
                }
            }
        }

    }
}