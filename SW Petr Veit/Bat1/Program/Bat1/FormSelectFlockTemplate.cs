﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormSelectFlockTemplate : Form {
        public FlockDefinition SelectedFlockDefinition;

        public FormSelectFlockTemplate(FlockDefinitionList flockDefinitionList) {
            InitializeComponent();

            userControlFlockList.SetData(flockDefinitionList);

            // Zaregistruju eventy
            userControlFlockList.DoubleClickEvent += DoubleClickEventHandler;
        }

        private bool SaveSelected(FlockDefinition flockDefinition) {
            if (flockDefinition == null) {
                return false;     // Nic nevybral
            }

            SelectedFlockDefinition = flockDefinition;

            return true;
        }
        
        private void buttonOk_Click(object sender, EventArgs e) {
            if (!SaveSelected(userControlFlockList.GetSelectedFlock())) {
                return;
            }
            DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// Double-click event handler
        /// </summary>
        /// <param name="flockDefinition">Flock to edit</param>
        private void DoubleClickEventHandler(FlockDefinition flockDefinition) {
            if (!SaveSelected(flockDefinition)) {
                return;
            }
            DialogResult = DialogResult.OK;
        }

        private void FormSelectFlockTemplate_Shown(object sender, EventArgs e) {
            userControlFlockList.Focus();
        }

    }
}
