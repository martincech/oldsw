﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormImportWeighings : Form {
        private List<Weighing> weighingList;

        /// <summary>
        /// List of selected weighings
        /// </summary>
        public List<Weighing> SelectedWeighingList;
        
        public FormImportWeighings(List<Weighing> weighingList) {
            InitializeComponent();

            // Preberu data
            this.weighingList = weighingList;

            // Vykreslim seznam vazeni
            ShowWeighings();
        }

        private void ShowWeighings() {
            // Zobrazim seznam vazeni
            dataGridViewWeighings.Rows.Clear();

            foreach (Weighing weighing in weighingList) {
                string dateTimeString;
                if (weighing.WeighingData.ResultType == ResultType.MANUAL) {
                    dateTimeString = weighing.WeighingResults.GetResult(Flag.ALL).WeighingStarted.ToString("G");    // Vcetne sekund
                } else {
                    dateTimeString = weighing.WeighingData.SampleList.MinDateTime.ToString("G");    // Vcetne sekund;
                }
                dataGridViewWeighings.Rows.Add(dateTimeString, weighing.WeighingData.File.Name, weighing.WeighingData.ScaleConfig.ScaleName);
            }
        }

        private void Edit() {
            if (dataGridViewWeighings.SelectedRows.Count == 0) {
                return;     // Zadne vazeni neni vybrane
            }
            
            // Nactu z databaze vybrane vazeni (pokud je jich vybrano vice, vezmu prvni v poradi)
            Weighing weighing = weighingList[dataGridViewWeighings.SelectedRows[0].Index];

            if (weighing.WeighingData.ResultType == ResultType.MANUAL) {
                return;         // Rucni vazeni nelze upravovat ani zobazit detaily
            }

            // Predani 12tis vzorku trva, dam presypaci hodiny
            FormEditWeighing form;
            Cursor.Current = Cursors.WaitCursor;
            try {
                // Nahraju z databaze seznam souboru
                NameNoteUniqueList fileList = new NameNoteUniqueList();
                foreach (NameNote file in Program.Database.LoadFileNameNoteList()) {
                    fileList.Add(file);
                }

                // Predam data oknu, muze editovat
                form = new FormEditWeighing(weighing.WeighingData, fileList, weighing.WeighingData.ScaleConfig, false, true);
            } finally {
                Cursor.Current = Cursors.Default;
            }
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Prekreslim seznam vazeni
            ShowWeighings();
        }

        private void FormImportWeighings_Shown(object sender, EventArgs e) {
            // Nastavim focus na seznam vazeni
            dataGridViewWeighings.Focus();
        }

        private void buttonSaveSelected_Click(object sender, EventArgs e) {
            // Vytvorim seznam vybranych vazeni
            SelectedWeighingList = new List<Weighing>();
            foreach (DataGridViewRow row in dataGridViewWeighings.SelectedRows) {
                SelectedWeighingList.Add(weighingList[row.Index]);
            }

            DialogResult = DialogResult.OK;
        }

        private void buttonEdit_Click(object sender, EventArgs e) {
            Edit();
        }

        private void dataGridViewWeighings_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
            if (e.RowIndex < 0) {
                return;     // Zahlavi
            }
            Edit();
        }

        private void buttonSaveAll_Click(object sender, EventArgs e) {
            // Vytvorim seznam vybranych vazeni
            SelectedWeighingList = new List<Weighing>(weighingList);

            DialogResult = DialogResult.OK;
        }

    }
}
