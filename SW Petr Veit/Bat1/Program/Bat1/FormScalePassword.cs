﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormScalePassword : Form {
        /// <summary>
        /// Array of displayed keys
        /// </summary>
        private PictureBox [] pictureBoxKeyArray;

        /// <summary>
        /// List of key images
        /// </summary>
        private ImageList imageListKey;

        /// <summary>
        /// Index of currently edited key
        /// </summary>
        private int currentEditIndex;

        /// <summary>
        /// The password is being currently edited
        /// </summary>
        private bool isEditing;

        /// <summary>
        /// Password
        /// </summary>
        public byte [] Password { get { return password; } }
        private byte [] password;

        private void RedrawPassword() {
            for (int keyIndex = 0; keyIndex < password.Length; keyIndex++) {
                pictureBoxKeyArray[keyIndex].Image = imageListKey.Images[password[keyIndex]];
            }
        }

        public void ClearPassword() {
            password = new byte[4] {(byte)Veit.Bat1.Keys.K_NULL, (byte)Veit.Bat1.Keys.K_NULL, (byte)Veit.Bat1.Keys.K_NULL, (byte)Veit.Bat1.Keys.K_NULL};
            isEditing = true;
            currentEditIndex = 0;       // Zacinam od prvni klavesy
            buttonOk.Enabled = false;
        }
        
        public FormScalePassword(ImageList imageListKey) {
            InitializeComponent();

            // Vytvorim pole obrazku s tlacitky
            pictureBoxKeyArray = new PictureBox[4] {pictureBoxKey1, pictureBoxKey2, pictureBoxKey3, pictureBoxKey4};

            // Preberu obrazky
            this.imageListKey = imageListKey;

            // Nastavim heslo na prazdne
            ClearPassword();

            // Prekreslim
            RedrawPassword();
        }

        private void buttonEsc_Click(object sender, EventArgs e) {
            if (!isEditing) {
                return;     // Heslo se needituje
            }

            // Nactu kod klavesy
            Veit.Bat1.Keys key = Veit.Bat1.Keys.K_NULL;
            if ((Button)sender == buttonEnter) {
                key = Veit.Bat1.Keys.K_ENTER;
            } else if ((Button)sender == buttonLeft) {
                key = Veit.Bat1.Keys.K_LEFT;
            } else if ((Button)sender == buttonEsc) {
                key = Veit.Bat1.Keys.K_ESC;
            } else if ((Button)sender == buttonUp) {
                key = Veit.Bat1.Keys.K_UP;
            } else if ((Button)sender == buttonRight) {
                key = Veit.Bat1.Keys.K_RIGHT;
            } else if ((Button)sender == buttonDown) {
                key = Veit.Bat1.Keys.K_DOWN;
            }

            // Pridam klavesu do hesla
            password[currentEditIndex] = (byte)key;
            currentEditIndex++;

            // Prekreslim
            RedrawPassword();

            if (currentEditIndex >= Veit.Bat1.Const.PASSWORD_LENGTH) {
                isEditing        = false;   // Zadal posledni klavesu, konec editace
                buttonOk.Enabled = true;    // Muze ulozit
            }
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            // Nastavim heslo na prazdne
            ClearPassword();

            // Prekreslim
            RedrawPassword();
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            if (isEditing) {
                return;     // Editace musi byt dokoncena
            }

            DialogResult = DialogResult.OK;
        }
    }
}
