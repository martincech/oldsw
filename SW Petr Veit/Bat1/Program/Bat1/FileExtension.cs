﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// File extensions used in the application
    /// </summary>
    public static class FileExtension {
        /// <summary>
        /// Database file
        /// </summary>
        public static readonly string DATABASE = AppConfig.DatabaseFileExtension;

        /// <summary>
        /// Backup file
        /// </summary>
        public static readonly string BACKUP = AppConfig.BackupFileExtension;

        /// <summary>
        /// Scale hardcopy
        /// </summary>
        public static readonly string HARDCOPY = AppConfig.HardcopyFileExtension;

        /// <summary>
        /// Bat1 export file
        /// </summary>
        public static readonly string EXPORT_BAT1 = AppConfig.ExportFileExtension;

        /// <summary>
        /// CSV export file
        /// </summary>
        public const string EXPORT_CSV = "csv";

    }
}
