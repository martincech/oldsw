﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Veit.Scale;

namespace Bat1 {
    public partial class FormUnits : Form {
        /// <summary>
        /// Selected units
        /// </summary>
        public Units SelectedUnits { get { return (Units)comboBoxUnits.SelectedIndex; } }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="units">Units selected by default</param>
        public FormUnits(Units units) {
            InitializeComponent();

            comboBoxUnits.SelectedIndex = (int)units;
            comboBoxUnits.Focus();
        }

    }
}
