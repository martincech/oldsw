﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// One file
    /// </summary>
    public class File : NameNote {

        /// <summary>
        /// File-specific setup
        /// </summary>
        public readonly FileConfig FileConfig;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name"></param>
        /// <param name="note"></param>
        /// <param name="fileSetup"></param>
        public File(string name, string note, FileConfig fileConfig) : base(name, note) {
            FileConfig = fileConfig;
        }
    }
}
