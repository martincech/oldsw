﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat1 {
    
    /// <summary>
    /// Flocks table
    /// </summary>
    public class DatabaseTableFlocks : DatabaseTable {

        // Tabulka s definicemi hejn.

        /// <summary>
        /// Maximum length of flock name
        /// </summary>
        public static int NAME_MAX_LENGTH = 30;     // Muze tam zadavat az 2 datumy, tj. radeji delsi

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableFlocks(DatabaseFactory factory)
            : base("Flocks", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "FlockId "         + factory.TypeLong + " PRIMARY KEY AUTOINCREMENT,"
                 + "Name "            + factory.TypeNVarChar  + "(" + NAME_MAX_LENGTH.ToString() + ") NOT NULL,"
                 + "Note "            + factory.TypeNVarChar  + "(100),"        // Poznamka krivky v PC muze byt dlouha
                 + "StartedDay "      + factory.TypeSmallInt  + ","             // Cislo dne startu vazeni odpovidajici prvnimu dni vazeni
                 + "CurveDefaultId "  + factory.TypeLong      + ","             // ID teoreticke rustove krivky samcu nebo default
                 + "CurveFemalesId "  + factory.TypeLong      + ","             // ID teoreticke rustove krivky samic
                 + "CONSTRAINT FK_P_CurvesDefault FOREIGN KEY (CurveDefaultId) REFERENCES Curves(CurveId)"
                 + "CONSTRAINT FK_P_CurvesFemales FOREIGN KEY (CurveFemalesId) REFERENCES Curves(CurveId)"
                 + ")");
        }

        private void AddCurve(DbCommand command, Curve curve, string parameterName) {
            if (curve == null) {
                command.Parameters.Add(factory.CreateParameter(parameterName, null));
            } else {
                command.Parameters.Add(factory.CreateParameter(parameterName, (long)curve.Id));
            } 
        }
        
        /// <summary>
        /// Add new flock
        /// </summary>
        /// <param name="flockDefinition">Flock</param>
        /// <returns>Id of new flock</returns>
        public long Add(FlockDefinition flockDefinition) {
            // Jmeno musi byt zadane
            if (flockDefinition.Name == "") {
                throw new Exception("Flock name cannot be empty");
            }

            // Hejno nesmi mit zadany ID. Pokud ma ID vyplneny, hejno uz bylo do DB ulozene a musim pouzit Update.
            if (flockDefinition.Id > 0) {
                throw new Exception("Flock ID > 0");
            }
            
            // Ulozim nove hejno
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (NULL, @Name, "
                                                           + "@Note, @StartedDay, @CurveDefaultId, @CurveFemalesId);"
                                                           + "SELECT last_insert_rowid()")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Name",       flockDefinition.Name));
                command.Parameters.Add(factory.CreateParameter("@Note",       flockDefinition.Note));
                command.Parameters.Add(factory.CreateParameter("@StartedDay", (short)flockDefinition.StartedDay));
                AddCurve(command, flockDefinition.CurveDefault, "@CurveDefaultId");
                AddCurve(command, flockDefinition.CurveFemales, "@CurveFemalesId");
                return (long)command.ExecuteScalar();
            }
        }

        /// <summary>
        /// Update flock data
        /// </summary>
        /// <param name="flockDefinition">Flock</param>
        /// <returns>Number of rows affected</returns>
        public int Update(FlockDefinition flockDefinition) {
            // Jmeno musi byt zadane
            if (flockDefinition.Name == "") {
                throw new Exception("Flock name cannot be empty");
            }
            
            // Hejno uz musi mit zadany ID, tj. uz je v databazi ulozene
            if (flockDefinition.Id <= 0) {
                throw new Exception("Flock ID <= 0");
            }

            // Updatuju hejno
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + "Name = @Name, Note = @Note, "
                                                           + "StartedDay = @StartedDay, "
                                                           + "CurveDefaultId = @CurveDefaultId, "
                                                           + "CurveFemalesId = @CurveFemalesId "
                                                           + "WHERE FlockId = @FlockId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FlockId",    (long)flockDefinition.Id));
                command.Parameters.Add(factory.CreateParameter("@Name",       flockDefinition.Name));
                command.Parameters.Add(factory.CreateParameter("@Note",       flockDefinition.Note));
                command.Parameters.Add(factory.CreateParameter("@StartedDay", (short)flockDefinition.StartedDay));
                AddCurve(command, flockDefinition.CurveDefault, "@CurveDefaultId");
                AddCurve(command, flockDefinition.CurveFemales, "@CurveFemalesId");
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Delete flock
        /// </summary>
        /// <param name="flockId">Flock Id</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long flockId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE FlockId = @FlockId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FlockId", flockId));
                return command.ExecuteNonQuery();
            }
        }

        private Curve ReaderToCurveId(DbDataReader reader, string columnName) {
            if (reader[columnName] == DBNull.Value) {
                return null;        // Krivka neni definovana
            }
            Curve curve = new Curve();
            curve.Id = (long)reader[columnName];  
            return curve;
        }
        
        private FlockDefinition ReaderToFlock(DbDataReader reader) {
            FlockDefinition flockDefinition = new FlockDefinition();
            flockDefinition.Id           = (long)   reader["FlockId"];
            flockDefinition.Name         = (string) reader["Name"];
            flockDefinition.Note         = (string) reader["Note"];
            flockDefinition.StartedDay   = (short)  reader["StartedDay"];
            flockDefinition.CurveDefault = ReaderToCurveId(reader, "CurveDefaultId");
            flockDefinition.CurveFemales = ReaderToCurveId(reader, "CurveFemalesId");
            return flockDefinition;
        }
        
        /// <summary>
        /// Load flock from database
        /// </summary>
        /// <param name="flockId">Flock Id</param>
        /// <returns>Flock instance</returns>
        public FlockDefinition Load(long flockId) {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName + " WHERE FlockId = @FlockId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@FlockId", flockId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    if (!reader.Read()) {
                        throw new Exception(String.Format("Flock Id {0} doesn't exist", flockId));
                    }
                    return ReaderToFlock(reader);
                }
            }
        }

        /// <summary>
        /// Load flock list from database
        /// </summary>
        /// <returns>FlockDefinitionList instance</returns>
        public FlockDefinitionList LoadList() {
            using (DbCommand command = factory.CreateCommand("SELECT * FROM " + tableName)) {
                using (DbDataReader reader = command.ExecuteReader()) {
                    FlockDefinitionList flockDefinitionList = new FlockDefinitionList();
                    while (reader.Read()) {
                        flockDefinitionList.Add(ReaderToFlock(reader));
                    }

                    return flockDefinitionList;
                }
            }
        }

        private int DeleteCurve(string columnName, long curveId) {
            using (DbCommand command = factory.CreateCommand("UPDATE " + tableName + " SET "
                                                           + columnName + " = NULL "
                                                           + "WHERE " + columnName + " = @CurveId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@CurveId", curveId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Delete specified growth curve from all flocks
        /// </summary>
        /// <param name="curveId">Curve Id</param>
        /// <returns>Number of rows affected</returns>
        public int DeleteCurve(long curveId) {
            return (DeleteCurve("CurveDefaultId", curveId)
                  + DeleteCurve("CurveFemalesId", curveId));    // Sectu pocet radku a vratim
        }

        /// <summary>
        /// Load number of flocks where specified growth curve is used
        /// </summary>
        /// <param name="curveId">Curve ID</param>
        /// <returns>Number of rows</returns>
        public int FlocksWithCurve(long curveId) {
            using (DbCommand command = factory.CreateCommand("SELECT COUNT(FlockId) FROM " + TableName + " "
                                                           + "WHERE (CurveDefaultId = @CurveId)"
                                                           + "OR (CurveFemalesId = @CurveId)")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@CurveId", curveId));
                try {
                    return (int)(long)command.ExecuteScalar();
                } catch {
                    // Tabulka je prazdna
                    return 0;
                }
            }
        }

    }
}