﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Globalization;

namespace Bat1 {
    /// <summary>
    /// Codes for supported languages
    /// </summary>
    public enum SwLanguage {
        UNDEFINED,      // Poradi se nesmi menit
        ENGLISH,        
        CZECH,
        PORTUGUESE,
        SPANISH,
        GERMAN,
        FRENCH,
        FINNISH,
        RUSSIAN,
        TURKISH,
        JAPANESE,
        POLISH,
        ITALIAN,
        _COUNT
    }

    public static class SwLanguageClass {
        // Jmena jazyku
        private const string STR_ENGLISH    = "English";
        private const string STR_CZECH      = "Česky";
        private const string STR_PORTUGUESE = "Português";
        private const string STR_SPANISH    = "Español";
        private const string STR_GERMAN     = "Deutsch";
        private const string STR_FRENCH     = "Français";
        private const string STR_FINNISH    = "Suomi";
        private const string STR_RUSSIAN    = "По-русски";
        private const string STR_TURKISH    = "Turkce";
        private const string STR_JAPANESE   = "ニホンゴ";
        private const string STR_POLISH     = "Polski";
        private const string STR_ITALIAN    = "Italiano";

        public static List<string> GetLanguageStrings() {
            List<string> list = new List<string>();
            for (SwLanguage language = SwLanguage.ENGLISH; language < SwLanguage._COUNT; language++) {
                list.Add(GetLanguageString(language));
            }
            list.Sort();        // Setridim podle abecedy
            return list;
        }

        public static string GetLanguageString(SwLanguage language) {
            switch (language) {
                case SwLanguage.ENGLISH:
                    return STR_ENGLISH;
                case SwLanguage.CZECH:
                    return STR_CZECH;
                case SwLanguage.PORTUGUESE:
                    return STR_PORTUGUESE;
                case SwLanguage.SPANISH:
                    return STR_SPANISH;
                case SwLanguage.GERMAN:
                    return STR_GERMAN;
                case SwLanguage.FRENCH:
                    return STR_FRENCH;
                case SwLanguage.FINNISH:
                    return STR_FINNISH;
                case SwLanguage.RUSSIAN:
                    return STR_RUSSIAN;
                case SwLanguage.TURKISH:
                    return STR_TURKISH;
                case SwLanguage.JAPANESE:
                    return STR_JAPANESE;
                case SwLanguage.POLISH:
                    return STR_POLISH;
                case SwLanguage.ITALIAN:
                    return STR_ITALIAN;
                default:
                    return "";
            }
        }

        public static SwLanguage GetLanguageCode(string languageStr) {
            if (languageStr == STR_ENGLISH) {
                return SwLanguage.ENGLISH;
            } else if (languageStr == STR_CZECH) {
                return SwLanguage.CZECH;
            } else if (languageStr == STR_PORTUGUESE) {
                return SwLanguage.PORTUGUESE;
            } else if (languageStr == STR_SPANISH) {
                return SwLanguage.SPANISH;
            } else if (languageStr == STR_GERMAN) {
                return SwLanguage.GERMAN;
            } else if (languageStr == STR_FRENCH) {
                return SwLanguage.FRENCH;
            } else if (languageStr == STR_FINNISH) {
                return SwLanguage.FINNISH;
            } else if (languageStr == STR_RUSSIAN) {
                return SwLanguage.RUSSIAN;
            } else if (languageStr == STR_TURKISH) {
                return SwLanguage.TURKISH;
            } else if (languageStr == STR_JAPANESE) {
                return SwLanguage.JAPANESE;
            } else if (languageStr == STR_POLISH) {
                return SwLanguage.POLISH;
            } else if (languageStr == STR_ITALIAN) {
                return SwLanguage.ITALIAN;
            } else {
                return SwLanguage.UNDEFINED;
            }
        }
        
        public static void SetLanguage(SwLanguage language) {
            string str;

            // Prevedu kod jazyka na string
            switch (language) {
                case SwLanguage.CZECH:
                    str = "cs";
                    break;
                case SwLanguage.PORTUGUESE:
                    str = "pt";
                    break;
                case SwLanguage.SPANISH:
                    str = "es";
                    break;
                case SwLanguage.GERMAN:
                    str = "de";
                    break;
                case SwLanguage.FRENCH:
                    str = "fr";
                    break;
                case SwLanguage.FINNISH:
                    str = "fi";
                    break;
                case SwLanguage.RUSSIAN:
                    str = "ru";
                    break;
                case SwLanguage.TURKISH:
                    str = "tr";
                    break;
                case SwLanguage.JAPANESE:
                    str = "ja";
                    break;
                case SwLanguage.POLISH:
                    str = "pl";
                    break;
                case SwLanguage.ITALIAN:
                    str = "it";
                    break;
                default:
                    str = "en";
                    break;
            }

            // Nastavim jazyk pro vsechna okna a resources
            CultureInfo cultureInfo = new System.Globalization.CultureInfo(str);
            Properties.Resources.Culture          = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
        }
    }
}
