﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// List of growth curves
    /// </summary>
    public class CurveList {
        /// <summary>
        /// List of defined curves sorted by name
        /// </summary>
        public List<Curve> List { get { return list; } }
        private List<Curve> list = new List<Curve>();

        /// <summary>
        /// Constructor
        /// </summary>
        public CurveList() {
        }

        /// <summary>
        /// Check if curve index is valid within the list
        /// </summary>
        /// <param name="index">Curve index</param>
        /// <returns>True if valid</returns>
        public bool CheckIndex(int index) {
          return (bool)(index >= 0 && index < list.Count);
        }

        /// <summary>
        /// Check if curve exists
        /// </summary>
        /// <param name="name">Curve name</param>
        /// <returns>True if exists</returns>
        public bool Exists(string name) {
            return list.Exists(delegate(Curve curve) { return curve.Name == name; });
        }
        
        /// <summary>
        /// Sort the list by name
        /// </summary>
        private void Sort() {
            list.Sort(Curve.CompareByName);
        }
        
        /// <summary>
        /// Check if curve name is valid
        /// </summary>
        /// <param name="name">Curve name</param>
        /// <returns>True if valid</returns>
        public bool CheckName(string name) {
            // Kontrola delky nazvu
            if (name.Length == 0) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Add curve to the list
        /// </summary>
        /// <param name="file">Curve to add</param>
        /// <returns>True if successful</returns>
        public bool Add(Curve curve) {
            // Kontrola jmena
            if (!CheckName(curve.Name)) {
                return false;       // Jmeno ma neplatny format
            }
            
            // Zkontroluju, zda uz krivka se zadanym jmenem v seznamu neexistuje
            if (Exists(curve.Name)) {
                return false;
            }

            // Pridam do seznamu a setridim podle jmena
            list.Add(curve);
            Sort();

            return true;
        }
        
        /// <summary>
        /// Delete curve from the list
        /// </summary>
        /// <param name="index">Curve index</param>
        public void Delete(int index) {
            if (!CheckIndex(index)) {
                return;
            }
            list.RemoveAt(index);
        }

        /// <summary>
        /// Rename curve
        /// </summary>
        /// <param name="index">Curve index</param>
        /// <param name="newName">New name</param>
        /// <returns>True if successful</returns>
        public bool Rename(int index, string newName) {
            if (!CheckIndex(index)) {
                return false;
            }

            // Zkontroluju platnost jmena
            if (newName == "") {
                return false;
            }
            
            // Zkontroluju, zda vubec je treba zmena
            if (list[index].Name == newName) {
                return true;        // Neni treba prejmenovavat, vratim true jako bych prejmenoval
            }
            
            // Zkontroluju, zda uz jmeno v seznamu neni na jine pozici
            if (Exists(newName)) {
                return false;       // Soubor se zadanym jmenem uz existuje
            }
            
            // Zmenim jmeno a znovu setridim
            list[index].Name = newName;
            Sort();

            return true;
        }

        /// <summary>
        /// Find curve index
        /// </summary>
        /// <param name="name">Curve name</param>
        /// <returns>Index in the list</returns>
        public int GetIndex(string name) {
            for (int i = 0; i < list.Count; i++) {
                if (list[i].Name == name) {
                    return i;
                }
            }

            // Nenasel jsem
            return -1;
        }

    }
}