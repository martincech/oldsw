﻿namespace Bat1 {
    partial class UserControlScaleConfigStatistics {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigStatistics));
            this.labelFilterSec = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBoxHistogram = new System.Windows.Forms.GroupBox();
            this.numericUpDownHistogramStep = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownHistogramRange = new System.Windows.Forms.NumericUpDown();
            this.labelHistogramRangePercent = new System.Windows.Forms.Label();
            this.labelHistogramRange = new System.Windows.Forms.Label();
            this.labelHistogramStepUnits = new System.Windows.Forms.Label();
            this.comboBoxHistogramMode = new System.Windows.Forms.ComboBox();
            this.labelHistogramStep = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.numericUpDownUniformityRange = new System.Windows.Forms.NumericUpDown();
            this.groupBoxHistogram.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHistogramStep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHistogramRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUniformityRange)).BeginInit();
            this.SuspendLayout();
            // 
            // labelFilterSec
            // 
            resources.ApplyResources(this.labelFilterSec, "labelFilterSec");
            this.labelFilterSec.Name = "labelFilterSec";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // groupBoxHistogram
            // 
            resources.ApplyResources(this.groupBoxHistogram, "groupBoxHistogram");
            this.groupBoxHistogram.Controls.Add(this.numericUpDownHistogramStep);
            this.groupBoxHistogram.Controls.Add(this.numericUpDownHistogramRange);
            this.groupBoxHistogram.Controls.Add(this.labelHistogramRangePercent);
            this.groupBoxHistogram.Controls.Add(this.labelHistogramRange);
            this.groupBoxHistogram.Controls.Add(this.labelHistogramStepUnits);
            this.groupBoxHistogram.Controls.Add(this.comboBoxHistogramMode);
            this.groupBoxHistogram.Controls.Add(this.labelHistogramStep);
            this.groupBoxHistogram.Controls.Add(this.label10);
            this.groupBoxHistogram.Name = "groupBoxHistogram";
            this.groupBoxHistogram.TabStop = false;
            // 
            // numericUpDownHistogramStep
            // 
            resources.ApplyResources(this.numericUpDownHistogramStep, "numericUpDownHistogramStep");
            this.numericUpDownHistogramStep.Name = "numericUpDownHistogramStep";
            this.numericUpDownHistogramStep.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownHistogramRange
            // 
            resources.ApplyResources(this.numericUpDownHistogramRange, "numericUpDownHistogramRange");
            this.numericUpDownHistogramRange.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownHistogramRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHistogramRange.Name = "numericUpDownHistogramRange";
            this.numericUpDownHistogramRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownHistogramRange.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownHistogramRange.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // labelHistogramRangePercent
            // 
            resources.ApplyResources(this.labelHistogramRangePercent, "labelHistogramRangePercent");
            this.labelHistogramRangePercent.Name = "labelHistogramRangePercent";
            // 
            // labelHistogramRange
            // 
            resources.ApplyResources(this.labelHistogramRange, "labelHistogramRange");
            this.labelHistogramRange.Name = "labelHistogramRange";
            // 
            // labelHistogramStepUnits
            // 
            resources.ApplyResources(this.labelHistogramStepUnits, "labelHistogramStepUnits");
            this.labelHistogramStepUnits.Name = "labelHistogramStepUnits";
            // 
            // comboBoxHistogramMode
            // 
            resources.ApplyResources(this.comboBoxHistogramMode, "comboBoxHistogramMode");
            this.comboBoxHistogramMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHistogramMode.FormattingEnabled = true;
            this.comboBoxHistogramMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxHistogramMode.Items"),
            resources.GetString("comboBoxHistogramMode.Items1")});
            this.comboBoxHistogramMode.Name = "comboBoxHistogramMode";
            this.comboBoxHistogramMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelHistogramStep
            // 
            resources.ApplyResources(this.labelHistogramStep, "labelHistogramStep");
            this.labelHistogramStep.Name = "labelHistogramStep";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // numericUpDownUniformityRange
            // 
            resources.ApplyResources(this.numericUpDownUniformityRange, "numericUpDownUniformityRange");
            this.numericUpDownUniformityRange.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownUniformityRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownUniformityRange.Name = "numericUpDownUniformityRange";
            this.numericUpDownUniformityRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownUniformityRange.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownUniformityRange.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // UserControlScaleConfigStatistics
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.numericUpDownUniformityRange);
            this.Controls.Add(this.groupBoxHistogram);
            this.Controls.Add(this.labelFilterSec);
            this.Controls.Add(this.label8);
            this.Name = "UserControlScaleConfigStatistics";
            this.groupBoxHistogram.ResumeLayout(false);
            this.groupBoxHistogram.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHistogramStep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHistogramRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownUniformityRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFilterSec;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBoxHistogram;
        private System.Windows.Forms.Label labelHistogramRangePercent;
        private System.Windows.Forms.Label labelHistogramRange;
        private System.Windows.Forms.Label labelHistogramStepUnits;
        private System.Windows.Forms.ComboBox comboBoxHistogramMode;
        private System.Windows.Forms.Label labelHistogramStep;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericUpDownUniformityRange;
        private System.Windows.Forms.NumericUpDown numericUpDownHistogramRange;
        private System.Windows.Forms.NumericUpDown numericUpDownHistogramStep;
    }
}
