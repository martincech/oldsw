﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// NameNote list with unique names
    /// </summary>
    public class NameNoteUniqueList : List<NameNote> {
        /// <summary>
        /// Add a new item to the list. If an item with specified name already exists, only note is updated in the existing item.
        /// </summary>
        /// <param name="item">New item</param>
        public new void Add(NameNote newItem) {
            // Zkontroluju, zda zadal nove jmeno souboru
            foreach (NameNote item in this) {
                if (item.Name == newItem.Name) {
                    // Polozka s timto nazvem uz v seznamu je, updatuju u nej jen poznamku
                    item.SetNote(newItem.Note);
                    return;
                }
            }

            // Pridam soubor do seznamu
            base.Add(newItem);
            Sort(NameNote.CompareByName);
        }

        /// <summary>
        /// Return note for a specified name
        /// </summary>
        /// <param name="fileName">Name</param>
        /// <returns>Note</returns>
        public string GetNote(string name) {
            foreach (NameNote item in this) {
                if (item.Name == name) {
                    return item.Note;
                }
            }
            return "";
        }

    }
}
