﻿namespace Bat1 {
    partial class UserControlTimeGraph {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.graph = new ZedGraph.ZedGraphControl();
            this.SuspendLayout();
            // 
            // graph
            // 
            this.graph.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graph.EditButtons = System.Windows.Forms.MouseButtons.Left;
            this.graph.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.graph.IsShowContextMenu = false;
            this.graph.Location = new System.Drawing.Point(0, 0);
            this.graph.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.graph.Name = "graph";
            this.graph.PanButtons = System.Windows.Forms.MouseButtons.Right;
            this.graph.PanButtons2 = System.Windows.Forms.MouseButtons.Right;
            this.graph.PanModifierKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.None)));
            this.graph.ScrollGrace = 0;
            this.graph.ScrollMaxX = 0;
            this.graph.ScrollMaxY = 0;
            this.graph.ScrollMaxY2 = 0;
            this.graph.ScrollMinX = 0;
            this.graph.ScrollMinY = 0;
            this.graph.ScrollMinY2 = 0;
            this.graph.Size = new System.Drawing.Size(614, 377);
            this.graph.TabIndex = 2;
            this.graph.MouseDownEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.graph_MouseDownEvent);
            this.graph.MouseUpEvent += new ZedGraph.ZedGraphControl.ZedMouseEventHandler(this.graph_MouseUpEvent);
            this.graph.ZoomEvent += new ZedGraph.ZedGraphControl.ZoomEventHandler(this.graph_ZoomEvent);
            // 
            // UserControlTimeGraph
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.graph);
            this.Name = "UserControlTimeGraph";
            this.Size = new System.Drawing.Size(614, 377);
            this.Load += new System.EventHandler(this.UserControlTimeGraph_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl graph;
    }
}
