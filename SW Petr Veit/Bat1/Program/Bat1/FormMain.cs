﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Veit.ScaleStatistics;
using Veit.Scale;

namespace Bat1 {
    public partial class FormMain : Form {
        private UserControlScale          userControlScale;
        private UserControlWeighings      userControlWeighings;
        private UserControlStatisticsTabs userControlStatisticsTabs;
        private UserControlFlocks         userControlFlocks;
        private UserControlMaintenance    userControlMaintenance;
        
        /// <summary>
        /// List of created user controls
        /// </summary>
        private List<UserControl> listCreatedPages = new List<UserControl>();

        public FormMain() {
            InitializeComponent();

            // Nastavim ikonu
            Icon = Properties.Resources.Bat1Icon;

            // Nastavim nazev aplikace
            Text = Program.ApplicationName;

            // Velikost okna nastavim podle rozliseni monitoru
            int areaWidth  = SystemInformation.WorkingArea.Width;
            int areaHeight = SystemInformation.WorkingArea.Height;

            // Pokud je rozliseni monitoru mene nez 1024x768, zmensim okno
            if (areaWidth <= 1024 && areaHeight <= 768) {
                Width  = areaWidth - 10;
                Height = areaHeight - 50;
            }

            // Pri vyssim DPI se default rozmer okna 1024x768 zvetsi (120DPI = 1292x812), tj. muze byt vetsi
            // nez velikost obrazovky napr. 1280x800.
            if (Width > areaWidth || Height > areaHeight) {
                // Nastavim okno pres celou obrazovku
                Location = new Point(0, 0);
                Width    = areaWidth;
                Height   = areaHeight;
            }
        }

        private void ClearIconsCheck() {
            toolStripButtonScale.Checked    = false;
            toolStripButtonWeighings.Checked   = false;
            toolStripButtonStatistics.Checked  = false;
            toolStripButtonFlocks.Checked      = false;
            toolStripButtonMaintenance.Checked = false;
        }
        
        /// <summary>
        /// Add control to list
        /// </summary>
        /// <param name="control"></param>
        private void AddPage(UserControl control) {
            panelPages.Controls.Add(control);
            control.Dock = DockStyle.Fill;
            listCreatedPages.Add(control);
        }

        /// <summary>
        /// Hide all pages except of visibleControl
        /// </summary>
        /// <param name="visibleControl">Control that should stay visible or null to hide all pages</param>
        private void HideAllPagesExcept(UserControl visibleControl) {
            foreach (UserControl control in listCreatedPages) {
                if (control != visibleControl) {
                    control.Hide();
                }
            }
        }
        
        /// <summary>
        /// Hide all pages
        /// </summary>
        private void HideAllPages() {
            HideAllPagesExcept(null);
        }

        /// <summary>
        /// Show specified page
        /// </summary>
        /// <param name="visibleControl"></param>
        private void ShowPage(UserControl visibleControl) {
            // Schovam vsechny detaily krome tech, ktere mam zobrazit
            HideAllPagesExcept(visibleControl);

            if (visibleControl == null) {
                // Chce pouze schovat vsechny detaily, nic nezobrazuju
                return;
            }
            
            // Zobrazim zvolene detaily
            visibleControl.Show();
        }

        /// <summary>
        /// Show scale page
        /// </summary>
        private void ShowScale() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlScale == null) {
                userControlScale = new UserControlScale();
                AddPage(userControlScale);
            }

            // Zobrazim
            ShowPage(userControlScale);
        }
        
        /// <summary>
        /// Show weighings page
        /// </summary>
        private void ShowWeighings() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlWeighings == null) {
                userControlWeighings = new UserControlWeighings(false);
                AddPage(userControlWeighings);
            } else {
                // Control uz existuje, pouze znovu nahraju vazeni z databaze (mohl nacist nova vazeni z vahy)
                userControlWeighings.ReloadWeighingList();
            }

            // Zobrazim
            ShowPage(userControlWeighings);
        }

        /// <summary>
        /// Show statistics page
        /// </summary>
        private void ShowStatistics() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlStatisticsTabs == null) {
                userControlStatisticsTabs = new UserControlStatisticsTabs();
                AddPage(userControlStatisticsTabs);
            }

            // Zobrazim
            ShowPage(userControlStatisticsTabs);

            // Updatuju Last weighings
            // Pozor: pri volani teto fce uz musi byt userControlStatisticsTabs zobrazeny. Nastavuje se zde property
            // Visible u orniho panelu a pokud je cely control skryty, Visible zustane vzdy false a nenastavi se.
            userControlStatisticsTabs.ClearData();
        }

        /// <summary>
        /// Show flocks page
        /// </summary>
        private void ShowFlocks() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlFlocks == null) {
                userControlFlocks = new UserControlFlocks();
                AddPage(userControlFlocks);
            }

            userControlFlocks.ClearData();

            // Zobrazim
            ShowPage(userControlFlocks);
        }

        /// <summary>
        /// Show maintenance page
        /// </summary>
        private void ShowMaintenance() {
            // Pokud jeste control neexistuje, vytvorim
            if (userControlMaintenance == null) {
                userControlMaintenance = new UserControlMaintenance();
                AddPage(userControlMaintenance);
            }

            // Zobrazim
            ShowPage(userControlMaintenance);
        }

        private void toolStripButtonWeighings_Click(object sender, EventArgs e) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                ShowWeighings();
                ClearIconsCheck();
                toolStripButtonWeighings.Checked = true;
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void toolStripButtonFlocks_Click(object sender, EventArgs e) {
            if (toolStripButtonFlocks.Checked) {
                return;     // Hejna jsou prave zobrazene
            }
            
            Cursor.Current = Cursors.WaitCursor;
            try {
                ClearIconsCheck();
                toolStripButtonFlocks.Checked = true;
                ShowFlocks();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void toolStripButtonMaintenance_Click(object sender, EventArgs e) {
            if (toolStripButtonMaintenance.Checked) {
                return;     // Udrzba je prave zobrazena
            }
            
            ClearIconsCheck();
            toolStripButtonMaintenance.Checked = true;
            ShowMaintenance();
        }

        private void toolStripButtonReadData_Click(object sender, EventArgs e) {
            Cursor.Current = Cursors.WaitCursor;
            try {
                ShowScale();
                ClearIconsCheck();
                toolStripButtonScale.Checked = true;
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void toolStripButtonStatistics_Click(object sender, EventArgs e) {
            if (toolStripButtonStatistics.Checked) {
                return;     // Statistika je prave zobrazena
            }
            
            Cursor.Current = Cursors.WaitCursor;
            try {
                ClearIconsCheck();
                toolStripButtonStatistics.Checked = true;
                ShowStatistics();
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e) {
            // Zkontroluju, zda byla naposledy nactena data ulozena do DB
            if (!SaveStatus.Check(Program.ApplicationName)) {
                e.Cancel = true;    // Nechce pokracovat
            }
        }

    }
}
