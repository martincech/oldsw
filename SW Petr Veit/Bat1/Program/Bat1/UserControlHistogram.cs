﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using Veit.ScaleStatistics;

namespace Bat1 {
    public partial class UserControlHistogram : UserControl {
        private int mouseDownX = 0, mouseDownY = 0;
        private bool undoZoom;

        private double xAxisMin = 0, xAxisMax = 1;

        private BoxObj boxUniformity;

        private Color uniformityScreenColor = Color.FromArgb(15, Color.Black);
        private Color uniformityPrintColor  = Color.Gainsboro;

        public ZedGraphControl Graph { get { return graph; } }

        /// <summary>
        /// Pen width for grid, tics and border
        /// </summary>
        private const float penWidth = 0.5F;          // Na monitoru se zobrazi korektne jako 1px, uzsi je jen pri tisku

        private void CreateGraph(ZedGraphControl zedGraphControl) {
			GraphPane graphPane = zedGraphControl.GraphPane;

            // Fonty s konstantni velikosti
            zedGraphControl.GraphPane.IsFontsScaled = false;

            // Barva pozadi stejna jako barva controlu, ktery histogram vlastni
            Fill fill = new Fill(Parent.BackColor);
            graphPane.Fill       = fill;
            graphPane.Chart.Fill = fill;

            // Nazev grafu
            graphPane.Title.IsVisible = false;

            // Schovam legendu
            graphPane.Legend.IsVisible = false;

            // Schovam mrizku okolo
            graphPane.Border.IsVisible = false;
            zedGraphControl.MasterPane.Border.IsVisible = false;
//            graphPane.Chart.Border.Color = Color.Gray;
            
            // Osa X
            graphPane.XAxis.Title.Text            = Properties.Resources.WEIGHT;
            graphPane.XAxis.Title.FontSpec.Size   = 11;
            graphPane.XAxis.Title.FontSpec.IsBold = false;
            graphPane.XAxis.MajorGrid.IsVisible   = false;          // Schovam mrizku
            graphPane.XAxis.MajorGrid.IsZeroLine  = false;          // Schovam cernou linku na hodnote 0
            graphPane.XAxis.MajorTic.IsOpposite   = false;
            graphPane.XAxis.MajorTic.Size         = 3.1F;
            graphPane.XAxis.MajorTic.IsInside     = false;
            graphPane.XAxis.MinorTic.IsOpposite   = false;
            graphPane.XAxis.MinorTic.Size         = 1.3F;
            graphPane.XAxis.MinorTic.IsInside     = false;
            graphPane.XAxis.Scale.FontSpec.Size   = 11;
    
            // Osa Y
            graphPane.YAxis.Title.Text            = Properties.Resources.COUNT;
            graphPane.YAxis.Title.FontSpec.Size   = 11;
            graphPane.YAxis.Title.FontSpec.IsBold = false;
            graphPane.YAxis.Scale.IsVisible       = false;          // Schovam hodnoty
            graphPane.YAxis.MajorGrid.IsVisible   = false;          // Schovam mrizku
            graphPane.YAxis.MajorGrid.IsZeroLine  = true;           // Zobrazim cernou linku na hodnote 0
            graphPane.YAxis.MajorTic.IsOutside    = false;
            graphPane.YAxis.MajorTic.IsOpposite   = false;
            graphPane.YAxis.MajorTic.IsInside     = false;
            graphPane.YAxis.MinorTic.IsOpposite   = false;
            graphPane.YAxis.MinorTic.IsInside     = false;
            graphPane.YAxis.MinorTic.IsOutside    = false;

			// Calculate the Axis Scale Ranges
			zedGraphControl.AxisChange();
		}

        public UserControlHistogram() {
            InitializeComponent();
        }

        public void SetForPrinting() {
            // Pozadi
            graph.GraphPane.Fill.Type       = FillType.None;
            graph.GraphPane.Chart.Fill.Type = FillType.None;

            // Rozsah histogramu
            boxUniformity.Fill.Color = uniformityPrintColor;
        }

        public void SetForScreen() {
            // Pozor, hodnoty musi odpovidat hodnotam ve fci CreateGraph()

            // Pozadi
            graph.GraphPane.Fill.Type       = FillType.Solid;
            graph.GraphPane.Chart.Fill.Type = FillType.Solid;

            // Rozsah histogramu
            boxUniformity.Fill.Color = uniformityScreenColor;
        }

        private void Redraw() {
            graph.ZoomOutAll(graph.GraphPane);  // Odstranim pripadny zoom
			graph.AxisChange();                 // Auto osy
            graph.Invalidate();                 // Prekreslim pomalu
        }
        
        public void ClearData() {
            // Smazu predchozi data
            graph.GraphPane.GraphObjList.Clear();     // Popisky nad sloupci, rozsah uniformity
            graph.GraphPane.CurveList.Clear();        // Prubeh

            // Prekreslim
            Redraw();
        }
        
        public void SetData(Histogram histogram, double average, int uniformityRange) {
			GraphPane graphPane = graph.GraphPane;
            double minValue = double.MaxValue, maxValue = double.MinValue;

            // Smazu predchozi data
            ClearData();

            if (histogram == null) {
                return;     // Necham histogram prazdny a koncim
            }

            // Ulozim do grafu data z histogramu, zaroven zjistim min a max hodnotu
			PointPairList list = new PointPairList();
			for (int i = 0; i < histogram.SlotsCount; i++) {
				list.Add(histogram.Values[i], histogram.Counts[i]);
                if (histogram.Values[i] < minValue) {
                    minValue = histogram.Values[i];
                }
                if (histogram.Values[i] > maxValue) {
                    maxValue = histogram.Values[i];
                }
            }
            BarItem barItem = graphPane.AddBar("", list, Color.Red);

            // Vypln sloupcu
            barItem.Bar.Fill = new Fill(Color.Red);

            // Sirka sloupcu
            graphPane.BarSettings.ClusterScaleWidthAuto = false;
            graphPane.BarSettings.ClusterScaleWidth     = histogram.Step * 1.6;
            
            // Hodnoty sloupcu - default se vykresluje svisly text, musim delat sam pomoci TextObj

            // The ValueHandler is a helper that does some position calculations for us.
            ValueHandler valueHandler = new ValueHandler(graphPane, true);

            CurveItem curve = graphPane.CurveList[0];
            BarItem bar = curve as BarItem;

            if (bar == null) {
                return;
            }
            
            IPointList points = curve.Points;

            for (int i = 0; i < points.Count; i++) {
                // Nuly nevykresluju
                if (points[i].Y == 0) {
                    continue;
                }

                double xVal = valueHandler.BarCenterValue(curve, curve.GetBarWidth(graphPane), i, points[i].X, 0);

                // Calculate the Y value at the center of each bar
                double yVal = points[i].Y;

                // create the text item (assumes the x axis is ordinal or text)
                // for negative bars, the label appears just above the zero value
                TextObj text = new TextObj(yVal.ToString("F0"), (float)xVal, (float)yVal);

                // Orezu hodnoty na ramecek grafu, jinak se pri zoomu nebo posuvu zobrazuji i mimo
                text.IsClippedToChartRect = true;
                
                // tell Zedgraph to use user scale units for locating the TextObj
                text.Location.CoordinateFrame = CoordType.AxisXYScale;
                text.FontSpec.Size = 9;
                // AlignH the left-center of the text to the specified point
                text.Location.AlignH = AlignH.Center;
                text.Location.AlignV = AlignV.Bottom;
                text.FontSpec.Border.IsVisible = false;
                // rotate the text 90 degrees
                text.FontSpec.Angle = 0;
                text.FontSpec.Fill.IsVisible = false;
                // add the TextObj to the list
                graphPane.GraphObjList.Add(text);
            }

            // Rozsahu uniformity
            // Zjistim maximalni vysku
            int maxCount = 0;
            foreach (int count in histogram.Counts) {
                if (count > maxCount) {
                    maxCount = count;
                }
            }
            // Vykreslim obdelnik, vysku nastavim na dvojnasobek nejvyssiho sloupce
            boxUniformity = new BoxObj(average * (1.0 - (double)uniformityRange / 100.0), (double)(2 * maxCount),
                                       2.0 * (double)uniformityRange * average / 100.0, (double)(2 * maxCount),
                                       Color.Transparent, uniformityScreenColor);
            boxUniformity.ZOrder = ZOrder.E_BehindCurves;     // Umisteni az za sloupci histogramu
            boxUniformity.IsClippedToChartRect = true;        // Orez rameckem grafu
            graphPane.GraphObjList.Add(boxUniformity);

            // Rucne nastavim minimum a maximum osy X, zaroven si meze zapamatuju pro rucni unzoom
            if (points.Count > 0) {
                xAxisMin = histogram.Values[0] - histogram.Step;
                if (xAxisMin < 0) {
                    xAxisMin = 0;
                }
                xAxisMax = histogram.Values[histogram.SlotsCount - 1] + histogram.Step;
                graphPane.XAxis.Scale.Min = xAxisMin;
                graphPane.XAxis.Scale.Max = xAxisMax;
            }

            // Prekreslim
            Redraw();
        }

        private void UserControlHistogram_Load(object sender, EventArgs e) {
            CreateGraph(graph);
        }

        private void graph_ZoomEvent(ZedGraphControl sender, ZoomState oldState, ZoomState newState) {
            if (undoZoom) {
                // Misto zvetseni nastavim puvodni meritko
                sender.ZoomOutAll(sender.GraphPane);
                
                // Osu X mam nastavenou rucne
                sender.GraphPane.XAxis.Scale.Min = xAxisMin;
                sender.GraphPane.XAxis.Scale.Max = xAxisMax;

                // Osa Y je automaticky
                sender.GraphPane.YAxis.Scale.MinAuto = true;
                sender.GraphPane.YAxis.Scale.MaxAuto = true;
            }
        }

        private bool graph_MouseUpEvent(ZedGraphControl sender, MouseEventArgs e) {
            undoZoom = false;
            if (e.Button == MouseButtons.Left) {
                if (e.X < mouseDownX && e.Y < mouseDownY) {
                    // Smer doleva nahoru => udelam unzoom stejne jak jsem zvykly
                    undoZoom = true;
                }
            }
            return default(bool);
        }

        private bool graph_MouseDownEvent(ZedGraphControl sender, MouseEventArgs e) {
            if (e.Button == MouseButtons.Left) {
                // Zapamatuju si souradnice stisku leveho tlacitka
                mouseDownX = e.X;
                mouseDownY = e.Y;
            }
            return default(bool);
        }
    }
}
