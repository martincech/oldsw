﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Veit.Database;
using Veit.Bat1;
using System.Data.Common;
using System.Data.SQLite;
using Veit.Scale;

namespace Bat1 {
    public class Database {

        /// <summary>
        /// Database factory used for connection
        /// </summary>
        public  DatabaseFactory Factory { get { return factory; } }
        private DatabaseFactory factory;
        
        /// <summary>
        /// Database file name including path
        /// </summary>
        public readonly string DatabaseFileName;

        /// <summary>
        /// Files table
        /// </summary>
        public  DatabaseTableFiles FilesTable { get { return filesTable; } }
        private DatabaseTableFiles filesTable;

        /// <summary>
        ///  ScaleConfigs table
        /// </summary>
        public  DatabaseTableScaleConfigs ScaleConfigsTable { get { return scaleConfigsTable; } }
        private DatabaseTableScaleConfigs scaleConfigsTable;

        /// <summary>
        /// Groups table
        /// </summary>
        public DatabaseTableGroups GroupsTable { get { return groupsTable; } }
        private DatabaseTableGroups groupsTable;

        /// <summary>
        /// Group files table
        /// </summary>
        public DatabaseTableGroupFiles GroupFilesTable { get { return groupFilesTable; } }
        private DatabaseTableGroupFiles groupFilesTable;
        
        /// <summary>
        /// Weighings table
        /// </summary>
        public  DatabaseTableWeighings WeighingsTable { get { return weighingsTable; } }
        private DatabaseTableWeighings weighingsTable;

        /// <summary>
        /// Samples table
        /// </summary>
        public  DatabaseTableSamples SamplesTable { get { return samplesTable; } }
        private DatabaseTableSamples samplesTable;

        /// <summary>
        /// Manual results table
        /// </summary>
        public  DatabaseTableManualResults ManualResultsTable { get { return manualResultsTable; } }
        private DatabaseTableManualResults manualResultsTable;

        /// <summary>
        /// Setup table
        /// </summary>
        public  DatabaseTableSetup SetupTable { get { return setupTable; } }
        private DatabaseTableSetup setupTable;

        /// <summary>
        /// Curves table
        /// </summary>
        public  DatabaseTableCurves CurvesTable { get { return curvesTable; } }
        private DatabaseTableCurves curvesTable;

        /// <summary>
        /// Curve points table
        /// </summary>
        public  DatabaseTableCurvePoints CurvePointsTable { get { return curvePointsTable; } }
        private DatabaseTableCurvePoints curvePointsTable;

        /// <summary>
        /// Flocks table
        /// </summary>
        public  DatabaseTableFlocks FlocksTable { get { return flocksTable; } }
        private DatabaseTableFlocks flocksTable;

        /// <summary>
        /// Flock files table
        /// </summary>
        public  DatabaseTableFlockFiles FlockFilesTable { get { return flockFilesTable; } }
        private DatabaseTableFlockFiles flockFilesTable;

        /// <summary>
        /// Table with selected flocks
        /// </summary>
        public  DatabaseTableSelectedFlocks SelectedFlocksTable { get { return selectedFlocksTable; } }
        private DatabaseTableSelectedFlocks selectedFlocksTable;

        /// <summary>
        ///  Constructor
        /// </summary>
        public Database(string databaseFileName) {
            // Ulozim si soubor databaze
            DatabaseFileName = databaseFileName;
            
            // Vytvorim spojeni
            factory = new DatabaseFactory(SQLiteFactory.Instance, GetConnectionString());
            factory.SetTypesSqlite();

            // Vytvorim vsechny tabulky
            filesTable          = new DatabaseTableFiles(factory);
            scaleConfigsTable   = new DatabaseTableScaleConfigs(factory);
            groupsTable         = new DatabaseTableGroups (factory);
            groupFilesTable     = new DatabaseTableGroupFiles(factory);
            weighingsTable      = new DatabaseTableWeighings(factory);
            samplesTable        = new DatabaseTableSamples(factory);
            manualResultsTable  = new DatabaseTableManualResults(factory);
            setupTable          = new DatabaseTableSetup(factory);
            curvesTable         = new DatabaseTableCurves(factory);
            curvePointsTable    = new DatabaseTableCurvePoints(factory);
            flocksTable         = new DatabaseTableFlocks(factory);
            flockFilesTable     = new DatabaseTableFlockFiles(factory);
            selectedFlocksTable = new DatabaseTableSelectedFlocks(factory);
        }

        private string GetConnectionString() {
            SQLiteConnectionStringBuilder builder = new SQLiteConnectionStringBuilder();
            builder.DataSource       = DatabaseFileName;
            builder.UseUTF16Encoding = false;                       // Chci kodovani UTF-8
            builder.LegacyFormat     = false;                       // Verze 3.3x ma lepsi kompresi
            builder.DateTimeFormat   = SQLiteDateFormats.JulianDay; // Format data/casu jako 8 bajtovy double
            return builder.ToString();
        }

        /// <summary>
        /// Check if database exists
        /// </summary>
        /// <returns>True if exists</returns>
        public bool Exists() {
            return System.IO.File.Exists(DatabaseFileName);
        }

        /// <summary>
        /// Create empty database
        /// </summary>
        public void CreateDatabase() {
            System.IO.File.Delete(DatabaseFileName);
            SQLiteConnection.CreateFile(DatabaseFileName);
        }
        
        /// <summary>
        /// Create all tables
        /// </summary>
        public void CreateTables() {
            // Otevru spojeni
            factory.OpenConnection();

            try {
                filesTable.Create();
                scaleConfigsTable.Create();
                groupsTable.Create();
                groupFilesTable.Create();
                weighingsTable.Create();
                samplesTable.Create();
                manualResultsTable.Create();
                setupTable.Create();
                curvesTable.Create();
                curvePointsTable.Create();
                flocksTable.Create();
                flockFilesTable.Create();
                selectedFlocksTable.Create();
            } finally {
                // Nakonec vzdy zavru spojeni
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Check integrity of the database file
        /// </summary>
        /// <returns>True if the database is OK</returns>
        public bool CheckIntegrity() {
            try {
                factory.OpenConnection();
            } catch {
                // Spatny format (napr. po rucni editaci souboru databaze)
                return false;
            }
            try {
                using (DbCommand command = factory.CreateCommand("PRAGMA integrity_check")) {
                    return (bool)((string)command.ExecuteScalar() == "ok");
                }
            } finally {
                factory.CloseConnection();
            }
        }
        
        /// <summary>
        /// Compact database file
        /// </summary>
        public void Vacuum() {
            factory.OpenConnection();
            try {
                using (DbCommand command = factory.CreateCommand("VACUUM")) {
                    command.ExecuteNonQuery();
                }
            } finally {
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Safely copy database file to another file
        /// </summary>
        /// <param name="destination">New file name including path</param>
        public void Copy(string destination) {
            // Pri otevreni spojeni se zaroven smaze pripadny journal, takze zbude jen soubor databaze
            factory.OpenConnection();       
            try {
                using (DbCommand command = factory.CreateCommand()) {
                    // Zamknu databazi
                    command.CommandText = "BEGIN IMMEDIATE";
                    command.ExecuteNonQuery();

                    // Zkopiruju soubor databaze, prepisu pokud uz existuje
                    try {
                        System.IO.File.Copy(DatabaseFileName, destination, true);
                    } finally {
                        // Uvolnim databazi i pokud se kopie nezadarila
                        command.CommandText = "ROLLBACK";
                        command.ExecuteNonQuery();
                    }
                }
            } finally {
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Check if setup is valid
        /// </summary>
        public void CheckSetup() {
            factory.OpenConnection();
            try {
                setupTable.Check();
            } finally {
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Check database version and update if necessary
        /// </summary>
        public bool Update() {
            factory.OpenConnection();
            try {
                // Nactu verzi programu, ktery pracoval s DB naposledy
                DatabaseVersion databaseVersion = setupTable.LoadVersion();

                // Pokud s DB uz pracoval nejaky novejsi SW, odmitnu DB spustit pod touto verzi SW.
                // DB by mohla obsahovat novejsi jazyky, novejsi polozky configu nebo muze mit DB zmenenou strukturu,
                // kterou tento SW nepodporuje. Uzivatel musi nejdriv upgradovat SW.
                if (databaseVersion.Minor > SwVersion.MINOR || databaseVersion.Database > SwVersion.DATABASE) {
                    return false;       // DB je novejsi nez SW, nelze ji pouzit
                }

                // Update vsech tabulek
                setupTable.Update();
                filesTable.Update();
                weighingsTable.Update();
                scaleConfigsTable.Update();

                // Tabulka s vybranymi hejny pribyla ve verzi 7.3.3.0
                if (!selectedFlocksTable.Exists()) {
                    selectedFlocksTable.Create();
                }

                // Ulozim do tabulky nastaveni aktualni verzi programu
                setupTable.SaveVersion();

                return true;
            } finally {
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Read info of all weighings
        /// </summary>
        /// <returns>List of WeighingSearchInfo</returns>
        public List<WeighingSearchInfo> ReadWeighingSearchInfo() {
            // Otevru spojeni
            factory.OpenConnection();

            try {
                using (DbCommand command = factory.CreateCommand(
                    "SELECT W.WeighingId AS WeighingId, W.SamplesMinDateTime AS SamplesMinDateTime, "
                    + "W.SamplesMaxDateTime AS SamplesMaxDateTime, W.Note AS Note, S.ScaleName AS ScaleName, "
                    + "F.Name AS FileName "
                    + "FROM Weighings W, ScaleConfigs S, Files F "
                    + "WHERE W.ScaleConfigId = S.ScaleConfigId AND W.FileId = F.FileId "
                    + "GROUP BY W.WeighingId, W.SamplesMinDateTime, W.SamplesMaxDateTime, F.Name, S.ScaleName, W.Note "
                    + "ORDER BY W.SamplesMinDateTime, F.Name, S.ScaleName"
                    )) {
                    using (DbDataReader reader = command.ExecuteReader()) {
                        List<WeighingSearchInfo> weighingInfoList = new List<WeighingSearchInfo>();
                        while (reader.Read()) {
                            WeighingSearchInfo weighingInfo = new WeighingSearchInfo();
                            weighingInfo.id          = (int)(long) reader["WeighingId"];
                            weighingInfo.minDateTime = (DateTime)  reader["SamplesMinDateTime"];
                            weighingInfo.maxDateTime = (DateTime)  reader["SamplesMaxDateTime"];
                            weighingInfo.scaleName   = (string)    reader["ScaleName"];
                            weighingInfo.fileName    = (string)    reader["FileName"];
                            weighingInfo.note        = (string)    reader["Note"];

                            weighingInfoList.Add(weighingInfo);
                        }
                        return weighingInfoList;
                    }
                }
            } finally {
                // Nakonec vzdy zavru spojeni
                factory.CloseConnection();
            }
        }
        
        /// <summary>
        /// Read list of all weighings sorted by weighing date/time
        /// </summary>
        /// <returns>List of weighings</returns>
        public List<Weighing> ReadWeighingIdList() {
            List<long> weighingIdList;
            
            factory.OpenConnection();
            try {
                // Nactu seznam ID vazeni serazeny podle data a casu vazeni
                weighingIdList = weighingsTable.ReadIdList();
            } finally {
                factory.CloseConnection();
            }

            // Nactu jednotliva vazeni
            List<Weighing> weighingList = new List<Weighing>();
            foreach (long weighingId in weighingIdList) {
                weighingList.Add(LoadWeighing(weighingId));
            }
            return weighingList;
        }

        /// <summary>
        /// Read list of weighing Ids that belong to a specified flock and falls within specified time filter. The list is sorted by date.
        /// </summary>
        /// <param name="flockDefinition">Flock definition</param>
        /// <param name="fromDate">Additional time filter (or DateTime.MinValue when NA)</param>
        /// <param name="toDate">Additional time filter (or DateTime.MaxValue when NA)</param>
        /// <returns>List of Ids</returns>
        public List<long> ReadWeighingIdList(FlockDefinition flockDefinition, DateTime fromDate, DateTime toDate) {
            List<long> idList = new List<long>();

            // Pokud hejno neobsahuje zadne soubory, vratim rovnou prazdny seznam
            if (flockDefinition.FlockFileList.Count == 0) {
                return idList;
            }

            // Vytvorim SQL prikaz, urcite tam bude aspon 1 podminka pro soubor
            string sql = "SELECT W.WeighingId AS WeighingId FROM Weighings W, Files F "
                       + "WHERE (W.FileId = F.FileId) AND (";
            
            int conditionIndex = 0;
            foreach (FlockFile flockFile in flockDefinition.FlockFileList) {
                if (conditionIndex > 0) {
                    sql += " OR ";      // Mezi jednotlivymi soubory pridam OR
                }
                string indexStr = conditionIndex.ToString();
                sql += "(F.Name = @FileName" + indexStr
                     + " AND W.SamplesMaxDateTime >= @FromDateTime" + indexStr
                     + " AND W.SamplesMinDateTime <= @ToDateTime" + indexStr + ")";

                conditionIndex++;
            }

            sql += ") ORDER BY W.SamplesMinDateTime, F.Name";

            // Nactu vysledky
            factory.OpenConnection();
            try {
                using (DbCommand command = factory.CreateCommand(sql)) {
                    // Vytvorim parametry
                    command.Parameters.Clear();
                    conditionIndex = 0;
                    foreach (FlockFile flockFile in flockDefinition.FlockFileList) {
                        string indexStr = conditionIndex.ToString();
                        command.Parameters.Add(factory.CreateParameter("@FileName" + indexStr, flockFile.FileName));
                        DateTime from = new DateTime(flockFile.From.Year, flockFile.From.Month, flockFile.From.Day, 0, 0, 0);
                        if (fromDate != DateTime.MinValue && fromDate > from) {
                            from = fromDate;    // Pokud zadal casovy filtr a pokud je datum Od starsi, omezim
                        }
                        command.Parameters.Add(factory.CreateParameter("@FromDateTime" + indexStr, from));
                        DateTime to = new DateTime(flockFile.To.Year, flockFile.To.Month, flockFile.To.Day, 23, 59, 59);
                        if (toDate != DateTime.MaxValue && toDate < to) {
                            to = toDate;        // Pokud zadal casovy filtr a pokud je datum Do mladsi, omezim
                        }
                        command.Parameters.Add(factory.CreateParameter("@ToDateTime" + indexStr, to));

                        conditionIndex++;
                    }

                    using (DbDataReader reader = command.ExecuteReader()) {
                        while (reader.Read()) {
                            idList.Add((long)reader["WeighingId"]);
                        }
                        return idList;
                    }
                }
            } finally {
                // Nakonec vzdy zavru spojeni
                factory.CloseConnection();
            }
            
        }

        /// <summary>
        /// Read list of weighing Ids that belong to a specified flock. The list is sorted by date.
        /// </summary>
        /// <param name="flockDefinition">Flock definition</param>
        /// <returns>List of Ids</returns>
        public List<long> ReadWeighingIdList(FlockDefinition flockDefinition) {
            return ReadWeighingIdList(flockDefinition, DateTime.MinValue, DateTime.MaxValue);   // Bez casoveho omezeni
        }
        
        /// <summary>
        /// Delete list of weighings in one transaction
        /// </summary>
        /// <param name="weighingIdList">List of weighing Ids to delete</param>
        /// <returns>True if successful</returns>
        public bool DeleteWeighings(List<long> weighingIdList) {
            long fileId, scaleConfigId;

            // Zacnu transakci - vsechna vazeni mazu v jedne transakci, jinak to trva dele
            Factory.BeginTransaction();

            try {
                foreach (long weighingId in weighingIdList) {
                    // Najdu pozadovane vazeni v databazi
                    if (!WeighingsTable.ReadIds(weighingId, out fileId, out scaleConfigId)) {
                        return false;
                    }
                    
                    // Smazu vzorky
                    SamplesTable.Delete(weighingId);

                    // Smazu rucni vysledky
                    ManualResultsTable.Delete(weighingId);

                    // Smazu vazeni
                    WeighingsTable.Delete(weighingId);

                    // Globalni config vahy, vcetne seznamu souboru a skupin muzu smazat pouze pokud uz config
                    // neni pouzity u zadneho jineho vazeni. Pokud je config pouzity jeste jinde, musim ho
                    // v databazi ponechat. Smaze se az pri smazani posledniho vazeni, ktere tento config pouziva.
                    if (!WeighingsTable.ScaleConfigExists(scaleConfigId)) {
                        // Smazu kompletni config, transakci ovladam zde
                        DeleteScaleConfigRaw(scaleConfigId);
                    }
                }

                // Ulozim transakci
                Factory.CommitTransaction();

                return true;
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Save sample list of a specified weighing within an existing transaction
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <param name="sampleList">Sample list to save</param>
        private void SaveSampleListRaw(long weighingId, SampleList sampleList) {
            // Transakce uz musi byt zahajena a obslouzena volajici fci
            sampleList.First();
            while (sampleList.Read()) {
                samplesTable.Add(weighingId, sampleList.Sample.weight,
                                 (short)sampleList.Sample.flag, sampleList.Sample.dateTime);
            }
        }

        /// <summary>
        /// Change units in ScaleConfig with specified Weighing ID
        /// </summary>
        /// <param name="weighingId">Weighing ID</param>
        /// <param name="scaleConfig">ScaleConfig with new units</param>
        private void ChangeUnitsInConfigRaw(long weighingId, ScaleConfig scaleConfig, out Units oldUnits, out long scaleConfigId) {
            // Podle zadaneho weighing ID najdu ID configu
            long fileId;
            if (!weighingsTable.ReadIds(weighingId, out fileId, out scaleConfigId)) {
                throw new Exception("Cannot find weighing");
            }

            // Nactu ulozene jednotky z DB a zjistim, zda je vubec treba zmena
            ScaleConfig oldScaleConfig = scaleConfigsTable.Load(scaleConfigId);
            if (oldScaleConfig == null) {
                throw new Exception("Cannot find scale config");
            }
            oldUnits = oldScaleConfig.Units.Units;
            if (oldUnits == scaleConfig.Units.Units) {
                return;       // Jednotky jsou stejne, neni treba zmena
            }

            // Zmenim jednotky v configu
            scaleConfigsTable.Update(scaleConfigId, scaleConfig);

            // Zmenim jednotky v configu u vsech souboru (i pokud nepouziva config pro kazdy soubor zvlast)
            filesTable.ChangeUnits(scaleConfigId, scaleConfig, oldUnits);
        }
        
        private void ChangeUnitsInSamplesRaw(long scaleConfigId, long excludeWeighingId, Units oldUnits, Units newUnits) {
            // Nactu seznam vazeni se zadanym configem
            List<long> weighingIdList = weighingsTable.ReadIdList(scaleConfigId);

            // Smazu ze seznamu pozadovane vazeni
            weighingIdList.Remove(excludeWeighingId);

            // U vsech vazeni prepoctu jednotky
            foreach (long weighingId in weighingIdList) {
                // Nactu vzorky z DB
                SampleList sampleList = samplesTable.Load(weighingId);

                // Prepoctu jednotky
                sampleList.Convert(oldUnits, newUnits);

                // Smazu vsechny vzorky
                samplesTable.Delete(weighingId);

                // Ulozim nove vzorky
                SaveSampleListRaw(weighingId, sampleList);
            }
        }

        /// <summary>
        /// Update file name, file note, weighing note and sample list of a specified weighing
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <param name="fileName">New file name</param>
        /// <param name="fileNote">New file note</param>
        /// <param name="weighingNote">New weighing note</param>
        /// <param name="sampleList">New samples</param>
        /// <param name="scaleConfig">ScaleConfig with new units</param>
        public void UpdateWeighing(long weighingId, string fileName, string fileNote, string weighingNote,
                                   SampleList sampleList, ScaleConfig scaleConfig) {
            Factory.BeginTransaction();
            try {
                // Nactu Id souboru
                long fileId = weighingsTable.ReadFileId(weighingId);
                if (fileId < 0) {
                    return;     // Nenasel jsem
                }

                // Updatuju jmeno a poznamku souboru
                filesTable.Update(fileId, fileName, fileNote);

                // Updatuju poznamku a rozsah datumu vazeni
                weighingsTable.Update(weighingId, weighingNote, sampleList.MinDateTime, sampleList.MaxDateTime);

                // Smazu vsechny vzorky
                samplesTable.Delete(weighingId);

                // Ulozim nove vzorky
                SaveSampleListRaw(weighingId, sampleList);

                // Pokud zmenil jednotky, musim je zmenit v ScaleConfig. Zaroven musim prepocitat vsechny vzorky, ktere
                // sdili tento ScaleConfig
                long scaleConfigId;
                Units oldUnits;
                ChangeUnitsInConfigRaw(weighingId, scaleConfig, out oldUnits, out scaleConfigId);

                if (oldUnits != scaleConfig.Units.Units) {
                    // Doslo ke zmene jednotek v configu, musim prepocitat vzorky ve vsech vazenich, ktere sdili tento config
                    // Pozor, vazeni s weighingId uz je v DB prepoctene, tj. musim ho vynechat
                    ChangeUnitsInSamplesRaw(scaleConfigId, weighingId, oldUnits, scaleConfig.Units.Units);
                }

                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Delete weighing
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>True is successful</returns>
        public bool DeleteWeighing(long weighingId) {
            List<long> list = new List<long>();
            list.Add(weighingId);
            return DeleteWeighings(list);
        }

        /// <summary>
        /// Load weighing data from database
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>WeighingData instance</returns>
        private WeighingData LoadWeighingData(long weighingId) {
            long fileId, scaleConfigId;
            ResultType resultType;
            RecordSource recordSource;
            string note;

            Factory.OpenConnection();

            try {
                // Najdu pozadovane vazeni v databazi
                if (!WeighingsTable.ReadInfo(weighingId, out fileId, out scaleConfigId, out resultType,
                                             out recordSource, out note)) {
                    return null;
                }

                // Nactu vsechny potrebne udaje z databaze
                
                // Config vahy
                ScaleConfig scaleConfig = LoadScaleConfigRaw(scaleConfigId);   // Komplet config vcetne souboru a skupin

                // Soubor, ktery se vazil nastavim jako ukazatel do configu vahy (tam uz mam vsechny soubory nactene)
                File file = scaleConfig.GetFile(FilesTable.LoadName(fileId));

                // Vzorky jen u vazeni stahnuteho z vahy (u rucne zadanych vysledku vzorky nejsou)
                SampleList  sampleList  = null;
                if (resultType != ResultType.MANUAL) {
                    sampleList = SamplesTable.Load(weighingId);
                }

                // Vytvorim a vratim vazeni
                return new WeighingData(weighingId, resultType, recordSource, file, sampleList, scaleConfig, note);
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load complete weighing from database
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>Weighing instance</returns>
        public Weighing LoadWeighing(long weighingId) {
            WeighingData weighingData = LoadWeighingData(weighingId);
            if (weighingData == null) {
                return null;
            }
            if (weighingData.ResultType == ResultType.MANUAL) {
                // Nactu jeste rucne zadane vysledky
                WeighingResults weighingResults = new WeighingResults(LoadManualResults(weighingId));
                return new Weighing(weighingData, weighingResults);
            } else {
                // U vysledku z vahy se vysledky vypocitaji automaticky ze vzorku
                return new Weighing(weighingData);
            }
        }

        /// <summary>
        /// Load sorted list of all file names used in the database
        /// </summary>
        /// <returns>Sorted list of file names</returns>
        public List<string> LoadFileNameList() {
            Factory.OpenConnection();
            try {
                return FilesTable.LoadNameList();
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load sorted list of all scale names used in the database
        /// </summary>
        /// <returns>Sorted list of scale names</returns>
        public List<string> LoadScaleNameList() {
            Factory.OpenConnection();
            try {
                return ScaleConfigsTable.LoadScaleNameList();
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Save manually entered results
        /// </summary>
        /// <param name="scaleName">Scale name</param>
        /// <param name="file">File name and note</param>
        /// <param name="recordSource">Saved in SW or imported</param>
        /// <param name="startDateTime">Date and time when weighing started</param>
        /// <param name="note">Weighing note</param>
        /// <param name="units">Units</param>
        /// <param name="resultList">List of results</param>
        public void SaveWeighing(string scaleName, NameNote file, RecordSource recordSource, DateTime startDateTime, string note, Units units, List<StatisticResult> resultList) {
            Factory.BeginTransaction();

            try {
                // Ulozim nastaveni vahy
                ScaleConfig config = new ScaleConfig(units);
                config.Version.Major = 0;       // Cislo verze vynuluju
                config.Version.Minor = 0;
                config.Version.Build = 0;
                config.Version.Hw    = 0;
                config.ScaleName     = scaleName;
                long scaleConfigId = scaleConfigsTable.Add(config);

                // Ulozim soubor jako soucast configu
                long fileId = filesTable.Add(new File(file.Name, file.Note, new FileConfig(config)), scaleConfigId);

                // Ulozim vazeni
                long weighingId = weighingsTable.Add(fileId, scaleConfigId, ResultType.MANUAL, recordSource, note, startDateTime, startDateTime.AddHours(1));

                // Ulozim vysledky
                foreach (StatisticResult result in resultList) {
                    if (result.Flag == Flag.ALL) {
                        continue;       // Pokud exportuju vazeni zadane rucne, je ve vysledcich i FLAG_ALL, ktery ale neexportuju
                    }
                    manualResultsTable.Add(weighingId, startDateTime, result.Flag, result.Count, result.Average, result.Sigma, result.Cv, (int)result.Uniformity);
                }

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Save list of weighings read from the scale. All weighings share the same ScaleConfig obtained
        /// form the first weighing.
        /// </summary>
        /// <param name="weighingData">Weighing list</param>
        public void SaveWeighingList(List<Weighing> weighingList) {
            // Vazeni nactena z vahy sdili jedno nastaveni vahy, proto je nutne ukladat seznam souboru najednou.
            // Nastaveni vahy se vytvori jen jedno (vcetne seznamu souboru a skupin), jednotliva vazeni se pak
            // odkazuji na config a prislusny soubor v ramci configu.

            if (weighingList.Count == 0) {
                return;
            }
            
            Factory.BeginTransaction();

            try {
                // Ulozim nastaveni vahy, vcetne seznamu souboru a skupin
                // Nastaveni vezmu z prvniho vazeni v seznamu (vsechna vazeni maji nastaveni shodne)
                long scaleConfigId = SaveScaleConfigRaw(weighingList[0].WeighingData.ScaleConfig);  // V ramci transakce

                // Postupne ulozim vsechna vazeni v seznamu
                foreach (Weighing weighing in weighingList) {
                    WeighingData weighingData = weighing.WeighingData;

                    // Nactu ID souboru tohoto vazeni (soubor uz se do tabulky ulozil v ramci configu vahy)
                    long fileId = filesTable.LoadId(scaleConfigId, weighingData.File.Name);
                    if (fileId < 0) {
                        continue;       // Soubor jsem nenasel (nemelo by nastat, soubor jsem pred chvili ulozil)
                    }

                    // Ulozim vazeni
                    long weighingId = weighingsTable.Add(fileId, scaleConfigId, weighingData.ResultType,
                                                         weighingData.RecordSource, weighingData.Note,
                                                         weighingData.SampleList.MinDateTime,
                                                         weighingData.SampleList.MaxDateTime);

                    // Ulozim vzorky
                    SaveSampleListRaw(weighingId, weighingData.SampleList);
                }

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Save one weighing with its own scale config. Used only for export of weighings from database, where
        /// each weighing must have its own config.
        /// </summary>
        /// <param name="weighing">Weighing to save</param>
        public void SaveWeighing(Weighing weighing) {
            if (weighing.WeighingData.ResultType == ResultType.MANUAL) {
                // Rucne zadane vazeni
                SaveWeighing(weighing.WeighingData.ScaleConfig.ScaleName,
                             new NameNote(weighing.WeighingData.File.Name, weighing.WeighingData.File.Note),
                             weighing.WeighingData.RecordSource,
                             weighing.GetMinDateTime(),
                             weighing.WeighingData.Note,
                             weighing.WeighingData.ScaleConfig.Units.Units,
                             weighing.WeighingResults.StatisticResultList);
                return;
            }

            // Vazeni nactene z vahy (stare nebo nove)
            Factory.BeginTransaction();
            try {
                // Ulozim nastaveni vahy, vcetne seznamu souboru a skupin
                long scaleConfigId = SaveScaleConfigRaw(weighing.WeighingData.ScaleConfig);  // V ramci transakce

                // Nactu ID souboru tohoto vazeni (soubor uz se do tabulky ulozil v ramci configu vahy)
                long fileId = filesTable.LoadId(scaleConfigId, weighing.WeighingData.File.Name);
                if (fileId < 0) {
                    // Soubor jsem nenasel (nemelo by nastat, soubor jsem pred chvili ulozil)
                    throw new Exception("Cannot find file ID " + fileId.ToString());
                }

                // Ulozim vazeni
                long weighingId = weighingsTable.Add(fileId, scaleConfigId, weighing.WeighingData.ResultType,
                                                     weighing.WeighingData.RecordSource, weighing.WeighingData.Note,
                                                     weighing.WeighingData.SampleList.MinDateTime,
                                                     weighing.WeighingData.SampleList.MaxDateTime);

                // Ulozim vzorky
                SaveSampleListRaw(weighingId, weighing.WeighingData.SampleList);

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }


        /// <summary>
        /// Load manual results from database
        /// </summary>
        /// <param name="weighingId">Weighing Id</param>
        /// <returns>List of StatisticResult</returns>
        private List<StatisticResult> LoadManualResults(long weighingId) {
            Factory.OpenConnection();
            try {
                return ManualResultsTable.Load(weighingId);
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load sorted list of all file names and last used notes from the database
        /// </summary>
        /// <returns>List of file names and notes</returns>
        public List<NameNote> LoadFileNameNoteList() {
            Factory.OpenConnection();
            try {
                return FilesTable.LoadNameNoteList();
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Check if weighing with specified file name and starting datetime exists
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="weighingStarted">Datetime when the weighing started</param>
        /// <param name="weighingId">Weighing ID of existing weighing</param>
        /// <returns>True if exists</returns>
        public bool WeighingExists(string fileName, DateTime weighingStarted, out long weighingId) {
            Factory.OpenConnection();

            try {
                using (DbCommand command = factory.CreateCommand(
                    "SELECT W.WeighingId AS WeighingId, F.FileId AS FileId "
                    + "FROM Weighings W, Files F "
                    + "WHERE W.SamplesMinDateTime = @SamplesMinDateTime AND W.FileId = F.FileId AND F.Name = @Name"
                    )) {
                    command.Parameters.Clear();
                    command.Parameters.Add(factory.CreateParameter("@SamplesMinDateTime", weighingStarted));
                    command.Parameters.Add(factory.CreateParameter("@Name",               fileName));
                    using (DbDataReader reader = command.ExecuteReader()) {
                        if (reader.Read()) {
                            // Vazeni existuje, nactu jeho id
                            weighingId = (long)reader["WeighingId"];
                            return true;
                        } else {
                            // Neexistuje
                            weighingId = 0;
                            return false;
                        }
                    }
                }
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load application setup from database
        /// </summary>
        public void LoadSetup() {
            factory.OpenConnection();
            try {
                Program.Setup = setupTable.Load();
            } finally {
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Save application setup to database
        /// </summary>
        public void SaveSetup() {
            factory.OpenConnection();
            try {
                setupTable.Save(Program.Setup);
            } finally {
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Save predefined scale config, including files and groups into the database within existing transaction
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        /// <param name="name">Name of predefined config</param>
        /// <returns>Config ID</returns>
        private long SaveScaleConfigRaw(ScaleConfig scaleConfig, string name) {
            // Transakce uz musi byt zahajena a obslouzena volajici fci

            // Globalni nastaveni
            long scaleConfigId = scaleConfigsTable.Add(scaleConfig, name);

            // Seznam souboru vcetne nastaveni souboru
            List<long> fileIdList = filesTable.Add(scaleConfig.FileList, scaleConfigId);

            // Skupiny
            foreach (FileGroup group in scaleConfig.FileGroupList.List) {
                long groupId = groupsTable.Add(group.Name, group.Note, scaleConfigId);

                // Vytvorim seznam ID souboru, ktere do skupiny patri
                List<long> fileIdInGroupList = new List<long>();
                foreach (int fileIndex in group.FileIndexList) {
                    fileIdInGroupList.Add(fileIdList[fileIndex]);
                }

                // Pridam do skupiny jednotlive soubory
                groupFilesTable.Add(groupId, fileIdInGroupList);
            }

            return scaleConfigId;
        }

        /// <summary>
        /// Save predefined scale config, including files and groups into the database
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        /// <param name="name">Name of predefined config</param>
        /// <returns>Config ID</returns>
        public long SaveScaleConfig(ScaleConfig scaleConfig, string name) {
            // Zacnu transakci
            Factory.BeginTransaction();

            try {
                long scaleConfigId = SaveScaleConfigRaw(scaleConfig, name);

                // Ulozim transakci
                Factory.CommitTransaction();

                return scaleConfigId;
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Save scale config, including files and groups into the database within existing transaction
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        /// <returns>Config ID</returns>
        private long SaveScaleConfigRaw(ScaleConfig scaleConfig) {
            return SaveScaleConfigRaw(scaleConfig, null);
        }

        /// <summary>
        /// Save scale config, including files and groups into the database
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        /// <returns>Config ID</returns>
        public long SaveScaleConfig(ScaleConfig scaleConfig) {
            return SaveScaleConfig(scaleConfig, null);
        }

        /// <summary>
        /// Check if a predefined scale config exists
        /// </summary>
        /// <param name="name">Config name</param>
        /// <returns>True if exists</returns>
        public bool ScaleConfigExists(string name) {
            Factory.OpenConnection();
            try {
                return ScaleConfigsTable.Exists(name);
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Delete scale config within an existing transaction
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        private void DeleteScaleConfigRaw(long scaleConfigId) {
            // Transakce uz musi byt zahajena a obslouzena volajici fci

            // Soubory definovane ve skupinach
            List<long> groupIdList = GroupsTable.LoadIds(scaleConfigId);        // Nahraju seznam ID skupin
            foreach (long groupId in groupIdList) {
                GroupFilesTable.Delete(groupId);
            }

            // Skupiny samotne
            GroupsTable.Delete(scaleConfigId);
            
            // Soubory
            FilesTable.Delete(scaleConfigId);

            // Globalni nastaveni
            ScaleConfigsTable.Delete(scaleConfigId);
        }

        /// <summary>
        /// Delete scale config
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        private void DeleteScaleConfig(long scaleConfigId) {
            // Zacnu transakci, musim mazat z vice tabulek naraz
            Factory.BeginTransaction();
            try {
                DeleteScaleConfigRaw(scaleConfigId);

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Delete predefined scale config
        /// </summary>
        /// <param name="name">Config name</param>
        public void DeleteScaleConfig(string name) {
            long id;

            Factory.OpenConnection();
            try {
                id = ScaleConfigsTable.GetId(name);
            } finally {
                Factory.CloseConnection();
            }

            // Dale se zahaji nova transakce
            DeleteScaleConfig(id);
        }

        /// <summary>
        /// Load predefined config names
        /// </summary>
        /// <returns>List of config names</returns>
        public List<string> LoadPredefinedScaleConfigNameList() {
            Factory.OpenConnection();
            try {
                return ScaleConfigsTable.LoadNameList();
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load complete scale config within an existing connection
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        private ScaleConfig LoadScaleConfigRaw(long scaleConfigId) {
            // Globalni nastaveni
            ScaleConfig scaleConfig = ScaleConfigsTable.Load(scaleConfigId);

            // Soubory prihraju do configu vahy
            List<long> fileIdList;
            scaleConfig.FileList = FilesTable.LoadFileList(scaleConfigId, out fileIdList);

            // Seznam skupin
            List<long> groupIdList;
            scaleConfig.FileGroupList = GroupsTable.Load(scaleConfigId, out groupIdList);
            
            // Soubory v jednotlivych skupinach
            int index = 0;
            foreach (long groupId in groupIdList) {
                scaleConfig.FileGroupList.List[index].FileIndexList.AddRange(GroupFilesTable.LoadFileIndexes(groupId, fileIdList));
                index++;
            }

            return scaleConfig;
        }

        /// <summary>
        /// Load complete scale config
        /// </summary>
        /// <param name="scaleConfigId">Scale config ID</param>
        public ScaleConfig LoadScaleConfig(long scaleConfigId) {
            Factory.OpenConnection();
            try {
                return LoadScaleConfigRaw(scaleConfigId);
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Get ID of predefined config
        /// </summary>
        /// <param name="configName">Config name</param>
        /// <returns>Config ID</returns>
        public long GetPredefinedScaleConfigId(string configName) {
            Factory.OpenConnection();
            try {
                return scaleConfigsTable.GetId(configName);
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load a list of all growth curves saved in the database
        /// </summary>
        /// <returns>CurveList instance</returns>
        public CurveList LoadCurveList() {
            Factory.OpenConnection();
            try {
                // Nahraju seznam s definicemi krivek
                CurveList curveList = curvesTable.LoadList();

                // Prihraju do nacteneho seznamu body krivek
                curvePointsTable.LoadList(ref curveList);

                return curveList;
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Add a growth curve including its points into the database. Curve ID will be automatically updated in the curve instance.
        /// </summary>
        /// <param name="curve">Curve to add</param>
        public void SaveCurve(ref Curve curve) {
            // Zacnu transakci
            Factory.BeginTransaction();

            try {
                // Definice krivky, zaroven vyplnim ID ulozene krivky
                curve.Id = curvesTable.Add(curve);

                // Body krivky
                curvePointsTable.Add(curve);

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Update growth curve including its points in the database
        /// </summary>
        /// <param name="curve">Curve to update</param>
        public void UpdateCurve(Curve curve) {
            // Zacnu transakci
            Factory.BeginTransaction();

            try {
                // Definice krivky
                curvesTable.Update(curve);

                // Smazu puvodni body krivky
                curvePointsTable.Delete(curve.Id);
                
                // Ulozim nove body krivky
                curvePointsTable.Add(curve);

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Delete growth curve including its points from the database. The curve will be deleted from all flocks as well.
        /// </summary>
        /// <param name="curve">Curve ID to delete</param>
        public void DeleteCurve(long curveId) {
            // Zacnu transakci
            Factory.BeginTransaction();

            try {
                // Vymazu krivku ze vsech hejn, kde je zminovana
                flocksTable.DeleteCurve(curveId);

                // Definice krivky
                curvesTable.Delete(curveId);

                // Body krivky
                curvePointsTable.Delete(curveId);

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

// NEFUNGUJE
/*        /// <summary>
        /// Read last weighings of all files
        /// </summary>
        /// <returns>List of WeighingSearchInfo</returns>
        public List<WeighingSearchInfo> ReadLastWeighings() {
            // Otevru spojeni
            factory.OpenConnection();

            try {
                // Staci mne ID vazeni, nic vic nepotrebuju
                using (DbCommand command = factory.CreateCommand(
                    "SELECT W.WeighingId AS WeighingId, MAX(W.SamplesMinDateTime) AS MaxSamplesMinDateTime "
                    + "FROM Files F INNER JOIN Weighings W ON W.FileId = F.FileId "
                    + "GROUP BY F.Name "
                    + "ORDER BY F.Name"
                    )) {
                    using (DbDataReader reader = command.ExecuteReader()) {
                        List<WeighingSearchInfo> weighingInfoList = new List<WeighingSearchInfo>();
                        while (reader.Read()) {
                            WeighingSearchInfo weighingInfo = new WeighingSearchInfo();
                            weighingInfo.id = (long)reader["WeighingId"];
                            weighingInfoList.Add(weighingInfo);
                        }
                        return weighingInfoList;
                    }
                }
            } finally {
                // Nakonec vzdy zavru spojeni
                factory.CloseConnection();
            }
        }*/

// FUNGUJE, ALE PROJIZDIM TO RUCNE
        /// <summary>
        /// Read last weighings of all files
        /// </summary>
        /// <returns>List of WeighingSearchInfo</returns>
        public List<WeighingSearchInfo> ReadLastWeighings() {
            // Otevru spojeni
            factory.OpenConnection();

            try {
                // Nactu seznam jmen souboru, neni treba tridit, na zaver se tridi podle casu
                List<string> fileNameList = new List<string>();
                using (DbCommand command = factory.CreateCommand(
                    "SELECT F.Name AS Name "
                    + "FROM Files F INNER JOIN Weighings W ON W.FileId = F.FileId "
                    + "GROUP BY F.Name"
                    )) {
                    using (DbDataReader reader = command.ExecuteReader()) {
                        while (reader.Read()) {
                            fileNameList.Add((string)reader["Name"]);
                        }
                    }
                }

                // Projizdim tabulku Weighings a hledam posledni vazeni u jednotlivych souboru
                // Staci mne ID vazeni, nic vic nepotrebuju
                List<WeighingSearchInfo> weighingInfoList = new List<WeighingSearchInfo>();
                foreach (string fileName in fileNameList) {
                    using (DbCommand command = factory.CreateCommand(
                        "SELECT W.WeighingId AS WeighingId "
                        + "FROM Weighings W INNER JOIN Files F ON W.FileId = F.FileId "
                        + "WHERE F.Name = @Name "
                        + "ORDER BY W.SamplesMinDateTime DESC "
                        + "LIMIT 1"
                        )) {
                        command.Parameters.Clear();
                        command.Parameters.Add(factory.CreateParameter("@Name", fileName));

                        using (DbDataReader reader = command.ExecuteReader()) {
                            while (reader.Read()) {
                                WeighingSearchInfo weighingInfo = new WeighingSearchInfo();
                                weighingInfo.id = (long)reader["WeighingId"];
                                weighingInfoList.Add(weighingInfo);
                            }
                        }
                    }
                }

                return weighingInfoList;
            } finally {
                // Nakonec vzdy zavru spojeni
                factory.CloseConnection();
            }
        }

        /// <summary>
        /// Delete flock including its files from the database
        /// </summary>
        /// <param name="flockId">Flock ID to delete</param>
        public void DeleteFlock(long flockId) {
            // Zacnu transakci
            Factory.BeginTransaction();

            try {
                // Definice hejna
                flocksTable.Delete(flockId);

                // Soubory hejna
                flockFilesTable.Delete(flockId);

                // Pokud mel hejno vybrane pro zobrazeni, smazu hejno i zde
                selectedFlocksTable.Delete(flockId);

                // Pripadnou teoretickou rustovou krivku nemazu, ponecham ji v seznamu

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        private void LoadCurve(ref Curve curve) {
            if (curve == null) {
                return;       // Krivka neni v tomto hejnu definovana
            }
            
            // Definice krivky
            curve = curvesTable.Load(curve.Id);

            // Body krivky
            curvePointsTable.Load(ref curve);
        }
        
        /// <summary>
        /// Load a list of all flocks saved in the database
        /// </summary>
        /// <returns>CurveList instance</returns>
        public FlockDefinitionList LoadFlockList() {
            Factory.OpenConnection();
            try {
                // Nahraju seznam s definicemi hejn
                FlockDefinitionList flockDefinitionList = flocksTable.LoadList();

                // Soubory hejna
                flockFilesTable.LoadList(ref flockDefinitionList);

                // Teoreticka rustova krivka (pokud je definovana)
                foreach (FlockDefinition flockDefinition in flockDefinitionList.List) {
                    LoadCurve(ref flockDefinition.CurveDefault);
                    LoadCurve(ref flockDefinition.CurveFemales);
                }

                return flockDefinitionList;
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Add a flock including its files into the database. Flock ID will be automatically updated in the flock instance.
        /// </summary>
        /// <param name="flockDefinition">Flock to add</param>
        public void SaveFlock(ref FlockDefinition flockDefinition) {
            // Zacnu transakci
            Factory.BeginTransaction();

            try {
                // Definice hejna, zaroven vyplnim ID ulozeneho hejna
                flockDefinition.Id = flocksTable.Add(flockDefinition);

                // Soubory hejna
                flockFilesTable.Add(flockDefinition);

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Update flock including its files in the database
        /// </summary>
        /// <param name="flockDefinition">Flock to update</param>
        public void UpdateFlock(FlockDefinition flockDefinition) {
            // Zacnu transakci
            Factory.BeginTransaction();

            try {
                // Definice hejna
                flocksTable.Update(flockDefinition);

                // Smazu puvodni soubory hejna
                flockFilesTable.Delete(flockDefinition.Id);

                // Ulozim nove soubory hejna
                flockFilesTable.Add(flockDefinition);

                // Ulozim transakci
                Factory.CommitTransaction();
            } catch (Exception Exception) {
                // Doslo k nejake chybe - zrusim celou transakci a poslu vyjimku dal
                Factory.RollbackTransaction();
                throw Exception;
            }
        }

        /// <summary>
        /// Load number of flocks where specified growth curve is used
        /// </summary>
        /// <param name="curveId">Curve ID</param>
        /// <returns>Number of rows</returns>
        public int FlocksWithCurve(long curveId) {
            Factory.OpenConnection();
            try {
                return flocksTable.FlocksWithCurve(curveId);
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Read flock definitions according to specified ID list
        /// </summary>
        /// <param name="idList">Flock ID list</param>
        /// <returns>FlockDefinitionList instance</returns>
        private FlockDefinitionList LoadFlockDefinitionList(List<long> idList) {
            FlockDefinitionList flockDefinitionList = new FlockDefinitionList();
            foreach (long id in idList) {
                FlockDefinition flockDefinition = flocksTable.Load(id);     // Definice
                flockFilesTable.Load(ref flockDefinition);                  // Prihraju jendotlivce soubory
                LoadCurve(ref flockDefinition.CurveDefault);                // Teoreticka rustova krivka (pokud je definovana)
                LoadCurve(ref flockDefinition.CurveFemales);
                flockDefinitionList.Add(flockDefinition);
            }
            return flockDefinitionList;
        }
        
        /// <summary>
        /// Load list of flocks that are curently being weighed
        /// </summary>
        /// <returns>List flock definitions</returns>
        public FlockDefinitionList LoadCurrentFlockList() {
            Factory.OpenConnection();
            try {
                // Nactu hejna, ktera se prave vazi
                return LoadFlockDefinitionList(flockFilesTable.LoadCurrentFlockIds());
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Return total number of flocks in the database
        /// </summary>
        /// <returns>Number of flocks</returns>
        public int FlockCount() {
            Factory.OpenConnection();
            try {
                return flocksTable.RowCount();
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Save list of selected flocks
        /// </summary>
        /// <param name="flockDefinitionList">Flock list</param>
        public void SaveSelectedFlocks(FlockDefinitionList flockDefinitionList) {
            Factory.OpenConnection();
            try {
                selectedFlocksTable.Save(flockDefinitionList);
            } finally {
                Factory.CloseConnection();
            }
        }

        /// <summary>
        /// Load list of selected flocks
        /// </summary>
        /// <returns>List of flock definitions</returns>
        public FlockDefinitionList LoadSelectedFlocks() {
            Factory.OpenConnection();
            try {
                // Nactu jednotliva hejna, ktera ma prave zobrazene a zaroven jsou v tabulce hejn.
                // SQLite nema referencni integritu, delam tedy radeji JOIN, abych nenacetl neexistujici hejno
                List<long> idList = new List<long>();
                using (DbCommand command = factory.CreateCommand("SELECT SelectedFlocks.FlockId "
                                                               + "FROM SelectedFlocks JOIN Flocks "
                                                               + "ON SelectedFlocks.FlockId = Flocks.FlockId")) {
                    using (DbDataReader reader = command.ExecuteReader()) {
                        while (reader.Read()) {
                            idList.Add((long)reader["FlockId"]);
                        }
                    }
                }
                
                // Nactu definici hejn
                return LoadFlockDefinitionList(idList);
            } finally {
                Factory.CloseConnection();
            }
        }

    }
}
