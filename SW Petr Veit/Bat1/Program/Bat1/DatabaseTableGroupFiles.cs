﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Veit.Database;

namespace Bat1 {
    public class DatabaseTableGroupFiles : DatabaseTable {
    
        // Tabulka s definici souboru v jednotlivych skupinach, tj. ktera skupina obsahuje ktere soubory.

        /// <summary>
        /// Constructor
        /// </summary>
        public DatabaseTableGroupFiles(DatabaseFactory factory)
            : base("GroupFiles", factory) {
        }
        
        /// <summary>
        /// Create new table
        /// </summary>
        public void Create() {
            Create("CREATE TABLE " + tableName + " ("
                 + "GroupId " + factory.TypeLong + " NOT NULL,"     // ID skupiny, do ktere soubor patri
                 + "FileId "  + factory.TypeLong + " NOT NULL,"     // ID souboru
                 + "CONSTRAINT FK_P_Files FOREIGN KEY (FileId) REFERENCES Files(FileId)"
                 + ")");
        }

        /// <summary>
        /// Add files to the group
        /// </summary>
        /// <param name="groupId">Group ID</param>
        /// <param name="fileIdList">List of File IDs</param>
        public void Add(long groupId, List<long> fileIdList) {
            using (DbCommand command = factory.CreateCommand("INSERT INTO " + tableName + " VALUES (@GroupId, @FileId)")) {
                foreach (long fileId in fileIdList) {
                    command.Parameters.Clear();
                    command.Parameters.Add(factory.CreateParameter("@GroupId", groupId));
                    command.Parameters.Add(factory.CreateParameter("@FileId",  fileId));
                    command.ExecuteNonQuery();
                }
            }
        }
        
        /// <summary>
        /// Delete all files that belong to specified group
        /// </summary>
        /// <param name="groupId">Group ID</param>
        /// <returns>Number of rows affected</returns>
        public int Delete(long groupId) {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + tableName + " WHERE GroupId = @GroupId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@GroupId", groupId));
                return command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Load file indexes
        /// </summary>
        /// <param name="groupId">Group ID</param>
        /// <param name="fileIdList">List of file IDs in the database</param>
        /// <returns>List of file indexes</returns>
        public List<int> LoadFileIndexes(long groupId, List<long> fileIdList) {
            using (DbCommand command = factory.CreateCommand("SELECT FileId FROM " + tableName + " WHERE GroupId = @GroupId")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@GroupId", groupId));
                using (DbDataReader reader = command.ExecuteReader()) {
                    List<int> fileIndexList = new List<int>();
                    while (reader.Read()) {
                        int index = fileIdList.IndexOf((int)(long)reader["FileId"]);
                        if (index < 0) {
                            continue;       // Nemelo by nastat, soubor nemuzu najit
                        }
                        fileIndexList.Add(index);
                    }
                    return fileIndexList;
                }
            }
        }

    }
}
