﻿namespace Bat1 {
    partial class UserControlScaleConfigSounds {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigSounds));
            this.groupBoxSaving = new System.Windows.Forms.GroupBox();
            this.numericUpDownSavingVolume = new System.Windows.Forms.NumericUpDown();
            this.comboBoxSavingHeavyTone = new System.Windows.Forms.ComboBox();
            this.comboBoxSavingDefaultTone = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxSavingOkTone = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxSavingLightTone = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxKeyboard = new System.Windows.Forms.GroupBox();
            this.numericUpDownKeyboardVolume = new System.Windows.Forms.NumericUpDown();
            this.comboBoxKeyboardTone = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBoxSpecialSounds = new System.Windows.Forms.CheckBox();
            this.groupBoxSaving.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingVolume)).BeginInit();
            this.groupBoxKeyboard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKeyboardVolume)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxSaving
            // 
            resources.ApplyResources(this.groupBoxSaving, "groupBoxSaving");
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingVolume);
            this.groupBoxSaving.Controls.Add(this.comboBoxSavingHeavyTone);
            this.groupBoxSaving.Controls.Add(this.comboBoxSavingDefaultTone);
            this.groupBoxSaving.Controls.Add(this.label5);
            this.groupBoxSaving.Controls.Add(this.label3);
            this.groupBoxSaving.Controls.Add(this.comboBoxSavingOkTone);
            this.groupBoxSaving.Controls.Add(this.label2);
            this.groupBoxSaving.Controls.Add(this.comboBoxSavingLightTone);
            this.groupBoxSaving.Controls.Add(this.label4);
            this.groupBoxSaving.Controls.Add(this.label1);
            this.groupBoxSaving.Name = "groupBoxSaving";
            this.groupBoxSaving.TabStop = false;
            // 
            // numericUpDownSavingVolume
            // 
            resources.ApplyResources(this.numericUpDownSavingVolume, "numericUpDownSavingVolume");
            this.numericUpDownSavingVolume.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDownSavingVolume.Name = "numericUpDownSavingVolume";
            this.numericUpDownSavingVolume.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownSavingVolume.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // comboBoxSavingHeavyTone
            // 
            resources.ApplyResources(this.comboBoxSavingHeavyTone, "comboBoxSavingHeavyTone");
            this.comboBoxSavingHeavyTone.DropDownHeight = 200;
            this.comboBoxSavingHeavyTone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSavingHeavyTone.FormattingEnabled = true;
            this.comboBoxSavingHeavyTone.Items.AddRange(new object[] {
            resources.GetString("comboBoxSavingHeavyTone.Items"),
            resources.GetString("comboBoxSavingHeavyTone.Items1"),
            resources.GetString("comboBoxSavingHeavyTone.Items2"),
            resources.GetString("comboBoxSavingHeavyTone.Items3"),
            resources.GetString("comboBoxSavingHeavyTone.Items4"),
            resources.GetString("comboBoxSavingHeavyTone.Items5"),
            resources.GetString("comboBoxSavingHeavyTone.Items6"),
            resources.GetString("comboBoxSavingHeavyTone.Items7"),
            resources.GetString("comboBoxSavingHeavyTone.Items8"),
            resources.GetString("comboBoxSavingHeavyTone.Items9"),
            resources.GetString("comboBoxSavingHeavyTone.Items10"),
            resources.GetString("comboBoxSavingHeavyTone.Items11")});
            this.comboBoxSavingHeavyTone.Name = "comboBoxSavingHeavyTone";
            this.comboBoxSavingHeavyTone.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // comboBoxSavingDefaultTone
            // 
            resources.ApplyResources(this.comboBoxSavingDefaultTone, "comboBoxSavingDefaultTone");
            this.comboBoxSavingDefaultTone.DropDownHeight = 200;
            this.comboBoxSavingDefaultTone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSavingDefaultTone.FormattingEnabled = true;
            this.comboBoxSavingDefaultTone.Items.AddRange(new object[] {
            resources.GetString("comboBoxSavingDefaultTone.Items"),
            resources.GetString("comboBoxSavingDefaultTone.Items1"),
            resources.GetString("comboBoxSavingDefaultTone.Items2"),
            resources.GetString("comboBoxSavingDefaultTone.Items3"),
            resources.GetString("comboBoxSavingDefaultTone.Items4"),
            resources.GetString("comboBoxSavingDefaultTone.Items5"),
            resources.GetString("comboBoxSavingDefaultTone.Items6"),
            resources.GetString("comboBoxSavingDefaultTone.Items7"),
            resources.GetString("comboBoxSavingDefaultTone.Items8"),
            resources.GetString("comboBoxSavingDefaultTone.Items9"),
            resources.GetString("comboBoxSavingDefaultTone.Items10"),
            resources.GetString("comboBoxSavingDefaultTone.Items11")});
            this.comboBoxSavingDefaultTone.Name = "comboBoxSavingDefaultTone";
            this.comboBoxSavingDefaultTone.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // comboBoxSavingOkTone
            // 
            resources.ApplyResources(this.comboBoxSavingOkTone, "comboBoxSavingOkTone");
            this.comboBoxSavingOkTone.DropDownHeight = 200;
            this.comboBoxSavingOkTone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSavingOkTone.FormattingEnabled = true;
            this.comboBoxSavingOkTone.Items.AddRange(new object[] {
            resources.GetString("comboBoxSavingOkTone.Items"),
            resources.GetString("comboBoxSavingOkTone.Items1"),
            resources.GetString("comboBoxSavingOkTone.Items2"),
            resources.GetString("comboBoxSavingOkTone.Items3"),
            resources.GetString("comboBoxSavingOkTone.Items4"),
            resources.GetString("comboBoxSavingOkTone.Items5"),
            resources.GetString("comboBoxSavingOkTone.Items6"),
            resources.GetString("comboBoxSavingOkTone.Items7"),
            resources.GetString("comboBoxSavingOkTone.Items8"),
            resources.GetString("comboBoxSavingOkTone.Items9"),
            resources.GetString("comboBoxSavingOkTone.Items10"),
            resources.GetString("comboBoxSavingOkTone.Items11")});
            this.comboBoxSavingOkTone.Name = "comboBoxSavingOkTone";
            this.comboBoxSavingOkTone.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // comboBoxSavingLightTone
            // 
            resources.ApplyResources(this.comboBoxSavingLightTone, "comboBoxSavingLightTone");
            this.comboBoxSavingLightTone.DropDownHeight = 200;
            this.comboBoxSavingLightTone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSavingLightTone.FormattingEnabled = true;
            this.comboBoxSavingLightTone.Items.AddRange(new object[] {
            resources.GetString("comboBoxSavingLightTone.Items"),
            resources.GetString("comboBoxSavingLightTone.Items1"),
            resources.GetString("comboBoxSavingLightTone.Items2"),
            resources.GetString("comboBoxSavingLightTone.Items3"),
            resources.GetString("comboBoxSavingLightTone.Items4"),
            resources.GetString("comboBoxSavingLightTone.Items5"),
            resources.GetString("comboBoxSavingLightTone.Items6"),
            resources.GetString("comboBoxSavingLightTone.Items7"),
            resources.GetString("comboBoxSavingLightTone.Items8"),
            resources.GetString("comboBoxSavingLightTone.Items9"),
            resources.GetString("comboBoxSavingLightTone.Items10"),
            resources.GetString("comboBoxSavingLightTone.Items11")});
            this.comboBoxSavingLightTone.Name = "comboBoxSavingLightTone";
            this.comboBoxSavingLightTone.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // groupBoxKeyboard
            // 
            resources.ApplyResources(this.groupBoxKeyboard, "groupBoxKeyboard");
            this.groupBoxKeyboard.Controls.Add(this.numericUpDownKeyboardVolume);
            this.groupBoxKeyboard.Controls.Add(this.comboBoxKeyboardTone);
            this.groupBoxKeyboard.Controls.Add(this.label8);
            this.groupBoxKeyboard.Controls.Add(this.label11);
            this.groupBoxKeyboard.Name = "groupBoxKeyboard";
            this.groupBoxKeyboard.TabStop = false;
            // 
            // numericUpDownKeyboardVolume
            // 
            resources.ApplyResources(this.numericUpDownKeyboardVolume, "numericUpDownKeyboardVolume");
            this.numericUpDownKeyboardVolume.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.numericUpDownKeyboardVolume.Name = "numericUpDownKeyboardVolume";
            this.numericUpDownKeyboardVolume.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownKeyboardVolume.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // comboBoxKeyboardTone
            // 
            resources.ApplyResources(this.comboBoxKeyboardTone, "comboBoxKeyboardTone");
            this.comboBoxKeyboardTone.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKeyboardTone.FormattingEnabled = true;
            this.comboBoxKeyboardTone.Items.AddRange(new object[] {
            resources.GetString("comboBoxKeyboardTone.Items"),
            resources.GetString("comboBoxKeyboardTone.Items1"),
            resources.GetString("comboBoxKeyboardTone.Items2"),
            resources.GetString("comboBoxKeyboardTone.Items3"),
            resources.GetString("comboBoxKeyboardTone.Items4"),
            resources.GetString("comboBoxKeyboardTone.Items5"),
            resources.GetString("comboBoxKeyboardTone.Items6"),
            resources.GetString("comboBoxKeyboardTone.Items7")});
            this.comboBoxKeyboardTone.Name = "comboBoxKeyboardTone";
            this.comboBoxKeyboardTone.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.Name = "label11";
            // 
            // checkBoxSpecialSounds
            // 
            resources.ApplyResources(this.checkBoxSpecialSounds, "checkBoxSpecialSounds");
            this.checkBoxSpecialSounds.Name = "checkBoxSpecialSounds";
            this.checkBoxSpecialSounds.UseVisualStyleBackColor = true;
            this.checkBoxSpecialSounds.CheckedChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // UserControlScaleConfigSounds
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.checkBoxSpecialSounds);
            this.Controls.Add(this.groupBoxKeyboard);
            this.Controls.Add(this.groupBoxSaving);
            this.Name = "UserControlScaleConfigSounds";
            this.groupBoxSaving.ResumeLayout(false);
            this.groupBoxSaving.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingVolume)).EndInit();
            this.groupBoxKeyboard.ResumeLayout(false);
            this.groupBoxKeyboard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownKeyboardVolume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSaving;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxSavingDefaultTone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxSavingHeavyTone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxSavingOkTone;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxSavingLightTone;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxKeyboard;
        private System.Windows.Forms.ComboBox comboBoxKeyboardTone;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBoxSpecialSounds;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingVolume;
        private System.Windows.Forms.NumericUpDown numericUpDownKeyboardVolume;
    }
}
