﻿namespace Bat1 {
    partial class UserControlScaleConfigFiles {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigFiles));
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonRename = new System.Windows.Forms.Button();
            this.buttonNew = new System.Windows.Forms.Button();
            this.listBoxFiles = new System.Windows.Forms.ListBox();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxSorting = new System.Windows.Forms.GroupBox();
            this.numericUpDownSortingHighLimit = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSortingLowLimit = new System.Windows.Forms.NumericUpDown();
            this.labelHighLimitUnits = new System.Windows.Forms.Label();
            this.labelLowLimitUnits = new System.Windows.Forms.Label();
            this.labelHighLimit = new System.Windows.Forms.Label();
            this.comboBoxSortingMode = new System.Windows.Forms.ComboBox();
            this.labelSortingMode = new System.Windows.Forms.Label();
            this.labelLowLimit = new System.Windows.Forms.Label();
            this.groupBoxSaving = new System.Windows.Forms.GroupBox();
            this.numericUpDownSavingStabilizationRange = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSavingStabilizationTime = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSavingMinimumWeight = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownSavingFilter = new System.Windows.Forms.NumericUpDown();
            this.labelStabilizationRangeUnits = new System.Windows.Forms.Label();
            this.labelStabilizationTimeSec = new System.Windows.Forms.Label();
            this.labelMinimumWeightUnits = new System.Windows.Forms.Label();
            this.labelFilterSec = new System.Windows.Forms.Label();
            this.labelStabilizationRange = new System.Windows.Forms.Label();
            this.labelStabilizationTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxSavingMode = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelMoreBirds = new System.Windows.Forms.Label();
            this.groupBoxDetails = new System.Windows.Forms.GroupBox();
            this.groupBoxMoreBirds = new System.Windows.Forms.GroupBox();
            this.checkBoxEnableMoreBirds = new System.Windows.Forms.CheckBox();
            this.numericUpDownNumberOfBirds = new System.Windows.Forms.NumericUpDown();
            this.buttonActive = new System.Windows.Forms.Button();
            this.groupBoxSorting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingHighLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingLowLimit)).BeginInit();
            this.groupBoxSaving.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationRange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingMinimumWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingFilter)).BeginInit();
            this.groupBoxDetails.SuspendLayout();
            this.groupBoxMoreBirds.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfBirds)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonDelete
            // 
            resources.ApplyResources(this.buttonDelete, "buttonDelete");
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonRename
            // 
            resources.ApplyResources(this.buttonRename, "buttonRename");
            this.buttonRename.Name = "buttonRename";
            this.buttonRename.UseVisualStyleBackColor = true;
            this.buttonRename.Click += new System.EventHandler(this.buttonRename_Click);
            // 
            // buttonNew
            // 
            resources.ApplyResources(this.buttonNew, "buttonNew");
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
            // 
            // listBoxFiles
            // 
            resources.ApplyResources(this.listBoxFiles, "listBoxFiles");
            this.listBoxFiles.FormattingEnabled = true;
            this.listBoxFiles.Name = "listBoxFiles";
            this.listBoxFiles.SelectedIndexChanged += new System.EventHandler(this.listBoxFiles_SelectedIndexChanged);
            this.listBoxFiles.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listBoxFiles_MouseDoubleClick);
            // 
            // textBoxNote
            // 
            resources.ApplyResources(this.textBoxNote, "textBoxNote");
            this.textBoxNote.Name = "textBoxNote";
            this.textBoxNote.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxNote_KeyDown);
            this.textBoxNote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxNote_KeyPress);
            this.textBoxNote.Leave += new System.EventHandler(this.textBoxNote_Leave);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // groupBoxSorting
            // 
            resources.ApplyResources(this.groupBoxSorting, "groupBoxSorting");
            this.groupBoxSorting.Controls.Add(this.numericUpDownSortingHighLimit);
            this.groupBoxSorting.Controls.Add(this.numericUpDownSortingLowLimit);
            this.groupBoxSorting.Controls.Add(this.labelHighLimitUnits);
            this.groupBoxSorting.Controls.Add(this.labelLowLimitUnits);
            this.groupBoxSorting.Controls.Add(this.labelHighLimit);
            this.groupBoxSorting.Controls.Add(this.comboBoxSortingMode);
            this.groupBoxSorting.Controls.Add(this.labelSortingMode);
            this.groupBoxSorting.Controls.Add(this.labelLowLimit);
            this.groupBoxSorting.Name = "groupBoxSorting";
            this.groupBoxSorting.TabStop = false;
            // 
            // numericUpDownSortingHighLimit
            // 
            resources.ApplyResources(this.numericUpDownSortingHighLimit, "numericUpDownSortingHighLimit");
            this.numericUpDownSortingHighLimit.Name = "numericUpDownSortingHighLimit";
            this.numericUpDownSortingHighLimit.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSortingLowLimit
            // 
            resources.ApplyResources(this.numericUpDownSortingLowLimit, "numericUpDownSortingLowLimit");
            this.numericUpDownSortingLowLimit.Name = "numericUpDownSortingLowLimit";
            this.numericUpDownSortingLowLimit.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelHighLimitUnits
            // 
            resources.ApplyResources(this.labelHighLimitUnits, "labelHighLimitUnits");
            this.labelHighLimitUnits.Name = "labelHighLimitUnits";
            // 
            // labelLowLimitUnits
            // 
            resources.ApplyResources(this.labelLowLimitUnits, "labelLowLimitUnits");
            this.labelLowLimitUnits.Name = "labelLowLimitUnits";
            // 
            // labelHighLimit
            // 
            resources.ApplyResources(this.labelHighLimit, "labelHighLimit");
            this.labelHighLimit.Name = "labelHighLimit";
            // 
            // comboBoxSortingMode
            // 
            resources.ApplyResources(this.comboBoxSortingMode, "comboBoxSortingMode");
            this.comboBoxSortingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSortingMode.FormattingEnabled = true;
            this.comboBoxSortingMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxSortingMode.Items"),
            resources.GetString("comboBoxSortingMode.Items1"),
            resources.GetString("comboBoxSortingMode.Items2")});
            this.comboBoxSortingMode.Name = "comboBoxSortingMode";
            this.comboBoxSortingMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelSortingMode
            // 
            resources.ApplyResources(this.labelSortingMode, "labelSortingMode");
            this.labelSortingMode.Name = "labelSortingMode";
            // 
            // labelLowLimit
            // 
            resources.ApplyResources(this.labelLowLimit, "labelLowLimit");
            this.labelLowLimit.Name = "labelLowLimit";
            // 
            // groupBoxSaving
            // 
            resources.ApplyResources(this.groupBoxSaving, "groupBoxSaving");
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingStabilizationRange);
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingStabilizationTime);
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingMinimumWeight);
            this.groupBoxSaving.Controls.Add(this.numericUpDownSavingFilter);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationRangeUnits);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationTimeSec);
            this.groupBoxSaving.Controls.Add(this.labelMinimumWeightUnits);
            this.groupBoxSaving.Controls.Add(this.labelFilterSec);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationRange);
            this.groupBoxSaving.Controls.Add(this.labelStabilizationTime);
            this.groupBoxSaving.Controls.Add(this.label6);
            this.groupBoxSaving.Controls.Add(this.comboBoxSavingMode);
            this.groupBoxSaving.Controls.Add(this.label7);
            this.groupBoxSaving.Controls.Add(this.label8);
            this.groupBoxSaving.Name = "groupBoxSaving";
            this.groupBoxSaving.TabStop = false;
            // 
            // numericUpDownSavingStabilizationRange
            // 
            resources.ApplyResources(this.numericUpDownSavingStabilizationRange, "numericUpDownSavingStabilizationRange");
            this.numericUpDownSavingStabilizationRange.DecimalPlaces = 1;
            this.numericUpDownSavingStabilizationRange.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.Maximum = new decimal(new int[] {
            990,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.Name = "numericUpDownSavingStabilizationRange";
            this.numericUpDownSavingStabilizationRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationRange.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSavingStabilizationTime
            // 
            resources.ApplyResources(this.numericUpDownSavingStabilizationTime, "numericUpDownSavingStabilizationTime");
            this.numericUpDownSavingStabilizationTime.DecimalPlaces = 1;
            this.numericUpDownSavingStabilizationTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationTime.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownSavingStabilizationTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationTime.Name = "numericUpDownSavingStabilizationTime";
            this.numericUpDownSavingStabilizationTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingStabilizationTime.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSavingMinimumWeight
            // 
            resources.ApplyResources(this.numericUpDownSavingMinimumWeight, "numericUpDownSavingMinimumWeight");
            this.numericUpDownSavingMinimumWeight.Name = "numericUpDownSavingMinimumWeight";
            this.numericUpDownSavingMinimumWeight.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownSavingFilter
            // 
            resources.ApplyResources(this.numericUpDownSavingFilter, "numericUpDownSavingFilter");
            this.numericUpDownSavingFilter.DecimalPlaces = 1;
            this.numericUpDownSavingFilter.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.Name = "numericUpDownSavingFilter";
            this.numericUpDownSavingFilter.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownSavingFilter.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // labelStabilizationRangeUnits
            // 
            resources.ApplyResources(this.labelStabilizationRangeUnits, "labelStabilizationRangeUnits");
            this.labelStabilizationRangeUnits.Name = "labelStabilizationRangeUnits";
            // 
            // labelStabilizationTimeSec
            // 
            resources.ApplyResources(this.labelStabilizationTimeSec, "labelStabilizationTimeSec");
            this.labelStabilizationTimeSec.Name = "labelStabilizationTimeSec";
            // 
            // labelMinimumWeightUnits
            // 
            resources.ApplyResources(this.labelMinimumWeightUnits, "labelMinimumWeightUnits");
            this.labelMinimumWeightUnits.Name = "labelMinimumWeightUnits";
            // 
            // labelFilterSec
            // 
            resources.ApplyResources(this.labelFilterSec, "labelFilterSec");
            this.labelFilterSec.Name = "labelFilterSec";
            // 
            // labelStabilizationRange
            // 
            resources.ApplyResources(this.labelStabilizationRange, "labelStabilizationRange");
            this.labelStabilizationRange.Name = "labelStabilizationRange";
            // 
            // labelStabilizationTime
            // 
            resources.ApplyResources(this.labelStabilizationTime, "labelStabilizationTime");
            this.labelStabilizationTime.Name = "labelStabilizationTime";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // comboBoxSavingMode
            // 
            resources.ApplyResources(this.comboBoxSavingMode, "comboBoxSavingMode");
            this.comboBoxSavingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSavingMode.FormattingEnabled = true;
            this.comboBoxSavingMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxSavingMode.Items"),
            resources.GetString("comboBoxSavingMode.Items1"),
            resources.GetString("comboBoxSavingMode.Items2")});
            this.comboBoxSavingMode.Name = "comboBoxSavingMode";
            this.comboBoxSavingMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // labelMoreBirds
            // 
            resources.ApplyResources(this.labelMoreBirds, "labelMoreBirds");
            this.labelMoreBirds.Name = "labelMoreBirds";
            // 
            // groupBoxDetails
            // 
            resources.ApplyResources(this.groupBoxDetails, "groupBoxDetails");
            this.groupBoxDetails.Controls.Add(this.groupBoxMoreBirds);
            this.groupBoxDetails.Controls.Add(this.label2);
            this.groupBoxDetails.Controls.Add(this.textBoxNote);
            this.groupBoxDetails.Controls.Add(this.groupBoxSorting);
            this.groupBoxDetails.Controls.Add(this.groupBoxSaving);
            this.groupBoxDetails.Name = "groupBoxDetails";
            this.groupBoxDetails.TabStop = false;
            // 
            // groupBoxMoreBirds
            // 
            resources.ApplyResources(this.groupBoxMoreBirds, "groupBoxMoreBirds");
            this.groupBoxMoreBirds.Controls.Add(this.checkBoxEnableMoreBirds);
            this.groupBoxMoreBirds.Controls.Add(this.numericUpDownNumberOfBirds);
            this.groupBoxMoreBirds.Controls.Add(this.labelMoreBirds);
            this.groupBoxMoreBirds.Name = "groupBoxMoreBirds";
            this.groupBoxMoreBirds.TabStop = false;
            // 
            // checkBoxEnableMoreBirds
            // 
            resources.ApplyResources(this.checkBoxEnableMoreBirds, "checkBoxEnableMoreBirds");
            this.checkBoxEnableMoreBirds.Name = "checkBoxEnableMoreBirds";
            this.checkBoxEnableMoreBirds.UseVisualStyleBackColor = true;
            this.checkBoxEnableMoreBirds.CheckedChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownNumberOfBirds
            // 
            resources.ApplyResources(this.numericUpDownNumberOfBirds, "numericUpDownNumberOfBirds");
            this.numericUpDownNumberOfBirds.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDownNumberOfBirds.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownNumberOfBirds.Name = "numericUpDownNumberOfBirds";
            this.numericUpDownNumberOfBirds.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownNumberOfBirds.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            this.numericUpDownNumberOfBirds.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.IntegerNumberKeyPress);
            // 
            // buttonActive
            // 
            resources.ApplyResources(this.buttonActive, "buttonActive");
            this.buttonActive.Name = "buttonActive";
            this.buttonActive.UseVisualStyleBackColor = true;
            this.buttonActive.Click += new System.EventHandler(this.buttonActive_Click);
            // 
            // UserControlScaleConfigFiles
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.buttonActive);
            this.Controls.Add(this.groupBoxDetails);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonRename);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.listBoxFiles);
            this.Name = "UserControlScaleConfigFiles";
            this.groupBoxSorting.ResumeLayout(false);
            this.groupBoxSorting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingHighLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSortingLowLimit)).EndInit();
            this.groupBoxSaving.ResumeLayout(false);
            this.groupBoxSaving.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationRange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingStabilizationTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingMinimumWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSavingFilter)).EndInit();
            this.groupBoxDetails.ResumeLayout(false);
            this.groupBoxDetails.PerformLayout();
            this.groupBoxMoreBirds.ResumeLayout(false);
            this.groupBoxMoreBirds.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownNumberOfBirds)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonRename;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.ListBox listBoxFiles;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxSorting;
        private System.Windows.Forms.ComboBox comboBoxSortingMode;
        private System.Windows.Forms.Label labelSortingMode;
        private System.Windows.Forms.Label labelLowLimit;
        private System.Windows.Forms.Label labelHighLimit;
        private System.Windows.Forms.GroupBox groupBoxSaving;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxSavingMode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelStabilizationRange;
        private System.Windows.Forms.Label labelStabilizationTime;
        private System.Windows.Forms.Label labelMoreBirds;
        private System.Windows.Forms.GroupBox groupBoxDetails;
        private System.Windows.Forms.Label labelHighLimitUnits;
        private System.Windows.Forms.Label labelLowLimitUnits;
        private System.Windows.Forms.Label labelStabilizationRangeUnits;
        private System.Windows.Forms.Label labelStabilizationTimeSec;
        private System.Windows.Forms.Label labelMinimumWeightUnits;
        private System.Windows.Forms.Label labelFilterSec;
        private System.Windows.Forms.NumericUpDown numericUpDownNumberOfBirds;
        private System.Windows.Forms.NumericUpDown numericUpDownSortingLowLimit;
        private System.Windows.Forms.NumericUpDown numericUpDownSortingHighLimit;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingFilter;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingMinimumWeight;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingStabilizationTime;
        private System.Windows.Forms.NumericUpDown numericUpDownSavingStabilizationRange;
        private System.Windows.Forms.GroupBox groupBoxMoreBirds;
        private System.Windows.Forms.CheckBox checkBoxEnableMoreBirds;
        private System.Windows.Forms.Button buttonActive;
    }
}
