﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlStatisticsTop : UserControl {
        public Button ButtonSelect { get { return buttonSelect; } }

        public ComboBox ComboBoxFlag { get { return comboBoxFlag; } }

        public DateTimePicker DateTimePickerFrom { get { return dateTimePickerFrom; } }
        public DateTimePicker DateTimePickerTo { get { return dateTimePickerTo; } }

        public bool IsTimeFilterVisible { get { return dateTimePickerFrom.Visible; } }

        public UserControlStatisticsTop() {
            InitializeComponent();

            // Default skryju casovy filtr i seznam flagu (seznam je prazdny)
            ShowTimeFilter(false);
            ShowFlagSelection(false);
        }

        private void TryToHide() {
            if (dateTimePickerFrom.Visible || comboBoxFlag.Visible || buttonSelect.Visible) {
                Visible = true;
            } else {
                Visible = false;
            }
        }

        private void ShowFlagSelection(bool visible) {
            if (visible) {
                Visible = true;
            }
            comboBoxFlag.Visible = visible;
        }

        public void ShowTimeFilter(bool visible) {
            if (visible) {
                Visible = true;
            }
            labelFrom.Visible          = visible;
            dateTimePickerFrom.Visible = visible;
            labelTo.Visible            = visible;
            dateTimePickerTo.Visible   = visible;
        }

        public void FillFlags(WeighingList weighingList) {
            // Naplnim seznam flagu
            comboBoxFlag.Items.Clear();
            comboBoxFlag.Items.Add(Properties.Resources.SEX_LIMIT + "..."); // Na prvni pozici dam popisku
            foreach (Flag flag in weighingList.FlagList) {
                comboBoxFlag.Items.Add(Sample.FlagToString(flag));
            }

            // Default zobrazim vsechny flagy
            comboBoxFlag.SelectedIndex = 0;    // Na prvni pozici je text "Sex/Limit..." (tj. Flag.ALL)
            
            // Pokud je v seznamu jen flag NONE (tj. flagy nepouziva), seznam skryju
            ShowFlagSelection(!weighingList.ContainsOnlyFlagNone());

            // Pokud je to mozne, skryju cely control
            TryToHide();
        }
    }
}
