﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// One group of files in the scale
    /// </summary>
    public class FileGroup : NameNote {

        /// <summary>
        /// Sorted list of file indexes (file list itself is defined elsewhere)
        /// </summary>
        public SortedIndexList FileIndexList { get { return fileIndexList; } }
        private SortedIndexList fileIndexList = new SortedIndexList();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="name">Name of the new group</param>
        public FileGroup(string name) : base (name, "") {
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="group">Source file group</param>
        public FileGroup(FileGroup group) : base(group.Name, group.Note) {
            fileIndexList.AddRange(group.FileIndexList);
        }

        /// <summary>
        /// Check if specified file already exists in the group
        /// </summary>
        /// <param name="index">File index</param>
        /// <returns>True if exists</returns>
        public bool FileExists(int index) {
            return fileIndexList.Contains(index);
        }
        
        /// <summary>
        /// Add file to the group
        /// </summary>
        /// <param name="index">File index</param>
        public void AddFile(int index) {
            fileIndexList.Add(index);
        }

        /// <summary>
        /// Delete file from the group
        /// </summary>
        /// <param name="index">File index</param>
        public void DeleteFile(int index) {
            fileIndexList.RemoveAndMove(index);
        }

        /// <summary>
        /// Replace file with another one
        /// </summary>
        /// <param name="oldIndex">Old file index</param>
        /// <param name="newIndex">New file index</param>
        public void ReplaceFile(int oldIndex, int newIndex) {
            fileIndexList.Replace(oldIndex, newIndex);
        }

        /// <summary>
        /// Make room for newly created file that isn't in the list so far
        /// </summary>
        /// <param name="index">File index</param>
        public void NewFileCreated(int index) {
            fileIndexList.MakeRoom(index);
        }

    }
}
