﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;

namespace Bat1 {
    public partial class UserControlScaleConfigPrinter : UserControlScaleConfigBase {

        public UserControlScaleConfigPrinter(ScaleConfig scaleConfig, bool readOnly) {
            InitializeComponent();

            // Preberu Read-only
            this.readOnly = readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
        }

        public override void Redraw() {
            isLoading = true;

            try {
                numericUpDownPaperWidth.Value = scaleConfig.Printer.PaperWidth;
                switch (scaleConfig.Printer.CommunicationSpeed) {
                    case 2400:
                        comboBoxSpeed.SelectedIndex = 0;
                        break;
                    case 4800:
                        comboBoxSpeed.SelectedIndex = 1;
                        break;
                    case 9600:
                        comboBoxSpeed.SelectedIndex = 2;
                        break;
                    case 19200:
                        comboBoxSpeed.SelectedIndex = 3;
                        break;
                    case 38400:
                        comboBoxSpeed.SelectedIndex = 4;
                        break;
                }
                comboBoxFormat.SelectedIndex = (int)scaleConfig.Printer.CommunicationFormat;
            } finally {
                isLoading = false;
            }
        }

        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }

            scaleConfig.Printer.PaperWidth = (int)numericUpDownPaperWidth.Value;
            switch (comboBoxSpeed.SelectedIndex) {
                case 0:
                    scaleConfig.Printer.CommunicationSpeed = 2400;
                    break;
                case 1:
                    scaleConfig.Printer.CommunicationSpeed = 4800;
                    break;
                case 2:
                    scaleConfig.Printer.CommunicationSpeed = 9600;
                    break;
                case 3:
                    scaleConfig.Printer.CommunicationSpeed = 19200;
                    break;
                case 4:
                    scaleConfig.Printer.CommunicationSpeed = 38400;
                    break;
            }
            scaleConfig.Printer.CommunicationFormat = (ComFormat)comboBoxFormat.SelectedIndex;
        }

        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e) {
            ControlsToConfig();
        }
        
    }
}
