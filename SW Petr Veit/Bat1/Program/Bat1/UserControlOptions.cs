﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlOptions : UserControl {
//        private bool isLoading = false;

        public UserControlOptions() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Vyplnim seznam podporovanych jazyku
            comboBoxLanguage.Items.AddRange(SwLanguageClass.GetLanguageStrings().ToArray());

            // Zobrazim aktualni nastaveni
//            isLoading = true;
            try {
                // Jazyk
                comboBoxLanguage.Text = SwLanguageClass.GetLanguageString(Program.Setup.Language);
                
                // Vaha verze 6
                comboBoxComPort.SelectedIndex =      Program.Setup.ScaleVersion6ComPort - 1;
                comboBoxUnits.SelectedIndex   = (int)Program.Setup.ScaleVersion6Units;

                // Format CSV
                RedrawCsvConfig();
            } finally {
//                isLoading = false;
            }
        }

        private void RedrawCsvConfig() {
            comboBoxCsvTemplate.SelectedIndex         = (int)Program.Setup.CsvConfig.Template;
            comboBoxCsvDelimiter.SelectedIndex        = (int)Program.Setup.CsvConfig.Delimiter;
            comboBoxCsvDecimalSeparator.SelectedIndex = (int)Program.Setup.CsvConfig.DecimalSeparator;
            comboBoxCsvEncoding.SelectedIndex         = (int)Program.Setup.CsvConfig.Encoding;
        }

        private void Save() {
            // Jazyk
            Program.Setup.Language = SwLanguageClass.GetLanguageCode(comboBoxLanguage.Text);
            
            // Vaha verze 6
            Program.Setup.ScaleVersion6ComPort = comboBoxComPort.SelectedIndex + 1;
            Program.Setup.ScaleVersion6Units   = (Units)comboBoxUnits.SelectedIndex;

            // Format CSV
            Program.Setup.CsvConfig.Template         = (CsvTemplate)        comboBoxCsvTemplate.SelectedIndex;
            Program.Setup.CsvConfig.Delimiter        = (CsvDelimiter)       comboBoxCsvDelimiter.SelectedIndex;
            Program.Setup.CsvConfig.DecimalSeparator = (CsvDecimalSeparator)comboBoxCsvDecimalSeparator.SelectedIndex;
            Program.Setup.CsvConfig.Encoding         = (CsvEncoding)        comboBoxCsvEncoding.SelectedIndex;

            Program.Database.SaveSetup();
        }
        
        private void comboBoxComPort_SelectionChangeCommitted(object sender, EventArgs e) {
            Save();
        }

        private void comboBoxCsvTemplate_SelectionChangeCommitted(object sender, EventArgs e) {
            // Nastavim vybranou sablonu
            switch ((CsvTemplate)comboBoxCsvTemplate.SelectedIndex) {
                case CsvTemplate.EXCEL:
                    Program.Setup.CsvConfig = Program.Setup.CsvConfigExcelTemplate;
                    break;

                case CsvTemplate.OPEN_OFFICE:
                    Program.Setup.CsvConfig = Program.Setup.CsvConfigOpenOfficeTemplate;
                    break;
            }

            // Prekreslim
            RedrawCsvConfig();

            // Ulozim setup
            Save();
        }

        private void comboBoxCsvDelimiter_SelectionChangeCommitted(object sender, EventArgs e) {
            // Pokud zmeni nejaky parametr CSV, zmenim sablonu na custom
            comboBoxCsvTemplate.SelectedIndex = (int)CsvTemplate.CUSTOM;

            // Ulozim setup
            Save();
        }

        private void comboBoxLanguage_SelectionChangeCommitted(object sender, EventArgs e) {
            // Ulozim setup
            Save();

            // Vyzvu uzivatele k restartu, za behu programu nelze jazyk prepnout
            MessageBox.Show(Properties.Resources.RESTART_SW, Program.ApplicationName);
         }
    }
}
