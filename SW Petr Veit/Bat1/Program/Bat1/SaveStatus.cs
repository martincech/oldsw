﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    /// <summary>
    /// Status of saving data read from a scale to the DB
    /// </summary>
    public static class SaveStatus {
        /// <summary>
        /// Data from scale saved or not
        /// </summary>
        public static bool Saved = true;

        /// <summary>
        /// Name of scale with unsaved data
        /// </summary>
        public static string ScaleName = "";

        /// <summary>
        /// Check if the data are saved. If not, ask user.
        /// </summary>
        /// <param name="windowTitle">Window title for the MessageBox</param>
        /// <returns>True if data are saved</returns>
        public static bool Check(string windowTitle) {
            if (Saved) {
                return true;        // Data jsou ulozena
            }
            
            // Data nejsou ulozena, zeptam se, zda chce pokracovat
            if (MessageBox.Show(String.Format(Properties.Resources.WEIGHINGS_NOT_SAVED, ScaleName), windowTitle, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return false;
            }
            return true;
        }
    }
}
