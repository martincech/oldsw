﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormLanguage : Form {
        public FormLanguage() {
            InitializeComponent();

            // Naplnim seznam jazyku
            listBoxLanguage.Items.AddRange(SwLanguageClass.GetLanguageStrings().ToArray());

            // Default nabidnu anglictinu
            listBoxLanguage.Text = SwLanguageClass.GetLanguageString(SwLanguage.ENGLISH);
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Ulozim vybrany jazyk
            Program.Setup.Language = SwLanguageClass.GetLanguageCode(listBoxLanguage.Text);
            Program.Database.SaveSetup();
            DialogResult = DialogResult.OK;
        }
    }
}
