﻿namespace Bat1 {
    partial class UserControlAbout {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelCompanyAddress = new System.Windows.Forms.Label();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.labelApplicationName = new System.Windows.Forms.Label();
            this.linkLabelMail = new System.Windows.Forms.LinkLabel();
            this.linkLabelWeb = new System.Windows.Forms.LinkLabel();
            this.labelVersion = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.labelCompanyAddress, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelCompanyName, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.pictureBoxLogo, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelApplicationName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.linkLabelMail, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.linkLabelWeb, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.labelVersion, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(386, 198);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // labelCompanyAddress
            // 
            this.labelCompanyAddress.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.labelCompanyAddress, 2);
            this.labelCompanyAddress.Location = new System.Drawing.Point(125, 46);
            this.labelCompanyAddress.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.labelCompanyAddress.Name = "labelCompanyAddress";
            this.labelCompanyAddress.Size = new System.Drawing.Size(119, 39);
            this.labelCompanyAddress.TabIndex = 6;
            this.labelCompanyAddress.Text = "tel: +420 545 235 252\r\ngsm: +420 602 754 178\r\nfax: +420 545 235 256";
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.labelCompanyName, 2);
            this.labelCompanyName.Location = new System.Drawing.Point(125, 25);
            this.labelCompanyName.Margin = new System.Windows.Forms.Padding(0, 12, 0, 0);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(86, 13);
            this.labelCompanyName.TabIndex = 2;
            this.labelCompanyName.Text = "VEIT Electronics";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Location = new System.Drawing.Point(1, 1);
            this.pictureBoxLogo.Margin = new System.Windows.Forms.Padding(1, 1, 24, 0);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBoxLogo, 5);
            this.pictureBoxLogo.Size = new System.Drawing.Size(100, 100);
            this.pictureBoxLogo.TabIndex = 0;
            this.pictureBoxLogo.TabStop = false;
            // 
            // labelApplicationName
            // 
            this.labelApplicationName.AutoSize = true;
            this.labelApplicationName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelApplicationName.Location = new System.Drawing.Point(125, 0);
            this.labelApplicationName.Margin = new System.Windows.Forms.Padding(0);
            this.labelApplicationName.Name = "labelApplicationName";
            this.labelApplicationName.Size = new System.Drawing.Size(38, 13);
            this.labelApplicationName.TabIndex = 0;
            this.labelApplicationName.Text = "BAT1";
            // 
            // linkLabelMail
            // 
            this.linkLabelMail.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.linkLabelMail, 2);
            this.linkLabelMail.Location = new System.Drawing.Point(125, 93);
            this.linkLabelMail.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.linkLabelMail.Name = "linkLabelMail";
            this.linkLabelMail.Size = new System.Drawing.Size(66, 13);
            this.linkLabelMail.TabIndex = 4;
            this.linkLabelMail.TabStop = true;
            this.linkLabelMail.Text = "veit@veit.cz";
            this.linkLabelMail.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelMail_LinkClicked);
            // 
            // linkLabelWeb
            // 
            this.linkLabelWeb.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.linkLabelWeb, 2);
            this.linkLabelWeb.Location = new System.Drawing.Point(125, 114);
            this.linkLabelWeb.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.linkLabelWeb.Name = "linkLabelWeb";
            this.linkLabelWeb.Size = new System.Drawing.Size(65, 13);
            this.linkLabelWeb.TabIndex = 5;
            this.linkLabelWeb.TabStop = true;
            this.linkLabelWeb.Text = "www.veit.cz";
            this.linkLabelWeb.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelWeb_LinkClicked);
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(171, 0);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelVersion.Size = new System.Drawing.Size(64, 13);
            this.labelVersion.TabIndex = 3;
            this.labelVersion.Text = "labelVersion";
            // 
            // UserControlAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserControlAbout";
            this.Size = new System.Drawing.Size(386, 198);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelCompanyAddress;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelApplicationName;
        private System.Windows.Forms.LinkLabel linkLabelMail;
        private System.Windows.Forms.LinkLabel linkLabelWeb;
        private System.Windows.Forms.Label labelVersion;
    }
}
