﻿namespace Bat1 {
    partial class UserControlManualResults {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlManualResults));
            this.tabControlResults = new System.Windows.Forms.TabControl();
            this.tabPageResults = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultNormal = new Bat1.UserControlWeighingResult();
            this.tabPageLight = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultLight = new Bat1.UserControlWeighingResult();
            this.tabPageOk = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultOk = new Bat1.UserControlWeighingResult();
            this.tabPageHeavy = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultHeavy = new Bat1.UserControlWeighingResult();
            this.tabPageMales = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultMales = new Bat1.UserControlWeighingResult();
            this.tabPageFemales = new System.Windows.Forms.TabPage();
            this.userControlWeighingResultFemales = new Bat1.UserControlWeighingResult();
            this.comboBoxFlag = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxNote = new System.Windows.Forms.TextBox();
            this.dateTimePickerStartTime = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.comboBoxFileName = new System.Windows.Forms.ComboBox();
            this.comboBoxScaleName = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.textBoxFileNote = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControlResults.SuspendLayout();
            this.tabPageResults.SuspendLayout();
            this.tabPageLight.SuspendLayout();
            this.tabPageOk.SuspendLayout();
            this.tabPageHeavy.SuspendLayout();
            this.tabPageMales.SuspendLayout();
            this.tabPageFemales.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlResults
            // 
            resources.ApplyResources(this.tabControlResults, "tabControlResults");
            this.tabControlResults.Controls.Add(this.tabPageResults);
            this.tabControlResults.Controls.Add(this.tabPageLight);
            this.tabControlResults.Controls.Add(this.tabPageOk);
            this.tabControlResults.Controls.Add(this.tabPageHeavy);
            this.tabControlResults.Controls.Add(this.tabPageMales);
            this.tabControlResults.Controls.Add(this.tabPageFemales);
            this.tabControlResults.Name = "tabControlResults";
            this.tabControlResults.SelectedIndex = 0;
            // 
            // tabPageResults
            // 
            resources.ApplyResources(this.tabPageResults, "tabPageResults");
            this.tabPageResults.Controls.Add(this.userControlWeighingResultNormal);
            this.tabPageResults.Name = "tabPageResults";
            this.tabPageResults.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultNormal
            // 
            resources.ApplyResources(this.userControlWeighingResultNormal, "userControlWeighingResultNormal");
            this.userControlWeighingResultNormal.Name = "userControlWeighingResultNormal";
            // 
            // tabPageLight
            // 
            resources.ApplyResources(this.tabPageLight, "tabPageLight");
            this.tabPageLight.Controls.Add(this.userControlWeighingResultLight);
            this.tabPageLight.Name = "tabPageLight";
            this.tabPageLight.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultLight
            // 
            resources.ApplyResources(this.userControlWeighingResultLight, "userControlWeighingResultLight");
            this.userControlWeighingResultLight.Name = "userControlWeighingResultLight";
            // 
            // tabPageOk
            // 
            resources.ApplyResources(this.tabPageOk, "tabPageOk");
            this.tabPageOk.Controls.Add(this.userControlWeighingResultOk);
            this.tabPageOk.Name = "tabPageOk";
            this.tabPageOk.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultOk
            // 
            resources.ApplyResources(this.userControlWeighingResultOk, "userControlWeighingResultOk");
            this.userControlWeighingResultOk.Name = "userControlWeighingResultOk";
            // 
            // tabPageHeavy
            // 
            resources.ApplyResources(this.tabPageHeavy, "tabPageHeavy");
            this.tabPageHeavy.Controls.Add(this.userControlWeighingResultHeavy);
            this.tabPageHeavy.Name = "tabPageHeavy";
            this.tabPageHeavy.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultHeavy
            // 
            resources.ApplyResources(this.userControlWeighingResultHeavy, "userControlWeighingResultHeavy");
            this.userControlWeighingResultHeavy.Name = "userControlWeighingResultHeavy";
            // 
            // tabPageMales
            // 
            resources.ApplyResources(this.tabPageMales, "tabPageMales");
            this.tabPageMales.Controls.Add(this.userControlWeighingResultMales);
            this.tabPageMales.Name = "tabPageMales";
            this.tabPageMales.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultMales
            // 
            resources.ApplyResources(this.userControlWeighingResultMales, "userControlWeighingResultMales");
            this.userControlWeighingResultMales.Name = "userControlWeighingResultMales";
            // 
            // tabPageFemales
            // 
            resources.ApplyResources(this.tabPageFemales, "tabPageFemales");
            this.tabPageFemales.Controls.Add(this.userControlWeighingResultFemales);
            this.tabPageFemales.Name = "tabPageFemales";
            this.tabPageFemales.UseVisualStyleBackColor = true;
            // 
            // userControlWeighingResultFemales
            // 
            resources.ApplyResources(this.userControlWeighingResultFemales, "userControlWeighingResultFemales");
            this.userControlWeighingResultFemales.Name = "userControlWeighingResultFemales";
            // 
            // comboBoxFlag
            // 
            resources.ApplyResources(this.comboBoxFlag, "comboBoxFlag");
            this.comboBoxFlag.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFlag.FormattingEnabled = true;
            this.comboBoxFlag.Items.AddRange(new object[] {
            resources.GetString("comboBoxFlag.Items"),
            resources.GetString("comboBoxFlag.Items1"),
            resources.GetString("comboBoxFlag.Items2"),
            resources.GetString("comboBoxFlag.Items3")});
            this.comboBoxFlag.Name = "comboBoxFlag";
            this.comboBoxFlag.SelectionChangeCommitted += new System.EventHandler(this.comboBoxFlag_SelectionChangeCommitted);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // textBoxNote
            // 
            resources.ApplyResources(this.textBoxNote, "textBoxNote");
            this.textBoxNote.Name = "textBoxNote";
            // 
            // dateTimePickerStartTime
            // 
            resources.ApplyResources(this.dateTimePickerStartTime, "dateTimePickerStartTime");
            this.dateTimePickerStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePickerStartTime.Name = "dateTimePickerStartTime";
            this.dateTimePickerStartTime.ShowUpDown = true;
            // 
            // dateTimePickerStartDate
            // 
            resources.ApplyResources(this.dateTimePickerStartDate, "dateTimePickerStartDate");
            this.dateTimePickerStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerStartDate.Name = "dateTimePickerStartDate";
            // 
            // comboBoxFileName
            // 
            resources.ApplyResources(this.comboBoxFileName, "comboBoxFileName");
            this.comboBoxFileName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxFileName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxFileName.DropDownHeight = 300;
            this.comboBoxFileName.FormattingEnabled = true;
            this.comboBoxFileName.Name = "comboBoxFileName";
            this.comboBoxFileName.TextChanged += new System.EventHandler(this.comboBoxFileName_TextChanged);
            this.comboBoxFileName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxScaleName_KeyPress);
            // 
            // comboBoxScaleName
            // 
            resources.ApplyResources(this.comboBoxScaleName, "comboBoxScaleName");
            this.comboBoxScaleName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBoxScaleName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBoxScaleName.DropDownHeight = 300;
            this.comboBoxScaleName.FormattingEnabled = true;
            this.comboBoxScaleName.Name = "comboBoxScaleName";
            this.comboBoxScaleName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxScaleName_KeyPress);
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // comboBoxUnits
            // 
            resources.ApplyResources(this.comboBoxUnits, "comboBoxUnits");
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.FormattingEnabled = true;
            this.comboBoxUnits.Items.AddRange(new object[] {
            resources.GetString("comboBoxUnits.Items"),
            resources.GetString("comboBoxUnits.Items1"),
            resources.GetString("comboBoxUnits.Items2")});
            this.comboBoxUnits.Name = "comboBoxUnits";
            // 
            // textBoxFileNote
            // 
            resources.ApplyResources(this.textBoxFileNote, "textBoxFileNote");
            this.textBoxFileNote.Name = "textBoxFileNote";
            this.textBoxFileNote.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxScaleName_KeyPress);
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // UserControlManualResults
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControlResults);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxFlag);
            this.Controls.Add(this.textBoxFileNote);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxUnits);
            this.Controls.Add(this.dateTimePickerStartDate);
            this.Controls.Add(this.dateTimePickerStartTime);
            this.Controls.Add(this.comboBoxFileName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.comboBoxScaleName);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxNote);
            this.Name = "UserControlManualResults";
            this.tabControlResults.ResumeLayout(false);
            this.tabPageResults.ResumeLayout(false);
            this.tabPageLight.ResumeLayout(false);
            this.tabPageOk.ResumeLayout(false);
            this.tabPageHeavy.ResumeLayout(false);
            this.tabPageMales.ResumeLayout(false);
            this.tabPageFemales.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlResults;
        private System.Windows.Forms.TabPage tabPageResults;
        private UserControlWeighingResult userControlWeighingResultNormal;
        private System.Windows.Forms.TabPage tabPageFemales;
        private UserControlWeighingResult userControlWeighingResultFemales;
        private System.Windows.Forms.TabPage tabPageMales;
        private UserControlWeighingResult userControlWeighingResultMales;
        private System.Windows.Forms.TabPage tabPageLight;
        private UserControlWeighingResult userControlWeighingResultLight;
        private System.Windows.Forms.TabPage tabPageOk;
        private UserControlWeighingResult userControlWeighingResultOk;
        private System.Windows.Forms.TabPage tabPageHeavy;
        private UserControlWeighingResult userControlWeighingResultHeavy;
        private System.Windows.Forms.ComboBox comboBoxFlag;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxNote;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartTime;
        private System.Windows.Forms.DateTimePicker dateTimePickerStartDate;
        private System.Windows.Forms.ComboBox comboBoxFileName;
        private System.Windows.Forms.ComboBox comboBoxScaleName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxFileNote;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
    }
}
