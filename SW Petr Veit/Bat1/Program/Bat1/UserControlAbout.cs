﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;           // Spusteni prohlizece

namespace Bat1 {
    public partial class UserControlAbout : UserControl {
        public UserControlAbout() {
            InitializeComponent();

            if (LicenseManager.UsageMode == LicenseUsageMode.Designtime) {
                return;         // V designeru z databaze nenahravam
            }

            // Zobrazim jmeno aplikace a verzi
            labelApplicationName.Text = Program.ApplicationName;
            labelVersion.Text         = SwVersion.ToString();

            // Zobrazim informace o firme
            labelCompanyName.Text    = AppConfig.CompanyName;
            labelCompanyAddress.Text = AppConfig.CompanyAddress;

            // Zobrazim mail a web
            linkLabelMail.Text = AppConfig.CompanyMail;
            linkLabelWeb.Text  = AppConfig.CompanyWeb;

            // Nahraju logo
            try {
                pictureBoxLogo.Load("Logo.png");
            } catch {
                // Soubor neexistuje, nic se nedeje
            }
        }

        private void linkLabelMail_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            // Otevru mailoveho klienta
            System.Diagnostics.Process.Start("mailto:" + AppConfig.CompanyMail + "?subject=" + AppConfig.CompanyMailSubject + " " + SwVersion.ToString());        
        }

        private void linkLabelWeb_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            // Spustim webovy prohlizec
            ProcessStartInfo startInfo = new ProcessStartInfo(AppConfig.CompanyWebAddress);
            try {
                // Pomoci try-catch osetrim pripad, kdy spusteni nepovoli napr. firewall
                Process.Start(startInfo);
            } catch {
            }
        }
    }
}
