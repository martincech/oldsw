﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormExportFormat : Form {
        /// <summary>
        /// Selected export format
        /// </summary>
        public ExportFormat ExportFormat { get { return exportFormat; } }
        private ExportFormat exportFormat;
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="enableBat1Format">Enable export to Bat1 format</param>
        public FormExportFormat(bool enableBat1Format) {
            InitializeComponent();

            if (enableBat1Format) {
                // Nastavim text u formatu BAT1 (podle nazvu vahy)
                radioButtonBat1.Text = String.Format(Properties.Resources.FILE_BAT1_EXPORT, Program.ApplicationName);

                // Nastavim naposledy pouzity format exportu
                if (Program.Setup.ExportFormat == ExportFormat.BAT1) {
                    radioButtonBat1.Checked = true;
                } else {
                    radioButtonCsv.Checked  = true;
                }
            } else {
                // Chce povolit jen export do CSV, export do Bat1 je zakazany (pri exportu statistiky hejn)
                radioButtonCsv.Checked  = true;
                radioButtonBat1.Enabled = false;
            }
        }

        private void buttonOk_Click(object sender, EventArgs e) {
            // Ulozim pouzity format exportu
            if (radioButtonBat1.Checked) {
                exportFormat = ExportFormat.BAT1;
            } else {
                exportFormat = ExportFormat.CSV;
            }

            // Ulozim naposledy pouzity export do databaze
            Program.Setup.ExportFormat = exportFormat;
            Program.Database.SaveSetup();

            DialogResult = DialogResult.OK;
        }
    }
}
