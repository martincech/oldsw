﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// List of file groups defined in the scale
    /// </summary>
    public class FileGroupList {
        /// <summary>
        /// List of file groups sorted by name
        /// </summary>
        public List<FileGroup> List { get { return list; } }
        private List<FileGroup> list = new List<FileGroup>();

        /// <summary>
        /// Constructor
        /// </summary>
        public FileGroupList() {
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="groupList">Source group list</param>
        public FileGroupList(FileGroupList groupList) {
            foreach (FileGroup group in groupList.List) {
                FileGroup fileGroup = new FileGroup(group);
                list.Add(fileGroup);
            }
        }
        
        /// <summary>
        /// Sort the list by name
        /// </summary>
        private void Sort() {
            list.Sort(FileGroup.CompareByName);
        }
        
        /// <summary>
        /// Check if specified group exists in the list
        /// </summary>
        /// <param name="name">Group name</param>
        /// <returns>True if exists</returns>
        public bool Exists(string name) {
            return list.Exists(delegate(FileGroup group) { return group.Name == name; });
        }

        /// <summary>
        /// Add new group to the list
        /// </summary>
        /// <param name="name">Group name</param>
        /// <returns>True if successful</returns>
        public bool Add(string name) {
            return Add(new FileGroup(name));
        }

        /// <summary>
        /// Add new group to the list
        /// </summary>
        /// <param name="name">Group name</param>
        /// <param name="note">Group note</param>
        /// <returns>True if successful</returns>
        public bool Add(string name, string note) {
            FileGroup group = new FileGroup(name);
            group.SetNote(note);
            return Add(group);
        }

        /// <summary>
        /// Add group to the list
        /// </summary>
        /// <param name="name">Group to add</param>
        /// <returns>True if successful</returns>
        public bool Add(FileGroup group) {
            if (Exists(group.Name)) {
                return false;       // Skupina s timto nazvem uz v seznamu je
            }

            // Pridam skupinu do seznamu a setridim podle jmena
            list.Add(group);
            Sort();
            return true;
        }

        /// <summary>
        /// Delete group from the list
        /// </summary>
        /// <param name="index">Group index</param>
        public void Delete(int index) {
            list.RemoveAt(index);
        }

        /// <summary>
        /// Rename group
        /// </summary>
        /// <param name="index">Group index</param>
        /// <param name="newName">New name</param>
        /// <returns>True if successful</returns>
        public bool Rename(int index, string newName) {
            // Zkontroluju platnost jmena
            if (!CheckValue.CheckScaleName(newName)) {
                return false;
            }
            
            // Zkontroluju, zda vubec je treba zmena
            if (list[index].Name == newName) {
                return true;        // Neni treba prejmenovavat, vratim true jako bych prejmenoval
            }
            
            // Zkontroluju, zda uz jmeno v seznamu neni na jine pozici
            if (Exists(newName)) {
                return false;       // Skupina se zadanym jmenem uz existuje
            }
            
            // Zmenim jmeno a znovu setridim
            list[index].Rename(newName);
            Sort();

            return true;
        }

        /// <summary>
        /// Say to all groups in the list that a new file has been created
        /// </summary>
        /// <param name="fileIndex">File index</param>
        public void CreateNewFile(int fileIndex) {
            foreach (FileGroup fileGroup in list) {
                fileGroup.NewFileCreated(fileIndex);
            }
        }
        
        /// <summary>
        /// Replace file with another one
        /// </summary>
        /// <param name="oldIndex">Old file index</param>
        /// <param name="newIndex">New file index</param>
        public void RenameFile(int oldIndex, int newIndex) {
            foreach (FileGroup fileGroup in list) {
                fileGroup.FileIndexList.Replace(oldIndex, newIndex);
            }
        }

        /// <summary>
        /// Delete file from all defined groups
        /// </summary>
        /// <param name="fileIndex">File index to delete</param>
        public void DeleteFile(int fileIndex) {
            foreach (FileGroup fileGroup in list) {
                fileGroup.DeleteFile(fileIndex);
            }
        }

        /// <summary>
        /// Find group index
        /// </summary>
        /// <param name="name">Group name</param>
        /// <returns>Index in the list</returns>
        public int GetIndex(string name) {
            for (int i = 0; i < list.Count; i++) {
                if (list[i].Name == name) {
                    return i;
                }
            }

            // Nenasel jsem
            return -1;
        }
    
    }
}
