﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class UserControlScaleSetup : UserControl {
        /// <summary>
        /// Name of currently open predefined config
        /// </summary>
        private string openConfigName = "";
        
        /// <summary>
        /// Waiting form
        /// </summary>
        private FormScaleWait formScaleWait;

        /// <summary>
        /// User control with scale config
        /// </summary>
        private UserControlScaleConfig userControlScaleConfig;

        /// <summary>
        /// Title of the parent form
        /// </summary>
        private readonly string formTitle;

        /// <summary>
        /// Constructor
        /// </summary>
        public UserControlScaleSetup(string formTitle) {
            InitializeComponent();

            // Vytvorim user control s configem
            userControlScaleConfig = new UserControlScaleConfig(false);
            userControlScaleConfig.Location = new Point(0, 0);
            userControlScaleConfig.Size     = new Size(699, 367);
            userControlScaleConfig.TabIndex = 0;
            userControlScaleConfig.Dock     = DockStyle.Fill;
            Controls.Add(userControlScaleConfig);

            // Ulozim si nazev okna (nazev ojan budu nasledne menit podle otevreneho configu)
            this.formTitle = formTitle;
        }

        /// <summary>
        /// Sets name of openned config
        /// </summary>
        /// <param name="configName">New name</param>
        private void SetOpenConfigName(string configName) {
            openConfigName = configName;
            if (configName == "") {
                ParentForm.Text = formTitle;
            } else {
                ParentForm.Text = formTitle + " (" + configName + ")";
            }
        }

        public void SetScaleDefaultConfig() {
            // Nastaveni vah
            ScaleConfig config = new ScaleConfig(Bat1Version7.DefaultConfig);
            
            // Soubor FILE000 po volbe Restore factory defaults, zaroven ho nastavim jako aktivni
            config.FileList.Add("FILE000", "", config);     
            config.ActiveFileIndex = 0;


            // !!! test
/*            config.FileList.Add("A", "", config);
            config.FileList.Add("B", "", config);
            config.FileList.Add("C", "", config);
            config.FileList.Add("D", "", config);
            config.FileList.Add("E", "", config);
            config.FileList.Add("F", "", config);
            config.FileList.Add("G", "", config);
            config.FileList.Add("H", "", config);
            config.FileList.Add("I", "", config);
            config.FileList.Add("J", "", config);
            config.FileList.Add("K", "", config);
            config.FileList.Add("L", "", config);
            config.FileList.Add("M", "", config);
            config.FileList.Add("N", "", config);
            config.FileList.Add("O", "", config);
            config.FileList.Add("P", "", config);
            config.FileList.Add("Q", "", config);
            config.FileList.Add("R", "", config);
            config.FileList.Add("S", "", config);
            config.FileList.Add("T", "", config);
            config.FileList.Add("U", "", config);
            config.FileList.Add("V", "", config);
            config.FileList.Add("W", "", config);
            config.FileList.Add("X", "", config);
            config.FileList.Add("Y", "", config);
            config.FileList.Add("Z", "", config);

            // !!! test
            string name = "AAAAAAAAAAAAAAA";
            for (int i = 0; i < 26; i++) {
                config.FileGroupList.Add(name, "POZNAMKA");

                // Oznacim soubory
                for (int j = 0; j <= i; j++) {
                    config.FileGroupList.List[i].FileIndexList.Add(j);
                }
                
                // Zmenim jmeno
                char[] array = name.ToCharArray();
                for (int j = 0; j < array.Length; j++) {
                    array[j]++;
                }
                name = new string(array);
            }*/
            
            userControlScaleConfig.SetScaleConfig(config);
        }

        /// <summary>
        /// Load config from the scale
        /// </summary>
        /// <param name="bat1">Bat1Version7 instance to read to</param>
        /// <returns>True if successful</returns>
        public static bool LoadConfig(out Bat1Version7 bat1) {
            try {
                bat1 = new Bat1Version7();
            } catch {
                // Driver vahy neni nainstalovan
                Cursor.Current = Cursors.Default;
                MessageBox.Show(Properties.Resources.DRIVER_NOT_INSTALLED, Program.ApplicationName);
                bat1 = null;
                return false;
            }
            
            // Nactu nastaveni z vahy
            if (!bat1.LoadConfig()) {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                return false;
            }

            // Zkontroluju, zda vaha neni novejsi nez SW
            if (!bat1.IsSupported()) {
                MessageBox.Show(Properties.Resources.SCALE_NOT_SUPPORTED, Program.ApplicationName);
                return false;
            }

            return true;
        }

        private void ShowWaitingForm() {
            formScaleWait = new FormScaleWait(ParentForm);
            formScaleWait.Show();
            formScaleWait.Refresh();
        }
        
        private bool LoadScaleConfig(out ScaleConfig scaleConfig) {
            Cursor.Current = Cursors.WaitCursor;
            ShowWaitingForm();

            scaleConfig = null;
            try {
                Bat1Version7 bat1;

                if (!LoadConfig(out bat1)) {
                    return false;
                }

                scaleConfig = Bat1Version7.ReadGlobalConfig();

                return true;
            } finally {
                formScaleWait.Close();
                Cursor.Current = Cursors.Default;
            }
        }
        
        
        /// <summary>
        /// Save all data to the scale
        /// </summary>
        /// <param name="bat1">Bat1 to use</param>
        /// <returns>True if successful</returns>
        private bool SaveDevice(Bat1Version7 bat1) {
            // Ulozim do vahy, pokud se delaly zmeny v souborech a skupinach, smazou se vsechny vzorky ve vaze
            if (!bat1.SaveDevice()) {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                return false;
            }

            return true;
        }

        private bool IsChangesSaved() {
            switch (MessageBox.Show(Properties.Resources.SAVE_CHANGES_QUESTION, Program.ApplicationName, MessageBoxButtons.YesNoCancel)) {
                case DialogResult.Yes:
                    buttonSaveTemplate_Click(null, null);
                    return true;

                case DialogResult.No:
                    return true;

                default:
                    return false;
            }
        }

        private void buttonLoadFromScale_Click(object sender, EventArgs e) {
            // Zeptam se na ulozeni zmen
            if (!IsChangesSaved()) {
                return;
            }

            ScaleConfig scaleConfig;
            if (!LoadScaleConfig(out scaleConfig)) {
                return;
            }
            userControlScaleConfig.SetScaleConfig(scaleConfig);
            SetOpenConfigName("");
        }

        private void buttonSaveToScale_Click(object sender, EventArgs e) {
            // Zapsanim configu se smazou vsechny vzorky. Zeptam se, zda chce pkracovat
            if (MessageBox.Show(Properties.Resources.ALL_DATA_WILL_BE_DELETED, buttonSaveToScale.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }
            
            Cursor.Current = Cursors.WaitCursor;
            Refresh();
            ShowWaitingForm();

            try {
                // Nactu aktualni nastaveni z vahy
                Bat1Version7 bat1;
                if (!LoadConfig(out bat1)) {
                    return;
                }

                // Zmenim v nastaveni veskera data (ponecham cislo verze atd)
                bat1.WriteConfig(userControlScaleConfig.ScaleConfig);

                // Ulozim zpet do vahy
                SaveDevice(bat1);
            } finally {
                formScaleWait.Close();
                Cursor.Current = Cursors.Default;
            }
        }

        private void buttonNew_Click(object sender, EventArgs e) {
            // Zeptam se na ulozeni zmen
            if (!IsChangesSaved()) {
                return;
            }
            
            // Udelam kopii default nastaveni
            SetScaleDefaultConfig();
            SetOpenConfigName("");
        }

        private void buttonSaveTemplate_Click(object sender, EventArgs e) {
            // Zeptam se na jmeno
            FormName form = new FormName(buttonSaveTemplate.Text, openConfigName, false);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }
            string name = form.EnteredName;

            // Zjistim, zda uz jmeno neexistuje
            if (Program.Database.ScaleConfigExists(name)) {
                if (MessageBox.Show(String.Format(Properties.Resources.TEMPLATE_ALREADY_EXISTS, name), buttonSaveTemplate.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                    return;         // Nechce prepsat
                }

                // Smazu existujici sablonu
                Program.Database.DeleteScaleConfig(name);
            }

            // Ulozim do DB
            Program.Database.SaveScaleConfig(userControlScaleConfig.ScaleConfig, name);
            
            // Zapamatuju si jmeno
            SetOpenConfigName(form.EnteredName);
        }

        private void buttonOpenTemplate_Click(object sender, EventArgs e) {
            // Zeptam se na ulozeni zmen
            if (!IsChangesSaved()) {
                return;
            }

            // Zeptam se na config, ktery chce otevrit
            FormOpenScaleConfig form = new FormOpenScaleConfig();
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Otevru vybrany config
            userControlScaleConfig.SetScaleConfig(Program.Database.LoadScaleConfig(Program.Database.GetPredefinedScaleConfigId(form.SelectedConfigName)));

            // Zapamatuju si jmeno otevreneho configu
            SetOpenConfigName(form.SelectedConfigName);
        }

/*        private void scaleNameToolStripMenuItem_Click(object sender, EventArgs e) {
            // Nactu stavajici jmeno
            ScaleConfig scaleConfig;
            if (!LoadScaleConfig(out scaleConfig)) {
                return;
            }

            // Necham uzivatele jmeno editovat
            FormName form = new FormName(scaleNameToolStripMenuItem.Text, scaleConfig.ScaleName, true);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Ulozim nove jmeno
            Bat1Version7 bat1 = new Bat1Version7();
            Cursor.Current = Cursors.WaitCursor;
            Refresh();
            try {
                if (!bat1.SetName(form.EnteredName)) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                    return;
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }*/

/*        private void dateAndTimeToolStripMenuItem_Click(object sender, EventArgs e) {
            if (MessageBox.Show(Properties.Resources.SET_DATE_TIME, dateAndTimeToolStripMenuItem.Text, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }
            
            Bat1Version7 bat1 = new Bat1Version7();
            Cursor.Current = Cursors.WaitCursor;
            Refresh();
            try {
                if (!bat1.SetDateTime()) {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show(Properties.Resources.SCALE_NOT_CONNECTED, Program.ApplicationName);
                    return;
                }
            } finally {
                Cursor.Current = Cursors.Default;
            }
        }*/

/*        private void loadFromWeighingToolStripMenuItem_Click(object sender, EventArgs e) {
            // Necham uzivatele vybrat vazeni, ze ktereho vezmu config
            FormWeighingManager form = new FormWeighingManager(true);   // Selection mode
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            // Config muzu vzit pouze z vazeni z nove vahy (ze stare vahy ani z rucne zadanych vysledku ne)
            if (form.SelectedWeighing.WeighingData.ResultType != ResultType.NEW_SCALE) {
                MessageBox.Show(Properties.Resources.SCALE_CONFIG_NOT_VALID, Program.ApplicationName);
                return;
            }

            // Preberu config z vybraneho vazeni
            userControlScaleConfig.SetScaleConfig(form.SelectedWeighing.WeighingData.ScaleConfig);
        }*/

        private void buttonImport_Click(object sender, EventArgs e) {
            // Zeptam se na ulozeni zmen
            if (!IsChangesSaved()) {
                return;
            }

            // Importuju
            ScaleConfig scaleConfig = new Export(this).ImportScaleConfig();
            if (scaleConfig == null) {
                return;
            }

            // Nastavim jako aktivni
            userControlScaleConfig.SetScaleConfig(scaleConfig);
            SetOpenConfigName("");
        }

        private void buttonExport_Click(object sender, EventArgs e) {
            // Exportuju, pokud ma otevreny preddefinovany config, ulozim i jeho nazev
            new Export(this).ExportScaleConfig(userControlScaleConfig.ScaleConfig, openConfigName);
        }


    }
}
