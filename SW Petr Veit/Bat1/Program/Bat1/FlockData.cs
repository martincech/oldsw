﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bat1 {
    /// <summary>
    /// Additional flock data in one weighing
    /// </summary>
    public struct FlockData {
        /// <summary>
        /// Day number
        /// </summary>
        public int Day;

        /// <summary>
        /// Target weight
        /// </summary>
        public float Target;

        /// <summary>
        /// Defference between Average and Target
        /// </summary>
        public float Difference;

        /// <summary>
        /// Number of birds above the target weight
        /// </summary>
        public int CountAbove;

        /// <summary>
        /// Number of birds below the target weight
        /// </summary>
        public int CountBelow;

        /// <summary>
        /// Weight gain between two weighings
        /// </summary>
        public float Gain;
    }
}
