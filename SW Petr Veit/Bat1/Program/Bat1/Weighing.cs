﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Scale;

namespace Bat1 {
    /// <summary>
    /// Source data and statistic results of one weighing
    /// </summary>
    public class Weighing {
        /// <summary>
        /// Source data
        /// </summary>
        public  WeighingData WeighingData { get { return weighingData; } }
        private WeighingData weighingData;

        /// <summary>
        /// Statistic results
        /// </summary>
        public  WeighingResults WeighingResults { get { return weighingResults; } }
        private WeighingResults weighingResults;

        /// <summary>
        /// Additional flock data used in statistics (if the weighing belongs to a flock)
        /// </summary>
        public FlockData FlockData;

        /// <summary>
        /// Constructor for manually entered results
        /// </summary>
        /// <param name="weighingData">Source weighing data</param>
        public Weighing(WeighingData weighingData, WeighingResults weighingResults) {
            this.weighingData    = weighingData;
            this.weighingResults = weighingResults;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="weighingData">Source weighing data</param>
        public Weighing(WeighingData weighingData) {
            this.weighingData = weighingData;
            RecalculateResults();
        }

        /// <summary>
        /// Get date and time of weighing start
        /// </summary>
        /// <returns></returns>
        public DateTime GetMinDateTime() {
            if (weighingData.ResultType == ResultType.MANUAL){
                // U rucne zadaneho vazeni vezmu datum startu z vysledku
                return GetMinDateTime(Flag.ALL);
            }

            // U vazeni z vahy beru start ze vzorku - napr. pri editaci vzorku behem importu musim brat v uvahu pouze
            // vzorky, ne vysledky. Pokud napr. umazu nejaky vzorek, behem editace se upravi jen seznam vzorku, vysledky
            // se neprepocitavaji.
            return weighingData.SampleList.MinDateTime;
        }

        public DateTime GetMinDateTime(Flag flag) {
            StatisticResult result = WeighingResults.GetResult(flag);
            if (result == null) {
                return DateTime.MinValue;
            }
            return result.WeighingStarted;
        }

        public DateTime GetMaxDateTime(Flag flag) {
            if (WeighingData.ResultType == ResultType.MANUAL) {
                // U rucne zadanych vysledku beru ze vazil 1 hodinu
                return GetMinDateTime(flag).AddHours(1);
            } else {
                return WeighingData.SampleList.MaxDateTime;
            }
        }

        /// <summary>
        /// Recalculate results for this weighing
        /// </summary>
        public void RecalculateResults() {
            weighingResults = new WeighingResults(weighingData.SampleList,
                                                  weighingData.ScaleConfig.StatisticConfig.UniformityRange,
                                                  weighingData.ScaleConfig.StatisticConfig.Histogram,
                                                  weighingData.ScaleConfig.Units.Units);
        }

        /// <summary>
        /// Recalculate weighing to new units
        /// </summary>
        /// <param name="units">New units</param>
        public void ChangeUnits(Units units) {
            // Pokud chce jednotky, ve kterych vazeni uz je, neni treba prepocet
            if (weighingData.ScaleConfig.Units.Units == units) {
                return;     
            }

            if (weighingData.ResultType == ResultType.MANUAL) {
                // U rucne zadanych vysledku prepocitam rovnou vysledek
                weighingResults.ChangeUnits(weighingData.ScaleConfig.Units.Units, units);
                weighingData.ScaleConfig.SetNewUnits(units);    // Config vahy
            } else {
                // U vysledku stazenych z vah zmenim vzorky a vysledky vypoctu znovu
                weighingData.SampleList.Convert(weighingData.ScaleConfig.Units.Units, units);
                
                // Config vahy musim zmenit pred prepoctem vysledku
                weighingData.ScaleConfig.SetNewUnits(units);

                // Prepocitam vysledky
                RecalculateResults();
            }
        }

        /// <summary>
        /// Compare function for List(Weighing).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByTime(Weighing x, Weighing y) {
            DateTime xDateTime = x.GetMinDateTime();
            DateTime yDateTime = y.GetMinDateTime();
            if (xDateTime < yDateTime) {
                return -1;
            }
            if (xDateTime > yDateTime) {
                return 1;
            }
            
            // Datumy jsou shodne, porovnam soubory
            int result = x.weighingData.File.Name.CompareTo(y.weighingData.File.Name);
            if (result < 0) {
                return -1;
            }
            if (result > 0) {
                return 1;
            }
            
            // Soubory jsou taky shodne, porovnam jeste vahy
            result = x.weighingData.ScaleConfig.ScaleName.CompareTo(y.weighingData.ScaleConfig.ScaleName);
            if (result < 0) {
                return -1;
            }
            if (result > 0) {
                return 1;
            }

            // Vse je shodne
            return 0;
        }

        /// <summary>
        /// Compare function for List(Weighing).Sort()
        /// </summary>
        /// <param name="x">First parameter</param>
        /// <param name="y">Second parameter</param>
        /// <returns>1, -1 or 0</returns>
        public static int CompareByFlockDay(Weighing x, Weighing y) {
            int xDay = x.FlockData.Day;
            int yDay = y.FlockData.Day;
            if (xDay < yDay) {
                return -1;
            }
            if (xDay > yDay) {
                return 1;
            }
            
            // Dny jsou shodne, porovnam soubory podle data a casu
            return CompareByTime(x, y);
        }

    }
}
