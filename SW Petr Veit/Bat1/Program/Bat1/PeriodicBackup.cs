﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Bat1 {

    /// <summary>
    /// Information about one periodic backup file
    /// </summary>
    public struct PeriodicBackupFile {
        /// <summary>
        /// File name
        /// </summary>
        public string Name;

        /// <summary>
        /// Date and time when the backup was created
        /// </summary>
        public DateTime Created;
    }
    
    /// <summary>
    /// Periodic database backup
    /// </summary>
    static class PeriodicBackup {
        /// <summary>
        /// Name of the direcotry for periodic backup
        /// </summary>
        const string DIRECTORY = "PeriodicBackup";

        /// <summary>
        /// Period of periodical backups in days
        /// </summary>
        const int BACKUP_PERIOD = 7;

        /// <summary>
        /// Maximum number of periodical backups
        /// </summary>
        const int MAX_BACKUP_COUNT = 12;

        /// <summary>
        /// Full backup directory path
        /// </summary>
        private static string BackupDirectory {
            get { return Program.DataFolder + @"\" + DIRECTORY; }
        }

        /// <summary>
        /// File name prefix "Bat1 "
        /// </summary>
        private static string Prefix {
            get { return Path.GetFileNameWithoutExtension(Program.DatabaseFileName) + " "; }
        }

        /// <summary>
        /// Create backup file name
        /// </summary>
        /// <param name="dateTime">Date of the backup</param>
        /// <returns>File name</returns>
        private static string CreateFileName(DateTime dateTime) {
            return Prefix + dateTime.ToString("yyyy-MM-dd HH-mm-ss") + "." + FileExtension.BACKUP;
        }

        /// <summary>
        /// Check if the filename is valid
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>True if the name is valid</returns>
        private static bool IsNameValid(string fileName) {
            if (Path.GetExtension(fileName) != "." + FileExtension.BACKUP) {
                return false;
            }
            
            // Zkontroluju predponu "Bat1 "
            fileName = Path.GetFileNameWithoutExtension(fileName);      // Umazu cestu a priponu
            if (!fileName.StartsWith(Prefix)) {
                return false;
            }

            // Zkontroluju delku nazvu
            if (fileName.Length != Prefix.Length + 19) {
                return false;
            }

            // Zkusim dekodovat datum
            try {
                GetFileDate(fileName);
            } catch {
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// Decode date from a file name
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>DateTime</returns>
        private static DateTime GetFileDate(string fileName) {
            // Umazu cestu a priponu
            fileName = Path.GetFileNameWithoutExtension(fileName);

            // Umazu uvodni "Bat1" s mezerou, zbude "2008-05-13"
            fileName = fileName.Remove(0, Prefix.Length);

            int year  = int.Parse(fileName.Substring(0, 4));
            int month = int.Parse(fileName.Substring(5, 2));
            int day   = int.Parse(fileName.Substring(8, 2));
            int hour  = int.Parse(fileName.Substring(11, 2));
            int min   = int.Parse(fileName.Substring(14, 2));
            int sec   = int.Parse(fileName.Substring(17, 2));
            
            return new DateTime(year, month, day, hour, min, sec);
        }

        /// <summary>
        /// Get list of available periodic backups
        /// </summary>
        /// <returns>List of backups</returns>
        public static List<PeriodicBackupFile> GetFileList() {
            List<PeriodicBackupFile> list = new List<PeriodicBackupFile>();

            // Pokud adresar neexistuje, vratim prazdny seznam
            if (!Directory.Exists(BackupDirectory)) {
                return list;
            }

            // Nactu vsechny soubory "Bat1 *.b1b" v adresari
            string[] files = Directory.GetFiles(BackupDirectory, Prefix + "*." + FileExtension.BACKUP);

            // Setridim seznam podle data (nazvy souboru jsou delane tak, aby stacilo tridit podle jmena)
            Array.Sort(files);

            // Vytvorim seznam souboru
            foreach (string fileName in files) {
                if (!IsNameValid(fileName)) {
                    continue;
                }
                PeriodicBackupFile periodicBackupFile;
                periodicBackupFile.Name    = Path.GetFullPath(fileName);
                periodicBackupFile.Created = GetFileDate(fileName);
                list.Add(periodicBackupFile);
            }

            return list;
        }

        /// <summary>
        /// Get date of the oldest backup
        /// </summary>
        /// <param name="list">List of backups</param>
        /// <returns>DateTime</returns>
        private static DateTime GetFirstBackupDate(List<PeriodicBackupFile> list) {
            DateTime minDateTime = DateTime.MaxValue;
            
            // Zjistim minimalni datum
            foreach (PeriodicBackupFile file in list) {
                if (file.Created < minDateTime) {
                    minDateTime = file.Created;
                }
            }

            return minDateTime;
        }

        /// <summary>
        /// Get date of the newest backup
        /// </summary>
        /// <param name="list">List of backups</param>
        /// <returns>DateTime</returns>
        private static DateTime GetLastBackupDate(List<PeriodicBackupFile> list) {
            DateTime maxDateTime = DateTime.MinValue;
            
            // Zjistim maximalni datum
            foreach (PeriodicBackupFile file in list) {
                if (file.Created > maxDateTime) {
                    maxDateTime = file.Created;
                }
            }

            return maxDateTime;
        }

        /// <summary>
        /// Delete the oldest backup
        /// </summary>
        /// <param name="list">List of backups</param>
        private static void DeleteFirstBackup(List<PeriodicBackupFile> list) {
            // Pokud je seznam prazdny, nic nemazu
            if (list.Count == 0) {
                return;
            }

            // Zjistim datum nejstarsi zalohy a smazu dany soubor
            System.IO.File.Delete(BackupDirectory + @"\" + CreateFileName(GetFirstBackupDate(list)));
        }

        /// <summary>
        /// Check if the period elapsed from the last periodic backup
        /// </summary>
        /// <param name="list">List of backups</param>
        /// <param name="period">Period in days</param>
        /// <returns>True if the period elapsed</returns>
        private static bool PeriodElapsed(List<PeriodicBackupFile> list, int period) {
            // Cas neberu v potaz, pouze datum. Pokud je napr. perioda 1 den a posledni zaloha 1.1.2008 23:59,
            // hned 2.1.2008 00:00 se provede dalsi zaloha
            TimeSpan span = DateTime.Now.Date - GetLastBackupDate(list).Date;       // Pouze datumy
            return (bool)((int)span.TotalDays >= period);
        }

        /// <summary>
        /// Execute periodic backup
        /// </summary>
        public static void Execute() {
            // Nactu seznam jiz vytvorenych zaloh
            List<PeriodicBackupFile> list = GetFileList();

            // Zkontroluju, zda uz ubehla zadana perioda od posledni periodicke zalohy
            if (!PeriodElapsed(list, BACKUP_PERIOD)) {
                return;
            }

            // Pokud adresar neexistuje, vytvorim ho
            if (!Directory.Exists(BackupDirectory)) {
                Directory.CreateDirectory(BackupDirectory);
            }

            // Zkontroluju maximalni pocet zaloh
            if (list.Count >= MAX_BACKUP_COUNT) {
                DeleteFirstBackup(list);
            }

            // Zalohuju
            Backup.BackupWithCompaction(BackupDirectory + @"\" + CreateFileName(DateTime.Now));
        }
    }
}
