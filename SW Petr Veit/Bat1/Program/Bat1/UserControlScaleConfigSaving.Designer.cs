﻿namespace Bat1 {
    partial class UserControlScaleConfigSaving {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlScaleConfigSaving));
            this.comboBoxSortingMode = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelStabilizationRangeUnits = new System.Windows.Forms.Label();
            this.labelStabilizationTimeSec = new System.Windows.Forms.Label();
            this.labelMinimumWeightUnits = new System.Windows.Forms.Label();
            this.labelFilterSec = new System.Windows.Forms.Label();
            this.labelStabilizationRange = new System.Windows.Forms.Label();
            this.labelStabilizationTime = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.checkBoxEnableFileSettings = new System.Windows.Forms.CheckBox();
            this.checkBoxEnableMoreBirds = new System.Windows.Forms.CheckBox();
            this.numericUpDownFilter = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMinimumWeight = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStabilizationTime = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownStabilizationRange = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinimumWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStabilizationTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStabilizationRange)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxSortingMode
            // 
            resources.ApplyResources(this.comboBoxSortingMode, "comboBoxSortingMode");
            this.comboBoxSortingMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSortingMode.FormattingEnabled = true;
            this.comboBoxSortingMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxSortingMode.Items"),
            resources.GetString("comboBoxSortingMode.Items1"),
            resources.GetString("comboBoxSortingMode.Items2")});
            this.comboBoxSortingMode.Name = "comboBoxSortingMode";
            this.comboBoxSortingMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // labelStabilizationRangeUnits
            // 
            resources.ApplyResources(this.labelStabilizationRangeUnits, "labelStabilizationRangeUnits");
            this.labelStabilizationRangeUnits.Name = "labelStabilizationRangeUnits";
            // 
            // labelStabilizationTimeSec
            // 
            resources.ApplyResources(this.labelStabilizationTimeSec, "labelStabilizationTimeSec");
            this.labelStabilizationTimeSec.Name = "labelStabilizationTimeSec";
            // 
            // labelMinimumWeightUnits
            // 
            resources.ApplyResources(this.labelMinimumWeightUnits, "labelMinimumWeightUnits");
            this.labelMinimumWeightUnits.Name = "labelMinimumWeightUnits";
            // 
            // labelFilterSec
            // 
            resources.ApplyResources(this.labelFilterSec, "labelFilterSec");
            this.labelFilterSec.Name = "labelFilterSec";
            // 
            // labelStabilizationRange
            // 
            resources.ApplyResources(this.labelStabilizationRange, "labelStabilizationRange");
            this.labelStabilizationRange.Name = "labelStabilizationRange";
            // 
            // labelStabilizationTime
            // 
            resources.ApplyResources(this.labelStabilizationTime, "labelStabilizationTime");
            this.labelStabilizationTime.Name = "labelStabilizationTime";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // comboBoxMode
            // 
            resources.ApplyResources(this.comboBoxMode, "comboBoxMode");
            this.comboBoxMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Items.AddRange(new object[] {
            resources.GetString("comboBoxMode.Items"),
            resources.GetString("comboBoxMode.Items1"),
            resources.GetString("comboBoxMode.Items2")});
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.SelectionChangeCommitted += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // checkBoxEnableFileSettings
            // 
            resources.ApplyResources(this.checkBoxEnableFileSettings, "checkBoxEnableFileSettings");
            this.checkBoxEnableFileSettings.Name = "checkBoxEnableFileSettings";
            this.checkBoxEnableFileSettings.UseVisualStyleBackColor = true;
            this.checkBoxEnableFileSettings.CheckedChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // checkBoxEnableMoreBirds
            // 
            resources.ApplyResources(this.checkBoxEnableMoreBirds, "checkBoxEnableMoreBirds");
            this.checkBoxEnableMoreBirds.Name = "checkBoxEnableMoreBirds";
            this.checkBoxEnableMoreBirds.UseVisualStyleBackColor = true;
            this.checkBoxEnableMoreBirds.CheckedChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownFilter
            // 
            resources.ApplyResources(this.numericUpDownFilter, "numericUpDownFilter");
            this.numericUpDownFilter.DecimalPlaces = 1;
            this.numericUpDownFilter.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownFilter.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            65536});
            this.numericUpDownFilter.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownFilter.Name = "numericUpDownFilter";
            this.numericUpDownFilter.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownFilter.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownMinimumWeight
            // 
            resources.ApplyResources(this.numericUpDownMinimumWeight, "numericUpDownMinimumWeight");
            this.numericUpDownMinimumWeight.Name = "numericUpDownMinimumWeight";
            this.numericUpDownMinimumWeight.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownStabilizationTime
            // 
            resources.ApplyResources(this.numericUpDownStabilizationTime, "numericUpDownStabilizationTime");
            this.numericUpDownStabilizationTime.DecimalPlaces = 1;
            this.numericUpDownStabilizationTime.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStabilizationTime.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownStabilizationTime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStabilizationTime.Name = "numericUpDownStabilizationTime";
            this.numericUpDownStabilizationTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStabilizationTime.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // numericUpDownStabilizationRange
            // 
            resources.ApplyResources(this.numericUpDownStabilizationRange, "numericUpDownStabilizationRange");
            this.numericUpDownStabilizationRange.DecimalPlaces = 1;
            this.numericUpDownStabilizationRange.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStabilizationRange.Maximum = new decimal(new int[] {
            990,
            0,
            0,
            65536});
            this.numericUpDownStabilizationRange.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStabilizationRange.Name = "numericUpDownStabilizationRange";
            this.numericUpDownStabilizationRange.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownStabilizationRange.ValueChanged += new System.EventHandler(this.comboBox_SelectionChangeCommitted);
            // 
            // UserControlScaleConfigSaving
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.numericUpDownStabilizationRange);
            this.Controls.Add(this.numericUpDownStabilizationTime);
            this.Controls.Add(this.numericUpDownMinimumWeight);
            this.Controls.Add(this.numericUpDownFilter);
            this.Controls.Add(this.checkBoxEnableFileSettings);
            this.Controls.Add(this.checkBoxEnableMoreBirds);
            this.Controls.Add(this.labelStabilizationRangeUnits);
            this.Controls.Add(this.labelStabilizationTimeSec);
            this.Controls.Add(this.labelMinimumWeightUnits);
            this.Controls.Add(this.labelFilterSec);
            this.Controls.Add(this.labelStabilizationRange);
            this.Controls.Add(this.comboBoxSortingMode);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelStabilizationTime);
            this.Controls.Add(this.comboBoxMode);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Name = "UserControlScaleConfigSaving";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMinimumWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStabilizationTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStabilizationRange)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxSortingMode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelStabilizationRangeUnits;
        private System.Windows.Forms.Label labelStabilizationTimeSec;
        private System.Windows.Forms.Label labelMinimumWeightUnits;
        private System.Windows.Forms.Label labelFilterSec;
        private System.Windows.Forms.Label labelStabilizationRange;
        private System.Windows.Forms.Label labelStabilizationTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox checkBoxEnableFileSettings;
        private System.Windows.Forms.CheckBox checkBoxEnableMoreBirds;
        private System.Windows.Forms.NumericUpDown numericUpDownFilter;
        private System.Windows.Forms.NumericUpDown numericUpDownMinimumWeight;
        private System.Windows.Forms.NumericUpDown numericUpDownStabilizationTime;
        private System.Windows.Forms.NumericUpDown numericUpDownStabilizationRange;
    }
}
