﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Bat1 {
    public partial class FormManualResults : Form {
        private Weighing editWeighing = null;

        public FormManualResults() {
            InitializeComponent();
        }

        public FormManualResults(Weighing weighing) {
            InitializeComponent();

            editWeighing = weighing;
            
            userControlManualResults.LoadWeighing(weighing);

            buttonSave.Text = Properties.Resources.SAVE_CHANGES;
            AcceptButton = buttonSave;
        }

        private void buttonSave_Click(object sender, EventArgs e) {
            userControlManualResults.Save();

            if (editWeighing != null) {
                DialogResult = DialogResult.OK;     // Pri editaci hned po ulozeni koncim
            }
        }
    }
}
