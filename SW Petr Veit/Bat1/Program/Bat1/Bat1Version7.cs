﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.Bat1;
using Veit.Bcd;
using Veit.Scale;
using Veit.ScaleStatistics;
using System.IO;

namespace Bat1 {
    /// <summary>
    /// Bat1 scale version 7 connection
    /// </summary>
    public class Bat1Version7 {

        /// <summary>
        /// Deafult scale config, initialized in a static constructor
        /// </summary>
        public static ScaleConfig DefaultConfig { get { return defaultConfig; } }
        private static ScaleConfig defaultConfig;

        /// <summary>
        /// Static constructor
        /// </summary>
        static Bat1Version7() {
            // Zakazu logger - staci jednou staticky
            Veit.Bat1.Dll.EnableLogger(false);

            // Vytvorim default config, aby byl dale k dispozici (nemuzu ho vytvaret kdykoliv, protoze Veit.Bat1.Dll
            // je staticka trida, tj. mohly by se narusit nactena data z vahy)
            Veit.Bat1.Dll.NewDevice();
            defaultConfig = ReadGlobalConfig();
        }
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Bat1Version7() {
        }

        /// <summary>
        /// Check if device is connected
        /// </summary>
        /// <returns>True if connected</returns>
        private bool CheckDevice() {
            // Kontrolovat musim aspon 2x. Pokud je vaha nepripojena, tak se nastavi nejaky flag, ktery se vynuluje pouze
            // dalsim volanim Veit.Bat1.Dll.CheckDevice(). Az nasledujicim volanim Veit.Bat1.Dll.CheckDevice() se opravdu
            // zkontroluje pripojeni.
            for (int i = 0; i < 3; i++) {
                if (Veit.Bat1.Dll.CheckDevice()) {
                    return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// Check if the scale is connected and ready to communicate
        /// </summary>
        /// <returns></returns>
        public bool Check() {
            // Kontrola pripojeni
            if (!CheckDevice()) {
                return false;
            }

            // Pokud je vaha zapnuta, vypnu ji - behem komunikace musi byt vypnuta (sice je to nutne jen pokud se zapisuje
            // do EEPROM, ale radeji to beru jako pravidlo pri kazde komunikaci. I uzivatel si lepe zvykne, kdyz se vypne vzdy)
            return PowerOff();
        }
        
        /// <summary>
        /// Switch the scale off
        /// </summary>
        /// <returns>True if successful</returns>
        private bool PowerOff() {
            bool powerOn;
            if (!Veit.Bat1.Dll.DeviceIsOn(out powerOn)) {
                return false;
            }
            if (powerOn) {
                if (!Veit.Bat1.Dll.DevicePowerOff()) {
                    return false;
                }
            }
            return true;
        }
        
        /// <summary>
        /// Decode date and time from internal format
        /// </summary>
        /// <param name="value">Date and time in internal format</param>
        /// <returns>DateTime instance</returns>
        private DateTime DecodeDateTime(int value) {
            int day, month, year, hour, min, sec;
            Veit.Bat1.Dll.DecodeTime(value, out day, out month, out year, out hour, out min, out sec);
            return new DateTime(year, month, day, hour, min, sec);
        }
        
        /// <summary>
        /// Read scale name
        /// </summary>
        /// <returns>Scale name</returns>
        public static string ReadScaleName() {
            StringBuilder stringBuilder = new StringBuilder();
            Veit.Bat1.Dll.GetScaleName(stringBuilder);
            return stringBuilder.ToString();
        }
        
        /// <summary>
        /// Check if password is null
        /// </summary>
        /// <param name="password">Password</param>
        /// <returns>True if null</returns>
        private static bool IsPasswordNull(byte [] password) {
            // Heslo je zakazane, pokud jsou vsechny klavesy K_NULL
            foreach (byte key in password) {
                if (key != (byte)Keys.K_NULL) {
                    return false;
                }
            }
            return true;
        }
        
        /// <summary>
        /// Read global scale config, scale doesn't need to be connected
        /// </summary>
        /// <returns>Global config</returns>
        public static ScaleConfig ReadGlobalConfig() {
            ScaleConfig scaleConfig = new ScaleConfig();

            // Verze
            int version = Bcd.ToInt32((int)Veit.Bat1.Dll.GetDeviceVersion());
            scaleConfig.Version.Major = version / 100;
            scaleConfig.Version.Minor = version % 100;
            scaleConfig.Version.Build = (int)Veit.Bat1.Dll.GetBuild();
            scaleConfig.Version.Hw    = (int)Veit.Bat1.Dll.GetHwVersion();

            // Jmeno vahy
            scaleConfig.ScaleName = ReadScaleName();

            // Country - jazyk prevedu podle verze vahy na univerzalni kod jazyka pouzity v DB
            scaleConfig.Country            = (Country)           Veit.Bat1.Dll.GetCountry();
            scaleConfig.LanguageInDatabase = ScaleLanguage.ConvertToDatabase(Veit.Bat1.Dll.GetLanguage(), scaleConfig.Version.Minor);     // Konvertuju na univerzalni kod jazyka pouzivany v DB
            scaleConfig.DateFormat         = (DateFormat)        Veit.Bat1.Dll.GetDeviceDateFormat();
            scaleConfig.DateSeparator1     =                     Veit.Bat1.Dll.GetDateSeparator1();
            scaleConfig.DateSeparator2     =                     Veit.Bat1.Dll.GetDateSeparator2();
            scaleConfig.TimeFormat         = (TimeFormat)        Veit.Bat1.Dll.GetDeviceTimeFormat();
            scaleConfig.TimeSeparator      =                     Veit.Bat1.Dll.GetTimeSeparator();
            scaleConfig.DaylightSavingMode = (DaylightSavingMode)Veit.Bat1.Dll.GetDaylightSavingType();

            // Format hmotnosti
            scaleConfig.Units.Units        = (Units)Veit.Bat1.Dll.GetWeighingUnits();
            scaleConfig.Units.Range        = (int)  Veit.Bat1.Dll.GetWeighingRange();
            scaleConfig.Units.Decimals     =        Veit.Bat1.Dll.GetWeighingDecimals();
            scaleConfig.Units.MaxDivision  = (int)  Veit.Bat1.Dll.GetWeighingMaxDivision();
            scaleConfig.Units.Division     =        ConvertWeight.IntToWeight(Veit.Bat1.Dll.GetWeighingDivision(), scaleConfig.Units.Units);
            scaleConfig.Units.WeighingCapacity = (WeighingCapacity)Veit.Bat1.Dll.GetWeighingCapacity();

            // Zvuky
            scaleConfig.Sounds.ToneDefault    = (Tone)Veit.Bat1.Dll.GetToneDefault();
            scaleConfig.Sounds.ToneLight      = (Tone)Veit.Bat1.Dll.GetToneLight();
            scaleConfig.Sounds.ToneOk         = (Tone)Veit.Bat1.Dll.GetToneOk();
            scaleConfig.Sounds.ToneHeavy      = (Tone)Veit.Bat1.Dll.GetToneHeavy();
            scaleConfig.Sounds.ToneKeyboard   = (Tone)Veit.Bat1.Dll.GetToneKeyboard();
            scaleConfig.Sounds.EnableSpecial  =       Veit.Bat1.Dll.GetEnableSpecialSounds();
            scaleConfig.Sounds.VolumeSaving   =       Veit.Bat1.Dll.GetVolumeSaving();
            scaleConfig.Sounds.VolumeKeyboard =       Veit.Bat1.Dll.GetVolumeKeyboard();

            // Zobrazeni
            scaleConfig.Display.Mode                = (DisplayMode)  Veit.Bat1.Dll.GetDisplayMode();
            scaleConfig.Display.Contrast            =                Veit.Bat1.Dll.GetDisplayContrast();
            scaleConfig.Display.Backlight.Mode      = (BacklightMode)Veit.Bat1.Dll.GetBacklightMode();
            scaleConfig.Display.Backlight.Intensity =                Veit.Bat1.Dll.GetBacklightIntensity();
            scaleConfig.Display.Backlight.Duration  =                Veit.Bat1.Dll.GetBacklightDuration();

            // Tiskarna
            scaleConfig.Printer.PaperWidth          =            Veit.Bat1.Dll.GetPrinterPaperWidth();
            scaleConfig.Printer.CommunicationFormat = (ComFormat)Veit.Bat1.Dll.GetPrinterCommunicationFormat();
            scaleConfig.Printer.CommunicationSpeed  =            Veit.Bat1.Dll.GetPrinterCommunicationSpeed();

            // Globalni nastaveni
            scaleConfig.PowerOffTimeout      = Veit.Bat1.Dll.GetPowerOffTimeout();
            scaleConfig.EnableFileParameters = Veit.Bat1.Dll.GetEnableFileParameters();

            // Heslo
            Veit.Bat1.Dll.GetPassword(scaleConfig.PasswordConfig.Password);
            scaleConfig.PasswordConfig.Enable = !IsPasswordNull(scaleConfig.PasswordConfig.Password);

            // Vazeni
            scaleConfig.WeighingConfig.Saving.EnableMoreBirds    = Veit.Bat1.Dll.GetEnableMoreBirds();
            scaleConfig.WeighingConfig.Saving.NumberOfBirds      = 1;     // Pouziva se u kazdeho souboru zvlast, default necham 1
            scaleConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)Veit.Bat1.Dll.GetWeightSortingMode();
            scaleConfig.WeighingConfig.WeightSorting.LowLimit    = 1;     // Pouziva se u kazdeho souboru zvlast
            scaleConfig.WeighingConfig.WeightSorting.HighLimit   = 2;     // Pouziva se u kazdeho souboru zvlast
            scaleConfig.WeighingConfig.Saving.Mode               = (SavingMode)   Veit.Bat1.Dll.GetSavingMode();
            scaleConfig.WeighingConfig.Saving.Filter             = (double)       Veit.Bat1.Dll.GetFilter() / 10.0;
            scaleConfig.WeighingConfig.Saving.StabilisationTime  = (double)       Veit.Bat1.Dll.GetStabilisationTime() / 10.0;
            scaleConfig.WeighingConfig.Saving.MinimumWeight      = ConvertWeight.IntToWeight(Veit.Bat1.Dll.GetMinimumWeight(), scaleConfig.Units.Units);
            scaleConfig.WeighingConfig.Saving.StabilisationRange = (double)       Veit.Bat1.Dll.GetStabilisationRange() / 10.0;
        
            // Statistika
            scaleConfig.StatisticConfig.UniformityRange =                Veit.Bat1.Dll.GetUniformityRange();
            scaleConfig.StatisticConfig.Histogram.Mode  = (HistogramMode)Veit.Bat1.Dll.GetHistogramMode();
            scaleConfig.StatisticConfig.Histogram.Range =                Veit.Bat1.Dll.GetHistogramRange();
            scaleConfig.StatisticConfig.Histogram.Step  =                ConvertWeight.IntToWeight(Veit.Bat1.Dll.GetHistogramStep(), scaleConfig.Units.Units);

            // Seznam souboru vcetne nastaveni souboru, zaroven nactu aktivni soubor
            scaleConfig.FileList = new FileList();
            int filesCount = Veit.Bat1.Dll.GetFilesCount();
            for (int index = 0; index < filesCount; index++) {
                StringBuilder stringBuilder = new StringBuilder();
                // Nactu jmeno
                Veit.Bat1.Dll.GetFileName(index, stringBuilder);
                string name = stringBuilder.ToString();

                // Nactu poznamku
                Veit.Bat1.Dll.GetFileNote(index, stringBuilder);
                string note = stringBuilder.ToString();
                
                // Vytvorim novy soubor, zaroven nactu jeho config a pridam ho do seznamu
                scaleConfig.FileList.Add(new File(name, note, ReadFileConfig(index)));

                // Zkontroluju, zda jde o aktivni soubor
                if (Veit.Bat1.Dll.IsCurrentFile(index)) {
                    scaleConfig.ActiveFileIndex = index;
                }
            }

            // Skupiny souboru
            scaleConfig.FileGroupList = new FileGroupList();
            int groupsCount = Veit.Bat1.Dll.GetGroupsCount();
            for (int index = 0; index < groupsCount; index++) {
                StringBuilder stringBuilder = new StringBuilder();
                // Nactu jmeno
                Veit.Bat1.Dll.GetGroupName(index, stringBuilder);
                string name = stringBuilder.ToString();

                // Nactu poznamku
                Veit.Bat1.Dll.GetGroupNote(index, stringBuilder);
                string note = stringBuilder.ToString();

                // Vytvorim novou skupinu
                FileGroup group = new FileGroup(name);
                group.SetNote(note);

                // Pridam do skupiny vsechny soubory
                filesCount = Veit.Bat1.Dll.GetGroupFilesCount(index);
                for (int fileIndex = 0; fileIndex < filesCount; fileIndex++) {
                    group.AddFile(Veit.Bat1.Dll.GetGroupFile(index, fileIndex));
                }
                
                // Pridam vytvorenou skupinu do seznamu
                scaleConfig.FileGroupList.Add(group);
            }

            return scaleConfig;
        }

        /// <summary>
        /// Write country config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        /// <param name="version">Version of connected scale</param>
        private void WriteCountryConfig(ScaleConfig scaleConfig, ScaleConfigVersion version) {
            Veit.Bat1.Dll.SetCountry(           (int)scaleConfig.Country);
            Veit.Bat1.Dll.SetLanguage(ScaleLanguage.ConvertToScale(scaleConfig.LanguageInDatabase, version.Minor));    // Kod jazyka pro tuto verzi vah
            Veit.Bat1.Dll.SetCodePage(Veit.Bat1.Locales.GetCodePage(ScaleLanguage.ConvertToScale(scaleConfig.LanguageInDatabase, ScaleVersion.MINOR)));  // Od verze 7.02.x musim nastavovat kodovou stranku rucne podle jazyka
            Veit.Bat1.Dll.SetDeviceDateFormat(  (int)scaleConfig.DateFormat);
            Veit.Bat1.Dll.SetDateSeparator1(         scaleConfig.DateSeparator1);
            Veit.Bat1.Dll.SetDateSeparator2(         scaleConfig.DateSeparator2);
            Veit.Bat1.Dll.SetDeviceTimeFormat(  (int)scaleConfig.TimeFormat);
            Veit.Bat1.Dll.SetTimeSeparator(          scaleConfig.TimeSeparator);
            Veit.Bat1.Dll.SetDaylightSavingType((int)scaleConfig.DaylightSavingMode);
        }

        /// <summary>
        /// Write scale config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteWeighingConfig(ScaleConfig scaleConfig) {
            Veit.Bat1.Dll.SetWeighingUnits((int)scaleConfig.Units.Units);
            Veit.Bat1.Dll.SetWeighingDivision(ConvertWeight.WeightToInt(scaleConfig.Units.Division, scaleConfig.Units.Units));
            Veit.Bat1.Dll.SetWeighingCapacity((int)scaleConfig.Units.WeighingCapacity);     // Musim volat az po SetWeighingUnits()
        }

        /// <summary>
        /// Write sounds config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteSoundsConfig(ScaleConfig scaleConfig) {
            Veit.Bat1.Dll.SetToneDefault(  (int) scaleConfig.Sounds.ToneDefault);
            Veit.Bat1.Dll.SetToneLight(    (int) scaleConfig.Sounds.ToneLight);
            Veit.Bat1.Dll.SetToneOk(       (int) scaleConfig.Sounds.ToneOk);
            Veit.Bat1.Dll.SetToneHeavy(    (int) scaleConfig.Sounds.ToneHeavy);
            Veit.Bat1.Dll.SetToneKeyboard( (int) scaleConfig.Sounds.ToneKeyboard);
            Veit.Bat1.Dll.SetEnableSpecialSounds(scaleConfig.Sounds.EnableSpecial);
            Veit.Bat1.Dll.SetVolumeSaving(       scaleConfig.Sounds.VolumeSaving);
            Veit.Bat1.Dll.SetVolumeKeyboard(     scaleConfig.Sounds.VolumeKeyboard);
        }

        /// <summary>
        /// Write display config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteDisplayConfig(ScaleConfig scaleConfig) {
            // Kontrast zde nenastavuju, nastavuje se extra
            Veit.Bat1.Dll.SetDisplayMode(       (int)scaleConfig.Display.Mode);
            Veit.Bat1.Dll.SetBacklightMode(     (int)scaleConfig.Display.Backlight.Mode);
            Veit.Bat1.Dll.SetBacklightIntensity(     scaleConfig.Display.Backlight.Intensity);
            Veit.Bat1.Dll.SetBacklightDuration(      scaleConfig.Display.Backlight.Duration);
        }

        /// <summary>
        /// Write printer config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WritePrinterConfig(ScaleConfig scaleConfig) {
            Veit.Bat1.Dll.SetPrinterPaperWidth(              scaleConfig.Printer.PaperWidth);
            Veit.Bat1.Dll.SetPrinterCommunicationFormat((int)scaleConfig.Printer.CommunicationFormat);
            Veit.Bat1.Dll.SetPrinterCommunicationSpeed(      scaleConfig.Printer.CommunicationSpeed);
        }

        /// <summary>
        /// Write auto power off config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteAutoPowerOffConfig(ScaleConfig scaleConfig) {
            Veit.Bat1.Dll.SetPowerOffTimeout(scaleConfig.PowerOffTimeout);
        }

        /// <summary>
        /// Write Password config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WritePasswordConfig(ScaleConfig scaleConfig) {
            if (scaleConfig.PasswordConfig.Enable) {
                Veit.Bat1.Dll.SetPassword(scaleConfig.PasswordConfig.Password);
            } else {
                byte [] password = {1, 2};
                Veit.Bat1.Dll.SetPassword(scaleConfig.PasswordConfig.Password);
            }
        }

        /// <summary>
        /// Write weighing config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteSavingConfig(ScaleConfig scaleConfig) {
            Veit.Bat1.Dll.SetWeightSortingMode(         (int)scaleConfig.WeighingConfig.WeightSorting.Mode);
            Veit.Bat1.Dll.SetSavingMode(                (int)scaleConfig.WeighingConfig.Saving.Mode);
            Veit.Bat1.Dll.SetFilter(            (int)(10.0 * scaleConfig.WeighingConfig.Saving.Filter));
            Veit.Bat1.Dll.SetStabilisationTime( (int)(10.0 * scaleConfig.WeighingConfig.Saving.StabilisationTime));
            Veit.Bat1.Dll.SetMinimumWeight(     ConvertWeight.WeightToInt(scaleConfig.WeighingConfig.Saving.MinimumWeight,      scaleConfig.Units.Units));
            Veit.Bat1.Dll.SetStabilisationRange((int)(10.0 * scaleConfig.WeighingConfig.Saving.StabilisationRange));
            Veit.Bat1.Dll.SetEnableMoreBirds(                scaleConfig.WeighingConfig.Saving.EnableMoreBirds);
            Veit.Bat1.Dll.SetEnableFileParameters(           scaleConfig.EnableFileParameters);
        }

        /// <summary>
        /// Write statistics config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteStatisticsConfig(ScaleConfig scaleConfig) {
            Veit.Bat1.Dll.SetUniformityRange(scaleConfig.StatisticConfig.UniformityRange);
            if (scaleConfig.StatisticConfig.Histogram.Mode == HistogramMode.RANGE) {
                Veit.Bat1.Dll.SetHistogramRange(scaleConfig.StatisticConfig.Histogram.Range);
            } else {
                Veit.Bat1.Dll.SetHistogramStep(ConvertWeight.WeightToInt(scaleConfig.StatisticConfig.Histogram.Step, scaleConfig.Units.Units));
            }
        }

        /// <summary>
        /// Check there is enough space to create new file or group
        /// </summary>
        /// <returns>True if can be created</returns>
        private bool CanCreateFileOrGroup() {
            // V souborovem systemu je misto jen na DIRECTORY_SIZE souboru a skupin dohromady
            return (bool)(Veit.Bat1.Dll.GetFilesCount() + Veit.Bat1.Dll.GetGroupsCount() < Veit.Bat1.Const.DIRECTORY_SIZE); 
        }
        
        /// <summary>
        /// Write file list config into the device and set acrive file
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteFilesConfig(ScaleConfig scaleConfig) {
            // Vytvorim vsechny soubory
            foreach (File file in scaleConfig.FileList.List) {
                if (!CanCreateFileOrGroup()) {
                    break;      // Na dalsi soubor uz neni misto
                }

                int index = Veit.Bat1.Dll.FileCreate();

                // Jmeno a poznamka
                Veit.Bat1.Dll.SetFileName(index, file.Name);
                Veit.Bat1.Dll.SetFileNote(index, file.Note);

                // Nastaveni souboru
                WeighingConfig weighingConfig = file.FileConfig.WeighingConfig;     // Pro zkraceni textu

                // Pocet kusu a meze nastavuji u kazdeho souboru zvlast i kdyz pouziva jen globalni parametry
                Veit.Bat1.Dll.SetFileNumberOfBirds(index, weighingConfig.Saving.NumberOfBirds);
                Veit.Bat1.Dll.SetFileLowLimit(index,      ConvertWeight.WeightToInt(weighingConfig.WeightSorting.LowLimit,  scaleConfig.Units.Units));
                Veit.Bat1.Dll.SetFileHighLimit(index,     ConvertWeight.WeightToInt(weighingConfig.WeightSorting.HighLimit, scaleConfig.Units.Units));

                if (!scaleConfig.EnableFileParameters) {
                    // Parametry souboru nejsou povoleny
                    continue;       
                }

                // Ostatni parametry souboru, ktere se nastavuji pouze pri povolenem file-specific settings
                Veit.Bat1.Dll.SetFileWeightSortingMode(index,  (int)       weighingConfig.WeightSorting.Mode);
                Veit.Bat1.Dll.SetFileEnableMoreBirds(index,                weighingConfig.Saving.EnableMoreBirds);
                Veit.Bat1.Dll.SetFileSavingMode(index,         (int)        weighingConfig.Saving.Mode);
                Veit.Bat1.Dll.SetFileFilter(index,             (int)(10.0 * weighingConfig.Saving.Filter));
                Veit.Bat1.Dll.SetFileStabilisationTime(index,  (int)(10.0 * weighingConfig.Saving.StabilisationTime));
                Veit.Bat1.Dll.SetFileMinimumWeight(index,      ConvertWeight.WeightToInt(weighingConfig.Saving.MinimumWeight,      scaleConfig.Units.Units));
                Veit.Bat1.Dll.SetFileStabilisationRange(index, (int)(10.0 * weighingConfig.Saving.StabilisationRange));
            }

            // Nastavim aktivni soubor
            if (scaleConfig.FileList.List.Count > 0 && scaleConfig.ActiveFileIndex >= 0) {
                Veit.Bat1.Dll.SetCurrentFile(scaleConfig.ActiveFileIndex);
            }
        }

        /// <summary>
        /// Write group list config into the device
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        private void WriteGroupsConfig(ScaleConfig scaleConfig) {
            foreach (FileGroup group in scaleConfig.FileGroupList.List) {
                if (!CanCreateFileOrGroup()) {
                    break;      // Na dalsi skupinu uz neni misto
                }

                int index = Veit.Bat1.Dll.GroupCreate();

                // Jmeno a poznamka
                Veit.Bat1.Dll.SetGroupName(index, group.Name);
                Veit.Bat1.Dll.SetGroupNote(index, group.Note);

                // Seznam souboru
                Veit.Bat1.Dll.GroupClearFiles(index);
                foreach (int fileIndex in group.FileIndexList) {
                    Veit.Bat1.Dll.AddGroupFile(index, fileIndex);
                }
            }
        }

        /// <summary>
        /// Delete all files and groups from the device
        /// </summary>
        private void DeleteAllFilesAndGroups() {
            Veit.Bat1.Dll.GroupsDeleteAll();
            Veit.Bat1.Dll.FilesDeleteAll();
        }

        /// <summary>
        /// Write whole config into the device. The device must be already read!
        /// </summary>
        /// <param name="scaleConfig">Scale config</param>
        public void WriteConfig(ScaleConfig scaleConfig) {
            // Nactu cislo verze prave pripojene vahy
            ScaleConfigVersion version = ReadGlobalConfig().Version;
            
            // Updatuju cely config
            // Smazu vsechny soubory a skupiny a pridam nove definovane. Vzorky se smazou automaticky.
            DeleteAllFilesAndGroups();
            WriteFilesConfig(scaleConfig);
            WriteGroupsConfig(scaleConfig);
            WriteCountryConfig(scaleConfig, version);
            WritePasswordConfig(scaleConfig);
            WriteDisplayConfig(scaleConfig);
            WriteSoundsConfig(scaleConfig);
            WriteSavingConfig(scaleConfig);
            WriteStatisticsConfig(scaleConfig);
            WriteWeighingConfig(scaleConfig);
            WritePrinterConfig(scaleConfig);
            WriteAutoPowerOffConfig(scaleConfig);
        }

        /// <summary>
        /// Save device
        /// </summary>
        /// <returns>True if successful</returns>
        public bool SaveDevice() {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }

            return Veit.Bat1.Dll.SaveDevice();
        }

        /// <summary>
        /// Read file-specific config
        /// </summary>
        /// <param name="index">File index</param>
        /// <returns>File-specific config</returns>
        private static FileConfig ReadFileConfig(int index) {
            FileConfig fileConfig = new FileConfig();
            Units units = (Units)Veit.Bat1.Dll.GetWeighingUnits();      // Globalni config zde nemam k dispozici

            // Pokud pouziva pouze globalni parametry, funkce GetFileXXX() nelze pouzit, musim hodnoty rucne nacist z globalniho configu
            // Pocet kurat a meze se ukladaji ke kazdemu souboru zvlast vzdy, i pokud ma nastavene pouziti globalniho configu
            fileConfig.WeighingConfig.Saving.NumberOfBirds    = Veit.Bat1.Dll.GetFileNumberOfBirds(index);
            fileConfig.WeighingConfig.WeightSorting.LowLimit  = ConvertWeight.IntToWeight(Veit.Bat1.Dll.GetFileLowLimit(index), units);
            fileConfig.WeighingConfig.WeightSorting.HighLimit = ConvertWeight.IntToWeight(Veit.Bat1.Dll.GetFileHighLimit(index), units);

            if (Veit.Bat1.Dll.GetEnableFileParameters()) {
                // Pouziva nastaveni pro kazdy soubor zvlast
                fileConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)Veit.Bat1.Dll.GetFileWeightSortingMode(index);
                fileConfig.WeighingConfig.Saving.EnableMoreBirds    =                Veit.Bat1.Dll.GetFileEnableMoreBirds(index);
                fileConfig.WeighingConfig.Saving.Mode               = (SavingMode)   Veit.Bat1.Dll.GetFileSavingMode(index);
                fileConfig.WeighingConfig.Saving.Filter             = (double)       Veit.Bat1.Dll.GetFileFilter(index) / 10.0;
                fileConfig.WeighingConfig.Saving.StabilisationTime  = (double)       Veit.Bat1.Dll.GetFileStabilisationTime(index) / 10.0;
                fileConfig.WeighingConfig.Saving.MinimumWeight      = ConvertWeight.IntToWeight(Veit.Bat1.Dll.GetFileMinimumWeight(index), units);
                fileConfig.WeighingConfig.Saving.StabilisationRange = (double)       Veit.Bat1.Dll.GetFileStabilisationRange(index) / 10.0;
            } else {
                // Pouziva pouze globalni parametry, pouze preberu
                fileConfig.WeighingConfig.WeightSorting.Mode        = (WeightSorting)Veit.Bat1.Dll.GetWeightSortingMode();
                fileConfig.WeighingConfig.Saving.EnableMoreBirds    =                Veit.Bat1.Dll.GetEnableMoreBirds();
                fileConfig.WeighingConfig.Saving.Mode               = (SavingMode)   Veit.Bat1.Dll.GetSavingMode();
                fileConfig.WeighingConfig.Saving.Filter             = (double)       Veit.Bat1.Dll.GetFilter() / 10.0;
                fileConfig.WeighingConfig.Saving.StabilisationTime  = (double)       Veit.Bat1.Dll.GetStabilisationTime() / 10.0;
                fileConfig.WeighingConfig.Saving.MinimumWeight      = ConvertWeight.IntToWeight(Veit.Bat1.Dll.GetMinimumWeight(), units);
                fileConfig.WeighingConfig.Saving.StabilisationRange = (double)       Veit.Bat1.Dll.GetStabilisationRange() / 10.0;
            }

            return fileConfig;
        }

        /// <summary>
        /// Read samples saved in a file
        /// </summary>
        /// <param name="fileIndex">File index</param>
        /// <returns>List of samples</returns>
        private SampleList ReadFileSamples(int fileIndex) {
            int samplesCount = Veit.Bat1.Dll.GetFileSamplesCount(fileIndex);

            // Nactu vsechny vzorky
            SampleList list = new SampleList();
            for (int sampleIndex = 0; sampleIndex < samplesCount; sampleIndex++) {
                Sample sample = new Sample();
                sample.dateTime = DecodeDateTime(Veit.Bat1.Dll.GetSampleTimestamp(fileIndex, sampleIndex));
                sample.weight   = (float)Veit.Bat1.Dll.DecodeWeight(Veit.Bat1.Dll.GetSampleWeight(fileIndex, sampleIndex));
                sample.flag     = (Flag)Veit.Bat1.Dll.GetSampleFlag(fileIndex, sampleIndex);
                list.Add(sample);
            }
            list.SortByTime();  // Radsi setridim, kdyby napr. behem vazeni zmenil datum ve vaze

            return list;
        }
        
        private WeighingData ReadFile(int fileIndex, ScaleConfig scaleConfig) {
            StringBuilder stringBuilder = new StringBuilder();

            // Jmeno souboru
            Veit.Bat1.Dll.GetFileName(fileIndex, stringBuilder);
            string name = stringBuilder.ToString();

            // Definici souboru vcetne nastaveni beru ze seznamu souboru v globalnim configu
            File file = scaleConfig.GetFile(name);

/*            // Poznamka
            Veit.Bat1.Dll.GetFileNote(fileIndex, stringBuilder);
            string note = stringBuilder.ToString();

            // Soubor vcetne nastaveni
            File file = new File(name, note, ReadFileConfig(fileIndex));*/

            // Vzorky
            SampleList sampleList = ReadFileSamples(fileIndex);

            return new WeighingData(-1, ResultType.NEW_SCALE, RecordSource.SAVE, file, sampleList, scaleConfig, "");
        }
        
        private List<WeighingData> ReadFiles(ScaleConfig scaleConfig) {
            List<WeighingData> fileList = new List<WeighingData>();

            // Zjistim pocet souboru ve vaze
            int filesCount = Veit.Bat1.Dll.GetFilesCount();

            // Postupne nacitam jednotlive soubory
            for (int fileIndex = 0; fileIndex < filesCount; fileIndex++) {
                fileList.Add(ReadFile(fileIndex, scaleConfig));
            }

            return fileList;
        }
        
        /// <summary>
        /// Load device
        /// </summary>
        /// <returns>True if successful</returns>
        public bool LoadDevice() {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }
            
            // Nahraju vsechna data z vahy
            if (!Veit.Bat1.Dll.LoadDevice()) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Load complete scale config only
        /// </summary>
        /// <returns>True if successful</returns>
        public bool LoadConfig() {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }
            
            // Nahraju z vahy nastaveni
            if (!Veit.Bat1.Dll.LoadConfiguration()) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check if the scale is supported by the SW. Scales with higher version numbers than the SW was designed to are not supported.
        /// </summary>
        /// <returns></returns>
        public bool IsSupported() {
            // Pozor, device uz musi byt nacteny
            
            // Nactu globalni config
            ScaleConfig scaleConfig = ReadGlobalConfig();

            // Vahy 7.00.x nepodporuju, jsou binarne nekompatibilni s 7.01 (7.00.x mela jen 64 souboru)
            if (scaleConfig.Version.Minor == 0) {
                return false;
            }
            
            // Cislo verze vahy musi byt mensi nebo rovno cislu verze, ktere podporuje SW.
            // Build neni treba kontrolovat
            if (scaleConfig.Version.Major > ScaleVersion.MAJOR) {
                return false;
            }
            if (scaleConfig.Version.Minor > ScaleVersion.MINOR) {
                return false;
            }

            // Podporovana vaha
            return true;
        }

        public bool ReadAll(out ScaleConfig scaleConfig, out List<WeighingData> weighingDataList) {
            scaleConfig      = null;
            weighingDataList = null;
            
            // Kontrola pripojeni
            if (!LoadDevice()) {
                return false;
            }

            // Globalni nastaveni vahy
            scaleConfig = ReadGlobalConfig();

            // Soubory
            weighingDataList = ReadFiles(scaleConfig);
            
            return true;
        }

        /// <summary>
        /// Clear samples in all files
        /// </summary>
        /// <returns>True if successful</returns>
        public bool ClearAllFiles() {
            // Nactu
            if (!LoadConfig()) {
                return false;
            }

            // Smazu vsechny vzorky
            int filesCount = Veit.Bat1.Dll.GetFilesCount();
            for (int index = 0; index < filesCount; index++) {
                Veit.Bat1.Dll.FileClearSamples(index);
            }

            // Ulozim zpet do vahy, kontrolovat pripojeni znovu nemusim
            return Veit.Bat1.Dll.SaveDevice();
        }

        /// <summary>
        /// Upload startup logo
        /// </summary>
        /// <param name="data">Logo data</param>
        /// <returns>True if successful</returns>
        public bool UploadLogo(byte[] data) {
            // Kontrola pripojeni, nic z vahy neni treba nacitat
            if (!Check()) {
                return false;
            }

            // Ulozim logo do vahy
            return Veit.Bat1.Dll.WriteEeprom(Veit.Bat1.Dll.GetLogoAddress(), data, Veit.Bat1.Dll.GetLogoSize());
        }

        public bool LoadEstimation(out float percent) {
            int promile;
            if (!Veit.Bat1.Dll.LoadEstimation(out promile)) {
                percent = 0;
                return false;
            }
            percent = (float)promile / 10.0f;
            return true;
        }

        /// <summary>
        /// Set computer date and time
        /// </summary>
        /// <returns>True if successful</returns>
        public bool SetDateTime() {
            // Pred zapisem casu musim nacist config, aby spravne fungovala fce EncodeTime(), ktera pouziva
            // konfiguraci pro nastaveni letni/zimni cas
            if (!LoadConfig()) {
                return false;
            }

            DateTime now = DateTime.Now;
            return Veit.Bat1.Dll.SetTime(Veit.Bat1.Dll.EncodeTime(now.Day, now.Month, now.Year, now.Hour, now.Minute, now.Second));
        }

        /// <summary>
        /// Reads date and time from the scale
        /// </summary>
        /// <param name="dateTime">Date and time</param>
        /// <returns>True if successful</returns>
        public bool GetDateTime(out DateTime dateTime) {
            int timestamp;
            if (!Veit.Bat1.Dll.GetTime(out timestamp)) {
                dateTime = DateTime.MinValue;
                return false;
            }

            int day, month, year, hour, min, sec;
            Veit.Bat1.Dll.DecodeTime(timestamp, out day, out month, out year, out hour, out min, out sec);
            dateTime = new DateTime(year, month, day, hour, min, sec);
            return true;
        }

        /// <summary>
        /// Set scale name
        /// </summary>
        /// <param name="name">New name</param>
        /// <returns>True if successful</returns>
        public bool SetName(string name) {
            // Pozor, obsah vahy uz musi byt nacteny, zde uz LoadDevice() nevolam. Predpokladane pouziti je:
            // 1. LoadDevice()
            // 2. Uzivatel edituje jmeno
            // 3. Zavolani teto fce

            // Nastavim nove jmeno
            Veit.Bat1.Dll.SetScaleName(name);

            // Ulozim zpet do vahy, kontrolovat pripojeni znovu nemusim
            return Veit.Bat1.Dll.SaveDevice();
        }

        /// <summary>
        /// Read whole EEPROM and save it as a file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>True if successful</returns>
        public bool ReadEeprom(string fileName) {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }

            int size = Veit.Bat1.Dll.GetEepromSize();
            byte[] buffer = new byte[size];
            if (!Veit.Bat1.Dll.ReadEeprom(0, buffer, size)) {
                return false;
            }
            FileStream fileStream = System.IO.File.Create(fileName);
            fileStream.Write(buffer, 0, size);
            fileStream.Close();

            return true;
        }

        /// <summary>
        /// Write whole EEPROM from a file
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>True if successful</returns>
        public bool WriteEeprom(string fileName) {
            // Kontrola pripojeni
            if (!Check()) {
                return false;
            }

            int size = Veit.Bat1.Dll.GetEepromSize();
            byte[] buffer = new byte[size];
            if (!System.IO.File.Exists(fileName)) {
                return false;
            }
            FileStream fileStream = System.IO.File.OpenRead(fileName);
            fileStream.Read(buffer, 0, size);
            fileStream.Close();
            if (!Veit.Bat1.Dll.WriteEeprom(0, buffer, size)) {
                return false;
            }
/*            if (!Veit.Bat1.Dll.DeviceByEeprom(buffer)) {
                return false;
            }*/

            return true;
        }

    
    }
}
