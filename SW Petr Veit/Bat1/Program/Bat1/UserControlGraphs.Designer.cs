﻿namespace Bat1 {
    partial class UserControlGraphs {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControlGraphs));
            this.comboBoxLeftAxisType = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.comboBoxCurves = new System.Windows.Forms.ComboBox();
            this.pictureBoxMouseInfo = new System.Windows.Forms.PictureBox();
            this.comboBoxRightAxisType = new System.Windows.Forms.ComboBox();
            this.toolTipDefault = new System.Windows.Forms.ToolTip(this.components);
            this.userControlTimeGraph = new Bat1.UserControlTimeGraph();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMouseInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBoxLeftAxisType
            // 
            resources.ApplyResources(this.comboBoxLeftAxisType, "comboBoxLeftAxisType");
            this.comboBoxLeftAxisType.DropDownHeight = 200;
            this.comboBoxLeftAxisType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxLeftAxisType.FormattingEnabled = true;
            this.comboBoxLeftAxisType.Name = "comboBoxLeftAxisType";
            this.toolTipDefault.SetToolTip(this.comboBoxLeftAxisType, resources.GetString("comboBoxLeftAxisType.ToolTip"));
            this.comboBoxLeftAxisType.SelectionChangeCommitted += new System.EventHandler(this.comboBoxLeftType_SelectionChangeCommitted);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.comboBoxCurves);
            this.panel1.Controls.Add(this.pictureBoxMouseInfo);
            this.panel1.Controls.Add(this.comboBoxRightAxisType);
            this.panel1.Controls.Add(this.comboBoxLeftAxisType);
            this.panel1.Name = "panel1";
            this.toolTipDefault.SetToolTip(this.panel1, resources.GetString("panel1.ToolTip"));
            // 
            // comboBoxCurves
            // 
            resources.ApplyResources(this.comboBoxCurves, "comboBoxCurves");
            this.comboBoxCurves.DropDownHeight = 200;
            this.comboBoxCurves.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCurves.FormattingEnabled = true;
            this.comboBoxCurves.Name = "comboBoxCurves";
            this.toolTipDefault.SetToolTip(this.comboBoxCurves, resources.GetString("comboBoxCurves.ToolTip"));
            this.comboBoxCurves.SelectionChangeCommitted += new System.EventHandler(this.comboBoxCurves_SelectionChangeCommitted);
            // 
            // pictureBoxMouseInfo
            // 
            resources.ApplyResources(this.pictureBoxMouseInfo, "pictureBoxMouseInfo");
            this.pictureBoxMouseInfo.Image = global::Bat1.Properties.Resources.GraphHelpInactive;
            this.pictureBoxMouseInfo.Name = "pictureBoxMouseInfo";
            this.pictureBoxMouseInfo.TabStop = false;
            this.toolTipDefault.SetToolTip(this.pictureBoxMouseInfo, resources.GetString("pictureBoxMouseInfo.ToolTip"));
            // 
            // comboBoxRightAxisType
            // 
            resources.ApplyResources(this.comboBoxRightAxisType, "comboBoxRightAxisType");
            this.comboBoxRightAxisType.DropDownHeight = 200;
            this.comboBoxRightAxisType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRightAxisType.FormattingEnabled = true;
            this.comboBoxRightAxisType.Name = "comboBoxRightAxisType";
            this.toolTipDefault.SetToolTip(this.comboBoxRightAxisType, resources.GetString("comboBoxRightAxisType.ToolTip"));
            this.comboBoxRightAxisType.SelectionChangeCommitted += new System.EventHandler(this.comboBoxLeftType_SelectionChangeCommitted);
            // 
            // userControlTimeGraph
            // 
            resources.ApplyResources(this.userControlTimeGraph, "userControlTimeGraph");
            this.userControlTimeGraph.Name = "userControlTimeGraph";
            this.toolTipDefault.SetToolTip(this.userControlTimeGraph, resources.GetString("userControlTimeGraph.ToolTip"));
            // 
            // UserControlGraphs
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.userControlTimeGraph);
            this.Controls.Add(this.panel1);
            this.Name = "UserControlGraphs";
            this.toolTipDefault.SetToolTip(this, resources.GetString("$this.ToolTip"));
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMouseInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxLeftAxisType;
        private System.Windows.Forms.Panel panel1;
        private UserControlTimeGraph userControlTimeGraph;
        private System.Windows.Forms.ComboBox comboBoxRightAxisType;
        private System.Windows.Forms.PictureBox pictureBoxMouseInfo;
        private System.Windows.Forms.ComboBox comboBoxCurves;
        private System.Windows.Forms.ToolTip toolTipDefault;
    }
}
