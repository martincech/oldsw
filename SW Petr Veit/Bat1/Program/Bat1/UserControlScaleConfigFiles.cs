﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Veit.Bat1;
using Veit.Scale;

namespace Bat1 {
    public partial class UserControlScaleConfigFiles : UserControlScaleConfigBase {
        private string windowTitle;

        private string activeFileName;
        
        public UserControlScaleConfigFiles(ScaleConfig scaleConfig, string windowTitle, bool readOnly) {
            InitializeComponent();

            // Read-only
            this.readOnly = readOnly;
            buttonDelete.Enabled = !readOnly;
            buttonNew.Enabled    = !readOnly;
            buttonRename.Enabled = !readOnly;
            buttonActive.Enabled = !readOnly;

            // Preberu config
            SetScaleConfig(scaleConfig);
            this.windowTitle = windowTitle;
        }

        private void RedrawFileList() {
            listBoxFiles.Items.Clear();
            int index = 0;
            foreach (File file in scaleConfig.FileList.List) {
                string str;
                if (index == scaleConfig.ActiveFileIndex) {
                    str = ">> " + file.ToString();
                } else {
                    str = file.ToString();
                }
                listBoxFiles.Items.Add(str);
                index++;
            }
        }

        private void SetUnits() {
            // Nastavim minimalni a maximalni hodnoty podle jednotek
            decimal minWeight = (decimal)ConvertWeight.MinWeight(scaleConfig.Units.Units);
            decimal maxWeight = (decimal)ConvertWeight.MaxWeight(scaleConfig.Units.Units, scaleConfig.Units.WeighingCapacity);

            numericUpDownSortingLowLimit.Minimum          = minWeight;
            numericUpDownSortingLowLimit.Maximum          = maxWeight;
            numericUpDownSortingHighLimit.Minimum         = minWeight;
            numericUpDownSortingHighLimit.Maximum         = maxWeight;
            numericUpDownSavingMinimumWeight.Minimum      = minWeight;
            numericUpDownSavingMinimumWeight.Maximum      = maxWeight;

            switch (scaleConfig.Units.Units) {
                case Units.G:
                    numericUpDownSortingLowLimit.DecimalPlaces          = 0;
                    numericUpDownSortingLowLimit.Increment              = 1;
                    numericUpDownSortingHighLimit.DecimalPlaces         = 0;
                    numericUpDownSortingHighLimit.Increment             = 1;
                    numericUpDownSavingMinimumWeight.DecimalPlaces      = 0;
                    numericUpDownSavingMinimumWeight.Increment          = 10;
                    break;

                default:    // KG nebo LB
                    numericUpDownSortingLowLimit.DecimalPlaces          = 3;
                    numericUpDownSortingLowLimit.Increment              = (decimal)0.001;
                    numericUpDownSortingHighLimit.DecimalPlaces         = 3;
                    numericUpDownSortingHighLimit.Increment             = (decimal)0.001;
                    numericUpDownSavingMinimumWeight.DecimalPlaces      = 3;
                    numericUpDownSavingMinimumWeight.Increment          = (decimal)0.010;
                    break;
            }
        }

        private void RedrawUnits() {
            string unitsString = ConvertWeight.UnitsToString(scaleConfig.Units.Units);
            
            labelLowLimitUnits.Text           = unitsString;
            labelHighLimitUnits.Text          = unitsString;
            labelMinimumWeightUnits.Text      = unitsString;
        }
        
        private void EnableMoreBirds(bool enable) {
            labelMoreBirds.Enabled             = enable;
            numericUpDownNumberOfBirds.Enabled = enable;
        }
        
        private void SetMoreBirds() {
            EnableMoreBirds(checkBoxEnableMoreBirds.Checked);
            checkBoxEnableMoreBirds.Enabled = scaleConfig.EnableFileParameters;
        }

        private void EnableLowLimit(bool enable) {
            labelLowLimit.Enabled                = enable;
            numericUpDownSortingLowLimit.Enabled = enable;
            labelLowLimitUnits.Enabled           = enable;
        }
        
        private void EnableHighLimit(bool enable) {
            labelHighLimit.Enabled                = enable;
            numericUpDownSortingHighLimit.Enabled = enable;
            labelHighLimitUnits.Enabled           = enable;
        }

        private void SetSorting() {
            if (!scaleConfig.EnableFileParameters && scaleConfig.WeighingConfig.WeightSorting.Mode == Veit.Bat1.WeightSorting.NONE) {
                // Pokud nepouziva nastaveni pro kazdy soubor ani nepouziva globalne trideni, zakazu cely group box
                groupBoxSorting.Enabled = false;
                return;
            }
            
            // Trideni povolim
            groupBoxSorting.Enabled = true;

            // Dalsi nastavim podle zvoleneho trideni
            switch (comboBoxSortingMode.SelectedIndex) {
                case (int)WeightSorting.NONE:
                    EnableLowLimit(false);
                    EnableHighLimit(false);
                    break;
                
                case (int)WeightSorting.LIGHT_HEAVY:
                    EnableLowLimit(true);
                    EnableHighLimit(false);
                    break;

                default: // WeightSorting.LIGHT_OK_HEAVY:
                    EnableLowLimit(true);
                    EnableHighLimit(true);
                    break;
            }
        }

        private void EnableSortingMode(bool enable) {
            comboBoxSortingMode.Enabled = enable;
            labelSortingMode.Enabled    = enable;
        }
        
        private void SetSaving() {
            if (!scaleConfig.EnableFileParameters) {
                groupBoxSaving.Enabled = false;
                return;
            }

            // Parametry ukladani povolim
            groupBoxSaving.Enabled = true;

            // Usataleni jen v automatickem rezimu
            bool autoMode = comboBoxSavingMode.SelectedIndex == (int)SavingMode.AUTOMATIC;
            labelStabilizationTime.Enabled                = autoMode;
            numericUpDownSavingStabilizationTime.Enabled  = autoMode;
            labelStabilizationTimeSec.Enabled             = autoMode;
            labelStabilizationRange.Enabled               = autoMode;
            numericUpDownSavingStabilizationRange.Enabled = autoMode;
            labelStabilizationRangeUnits.Enabled          = autoMode;

            // Pokud vybral Manual by sex, zakazu trideni (tyto 2 rezimy se vylucuji)
            if (comboBoxSavingMode.SelectedIndex == (int)SavingMode.MANUAL_BY_SEX) {
                comboBoxSortingMode.SelectedIndex = (int)WeightSorting.NONE;
                EnableSortingMode(false);
                SetSorting();       // Prekreslim jeste trideni (disabluji se editboxy hmotnosti mezi)
            } else {
                EnableSortingMode(true);
            }
        }

        private void RedrawFile(int index) {
            if (index < 0) {
                groupBoxDetails.Enabled = false;
                groupBoxDetails.Text    = "";
                return;
            }
            
            File file = scaleConfig.FileList.List[index];

            isLoading = true;
            try {
                // Nazev group boxu nastavim na jmeno souboru
                groupBoxDetails.Enabled = true;
                groupBoxDetails.Text = file.Name;

                // Poznamka
                textBoxNote.Text = file.Note;

                // Vazeni vice kusu
                if (scaleConfig.EnableFileParameters) {
                    // Vice kusu si ridi kazdy soubor zvlast
                    checkBoxEnableMoreBirds.Checked = file.FileConfig.WeighingConfig.Saving.EnableMoreBirds;
                } else {
                    // Povoleni vice kusu je globalni
                    checkBoxEnableMoreBirds.Checked = scaleConfig.WeighingConfig.Saving.EnableMoreBirds;
                }
                numericUpDownNumberOfBirds.Text = file.FileConfig.WeighingConfig.Saving.NumberOfBirds.ToString();

                // Trideni
                comboBoxSortingMode.SelectedIndex  = (int)file.FileConfig.WeighingConfig.WeightSorting.Mode;
                numericUpDownSortingLowLimit.Text  =      DisplayFormat.RoundWeight(file.FileConfig.WeighingConfig.WeightSorting.LowLimit, scaleConfig.Units.Units);
                numericUpDownSortingHighLimit.Text =      DisplayFormat.RoundWeight(file.FileConfig.WeighingConfig.WeightSorting.HighLimit, scaleConfig.Units.Units);

                // Ukladani
                comboBoxSavingMode.SelectedIndex           = (int)file.FileConfig.WeighingConfig.Saving.Mode;
                numericUpDownSavingFilter.Text             =      file.FileConfig.WeighingConfig.Saving.Filter.ToString();
                numericUpDownSavingMinimumWeight.Text      =      DisplayFormat.RoundWeight(file.FileConfig.WeighingConfig.Saving.MinimumWeight, scaleConfig.Units.Units);
                numericUpDownSavingStabilizationTime.Text  =      file.FileConfig.WeighingConfig.Saving.StabilisationTime.ToString();
                numericUpDownSavingStabilizationRange.Text =      file.FileConfig.WeighingConfig.Saving.StabilisationRange.ToString("N1");

                // Globalni nastaveni s vlivem na soubory
//                checkBoxEnableMoreBirds.Checked    = scaleConfig.WeighingConfig.Saving.EnableMoreBirds;

                // Povoleni zobrazeni na zaklade nastaveni
                SetMoreBirds();
                SetSorting();
                SetSaving();
            } finally {
                isLoading = false;
            }
        }
        
        public override void Redraw() {
            // Nastavim minimalni a maximalni hodnoty podle jednotek
            SetUnits();

            RedrawFile(listBoxFiles.SelectedIndex);
            
            RedrawUnits();
        }

        private void IntegerNumberKeyPress(object sender, KeyPressEventArgs e) {
            // Pustim jen cislice
            if (!KeyFilter.IsInt(e.KeyChar)) {
                e.Handled = true;
            }
        }

        public override void SetScaleConfig(ScaleConfig scaleConfig) {
            // Preberu config
            this.scaleConfig = scaleConfig;

            // Prekreslim
            isLoading = true;
            try {
                RedrawFileList();
                RedrawUnits();

                // Pokud je v seznamu nejaky soubor, zobrazim prvni
                if (scaleConfig.FileList.List.Count > 0) {
                    listBoxFiles.SelectedIndex = 0;
                }
                RedrawFile(listBoxFiles.SelectedIndex);
            } finally {
                isLoading = false;
            } 
        }

        private void SaveNote() {
            if (readOnly || isLoading) {
                return;
            }
            
            if (listBoxFiles.SelectedIndex < 0) {
                return;     // Neni vybrany zadny soubor
            }
            File file = scaleConfig.FileList.List[listBoxFiles.SelectedIndex];

            // Radsi projedu zadany text a vyhazim neplatne znaky (pomoci Ctrl+V muze vlozit cokoliv)
            string validNote = "";
            foreach (char ch in textBoxNote.Text) {
                if (!KeyFilter.IsScaleChar(ch)) {
                    continue;       // Neplatny znak vynecham
                }
                validNote += ch;
            }
            textBoxNote.Text = validNote;

            // Ulozim poznamku
            if (validNote == file.Note) {
                return;     // Poznamka zustala stejna
            }
            
            // Poznamka je zmenena, ulozim do skupiny
            file.SetNote(validNote);

            // Prekreslim seznam skupin (ukazde skupiny je v zavorce i poznamka) a preskocim zpet na aktualni skupinu
            int index = listBoxFiles.SelectedIndex;
            RedrawFileList();
            listBoxFiles.SelectedIndex = index;
        }

        private void CopyToFiles() {
            // Pokud nechce parametry pro kazdy soubor zvlast, prekopiruju globalni nastaveni ukladani do kazdeho souboru
            if (scaleConfig.EnableFileParameters) {
                return;
            }
            foreach (File file in scaleConfig.FileList.List) {
                // Kopiruju jen nastaveni souboru, pocet kusu vazenych naraz ponecham
                file.FileConfig.WeighingConfig.Saving.Mode = scaleConfig.WeighingConfig.Saving.Mode;
                file.FileConfig.WeighingConfig.Saving.EnableMoreBirds = scaleConfig.WeighingConfig.Saving.EnableMoreBirds;
                file.FileConfig.WeighingConfig.Saving.Filter = scaleConfig.WeighingConfig.Saving.Filter;
                file.FileConfig.WeighingConfig.Saving.StabilisationTime = scaleConfig.WeighingConfig.Saving.StabilisationTime;
                file.FileConfig.WeighingConfig.Saving.MinimumWeight = scaleConfig.WeighingConfig.Saving.MinimumWeight;
                file.FileConfig.WeighingConfig.Saving.StabilisationRange = scaleConfig.WeighingConfig.Saving.StabilisationRange;

                // Trideni, kopiruju pouze rezim, hmotnosti musim ponechat u kazdeho souboru zvlast i pri globalnich parametrech
                file.FileConfig.WeighingConfig.WeightSorting.Mode = scaleConfig.WeighingConfig.WeightSorting.Mode;
            }
        }

        private void ControlsToConfig() {
            if (readOnly || isLoading) {
                return;
            }

            if (listBoxFiles.SelectedIndex < 0) {
                return;
            }    
            
            File file = scaleConfig.FileList.List[listBoxFiles.SelectedIndex];

            // Vazeni vice kusu
            if (checkBoxEnableMoreBirds.Enabled) {
                // File-specific nastaveni je povoleno
                file.FileConfig.WeighingConfig.Saving.EnableMoreBirds = checkBoxEnableMoreBirds.Checked;
            }
            file.FileConfig.WeighingConfig.Saving.NumberOfBirds = (int)numericUpDownNumberOfBirds.Value;
            SetMoreBirds();

            // Trideni
            if (groupBoxSorting.Enabled) {
                // Trideni je povoleno
                file.FileConfig.WeighingConfig.WeightSorting.Mode = (WeightSorting)comboBoxSortingMode.SelectedIndex;
                switch (file.FileConfig.WeighingConfig.WeightSorting.Mode) {
                    case WeightSorting.NONE:
                        break;
                    
                    case WeightSorting.LIGHT_HEAVY:
                        file.FileConfig.WeighingConfig.WeightSorting.LowLimit = (double)numericUpDownSortingLowLimit.Value;
                        break;

                    case WeightSorting.LIGHT_OK_HEAVY:
                        double lowLimit  = (double)numericUpDownSortingLowLimit.Value;
                        double highLimit = (double)numericUpDownSortingHighLimit.Value;

                        if (highLimit <= lowLimit) {
                            // Spodni mez musi byt mensi nez horni mez, nastavim rucne na default hodnoty
                            lowLimit  = ConvertWeight.Convert(Bat1Version7.DefaultConfig.WeighingConfig.WeightSorting.LowLimit,
                                                              Bat1Version7.DefaultConfig.Units.Units,
                                                              scaleConfig.Units.Units);
                            highLimit = ConvertWeight.Convert(Bat1Version7.DefaultConfig.WeighingConfig.WeightSorting.HighLimit,
                                                              Bat1Version7.DefaultConfig.Units.Units,
                                                              scaleConfig.Units.Units);
                        }

                        file.FileConfig.WeighingConfig.WeightSorting.LowLimit  = lowLimit;
                        file.FileConfig.WeighingConfig.WeightSorting.HighLimit = highLimit;
                        break;
                }
            }
            SetSorting();

            // Ukladani
            if (scaleConfig.EnableFileParameters) {
                file.FileConfig.WeighingConfig.Saving.Mode               = (SavingMode)comboBoxSavingMode.SelectedIndex;
                file.FileConfig.WeighingConfig.Saving.Filter             = (double)numericUpDownSavingFilter.Value;
                file.FileConfig.WeighingConfig.Saving.MinimumWeight      = (double)numericUpDownSavingMinimumWeight.Value;
                if (file.FileConfig.WeighingConfig.Saving.Mode == SavingMode.AUTOMATIC) {
                    file.FileConfig.WeighingConfig.Saving.StabilisationTime  = (double)numericUpDownSavingStabilizationTime.Value;
                    file.FileConfig.WeighingConfig.Saving.StabilisationRange = (double)numericUpDownSavingStabilizationRange.Value;
                }
            }
            SetSaving();

            // Pokud pouziva globalni nastaveni, prekopiruju nastaveni ukladani do kazdeho souboru
            CopyToFiles();
        }

        private void SaveActiveFile() {
            if (scaleConfig.ActiveFileIndex < 0 || scaleConfig.ActiveFileIndex >= scaleConfig.FileList.List.Count) {
                activeFileName = "";
                return;
            }

            activeFileName = scaleConfig.FileList.List[scaleConfig.ActiveFileIndex].Name;
        }

        private void RestoreActiveFile() {
            if (activeFileName == "" || scaleConfig.FileList.List.Count == 0) {
                return;
            }

            scaleConfig.ActiveFileIndex = scaleConfig.FileList.GetIndex(activeFileName);
        }


        private void buttonNew_Click(object sender, EventArgs e) {
            listBoxFiles.Focus();       // Default dam focus zpet na seznam

            FormName form = new FormName(windowTitle, "", true);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }
            if (scaleConfig.FileList.Exists(form.EnteredName)) {
                MessageBox.Show(String.Format(Properties.Resources.FILE_ALREADY_EXISTS, form.EnteredName), windowTitle);
                return;
            }

            // Zapamatuju si aktivni soubor
            SaveActiveFile();

            scaleConfig.FileList.Add(form.EnteredName, scaleConfig);

            // Obnovim aktivni soubor
            RestoreActiveFile();

            // Pokud prave vytvoril prvni soubor, nastavim ho automaticky jako aktivni
            if (scaleConfig.FileList.List.Count == 1) {
                scaleConfig.ActiveFileIndex = 0;
            }

            // Oznamim vytvoreni noveho souboru vsem skupinam souboru
            scaleConfig.FileGroupList.CreateNewFile(scaleConfig.FileList.GetIndex(form.EnteredName));

            // Prekreslim a preskocim na prave vytvoreny soubor
            RedrawFileList();
            listBoxFiles.SelectedIndex = scaleConfig.FileList.GetIndex(form.EnteredName);
            listBoxFiles.Focus();
        }

        private void listBoxFiles_MouseDoubleClick(object sender, MouseEventArgs e) {
            if (readOnly) {
                return;
            }
            buttonRename_Click(sender, null);
        }

        private void buttonRename_Click(object sender, EventArgs e) {
            int index = listBoxFiles.SelectedIndex;
            listBoxFiles.Focus();       // Default dam focus zpet na seznam

            if (index < 0) {
                // Nevybral zadnou polozku
                return;
            }
            FormName form = new FormName(windowTitle, scaleConfig.FileList.List[index].Name, true);
            if (form.ShowDialog() != DialogResult.OK) {
                return;
            }

            if (form.EnteredName == scaleConfig.FileList.List[index].Name) {
                // Jmeno beze zmeny
                return;
            }

            // Zmenil jmeno
            if (scaleConfig.FileList.Exists(form.EnteredName)) {
                // Nove zadane jmeno uz existuje
                MessageBox.Show(String.Format(Properties.Resources.FILE_ALREADY_EXISTS, form.EnteredName), windowTitle);
                return;
            }

            // Zapamatuju si aktivni soubor
            SaveActiveFile();

            // Prejmenuju, zaroven dojde k novemu setrideni seznamu
            scaleConfig.FileList.Rename(index, form.EnteredName);

            // Pokud jsem prejmenoval prave aktivni soubor, udrzim ho jako aktivni. Jinak obnovim puvodne aktivni.
            if (index == scaleConfig.ActiveFileIndex) {
                scaleConfig.ActiveFileIndex = scaleConfig.FileList.GetIndex(form.EnteredName);
            } else {
                RestoreActiveFile();
            }

            // Oznamim prejmenovani souboru vsem skupinam souboru
            scaleConfig.FileGroupList.RenameFile(index, scaleConfig.FileList.GetIndex(form.EnteredName));

            // Prekreslim a preskocim na editovany soubor
            RedrawFileList();
            listBoxFiles.SelectedIndex = scaleConfig.FileList.GetIndex(form.EnteredName);
            listBoxFiles.Focus();
        }

        private void buttonDelete_Click(object sender, EventArgs e) {
            int index = listBoxFiles.SelectedIndex;
            listBoxFiles.Focus();       // Default dam focus zpet na seznam
            if (index < 0) {
                return;
            }

            if (MessageBox.Show(String.Format(Properties.Resources.DELETE_FILE,
                                scaleConfig.FileList.List[index].Name),
                                windowTitle, MessageBoxButtons.YesNo) != DialogResult.Yes) {
                return;
            }

            // Zapamatuju si aktivni soubor
            SaveActiveFile();

            scaleConfig.FileList.Delete(index);
            
            // Pokud jsem smazal prave aktivni soubor, nastavim jako aktivni prvni soubor.
            // Jinak obnovim puvodne aktivni.
            if (index == scaleConfig.ActiveFileIndex) {
                if (scaleConfig.FileList.List.Count > 0) {
                    scaleConfig.ActiveFileIndex = 0;
                } else {
                    scaleConfig.ActiveFileIndex = -1;       // Prazdny seznam
                }
            } else {
                RestoreActiveFile();
            }

            // Oznamim smazani souboru vsem skupinam souboru
            scaleConfig.FileGroupList.DeleteFile(index);
            
            // Prekreslim a focus nastavim na nejblizsi polozku
            RedrawFileList();
            if (scaleConfig.FileList.List.Count == 0) {
                // Seznam uz je prazdny
                index = -1;
            } else if (index >= scaleConfig.FileList.List.Count) {
                // Smazal posledni polozku v seznamu, prejdu na predposledni
                index = scaleConfig.FileList.List.Count - 1;
            } // else ponecham na polozce za smazanou polozkou
            listBoxFiles.SelectedIndex = index;
            listBoxFiles.Focus();
            RedrawFile(index);
        }

        private void listBoxFiles_SelectedIndexChanged(object sender, EventArgs e) {
            RedrawFile(listBoxFiles.SelectedIndex);
        }

        private void comboBox_SelectionChangeCommitted(object sender, EventArgs e) {
            ControlsToConfig();
        }

        private void textBoxNote_KeyPress(object sender, KeyPressEventArgs e) {
            e.KeyChar = Char.ToUpper(e.KeyChar);
            if (!KeyFilter.IsScaleChar(e.KeyChar)) {
                e.Handled = true;
            }
        }

        private void textBoxNote_Leave(object sender, EventArgs e) {
            SaveNote();
        }

        private void textBoxNote_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == System.Windows.Forms.Keys.Enter) {
                SaveNote();     // Po stisku Enter ulozim
            }
        }

        private void buttonActive_Click(object sender, EventArgs e) {
            int index = listBoxFiles.SelectedIndex;
            listBoxFiles.Focus();       // Default dam focus zpet na seznam
            if (index < 0) {
                return;
            }

            // Oznacim vybrany soubor jako aktivni
            scaleConfig.ActiveFileIndex = index;

            // Prekreslim seznam souboru a ponecham vybrany soubor
            RedrawFileList();
            listBoxFiles.SelectedIndex = index;
            listBoxFiles.Focus();
        }

    }
}
