﻿namespace Bat1Diag {
    partial class FormDiagMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.buttonUploadLogo = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.buttonUploadLogoAgain = new System.Windows.Forms.Button();
            this.buttonWriteDiagnostics = new System.Windows.Forms.Button();
            this.labelVersion = new System.Windows.Forms.Label();
            this.buttonWriteDemoData = new System.Windows.Forms.Button();
            this.buttonWriteLargeDemoData = new System.Windows.Forms.Button();
            this.buttonManyFiles = new System.Windows.Forms.Button();
            this.buttonOneBigFile = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonUploadLogo
            // 
            this.buttonUploadLogo.Location = new System.Drawing.Point(12, 37);
            this.buttonUploadLogo.Name = "buttonUploadLogo";
            this.buttonUploadLogo.Size = new System.Drawing.Size(135, 23);
            this.buttonUploadLogo.TabIndex = 0;
            this.buttonUploadLogo.Text = "Upload logo";
            this.buttonUploadLogo.UseVisualStyleBackColor = true;
            this.buttonUploadLogo.Click += new System.EventHandler(this.buttonUploadLogo_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.DefaultExt = "bmp";
            this.openFileDialog.Filter = "*.bmp|*.bmp";
            // 
            // buttonUploadLogoAgain
            // 
            this.buttonUploadLogoAgain.Enabled = false;
            this.buttonUploadLogoAgain.Location = new System.Drawing.Point(153, 37);
            this.buttonUploadLogoAgain.Name = "buttonUploadLogoAgain";
            this.buttonUploadLogoAgain.Size = new System.Drawing.Size(75, 23);
            this.buttonUploadLogoAgain.TabIndex = 1;
            this.buttonUploadLogoAgain.Text = "Again";
            this.buttonUploadLogoAgain.UseVisualStyleBackColor = true;
            this.buttonUploadLogoAgain.Click += new System.EventHandler(this.buttonUploadLogoAgain_Click);
            // 
            // buttonWriteDiagnostics
            // 
            this.buttonWriteDiagnostics.Location = new System.Drawing.Point(12, 80);
            this.buttonWriteDiagnostics.Name = "buttonWriteDiagnostics";
            this.buttonWriteDiagnostics.Size = new System.Drawing.Size(216, 23);
            this.buttonWriteDiagnostics.TabIndex = 2;
            this.buttonWriteDiagnostics.Text = "Write diagnostics to scale";
            this.buttonWriteDiagnostics.UseVisualStyleBackColor = true;
            this.buttonWriteDiagnostics.Click += new System.EventHandler(this.buttonWriteDiagnostics_Click);
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(12, 9);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(64, 13);
            this.labelVersion.TabIndex = 3;
            this.labelVersion.Text = "labelVersion";
            // 
            // buttonWriteDemoData
            // 
            this.buttonWriteDemoData.Location = new System.Drawing.Point(12, 123);
            this.buttonWriteDemoData.Name = "buttonWriteDemoData";
            this.buttonWriteDemoData.Size = new System.Drawing.Size(216, 23);
            this.buttonWriteDemoData.TabIndex = 4;
            this.buttonWriteDemoData.Text = "Write demo data to scale";
            this.buttonWriteDemoData.UseVisualStyleBackColor = true;
            this.buttonWriteDemoData.Click += new System.EventHandler(this.buttonWriteDemoData_Click);
            // 
            // buttonWriteLargeDemoData
            // 
            this.buttonWriteLargeDemoData.Location = new System.Drawing.Point(12, 152);
            this.buttonWriteLargeDemoData.Name = "buttonWriteLargeDemoData";
            this.buttonWriteLargeDemoData.Size = new System.Drawing.Size(216, 23);
            this.buttonWriteLargeDemoData.TabIndex = 5;
            this.buttonWriteLargeDemoData.Text = "Write large demo data to scale";
            this.buttonWriteLargeDemoData.UseVisualStyleBackColor = true;
            this.buttonWriteLargeDemoData.Click += new System.EventHandler(this.buttonWriteLargeDemoData_Click);
            // 
            // buttonManyFiles
            // 
            this.buttonManyFiles.Location = new System.Drawing.Point(12, 181);
            this.buttonManyFiles.Name = "buttonManyFiles";
            this.buttonManyFiles.Size = new System.Drawing.Size(216, 23);
            this.buttonManyFiles.TabIndex = 6;
            this.buttonManyFiles.Text = "Write many files to the scale";
            this.buttonManyFiles.UseVisualStyleBackColor = true;
            this.buttonManyFiles.Click += new System.EventHandler(this.buttonManyFiles_Click);
            // 
            // buttonOneBigFile
            // 
            this.buttonOneBigFile.Location = new System.Drawing.Point(12, 210);
            this.buttonOneBigFile.Name = "buttonOneBigFile";
            this.buttonOneBigFile.Size = new System.Drawing.Size(216, 23);
            this.buttonOneBigFile.TabIndex = 7;
            this.buttonOneBigFile.Text = "Write one big file to the scale";
            this.buttonOneBigFile.UseVisualStyleBackColor = true;
            this.buttonOneBigFile.Click += new System.EventHandler(this.buttonOneBigFile_Click);
            // 
            // FormDiagMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 245);
            this.Controls.Add(this.buttonOneBigFile);
            this.Controls.Add(this.buttonManyFiles);
            this.Controls.Add(this.buttonWriteLargeDemoData);
            this.Controls.Add(this.buttonWriteDemoData);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.buttonWriteDiagnostics);
            this.Controls.Add(this.buttonUploadLogoAgain);
            this.Controls.Add(this.buttonUploadLogo);
            this.Name = "FormDiagMain";
            this.Text = "BAT1 scale diagnostics";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonUploadLogo;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button buttonUploadLogoAgain;
        private System.Windows.Forms.Button buttonWriteDiagnostics;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Button buttonWriteDemoData;
        private System.Windows.Forms.Button buttonWriteLargeDemoData;
        private System.Windows.Forms.Button buttonManyFiles;
        private System.Windows.Forms.Button buttonOneBigFile;
    }
}

