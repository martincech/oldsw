//*****************************************************************************
//
//   MBCom.h     Modbus serial communication
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBCom_H__
   #define __MBCom_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MBDef_H__
   #include "MBDef.h"
#endif

// pomocna makra :
#define ComDisableInt()   ES  = 0
#define ComEnableInt()    ES  = 1
#define ComEnableRx()     REN = 1
#define ComDisableRx()    REN = 0
#define ComStartTx()      TI  = 1

// makro pro timer :
#define ComStopRx()       ComDisableRx();ComDisableInt()
#define ComStopTx()       ComDisableInt()


// Stav zarizeni :
typedef enum {
   COM_STATUS_STOPPED,                 // zastaveno, ceka na povel
   COM_STATUS_RX_RUNNING,              // bezi Rx
   COM_STATUS_RX_DONE,                 // dokoncen prijem, prevezmi data
   COM_STATUS_RX_PARITY,               // dokoncen prijem, chyba parity
   COM_STATUS_RX_OVERRUN,              // dokoncen prijem, pocet znaku presahl velikost bufferu
   COM_STATUS_RX_TIMEOUT,              // dokoncen prijem, timeout
   COM_STATUS_TX_RUNNING,              // bezi Tx
   _COM_STATUS_LAST
} TComStatus;

// Rezim :
typedef enum {
   COM_MODE_NONE,
   COM_MODE_TIMEOUT = 0x01,            // meziznakovy timeout
   COM_MODE_LEADER  = 0x02,            // vedouci znak
   COM_MODE_TRAILER = 0x04,            // ukoncovaci znak
   _COM_MODE_LAST
} TComMode;

// Globalni data :

extern byte __xdata__ ComBuffer[ COM_DATA_SIZE];   // datovy buffer
extern com_pointer_t  ComDataSize;                 // delka prijatych/vysilanych dat

extern byte volatile ComStatus;          // stav zarizeni
extern word volatile ComTimeoutCounter;  // citac meziznakoveho timeoutu



TYesNo ComInit( word Baud, byte Parity, byte Mode);
// Inicializace komunikace

void ComSetTimeout( word ms);
// Nastaveni meziznakoveho timeoutu

void ComSetTxSpace( word ms);
// Nastaveni mezery mezi Rx a Tx

#define ComTimeoutDone()    ComStopRx();ComStatus = COM_STATUS_RX_TIMEOUT
// trigger v preruseni casovace

void ComTxStart( void);
// Zahaji vysilani bufferu

void ComRxStart( void);
// Zahaji prijem do bufferu

void ComStopAll( void);
// Ukonci praci v preruseni (Rx i Tx)

void ComDiag( void);
// Print diagnostics

#endif

