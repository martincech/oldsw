//******************************************************************************
//
//   MBPdu.h         Modbus PDU definitions
//   Version 1.0 (c) VymOs
//
//******************************************************************************

#ifndef __MBPdu_H__
   #define __MBPdu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//-----------------------------------------------------------------------------
// Pomocne funkce
//-----------------------------------------------------------------------------

#define MBBitsToBytes( Bits)   (((Bits) + 7) / 8)        // pocet bytu pro pocet bitu

#ifdef __C51__
   // Keil
   #define MBGetWord( Data) (Data)
   // Konverze na big endian
#else
   // Borland
   word MBGetWord( word Data);
   // Konverze na big endian
#endif

#define MB_MIN_ITEM_ADDRESS   0x0000        // minimalni adresa veliciny
#define MB_MAX_ITEM_ADDRESS   0xFFFF        // maximalni adresa veliciny

//-----------------------------------------------------------------------------
// Kody vyjimek
//-----------------------------------------------------------------------------

typedef enum {
   MBEX_NONE                     = 0x00,
   MBEX_ILLEGAL_FUNCTION         = 0x01,   // function code not served
   MBEX_ILLEGAL_DATA_ADDRESS     = 0x02,   // data address not allowable
   MBEX_ILLEGAL_DATA_VALUE       = 0x03,   // data in the query is not allowable
   MBEX_SLAVE_DEVICE_FAILURE     = 0x04,   // unrecoverable error
   MBEX_ACKNOWLEDGE              = 0x05,   // programming is running, wait for response
   MBEX_SLAVE_DEVICE_BUSY        = 0x06,   // long duration program is running, wait for response
   MBEX_MEMORY_PARITY_ERROR      = 0x08,   // memory consistency error
   MBEX_GATEWAY_PATH_UNAVAILABLE = 0x0A,   // gateway processing - no path to target
   MBEX_GATEWAY_TARGET_FAILED    = 0x0B,   // gateway processing - no response by target
   _MBEX_LAST
} TMBExceptionType;

//-----------------------------------------------------------------------------
// Stav seriove komunikace
//-----------------------------------------------------------------------------

typedef struct {
   word ExceptionStatus;               // maska vyjimek
   word TotalCount;                    // total count of received messages (own and foreign)
   word ErrorCount;                    // CRC errors
   word ExceptionCount;                // exception responses
   word MessageCount;                  // messages to device or broadcast
   word NoResponseCount;               // messages without response (eg. broadcast)
   word NakCount;                      // negative acknowledge exceptions
   word BusyCount;                     // slave busy exception responses
   word OverrunCount;                  // overrun bus characters
   word ComEventCount;                 // pocet zpracovanych povelu
} TMBComStatus;

//-----------------------------------------------------------------------------
// Funkcni kody
//-----------------------------------------------------------------------------

typedef enum {
   MBF_READ_COILS               = 0x01,
   MBF_READ_DISCRETE            = 0x02,
   MBF_READ_HOLDING_REGISTERS   = 0x03,
   MBF_READ_INPUT_REGISTERS     = 0x04,
   MBF_WRITE_SINGLE_COIL        = 0x05,
   MBF_WRITE_SINGLE_REGISTER    = 0x06,
   MBF_READ_EXCEPTION_STATUS    = 0x07,
   MBF_DIAGNOSTIC               = 0x08,
   MBF_GET_COM_EVENT_COUNTER    = 0x0B,
   MBF_GET_COM_EVENT_LOG        = 0x0C,
   MBF_WRITE_MULTIPLE_COILS     = 0x0F,
   MBF_WRITE_MULTIPLE_REGISTERS = 0x10,
   MBF_REPORT_SLAVE_ID          = 0x11,
   MBF_READ_FILE_RECORD         = 0x14,
   MBF_WRITE_FILE_RECORD        = 0x15,
   MBF_MASK_WRITE_REGISTER      = 0x16,
   MBF_RW_MULTIPLE_REGISTERS    = 0x17,
   MBF_READ_FIFO_QUEUE          = 0x18,
   MBF_READ_DEVICE_ID           = 0x2B,
   _MBF_LAST
} TMBFunction;

//-----------------------------------------------------------------------------
// Exception
//-----------------------------------------------------------------------------

typedef struct {
   byte Function;
   byte Code;
} TExceptionReply;


//-----------------------------------------------------------------------------
// Read discrete
//-----------------------------------------------------------------------------

#define MB_MAX_READ_DISCRETE_COUNT 2000

typedef struct {
   byte Function;
   word Address;
   word Count;
} TReadDiscrete;

// reply :
typedef struct {
   byte Function;
   byte ByteCount;
   byte Data[ 1];
} TReadDiscreteReply;

#define SizeofTReadDiscreteReply() (sizeof( TReadDiscreteReply) - 1)

//-----------------------------------------------------------------------------
// Read coils
//-----------------------------------------------------------------------------

#define MB_MAX_READ_COILS_COUNT 2000

typedef struct {
   byte Function;
   word Address;
   word Count;
} TReadCoils;

// reply :
typedef struct {
   byte Function;
   byte ByteCount;
   byte Data[ 1];
} TReadCoilsReply;

#define SizeofTReadCoilsReply() (sizeof( TReadCoilsReply) - 1)

//-----------------------------------------------------------------------------
// Write Single Coil
//-----------------------------------------------------------------------------

#define MB_COIL_ON     0xFF00
#define MB_COIL_OFF    0x0000

typedef struct {
   byte Function;
   word Address;
   word Value;
} TWriteSingleCoil;

// reply :
typedef TWriteSingleCoil TWriteSingleCoilReply;

//-----------------------------------------------------------------------------
// Write coils
//-----------------------------------------------------------------------------

#define MB_MAX_WRITE_COILS_COUNT 1968

typedef struct {
   byte Function;
   word Address;
   word Count;
   byte ByteCount;
   byte Data[ 1];
} TWriteCoils;

#define SizeofTWriteCoils() (sizeof( TWriteCoils) - 1)

// reply :
typedef struct {
   byte Function;
   word Address;
   word Count;
} TWriteCoilsReply;

//-----------------------------------------------------------------------------
// Read Input Registers
//-----------------------------------------------------------------------------

#define MB_MAX_READ_INPUT_REGISTERS_COUNT 125

typedef struct {
   byte Function;
   word Address;
   word Count;
} TReadInputRegisters;

// reply :
typedef struct {
   byte Function;
   byte ByteCount;
   word Data[ 1];
} TReadInputRegistersReply;

#define SizeofTReadInputRegistersReply() (sizeof( TReadInputRegistersReply) - 2)

//-----------------------------------------------------------------------------
// Read (holding) registers
//-----------------------------------------------------------------------------

#define MB_MAX_READ_REGISTERS_COUNT 125

typedef struct {
   byte Function;
   word Address;
   word Count;
} TReadRegisters;

// reply :
typedef struct {
   byte Function;
   byte ByteCount;
   word Data[ 1];
} TReadRegistersReply;

#define SizeofTReadRegistersReply() (sizeof( TReadRegistersReply) - 2)

//-----------------------------------------------------------------------------
// Write Single (holding) Register
//-----------------------------------------------------------------------------

typedef struct {
   byte Function;
   word Address;
   word Value;
} TWriteSingleRegister;

// reply :
typedef TWriteSingleRegister TWriteSingleRegisterReply;

//-----------------------------------------------------------------------------
// Write (multiple) registers
//-----------------------------------------------------------------------------

#define MB_MAX_WRITE_REGISTERS_COUNT 120

typedef struct {
   byte Function;
   word Address;
   word Count;
   byte ByteCount;
   word Data[ 1];
} TWriteRegisters;

#define SizeofTWriteRegisters() (sizeof( TWriteRegisters) - 2)

// reply :
typedef struct {
   byte Function;
   word Address;
   word Count;
} TWriteRegistersReply;

//-----------------------------------------------------------------------------
// Mask write register
//-----------------------------------------------------------------------------

typedef struct {
   byte Function;
   word Address;
   word AndMask;
   word OrMask;
} TMaskWriteRegister;

// reply :
typedef TMaskWriteRegister TMaskWriteRegisterReply;

//-----------------------------------------------------------------------------
// Read / Write (multiple) registers
//-----------------------------------------------------------------------------

#define MB_MAX_RW_READ_REGISTERS_COUNT  118  // upravit podle delky paketu
#define MB_MAX_RW_WRITE_REGISTERS_COUNT 118  // upravit podle delky paketu

typedef struct {
   byte Function;
   word ReadAddress;
   word ReadCount;
   word WriteAddress;
   word WriteCount;
   byte WriteByteCount;
   word Data[ 1];                      // zapisovana data
} TReadWriteRegisters;

#define SizeofTReadWriteRegisters() (sizeof( TReadWriteRegisters) - 2)

// reply :
typedef struct {
   byte Function;
   byte ByteCount;
   word Data[ 1];                      // prectena data
} TReadWriteRegistersReply;

#define SizeofTReadWriteRegistersReply() (sizeof( TReadWriteRegistersReply) - 2)

//-----------------------------------------------------------------------------
// Read FIFO queue
//-----------------------------------------------------------------------------

#define MB_MAX_FIFO_COUNT 31

typedef struct {
   byte Function;
   word Address;
} TReadFifoQueue;

// reply :
typedef struct {
   byte Function;
   byte ByteCount;
   word FifoCount;
   word Data[ 1];
} TReadFifoQueueReply;

#define SizeofTReadFifoQueueReply() (sizeof( TReadFifoQueueReply) - 2)

//-----------------------------------------------------------------------------
// Exception status
//-----------------------------------------------------------------------------

typedef struct {
   byte Function;
} TExceptionStatus;

// reply :
typedef struct {
   byte Function;
   byte Status;
} TExceptionStatusReply;

//-----------------------------------------------------------------------------
// Diagnostic
//-----------------------------------------------------------------------------

typedef enum {
   MBD_RETURN_QUERY_DATA          = 0x00,
   MBD_RESTART_COMMUNICATION      = 0x01,
   MBD_RETURN_DIAGNOSTIC_REGISTER = 0x02,
   MBD_CHANGE_DELIMITER           = 0x03,
   MBD_LISTEN_ONLY                = 0x04,
   MBD_CLEAR_STATUS               = 0x0A,
   MBD_TOTAL_COUNT                = 0x0B,
   MBD_ERROR_COUNT                = 0x0C,
   MBD_EXCEPTION_COUNT            = 0x0D,
   MBD_MESSAGE_COUNT              = 0x0E,
   MBD_NO_RESPONSE_COUNT          = 0x0F,
   MBD_NAK_COUNT                  = 0x10,
   MBD_BUSY_COUNT                 = 0x11,
   MBD_OVERRUN_COUNT              = 0x12,
   MBD_CLEAR_OVERRUN_COUNT        = 0x14,
   _MBD_LAST
} TDiagnosticSubFunction;

typedef struct {
   byte Function;
   word SubFunction;
   word Data;
} TDiagnostic;

// reply :
typedef TDiagnostic TDiagnosticReply;

//-----------------------------------------------------------------------------
// Com Event Counter
//-----------------------------------------------------------------------------

typedef struct {
   byte Function;
} TComEventCounter;

// reply :
typedef struct {
   byte Function;
   word Status;
   word EventCount;
} TComEventCounterReply;

//-----------------------------------------------------------------------------
// Com Event Log
//-----------------------------------------------------------------------------

#define MB_MAX_EVENT_COUNT 64

typedef struct {
   byte Function;
} TComEventLog;

// reply :
typedef struct {
   byte Function;
   byte ByteCount;
   word Status;
   word EventCount;
   word TotalCount;
   word Log[ 1];
} TComEventLogReply;

#define SizeofTComEventLogReply() (sizeof( TComEventLogReply) - 2)

//-----------------------------------------------------------------------------
// Slave ID
//-----------------------------------------------------------------------------

#define SID_ADDITIONAL_DATA_SIZE 4

typedef word TSlaveIdData;                          // device specific size

typedef struct {
   byte Function;
} TSlaveId;

// reply :
typedef struct {
   byte            Function;
   byte            ByteCount;
   TSlaveIdData    SlaveId;
   byte            RunIndicator;
   byte            Data[ SID_ADDITIONAL_DATA_SIZE];
} TSlaveIdReply;

//-----------------------------------------------------------------------------
// Encapsulated service interface
//-----------------------------------------------------------------------------

#define MB_MEI_DEVICE_IDENTIFICATION 0x0E

// kod identifikace :
typedef enum {
   MEI_BASIC_IDENTIFICATION     = 0x01,
   MEI_REGULAR_IDENTIFICATION   = 0x02,
   MEI_EXTENDED_IDENTIFICATION  = 0x03,
   MEI_SPECIFIC_IDENTIFICATION  = 0x04,
   _MEI_LAST
} TIdCode;

// konformita :
typedef enum {
   // stream access only :
   CL_BASIC_STREAM     = 0x01,
   CL_REGULAR_STREAM   = 0x02,
   CL_EXTENDED_STREAM  = 0x03,
   // stream and individual access :
   CL_BASIC_SINGLE     = 0x81,
   CL_REGULAR_SINGLE   = 0x82,
   CL_EXTENDED_SINGLE  = 0x83,
   _CL_LAST
} TConfirmityLevel;

// identifikace objektu :
typedef enum {
   // basic / mandatory :
   MEI_OBJECT_VENDOR       = 0x00,
   MEI_OBJECT_PRODUCT_CODE = 0x01,
   MEI_OBJECT_REVISION     = 0x02,
   // regular :
   MEI_OBJECT_URL          = 0x03,   // vendor URL
   MEI_OBJECT_PRODUCT      = 0x04,   // product name
   MEI_OBJECT_MODEL        = 0x05,   // mode name
   MEI_OBJECT_APPLICATION  = 0x06,   // application name
   // extended
   _MEI_FIRST_EXTENDED     = 0x80,
   _MEI_OBJECT_LAST
} TObjectId;

typedef struct {
   byte Function;
   byte MEIType;
   byte IdCode;
   byte ObjectId;
} TIdentification;

// reply :

// zaznam o objektu :
typedef struct {
   byte Id;
   byte Length;
   byte Data[ 1];
} TObjectData;

#define SizeofTObjectData() (sizeof(TObjectData) - 1)

typedef struct {
   byte Function;
   byte MEIType;
   byte IdCode;
   byte ConfirmityLevel;
   byte MoreFollows;
   byte NextObjectId;
   byte NumberOfObjects;
   TObjectData Object[ 1];
} TIdentificationReply;

#define SizeofTIdentificationReply() (sizeof( TIdentificationReply) - sizeof(TObjectData))

//-----------------------------------------------------------------------------
// Union prijatych PDU
//-----------------------------------------------------------------------------

typedef union {
   byte                 Function;                      // samotny funkcni kod
   TReadDiscrete        ReadDiscrete;
   TReadCoils           ReadCoils;
   TWriteSingleCoil     WriteSingleCoil;
   TWriteCoils          WriteCoils;
   TReadInputRegisters  ReadInputRegisters;
   TReadRegisters       ReadRegisters;
   TWriteSingleRegister WriteSingleRegister;
   TWriteRegisters      WriteRegisters;
   TMaskWriteRegister   MaskWriteRegister;
   TReadWriteRegisters  ReadWriteRegisters;
   TReadFifoQueue       ReadFifoQueue;
   TExceptionStatus     ExceptionStatus;
   TDiagnostic          Diagnostic;
   TComEventCounter     ComEventCounter;
   TSlaveId             SlaveId;
   TIdentification      Identification;
} TMBPdu;

//-----------------------------------------------------------------------------
// Union vyslanych PDU
//-----------------------------------------------------------------------------

typedef union {
   byte                      Function;                  // samotny funkcni kod
   TExceptionReply           Exception;
   TReadDiscreteReply        ReadDiscrete;
   TReadCoilsReply           ReadCoils;
   TWriteSingleCoilReply     WriteSingleCoil;
   TWriteCoilsReply          WriteCoils;
   TReadInputRegistersReply  ReadInputRegisters;
   TReadRegistersReply       ReadRegisters;
   TWriteSingleRegisterReply WriteSingleRegister;
   TWriteRegistersReply      WriteRegisters;
   TMaskWriteRegisterReply   MaskWriteRegister;
   TReadWriteRegistersReply  ReadWriteRegisters;
   TReadFifoQueueReply       ReadFifoQueue;
   TExceptionStatusReply     ExceptionStatus;
   TDiagnosticReply          Diagnostic;
   TComEventCounterReply     ComEventCounter;
   TSlaveIdReply             SlaveId;
   TIdentificationReply      Identification;
} TMBPduReply;

#endif
