//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
USERES("Tester.res");
USEFORM("Main.cpp", MainForm);
USEUNIT("Workplace.cpp");
USEFORM("Properties.cpp", PropertiesForm);
USEFORM("..\UartSetup.cpp", UartSetupDialog);
USEUNIT("..\..\CRT\Crt.cpp");
USEUNIT("..\CrtLogger.cpp");
USEUNIT("..\..\Memory\ObjectMemory.cpp");
USEUNIT("AsyUart.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TMainForm), &MainForm);
                 Application->CreateForm(__classid(TPropertiesForm), &PropertiesForm);
                 Application->CreateForm(__classid(TUartSetupDialog), &UartSetupDialog);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        return 0;
}
//---------------------------------------------------------------------------
