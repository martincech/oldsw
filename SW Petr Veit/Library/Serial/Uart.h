//******************************************************************************
//
//   Uart.h       Abstract asynchronous serial device RS232/RS422/RS485
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#ifndef UartH
   #define UartH

#ifndef SerialH
   #include "Serial.h"
#endif

//******************************************************************************
// TUart
//******************************************************************************

class TUart : virtual public TSerial
{
public :
   //---------------- UART parameters

   // Handshake Type :
   typedef enum {
      NO_HANDSHAKE,
      HARDWARE_HANDSHAKE,
      XON_XOFF_HANDSHAKE,
      HALF_DUPLEX_HANDSHAKE, // RS485 via RTS
   } THandshake;

   // Parity Type :
   typedef enum {
      NO_PARITY,
      ODD_PARITY,
      EVEN_PARITY,
      MARK_PARITY,
      SPACE_PARITY,
   } TParity;

   // Communication parameters :
   typedef struct {
      int        BaudRate;   // 300,600...115200
      int        DataBits;   // 5..8
      int        StopBits;   // stop bit count * 10 (10,15,20)
      TParity    Parity;     // Constants via TParity
      THandshake Handshake;  // Constants via THandshake
   } TParameters;

   //---------------- Functions

   TUart();
   // Constructor
   virtual ~TUart();
   // Destructor

   //--- inherited from TSerial :

   virtual bool Open( TIdentifier Identifier) = 0;
   // Open device by <Identifier>
   virtual void Close() = 0;
   // Close device
   virtual int Write( void *Data, int Length) = 0;
   // Write <Data> of size <Length>, returns written length (or 0 if error)
   virtual int Read( void *Data, int Length) = 0;
   // Read data <Data> of size <Length>, returns true length (or 0 if error)
   virtual void Flush() = 0;
   // Make input/output queue empty
#ifdef __PERSISTENT__
   virtual bool Load( TObjectMemory *Memory) = 0;
   // Load setup from <Memory>
   virtual void Save( TObjectMemory *Memory) = 0;
   // Save setup to <Memory>
#endif

   //--- UART functionality :

   virtual void SetRxNowait() = 0;
   // Set timing of receiver - returns collected data immediately
   virtual void SetRxWait( int ReplyTime, int IntercharacterTime) = 0;
   // Set timing of receiver :
   // If <IntercharacterTime> = 0 waits <ReplyTime> for whole message
   // If <IntercharacterTime> > 0 waits <ReplyTime> for first
   // character, next waits <IntercharacterTime> * count of characters
   virtual bool SetParameters( const TParameters &Parameters) = 0;
   // Set parameters of communication
   virtual void GetParameters( TParameters &Parameters)       = 0;
   // Get parameters of communication

  __property bool CTS                         = {read=GetCTS};
  __property bool DSR                         = {read=GetDSR};
  __property bool RI                          = {read=GetRI};
  __property bool DCD                         = {read=GetDCD};
  __property bool DTR                         = {write=SetDTR, read=FDtr};
  __property bool RTS                         = {write=SetRTS, read=FRts};
  __property bool TxD                         = {write=SetTxD, read=FTxD};

//------------------------------------------------------------------------------
protected :
   bool        FDtr;
   bool        FRts;
   bool        FTxD;
   TParameters FParameters;

   //--- Inherited from TSerial :
   virtual TName GetName()   = 0;
   virtual bool  GetIsOpen() = 0;
   //--- Own property setters/getters :
   virtual bool  GetCTS() = 0;
   virtual bool  GetDSR() = 0;
   virtual bool  GetRI()  = 0;
   virtual bool  GetDCD() = 0;
   virtual void  SetDTR(bool Status) = 0;
   virtual void  SetRTS(bool Status) = 0;
   virtual void  SetTxD(bool Status) = 0;
}; // TUart

//******************************************************************************
// inline
//******************************************************************************

inline TUart::TUart(): TSerial()
// Constructor
{
} // TUart

inline TUart::~TUart()
{
} // ~TUart

#endif
