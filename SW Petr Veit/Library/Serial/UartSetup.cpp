//******************************************************************************
//
//   UartSetup.cpp UART parameters dialog
//   Version 1.0   (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "UartSetup.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

TUartSetupDialog *UartSetupDialog;

//******************************************************************************
// Constructor
//******************************************************************************

__fastcall TUartSetupDialog::TUartSetupDialog(TComponent* Owner)
   : TForm(Owner)
{
} // TUartSetupDialog

//******************************************************************************
// Run configuration
//******************************************************************************

bool __fastcall TUartSetupDialog::Execute( TUart::TParameters &Parameters, TString Caption)
// Run setup dialog modal
{
   // Title
   this->Caption = Caption;
   // Default Button is Cancel :
   this->ActiveControl = UartSetupCancel;
   SetupByParameters( Parameters);
   // Show dialogu :
   if( ShowModal() != mrOk){
      return( false);  // cancel
   }
   // Translate backu to Parameters :
   ConvertToParameters( Parameters);
   return( true);
} // UartSetup

//******************************************************************************
// Nastaveni dialogu podle Parameters
//******************************************************************************

void __fastcall TUartSetupDialog::SetupByParameters( TUart::TParameters &Parameters)
// Nastaveni dialogu podle Parameters
{
   // Baudy
   switch( Parameters.BaudRate){
      case 300 :
         UartSetupBaudRate->ItemIndex = 0;
         break;
      case 600 :
         UartSetupBaudRate->ItemIndex = 1;
         break;
      case 1200 :
         UartSetupBaudRate->ItemIndex = 2;
         break;
      case 2400 :
         UartSetupBaudRate->ItemIndex = 3;
         break;
      case 4800 :
         UartSetupBaudRate->ItemIndex = 4;
         break;
      case 9600 :
      default :
         UartSetupBaudRate->ItemIndex = 5;
         break;
      case 19200 :
         UartSetupBaudRate->ItemIndex = 6;
         break;
      case 38400 :
         UartSetupBaudRate->ItemIndex = 7;
         break;
      case 57600 :
         UartSetupBaudRate->ItemIndex = 8;
         break;
      case 115200 :
         UartSetupBaudRate->ItemIndex = 9;
         break;
   }
   // Data bits :
   switch( Parameters.DataBits){
      case 5 :
         UartSetupDataBits->ItemIndex = 0;
         break;
      case 6 :
         UartSetupDataBits->ItemIndex = 1;
         break;
      case 7 :
         UartSetupDataBits->ItemIndex = 2;
         break;
      case 8 :
      default :
         UartSetupDataBits->ItemIndex = 3;
         break;
   }
   // Parity :
   switch( Parameters.Parity){
      case TUart::NO_PARITY :
      default :
         UartSetupParity->ItemIndex = 0;
         break;
      case TUart::ODD_PARITY :
         UartSetupParity->ItemIndex = 1;
         break;
      case TUart::EVEN_PARITY :
         UartSetupParity->ItemIndex = 2;
         break;
      case TUart::MARK_PARITY :
         UartSetupParity->ItemIndex = 3;
         break;
      case TUart::SPACE_PARITY :
         UartSetupParity->ItemIndex = 4;
         break;
   }
   // Stop Bits :
   switch( Parameters.StopBits){
      case 10 :
      default :
         UartSetupStopBits->ItemIndex = 0;
         break;
      case 15 :
         UartSetupStopBits->ItemIndex = 1;
         break;
      case 20 :
         UartSetupStopBits->ItemIndex = 2;
         break;
   }
   // Handshake :
   switch( Parameters.Handshake){
      case TUart::NO_HANDSHAKE :
      default :
         UartSetupHandshake->ItemIndex = 0;
         break;
      case TUart::HARDWARE_HANDSHAKE :
         UartSetupHandshake->ItemIndex = 1;
         break;
      case TUart::XON_XOFF_HANDSHAKE :
         UartSetupHandshake->ItemIndex = 2;
         break;
      case TUart::HALF_DUPLEX_HANDSHAKE :
         UartSetupHandshake->ItemIndex = 3;
         break;
   }
} // SetupByParameters

//******************************************************************************
//  Set Parameters by dialog
//******************************************************************************

void __fastcall TUartSetupDialog::ConvertToParameters( TUart::TParameters &Parameters)
// Nastaveni Parameters podle dialogu
{
   // Baud :
   switch( UartSetupBaudRate->ItemIndex){
      case 0 :
         Parameters.BaudRate = 300;
         break;
      case 1 :
         Parameters.BaudRate = 600;
         break;
      case 2 :
         Parameters.BaudRate = 1200;
         break;
      case 3 :
         Parameters.BaudRate = 2400;
         break;
      case 4 :
         Parameters.BaudRate = 4800;
         break;
      case 5 :
         Parameters.BaudRate = 9600;
         break;
      case 6 :
         Parameters.BaudRate = 19200;
         break;
      case 7 :
         Parameters.BaudRate = 38400;
         break;
      case 8 :
         Parameters.BaudRate = 57600;
         break;
      case 9 :
         Parameters.BaudRate = 115200;
         break;
      IDEFAULT
   }
   // Data Bits :
   switch( UartSetupDataBits->ItemIndex){
      case 0 :
         Parameters.DataBits = 5;
         break;
      case 1 :
         Parameters.DataBits = 6;
         break;
      case 2 :
         Parameters.DataBits = 7;
         break;
      case 3 :
         Parameters.DataBits = 8;
         break;
      IDEFAULT
   }
   // Parity :
   switch( UartSetupParity->ItemIndex){
      case 0 :
         Parameters.Parity = TUart::NO_PARITY;
         break;
      case 1 :
         Parameters.Parity = TUart::ODD_PARITY;
         break;
      case 2 :
         Parameters.Parity = TUart::EVEN_PARITY;
         break;
      case 3 :
         Parameters.Parity = TUart::MARK_PARITY;
         break;
      case 4 :
         Parameters.Parity = TUart::SPACE_PARITY;
         break;
      IDEFAULT
   }
   // Stop Bits :
   switch( UartSetupStopBits->ItemIndex){
      case 0 :
         Parameters.StopBits = 10;
         break;
      case 1 :
         Parameters.StopBits = 15;
         break;
      case 2 :
         Parameters.StopBits = 20;
         break;
      IDEFAULT
   }
   // Rizeni Toku :
   switch( UartSetupHandshake->ItemIndex){
      case 0 :
         Parameters.Handshake = TUart::NO_HANDSHAKE;
         break;
      case 1 :
         Parameters.Handshake = TUart::HARDWARE_HANDSHAKE;
         break;
      case 2 :
         Parameters.Handshake = TUart::XON_XOFF_HANDSHAKE;
         break;
      case 3 :
         Parameters.Handshake = TUart::HALF_DUPLEX_HANDSHAKE;
         break;

      IDEFAULT
   }
} // ConvertToParameters

//******************************************************************************
// Validity check
//******************************************************************************

void __fastcall TUartSetupDialog::UartSetupOKClick(TObject *Sender)
{
   ModalResult = mrNone;
   if( UartSetupDataBits->ItemIndex == 0 &&   // 5 bits
       UartSetupStopBits->ItemIndex == 2){    // 2 stopbits
      Application->MessageBox( "Unable set 5 data bits with 2 stop bits", "Error", MB_OK);
      return;
   }
   if( UartSetupDataBits->ItemIndex > 0 &&    // 6-8 bits
       UartSetupStopBits->ItemIndex == 1){    // 1.5 stop bits
      Application->MessageBox( "Unable set 1.5 stop bits", "Error", MB_OK);
      return;
   }
   ModalResult = mrOk;
} // UartSetupOKClick

