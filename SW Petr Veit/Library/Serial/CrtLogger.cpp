//******************************************************************************
//
//   CrtLogger.cpp   Raw data viewer
//   Version 1.0     (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop
#include "../Unisys/uni.h"

#include "CrtLogger.h"

#define BinaryColumns      16      // binary dump - numbers/line
#define TextColumns        32      // text dump   - charcters/line
#define MixedColumns       16      // mixed dump  - numeber & characters / line
#define UNKNOWN_CHARACTER  '.'     // nonprintable character

//---------------------------------------------------------------------------

#pragma package(smart_init)

TCrtLogger *Logger;    // global pointer

//******************************************************************************
// Constructor
//******************************************************************************

TCrtLogger::TCrtLogger( TCrt *Crt)
// Constructor
{
   this->Crt = Crt;
   FMode     = BINARY;
   Logger    = this;
} // TCrtLogger

//******************************************************************************
// Write data
//******************************************************************************

void TCrtLogger::Write( TDataType Type, void *Buffer, int Length)
// Write data
{
   if( Length <= 0){
      return;
   }
   if( !Buffer){
      IERROR;
   }
   switch( Type){
      case UNKNOWN :
         Crt->puts( "?? :");
         break;
      case RX_DATA :
         Crt->puts( "RX :");
         break;
      case TX_DATA :
         Crt->puts( "TX :");
         break;
      case RX_GARBAGE :
         Crt->puts( "RX GARBAGE :");
         break;
   }
   switch( FMode){
      case BINARY :
         BinaryWrite( (char *)Buffer, Length);
         break;
      case TEXT :
         TextWrite( (char *)Buffer, Length);
         break;
      case MIXED :
         MixedWrite( (char *)Buffer, Length);
         break;
      IDEFAULT
   }
} // Write

//******************************************************************************
// Write report
//******************************************************************************

void TCrtLogger::Report( char *Format, ...)
// Write report
{
va_list Arg;

   va_start( Arg, Format);
   Crt->vprintf( Format, Arg);
} // Report

//******************************************************************************
// Binary dump
//******************************************************************************

void TCrtLogger::BinaryWrite( char *Buffer, int Length)
// Binary dump
{
   int i = 0;
   while( 1){
      for( int j = 0; j < BinaryColumns; j++){
         Crt->printf( "%02X ", (unsigned char)Buffer[ i++]);
         if( i >= Length){
            goto next;
         }
      }
      Crt->printf( "\n");
   }
   next :
   Crt->printf( "\n");
} // BinaryWrite

//******************************************************************************
// Text dump
//******************************************************************************

void TCrtLogger::TextWrite( char *Buffer, int Length)
// Text dump
{
char  Temp[ 128];

   int i = 0;
   while( 1){
      memset( Temp, 0, sizeof( Temp));
      for( int j = 0; j < TextColumns; j++){
         Temp[ j] = Buffer[ i++];
         if( i >= Length){
            goto next;
         }
      }
      TextLineWrite( Temp);
   }
   next :
   TextLineWrite( Temp);
} // TextWrite

//******************************************************************************
// Mixed dump
//******************************************************************************

void TCrtLogger::MixedWrite( char *Buffer, int Length)
// Mixed dump
{
char  Temp[ 128];
int   LineLength;

   int i = 0;
   while( 1){
      memset( Temp, 0, sizeof( Temp));
      for( int j = 0; j < MixedColumns; j++){
         Temp[ j] = Buffer[ i++];
         if( i >= Length){
            LineLength = j + 1;
            goto next;
         }
      }
      MixedLineWrite( Temp, MixedColumns);
   }
   next :
   MixedLineWrite( Temp, LineLength);
} // MixedWrite

//******************************************************************************
// Text line
//******************************************************************************

void TCrtLogger::TextLineWrite( char *Line)
// Write text line
{
   for( int i = 0; i < TextColumns; i++){
      if( Line[ i] == 0){
         Line[ i] = ' ';
      } else if( !isprint( Line[ i])){
         Line[ i] = UNKNOWN_CHARACTER;
      }
   }
   Line[ TextColumns] = '\0';
   Crt->puts( Line);
} // TextLineWrite

//******************************************************************************
// Write mixed line
//******************************************************************************

void TCrtLogger::MixedLineWrite( char *Line, int LineLength)
// Write mixed line
{
   for( int i = 0; i < LineLength; i++){
      Crt->printf( "%02X ", (unsigned char)Line[ i]);
   }
   for( int i = 0; i < MixedColumns - LineLength; i++){
      Crt->printf( "   ");
   }
   Crt->printf( "   |   ");
   for( int i = 0; i < MixedColumns; i++){
      if( Line[ i] == 0){
         Line[ i] = ' ';
      } else if( !isprint( Line[ i])){
         Line[ i] = UNKNOWN_CHARACTER;
      }
   }
   Line[ MixedColumns] = '\0';
   Crt->printf( "%s\n", Line);
} // MixedLineWrite

