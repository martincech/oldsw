//******************************************************************************
//
//   SFolders.cpp       Special folders in Windows
//   Version 1.0
//
//******************************************************************************

#ifndef SpecialFoldersH
   #define SpecialFoldersH

#include <dbtables.hpp>   

class TSpecialFolders {

public:

  TSpecialFolders(String ApplicationName);
  // Constructor

  TSpecialFolders(String ApplicationName, String ManualWorkingDirectory);
  // Contructor s manualni volbou pracovniho adresare <ManualWorkingDirectory>

  void GetFolders();
  // Nacte adresare do promennych

  String GetFolder(int FolderNumber);
  // Nacte cestu k adresari <FolderNumber>. V pripade chyby vrati prazdny string.

  bool SameColumns(AnsiString File);
  // Pokud jde o soubor s priponou DB, porovna vsechny sloupce v tabulce <File> a odpovidajici tabulce v pracovnim adresari.
  // Vrati true, pokud jsou sloupce stejne.

  void CopyWorkingFile(String File);
  // Zkopiruje soubor <File> do pracovniho adresare a nastavi jeho atributy tak, aby se dal soubor upravovat

  void PrepareData(TStringList *Files, TSession *Session, TDatabase *Database);
  // Vytvori pracovni adresar, zkopiruje do nej datove soubory v seznamu <Files> a nastavi databazi <Database> na tento adresar

  __property String ApplicationName = {read=FApplicationName, write=FApplicationName};

  __property String Documents = {read=FDocuments};
  __property String Working   = {read=FWorking};
  __property String Running   = {read=FRunning};
  __property String Temporary = {read=FTemporary};

protected:

  String FApplicationName;      // Jmeno aplikace

  String FDocuments;            // Adresar s dokumenty
  String FWorking;              // Adresar pro pracovni data aplikace
  String FRunning;              // Adresar, odkud byl program spusten
  String FTemporary;            // Adresar s docasnymi soubory

  String ManualWorkingDir;      // Adresar pro pracovni data, ktery zvolil uzivatel rucne (NULL pokud zadny nezvolil)

  void Init(String ApplicationName);

}; // TSpecialFolders

#endif
