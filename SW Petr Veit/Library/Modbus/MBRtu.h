//*****************************************************************************
//
//   MBRtu.h     Modbus serial communication - RTU packets
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBRtu_H__
   #define __MBRtu_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

//------------------------------------------------------------------------------
//   Rx
//------------------------------------------------------------------------------

TYesNo MBRReceive( void **Pdu, word *Size);
// Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu>
// a velikost dat <Size>

byte MBRGetAddress( void);
// Vrati adresu paketu. POZOR, volat az po MBRReceive

//------------------------------------------------------------------------------
//   Tx
//------------------------------------------------------------------------------

void MBRWriteAddress( byte Address);
// Zapise adresu paketu

void MBRWriteByte( byte Data);
// Zapise 8bitove slovo do paketu

void MBRWriteWord( word Data);
// Zapise 16bitove slovo do paketu

void MBRWriteCrc( void);
// Spocita a zapise zabezpeceni

#endif
