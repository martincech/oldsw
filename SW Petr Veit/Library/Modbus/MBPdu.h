//******************************************************************************
//
//   MBPdu.h       Modbus PDU definitions
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __MBPdu_H__
   #pragma pack( push, 1)                 // byte alignment
   #include "..\..\Intel51\Vym\inc\MBPdu.h"
   #pragma pack( pop)                     // original alignment
#endif
