//*****************************************************************************
//
//   MBCom.c     Modbus serial communication
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MBCom.h"
#include "..\Serial\Uart\ComUart.h"
#include "..\Serial\CrtLogger.h"

#define RxDump()    //if( Logger) { Logger->Write( TLogger::RX_DATA, ComBuffer, ComDataSize);}
#define RxGarbage() //if( Logger) { Logger->Write( TLogger::RX_GARBAGE, ComBuffer, ComDataSize);}
#define TxDump()    //if( Logger) { Logger->Write( TLogger::TX_DATA, ComBuffer, ComDataSize);}

byte ComBuffer[ COM_DATA_SIZE];   // datovy buffer
int  ComDataSize;                 // delka prijatych/vysilanych dat
byte ComStatus;

static TComUart *Uart = 0;
static byte _Mode;
static word _TotalTimeout;
static word _IntercharacterTimeout;

//******************************************************************************
// Initialize
//******************************************************************************

TYesNo ComInit( TString ComName, word Baud, byte Parity, byte Mode)
// Inicializace komunikace
{
   if( Uart){
      delete Uart;  // already initialized
   }
   // open new port :
   Uart = new TComUart;
   TIdentifier Identifier;
   if( !Uart->Locate( ComName, Identifier)){
      delete Uart;
      return( NO);
   }
   if( !Uart->Open( Identifier)){
      return( NO);
   }
   // set communication parameters :
   TUart::TParameters Parameters;
   if( Baud == COM_115200Bd){
      Parameters.BaudRate  = 115200;
   } else {
      Parameters.BaudRate  = Baud;
   }
   switch( Parity){
      case COM_PARITY_NONE :
         Parameters.DataBits  = 8;
         Parameters.Parity    = TUart::NO_PARITY;
         break;
      case COM_PARITY_EVEN :
         Parameters.DataBits  = 8;
         Parameters.Parity    = TUart::EVEN_PARITY;
         break;
      case COM_PARITY_ODD :
         Parameters.DataBits  = 8;
         Parameters.Parity    = TUart::ODD_PARITY;
         break;
      case COM_PARITY_MARK :
         Parameters.DataBits  = 8;
         Parameters.Parity    = TUart::MARK_PARITY;
         break;
      case COM_PARITY_7BITS_EVEN :
         Parameters.DataBits  = 7;
         Parameters.Parity    = TUart::EVEN_PARITY;
         break;
      case COM_PARITY_7BITS_ODD :
         Parameters.DataBits  = 7;
         Parameters.Parity    = TUart::ODD_PARITY;
         break;
      case COM_PARITY_7BITS_MARK :
         Parameters.DataBits  = 7;
         Parameters.Parity    = TUart::MARK_PARITY;
         break;
      IDEFAULT
   }
   Parameters.StopBits  = 10;
   Parameters.Handshake = TUart::NO_HANDSHAKE;
   Uart->SetParameters( Parameters);
   _Mode = Mode;
   ComStatus = COM_STATUS_STOPPED;
   return( YES);
} // ComInit

//******************************************************************************
// Set Total Timeout
//******************************************************************************

void ComSetTimeouts( word TotalTimeout, word IntercharacterTimeout)
// Nastaveni timeoutu
{
   _TotalTimeout          = TotalTimeout;
   _IntercharacterTimeout = IntercharacterTimeout;
} // ComSetTotalTimeout

//******************************************************************************
// Tx
//******************************************************************************

void ComTxStart( void)
// Zahaji vysilani bufferu
{
   if( !ComDataSize){
      IERROR;
   }
   Uart->Flush();
   Uart->Write( ComBuffer, ComDataSize);
   ComStatus = COM_STATUS_TX_DONE;
   TxDump();
} // ComTxStart

//******************************************************************************
// Rx
//******************************************************************************

void ComRxStart( void)
// Zahaji prijem do bufferu
{
int Size;
byte Ch;

   ComDataSize = 0;
   Uart->SetRxWait( _TotalTimeout, 0);   // pocatecni timeout do odpovedi
   Size = Uart->Read( &Ch, 1);
   if( Size !=1 ){
      ComStatus = COM_STATUS_RX_TIMEOUT;
      return;
   }
   ComBuffer[ ComDataSize++] = Ch;
   if( _Mode & COM_MODE_LEADER){
      if( Ch != COM_LEADER_CHAR){
         ComStatus = COM_STATUS_RX_TIMEOUT;
         RxGarbage();
         return;
      }
   }
   Uart->SetRxWait( _IntercharacterTimeout, 0);        // meziznakovy timeout
   while( 1){
      if( ComDataSize == COM_DATA_SIZE){
         ComStatus = COM_STATUS_RX_OVERRUN;
         RxDump();
         return;
      }
      Size = Uart->Read( &Ch, 1);
      if( Size !=1 ){
         ComStatus = COM_STATUS_RX_TIMEOUT;
         RxDump();
         return;
      }
      ComBuffer[ ComDataSize++] = Ch;
      if( _Mode & COM_MODE_TRAILER){
         if( Ch == COM_TRAILER_CHAR){
            ComStatus = COM_STATUS_RX_DONE;
            RxDump();
            return;
         }
      }
   }
} // ComRxStart

