//******************************************************************************
//
//   MBDef.h       Modbus common data definitions
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __MBDef_H__
   #pragma pack( push, 1)                 // byte alignment
   #include "..\..\Intel51\Vym\inc\MBDef.h"
   #pragma pack( pop)                     // original alignment
#endif
 