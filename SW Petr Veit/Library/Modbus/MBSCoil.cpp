//******************************************************************************
//
//   MBSCoil.c    Modbus send coil request
//   Version 1.0  (c) VymOs
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MB.h"
#include "MBPdu.h"
#include "MBPacket.h"
#include "MBCb.h"
#include "MBCom.h"

//******************************************************************************
// Read Coils
//******************************************************************************

void MBReadCoils( word Address, word Count)
// Cteni stavu
{
   if( Count > MB_MAX_READ_COILS_COUNT){
      IERROR;
   }
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_READ_COILS);
   MBWriteItemAddress( Address);
   MBWriteCount( Count);
   MBWriteCrc();
   ComTxStart();
} // MBReadCoils

//******************************************************************************
// Write Single Coil
//******************************************************************************

void MBWriteSingleCoil( word Address, byte Value)
// Vysle zapis
{
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_WRITE_SINGLE_COIL);
   MBWriteItemAddress( Address);
   if( Value){
      MBWriteWord( MB_COIL_ON);
   } else {
      MBWriteWord( MB_COIL_OFF);
   }
   MBWriteCrc();
   ComTxStart();
} // MBWriteSingleCoil

//******************************************************************************
// Write Coils
//******************************************************************************

void MBWriteCoils( word Address, word Count, byte *Data)
// Zapis pole hodnot
{
byte ByteCount;
byte i;

   if( Count > MB_MAX_WRITE_COILS_COUNT){
      IERROR;
   }
   MBWriteAddress(  MBCGetSlaveAddress());
   MBWriteFunction( MBF_WRITE_MULTIPLE_COILS);
   MBWriteItemAddress( Address);
   MBWriteCount( Count);
   ByteCount = MBBitsToBytes( Count);
   MBWriteByte( ByteCount);
   for( i = 0; i < ByteCount; i++){
      MBWriteByte( Data[ i]);
   }
   MBWriteCrc();
   ComTxStart();
} // MBWriteCoils

