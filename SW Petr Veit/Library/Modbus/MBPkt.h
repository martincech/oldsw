//******************************************************************************
//
//   MBPkt.h      Modbus packte definitions
//   Version 0.0  (c) VymOs
//
//******************************************************************************

#ifndef __MBPkt_H__
   #pragma pack( push, 1)                 // byte alignment
   #include "..\..\Intel51\Vym\inc\MBPkt.h"
   #pragma pack( pop)                     // original alignment
#endif
