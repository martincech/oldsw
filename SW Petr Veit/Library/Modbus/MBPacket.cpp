//*****************************************************************************
//
//   MBPacket.c  Modbus serial communication - RTU/ASCII packets
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MBPacket.h"
#include "MBRtu.h"
#include "MBAscii.h"
#include "MBCom.h"

static byte IsAscii;

//-----------------------------------------------------------------------------
// Rezim
//-----------------------------------------------------------------------------

void MBSetMode( TString ComName, word Baud, byte Parity, word TotalTimeout, word IntercharacterTimeout, byte Mode)
// Nastavi rezim paketove urovne
{
   IsAscii = Mode == MB_ASCII_MODE;
   if( IsAscii){
      ComInit( ComName, Baud, Parity, COM_MODE_TIMEOUT | COM_MODE_TRAILER | COM_MODE_LEADER);
   } else {
      ComInit( ComName, Baud, Parity, COM_MODE_TIMEOUT);
   }
   ComSetTimeouts( TotalTimeout, IntercharacterTimeout);
} // MBSetMode

//-----------------------------------------------------------------------------
// Mode
//-----------------------------------------------------------------------------

byte MBIsAscii( void)
// Vrati YES, je-li v ASCII modu
{
   return( IsAscii);
} // MBIsAscii

//-----------------------------------------------------------------------------
// Prijem paketu
//-----------------------------------------------------------------------------

TYesNo MBReceive( void **Pdu, word *Size)
// Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu>
// a velikost dat <Size>
{
   if( IsAscii){
      return( MBAReceive( Pdu, Size));
   } else {
      return( MBRReceive( Pdu, Size));
   }
} // MBReceive

//-----------------------------------------------------------------------------
// Rx Adresa
//-----------------------------------------------------------------------------

byte MBGetAddress( void)
// Vrati adresu paketu. POZOR, volat az po MBAReceive
{
   if( IsAscii){
      return( MBAGetAddress());
   } else {
      return( MBRGetAddress());
   }
} // MBGetAddress

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Tx Adresa
//-----------------------------------------------------------------------------

void MBWriteAddress( byte Address)
// Zapise adresu paketu
{
   if( IsAscii){
      MBAWriteAddress( Address);
   } else {
      MBRWriteAddress( Address);
   }
} // MBWriteAddress

//-----------------------------------------------------------------------------
// Tx Byte
//-----------------------------------------------------------------------------

void MBWriteByte( byte Data)
// Zapise 8bitove slovo do paketu
{
   if( IsAscii){
      MBAWriteByte( Data);
   } else {
      MBRWriteByte( Data);
   }
} // MBWriteByte

//-----------------------------------------------------------------------------
// Tx Word
//-----------------------------------------------------------------------------

void MBWriteWord( word Data)
// Zapise 16bitove slovo do paketu
{
   if( IsAscii){
      MBAWriteWord( Data);
   } else {
      MBRWriteWord( Data);
   }
} // MBWriteWord

//-----------------------------------------------------------------------------
// Tx Crc
//-----------------------------------------------------------------------------

void MBWriteCrc( void)
// Spocita a zapise zabezpeceni
{
   if( IsAscii){
      MBAWriteCrc();
   } else {
      MBRWriteCrc();
   }
} // MBWriteCrc

//******************************************************************************
// Get Word
//******************************************************************************

#ifdef __WIN32__

word MBGetWord( word Data)
// Konverze na big endian
{
   return( ((Data & 0xFF) << 8) | ((Data >> 8) & 0xFF));
} // MBGetWord

#endif

