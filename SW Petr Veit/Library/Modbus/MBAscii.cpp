//*****************************************************************************
//
//   MBAscii.c   Modbus serial communication - ASCII packets
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "MBAscii.h"
#include "MBPkt.h"
#include "MBCom.h"

//-----------------------------------------------------------------------------
// Prijem paketu
//-----------------------------------------------------------------------------

TYesNo MBAReceive( void **Pdu, word *Size)
// Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu>
// a velikost dat <Size>
{
word i;
word j;
word Count;
char ch;
byte b;

   Count = ComDataSize;
   // minimalni delka ramce :
   if( Count <= MB_ASCII_MIN_SIZE){
      return( NO);
   }
   if( Count > MB_ASCII_MAX_SIZE){
      return( NO);
   }
   // kontrola ramovacich znaku :
   if( ComBuffer[ 0] != MB_ASCII_LEADING_CHAR){
      return( NO);
   }
   if( ComBuffer[ Count - 2] != MB_ASCII_TRAILER1_CHAR){
      return( NO);
   }
   if( ComBuffer[ Count - 1] != MB_ASCII_TRAILER2_CHAR){
      return( NO);
   }
   // kontrola delky :
   Count -= MB_ASCII_FRAME_SIZE;
   if( Count & 0x01){
      return( NO);                     // lichy pocet znaku uvnitr ramu
   }
   // kontrola znaku :
   for( i = 1; i <= Count; i++){
      if( ComBuffer[ i] < '0' ||
          ComBuffer[ i] > '9' &&
          ComBuffer[ i] < 'A' ||
          ComBuffer[ i] > 'F'){
         return( NO);                  // jiny znak nez hexa cislice
      }
   }
   // prevod do binarni podoby :
   i = 1;                              // source char
   Count >>= 1;                        // pocet bytu
   for( j = 0; j < Count; j++){
      ch  = ComBuffer[ i++];
      b   = char2hex( ch) << 4;        // high nibble
      ch  = ComBuffer[ i++];
      b  |= char2hex( ch);             // low nibble
      ComBuffer[ j] = b;
   }
   // kontrola zabezpeceni :
   b = 0;
   for( i = 0; i < Count; i++){
      b += ComBuffer[ i];
   }
   if( b != 0){
      return( NO);                     // jiny znak nez hexa cislice
   }
   *Size = Count - MB_ASCII_ADDRESS_SIZE - MB_ASCII_LRC_SIZE;
   *Pdu  = &ComBuffer[ 1];
   return( YES);
} // MBAReceive

//-----------------------------------------------------------------------------
// Rx Adresa
//-----------------------------------------------------------------------------

byte MBAGetAddress( void)
// Vrati adresu paketu. POZOR, volat az po MBAReceive
{
   return( ComBuffer[ 0]);
} // MBAGetAddress

//-----------------------------------------------------------------------------

static byte Lrc;

//-----------------------------------------------------------------------------
// Tx Adresa
//-----------------------------------------------------------------------------

void MBAWriteAddress( byte Address)
// Zapise adresu paketu
{
   ComDataSize = 0;
   Lrc         = 0;
   ComBuffer[ ComDataSize++] = MB_ASCII_LEADING_CHAR;
   MBAWriteByte( Address);
} // MBAWriteAddress

//-----------------------------------------------------------------------------
// Tx Byte
//-----------------------------------------------------------------------------

void MBAWriteByte( byte Data)
// Zapise 8bitove slovo do paketu
{
   ComBuffer[ ComDataSize++] = nibble2hex( Data >> 4);    // high nibble
   ComBuffer[ ComDataSize++] = nibble2hex( Data & 0x0F);  // low  nibble
   Lrc += Data;
} // MBAWriteByte

//-----------------------------------------------------------------------------
// Tx Word
//-----------------------------------------------------------------------------

void MBAWriteWord( word Data)
// Zapise 16bitove slovo do paketu
{
   MBAWriteByte( Data >> 8);           // MSB first
   MBAWriteByte( Data & 0xFF);         // LSB
} // MBAWriteWord

//-----------------------------------------------------------------------------
// Tx Crc
//-----------------------------------------------------------------------------

void MBAWriteCrc( void)
// Spocita a zapise zabezpeceni
{
   Lrc = -Lrc;
   MBAWriteByte( Lrc);
   ComBuffer[ ComDataSize++] = MB_ASCII_TRAILER1_CHAR;
   ComBuffer[ ComDataSize++] = MB_ASCII_TRAILER2_CHAR;
} // MBAWriteCrc
