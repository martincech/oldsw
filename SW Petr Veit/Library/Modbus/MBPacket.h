//******************************************************************************
//
//   MBPacket.h      Modbus serial communication - RTU/ASCII packets
//   Version 1.0 (c) VymOs
//
//******************************************************************************

#ifndef __MBPacket_H__
   #define __MBPacket_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MBDef.h"
   #include "MBDef.h"
#endif

//------------------------------------------------------------------------------

void MBSetMode( TString ComName, word Baud, byte Parity, word TotalTimeout, word IntercharacterTimeout, byte Mode);
// Nastavi rezim paketove urovne

byte MBIsAscii( void);
// Vrati YES, je-li v ASCII modu

//------------------------------------------------------------------------------
//   Rx
//------------------------------------------------------------------------------

TYesNo MBReceive( void **Pdu, word *Size);
// Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu>
// a velikost dat <Size>

byte MBGetAddress( void);
// Vrati adresu paketu. POZOR, volat az po MBRReceive

//------------------------------------------------------------------------------
//   Tx
//------------------------------------------------------------------------------

void MBWriteAddress( byte Address);
// Zapise adresu paketu

void MBWriteByte( byte Data);
// Zapise 8bitove slovo do paketu

void MBWriteWord( word Data);
// Zapise 16bitove slovo do paketu

void MBWriteCrc( void);
// Spocita a zapise zabezpeceni

#endif
