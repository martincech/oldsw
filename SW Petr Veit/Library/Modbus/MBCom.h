//*****************************************************************************
//
//   MBCom.h     Modbus serial communication
//   Version 1.0 (c) VymOs
//
//*****************************************************************************

#ifndef __MBCom_H__
   #define __MBCom_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifndef __MBDef_H__
   #include "MBDef.h"
#endif

// Stav zarizeni :
typedef enum {
   COM_STATUS_STOPPED,                 // zastaveno, ceka na povel
   COM_STATUS_RX_RUNNING,              // bezi Rx
   COM_STATUS_RX_DONE,                 // dokoncen prijem, prevezmi data
   COM_STATUS_RX_PARITY,               // dokoncen prijem, chyba parity
   COM_STATUS_RX_OVERRUN,              // dokoncen prijem, pocet znaku presahl velikost bufferu
   COM_STATUS_RX_TIMEOUT,              // dokoncen prijem, timeout
   COM_STATUS_TX_RUNNING,              // bezi Tx
   COM_STATUS_TX_DONE,                 // dokonceno vysilani
   _COM_STATUS_LAST
} TComStatus;

// Rezim :
typedef enum {
   COM_MODE_NONE,
   COM_MODE_TIMEOUT = 0x01,            // meziznakovy timeout
   COM_MODE_LEADER  = 0x02,            // vedouci znak
   COM_MODE_TRAILER = 0x04,            // ukoncovaci znak
   _COM_MODE_LAST
} TComMode;

// Globalni data :

extern byte ComBuffer[ COM_DATA_SIZE];   // datovy buffer
extern int  ComDataSize;                 // delka prijatych/vysilanych dat

extern byte ComStatus;

TYesNo ComInit( TString ComName, word Baud, byte Parity, byte Mode);
// Inicializace komunikace

void ComSetTimeouts( word TotalTimeout, word IntercharacterTimeout);
// Nastaveni timeoutu

void ComTxStart( void);
// Zahaji vysilani bufferu

void ComRxStart( void);
// Zahaji prijem do bufferu

#endif

