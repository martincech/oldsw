//******************************************************************************
//
//   XmlExport.cpp       Export to XML file
//   Version 1.4
//
//******************************************************************************

#include <vcl.h>
#pragma hdrstop

#include "XmlExport.h"

#pragma package(smart_init)

//---------------------------------------------------------------------------
// Constructor
//---------------------------------------------------------------------------

TXmlExport::TXmlExport(String fileName) {
  // Contructor
  FStream = new TFileStream(fileName, fmCreate);

  FStartTagList = new TStringList();

  // Hlavicka vcetne Byte Order Mark (BOM) pro UTF-8
  Write(0xEF);
  Write(0xBB);
  Write(0xBF);
  WriteLine("<?xml version='1.0' encoding='utf-8'?>");
}

//---------------------------------------------------------------------------
// Destructor
//---------------------------------------------------------------------------

TXmlExport::~TXmlExport() {
  delete FStream;
  delete FStartTagList;
}

//---------------------------------------------------------------------------
// Zapis start tagu s atributy
//---------------------------------------------------------------------------

void TXmlExport::StartTag(AnsiString name, AnsiString attribute) {
  AnsiString text = name;
  if (attribute != "") {
    text += " " + attribute;
  }
  WriteTag("<" + text + ">");
  FStartTagList->Add(name);
}

//---------------------------------------------------------------------------
// Zapis start tagu bez atributu
//---------------------------------------------------------------------------

void TXmlExport::StartTag(AnsiString name) {
  StartTag(name, "");
}

//---------------------------------------------------------------------------
// Zapis end tagu
//---------------------------------------------------------------------------

void TXmlExport::EndTag() {
  if (FStartTagList->Count == 0) {
    return;
  }

  AnsiString tag = FStartTagList->Strings[FStartTagList->Count - 1];
  FStartTagList->Delete(FStartTagList->Count - 1);

  WriteTag("</" + tag + ">");
}

//---------------------------------------------------------------------------
// Zapis tagu s hodnotou na jeden radek
//---------------------------------------------------------------------------

void TXmlExport::ValueTag(AnsiString name, AnsiString value) {
  WriteTag("<" + name + ">" + value + "</" + name + ">");
}


//---------------------------------------------------------------------------
// Zapis empty element tagu
//---------------------------------------------------------------------------

void TXmlExport::EmptyElementTag(AnsiString text) {
  WriteTag("<" + text + " />");
}

//---------------------------------------------------------------------------
// Zapis znaku
//---------------------------------------------------------------------------

void TXmlExport::Write(char ch) {
  FStream->Write(&ch, 1);
}

//---------------------------------------------------------------------------
// Zapis stringu
//---------------------------------------------------------------------------

void TXmlExport::Write(AnsiString text) {
  FStream->Write(text.c_str(), text.Length());
}

//---------------------------------------------------------------------------
// Zapis radku
//---------------------------------------------------------------------------

void TXmlExport::WriteLine(AnsiString text) {
  Write(text);
  Write(0x0D);
  Write(0x0A);
}

//---------------------------------------------------------------------------
// Zapis tagu
//---------------------------------------------------------------------------

void TXmlExport::WriteTag(AnsiString text) {
  // Odsazeni
  for (int i = 0; i < FStartTagList->Count; i++) {
    Write("  ");
  }

  // Samotny tag
  WriteLine(text);
}

