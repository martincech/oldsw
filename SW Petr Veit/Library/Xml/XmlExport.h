//******************************************************************************
//
//   XmlExport.cpp       XML Export
//   Version 1.0
//
//******************************************************************************

#ifndef XmlExportH
   #define XmlExportH

class TXmlExport {

public:

  TXmlExport(String fileName);
  // Constructor

  ~TXmlExport();
  // Destructor

  void StartTag(AnsiString name, AnsiString attribute);
  // Zapis start tagu s atributy

  void StartTag(AnsiString name);
  // Zapis start tagu bez atributu

  void EndTag();
  // Zapis end tagu

  void ValueTag(AnsiString name, AnsiString value);
  // Zapis tagu s hodnotou na jeden radek

  void EmptyElementTag(AnsiString text);
  // Zapis empty element tagu

  
private:

  void Write(char ch);
  void Write(AnsiString text);
  void WriteLine(AnsiString text);
  void WriteTag(AnsiString text);


protected:

  TFileStream *FStream;         // Zapisovany soubor
  TStringList *FStartTagList;   // Seznam tagu

}; // TXmlExport

#endif
