//******************************************************************************
//
//   Excel.cpp          Export to MS Excel
//   Version 1.0
//
//******************************************************************************

#include <system.hpp>   // Typ Variant

#ifndef ExcelH
  #define ExcelH

class TExcel {

public:

  ~TExcel();
  // Destructor

  bool Open(AnsiString SheetName);
  // Otevre a pripravi Excel

  void PutStr(int X, int Y, AnsiString Str);
  // Ulozi do Excelu string <Str> na pozici X, Y

  void PutInt(int X, int Y, int Int);
  // Ulozi do Excelu integer <Int> na pozici X, Y

  void PutFloat(int X, int Y, double d);
  // Ulozi do Excelu double <d> na pozici X, Y

protected:

  Variant XL;
  Variant Sheet;
  
}; // TExcel

#endif // ExcelH

