//******************************************************************************
//
//   Excel.cpp          Export to MS Excel
//   Version 1.0
//
//******************************************************************************

// Vyzkouseno na: Excel2000 (u me), Excel2003 (asimov, Inza), Excel2007 (brach, staroch)

#include <vcl.h>
#pragma hdrstop

#include <comobj.hpp>           // Excel
#include <utilcls.h>            // Excel

#include "Excel.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// Desktruktor
//---------------------------------------------------------------------------

TExcel::~TExcel() {
  // Destructor
  XL = Unassigned;              // Uvolnim
}

//---------------------------------------------------------------------------
// Otevreni Excelu
//---------------------------------------------------------------------------

bool TExcel::Open(AnsiString SheetName) {
  // Otevre a pripravi Excel
  Variant v0;
  Variant File;

  try {
    XL = CreateOleObject("Excel.Application");
    XL.OlePropertySet("Visible", true);
    // Zalozim novy soubor
    v0 = XL.OlePropertyGet("Workbooks");
    v0.OleProcedure("Add");
    File = v0.OlePropertyGet("Item", 1);        // Soubor
    // Prvni list
    Sheet = File.OlePropertyGet("Worksheets").OlePropertyGet("Item", 1);
    Sheet.OlePropertySet("Name", SheetName);   // Pojmenuju
  } catch(...) {
    return false;
  }
  return true;
}

//---------------------------------------------------------------------------
// Vlozeni stringu
//---------------------------------------------------------------------------

void TExcel::PutStr(int X, int Y, String Str) {
  // Ulozi do Excelu string <Str> na pozici X, Y
  Sheet.OlePropertyGet("Cells").OlePropertyGet("Item", Y, X).OlePropertySet("Value", Str);
}

//---------------------------------------------------------------------------
// Vlozeni integeru
//---------------------------------------------------------------------------

void TExcel::PutInt(int X, int Y, int Int) {
  // Ulozi do Excelu integer <Int> na pozici X, Y
  Sheet.OlePropertyGet("Cells").OlePropertyGet("Item", Y, X).OlePropertySet("Value", Int);
}

//---------------------------------------------------------------------------
// Vlozeni double
//---------------------------------------------------------------------------

void TExcel::PutFloat(int X, int Y, double d) {
  // Ulozi do Excelu double <d> na pozici X, Y
  Sheet.OlePropertyGet("Cells").OlePropertyGet("Item", Y, X).OlePropertySet("Value", d);
}

