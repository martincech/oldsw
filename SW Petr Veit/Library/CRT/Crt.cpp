//******************************************************************************
//
//   Crt.cpp      CRT emulator
//   Version 1.0  (c) VymOs
//
//******************************************************************************

//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "../Unisys/uni.h"
#include <stdio.h>

#include "Crt.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//******************************************************************************
// Konstruktor
//******************************************************************************

TCrt::TCrt( TMemo *Memo)
{
   this->Memo = Memo;
   Rows = 25;
   Cols = 80;
   LastLine     = -1;
   LastLinefeed = true; // zaciname novym radkem
} // TCrt

//******************************************************************************
// Mazani obrazovky
//******************************************************************************

void TCrt::Clear()
// smaze obrazovku
{
   Memo->Lines->Clear();
   LastLine     = -1;
   LastLinefeed = true; // zaciname novym radkem
} // Clear

//******************************************************************************
// Zapis retezce
//******************************************************************************

int TCrt::puts( char *String)
// zapis retezce a novy readek
{
   if( Memo->Lines->Count + 1 >= Rows - 1){
      // prekrocen pocet
      Memo->Lines->Delete( 0);                    // zrus prvni
   }
   LastLine     = Memo->Lines->Add( String);
   LastLinefeed = true;                           // tento radek ukoncen
   return( 1);
} // puts

//******************************************************************************
// Printf
//******************************************************************************

int TCrt::printf( char *format,...)
// Write string
{
va_list arg;

   va_start( arg, format);
   return( vprintf( format, arg));
} // printf

//******************************************************************************
// Vprintf
//******************************************************************************

int __cdecl TCrt::vprintf( char *format, va_list arg)
// Write string by list
{
char buff[ 256];
int     ret;

   // formatovani retezce :
   ret = vsprintf( buff, format, arg);
   // rozdeleni na radky :
   TStringList *Stl = new TStringList();
   Stl->SetText( buff);
   // pokracovaci radek :
   if( LastLinefeed == false){
      // predchozi radek nebyl ukoncen - zretez jej
      Memo->Lines->Strings[ LastLine] =
                             Memo->Lines->Strings[ LastLine] + Stl->Strings[ 0];
      Stl->Delete( 0);
   }
   // ukonceni radku :
   int last     = strlen( buff) - 1;
   LastLinefeed = buff[ last] == '\n' || buff[ last] == '\r';
   // rolovani :
   if( Memo->Lines->Count + Stl->Count >= Rows - 1){
      // prekrocen pocet
      int count = Memo->Lines->Count + Stl->Count - Rows + 2;
      for( int i = 0; i < count; i++){
         Memo->Lines->Delete( 0);                    // zrus prvni
      }
   }
   // nove pridej nakonec
   for( int i = 0; i < Stl->Count; i++){
      LastLine = Memo->Lines->Add( Stl->Strings[ i]);
   }
   delete Stl;
   return( ret);
} // printf

//******************************************************************************
// Resize
//******************************************************************************

void TCrt::Resize( TCanvas *Canvas)
// Memo resize callback
{
   // rows count :
   Canvas->Font = Memo->Font;
   int Height = Canvas->TextHeight( "Ag");
   FRows   = Height > 0 ? Memo->Height / Height : 1;

   // cols count :
   int Width  = Canvas->TextWidth( "MMWW");
   Width  /= 4;
   FCols   = Width > 0 ? Memo->Width / Width : 1;

   // clear window :
//   Clear();
} // Resize

