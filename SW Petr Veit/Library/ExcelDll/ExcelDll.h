// Inicializace
extern "C" __declspec(dllimport) void ExcelInit();

// Ukonceni
extern "C" __declspec(dllimport) void ExcelDestroy();

// Otevreni Excelu
extern "C" __declspec(dllimport) bool ExcelOpen(ShortString pageName);
extern "C" __declspec(dllimport) void ExcelOpenEmpty();

// Vlozeni stringu
extern "C" __declspec(dllimport) void ExcelPutString(int x, int y, ShortString text);

// Vlozeni integeru
extern "C" __declspec(dllimport) void ExcelPutInt(int x, int y, int value);

// Vlozeni floatu
extern "C" __declspec(dllimport) void ExcelPutFloat(int x, int y, double value);

 