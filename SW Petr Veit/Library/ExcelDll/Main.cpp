//---------------------------------------------------------------------------

#include <windows.h>
#include "..\Excel\Excel.h"
//---------------------------------------------------------------------------
//   Important note about DLL memory management when your DLL uses the
//   static version of the RunTime Library:
//
//   If your DLL exports any functions that pass String objects (or structs/
//   classes containing nested Strings) as parameter or function results,
//   you will need to add the library MEMMGR.LIB to both the DLL project and
//   any other projects that use the DLL.  You will also need to use MEMMGR.LIB
//   if any other projects which use the DLL will be performing new or delete
//   operations on any non-TObject-derived classes which are exported from the
//   DLL. Adding MEMMGR.LIB to your project will change the DLL and its calling
//   EXE's to use the BORLNDMM.DLL as their memory manager.  In these cases,
//   the file BORLNDMM.DLL should be deployed along with your DLL.
//
//   To avoid using BORLNDMM.DLL, pass string information using "char *" or
//   ShortString parameters.
//
//   If your DLL uses the dynamic version of the RTL, you do not need to
//   explicitly add MEMMGR.LIB as this will be done implicitly for you
//---------------------------------------------------------------------------

static TExcel *excel = NULL;

//---------------------------------------------------------------------------
// Inicializace
//---------------------------------------------------------------------------

extern "C" __declspec(dllexport) void ExcelInit() {
  if (excel != NULL) {
    return;
  }

  excel = new TExcel();
}

//---------------------------------------------------------------------------
// Ukonceni
//---------------------------------------------------------------------------

extern "C" __declspec(dllexport) void ExcelDestroy() {
  if (excel == NULL) {
    return;
  }

  delete excel;
  excel = NULL;
}

//---------------------------------------------------------------------------
// Otevreni Excelu
//---------------------------------------------------------------------------

extern "C" __declspec(dllexport) bool ExcelOpen(ShortString pageName) {
  return excel->Open(pageName);
}

extern "C" __declspec(dllexport) void ExcelOpenEmpty() {
  excel->Open("");
}

//---------------------------------------------------------------------------
// Vlozeni stringu
//---------------------------------------------------------------------------

extern "C" __declspec(dllexport) void ExcelPutString(int x, int y, ShortString text) {
  if (x == 0 || y == 0) {
    return;             // Index zacina od 1
  }
  excel->PutStr(x, y, text);
}

//---------------------------------------------------------------------------
// Vlozeni integeru
//---------------------------------------------------------------------------

extern "C" __declspec(dllexport) void ExcelPutInt(int x, int y, int value) {
  if (x == 0 || y == 0) {
    return;             // Index zacina od 1
  }
  excel->PutInt(x, y, value);
}

//---------------------------------------------------------------------------
// Vlozeni floatu
//---------------------------------------------------------------------------

extern "C" __declspec(dllexport) void ExcelPutFloat(int x, int y, double value) {
  if (x == 0 || y == 0) {
    return;             // Index zacina od 1
  }
  excel->PutFloat(x, y, value);
}

//---------------------------------------------------------------------------
// Main
//---------------------------------------------------------------------------

#pragma argsused
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
        return 1;
}
//---------------------------------------------------------------------------
 