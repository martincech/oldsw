﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace Veit.Database {
    
    /// <summary>
    /// Firtebird database table
    /// </summary>
    public class DatabaseTable {

        /// <summary>
        /// Table name
        /// </summary>
        public string TableName { get { return tableName; } }
        protected string tableName;
        
        /// <summary>
        /// Database factory used for connection
        /// </summary>
        protected DatabaseFactory factory;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="tableName">Table name</param>
        public DatabaseTable(string tableName, DatabaseFactory factory) {
            this.tableName = tableName;
            this.factory   = factory;
        }

        /// <summary>
        /// Create new table
        /// </summary>
        /// <param name="commandText">SQL command text</param>
        public void Create(string commandText) {
            using (DbCommand command = factory.CreateCommand(commandText)) {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Create autoincrement column in Firebird database using Generators and Triggers
        /// </summary>
        /// <param name="columnName">Name of column that should be auto incremented</param>
        /// <param name="columnPosition">Position of column that should be auto incremented</param>
        public void CreateFirebirdAutoincrement(string columnName, int columnPosition) {
            // Vytvorim generator
            string generatorName = "Generator" + columnName;
            using (DbCommand command = factory.CreateCommand("CREATE GENERATOR " + generatorName)) {
                command.ExecuteNonQuery();
            }
            
            // Nastavim generator na nulu
            using (DbCommand command = factory.CreateCommand("SET GENERATOR " + generatorName + " TO 0")) {
                command.ExecuteNonQuery();
            }

            // Vytvorim trigger
            using (DbCommand command = factory.CreateCommand("CREATE TRIGGER Trigger" + columnName + " FOR " + TableName + " "
                                                           + "ACTIVE BEFORE INSERT POSITION " + columnPosition.ToString() + " "
                                                           + "AS "
                                                           + "BEGIN "
                                                           + "IF (NEW." + columnName + " IS NULL) THEN "
                                                           + "NEW." + columnName + " = GEN_ID(" + generatorName + ",1); "
                                                           + "END")) {                    
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Get maximum value of numeric field
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <returns>Maximum field value or 0 if not found</returns>
        public int GetMaxValue(string fieldName) {
            int maxValue;
            using (DbCommand command = factory.CreateCommand("SELECT MAX(" + fieldName + ") FROM " + TableName)) {
                try {
                    maxValue = (int)command.ExecuteScalar();
                } catch {
                    // Tabulka je prazdna
                    maxValue = 0;
                }
            }
            return maxValue;
        }

        /// <summary>
        /// Check if the value exists
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Text to look for</param>
        /// <returns>True if exists</returns>
        public bool ValueExists(string fieldName, string value) {
            int count;

            using (DbCommand command = factory.CreateCommand("SELECT COUNT(" + fieldName + ") FROM " + TableName + " "
                                                           + "WHERE UPPER(" + fieldName + ") = @Value")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Value", value.ToUpper()));
                try {
                    count = (int)(long)command.ExecuteScalar();
                } catch {
                    // Tabulka je prazdna
                    count = 0;
                }
            }
            return (count > 0);
        }

        /// <summary>
        /// Read number of rows where a column has specified value
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Value to look for</param>
        /// <returns>Number of rows</returns>
        public int ValueCount(string fieldName, long value) {
            using (DbCommand command = factory.CreateCommand("SELECT COUNT(" + fieldName + ") FROM " + TableName + " "
                                                           + "WHERE " + fieldName + " = @Value")) {
                command.Parameters.Clear();
                command.Parameters.Add(factory.CreateParameter("@Value", value));
                try {
                    return (int)(long)command.ExecuteScalar();
                } catch {
                    // Tabulka je prazdna
                    return 0;
                }
            }
        }

        /// <summary>
        /// Check if the value exists
        /// </summary>
        /// <param name="fieldName">Field name</param>
        /// <param name="value">Integer value to look for</param>
        /// <returns>True if exists</returns>
        public bool ValueExists(string fieldName, long value) {
            return (ValueCount(fieldName, value) > 0);
        }

        /// <summary>
        /// Return total number of rows in a table
        /// </summary>
        /// <returns>Number of rows</returns>
        public int RowCount() {
            using (DbCommand command = factory.CreateCommand("SELECT COUNT(*) FROM " + TableName)) {
                try {
                    return (int)(long)command.ExecuteScalar();
                } catch {
                    return 0;       // Tabulka je prazdna
                }
            }
        }

        /// <summary>
        /// Delete all rows from a table
        /// </summary>
        public void Clear() {
            using (DbCommand command = factory.CreateCommand("DELETE FROM " + TableName)) {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Check if the table exists
        /// </summary>
        /// <returns>True if exists</returns>
        public bool Exists() {
            // DbConnection.GetSchema by melo fungovat na vsech databazich
            return (factory.Connection.GetSchema("TABLES", new string[] { null, null, TableName, null }).Rows.Count > 0);
        }

        /// <summary>
        /// Drop table if the table exists
        /// </summary>
        public void DropIfExists() {
            using (DbCommand command = factory.CreateCommand("DROP TABLE IF EXISTS " + TableName)) {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Get list of column names in the table
        /// </summary>
        /// <returns>List of column names</returns>
        public List<string> GetColumnNameList() {
            DataTable dataTable = factory.Connection.GetSchema("COLUMNS", new string[] { null, null, tableName, null });
            List<string> columnNameList = new List<string>();
            foreach (DataRow dataRow in  dataTable.Rows) {
                columnNameList.Add((string)dataRow["COLUMN_NAME"]);
            }
            return columnNameList;
        }

        /// <summary>
        /// Check if a specified column exists in the table
        /// </summary>
        /// <param name="columnName">Column name</param>
        /// <returns>True if exists</returns>
        public bool ColumnExists(string columnName) {
            return GetColumnNameList().Contains(columnName);
        }

        /// <summary>
        /// Add new column to the table. New column is always added as the last column.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <param name="type">Column type</param>
        public void AddColumn(string name, string type) {
            using (DbCommand command = factory.CreateCommand("ALTER TABLE " + TableName + " ADD COLUMN " + name
                                                           + " " + type)) {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Add new string column to the table. New column is always added as the last column.
        /// </summary>
        /// <param name="name">Column name</param>
        /// <param name="type">String length</param>
        public void AddColumn(string name, int length) {
            using (DbCommand command = factory.CreateCommand("ALTER TABLE " + TableName + " ADD COLUMN " + name
                                                           + " " + factory.TypeNVarChar + "(" + length.ToString() + ")")) {
                command.ExecuteNonQuery();
            }
        }

    }
}
