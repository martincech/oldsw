﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Veit.BinaryFile {
    public static class BinaryFile {
        public static void WriteToFile(byte[] buff, string fileName) {
            FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite);
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(buff);
            bw.Close();
        }

        public static byte[] ReadFromFile(string fileName, int size) {
            byte[] b = new byte[size];
            using (FileStream stream = File.Open(fileName, FileMode.Open)) {
                using (BinaryReader reader = new BinaryReader(stream)) {
                    for (int i = 0; i < size; i++) {
                        b[i] = reader.ReadByte();
                    }
                }
            }
            return b;
        }
    }
}
