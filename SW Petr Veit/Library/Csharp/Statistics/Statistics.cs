﻿//******************************************************************************
//
//   Statistics.cs      Statistic calculations
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;

namespace Veit.Statistics {
    public class Histogram {
        /// <summary>
        /// Number of slots in histogram
        /// </summary>
        public readonly int SlotsCount;

        /// <summary>
        /// Array of values
        /// </summary>
        public double[] Values;
        
        /// <summary>
        /// Array of counts
        /// </summary>
        public int[] Counts;

        /// <summary>
        /// Histogram step
        /// </summary>
        public double Step;

        public Histogram(int slotsCount) {
            SlotsCount = slotsCount;
            Values     = new double[slotsCount];
            Counts     = new int[slotsCount];
            Step       = 0;
        }
    }
    
    /// <summary>
    /// Statistic calculations
    /// </summary>
    public class Statistics {
        
        /// <summary>
        /// List of values for calculation
        /// </summary>
        private List<double> valueList = new List<double>();        

        /// <summary>
        /// Constructor
        /// </summary>
        public Statistics() {
            Clear();
        }

        /// <summary>
        /// Clear all items
        /// </summary>
        public void Clear() {
            valueList.Clear();
        }

        /// <summary>
        /// Add value
        /// </summary>
        /// <param name="value">Added value</param>
        public void Add(double value) {
            valueList.Add(value);
        }
        
        /// <summary>
        /// Get number of values
        /// </summary>
        /// <returns>Number of values</returns>
        public int Count() {
            return valueList.Count;
        }

        /// <summary>
        /// Calculate average value
        /// </summary>
        /// <returns>Average value</returns>
        public double Average() {
            if (valueList.Count == 0) {
                return 0;       // Prazdny seznam
            }
            
            double sum = 0;
            foreach (double value in valueList) {
                sum += value;
            }
            return sum / (double)valueList.Count;
        }

        /// <summary>
        /// Calculate standard deviation
        /// </summary>
        /// <returns>Standard deviation</returns>
        public double Sigma() {
            if (valueList.Count == 0) {
                return 0;       // Prazdny seznam
            }
            
            double average = Average();
            double sum = 0;
            foreach (double value in valueList) {
                sum += Math.Pow(value - average, 2);
            }
            
            return Math.Sqrt(sum / Count());
        }

        /// <summary>
        /// Calculate coefficient of variation
        /// </summary>
        /// <returns>Coefficient of variation in percents</returns>
        public double Variation() {
            double average = Average();

            if (average == 0) {
                return 0;       // Deleni nulou
            }
            return 100.0 * Sigma() / average;
        }

        /// <summary>
        /// Calculate uniformity
        /// </summary>
        /// <param name="Range">Range in percents</param>
        /// <returns>Uniformity</returns>
        public double Uniformity(int Range) {
            if (valueList.Count == 0) {
                return 100.0;       // Prazdny seznam
            }
            
            double average = Average();
            double min = average * (double)(100 - Range) / 100.0;
            double max = average * (double)(100 + Range) / 100.0;
            int inside = 0, outside = 0;        // Pocet vzorku uvnitr a vne pasma uniformity

            foreach (double value in valueList) {
                if (value < min || value > max) {
                    outside++;      // Vzorek je mimo pasmo
                } else {
                    inside++;       // Vzorek je v pasmu
                }
            }

            return 100.0 * (double)inside / (double)(inside + outside);
        }

        /// <summary>
        /// Calculate histogram
        /// </summary>
        /// <param name="step">Step of histogram</param>
        /// <param name="values">Array of bar values</param>
        /// <param name="counts">Array of value counts</param>
        public Histogram Histogram(double step, int slotsCount) {
            if (slotsCount < 3) {
                // Histogram musi mit minimalne 3 sloupce (mimo pod, 1 sloupec a mimo nad)
                throw new Exception("Histogram must have minimum 3 slots");         
            }
            
            Histogram histogram = new Histogram(slotsCount);
            histogram.Step = step;      // Ulozim si krok
            
            // Vyplnim hodnoty, vcetne krajnich sloupcu. Ty jsou sice mimo rozsah, ale pro jednoduche
            // vykreslovani v grafu je vyplnim take

            // Hodnota prvniho sloupce
            if (slotsCount % 2 == 0) {
                // Sudy pocet sloupcu
                histogram.Values[0] = Average() - ((double)slotsCount / 2.0 - 0.5) * step;
            } else {
                // Lichy pocet
                histogram.Values[0] = Average() - (((double)slotsCount - 1.0) / 2.0) * step;
            }

            // Osetrim, aby levy sloupec nesel do zapornych hodnot
            if (histogram.Values[0] < 0) {
                histogram.Values[0] = 0;
            }
            
            // Hodnoty ostatnich sloupcu nastavim se zadanym krokem
            for (int i = 1; i < slotsCount; i++) {
                histogram.Values[i] = histogram.Values[i - 1] + step;
            }

            // Vyplnim vysky histogramu
            foreach (double value in valueList) {
                if (value < histogram.Values[1] - step / 2.0) {
                    // Pod rozsahem
                    histogram.Counts[0]++;
                    continue;
                }
                if (value > histogram.Values[slotsCount - 2] + step / 2.0) {
                    // Nad rozsahem
                    histogram.Counts[slotsCount - 1]++;
                    continue;
                }
                // V rozsahu
                for (int i = 1; i < slotsCount - 1; i++) {
                    if (value < histogram.Values[i] + step / 2.0) {
                        histogram.Counts[i]++;
                        break;
                    }
                }
            }//foreach

            return histogram;
        }
    }
}