﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Veit.Bat1 {
    /// <summary>
    /// Conversion of the logo in the scale
    /// </summary>
    public static class Bat1Logo {
        /// <summary>
        /// Middle gray color
        /// </summary>
        private static Color middleGrayColor = Color.FromArgb(0x7F, 0x7F, 0x7F);
        
        public static bool ConvertImage(string fileName, out byte [] data) {
            data = null;
            
            // Nactu obrazek
            Bitmap bitmap;
            try {
                bitmap = new Bitmap(fileName);
            } catch {
                return false;       // Soubor neexistuje nebo ma BMP spatny format
            }

            try {
                // Zkontroluju rozmery
                if (bitmap.Size.Width != Logo.WIDTH || bitmap.Size.Height != Logo.HEIGHT) {
                    return false;       // Spatny rozmer obrazku
                }

                // Zkonvertuju obrazek na pole bajtu
                byte [,,] logo = new byte[Logo.PLANES, Logo.ROWS, Logo.WIDTH];

                // Pole je default vyplnene nulami
                for (int x = 0; x < Logo.WIDTH; x++) {
                    for (int y = 0; y < Logo.HEIGHT; y++) {
                        Color color = bitmap.GetPixel(x, y);
                        int yAddress = y / 8;
                        int yMask    = 1 << (y % 8);
                        // Barvy musim vzdy porovnavat pomoci fce ToArgb(), jinak nefunguje napr. cerna ani bila
                        if (color.ToArgb() == Color.White.ToArgb()) {
                            continue;       // Bilou mam predvyplnenou
                        }
                        // plane 0
                        if (color.ToArgb() == Color.Black.ToArgb() || color.ToArgb() >= middleGrayColor.ToArgb()) {
                            logo[0, yAddress, x] |= (byte)yMask;
                        }
                        // plane 1
                        if (color.ToArgb() == Color.Black.ToArgb() || color.ToArgb() < middleGrayColor.ToArgb()){ // dark gray
                           logo[1, yAddress, x] |= (byte)yMask;
                        }
                    }
                }

                // Zkonvertuju na jednorozmerne pole
                data = new byte[logo.Length];
                int index = 0;
                for (int plane = 0; plane < Logo.PLANES; plane++) {
                    for (int row = 0; row < Logo.ROWS; row++) {
                        for (int x = 0; x < Logo.WIDTH; x++) {
                            data[index] = logo[plane, row, x];
                            index++;
                        }
                    }
                }
                
                return true;
            } finally {
                bitmap.Dispose();       // Jinak zustane soubor s bitmapou zamknuty, dokud neukoncim program
            }
        }
    }
}
