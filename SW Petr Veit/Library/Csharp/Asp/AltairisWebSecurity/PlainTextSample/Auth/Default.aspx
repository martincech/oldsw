<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Auth_Default" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Altairis.Web.Security - Plain Text Membersip Provider Sample Web Site</title>
    <link rel="stylesheet" type="text/css" href="~/Images/StyleSheet.css" />
</head>
<body>
    <form id="form1" runat="server">
        <h1>Altairis.Web.Security - Plain Text Membersip Provider Sample Web Site</h1>
        <asp:LoginName ID="LoginName1" runat="server" FormatString="Logged in as {0}." />
        <asp:LoginStatus ID="LoginStatus1" runat="server" />
        <h2>Change password</h2>
        <asp:ChangePassword ID="ChangePassword1" runat="server"></asp:ChangePassword>
    </form>
    <div class="footer">
        <a href="http://www.codeplex.com/AltairisWebSecurity/">Altairis.Web.Security - Plain Text Membersip Provider</a> | &copy; <a href="http://www.rider.cz/">Michal A. Valasek</a> - <a href="http://www.altairis.cz/">Altairis</a>, 2006-2009
    </div>
</body>
</html>
