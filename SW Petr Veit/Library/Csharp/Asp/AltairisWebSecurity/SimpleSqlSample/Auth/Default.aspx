<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Auth_Default" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Altairis.Web.Security - Simple SQL Providers Sample Web Site</title>
    <link rel="stylesheet" type="text/css" href="~/Images/StyleSheet.css" />
</head>
<body>
    <form id="form1" runat="server">
        <h1>Altairis.Web.Security - Simple SQL Providers Sample Web Site</h1>
        <h2>Change user profile</h2>
        <asp:MultiView ID="MultiViewPage" runat="server" ActiveViewIndex="0">
            <asp:View ID="ViewForm" runat="server">
                <table>
                    <tr>
                        <th>
                            User name:
                        </th>
                        <td>
                            <asp:LoginName ID="LoginName1" runat="server" />
                            <asp:LoginStatus ID="LoginStatus1" runat="server" LogoutText="[logout]" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Full name:
                        </th>
                        <td>
                            <asp:TextBox ID="TextBoxFullName" runat="server" MaxLength="50" Width="300px" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Full name is missing" ControlToValidate="TextBoxFullName" Display="Dynamic">*</asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <th>
                            Birth date:
                        </th>
                        <td>
                            <asp:TextBox ID="TextBoxBirthDate" runat="server" Width="300px" />
                        </td>
                        <td>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Birth date is missing" ControlToValidate="TextBoxBirthDate" Display="Dynamic">*</asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Birth date format invalid" ControlToValidate="TextBoxBirthDate" Display="Dynamic" Operator="DataTypeCheck" Type="Date">*</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                    <th>
                    Page size:
                    </th>
                    <td>
                        <asp:TextBox ID="TextBoxPageSize" runat="server" Width="300px" /></td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBoxPageSize" Display="Dynamic" ErrorMessage="Page size is missing">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="TextBoxPageSize" Display="Dynamic" ErrorMessage="Page size must be integer greateer than 0" Operator="GreaterThan" Type="Integer" ValueToCompare="0">*</asp:CompareValidator></td>
                    </tr>
                    <tr>
                        <th>
                        </th>
                        <td colspan="2">
                            <asp:Button ID="ButtonSubmit" runat="server" Text="Submit" OnClick="ButtonSubmit_Click" />
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="ViewResult" runat="server">Your profile was updated.<br />
                <a href="Default.aspx">[back]</a> </asp:View>
        </asp:MultiView>
        <h2>Change password</h2>
        <asp:ChangePassword ID="ChangePassword1" runat="server"></asp:ChangePassword>
    </form>
    <div class="footer">
        <a href="http://www.codeplex.com/AltairisWebSecurity/">Altairis.Web.Security - Simple SQL Providers</a> | &copy; <a href="http://www.rider.cz/">Michal A. Valasek</a> - <a href="http://www.altairis.cz/">Altairis</a>, 2006-2009
    </div>
</body>
</html>
