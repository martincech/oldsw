﻿//******************************************************************************
//
//   Endian.cs          Endian conversions
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Veit.Endian {

    /// <summary>
    /// Endian conversions
    /// </summary>
    public static class Endian {

        public static short SwapInt16(short v) {
            return (short)(((v & 0xff) << 8) | ((v >> 8) & 0xff));
        }

        public static ushort SwapUInt16(ushort v) {
            return (ushort)(((v & 0xff) << 8) | ((v >> 8) & 0xff));
        }

        public static int SwapInt32(int v) {
            return (int)(((SwapInt16((short)v) & 0xffff) << 0x10) | (SwapInt16((short)(v >> 0x10)) & 0xffff));
        }

        public static uint SwapUInt32(uint v) {
            return (uint)(((SwapUInt16((ushort)v) & 0xffff) << 0x10) | (SwapUInt16((ushort)(v >> 0x10)) & 0xffff));
        }

        public static long SwapInt64(long v) {
            return (long)(((SwapInt32((int)v) & 0xffffffffL) << 0x20) | (SwapInt32((int)(v >> 0x20)) & 0xffffffffL));
        }

        public static ulong SwapUInt64(ulong v) {
            return (ulong)(((SwapUInt32((uint)v) & 0xffffffffL) << 0x20) | (SwapUInt32((uint)(v >> 0x20)) & 0xffffffffL));
        }

        public static float SwapFloat(float input) {
            byte[] tmpIn = BitConverter.GetBytes(input);
            byte[] tmpOut = new byte[4];
            tmpOut[0] = tmpIn[3];
            tmpOut[1] = tmpIn[2];
            tmpOut[2] = tmpIn[1];
            tmpOut[3] = tmpIn[0];
            return BitConverter.ToSingle(tmpOut, 0);
        }

        public static double SwapDouble(double input) {
            byte[] tmpIn = BitConverter.GetBytes(input);
            byte[] tmpOut = new byte[8];
            tmpOut[0] = tmpIn[7];
            tmpOut[1] = tmpIn[6];
            tmpOut[2] = tmpIn[5];
            tmpOut[3] = tmpIn[4];
            tmpOut[4] = tmpIn[3];
            tmpOut[5] = tmpIn[2];
            tmpOut[6] = tmpIn[1];
            tmpOut[7] = tmpIn[0];
            return BitConverter.ToSingle(tmpOut, 0);
        }

    }

    

}