//******************************************************************************
//
//   SFolders.cs       Special folders in Windows
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;                     // Application.StartupPath atd.
using System.IO;                                // Prace se soubory

namespace Veit.SFolders {
    class SFolders {
        
        // Jmeno aplikace
        private string applicationName;         
        
        // Adresar pro pracovni data ve Windows
        private string windowsWorkingFolder;
        
        /// <summary>
        /// Adresar pro pracovni data aplikace
        /// </summary>
        public string DataFolder { get { return dataFolder; } }
        private string dataFolder;

        /// <summary>
        /// Adresar, odkud byl program spusten
        /// </summary>
        public string RunningFolder { get { return runningFolder; } }
        private string runningFolder;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="applicationName">Application name</param>
        public SFolders(string applicationName) {
            this.applicationName = applicationName;
            
            // Nactu nazvy adresaru
            runningFolder        = Application.StartupPath;                                                     // Umisteni programu
            windowsWorkingFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);   // Pracovni adresar ve Windows
            dataFolder           = windowsWorkingFolder + "\\" + applicationName;                               // Adresar pro data aplikace
        }

        /// <summary>
        /// Add path to file name
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>File name including path to data folder</returns>
        public string GetFullFileName(string fileName) {
            return dataFolder + "\\" + fileName;
        }
        
        /// <summary>
        /// Copy file into the data directory and set its parameters so the file can be modified
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <returns>True if successful</returns>
        private bool CopyWorkingFile(string fileName) {
            string destination = GetFullFileName(fileName);
            
            try {
                // Zkopiruju soubor
                File.Exists(destination);
                // Nastavim atributy, aby sel soubor upravovat
                File.SetAttributes(destination, FileAttributes.Normal);
                return true;
            } catch {
                // Soubor nejde zkopirovat nebo nejdou nastavit jeho parametry
                return false;
            }
        }
        
        /// <summary>
        /// Prepare data
        /// </summary>
        /// <param name="files">List of files to be copied from running directory to data directory</param>
        /// <returns>True if successful</returns>
        public bool PrepareData(List<string> files) {
            // Vytvori pracovni adresar, zkopiruje do nej datove soubory v seznamu <files>
            // Zkontroluju, zda existuje pracovni adresar
            try {
                if (!Directory.Exists(dataFolder)) {
                    Directory.CreateDirectory(dataFolder);
                }
            } catch {
                // Toto by se nemelo stat, bez toho nelze pokracovat
                return false;
            }
            // Nakopiruju do pracovniho adresare potrebne soubory (ktere se nevytvareji, ale je treba je kopirovat)
            if (files == null) {
                // Neche kopirovat zadne soubory
                return true;
            }
            foreach (string file in files) {
                if (!CopyWorkingFile(file)) {
                    return false;
                }
            }
            return true;
        }

    } // class SFolders
}
