//******************************************************************************
//
//   LogFile.cs         Saving log to a file
//   Version 1.0
//
//******************************************************************************

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;                        // Prace se soubory

namespace Veit.LogFile {
    /// <summary>
    /// Saving log to a file
    /// </summary>
    class LogFile {
        /// <summary>
        /// File name where to save the log
        /// </summary>
        private string fileName;
        
        /// <summary>
        /// File encoding 
        /// </summary>
        private Encoding encoding;
        // - Encoding.Default: nastavi ANSI kodovani, ktere je prave nastavene ve Windows,
        //                     tj. v CZ Win jsou citelne jen ceske znaky atd.
        // - Encoding.Unicode: nastavi Unicode UTF-16, v Excelu i OpenOffice funguje dobre.
        // - Encoding.UTF8: nastavi Unicode UTF-8, v Excelu nefunguje, i kdyz napr. prohlizec
        //                  v TotalCmd text zobrazi korektne, tj. export jako takovy funguje
        //                  jak ma. V OpenOffice funguje.
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">File name</param>
        /// <param name="overwrite">If the file already exists, overwrite it</param>
        /// <param name="encoding">File encoding</param>
        public LogFile(string fileName, bool overwrite, Encoding encoding) {
            // Preberu parametry
            this.fileName = fileName;
            this.encoding = encoding;

            // Zkontroluju, zda uz soubor neexistuje
            if (File.Exists(fileName)) {
                if (!overwrite) {
                    // Nechce prepsat, hodim vyjimku
                    throw new Exception("File already exists");
                }
                // Chce prepsat, smazu stavajici soubor
                File.Delete(fileName);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="fileName">File name</param>
        public LogFile(string fileName) : this(fileName, true, Encoding.Default) {
            // Default existujici soubor prepisu a pouzivam ANSI nastavene ve Windows
        }

        /// <summary>
        /// Add new line to the log
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <param name="addTime">If a timestamp should be added</param>
        /// <returns>True if successful</returns>
        private bool Add(string text, bool addTime) {
            try {
                // Otevru soubor a zapisu novy radek
                using (StreamWriter writer = new StreamWriter(new FileStream(fileName, FileMode.Append), encoding)) {
                    string textToAdd = addTime ? DateTime.Now.ToString() + "        " + text : text;
                    writer.WriteLine(textToAdd);
                    writer.Close();
                }
            } catch {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Add new line to the log
        /// </summary>
        /// <param name="text">Text to add</param>
        /// <returns>True if successful</returns>
        public bool AddText(string text) {
            return Add(text, true);
        }

        public bool AddEmptyLine() {
            return Add("", false);
        }

    }
}
