﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veit.UsbReader {
    public class At45dbxx {

        private UsbSpi usbSpi;
        
        public const int FLASH_PAGE_SIZE = 528; // real page size
        public const int FLASH_PAGES     = 4096; // pages count

        private const string USB_DEVICE_NAME = "VEIT USB Reader A";           // USB adapter name
        private const string USB_DUTCHMAN_NAME = "ComScale USB Reader A";       // USB adapter name

        private const int SPI_CLOCK_RATE = 1000000;        // clock rate [Hz]

        // status register :
        private const byte FLASH_STATUS_RDY = 0x80;        // ready bit
        private const byte FLASH_STATUS_COMP = 0x40;        // comparation flag
        // ready timeout :
        private const byte FLASH_TIMEOUT     =   50;        // max. busy duration [ms]
        // commands :
        private const byte FLASH_ARRAY_READ    = 0xE8;     // SPI mode 0 or 3
        private const byte FLASH_PAGE_READ     = 0xD2;
        private const byte FLASH_BUFFER1_READ  = 0xD4;
        private const byte FLASH_BUFFER2_READ  = 0xD6;
        private const byte FLASH_STATUS_READ   = 0xD7;

        private const byte FLASH_BUFFER1_WRITE = 0x84;
        private const byte FLASH_BUFFER2_WRITE = 0x87;
        private const byte FLASH_BUFFER1_SAVE  = 0x83;    // buffer 1 to main memory page with Erase
        private const byte FLASH_BUFFER2_SAVE  = 0x86;   // buffer 2 to main memory page with Erase
        private const byte FLASH_PAGE_ERASE    = 0x81;

        private const byte FLASH_PAGE_TO_BUFFER1 = 0x53;
        private const byte FLASH_PAGE_TO_BUFFER2 = 0x55;


        //------- AT45DB161 only :
        private const byte FLASH_SIGNATURE      = 0x2C;
        private const byte FLASH_SIGNATURE_MASK = 0x3C;

//        private ushort MkBufferAddress(ushort a) { return (ushort)(a & 0x1FF); }
//        private ushort MkPageAddress( a)       ((word)((a) >> 9))

        private ushort BufferLowAddress(ushort b) { return (ushort)(b & 0x00FF); }
        private ushort BufferHighAddress(ushort b) { return (ushort)((b >> 8) & 0x01); }
        private ushort PageLowAddress(ushort p) { return (ushort)(p << 2); }
        private ushort PageHighAddress(ushort p) { return (ushort)(p >> 6); }

        
        public At45dbxx() {
            // Constructor
            usbSpi = new UsbSpi();
        }

        public void Close() {
            usbSpi.Close();
        }
        
        public bool Connected() {
            // Check if the adapter/module are present
            // read chip signature :
            byte Value;
            if( !Status( out Value)){
                return( false);                  // probably no adapter
            }
            if( Value == 0xFF){
                return( false);                  // module not present
            }
            if( (Value & FLASH_SIGNATURE_MASK) != FLASH_SIGNATURE){
                return( false);
            }
            return( true);
        }

        public bool WaitForReady() {
            // Wait for flash ready
            byte Value;
            for( int i = 0; i < FLASH_TIMEOUT; i++){
                if( !Status( out Value)){
                    return( false);               // probably no adapter
                }
                if( (Value & FLASH_STATUS_RDY) == FLASH_STATUS_RDY){
                    return( true);                // done
                }
                System.Threading.Thread.Sleep( 1);
            }
            return( false);                     // timeout
        }

        public bool WriteBuffer( ushort Offset, byte[] Data, int Length) {
            // Write data into flash buffer
           if( Length > FLASH_PAGE_SIZE){
               return false;
           }
           if( !CheckConnect()){
              return( false);
           }
           if( !usbSpi.ClrCS()){                 // chipselect
              return( false);
           }
           // prepare command :
           byte[] Buffer = new byte[ FLASH_PAGE_SIZE + 16];
           int Index = 0;
           Buffer[ Index++] = FLASH_BUFFER1_WRITE;
           Buffer[ Index++] = 0;               // don't care
           Buffer[ Index++] = (byte)BufferHighAddress( Offset);
           Buffer[ Index++] = (byte)BufferLowAddress( Offset);
           // data :
           for( int i = 0; i < Length; i++){
              Buffer[ Index++] = Data[ i];
           }
           // send command & data :
           if( !usbSpi.Write( Buffer, Index)){
              return( false);
           }
           if( !usbSpi.SetCS()){                 // deselect
              return( false);
           }
           return( true);
        }

        public bool SaveBuffer( ushort Page) {
            // Save buffer to flash array
           if( !CheckConnect()){
              return( false);
           }
           if( !usbSpi.ClrCS()){                 // chipselect
              return( false);
           }
           // prepare command :
           byte[] Buffer = new byte[ 16];
           int Index = 0;
           Buffer[ Index++] = FLASH_BUFFER1_SAVE;
           Buffer[ Index++] = (byte)PageHighAddress( Page);
           Buffer[ Index++] = (byte)PageLowAddress( Page);
           Buffer[ Index++] = 0;               // don't care
           // send command & data :
           if( !usbSpi.Write( Buffer, Index)){
              return( false);
           }
           if( !usbSpi.SetCS()){                 // deselect
              return( false);
           }
           return( true);
        } // SaveBuffer

        public bool LoadBuffer( ushort Page) {
            // Load buffer from flash array
           if( !CheckConnect()){
              return( false);
           }
           if( !usbSpi.ClrCS()){                 // chipselect
              return( false);
           }
           // prepare command :
           byte[] Buffer = new byte[ 16];
           int Index = 0;
           Buffer[ Index++] = FLASH_PAGE_TO_BUFFER1;
           Buffer[ Index++] = (byte)PageHighAddress( Page);
           Buffer[ Index++] = (byte)PageLowAddress( Page);
           Buffer[ Index++] = 0;               // don't care
           // send command & data :
           if( !usbSpi.Write( Buffer, Index)){
              return( false);
           }
           if( !usbSpi.SetCS()){                 // deselect
              return( false);
           }
           return( true);
        } // LoadBuffer

        public bool ReadArray( ushort Page, ushort Offset, byte[] Data, int Length) {
            // Read data from flash array
           if( !CheckConnect()){
              return( false);
           }
           if( !usbSpi.ClrCS()){                 // chipselect
              return( false);
           }
           // prepare command :
           byte[] Buffer = new byte[ 16];
           int Index = 0;
           Buffer[ Index++] = FLASH_ARRAY_READ;
           Buffer[ Index++] = (byte)PageHighAddress( Page);
           Buffer[ Index++] = (byte)(PageLowAddress( Page) | BufferHighAddress( Offset));
           Buffer[ Index++] = (byte)BufferLowAddress( Offset);
           Buffer[ Index++] = 0;               // additional don't care
           Buffer[ Index++] = 0;               // additional don't care
           Buffer[ Index++] = 0;               // additional don't care
           Buffer[ Index++] = 0;               // additional don't care
           // send command :
           if( !usbSpi.Write( Buffer, Index)){
              return( false);
           }
           // read data :
           if( !usbSpi.Read( Data, Length)){
              return( false);
           }
           if( !usbSpi.SetCS()){                 // deselect
              return( false);
           }
           return( true);
        } // ReadArray

        private bool CheckConnect() {
            // Try connect adapter
           if( usbSpi.IsOpen){
              return( true);                   // already opened
           }
           // open adapter :
           int Identifier;
           if( !usbSpi.Locate( USB_DEVICE_NAME, out Identifier)){
               if( !usbSpi.Locate( USB_DUTCHMAN_NAME, out Identifier)){
                    return( false);
               }
           }
           if( !usbSpi.Open( Identifier)){
              return( false);
           }
           // set SPI parameters :
           usbSpi.Flush();
           usbSpi.SetMode(ClockPolarity.CLOCK_LOW,
                          ReadStrobe.READ_ON_FALL,
                          WriteStrobe.WRITE_ON_FALL);
           if( !usbSpi.SetClockRate( SPI_CLOCK_RATE)){
              return( false);
           }
           // deselect & inactive clock :
           if( !usbSpi.SetCS()){
              return( false);
           }
           return( true);
        } // CheckConnect

        private bool Status( out byte Value) {
            // Read flash status
            Value = 0;
           if( !CheckConnect()){
              return( false);
           }
           if( !usbSpi.ClrCS()){                 // chipselect
              return( false);
           }
           if( !usbSpi.WriteByte( FLASH_STATUS_READ)){
              return( false);
           }
           if( !usbSpi.ReadByte( out Value)){
              return( false);
           }
           if( !usbSpi.SetCS()){                 // deselect
              return( false);
           }
           return( true);
        } // Status


    }
}
