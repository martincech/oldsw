﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace Veit.KeyFilter {
    /// <summary>
    /// Key press filter
    /// </summary>
    static class KeyFilter {
        /// <summary>
        /// Decimal separator for current culture
        /// </summary>
        public static char DecimalSeparator {
            get { return CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator.ToCharArray()[0]; }
        }
        
        /// <summary>
        /// Filter integer number
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>True if the key is valid</returns>
        static public bool IsInt(char key) {
            return key == (char)Keys.Back || Char.IsDigit(key);
        }

        /// <summary>
        /// Filter float number
        /// </summary>
        /// <param name="key">Key to check</param>
        /// <returns>True if the key is valid</returns>
        static public bool IsFloat(char key) {
            return key == (char)Keys.Back || Char.IsDigit(key) || key == DecimalSeparator;
        }
    }
}
