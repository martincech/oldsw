﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veit.ModbusProtocol {
    public class ModbusAscii {
        
        // ':' address (2 char) PDU (char touples) LRC (2 chars) '<CR>''<LF>'

        private const int  FRAME_SIZE    = 3;            // leading/trailing chars
        private const byte LEADING_CHAR  = (byte)':';    // uvodni znak
        private const byte TRAILER1_CHAR = (byte)'\r';   // predposledni znak
        private const byte TRAILER2_CHAR = (byte)'\n';   // posledni znak

        private const int ADDRESS_SIZE = 1;      // byte
        private const int LRC_SIZE     = 1;      // byte

        private const int MIN_SIZE = (FRAME_SIZE + 2 * ADDRESS_SIZE + 2 * LRC_SIZE); // minimalni ramec
        private const int MAX_SIZE = (MIN_SIZE + 2 * ModbusConst.MAX_PDU_SIZE);

        private ModbusCom modbusCom;
        private byte lrc;

        
        public ModbusAscii(ModbusCom modbusCom) {
            this.modbusCom = modbusCom;
        }

        private byte Char2hex(byte ch) {
            return (byte)((ch) <= '9' ? (ch) - '0' : (ch) - 'A' + 10);
        }

        private byte Nibble2hex(byte n) {
            return (byte)((n) > 9 ? 'A' + (n) - 10 : '0' + (n));
        }
        
        private byte Char2dec(byte ch) {
            return (byte)((ch) - '0');
        }

        public bool Receive(out byte[] Pdu, out int Size) {
            // Dekoduje prijaty paket, vraci ukazatel na zacatek <Pdu> a velikost dat <Size>
            int i;
            int j;
            int Count;
            byte ch;
            byte b;

            Size = 0;
            Pdu = null;
            Count = modbusCom.DataSize;
            // minimalni delka ramce :
            if (Count <= MIN_SIZE) {
                return false;
            }
            if (Count > MAX_SIZE) {
                return false;
            }
            // kontrola ramovacich znaku :
            if (modbusCom.Buffer[0] != LEADING_CHAR) {
                return false;
            }
            if (modbusCom.Buffer[Count - 2] != TRAILER1_CHAR) {
                return false;
            }
            if (modbusCom.Buffer[Count - 1] != TRAILER2_CHAR) {
                return false;
            }
            // kontrola delky :
            Count -= FRAME_SIZE;
            if ((Count & 0x01) == 0x01) {
                return false;                     // lichy pocet znaku uvnitr ramu
            }
            // kontrola znaku :
            for (i = 1; i <= Count; i++) {
                if (modbusCom.Buffer[i] < '0' ||
                    modbusCom.Buffer[i] > '9' &&
                    modbusCom.Buffer[i] < 'A' ||
                    modbusCom.Buffer[i] > 'F'){
                    return false;                  // jiny znak nez hexa cislice
                }
            }
            // prevod do binarni podoby :
            i = 1;                              // source char
            Count >>= 1;                        // pocet bytu
            for (j = 0; j < Count; j++) {
                ch  = modbusCom.Buffer[ i++];
                b   = (byte)(Char2hex(ch) << 4);        // high nibble
                ch  = modbusCom.Buffer[i++];
                b  |= Char2hex(ch);             // low nibble
                modbusCom.Buffer[j] = b;
            }
            // kontrola zabezpeceni :
            b = 0;
            for (i = 0; i < Count; i++) {
                b += modbusCom.Buffer[i];
            }
            if (b != 0) {
                return false;                     // jiny znak nez hexa cislice
            }
            Size = Count - ADDRESS_SIZE - LRC_SIZE;
            Pdu = new byte[Size];
            for (i = 1; i <= Size; i++) {
                Pdu[i - 1] = modbusCom.Buffer[i];
            }
            return true;
        }

        public byte GetAddress() {
            // Vrati adresu paketu. POZOR, volat az po MBAReceive
            return modbusCom.Buffer[0];
        }

        public void WriteAddress(byte Address) {
            // Zapise adresu paketu
            modbusCom.DataSize = 0;
            lrc                = 0;
            modbusCom.Buffer[modbusCom.DataSize++] = LEADING_CHAR;
            WriteByte(Address);
        }

        public void WriteByte(byte Data) {
            // Zapise 8bitove slovo do paketu
            modbusCom.Buffer[modbusCom.DataSize++] = Nibble2hex((byte)(Data >> 4));    // high nibble
            modbusCom.Buffer[modbusCom.DataSize++] = Nibble2hex((byte)(Data & 0x0F));  // low  nibble
            lrc += Data;
        }

        public void WriteWord(ushort Data) {
            // Zapise 16bitove slovo do paketu
           WriteByte((byte)(Data >> 8));           // MSB first
           WriteByte((byte)(Data & 0xFF));         // LSB
        }

        public void WriteCrc() {
            // Spocita a zapise zabezpeceni
           lrc = (byte)-lrc;
           WriteByte(lrc);
           modbusCom.Buffer[modbusCom.DataSize++] = TRAILER1_CHAR;
           modbusCom.Buffer[modbusCom.DataSize++] = TRAILER2_CHAR;
        }

    }
}
