﻿using System;
using System.Collections.Generic;
using System.Text;
using Device.Serial;

namespace Veit.ModbusProtocol {

    /// <summary>
    /// Serial communication for both ASCII and RTU modes
    /// </summary>
    public class ModbusCom {

        /// <summary>
        /// Stav zarizeni
        /// </summary>
        public enum ComStatus {
            STOPPED,                 // zastaveno, ceka na povel
            RX_RUNNING,              // bezi Rx
            RX_DONE,                 // dokoncen prijem, prevezmi data
            RX_PARITY,               // dokoncen prijem, chyba parity
            RX_OVERRUN,              // dokoncen prijem, pocet znaku presahl velikost bufferu
            RX_TIMEOUT,              // dokoncen prijem, timeout
            TX_RUNNING,              // bezi Tx
            TX_DONE,                 // dokonceno vysilani
            _LAST
        }

        /// <summary>
        /// Rezim
        /// </summary>
        public enum ComMode : byte {
            NONE,
            TIMEOUT = 0x01,            // meziznakovy timeout
            LEADER  = 0x02,            // vedouci znak
            TRAILER = 0x04,            // ukoncovaci znak
            _LAST
        }
        
        private const int  DATA_SIZE    = 520;  // velikost bufferu
        private const byte LEADER_CHAR  = (byte)':';
        private const byte TRAILER_CHAR = (byte)'\n';

        public byte[] Buffer;                   // datovy buffer
        public int DataSize;                   // delka prijatych/vysilanych dat
        public ComStatus Status;

        private Uart uart;
        private byte mode;                      // Kombinace ComMode
        private int totalTimeout;
        private int intercharacterTimeout;

        /// <summary>
        /// Returns true if the COM port is open
        /// </summary>
        public bool IsOpen { get { return uart.IsOpen; } }

        public ModbusCom(int portNumber, int baudRate, Uart.Parity parity, int dataBits, byte mode) {
            Buffer = new byte[DATA_SIZE];

            uart = new Uart();
            if (uart.Open("COM" + portNumber.ToString())) {
                uart.SetParameters(baudRate, parity, dataBits, Uart.StopBit.One, Uart.Handshake.None);
            }

            this.mode = mode;
            Status    = ComStatus.STOPPED;
        }

        /// <summary>
        /// Set Total Timeout
        /// </summary>
        /// <param name="totalTimeout"></param>
        /// <param name="intercharacterTimeout"></param>
        public void SetTimeouts(int totalTimeout, int intercharacterTimeout) {
           this.totalTimeout          = totalTimeout;
           this.intercharacterTimeout = intercharacterTimeout;
        }

        public void Close() {
            uart.Close();
        }

        /// <summary>
        /// Zahaji vysilani bufferu
        /// </summary>
        public void ComTxStart() {
            if (!uart.IsOpen) {
                return;
            }
            if (DataSize == 0) {
                throw new ArgumentException("DataSize is zero");
            }
            uart.Flush();
            uart.Write(Buffer, DataSize);
            Status = ComStatus.TX_DONE;
        }

        /// <summary>
        /// Zahaji prijem do bufferu
        /// </summary>
        public void ComRxStart() {
            if (!uart.IsOpen) {
                return;
            }

            int size;
            byte[] ch = new byte[1];

            // Smazani bufferu, pouze pro jednodussi diagnostiku, jinak neni treba
            for (int i = 0; i < DATA_SIZE; i++) {
                Buffer[i] = 0;
            }

            DataSize = 0;
            uart.SetRxWait(totalTimeout);   // pocatecni timeout do odpovedi
            size = uart.Read(ch, 1);
            if (size != 1) {
                Status = ComStatus.RX_TIMEOUT;
                return;
            }
            Buffer[DataSize++] = ch[0];
            if ((mode & (byte)ComMode.LEADER) == (byte)ComMode.LEADER) {
                if (ch[0] != LEADER_CHAR) {
                    Status = ComStatus.RX_TIMEOUT;
                    return;
                }
            }
            uart.SetRxWait(intercharacterTimeout);        // meziznakovy timeout
            while (true) {
                if (DataSize == DATA_SIZE) {
                    Status = ComStatus.RX_OVERRUN;
                    return;
                }
                size = uart.Read(ch, 1);
                if (size !=1) {
                    Status = ComStatus.RX_TIMEOUT;
                    return;
                }
                Buffer[DataSize++] = ch[0];
                if ((mode & (byte)ComMode.TRAILER) == (byte)ComMode.TRAILER) {
                    if (ch[0] == TRAILER_CHAR) {
                        Status = ComStatus.RX_DONE;
                        return;
                    }
                }
            }
        }

    }
}
