﻿using System;
using System.Collections.Generic;
using System.Text;
using Veit.ModbusProtocol;

namespace Veit.Bat2Modbus {

    public struct FloatBuilder {
        public ushort Lsb;
        public ushort Msb;
        
        /// <summary>
        /// Create 4-byte float number from 2-byte MSB and LSB
        /// </summary>
        public float Float {
            get {
                byte[] arrayLsb = BitConverter.GetBytes(Lsb);
                byte[] arrayMsb = BitConverter.GetBytes(Msb);
                byte[] array = new byte[4];
                arrayLsb.CopyTo(array, 0);
                arrayMsb.CopyTo(array, 2);
                return BitConverter.ToSingle(array, 0);
            }
        }
    }
    
    /// <summary>
    /// Statistics of one day
    /// </summary>
    public struct Statistics {
        /// <summary>
        /// Date of the day
        /// </summary>
        public DateTimeBuilder DateTimeBuilder;
        
        /// <summary>
        /// Day number
        /// </summary>
        public int DayNumber;

        /// <summary>
        /// Target weight
        /// </summary>
        public float Target;

        /// <summary>
        /// Number of birds weighed
        /// </summary>
        public int Count;

        /// <summary>
        /// Average weight
        /// </summary>
        public float Average;

        /// <summary>
        /// Average from previous day
        /// </summary>
        public float LastAverage;

        /// <summary>
        /// Standard deviation
        /// </summary>
        public float Sigma;

        /// <summary>
        /// Uniformity in %
        /// </summary>
        public int Uniformity;

        /// <summary>
        /// Coefficient of variation
        /// </summary>
        public float Cv;
    }
    
    /// <summary>
    /// Calculations for statistics of one day
    /// </summary>
    public struct StatisticsBuilder {
        private const float MIN_AVERAGE = 1e-03f;
        private const float MIN_SIGMA   = 1e-03f;

        private const float INV_SQRT_2PI = 0.3989422804f;     // 1/(SQRT(2*PI))

        // Konstanty aproximacniho polynomu :
        private const float POLY_P  =  0.231641900f;
        private const float POLY_B1 =  0.319381530f;
        private const float POLY_B2 = -0.356563782f;
        private const float POLY_B3 =  1.781477937f;
        private const float POLY_B4 = -1.821255978f;
        private const float POLY_B5 =  1.330274429f;

        /// <summary>
        /// Final statistic values
        /// </summary>
        public Statistics Statistics;
        
        /// <summary>
        /// Sum of weights
        /// </summary>
        public FloatBuilder XSum;

        /// <summary>
        /// Sum of weights^2
        /// </summary>
        public FloatBuilder X2Sum;
        
        /// <summary>
        /// Real uniformity is used or not
        /// </summary>
        public bool IsRealUniformity;
        
        /// <summary>
        /// Uniformity
        /// </summary>
        public int RealUniformity;

        /// <summary>
        /// Uniformity range in %
        /// </summary>
        public int UniformityRange;

        /// <summary>
        /// Calculate missing statistic values
        /// </summary>
        public void CalcStatistics() {
            float average, sigma;

            average = Statistics.Count == 0 ? 0 : XSum.Float / Statistics.Count;
            sigma   = CalcSigma();

            // Vypocet statistickych hodnot
            Statistics.Cv         = CalcVariation(average, sigma);
            Statistics.Uniformity = IsRealUniformity ? RealUniformity : CalcUniformity(average, sigma);
            Statistics.Average    = average / 1000;   // Prepocet na kg/lb
            Statistics.Sigma      = sigma  / 1000;
        }

        private float CalcSigma() {
            float tmp;

            if (Statistics.Count <= 1) {
                return 0;
            }
            tmp = XSum.Float * XSum.Float / Statistics.Count;
            tmp = X2Sum.Float - tmp;
            if (tmp <= MIN_SIGMA) {
                return 0;
            }
            return (float)Math.Sqrt(1/(float)(Statistics.Count - 1) * tmp);
        }
        
        private float CalcVariation(float average, float sigma) {
           if (average < MIN_AVERAGE) {
                return 0;         // "deleni nulou"
           }
           return sigma / average * 100;
        }

        private int CalcUniformity(float average, float sigma) {
            float x, qx;        // normalizovana odchylka, gaussint

            if (sigma < MIN_SIGMA) {
                return 100;     // "deleni nulou"
            }
            x = (float)UniformityRange / 100.0f * average / sigma; // normalizace absolutni tolerance pomoci sigma
            if( x > 3){
                return 100;                      // >3*sigma, vsechny hodnoty v toleranci
            }
            qx  = GaussIntegral( x, Gauss( x));  // plocha krivky nad pozadovanou mez
            qx *= 2;                             // plocha krivky nad a pod pozadovaou mez
            qx  = 1 - qx;                        // plocha uvnitr mezi
            return (int)(qx * 100.0f);           // v procentech
        }

        float Gauss(float x) {
            return INV_SQRT_2PI * (float)Math.Exp( -x * x / 2);
        }

        float GaussIntegral(float x, float gaussx) {
            // gaussova krivka integrovana od <x> do +oo, <gaussx> je hodnota gaussovy
            // krivky pro <x>
            float t;
            float tn;
            float poly;

            t = 1 /(1 + POLY_P * x); // zakladni mocnina
            poly  = POLY_B1 * t;     // prvni aproximace
            tn    = t*t;             // druha mocnina
            poly += POLY_B2 * tn;
            tn   *= t;               // treti mocnina
            poly += POLY_B3 * tn;
            tn   *= t;               // ctvrta mocnina
            poly += POLY_B4 * tn;
            tn   *= t;               // pata mocnina
            poly += POLY_B5 * tn;
            return gaussx * poly;
        }

    }
    
    /// <summary>
    /// Status of the weighing process
    /// </summary>
    public enum WeighingStatus {
        STOPPED,
        WAITING,
        STARTED,
        PAUSED
    }
    
    /// <summary>
    /// Date and time builder
    /// </summary>
    public struct DateTimeBuilder {
        public int Day;
        public int Month;
        public int Year;
        public int Hour;
        public int Min;
        
        public DateTime DateTime {
            get {
                return new DateTime(Year, Month, Day, Hour, Min, 0);
            }
        }
    }

    /// <summary>
    /// Current state of one scale
    /// </summary>
    public class CurrentState {
        /// <summary>
        /// Maximum lenght of the scale name
        /// </summary>
        private const int ID_MAX_LENGTH = 10;

        /// <summary>
        /// Maximum day number
        /// </summary>
        public const int CURVE_MAX_DAY = 999;

        /// <summary>
        /// Weighing is started
        /// </summary>
        public bool IsStarted;

        /// <summary>
        /// Weighing is waiting for start
        /// </summary>
        public bool IsWaiting;

        /// <summary>
        /// Weighing is paused
        /// </summary>
        public bool IsPaused;

        /// <summary>
        /// Raw scale identification number as an array of characters
        /// </summary>
        public byte[] IdentificationNumber;
        
        /// <summary>
        /// Scale name
        /// </summary>
        public string ScaleName {
            get {
                // Z pole znaku vytvorim string
                StringBuilder stringBuilder = new StringBuilder();
                foreach (byte b in IdentificationNumber) {
                    stringBuilder.Append((char)b);
                }
                return stringBuilder.ToString().Trim();
            }
        }

        /// <summary>
        /// Actual date and time in the scale
        /// </summary>
        public DateTimeBuilder DateTimeBuilder;
        
        /// <summary>
        /// Weighing status
        /// </summary>
        public WeighingStatus Status {
            get {
                if (!IsStarted) {
                    return WeighingStatus.STOPPED;
                }
                if (IsWaiting) {
                    return WeighingStatus.WAITING;
                }
                if (IsPaused) {
                    return WeighingStatus.PAUSED;
                }
                return WeighingStatus.STARTED;
            }
        }

        /// <summary>
        /// Day number of first day of weighing
        /// </summary>
        public int CurveDayShift;
        
        /// <summary>
        /// Statistics of the current day of weighing
        /// </summary>
        public StatisticsBuilder StatisticsBuilder;

        /// <summary>
        /// Constructor
        /// </summary>
        public CurrentState() {
            // Vytvorim pole pro id. cislo
            IdentificationNumber = new byte[ID_MAX_LENGTH];
        }

        /// <summary>
        /// Copy constructor
        /// </summary>
        /// <param name="source">Source</param>
        public CurrentState(CurrentState source) {
            IsStarted         = source.IsStarted;
            IsWaiting         = source.IsWaiting;
            IsPaused          = source.IsPaused;
            IdentificationNumber = new byte[ID_MAX_LENGTH];
            source.IdentificationNumber.CopyTo(IdentificationNumber, 0);
            DateTimeBuilder   = source.DateTimeBuilder;
            CurveDayShift     = source.CurveDayShift;
            StatisticsBuilder = source.StatisticsBuilder;
        }

        /// <summary>
        /// Calculate daily gain
        /// </summary>
        /// <returns>Daily gain</returns>
        public float DailyGain {
            get {
                if (StatisticsBuilder.Statistics.DayNumber == CurveDayShift) {
                    return 0;       // Prvni den je prirustek nulovy
                }
                return StatisticsBuilder.Statistics.Average - StatisticsBuilder.Statistics.LastAverage;
            }
        }
    }
    
    /// <summary>
    /// BAT2 scale connected to a serial port using the MODBUS protocol
    /// </summary>
    public class Bat2Modbus {
        /// <summary>
        /// COM port number
        /// </summary>
        private int portNumber;

        /// <summary>
        /// Scale address in the MODBUS network
        /// </summary>
        private byte address;

        /// <summary>
        /// Modbus object shared by all scales in the network
        /// </summary>
        private Modbus modbus;

        /// <summary>
        /// Counter of all sent messages
        /// </summary>
        public int MessageCounter { get { return messageCounter; } }
        private int messageCounter;

        /// <summary>
        /// Counter of wrong replies
        /// </summary>
        public int ErrorCounter { get { return errorCounter; } }
        private int errorCounter;

        /// <summary>
        /// Current state of the scale
        /// </summary>
        public CurrentState CurrentState;

        /// <summary>
        /// Reading of archive is finished
        /// </summary>
        public bool IsReadFinished { get { return (bool)(modbus.LastError == Modbus.Error.OK && modbus.LastException == ModbusPdu.ExceptionType.ILLEGAL_DATA_VALUE); } }
        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="portNumber"></param>
        /// <param name="address"></param>
        /// <param name="modbus"></param>
        public Bat2Modbus(int portNumber, byte address, Modbus modbus) {
            // Preberu parametry
            this.portNumber = portNumber;
            this.address    = address;
            this.modbus     = modbus;

            // Vytvorim aktualni stav
            CurrentState = new CurrentState();
        }

        /// <summary>
        /// Open COM port prior to any communication
        /// </summary>
        public void Open() {
            modbus.Open(portNumber, address);
        }

        /// <summary>
        /// Close COM port after the communication is finished
        /// </summary>
        public void Close() {
            modbus.Close();
        }

        /// <summary>
        /// Read config
        /// </summary>
        /// <param name="repetitions">Number of attempts</param>
        /// <returns>True if succesfull</returns>
        public bool ReadConfig(int repetitions) {
            return ReadAndDecodeRegisters(Registers.MBREG_CONFIG_VERSION, Registers.MBREG_CONFIG_CORRECTION_CORRECTION, repetitions);
        }

        /// <summary>
        /// Read config
        /// </summary>
        /// <returns>True if succesfull</returns>
        public bool ReadConfig() {
            return ReadConfig(1);
        }

        /// <summary>
        /// Read pause of weighing
        /// </summary>
        /// <param name="repetitions">Number of attempts</param>
        /// <returns>True if succesfull</returns>
        public bool ReadPause(int repetitions) {
            return ReadAndDecodeRegisters(Registers.MBREG_WEIGHING_PAUSE, Registers.MBREG_WEIGHING_PAUSE, repetitions);
        }
        
        /// <summary>
        /// Read pause of weighing
        /// </summary>
        /// <returns>True if succesfull</returns>
        public bool ReadPause() {
            return ReadPause(1);
        }

        /// <summary>
        /// Read current date and time
        /// </summary>
        /// <param name="repetitions">Number of attempts</param>
        /// <returns>True if succesfull</returns>
        public bool ReadDateTime(int repetitions) {
            return ReadAndDecodeRegisters(Registers.MBREG_DATETIME_DAY, Registers.MBREG_DATETIME_MIN, repetitions);
        }

        /// <summary>
        /// Read current date and time
        /// </summary>
        /// <returns>True if succesfull</returns>
        public bool ReadDateTime() {
            return ReadDateTime(1);
        }

        /// <summary>
        /// Read today's statistics
        /// </summary>
        /// <param name="repetitions">Number of attempts</param>
        /// <returns>True if succesfull</returns>
        public bool ReadToday(int repetitions) {
            return WriteAndDecodeArchive(Registers.MBREG_RESULTS_LOAD_TODAY, 0, repetitions);
        }

        /// <summary>
        /// Read today's statistics
        /// </summary>
        /// <returns>True if succesfull</returns>
        public bool ReadToday() {
            return ReadToday(1);
        }

        /// <summary>
        /// Read statistics of the first day in archive
        /// </summary>
        /// <param name="repetitions">Number of attempts</param>
        /// <returns>True if succesfull</returns>
        public bool ReadFirstDay(int repetitions) {
            return WriteAndDecodeArchive(Registers.MBREG_RESULTS_LOAD_FIRST_DAY, 0, repetitions);
        }

        /// <summary>
        /// Read statistics of the first day in archive
        /// </summary>
        /// <returns>True if succesfull</returns>
        public bool ReadFirstDay() {
            return ReadFirstDay(1);
        }

        /// <summary>
        /// Read statistics of the next day
        /// </summary>
        /// <param name="repetitions">Number of attempts</param>
        /// <returns>True if succesfull</returns>
        public bool ReadNextDay(int repetitions) {
            return WriteAndDecodeArchive(Registers.MBREG_RESULTS_LOAD_NEXT_DAY, 0, repetitions);
        }

        /// <summary>
        /// Read statistics of the next day
        /// </summary>
        /// <returns>True if succesfull</returns>
        public bool ReadNextDay() {
            return ReadNextDay(1);
        }

        /// <summary>
        /// Read and decode statistics
        /// </summary>
        /// <returns>True if successful</returns>
        private bool ReadAndDecodeArchive() {
            CurrentState oldCurrentState = CurrentState;        // Zaloha puvodniho stavu

            // Nactu a dekoduju statistiku
            if (modbus.Mode == ModbusPacket.PacketMode.RTU) {
                // V RTU modu muzu nacitat naraz 125 registru, zvladnu to naraz
                if (!ReadAndDecodeRegisters(Registers.MBREG_RESULTS_DAYNUMBER, Registers.MBREG_RESULTS_REALUNIFORMITY_M)) {
                    CurrentState = oldCurrentState;     // Obnovim puvodni hodnotu
                    return false;
                }
            } else {
                // V ASCII modu muzu nacitat naraz jen 61 registru, musim natrikrat
                if (!ReadAndDecodeRegisters(Registers.MBREG_RESULTS_DAYNUMBER, Registers.MBREG_RESULTS_HIST_F_COUNT_39)
                 || !ReadAndDecodeRegisters(Registers.MBREG_RESULTS_HIST_M_CENTER_HI, Registers.MBREG_RESULTS_REALUNIFORMITY_M)) {
                    CurrentState = oldCurrentState;     // Obnovim puvodni hodnotu
                    return false;
                }
            }

            return true;
        }
        
        /// <summary>
        /// Read registers</start>
        /// </summary>
        /// <param name="start">First register</param>
        /// <param name="stop">Last register</param>
        /// <returns>True if successful</returns>
        private bool ReadRegisters(Registers start, Registers stop) {
            // Nactu pozadovane registry
            try {
                modbus.ReadRegisters((ushort)start, (ushort)(stop - start + 1));
            } catch {
                return false;     // Port neni dostupny
            }

            // Pockam na odpoved
            return ReadReply();
        }

        /// <summary>
        /// Read reply from the scale
        /// </summary>
        /// <returns>True if successful</returns>
        private bool ReadReply() {
            // Prictu pocet poslanych zprav
            messageCounter++;

            // Cekam na odpoved
            do {
                try {
                    modbus.Execute();
                } catch {
                    return false;
                }
            } while (!modbus.Finished);

            // Zkontroluju vysledek
            if (modbus.LastException != ModbusPdu.ExceptionType.NONE || modbus.LastError != Modbus.Error.OK) {
                // Doslo k vyjimce nebo chybe
                errorCounter++;
                return false;
            }
            
            // Odpoved jsem nacetl v poradku
            return true;
        }

        /// <summary>
        /// Read registers and decode them into the global struct
        /// </summary>
        /// <param name="start">First register</param>
        /// <param name="stop">Last register</param>
        /// <returns>True if successful</returns>
        private bool ReadAndDecodeRegisters(Registers start, Registers stop) {
            if (!ReadRegisters(start, stop)) {
                return false;
            }
            return DecodeRegisters(start);
        }

        /// <summary>
        /// Read registers with several attempts and decode them into the global struct
        /// </summary>
        /// <param name="start">First register</param>
        /// <param name="stop">Last register</param>
        /// <param name="repetitions">Maximum naumber of attempts</param>
        /// <returns>True if successful</returns>
        private bool ReadAndDecodeRegisters(Registers start, Registers stop, int repetitions) {
            for (int errors = 0; errors < repetitions; errors++) {
                if (ReadAndDecodeRegisters(start, stop)) {
                    return true;        // V poradku
                }
            }

            // Nepodarilo se ani na vicekrat, vaha neodpovida
            return false;
        }

        /// <summary>
        /// Write to one register, then read the archive registers and decode them into the global struct
        /// </summary>
        /// <param name="writeAddress">Address of the write register</param>
        /// <param name="writeValue">Value to be written</param>
        /// <param name="repetitions">Maximum naumber of attempts</param>
        /// <returns>True if successful</returns>
        private bool WriteAndDecodeArchive(Registers writeAddress, ushort writeValue, int repetitions) {
            for (int errors = 0; errors < repetitions; errors++) {
                // Zapisu prikaz
                if (!WriteRegister(writeAddress, writeValue)) {
                    continue;       // Zkusim o znovu
                }

                // Nactu a dekoduju statistiku
                if (ReadAndDecodeArchive()) {
                    // Podarilo se, vypoctu kompletni statistiku a koncim
                    CurrentState.StatisticsBuilder.CalcStatistics();
                    return true;
                }
            }

            // Nepodarilo se ani na vicekrat, vaha neodpovida
            return false;
        }

        /// <summary>
        /// Write register
        /// </summary>
        /// <param name="address">Register addres</param>
        /// <param name="value">Value to be written</param>
        /// <returns>True if successful</returns>
        private bool WriteRegister(Registers address, ushort value) {
            try {
                modbus.WriteSingleRegister((ushort)address, value);
            } catch {
                return false;       // Port neni dostupny
            }

            // Pockam na odpoved
            return ReadReply();
        }

        /// <summary>
        /// Write values to a list of registers
        /// </summary>
        /// <param name="start">Address of the first register</param>
        /// <param name="stop">Address of the last register</param>
        /// <param name="data">Array of values to be written</param>
        /// <returns>True if successful</returns>
        private bool WriteRegisters(Registers start, Registers stop, ushort[] data) {
            // Zapisu pozadovane registry
            try {
                modbus.WriteRegisters((ushort)start, (ushort)(stop - start + 1), data);
            } catch {
                return false;       // Port neni dostupny
            }

            // Pockam na odpoved
            return ReadReply();
        }

        /// <summary>
        /// Decode registers
        /// </summary>
        /// <returns></returns>
        private bool DecodeRegisters(Registers startAddress) {
            if (modbus.ReadDataCount == 0) {
                return false;                               // Nejsou nacteny zadne registry
            }

            CurrentState oldCurrentScale = CurrentState;    // Zaloha soucasneho stavu
            for (int i = 0; i < modbus.ReadDataCount; i++) {
                if (!DecodeRegister((Registers)startAddress, modbus.ReadData[i])) {
                    CurrentState = oldCurrentScale;         // Obnovim data (dekodovanim predchozich registru jsem data castecne modifikoval)
                    return false;
                }
                startAddress++;
            }
            return true;
        }


        /// <summary>
        /// Check letter in identification number
        /// </summary>
        /// <param name="ch">Letter</param>
        /// <returns>True if valid</returns>
        private bool CheckIdLetter(byte ch) {
            if (ch >= '0' && ch <= '9') {
                return true;
            }
            if (ch == ' ') {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check range of register value
        /// </summary>
        /// <param name="value">Register value</param>
        /// <param name="min">Minimum value</param>
        /// <param name="max">Maximum value</param>
        /// <returns></returns>
        private bool CheckRange(ushort value, ushort min, ushort max) {
            return (bool)(value >= min && value <= max);
        }

        /// <summary>
        /// Get LSB byte from word
        /// </summary>
        /// <param name="data">Word number</param>
        /// <returns>LSB</returns>
        private byte GetWordLsb(ushort data) {
            return (byte)(data & 0x00FF);
        }

        /// <summary>
        /// Get MSB byte from word
        /// </summary>
        /// <param name="data">Word number</param>
        /// <returns>MSB</returns>
        private byte GetWordMsb(ushort data) {
            return (byte)(data >> 8);
        }

        /// <summary>
        /// Decode one register
        /// </summary>
        /// <param name="address">Register address</param>
        /// <param name="value">Register value</param>
        /// <returns>True if successful</returns>
        private bool DecodeRegister(Registers address, ushort value) {
            // Vylouceni histogramu
            if (address >= Registers.MBREG_RESULTS_HIST_F_CENTER_HI && address <= Registers.MBREG_RESULTS_HIST_M_COUNT_39) {
                return true;
            }
            
            // Otocim LSB a MSB u hodnoty
            value = Endian.Endian.SwapUInt16(value);

          switch(address) {

            // Konfigurace -----------------------------------------------------------------------------
            case Registers.MBREG_CONFIG_VERSION:
            case Registers.MBREG_CONFIG_BUILD:
            case Registers.MBREG_CONFIG_HWVERSION:
              return true;
            case Registers.MBREG_CONFIG_ID_0:
            case Registers.MBREG_CONFIG_ID_1:
            case Registers.MBREG_CONFIG_ID_2:
            case Registers.MBREG_CONFIG_ID_3:
            case Registers.MBREG_CONFIG_ID_4:
                if (!CheckIdLetter(GetWordMsb(value)) || !CheckIdLetter(GetWordLsb(value))) {
                    return false;
                }
                CurrentState.IdentificationNumber[2 * (address - Registers.MBREG_CONFIG_ID_0)]     = GetWordMsb(value);
                CurrentState.IdentificationNumber[2 * (address - Registers.MBREG_CONFIG_ID_0) + 1] = GetWordLsb(value);
              return true;
            case Registers.MBREG_CONFIG_LANGUAGE:
            case Registers.MBREG_CONFIG_MARGIN_ABOVE_FEMALES:
            case Registers.MBREG_CONFIG_MARGIN_ABOVE_MALES:
            case Registers.MBREG_CONFIG_MARGIN_BELOW_FEMALES:
            case Registers.MBREG_CONFIG_MARGIN_BELOW_MALES:
            case Registers.MBREG_CONFIG_FILTER:
            case Registers.MBREG_CONFIG_STABILIZATIONRANGE:
            case Registers.MBREG_CONFIG_STABILIZATIONTIME:
            case Registers.MBREG_CONFIG_AUTOMODE:
            case Registers.MBREG_CONFIG_JUMPMODE:
            case Registers.MBREG_CONFIG_UNITS:
            case Registers.MBREG_CONFIG_HISTOGRAMRANGE:
                return true;
            case Registers.MBREG_CONFIG_UNIFORMITYRANGE:
                CurrentState.StatisticsBuilder.UniformityRange = value;
                return true;
            case Registers.MBREG_CONFIG_WS_RUNNING:
              CurrentState.IsStarted = (bool)(value > 0);
              return true;
            case Registers.MBREG_CONFIG_WS_WAITING:
              CurrentState.IsWaiting = (bool)(value > 0);
              return true;
            case Registers.MBREG_CONFIG_WS_USEFLOCK:
            case Registers.MBREG_CONFIG_WS_CURRENTFLOCK:
              return true;
            case Registers.MBREG_CONFIG_WS_CURVEDAYSHIFT:
                if (!CheckRange(value, 0, CurrentState.CURVE_MAX_DAY)) {
                    return false;
                }
                CurrentState.CurveDayShift = value;
                return true;
            case Registers.MBREG_CONFIG_WS_DATETIME_HOUR:
            case Registers.MBREG_CONFIG_WS_DATETIME_MIN:
            case Registers.MBREG_CONFIG_WS_DATETIME_DAY:
            case Registers.MBREG_CONFIG_WS_DATETIME_MONTH:
            case Registers.MBREG_CONFIG_WS_DATETIME_YEAR:
            case Registers.MBREG_CONFIG_WS_QW_USEBOTHGENDERS:
            case Registers.MBREG_CONFIG_WS_QW_INITIALWFEMALES:
            case Registers.MBREG_CONFIG_WS_QW_INITIALWMALES:
            case Registers.MBREG_CONFIG_WS_UNIFORMITYRANGE:
            case Registers.MBREG_CONFIG_WS_ONLINE:
            case Registers.MBREG_CONFIG_BACKLIGHT:
            case Registers.MBREG_CONFIG_RS485_ADDRESS:
            case Registers.MBREG_CONFIG_RS485_SPEED_HI:
            case Registers.MBREG_CONFIG_RS485_SPEED_LO:
            case Registers.MBREG_CONFIG_RS485_PARITY:
            case Registers.MBREG_CONFIG_RS485_REPLYDELAY:
            case Registers.MBREG_CONFIG_RS485_SILENTINTERVAL:
            case Registers.MBREG_CONFIG_RS485_PROTOCOL:
            case Registers.MBREG_CONFIG_COMPARISONFLOCK:
            case Registers.MBREG_CONFIG_CORRECTION_DAY1:
            case Registers.MBREG_CONFIG_CORRECTION_DAY2:
            case Registers.MBREG_CONFIG_CORRECTION_CORRECTION:

            // Hejna -----------------------------------------------------------------------------
            // Nacitam hejno, ktere jsem si naposledy vyzadal
            case Registers.MBREG_FLOCK_HEADER_NUMBER:
            case Registers.MBREG_FLOCK_HEADER_TITLE_1:
            case Registers.MBREG_FLOCK_HEADER_TITLE_2:
            case Registers.MBREG_FLOCK_HEADER_TITLE_3:
            case Registers.MBREG_FLOCK_HEADER_TITLE_4:
            case Registers.MBREG_FLOCK_HEADER_USECURVES:
            case Registers.MBREG_FLOCK_HEADER_USEBOTHGENDERS:
            case Registers.MBREG_FLOCK_HEADER_WEIGHFROM:
            case Registers.MBREG_FLOCK_HEADER_WEIGHTILL:
                return true;

            // Datum a cas -----------------------------------------------------------------------------
            case Registers.MBREG_DATETIME_DAY:
                if (!CheckRange(value, 1, 31)) {
                    return false;
                }
                CurrentState.DateTimeBuilder.Day = value;
                return true;
            case Registers.MBREG_DATETIME_MONTH:
                if (!CheckRange(value, 1, 12)) {
                    return false;
                }
                CurrentState.DateTimeBuilder.Month = value;
                return true;
            case Registers.MBREG_DATETIME_YEAR:
                if (!CheckRange(value, 1, 3000)) {
                    return false;
                }
                CurrentState.DateTimeBuilder.Year = value;
                return true;
            case Registers.MBREG_DATETIME_HOUR:
                if (!CheckRange(value, 0, 23)) {
                    return false;
                }
                CurrentState.DateTimeBuilder.Hour = value;
                return true;
            case Registers.MBREG_DATETIME_MIN:
                if (!CheckRange(value, 0, 59)) {
                    return false;
                }
                CurrentState.DateTimeBuilder.Min = value;
                return true;

            // Stav vazeni -----------------------------------------------------------------------------
            case Registers.MBREG_WEIGHING_PAUSE:
              CurrentState.IsPaused = (bool)(value > 0);
              return true;

            // Vysledky vazeni -----------------------------------------------------------------------------
            case Registers.MBREG_RESULTS_DAYNUMBER:
                if (!CheckRange(value, 0, CurrentState.CURVE_MAX_DAY)) {
                    return false;
                }
                CurrentState.StatisticsBuilder.Statistics.DayNumber = value;
                return true;
            case Registers.MBREG_RESULTS_DATETIME_DAY:
                if (!CheckRange(value, 1, 31)) {
                    return false;
                }
                CurrentState.StatisticsBuilder.Statistics.DateTimeBuilder.Day = value;
                return true;
            case Registers.MBREG_RESULTS_DATETIME_MONTH:
                if (!CheckRange(value, 1, 12)) {
                    return false;
                }
                CurrentState.StatisticsBuilder.Statistics.DateTimeBuilder.Month = value;
                return true;
            case Registers.MBREG_RESULTS_DATETIME_YEAR:
                if (!CheckRange(value, 1, 3000)) {
                    return false;
                }
                CurrentState.StatisticsBuilder.Statistics.DateTimeBuilder.Year = value;
                return true;
            case Registers.MBREG_RESULTS_DATETIME_HOUR:
                if (!CheckRange(value, 0, 23)) {
                    return false;
                }
                CurrentState.StatisticsBuilder.Statistics.DateTimeBuilder.Hour = value;
                return true;
            case Registers.MBREG_RESULTS_DATETIME_MIN:
                if (!CheckRange(value, 0, 59)) {
                    return false;
                }
                CurrentState.StatisticsBuilder.Statistics.DateTimeBuilder.Min = value;
                return true;
            case Registers.MBREG_RESULTS_STAT_F_XSUM_HI:
                CurrentState.StatisticsBuilder.XSum.Msb = value;
                return true;
            case Registers.MBREG_RESULTS_STAT_F_XSUM_LO:
                CurrentState.StatisticsBuilder.XSum.Lsb = value;
                return true;
            case Registers.MBREG_RESULTS_STAT_F_X2SUM_HI:
                CurrentState.StatisticsBuilder.X2Sum.Msb = value;
                return true;
            case Registers.MBREG_RESULTS_STAT_F_X2SUM_LO:
                CurrentState.StatisticsBuilder.X2Sum.Lsb = value;
                return true;
            case Registers.MBREG_RESULTS_STAT_F_COUNT:
                CurrentState.StatisticsBuilder.Statistics.Count = value;
                return true;
            case Registers.MBREG_RESULTS_STAT_M_XSUM_HI:
            case Registers.MBREG_RESULTS_STAT_M_XSUM_LO:
            case Registers.MBREG_RESULTS_STAT_M_X2SUM_HI:
            case Registers.MBREG_RESULTS_STAT_M_X2SUM_LO:
            case Registers.MBREG_RESULTS_STAT_M_COUNT:
                return true;
            case Registers.MBREG_RESULTS_LASTAVERAGE_F:
                CurrentState.StatisticsBuilder.Statistics.LastAverage = (float)value / 1000.0f;       // Prepocet na kg/lb
                return true;
            case Registers.MBREG_RESULTS_LASTAVERAGE_M:
                return true;
            case Registers.MBREG_RESULTS_TARGETWEIGHT_F:
                CurrentState.StatisticsBuilder.Statistics.Target = (float)value / 1000.0f;            // Prepocet na kg/lb
                return true;
            case Registers.MBREG_RESULTS_TARGETWEIGHT_M:
                return true;
            case Registers.MBREG_RESULTS_REALUNIFORMITYUSED:
                CurrentState.StatisticsBuilder.IsRealUniformity = (bool)(value > 0);
                return true;
            case Registers.MBREG_RESULTS_REALUNIFORMITY_F:
                CurrentState.StatisticsBuilder.RealUniformity = value;
                return true;
            case Registers.MBREG_RESULTS_REALUNIFORMITY_M:

            // Kalibrace -----------------------------------------------------------------------------
            case Registers.MBREG_CAL_ZEROCAL_HI:
            case Registers.MBREG_CAL_ZEROCAL_LO:
            case Registers.MBREG_CAL_RANGECAL_HI:
            case Registers.MBREG_CAL_RANGECAL_LO:
            case Registers.MBREG_CAL_RANGE_HI:
            case Registers.MBREG_CAL_RANGE_LO:
            case Registers.MBREG_CAL_DIVISION:
                return true;

            default:
              return false;

          }//switch
        }



    }
}
