//******************************************************************************
//
//   Counter.h    Zpracovani data a casu
//   Version 0.0  (c) Vymos
//
//******************************************************************************

#ifndef __Counter_H
   #define __Counter_H

#ifndef __Uni_H
   #include "Uni.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

void CounterInit( void);
// Inicializuje modul, je zarazena automaticky do startup

void CounterToDateTime( dword Counter, int *Year, int *Month, int *Day,
                        int *Hour, int *Min, int *Sec, int *IsDst);
// Prevede interni reprezentaci casu na datum a cas

dword CounterFromDateTime( int Year, int Month, int Day, int Hour, int Min, int Sec);
// Prevede datum na interni reprezentaci

#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
#ifndef __DISABLE_DELPHI__
  #include <system.hpp>           // AnsiString

   AnsiString DTString( TDateTime dt);
   // Konvertuje TDateTime na lokalizovany retezec

   AnsiString DTUnifiedString( TDateTime dt);
   // Konvertuje TDateTime na unifikovany retezec

   TDateTime  DTUnifiedDateTime( AnsiString Date);
   // Konvertuje unifikovany retezec na TDateTime
#endif
#endif

#endif