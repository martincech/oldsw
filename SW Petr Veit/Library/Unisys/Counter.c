//******************************************************************************
//
//   Counter.c    Zpracovani data a casu
//   Version 0.0  (c) Vymos
//
//******************************************************************************

#define __DEFINE_BOOL__
#include "uni.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// Nastaveni prechodu :

#define DST_START_MONTH  3
#define DST_START_WEEK   5
#define DST_START_DOW    0       // Sunday
#define DST_START_HOUR   1
#define DST_BIAS         3600    // posun letniho casu

#define DST_END_MONTH    10
#define DST_END_WEEK     5
#define DST_END_DOW      0       // Sunday
#define DST_END_HOUR     1

// pri prechodu na letni cas CounterFromDateTime nezna cas mezi 02:00:00 a
// 02:59:59. Pri prechodu z letniho na zimni cas >=02:00:00 interpretuje jako
// zimni. Prekryta oblast mezi 02:00:00 a 03:00:00 je jako letni cas nedostupna


// dny v mesici :
static char MDays[12] = {
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
};

static bool TimeIsDst( int Year, int Month, int Day, int Hour);
// Vrati YES, je-li letni cas. Vstupni parametry jsou GMT

static int GetTransitionDay( int Year, int Month, int Week, int Dow);
// vrati datum (den v mesici), na ktery pripada prechod

//******************************************************************************
// Inicializace
//******************************************************************************

void CounterInit( void)
// Inicializuje modul
{
   putenv( "TZ=SEC-1");
   tzset();
} // CounterInit

// Automaticka inicializace :
#pragma startup CounterInit 255

//******************************************************************************
// Prevod casu
//******************************************************************************

void CounterToDateTime( dword Counter, int *Year, int *Month, int *Day, int *Hour, int *Min, int *Sec, int *IsDst)
// Prevede interni reprezentaci casu na datum a cas
{
struct tm *tm;

   tm    = localtime( &(long)Counter);
   if( tm == NULL){
      return;
   }
   set_nonull( IsDst, NO);
   *Sec   = tm->tm_sec;
   *Min   = tm->tm_min;
   *Hour  = tm->tm_hour;
   *Day   = tm->tm_mday;
   *Month = tm->tm_mon  + 1;
   *Year  = tm->tm_year + 1900;
   // uprav na GMT a zjisti letni cas
   if( !TimeIsDst( tm->tm_year+1900, tm->tm_mon+1, tm->tm_mday, tm->tm_hour-1)){
      return;                     // zimni cas
   }
   // letni cas
   Counter += DST_BIAS;           // koriguj na letni cas
   tm    = localtime( &(long)Counter);
   set_nonull( IsDst, YES);
   *Sec   = tm->tm_sec;
   *Min   = tm->tm_min;
   *Hour  = tm->tm_hour;
   *Day   = tm->tm_mday;
   *Month = tm->tm_mon  + 1;
   *Year  = tm->tm_year + 1900;
} // CounterToDateTime

//******************************************************************************
// Prevod casu
//******************************************************************************

dword CounterFromDateTime( int Year, int Month, int Day, int Hour, int Min, int Sec)
// Prevede datum na interni reprezentaci
{
struct tm tm;
time_t Counter;

   // uprav na GMT a zjisti letni cas :
   if( TimeIsDst( Year, Month, Day, Hour-1)){
      Hour -= DST_BIAS / 3600;
   }
   tm.tm_sec   = Sec;
   tm.tm_min   = Min;
   tm.tm_hour  = Hour;
   tm.tm_mday  = Day;
   tm.tm_mon   = Month - 1;
   tm.tm_year  = Year  - 1900;
   tm.tm_isdst = 0;
   Counter = mktime( &tm);
   if( Counter < 0){
      return( 0);
   }
   return( Counter);
} // CounterFromDateTime

//******************************************************************************
// Letni cas
//******************************************************************************

static bool TimeIsDst( int Year, int Month, int Day, int Hour)
// Vrati YES, je-li letni cas. Vstupni parametry jsou GMT
{
int StartDay;

   if( Year < 1970){
      return( NO);
   }
   if( Month < DST_START_MONTH || Month > DST_END_MONTH){
      return( NO);     // mimo letni cas
   }
   if( Month > DST_START_MONTH && Month < DST_END_MONTH){
      return( YES);      // zcela jiste letni cas
   }
   if( Month == DST_START_MONTH){
      // zahajeni letniho casu
      StartDay = GetTransitionDay( Year, DST_START_MONTH, DST_START_WEEK, DST_START_DOW);
      if( Day > StartDay){
         return( YES);   // den po zahajeni
      }
      if( Day < StartDay){
         return( NO);  // den pred zahajenim
      }
      // v den prechodu :
      if( Hour >= DST_START_HOUR){
         return( YES);   // hodina po prechodu
      } else {
         return( NO);  // hodina pred prechodem
      }
   } else { // Month == DST_END_MONTH
      // konec letniho casu
      StartDay = GetTransitionDay( Year, DST_END_MONTH, DST_END_WEEK, DST_END_DOW);
      if( Day > StartDay){
         return( NO);   // den po ukonceni
      }
      if( Day < StartDay){
         return( YES);    // den pred ukoncenim
      }
      // v den prechodu :
      if( Hour >= DST_END_HOUR){
         return( NO);   // hodina po prechodu
      } else {
         return( YES);    // hodina pred prechodem
      }
   }
} // TimeIsDst

//******************************************************************************
// Prechodovy den
//******************************************************************************

static int GetTransitionDay( int Year, int Month, int Week, int Dow)
// vrati datum (den v mesici), na ktery pripada prechod
{
int BeginOfMonth;
int Day;
struct tm tm;
struct tm *ptm;
time_t now;

   // urci prvni den v mesici
   tm.tm_sec    = 0;
   tm.tm_min    = 0;
   tm.tm_hour   = 12;               // ignoruj casovou zonu a DST
   tm.tm_mday   = 1;
   tm.tm_mon    = Month - 1;
   tm.tm_year   = Year  - 1900;
   tm.tm_isdst  = 0;
   now = mktime( &tm);
   ptm = gmtime( &now);
   BeginOfMonth = ptm->tm_wday;     // 0-Ne, 1-Po...6-So
   Day = Dow -  BeginOfMonth + 1;   // datum pozadovaneho dne v tydnu
   Day = Day <= 0 ? Day + 7 : Day;  // pretok pres nedeli
   if( Week < 5){
      Day += (Week - 1) * 7;        // pozadovany tyden
   } else {
      // Korekce pro tyden = 5
      Day += (Week - 1) * 7;        // pozadovany tyden
      if( Day > MDays[ Month - 1]){
         //!!! na prechodny unor kasleme
         Day -= 7;
      }
   }
   return( Day);
} // GetTransitionDay



