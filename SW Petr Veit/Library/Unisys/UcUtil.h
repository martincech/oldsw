//******************************************************************************
//
//   UcUtil.h     uC Date / Time computing
//   Version 0.0  (c) Vymos
//
//******************************************************************************

#ifndef __UcUtil_H
   #define __UcUtil_H

#ifndef __Uni_H
   #include "uni.h"
#endif

// Konvence :

// - den, podle poctu dnu v mesici
// - mesic, 1..12
// - rok, Year - 2000

// Datove typy :

typedef word  TUcDate;        // ulozeni data
typedef word  TUcMinTime;     // cas s presnosti na minuty
typedef dword TUcSecTime;     // cas s presnosti na sekundy

// Konstanty :

#define UC_MIN_TIME_DAY      (24 * 60)      // jeden den v minutovem case
#define UC_SEC_TIME_DAY      (24 * 3600)    // jeden den v sekundovem case
#define UC_INVALID_DATE      0xFFFF         // neplatne datum v interni reprezentaci

// dny v tydnu :

typedef enum {
   UC_MONDAY,
   UC_TUESDAY,
   UC_WEDNESDAY,
   UC_THURSDAY,
   UC_FRIDAY,
   UC_SATURDAY,
   UC_SUNDAY
} TUcDow;

// mesice :

typedef enum {
   UC_JANUARY = 1,
   UC_FEBRUARY,
   UC_MARCH,
   UC_APRIL,
   UC_MAY,
   UC_JUNE,
   UC_JULE,
   UC_AUGUST,
   UC_SEPTEMBER,
   UC_OCTOBER,
   UC_NOVEMBER,
   UC_DECEMBER
} TUcMonths;


// Makra :

#define UcMonthDay( m)      _UcMDay[ (m) - 1]     // pocet dnu v mesici
#define UcYearDay( m)       _UcYDay[ (m) - 1]     // dnu od zacatku roku
#define UcIsLeapYear( y)    (!((y) & 0x03))       // prestupny rok

// Globalni data :

extern byte _UcMDay[];
extern word _UcYDay[];

TUcDate UcDate( byte Year, byte Month, byte Day);
// Prepocita datum na interni reprezentaci

void UcSplit( TUcDate Date, byte *Year, byte *Month, byte *Day);
// Prevede interni reprezentaci na datum

byte UcDow( TUcDate Date);
// Vrati den v tydnu

bool UcIsDst( byte Year, byte Month, byte Day, byte Hour);
// Vrati YES pro letni cas. Vstupni hodnoty jsou zimni cas

#endif
   